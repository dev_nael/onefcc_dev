'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class MWorkFlow_Progress
    Public Property PK_MWorkflow_Progress_ID As Integer
    Public Property FK_Module_ID As Nullable(Of Integer)
    Public Property FK_Unik_ID As String
    Public Property FK_MWorkflow_ID As Nullable(Of Integer)
    Public Property WorkflowLevel As Nullable(Of Integer)
    Public Property FK_MWorkFlowUserType_ID As Nullable(Of Integer)
    Public Property FK_MWorkflowRole_ID As Nullable(Of Integer)
    Public Property FK_MWorkflowUser_ID As Nullable(Of Integer)
    Public Property FK_MWorkflow_Status_ID As Nullable(Of Integer)
    Public Property SLAType As String
    Public Property SLAValue As Nullable(Of Integer)
    Public Property BreachSLAAction As String
    Public Property BreachSLAActionLevelTo As Nullable(Of Integer)
    Public Property RevisedActionLevelTo As Nullable(Of Integer)
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class
