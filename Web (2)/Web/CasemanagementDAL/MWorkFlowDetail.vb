'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class MWorkFlowDetail
    Public Property PK_WorkFlowDetail As Integer
    Public Property FK_WorkFlow As Integer
    Public Property RoleID As Integer
    Public Property UserType As Integer
    Public Property Level As Integer
    Public Property SLAType As Integer
    Public Property SLAValue As Integer
    Public Property BreachSLAAction As Integer
    Public Property BreachSLAActionTo As Integer
    Public Property RevisedActionTo As Integer
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class
