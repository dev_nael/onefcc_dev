﻿<Serializable()>
Public Class IFTIDataBLL

    Public objIFTI As NawaDevDAL.IFTI
    Public objIFTIBeneficiary As List(Of NawaDevDAL.IFTI_Beneficiary)
    Public objIFTIErrorDescription As List(Of NawaDevDAL.IFTI_ErrorDescription)

End Class
