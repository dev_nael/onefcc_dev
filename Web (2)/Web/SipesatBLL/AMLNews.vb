﻿Imports Ext.Net
Imports NawaDevDAL
Public Class AMLNews
    Implements IDisposable
    Shared Function getobjNews(id As Integer) As AML_News
        Dim objNews As New AML_News
        Using objdb As New NawaDatadevEntities
            Dim Employee As AML_News = objdb.AML_News.Where(Function(x) x.PK_AML_News_ID = id).FirstOrDefault
            If Employee IsNot Nothing Then
                objNews = Employee
            End If
        End Using
        Return objNews
    End Function

    Shared Function getRole(id As Integer) As MRole
        Dim objRole As New MRole
        Using objdb As New NawaDatadevEntities
            Dim Role As MRole = objdb.MRoles.Where(Function(x) x.PK_MRole_ID = id).FirstOrDefault
            If Role IsNot Nothing Then
                objRole = Role
            End If
        End Using
        Return objRole
    End Function

    Public Shared Sub DeleteTanpaApproval(objNews As AML_News, objModule As NawaDAL.Module)
        Using objDb As New NawaDatadevEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try
                    objDb.Entry(objNews).State = Entity.EntityState.Deleted
                    objDb.SaveChanges()

                    'AuditTrail
                    Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Delete, objModule.ModuleLabel)

                    'AuditTrailDetail
                    NawaFramework.CreateAuditTrailDetailDelete(objDb, header.PK_AuditTrail_ID, objNews)
                    'If objListUserAddress.Count > 0 Then
                    '    For Each item As RCM_UserAddress In objListUserAddress
                    '        NawaFramework.CreateAuditTrailDetailDelete(objDb, header.PK_AuditTrail_ID, item)
                    '    Next
                    'End If
                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()

                    Throw ex
                End Try
            End Using
        End Using
    End Sub
    Public Shared Sub DeleteWithApproval(objNews As AML_News, objModule As NawaDAL.Module)
        Using objDb As New NawaDatadevEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try
                    'Dim alternateby As String = ""
                    'If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                    '    alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    'End If

                    'objUser.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    'objUser.CreatedDate = Now

                    'If objListUserAddress.Count > 0 Then
                    '    For Each useraddress As RCM_UserAddress In objListUserAddress
                    '        useraddress.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    '        useraddress.CreatedDate = Now
                    '    Next
                    'End If

                    Dim objdataNews As New AMLNewsClass
                    With objdataNews
                        .objdataNews = objNews
                        '.objlistdataUserAddress = objListUserAddress
                    End With

                    Dim xmldata As String = NawaBLL.Common.Serialize(objdataNews)
                    'Dim xmldataOld As String = NawaBLL.Common.Serialize(getObjUser(objUser.PK_User_ID))
                    Dim objModuleApproval As New ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = objNews.PK_AML_News_ID
                        .ModuleField = xmldata
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objDb.Entry(objModuleApproval).State = Entity.EntityState.Added

                    'AuditTrail
                    Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval, NawaBLL.Common.ModuleActionEnum.Delete, objModule.ModuleLabel)

                    'AuditTrailDetail
                    NawaFramework.CreateAuditTrailDetailDelete(objDb, header.PK_AuditTrail_ID, objNews)
                    'If objListUserAddress.Count > 0 Then
                    '    For Each item As RCM_UserAddress In objListUserAddress
                    '        NawaFramework.CreateAuditTrailDetailDelete(objDb, header.PK_AuditTrail_ID, item)
                    '    Next
                    'End If

                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub SettingColor(formPanelOld As FormPanel, formPanelNew As FormPanel, unikkeyold As String, unikkeynew As String)


        'Dim objModuleData As NawaDAL.Module = ModuleBLL.Deserialize(objdata, GetType(NawaDAL.Module))
        'Dim objModuleDataBefore As NawaDAL.Module = ModuleBLL.Deserialize(objdata, GetType(NawaDAL.Module))
        Dim objdisplayOld As DisplayField
        Dim objdisplayNew As DisplayField

        Dim listfieldnews As List(Of String) = New List(Of String)
        listfieldnews.Add("PK_AML_NEWS_ID")
        listfieldnews.Add("Title")
        listfieldnews.Add("News_Content")
        listfieldnews.Add("News_Content_Short")
        listfieldnews.Add("News_Image")
        listfieldnews.Add("Attachment_Name")
        listfieldnews.Add("Valid_From")
        listfieldnews.Add("Valid_To")
        listfieldnews.Add("CreatedBy")
        'objSchemaModuleField = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(objSchemaModule.PK_Module_ID).OrderBy(Function(y) y.Sequence).ToList
        For Each item As String In listfieldnews
            objdisplayOld = formPanelOld.FindControl(item & unikkeynew)
            objdisplayNew = formPanelNew.FindControl(item & unikkeyold)
            If Not objdisplayOld Is Nothing And Not objdisplayNew Is Nothing Then

                If objdisplayOld.Text <> objdisplayNew.Text Then
                    objdisplayOld.FieldStyle = "Color:red"
                    objdisplayNew.FieldStyle = "Color:red"
                End If
            End If
        Next

        'For Each item As ModuleField In objSchemaModuleField.Where(Function(x) x.FK_FieldType_ID = Common.MFieldType.VARCHARValue And x.SizeField > 200)
        '        Dim objdisplayOldArea As Ext.Net.TextArea = objPanelOld.FindControl("DspArea" & item.FieldName & unikkeyold)
        '        Dim objdisplayNewArea As Ext.Net.TextArea = objPanelNew.FindControl("DspArea" & item.FieldName & unikkeynew)

        '        If Not objdisplayOldArea Is Nothing And Not objdisplayNewArea Is Nothing Then

        '            If objdisplayOldArea.Text <> objdisplayNewArea.Text Then
        '                objdisplayOldArea.FieldStyle = "Color:red"
        '                objdisplayNewArea.FieldStyle = "Color:red"
        '            End If
        '        End If
        '    Next





    End Sub

    Public Shared Sub SaveAddTanpaApproval(objNews As NawaDevDAL.AML_News, objModule As NawaDAL.Module)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try

                    Dim dateFormat As String = "dd-MM-yyyy HH:mm:ss"

                    Dim objSysParamDateFormat As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(37)

                    If objSysParamDateFormat IsNot Nothing Then
                        dateFormat = objSysParamDateFormat.SettingValue
                    End If

                    objNews.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objNews.CreatedDate = Now
                    objNews.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objNews.LastUpdateDate = Now
                    objNews.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objNews.ApprovedDate = Now

                    objDb.Entry(objNews).State = Entity.EntityState.Added
                    objDb.SaveChanges()

                    'AuditTrail
                    Dim header As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                    'AuditTrailDetail
                    NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, objNews)

                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()

                    Throw ex
                End Try
            End Using
        End Using
    End Sub
    Public Shared Sub SaveAddWithApproval(objNews As AML_News, objModule As NawaDAL.Module)
        Using objDb As New NawaDatadevEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try
                    Dim alternateby As String = ""
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    objNews.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objNews.CreatedDate = Now

                    'AuditTrail
                    Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                    'AuditTrailDetail
                    NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, objNews)

                    Dim objdataNews As New AMLNewsClass
                    With objdataNews
                        .objdataNews = objNews
                    End With

                    Dim xmldata As String = NawaBLL.Common.Serialize(objdataNews)
                    'Dim xmldataOld As String = NawaBLL.Common.Serialize(getObjUser(objUser.PK_User_ID))
                    Dim objModuleApproval As New ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = 0 '---- Module Key ????' -----
                        .ModuleField = xmldata
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objDb.Entry(objModuleApproval).State = Entity.EntityState.Added

                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                End Try
            End Using
        End Using
    End Sub
    Public Shared Sub SaveEditTanpaApproval(objNews As AML_News, objModule As NawaDAL.Module)
        Using objDb As New NawaDatadevEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try
                    Dim dateFormat As String = "dd-MM-yyyy HH:mm:ss"

                    Dim objSysParamDateFormat As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(37)

                    If objSysParamDateFormat IsNot Nothing Then
                        dateFormat = objSysParamDateFormat.SettingValue
                    End If

                    'objUser.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    'objUser.CreatedDate = Now
                    objNews.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objNews.LastUpdateDate = Now
                    objNews.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objNews.ApprovedDate = Now

                    'If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                    '    objUser.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    'Else
                    '    objUser.Alternateby = ""
                    'End If

                    Dim objNewsOld As AML_News = getobjNews(objNews.PK_AML_News_ID)
                    'Dim isSame As Boolean = True
                    'If objNews.News_Content <> objNewsOld.News_Content Then
                    '    isSame = False
                    'ElseIf objNews.News_Content_Short <> objNewsOld.News_Content_Short Then
                    '    isSame = False
                    'ElseIf objNews.News_Title <> objNewsOld.News_Title Then
                    '    isSame = False
                    'ElseIf objNews.IsPublic <> objNewsOld.IsPublic Then
                    '    isSame = False
                    'End If

                    ''If isSame Then
                    ''    Throw New ApplicationException("There is no Changes Data.Please Edit First before Saved.")
                    ''End If

                    objDb.Entry(objNews).State = Entity.EntityState.Modified
                    objDb.SaveChanges()
                    'AuditTrail
                    Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)
                    NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, objNews, objNewsOld)
                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()

                    Throw ex
                End Try
            End Using
        End Using
    End Sub
    Public Shared Sub SaveEditWithApproval(objNews As AML_News, objModule As NawaDAL.Module)
        Using objdb As New NawaDatadevEntities
            Using objtrans As Entity.DbContextTransaction = objdb.Database.BeginTransaction
                Try
                    Dim alternateby As String = ""
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'objUser.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    'objUser.CreatedDate = Now

                    'If objListUserAddress.Count > 0 Then
                    '    For Each useraddress As RCM_UserAddress In objListUserAddress
                    'useraddress.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    'useraddress.CreatedDate = Now
                    'Next
                    'End If

                    Dim objdataNews As New AMLNewsClass
                    With objdataNews
                        .objdataNews = objNews
                        '.objlistdataUserAddress = objListUserAddress
                    End With

                    Dim objdataNewsOld As New AMLNewsClass
                    With objdataNewsOld
                        .objdataNews = getobjNews(objNews.PK_AML_News_ID)
                        '.objlistdataUserAddress = getListAddressByFk(objUser.PK_User_ID)
                    End With

                    ''Dim objUserOld As RCM_User = getObjUser(objUser.PK_User_ID)
                    'Dim isSame As Boolean = True
                    'If objNews.News_Content <> objdataNewsOld.objdataNews.News_Content Then
                    '    isSame = False
                    'ElseIf objNews.News_Content_Short <> objdataNewsOld.objdataNews.News_Content_Short Then
                    '    isSame = False
                    'ElseIf objNews.News_Title <> objdataNewsOld.objdataNews.News_Title Then
                    '    isSame = False
                    'ElseIf objNews.IsPublic <> objdataNewsOld.objdataNews.IsPublic Then
                    '    isSame = False
                    'End If

                    Dim xmldata As String = NawaBLL.Common.Serialize(objdataNews)
                    Dim xmldataOld As String = NawaBLL.Common.Serialize(objdataNewsOld)
                    'Dim xmldataOld As String = NawaBLL.Common.Serialize(getObjUser(objUser.PK_User_ID))
                    Dim objModuleApproval As New ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = objNews.PK_AML_News_ID
                        .ModuleField = xmldata
                        .ModuleFieldBefore = xmldataOld
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objdb.Entry(objModuleApproval).State = Entity.EntityState.Added

                    'AuditTrail
                    Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)

                    'AuditTrailDetail
                    NawaFramework.CreateAuditTrailDetailEdit(objdb, header.PK_AuditTrail_ID, objNews, objdataNewsOld.objdataNews)
                    'If objListUserAddress.Count > 0 Then
                    '    For Each itemInDB As RCM_UserAddress In objDataUserOld.objlistdataUserAddress
                    '        Dim result As RCM_UserAddress = objListUserAddress.Where(Function(x) x.PK_Address_ID = itemInDB.PK_Address_ID).FirstOrDefault
                    '        If result IsNot Nothing Then 'modify

                    '            If result.Address <> itemInDB.Address Then
                    '                isSame = False
                    '            ElseIf result.Provinsi <> itemInDB.Provinsi Then
                    '                isSame = False
                    '            ElseIf result.Kota <> itemInDB.Kota Then
                    '                isSame = False
                    '            ElseIf result.Kabupaten <> itemInDB.Kabupaten Then
                    '                isSame = False
                    '            ElseIf result.KodePos <> itemInDB.KodePos Then
                    '                isSame = False
                    '            ElseIf result.NoTlp <> itemInDB.NoTlp Then
                    '                isSame = False
                    '            ElseIf result.Kecamatan <> itemInDB.Kecamatan Then
                    '                isSame = False
                    '            ElseIf result.NamaPenerima <> itemInDB.NamaPenerima Then
                    '                isSame = False
                    '            End If

                    '            NawaFramework.CreateAuditTrailDetailEdit(objdb, header.PK_AuditTrail_ID, result, itemInDB)
                    '        Else 'delete
                    '            isSame = False
                    '            NawaFramework.CreateAuditTrailDetailDelete(objdb, header.PK_AuditTrail_ID, itemInDB)
                    '        End If
                    '    Next
                    '    For Each item As RCM_UserAddress In objListUserAddress
                    '        If item.PK_Address_ID < 0 Then 'add
                    '            isSame = False
                    '            item.FK_User_ID = objUser.PK_User_ID
                    '            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    '            item.CreatedDate = Now
                    '            NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, item)
                    '        End If
                    '    Next
                    'End If

                    'If isSame Then
                    '    Throw New ApplicationException("There is no Changes Data.Please Edit First before Saved.")
                    'End If

                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub


    Public Shared Sub LoadPanel(objPanel As FormPanel, objdata As String, strModulename As String, unikkey As String)
        Dim objgoAML_NewsDataBLL As AMLNewsClass = NawaBLL.Common.Deserialize(objdata, GetType(AMLNewsClass))
        Dim objNews As AML_News = objgoAML_NewsDataBLL.objdataNews
        Using db As NawaDatadevEntities = New NawaDatadevEntities
            If Not objNews Is Nothing Then
                Dim strunik As String = unikkey
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ID", "PK_AML_NEWS_ID" & strunik, objNews.PK_AML_News_ID)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "News Title", "Title" & strunik, objNews.News_Title)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "News Content", "News_Content" & strunik, objNews.News_Content)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "News Content Short", "News_Content_Short" & strunik, objNews.News_Content_Short)
                If objNews.IsPublic = False Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Is Public", "Is_Public" & strunik, "False")
                ElseIf objNews.IsPublic = True Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Is Public", "Is_Public" & strunik, "True")
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Is Public", "Is_Public" & strunik, Nothing)
                End If

                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Valid From", "Valid_From" & strunik, objNews.ValidFrom)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Valid To", "Valid_To" & strunik, objNews.ValidTo)
                If objNews.News_Image IsNot Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "News Image", "News_Image" & strunik, objNews.Image_Name)
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "News Image", "News_Image" & strunik, Nothing)
                End If
                If objNews.News_Attachment IsNot Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Attachment Name", "Attachment_Name" & strunik, objNews.Attachment_Name)
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Attachment Name", "Attachment_Name" & strunik, Nothing)
                End If

                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CreatedBy", "CreatedBy" & strunik, objNews.CreatedBy)
            End If
        End Using
    End Sub

    Shared Function Accept(ID As String) As Boolean
        Using objdb As New NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim objApproval As ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module

                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert

                            Dim objModuledata As AMLNewsClass = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AMLNewsClass))
                            Dim objNews As AML_News = objModuledata.objdataNews

                            objNews.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objNews.ApprovedDate = Now
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                objNews.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                objNews.Alternateby = ""
                            End If
                            objdb.Entry(objNews).State = Entity.EntityState.Added
                            objdb.SaveChanges()


                            'For Each itemx As RCM_UserAddress In objUserAddress
                            '    itemx.FK_User_ID = objUser.PK_User_ID
                            '    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            '    itemx.ApprovedDate = Now
                            '    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                            '        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            '    Else
                            '        itemx.Alternateby = ""
                            '    End If
                            '    objdb.Entry(itemx).State = Entity.EntityState.Added
                            'Next
                            'objdb.SaveChanges()


                            'AuditTrail
                            Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                            'AuditTrailDetail
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, objNews)
                            'If objUserAddress.Count > 0 Then
                            '    For Each item As RCM_UserAddress In objUserAddress
                            '        NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, item)
                            '    Next
                            'End If
                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objModuledata As AMLNewsClass = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AMLNewsClass))
                            Dim objModuledataOld As AMLNewsClass = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(AMLNewsClass))
                            Dim objUser As AML_News = objModuledata.objdataNews
                            Dim objUserOld As AML_News = objModuledataOld.objdataNews
                            'Dim objUserAddress As List(Of RCM_UserAddress) = objModuledata.objlistdataUserAddress
                            'Dim objUserAddressOld As List(Of RCM_UserAddress) = objModuledataOld.objlistdataUserAddress

                            objUser.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objUser.ApprovedDate = Now
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                objUser.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                objUser.Alternateby = ""
                            End If
                            objdb.Entry(objUser).State = Entity.EntityState.Modified
                            objdb.SaveChanges()
                            'AuditTrail
                            Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, objUser)

                            'If objUserAddress.Count > 0 Then
                            '    For Each itemInDB As RCM_UserAddress In objUserAddressOld
                            '        Dim result As RCM_UserAddress = objUserAddress.Where(Function(x) x.PK_Address_ID = itemInDB.PK_Address_ID).FirstOrDefault
                            '        If result IsNot Nothing Then 'modify
                            '            result.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            '            result.ApprovedDate = Now
                            '            result.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            '            result.LastUpdateDate = Now

                            '            objdb.Entry(result).State = Entity.EntityState.Modified
                            '            objdb.SaveChanges()
                            '            NawaFramework.CreateAuditTrailDetailEdit(objdb, header.PK_AuditTrail_ID, result, itemInDB)
                            '        Else 'delete
                            '            objdb.Entry(itemInDB).State = Entity.EntityState.Deleted
                            '            objdb.SaveChanges()
                            '            NawaFramework.CreateAuditTrailDetailDelete(objdb, header.PK_AuditTrail_ID, itemInDB)
                            '        End If
                            '    Next
                            '    For Each item As RCM_UserAddress In objUserAddress
                            '        If item.PK_Address_ID < 0 Then 'add
                            '            item.FK_User_ID = objUser.PK_User_ID
                            '            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            '            item.ApprovedDate = Now

                            '            objdb.Entry(item).State = Entity.EntityState.Added
                            '            objdb.SaveChanges()
                            '            NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, item)
                            '        End If
                            '    Next
                            'End If
                            'AuditTrailDetail
                            'NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, objUser)
                            'If objUserAddress.Count > 0 Then
                            '    For Each item As RCM_UserAddress In objUserAddress
                            '        NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, item)
                            '    Next
                            'End If
                        Case NawaBLL.Common.ModuleActionEnum.Delete

                            Dim objModuledata As AMLNewsClass = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AMLNewsClass))
                            Dim objUser As AML_News = objModuledata.objdataNews
                            'Dim objUserAddress As List(Of RCM_UserAddress) = objModuledata.objlistdataUserAddress

                            'objUser.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            'objUser.ApprovedDate = Now
                            'If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                            '    objUser.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            'Else
                            '    objUser.Alternateby = ""
                            'End If
                            objdb.Entry(objUser).State = Entity.EntityState.Deleted
                            objdb.SaveChanges()


                            'For Each itemx As RCM_UserAddress In objUserAddress
                            '    'itemx.FK_User_ID = objUser.PK_User_ID
                            '    'itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            '    'itemx.ApprovedDate = Now
                            '    'If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                            '    '    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            '    'Else
                            '    '    itemx.Alternateby = ""
                            '    'End If
                            '    objdb.Entry(itemx).State = Entity.EntityState.Deleted
                            '    objdb.SaveChanges()
                            'Next


                            'AuditTrail
                            Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Delete, objModule.ModuleLabel)

                            'AuditTrailDetail
                            NawaFramework.CreateAuditTrailDetailDelete(objdb, header.PK_AuditTrail_ID, objUser)
                            'If objUserAddress.Count > 0 Then
                            '    For Each item As RCM_UserAddress In objUserAddress
                            '        NawaFramework.CreateAuditTrailDetailDelete(objdb, header.PK_AuditTrail_ID, item)
                            '    Next
                            'End If
                    End Select
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function
    Shared Function Reject(ID As String) As Boolean

        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim objApproval As ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As New NawaDevDAL.Module

                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert

                            Dim objModuledata As AMLNewsClass = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AMLNewsClass))
                            Dim objUser As AML_News = objModuledata.objdataNews
                            'Dim objUserAddress As List(Of RCM_UserAddress) = objModuledata.objlistdataUserAddress

                            'audittrail                            
                            Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.Rejected, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                            'AuditTrailDetail
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, objUser)
                            'If objUserAddress.Count > 0 Then
                            '    For Each item As RCM_UserAddress In objUserAddress
                            '        NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, item)
                            '    Next
                            'End If
                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objModuledata As AMLNewsClass = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AMLNewsClass))
                            Dim objModuledataOld As AMLNewsClass = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(AMLNewsClass))
                            Dim objUser As AML_News = objModuledata.objdataNews
                            Dim objUserOld As AML_News = objModuledataOld.objdataNews
                            'Dim objUserAddress As List(Of RCM_UserAddress) = objModuledata.objlistdataUserAddress
                            'Dim objUserAddressOld As List(Of RCM_UserAddress) = objModuledataOld.objlistdataUserAddress

                            objUser.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objUser.ApprovedDate = Now
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                objUser.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                objUser.Alternateby = ""
                            End If
                            'objdb.Entry(objUser).State = Entity.EntityState.Modified
                            'objdb.SaveChanges()
                            'AuditTrail
                            Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.Rejected, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, objUser)

                            'If objUserAddress.Count > 0 Then
                            '    For Each itemInDB As RCM_UserAddress In objUserAddressOld
                            '        Dim result As RCM_UserAddress = objUserAddress.Where(Function(x) x.PK_Address_ID = itemInDB.PK_Address_ID).FirstOrDefault
                            '        If result IsNot Nothing Then 'modify
                            '            result.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            '            result.ApprovedDate = Now
                            '            result.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            '            result.LastUpdateDate = Now

                            '            'objdb.Entry(result).State = Entity.EntityState.Modified
                            '            'objdb.SaveChanges()
                            '            NawaFramework.CreateAuditTrailDetailEdit(objdb, header.PK_AuditTrail_ID, result, itemInDB)
                            '        Else 'delete
                            '            'objdb.Entry(itemInDB).State = Entity.EntityState.Deleted
                            '            'objdb.SaveChanges()
                            '            NawaFramework.CreateAuditTrailDetailDelete(objdb, header.PK_AuditTrail_ID, itemInDB)
                            '        End If
                            '    Next
                            '    For Each item As RCM_UserAddress In objUserAddress
                            '        If item.PK_Address_ID < 0 Then 'add
                            '            'item.FK_User_ID = objUser.PK_User_ID
                            '            'item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            '            'item.ApprovedDate = Now

                            '            'objdb.Entry(item).State = Entity.EntityState.Added
                            '            'objdb.SaveChanges()
                            '            NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, item)
                            '        End If
                            '    Next
                            'End If

                            'AuditTrailDetail
                            'NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, objUser)
                            'If objUserAddress.Count > 0 Then
                            '    For Each item As RCM_UserAddress In objUserAddress
                            '        NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, item)
                            '    Next
                            'End If
                        Case NawaBLL.Common.ModuleActionEnum.Delete

                            Dim objModuledata As AMLNewsClass = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AMLNewsClass))
                            Dim objUser As AML_News = objModuledata.objdataNews
                            'Dim objUserAddress As List(Of RCM_UserAddress) = objModuledata.objlistdataUserAddress

                            'objUser.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            'objUser.ApprovedDate = Now
                            'If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                            '    objUser.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            'Else
                            '    objUser.Alternateby = ""
                            'End If
                            'objdb.Entry(objUser).State = Entity.EntityState.Deleted
                            'objdb.SaveChanges()


                            'For Each itemx As RCM_UserAddress In objUserAddress
                            '    'itemx.FK_User_ID = objUser.PK_User_ID
                            '    'itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            '    'itemx.ApprovedDate = Now
                            '    'If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                            '    '    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            '    'Else
                            '    '    itemx.Alternateby = ""
                            '    'End If
                            '    'objdb.Entry(itemx).State = Entity.EntityState.Deleted
                            '    'objdb.SaveChanges()
                            'Next


                            'AuditTrail
                            Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.Rejected, NawaBLL.Common.ModuleActionEnum.Delete, objModule.ModuleLabel)

                            'AuditTrailDetail
                            NawaFramework.CreateAuditTrailDetailDelete(objdb, header.PK_AuditTrail_ID, objUser)
                            'If objUserAddress.Count > 0 Then
                            '    For Each item As RCM_UserAddress In objUserAddress
                            '        NawaFramework.CreateAuditTrailDetailDelete(objdb, header.PK_AuditTrail_ID, item)
                            '    Next
                            'End If
                    End Select
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function
#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        ' TODO: uncomment the following line if Finalize() is overridden above.
        ' GC.SuppressFinalize(Me)
    End Sub


#End Region
End Class
