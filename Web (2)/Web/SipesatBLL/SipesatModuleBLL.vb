﻿Imports System.Data.Entity
Imports Ext.Net
Imports NawaBLL.Nawa
Imports NawaBLL
Imports System.Reflection

Public Class SipesatModuleBLL
    'action 1 = add
    'action 2 = edit
    Shared Function checkValidation(report As NawaDevDAL.AML_Sipesat_Report, objmodule As NawaDAL.Module, Action As Integer)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Return ""
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Shared Function saveReportTanpaApproval(report As NawaDevDAL.AML_Sipesat_Report, objmodule As NawaDAL.Module, Action As Integer)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'daniel 20 Nov 2020 fix bug audittrial
                    Dim reportold As NawaDevDAL.AML_Sipesat_Report = objdb.AML_Sipesat_Report.Where(Function(x) x.PK_Report_ID = report.PK_Report_ID).FirstOrDefault
                    Dim alternateby As String = ""
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If
                    With report
                        If Action = 1 Then
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                        End If
                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                        .LastUpdateDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .Alternateby = alternateby
                        .Active = 1
                        .IsValid = 1
                        .Jenis_Informasi = 2
                        .Status = 1
                    End With
                    If Action = 1 Then
                        objdb.Entry(report).State = Entity.EntityState.Added
                    ElseIf Action = 2 Then
                        objdb.Entry(report).State = Entity.EntityState.Modified
                    End If
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim StatusEffectedDatabase As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim ModuleAction As Integer
                    If Action = 1 Then
                        ModuleAction = NawaBLL.Common.ModuleActionEnum.Insert
                    ElseIf Action = 2 Then
                        ModuleAction = NawaBLL.Common.ModuleActionEnum.Update
                    End If
                    Dim modulename As String = objmodule.ModuleLabel

                    Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, StatusEffectedDatabase, ModuleAction, modulename)
                    If Action = 1 Then
                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, report)
                    ElseIf Action = 2 Then
                        NawaFramework.CreateAuditTrailDetailEdit(objdb, objaudittrailheader.PK_AuditTrail_ID, report, reportold)
                    End If
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Shared Function saveReportApproval(report As NawaDevDAL.AML_Sipesat_Report, objmodule As NawaDAL.Module, Action As Integer)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim reportold As NawaDevDAL.AML_Sipesat_Report = objdb.AML_Sipesat_Report.Where(Function(x) x.PK_Report_ID = report.PK_Report_ID).FirstOrDefault
                    Dim alternateby As String = ""
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If
                    With report
                        If Action = 1 Then
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                        End If
                        .LastUpdateDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .Alternateby = alternateby
                        .Active = 1
                        .IsValid = 1
                        .Jenis_Informasi = 2
                        .Status = 1
                    End With

                    Dim dataXML As String = NawaBLL.Common.Serialize(report)

                    Dim objModuleApproval As New NawaDevDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objmodule.ModuleName
                        .ModuleField = dataXML
                        .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        If Action = 1 Then
                            .ModuleKey = 0
                            .ModuleFieldBefore = ""
                            .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                        ElseIf Action = 2 Then
                            Dim dataXMLold As String = NawaBLL.Common.Serialize(reportold)
                            .ModuleKey = report.PK_Report_ID
                            .ModuleFieldBefore = dataXMLold
                            .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                        End If
                    End With
                    objdb.Entry(objModuleApproval).State = Entity.EntityState.Added

                    Dim objATHeader As New NawaDevDAL.AuditTrailHeader
                    objATHeader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objATHeader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                    objATHeader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    If Action = 1 Then
                        objATHeader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                    ElseIf Action = 2 Then
                        objATHeader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                    End If
                    objATHeader.ModuleLabel = objmodule.ModuleLabel
                    objdb.Entry(objATHeader).State = Entity.EntityState.Added

                    Dim objtype As Type = report.GetType
                    Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                    For Each item As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New NawaDevDAL.AuditTrailDetail
                        objaudittraildetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                        objaudittraildetail.FieldName = item.Name
                        If Not item.GetValue(report, Nothing) Is Nothing Then
                            objaudittraildetail.NewValue = item.GetValue(report, Nothing)
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        If Action = 1 Then
                            objaudittraildetail.OldValue = ""
                        ElseIf Action = 2 Then
                            If Not item.GetValue(report, Nothing) Is Nothing Then
                                objaudittraildetail.OldValue = item.GetValue(reportold, Nothing)
                            Else
                                objaudittraildetail.OldValue = ""
                            End If
                        End If
                        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                    Next
                    reportold.Status = 4
                    objdb.Entry(reportold).State = Entity.EntityState.Modified
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Public Sub LoadPanel(formPanelNew As FormPanel, moduleField As String, unikID As String)
        Dim objReportBLL As NawaDevDAL.AML_Sipesat_Report = Common.Deserialize(moduleField, GetType(NawaDevDAL.AML_Sipesat_Report))
        Dim customertype, infotype As String
        If Not objReportBLL Is Nothing Then
            BLL.NawaFramework.ExtDisplayField(formPanelNew, "IDPJK", "PK_ID" & unikID, objReportBLL.PK_ID)
            'Edited by Felix 29 jan 2021, kasih if

            'customertype = getcustomertype(objReportBLL.PK_Customer_Type_ID)
            'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Kode Nasabah", "Description" & unikID, customertype)
            'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nama Nasabah", "Name" & unikID, objReportBLL.Name)
            'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tempat Lahir", "Place_Birth" & unikID, objReportBLL.Place_Birth)
            'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tanggal Lahir", "PK_Date_Birth" & unikID, objReportBLL.PK_Date_Birth)
            'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Alamat", "Address" & unikID, objReportBLL.Address)
            'BLL.NawaFramework.ExtDisplayField(formPanelNew, "NIK", "No_KTP" & unikID, objReportBLL.No_KTP)
            'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nomor Identitas Lain", "PK_Other_ID" & unikID, objReportBLL.PK_Other_ID)
            'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nomor CIF", "No_CIF" & unikID, objReportBLL.No_CIF)
            'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nomor NPWP", "No_NPWP" & unikID, objReportBLL.No_NPWP)
            'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Period", "Period" & unikID, objReportBLL.Period)
            'infotype = getinfotype(objReportBLL.Jenis_Informasi)
            'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Jenis Informasi", "Jenis_Informasi" & unikID, infotype)

            If Not objReportBLL.PK_Customer_Type_ID Is Nothing Then
                customertype = getcustomertype(objReportBLL.PK_Customer_Type_ID)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Kode Nasabah", "Description" & unikID, customertype)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Kode Nasabah", "Description" & unikID, "")
            End If

            If Not objReportBLL.Name Is Nothing Then
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nama Nasabah", "Name" & unikID, objReportBLL.Name)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nama Nasabah", "Name" & unikID, "")
            End If

            If Not objReportBLL.Place_Birth Is Nothing Then
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tempat Lahir", "Place_Birth" & unikID, objReportBLL.Place_Birth)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tempat Lahir", "Place_Birth" & unikID, "")
            End If

            If Not objReportBLL.PK_Date_Birth Is Nothing Then
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tanggal Lahir", "PK_Date_Birth" & unikID, objReportBLL.PK_Date_Birth)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tanggal Lahir", "PK_Date_Birth" & unikID, "")
            End If

            If Not objReportBLL.Address Is Nothing Then
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Alamat", "Address" & unikID, objReportBLL.Address)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Alamat", "Address" & unikID, "")
            End If

            If Not objReportBLL.No_KTP Is Nothing Then
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "NIK", "No_KTP" & unikID, objReportBLL.No_KTP)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "NIK", "No_KTP" & unikID, "")
            End If

            If Not objReportBLL.PK_Other_ID Is Nothing Then
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nomor Identitas Lain", "PK_Other_ID" & unikID, objReportBLL.PK_Other_ID)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nomor Identitas Lain", "PK_Other_ID" & unikID, "")
            End If

            If Not objReportBLL.No_CIF Is Nothing Then
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nomor CIF", "No_CIF" & unikID, objReportBLL.No_CIF)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nomor CIF", "No_CIF" & unikID, "")
            End If

            If Not objReportBLL.No_NPWP Is Nothing Then
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nomor NPWP", "No_NPWP" & unikID, objReportBLL.No_NPWP)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nomor NPWP", "No_NPWP" & unikID, "")
            End If

            If Not objReportBLL.Period Is Nothing Then
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Period", "Period" & unikID, objReportBLL.Period)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Period", "Period" & unikID, "")
            End If

            If Not objReportBLL.Jenis_Informasi Is Nothing Then
                infotype = getinfotype(objReportBLL.Jenis_Informasi)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Jenis Informasi", "Jenis_Informasi" & unikID, infotype)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Jenis Informasi", "Jenis_Informasi" & unikID, "")
            End If
            'end of edit 29 Jan 2021
        End If
    End Sub

    Function getcustomertype(type As Integer) As String
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objdb.goAML_Ref_Customer_Type Where x.PK_Customer_Type_ID = type Select x.Description).FirstOrDefault
        End Using
    End Function

    Function getinfotype(type As Integer) As String
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objdb.AML_Sipesat_Ref_InfoType Where x.PK_AML_Sipesat_InfoType = type Select x.Description).FirstOrDefault
        End Using
    End Function

    'Daniel 19 Nov 2020
    Public Sub Accept(pK_ModuleApproval_ID As String)
        Using objDB As New NawaDevDAL.NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objApproval As NawaDevDAL.ModuleApproval = objDB.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = pK_ModuleApproval_ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objDB.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If
                    Dim objhistory As New NawaDevDAL.goAML_Module_Note_History
                    With objhistory
                        .FK_Module_ID = CLng(objModule.PK_Module_ID)
                        .UnikID = objApproval.ModuleKey
                        .UserID = NawaBLL.Common.SessionCurrentUser.PK_MUser_ID
                        .UserName = NawaBLL.Common.SessionCurrentUser.UserName
                        .RoleID = NawaBLL.Common.SessionCurrentUser.FK_MRole_ID
                        .Status = "Approve"
                        .Active = 1
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                    End With
                    objDB.Entry(objhistory).State = EntityState.Added
                    Select Case objApproval.PK_ModuleAction_ID
                        Case Common.ModuleActionEnum.Update
                            Dim objModuledata As NawaDevDAL.AML_Sipesat_Report = Common.Deserialize(objApproval.ModuleField, GetType(NawaDevDAL.AML_Sipesat_Report))
                            Dim objModuledataOld As NawaDevDAL.AML_Sipesat_Report = Common.Deserialize(objApproval.ModuleFieldBefore, GetType(NawaDevDAL.AML_Sipesat_Report))
                            With objModuledata
                                .Status = 1
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objDB.Entry(objModuledata).State = EntityState.Modified

                            Dim objATHeader As New NawaDevDAL.AuditTrailHeader With
                            {
                                .ApproveBy = Common.SessionCurrentUser.UserID,
                                .CreatedBy = objApproval.CreatedBy,
                                .CreatedDate = Now,
                                .FK_AuditTrailStatus_ID = Common.AuditTrailStatusEnum.AffectedToDatabase,
                                .FK_ModuleAction_ID = Common.ModuleActionEnum.Update,
                                .ModuleLabel = objModule.ModuleLabel
                            }
                            objDB.Entry(objATHeader).State = EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.GetType
                            Dim properties() As PropertyInfo = objtype.GetProperties
                            For Each item As PropertyInfo In properties
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = If(Not item.GetValue(objModuledataOld, Nothing) Is Nothing, item.GetValue(objModuledataOld, Nothing), ""),
                                    .NewValue = If(Not item.GetValue(objModuledata, Nothing) Is Nothing, item.GetValue(objModuledata, Nothing), "")
                                }
                                objDB.Entry(objATDetail).State = EntityState.Added
                            Next
                    End Select
                    objDB.Entry(objApproval).State = EntityState.Deleted
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    'Daniel 19 Nov 2020
    Public Sub Reject(pK_ModuleApproval_ID As Long)
        Using objDB As New NawaDevDAL.NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objApproval As NawaDevDAL.ModuleApproval = objDB.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = pK_ModuleApproval_ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objDB.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If
                    Dim objhistory As New NawaDevDAL.goAML_Module_Note_History
                    With objhistory
                        .FK_Module_ID = objModule.PK_Module_ID
                        .UnikID = objApproval.ModuleKey
                        .UserID = NawaBLL.Common.SessionCurrentUser.PK_MUser_ID
                        .UserName = NawaBLL.Common.SessionCurrentUser.UserName
                        .RoleID = NawaBLL.Common.SessionCurrentUser.FK_MRole_ID
                        .Status = "Reject"
                        .Active = 1
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                    End With
                    objDB.Entry(objhistory).State = EntityState.Added
                    Select Case objApproval.PK_ModuleAction_ID
                        Case Common.ModuleActionEnum.Update
                            Dim objModuledata As NawaDevDAL.AML_Sipesat_Report = Common.Deserialize(objApproval.ModuleField, GetType(NawaDevDAL.AML_Sipesat_Report))
                            Dim objModuledataOld As NawaDevDAL.AML_Sipesat_Report = Common.Deserialize(objApproval.ModuleFieldBefore, GetType(NawaDevDAL.AML_Sipesat_Report))
                            With objModuledataOld
                                .Status = 1
                            End With
                            objDB.Entry(objModuledataOld).State = EntityState.Modified

                            Dim objATHeader As New NawaDevDAL.AuditTrailHeader With
                            {
                                .ApproveBy = Common.SessionCurrentUser.UserID,
                                .CreatedBy = objApproval.CreatedBy,
                                .CreatedDate = Now,
                                .FK_AuditTrailStatus_ID = Common.AuditTrailStatusEnum.Rejected,
                                .FK_ModuleAction_ID = Common.ModuleActionEnum.Update,
                                .ModuleLabel = objModule.ModuleLabel
                            }
                            objDB.Entry(objATHeader).State = EntityState.Added
                            Dim objtype As Type = objModuledata.GetType
                            Dim properties() As PropertyInfo = objtype.GetProperties
                            For Each item As PropertyInfo In properties
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = If(Not item.GetValue(objModuledataOld, Nothing) Is Nothing, item.GetValue(objModuledataOld, Nothing), ""),
                                    .NewValue = If(Not item.GetValue(objModuledata, Nothing) Is Nothing, item.GetValue(objModuledata, Nothing), "")
                                }
                                objDB.Entry(objATDetail).State = EntityState.Added
                            Next
                    End Select
                    objDB.Entry(objApproval).State = EntityState.Deleted
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub
End Class
