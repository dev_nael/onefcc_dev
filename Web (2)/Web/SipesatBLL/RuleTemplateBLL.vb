﻿Imports NawaDevDAL
Imports NawaDevBLL
Imports System.Data
Imports NawaBLL
Public Class RuleTemplateBLL
    Shared Function GetRuleTemplate(ID As String) As OneFcc_MS_Rule_Template
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.OneFcc_MS_Rule_Template Where x.PK_Rule_Template_ID = ID Select x).FirstOrDefault
        End Using
    End Function

    Shared Function GetRuleTemplateDetail(ID As String) As List(Of OneFcc_MS_Rule_Template_Detail)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.OneFcc_MS_Rule_Template_Detail Where x.FK_Rule_Template_ID = ID Select x).ToList
        End Using
    End Function

    Function SubmitDocument(objData As OneFcc_MS_Rule_Template, objDetails As List(Of OneFcc_MS_Rule_Template_Detail), objModule As NawaDAL.Module)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try

                    objData.[Active] = True
                    objData.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.CreatedDate = Now.ToString("yyyy-MM-dd")
                    objData.LastUpdateDate = Now.ToString("yyyy-MM-dd")
                    If NawaBLL.Common.SessionAlternateUser Is Nothing Then
                        objData.Alternateby = NawaBLL.Common.SessionCurrentUser.UserID
                    Else
                        objData.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If
                    objdb.Entry(objData).State = Entity.EntityState.Added

                    Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaDevBLL.NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, Common.AuditTrailStatusEnum.WaitingToApproval, Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                    objdb.SaveChanges()

                    SaveDetail(objDetails, objData.PK_Rule_Template_ID)
                    objtrans.Commit()
                Catch
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Function EditDocument(objData As OneFcc_MS_Rule_Template, objDataOld As OneFcc_MS_Rule_Template, objDetails As List(Of OneFcc_MS_Rule_Template_Detail), objModule As NawaDAL.Module)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try

                    objData.[Active] = True
                    objData.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.CreatedDate = Now.ToString("yyyy-MM-dd")
                    objData.LastUpdateDate = Now.ToString("yyyy-MM-dd")
                    If NawaBLL.Common.SessionAlternateUser Is Nothing Then
                        objData.Alternateby = NawaBLL.Common.SessionCurrentUser.UserID
                    Else
                        objData.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If
                    objdb.Entry(objData).State = Entity.EntityState.Modified

                    Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaDevBLL.NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, Common.AuditTrailStatusEnum.AffectedToDatabase, Common.ModuleActionEnum.Insert, objModule.ModuleLabel)
                    NawaFramework.CreateAuditTrailDetailEdit(objdb, objaudittrailheader.PK_AuditTrail_ID, objData, objDataOld)

                    objdb.SaveChanges()

                    SaveDetail(objDetails, objData.PK_Rule_Template_ID)
                    objtrans.Commit()
                Catch
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Protected Sub SaveDetail(objDetails As List(Of OneFcc_MS_Rule_Template_Detail), ID As String)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    For Each itemx As OneFcc_MS_Rule_Template_Detail In (From x In objdb.OneFcc_MS_Rule_Template_Detail Where x.FK_Rule_Template_ID = ID).ToList
                        Dim objcek As OneFcc_MS_Rule_Template_Detail = objDetails.Find(Function(x) x.PK_Rule_Template_Detail_ID = itemx.PK_Rule_Template_Detail_ID)
                        If objcek Is Nothing Then
                            objdb.Entry(itemx).State = Entity.EntityState.Deleted
                            objdb.SaveChanges()

                        End If
                    Next
                    For Each item As OneFcc_MS_Rule_Template_Detail In objDetails
                        Dim obcek As OneFcc_MS_Rule_Template_Detail = (From x In objdb.OneFcc_MS_Rule_Template_Detail Where x.PK_Rule_Template_Detail_ID = item.PK_Rule_Template_Detail_ID Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            Dim oNew As New OneFcc_MS_Rule_Template_Detail
                            oNew.FK_Rule_Template_ID = ID
                            oNew.Variable_Name = item.Variable_Name
                            oNew.Variable_Description = item.Variable_Description
                            oNew.FK_FieldType_ID = item.FK_FieldType_ID
                            oNew.FK_ExtType_ID = item.FK_ExtType_ID
                            oNew.Tabel_Reference_Name = item.Tabel_Reference_Name
                            oNew.Tabel_Reference_Name_Alias = item.Tabel_Reference_Name_Alias
                            oNew.Table_Reference_Field_Key = item.Table_Reference_Field_Key
                            oNew.Table_Reference_Field_Display_Name = item.Table_Reference_Field_Display_Name
                            oNew.Table_Reference_Filter = item.Table_Reference_Filter
                            oNew.Table_Reference_Additonal_Join = item.Table_Reference_Additonal_Join

                            oNew.CreatedDate = Now.ToString("yyyy-MM-dd")
                            oNew.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            oNew.LastUpdateDate = Now.ToString("yyyy-MM-dd")
                            oNew.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            oNew.Active = True
                            objdb.Entry(oNew).State = Entity.EntityState.Added
                            objdb.SaveChanges()
                        Else
                            obcek.Variable_Name = item.Variable_Name
                            obcek.Variable_Description = item.Variable_Description
                            obcek.FK_FieldType_ID = item.FK_FieldType_ID
                            obcek.FK_ExtType_ID = item.FK_ExtType_ID
                            obcek.Tabel_Reference_Name = item.Tabel_Reference_Name
                            obcek.Tabel_Reference_Name_Alias = item.Tabel_Reference_Name_Alias
                            obcek.Table_Reference_Field_Key = item.Table_Reference_Field_Key
                            obcek.Table_Reference_Field_Display_Name = item.Table_Reference_Field_Display_Name
                            obcek.Table_Reference_Filter = item.Table_Reference_Filter
                            obcek.Table_Reference_Additonal_Join = item.Table_Reference_Additonal_Join

                            obcek.LastUpdateDate = Now.ToString("yyyy-MM-dd")
                            obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objdb.Entry(obcek).State = Entity.EntityState.Modified
                            objdb.SaveChanges()

                        End If

                    Next
                    objtrans.Commit()
                Catch
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

End Class
