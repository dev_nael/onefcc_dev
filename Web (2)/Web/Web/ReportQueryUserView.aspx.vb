﻿Imports System.Globalization
Imports OfficeOpenXml
Partial Class ReportQueryUserView
    Inherits Parent




    Public Property strWhereClause() As String
        Get
            Return Session("ReportQueryUserView.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("ReportQueryUserView.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("ReportQueryUserView.strSort")
        End Get
        Set(ByVal value As String)
            Session("ReportQueryUserView.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("ReportQueryUserView.indexStart")
        End Get
        Set(ByVal value As String)
            Session("ReportQueryUserView.indexStart") = value
        End Set
    End Property

    Public Property StrFieldName() As String
        Get
            Return Session("ReportQueryUserView.StrFieldName")
        End Get
        Set(ByVal value As String)
            Session("ReportQueryUserView.StrFieldName") = value
        End Set
    End Property
    Public Property StrTableName() As String
        Get
            Return Session("ReportQueryUserView.StrTableName")
        End Get
        Set(ByVal value As String)
            Session("ReportQueryUserView.StrTableName") = value
        End Set
    End Property


    Public Property StrFirstFilter() As String
        Get
            Return Session("ReportQueryUserView.StrFirstFilter")
        End Get
        Set(ByVal value As String)
            Session("ReportQueryUserView.StrFirstFilter") = value
        End Set
    End Property

    Public Property StrFirstSort() As String
        Get
            Return Session("ReportQueryUserView.StrFirstSort")
        End Get
        Set(ByVal value As String)
            Session("ReportQueryUserView.StrFirstSort") = value
        End Set
    End Property


    Public Property StrUnikKey() As String
        Get
            Return Session("ReportQueryUserView.StrUnikKey")
        End Get
        Set(ByVal value As String)
            Session("ReportQueryUserView.StrUnikKey") = value
        End Set

    End Property
    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("ReportQueryUserView.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("ReportQueryUserView.ObjModule") = value
        End Set
    End Property

    '20220916
    Public Property totalRow() As Integer
        Get
            Return Session("ReportQueryUserView.totalRow")
        End Get
        Set(ByVal value As Integer)
            Session("ReportQueryUserView.totalRow") = value
        End Set
    End Property
    Public Property PK_BackgroundProcess_ID() As Long
        Get
            Return Session("ReportQueryUserView.PK_BackgroundProcess_ID")
        End Get
        Set(ByVal value As Long)
            Session("ReportQueryUserView.PK_BackgroundProcess_ID") = value
        End Set
    End Property
    Protected Sub Page_Load1(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then

                Dim strmodule As String = ""
                strmodule = Request.Params("ModuleID")


                Try
                    Dim intmodule As Integer = NawaBLL.V2.Common.Common.DecryptQueryString(strmodule, NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey)
                    Me.ObjModule = NawaBLL.V2.ModuleBLL.ModuleBLL.GetModuleByModuleID(intmodule)


                    If Not NawaBLL.V2.ModuleBLL.ModuleBLL.GetHakAkses(NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.V2.Common.Common.ModuleActionEnum.view) Then
                        Dim strIDCode As String = 1
                        strIDCode = NawaBLL.V2.Common.Common.EncryptQueryString(strIDCode, NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey)

                        Ext.Net.X.Redirect(String.Format(NawaBLL.V2.Common.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    FormPanelInput.Title = ObjModule.ModuleLabel & " View"
                    Panelconfirmation.Title = ObjModule.ModuleLabel & " View"


                    If Not Ext.Net.X.IsAjaxRequest Then


                        NawaBLL.V2.NawaFramework.Nawa.BLL.NawaFramework.GetPicker(cboReport, "SELECT PK_ReportQuery_ID , Reportname FROM ReportQuery WHERE Active=1", True, True)



                    End If

                Catch ex As Exception
                    Throw New Exception("Invalid Module ID")
                End Try
            End If


            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"

            End If

            'objEodTask.BentukformAdd()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))



                    NawaBLL.V2.ReportQueryBLL.ReportQueryBLL.PreviewReportUser(GridPreview, StrUnikKey, False, True, StrTableName, StrFieldName, StrFirstFilter, StrFirstSort)


                    If strWhereClause = "" Then
                        strWhereClause += StrFirstFilter
                    Else
                        If StrFirstFilter <> "" Then
                            strWhereClause += " and " & StrFirstFilter
                        End If

                    End If


                    If strOrder = "" And StrFirstSort <> "" Then
                        strOrder += StrFirstSort
                    Else
                        If StrFirstSort <> "" Then
                            strOrder = StrFirstSort & "," & strOrder
                        End If

                    End If

                    Using objtbl As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(StrTableName, StrFieldName, strWhereClause, Me.strOrder, indexStart, NawaBLL.V2.Basic.SystemParameterBLL.GetPageSize, 0)

                        For Each item As Object In GridPreview.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next
                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(ObjModule.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.V2.Basic.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 0
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                intcolnumber = intcolnumber + 1
                                If item.DataType = GetType(Date) Then

                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                        End Using
                    End Using

                    Dim arrByte As Byte() = IO.File.ReadAllBytes(objfileinfo.FullName)
                    If objfileinfo.Exists Then objfileinfo.Delete()

                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(arrByte)
                    Response.End()
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)

                    NawaBLL.V2.ReportQueryBLL.ReportQueryBLL.PreviewReportUser(GridPreview, StrUnikKey, False, True, StrTableName, StrFieldName, StrFirstFilter, StrFirstSort)


                    Using objtbl As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(StrTableName, StrFieldName, strWhereClause, Me.strOrder, indexStart, NawaBLL.V2.Basic.SystemParameterBLL.GetPageSize, 0)


                        For Each item As Object In GridPreview.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                    End Using

                    Dim arrByte As Byte() = IO.File.ReadAllBytes(objfileinfo.FullName)
                    If objfileinfo.Exists Then objfileinfo.Delete()

                    Response.Clear()
                    Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                    Response.Charset = ""
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Me.EnableViewState = False
                    'Response.ContentType = "application/ms-excel"
                    Response.ContentType = "text/csv"
                    Response.BinaryWrite(arrByte)
                    Response.End()
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                If objfileinfo.Exists Then objfileinfo.Delete()
            End If
        End Try
    End Sub

    <DirectMethod>
    Public Sub GoToBackgroundProcess()
        Dim Moduleid As String = NawaBLL.V2.Common.Common.EncryptQueryString(NawaBLL.V2.BackgroundProcessBLL.BackgroundProcessBLL.GetModuleID(), NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey)
        Dim dataid As String = NawaBLL.V2.Common.Common.EncryptQueryString(PK_BackgroundProcess_ID, NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey)
        Ext.Net.X.Redirect(NawaBLL.V2.Common.Common.GetApplicationPath & "/Parameter/BackgroundProcessView.aspx?ModuleID=" & Moduleid & "&PK_BackgroundProcess_ID=" & dataid)
    End Sub
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    If totalRow > CInt(NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetSystemParameterByPk(61).SettingValue) And CInt(NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetSystemParameterByPk(61).SettingValue) > 0 Then
                        PK_BackgroundProcess_ID = NawaBLL.V2.ReportQueryBLL.ReportQueryBLL.BackgroundReportQuery(StrTableName, StrFieldName, strWhereClause, Me.strOrder, totalRow, ObjModule.PK_Module_ID, 51)
                        'Ext.Net.X.Msg.Alert("Info", "File will be Generated in Background Process").Show()
                        Ext.Net.X.Msg.Show(New MessageBoxConfig() With
                        {
                            .Title = "Info",
                            .Message = "File will be Generated in Background Process",
                            .Buttons = MessageBox.Button.OK,
                            .Icon = MessageBox.Icon.INFO,
                            .MessageBoxButtonsConfig = New MessageBoxButtonsConfig() With {
                                .Ok = New MessageBoxButtonConfig() With {.Handler = "NawadataDirect.GoToBackgroundProcess();", .Text = "OK"}
                            }
                        })
                    Else
                        Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                        objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))

                        NawaBLL.V2.ReportQueryBLL.ReportQueryBLL.PreviewReportUser(GridPreview, StrUnikKey, False, True, StrTableName, StrFieldName, StrFirstFilter, StrFirstSort)

                        If strWhereClause = "" Then
                            strWhereClause += StrFirstFilter
                        Else
                            If StrFirstFilter <> "" Then
                                strWhereClause += " and " & StrFirstFilter
                            End If

                        End If


                        If strOrder = "" And StrFirstSort <> "" Then
                            strOrder += StrFirstSort
                        Else
                            If StrFirstSort <> "" Then
                                strOrder = StrFirstSort & "," & strOrder
                            End If

                        End If

                        Using objtbl As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(StrTableName, StrFieldName, strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)


                            For Each item As Object In GridPreview.ColumnModel.Columns

                                If item.Hidden Then
                                    If objtbl.Columns.Contains(item.Text) Then
                                        objtbl.Columns.Remove(item.Text)
                                    End If

                                End If
                            Next
                            Using resource As New ExcelPackage(objfileinfo)
                                Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(ObjModule.ModuleName)
                                ws.Cells("A1").LoadFromDataTable(objtbl, True)
                                Dim dateformat As String = NawaBLL.V2.Basic.SystemParameterBLL.GetDateFormat
                                Dim intcolnumber As Integer = 0
                                For Each item As System.Data.DataColumn In objtbl.Columns
                                    intcolnumber = intcolnumber + 1
                                    If item.DataType = GetType(Date) Then

                                        ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                    End If
                                Next
                                ws.Cells(ws.Dimension.Address).AutoFitColumns()
                                resource.Save()
                            End Using
                        End Using

                        Dim arrByte As Byte() = IO.File.ReadAllBytes(objfileinfo.FullName)
                        If objfileinfo.Exists Then objfileinfo.Delete()

                        Response.Clear()
                        Response.ClearHeaders()
                        Response.ContentType = "application/vnd.ms-excel"
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                        Response.Charset = ""
                        Response.AddHeader("cache-control", "max-age=0")
                        Me.EnableViewState = False
                        Response.ContentType = "ContentType"
                        Response.BinaryWrite(arrByte)
                        Response.End()
                    End If
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    If totalRow > CInt(NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetSystemParameterByPk(61).SettingValue) And CInt(NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetSystemParameterByPk(61).SettingValue) > 0 Then
                        PK_BackgroundProcess_ID = NawaBLL.V2.ReportQueryBLL.ReportQueryBLL.BackgroundReportQuery(StrTableName, StrFieldName, strWhereClause, Me.strOrder, totalRow, ObjModule.PK_Module_ID, 52)
                        'Ext.Net.X.Msg.Alert("Info", "File will be Generated in Background Process").Show()
                        Ext.Net.X.Msg.Show(New MessageBoxConfig() With
                        {
                            .Title = "Info",
                            .Message = "File will be Generated in Background Process",
                            .Buttons = MessageBox.Button.OK,
                            .Icon = MessageBox.Icon.INFO,
                            .MessageBoxButtonsConfig = New MessageBoxButtonsConfig() With {
                                .Ok = New MessageBoxButtonConfig() With {.Handler = "NawadataDirect.GoToBackgroundProcess();", .Text = "OK"}
                            }
                        })
                    Else


                        Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                        objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                        Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)


                        NawaBLL.V2.ReportQueryBLL.ReportQueryBLL.PreviewReportUser(GridPreview, StrUnikKey, False, True, StrTableName, StrFieldName, StrFirstFilter, StrFirstSort)

                        Using objtbl As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(StrTableName, StrFieldName, strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)





                            For Each item As Object In GridPreview.ColumnModel.Columns

                                If item.Hidden Then
                                    If objtbl.Columns.Contains(item.Text) Then
                                        objtbl.Columns.Remove(item.Text)
                                    End If

                                End If
                            Next
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                            For i As Integer = 0 To objtbl.Rows.Count - 1
                                For k As Integer = 0 To objtbl.Columns.Count - 1
                                    'add separator
                                    stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                                Next
                                'append new line
                                stringWriter_Temp.Write(vbCr & vbLf)
                            Next
                            stringWriter_Temp.Close()
                        End Using

                        Dim arrByte As Byte() = IO.File.ReadAllBytes(objfileinfo.FullName)
                        If objfileinfo.Exists Then objfileinfo.Delete()

                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(arrByte)
                        Response.End()
                    End If
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub Storetrigger_Readdata(sender As Object, e As StoreReadDataEventArgs)
        Try



            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.V2.NawaFramework.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)
            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strTable As String = ""


            NawaBLL.V2.NawaFramework.Nawa.BLL.NawaFramework.GetPicker(cboReport, "SELECT PK_ReportQuery_ID, Reportname  FROM ReportQuery WHERE Active=1", False, True)

            Dim DataPaging As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging("ReportQuery", "PK_ReportQuery_ID, Reportname ", strfilter, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)

            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If

            e.Total = inttotalRecord


            Dim objStoredata As Ext.Net.Store = CType(sender, Ext.Net.Store)
            objStoredata.DataSource = DataPaging
            objStoredata.DataBind()



        Catch ex As Exception
            Throw
        End Try
    End Sub
    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try


            Dim strProcessid As String = cboReport.Value

            If strProcessid <> "" Then
                StrUnikKey = Split(strProcessid, "-")(0)
                GridPreview.Hidden = False


                'NawaBLL.V2.ReportQueryBLL.ReportQueryBLL.PreviewReportUser(GridPreview, StrUnikKey, True, True, StrTableName, StrFieldName, StrFirstFilter, StrFirstSort)

                NawaBLL.V2.ReportQueryBLL.ReportQueryBLL.PreviewReportUserNew(GridPreview, StrUnikKey, True, True, StrTableName, StrFieldName, StrFirstFilter, StrFirstSort)

                GridPreview.Reconfigure()

                GridPreview.GetStore.Reload()

            Else
                Throw New Exception("Please Select a Report")
            End If




        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub bindgrid()
        GridPreview.Store(0).Reload()
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            '20220808 Fix Bug Invalid Module ID
            Ext.Net.X.Redirect(NawaBLL.V2.Common.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & Moduleid)
            '20220808 Fix Bug Invalid Module ID
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub storePreview_Readdata(sender As Object, e As StoreReadDataEventArgs)
        Try


            indexStart = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer

            Dim strfilter As String = NawaBLL.V2.NawaFramework.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)


            'Dim index As Integer = strfilter.IndexOf("'%")


            'Dim strcontentfilter As String = ""
            'If index > 0 Then
            '    strcontentfilter = strcontrolfilter.Substring(index + 2, strcontrolfilter.IndexOf("%'", index + 2) - index - 2)
            '    If DateTime.TryParseExact(strcontentfilter, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, Nothing) Then
            '        Dim i As Integer
            '        i = 0
            '    End If

            'End If






            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strTable As String = ""



            NawaBLL.V2.ReportQueryBLL.ReportQueryBLL.PreviewReportUserNew(GridPreview, StrUnikKey, False, True, StrTableName, StrFieldName, StrFirstFilter, StrFirstSort)


            If strfilter = "" Then
                strfilter += StrFirstFilter
            Else
                If StrFirstFilter <> "" Then
                    strfilter += " and " & StrFirstFilter
                End If

            End If



            If strsort = "" Then
                strsort += StrFirstSort
            Else
                strsort = strsort
            End If

            Me.strWhereClause = strfilter
            Me.strOrder = strsort

            Dim DataPaging As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(StrTableName, StrFieldName, strfilter, strsort, indexStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)

            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If

            e.Total = inttotalRecord

            '20220916
            If inttotalRecord <> totalRow Then
                totalRow = inttotalRecord
            End If


            Dim objStoredata As Ext.Net.Store = CType(sender, Ext.Net.Store)
            objStoredata.DataSource = DataPaging
            objStoredata.DataBind()


        Catch ex As Exception
            Throw
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.V2.Common.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class
