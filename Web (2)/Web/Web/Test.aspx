﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="Test.aspx.vb" Inherits="Test" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:Panel ID="Panel1" runat="server" Title="Customer Info Detail" Layout="AnchorLayout" AnchorHorizontal="90%" BodyPadding="5" AutoScroll="true">
        <Items>

            <ext:fieldset ID="Panel6" runat="server" Height="300" Border="true" Layout="HBoxLayout" Collapsible="true">
                <Items>
                    <ext:FieldSet ID="FieldSet1" runat="server" Flex="1" Height="290"  AutoScroll="true">
                        <Items>
                            <ext:DisplayField ID="DisplayField2" runat="server" FieldLabel="field1" Text="value1">
                            </ext:DisplayField>
                            <ext:DisplayField ID="DisplayField3" runat="server" FieldLabel="field1" Text="value1">
                            </ext:DisplayField>
                            <ext:DisplayField ID="DisplayField4" runat="server" FieldLabel="field1" Text="value1">
                            </ext:DisplayField>
                            <ext:DisplayField ID="DisplayField5" runat="server" FieldLabel="field1" Text="value1">
                            </ext:DisplayField>
                            <ext:DisplayField ID="DisplayField6" runat="server" FieldLabel="field1" Text="value1">
                            </ext:DisplayField>
                            <ext:DisplayField ID="DisplayField7" runat="server" FieldLabel="field1" Text="value1">
                            </ext:DisplayField>
                            <ext:DisplayField ID="DisplayField8" runat="server" FieldLabel="field1" Text="value1">
                            </ext:DisplayField>

                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet ID="FieldSet2" runat="server"  Flex="1" Height="290" AutoScroll="true">
                        <Items></Items>
                    </ext:FieldSet>
                    <ext:FieldSet ID="FieldSet3" runat="server"  Flex="1" Height="290" AutoScroll="true">
                        <Items></Items>
                    </ext:FieldSet>
                </Items>
            </ext:fieldset>

            <ext:TabPanel ID="TabPanel1" runat="server" Height="300" Border="true" BodyPadding="5">
                <Items>
                    <ext:Panel ID="Panel3" runat="server" Title="Customer Profile" AutoScroll="true">
                        <Items>

                            <ext:FieldSet ID="FieldSet4" runat="server" Title="Personal & General Information" Flex="1" Height="490" Collapsible="true" AutoScroll="true">
                                <Items>

                                    <ext:DisplayField ID="DisplayField13" runat="server" FieldLabel="field1" Text="value1">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField14" runat="server" FieldLabel="field1" Text="value1">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField15" runat="server" FieldLabel="field1" Text="value1">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField16" runat="server" FieldLabel="field1" Text="value1">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField17" runat="server" FieldLabel="field1" Text="value1">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField18" runat="server" FieldLabel="field1" Text="value1">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField19" runat="server" FieldLabel="field1" Text="value1">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField20" runat="server" FieldLabel="field1" Text="value1">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField21" runat="server" FieldLabel="field1" Text="value1">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField22" runat="server" FieldLabel="field1" Text="value1">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField23" runat="server" FieldLabel="field1" Text="value1">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField24" runat="server" FieldLabel="field1" Text="value1">
                                    </ext:DisplayField>
                                </Items>
                            </ext:FieldSet>
                                <ext:FieldSet ID="FieldSet5" runat="server" Title="Address" Flex="1" Height="490" Collapsible="true" AutoScroll="true">
                                <Items>
                                    <ext:GridPanel ID="GridPanel1" runat="server" Height="300" Title="Title"></ext:GridPanel>
                                    
                                </Items>
                            </ext:FieldSet>
                        </Items>
                    </ext:Panel>
                    <ext:Panel ID="Panel4" runat="server" Title="Account Profile">
                        <Items></Items>
                    </ext:Panel>
                    <ext:Panel ID="Panel5" runat="server" Title="AML Risk Score">
                        <Items></Items>
                    </ext:Panel>

                </Items>
            </ext:TabPanel>
        </Items>
    </ext:Panel>
</asp:Content>