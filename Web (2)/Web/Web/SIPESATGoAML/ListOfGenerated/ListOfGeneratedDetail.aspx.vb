﻿Imports System.IO
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Entity

Partial Class SipesatGoAML_ListOfGeneratedDetail
    Inherits Parent

    Public Property Filetodownload() As String
        Get
            Return Session("SipesatGoAML_ListOfGeneratedDetail.Filetodownload")
        End Get
        Set(ByVal value As String)
            Session("SipesatGoAML_ListOfGeneratedDetail.Filetodownload") = value
        End Set
    End Property
    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("SipesatGoAML_ListOfGeneratedDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("SipesatGoAML_ListOfGeneratedDetail.ObjModule") = value
        End Set
    End Property

    'Public Property ObjGeneratedgoAML() As goAML_Generate_XML
    '    Get
    '        Return Session("SipesatGoAML_ListOfGeneratedDetail.ObjGeneratedgoAML")
    '    End Get
    '    Set(ByVal value As goAML_Generate_XML)
    '        Session("SipesatGoAML_ListOfGeneratedDetail.ObjGeneratedgoAML") = value
    '    End Set
    'End Property

    Public Property TotalValid() As String
        Get
            Return Session("SipesatGoAML_ListOfGeneratedDetail.TotalValid")
        End Get
        Set(ByVal value As String)
            Session("SipesatGoAML_ListOfGeneratedDetail.TotalValid") = value
        End Set
    End Property

    Public Property IsAlreadyValidateSchema() As String
        Get
            Return Session("SipesatGoAML_ListOfGeneratedDetail.IsAlreadyValidateSchema")
        End Get
        Set(ByVal value As String)
            Session("SipesatGoAML_ListOfGeneratedDetail.IsAlreadyValidateSchema") = value
        End Set
    End Property

    Public Property ValidateSchemaResultValid() As String
        Get
            Return Session("SipesatGoAML_ListOfGeneratedDetail.ValidateSchemaResultValid")
        End Get
        Set(ByVal value As String)
            Session("SipesatGoAML_ListOfGeneratedDetail.ValidateSchemaResultValid") = value
        End Set
    End Property

    Public Property LastUpdateDateResult() As String
        Get
            Return Session("SipesatGoAML_ListOfGeneratedDetail.LastUpdateDateResult")
        End Get
        Set(ByVal value As String)
            Session("SipesatGoAML_ListOfGeneratedDetail.LastUpdateDateResult") = value
        End Set
    End Property
    Public Property Period() As String
        Get
            Return Session("SipesatGoAML_ListOfGeneratedDetail.Period")
        End Get
        Set(ByVal value As String)
            Session("SipesatGoAML_ListOfGeneratedDetail.Period") = value
        End Set
    End Property

    '' Add 31-Dec-2021
    Public Property IDListOfGenerated() As Long
        Get
            Return Session("SipesatGoAML_ListOfGeneratedDetail.IDListOfGenerated")
        End Get
        Set(ByVal value As Long)
            Session("SipesatGoAML_ListOfGeneratedDetail.IDListOfGenerated") = value
        End Set
    End Property
    '' End 31-Dec-2021

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()

                Dim Moduleid As String = Request.Params("ModuleID")
                Dim intModuleid As Integer

                Try
                    intModuleid = Common.DecryptQueryString(Moduleid, SystemParameterBLL.GetEncriptionKey)

                    IDListOfGenerated = Common.DecryptQueryString(Request.Params("ID"), SystemParameterBLL.GetEncriptionKey) '' Add 31-Dec-2021
                    ObjModule = ModuleBLL.GetModuleByModuleID(intModuleid)

                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    FormPanelInput.Title = ObjModule.ModuleLabel & " Generate"

                    LoadData()

                    TaskManager1.StartTask("refreshScreeningStatus")
                Catch ex As Exception
                    ErrorSignal.FromCurrentContext.Raise(ex)
                    Net.X.Msg.Alert("Error", ex.Message).Show()
                End Try
            End If


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadData()

        'Dim dtListOfGenerated_SipesatGoAML As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text,
        '    " select a.Period, a.Last_Update_Date_Report, isnull(a.Total_Validated,0) as Total_Validated, isnull(a.Total_Generated,0) as Total_Generated , " &
        '    "        b.Keterangan,Error_Message_API, isnull(a.Error_Message_API,'') as Error_Message_API " &
        '    " ,isnull(a.Total_Valid,0) as Total_Valid, isnull(a.Total_Invalid,0) as Total_Invalid,CASE WHEN  IsAlreadyValidateSchema =1 THEN 'Yes' ELSE 'No' END as IsAlreadyValidateSchema   " &
        '    " ,CASE WHEN ValidateSchemaResultValid=1 THEN '1 - Valid' when ValidateSchemaResultValid is null then '2 - Not Yet Validate' ELSE '3 - Not Valid' END ValidateSchemaResultValid " &
        '    " ,isnull(convert(varchar(10),a.Download_Date,121),'') as Download_Date " &
        '    " , File_Zip, File_ZipName " &
        '    " FROM SipesatGoAML_ListOfGenerated a " &
        '    " inner join goaml_ref_statusGenerateAPI b " &
        '    " on a.Status_API = b.kode " &
        '    " where a.NO_ID = '" & Common.DecryptQueryString(Request.Params("ID"), SystemParameterBLL.GetEncriptionKey) & "' ", Nothing)

        Dim objGenerateParalelStatus(0) As SqlParameter
        objGenerateParalelStatus(0) = New SqlParameter
        objGenerateParalelStatus(0).ParameterName = "@PK_ListOfGenerate_ID"
        objGenerateParalelStatus(0).Value = IDListOfGenerated

        Dim dtListOfGenerated_SipesatGoAML As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SipesatGoAML_GET_ListOfGeneratedStatus", objGenerateParalelStatus)

        If dtListOfGenerated_SipesatGoAML IsNot Nothing Then
            For Each row As DataRow In dtListOfGenerated_SipesatGoAML.Rows

                lblPeriod.Text = row.Item("Period")
                Period = row.Item("Period")

                If Not IsDBNull(row.Item("Last_Update_Date_Report")) Then
                    lblLastUpdateDateReport.Text = Convert.ToDateTime(row.Item("Last_Update_Date_Report")).ToString("dd-MMM-yyyy")

                End If
                If Not IsDBNull(row.Item("Last_Update_Date_Report")) Then
                    LastUpdateDateResult = Convert.ToDateTime(row.Item("Last_Update_Date_Report")).ToString("dd-MMM-yyyy")

                End If

                lblXMLValidatedCount.Text = row.Item("Total_Validated")
                lblXMLGeneratedCount.Text = row.Item("Total_Generated")
                lblStatusAPI.Text = row.Item("KeteranganStatusAPI")
                lblErrorMessageAPI.Text = row.Item("Error_Message_API")
                lblTotalValid.Text = row.Item("Total_Valid")
                TotalValid = row.Item("Total_Valid")

                lblTotalInvalid.Text = row.Item("Total_Invalid")
                lblAlreadyValidateSchema.Text = row.Item("IsAlreadyValidateSchema")
                IsAlreadyValidateSchema = row.Item("IsAlreadyValidateSchema")

                lblValidateSchemaResultValid.Text = row.Item("ValidateSchemaResultValid")
                ValidateSchemaResultValid = row.Item("ValidateSchemaResultValid")

                If row.Item("Download_Date") = "" Then
                    lblDownloadDate.Text = ""
                Else
                    lblDownloadDate.Text = Convert.ToDateTime(row.Item("Download_Date")).ToString("dd-MMM-yyyy")
                End If


                If row.Item("File_Zip") IsNot Nothing And Not (IsDBNull(row.Item("File_Zip"))) And Not (IsDBNull(row.Item("File_ZipName"))) Then
                    btnDownloadXML.Hidden = False
                    btnSaveUpload.Disabled = True
                Else
                    btnDownloadXML.Hidden = True
                    btnSaveUpload.Disabled = False
                End If

            Next
        End If

    End Sub

    Private Sub ClearSession()
        ObjModule = Nothing
        'ObjGeneratedgoAML = Nothing

        Filetodownload = ""

        lblXMLValidatedCount.Text = ""
        lblXMLGeneratedCount.Text = ""
        lblStatusAPI.Text = ""
        lblErrorMessageAPI.Text = ""
    End Sub
    Protected Sub StoreValidation_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            StoreView.PageSize = SystemParameterBLL.GetPageSize
            Dim intStart As Integer = e.Start
            Dim intTotalRowCount As Long = 0

            Dim intLimit As Int16 = e.Limit
            Dim strfilter As String = Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)
            Dim strsort As String = ""

            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuerySchema As String = "select * from (SELECT gavsxr.UnikReference,gavsxr.ErrorMessage FROM SipesatGoAML_ValidateSchemaXSD_Report AS gavsxr WHERE gavsxr.Month_Period='" & Period & "' " &
                 " And datediff(day,gavsxr.LastUpdateDate ,'" & LastUpdateDateResult & "') = 0)xx "
            If strfilter.Length > 0 Then
                strQuerySchema &= " where " & strfilter
            End If
            Dim objListParam(3) As SqlParameter
            objListParam(0) = New SqlParameter
            objListParam(0).ParameterName = "@querydata"
            objListParam(0).SqlDbType = SqlDbType.VarChar
            objListParam(0).Value = strQuerySchema
            objListParam(1) = New SqlParameter
            objListParam(1).ParameterName = "@orderby"
            objListParam(1).SqlDbType = SqlDbType.VarChar
            objListParam(1).Value = strsort
            objListParam(2) = New SqlParameter
            objListParam(2).ParameterName = "@PageNum"
            objListParam(2).SqlDbType = SqlDbType.Int
            objListParam(2).Value = intStart
            objListParam(3) = New SqlParameter
            objListParam(3).ParameterName = "@PageSize"
            objListParam(3).SqlDbType = SqlDbType.Int
            objListParam(3).Value = intLimit
            'query hanya ambil 1 record untuk simpen schemanya saja(pagesize=1) biar enteng
            Dim DataPaging As DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuerySchema, strsort, intStart, intLimit, intTotalRowCount)
            e.Total = intTotalRowCount

            GridPanelValidation.GetStore.DataSource = DataPaging
            GridPanelValidation.GetStore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnValidate_DirectClick(sender As Object, e As EventArgs)
        Try
            If TotalValid <> "" Then
                If TotalValid = "0" Then
                    Throw New Exception("Total Valid is 0.Please correct the invalid data first before validate schema.")
                End If

                Dim objGenerateParalelStatus(0) As SqlParameter
                objGenerateParalelStatus(0) = New SqlParameter
                objGenerateParalelStatus(0).ParameterName = "@PK_ListOfGenerate_ID"
                objGenerateParalelStatus(0).Value = IDListOfGenerated

                Dim dtGenerateParalelStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SipesatGoAML_GET_ListOfGeneratedStatus", objGenerateParalelStatus)

                If dtGenerateParalelStatus IsNot Nothing And dtGenerateParalelStatus.Rows.Count > 0 Then
                    For Each row As DataRow In dtGenerateParalelStatus.Rows
                        lblXMLValidatedCount.Text = row.Item("Total_Validated")

                        lblXMLGeneratedCount.Text = row.Item("Total_Generated")
                        lblStatusAPI.Text = row.Item("KeteranganStatusAPI")

                        lblErrorMessageAPI.Text = row.Item("Error_Message_API")

                        If row.Item("kodeStatusAPI") <> "1" And row.Item("kodeStatusAPI") <> "4" Then
                            Dim objParam(0) As SqlParameter
                            objParam(0) = New SqlParameter
                            objParam(0).ParameterName = "@PK_ListOfGenerate_ID"
                            objParam(0).Value = IDListOfGenerated
                            objParam(0).DbType = SqlDbType.BigInt

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SipesatGoAML_VALIDATE_XML_SaveEOD", objParam)

                            lblStatusAPI.Text = "Process Validate XML"
                        ElseIf row.Item("kodeStatusAPI") = "4" Then
                            Throw New Exception("Process Validate XML by API is in progress. Please check again later.")
                        ElseIf row.Item("kodeStatusAPI") = "1" Then
                            Throw New Exception("Process Generate XML by API is in progress. Please check again later.")
                        End If
                    Next
                Else
                    Dim objParam(0) As SqlParameter
                    objParam(0) = New SqlParameter
                    objParam(0).ParameterName = "@PK_ListOfGenerate_ID"
                    objParam(0).Value = IDListOfGenerated
                    objParam(0).DbType = SqlDbType.BigInt

                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SipesatGoAML_VALIDATE_XML_SaveEOD", objParam)

                    lblStatusAPI.Text = "Process Validate XML"
                End If

                'ObjGeneratedgoAML = Nothing
                LoadData()
                StoreView.Reload()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub BtnSave_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)

        Dim isHarusValid As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select parametervalue from SIPESATGOAML_GLOBAL_PARAMETER where PK_GLOBAL_PARAMETER_ID = 24 ", Nothing)

        Try
            If TotalValid = "0" Then
                'Throw New Exception("There is not data valid")
                Throw New Exception("Can't generate XML if there is still invalid data.")
            End If

            If IsAlreadyValidateSchema.ToLower = "false" Or ValidateSchemaResultValid.ToLower = "" Then
                Throw New Exception("Please Validate Schema first before Generate XML")
            End If

            ''If Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "false" Or Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "" Then '' Edited By Felix 27 Aug 2020
            If (ValidateSchemaResultValid.ToLower = "false" Or ValidateSchemaResultValid.ToLower = "") And isHarusValid = "1" Then
                Throw New Exception("There is Validate Schema result Fail. Please fix the fail result first before generate xml")
            End If

            '' Remark karena harusnya ttp bisa generate, tapi yg valid aja.
            '5-Aug-2021 Adi : Menambahkan validasi jika masih ada report yang akan di generate statusnya 4 (Waiting for Approval) atau 5 (Need Correction)
            'Using objdb As New NawaDatadevEntities
            '    Dim objCekApproval = objdb.goAML_Report.Where(Function(x) x.Report_Code = ObjGeneratedgoAML.Jenis_Laporan And DbFunctions.TruncateTime(x.Transaction_Date) = DbFunctions.TruncateTime(ObjGeneratedgoAML.Transaction_Date) And
            '                                                    DbFunctions.TruncateTime(x.LastUpdateDate) = DbFunctions.TruncateTime(ObjGeneratedgoAML.Last_Update_Date) And (x.Status = 4 Or x.Status = 5)).ToList
            '    If objCekApproval IsNot Nothing AndAlso objCekApproval.Count > 0 Then
            '        Throw New Exception("Can't generate XML. There is still " & objCekApproval.Count & " invalid reports or are in Approval.")
            '    End If
            'End Using
            'End of 5-Aug-2021 Adi : Menambahkan validasi jika masih ada report yang akan di generate statusnya 4 (Waiting for Approval) atau 5 (Need Correction)


            'Dim path As String = Server.MapPath("~\goaml\FolderExport\")
            ''  Dim path As String = SystemParameterBLL.GetSystemParameterByPk(9017).SettingValue

            'Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
            'If Not Directory.Exists(Dirpath) Then
            '    Directory.CreateDirectory(Dirpath)
            'End If

            '' Edit 31-Dec-2021
            'NawaDevBLL.GeneratePPATKBLL.ValidateSchema(ObjGeneratedgoAML.Transaction_Date, ObjGeneratedgoAML.Jenis_Laporan, ObjGeneratedgoAML.Last_Update_Date)

            Dim objGenerateParalelStatus(0) As SqlParameter
            objGenerateParalelStatus(0) = New SqlParameter
            objGenerateParalelStatus(0).ParameterName = "@PK_ListOfGenerate_ID"
            objGenerateParalelStatus(0).Value = IDListOfGenerated

            Dim dtGenerateParalelStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SipesatGoAML_GET_ListOfGeneratedStatus", objGenerateParalelStatus)

            If dtGenerateParalelStatus IsNot Nothing Then
                For Each row As DataRow In dtGenerateParalelStatus.Rows
                    lblXMLValidatedCount.Text = row.Item("Total_Validated")

                    lblXMLGeneratedCount.Text = row.Item("Total_Generated")
                    lblStatusAPI.Text = row.Item("KeteranganStatusAPI")

                    lblErrorMessageAPI.Text = row.Item("Error_Message_API")

                    If row.Item("File_Zip") IsNot Nothing And Not (IsDBNull(row.Item("File_Zip"))) And Not (IsDBNull(row.Item("File_ZipName"))) Then


                    ElseIf row.Item("kodeStatusAPI") = "6" Then
                        Dim objParam(0) As SqlParameter
                        objParam(0) = New SqlParameter
                        objParam(0).ParameterName = "@PK_ListOfGenerate_ID"
                        objParam(0).Value = IDListOfGenerated
                        objParam(0).DbType = SqlDbType.BigInt

                        'objParam(1) = New SqlParameter
                        'objParam(1).ParameterName = "@User_ID"
                        'objParam(1).Value = Common.SessionCurrentUser.UserID
                        'objParam(1).DbType = SqlDbType.VarChar

                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SipesatGoAML_GENERATE_XML_SaveEOD", objParam)

                        'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update goAML_GenerateXMLParalel " &
                        '    " set StatusGenerate = 1 " &
                        '    " ,LastUpdateDate = getdate() " &
                        '" where FK_goAML_Generate_XML_NOID =  '" & IDListOfGenerated & "'", Nothing)
                        'lblStatusAPI.Text = "Process Generate XML"
                    ElseIf row.Item("kodeStatusAPI") = "1" Then
                        Throw New Exception("Process Generate XML by API is in progress. Please check again later.")
                    ElseIf row.Item("kodeStatusAPI") = "4" Then
                        Throw New Exception("Process Validate XML by API is in progress. Please check again later.")
                    End If

                Next

            End If



        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Sub Download()
        If Filetodownload <> "" Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(Filetodownload))
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "application/zip"
            Response.BinaryWrite(IO.File.ReadAllBytes(Filetodownload))
            Response.End()

            ''06-Jan-2022 Felix , hapus setelah download
            If IO.File.Exists(Filetodownload) Then
                IO.File.Delete(Filetodownload)
            End If

            '5-Aug-2021 Adi : kosongkan agar tidak ke-download saat invalid
            Filetodownload = ""
        End If
    End Sub
    Protected Sub btnCancelUpload_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            '2020 09 15 Added By Apuy
            If ObjModule.UrlView.Contains("ReportType") Then
                Ext.Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "ModuleID=" & Moduleid)
            Else
                Ext.Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

    End Sub

    Protected Sub btnDownloadXML_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)

        Dim isHarusValid As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select parametervalue from SIPESATGOAML_GLOBAL_PARAMETER where PK_GLOBAL_PARAMETER_ID = 24 ", Nothing)

        Try
            Dim path As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select parametervalue from SIPESATGOAML_GLOBAL_PARAMETER where PK_GLOBAL_PARAMETER_ID = 22 ", Nothing)
            path = path & "\FolderExport\"

            Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
            If Not Directory.Exists(Dirpath) Then
                Directory.CreateDirectory(Dirpath)
            End If

            '' Edit 31-Dec-2021
            'NawaDevBLL.GeneratePPATKBLL.ValidateSchema(ObjGeneratedgoAML.Transaction_Date, ObjGeneratedgoAML.Jenis_Laporan, ObjGeneratedgoAML.Last_Update_Date)

            Dim objGenerateParalelStatus(0) As SqlParameter
            objGenerateParalelStatus(0) = New SqlParameter
            objGenerateParalelStatus(0).ParameterName = "@PK_ListOfGenerate_ID"
            objGenerateParalelStatus(0).Value = IDListOfGenerated

            Dim dtGenerateParalelStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SipesatGoAML_GET_ListOfGeneratedStatus", objGenerateParalelStatus)

            If dtGenerateParalelStatus IsNot Nothing Then
                For Each row As DataRow In dtGenerateParalelStatus.Rows
                    lblXMLValidatedCount.Text = row.Item("Total_Validated")

                    lblXMLGeneratedCount.Text = row.Item("Total_Generated")
                    lblStatusAPI.Text = row.Item("KeteranganStatusAPI")

                    lblErrorMessageAPI.Text = row.Item("Error_Message_API")

                    If row.Item("File_Zip") IsNot Nothing And Not (IsDBNull(row.Item("File_Zip"))) And Not (IsDBNull(row.Item("File_ZipName"))) Then

                        Dim listZip As New List(Of String)
                        Dim dtZip As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text,
                                                                                " select File_ZipName,File_Zip " &
                                                                                " FROM SipesatGoAML_ListOfGenerated " &
                                                                                " where NO_ID = " & IDListOfGenerated, Nothing)
                        listZip.Clear() '' Added on 02 Feb 2021, untuk hapus attachemnt report sebelumnya, Thanks to Pak Bom

                        If dtZip.Rows.Count > 0 Then
                            For Each itemattachment As Data.DataRow In dtZip.Rows
                                Dim fileattachmentname As String = Dirpath & itemattachment(0)
                                IO.File.WriteAllBytes(fileattachmentname, itemattachment(1))
                                listZip.Add(fileattachmentname)

                            Next
                        End If
                        Filetodownload = listZip.Item(0).ToString

                        If Filetodownload Is Nothing Then
                            Throw New Exception("Not Exists Report")

                        End If

                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update SipesatGoAML_ListOfGenerated " &
                            " set Status_API = 7 " &
                            " ,LastUpdateDate = getdate() " &
                            " ,Download_Date = getdate() " &
                        " where NO_ID =  '" & IDListOfGenerated & "'", Nothing)

                        lblStatusAPI.Text = "XML Generated"
                    ElseIf row.Item("kodeStatusAPI") = "6" Then
                        'Dim objParam(1) As SqlParameter
                        'objParam(0) = New SqlParameter
                        'objParam(0).ParameterName = "@PK_ListOfGenerate_ID"
                        'objParam(0).Value = IDListOfGenerated
                        'objParam(0).DbType = SqlDbType.BigInt

                        'objParam(1) = New SqlParameter
                        'objParam(1).ParameterName = "@User_ID"
                        'objParam(1).Value = Common.SessionCurrentUser.UserID
                        'objParam(1).DbType = SqlDbType.VarChar

                        'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_GENERATE_XML_SaveEOD", objParam)

                        'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update goAML_GenerateXMLParalel " &
                        '    " set StatusGenerate = 1 " &
                        '    " ,LastUpdateDate = getdate() " &
                        '" where FK_goAML_Generate_XML_NOID =  '" & IDListOfGenerated & "'", Nothing)
                        'lblStatusAPI.Text = "Process Generate XML"
                    ElseIf row.Item("kodeStatusAPI") = "1" Then
                        Throw New Exception("Process Generate XML by API is in progress. Please check again later.")
                    ElseIf row.Item("kodeStatusAPI") = "4" Then
                        Throw New Exception("Process Validate XML by API is in progress. Please check again later.")
                    End If

                Next

            End If


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class