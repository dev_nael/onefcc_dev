﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="ListOfGeneratedDetail.aspx.vb" Inherits="SipesatGoAML_ListOfGeneratedDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        var columnAutoResize = function (grid) {

            grid.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {
                    col.autoSize();
                }
            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };

        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:FormPanel ID="FormPanelInput" runat="server" Layout="AnchorLayout" ButtonAlign="Center" Title="" BodyStyle="padding:20px" AutoScroll="true" Hidden="false" ClientIDMode="Static">
        <Items>
            <ext:Container runat="server" Layout="ColumnLayout" AnchorHorizontal="100%">
                <Items>
                    <ext:Container runat="server" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField runat="server" ID="lblPeriod" FieldLabel="Period" Text="A value here 1" />
                            <ext:DisplayField runat="server" ID="lblLastUpdateDateReport" FieldLabel="Last Update Date" Text="A value here 1" />
                            <ext:DisplayField runat="server" ID="lblXMLValidatedCount" FieldLabel="Total XML Validated" Text="A value here 1" />
                            <ext:DisplayField runat="server" ID="lblXMLGeneratedCount" FieldLabel="Total XML Generated" Text="A value here 1" />
                            <ext:DisplayField runat="server" ID="lblStatusAPI" FieldLabel="Status from API" Text="A value here 1" />
                            <ext:DisplayField runat="server" ID="lblErrorMessageAPI" FieldLabel="Error Message from API" Text="A value here 1" />
                        </Items>
                    </ext:Container>
                    <ext:Container runat="server" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField runat="server" ID="lblTotalValid" FieldLabel="Total Valid" Text="" />
                            <ext:DisplayField runat="server" ID="lblTotalInvalid" FieldLabel="Total Invalid" Text="A value here 1" />
                            <ext:DisplayField runat="server" ID="lblAlreadyValidateSchema" FieldLabel="Already Validate Schema" Text="" />
                            <ext:DisplayField runat="server" ID="lblValidateSchemaResultValid" FieldLabel="Validate Schema Result" Text="A value here 1" />
                            <ext:DisplayField runat="server" ID="lblDownloadDate" FieldLabel="Download Date" Text="A value here 1" />
                            <ext:Button ID="btnDownloadXML" runat="server" Text="Download XML" Icon="Disk" Hidden="true">
                                <DirectEvents>
                                    <Click OnEvent="btnDownloadXML_DirectClick" Success="NawadataDirect.Download({isUpload : true});">
                                        <EventMask MinDelay="500" Msg="Validating..." ShowMask="true"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Container>

            <ext:GridPanel ID="GridPanelValidation" ClientIDMode="Static" runat="server" Height="400" Title="Validation Schema">
                <Store>
                    <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="StoreValidation_ReadData">
                        <Sorters>
                        </Sorters>
                        <Proxy>

                            <ext:PageProxy />
                        </Proxy>
                        <Reader>
                        </Reader>

                        <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="UnikReference" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ErrorMessage" Type="String"></ext:ModelField>

                                        </Fields>
                                    </ext:Model>
                                </Model>
                    </ext:Store>

                </Store>
                  <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column7" runat="server" DataIndex="UnikReference" Text="Unik Reference" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="ErrorMessage" Text="ErrorMessage" MinWidth="130" Flex="1"></ext:Column>

                            </Columns>
                        </ColumnModel>
                <BottomBar>

                <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True">
                    <Items>

                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
                 <View>
                <ext:GridView runat="server" EnableTextSelection="true" />
            </View>
            </ext:GridPanel>

            <%--add 07-Jan-2022--%>
            <ext:Panel ID="panel_TaskManager" runat="server" MarginSpec="10 0" Border="true" BodyPadding="10" Hidden="true">
                <Content>
                    <ext:TaskManager ID="TaskManager1" runat="server">
                        <Tasks>
                            <ext:Task
                                TaskID="refreshScreeningStatus"
                                Interval="20000"
                                AutoRun="false"
                                >
                                <DirectEvents>
                                    <Update OnEvent="LoadData">
                                        <%--<EventMask Msg="Loading..." ShowMask="true" MinDelay="200"></EventMask>--%>
                                    </Update>
                                </DirectEvents>
                            </ext:Task>
                        </Tasks>
                    </ext:TaskManager>
                </Content>
            </ext:Panel>
            <%--End 07-Jan-2022--%>
        </Items>
        <Buttons>

            <ext:Button ID="btnValidateSchema" runat="server" Text="Validate Schema" Icon="Disk" >
                <DirectEvents>
                    <Click OnEvent="BtnValidate_DirectClick">
                        <EventMask MinDelay="500" Msg="Validating..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

            <ext:Button ID="btnSaveUpload" runat="server" Text="Generate" Icon="Disk" >
                <DirectEvents>
                    <Click OnEvent="BtnSave_DirectClick"  Success="NawadataDirect.Download({isUpload : true});">
                        <ExtraParams>
                            <ext:Parameter Name="command" Value="New" Mode="Value">
                            </ext:Parameter>

                        </ExtraParams>
                        <EventMask Msg="Loading..." ShowMask="true" ></EventMask>
                    </Click>
                </DirectEvents>

            </ext:Button>
            <ext:Button ID="btnCancelUpload" runat="server" Text="Back" Icon="Cancel">
                <DirectEvents>
                    <Click OnEvent="btnCancelUpload_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>