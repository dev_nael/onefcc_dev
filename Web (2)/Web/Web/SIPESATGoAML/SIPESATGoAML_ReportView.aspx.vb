﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Ext.Net
Imports NawaBLL
Imports NawaDAL
Imports OfficeOpenXml
Partial Class SIPESATGoAML_ReportView
    Inherits Parent
    Public objFormModuleView As NawaBLL.FormModuleView

    Public Property Filetodownload() As String
        Get
            Return Session("SIPESATGoAML_ReportView.Filetodownload")
        End Get
        Set(ByVal value As String)
            Session("SIPESATGoAML_ReportView.Filetodownload") = value
        End Set
    End Property

    Public Property strWhereClause() As String
        Get
            Return Session("SIPESATGoAML_ReportView.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("SIPESATGoAML_ReportView.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("SIPESATGoAML_ReportView.strSort")
        End Get
        Set(ByVal value As String)
            Session("SIPESATGoAML_ReportView.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("SIPESATGoAML_ReportView.indexStart")
        End Get
        Set(ByVal value As String)
            Session("SIPESATGoAML_ReportView.indexStart") = value
        End Set
    End Property

    Public Property objModule() As NawaDAL.Module
        Get
            Return Session("SIPESATGoAML_ReportView.objModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("SIPESATGoAML_ReportView.objModule") = value
        End Set
    End Property

    Public Property combinedReportID() As String
        Get
            Return Session("SIPESATGoAML_ReportView.combinedReportID")
        End Get
        Set(value As String)
            Session("SIPESATGoAML_ReportView.combinedReportID") = value
        End Set
    End Property

    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            'Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.AddHeader("content-disposition", "attachment;filename=" & objModule.ModuleLabel & ".xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        'Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.AddHeader("content-disposition", "attachment;filename=" & objModule.ModuleLabel & ".csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            'Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.AddHeader("content-disposition", "attachment;filename=" & objModule.ModuleLabel & ".xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        'Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.AddHeader("content-disposition", "attachment;filename=" & objModule.ModuleLabel & ".csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub


    <DirectMethod>
    Public Sub BtnAdd_Click()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            'Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")

            '----------------25/01/2020 BSIM
            Dim tempUrl As String = ""

            If Request.Url.ToString().Contains("?") Then

                tempUrl = Request.Url.ToString().Split("?")(1)

            End If

            If String.IsNullOrEmpty(tempUrl) Then
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")
            Else
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?{0}", tempUrl), "Loading")
            End If
            '-----------------------------

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub goAML_Report_ReportView_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer

            intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            Me.objModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                Dim strIDCode As String = 1
                BtnAdd.Hidden = True
                strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                Exit Sub
            End If

            objFormModuleView.ModuleID = objModule.PK_Module_ID
            objFormModuleView.ModuleName = objModule.ModuleName

            objFormModuleView.AddField("PK_Report_ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.INTValue, , , , , )
            'objFormModuleView.AddField("Aging", "Aging", 2, False, True, NawaBLL.Common.MFieldType.INTValue, , , , , )
            objFormModuleView.AddField("submission_date", "Submission Date", 3, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )
            objFormModuleView.AddField("report_code", "Report Code", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
            'objFormModuleView.AddField("Report", "Jenis Laporan", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
            objFormModuleView.AddField("submission", "Submission", 6, False, False, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
            'objFormModuleView.AddField("transaction_date", "Tanggal Transaksi", 7, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,,)
            'objFormModuleView.AddField("TransaksiUnik", "Transaksi Unik", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
            objFormModuleView.AddField("ValueTransaksiUnik", "Unik Reference", 9, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
            objFormModuleView.AddField("submit_date", "Submit Date", 10, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,,)
            'objFormModuleView.AddField("PPATKNo", "No PPATK", 11, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
            objFormModuleView.AddField("statusPPATK", "Status", 12, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
            'objFormModuleView.AddField("ID_Laporan_PPATK_Result", "ID PPATK Result", 13, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
            objFormModuleView.AddField("isValid", "Is Valid", 13, False, True, NawaBLL.Common.MFieldType.BooleanValue,,,,, )
            objFormModuleView.AddField("MonthPeriod", "Month Period", 14, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
            objFormModuleView.AddField("lastUpdateDate", "Last Update Date", 15, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,,)
            objFormModuleView.AddField("statusID", "StatusID", 16, False, False, NawaBLL.Common.MFieldType.INTValue,,,,,)
            objFormModuleView.AddField("ErrorMessage", "Error Message", 17, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
            objFormModuleView.SettingFormView()

            Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
            objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"

            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            'Begin Penambahan Advanced Filter
            If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                Toolbar2.Hidden = True
            Else
                Toolbar2.Hidden = False
            End If
            LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")
            'END Penambahan Advanced Filter



            Dim intStart As Integer = e.Start
                Dim intLimit As Int16 = e.Limit
                Dim inttotalRecord As Integer
                Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

                Dim strsort As String = ""
                For Each item As DataSorter In e.Sort
                    If strsort = "" Then
                        strsort += item.Property & " " & item.Direction.ToString
                    Else
                        strsort += ", " & item.Property & " " & item.Direction.ToString
                    End If
                Next
                Me.indexStart = intStart
                Me.strWhereClause = strfilter




                'Begin Penambahan Advanced Filter
                If strWhereClause.Length > 0 Then
                    If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                        strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                        intStart = 0
                    End If
                Else
                    If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                        strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                        intStart = 0
                    End If
                End If
                'END Penambahan Advanced Filter

                If strsort = "" Then
                    strsort = "PK_REPORT_ID Desc"
                End If
                Me.strOrder = strsort

                Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
                'Dim DataPaging As Data.DataTable = DLL.SQLHelper.ExecuteTabelPaging("CustomerInformation_WebTempTable", "CIFNo, Name, DateOfBirth, AccountOwnerId, OpeningDate, WorkingUnitName, IsCustomerInList", strfilter, strsort, intStart, intLimit, inttotalRecord)
                ''-- start paging ------------------------------------------------------------
                Dim limit As Integer = e.Limit
                If (e.Start + e.Limit) > inttotalRecord Then
                    limit = inttotalRecord - e.Start
                End If
                'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
                ''-- end paging ------------------------------------------------------------
                e.Total = inttotalRecord
                GridpanelView.GetStore.DataSource = DataPaging
                GridpanelView.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update Penambahan Advance Filter

    Protected Sub ExportXmlForImport(sender As Object, e As EventArgs)
        Try
            Dim dtyperowselectionmodel As RowSelectionModel = TryCast(Me.GridpanelView.GetSelectionModel(), RowSelectionModel)
            If dtyperowselectionmodel.SelectedRows.Count > 0 Then
                Dim SelectionID As New List(Of String)
                Dim IDResultTemp As String = ""
                Dim counterflag As Integer = 1
                For Each item As SelectedRow In dtyperowselectionmodel.SelectedRows
                    SelectionID.Add(item.RecordID.ToString())
                    If counterflag = 1 Then
                        IDResultTemp = item.RecordID.ToString()
                        counterflag = counterflag + 1
                    Else
                        IDResultTemp = IDResultTemp & ", " & item.RecordID.ToString()
                    End If
                Next
                combinedReportID = IDResultTemp

                '5-Apr-2022 Adi : Hanya report2 yang statusnya 1=Waiting For Generate, 2=Waiting For Upload Reference No,
                '3=Reference No Uploaded can be selected
                Dim strQuery As String = "SELECT COUNT(1) FROM goAML_Report WHERE Status IN (1,2,3) AND PK_Report_ID IN (" & combinedReportID & ")"
                Dim countNotReadyForGenerate As Integer = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                If Not IsNothing(countNotReadyForGenerate) AndAlso countNotReadyForGenerate <> dtyperowselectionmodel.SelectedRows.Count Then
                    Throw New ApplicationException("The selected rows contain report(s) with status that not allowed to Generate XML.<br><br>Report status allowed to Generate XML:<br>1. Waiting For Generate<br>2. Waiting For Upload Reference No<br>3. Reference No Uploaded")
                End If

                BindReportID(StoreListReportID, SelectionID)
                Dim UserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                NawaDevBLL.GeneratePPATKBLL.InsertToTabelSelectedReport(UserID, IDResultTemp)
                NawaDevBLL.GeneratePPATKBLL.ValidateSchemaReportSelected(IDResultTemp)
                Dim dtreport As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT TOP(1) * FROM goAML_Generate_XML_" & UserID, Nothing)
                If dtreport IsNot Nothing Then
                    For Each item In dtreport.Rows
                        lblStatusUpload.Text = item(9)
                        lblTotalInvalid.Text = item(5)
                        lblTotalValid.Text = item(4)
                        If Convert.ToString(item(11)).ToLower = "true" Then
                            lblAlreadyValidateSchema.Text = "Yes"
                        Else
                            lblAlreadyValidateSchema.Text = "No"
                        End If
                        If Convert.ToString(item(12)).ToLower = "true" Then
                            lblValidateSchemaResultValid.Text = "Valid"
                        Else
                            lblValidateSchemaResultValid.Text = "Not Valid"
                        End If
                    Next
                End If
                Store1.Reload()
                PanelShowResult.Show()
            Else    '5-Apr-2022 Adi : show information if no reports selected
                Throw New ApplicationException("Please select minimum 1 report.")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindReportID(store As Store, list As List(Of String))
        Try
            Dim objtable As New DataTable
            objtable.Columns.Add(New DataColumn("ListReportID", GetType(String)))
            For Each Items As String In list
                Dim rows As DataRow = objtable.NewRow()
                rows("ListReportID") = Items
                objtable.Rows.Add(rows)
            Next
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub BtnDownloadSelected_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim ReportType As String = Request.Params("ReportType")
            'Dim FileReturn As String = ""

            '5-Apr-2022 Adi : Cegah Generate XML jika masih ada yg invalid. Confirmed by Bombom and Nunung
            'Dim isHarusValid As String = getParameterGlobalByPK(12)
            'If lblTotalValid.Text = "0" Then
            '    Throw New Exception("There is not data valid")
            'End If

            'If (lblValidateSchemaResultValid.Text = "Not Valid" Or lblValidateSchemaResultValid.Text = "") And isHarusValid = "1" Then
            '    'Throw New Exception("There is Validate Schema result Fail. Please fix the fail result first before generate xml")
            'End If

            If lblTotalInvalid.Text <> "0" Then
                Throw New ApplicationException("Can't generate XML. There are some invalid data.")
            End If
            If (lblValidateSchemaResultValid.Text = "Not Valid" Or lblValidateSchemaResultValid.Text = "") Then
                Throw New ApplicationException("There are some invalid data after Validate XSD Schema.<br>Please fix the invalid data before generate XML.")
            End If

            Dim path As String = Server.MapPath("~\goaml\FolderExport\")

            Dim Dirpath = path & NawaBLL.Common.SessionCurrentUser.UserID & "\"
            If Not Directory.Exists(Dirpath) Then
                Directory.CreateDirectory(Dirpath)
            End If

            Filetodownload = NawaDevBLL.GeneratePPATKBLL.GenerateReportXMLBySelectedReport(Date.Now, ReportType, Dirpath, combinedReportID)
            If Filetodownload Is Nothing Then
                'Throw New Exception("Not Exists Report")
                Throw New ApplicationException("There is no XML result to be downloaded.")
            End If
            'FileReturn = "C:\Users\NDS.NDS-LPT-0160\Documents\GoAML\Development\Web\Web\goaml\FolderExport\Sysadmin\20210211-20210127-001-LAPT.zip"
            'Response.Clear()
            'Response.ClearHeaders()
            'Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(FileReturn))
            'Response.Charset = ""
            'Response.AddHeader("cache-control", "max-age=0")
            'Me.EnableViewState = False
            'Response.ContentType = "application/zip"
            'Response.BinaryWrite(File.ReadAllBytes(FileReturn))
            'Response.End()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Sub Download()
        Try
            If Filetodownload <> "" Then
                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(Filetodownload))
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "application/zip"
                Response.BinaryWrite(IO.File.ReadAllBytes(Filetodownload))
                Response.End()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_DirectClick()
        Try
            PanelShowResult.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Function getParameterGlobalByPK(id As Integer) As String
        Try
            Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
                Dim paramValues As String = ""
                Dim paramGlobal As NawaDevDAL.goAML_Ref_ReportGlobalParameter = objdb.goAML_Ref_ReportGlobalParameter.Where(Function(x) x.PK_GlobalReportParameter_ID = id).FirstOrDefault

                If Not paramGlobal Is Nothing Then
                    paramValues = paramGlobal.ParameterValue
                End If
                Return paramValues
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub StoreValidation_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            StoreView.PageSize = SystemParameterBLL.GetPageSize
            Dim intStart As Integer = e.Start
            Dim intTotalRowCount As Long = 0

            Dim intLimit As Int16 = e.Limit
            Dim strfilter As String = Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)
            Dim strsort As String = ""

            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Dim strQuerySchema As String = "SELECT UnikReference, ErrorMessage FROM goAML_ValidateSchemaXSD_Report_" & Common.SessionCurrentUser.UserID
            If strfilter.Length > 0 Then
                strQuerySchema &= " where " & strfilter
            End If
            Dim objListParam(3) As SqlParameter
            objListParam(0) = New SqlParameter
            objListParam(0).ParameterName = "@querydata"
            objListParam(0).SqlDbType = SqlDbType.VarChar
            objListParam(0).Value = strQuerySchema
            objListParam(1) = New SqlParameter
            objListParam(1).ParameterName = "@orderby"
            objListParam(1).SqlDbType = SqlDbType.VarChar
            objListParam(1).Value = strsort
            objListParam(2) = New SqlParameter
            objListParam(2).ParameterName = "@PageNum"
            objListParam(2).SqlDbType = SqlDbType.Int
            objListParam(2).Value = intStart
            objListParam(3) = New SqlParameter
            objListParam(3).ParameterName = "@PageSize"
            objListParam(3).SqlDbType = SqlDbType.Int
            objListParam(3).Value = intLimit
            'query hanya ambil 1 record untuk simpen schemanya saja(pagesize=1) biar enteng
            Dim DataPaging As DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuerySchema, strsort, intStart, intLimit, intTotalRowCount)
            e.Total = intTotalRowCount

            GridPanelValidation.GetStore.DataSource = DataPaging
            GridPanelValidation.GetStore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

End Class
