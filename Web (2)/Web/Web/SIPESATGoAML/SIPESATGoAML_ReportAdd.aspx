﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="SIPESATGoAML_ReportAdd.aspx.vb" Inherits="SIPESATGoAML_ReportAdd" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelReport" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true">
        <DockedItems>
            <ext:Toolbar ID="ToolbarInput" ClientIDMode="Static" runat="server" EnableOverflow="true" Dock="Top" Layout="FitLayout" MaxHeight="150"  >                
                <Items>
                      <ext:InfoPanel ID="info_ValidationResult" ClientIDMode="Static" runat="server" UI="Warning" Layout="AnchorLayout" Title="Validation Result" AutoScroll="true" IconCls="#Error" MarginSpec="5 10 0 0" Border="True" Hidden="true" Collapsible="true"></ext:InfoPanel>
                </Items>
            </ext:Toolbar>                                                     
        </DockedItems>
        <Items>
            <%-- Report General Information --%>
            <ext:Panel runat="server" ID="pnl_GeneralInformation" Title="General Information" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="10">
                <Content>
                    <ext:DisplayField ID="txt_FK_Report_ID" runat="server" FieldLabel="Report ID" AnchorHorizontal="100%" />
                    <ext:DisplayField ID="txt_ReportStatus" runat="server" FieldLabel="Status" AnchorHorizontal="100%" />
                    <ext:DisplayField ID="txt_RentityID" runat="server" FieldLabel="Organization ID" AnchorHorizontal="70%" />
                    <NDS:NDSDropDownField ID="cmb_SubmisionCode" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="vw_SIPESATGoAML_Submission_Type" Label="Submission Type" AnchorHorizontal="70%" AllowBlank="false" />
                    <%--<NDS:NDSDropDownField ID="cmb_JenisLaporan" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" OnOnValueChanged="cmb_jenisLaporan_Change" StringTable="goAML_Ref_Jenis_Laporan" Label="Report Code" AnchorHorizontal="70%" AllowBlank="false" />--%>
                   <%-- <NDS:NDSDropDownField ID="cmb_JenisLaporan" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Jenis_Laporan" Label="Report Code" AnchorHorizontal="70%" AllowBlank="false" />--%>
                    <ext:DisplayField ID="cmb_JenisLaporan" runat="server" FieldLabel="Report Code" AnchorHorizontal="70%" />
                   <%-- <NDS:NDSDropDownField ID="cmb_ReportType" ValueField="PK_Ref_Report_Type" DisplayField="Description" runat="server" StringField="PK_Ref_Report_Type,Description" StringTable="vw_MappingJenisLaporanUnikID" Label="Report Unique Type" AnchorHorizontal="70%" AllowBlank="false" />
                    <ext:TextField runat="server" ID="txt_ReportUniqueValue" FieldLabel="Report Unique Value (CIF)" EnforceMaxLength="True" maxlength="50"  AllowBlank="false" AnchorHorizontal="70%" />--%>
                    <ext:DisplayField runat="server" ID="txt_ReportUniqueValue" FieldLabel="Report Unique Value (CIF)" AnchorHorizontal="70%" />
                    <ext:DisplayField ID="txt_Entity_Reference" runat="server" FieldLabel="Report Ref. Number" AnchorHorizontal="70%" />
                    
                    <%--<ext:DateField ID="df_MonthPeriod" runat="server" FieldLabel="Month Period" AnchorHorizontal="40%" Format="MM-yyyy" AllowBlank="false" />--%>
                    <NDS:NDSDropDownField ID="df_MonthPeriod" ValueField="valueDate" DisplayField="TextDate" runat="server" StringField="No,valueDate,TextDate" StringTable="vw_SIPESATGoAML_MONTH_PERIOD" Label="Month Period" AnchorHorizontal="70%" AllowBlank="false" />

                    <ext:DateField ID="txt_SubmissionDate" runat="server" FieldLabel="Submission Date" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" />
                    <ext:DisplayField ID="txt_CurrencyLocal" runat="server" FieldLabel="Local Currency" AnchorHorizontal="70%" />
                </Content>
            </ext:Panel>
            <%-- End of Report General Information --%>

            <%-- Reporting Office Location --%>
            <ext:Panel runat="server" ID="pnl_ReportingOffice" Title="Reporting Office Location" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="10">
                <Items>
                    <ext:DisplayField ID="txt_Rentity_AddressType" runat="server" FieldLabel="Address Type" AnchorHorizontal="100%" />
                    <ext:DisplayField ID="txt_Rentity_Address" runat="server" FieldLabel="ADDRESS" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="100"  />
                    <%--<ext:DisplayField ID="txt_Rentity_Town" runat="server" FieldLabel="Town" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />--%>
                    <ext:DisplayField ID="txt_Rentity_CITY" runat="server" FieldLabel="CITY" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <%--<ext:DisplayField ID="txt_Rentity_Zip" runat="server" FieldLabel="Zip Code" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="10" />--%>
                    <ext:DisplayField ID="txt_Rentity_Country" runat="server" FieldLabel="Country" AnchorHorizontal="100%" />
                    <ext:DisplayField ID="txt_Rentity_State" runat="server" FieldLabel="Province" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <%--<ext:DisplayField ID="txt_Rentity_Comments" runat="server" FieldLabel="Notes" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="4000"  />--%>
                </Items>
            </ext:Panel>
            <%-- End of Reporting Office Location --%>

            <%-- List of Activity --%>
            <ext:Panel runat="server" ID="pnl_Activity" Title="List of Activities" MarginSpec="0 0 10 0" Collapsible="true" Border="true" Layout="AnchorLayout" BodyPadding="0">
                <Items>
                    <ext:GridPanel ID="gp_Activity" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Activity_Add" Text="Add Activity" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Activity_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Activity" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model357" IDProperty="PK_ACT_REPORTPARTYTYPE_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_ACT_REPORTPARTYTYPE_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="PartyType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SIGNIFICANCE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="REASON" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn89" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column314" runat="server" DataIndex="PartyType" Text="Activity Subject" Flex="1"></ext:Column>
                                <ext:Column ID="Column315" runat="server" DataIndex="SIGNIFICANCE" Text="Significance" Flex="1"></ext:Column>
                                <ext:Column ID="Column316" runat="server" DataIndex="REASON" Text="Reason" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Activity" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_Activity">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_ACT_REPORTPARTYTYPE_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            <%-- End of List of Activity --%>

            <%-- List of Indicator --%>
<%--            <ext:Panel runat="server" ID="pnl_Indicator" Title="List of Indicators" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="0">
                <Items>
                    <ext:GridPanel ID="gp_Indicator" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Indicator_Add" Text="Add Indicator" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Indicator_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Indicator" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model355" IDProperty="PK_Report_Indicator">
                                        <Fields>
                                            <ext:ModelField Name="PK_Report_Indicator" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Indicator" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn88" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column312" runat="server" DataIndex="FK_Indicator" Text="Kode" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column313" runat="server" DataIndex="Keterangan" Text="Keterangan" Flex="1" CellWrap="true"></ext:Column>
                                <ext:CommandColumn ID="cc_Indicator" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_Indicator">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Report_Indicator" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>--%>
            <%-- End of List of Indicator --%>

            <%-- Approval History --%>
            <ext:Panel runat="server" ID="pnl_ApprovalHistory" Title="Approval History" MarginSpec="0 0 10 0" Collapsible="true" Hidden="true" Border="true" Layout="AnchorLayout" BodyPadding="0">
                <Items>
                    <ext:GridPanel ID="gp_ApprovalHistory" runat="server">
                        <Store>
                            <ext:Store ID="store_ApprovalHistory" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" IDProperty="PK_SIPESATGoAML_Note_History_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPESATGoAML_Note_History_ID" Type="Int"></ext:ModelField>
                                            <%--<ext:ModelField Name="FK_Module_ID" Type="Int"></ext:ModelField>--%>
                                            <ext:ModelField Name="UnikID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="UserID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UserName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Notes" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>

                        <ColumnModel runat="server">
                            <Columns>
                                <ext:RowNumbererColumn runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="User Name" DataIndex="UserName" CellWrap="true" flex="1" />
                                <ext:Column runat="server" Text="Status" DataIndex="Status" CellWrap="true" flex="1" />
                                <ext:DateColumn runat="server" Text="Created Date" DataIndex="CreatedDate" CellWrap="true" flex="1" Format="dd-MMM-yyyy hh:mm:ss" />
                                <ext:Column runat="server" Text="Notes" DataIndex="Notes" CellWrap="true" flex="4" />
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar24" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            <%-- End of Approval History --%>

            <ext:TextArea ID="txt_Report_ApprovalNotes" runat="server" FieldLabel="Submission Note" AnchorHorizontal="100%" AllowBlank="true" Margin="10"/>

        </Items>
        <Buttons>
            <ext:Button ID="btn_Report_Save" runat="server" Icon="Disk" Text="Save Report">
                <DirectEvents>
                    <Click OnEvent="btn_Report_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Report..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Report_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_Report_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                        <ExtraParams>
                            <ext:Parameter Name="isDeleteDraft" Value="0" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Report_Back_DeleteDraft" runat="server" Icon="Delete" Text="Back & Delete Draft" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btn_Report_Back_Click">
                        <Confirmation ConfirmRequest="true" Message="Are You sure to Delete the Draft?" Title="Back & Delete Draft"></Confirmation>
                        <EventMask ShowMask="true" Msg="Exit and Deleting Draft..." MinDelay="50"></EventMask>
                        <ExtraParams>
                            <ext:Parameter Name="isDeleteDraft" Value="1" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    
    <%-- ================== ACCOUNT PARTY ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_AccountParty" Title="Account Party" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
            <ext:Button ID="btn_Account_Import_Account" runat="server" Icon="Zoom" Text="Search Data Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_Account_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

            <%-- Account General Information --%>
            <ext:Panel runat="server" ID="pnl_Account_GeneralInfo" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_InstitutionName" FieldLabel="Financial Institution Name" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_InstitutionCode" FieldLabel="Financial Institution Code" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="50"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_SwiftCode" FieldLabel="Swift Code" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="50"/>
                    <%--<ext:Checkbox runat="server"  LabelWidth="200" ID="chk_Account_NonBankingInstitution" FieldLabel="Non Bank Institution"></ext:Checkbox>--%>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Branch" FieldLabel="Account Opening Branch" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Number" FieldLabel="Account Number" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="50" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Account_CurrencyCode" Label="Account Currency" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Mata_Uang" AnchorHorizontal="70%"/>
                    <%--<ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Label" FieldLabel="Account Name" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255"/>--%>
                    <%--<ext:TextField runat="server" LabelWidth="200" ID="txt_Account_IBAN" FieldLabel="IBAN" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="34" />--%>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_ClientNumber" FieldLabel="Client Number" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="30"/>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Account_Type" Label="Account Type" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_SIPESATGoAML_Jenis_Rekening" AnchorHorizontal="70%"/>

                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Account_OpeningDate" FieldLabel="Opening Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Account_ClosingDate" FieldLabel="Closing Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />
                    <%--<ext:NumberField runat="server" LabelWidth="200" ID="txt_Account_Balance" FieldLabel="Balance Amount" AnchorHorizontal="70%" MouseWheelEnabled="false" />
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Account_BalanceDate" FieldLabel="Balance Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />--%>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Account_StatusCode" Label="Account Status" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Status_Rekening" AnchorHorizontal="70%"/>
                    <%--<ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Beneficiary" FieldLabel="Beneficiary Name" AnchorHorizontal="70%"  EnforceMaxLength="True" maxlength="50"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_BeneficiaryComment" FieldLabel="Beneficiary Note" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />--%>
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_Account_Comment" FieldLabel="Notes" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"  />

                </Content>
            </ext:Panel>

            <%--Signatory--%>
            <ext:GridPanel ID="gp_Account_Signatory" runat="server" AutoScroll="true" Title="Account Signatory" Border="true" EmptyText="No data available" MarginSpec="10 0">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <TopBar>
                    <ext:Toolbar runat="server">
                        <Items>
                            <ext:Button runat="server" ID="btn_Account_Signatory_Add" Text="Add Signatory" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btn_Person_Entry_Click">
                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="persontype" Value="'AccountSignatory'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="store_Account_Signatory" runat="server">
                        <Model>
                            <ext:Model runat="server" ID="Model134" IDProperty="PK_Account_Signatory_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_Account_Signatory_ID" Type="String"></ext:ModelField>
                                    <%--<ext:ModelField Name="isPrimary" Type="Boolean"></ext:ModelField>--%>
                                    <ext:ModelField Name="LAST_NAME" Type="String"></ext:ModelField>
                                    <%--<ext:ModelField Name="RoleName" Type="String"></ext:ModelField>--%>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn26" runat="server" Text="No"></ext:RowNumbererColumn>
                        <%--<ext:Column ID="Column88" runat="server" DataIndex="isPrimary" Text="Primary" Flex="1"></ext:Column>--%>
                        <ext:Column ID="Column89" runat="server" DataIndex="LAST_NAME" Text="Name" Flex="1"></ext:Column>
                        <%--<ext:Column ID="Column90" runat="server" DataIndex="RoleName" Text="Role" Flex="1"></ext:Column>--%>
                        <ext:CommandColumn ID="cc_Account_Signatory" runat="server" Text="Action" MinWidth="220">
                            <Commands>
                                <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                    <ToolTip Text="Edit"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                    <ToolTip Text="Delete"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="btn_Person_Entry_Click">
                                    <EventMask ShowMask="true"></EventMask>
                                    <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Account_Signatory_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="partytype" Value="'AccountSignatory'" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
            </ext:GridPanel>

            <%-- Account Entity --%>
          <%--  <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Account_IsEntity" FieldLabel="Corporate Account" Checked="false" MarginSpec="10 0 0 0">
                <DirectEvents>
                    <Change OnEvent="chk_Account_IsEntity_Changed"></Change>
                </DirectEvents>
            </ext:Checkbox>--%>

            <ext:Panel runat="server" ID="pnl_Account_Entity" MarginSpec="0 0 10 0" Border="true" Title="Corporate Account Information" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%" Hidden="true">
                <Content>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_CorporateName" FieldLabel="Corporate Name" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <%--<ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_CommercialName" FieldLabel="Commercial Name" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Account_Entity_IncorporationLegalForm" Label="Corporate Legal Form" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Bentuk_Badan_Usaha" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_IncorporationNumber" FieldLabel="Business License Number" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />--%>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_Business" FieldLabel="Line of Business" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />

                    <%-- Account Entity Address --%>
                    <ext:GridPanel ID="gp_Account_Entity_Address" runat="server" AutoScroll="true" Title="ADDRESS" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Account_Entity_Address_Add" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Address_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Account_Entity_Address" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model19" IDProperty="PK_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AddressType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column8" runat="server" DataIndex="AddressType" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="ADDRESS" Text="ADDRESS" Flex="1"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="CITY" Text="CITY" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Account_Entity_Address" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Address_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Account Entity Phone --%>
                    <ext:GridPanel ID="gp_Account_Entity_Phone" runat="server" AutoScroll="true" Title="Phone" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Account_Entity_Phone_Add" Text="Add Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Phone_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Account_Entity_Phone" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model18" IDProperty="PK_Phone_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Phone_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ContactType" Type="String"></ext:ModelField>
                                            <%--<ext:ModelField Name="TPH_COUNTRY_PREFIX" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="TPH_COMMUNICATION_TYPE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TPH_NUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column5" runat="server" DataIndex="ContactType" Text="Contact Type" Flex="1"></ext:Column>
                                <%--<ext:Column ID="Column6" runat="server" DataIndex="TPH_COUNTRY_PREFIX" Text="Country Prefix" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column6" runat="server" DataIndex="TPH_COMMUNICATION_TYPE" Text="Communication Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="TPH_NUMBER" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Account_Entity_Phone" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Phone_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Phone_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%--<ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_Email" FieldLabel="Corporate Email" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_URL" FieldLabel="Corporate Website" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />--%>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_IncorporationState" FieldLabel="State/Province" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255"  />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Account_Entity_IncorporationCountryCode" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="70%"  />

                    <%-- Account Entity Director --%>
                    <%--<ext:GridPanel ID="gp_Account_Entity_Director" runat="server" AutoScroll="true" Title="Director" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Account_Entity_Director_Add" Text="Add Director" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Person_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'AccountEntityDirector'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Account_Entity_Director" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model21" IDProperty="PK_Account_Entity_Director_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Account_Entity_Director_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LAST_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleName" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn36" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column129" runat="server" DataIndex="LAST_NAME" Text="Name" Flex="1"></ext:Column>
                                <ext:Column ID="Column130" runat="server" DataIndex="RoleName" Text="Role" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Account_Entity_Director" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Person_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Account_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'AccountEntityDirector'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>--%>

                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Account_Entity_IncorporationDate" FieldLabel="Date of Establishment" AnchorHorizontal="40%" Format="dd-MMM-yyyy" />
                    <%--<ext:Checkbox runat="server" LabelWidth="200" ID="chk_Account_Entity_IsClosed" FieldLabel="Is Closed">
                        <DirectEvents>
                            <Change OnEvent="chk_Account_Entity_IsClosed_Changed"></Change>
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField runat="server" LabelWidth="200" Hidden="true" ID="txt_Account_Entity_ClosingDate" FieldLabel="Closing Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />--%>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_TaxNumber" FieldLabel="Tax Number" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="100"  />
                    <%--<ext:TextArea runat="server" LabelWidth="200" ID="txt_Account_Entity_Comment" FieldLabel="Notes" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"  />--%>
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
           <%-- <ext:Button ID="btn_Account_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Account_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>--%>
          <%--  <ext:Button ID="btn_Account_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Account_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>--%>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.95});" />
            <Resize Handler="#{window_AccountParty}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF PARTY EDIT WINDOWS (ACCOUNT) ========================== --%>

    <%-- ================== PERSON PARTY ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_PersonParty" Title="Person Party" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
            <ext:Button ID="btn_Person_Import_Customer" runat="server" Icon="Zoom" Text="Search Data Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_Customer_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <%--<ext:Button ID="btn_Person_Import_WIC" runat="server" Icon="Zoom" Text="Search Data Non Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_WIC_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>--%>

            <%-- Person Information --%>
            <ext:Panel runat="server" ID="pnl_PersonParty" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                      <%-- Relationship --%>
            <ext:Panel runat="server" ID="PnlRelationship" Title="Relationship" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="10">
                <Items>
                    <ext:TextField ID="TxtClientNumber_Relationship" runat="server"  LabelWidth="190" FieldLabel="Client Number" AnchorHorizontal="70%" MaxLength="255" EnforceMaxLength="true" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_IsApproxFrom_Relationship" FieldLabel="Is Approx From"></ext:Checkbox>
                    <ext:DateField runat="server"  LabelWidth="190" ID="TxtValidFrom_Relationship" FieldLabel="Valid From" AnchorHorizontal="60%" Format="dd-MMM-yyyy" AllowBlank="false"/>
                    <%--<ext:DisplayField ID="txt_Rentity_Town" runat="server" FieldLabel="Town" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />--%>
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_IsApproxTo_Relationship" FieldLabel="Is Approx To"></ext:Checkbox>
                    <ext:DateField runat="server"  LabelWidth="190" ID="TxtValidTo_Relationship" FieldLabel="Valid To" AnchorHorizontal="60%" Format="dd-MMM-yyyy" AllowBlank="false" />
                    <%--<ext:DisplayField ID="txt_Rentity_Zip" runat="server" FieldLabel="Zip Code" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="10" />--%>
                    <ext:TextArea runat="server"  LabelWidth="190" ID="txtComments_Relationship" FieldLabel="Comments" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="8000"  />
                                    </Items>
            </ext:Panel>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_Gender" Label="Gender" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Jenis_Kelamin" AnchorHorizontal="70%"   />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Title" FieldLabel="Title" EnforceMaxLength="True" maxlength="30" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_LastName" FieldLabel="Full Name" EnforceMaxLength="True" maxlength="100" AnchorHorizontal="70%" />
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Person_BirthDate" FieldLabel="Date of Birth" AnchorHorizontal="60%" Format="dd-MMM-yyyy" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_BirthPlace" FieldLabel="Place of Birth" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_MotherName" FieldLabel="Mother's Name" EnforceMaxLength="True" maxlength="100" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Alias" FieldLabel="Alias Name" EnforceMaxLength="True" maxlength="100" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_SSN" FieldLabel="SSN/NIK" EnforceMaxLength="True" maxlength="25" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_PassportNumber" FieldLabel="Passport No." EnforceMaxLength="True" maxlength="25" AnchorHorizontal="70%" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_PassportCountry" Label="Passport Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_IDNumber" FieldLabel="Other ID Number" EnforceMaxLength="True" maxlength="25" AnchorHorizontal="70%" />

                    <%-- Person Address --%>
                    <ext:GridPanel ID="gp_Person_Address" runat="server" AutoScroll="true" Title="ADDRESS" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Person_Address_Add" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Address_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Person_Address" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model31" IDProperty="PK_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AddressType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column21" runat="server" DataIndex="AddressType" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="ADDRESS" Text="ADDRESS" Flex="1"></ext:Column>
                                <ext:Column ID="Column23" runat="server" DataIndex="CITY" Text="CITY" Flex="1"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Person_Address" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Address_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Person Phone --%>
                    <ext:GridPanel ID="gp_Person_Phone" runat="server" AutoScroll="true" Title="Phone" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Person_Phone_Add" Text="Add Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Phone_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Person_Phone" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model30" IDProperty="PK_Phone_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Phone_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ContactType" Type="String"></ext:ModelField>
                                            <%--<ext:ModelField Name="TPH_COUNTRY_PREFIX" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="TPH_COMMUNICATION_TYPE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TPH_NUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column18" runat="server" DataIndex="ContactType" Text="Contact Type" Flex="1"></ext:Column>
                                <%--<ext:Column ID="Column19" runat="server" DataIndex="TPH_COUNTRY_PREFIX" Text="Country Prefix" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column19" runat="server" DataIndex="TPH_COMMUNICATION_TYPE" Text="Communication Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="TPH_NUMBER" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Person_Phone" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Phone_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Phone_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_Nationality1" Label="Nationality 1" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_Nationality2" Label="Nationality 2" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_Nationality3" Label="Nationality 3" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_Residence" Label="Domicile Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Email1" FieldLabel="Email 1" EnforceMaxLength="True" maxlength="25" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Email2" FieldLabel="Email 2" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Email3" FieldLabel="Email 3" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Email4" FieldLabel="Email 4" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Email5" FieldLabel="Email 5" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Occupation" FieldLabel="Occupation" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_EmployerName" FieldLabel="Work Place" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />

                    <%-- Person Employer Address --%>
                    <ext:GridPanel ID="gp_Person_EmployerAddress" runat="server" AutoScroll="true" Title="Work Place Address" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Person_EmployerAddress_Add" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Address_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="1" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Person_EmployerAddress" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model36" IDProperty="PK_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AddressType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column25" runat="server" DataIndex="AddressType" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="ADDRESS" Text="ADDRESS" Flex="1"></ext:Column>
                                <ext:Column ID="Column27" runat="server" DataIndex="CITY" Text="CITY" Flex="1"></ext:Column>
                                <ext:Column ID="Column28" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Person_EmployerAddress" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Address_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="1" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Person Employer Phone --%>
                    <ext:GridPanel ID="gp_Person_EmployerPhone" runat="server" AutoScroll="true" Title="Work Place Phone" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Person_EmployerPhone_Add" Text="Add Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Phone_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="1" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Person_EmployerPhone" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model37" IDProperty="PK_Phone_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Phone_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ContactType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TPH_COUNTRY_PREFIX" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TPH_NUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column29" runat="server" DataIndex="ContactType" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column30" runat="server" DataIndex="TPH_COUNTRY_PREFIX" Text="Country Prefix" Flex="1"></ext:Column>
                                <ext:Column ID="Column31" runat="server" DataIndex="TPH_NUMBER" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Person_EmployerPhone" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Phone_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Phone_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="1" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Person Identification --%>
                    <ext:GridPanel ID="gp_Person_Identification" runat="server" AutoScroll="true" Title="Identification" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Person_Identification_Add" Text="Add Identification" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Identification_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Person_Identification" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model38" IDProperty="PK_Identification_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TYPE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NUMBER" Type="String"></ext:ModelField>
                                            <%--<ext:ModelField Name="ISSUED_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EXPIRY_DATE" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn32" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column108" runat="server" DataIndex="TYPE" Text="Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column109" runat="server" DataIndex="NUMBER" Text="Number" Flex="1" MinWidth="150"></ext:Column>
                                <%--<ext:DateColumn ID="Column110" runat="server" DataIndex="ISSUED_DATE" Text="Issue Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:DateColumn ID="Column111" runat="server" DataIndex="EXPIRY_DATE" Text="Expired Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>--%>
                                <ext:Column ID="Column112" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Person_Identification" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Identification_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Person_IsDecease" FieldLabel="Has Passed Away">
                        <DirectEvents>
                            <Change OnEvent="chk_Person_IsDecease_Changed" />
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Person_DeceaseDate" FieldLabel="Date of Passed Away" AnchorHorizontal="60%" Format="dd-MMM-yyyy" Hidden="true"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_TaxNumber" FieldLabel="Tax Number" EnforceMaxLength="True" maxlength="100" AnchorHorizontal="70%" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Person_IsPEP" FieldLabel="PEP"></ext:Checkbox>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_SourceOfWealth" FieldLabel="Source of Wealth" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_Person_Comment" FieldLabel="Notes" EnforceMaxLength="True" maxlength="4000" AnchorHorizontal="70%" />

                    <%-- For Account Signatory 
                    <%--<ext:Checkbox runat="server" LabelWidth="200" ID="chk_Person_AccountSignatory_IsPrimary" FieldLabel="Is Primary"></ext:Checkbox>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_AccountSignatory_Role" Label="Role" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Peran_orang_dalam_rekening" AnchorHorizontal="70%"/>--%>

                    <%-- For Director --%>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_Director_Role" Label="Role" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Peran_orang_dalam_Korporasi" AnchorHorizontal="70%"/>
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Person_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Person_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Person_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Person_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.85, height: size.height * 0.8});" />
            <Resize Handler="#{window_PersonParty}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF PERSON PARTY ENTRY WINDOWS ========================== --%>

    <%-- ================== ENTITY PARTY ENTRY WINDOWS ========================== --%>
    <%--<ext:Window ID="window_EntityParty" Title="Entity Party" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
            <ext:Button ID="btn_Entity_Import_Customer" runat="server" Icon="Zoom" Text="Search Data Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_Customer_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Entity_Import_WIC" runat="server" Icon="Zoom" Text="Search Data Non Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_WIC_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>--%>

            <%-- Entity Information --%>
            <%--<ext:Panel runat="server" ID="pnl_EntityParty" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_CorporateName" FieldLabel="Corporate Name" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_CommercialName" FieldLabel="Commercial Name" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Entity_IncorporationLegalForm" Label="Legal Form" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Bentuk_Badan_Usaha" AnchorHorizontal="70%"   />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_IncorporationNumber" FieldLabel="Corporate License Number" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_Business" FieldLabel="Line of Business" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />--%>

                    <%-- Entity Address --%>
                    <%--<ext:GridPanel ID="gp_Entity_Address" runat="server" AutoScroll="true" Title="ADDRESS" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Entity_Address_Add" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Address_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Entity_Address" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model25" IDProperty="PK_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Address_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="AddressType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column15" runat="server" DataIndex="AddressType" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="ADDRESS" Text="ADDRESS" Flex="1"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="CITY" Text="CITY" Flex="1"></ext:Column>
                                <ext:Column ID="Column32" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Entity_Address" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Address_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>--%>

                    <%-- Entity Phone --%>
                    <%--<ext:GridPanel ID="gp_Entity_Phone" runat="server" AutoScroll="true" Title="Phone" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Entity_Phone_Add" Text="Add Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Phone_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Entity_Phone" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model24" IDProperty="PK_Phone_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Phone_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="ContactType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TPH_COUNTRY_PREFIX" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TPH_NUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column12" runat="server" DataIndex="ContactType" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="TPH_COUNTRY_PREFIX" Text="Country Prefix" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="TPH_NUMBER" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Entity_Phone" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Phone_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Phone_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_Email" FieldLabel="Corporate Email" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_URL" FieldLabel="Corporate Website" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_IncorporationState" FieldLabel="Province" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255"  />
                    <NDS:NDSDropDownField LabelWidth="200" ID="cmb_Entity_IncorporationCountryCode" Label="Country" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"  />--%>

                    <%-- Entity Director --%>
                    <%--<ext:GridPanel ID="gp_Entity_Director" runat="server" AutoScroll="true" Title="Director" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Entity_Director_Add" Text="Add Director" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Person_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'EntityDirector'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Entity_Director" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model22" IDProperty="PK_Entity_Director_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Entity_Director_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="LAST_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleName" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn43" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column154" runat="server" DataIndex="LAST_NAME" Text="Name" Flex="1"></ext:Column>
                                <ext:Column ID="Column155" runat="server" DataIndex="RoleName" Text="Role" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Entity_Director" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Person_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'EntityDirector'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Entity_IncorporationDate" FieldLabel="Date of Established" AnchorHorizontal="40%" Format="dd-MMM-yyyy" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Entity_IsClosed" FieldLabel="Is Closed">
                        <DirectEvents>
                            <Change OnEvent="chk_Entity_IsClosed_Changed"></Change>
                        </DirectEvents>
                    </ext:Checkbox>

                    <ext:DateField runat="server" Hidden="true" LabelWidth="200" ID="txt_Entity_ClosingDate" FieldLabel="Closing Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_TaxNumber" FieldLabel="Tax Number" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="100"  />
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_Entity_Comment" FieldLabel="Notes" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"  />
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Entity_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Entity_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Entity_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Entity_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.95});" />
            <Resize Handler="#{window_EntityParty}.center()" />
        </Listeners>
    </ext:Window>--%>
    <%-- ================== END OF ENTITY PARTY ENTRY WINDOWS ========================== --%>

    <%-- ================== PHONE ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_Phone" Layout="AnchorLayout" Title="Phone" runat="server" Maximizable="true" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:Panel runat="server" ID="pnl_Phone" MarginSpec="0 0 10 0" Layout="AnchorLayout" BodyPadding="20" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_Phone_ContactType" Label="Contact Type" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Kategori_Kontak" AnchorHorizontal="100%"  />
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_Phone_CommunicationType" Label="Communication Type" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Jenis_Alat_Komunikasi" AnchorHorizontal="100%"  />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Phone_CountryPrefix" FieldLabel="Country Prefix" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="4" />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Phone_Number" FieldLabel="Phone Number" AnchorHorizontal="100%" EnableKeyEvents="true" MaskRe="[0-9]" EnforceMaxLength="True" maxlength="50" />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Phone_Extension" FieldLabel="Extension" AnchorHorizontal="100%" EnableKeyEvents="true" MaskRe="[0-9]" EnforceMaxLength="True" maxlength="50" />
                    <ext:TextArea runat="server" LabelWidth="150" ID="txt_Phone_Comment" FieldLabel="Notes" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="4000"  />
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Phone_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Phone_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Phone_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Phone_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.6, height: size.height * 0.8});" />
            <Resize Handler="#{window_Phone}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF PHONE ENTRY WINDOWS ========================== --%>

    <%-- ================== ADDRESS ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_Address" Layout="AnchorLayout" Title="ADDRESS" runat="server" Modal="true" Hidden="true" Maximizable="true" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:Panel runat="server" ID="pnl_Address" MarginSpec="0 0 10 0" Layout="AnchorLayout" BodyPadding="20" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_Address_Type" Label="Address Type" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Kategori_Kontak" AnchorHorizontal="100%"  />
                    <ext:TextArea runat="server" LabelWidth="150" ID="txt_Address_Address" FieldLabel="ADDRESS" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="100"  />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Address_Town" FieldLabel="Town" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Address_CITY" FieldLabel="CITY" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Address_Zip" FieldLabel="Zip Code" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="10" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_Address_Country" Label="Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="100%" OnOnValueChanged="cmb_Address_Country_Changed" />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Address_State" FieldLabel="Province" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <ext:TextArea runat="server" LabelWidth="150" ID="txt_Address_Comment" FieldLabel="Notes" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="4000"  />
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Address_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Address_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Address_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Address_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.6, height: size.height * 0.95});" />
            <Resize Handler="#{window_Address}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF ADDRESS ENTRY WINDOWS ========================== --%>

    <%-- ================== IDENTIFICATION ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_Identification" Layout="AnchorLayout" Title="Identification" runat="server" Modal="true" Hidden="true" Maximizable="true" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:Panel runat="server" ID="pnl_Identification" MarginSpec="0 0 10 0" Layout="AnchorLayout" BodyPadding="20" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_Identification_Type" Label="Identification Type" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Jenis_Dokumen_Identitas" AnchorHorizontal="100%"  />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Identification_Number" FieldLabel="Identification Number" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <ext:DateField runat="server" LabelWidth="150" ID="txt_Identification_IssueDate" FieldLabel="Issue Date" Format="dd-MMM-yyyy" AnchorHorizontal="60%" />
                    <ext:DateField runat="server" LabelWidth="150" ID="txt_Identification_ExpiredDate" FieldLabel="Expired Date" Format="dd-MMM-yyyy" AnchorHorizontal="60%" />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Identification_IssueBy" FieldLabel="Issue By" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_Identification_Country" Label="Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="100%"  />
                    <ext:TextArea runat="server" LabelWidth="150" ID="txt_Identification_Comment" FieldLabel="Notes" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="4000"  />
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Identification_Save" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">
                <DirectEvents>
                    <Click OnEvent="btn_Identification_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Identification_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Identification_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.6, height: size.height * 0.8});" />
            <Resize Handler="#{Window_Identification}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF IDENTIFICATION ENTRY WINDOWS ========================== --%>
    
    <%-- ================== ACTIVITY ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_Activity" Layout="AnchorLayout" Title="Activity" runat="server" Modal="true" Hidden="true" Maximized="true" BodyPadding="10" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:InfoPanel ID="info_ValidationResultActivity" ClientIDMode="Static" runat="server" UI="Warning" Layout="AnchorLayout" Title="Validation Result" AutoScroll="true" IconCls="#Error" MarginSpec="10 0" Border="True" Hidden="true" Collapsible="true"></ext:InfoPanel>

            <ext:Panel runat="server" ID="Panel2" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <%--<NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Activity_Information" Label="Party Type" ValueField="PK_Sender_To_Information" DisplayField="Sender_To_Information" StringField="PK_Sender_To_Information,Sender_To_Information" StringTable="GoAML_Ref_Sender_To_Information" AnchorHorizontal="70%" OnOnValueChanged="cmb_Activity_Information_Changed" />--%>
                    <%--<NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Activity_Information" Label="Party Type" ValueField="PK_Sender_To_Information" DisplayField="Sender_To_Information" StringField="PK_Sender_To_Information,Sender_To_Information" StringTable="GoAML_Ref_Sender_To_Information" AnchorHorizontal="70%" OnOnValueChanged="cmb_Activity_Information_Changed" />--%>
                 <%--   <ext:DisplayField runat="server" LabelWidth="200" ID="cmb_Activity_Information" FieldLabel="Party Type" AnchorHorizontal="70%" Text="Account" FieldStyle="background-color: #FFE4C4;"></ext:DisplayField>--%>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Activity_Information" Label="Party Type" ValueField="PK_Sender_To_Information" DisplayField="Sender_To_Information" StringField="PK_Sender_To_Information,Sender_To_Information" StringTable="GoAML_Ref_Sender_To_Information" AnchorHorizontal="70%" OnOnValueChanged="cmb_Activity_Information_Changed" StringFilter="PK_Sender_To_Information <> 1" AllowBlank="false" />
                    <ext:NumberField runat="server" LabelWidth="200" ID="txt_Activity_Significance" FieldLabel="Significance" AnchorHorizontal="70%" MaxLength="4" EnforceMaxLength="True" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Activity_Reason" FieldLabel="Reason" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"/>
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_Activity_Comment" FieldLabel="Notes" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"  />

                    <ext:FieldSet runat="server" ColumnWidth="0.5" ID="FieldSet5" Layout="AnchorLayout" Padding="10" MarginSpec="20 0 0 0" Title="Party Information">
                        <Content>
                            <ext:DisplayField runat="server" LabelWidth="150" ID="txt_Activity_CIFNO_ACCOUNTNO" FieldLabel="CIF" AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:DisplayField runat="server" LabelWidth="150" ID="df_Activity_Name" FieldLabel="Name" AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:TextField runat="server" LabelWidth="150" ID="txt_Activity_Name" FieldLabel="Name" Hidden="true" AnchorHorizontal="100%"></ext:TextField>
                            <%--<ext:DisplayField runat="server" LabelWidth="150" ID="txt_Activity_Address" FieldLabel="ADDRESS" Hidden="true" AnchorHorizontal="100%"></ext:DisplayField>--%>

                            <ext:Button ID="btn_Activity_Edit" runat="server" Icon="Pencil" Text="Edit" MarginSpec="0 10 0 0">
                                <DirectEvents>
                                    <Click OnEvent="btn_PartyEntry_Click">
                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Edit'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="partyside" Value="'Activity'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btn_Activity_Detail" runat="server" Icon="ApplicationViewDetail" Text="Detail">
                                <DirectEvents>
                                    <Click OnEvent="btn_PartyEntry_Click">
                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Detail'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="partyside" Value="'Activity'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Content>
                    </ext:FieldSet>
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Activity_Save" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">
                <DirectEvents>
                    <Click OnEvent="btn_Activity_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Activity_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Activity_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.95});" />
            <Resize Handler="#{Window_Activity}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF MULTIPARTY ENTRY WINDOWS ========================== --%>

    <%-- ================== INDICATOR ENTRY WINDOWS ========================== --%>
    <%--<ext:Window ID="Window_Indicator" Layout="AnchorLayout" Title="Report Indicator" runat="server" Modal="true" Hidden="true" Maximizable="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
		   <ext:Panel ID="pnl_Cmb_Indicator" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField ID="cmb_Indicator" LabelWidth="120" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="vw_goaml_ref_indicator_laporan" StringFilter="active=1" Label="Report Indicator" AnchorHorizontal="100%" AllowBlank="false" />
                </Content>
            </ext:Panel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.8, height: size.height * 0.5});" />
            <Resize Handler="#{Window_Indicator}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_Indicator_Save" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">
                <DirectEvents>
                    <Click OnEvent="btn_Indicator_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Indicator_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Indicator_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>--%>
    <%-- ================== END OF INDICATOR ENTRY WINDOWS ========================== --%>
    
    <%-- ================== IMPORT PARTY WINDOWS ========================== --%>
    <ext:Window ID="window_Import_Party" runat="server" Modal="true" Hidden="true" BodyStyle="padding:0px" AutoScroll="true" ButtonAlign="Center" MinHeight="430">
        <Items>
            <%-- Grid Panel Account --%>
          <%--  <ext:GridPanel ID="gp_Import_Party_Account" runat="server" ClientIDMode="Static" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store_Import_Party_Account" runat="server" IsPagingStore="true" RemoteFilter="true"  RemoteSort="true" 
                        OnReadData="store_ReadData_Import_Party_Account" RemotePaging="true" ClientIDMode="Static" PageSize="10">
                        <Model>
                            <%--<ext:Model runat="server" ID="model_Import_Party_Account" IDProperty="Account_No">--%>
                            <%--<ext:Model runat="server" ID="model_Import_Party_Account" IDProperty="PK_Customer_ID">
                                <Fields>--%>
                                    <%--<ext:ModelField Name="CIF" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Account_No" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Account_Name" Type="String"></ext:ModelField>--%>
		                        <%--    <ext:ModelField Name="PK_Customer_ID" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="FK_Customer_Type_ID" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="Customer_Type" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="NIK" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="TanggalLahir" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="NamaLengkap" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="filterHeader_Import_Party_Account" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>--%>
                        <%--<ext:RowNumbererColumn ID="RowNumber_Import_Party_Account" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="col_Import_Party_Account_CIF" runat="server" DataIndex="CIF" Text="CIF" Width="150"></ext:Column>
                        <ext:Column ID="col_Import_Party_Account_AccNo" runat="server" DataIndex="Account_No" Text="Account No" Width="150" ></ext:Column>
                        <ext:Column ID="col_Import_Party_Account_Name" runat="server" DataIndex="Account_Name" Text="Name" Flex="1"></ext:Column>--%>
		               <%-- <ext:RowNumbererColumn ID="RowNumber_Import_Party_Customer_Individual" runat="server" Text="No"></ext:RowNumbererColumn>
		                <ext:Column ID="col_Import_Party_Customer_Individual_PK" runat="server" DataIndex="PK_Customer_ID" Text="PK_Customer_ID" Flex="1" Hidden="true"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Individual_CIF" runat="server" DataIndex="CIF" Text="CIF" Flex="1"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Individual_Type" runat="server" DataIndex="Customer_Type" Text="Customer Type" Flex="1"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Individual_NIK" runat="server" DataIndex="NIK" Text="NIK" Width="150"></ext:Column>
		                <ext:DateColumn ID="col_Import_Party_Customer_Individual_TanggalLahir" runat="server" DataIndex="TanggalLahir" Text="Tanggal Lahir" Flex="1" Format="dd-MMM-Y"></ext:DateColumn>
		                <ext:Column ID="col_Import_Party_Customer_Individual_Name" runat="server" DataIndex="NamaLengkap" Text="Name" Width="300"></ext:Column>
                        <ext:CommandColumn ID="cc_Import_Party_Account" runat="server" Text="Action">

                        <Commands>
                            <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
                                <ToolTip Text="Select"></ToolTip>
                            </ext:GridCommand>
                        </Commands>

                        <DirectEvents>
                            <Command OnEvent="gc_Import_Party_Account">
                                <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="200"></EventMask>
                                <ExtraParams>
                                    <ext:Parameter Name="PK_Customer_ID" Value="record.data.PK_Customer_ID" Mode="Raw"></ext:Parameter>
                                    <ext:Parameter Name="CIF" Value="record.data.CIF" Mode="Raw"></ext:Parameter>
                                </ExtraParams>
                            </Command>
                        </DirectEvents>
                    </ext:CommandColumn>
                </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_Import_Party_Account" runat="server" HideRefresh="false" >
                        <Items>  --%>
                            <%--<ext:Button runat="server" Text="Close Picker" ID="Button5">
                                <Listeners>
                                    <Click Handler="#{window_Import_Party}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>--%>
                   <%--     </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>--%>

            <%-- Grid Panel WIC Individual --%>
            <ext:GridPanel ID="gp_Import_Party_WIC_Individual" runat="server" ClientIDMode="Static" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="false" />
                </View>
                <Store>
                    <ext:Store ID="store_Import_Party_WIC_Individual" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" 
                        OnReadData="store_ReadData_Import_Party_WIC_Individual" RemotePaging="true" ClientIDMode="Static" PageSize="10">
                <Model>
                    <ext:Model runat="server" ID="model_Import_Party_WIC_Individual" IDProperty="PK_Customer_ID">
                        <Fields>
                            <ext:ModelField Name="PK_Customer_ID" Type="String"></ext:ModelField>
                            <ext:ModelField Name="WIC_No" Type="String"></ext:ModelField>
                            <ext:ModelField Name="FK_Customer_Type_ID" Type="String"></ext:ModelField>
                            <ext:ModelField Name="WIC_Type" Type="String"></ext:ModelField>
                            <ext:ModelField Name="NIK" Type="String"></ext:ModelField>
                            <ext:ModelField Name="TanggalLahir" Type="String"></ext:ModelField>
                            <ext:ModelField Name="NamaLengkap" Type="String"></ext:ModelField>
                        </Fields>
                    </ext:Model>
                </Model>
                        <Sorters>
                        </Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="filterHeader_Import_Party_WIC_Individual" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumber_Import_Party_WIC_Individual" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="col_Import_Party_WIC_Individual_PK" runat="server" DataIndex="PK_Customer_ID" Text="PK_Customer_ID" Flex="1" Hidden="true"></ext:Column>
                        <ext:Column ID="col_Import_Party_WIC_Individual_No" runat="server" DataIndex="WIC_No" Text="WIC No" Flex="1"></ext:Column>
                        <ext:Column ID="col_Import_Party_WIC_Individual_Type" runat="server" DataIndex="WIC_Type" Text="WIC Type" Flex="1"></ext:Column>
                        <ext:Column ID="col_Import_Party_WIC_Individual_NIK" runat="server" DataIndex="NIK" Text="NIK" Width="150"></ext:Column>
                        <ext:DateColumn ID="col_Import_Party_WIC_Individual_TanggalLahir" runat="server" DataIndex="TanggalLahir" Text="Tanggal Lahir" Flex="1" Format="dd-MMM-Y"></ext:DateColumn>
                        <ext:Column ID="col_Import_Party_WIC_Individual_Name" runat="server" DataIndex="NamaLengkap" Text="Name" Width="300"></ext:Column>
                        <ext:CommandColumn ID="cc_Import_Party_WIC_Individual" runat="server" Text="Action">
                            <Commands>
                                <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
                                    <ToolTip Text="Select"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_Import_Party_WIC">
                                    <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="200"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="PK_Customer_ID" Value="record.data.PK_Customer_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="WIC_No" Value="record.data.WIC_No" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_Import_Party_WIC_Individual" runat="server" HideRefresh="false" >
                        <Items>  
           <%-- Comment Item--%>
                            <ext:Button runat="server" Text="Close Picker" ID="btn_Import_Party_WIC_Individual">
                                <Listeners>
                                    <Click Handler="#{window_Import_Party}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>
           <%-- Comment Item--%>
                     </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            
            <%-- Grid Panel WIC Corporate --%>
            <ext:GridPanel ID="gp_Import_Party_WIC_Corporate" runat="server" ClientIDMode="Static" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="false" />
                </View>
                <Store>
                    <ext:Store ID="store_Import_Party_WIC_Corporate" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" 
                        OnReadData="store_ReadData_Import_Party_WIC_Corporate" RemotePaging="true" ClientIDMode="Static" PageSize="10">
                <Model>
                    <ext:Model runat="server" ID="model_Import_Party_WIC_Corporate" IDProperty="PK_Customer_ID">
                        <Fields>
                            <ext:ModelField Name="PK_Customer_ID" Type="String"></ext:ModelField>
                            <ext:ModelField Name="WIC_No" Type="String"></ext:ModelField>
                            <ext:ModelField Name="FK_Customer_Type_ID" Type="String"></ext:ModelField>
                            <ext:ModelField Name="WIC_Type" Type="String"></ext:ModelField>
                            <ext:ModelField Name="NIK" Type="String"></ext:ModelField>
                            <ext:ModelField Name="TanggalLahir" Type="String"></ext:ModelField>
                            <ext:ModelField Name="NamaLengkap" Type="String"></ext:ModelField>
                        </Fields>
                    </ext:Model>
                </Model>
                        <Sorters>
                        </Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="filterHeader_Import_Party_WIC_Corporate" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumber_Import_Party_WIC_Corporate" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="col_Import_Party_WIC_Corporate_PK" runat="server" DataIndex="PK_Customer_ID" Text="PK_Customer_ID" Flex="1" Hidden="true"></ext:Column>
                        <ext:Column ID="col_Import_Party_WIC_Corporate_No" runat="server" DataIndex="WIC_No" Text="WIC No" Flex="1"></ext:Column>
                        <ext:Column ID="col_Import_Party_WIC_Corporate_Type" runat="server" DataIndex="WIC_Type" Text="WIC Type" Flex="1"></ext:Column>
                        <ext:Column ID="col_Import_Party_WIC_Corporate_NIK" runat="server" DataIndex="NIK" Text="NIK" Width="150"></ext:Column>
                        <ext:DateColumn ID="col_Import_Party_WIC_Corporate_TanggalLahir" runat="server" DataIndex="TanggalLahir" Text="Tanggal Lahir" Flex="1" Format="dd-MMM-Y"></ext:DateColumn>
                        <ext:Column ID="col_Import_Party_WIC_Corporate_Name" runat="server" DataIndex="NamaLengkap" Text="Name" Width="300"></ext:Column>
                        <ext:CommandColumn ID="cc_Import_Party_WIC_Corporate" runat="server" Text="Action">
                            <Commands>
                                <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
                                    <ToolTip Text="Select"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_Import_Party_WIC">
                                    <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="200"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="PK_Customer_ID" Value="record.data.PK_Customer_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="WIC_No" Value="record.data.WIC_No" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_Import_Party_WIC_Corporate" runat="server" HideRefresh="false" >
                        <Items>  
           <%-- Comment Item--%>
                            <ext:Button runat="server" Text="Close Picker" ID="Button_Import_Party_WIC_Corporate">
                                <Listeners>
                                    <Click Handler="#{window_Import_Party}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>
           <%-- Comment Item--%>
                      </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>

            <%-- Grid Panel Customer Individual --%>
            <ext:GridPanel ID="gp_Import_Party_Customer_Individual" runat="server" ClientIDMode="Static" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="false" />
                </View>
                <Store>
                    <ext:Store ID="store_Import_Party_Customer_Individual" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" 
                        OnReadData="store_ReadData_Import_Party_Customer_Individual" RemotePaging="true" ClientIDMode="Static" PageSize="10">
		                <Model>
		                    <ext:Model runat="server" ID="model_Import_Party_Customer_Individual" IDProperty="PK_Customer_ID">
		                        <Fields>
		                            <ext:ModelField Name="PK_Customer_ID" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="FK_Customer_Type_ID" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="Customer_Type" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="NIK" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="TanggalLahir" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="NamaLengkap" Type="String"></ext:ModelField>
		                        </Fields>
		                    </ext:Model>
		                </Model>
		                <Sorters></Sorters>
		                <Proxy>
		                    <ext:PageProxy />
		                </Proxy>
	                </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="filterHeader_Import_Party_Customer_Individual" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
		                <ext:RowNumbererColumn ID="RowNumber_Import_Party_Customer_Individual" runat="server" Text="No"></ext:RowNumbererColumn>
		                <ext:Column ID="col_Import_Party_Customer_Individual_PK" runat="server" DataIndex="PK_Customer_ID" Text="PK_Customer_ID" Flex="1" Hidden="true"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Individual_CIF" runat="server" DataIndex="CIF" Text="CIF" Flex="1"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Individual_Type" runat="server" DataIndex="Customer_Type" Text="Customer Type" Flex="1"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Individual_NIK" runat="server" DataIndex="NIK" Text="NIK" Width="150"></ext:Column>
		                <ext:DateColumn ID="col_Import_Party_Customer_Individual_TanggalLahir" runat="server" DataIndex="TanggalLahir" Text="Tanggal Lahir" Flex="1" Format="dd-MMM-Y"></ext:DateColumn>
		                <ext:Column ID="col_Import_Party_Customer_Individual_Name" runat="server" DataIndex="NamaLengkap" Text="Name" Width="300"></ext:Column>
		                <ext:CommandColumn ID="cc_Import_Party_Customer_Individual" runat="server" Text="Action">
	                        <Commands>
	                            <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
	                                <ToolTip Text="Select"></ToolTip>
	                            </ext:GridCommand>
	                        </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_Import_Party_Customer">
                                    <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="200"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="PK_Customer_ID" Value="record.data.PK_Customer_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="CIF" Value="record.data.CIF" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_Import_Party_Customer_Individual" runat="server" HideRefresh="false" >
                        <Items>  
           <%-- Comment Item--%>
                            <ext:Button runat="server" Text="Close Picker" ID="Button7">
                                <Listeners>
                                    <Click Handler="#{window_Import_Party}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>
           <%-- Comment Item--%>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>

            <%-- Grid Panel Customer Corporate --%>
            <ext:GridPanel ID="gp_Import_Party_Customer_Corporate" runat="server" ClientIDMode="Static" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="false" />
                </View>
                <Store>
                    <ext:Store ID="store_Import_Party_Customer_Corporate" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" 
                        OnReadData="store_ReadData_Import_Party_Customer_Corporate" RemotePaging="true" ClientIDMode="Static" PageSize="10">
		                <Model>
		                    <ext:Model runat="server" ID="model_Import_Party_Customer_Corporate" IDProperty="PK_Customer_ID">
		                        <Fields>
		                            <ext:ModelField Name="PK_Customer_ID" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="FK_Customer_Type_ID" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="Customer_Type" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="NIK" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="TanggalLahir" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="NamaLengkap" Type="String"></ext:ModelField>
		                        </Fields>
		                    </ext:Model>
		                </Model>
		                <Sorters></Sorters>
		                <Proxy>
		                    <ext:PageProxy />
		                </Proxy>
	                </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="filterHeader_Import_Party_Customer_Corporate" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
		                <ext:RowNumbererColumn ID="RowNumber_Import_Party_Customer_Corporate" runat="server" Text="No"></ext:RowNumbererColumn>
		                <ext:Column ID="col_Import_Party_Customer_Corporate_PK" runat="server" DataIndex="PK_Customer_ID" Text="PK_Customer_ID" Flex="1" Hidden="true"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Corporate_CIF" runat="server" DataIndex="CIF" Text="CIF" Flex="1"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Corporate_Type" runat="server" DataIndex="Customer_Type" Text="Customer Type" Flex="1"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Corporate_NIK" runat="server" DataIndex="NIK" Text="NIK" Width="150"></ext:Column>
		                <ext:DateColumn ID="col_Import_Party_Customer_Corporate_TanggalLahir" runat="server" DataIndex="TanggalLahir" Text="Tanggal Lahir" Flex="1" Format="dd-MMM-Y"></ext:DateColumn>
		                <ext:Column ID="col_Import_Party_Customer_Corporate_Name" runat="server" DataIndex="NamaLengkap" Text="Name" Width="300"></ext:Column>
		                <ext:CommandColumn ID="cc_Import_Party_Customer_Corporate" runat="server" Text="Action">
	                        <Commands>
	                            <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
	                                <ToolTip Text="Select"></ToolTip>
	                            </ext:GridCommand>
	                        </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_Import_Party_Customer">
                                    <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="200"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="PK_Customer_ID" Value="record.data.PK_Customer_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="CIF" Value="record.data.CIF" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_Import_Party_Customer_Corporate" runat="server" HideRefresh="false" >
                        <Items>  
           <%-- Comment Item--%>
                           <ext:Button runat="server" Text="Close Picker" ID="Button_Import_Party_Customer_Corporate">
                                <Listeners>
                                    <Click Handler="#{window_Import_Party}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>
           <%-- Comment Item--%>
                     </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>

        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.9});" />
            <Resize Handler="#{window_Import_Party}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_Import_Party_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Import_Party_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%-- ================== END OF IMPORT PARTY WINDOWS ========================== --%>

    <%-- ================== CONFIRMATION PANEL ========================== --%>
    <ext:FormPanel ID="pnl_Confirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="lbl_Confirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Confirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <%-- ================== END OF CONFIRMATION PANEL ========================== --%>

      <%-- ================== PERSON PARTY ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window1" Title="Person Party" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
            <ext:Button ID="Button1" runat="server" Icon="Zoom" Text="Search Data Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_Customer_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Person_Import_WIC" runat="server" Icon="Zoom" Text="Search Data Non Customer" MarginSpec="0 10 10 0" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btn_Import_WIC_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

            <%-- Person Information --%>
            <ext:Panel runat="server" ID="Panel1" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                  
                    <%-- Person Address --%>
                    <ext:GridPanel ID="GridPanel1" runat="server" AutoScroll="true" Title="ADDRESS" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="Button2" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Address_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store1" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model1" IDProperty="PK_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AddressType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column1" runat="server" DataIndex="AddressType" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="ADDRESS" Text="ADDRESS" Flex="1"></ext:Column>
                                <ext:Column ID="Column3" runat="server" DataIndex="CITY" Text="CITY" Flex="1"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Address_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Person Phone --%>
                    <ext:GridPanel ID="GridPanel2" runat="server" AutoScroll="true" Title="Phone" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="Button3" Text="Add Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Phone_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store2" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model2" IDProperty="PK_Phone_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Phone_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ContactType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TPH_COUNTRY_PREFIX" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TPH_NUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column12" runat="server" DataIndex="ContactType" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="TPH_COUNTRY_PREFIX" Text="Country Prefix" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="TPH_NUMBER" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Phone_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Phone_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

               

                    <%-- Person Identification --%>
                    <ext:GridPanel ID="GridPanel3" runat="server" AutoScroll="true" Title="Identification" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="Button4" Text="Add Identification" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Identification_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store3" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model3" IDProperty="PK_Identification_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Issue_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Expiry_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column15" runat="server" DataIndex="Type" Text="Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="Number" Text="Number" Flex="1" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="Column110" runat="server" DataIndex="Issue_Date" Text="Issue Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:DateColumn ID="Column111" runat="server" DataIndex="Expiry_Date" Text="Expired Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column17" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Identification_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                <%--    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Person_IsDecease" FieldLabel="Has Passed Away">
                        <DirectEvents>
                            <Change OnEvent="chk_Person_IsDecease_Changed" />
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Person_DeceaseDate" FieldLabel="Date of Passed Away" AnchorHorizontal="60%" Format="dd-MMM-yyyy" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_TaxNumber" FieldLabel="Tax Number" EnforceMaxLength="True" maxlength="100" AnchorHorizontal="70%" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="Checkbox1" FieldLabel="PEP"></ext:Checkbox>
                    <ext:TextField runat="server" LabelWidth="200" ID="TextField8" FieldLabel="Source of Wealth" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_Person_Comment" FieldLabel="Notes" EnforceMaxLength="True" maxlength="4000" AnchorHorizontal="70%" />--%>

                    <%-- For Account Signatory --%>
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Person_AccountSignatory_IsPrimary" FieldLabel="Is Primary"></ext:Checkbox>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_AccountSignatory_Role" Label="Role" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Peran_orang_dalam_rekening" AnchorHorizontal="70%"/>

                    <%-- For Director --%>
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="Button5" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Person_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="Button6" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Person_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.85, height: size.height * 0.8});" />
            <Resize Handler="#{window_PersonParty}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF PERSON PARTY ENTRY WINDOWS ========================== --%>


    <%-- ================== ENTITY PARTY ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_EntityParty" Title="Entity Party" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
            <ext:Button ID="btn_Entity_Import_Customer" runat="server" Icon="Zoom" Text="Search Data Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_Customer_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Entity_Import_WIC" runat="server" Icon="Zoom" Text="Search Data Non Customer" MarginSpec="0 10 10 0" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btn_Import_WIC_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

            <%-- Entity Information --%>
            <ext:Panel runat="server" ID="pnl_EntityParty" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                        <%-- Relationship --%>
            <ext:Panel runat="server" ID="pnl_Relationship_Entity" Title="Relationship" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="10">
                <Items>
          <%--          <ext:TextField ID="TextField9" runat="server"  LabelWidth="190" FieldLabel="Client Number" AnchorHorizontal="70%" MaxLength="255" EnforceMaxLength="true" />--%>
                    <ext:TextField ID="txt_ClientNumber_Relationship_Entity" runat="server"  LabelWidth="190" FieldLabel="Client Number" AnchorHorizontal="70%" MaxLength="255" EnforceMaxLength="true"  />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_IsApproxFrom_Relationship_Entity" FieldLabel="Is Approx From"></ext:Checkbox>
                    <ext:DateField runat="server"  LabelWidth="190" ID="txt_Validfrom_Relationship_Entity" FieldLabel="Valid From" AnchorHorizontal="60%" Format="dd-MMM-yyyy" AllowBlank="false" />
                    <%--<ext:DisplayField ID="txt_Rentity_Town" runat="server" FieldLabel="Town" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />--%>
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_IsApproxTo_Relationship_Entity" FieldLabel="Is Approx To"></ext:Checkbox>
                    <ext:DateField runat="server"  LabelWidth="190" ID="txt_Validto_Relationship_Entity" FieldLabel="Valid To" AnchorHorizontal="60%" Format="dd-MMM-yyyy" AllowBlank="false" />
                    <%--<ext:DisplayField ID="txt_Rentity_Zip" runat="server" FieldLabel="Zip Code" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="10" />--%>
                    <ext:TextArea runat="server"  LabelWidth="190" ID="txt_Comments_Relationship_Entity" FieldLabel="Comments" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="8000"  />
                                    </Items>
            </ext:Panel>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_CorporateName" FieldLabel="Corporate Name" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_CommercialName" FieldLabel="Commercial Name" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Entity_IncorporationLegalForm" Label="Legal Form" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Bentuk_Badan_Usaha" AnchorHorizontal="70%"   />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_IncorporationNumber" FieldLabel="Corporate License Number" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_Business" FieldLabel="Line of Business" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />

                    <%-- Entity Address --%>
                    <ext:GridPanel ID="gp_Entity_Address" runat="server" AutoScroll="true" Title="ADDRESS" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Entity_Address_Add" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Address_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Entity_Address" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model25" IDProperty="PK_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Address_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="AddressType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column32" runat="server" DataIndex="AddressType" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column33" runat="server" DataIndex="ADDRESS" Text="ADDRESS" Flex="1"></ext:Column>
                                <ext:Column ID="Column34" runat="server" DataIndex="CITY" Text="CITY" Flex="1"></ext:Column>
                                <ext:Column ID="Column35" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Entity_Address" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Address_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Entity Phone --%>
                    <ext:GridPanel ID="gp_Entity_Phone" runat="server" AutoScroll="true" Title="Phone" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Entity_Phone_Add" Text="Add Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Phone_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Entity_Phone" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model24" IDProperty="PK_Phone_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Phone_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="ContactType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TPH_COUNTRY_PREFIX" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TPH_NUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column36" runat="server" DataIndex="ContactType" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column37" runat="server" DataIndex="TPH_COUNTRY_PREFIX" Text="Country Prefix" Flex="1"></ext:Column>
                                <ext:Column ID="Column38" runat="server" DataIndex="TPH_NUMBER" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Entity_Phone" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Phone_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Phone_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_Email" FieldLabel="Corporate Email" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_URL" FieldLabel="Corporate Website" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_IncorporationState" FieldLabel="Province" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255"  />
                    <NDS:NDSDropDownField LabelWidth="200" ID="cmb_Entity_IncorporationCountryCode" Label="Country" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"  />

                    <%-- Entity Director --%>
                    <ext:GridPanel ID="gp_Entity_Director" runat="server" AutoScroll="true" Title="Director" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Entity_Director_Add" Text="Add Director" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Person_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'EntityDirector'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Entity_Director" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model22" IDProperty="PK_Entity_Director_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Entity_Director_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="LAST_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleName" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn43" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column154" runat="server" DataIndex="LAST_NAME" Text="Name" Flex="1"></ext:Column>
                                <ext:Column ID="Column155" runat="server" DataIndex="RoleName" Text="Role" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Entity_Director" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Person_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'EntityDirector'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Entity_IncorporationDate" FieldLabel="Date of Established" AnchorHorizontal="40%" Format="dd-MMM-yyyy" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Entity_IsClosed" FieldLabel="Is Closed">
                        <DirectEvents>
                            <Change OnEvent="chk_Entity_IsClosed_Changed"></Change>
                        </DirectEvents>
                    </ext:Checkbox>

                    <ext:DateField runat="server" Hidden="true" LabelWidth="200" ID="txt_Entity_ClosingDate" FieldLabel="Closing Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_TaxNumber" FieldLabel="Tax Number" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="100"  />
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_Entity_Comment" FieldLabel="Notes" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"  />
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
          <ext:Button ID="btn_Entity_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Entity_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
           <ext:Button ID="btn_Entity_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Entity_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.95});" />
            <Resize Handler="#{window_EntityParty}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF ENTITY PARTY ENTRY WINDOWS ========================== --%>

</asp:Content>

