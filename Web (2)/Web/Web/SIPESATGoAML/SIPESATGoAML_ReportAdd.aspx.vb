﻿Imports Elmah
Imports System.Data
Imports SIPESATGoAMLBLL
Imports System.Data.SqlClient


Partial Class SIPESATGoAML_ReportAdd
    Inherits ParentPage

    Dim drTemp As DataRow
    Dim strSQL As String

#Region "Session"
    Public Property IDModule() As String
        Get
            Return Session("SIPESATGoAML_ReportAdd.IDModule")
        End Get
        Set(ByVal value As String)
            Session("SIPESATGoAML_ReportAdd.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("SIPESATGoAML_ReportAdd.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("SIPESATGoAML_ReportAdd.IDUnik") = value
        End Set
    End Property

    Public Property objGoAML_Report_Class() As SIPESATGoAMLBLL.SIPESATGoAML_Report_Class
        Get
            Return Session("SIPESATGoAML_ReportAdd.objGoAML_Report_Class")
        End Get
        Set(ByVal value As SIPESATGoAMLBLL.SIPESATGoAML_Report_Class)
            Session("SIPESATGoAML_ReportAdd.objGoAML_Report_Class") = value
        End Set
    End Property

    Public Property objGoAML_Activity_Class() As SIPESATGoAMLBLL.SIPESATGoAML_Activity_Class
        Get
            Return Session("SIPESATGoAML_ReportAdd.objGoAML_Activity_Class")
        End Get
        Set(ByVal value As SIPESATGoAMLBLL.SIPESATGoAML_Activity_Class)
            Session("SIPESATGoAML_ReportAdd.objGoAML_Activity_Class") = value
        End Set
    End Property

    'Public Property obj_goAML_Report_Indicator() As SIPESATGOAMLBLL.SIPESATGOAML_Report_Indicator
    '    Get
    '        Return Session("SIPESATGoAML_ReportEdit.obj_goAML_Report_Indicator")
    '    End Get
    '    Set(ByVal value As SIPESATGOAMLBLL.SIPESATGOAML_Report_Indicator)
    '        Session("SIPESATGoAML_ReportEdit.obj_goAML_Report_Indicator") = value
    '    End Set
    'End Property

    '-------------------------------- Session untuk menampung variable2 yang dibutuhkan untuk menggunakan reusable windows objects
    Private Enum enumPartySide
        _From = 1
        _To = 2
        _Conductor = 3
        _TParty = 4
        _Activity = 5
    End Enum

    Private Enum enumPartyType
        _Account = 1
        _Person = 2
        _Entity = 3
    End Enum

    Public Property PartySide() As Integer       '1=From/2=To/3=Conductor/4=TParty/5=Activity
        Get
            Return Session("SIPESATGoAML_ReportAdd.PartySide")
        End Get
        Set(ByVal value As Integer)
            Session("SIPESATGoAML_ReportAdd.PartySide") = value
        End Set
    End Property

    Public Property PartyType() As Integer       '1=Account/2=Person/3=Entity
        Get
            Return Session("SIPESATGoAML_ReportAdd.PartyType")
        End Get
        Set(ByVal value As Integer)
            Session("SIPESATGoAML_ReportAdd.PartyType") = value
        End Set
    End Property

    Public Property PartySubType() As Integer       'As of enum IdentificationPersonType in GoAML_Report_BLL_
        Get
            Return Session("SIPESATGoAML_ReportAdd.PartySubType")
        End Get
        Set(ByVal value As Integer)
            Session("SIPESATGoAML_ReportAdd.PartySubType") = value
        End Set
    End Property

    Public Property IsMyClient() As Boolean
        Get
            Return Session("SIPESATGoAML_ReportAdd.IsMyClient")
        End Get
        Set(ByVal value As Boolean)
            Session("SIPESATGoAML_ReportAdd.IsMyClient") = value
        End Set
    End Property

    Public Property IsEmployer() As Boolean
        Get
            Return Session("SIPESATGoAML_ReportAdd.IsEmployer")
        End Get
        Set(ByVal value As Boolean)
            Session("SIPESATGoAML_ReportAdd.IsEmployer") = value
        End Set
    End Property

    Public Property PK_Activity_ID() As String
        Get
            Return Session("SIPESATGoAML_ReportAdd.PK_Activity_ID")
        End Get
        Set(ByVal value As String)
            Session("SIPESATGoAML_ReportAdd.PK_Activity_ID") = value
        End Set
    End Property

    Public Property PK_Party_ID() As String
        Get
            Return Session("SIPESATGoAML_ReportAdd.PK_Party_ID")
        End Get
        Set(ByVal value As String)
            Session("SIPESATGoAML_ReportAdd.PK_Party_ID") = value
        End Set
    End Property

    Public Property PK_PartySub_ID() As String
        Get
            Return Session("SIPESATGoAML_ReportAdd.PK_PartySub_ID")
        End Get
        Set(ByVal value As String)
            Session("SIPESATGoAML_ReportAdd.PK_PartySub_ID") = value
        End Set
    End Property

    Public Property PK_Address_ID() As String
        Get
            Return Session("SIPESATGoAML_ReportAdd.PK_Address_ID")
        End Get
        Set(ByVal value As String)
            Session("SIPESATGoAML_ReportAdd.PK_Address_ID") = value
        End Set
    End Property

    Public Property PK_Phone_ID() As String
        Get
            Return Session("SIPESATGoAML_ReportAdd.PK_Phone_ID")
        End Get
        Set(ByVal value As String)
            Session("SIPESATGoAML_ReportAdd.PK_Phone_ID") = value
        End Set
    End Property

    Public Property PK_Identification_ID() As String
        Get
            Return Session("SIPESATGoAML_ReportAdd.PK_Identification_ID")
        End Get
        Set(ByVal value As String)
            Session("SIPESATGoAML_ReportAdd.PK_Identification_ID") = value
        End Set
    End Property

    'Ada bugs gak tau causenya tapi kalau saat load gridpanel dibind new datatable kemudian di load misal Address padahal datanya ada tapi gridpanelnya kosong
    Public Property IsNewRecord() As Boolean
        Get
            Return Session("SIPESATGoAML_ReportAdd.IsNewRecord")
        End Get
        Set(ByVal value As Boolean)
            Session("SIPESATGoAML_ReportAdd.IsNewRecord") = value
        End Set
    End Property

    'Bedakan PK_Party_ID untuk AccountEntity dan Account walaupun levelnya sejajar
    Public Property PK_Party_AccountEntity_ID() As String
        Get
            Return Session("SIPESATGoAML_ReportAdd.PK_Party_AccountEntity_ID")
        End Get
        Set(ByVal value As String)
            Session("SIPESATGoAML_ReportAdd.PK_Party_AccountEntity_ID") = value
        End Set
    End Property

    'Public Property PK_Indicator_ID() As String
    '    Get
    '        Return Session("SIPESATGoAML_ReportAdd.PK_Indicator_ID")
    '    End Get
    '    Set(ByVal value As String)
    '        Session("SIPESATGoAML_ReportAdd.PK_Indicator_ID") = value
    '    End Set
    'End Property

    'Session untuk menampung fitur Import from goAML Ref Account/WIC
    Public Property SearchType() As String
        Get
            Return Session("SIPESATGoAML_ReportAdd.SearchType")
        End Get
        Set(ByVal value As String)
            Session("SIPESATGoAML_ReportAdd.SearchType") = value
        End Set
    End Property

    'Session untuk menampung ReadOnly Level
    Public Property ReadOnlyLevel() As Integer
        Get
            Return Session("SIPESATGoAML_ReportAdd.ReadOnlyLevel")
        End Get
        Set(ByVal value As Integer)
            Session("SIPESATGoAML_ReportAdd.ReadOnlyLevel") = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As New Integer
            If IDModule IsNot Nothing Then
                intModuleID = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            If Not Ext.Net.X.IsAjaxRequest Then
                Me.PartyType = enumPartyType._Account
                FormPanelReport.Title = ObjModule.ModuleLabel & " - Add"

                'Set Gridpanel command column location
                SetCommandColumnLocation()

                'Load Report
                'LoadReport()

                'Load Reporting Office Location
                LoadReportingOfficeLocation()

                'Load Approval History
                'LoadApprovalHistory()

                'Set Mandatory Fields
                SetMandatoryFieldReport()
                'SetMandatoryFieldActivity()
                SetMandatoryFieldAddressPhoneIdentification()

                'Set Max Date for all DateFields
                SetMaxDate()

                'Set Initial Report Data
                SetInitialReportData()

                '10-Mar-2022 Adi : Filter Report code jika Parameter reporttype exists
                'Dim ReportType As String = Request.Params("ReportType")
                'If Not String.IsNullOrEmpty(ReportType) Then
                '    cmb_JenisLaporan.StringFilter = "ReportType LIKE '%" & ReportType & "%'"
                'End If
                ' 19 Jan 2023 Ari : di unhide untuk button activity information
                cmb_Activity_Information.SetTextValue("")
                df_MonthPeriod.SetTextValue("")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

#Region "GoAML Report"
    'Set Title & Status
    Protected Sub SetReportStatusAndValidationResult()
        Try
            '11-Mar-2022 Adi : Update Report Valid/Invalid
            UpdateReportIsValid()
            'End of 11-Mar-2022 Adi : Update Report Valid/Invalid

            With objGoAML_Report_Class.obj_goAML_Report
                If Not IsNothing(.STATUS) Then
                    drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("SIPESATGoAML_Status_Report", "PK_SIPESATGoAML_Status_Report_ID", .STATUS)
                    If drTemp IsNot Nothing Then
                        'FormPanelReport.Title = ObjModule.ModuleLabel & " - Edit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ Status : " & .Status & " - " & drTemp("Description") & " ]"
                        txt_ReportStatus.Value = .STATUS & " - " & drTemp("Description")
                        FormPanelReport.Title = ObjModule.ModuleLabel & " - Add &nbsp;&nbsp;&nbsp;[ " & UCase(drTemp("Description")) & " ]"
                    End If
                End If

                Dim strValidationResult As String = ""
                strValidationResult = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.getValidationReportByReportID(.PK_REPORT_ID)
                If Not String.IsNullOrEmpty(strValidationResult) Then
                    SIPESATGoAMLBLL.NawaFramework.extInfoPanelupdate(FormPanelReport, strValidationResult, "info_ValidationResult")
                    FormPanelReport.Body.ScrollTo(Direction.Top, 0)
                    info_ValidationResult.Hidden = False
                Else
                    info_ValidationResult.Hidden = True
                End If
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Load Report 
    Protected Sub LoadReport()
        Try
            'Load Report Class
            objGoAML_Report_Class = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.GetGoAMLReportClassByID(IDUnik)

            If objGoAML_Report_Class.obj_goAML_Report IsNot Nothing Then
                With objGoAML_Report_Class.obj_goAML_Report
                    'Load General Information
                    txt_FK_Report_ID.Value = .PK_REPORT_ID

                    If Not IsNothing(.SUBMISSION_CODE) Then
                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Submission_Type", "Kode", .SUBMISSION_CODE)
                        If drTemp IsNot Nothing Then
                            cmb_SubmisionCode.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                        End If
                    End If

                    If Not IsNothing(.REPORT_CODE) Then
                        'drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("goAML_Ref_Jenis_Laporan", "Kode", .REPORT_CODE)
                        'If drTemp IsNot Nothing Then
                        '    cmb_JenisLaporan.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                        'End If
                        cmb_JenisLaporan.Text = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID(3)


                        'Filter Unique Type based on Report Code
                        'cmb_ReportType.StringFilter = "jenisLaporan = '" & .Report_Code & "'"

                        'Hide/Show panel Transaction/Activity
                        pnl_Activity.Hidden = False
                        BindActivity()

                    End If

                    txt_ReportUniqueValue.Value = .UNIKREFERENCE
                    txt_RentityID.Text = .RENTITY_ID
                    txt_Entity_Reference.Value = .ENTITY_REFERENCE
                    txt_SubmissionDate.Value = .SUBMISSION_DATE
                    txt_CurrencyLocal.Value = .CURRENCY_CODE_LOCAL
                End With
            End If
            If objGoAML_Report_Class.list_goAML_Act_ReportPartyType.Count >= 1 Then
                btn_Activity_Add.Hidden = True
            Else
                Session("ActivityIsDraft_SipesatGoAML") = 1
                btn_Activity_Add.Hidden = False
            End If
            '15-Mar-2022 Adi : Pindah sini karena pengaruh ke flag Valid/Invalid
            SetReportStatusAndValidationResult()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadReportingOfficeLocation()
        'TAG Harus Ganti ISI
        'Dim drLocation As DataRow = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("goAML_Ref_Address", "FK_Ref_Detail_Of", 9)
        'If drLocation IsNot Nothing Then
        Dim tempStringGlobalParam As String = ""
        'Global Param (5) = Tipe Alamat = K
        tempStringGlobalParam = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID("5")
        'If Not IsDBNull(drLocation("Address_Type")) Then
        If Not String.IsNullOrEmpty(tempStringGlobalParam) Then
            'drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("goAML_Ref_Kategori_Kontak", "Kode", drLocation("Address_Type"))
            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("goAML_Ref_Kategori_Kontak", "Kode", tempStringGlobalParam)
            If drTemp IsNot Nothing Then
                txt_Rentity_AddressType.Text = drTemp("Kode") & " - " & drTemp("Keterangan")
            Else
                txt_Rentity_AddressType.Text = tempStringGlobalParam
            End If
        End If

        tempStringGlobalParam = ""
        'Global Param (6) = Alamat = Sesame Street
        tempStringGlobalParam = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID("6")
        'txt_Rentity_Address.Text = drLocation("Address")
        txt_Rentity_Address.Text = tempStringGlobalParam
        'txt_Rentity_Town.Text = drLocation("Town")
        tempStringGlobalParam = ""
        'Global Param (7) = Kota/Kabupaten = Kota/Kabutan Dummy
        tempStringGlobalParam = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID("7")
        'txt_Rentity_City.Text = drLocation("City")
        txt_Rentity_City.Text = tempStringGlobalParam
        'txt_Rentity_Zip.Text = drLocation("Zip")

        tempStringGlobalParam = ""
        'Global Param (8) = Negara = ID
        tempStringGlobalParam = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID("8")
        'If Not IsDBNull(drLocation("Country_Code")) Then
        If Not String.IsNullOrEmpty(tempStringGlobalParam) Then
            'drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("goAML_Ref_Nama_Negara", "Kode", drLocation("Country_Code"))
            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("goAML_Ref_Nama_Negara", "Kode", tempStringGlobalParam)
            If drTemp IsNot Nothing Then
                txt_Rentity_Country.Text = drTemp("Kode") & " - " & drTemp("Keterangan")
            Else
                txt_Rentity_Country.Text = tempStringGlobalParam
            End If
        End If

        tempStringGlobalParam = ""
        'Global Param (9) = Provinsi = Provinsi Dummy
        tempStringGlobalParam = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID("9")
        'txt_Rentity_State.Text = drLocation("State")
        txt_Rentity_State.Text = tempStringGlobalParam
        'txt_Rentity_Comments.Text = drLocation("Comments")
        'End If
    End Sub

    'Protected Sub cmb_JenisLaporan_Change(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim strJenisLaporan As String = cmb_JenisLaporan.StringValue

    '        If Not String.IsNullOrEmpty(strJenisLaporan) Then
    '            cmb_ReportType.StringFilter = "jenisLaporan = '" & strJenisLaporan & "'"
    '        Else
    '            cmb_ReportType.StringFilter = "1=2"
    '        End If

    '        'Get TRN of ACT based on Report Type
    '        Me.TRN_or_ACT = GoAML_Report_BLL_.getTRNorACT(strJenisLaporan)

    '        'Show the Indicator Panel if report code is mapped
    '        If GoAML_Report_BLL_.isUseIndicator(strJenisLaporan) Then
    '            pnl_Indicator.Hidden = False
    '            BindIndicator()
    '        Else
    '            pnl_Indicator.Hidden = True
    '        End If

    '        'Hide FIU Ref Number if Report Code isn't PPATK Request
    '        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("GoAML_Laporan_Req_PPATK", "Kode", strJenisLaporan)
    '        If drTemp IsNot Nothing Then
    '            txt_FIU_Ref_Number.Hidden = False
    '            txt_FIU_Ref_Number.AllowBlank = False
    '        Else
    '            txt_FIU_Ref_Number.Hidden = True
    '            txt_FIU_Ref_Number.AllowBlank = True
    '        End If

    '        'Hide/Show panel Transaction/Activity
    '        If Me.TRN_or_ACT = "TRN" Then
    '            pnl_Transaction.Hidden = False
    '            pnl_Activity.Hidden = True

    '            objGoAML_Report_Class.list_goAML_Act_ReportPartyType = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Act_ReportPartyType)
    '            BindTransaction()
    '        Else
    '            pnl_Transaction.Hidden = True
    '            pnl_Activity.Hidden = False

    '            objGoAML_Report_Class.list_goAML_Transaction = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Transaction)
    '            BindActivity()
    '        End If

    '        'Show the Indicator Panel if report code is mapped
    '        If GoAML_Report_BLL_.isUseIndicator(strJenisLaporan) Then
    '            pnl_Indicator.Hidden = False
    '            BindIndicator()
    '        Else
    '            pnl_Indicator.Hidden = True
    '        End If

    '        'Filter Transmode Code by Report Code
    '        If Not String.IsNullOrEmpty(strJenisLaporan) Then
    '            cmb_TransmodeCode.StringFilter = "kode_laporan = '" & strJenisLaporan & "'"
    '        Else
    '            cmb_TransmodeCode.StringFilter = "1=2"
    '        End If

    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub LoadApprovalHistory()
        Try
            strSQL = "SELECT * FROM SIPESATGoAML_Note_History"
            strSQL += " WHERE UnikID = " & IDUnik

            Dim dtApprovalHistory As DataTable = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataTableByQuery(strSQL)

            If dtApprovalHistory IsNot Nothing Then
                gp_ApprovalHistory.GetStore.DataSource = dtApprovalHistory
                gp_ApprovalHistory.GetStore.DataBind()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Activity"
    Protected Sub BindActivity()
        Try
            If objGoAML_Report_Class.list_goAML_Act_ReportPartyType IsNot Nothing Then
                Dim dtActivity As DataTable = NawaBLL.Common.CopyGenericToDataTable(objGoAML_Report_Class.list_goAML_Act_ReportPartyType)
                dtActivity.Columns.Add(New DataColumn("PartyType", GetType(String)))

                Using objdb As New NawaDevDAL.NawaDatadevEntities
                    Dim listType = objdb.goAML_Ref_Sender_To_Information.ToList

                    For Each row In dtActivity.Rows
                        If Not IsDBNull(row("SubNodeType")) Then
                            Dim objType = listType.Where(Function(x) x.PK_Sender_To_Information = row("SubNodeType")).FirstOrDefault
                            If objType IsNot Nothing Then
                                row("PartyType") = objType.PK_Sender_To_Information & " - " & objType.Sender_To_Information
                            End If
                        End If
                        'row("PartyType") = "Account"
                    Next
                End Using

                'Bind to gridpanel
                If dtActivity IsNot Nothing Then
                    gp_Activity.GetStore.DataSource = dtActivity
                    gp_Activity.GetStore.DataBind()
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_Activity_Add_Click(sender As Object, e As DirectEventArgs)
        Try
            If objGoAML_Report_Class.list_goAML_Act_ReportPartyType.Count >= 1 Then
                Throw New ApplicationException("Untuk Pelaporan ini setiap Report hanya boleh ada 1 Activity")
            End If
            Me.IsNewRecord = True
            SetReadOnlyWindowActivity(False)

            'Validate Report first, because once user click save transaction, report must be saved also
            ValidateReport()

            'Save Report as Draft (Status 0) to Get PK_Report_ID, so it can be assigned to all report Hierarchy
            'Populate Data
            PopulateReportData()

            With objGoAML_Report_Class.obj_goAML_Report
                .STATUS = 0     'Saved as Draft
            End With

            Dim lngReportID As Long = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.SaveReportAdd(ObjModule, objGoAML_Report_Class)
            objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID = lngReportID

            SetReportStatusAndValidationResult()
            btn_Report_Back_DeleteDraft.Hidden = False

            'Clean window Activity
            CleanWindowActivity()

            'Create new activity Class
            If objGoAML_Report_Class.list_goAML_Act_ReportPartyType Is Nothing Then
                objGoAML_Report_Class.list_goAML_Act_ReportPartyType = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_REPORTPARTYTYPE)
            End If

            Dim lngPK As Long = 0
            If objGoAML_Report_Class.list_goAML_Act_ReportPartyType.Count = 0 Then
                lngPK = -1
            Else
                lngPK = objGoAML_Report_Class.list_goAML_Act_ReportPartyType.Min(Function(x) x.PK_ACT_REPORTPARTYTYPE_ID) - 1
                If lngPK >= 0 Then
                    lngPK = -1
                End If
            End If

            objGoAML_Activity_Class = New SIPESATGoAMLBLL.SIPESATGoAML_Activity_Class
            With objGoAML_Activity_Class.obj_goAML_Act_ReportPartyType
                .PK_ACT_REPORTPARTYTYPE_ID = lngPK
                .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
            End With

            Me.PK_Activity_ID = lngPK

            window_Activity.Title = "Activity - Add"
            window_Activity.Hidden = False
            window_Activity.Body.ScrollTo(Direction.Top, 0)

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Activity_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            window_Activity.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Activity_Save_Click(sender As Object, e As DirectEventArgs)
        Try
            ValidateActivity()

            'Save Report Dulu untuk mengamankan data2 SavedState untuk perubahan status pertama kali ke 9
            'Also populate Report data, because once user click save transaction, report must be saved also (save data per transaction for performance)
            PopulateReportData()
            'Set report status to 9 (In Progress Editing)
            With objGoAML_Report_Class.obj_goAML_Report
                'If Not txt_TransactionDate.SelectedDate = DateTime.MinValue Then
                '    .Transaction_Date = txt_TransactionDate.SelectedDate
                'End If
                'If Me.TRN_or_ACT = "ACT" Then
                '    If txt_SubmissionDate.SelectedDate <> DateTime.MinValue Then
                '        .Transaction_Date = txt_SubmissionDate.SelectedDate
                '    End If
                'End If
                If txt_SubmissionDate.SelectedDate <> DateTime.MinValue Then
                    .SUBMISSION_DATE = txt_SubmissionDate.SelectedDate
                End If
                .STATUS = 0     'Draft
            End With

            Dim lngReportID As Long = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.SaveReportAdd(ObjModule, objGoAML_Report_Class)
            objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID = lngReportID

            Dim objCek = objGoAML_Report_Class.list_goAML_Act_ReportPartyType.Where(Function(x) x.PK_ACT_REPORTPARTYTYPE_ID = Me.PK_Activity_ID).FirstOrDefault

            With objGoAML_Activity_Class.obj_goAML_Act_ReportPartyType
                .FK_Report_ID = lngReportID
                .SUBNODETYPE = cmb_Activity_Information.SelectedItemValue
                .SIGNIFICANCE = CInt(txt_Activity_Significance.Value)
                .REASON = txt_Activity_Reason.Value
                .COMMENTS = txt_Activity_Comment.Value

                If objCek Is Nothing Then
                    .Active = True
                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .CreatedDate = DateTime.Now
                Else
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateDate = DateTime.Now
                End If
            End With

            'Delete unnecessary related data
            'Select Case cmb_Activity_Information.SelectedItemValue
            '    Case enumPartyType._Account
            '        Delete_Activity_Person_RelatedData()
            '        Delete_Activity_Entity_RelatedData()
            '    Case enumPartyType._Person
            '        Delete_Activity_Account_RelatedData()
            '        Delete_Activity_Entity_RelatedData()
            '    Case enumPartyType._Entity
            '        Delete_Activity_Account_RelatedData()
            '        Delete_Activity_Person_RelatedData()
            'End Select



            Dim strValidationResult As String = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.validateActivity(objGoAML_Report_Class, objGoAML_Activity_Class)
            If Not String.IsNullOrWhiteSpace(strValidationResult) Then
                SIPESATGoAMLBLL.NawaFramework.extInfoPanelupdate(FormPanelReport, strValidationResult, "info_ValidationResultActivity")
                window_Activity.Body.ScrollTo(Direction.Top, 0)
                info_ValidationResultActivity.Hidden = False

                Throw New ApplicationException("The activity still have some invalid data.")
                Exit Sub
            Else
                info_ValidationResultActivity.Hidden = True
            End If

            'Save Activity
            SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.SaveActivity(ObjModule, objGoAML_Activity_Class, objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID)

            'ReLoad Object Report Class agar ID Activity update
            objGoAML_Report_Class = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.GetGoAMLReportClassByID(objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID)

            'Bind to Gridpanel
            BindActivity()

            'objGoAML_Report_Class = SIPESATGoAML_Report_BLL.GetGoAMLReportClassByID(IDUnik)

            'If objGoAML_Report_Class.obj_goAML_Report IsNot Nothing Then
            '    txt_ReportUniqueValue.Text = objGoAML_Report_Class.obj_goAML_Report.UNIKREFERENCE
            'End If
            ' 27 Dec 2022 Ari : Report Uniqe Value dari goaml report 
            'txt_ReportUniqueValue.Text = objGoAML_Activity_Class.list.FirstOrDefault.CLIENT_NUMBER

            'Hapus saja Validation Report untuk ID bersangkutan jika sudah tidak ada invalid data
            Dim strQuery As String = "DELETE a FROM SIPESATGOAML_ValidationReport a JOIN Module m on a.ModuleID = m.PK_Module_ID AND m.ModuleName='SIPESATGOAML_ACT_REPORTPARTYTYPE' WHERE a.RecordID=" & objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID & " AND a.KeyFieldValue=" & objGoAML_Activity_Class.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Set Report Status
            SetReportStatusAndValidationResult()

            If cmb_Activity_Information.SelectedItemValue = 2 Then
                txt_ReportUniqueValue.Value = TxtClientNumber_Relationship.Value
            Else
                txt_ReportUniqueValue.Value = txt_ClientNumber_Relationship_Entity.Value
            End If

            If objGoAML_Report_Class.list_goAML_Act_ReportPartyType.Count >= 1 Then
                btn_Activity_Add.Hidden = True
            Else
                btn_Activity_Add.Hidden = False
            End If
            'Hide Activity Window
            window_Activity.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_Activity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value

            Me.PK_Activity_ID = ID
            If Me.ReadOnlyLevel = 0 Then
                Me.ReadOnlyLevel = 1
            End If

            If strCommandName = "Delete" Then

                'Delete data dari Database
                SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.DeleteActivityByActivityID(ID)
                objGoAML_Report_Class.list_goAML_Act_ReportPartyType.Remove(objGoAML_Report_Class.list_goAML_Act_ReportPartyType.Where(Function(x) x.PK_ACT_REPORTPARTYTYPE_ID = ID).FirstOrDefault)

                'Hapus saja Validation Report untuk Activity ID bersangkutan jika sudah tidak ada invalid data
                Dim strQuery As String = "DELETE a FROM ValidationReport a JOIN Module m on a.ModuleID = m.PK_Module_ID AND m.ModuleName='goAML_Act_ReportPartyType' WHERE a.RecordID=" & objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID & " AND a.KeyFieldValue=" & ID
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                BindActivity()
                If objGoAML_Report_Class.list_goAML_Act_ReportPartyType.Count >= 1 Then
                    btn_Activity_Add.Hidden = True
                Else
                    btn_Activity_Add.Hidden = False
                End If
                SetReportStatusAndValidationResult()
            ElseIf strCommandName = "Edit" Then
                SetReadOnlyWindowActivity(False)
                LoadActivity("Edit")
            ElseIf strCommandName = "Detail" Then
                SetReadOnlyWindowActivity(True)
                LoadActivity("Detail")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadActivity(strCommandName As String)
        Try
            'Change window_s title
            window_Activity.Title = "Activity - " & strCommandName

            'Clean window Activity
            CleanWindowActivity()

            'Load Transaction Class by Trx ID
            objGoAML_Activity_Class = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.GetGoAMLActivityClassByID(Me.PK_Activity_ID)

            If objGoAML_Activity_Class.obj_goAML_Act_ReportPartyType IsNot Nothing Then
                With objGoAML_Activity_Class.obj_goAML_Act_ReportPartyType
                    txt_Activity_Significance.Value = .SIGNIFICANCE
                    txt_Activity_Reason.Value = .REASON
                    txt_Activity_Comment.Value = .COMMENTS
                    ' 19 Jan 2023 Ari : Unhide subnodetype
                    If Not IsNothing(.SUBNODETYPE) Then
                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("GoAML_Ref_Sender_To_Information", "PK_Sender_To_Information", .SUBNODETYPE)
                        If drTemp IsNot Nothing Then
                            cmb_Activity_Information.SetTextWithTextValue(drTemp("PK_Sender_To_Information"), drTemp("Sender_To_Information"))
                        End If
                    End If
                End With
            End If

            'Load Activity Short Information
            LoadActivityShortInfo()

            'Load Validation Result
            Dim strValidationResult As String = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.getValidationReportByTransactionOrActivityID(objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID, objGoAML_Activity_Class.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID)
            If Not String.IsNullOrWhiteSpace(strValidationResult) Then
                SIPESATGoAMLBLL.NawaFramework.extInfoPanelupdate(FormPanelReport, strValidationResult, "info_ValidationResultActivity")
                info_ValidationResultActivity.Hidden = False
            Else
                info_ValidationResultActivity.Hidden = True
            End If

            'Show window Activity
            window_Activity.Hidden = False
            window_Activity.Body.ScrollTo(Direction.Top, 0)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadActivityShortInfo()
        Try
            With objGoAML_Activity_Class.obj_goAML_Act_ReportPartyType
                ' 27 Dec 2022 Ari : Hide dan replace sama dengan goaml report
                'Dim objAccount = objGoAML_Activity_Class.list_goAML_Act_Account.Where(Function(x) x.FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID).FirstOrDefault
                'If objAccount IsNot Nothing Then
                '    drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Customer", "CIF", objAccount.ACCOUNT)
                '    If drTemp IsNot Nothing Then
                '        If Not IsDBNull(drTemp("CIF")) Then
                '            txt_Activity_CIFNO_ACCOUNTNO.Value = drTemp("CIF")
                '            txt_Activity_Name.Value = drTemp("CIF")
                '        Else
                '            txt_Activity_CIFNO_ACCOUNTNO.Value = ""
                '            txt_Activity_Name.Value = Nothing
                '        End If
                '        If Not IsDBNull(drTemp("NamaLengkap")) Then
                '            df_Activity_Name.Value = drTemp("NamaLengkap")
                '        Else
                '            df_Activity_Name.Value = ""
                '        End If
                '        df_Activity_Name.FieldLabel = "Name"

                '        'Get Top 1 Address
                '        'If Not IsDBNull(drTemp("CIF")) Then
                '        '    Dim drCustomer As DataRow = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("goAML_Ref_Customer", "CIF", drTemp("CIF"))
                '        '    If drCustomer IsNot Nothing Then
                '        '        Dim drAdrress As DataRow = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.PullDataAsDataRowSingleString("SELECT TOP 1 * FROM goAML_Ref_Address WHERE FK_Ref_Detail_Of=1 AND FK_To_Table_ID=" & drCustomer("PK_Customer_ID"))
                '        '        If drAdrress IsNot Nothing Then
                '        '            txt_Activity_Address.Value = drAdrress("Address")
                '        '        End If
                '        '    End If
                '        'End If
                '    Else
                '        txt_Activity_CIFNO_ACCOUNTNO.Value = objAccount.ACCOUNT
                '        'If Not String.IsNullOrEmpty(objAccount.CLIENT_NUMBER) Then
                '        '    txt_Activity_CIFNO_ACCOUNTNO.Value = objAccount.ACCOUNT & " | " & objAccount.CLIENT_NUMBER
                '        'End If
                '        txt_Activity_Name.Value = objAccount.ACCOUNT
                '        df_Activity_Name.Value = objAccount.INSTITUTION_NAME
                '        df_Activity_Name.FieldLabel = "Institution Name"
                '    End If
                'End If
                If cmb_Activity_Information.SelectedItemValue = 2 Then     'Person
                    Dim objPerson = objGoAML_Activity_Class.list_goAML_Act_Person.Where(Function(x) x.FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID).FirstOrDefault
                    If objPerson IsNot Nothing Then
                        If objGoAML_Activity_Class.list_goAML_Act_Person_Relationship IsNot Nothing Then
                            For Each itemRelationship In objGoAML_Activity_Class.list_goAML_Act_Person_Relationship
                                txt_Activity_CIFNO_ACCOUNTNO.Value = itemRelationship.CLIENT_NUMBER
                            Next
                        End If

                        txt_Activity_Name.Value = objPerson.LAST_NAME
                        df_Activity_Name.Value = objPerson.LAST_NAME

                        'Get Top 1 Address
                        Dim objPersonAddress = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Where(Function(x) x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID).FirstOrDefault
                        'If objPersonAddress IsNot Nothing Then
                        '    txt_Activity_Address.Value = objPersonAddress.Address
                        'End If
                    End If
                ElseIf cmb_Activity_Information.SelectedItemValue = 3 Then    'Entity
                    Dim objEntity = objGoAML_Activity_Class.list_goAML_Act_Entity.Where(Function(x) x.FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID).FirstOrDefault
                    If objEntity IsNot Nothing Then
                        'Bind Relationship
                        Dim listEntityrelationship = objGoAML_Activity_Class.list_goAML_Act_Entity_Relationship.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
                        If listEntityrelationship Is Nothing Then
                            listEntityrelationship = New List(Of SIPESATGOAML_ACT_ENTITY_RELATIONSHIP)
                        End If

                        Dim dtEntityrelationship As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEntityrelationship)

                        If dtEntityrelationship IsNot Nothing Then
                            For Each itemEntityRelationship As DataRow In dtEntityrelationship.Rows
                                If Not IsDBNull(itemEntityRelationship("CLIENT_NUMBER")) Then
                                    txt_Activity_CIFNO_ACCOUNTNO.Value = itemEntityRelationship("CLIENT_NUMBER")
                                End If

                            Next

                        End If

                        txt_Activity_Name.Value = objEntity.NAME
                        df_Activity_Name.Value = objEntity.NAME

                        'Get Top 1 Address
                        Dim objEntityAddress = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).FirstOrDefault
                        'If objEntityAddress IsNot Nothing Then
                        '    txt_Activity_Address.Value = objEntityAddress.Address
                        'End If
                    End If
                End If
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub cmb_Activity_Information_Changed()
    '    Try
    '        Me.PartyType = cmb_Activity_Information.SelectedItemValue
    '        LoadActivityShortInfo()
    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub LoadIndicator(strCommandName As String)
    '    Try
    '        'Change window_s title
    '        Window_Indicator.Title = "Report Indicator - " & strCommandName

    '        'Load Indicator
    '        obj_goAML_Report_Indicator = GoAML_Report_BLL_.GetGoAMLReportIndicatorByID(Me.PK_Indicator_ID)
    '        If obj_goAML_Report_Indicator IsNot Nothing Then
    '            With obj_goAML_Report_Indicator
    '                If Not IsNothing(.FK_Indicator) Then
    '                    drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goaml_ref_indicator_laporan", "kode", .FK_Indicator)
    '                    If drTemp IsNot Nothing Then
    '                        cmb_Indicator.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                    End If
    '                End If
    '            End With
    '        End If

    '        'Show window Indicator
    '        Window_Indicator.Hidden = False
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub
#End Region

    '#Region "Indicator"
    '    Protected Sub BindIndicator()
    '        Try
    '            If objGoAML_Report_Class.list_goAML_Report_Indicator Is Nothing Then
    '                objGoAML_Report_Class.list_goAML_Report_Indicator = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Report_Indicator)
    '            End If

    '            Dim dtIndicator As DataTable = NawaBLL.Common.CopyGenericToDataTable(objGoAML_Report_Class.list_goAML_Report_Indicator)
    '            dtIndicator.Columns.Add(New DataColumn("Keterangan", GetType(String)))

    '            Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                Dim listIndikator = objdb.goAML_Ref_Indikator_Laporan.ToList

    '                For Each row In dtIndicator.Rows
    '                    If Not IsDBNull(row("FK_Indicator")) Then
    '                        Dim objIndicator = listIndikator.Where(Function(x) x.Kode = row("FK_Indicator")).FirstOrDefault
    '                        If objIndicator IsNot Nothing Then
    '                            row("Keterangan") = objIndicator.Kode & " - " & objIndicator.Keterangan
    '                        End If
    '                    End If
    '                Next
    '            End Using

    '            'Bind to gridpanel
    '            gp_Indicator.GetStore.DataSource = dtIndicator
    '            gp_Indicator.GetStore.DataBind()
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub

    '    Protected Sub btn_Indicator_Add_Click(sender As Object, e As DirectEventArgs)
    '        Try
    '            Me.IsNewRecord = True
    '            SetReadOnlyWindowIndicator(False)

    '            'Validate Report first, because once user click save transaction, report must be saved also
    '            ValidateReport()

    '            'Save Report as Draft (Status 0) to Get PK_Report_ID, so it can be assigned to all report Hierarchy
    '            'Populate Data
    '            PopulateReportData()

    '            With objGoAML_Report_Class.obj_goAML_Report
    '                .Status = 0     'Saved as Draft
    '            End With

    '            Dim lngReportID As Long = GoAML_Report_BLL_.SaveReportAdd(ObjModule, objGoAML_Report_Class)
    '            objGoAML_Report_Class.obj_goAML_Report.PK_Report_ID = lngReportID

    '            SetReportStatusAndValidationResult()
    '            btn_Report_Back_DeleteDraft.Hidden = False

    '            'Clean window Indicator
    '            cmb_Indicator.SetTextValue("")

    '            'Create new Indicator 
    '            If objGoAML_Report_Class.list_goAML_Report_Indicator Is Nothing Then
    '                objGoAML_Report_Class.list_goAML_Report_Indicator = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Report_Indicator)
    '            End If

    '            Dim lngPK As Long = 0
    '            If objGoAML_Report_Class.list_goAML_Report_Indicator.Count = 0 Then
    '                lngPK = -1
    '            Else
    '                lngPK = objGoAML_Report_Class.list_goAML_Report_Indicator.Min(Function(x) x.PK_Report_Indicator) - 1
    '                If lngPK >= 0 Then
    '                    lngPK = -1
    '                End If
    '            End If

    '            obj_goAML_Report_Indicator = New SIPESATGOAMLBLL.SIPESATGOAML_Report_Indicator
    '            With obj_goAML_Report_Indicator
    '                .PK_Report_Indicator = lngPK
    '                .FK_Report = objGoAML_Report_Class.obj_goAML_Report.PK_Report_ID
    '            End With

    '            Me.PK_Indicator_ID = lngPK

    '            Window_Indicator.Title = "Report Indicator - Add"
    '            Window_Indicator.Hidden = False
    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub gc_Indicator(sender As Object, e As DirectEventArgs)
    '        Try
    '            Dim ID As String = e.ExtraParams(0).Value
    '            Dim strCommandName As String = e.ExtraParams(1).Value

    '            Me.PK_Indicator_ID = ID
    '            If Me.ReadOnlyLevel = 0 Then
    '                Me.ReadOnlyLevel = 1
    '            End If

    '            If strCommandName = "Delete" Then
    '                'To Do Delete langsung dari database
    '                GoAML_Report_BLL_.DeleteIndicatorByIndicatorID(ID)
    '                objGoAML_Report_Class.list_goAML_Report_Indicator.Remove(objGoAML_Report_Class.list_goAML_Report_Indicator.Where(Function(x) x.PK_Report_Indicator = ID).FirstOrDefault)
    '                BindIndicator()
    '            ElseIf strCommandName = "Edit" Then
    '                'ValidateReport()
    '                SetReadOnlyWindowIndicator(False)
    '                LoadIndicator("Edit")
    '            ElseIf strCommandName = "Detail" Then
    '                'ValidateReport()
    '                SetReadOnlyWindowIndicator(True)
    '                LoadIndicator("Detail")
    '            End If
    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub btn_Indicator_Save_Click(sender As Object, e As DirectEventArgs)
    '        Try
    '            'Validate
    '            If String.IsNullOrEmpty(cmb_Indicator.SelectedItemValue) Then
    '                Throw New ApplicationException("Indicator is required")
    '            End If

    '            Dim objCek = objGoAML_Report_Class.list_goAML_Report_Indicator.Where(Function(x) x.PK_Report_Indicator = Me.PK_Indicator_ID).FirstOrDefault

    '            With obj_goAML_Report_Indicator
    '                .FK_Indicator = cmb_Indicator.SelectedItemValue

    '                If objCek Is Nothing Then
    '                    .Active = True
    '                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                    .CreatedDate = DateTime.Now
    '                Else
    '                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                    .LastUpdateDate = DateTime.Now
    '                End If
    '            End With

    '            'Save Report
    '            'Also populate Report data, because once user click save transaction, report must be saved also (save data per transaction for performance)
    '            PopulateReportData()
    '            'Set report status to 9 (In Progress Editing)
    '            With objGoAML_Report_Class.obj_goAML_Report
    '                If Not txt_TransactionDate.SelectedDate = DateTime.MinValue Then
    '                    .Transaction_Date = txt_TransactionDate.SelectedDate
    '                ElseIf Not txt_SubmissionDate.SelectedDate = DateTime.MinValue Then
    '                    .Transaction_Date = txt_SubmissionDate.SelectedDate
    '                End If
    '                .Status = 0     'Draft
    '            End With
    '            GoAML_Report_BLL_.SaveReportAdd(ObjModule, objGoAML_Report_Class)

    '            'Save Indicator
    '            GoAML_Report_BLL_.SaveIndicator(ObjModule, obj_goAML_Report_Indicator, objGoAML_Report_Class.obj_goAML_Report.PK_Report_ID)

    '            'ReLoad Object Report Class agar ID Indicator update
    '            objGoAML_Report_Class = GoAML_Report_BLL_.GetGoAMLReportClassByID(objGoAML_Report_Class.obj_goAML_Report.PK_Report_ID)

    '            'Bind to Gridpanel
    '            BindIndicator()

    '            'Set Report Status
    '            SetReportStatusAndValidationResult()

    '            'Hide Indicator Window
    '            Window_Indicator.Hidden = True
    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub btn_Indicator_Back_Click(sender As Object, e As DirectEventArgs)
    '        Try
    '            Window_Indicator.Hidden = True
    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub
    '#End Region

#Region "Form"
    Protected Sub btn_Report_Save_Click()
        Try

            'Validasi By Form
            ValidateReport()
            ValidateTransactionActivityIndicatorCount()

            'Populate Data
            PopulateReportData()

            'Jalankan Validasi Level Report & Indicator
            Dim strValidationResult As String = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.validateReportAndIndicator(objGoAML_Report_Class)
            If Not String.IsNullOrWhiteSpace(strValidationResult) Then
                SIPESATGoAMLBLL.NawaFramework.extInfoPanelupdate(FormPanelReport, strValidationResult, "info_ValidationResult")
                FormPanelReport.Body.ScrollTo(Direction.Top, 0)
                objGoAML_Report_Class.obj_goAML_Report.ISVALID = 0
                info_ValidationResult.Hidden = False
                Exit Sub
            Else
                objGoAML_Report_Class.obj_goAML_Report.ISVALID = 1
                info_ValidationResult.Hidden = True
            End If

            With objGoAML_Report_Class.obj_goAML_Report
                .LastUpdateDate = DateTime.Now
                ' 27 Dec 2022 Ari Hide karena tidak menggunakan account
                'If objGoAML_Activity_Class IsNot Nothing AndAlso objGoAML_Activity_Class.list_goAML_Act_Account.Count > 0 Then
                '    .UNIKREFERENCE = objGoAML_Activity_Class.list_goAML_Act_Account.FirstOrDefault.CLIENT_NUMBER
                'End If
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not ObjModule.IsUseApproval Then
                    .STATUS = 1
                    lbl_Confirmation.Text = "Data Saved into Database"
                Else
                    .STATUS = 4
                    lbl_Confirmation.Text = "Data Saved into Pending Approval"
                End If
                Dim CIF As Long
                If cmb_Activity_Information.SelectedItemValue = 2 Then
                    .UNIKREFERENCE = TxtClientNumber_Relationship.Value
                    CIF = TxtClientNumber_Relationship.Value
                Else
                    .UNIKREFERENCE = txt_ClientNumber_Relationship_Entity.Value
                    CIF = txt_ClientNumber_Relationship_Entity.Value
                End If

                Dim intPeriode As Integer = Convert.ToInt32(DateTime.Now.Month)
                .ENTITY_REFERENCE = "OneFCC_IPSAT-" & CIF & "-" & intPeriode & "-" & DateTime.Now.Year
            End With

            'Save Report
            SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.SaveReportAdd(ObjModule, objGoAML_Report_Class)

            '18-Mar-2022 Pindah dari paling atas. Karena kalau kena validasi nanti ter-create berkali2.
            'Create Submission Notes
            Dim strNotes As String = txt_Report_ApprovalNotes.Value
            If String.IsNullOrWhiteSpace(strNotes) Then
                strNotes = ""
            End If
            SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.CreateNotes(SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumActionApproval.Request, SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumActionForm.Add, objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID, strNotes)

            Dim strQuery As String = "delete from SIPESATGOAML_REPORT_ERROR_MESSAGE where ERROR_MESSAGE = '' and FK_SIPESATGOAML_REPORT_HEADER_ID =" & objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


            '11-Mar-2022 Adi : Update Report Valid/Invalid
            UpdateReportIsValid()
            'End of 11-Mar-2022 Adi : Update Report Valid/Invalid

            'Recalculate List of Generated XML
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPESATGOAML_Update_ListOfGenerated", Nothing)

            'Show Confirmation Panel
            FormPanelReport.Hidden = True
            pnl_Confirmation.Hidden = False

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub PopulateReportData()
        Try
            With objGoAML_Report_Class.obj_goAML_Report
                .RENTITY_ID = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID(1)
                '.Rentity_Branch = GoAML_Global_BLL_.getGlobalParameterValueByID(7)
                .SUBMISSION_CODE = cmb_SubmisionCode.SelectedItemValue
                '.REPORT_CODE = cmb_JenisLaporan.SelectedItemValue
                .REPORT_CODE = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID(3)
                If Not txt_SubmissionDate.SelectedDate = DateTime.MinValue Then
                    .SUBMISSION_DATE = txt_SubmissionDate.SelectedDate
                Else
                    .SUBMISSION_DATE = Nothing
                End If
                If Not String.IsNullOrEmpty(df_MonthPeriod.SelectedItemValue) Then
                    .MONTH_PERIOD = df_MonthPeriod.SelectedItemValue
                Else
                    .MONTH_PERIOD = Nothing
                End If
                'If Not df_MonthPeriod.SelectedDate = DateTime.MinValue Then
                '    .MONTH_PERIOD = df_MonthPeriod.SelectedDate.ToString("MM-yyyy")
                'Else
                '    .MONTH_PERIOD = Nothing
                'End If
                '.MONTH_PERIOD = df_MonthPeriod.Text

                .CURRENCY_CODE_LOCAL = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID(4)
                '.Reason = txt_Catatan.Value
                '.Action = txt_Action.Value
                '.FK_Address_ID = 35
                '.FK_Report_Type_ID = cmb_ReportType.SelectedItemValue
                '.Fiu_Ref_Number = txt_FIU_Ref_Number.Value
                .UNIKREFERENCE = txt_ReportUniqueValue.Value
                .ISVALID = True     'Default true. Nanti akan diupdate saat panggil SP Report Validation
                .MarkedAsDelete = False

                'If Not txt_TransactionDate.SelectedDate = DateTime.MinValue Then
                '    .Transaction_Date = txt_TransactionDate.SelectedDate
                'End If
                'If GoAML_Report_BLL_.getTRNorACT(cmb_JenisLaporan.SelectedItemValue) = "ACT" Then
                '    .Transaction_Date = txt_SubmissionDate.SelectedDate
                'End If

                If .PK_REPORT_ID < 0 Then
                    .Active = True
                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .CreatedDate = DateTime.Now
                Else
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateDate = DateTime.Now
                End If
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_Report_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim strIsDeleteDraft As String = e.ExtraParams(0).Value

            'Keep or Delete Draft before Exit
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            '5-Jan-2022 Adi : Saran dari Cici jika tidak ada transaksi/activity sama sekali dihapus saja Draftnya
            'If strIsDeleteDraft = "1" Then
            If strIsDeleteDraft = "1" OrElse (objGoAML_Report_Class.list_goAML_Act_ReportPartyType Is Nothing OrElse objGoAML_Report_Class.list_goAML_Act_ReportPartyType.Count = 0) Then
                SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.DeleteReportByReportID(objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID, strUserID, strUserID)
            End If

            'Back to form View
            Dim strEncryptedModuleID = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & strEncryptedModuleID)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & strEncryptedModuleID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        If buttonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub

    Sub SetCommandColumnLocation()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim buttonPosition As Integer = 1
        If objParamSettingbutton IsNot Nothing Then
            buttonPosition = objParamSettingbutton.SettingValue
        End If

        'Report Level
        ColumnActionLocation(gp_Activity, cc_Activity, buttonPosition)

        'Transaction/Activity Level
        '--- Person
        ColumnActionLocation(gp_Person_Phone, cc_Person_Phone, buttonPosition)
        ColumnActionLocation(gp_Person_Address, cc_Person_Address, buttonPosition)
        ColumnActionLocation(gp_Person_Identification, cc_Person_Identification, buttonPosition)
        ColumnActionLocation(gp_Person_EmployerPhone, cc_Person_EmployerPhone, buttonPosition)
        ColumnActionLocation(gp_Person_EmployerAddress, cc_Person_EmployerAddress, buttonPosition)

        '--- Entity
        ColumnActionLocation(gp_Entity_Phone, cc_Entity_Phone, buttonPosition)
        ColumnActionLocation(gp_Entity_Address, cc_Entity_Address, buttonPosition)
        ColumnActionLocation(gp_Entity_Director, cc_Entity_Director, buttonPosition)
    End Sub

    Protected Sub BindGridPanel(gpObject As Ext.Net.GridPanel, dtObject As DataTable)
        Try
            gpObject.GetStore.DataSource = dtObject
            gpObject.GetStore.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ReportAdd_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Insert
    End Sub

    Protected Sub btn_PartyEntry_Click(sender As Object, e As DirectEventArgs)
        Try
            PnlRelationship.Hidden = False
            'gp_Person_Identification.Hidden = True
            Dim strID As String = e.ExtraParams(0).Value
            Dim strCommmand As String = e.ExtraParams(1).Value
            Dim strPartySide As String = e.ExtraParams(2).Value
            Dim strWindowTitle As String = ""
            If String.IsNullOrEmpty(cmb_Activity_Information.SelectedItemValue) Then
                Throw New ApplicationException("Activity Party Type is required.")
            End If
            If Me.ReadOnlyLevel = 0 Then
                Me.ReadOnlyLevel = 4
            End If

            'Define windows title based on Party Side
            strWindowTitle = strPartySide

            'If String.IsNullOrEmpty(cmb_Activity_Information.SelectedItemValue) Then
            '    Throw New ApplicationException("Activity Party Type is required.")
            'End If

            Me.PartySide = enumPartySide._Activity
            'Me.PartyType = cmb_Activity_Information.SelectedItemValue
            Me.IsMyClient = False

            'Kosongkan party sub type karena ini adalah level 1 (Party)
            Me.PartySubType = 0
            Me.PK_PartySub_ID = 0

            'Get PK_Party_ID
            Me.IsNewRecord = False
            Me.PartyType = cmb_Activity_Information.SelectedItemValue
            get_Existing_PK_Party_ID()

            'My Client/Not My Client
            If Me.IsMyClient Then
                strWindowTitle = strWindowTitle & " - My Client"
            Else
                strWindowTitle = strWindowTitle & " - Not My Client"
            End If

            'Open windows based on Party Type : 1=Account, 2=Person, 3=Entity
            Select Case Me.PartyType
                'Case enumPartyType._Account
                '    CleanWindowAccountParty()
                '    If strCommmand = "Edit" Or strCommmand = "Detail" Then
                '        LoadAccount()
                '    End If

                '    If strCommmand = "Edit" Then
                '        SetReadOnlyWindowAccount(False)
                '    Else
                '        SetReadOnlyWindowAccount(True)
                '    End If

                '    window_AccountParty.Title = "Customer - Account"
                '    window_AccountParty.Hidden = False
                '    window_AccountParty.Body.ScrollTo(Direction.Top, 0)

                Case enumPartyType._Person
                    CleanWindowPersonParty()
                    If strCommmand = "Edit" Or strCommmand = "Detail" Then
                        LoadPerson()
                    End If

                    If strCommmand = "Edit" Then
                        SetReadOnlyWindowPerson(False)
                    Else
                        SetReadOnlyWindowPerson(True)
                    End If

                    window_PersonParty.Title = "Person ( " & strWindowTitle & " )"
                    window_PersonParty.Hidden = False
                    window_PersonParty.Body.ScrollTo(Direction.Top, 0)

                Case enumPartyType._Entity
                    CleanWindowEntityParty()
                    If strCommmand = "Edit" Or strCommmand = "Detail" Then
                        LoadEntity()
                    End If

                    If strCommmand = "Edit" Then
                        SetReadOnlyWindowEntity(False)
                    Else
                        SetReadOnlyWindowEntity(True)
                    End If

                    window_EntityParty.Title = "Entity ( " & strWindowTitle & " )"
                    window_EntityParty.Hidden = False
                    window_EntityParty.Body.ScrollTo(Direction.Top, 0)
            End Select

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Confirmation_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim strEncryptedModuleID = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & strEncryptedModuleID)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & strEncryptedModuleID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

#End Region
    ' 27 Dec 2022 Ari : Hide Account
    '#Region "Account"
    '    Protected Sub chk_Account_IsEntity_Changed()
    '        Try
    '            If chk_Account_IsEntity.Checked Then
    '                pnl_Account_Entity.Hidden = False
    '            Else
    '                pnl_Account_Entity.Hidden = True
    '            End If
    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    'Protected Sub chk_Account_Entity_IsClosed_Changed()
    '    '    Try
    '    '        If chk_Account_Entity_IsClosed.Checked Then
    '    '            txt_Account_Entity_ClosingDate.Hidden = False
    '    '        Else
    '    '            txt_Account_Entity_ClosingDate.Value = Nothing
    '    '            txt_Account_Entity_ClosingDate.Hidden = True
    '    '        End If
    '    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    '    Catch ex As Exception
    '    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    '    End Try
    '    'End Sub

    '    Protected Sub btn_Account_Save_Click()
    '        Try
    '            ValidateAccount()

    '            Dim objAccount = objGoAML_Activity_Class.list_goAML_Act_Account.Where(Function(x) x.PK_ACT_ACCOUNT_ID = Me.PK_Party_ID).FirstOrDefault
    '            If objAccount Is Nothing Then
    '                objAccount = New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACCOUNT
    '                objAccount.PK_ACT_ACCOUNT_ID = Me.PK_Party_ID
    '                Me.IsNewRecord = True
    '            Else
    '                Me.IsNewRecord = False
    '            End If

    '            With objAccount
    '                .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
    '                .FK_Act_ReportParty_ID = Me.PK_Activity_ID

    '                .Institution_Name = txt_Account_InstitutionName.Value
    '                .INSTITUTION_CODE = txt_Account_InstitutionCode.Value
    '                .Swift_Code = txt_Account_SwiftCode.Value
    '                '.Non_Banking_Institution = chk_Account_NonBankingInstitution.Checked
    '                .Branch = txt_Account_Branch.Value
    '                .Account = txt_Account_Number.Value
    '                .Currency_Code = cmb_Account_CurrencyCode.SelectedItemValue
    '                '.ACCOUNT_NAME = txt_Account_Label.Value
    '                '.iban = txt_Account_IBAN.Value
    '                .Client_Number = txt_Account_ClientNumber.Value
    '                .Personal_Account_Type = cmb_Account_Type.SelectedItemValue
    '                If Not txt_Account_OpeningDate.SelectedDate = DateTime.MinValue Then
    '                    .Opened = txt_Account_OpeningDate.SelectedDate
    '                Else
    '                    .Opened = Nothing
    '                End If
    '                If Not txt_Account_ClosingDate.SelectedDate = DateTime.MinValue Then
    '                    .Closed = txt_Account_ClosingDate.SelectedDate
    '                Else
    '                    .Closed = Nothing
    '                End If
    '                'If Not txt_Account_BalanceDate.SelectedDate = DateTime.MinValue Then
    '                '    .Date_Balance = txt_Account_BalanceDate.SelectedDate
    '                'Else
    '                '    .Date_Balance = Nothing
    '                'End If

    '                'If String.IsNullOrEmpty(txt_Account_Balance.Text) Or txt_Account_Balance.Text = "NA" Then
    '                '    .Balance = Nothing
    '                'Else
    '                '    .Balance = CDbl(txt_Account_Balance.Value)
    '                'End If

    '                .Status_Code = cmb_Account_StatusCode.SelectedItemValue
    '                '.Beneficiary = txt_Account_Beneficiary.Value
    '                '.Beneficiary_Comment = txt_Account_BeneficiaryComment.Value
    '                .Comments = txt_Account_Comment.Value
    '                .IsRekeningKorporasi = chk_Account_IsEntity.Checked

    '                If Me.IsNewRecord Then
    '                    objAccount.Active = True
    '                    objAccount.CreatedDate = DateTime.Now
    '                    objAccount.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID

    '                    objGoAML_Activity_Class.list_goAML_Act_Account.Add(objAccount)
    '                Else
    '                    objAccount.LastUpdateDate = DateTime.Now
    '                    objAccount.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                End If

    '                'Account Entity
    '                If chk_Account_IsEntity.Checked Then
    '                    Dim objAccountEntity = objGoAML_Activity_Class.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID).FirstOrDefault
    '                    If objAccountEntity Is Nothing Then
    '                        objAccountEntity = New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ENTITY_ACCOUNT
    '                        objAccountEntity.PK_ACT_ENTITY_ACCOUNT_ID = Me.PK_Party_AccountEntity_ID
    '                        Me.IsNewRecord = True
    '                    Else
    '                        Me.IsNewRecord = False
    '                    End If

    '                    With objAccountEntity
    '                        .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
    '                        .FK_Activity_ReportParty_ID = Me.PK_Activity_ID
    '                        .FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID

    '                        .Name = txt_Account_Entity_CorporateName.Value
    '                        '.Commercial_Name = txt_Account_Entity_CommercialName.Value
    '                        '.Incorporation_Legal_Form = cmb_Account_Entity_IncorporationLegalForm.SelectedItemValue
    '                        '.Incorporation_Number = txt_Account_Entity_IncorporationNumber.Value
    '                        .Business = txt_Account_Entity_Business.Value
    '                        '.Email = txt_Account_Entity_Email.Value
    '                        '.Url = txt_Account_Entity_URL.Value
    '                        .Incorporation_State = txt_Account_Entity_IncorporationState.Value
    '                        .Incorporation_Country_Code = cmb_Account_Entity_IncorporationCountryCode.SelectedItemValue
    '                        If Not txt_Account_Entity_IncorporationDate.SelectedDate = DateTime.MinValue Then
    '                            .Incorporation_Date = txt_Account_Entity_IncorporationDate.SelectedDate
    '                        Else
    '                            .Incorporation_Date = Nothing
    '                        End If

    '                        '.Business_Closed = chk_Account_Entity_IsClosed.Checked
    '                        'If chk_Account_Entity_IsClosed.Checked Then
    '                        '    If Not txt_Account_Entity_ClosingDate.SelectedDate = DateTime.MinValue Then
    '                        '        .Date_Business_Closed = txt_Account_Entity_ClosingDate.SelectedDate
    '                        '    End If
    '                        'Else
    '                        '    .Date_Business_Closed = Nothing
    '                        'End If
    '                        .TAX_NUMBER = txt_Account_Entity_TaxNumber.Value
    '                        '.Comments = txt_Account_Entity_Comment.Value
    '                    End With

    '                    If Me.IsNewRecord Then
    '                        objAccountEntity.Active = True
    '                        objAccountEntity.CreatedDate = DateTime.Now
    '                        objAccountEntity.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID

    '                        objGoAML_Activity_Class.list_goAML_Act_Entity_Account.Add(objAccountEntity)
    '                    Else
    '                        objAccountEntity.LastUpdateDate = DateTime.Now
    '                        objAccountEntity.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                    End If
    '                End If
    '            End With

    '            'Load Party Short Information
    '            LoadActivityShortInfo()

    '            window_AccountParty.Hidden = True
    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub btn_Account_Back_Click()
    '        Try
    '            window_AccountParty.Hidden = True
    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub LoadAccount()
    '        Try
    '            Dim objAccount As SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACCOUNT = objGoAML_Activity_Class.list_goAML_Act_Account.Where(Function(x) x.PK_ACT_ACCOUNT_ID = Me.PK_Party_ID).FirstOrDefault
    '            If objAccount IsNot Nothing Then
    '                With objAccount
    '                    txt_Account_InstitutionName.Value = .INSTITUTION_NAME
    '                    txt_Account_InstitutionCode.Value = .INSTITUTION_CODE
    '                    txt_Account_SwiftCode.Value = .SWIFT_CODE

    '                    'If Not IsNothing(.Non_Banking_Institution) Then
    '                    '    chk_Account_NonBankingInstitution.Checked = .Non_Banking_Institution
    '                    'End If

    '                    txt_Account_Branch.Value = .Branch
    '                    txt_Account_Number.Value = .Account

    '                    If Not IsNothing(.Currency_Code) Then
    '                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Mata_Uang", "kode", .CURRENCY_CODE)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_Account_CurrencyCode.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                        End If
    '                    End If

    '                    'txt_Account_Label.Value = .ACCOUNT_NAME
    '                    'txt_Account_IBAN.Value = .iban
    '                    txt_Account_ClientNumber.Value = .Client_Number

    '                    If Not IsNothing(.Personal_Account_Type) Then
    '                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_SIPESATGoAML_Jenis_Rekening", "kode", .PERSONAL_ACCOUNT_TYPE)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_Account_Type.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                        End If
    '                    End If

    '                    If .Opened.HasValue Then
    '                        txt_Account_OpeningDate.Value = .Opened
    '                    End If

    '                    If .Closed.HasValue Then
    '                        txt_Account_ClosingDate.Value = .Closed
    '                    End If

    '                    'txt_Account_Balance.Value = .Balance
    '                    'If .Date_Balance.HasValue Then
    '                    '    txt_Account_BalanceDate.Value = .Date_Balance
    '                    'End If

    '                    If Not IsNothing(.Status_Code) Then
    '                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Status_Rekening", "kode", .STATUS_CODE)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_Account_StatusCode.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                        End If
    '                    End If

    '                    'txt_Account_Beneficiary.Value = .Beneficiary
    '                    'txt_Account_BeneficiaryComment.Value = .Beneficiary_Comment
    '                    txt_Account_Comment.Value = .COMMENTS

    '                    'Bind Signatory
    '                    Dim listAccountSignatory = objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Where(Function(x) x.FK_ACT_ACCOUNT_ID = .PK_ACT_ACCOUNT_ID).ToList
    '                    If listAccountSignatory Is Nothing Then
    '                        listAccountSignatory = New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGNATORY)
    '                    End If

    '                    Dim dtAccountSignatory As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountSignatory)
    '                    dtAccountSignatory.Columns.Add(New DataColumn("PK_Account_Signatory_ID", GetType(String)))
    '                    'dtAccountSignatory.Columns.Add(New DataColumn("RoleName", GetType(String)))

    '                    Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                        'Dim listRole = objdb.goAML_Ref_Peran_orang_dalam_rekening.ToList
    '                        For Each row In dtAccountSignatory.Rows
    '                            row("PK_Account_Signatory_ID") = row("PK_ACT_ACC_SIGNATORY_ID")   'Karena pakai window yg sama, PK harus disalin
    '                            'If Not IsDBNull(row("role")) Then
    '                            '    Dim objRole = listRole.Where(Function(x) x.Kode = row("role")).FirstOrDefault
    '                            '    If objRole IsNot Nothing Then
    '                            '        row("RoleName") = objRole.Kode & " - " & objRole.Keterangan
    '                            '    End If
    '                            'End If
    '                        Next
    '                    End Using
    '                    BindGridPanel(gp_Account_Signatory, dtAccountSignatory)

    '                    'Is Entity Account?
    '                    If .IsRekeningKorporasi IsNot Nothing AndAlso .IsRekeningKorporasi Then
    '                        chk_Account_IsEntity.Checked = True
    '                        pnl_Account_Entity.Hidden = False

    '                        Dim objAccountEntity As SIPESATGoAMLDAL.SIPESATGOAML_ACT_ENTITY_ACCOUNT = objGoAML_Activity_Class.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID).FirstOrDefault
    '                        If objAccountEntity IsNot Nothing Then
    '                            With objAccountEntity
    '                                txt_Account_Entity_CorporateName.Value = .NAME
    '                                'txt_Account_Entity_CommercialName.Value = .Commercial_Name

    '                                'If Not IsNothing(.Incorporation_Legal_Form) Then
    '                                '    drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Bentuk_Badan_Usaha", "kode", .INCORPORATION_LEGAL_FORM)
    '                                '    If drTemp IsNot Nothing Then
    '                                '        cmb_Account_Entity_IncorporationLegalForm.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                '    End If
    '                                'End If

    '                                'txt_Account_Entity_IncorporationNumber.Value = .Incorporation_Number
    '                                txt_Account_Entity_Business.Value = .BUSINESS
    '                                'txt_Account_Entity_Email.Value = .Email
    '                                'txt_Account_Entity_URL.Value = .Url
    '                                txt_Account_Entity_IncorporationState.Value = .Incorporation_State

    '                                If Not IsNothing(.Incorporation_Country_Code) Then
    '                                    drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "kode", .INCORPORATION_COUNTRY_CODE)
    '                                    If drTemp IsNot Nothing Then
    '                                        cmb_Account_Entity_IncorporationCountryCode.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                    End If
    '                                End If

    '                                If .Incorporation_Date.HasValue Then
    '                                    txt_Account_Entity_IncorporationDate.Value = .Incorporation_Date
    '                                End If

    '                                'If .Business_Closed IsNot Nothing AndAlso .Business_Closed Then
    '                                '    chk_Account_Entity_IsClosed.Checked = True
    '                                '    If .Date_Business_Closed.HasValue Then
    '                                '        txt_Account_Entity_ClosingDate.Value = .Date_Business_Closed
    '                                '    End If
    '                                '    txt_Account_Entity_ClosingDate.Hidden = False
    '                                'Else
    '                                '    txt_Account_Entity_ClosingDate.Hidden = True
    '                                'End If

    '                                txt_Account_Entity_TaxNumber.Value = .TAX_NUMBER
    '                                'txt_Account_Entity_Comment.Value = .Comments

    '                                Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                                    'Bind Address
    '                                    Dim listAccountEntityAddress = objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Address.Where(Function(x) x.FK_ACT_ENTITY_ACCOUNT_ID = objAccountEntity.PK_ACT_ENTITY_ACCOUNT_ID).ToList
    '                                    If listAccountEntityAddress Is Nothing Then
    '                                        listAccountEntityAddress = New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_ENTITY_ADDRESS)
    '                                    End If
    '                                    Dim dtAccountEntityAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountEntityAddress)
    '                                    dtAccountEntityAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
    '                                    dtAccountEntityAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
    '                                    dtAccountEntityAddress.Columns.Add(New DataColumn("Country", GetType(String)))

    '                                    Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                                    Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList

    '                                    For Each row In dtAccountEntityAddress.Rows
    '                                        row("PK_Address_ID") = row("PK_ACT_ACC_ENTITY_ADDRESS_ID")
    '                                        If Not IsDBNull(row("Address_Type")) Then
    '                                            Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
    '                                            If objAddressType IsNot Nothing Then
    '                                                row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
    '                                            End If
    '                                        End If
    '                                        If Not IsDBNull(row("Country_Code")) Then
    '                                            Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
    '                                            If objCountry IsNot Nothing Then
    '                                                row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                                            End If
    '                                        End If
    '                                    Next
    '                                    BindGridPanel(gp_Account_Entity_Address, dtAccountEntityAddress)

    '                                    'Bind Phone
    '                                    Dim listAccountEntityPhone = objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Phone.Where(Function(x) x.FK_ACT_ACC_ENTITY_ID = objAccountEntity.PK_ACT_ENTITY_ACCOUNT_ID).ToList
    '                                    If listAccountEntityPhone Is Nothing Then
    '                                        listAccountEntityPhone = New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_ENTITY_PHONE)
    '                                    End If
    '                                    Dim dtAccountEntityPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountEntityPhone)
    '                                    dtAccountEntityPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
    '                                    dtAccountEntityPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
    '                                    dtAccountEntityPhone.Columns.Add(New DataColumn("CommunicationType", GetType(String)))

    '                                    Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                                    Dim listCommunicationType = objdb.goAML_Ref_Jenis_Alat_Komunikasi.ToList

    '                                    For Each row In dtAccountEntityPhone.Rows
    '                                        row("PK_Phone_ID") = row("PK_ACT_ACC_ENTITY_PHONE_ID")
    '                                        If Not IsDBNull(row("Tph_Contact_Type")) Then
    '                                            Dim objContactType = listContactType.Where(Function(x) x.Kode = row("Tph_Contact_Type")).FirstOrDefault
    '                                            If objContactType IsNot Nothing Then
    '                                                row("ContactType") = objContactType.Kode & " - " & objContactType.Keterangan
    '                                            End If
    '                                        End If
    '                                        If Not IsDBNull(row("TPH_COMMUNICATION_TYPE")) Then
    '                                            Dim objCommunicationType = listCommunicationType.Where(Function(x) x.Kode = row("TPH_COMMUNICATION_TYPE")).FirstOrDefault
    '                                            If objCommunicationType IsNot Nothing Then
    '                                                row("CommunicationType") = objCommunicationType.Kode & " - " & objCommunicationType.Keterangan
    '                                            End If
    '                                        End If
    '                                    Next
    '                                    BindGridPanel(gp_Account_Entity_Phone, dtAccountEntityPhone)

    '                                    'Bind Director
    '                                    'sementara ga ada director
    '                                    'Dim listAccountEntityDirector = objGoAML_Activity_Class.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = objAccountEntity.PK_SIPESATGOAML_Act_Entity_account).ToList
    '                                    'If listAccountEntityDirector Is Nothing Then
    '                                    '    listAccountEntityDirector = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Ent_Director)
    '                                    'End If

    '                                    'Dim dtAccountEntityDirector As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountEntityDirector)
    '                                    'dtAccountEntityDirector.Columns.Add(New DataColumn("PK_Account_Entity_Director_ID", GetType(String)))
    '                                    'dtAccountEntityDirector.Columns.Add(New DataColumn("RoleName", GetType(String)))

    '                                    'Dim listRole = objdb.goAML_Ref_Peran_orang_dalam_Korporasi.ToList
    '                                    'For Each row In dtAccountEntityDirector.Rows
    '                                    '    row("PK_Account_Entity_Director_ID") = row("PK_Act_Acc_Ent_Director_ID")   'Karena pakai window yg sama, PK harus disalin
    '                                    '    If Not IsDBNull(row("role")) Then
    '                                    '        Dim objRole = listRole.Where(Function(x) x.Kode = row("role")).FirstOrDefault
    '                                    '        If objRole IsNot Nothing Then
    '                                    '            row("RoleName") = objRole.Kode & " - " & objRole.Keterangan
    '                                    '        End If
    '                                    '    End If
    '                                    'Next
    '                                    'BindGridPanel(gp_Account_Entity_Director, dtAccountEntityDirector)
    '                                End Using
    '                            End With

    '                        End If
    '                    Else
    '                        chk_Account_IsEntity.Checked = False
    '                        pnl_Account_Entity.Hidden = True

    '                        'Bind data kosong ke Gridpanel
    '                        'Bind Address
    '                        Dim listAccountEntityAddress = New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_ENTITY_ADDRESS)
    '                        Dim dtAccountEntityAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountEntityAddress)
    '                        dtAccountEntityAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
    '                        dtAccountEntityAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
    '                        dtAccountEntityAddress.Columns.Add(New DataColumn("Country", GetType(String)))
    '                        BindGridPanel(gp_Account_Entity_Address, dtAccountEntityAddress)

    '                        'Bind Phone
    '                        Dim listAccountEntityPhone = New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_ENTITY_PHONE)
    '                        Dim dtAccountEntityPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountEntityPhone)
    '                        dtAccountEntityPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
    '                        dtAccountEntityPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
    '                        dtAccountEntityPhone.Columns.Add(New DataColumn("CommunicationType", GetType(String)))
    '                        BindGridPanel(gp_Account_Entity_Phone, dtAccountEntityPhone)

    '                        'Bind Director
    '                        'sementara ga ada director
    '                        'Dim listAccountEntityDirector = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Ent_Director)
    '                        'Dim dtAccountEntityDirector As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountEntityDirector)
    '                        'dtAccountEntityDirector.Columns.Add(New DataColumn("PK_Account_Entity_Director_ID", GetType(String)))
    '                        'dtAccountEntityDirector.Columns.Add(New DataColumn("RoleName", GetType(String)))
    '                        'BindGridPanel(gp_Account_Entity_Director, dtAccountEntityDirector)
    '                    End If
    '                End With
    '            End If
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub

    '#End Region


#Region "Person"
    Protected Sub btn_Person_Entry_Click(sender As Object, e As DirectEventArgs)
        Try
            PnlRelationship.Hidden = True
            Dim strID As String = e.ExtraParams(0).Value
            Dim strCommand As String = e.ExtraParams(1).Value
            Dim strPersonType As String = e.ExtraParams(2).Value
            Dim strWindowTitle As String = ""

            If Me.ReadOnlyLevel = 0 Then
                Me.ReadOnlyLevel = 5
            End If

            'Define windows title based on Party Side
            If IsNothing(Me.IsMyClient) Then
                Me.IsMyClient = True
            End If

            Dim strPartySide As String = ""
            Select Case Me.PartySide
                'Case enumPartySide._From
                '    strPartySide = "From"
                'Case enumPartySide._To
                '    strPartySide = "To"
                'Case enumPartySide._Conductor
                '    strPartySide = "Conductor"
                'Case enumPartySide._TParty
                '    strPartySide = "T-Party"
                Case enumPartySide._Activity
                    strPartySide = "Activity"
            End Select


            Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity
            strWindowTitle = "Entity Director (" & strPartySide & ")"


            If Me.IsMyClient Then
                strWindowTitle = strWindowTitle & " - My Client"
            Else
                strWindowTitle = strWindowTitle & " - Not My Client"
            End If

            'Get PK_PartySub_ID
            Me.IsNewRecord = False
            If strID <> 0 Then
                Me.PK_PartySub_ID = strID
            Else
                get_New_PK_PartySub_ID()
            End If

            If strCommand = "Add" Or strCommand = "Edit" Or strCommand = "Detail" Then
                CleanWindowPersonParty()
                If strCommand = "Edit" Or strCommand = "Detail" Then
                    LoadPerson()
                End If

                If strCommand = "Add" Or strCommand = "Edit" Then
                    SetReadOnlyWindowPerson(False)
                Else
                    SetReadOnlyWindowPerson(True)
                End If

                'Open windows Person
                'txt_Person_Title.Focus()
                window_PersonParty.Title = strWindowTitle
                window_PersonParty.Hidden = False
                window_PersonParty.Body.ScrollTo(Direction.Top, 0)

            ElseIf strCommand = "Delete" Then
                DeletePerson()
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Protected Sub btn_Person_Save_Click()
    '    Try
    '        ValidatePerson()

    '        If Me.PartyType = enumPartyType._Person Then
    '            Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Person.Where(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ID = Me.PK_Party_ID).FirstOrDefault
    '            If objCek Is Nothing Then
    '                objCek = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON
    '                objCek.PK_SIPESATGOAML_ACT_PERSON_ID = Me.PK_Party_ID
    '                Me.IsNewRecord = True
    '            Else
    '                Me.IsNewRecord = False
    '            End If

    '            With objCek
    '                .FK_REPORT_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
    '                .FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID

    '                If Not String.IsNullOrEmpty(cmb_Person_Gender.SelectedItemValue) Then
    '                    .GENDER = cmb_Person_Gender.SelectedItemValue
    '                End If
    '                .TITLE = txt_Person_Title.Value
    '                .LAST_NAME = txt_Person_LastName.Value
    '                If txt_Person_BirthDate.SelectedDate <> DateTime.MinValue Then
    '                    .BIRTHDATE = txt_Person_BirthDate.SelectedDate
    '                End If
    '                .BITH_PLACE = txt_Person_BirthPlace.Value
    '                .MOTHERS_NAME = txt_Person_MotherName.Value
    '                .ALIAS = txt_Person_Alias.Value
    '                .SSN = txt_Person_SSN.Value
    '                .PASSPORT_NUMBER = txt_Person_PassportNumber.Value
    '                .PASSPORT_COUNTRY = cmb_Person_PassportCountry.SelectedItemValue
    '                .ID_NUMBER = txt_Person_IDNumber.Value
    '                .NATIONALITY1 = cmb_Person_Nationality1.SelectedItemValue
    '                .NATIONALITY2 = cmb_Person_Nationality2.SelectedItemValue
    '                .NATIONALITY3 = cmb_Person_Nationality3.SelectedItemValue
    '                .RESIDENCE = cmb_Person_Residence.SelectedItemValue
    '                .EMAIL = txt_Person_Email1.Value
    '                .EMAIL2 = txt_Person_Email2.Value
    '                .EMAIL3 = txt_Person_Email3.Value
    '                .EMAIL4 = txt_Person_Email4.Value
    '                .EMAIL5 = txt_Person_Email5.Value
    '                .OCCUPATION = txt_Person_Occupation.Value
    '                .EMPLOYER_NAME = txt_Person_EmployerName.Value
    '                'If chk_Person_IsDecease.Checked Then
    '                '    .DECEASED = True
    '                '    If Not txt_Person_DeceaseDate.SelectedDate = DateTime.MinValue Then
    '                '        .DECEASED_DATE = txt_Person_DeceaseDate.SelectedDate
    '                '    End If
    '                'Else
    '                '    .DECEASED = False
    '                '    .DECEASED_DATE = Nothing
    '                'End If
    '                .TAX_NUMBER = txt_Person_TaxNumber.Value
    '                .TAX_REG_NUMBER = chk_Person_IsPEP.Checked
    '                .SOURCE_OF_WEALTH = txt_Person_SourceOfWealth.Value
    '                .COMMENT = txt_Person_Comment.Value
    '            End With

    '            If Me.IsNewRecord Then
    '                objCek.Active = True
    '                objCek.CreatedDate = DateTime.Now
    '                objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                objGoAML_Activity_Class.list_goAML_Act_Person.Add(objCek)
    '            Else
    '                objCek.LastUpdateDate = DateTime.Now
    '                objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
    '            End If
    '        Else
    '            Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_ID = Me.PK_PartySub_ID).FirstOrDefault
    '            If objCek Is Nothing Then
    '                objCek = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR
    '                objCek.PK_SIPESATGOAML_ACT_DIRECTOR_ID = Me.PK_PartySub_ID
    '                Me.IsNewRecord = True
    '            Else
    '                Me.IsNewRecord = False
    '            End If

    '            With objCek
    '                .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
    '                .FK_ReportParty_ID = Me.PK_Activity_ID
    '                .FK_Act_Entity_ID = Me.PK_Party_ID
    '                .FK_Sender_Information = enumPartyType._Entity

    '                If Not String.IsNullOrEmpty(cmb_Person_Gender.SelectedItemValue) Then
    '                    .Gender = cmb_Person_Gender.SelectedItemValue
    '                End If
    '                .Title = txt_Person_Title.Value
    '                .Last_Name = txt_Person_LastName.Value
    '                If txt_Person_BirthDate.SelectedDate <> DateTime.MinValue Then
    '                    .BirthDate = txt_Person_BirthDate.SelectedDate
    '                End If
    '                .Birth_Place = txt_Person_BirthPlace.Value
    '                .Mothers_Name = txt_Person_MotherName.Value
    '                .Alias = txt_Person_Alias.Value
    '                .SSN = txt_Person_SSN.Value
    '                .Passport_Number = txt_Person_PassportNumber.Value
    '                .Passport_Country = cmb_Person_PassportCountry.SelectedItemValue
    '                .Id_Number = txt_Person_IDNumber.Value
    '                .Nationality1 = cmb_Person_Nationality1.SelectedItemValue
    '                .Nationality2 = cmb_Person_Nationality2.SelectedItemValue
    '                .Nationality3 = cmb_Person_Nationality3.SelectedItemValue
    '                .Residence = cmb_Person_Residence.SelectedItemValue
    '                .Email = txt_Person_Email1.Value
    '                .email2 = txt_Person_Email2.Value
    '                .email3 = txt_Person_Email3.Value
    '                .email4 = txt_Person_Email4.Value
    '                .email5 = txt_Person_Email5.Value
    '                .Occupation = txt_Person_Occupation.Value
    '                .Employer_Name = txt_Person_EmployerName.Value

    '                .Deceased = False
    '                .DECEASED_DATE = Nothing

    '                .Tax_Number = txt_Person_TaxNumber.Value
    '                .Tax_Reg_Number = chk_Person_IsPEP.Checked
    '                .Source_Of_Wealth = txt_Person_SourceOfWealth.Value
    '                .Comment = txt_Person_Comment.Value

    '                .role = cmb_Person_Director_Role.SelectedItemValue
    '            End With

    '            If Me.IsNewRecord Then
    '                objCek.Active = True
    '                objCek.CreatedDate = DateTime.Now
    '                objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                objGoAML_Activity_Class.list_goAML_Act_Director.Add(objCek)
    '            Else
    '                objCek.LastUpdateDate = DateTime.Now
    '                objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
    '            End If

    '            'Bind to Grid Panel
    '            Dim listAccountDirector = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.FK_ACT_ENTITY_ID = Me.PK_Party_ID).ToList
    '            If listAccountDirector Is Nothing Then
    '                listAccountDirector = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR)
    '            End If

    '            Dim dtAccountDirector As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountDirector)
    '            dtAccountDirector.Columns.Add(New DataColumn("PK_Entity_Director_ID", GetType(String)))
    '            dtAccountDirector.Columns.Add(New DataColumn("RoleName", GetType(String)))

    '            Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                Dim listRole = objdb.goAML_Ref_Peran_orang_dalam_Korporasi.ToList
    '                For Each row In dtAccountDirector.Rows
    '                    row("PK_Entity_Director_ID") = row("PK_SIPESATGOAML_ACT_DIRECTOR_ID")   'Karena pakai window yg sama, PK harus disalin
    '                    If Not IsDBNull(row("role")) Then
    '                        Dim objRole = listRole.Where(Function(x) x.Kode = row("role")).FirstOrDefault
    '                        If objRole IsNot Nothing Then
    '                            row("RoleName") = objRole.Kode & " - " & objRole.Keterangan
    '                        End If
    '                    End If
    '                Next
    '            End Using
    '            BindGridPanel(gp_Entity_Director, dtAccountDirector)
    '        End If
    '        Dim lngPK_Relationship As Long = 0
    '        If objGoAML_Activity_Class.list_goAML_Act_Person_Relationship.Count > 0 Then
    '            lngPK_Relationship = objGoAML_Activity_Class.list_goAML_Act_Person_Relationship.Min(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_RELATIONSHIP_ID)
    '            If lngPK_Relationship > 0 Then
    '                lngPK_Relationship = 0
    '            End If
    '        Else
    '            lngPK_Relationship = -1
    '        End If
    '        Dim IdPerson As Long = 0
    '        Dim objPerson = objGoAML_Activity_Class.list_goAML_Act_Person.Where(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ID = Me.PK_Party_ID).FirstOrDefault
    '        If objPerson IsNot Nothing Then
    '            IdPerson = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID
    '        End If


    '        Dim CountRelationshipNull As Integer = 0
    '        Dim ObjRelationship As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_RELATIONSHIP
    '        If TxtClientNumber_Relationship.Value IsNot Nothing Then
    '            ObjRelationship.CLIENT_NUMBER = TxtClientNumber_Relationship.Value
    '        Else
    '            CountRelationshipNull = CountRelationshipNull + 1
    '        End If
    '        If txtComments_Relationship.Value IsNot Nothing Then
    '            ObjRelationship.COMMENTS = txtComments_Relationship.Value
    '        Else
    '            CountRelationshipNull = CountRelationshipNull + 1
    '        End If
    '        If Not TxtValidFrom_Relationship.SelectedDate = DateTime.MinValue Then
    '            ObjRelationship.VALID_FROM = TxtValidFrom_Relationship.Value
    '        Else
    '            CountRelationshipNull = CountRelationshipNull + 1
    '        End If
    '        If Not TxtValidTo_Relationship.SelectedDate = DateTime.MinValue Then
    '            ObjRelationship.VALID_TO = TxtValidTo_Relationship.Value
    '        Else
    '            CountRelationshipNull = CountRelationshipNull + 1
    '        End If
    '        If CountRelationshipNull > 0 Then
    '            ObjRelationship.CreatedDate = DateTime.Now
    '            ObjRelationship.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID

    '            ObjRelationship.PK_SIPESATGOAML_ACT_PERSON_RELATIONSHIP_ID = lngPK_Relationship
    '            ObjRelationship.FK_ACT_PERSON_ID = IdPerson
    '            objGoAML_Activity_Class.list_goAML_Act_Person_Relationship.Add(ObjRelationship)
    '        Else
    '            Dim ObjRelationshipNull As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_RELATIONSHIP
    '            objGoAML_Activity_Class.list_goAML_Act_Person_Relationship.Add(ObjRelationshipNull)
    '        End If




    '        'Load Party Short Information
    '        LoadActivityShortInfo()

    '        'Close Window Person
    '        window_PersonParty.Hidden = True

    '        ' 27 Dec 2022 Ari : Hide
    '        'If Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.Signatory Then
    '        '    Dim objCek = objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Where(Function(x) x.PK_ACT_ACC_SIGNATORY_ID = Me.PK_PartySub_ID).FirstOrDefault
    '        '    If objCek Is Nothing Then
    '        '        objCek = New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGNATORY
    '        '        objCek.PK_ACT_ACC_SIGNATORY_ID = Me.PK_PartySub_ID
    '        '        Me.IsNewRecord = True
    '        '    Else
    '        '        Me.IsNewRecord = False
    '        '    End If

    '        '    With objCek
    '        '        .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
    '        '        .FK_ACT_REPORTPARTYTYPE_ID = Me.PK_Activity_ID
    '        '        .FK_ACT_ACCOUNT_ID = Me.PK_Party_ID

    '        '        If Not String.IsNullOrEmpty(cmb_Person_Gender.SelectedItemValue) Then
    '        '            .Gender = cmb_Person_Gender.SelectedItemValue
    '        '        End If
    '        '        '.Title = txt_Person_Title.Value
    '        '        .Last_Name = txt_Person_LastName.Value
    '        '        If txt_Person_BirthDate.SelectedDate <> DateTime.MinValue Then
    '        '            .BirthDate = txt_Person_BirthDate.SelectedDate
    '        '        End If
    '        '        .Birth_Place = txt_Person_BirthPlace.Value
    '        '        '.Mothers_Name = txt_Person_MotherName.Value
    '        '        '.Alias = txt_Person_Alias.Value
    '        '        .SSN = txt_Person_SSN.Value
    '        '        .Passport_Number = txt_Person_PassportNumber.Value
    '        '        .Passport_Country = cmb_Person_PassportCountry.SelectedItemValue
    '        '        .Id_Number = txt_Person_IDNumber.Value
    '        '        .Nationality1 = cmb_Person_Nationality1.SelectedItemValue
    '        '        '.Nationality2 = cmb_Person_Nationality2.SelectedItemValue
    '        '        '.Nationality3 = cmb_Person_Nationality3.SelectedItemValue
    '        '        .Residence = cmb_Person_Residence.SelectedItemValue
    '        '        '.Email = txt_Person_Email1.Value
    '        '        '.email2 = txt_Person_Email2.Value
    '        '        '.email3 = txt_Person_Email3.Value
    '        '        '.email4 = txt_Person_Email4.Value
    '        '        '.email5 = txt_Person_Email5.Value
    '        '        .Occupation = txt_Person_Occupation.Value
    '        '        .Employer_Name = txt_Person_EmployerName.Value
    '        '        'If chk_Person_IsDecease.Checked Then
    '        '        '    .Deceased = True
    '        '        '    'If Not txt_Person_DeceaseDate.SelectedDate = DateTime.MinValue Then
    '        '        '    '    .Deceased_Date = txt_Person_DeceaseDate.SelectedDate
    '        '        '    'End If
    '        '        'Else
    '        '        '    .Deceased = False
    '        '        '    '.Deceased_Date = Nothing
    '        '        'End If
    '        '        '.Tax_Number = txt_Person_TaxNumber.Value
    '        '        'If chk_Person_IsPEP.Checked Then
    '        '        '    .TAX_REG_NUMBER = "Y"
    '        '        'Else
    '        '        '    .TAX_REG_NUMBER = "T"
    '        '        'End If
    '        '        .TAX_REG_NUMBER = chk_Person_IsPEP.Checked
    '        '        .SOURCE_OF_WEALTH = txt_Person_SourceOfWealth.Value
    '        '        '.Comment = txt_Person_Comment.Value

    '        '        '.isPrimary = chk_Person_AccountSignatory_IsPrimary.Checked
    '        '        '.role = cmb_Person_AccountSignatory_Role.SelectedItemValue
    '        '    End With

    '        '    If Me.IsNewRecord Then
    '        '        objCek.Active = True
    '        '        objCek.CreatedDate = DateTime.Now
    '        '        objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '        '        objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Add(objCek)
    '        '    Else
    '        '        objCek.LastUpdateDate = DateTime.Now
    '        '        objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
    '        '    End If

    '        '    'Bind to Grid Panel
    '        '    Dim listAccountSignatory = objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Where(Function(x) x.FK_ACT_ACCOUNT_ID = Me.PK_Party_ID).ToList
    '        '    If listAccountSignatory Is Nothing Then
    '        '        listAccountSignatory = New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGNATORY)
    '        '    End If

    '        '    Dim dtAccountSignatory As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountSignatory)
    '        '    dtAccountSignatory.Columns.Add(New DataColumn("PK_Account_Signatory_ID", GetType(String)))
    '        '    'dtAccountSignatory.Columns.Add(New DataColumn("RoleName", GetType(String)))

    '        '    Using objdb As New NawaDevDAL.NawaDatadevEntities
    '        '        'Dim listRole = objdb.goAML_Ref_Peran_orang_dalam_rekening.ToList
    '        '        For Each row In dtAccountSignatory.Rows
    '        '            row("PK_Account_Signatory_ID") = row("PK_ACT_ACC_SIGNATORY_ID")   'Karena pakai window yg sama, PK harus disalin
    '        '            'If Not IsDBNull(row("role")) Then
    '        '            '    Dim objRole = listRole.Where(Function(x) x.Kode = row("role")).FirstOrDefault
    '        '            '    If objRole IsNot Nothing Then
    '        '            '        row("RoleName") = objRole.Kode & " - " & objRole.Keterangan
    '        '            '    End If
    '        '            'End If
    '        '        Next
    '        '    End Using
    '        '    BindGridPanel(gp_Account_Signatory, dtAccountSignatory)
    '        '    'ElseIf Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount Then
    '        '    '    Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.PK_Act_Acc_Ent_Director_ID = Me.PK_PartySub_ID).FirstOrDefault
    '        '    '    If objCek Is Nothing Then
    '        '    '        objCek = New SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Ent_Director
    '        '    '        objCek.PK_Act_Acc_Ent_Director_ID = Me.PK_PartySub_ID
    '        '    '        Me.IsNewRecord = True
    '        '    '    Else
    '        '    '        Me.IsNewRecord = False
    '        '    '    End If

    '        '    '    With objCek
    '        '    '        .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
    '        '    '        .FK_Act_Acc_Entity_ID = Me.PK_Party_AccountEntity_ID    'Di sini bedanya dengan Director Entity Biasa

    '        '    '        If Not String.IsNullOrEmpty(cmb_Person_Gender.SelectedItemValue) Then
    '        '    '            .Gender = cmb_Person_Gender.SelectedItemValue
    '        '    '        End If
    '        '    '        .Title = txt_Person_Title.Value
    '        '    '        .Last_Name = txt_Person_LastName.Value
    '        '    '        If txt_Person_BirthDate.SelectedDate <> DateTime.MinValue Then
    '        '    '            .BirthDate = txt_Person_BirthDate.SelectedDate
    '        '    '        End If
    '        '    '        .Birth_Place = txt_Person_BirthPlace.Value
    '        '    '        .Mothers_Name = txt_Person_MotherName.Value
    '        '    '        .Alias = txt_Person_Alias.Value
    '        '    '        .SSN = txt_Person_SSN.Value
    '        '    '        .Passport_Number = txt_Person_PassportNumber.Value
    '        '    '        .Passport_Country = cmb_Person_PassportCountry.SelectedItemValue
    '        '    '        .Id_Number = txt_Person_IDNumber.Value
    '        '    '        .Nationality1 = cmb_Person_Nationality1.SelectedItemValue
    '        '    '        .Nationality2 = cmb_Person_Nationality2.SelectedItemValue
    '        '    '        .Nationality3 = cmb_Person_Nationality3.SelectedItemValue
    '        '    '        .Residence = cmb_Person_Residence.SelectedItemValue
    '        '    '        .Email = txt_Person_Email1.Value
    '        '    '        .email2 = txt_Person_Email2.Value
    '        '    '        .email3 = txt_Person_Email3.Value
    '        '    '        .email4 = txt_Person_Email4.Value
    '        '    '        .email5 = txt_Person_Email5.Value
    '        '    '        .Occupation = txt_Person_Occupation.Value
    '        '    '        .Employer_Name = txt_Person_EmployerName.Value
    '        '    '        If chk_Person_IsDecease.Checked Then
    '        '    '            .Deceased = True
    '        '    '            If Not txt_Person_DeceaseDate.SelectedDate = DateTime.MinValue Then
    '        '    '                .Deceased_Date = txt_Person_DeceaseDate.SelectedDate
    '        '    '            End If
    '        '    '        Else
    '        '    '            .Deceased = False
    '        '    '            .Deceased_Date = Nothing
    '        '    '        End If
    '        '    '        .Tax_Number = txt_Person_TaxNumber.Value
    '        '    '        .Tax_Reg_Number = chk_Person_IsPEP.Checked
    '        '    '        .Source_Of_Wealth = txt_Person_SourceOfWealth.Value
    '        '    '        .Comment = txt_Person_Comment.Value

    '        '    '        .role = cmb_Person_Director_Role.SelectedItemValue
    '        '    '    End With

    '        '    '    If Me.IsNewRecord Then
    '        '    '        objCek.Active = True
    '        '    '        objCek.CreatedDate = DateTime.Now
    '        '    '        objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '        '    '        objGoAML_Activity_Class.list_goAML_Act_Acc_Ent_Director.Add(objCek)
    '        '    '    Else
    '        '    '        objCek.LastUpdateDate = DateTime.Now
    '        '    '        objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
    '        '    '    End If

    '        '    '    'Bind to Grid Panel
    '        '    '    Dim listAccountDirector = objGoAML_Activity_Class.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = Me.PK_Party_AccountEntity_ID).ToList
    '        '    '    If listAccountDirector Is Nothing Then
    '        '    '        listAccountDirector = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Ent_Director)
    '        '    '    End If

    '        '    '    Dim dtAccountDirector As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountDirector)
    '        '    '    dtAccountDirector.Columns.Add(New DataColumn("PK_Account_Entity_Director_ID", GetType(String)))
    '        '    '    dtAccountDirector.Columns.Add(New DataColumn("RoleName", GetType(String)))

    '        '    '    Using objdb As New NawaDevDAL.NawaDatadevEntities
    '        '    '        Dim listRole = objdb.goAML_Ref_Peran_orang_dalam_Korporasi.ToList
    '        '    '        For Each row In dtAccountDirector.Rows
    '        '    '            row("PK_Account_Entity_Director_ID") = row("PK_Act_Acc_Ent_Director_ID")   'Karena pakai window yg sama, PK harus disalin
    '        '    '            If Not IsDBNull(row("role")) Then
    '        '    '                Dim objRole = listRole.Where(Function(x) x.Kode = row("role")).FirstOrDefault
    '        '    '                If objRole IsNot Nothing Then
    '        '    '                    row("RoleName") = objRole.Kode & " - " & objRole.Keterangan
    '        '    '                End If
    '        '    '            End If
    '        '    '        Next
    '        '    '    End Using
    '        '    '    BindGridPanel(gp_Account_Entity_Director, dtAccountDirector)
    '        '    'ElseIf Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity Then
    '        '    '    Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.PK_SIPESATGOAML_Act_Director_ID = Me.PK_PartySub_ID).FirstOrDefault
    '        '    '    If objCek Is Nothing Then
    '        '    '        objCek = New SIPESATGOAML_ACT_DIRECTOR
    '        '    '        objCek.PK_SIPESATGOAML_Act_Director_ID = Me.PK_PartySub_ID
    '        '    '        Me.IsNewRecord = True
    '        '    '    Else
    '        '    '        Me.IsNewRecord = False
    '        '    '    End If

    '        '    '    With objCek
    '        '    '        .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
    '        '    '        .FK_ReportParty_ID = Me.PK_Activity_ID
    '        '    '        .FK_Act_Entity_ID = Me.PK_Party_ID
    '        '    '        .FK_Sender_Information = enumPartyType._Entity

    '        '    '        If Not String.IsNullOrEmpty(cmb_Person_Gender.SelectedItemValue) Then
    '        '    '            .Gender = cmb_Person_Gender.SelectedItemValue
    '        '    '        End If
    '        '    '        .Title = txt_Person_Title.Value
    '        '    '        .Last_Name = txt_Person_LastName.Value
    '        '    '        If txt_Person_BirthDate.SelectedDate <> DateTime.MinValue Then
    '        '    '            .BirthDate = txt_Person_BirthDate.SelectedDate
    '        '    '        End If
    '        '    '        .Birth_Place = txt_Person_BirthPlace.Value
    '        '    '        .Mothers_Name = txt_Person_MotherName.Value
    '        '    '        .Alias = txt_Person_Alias.Value
    '        '    '        .SSN = txt_Person_SSN.Value
    '        '    '        .Passport_Number = txt_Person_PassportNumber.Value
    '        '    '        .Passport_Country = cmb_Person_PassportCountry.SelectedItemValue
    '        '    '        .Id_Number = txt_Person_IDNumber.Value
    '        '    '        .Nationality1 = cmb_Person_Nationality1.SelectedItemValue
    '        '    '        .Nationality2 = cmb_Person_Nationality2.SelectedItemValue
    '        '    '        .Nationality3 = cmb_Person_Nationality3.SelectedItemValue
    '        '    '        .Residence = cmb_Person_Residence.SelectedItemValue
    '        '    '        .Email = txt_Person_Email1.Value
    '        '    '        .email2 = txt_Person_Email2.Value
    '        '    '        .email3 = txt_Person_Email3.Value
    '        '    '        .email4 = txt_Person_Email4.Value
    '        '    '        .email5 = txt_Person_Email5.Value
    '        '    '        .Occupation = txt_Person_Occupation.Value
    '        '    '        .Employer_Name = txt_Person_EmployerName.Value
    '        '    '        If chk_Person_IsDecease.Checked Then
    '        '    '            .Deceased = True
    '        '    '            If Not txt_Person_DeceaseDate.SelectedDate = DateTime.MinValue Then
    '        '    '                .Deceased_Date = txt_Person_DeceaseDate.SelectedDate
    '        '    '            End If
    '        '    '        Else
    '        '    '            .Deceased = False
    '        '    '            .Deceased_Date = Nothing
    '        '    '        End If
    '        '    '        .Tax_Number = txt_Person_TaxNumber.Value
    '        '    '        .Tax_Reg_Number = chk_Person_IsPEP.Checked
    '        '    '        .Source_Of_Wealth = txt_Person_SourceOfWealth.Value
    '        '    '        .Comment = txt_Person_Comment.Value

    '        '    '        .role = cmb_Person_Director_Role.SelectedItemValue
    '        '    '    End With

    '        '    '    If Me.IsNewRecord Then
    '        '    '        objCek.Active = True
    '        '    '        objCek.CreatedDate = DateTime.Now
    '        '    '        objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '        '    '        objGoAML_Activity_Class.list_goAML_Act_Director.Add(objCek)
    '        '    '    Else
    '        '    '        objCek.LastUpdateDate = DateTime.Now
    '        '    '        objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
    '        '    '    End If

    '        '    '    'Bind to Grid Panel
    '        '    '    Dim listAccountDirector = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.FK_Act_Entity_ID = Me.PK_Party_ID).ToList
    '        '    '    If listAccountDirector Is Nothing Then
    '        '    '        listAccountDirector = New List(Of SIPESATGOAML_ACT_DIRECTOR)
    '        '    '    End If

    '        '    '    Dim dtAccountDirector As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountDirector)
    '        '    '    dtAccountDirector.Columns.Add(New DataColumn("PK_Entity_Director_ID", GetType(String)))
    '        '    '    dtAccountDirector.Columns.Add(New DataColumn("RoleName", GetType(String)))

    '        '    '    Using objdb As New NawaDevDAL.NawaDatadevEntities
    '        '    '        Dim listRole = objdb.goAML_Ref_Peran_orang_dalam_Korporasi.ToList
    '        '    '        For Each row In dtAccountDirector.Rows
    '        '    '            row("PK_Entity_Director_ID") = row("PK_SIPESATGOAML_Act_Director_ID")   'Karena pakai window yg sama, PK harus disalin
    '        '    '            If Not IsDBNull(row("role")) Then
    '        '    '                Dim objRole = listRole.Where(Function(x) x.Kode = row("role")).FirstOrDefault
    '        '    '                If objRole IsNot Nothing Then
    '        '    '                    row("RoleName") = objRole.Kode & " - " & objRole.Keterangan
    '        '    '                End If
    '        '    '            End If
    '        '    '        Next
    '        '    '    End Using
    '        '    '    BindGridPanel(gp_Entity_Director, dtAccountDirector)
    '        'End If

    '        'Close Window Person
    '        window_PersonParty.Hidden = True

    '        'Balikin lagi PartySubType jadi 0 agar pas load address gak error
    '        Me.PartySubType = 0
    '        Me.PK_PartySub_ID = 0
    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub btn_Person_Save_Click()
        Try
            ValidatePerson()

            If Me.PartyType = enumPartyType._Person Then
                Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Person.Where(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ID = Me.PK_Party_ID).FirstOrDefault
                If objCek Is Nothing Then
                    objCek = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON
                    objCek.PK_SIPESATGOAML_ACT_PERSON_ID = Me.PK_Party_ID
                    Me.IsNewRecord = True
                Else
                    Me.IsNewRecord = False
                End If

                With objCek
                    .FK_REPORT_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                    .FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID

                    If Not String.IsNullOrEmpty(cmb_Person_Gender.SelectedItemValue) Then
                        .GENDER = cmb_Person_Gender.SelectedItemValue
                    End If
                    .TITLE = txt_Person_Title.Value
                    .LAST_NAME = txt_Person_LastName.Value
                    If txt_Person_BirthDate.SelectedDate <> DateTime.MinValue Then
                        .BIRTHDATE = txt_Person_BirthDate.SelectedDate
                    End If
                    .BITH_PLACE = txt_Person_BirthPlace.Value
                    .MOTHERS_NAME = txt_Person_MotherName.Value
                    .ALIAS = txt_Person_Alias.Value
                    .SSN = txt_Person_SSN.Value
                    .PASSPORT_NUMBER = txt_Person_PassportNumber.Value
                    .PASSPORT_COUNTRY = cmb_Person_PassportCountry.SelectedItemValue
                    .ID_NUMBER = txt_Person_IDNumber.Value
                    .NATIONALITY1 = cmb_Person_Nationality1.SelectedItemValue
                    .NATIONALITY2 = cmb_Person_Nationality2.SelectedItemValue
                    .NATIONALITY3 = cmb_Person_Nationality3.SelectedItemValue
                    .RESIDENCE = cmb_Person_Residence.SelectedItemValue
                    .EMAIL = txt_Person_Email1.Value
                    .EMAIL2 = txt_Person_Email2.Value
                    .EMAIL3 = txt_Person_Email3.Value
                    .EMAIL4 = txt_Person_Email4.Value
                    .EMAIL5 = txt_Person_Email5.Value
                    .OCCUPATION = txt_Person_Occupation.Value
                    .EMPLOYER_NAME = txt_Person_EmployerName.Value
                    If chk_Person_IsDecease.Checked Then
                        .DECEASED = True
                        If Not txt_Person_DeceaseDate.SelectedDate = DateTime.MinValue Then
                            .DECEASED_DATE = txt_Person_DeceaseDate.SelectedDate
                        End If
                    Else
                        .DECEASED = False
                        .DECEASED_DATE = Nothing
                    End If
                    .TAX_NUMBER = txt_Person_TaxNumber.Value
                    .TAX_REG_NUMBER = chk_Person_IsPEP.Checked
                    .SOURCE_OF_WEALTH = txt_Person_SourceOfWealth.Value
                    .COMMENT = txt_Person_Comment.Value
                End With

                If Me.IsNewRecord Then
                    objCek.Active = True
                    objCek.CreatedDate = DateTime.Now
                    objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objGoAML_Activity_Class.list_goAML_Act_Person.Add(objCek)
                Else
                    objCek.LastUpdateDate = DateTime.Now
                    objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                End If

                Dim lngPK_Relationship As Long = 0
                If objGoAML_Activity_Class.list_goAML_Act_Person_Relationship.Count = 0 Then
                    If objGoAML_Activity_Class.list_goAML_Act_Person_Relationship.Count > 0 Then
                        lngPK_Relationship = objGoAML_Activity_Class.list_goAML_Act_Person_Relationship.Min(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_RELATIONSHIP_ID)
                        If lngPK_Relationship > 0 Then
                            lngPK_Relationship = 0
                        End If
                    Else
                        lngPK_Relationship = -1
                    End If
                    Dim IdPerson As Long = 0
                    Dim objPerson = objGoAML_Activity_Class.list_goAML_Act_Person.Where(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ID = Me.PK_Party_ID).FirstOrDefault
                    If objPerson IsNot Nothing Then
                        IdPerson = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID
                    End If



                    Dim ObjRelationship As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_RELATIONSHIP
                    ObjRelationship.CLIENT_NUMBER = TxtClientNumber_Relationship.Value
                    ObjRelationship.COMMENTS = TxtClientNumber_Relationship.Value
                    ObjRelationship.CreatedDate = DateTime.Now
                    ObjRelationship.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    If TxtValidFrom_Relationship.SelectedDate = DateTime.MinValue Then
                        ObjRelationship.VALID_FROM = Nothing
                    Else
                        ObjRelationship.VALID_FROM = TxtValidFrom_Relationship.Value
                    End If

                    If chk_IsApproxFrom_Relationship.Checked Then
                        ObjRelationship.IS_APPROX_FROM_DATE = True
                    Else
                        ObjRelationship.IS_APPROX_FROM_DATE = chk_IsApproxFrom_Relationship.Checked
                    End If


                    If TxtValidTo_Relationship.SelectedDate = DateTime.MinValue Then
                        ObjRelationship.VALID_TO = Nothing
                    Else
                        ObjRelationship.VALID_TO = TxtValidTo_Relationship.Value
                    End If

                    If chk_IsApproxFrom_Relationship.Checked Then
                        ObjRelationship.IS_APPROX_TO_DATE = True
                    Else
                        ObjRelationship.IS_APPROX_TO_DATE = chk_IsApproxFrom_Relationship.Checked
                    End If

                    ObjRelationship.PK_SIPESATGOAML_ACT_PERSON_RELATIONSHIP_ID = lngPK_Relationship
                    ObjRelationship.FK_ACT_PERSON_ID = IdPerson

                    objGoAML_Activity_Class.list_goAML_Act_Person_Relationship.Add(ObjRelationship)
                Else
                    For Each item In objGoAML_Activity_Class.list_goAML_Act_Person_Relationship
                        item.COMMENTS = txtComments_Relationship.Value
                        item.CLIENT_NUMBER = TxtClientNumber_Relationship.Value
                        If TxtValidFrom_Relationship.SelectedDate = DateTime.MinValue Then
                            item.VALID_FROM = Nothing
                        Else
                            item.VALID_FROM = TxtValidFrom_Relationship.Value
                        End If

                        If TxtValidTo_Relationship.SelectedDate = DateTime.MinValue Then
                            item.VALID_TO = Nothing
                        Else
                            item.VALID_TO = TxtValidTo_Relationship.Value
                        End If

                        If chk_IsApproxFrom_Relationship.Checked Then
                            item.IS_APPROX_FROM_DATE = True
                        Else
                            item.IS_APPROX_FROM_DATE = chk_IsApproxFrom_Relationship.Checked
                        End If

                        If chk_IsApproxTo_Relationship.Checked Then
                            item.IS_APPROX_TO_DATE = True
                        Else
                            item.IS_APPROX_TO_DATE = chk_IsApproxTo_Relationship.Checked
                        End If
                    Next
                End If
            Else
                Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_ID = Me.PK_PartySub_ID).FirstOrDefault
                If objCek Is Nothing Then
                    objCek = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR
                    objCek.PK_SIPESATGOAML_ACT_DIRECTOR_ID = Me.PK_PartySub_ID
                    Me.IsNewRecord = True
                Else
                    Me.IsNewRecord = False
                End If

                With objCek
                    .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                    .FK_ReportParty_ID = Me.PK_Activity_ID
                    .FK_Act_Entity_ID = Me.PK_Party_ID
                    .FK_Sender_Information = enumPartyType._Entity

                    If Not String.IsNullOrEmpty(cmb_Person_Gender.SelectedItemValue) Then
                        .Gender = cmb_Person_Gender.SelectedItemValue
                    End If
                    .Title = txt_Person_Title.Value
                    .Last_Name = txt_Person_LastName.Value
                    If txt_Person_BirthDate.SelectedDate <> DateTime.MinValue Then
                        .BirthDate = txt_Person_BirthDate.SelectedDate
                    End If
                    .Birth_Place = txt_Person_BirthPlace.Value
                    .Mothers_Name = txt_Person_MotherName.Value
                    .Alias = txt_Person_Alias.Value
                    .SSN = txt_Person_SSN.Value
                    .Passport_Number = txt_Person_PassportNumber.Value
                    .Passport_Country = cmb_Person_PassportCountry.SelectedItemValue
                    .Id_Number = txt_Person_IDNumber.Value
                    .Nationality1 = cmb_Person_Nationality1.SelectedItemValue
                    .Nationality2 = cmb_Person_Nationality2.SelectedItemValue
                    .Nationality3 = cmb_Person_Nationality3.SelectedItemValue
                    .Residence = cmb_Person_Residence.SelectedItemValue
                    .Email = txt_Person_Email1.Value
                    .email2 = txt_Person_Email2.Value
                    .email3 = txt_Person_Email3.Value
                    .email4 = txt_Person_Email4.Value
                    .email5 = txt_Person_Email5.Value
                    .Occupation = txt_Person_Occupation.Value
                    .Employer_Name = txt_Person_EmployerName.Value

                    .Deceased = False
                    .DECEASED_DATE = Nothing

                    .Tax_Number = txt_Person_TaxNumber.Value
                    .Tax_Reg_Number = chk_Person_IsPEP.Checked
                    .Source_Of_Wealth = txt_Person_SourceOfWealth.Value
                    .Comment = txt_Person_Comment.Value

                    .role = cmb_Person_Director_Role.SelectedItemValue
                End With

                If Me.IsNewRecord Then
                    objCek.Active = True
                    objCek.CreatedDate = DateTime.Now
                    objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objGoAML_Activity_Class.list_goAML_Act_Director.Add(objCek)
                Else
                    objCek.LastUpdateDate = DateTime.Now
                    objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                End If

                'Bind to Grid Panel
                Dim listAccountDirector = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.FK_ACT_ENTITY_ID = Me.PK_Party_ID).ToList
                If listAccountDirector Is Nothing Then
                    listAccountDirector = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR)
                End If

                Dim dtAccountDirector As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountDirector)
                dtAccountDirector.Columns.Add(New DataColumn("PK_Entity_Director_ID", GetType(String)))
                dtAccountDirector.Columns.Add(New DataColumn("RoleName", GetType(String)))

                Using objdb As New NawaDevDAL.NawaDatadevEntities
                    Dim listRole = objdb.goAML_Ref_Peran_orang_dalam_Korporasi.ToList
                    For Each row In dtAccountDirector.Rows
                        row("PK_Entity_Director_ID") = row("PK_SIPESATGOAML_ACT_DIRECTOR_ID")   'Karena pakai window yg sama, PK harus disalin
                        If Not IsDBNull(row("role")) Then
                            Dim objRole = listRole.Where(Function(x) x.Kode = row("role")).FirstOrDefault
                            If objRole IsNot Nothing Then
                                row("RoleName") = objRole.Kode & " - " & objRole.Keterangan
                            End If
                        End If
                    Next
                End Using
                BindGridPanel(gp_Entity_Director, dtAccountDirector)
            End If

            ''Load Party Short Information
            LoadActivityShortInfo()

            ''Close Window Person
            'window_PersonParty.Hidden = True

            ' 27 Dec 2022 Ari : Hide
            'If Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.Signatory Then
            '    Dim objCek = objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Where(Function(x) x.PK_ACT_ACC_SIGNATORY_ID = Me.PK_PartySub_ID).FirstOrDefault
            '    If objCek Is Nothing Then
            '        objCek = New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGNATORY
            '        objCek.PK_ACT_ACC_SIGNATORY_ID = Me.PK_PartySub_ID
            '        Me.IsNewRecord = True
            '    Else
            '        Me.IsNewRecord = False
            '    End If

            '    With objCek
            '        .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
            '        .FK_ACT_REPORTPARTYTYPE_ID = Me.PK_Activity_ID
            '        .FK_ACT_ACCOUNT_ID = Me.PK_Party_ID

            '        If Not String.IsNullOrEmpty(cmb_Person_Gender.SelectedItemValue) Then
            '            .Gender = cmb_Person_Gender.SelectedItemValue
            '        End If
            '        '.Title = txt_Person_Title.Value
            '        .Last_Name = txt_Person_LastName.Value
            '        If txt_Person_BirthDate.SelectedDate <> DateTime.MinValue Then
            '            .BirthDate = txt_Person_BirthDate.SelectedDate
            '        End If
            '        .Birth_Place = txt_Person_BirthPlace.Value
            '        '.Mothers_Name = txt_Person_MotherName.Value
            '        '.Alias = txt_Person_Alias.Value
            '        .SSN = txt_Person_SSN.Value
            '        .Passport_Number = txt_Person_PassportNumber.Value
            '        .Passport_Country = cmb_Person_PassportCountry.SelectedItemValue
            '        .Id_Number = txt_Person_IDNumber.Value
            '        .Nationality1 = cmb_Person_Nationality1.SelectedItemValue
            '        '.Nationality2 = cmb_Person_Nationality2.SelectedItemValue
            '        '.Nationality3 = cmb_Person_Nationality3.SelectedItemValue
            '        .Residence = cmb_Person_Residence.SelectedItemValue
            '        '.Email = txt_Person_Email1.Value
            '        '.email2 = txt_Person_Email2.Value
            '        '.email3 = txt_Person_Email3.Value
            '        '.email4 = txt_Person_Email4.Value
            '        '.email5 = txt_Person_Email5.Value
            '        .Occupation = txt_Person_Occupation.Value
            '        .Employer_Name = txt_Person_EmployerName.Value
            '        'If chk_Person_IsDecease.Checked Then
            '        '    .Deceased = True
            '        '    'If Not txt_Person_DeceaseDate.SelectedDate = DateTime.MinValue Then
            '        '    '    .Deceased_Date = txt_Person_DeceaseDate.SelectedDate
            '        '    'End If
            '        'Else
            '        '    .Deceased = False
            '        '    '.Deceased_Date = Nothing
            '        'End If
            '        '.Tax_Number = txt_Person_TaxNumber.Value
            '        'If chk_Person_IsPEP.Checked Then
            '        '    .TAX_REG_NUMBER = "Y"
            '        'Else
            '        '    .TAX_REG_NUMBER = "T"
            '        'End If
            '        .TAX_REG_NUMBER = chk_Person_IsPEP.Checked
            '        .SOURCE_OF_WEALTH = txt_Person_SourceOfWealth.Value
            '        '.Comment = txt_Person_Comment.Value

            '        '.isPrimary = chk_Person_AccountSignatory_IsPrimary.Checked
            '        '.role = cmb_Person_AccountSignatory_Role.SelectedItemValue
            '    End With

            '    If Me.IsNewRecord Then
            '        objCek.Active = True
            '        objCek.CreatedDate = DateTime.Now
            '        objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            '        objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Add(objCek)
            '    Else
            '        objCek.LastUpdateDate = DateTime.Now
            '        objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
            '    End If

            '    'Bind to Grid Panel
            '    Dim listAccountSignatory = objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Where(Function(x) x.FK_ACT_ACCOUNT_ID = Me.PK_Party_ID).ToList
            '    If listAccountSignatory Is Nothing Then
            '        listAccountSignatory = New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGNATORY)
            '    End If

            '    Dim dtAccountSignatory As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountSignatory)
            '    dtAccountSignatory.Columns.Add(New DataColumn("PK_Account_Signatory_ID", GetType(String)))
            '    'dtAccountSignatory.Columns.Add(New DataColumn("RoleName", GetType(String)))

            '    Using objdb As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
            '        'Dim listRole = objdb.goAML_Ref_Peran_orang_dalam_rekening.ToList
            '        For Each row In dtAccountSignatory.Rows
            '            row("PK_Account_Signatory_ID") = row("PK_ACT_ACC_SIGNATORY_ID")   'Karena pakai window yg sama, PK harus disalin
            '            'If Not IsDBNull(row("role")) Then
            '            '    Dim objRole = listRole.Where(Function(x) x.Kode = row("role")).FirstOrDefault
            '            '    If objRole IsNot Nothing Then
            '            '        row("RoleName") = objRole.Kode & " - " & objRole.Keterangan
            '            '    End If
            '            'End If
            '        Next
            '    End Using
            '    BindGridPanel(gp_Account_Signatory, dtAccountSignatory)
            '    'ElseIf Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount Then
            '    '    Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.PK_Act_Acc_Ent_Director_ID = Me.PK_PartySub_ID).FirstOrDefault
            '    '    If objCek Is Nothing Then
            '    '        objCek = New SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Ent_Director
            '    '        objCek.PK_Act_Acc_Ent_Director_ID = Me.PK_PartySub_ID
            '    '        Me.IsNewRecord = True
            '    '    Else
            '    '        Me.IsNewRecord = False
            '    '    End If

            '    '    With objCek
            '    '        .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
            '    '        .FK_Act_Acc_Entity_ID = Me.PK_Party_AccountEntity_ID    'Di sini bedanya dengan Director Entity Biasa

            '    '        If Not String.IsNullOrEmpty(cmb_Person_Gender.SelectedItemValue) Then
            '    '            .Gender = cmb_Person_Gender.SelectedItemValue
            '    '        End If
            '    '        .Title = txt_Person_Title.Value
            '    '        .Last_Name = txt_Person_LastName.Value
            '    '        If txt_Person_BirthDate.SelectedDate <> DateTime.MinValue Then
            '    '            .BirthDate = txt_Person_BirthDate.SelectedDate
            '    '        End If
            '    '        .Birth_Place = txt_Person_BirthPlace.Value
            '    '        .Mothers_Name = txt_Person_MotherName.Value
            '    '        .Alias = txt_Person_Alias.Value
            '    '        .SSN = txt_Person_SSN.Value
            '    '        .Passport_Number = txt_Person_PassportNumber.Value
            '    '        .Passport_Country = cmb_Person_PassportCountry.SelectedItemValue
            '    '        .Id_Number = txt_Person_IDNumber.Value
            '    '        .Nationality1 = cmb_Person_Nationality1.SelectedItemValue
            '    '        .Nationality2 = cmb_Person_Nationality2.SelectedItemValue
            '    '        .Nationality3 = cmb_Person_Nationality3.SelectedItemValue
            '    '        .Residence = cmb_Person_Residence.SelectedItemValue
            '    '        .Email = txt_Person_Email1.Value
            '    '        .email2 = txt_Person_Email2.Value
            '    '        .email3 = txt_Person_Email3.Value
            '    '        .email4 = txt_Person_Email4.Value
            '    '        .email5 = txt_Person_Email5.Value
            '    '        .Occupation = txt_Person_Occupation.Value
            '    '        .Employer_Name = txt_Person_EmployerName.Value
            '    '        If chk_Person_IsDecease.Checked Then
            '    '            .Deceased = True
            '    '            If Not txt_Person_DeceaseDate.SelectedDate = DateTime.MinValue Then
            '    '                .Deceased_Date = txt_Person_DeceaseDate.SelectedDate
            '    '            End If
            '    '        Else
            '    '            .Deceased = False
            '    '            .Deceased_Date = Nothing
            '    '        End If
            '    '        .Tax_Number = txt_Person_TaxNumber.Value
            '    '        .Tax_Reg_Number = chk_Person_IsPEP.Checked
            '    '        .Source_Of_Wealth = txt_Person_SourceOfWealth.Value
            '    '        .Comment = txt_Person_Comment.Value

            '    '        .role = cmb_Person_Director_Role.SelectedItemValue
            '    '    End With

            '    '    If Me.IsNewRecord Then
            '    '        objCek.Active = True
            '    '        objCek.CreatedDate = DateTime.Now
            '    '        objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            '    '        objGoAML_Activity_Class.list_goAML_Act_Acc_Ent_Director.Add(objCek)
            '    '    Else
            '    '        objCek.LastUpdateDate = DateTime.Now
            '    '        objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
            '    '    End If

            '    '    'Bind to Grid Panel
            '    '    Dim listAccountDirector = objGoAML_Activity_Class.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = Me.PK_Party_AccountEntity_ID).ToList
            '    '    If listAccountDirector Is Nothing Then
            '    '        listAccountDirector = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Ent_Director)
            '    '    End If

            '    '    Dim dtAccountDirector As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountDirector)
            '    '    dtAccountDirector.Columns.Add(New DataColumn("PK_Account_Entity_Director_ID", GetType(String)))
            '    '    dtAccountDirector.Columns.Add(New DataColumn("RoleName", GetType(String)))

            '    '    Using objdb As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
            '    '        Dim listRole = objdb.goAML_Ref_Peran_orang_dalam_Korporasi.ToList
            '    '        For Each row In dtAccountDirector.Rows
            '    '            row("PK_Account_Entity_Director_ID") = row("PK_Act_Acc_Ent_Director_ID")   'Karena pakai window yg sama, PK harus disalin
            '    '            If Not IsDBNull(row("role")) Then
            '    '                Dim objRole = listRole.Where(Function(x) x.Kode = row("role")).FirstOrDefault
            '    '                If objRole IsNot Nothing Then
            '    '                    row("RoleName") = objRole.Kode & " - " & objRole.Keterangan
            '    '                End If
            '    '            End If
            '    '        Next
            '    '    End Using
            '    '    BindGridPanel(gp_Account_Entity_Director, dtAccountDirector)
            '    'ElseIf Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity Then
            '    '    Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.PK_SIPESATGOAML_Act_Director_ID = Me.PK_PartySub_ID).FirstOrDefault
            '    '    If objCek Is Nothing Then
            '    '        objCek = New SIPESATGOAML_ACT_DIRECTOR
            '    '        objCek.PK_SIPESATGOAML_Act_Director_ID = Me.PK_PartySub_ID
            '    '        Me.IsNewRecord = True
            '    '    Else
            '    '        Me.IsNewRecord = False
            '    '    End If

            '    '    With objCek
            '    '        .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
            '    '        .FK_ReportParty_ID = Me.PK_Activity_ID
            '    '        .FK_Act_Entity_ID = Me.PK_Party_ID
            '    '        .FK_Sender_Information = enumPartyType._Entity

            '    '        If Not String.IsNullOrEmpty(cmb_Person_Gender.SelectedItemValue) Then
            '    '            .Gender = cmb_Person_Gender.SelectedItemValue
            '    '        End If
            '    '        .Title = txt_Person_Title.Value
            '    '        .Last_Name = txt_Person_LastName.Value
            '    '        If txt_Person_BirthDate.SelectedDate <> DateTime.MinValue Then
            '    '            .BirthDate = txt_Person_BirthDate.SelectedDate
            '    '        End If
            '    '        .Birth_Place = txt_Person_BirthPlace.Value
            '    '        .Mothers_Name = txt_Person_MotherName.Value
            '    '        .Alias = txt_Person_Alias.Value
            '    '        .SSN = txt_Person_SSN.Value
            '    '        .Passport_Number = txt_Person_PassportNumber.Value
            '    '        .Passport_Country = cmb_Person_PassportCountry.SelectedItemValue
            '    '        .Id_Number = txt_Person_IDNumber.Value
            '    '        .Nationality1 = cmb_Person_Nationality1.SelectedItemValue
            '    '        .Nationality2 = cmb_Person_Nationality2.SelectedItemValue
            '    '        .Nationality3 = cmb_Person_Nationality3.SelectedItemValue
            '    '        .Residence = cmb_Person_Residence.SelectedItemValue
            '    '        .Email = txt_Person_Email1.Value
            '    '        .email2 = txt_Person_Email2.Value
            '    '        .email3 = txt_Person_Email3.Value
            '    '        .email4 = txt_Person_Email4.Value
            '    '        .email5 = txt_Person_Email5.Value
            '    '        .Occupation = txt_Person_Occupation.Value
            '    '        .Employer_Name = txt_Person_EmployerName.Value
            '    '        If chk_Person_IsDecease.Checked Then
            '    '            .Deceased = True
            '    '            If Not txt_Person_DeceaseDate.SelectedDate = DateTime.MinValue Then
            '    '                .Deceased_Date = txt_Person_DeceaseDate.SelectedDate
            '    '            End If
            '    '        Else
            '    '            .Deceased = False
            '    '            .Deceased_Date = Nothing
            '    '        End If
            '    '        .Tax_Number = txt_Person_TaxNumber.Value
            '    '        .Tax_Reg_Number = chk_Person_IsPEP.Checked
            '    '        .Source_Of_Wealth = txt_Person_SourceOfWealth.Value
            '    '        .Comment = txt_Person_Comment.Value

            '    '        .role = cmb_Person_Director_Role.SelectedItemValue
            '    '    End With

            '    '    If Me.IsNewRecord Then
            '    '        objCek.Active = True
            '    '        objCek.CreatedDate = DateTime.Now
            '    '        objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            '    '        objGoAML_Activity_Class.list_goAML_Act_Director.Add(objCek)
            '    '    Else
            '    '        objCek.LastUpdateDate = DateTime.Now
            '    '        objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
            '    '    End If

            '    '    'Bind to Grid Panel
            '    '    Dim listAccountDirector = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.FK_Act_Entity_ID = Me.PK_Party_ID).ToList
            '    '    If listAccountDirector Is Nothing Then
            '    '        listAccountDirector = New List(Of SIPESATGOAML_ACT_DIRECTOR)
            '    '    End If

            '    '    Dim dtAccountDirector As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountDirector)
            '    '    dtAccountDirector.Columns.Add(New DataColumn("PK_Entity_Director_ID", GetType(String)))
            '    '    dtAccountDirector.Columns.Add(New DataColumn("RoleName", GetType(String)))

            '    '    Using objdb As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
            '    '        Dim listRole = objdb.goAML_Ref_Peran_orang_dalam_Korporasi.ToList
            '    '        For Each row In dtAccountDirector.Rows
            '    '            row("PK_Entity_Director_ID") = row("PK_SIPESATGOAML_Act_Director_ID")   'Karena pakai window yg sama, PK harus disalin
            '    '            If Not IsDBNull(row("role")) Then
            '    '                Dim objRole = listRole.Where(Function(x) x.Kode = row("role")).FirstOrDefault
            '    '                If objRole IsNot Nothing Then
            '    '                    row("RoleName") = objRole.Kode & " - " & objRole.Keterangan
            '    '                End If
            '    '            End If
            '    '        Next
            '    '    End Using
            '    '    BindGridPanel(gp_Entity_Director, dtAccountDirector)
            'End If

            'Close Window Person
            window_PersonParty.Hidden = True

            'Balikin lagi PartySubType jadi 0 agar pas load address gak error
            Me.PartySubType = 0
            Me.PK_PartySub_ID = 0
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Person_Back_Click()
        Try
            'Balikin lagi PartySubType jadi 0 agar pas load address gak error
            Me.PartySubType = 0
            Me.PK_PartySub_ID = 0
            window_PersonParty.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub chk_Person_IsDecease_Changed()
        Try
            If chk_Person_IsDecease.Checked Then
                txt_Person_DeceaseDate.Hidden = False
            Else
                txt_Person_DeceaseDate.Hidden = True
                txt_Person_DeceaseDate.Value = Nothing
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadPerson()
        Try
            If Me.PartySubType = 0 Then     'Level 1 (Person & Conductor)
                Dim objPerson = objGoAML_Activity_Class.list_goAML_Act_Person.Where(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ID = Me.PK_Party_ID).FirstOrDefault
                If objPerson IsNot Nothing Then
                    Me.PK_Party_ID = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID
                    With objPerson
                        If Not IsNothing(.GENDER) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Jenis_Kelamin", "Kode", .GENDER)
                            If drTemp IsNot Nothing Then
                                cmb_Person_Gender.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If

                        txt_Person_Title.Value = .TITLE
                        txt_Person_LastName.Value = .LAST_NAME

                        If .BIRTHDATE.HasValue Then
                            txt_Person_BirthDate.Value = .BIRTHDATE
                        End If

                        txt_Person_BirthPlace.Value = .BITH_PLACE
                        txt_Person_MotherName.Value = .MOTHERS_NAME
                        txt_Person_Alias.Value = .ALIAS
                        txt_Person_SSN.Value = .SSN
                        txt_Person_PassportNumber.Value = .PASSPORT_NUMBER

                        If Not IsNothing(.PASSPORT_COUNTRY) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .PASSPORT_COUNTRY)
                            If drTemp IsNot Nothing Then
                                cmb_Person_PassportCountry.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If

                        txt_Person_IDNumber.Value = .ID_NUMBER

                        If Not IsNothing(.NATIONALITY1) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .NATIONALITY1)
                            If drTemp IsNot Nothing Then
                                cmb_Person_Nationality1.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If
                        If Not IsNothing(.NATIONALITY2) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .NATIONALITY2)
                            If drTemp IsNot Nothing Then
                                cmb_Person_Nationality2.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If
                        If Not IsNothing(.NATIONALITY3) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .NATIONALITY3)
                            If drTemp IsNot Nothing Then
                                cmb_Person_Nationality3.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If
                        If Not IsNothing(.RESIDENCE) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .RESIDENCE)
                            If drTemp IsNot Nothing Then
                                cmb_Person_Residence.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If

                        txt_Person_Email1.Value = .EMAIL
                        txt_Person_Email2.Value = .EMAIL2
                        txt_Person_Email3.Value = .EMAIL3
                        txt_Person_Email4.Value = .EMAIL4
                        txt_Person_Email5.Value = .EMAIL5
                        txt_Person_Occupation.Value = .OCCUPATION
                        txt_Person_EmployerName.Value = .EMPLOYER_NAME

                        If Not IsNothing(.DECEASED) AndAlso .DECEASED Then
                            chk_Person_IsDecease.Checked = True
                            If .DECEASED_DATE.HasValue Then
                                txt_Person_DeceaseDate.Value = .DECEASED_DATE
                            End If
                            txt_Person_DeceaseDate.Hidden = False
                        Else
                            chk_Person_IsDecease.Checked = False
                            txt_Person_DeceaseDate.Value = Nothing
                            txt_Person_DeceaseDate.Hidden = True
                        End If

                        txt_Person_TaxNumber.Value = .TAX_NUMBER
                        If Not IsNothing(.TAX_REG_NUMBER) AndAlso .TAX_REG_NUMBER Then
                            chk_Person_IsPEP.Checked = True
                        Else
                            chk_Person_IsPEP.Checked = False
                        End If
                        txt_Person_SourceOfWealth.Value = .SOURCE_OF_WEALTH
                        txt_Person_Comment.Value = .COMMENT

                        'Address
                        Dim listAddress As List(Of SIPESATGOAML_ACT_PERSON_ADDRESS) = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Where(Function(x) x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID And x.ISEMPLOYER = False).ToList
                        If listAddress Is Nothing Then
                            listAddress = New List(Of SIPESATGOAML_ACT_PERSON_ADDRESS)
                        End If
                        Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
                        dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
                        dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
                        dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
                        Using objdb As New NawaDevDAL.NawaDatadevEntities
                            Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
                            Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                            For Each row In dtAddress.Rows
                                row("PK_Address_ID") = row("PK_SIPESATGOAML_Act_Person_Address_ID")
                                If Not IsDBNull(row("Address_Type")) Then
                                    Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
                                    If objAddressType IsNot Nothing Then
                                        row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
                                    End If
                                End If

                                If Not IsDBNull(row("Country_Code")) Then
                                    Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
                                    If objCountry IsNot Nothing Then
                                        row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                                    End If
                                End If
                            Next
                        End Using
                        BindGridPanel(gp_Person_Address, dtAddress)

                        'Phone
                        Dim listPhone As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_PHONE) = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Where(Function(x) x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID And x.ISEMPLOYER = False).ToList
                        If listPhone Is Nothing Then
                            listPhone = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_PHONE)
                        End If

                        Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
                        dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
                        dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
                        Using objdb As New NawaDevDAL.NawaDatadevEntities
                            Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
                            Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                            For Each row In dtPhone.Rows
                                row("PK_Phone_ID") = row("PK_SIPESATGOAML_Act_Person_Phone_ID")
                                If Not IsDBNull(row("tph_contact_type")) Then
                                    Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
                                    If objPhoneType IsNot Nothing Then
                                        row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
                                    End If
                                End If
                            Next
                        End Using
                        BindGridPanel(gp_Person_Phone, dtPhone)

                        'Employer Address
                        Dim listEmployerAddress As List(Of SIPESATGOAML_ACT_PERSON_ADDRESS) = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Where(Function(x) x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID And x.ISEMPLOYER = True).ToList
                        If listEmployerAddress Is Nothing Then
                            listEmployerAddress = New List(Of SIPESATGOAML_ACT_PERSON_ADDRESS)
                        End If

                        Dim dtEmployerAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerAddress)
                        dtEmployerAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
                        dtEmployerAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
                        dtEmployerAddress.Columns.Add(New DataColumn("Country", GetType(String)))
                        Using objdb As New NawaDevDAL.NawaDatadevEntities
                            Dim listEmployerAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
                            Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                            For Each row In dtEmployerAddress.Rows
                                row("PK_Address_ID") = row("PK_SIPESATGOAML_Act_Person_Address_ID")
                                If Not IsDBNull(row("Address_Type")) Then
                                    Dim objEmployerAddressType = listEmployerAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
                                    If objEmployerAddressType IsNot Nothing Then
                                        row("AddressType") = objEmployerAddressType.Kode & " - " & objEmployerAddressType.Keterangan
                                    End If
                                End If

                                If Not IsDBNull(row("Country_Code")) Then
                                    Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
                                    If objCountry IsNot Nothing Then
                                        row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                                    End If
                                End If
                            Next
                        End Using
                        BindGridPanel(gp_Person_EmployerAddress, dtEmployerAddress)

                        'Employer Phone
                        Dim listEmployerPhone As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_PHONE) = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Where(Function(x) x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID And x.ISEMPLOYER = True).ToList
                        If listEmployerPhone Is Nothing Then
                            listEmployerPhone = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_PHONE)
                        End If

                        Dim dtEmployerPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerPhone)
                        dtEmployerPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
                        dtEmployerPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
                        Using objdb As New NawaDevDAL.NawaDatadevEntities
                            Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
                            For Each row In dtEmployerPhone.Rows
                                row("PK_Phone_ID") = row("PK_SIPESATGOAML_Act_Person_Phone_ID")
                                If Not IsDBNull(row("tph_contact_type")) Then
                                    Dim objEmployerPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
                                    If objEmployerPhoneType IsNot Nothing Then
                                        row("ContactType") = objEmployerPhoneType.Kode & " - " & objEmployerPhoneType.Keterangan
                                    End If
                                End If
                            Next
                        End Using
                        BindGridPanel(gp_Person_EmployerPhone, dtEmployerPhone)


                        'Identification
                        Dim listIdentification As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION) = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID).ToList
                        If listIdentification Is Nothing Then
                            listIdentification = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION)
                        End If

                        Dim dtIdentification As DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
                        dtIdentification.Columns.Add(New DataColumn("PK_Identification_ID", GetType(String)))
                        dtIdentification.Columns.Add(New DataColumn("Country", GetType(String)))
                        Using objdb As New NawaDevDAL.NawaDatadevEntities
                            Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                            For Each row In dtIdentification.Rows
                                row("PK_Identification_ID") = row("PK_ACTIVITY_PERSON_IDENTIFICATION_ID")
                                If Not IsDBNull(row("Issued_Country")) Then
                                    Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Issued_Country")).FirstOrDefault
                                    If objCountry IsNot Nothing Then
                                        row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                                    End If
                                End If
                            Next
                        End Using
                        BindGridPanel(gp_Person_Identification, dtIdentification)

                        If objGoAML_Activity_Class.list_goAML_Act_Person_Relationship IsNot Nothing Then
                            For Each itemRelationship In objGoAML_Activity_Class.list_goAML_Act_Person_Relationship
                                txtComments_Relationship.Value = itemRelationship.COMMENTS
                                TxtClientNumber_Relationship.Value = itemRelationship.CLIENT_NUMBER
                                TxtValidFrom_Relationship.Value = itemRelationship.VALID_FROM
                                TxtValidTo_Relationship.Value = itemRelationship.VALID_TO
                                If itemRelationship.IS_APPROX_FROM_DATE IsNot Nothing Then
                                    chk_IsApproxFrom_Relationship.Checked = itemRelationship.IS_APPROX_FROM_DATE

                                Else
                                    chk_IsApproxFrom_Relationship.Checked = False
                                End If
                                If itemRelationship.IS_APPROX_TO_DATE IsNot Nothing Then
                                    chk_IsApproxTo_Relationship.Checked = itemRelationship.IS_APPROX_TO_DATE

                                Else
                                    chk_IsApproxTo_Relationship.Checked = False

                                End If
                            Next
                        End If

                        chk_Person_AccountSignatory_IsPrimary.Hidden = True
                        cmb_Person_AccountSignatory_Role.IsHidden = True
                        cmb_Person_Director_Role.IsHidden = True
                        txt_Person_Title.Focus()
                    End With
                End If
            Else
                LoadEntityDirector()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub LoadAccountSignatory()
    '    Try
    '        Dim objSignatory = objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Where(Function(x) x.PK_ACT_ACC_SIGNATORY_ID = Me.PK_PartySub_ID).FirstOrDefault
    '        If objSignatory IsNot Nothing Then
    '            With objSignatory
    '                If Not IsNothing(.Gender) Then
    '                    drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Jenis_Kelamin", "Kode", .GENDER)
    '                    If drTemp IsNot Nothing Then
    '                        cmb_Person_Gender.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                    End If
    '                End If

    '                'txt_Person_Title.Value = .Title
    '                txt_Person_LastName.Value = .Last_Name

    '                If .BirthDate.HasValue Then
    '                    txt_Person_BirthDate.Value = .BirthDate
    '                End If

    '                txt_Person_BirthPlace.Value = .BIRTH_PLACE
    '                'txt_Person_MotherName.Value = .Mothers_Name
    '                'txt_Person_Alias.Value = .Alias
    '                txt_Person_SSN.Value = .SSN
    '                txt_Person_PassportNumber.Value = .Passport_Number

    '                If Not IsNothing(.Passport_Country) Then
    '                    drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .PASSPORT_COUNTRY)
    '                    If drTemp IsNot Nothing Then
    '                        cmb_Person_PassportCountry.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                    End If
    '                End If

    '                txt_Person_IDNumber.Value = .Id_Number

    '                If Not IsNothing(.Nationality1) Then
    '                    drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .NATIONALITY1)
    '                    If drTemp IsNot Nothing Then
    '                        cmb_Person_Nationality1.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                    End If
    '                End If
    '                'If Not IsNothing(.Nationality2) Then
    '                '    drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality2)
    '                '    If drTemp IsNot Nothing Then
    '                '        cmb_Person_Nationality2.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                '    End If
    '                'End If
    '                'If Not IsNothing(.Nationality3) Then
    '                '    drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality3)
    '                '    If drTemp IsNot Nothing Then
    '                '        cmb_Person_Nationality3.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                '    End If
    '                'End If
    '                If Not IsNothing(.Residence) Then
    '                    drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .RESIDENCE)
    '                    If drTemp IsNot Nothing Then
    '                        cmb_Person_Residence.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                    End If
    '                End If

    '                'txt_Person_Email1.Value = .EMAIL
    '                'txt_Person_Email2.Value = .email2
    '                'txt_Person_Email3.Value = .email3
    '                'txt_Person_Email4.Value = .email4
    '                'txt_Person_Email5.Value = .email5
    '                txt_Person_Occupation.Value = .Occupation
    '                txt_Person_EmployerName.Value = .EMPLOYER_NAME

    '                'If Not IsNothing(.Deceased) AndAlso .Deceased Then
    '                '    chk_Person_IsDecease.Checked = True
    '                '    'If .Deceased_Date.HasValue Then
    '                '    '    txt_Person_DeceaseDate.Value = .Deceased_Date
    '                '    'End If
    '                '    'txt_Person_DeceaseDate.Hidden = False
    '                'Else
    '                '    chk_Person_IsDecease.Checked = False
    '                '    'txt_Person_DeceaseDate.Value = Nothing
    '                '    'txt_Person_DeceaseDate.Hidden = True
    '                'End If

    '                'txt_Person_TaxNumber.Value = .Tax_Number
    '                If Not IsNothing(.Tax_Reg_Number) AndAlso .Tax_Reg_Number Then
    '                    chk_Person_IsPEP.Checked = True
    '                Else
    '                    chk_Person_IsPEP.Checked = False
    '                End If
    '                txt_Person_SourceOfWealth.Value = .SOURCE_OF_WEALTH
    '                'txt_Person_Comment.Value = .Comment

    '                'If Not IsNothing(.isPrimary) And .isPrimary Then
    '                '    chk_Person_AccountSignatory_IsPrimary.Checked = True
    '                'Else
    '                '    chk_Person_AccountSignatory_IsPrimary.Checked = False
    '                'End If

    '                'If Not IsNothing(.role) Then
    '                '    drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Peran_orang_dalam_rekening", "Kode", .ROLE)
    '                '    If drTemp IsNot Nothing Then
    '                '        cmb_Person_AccountSignatory_Role.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                '    End If
    '                'End If

    '                'Address
    '                Dim listAddress As List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_ADDRESS) = objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = objSignatory.PK_ACT_ACC_SIGNATORY_ID And x.ISEMPLOYER = False).ToList
    '                If listAddress Is Nothing Then
    '                    listAddress = New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_ADDRESS)
    '                End If
    '                Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
    '                dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
    '                dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
    '                dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
    '                Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                    Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                    Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                    For Each row In dtAddress.Rows
    '                        row("PK_Address_ID") = row("PK_ACT_ACC_SIGN_ADDRESS_ID")
    '                        If Not IsDBNull(row("Address_Type")) Then
    '                            Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
    '                            If objAddressType IsNot Nothing Then
    '                                row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
    '                            End If
    '                        End If

    '                        If Not IsDBNull(row("Country_Code")) Then
    '                            Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
    '                            If objCountry IsNot Nothing Then
    '                                row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                            End If
    '                        End If
    '                    Next
    '                End Using
    '                BindGridPanel(gp_Person_Address, dtAddress)

    '                'Phone
    '                Dim listPhone As List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_PHONE) = objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = objSignatory.PK_ACT_ACC_SIGNATORY_ID And x.ISEMPLOYER = False).ToList
    '                If listPhone Is Nothing Then
    '                    listPhone = New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_PHONE)
    '                End If

    '                Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
    '                dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
    '                dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
    '                dtPhone.Columns.Add(New DataColumn("CommunicationType", GetType(String)))

    '                Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                    Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                    Dim listCommunicationType = objdb.goAML_Ref_Jenis_Alat_Komunikasi.ToList
    '                    'Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                    For Each row In dtPhone.Rows
    '                        row("PK_Phone_ID") = row("PK_ACT_ACC_SIGN_PHONE_ID")
    '                        If Not IsDBNull(row("tph_contact_type")) Then
    '                            Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
    '                            If objPhoneType IsNot Nothing Then
    '                                row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
    '                            End If
    '                        End If
    '                        If Not IsDBNull(row("TPH_COMMUNICATION_TYPE")) Then
    '                            Dim objCommunicationType = listCommunicationType.Where(Function(x) x.Kode = row("TPH_COMMUNICATION_TYPE")).FirstOrDefault
    '                            If objCommunicationType IsNot Nothing Then
    '                                row("CommunicationType") = objCommunicationType.Kode & " - " & objCommunicationType.Keterangan
    '                            End If
    '                        End If
    '                    Next
    '                End Using
    '                BindGridPanel(gp_Person_Phone, dtPhone)


    '                'Employer Address
    '                'Dim listEmployerAddress As List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_ADDRESS) = objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = objSignatory.PK_ACT_ACC_SIGNATORY_ID And x.ISEMPLOYER = True).ToList
    '                'If listEmployerAddress Is Nothing Then
    '                '    listEmployerAddress = New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_ADDRESS)
    '                'End If

    '                'Dim dtEmployerAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerAddress)
    '                'dtEmployerAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
    '                'dtEmployerAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
    '                'dtEmployerAddress.Columns.Add(New DataColumn("Country", GetType(String)))
    '                'Using objdb As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
    '                '    Dim listEmployerAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                '    Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                '    For Each row In dtEmployerAddress.Rows
    '                '        row("PK_Address_ID") = row("PK_SIPESATGOAML_Act_Acc_Entity_Address_ID")
    '                '        If Not IsDBNull(row("Address_Type")) Then
    '                '            Dim objEmployerAddressType = listEmployerAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
    '                '            If objEmployerAddressType IsNot Nothing Then
    '                '                row("AddressType") = objEmployerAddressType.Kode & " - " & objEmployerAddressType.Keterangan
    '                '            End If
    '                '        End If

    '                '        If Not IsDBNull(row("Country_Code")) Then
    '                '            Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
    '                '            If objCountry IsNot Nothing Then
    '                '                row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                '            End If
    '                '        End If
    '                '    Next
    '                'End Using
    '                'BindGridPanel(gp_Person_EmployerAddress, dtEmployerAddress)

    '                'Employer Phone
    '                'Dim listEmployerPhone As List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_PHONE) = objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = objSignatory.PK_ACT_ACC_SIGNATORY_ID And x.ISEMPLOYER = True).ToList
    '                'If listEmployerPhone Is Nothing Then
    '                '    listEmployerPhone = New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_PHONE)
    '                'End If

    '                'Dim dtEmployerPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerPhone)
    '                'dtEmployerPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
    '                'dtEmployerPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
    '                'Using objdb As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
    '                '    Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                '    For Each row In dtEmployerPhone.Rows
    '                '        row("PK_Phone_ID") = row("PK_SIPESATGOAML_act_acc_sign_Phone_ID")
    '                '        If Not IsDBNull(row("tph_contact_type")) Then
    '                '            Dim objEmployerPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
    '                '            If objEmployerPhoneType IsNot Nothing Then
    '                '                row("ContactType") = objEmployerPhoneType.Kode & " - " & objEmployerPhoneType.Keterangan
    '                '            End If
    '                '        End If
    '                '    Next
    '                'End Using
    '                'BindGridPanel(gp_Person_EmployerPhone, dtEmployerPhone)

    '                'Identification
    '                Dim listIdentification As List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION) = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_ACT_ACC_SIGN_ID = objSignatory.PK_ACT_ACC_SIGNATORY_ID).ToList
    '                If listIdentification Is Nothing Then
    '                    listIdentification = New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION)
    '                End If

    '                Dim dtIdentification As DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
    '                dtIdentification.Columns.Add(New DataColumn("PK_Identification_ID", GetType(String)))
    '                dtIdentification.Columns.Add(New DataColumn("Country", GetType(String)))
    '                Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                    Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                    For Each row In dtIdentification.Rows
    '                        row("PK_Identification_ID") = row("PK_ACTIVITY_PERSON_IDENTIFICATION_ID")
    '                        If Not IsDBNull(row("Issued_Country")) Then
    '                            Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Issued_Country")).FirstOrDefault
    '                            If objCountry IsNot Nothing Then
    '                                row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                            End If
    '                        End If
    '                    Next
    '                End Using
    '                BindGridPanel(gp_Person_Identification, dtIdentification)

    '                'chk_Person_AccountSignatory_IsPrimary.Hidden = False
    '                'cmb_Person_AccountSignatory_Role.IsHidden = False
    '                'cmb_Person_Director_Role.IsHidden = True
    '                'txt_Person_Title.Focus()
    '            End With
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    '    Protected Sub LoadAccountEntityDirector()
    '        Try
    '            If Me.TRN_or_ACT = "TRN" Then   'Transaction
    '                If Me.BiParty_or_MultiParty = 1 Then    'Bi-Party
    '                    Dim objDirector = objGoAML_Transaction_Class.list_goAML_Trn_Director.Where(Function(x) x.PK_SIPESATGOAML_Trn_Director_ID = Me.PK_PartySub_ID).FirstOrDefault
    '                    If objDirector IsNot Nothing Then
    '                        With objDirector
    '                            If Not IsNothing(.Gender) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Jenis_Kelamin", "Kode", .Gender)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_Gender.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If

    '                            txt_Person_Title.Value = .Title
    '                            txt_Person_LastName.Value = .Last_Name

    '                            If .BirthDate.HasValue Then
    '                                txt_Person_BirthDate.Value = .BirthDate
    '                            End If

    '                            txt_Person_BirthPlace.Value = .Birth_Place
    '                            txt_Person_MotherName.Value = .Mothers_Name
    '                            txt_Person_Alias.Value = .Alias
    '                            txt_Person_SSN.Value = .SSN
    '                            txt_Person_PassportNumber.Value = .Passport_Number

    '                            If Not IsNothing(.Passport_Country) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Passport_Country)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_PassportCountry.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If

    '                            txt_Person_IDNumber.Value = .Id_Number

    '                            If Not IsNothing(.Nationality1) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality1)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_Nationality1.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If
    '                            If Not IsNothing(.Nationality2) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality2)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_Nationality2.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If
    '                            If Not IsNothing(.Nationality3) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality3)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_Nationality3.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If
    '                            If Not IsNothing(.Residence) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Residence)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_Residence.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If

    '                            txt_Person_Email1.Value = .Email
    '                            txt_Person_Email2.Value = .email2
    '                            txt_Person_Email3.Value = .email3
    '                            txt_Person_Email4.Value = .email4
    '                            txt_Person_Email5.Value = .email5
    '                            txt_Person_Occupation.Value = .Occupation
    '                            txt_Person_EmployerName.Value = .Employer_Name

    '                            If Not IsNothing(.Deceased) AndAlso .Deceased Then
    '                                chk_Person_IsDecease.Checked = True
    '                                If .Deceased_Date.HasValue Then
    '                                    txt_Person_DeceaseDate.Value = .Deceased_Date
    '                                End If
    '                                txt_Person_DeceaseDate.Hidden = False
    '                            Else
    '                                chk_Person_IsDecease.Checked = False
    '                                txt_Person_DeceaseDate.Value = Nothing
    '                                txt_Person_DeceaseDate.Hidden = True
    '                            End If

    '                            txt_Person_TaxNumber.Value = .Tax_Number
    '                            If Not IsNothing(.Tax_Reg_Number) AndAlso .Tax_Reg_Number Then
    '                                chk_Person_IsPEP.Checked = True
    '                            Else
    '                                chk_Person_IsPEP.Checked = False
    '                            End If
    '                            txt_Person_SourceOfWealth.Value = .Source_Of_Wealth
    '                            txt_Person_Comment.Value = .Comment

    '                            If Not IsNothing(.role) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Peran_orang_dalam_Korporasi", "Kode", .role)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_Director_Role.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If

    '                            'Address
    '                            Dim listAddress As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Director_Address) = objGoAML_Transaction_Class.list_goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = objDirector.PK_SIPESATGOAML_Trn_Director_ID And x.isEmployer = False).ToList
    '                            If listAddress Is Nothing Then
    '                                listAddress = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Director_Address)
    '                            End If
    '                            Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
    '                            dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
    '                            dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
    '                            dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
    '                            Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                                Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                                Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                                For Each row In dtAddress.Rows
    '                                    row("PK_Address_ID") = row("PK_SIPESATGOAML_Trn_Director_Address_ID")
    '                                    If Not IsDBNull(row("Address_Type")) Then
    '                                        Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
    '                                        If objAddressType IsNot Nothing Then
    '                                            row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
    '                                        End If
    '                                    End If

    '                                    If Not IsDBNull(row("Country_Code")) Then
    '                                        Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
    '                                        If objCountry IsNot Nothing Then
    '                                            row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                                        End If
    '                                    End If
    '                                Next
    '                            End Using
    '                            BindGridPanel(gp_Person_Address, dtAddress)

    '                            'Phone
    '                            Dim listPhone As List(Of SIPESATGOAMLBLL.SIPESATGOAML_trn_Director_Phone) = objGoAML_Transaction_Class.list_goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = objDirector.PK_SIPESATGOAML_Trn_Director_ID And x.isEmployer = False).ToList
    '                            If listPhone Is Nothing Then
    '                                listPhone = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_trn_Director_Phone)
    '                            End If

    '                            Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
    '                            dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
    '                            dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
    '                            Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                                Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                                Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                                For Each row In dtPhone.Rows
    '                                    row("PK_Phone_ID") = row("PK_SIPESATGOAML_trn_Director_Phone")
    '                                    If Not IsDBNull(row("tph_contact_type")) Then
    '                                        Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
    '                                        If objPhoneType IsNot Nothing Then
    '                                            row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
    '                                        End If
    '                                    End If
    '                                Next
    '                            End Using
    '                            BindGridPanel(gp_Person_Phone, dtPhone)

    '                            'Employer Address
    '                            Dim listEmployerAddress As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Director_Address) = objGoAML_Transaction_Class.list_goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = objDirector.PK_SIPESATGOAML_Trn_Director_ID And x.isEmployer = True).ToList
    '                            If listEmployerAddress Is Nothing Then
    '                                listEmployerAddress = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Director_Address)
    '                            End If

    '                            Dim dtEmployerAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerAddress)
    '                            dtEmployerAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
    '                            dtEmployerAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
    '                            dtEmployerAddress.Columns.Add(New DataColumn("Country", GetType(String)))
    '                            Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                                Dim listEmployerAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                                Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                                For Each row In dtEmployerAddress.Rows
    '                                    row("PK_Address_ID") = row("PK_SIPESATGOAML_Trn_Director_Address_ID")
    '                                    If Not IsDBNull(row("Address_Type")) Then
    '                                        Dim objEmployerAddressType = listEmployerAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
    '                                        If objEmployerAddressType IsNot Nothing Then
    '                                            row("AddressType") = objEmployerAddressType.Kode & " - " & objEmployerAddressType.Keterangan
    '                                        End If
    '                                    End If

    '                                    If Not IsDBNull(row("Country_Code")) Then
    '                                        Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
    '                                        If objCountry IsNot Nothing Then
    '                                            row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                                        End If
    '                                    End If
    '                                Next
    '                            End Using
    '                            BindGridPanel(gp_Person_EmployerAddress, dtEmployerAddress)

    '                            'Employer Phone
    '                            Dim listEmployerPhone As List(Of SIPESATGOAMLBLL.SIPESATGOAML_trn_Director_Phone) = objGoAML_Transaction_Class.list_goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = objDirector.PK_SIPESATGOAML_Trn_Director_ID And x.isEmployer = True).ToList
    '                            If listEmployerPhone Is Nothing Then
    '                                listEmployerPhone = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_trn_Director_Phone)
    '                            End If

    '                            Dim dtEmployerPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerPhone)
    '                            dtEmployerPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
    '                            dtEmployerPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
    '                            Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                                Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                                For Each row In dtEmployerPhone.Rows
    '                                    row("PK_Phone_ID") = row("PK_SIPESATGOAML_trn_Director_Phone")
    '                                    If Not IsDBNull(row("tph_contact_type")) Then
    '                                        Dim objEmployerPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
    '                                        If objEmployerPhoneType IsNot Nothing Then
    '                                            row("ContactType") = objEmployerPhoneType.Kode & " - " & objEmployerPhoneType.Keterangan
    '                                        End If
    '                                    End If
    '                                Next
    '                            End Using
    '                            BindGridPanel(gp_Person_EmployerPhone, dtEmployerPhone)

    '                            'Identification
    '                            Dim listIdentification As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Transaction_Person_Identification) = objGoAML_Transaction_Class.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount And x.FK_Person_ID = objDirector.PK_SIPESATGOAML_Trn_Director_ID).ToList
    '                            If listIdentification Is Nothing Then
    '                                listIdentification = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Transaction_Person_Identification)
    '                            End If

    '                            Dim dtIdentification As DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
    '                            dtIdentification.Columns.Add(New DataColumn("PK_Identification_ID", GetType(String)))
    '                            dtIdentification.Columns.Add(New DataColumn("Country", GetType(String)))
    '                            Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                                Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                                For Each row In dtIdentification.Rows
    '                                    row("PK_Identification_ID") = row("PK_SIPESATGOAML_Transaction_Person_Identification_ID")
    '                                    If Not IsDBNull(row("Issued_Country")) Then
    '                                        Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Issued_Country")).FirstOrDefault
    '                                        If objCountry IsNot Nothing Then
    '                                            row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                                        End If
    '                                    End If
    '                                Next
    '                            End Using
    '                            BindGridPanel(gp_Person_Identification, dtIdentification)

    '                            chk_Person_AccountSignatory_IsPrimary.Hidden = True
    '                            cmb_Person_AccountSignatory_Role.IsHidden = True
    '                            cmb_Person_Director_Role.IsHidden = False
    '                            txt_Person_Title.Focus()
    '                        End With
    '                    End If
    '                Else    'Multi-Party
    '                    Dim objDirector = objGoAML_Transaction_Class.list_goAML_Trn_Par_Acc_Ent_Director.Where(Function(x) x.PK_Trn_Par_Acc_Ent_Director_ID = Me.PK_PartySub_ID).FirstOrDefault
    '                    If objDirector IsNot Nothing Then
    '                        With objDirector
    '                            If Not IsNothing(.Gender) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Jenis_Kelamin", "Kode", .Gender)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_Gender.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If

    '                            txt_Person_Title.Value = .Title
    '                            txt_Person_LastName.Value = .Last_Name

    '                            If .BirthDate.HasValue Then
    '                                txt_Person_BirthDate.Value = .BirthDate
    '                            End If

    '                            txt_Person_BirthPlace.Value = .Birth_Place
    '                            txt_Person_MotherName.Value = .Mothers_Name
    '                            txt_Person_Alias.Value = .Alias
    '                            txt_Person_SSN.Value = .SSN
    '                            txt_Person_PassportNumber.Value = .Passport_Number

    '                            If Not IsNothing(.Passport_Country) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Passport_Country)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_PassportCountry.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If

    '                            txt_Person_IDNumber.Value = .Id_Number

    '                            If Not IsNothing(.Nationality1) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality1)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_Nationality1.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If
    '                            If Not IsNothing(.Nationality2) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality2)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_Nationality2.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If
    '                            If Not IsNothing(.Nationality3) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality3)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_Nationality3.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If
    '                            If Not IsNothing(.Residence) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Residence)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_Residence.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If

    '                            txt_Person_Email1.Value = .Email
    '                            txt_Person_Email2.Value = .email2
    '                            txt_Person_Email3.Value = .email3
    '                            txt_Person_Email4.Value = .email4
    '                            txt_Person_Email5.Value = .email5
    '                            txt_Person_Occupation.Value = .Occupation
    '                            txt_Person_EmployerName.Value = .Employer_Name

    '                            If Not IsNothing(.Deceased) AndAlso .Deceased Then
    '                                chk_Person_IsDecease.Checked = True
    '                                If .Deceased_Date.HasValue Then
    '                                    txt_Person_DeceaseDate.Value = .Deceased_Date
    '                                End If
    '                                txt_Person_DeceaseDate.Hidden = False
    '                            Else
    '                                chk_Person_IsDecease.Checked = False
    '                                txt_Person_DeceaseDate.Value = Nothing
    '                                txt_Person_DeceaseDate.Hidden = True
    '                            End If

    '                            txt_Person_TaxNumber.Value = .Tax_Number
    '                            If Not IsNothing(.Tax_Reg_Number) AndAlso .Tax_Reg_Number Then
    '                                chk_Person_IsPEP.Checked = True
    '                            Else
    '                                chk_Person_IsPEP.Checked = False
    '                            End If
    '                            txt_Person_SourceOfWealth.Value = .Source_Of_Wealth
    '                            txt_Person_Comment.Value = .Comment

    '                            If Not IsNothing(.role) Then
    '                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Peran_orang_dalam_Korporasi", "Kode", .role)
    '                                If drTemp IsNot Nothing Then
    '                                    cmb_Person_Director_Role.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                                End If
    '                            End If

    '                            'Address
    '                            Dim listAddress As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Par_Acc_Ent_Director_Address) = objGoAML_Transaction_Class.list_goAML_Trn_Par_Acc_Ent_Director_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID And x.isemployer = False).ToList
    '                            If listAddress Is Nothing Then
    '                                listAddress = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Par_Acc_Ent_Director_Address)
    '                            End If
    '                            Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
    '                            dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
    '                            dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
    '                            dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
    '                            Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                                Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                                Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                                For Each row In dtAddress.Rows
    '                                    row("PK_Address_ID") = row("PK_Trn_Par_Acc_Entity_Address_ID")
    '                                    If Not IsDBNull(row("Address_Type")) Then
    '                                        Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
    '                                        If objAddressType IsNot Nothing Then
    '                                            row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
    '                                        End If
    '                                    End If

    '                                    If Not IsDBNull(row("Country_Code")) Then
    '                                        Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
    '                                        If objCountry IsNot Nothing Then
    '                                            row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                                        End If
    '                                    End If
    '                                Next
    '                            End Using
    '                            BindGridPanel(gp_Person_Address, dtAddress)

    '                            'Phone
    '                            Dim listPhone As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Par_Acc_Ent_Director_Phone) = objGoAML_Transaction_Class.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID And x.isemployer = False).ToList
    '                            If listPhone Is Nothing Then
    '                                listPhone = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Par_Acc_Ent_Director_Phone)
    '                            End If

    '                            Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
    '                            dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
    '                            dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
    '                            Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                                Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                                Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                                For Each row In dtPhone.Rows
    '                                    row("PK_Phone_ID") = row("PK_Trn_Par_Acc_Ent_Director_Phone_ID")
    '                                    If Not IsDBNull(row("tph_contact_type")) Then
    '                                        Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
    '                                        If objPhoneType IsNot Nothing Then
    '                                            row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
    '                                        End If
    '                                    End If
    '                                Next
    '                            End Using
    '                            BindGridPanel(gp_Person_Phone, dtPhone)

    '                            'Employer Address
    '                            Dim listEmployerAddress As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Par_Acc_Ent_Director_Address) = objGoAML_Transaction_Class.list_goAML_Trn_Par_Acc_Ent_Director_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID And x.isemployer = True).ToList
    '                            If listEmployerAddress Is Nothing Then
    '                                listEmployerAddress = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Par_Acc_Ent_Director_Address)
    '                            End If

    '                            Dim dtEmployerAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerAddress)
    '                            dtEmployerAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
    '                            dtEmployerAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
    '                            dtEmployerAddress.Columns.Add(New DataColumn("Country", GetType(String)))
    '                            Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                                Dim listEmployerAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                                Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                                For Each row In dtEmployerAddress.Rows
    '                                    row("PK_Address_ID") = row("PK_Trn_Par_Acc_Entity_Address_ID")
    '                                    If Not IsDBNull(row("Address_Type")) Then
    '                                        Dim objEmployerAddressType = listEmployerAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
    '                                        If objEmployerAddressType IsNot Nothing Then
    '                                            row("AddressType") = objEmployerAddressType.Kode & " - " & objEmployerAddressType.Keterangan
    '                                        End If
    '                                    End If

    '                                    If Not IsDBNull(row("Country_Code")) Then
    '                                        Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
    '                                        If objCountry IsNot Nothing Then
    '                                            row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                                        End If
    '                                    End If
    '                                Next
    '                            End Using
    '                            BindGridPanel(gp_Person_EmployerAddress, dtEmployerAddress)

    '                            'Employer Phone
    '                            Dim listEmployerPhone As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Par_Acc_Ent_Director_Phone) = objGoAML_Transaction_Class.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID And x.isemployer = True).ToList
    '                            If listEmployerPhone Is Nothing Then
    '                                listEmployerPhone = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Par_Acc_Ent_Director_Phone)
    '                            End If

    '                            Dim dtEmployerPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerPhone)
    '                            dtEmployerPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
    '                            dtEmployerPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
    '                            Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                                Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                                For Each row In dtEmployerPhone.Rows
    '                                    row("PK_Phone_ID") = row("PK_Trn_Par_Acc_Ent_Director_Phone_ID")
    '                                    If Not IsDBNull(row("tph_contact_type")) Then
    '                                        Dim objEmployerPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
    '                                        If objEmployerPhoneType IsNot Nothing Then
    '                                            row("ContactType") = objEmployerPhoneType.Kode & " - " & objEmployerPhoneType.Keterangan
    '                                        End If
    '                                    End If
    '                                Next
    '                            End Using
    '                            BindGridPanel(gp_Person_EmployerPhone, dtEmployerPhone)

    '                            'Identification
    '                            Dim listIdentification As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Transaction_Party_Identification) = objGoAML_Transaction_Class.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount And x.FK_Person_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID).ToList
    '                            If listIdentification Is Nothing Then
    '                                listIdentification = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Transaction_Party_Identification)
    '                            End If

    '                            Dim dtIdentification As DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
    '                            dtIdentification.Columns.Add(New DataColumn("PK_Identification_ID", GetType(String)))
    '                            dtIdentification.Columns.Add(New DataColumn("Country", GetType(String)))
    '                            Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                                Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                                For Each row In dtIdentification.Rows
    '                                    row("PK_Identification_ID") = row("PK_Transaction_Party_Identification_ID")
    '                                    If Not IsDBNull(row("Issued_Country")) Then
    '                                        Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Issued_Country")).FirstOrDefault
    '                                        If objCountry IsNot Nothing Then
    '                                            row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                                        End If
    '                                    End If
    '                                Next
    '                            End Using
    '                            BindGridPanel(gp_Person_Identification, dtIdentification)

    '                            chk_Person_AccountSignatory_IsPrimary.Hidden = True
    '                            cmb_Person_AccountSignatory_Role.IsHidden = True
    '                            cmb_Person_Director_Role.IsHidden = False
    '                            txt_Person_Title.Focus()
    '                        End With
    '                    End If
    '                End If
    '            ElseIf Me.TRN_or_ACT = "ACT" Then    'Activity
    '                Dim objDirector = objGoAML_Activity_Class.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.PK_Act_Acc_Ent_Director_ID = Me.PK_PartySub_ID).FirstOrDefault
    '                If objDirector IsNot Nothing Then
    '                    With objDirector
    '                        If Not IsNothing(.Gender) Then
    '                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Jenis_Kelamin", "Kode", .Gender)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_Person_Gender.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                            End If
    '                        End If

    '                        txt_Person_Title.Value = .Title
    '                        txt_Person_LastName.Value = .Last_Name

    '                        If .BirthDate.HasValue Then
    '                            txt_Person_BirthDate.Value = .BirthDate
    '                        End If

    '                        txt_Person_BirthPlace.Value = .Birth_Place
    '                        txt_Person_MotherName.Value = .Mothers_Name
    '                        txt_Person_Alias.Value = .Alias
    '                        txt_Person_SSN.Value = .SSN
    '                        txt_Person_PassportNumber.Value = .Passport_Number

    '                        If Not IsNothing(.Passport_Country) Then
    '                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Passport_Country)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_Person_PassportCountry.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                            End If
    '                        End If

    '                        txt_Person_IDNumber.Value = .Id_Number

    '                        If Not IsNothing(.Nationality1) Then
    '                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality1)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_Person_Nationality1.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                            End If
    '                        End If
    '                        If Not IsNothing(.Nationality2) Then
    '                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality2)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_Person_Nationality2.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                            End If
    '                        End If
    '                        If Not IsNothing(.Nationality3) Then
    '                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality3)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_Person_Nationality3.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                            End If
    '                        End If
    '                        If Not IsNothing(.Residence) Then
    '                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Residence)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_Person_Residence.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                            End If
    '                        End If

    '                        txt_Person_Email1.Value = .Email
    '                        txt_Person_Email2.Value = .email2
    '                        txt_Person_Email3.Value = .email3
    '                        txt_Person_Email4.Value = .email4
    '                        txt_Person_Email5.Value = .email5
    '                        txt_Person_Occupation.Value = .Occupation
    '                        txt_Person_EmployerName.Value = .Employer_Name

    '                        If Not IsNothing(.Deceased) AndAlso .Deceased Then
    '                            chk_Person_IsDecease.Checked = True
    '                            If .Deceased_Date.HasValue Then
    '                                txt_Person_DeceaseDate.Value = .Deceased_Date
    '                            End If
    '                            txt_Person_DeceaseDate.Hidden = False
    '                        Else
    '                            chk_Person_IsDecease.Checked = False
    '                            txt_Person_DeceaseDate.Value = Nothing
    '                            txt_Person_DeceaseDate.Hidden = True
    '                        End If

    '                        txt_Person_TaxNumber.Value = .Tax_Number
    '                        If Not IsNothing(.Tax_Reg_Number) AndAlso .Tax_Reg_Number Then
    '                            chk_Person_IsPEP.Checked = True
    '                        Else
    '                            chk_Person_IsPEP.Checked = False
    '                        End If
    '                        txt_Person_SourceOfWealth.Value = .Source_Of_Wealth
    '                        txt_Person_Comment.Value = .Comment

    '                        If Not IsNothing(.role) Then
    '                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Peran_orang_dalam_Korporasi", "Kode", .role)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_Person_Director_Role.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                            End If
    '                        End If

    '                        'Address
    '                        Dim listAddress As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Entity_Director_address) = objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = objDirector.PK_Act_Acc_Ent_Director_ID And x.isEmployer = False).ToList
    '                        If listAddress Is Nothing Then
    '                            listAddress = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Entity_Director_address)
    '                        End If
    '                        Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
    '                        dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
    '                        dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
    '                        dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
    '                        Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                            Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                            Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                            For Each row In dtAddress.Rows
    '                                row("PK_Address_ID") = row("PK_SIPESATGOAML_Act_Acc_Entity_Director_address_ID")
    '                                If Not IsDBNull(row("Address_Type")) Then
    '                                    Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
    '                                    If objAddressType IsNot Nothing Then
    '                                        row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
    '                                    End If
    '                                End If

    '                                If Not IsDBNull(row("Country_Code")) Then
    '                                    Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
    '                                    If objCountry IsNot Nothing Then
    '                                        row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                                    End If
    '                                End If
    '                            Next
    '                        End Using
    '                        BindGridPanel(gp_Person_Address, dtAddress)

    '                        'Phone
    '                        Dim listPhone As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Entity_Director_Phone) = objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = objDirector.PK_Act_Acc_Ent_Director_ID And x.isEmployer = False).ToList
    '                        If listPhone Is Nothing Then
    '                            listPhone = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Entity_Director_Phone)
    '                        End If

    '                        Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
    '                        dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
    '                        dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
    '                        Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                            Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                            Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                            For Each row In dtPhone.Rows
    '                                row("PK_Phone_ID") = row("PK_SIPESATGOAML_Act_Acc_Entity_Director_Phone_ID")
    '                                If Not IsDBNull(row("tph_contact_type")) Then
    '                                    Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
    '                                    If objPhoneType IsNot Nothing Then
    '                                        row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
    '                                    End If
    '                                End If
    '                            Next
    '                        End Using
    '                        BindGridPanel(gp_Person_Phone, dtPhone)

    '                        'Employer Address
    '                        Dim listEmployerAddress As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Entity_Director_address) = objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = objDirector.PK_Act_Acc_Ent_Director_ID And x.isEmployer = True).ToList
    '                        If listEmployerAddress Is Nothing Then
    '                            listEmployerAddress = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Entity_Director_address)
    '                        End If

    '                        Dim dtEmployerAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerAddress)
    '                        dtEmployerAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
    '                        dtEmployerAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
    '                        dtEmployerAddress.Columns.Add(New DataColumn("Country", GetType(String)))
    '                        Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                            Dim listEmployerAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                            Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                            For Each row In dtEmployerAddress.Rows
    '                                row("PK_Address_ID") = row("PK_SIPESATGOAML_Act_Acc_Entity_Director_address_ID")
    '                                If Not IsDBNull(row("Address_Type")) Then
    '                                    Dim objEmployerAddressType = listEmployerAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
    '                                    If objEmployerAddressType IsNot Nothing Then
    '                                        row("AddressType") = objEmployerAddressType.Kode & " - " & objEmployerAddressType.Keterangan
    '                                    End If
    '                                End If

    '                                If Not IsDBNull(row("Country_Code")) Then
    '                                    Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
    '                                    If objCountry IsNot Nothing Then
    '                                        row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                                    End If
    '                                End If
    '                            Next
    '                        End Using
    '                        BindGridPanel(gp_Person_EmployerAddress, dtEmployerAddress)

    '                        'Employer Phone
    '                        Dim listEmployerPhone As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Entity_Director_Phone) = objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = objDirector.PK_Act_Acc_Ent_Director_ID And x.isEmployer = True).ToList
    '                        If listEmployerPhone Is Nothing Then
    '                            listEmployerPhone = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Act_Acc_Entity_Director_Phone)
    '                        End If

    '                        Dim dtEmployerPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerPhone)
    '                        dtEmployerPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
    '                        dtEmployerPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
    '                        Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                            Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                            For Each row In dtEmployerPhone.Rows
    '                                row("PK_Phone_ID") = row("PK_SIPESATGOAML_Act_Acc_Entity_Director_Phone_ID")
    '                                If Not IsDBNull(row("tph_contact_type")) Then
    '                                    Dim objEmployerPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
    '                                    If objEmployerPhoneType IsNot Nothing Then
    '                                        row("ContactType") = objEmployerPhoneType.Kode & " - " & objEmployerPhoneType.Keterangan
    '                                    End If
    '                                End If
    '                            Next
    '                        End Using
    '                        BindGridPanel(gp_Person_EmployerPhone, dtEmployerPhone)

    '                        'Identification
    '                        Dim listIdentification As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION) = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount And x.FK_Act_Person_ID = objDirector.PK_Act_Acc_Ent_Director_ID).ToList
    '                        If listIdentification Is Nothing Then
    '                            listIdentification = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION)
    '                        End If

    '                        Dim dtIdentification As DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
    '                        dtIdentification.Columns.Add(New DataColumn("PK_Identification_ID", GetType(String)))
    '                        dtIdentification.Columns.Add(New DataColumn("Country", GetType(String)))
    '                        Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                            Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                            For Each row In dtIdentification.Rows
    '                                row("PK_Identification_ID") = row("PK_SIPESATGOAML_Activity_Person_Identification_ID")
    '                                If Not IsDBNull(row("Issued_Country")) Then
    '                                    Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Issued_Country")).FirstOrDefault
    '                                    If objCountry IsNot Nothing Then
    '                                        row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                                    End If
    '                                End If
    '                            Next
    '                        End Using
    '                        BindGridPanel(gp_Person_Identification, dtIdentification)

    '                        chk_Person_AccountSignatory_IsPrimary.Hidden = True
    '                        cmb_Person_AccountSignatory_Role.IsHidden = True
    '                        cmb_Person_Director_Role.IsHidden = False
    '                        txt_Person_Title.Focus()
    '                    End With
    '                End If
    '            End If

    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub

    Protected Sub LoadEntityDirector()
        Try
            Dim objDirector = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_ID = Me.PK_PartySub_ID).FirstOrDefault
            If objDirector IsNot Nothing Then
                With objDirector
                    If Not IsNothing(.GENDER) Then
                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Jenis_Kelamin", "Kode", .GENDER)
                        If drTemp IsNot Nothing Then
                            cmb_Person_Gender.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                        End If
                    End If

                    txt_Person_Title.Value = .TITLE
                    txt_Person_LastName.Value = .LAST_NAME

                    If .BIRTHDATE.HasValue Then
                        txt_Person_BirthDate.Value = .BIRTHDATE
                    End If

                    txt_Person_BirthPlace.Value = .BIRTH_PLACE
                    txt_Person_MotherName.Value = .MOTHERS_NAME
                    txt_Person_Alias.Value = .ALIAS
                    txt_Person_SSN.Value = .SSN
                    txt_Person_PassportNumber.Value = .PASSPORT_NUMBER

                    If Not IsNothing(.PASSPORT_COUNTRY) Then
                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .PASSPORT_COUNTRY)
                        If drTemp IsNot Nothing Then
                            cmb_Person_PassportCountry.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                        End If
                    End If

                    txt_Person_IDNumber.Value = .ID_NUMBER

                    If Not IsNothing(.NATIONALITY1) Then
                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .NATIONALITY1)
                        If drTemp IsNot Nothing Then
                            cmb_Person_Nationality1.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                        End If
                    End If
                    If Not IsNothing(.NATIONALITY2) Then
                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .NATIONALITY2)
                        If drTemp IsNot Nothing Then
                            cmb_Person_Nationality2.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                        End If
                    End If
                    If Not IsNothing(.NATIONALITY3) Then
                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .NATIONALITY3)
                        If drTemp IsNot Nothing Then
                            cmb_Person_Nationality3.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                        End If
                    End If
                    If Not IsNothing(.RESIDENCE) Then
                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .RESIDENCE)
                        If drTemp IsNot Nothing Then
                            cmb_Person_Residence.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                        End If
                    End If

                    txt_Person_Email1.Value = .EMAIL
                    txt_Person_Email2.Value = .EMAIL2
                    txt_Person_Email3.Value = .EMAIL3
                    txt_Person_Email4.Value = .EMAIL4
                    txt_Person_Email5.Value = .EMAIL5
                    txt_Person_Occupation.Value = .OCCUPATION
                    txt_Person_EmployerName.Value = .EMPLOYER_NAME


                    'chk_Person_IsDecease.Checked = False
                    txt_Person_DeceaseDate.Value = Nothing
                    txt_Person_DeceaseDate.Hidden = True


                    txt_Person_TaxNumber.Value = .TAX_NUMBER
                    If Not IsNothing(.TAX_REG_NUMBER) AndAlso .TAX_REG_NUMBER Then
                        chk_Person_IsPEP.Checked = True
                    Else
                        chk_Person_IsPEP.Checked = False
                    End If
                    txt_Person_SourceOfWealth.Value = .SOURCE_OF_WEALTH
                    txt_Person_Comment.Value = .COMMENT

                    If Not IsNothing(.ROLE) Then
                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Peran_orang_dalam_Korporasi", "Kode", .ROLE)
                        If drTemp IsNot Nothing Then
                            cmb_Person_Director_Role.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                        End If
                    End If

                    'Address
                    Dim listAddress As List(Of SIPESATGOAML_ACT_DIRECTOR_ADDRESS) = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objDirector.PK_SIPESATGOAML_ACT_DIRECTOR_ID And x.ISEMPLOYER = False).ToList
                    If listAddress Is Nothing Then
                        listAddress = New List(Of SIPESATGOAML_ACT_DIRECTOR_ADDRESS)
                    End If
                    Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
                    dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
                    dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
                    dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
                    Using objdb As New NawaDevDAL.NawaDatadevEntities
                        Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                        If dtAddress IsNot Nothing Then
                            For Each row In dtAddress.Rows
                                row("PK_Address_ID") = row("PK_SIPESATGOAML_Act_Director_Address_ID")
                                If Not IsDBNull(row("Address_Type")) Then
                                    Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
                                    If objAddressType IsNot Nothing Then
                                        row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
                                    End If
                                End If

                                If Not IsDBNull(row("Country_Code")) Then
                                    Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
                                    If objCountry IsNot Nothing Then
                                        row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                                    End If
                                End If
                            Next
                        End If

                    End Using
                    BindGridPanel(gp_Person_Address, dtAddress)

                    'Phone
                    Dim listPhone As List(Of SIPESATGOAML_ACT_DIRECTOR_PHONE) = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objDirector.PK_SIPESATGOAML_ACT_DIRECTOR_ID And x.ISEMPLOYER = False).ToList
                    If listPhone Is Nothing Then
                        listPhone = New List(Of SIPESATGOAML_ACT_DIRECTOR_PHONE)
                    End If

                    Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
                    dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
                    dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
                    Using objdb As New NawaDevDAL.NawaDatadevEntities
                        Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                        If dtPhone IsNot Nothing Then
                            For Each row In dtPhone.Rows
                                row("PK_Phone_ID") = row("PK_SIPESATGOAML_Act_Director_Phone")
                                If Not IsDBNull(row("tph_contact_type")) Then
                                    Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
                                    If objPhoneType IsNot Nothing Then
                                        row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
                                    End If
                                End If
                            Next
                        End If

                    End Using
                    BindGridPanel(gp_Person_Phone, dtPhone)

                    'Employer Address
                    Dim listEmployerAddress As List(Of SIPESATGOAML_ACT_DIRECTOR_ADDRESS) = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objDirector.PK_SIPESATGOAML_ACT_DIRECTOR_ID And x.ISEMPLOYER = True).ToList
                    If listEmployerAddress Is Nothing Then
                        listEmployerAddress = New List(Of SIPESATGOAML_ACT_DIRECTOR_ADDRESS)
                    End If

                    Dim dtEmployerAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerAddress)
                    dtEmployerAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
                    dtEmployerAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
                    dtEmployerAddress.Columns.Add(New DataColumn("Country", GetType(String)))
                    Using objdb As New NawaDevDAL.NawaDatadevEntities
                        Dim listEmployerAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                        If dtEmployerAddress IsNot Nothing Then
                            For Each row In dtEmployerAddress.Rows
                                row("PK_Address_ID") = row("PK_SIPESATGOAML_Act_Director_Address_ID")
                                If Not IsDBNull(row("Address_Type")) Then
                                    Dim objEmployerAddressType = listEmployerAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
                                    If objEmployerAddressType IsNot Nothing Then
                                        row("AddressType") = objEmployerAddressType.Kode & " - " & objEmployerAddressType.Keterangan
                                    End If
                                End If

                                If Not IsDBNull(row("Country_Code")) Then
                                    Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
                                    If objCountry IsNot Nothing Then
                                        row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                                    End If
                                End If
                            Next
                        End If

                    End Using
                    BindGridPanel(gp_Person_EmployerAddress, dtEmployerAddress)

                    'Employer Phone
                    Dim listEmployerPhone As List(Of SIPESATGOAML_ACT_DIRECTOR_PHONE) = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objDirector.PK_SIPESATGOAML_ACT_DIRECTOR_ID And x.ISEMPLOYER = True).ToList
                    If listEmployerPhone Is Nothing Then
                        listEmployerPhone = New List(Of SIPESATGOAML_ACT_DIRECTOR_PHONE)
                    End If

                    Dim dtEmployerPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerPhone)
                    dtEmployerPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
                    dtEmployerPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
                    Using objdb As New NawaDevDAL.NawaDatadevEntities
                        Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
                        If dtEmployerPhone IsNot Nothing Then
                            For Each row In dtEmployerPhone.Rows
                                row("PK_Phone_ID") = row("PK_SIPESATGOAML_Act_Director_Phone")
                                If Not IsDBNull(row("tph_contact_type")) Then
                                    Dim objEmployerPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
                                    If objEmployerPhoneType IsNot Nothing Then
                                        row("ContactType") = objEmployerPhoneType.Kode & " - " & objEmployerPhoneType.Keterangan
                                    End If
                                End If
                            Next
                        End If

                    End Using
                    BindGridPanel(gp_Person_EmployerPhone, dtEmployerPhone)

                    'Identification
                    Dim listIdentification As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_IDENTIFICATION) = objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objDirector.PK_SIPESATGOAML_ACT_DIRECTOR_ID).ToList
                    If listIdentification Is Nothing Then
                        listIdentification = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_IDENTIFICATION)
                    End If

                    Dim dtIdentification As DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
                    dtIdentification.Columns.Add(New DataColumn("PK_Identification_ID", GetType(String)))
                    dtIdentification.Columns.Add(New DataColumn("Country", GetType(String)))
                    Using objdb As New NawaDevDAL.NawaDatadevEntities
                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                        If dtIdentification IsNot Nothing Then
                            For Each row In dtIdentification.Rows
                                row("PK_Identification_ID") = row("PK_ACT_DIRECTOR_IDENTIFICATION_ID")
                                If Not IsDBNull(row("Issued_Country")) Then
                                    Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Issued_Country")).FirstOrDefault
                                    If objCountry IsNot Nothing Then
                                        row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                                    End If
                                End If
                            Next
                        End If

                    End Using
                    BindGridPanel(gp_Person_Identification, dtIdentification)

                    chk_Person_AccountSignatory_IsPrimary.Hidden = True
                    cmb_Person_AccountSignatory_Role.IsHidden = True
                    cmb_Person_Director_Role.IsHidden = False
                    txt_Person_Title.Focus()
                End With
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    '    Protected Sub LoadConductor()
    '        Try
    '            Dim objConductor = objGoAML_Transaction_Class.list_goAML_Trn_Conductor.Where(Function(x) x.PK_SIPESATGOAML_Trn_Conductor_ID = Me.PK_Party_ID).FirstOrDefault
    '            If objConductor IsNot Nothing Then
    '                With objConductor
    '                    If Not IsNothing(.Gender) Then
    '                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Jenis_Kelamin", "Kode", .Gender)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_Person_Gender.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                        End If
    '                    End If

    '                    txt_Person_Title.Value = .Title
    '                    txt_Person_LastName.Value = .Last_Name

    '                    If .BirthDate.HasValue Then
    '                        txt_Person_BirthDate.Value = .BirthDate
    '                    End If

    '                    txt_Person_BirthPlace.Value = .Birth_Place
    '                    txt_Person_MotherName.Value = .Mothers_Name
    '                    txt_Person_Alias.Value = .Alias
    '                    txt_Person_SSN.Value = .SSN
    '                    txt_Person_PassportNumber.Value = .Passport_Number

    '                    If Not IsNothing(.Passport_Country) Then
    '                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Passport_Country)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_Person_PassportCountry.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                        End If
    '                    End If

    '                    txt_Person_IDNumber.Value = .Id_Number

    '                    If Not IsNothing(.Nationality1) Then
    '                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality1)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_Person_Nationality1.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                        End If
    '                    End If
    '                    If Not IsNothing(.Nationality2) Then
    '                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality2)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_Person_Nationality2.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                        End If
    '                    End If
    '                    If Not IsNothing(.Nationality3) Then
    '                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Nationality3)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_Person_Nationality3.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                        End If
    '                    End If
    '                    If Not IsNothing(.Residence) Then
    '                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .Residence)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_Person_Residence.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
    '                        End If
    '                    End If

    '                    txt_Person_Email1.Value = .Email
    '                    txt_Person_Email2.Value = .email2
    '                    txt_Person_Email3.Value = .email3
    '                    txt_Person_Email4.Value = .email4
    '                    txt_Person_Email5.Value = .email5
    '                    txt_Person_Occupation.Value = .Occupation
    '                    txt_Person_EmployerName.Value = .Employer_Name

    '                    If Not IsNothing(.Deceased) AndAlso .Deceased Then
    '                        chk_Person_IsDecease.Checked = True
    '                        If .Deceased_Date.HasValue Then
    '                            txt_Person_DeceaseDate.Value = .Deceased_Date
    '                        End If
    '                        txt_Person_DeceaseDate.Hidden = False
    '                    Else
    '                        chk_Person_IsDecease.Checked = False
    '                        txt_Person_DeceaseDate.Value = Nothing
    '                        txt_Person_DeceaseDate.Hidden = True
    '                    End If

    '                    txt_Person_TaxNumber.Value = .Tax_Number
    '                    If Not IsNothing(.Tax_Reg_Number) AndAlso .Tax_Reg_Number Then
    '                        chk_Person_IsPEP.Checked = True
    '                    Else
    '                        chk_Person_IsPEP.Checked = False
    '                    End If
    '                    txt_Person_SourceOfWealth.Value = .Source_Of_Wealth
    '                    txt_Person_Comment.Value = .Comment

    '                    'Address
    '                    Dim listAddress As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Conductor_Address) = objGoAML_Transaction_Class.list_goAML_Trn_Conductor_Address.Where(Function(x) x.FK_Trn_Conductor_ID = objConductor.PK_SIPESATGOAML_Trn_Conductor_ID And x.isEmployer = False).ToList
    '                    If listAddress Is Nothing Then
    '                        listAddress = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Conductor_Address)
    '                    End If
    '                    Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
    '                    dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
    '                    dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
    '                    dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
    '                    Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                        Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                        For Each row In dtAddress.Rows
    '                            row("PK_Address_ID") = row("PK_SIPESATGOAML_Trn_Conductor_Address_ID")
    '                            If Not IsDBNull(row("Address_Type")) Then
    '                                Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
    '                                If objAddressType IsNot Nothing Then
    '                                    row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
    '                                End If
    '                            End If

    '                            If Not IsDBNull(row("Country_Code")) Then
    '                                Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
    '                                If objCountry IsNot Nothing Then
    '                                    row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                                End If
    '                            End If
    '                        Next
    '                    End Using
    '                    BindGridPanel(gp_Person_Address, dtAddress)

    '                    'Phone
    '                    Dim listPhone As List(Of SIPESATGOAMLBLL.SIPESATGOAML_trn_Conductor_Phone) = objGoAML_Transaction_Class.list_goAML_trn_Conductor_Phone.Where(Function(x) x.FK_Trn_Conductor_ID = objConductor.PK_SIPESATGOAML_Trn_Conductor_ID And x.isEmployer = False).ToList
    '                    If listPhone Is Nothing Then
    '                        listPhone = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_trn_Conductor_Phone)
    '                    End If

    '                    Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
    '                    dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
    '                    dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
    '                    Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                        Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                        For Each row In dtPhone.Rows
    '                            row("PK_Phone_ID") = row("PK_SIPESATGOAML_trn_Conductor_Phone")
    '                            If Not IsDBNull(row("tph_contact_type")) Then
    '                                Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
    '                                If objPhoneType IsNot Nothing Then
    '                                    row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
    '                                End If
    '                            End If
    '                        Next
    '                    End Using
    '                    BindGridPanel(gp_Person_Phone, dtPhone)

    '                    'Employer Address
    '                    Dim listEmployerAddress As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Conductor_Address) = objGoAML_Transaction_Class.list_goAML_Trn_Conductor_Address.Where(Function(x) x.FK_Trn_Conductor_ID = objConductor.PK_SIPESATGOAML_Trn_Conductor_ID And x.isEmployer = True).ToList
    '                    If listEmployerAddress Is Nothing Then
    '                        listEmployerAddress = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Trn_Conductor_Address)
    '                    End If

    '                    Dim dtEmployerAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerAddress)
    '                    dtEmployerAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
    '                    dtEmployerAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
    '                    dtEmployerAddress.Columns.Add(New DataColumn("Country", GetType(String)))
    '                    Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                        Dim listEmployerAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                        For Each row In dtEmployerAddress.Rows
    '                            row("PK_Address_ID") = row("PK_SIPESATGOAML_Trn_Conductor_Address_ID")
    '                            If Not IsDBNull(row("Address_Type")) Then
    '                                Dim objEmployerAddressType = listEmployerAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
    '                                If objEmployerAddressType IsNot Nothing Then
    '                                    row("AddressType") = objEmployerAddressType.Kode & " - " & objEmployerAddressType.Keterangan
    '                                End If
    '                            End If

    '                            If Not IsDBNull(row("Country_Code")) Then
    '                                Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
    '                                If objCountry IsNot Nothing Then
    '                                    row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                                End If
    '                            End If
    '                        Next
    '                    End Using
    '                    BindGridPanel(gp_Person_EmployerAddress, dtEmployerAddress)

    '                    'Employer Phone
    '                    Dim listEmployerPhone As List(Of SIPESATGOAMLBLL.SIPESATGOAML_trn_Conductor_Phone) = objGoAML_Transaction_Class.list_goAML_trn_Conductor_Phone.Where(Function(x) x.FK_Trn_Conductor_ID = objConductor.PK_SIPESATGOAML_Trn_Conductor_ID And x.isEmployer = True).ToList
    '                    If listEmployerPhone Is Nothing Then
    '                        listEmployerPhone = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_trn_Conductor_Phone)
    '                    End If

    '                    Dim dtEmployerPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEmployerPhone)
    '                    dtEmployerPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
    '                    dtEmployerPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
    '                    Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                        Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
    '                        For Each row In dtEmployerPhone.Rows
    '                            row("PK_Phone_ID") = row("PK_SIPESATGOAML_trn_Conductor_Phone")
    '                            If Not IsDBNull(row("tph_contact_type")) Then
    '                                Dim objEmployerPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
    '                                If objEmployerPhoneType IsNot Nothing Then
    '                                    row("ContactType") = objEmployerPhoneType.Kode & " - " & objEmployerPhoneType.Keterangan
    '                                End If
    '                            End If
    '                        Next
    '                    End Using
    '                    BindGridPanel(gp_Person_EmployerPhone, dtEmployerPhone)

    '                    'Identification
    '                    Dim listIdentification As List(Of SIPESATGOAMLBLL.SIPESATGOAML_Transaction_Person_Identification) = objGoAML_Transaction_Class.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.Conductor And x.FK_Person_ID = objConductor.PK_SIPESATGOAML_Trn_Conductor_ID).ToList
    '                    If listIdentification Is Nothing Then
    '                        listIdentification = New List(Of SIPESATGOAMLBLL.SIPESATGOAML_Transaction_Person_Identification)
    '                    End If

    '                    Dim dtIdentification As DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
    '                    dtIdentification.Columns.Add(New DataColumn("PK_Identification_ID", GetType(String)))
    '                    dtIdentification.Columns.Add(New DataColumn("Country", GetType(String)))
    '                    Using objdb As New NawaDevDAL.NawaDatadevEntities
    '                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
    '                        For Each row In dtIdentification.Rows
    '                            row("PK_Identification_ID") = row("PK_SIPESATGOAML_Transaction_Person_Identification_ID")
    '                            If Not IsDBNull(row("Issued_Country")) Then
    '                                Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Issued_Country")).FirstOrDefault
    '                                If objCountry IsNot Nothing Then
    '                                    row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
    '                                End If
    '                            End If
    '                        Next
    '                    End Using
    '                    BindGridPanel(gp_Person_Identification, dtIdentification)

    '                    chk_Person_AccountSignatory_IsPrimary.Hidden = True
    '                    cmb_Person_AccountSignatory_Role.IsHidden = True
    '                    cmb_Person_Director_Role.IsHidden = True
    '                    txt_Person_Title.Focus()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub

    Protected Sub DeletePerson()
        Try

            'Dim objCek = objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Where(Function(x) x.PK_ACT_ACC_SIGNATORY_ID = Me.PK_PartySub_ID).FirstOrDefault
            'If objCek IsNot Nothing Then
            '    objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Remove(objCek)

            '    'Bind to Grid Panel
            '    Dim listAccountSignatory = objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Where(Function(x) x.FK_ACT_ACCOUNT_ID = Me.PK_Party_ID).ToList
            '    If listAccountSignatory Is Nothing Then
            '        listAccountSignatory = New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGNATORY)
            '    End If

            '    Dim dtAccountSignatory As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountSignatory)
            '    dtAccountSignatory.Columns.Add(New DataColumn("PK_Account_Signatory_ID", GetType(String)))
            '    'dtAccountSignatory.Columns.Add(New DataColumn("RoleName", GetType(String)))

            '    Using objdb As New NawaDevDAL.NawaDatadevEntities
            '        'Dim listRole = objdb.goAML_Ref_Peran_orang_dalam_rekening.ToList
            '        For Each row In dtAccountSignatory.Rows
            '            row("PK_Account_Signatory_ID") = row("PK_ACT_ACC_SIGNATORY_ID")   'Karena pakai window yg sama, PK harus disalin
            '            'If Not IsDBNull(row("role")) Then
            '            '    Dim objRole = listRole.Where(Function(x) x.Kode = row("role")).FirstOrDefault
            '            '    If objRole IsNot Nothing Then
            '            '        row("RoleName") = objRole.Kode & " - " & objRole.Keterangan
            '            '    End If
            '            'End If
            '        Next
            '    End Using
            '    BindGridPanel(gp_Account_Signatory, dtAccountSignatory)

            '    'Delete Address
            '    Dim listAddress As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_ADDRESS)
            '    listAddress = objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = objCek.PK_ACT_ACC_SIGNATORY_ID).ToList
            '    If listAddress IsNot Nothing Then
            '        For Each itemx In listAddress
            '            objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Address.Remove(itemx)
            '        Next
            '    End If

            '    'Delete Phone
            '    Dim listPhone As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_PHONE)
            '    listPhone = objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = objCek.PK_ACT_ACC_SIGNATORY_ID).ToList
            '    If listPhone IsNot Nothing Then
            '        For Each itemx In listPhone
            '            objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Phone.Remove(itemx)
            '        Next
            '    End If

            '    'Delete Identification
            '    Dim listIdentification As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION)
            '    listIdentification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_ACT_ACC_SIGN_ID = objCek.PK_ACT_ACC_SIGNATORY_ID).ToList
            '    If listIdentification IsNot Nothing Then
            '        For Each itemx In listIdentification
            '            objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Remove(itemx)
            '        Next
            '    End If
            'End If
            Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_ID = Me.PK_PartySub_ID).FirstOrDefault
            If objCek IsNot Nothing Then
                objGoAML_Activity_Class.list_goAML_Act_Director.Remove(objCek)

                'Bind to Grid Panel
                Dim listAccountDirector = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.FK_ACT_ENTITY_ID = Me.PK_Party_ID).ToList
                If listAccountDirector Is Nothing Then
                    listAccountDirector = New List(Of SIPESATGOAML_ACT_DIRECTOR)
                End If

                Dim dtAccountDirector As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAccountDirector)
                dtAccountDirector.Columns.Add(New DataColumn("PK_Account_Entity_Director_ID", GetType(String)))
                dtAccountDirector.Columns.Add(New DataColumn("RoleName", GetType(String)))

                'Using objdb As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
                '    Dim listRole = objdb.goAML_Ref_Peran_orang_dalam_Korporasi.ToList
                '    For Each row In dtAccountDirector.Rows
                '        row("PK_Account_Entity_Director_ID") = row("PK_SIPESATGOAML_Act_Director_ID")   'Karena pakai window yg sama, PK harus disalin
                '        If Not IsDBNull(row("role")) Then
                '            Dim objRole = listRole.Where(Function(x) x.Kode = row("role")).FirstOrDefault
                '            If objRole IsNot Nothing Then
                '                row("RoleName") = objRole.Kode & " - " & objRole.Keterangan
                '            End If
                '        End If
                '    Next
                'End Using
                BindGridPanel(gp_Entity_Director, dtAccountDirector)

                'Delete Address
                Dim listAddress As New List(Of SIPESATGOAML_ACT_DIRECTOR_ADDRESS)
                listAddress = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objCek.PK_SIPESATGOAML_ACT_DIRECTOR_ID).ToList
                If listAddress IsNot Nothing Then
                    For Each itemx In listAddress
                        objGoAML_Activity_Class.list_goAML_Act_Director_Address.Remove(itemx)
                    Next
                End If

                'Delete Phone
                Dim listPhone As New List(Of SIPESATGOAML_ACT_DIRECTOR_PHONE)
                listPhone = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objCek.PK_SIPESATGOAML_ACT_DIRECTOR_ID).ToList
                If listPhone IsNot Nothing Then
                    For Each itemx In listPhone
                        objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Remove(itemx)
                    Next
                End If

                ''Delete Identification
                'Dim listIdentification As New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION)
                'listIdentification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = Me.PartySubType And x.FK_Act_Person_ID = objCek.PK_SIPESATGOAML_Act_Director_ID).ToList
                'If listIdentification IsNot Nothing Then
                '    For Each itemx In listIdentification
                '        objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Remove(itemx)
                '    Next
                'End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region


#Region "Entity"
    Protected Sub btn_Entity_Save_Click()
        Try
            ValidateEntity()


            Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Entity.Where(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_ID = Me.PK_Party_ID).FirstOrDefault
            If objCek Is Nothing Then
                objCek = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY
                objCek.PK_SIPESATGOAML_ACT_ENTITY_ID = Me.PK_Party_ID
                Me.IsNewRecord = True
            Else
                Me.IsNewRecord = False
            End If

            With objCek
                .FK_REPORT_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                .FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID

                .NAME = txt_Entity_CorporateName.Text
                .COMMERCIAL_NAME = txt_Entity_CommercialName.Text
                .INCORPORATION_LEGAL_FORM = cmb_Entity_IncorporationLegalForm.SelectedItemValue
                .INCORPORATION_NUMBER = txt_Entity_IncorporationNumber.Text
                .BUSINESS = txt_Entity_Business.Text
                .EMAIL = txt_Entity_Email.Text
                .URL = txt_Entity_URL.Text
                .INCORPORATION_STATE = txt_Entity_IncorporationState.Text
                .INCORPORATION_COUNTRY_CODE = cmb_Entity_IncorporationCountryCode.SelectedItemValue
                If Not txt_Entity_IncorporationDate.SelectedDate = DateTime.MinValue Then
                    .INCORPORATION_DATE = txt_Entity_IncorporationDate.SelectedDate
                Else
                    .INCORPORATION_DATE = Nothing
                End If

                .BUSINESS_CLOSED = chk_Entity_IsClosed.Checked
                If chk_Entity_IsClosed.Checked Then
                    If Not txt_Entity_ClosingDate.SelectedDate = DateTime.MinValue Then
                        .DATE_BUSINESS_CLOSED = txt_Entity_ClosingDate.SelectedDate
                    Else
                        .DATE_BUSINESS_CLOSED = Nothing
                    End If
                End If

                .TAX_NUMBER = txt_Entity_TaxNumber.Text
                .COMMENTS = txt_Entity_Comment.Text
            End With

            If Me.IsNewRecord Then
                objCek.Active = True
                objCek.CreatedDate = DateTime.Now
                objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID

                objGoAML_Activity_Class.list_goAML_Act_Entity.Add(objCek)
            Else
                objCek.LastUpdateDate = DateTime.Now
                objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
            End If

            Dim lngPK_Relationship As Long = 0
            If objGoAML_Activity_Class.list_goAML_Act_Entity_Relationship.Count = 0 Then
                If objGoAML_Activity_Class.list_goAML_Act_Entity_Relationship.Count > 0 Then
                    lngPK_Relationship = objGoAML_Activity_Class.list_goAML_Act_Entity_Relationship.Min(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_RELATIONSHIP_ID)
                    If lngPK_Relationship > 0 Then
                        lngPK_Relationship = 0
                    End If
                Else
                    lngPK_Relationship = -1
                End If
                Dim Identity As Long = 0
                Dim objentity = objGoAML_Activity_Class.list_goAML_Act_Entity.Where(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_ID = Me.PK_Party_ID).FirstOrDefault
                If objentity IsNot Nothing Then
                    Identity = objentity.PK_SIPESATGOAML_ACT_ENTITY_ID
                End If



                Dim ObjRelationship As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_RELATIONSHIP
                ObjRelationship.CLIENT_NUMBER = txt_ClientNumber_Relationship_Entity.Value
                ObjRelationship.COMMENTS = txt_ClientNumber_Relationship_Entity.Value
                ObjRelationship.CreatedDate = DateTime.Now
                ObjRelationship.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                If txt_Validfrom_Relationship_Entity.SelectedDate = DateTime.MinValue Then
                    ObjRelationship.VALID_FROM = Nothing
                Else

                    ObjRelationship.VALID_FROM = txt_Validfrom_Relationship_Entity.Value
                End If
                If txt_Validto_Relationship_Entity.SelectedDate = DateTime.MinValue Then
                    ObjRelationship.VALID_TO = Nothing
                Else

                    ObjRelationship.VALID_TO = txt_Validto_Relationship_Entity.Value
                End If
                If chk_IsApproxFrom_Relationship_Entity.Checked Then
                    ObjRelationship.IS_APPROX_FROM_DATE = True
                Else

                    ObjRelationship.IS_APPROX_FROM_DATE = chk_IsApproxFrom_Relationship_Entity.Value
                End If
                If chk_IsApproxTo_Relationship_Entity.Checked Then
                    ObjRelationship.IS_APPROX_TO_DATE = True
                Else

                    ObjRelationship.IS_APPROX_TO_DATE = chk_IsApproxTo_Relationship_Entity.Value
                End If


                ObjRelationship.PK_SIPESATGOAML_ACT_ENTITY_RELATIONSHIP_ID = lngPK_Relationship
                ObjRelationship.FK_ACT_ENTITY_ID = Identity

                objGoAML_Activity_Class.list_goAML_Act_Entity_Relationship.Add(ObjRelationship)
            Else
                For Each item In objGoAML_Activity_Class.list_goAML_Act_Entity_Relationship
                    item.COMMENTS = txt_Comments_Relationship_Entity.Value
                    item.CLIENT_NUMBER = txt_ClientNumber_Relationship_Entity.Value
                    If txt_Validfrom_Relationship_Entity.SelectedDate = DateTime.MinValue Then
                        item.VALID_FROM = Nothing
                    Else
                        item.VALID_FROM = txt_Validfrom_Relationship_Entity.Value
                    End If

                    If txt_Validto_Relationship_Entity.SelectedDate = DateTime.MinValue Then
                        item.VALID_TO = Nothing
                    Else
                        item.VALID_TO = txt_Validto_Relationship_Entity.Value
                    End If

                    If chk_IsApproxFrom_Relationship_Entity.Checked Then
                        item.IS_APPROX_FROM_DATE = True
                    Else
                        item.IS_APPROX_FROM_DATE = chk_IsApproxFrom_Relationship_Entity.Checked
                    End If

                    If chk_IsApproxTo_Relationship_Entity.Checked Then
                        item.IS_APPROX_TO_DATE = True
                    Else
                        item.IS_APPROX_TO_DATE = chk_IsApproxTo_Relationship_Entity.Value
                    End If

                Next
            End If

            LoadActivityShortInfo()


            'Close window
            window_EntityParty.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Entity_Back_Click()
        Try
            window_EntityParty.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub chk_Entity_IsClosed_Changed()
        Try
            If chk_Entity_IsClosed.Checked Then
                txt_Entity_ClosingDate.Hidden = False
            Else
                txt_Entity_ClosingDate.Hidden = True
                txt_Entity_ClosingDate.Value = Nothing
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadEntity()
        Try
            Dim objEntity As SIPESATGOAML_ACT_ENTITY = objGoAML_Activity_Class.list_goAML_Act_Entity.Where(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_ID = Me.PK_Party_ID).FirstOrDefault
            If objEntity IsNot Nothing Then
                With objEntity
                    txt_Entity_CorporateName.Value = .NAME
                    txt_Entity_CommercialName.Value = .COMMERCIAL_NAME

                    If Not IsNothing(.INCORPORATION_LEGAL_FORM) Then
                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Bentuk_Badan_Usaha", "kode", .INCORPORATION_LEGAL_FORM)
                        If drTemp IsNot Nothing Then
                            cmb_Entity_IncorporationLegalForm.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                        End If
                    End If

                    txt_Entity_IncorporationNumber.Value = .INCORPORATION_NUMBER
                    txt_Entity_Business.Value = .BUSINESS
                    txt_Entity_Email.Value = .EMAIL
                    txt_Entity_URL.Value = .URL
                    txt_Entity_IncorporationState.Value = .INCORPORATION_STATE

                    If Not IsNothing(.INCORPORATION_COUNTRY_CODE) Then
                        drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "kode", .INCORPORATION_COUNTRY_CODE)
                        If drTemp IsNot Nothing Then
                            cmb_Entity_IncorporationCountryCode.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                        End If
                    End If

                    If .INCORPORATION_DATE.HasValue Then
                        txt_Entity_IncorporationDate.Value = .INCORPORATION_DATE
                    End If

                    If .BUSINESS_CLOSED IsNot Nothing AndAlso .BUSINESS_CLOSED Then
                        chk_Entity_IsClosed.Checked = True
                        If .DATE_BUSINESS_CLOSED.HasValue Then
                            txt_Entity_ClosingDate.Value = .DATE_BUSINESS_CLOSED
                        End If
                        txt_Entity_ClosingDate.Hidden = False
                    Else
                        txt_Entity_ClosingDate.Hidden = True
                    End If

                    txt_Entity_TaxNumber.Value = .TAX_NUMBER
                    txt_Entity_Comment.Value = .COMMENTS


                    'Bind Address
                    'Address
                    Dim listAddress As List(Of SIPESATGOAML_ACT_ENTITY_ADDRESS) = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
                    If listAddress Is Nothing Then
                        listAddress = New List(Of SIPESATGOAML_ACT_ENTITY_ADDRESS)
                    End If
                    Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
                    dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
                    dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
                    dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
                    Using objdbaddress As New NawaDevDAL.NawaDatadevEntities
                        Dim listAddressType = objdbaddress.goAML_Ref_Kategori_Kontak.ToList
                        Dim listCountry = objdbaddress.goAML_Ref_Nama_Negara.ToList
                        For Each row In dtAddress.Rows
                            row("PK_Address_ID") = row("PK_SIPESATGOAML_Act_Entity_Address_ID")
                            If Not IsDBNull(row("Address_Type")) Then
                                Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
                                If objAddressType IsNot Nothing Then
                                    row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
                                End If
                            End If

                            If Not IsDBNull(row("Country_Code")) Then
                                Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
                                If objCountry IsNot Nothing Then
                                    row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                                End If
                            End If
                        Next
                    End Using
                    BindGridPanel(gp_Entity_Address, dtAddress)

                    'Bind Phone
                    'Phone
                    Dim listPhone As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_PHONE) = objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
                    If listPhone Is Nothing Then
                        listPhone = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_PHONE)
                    End If

                    Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
                    dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
                    dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
                    Using objdbphone As New NawaDevDAL.NawaDatadevEntities
                        Dim listContactType = objdbphone.goAML_Ref_Kategori_Kontak.ToList
                        Dim listCountry = objdbphone.goAML_Ref_Nama_Negara.ToList
                        For Each row In dtPhone.Rows
                            row("PK_Phone_ID") = row("PK_SIPESATGOAML_ACT_ENTITY_PHONE")
                            If Not IsDBNull(row("tph_contact_type")) Then
                                Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
                                If objPhoneType IsNot Nothing Then
                                    row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
                                End If
                            End If
                        Next
                    End Using
                    BindGridPanel(gp_Entity_Phone, dtPhone)

                    'Bind Director
                    Dim listEntityDirector = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
                    If listEntityDirector Is Nothing Then
                        listEntityDirector = New List(Of SIPESATGOAML_ACT_DIRECTOR)
                    End If

                    Dim dtEntityDirector As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEntityDirector)
                    dtEntityDirector.Columns.Add(New DataColumn("PK_Entity_Director_ID", GetType(String)))
                    dtEntityDirector.Columns.Add(New DataColumn("RoleName", GetType(String)))
                    Using objdbphone As New NawaDevDAL.NawaDatadevEntities
                        Dim listRole = objdbphone.goAML_Ref_Peran_orang_dalam_Korporasi.ToList
                        For Each row In dtEntityDirector.Rows
                            row("PK_Entity_Director_ID") = row("PK_SIPESATGOAML_Act_Director_ID")   'Karena pakai window yg sama, PK harus disalin
                            If Not IsDBNull(row("role")) Then
                                Dim objRole = listRole.Where(Function(x) x.Kode = row("role")).FirstOrDefault
                                If objRole IsNot Nothing Then
                                    row("RoleName") = objRole.Kode & " - " & objRole.Keterangan
                                End If
                            End If
                        Next
                    End Using

                    BindGridPanel(gp_Entity_Director, dtEntityDirector)

                    'Bind Relationship
                    Dim listEntityrelationship = objGoAML_Activity_Class.list_goAML_Act_Entity_Relationship.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
                    If listEntityrelationship Is Nothing Then
                        listEntityrelationship = New List(Of SIPESATGOAML_ACT_ENTITY_RELATIONSHIP)
                    End If

                    Dim dtEntityrelationship As DataTable = NawaBLL.Common.CopyGenericToDataTable(listEntityrelationship)

                    If dtEntityrelationship IsNot Nothing Then
                        For Each item As DataRow In dtEntityrelationship.Rows
                            If Not IsDBNull(item("COMMENTS")) Then
                                txt_Comments_Relationship_Entity.Value = item("COMMENTS")
                            End If
                            If Not IsDBNull(item("CLIENT_NUMBER")) Then
                                txt_ClientNumber_Relationship_Entity.Value = item("CLIENT_NUMBER")
                            End If
                            If Not IsDBNull(item("VALID_FROM")) Then
                                txt_Validfrom_Relationship_Entity.Value = item("VALID_FROM")
                            End If
                            If Not IsDBNull(item("VALID_TO")) Then
                                txt_Validto_Relationship_Entity.Value = item("VALID_TO")
                            End If
                            If Not IsDBNull(item("IS_APPROX_FROM_DATE")) Then
                                chk_IsApproxFrom_Relationship_Entity.Checked = item("IS_APPROX_FROM_DATE")
                            End If
                            If Not IsDBNull(item("IS_APPROX_TO_DATE")) Then
                                chk_IsApproxTo_Relationship_Entity.Checked = item("IS_APPROX_TO_DATE")
                            End If

                        Next


                    End If



                End With
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region


#Region "Phone"
    Protected Sub btn_Phone_Entry_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strCommand As String = e.ExtraParams(1).Value

            If Me.ReadOnlyLevel = 0 Then
                Me.ReadOnlyLevel = 6
            End If

            Me.IsEmployer = False
            If e.ExtraParams(2) IsNot Nothing Then
                If UCase(e.ExtraParams(2).Name) = "ISEMPLOYER" Then
                    Dim strIsEmployer = e.ExtraParams(2).Value
                    If strIsEmployer <> "0" Then
                        Me.IsEmployer = True
                    End If
                End If
            End If

            Me.PK_Phone_ID = strID
            If strCommand = "Add" Or strCommand = "Edit" Or strCommand = "Detail" Then
                CleanWindowPhone()
                If strCommand = "Edit" Or strCommand = "Detail" Then
                    LoadPhone()
                End If

                If strCommand = "Add" Or strCommand = "Edit" Then
                    SetReadOnlyWindowPhone(False)
                Else
                    SetReadOnlyWindowPhone(True)
                End If

                Dim strWindowTitle As String = "Phone"
                If Me.IsEmployer Then
                    strWindowTitle = strWindowTitle & " - Work Place"
                End If

                'Open windows Phone
                window_Phone.Title = strWindowTitle
                window_Phone.Hidden = False
                window_Phone.Body.ScrollTo(Direction.Top, 0)

            ElseIf strCommand = "Delete" Then
                DeletePhone()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadPhone()
        Try
            If Me.PartySubType = 0 Then     'Level 1 (AccountEntity/Person/Entity)
                If Me.PartyType = enumPartyType._Person Then    'Person
                    Dim objPhone = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Where(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_PHONE_ID = Me.PK_Phone_ID).FirstOrDefault
                    If objPhone IsNot Nothing Then
                        With objPhone
                            If Not IsNothing(.TPH_CONTACT_TYPE) Then
                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Kategori_Kontak", "Kode", .TPH_CONTACT_TYPE)
                                If drTemp IsNot Nothing Then
                                    cmb_Phone_ContactType.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                                End If
                            End If
                            If Not IsNothing(.TPH_COMMUNICATION_TYPE) Then
                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Jenis_Alat_Komunikasi", "Kode", .TPH_COMMUNICATION_TYPE)
                                If drTemp IsNot Nothing Then
                                    cmb_Phone_CommunicationType.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                                End If
                            End If

                            txt_Phone_CountryPrefix.Value = .TPH_COUNTRY_PREFIX
                            txt_Phone_Number.Value = .TPH_NUMBER
                            txt_Phone_Extension.Value = .TPH_EXTENSION
                            txt_Phone_Comment.Value = .COMMENTS
                        End With
                    End If
                ElseIf Me.PartyType = enumPartyType._Entity Then    'Entity
                    Dim objPhone = objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Where(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_PHONE = Me.PK_Phone_ID).FirstOrDefault
                    If objPhone IsNot Nothing Then
                        With objPhone
                            If Not IsNothing(.TPH_CONTACT_TYPE) Then
                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Kategori_Kontak", "Kode", .TPH_CONTACT_TYPE)
                                If drTemp IsNot Nothing Then
                                    cmb_Phone_ContactType.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                                End If
                            End If
                            If Not IsNothing(.TPH_COMMUNICATION_TYPE) Then
                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Jenis_Alat_Komunikasi", "Kode", .TPH_COMMUNICATION_TYPE)
                                If drTemp IsNot Nothing Then
                                    cmb_Phone_CommunicationType.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                                End If
                            End If

                            txt_Phone_CountryPrefix.Value = .TPH_COUNTRY_PREFIX
                            txt_Phone_Number.Value = .TPH_NUMBER
                            txt_Phone_Extension.Value = .TPH_EXTENSION
                            txt_Phone_Comment.Value = .COMMENTS
                        End With
                    End If
                End If
            Else    'Level 2 (AccountSignatory/AccountEntityDirector/EntityDirector)
                Dim objPhone = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Where(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_PHONE = Me.PK_Phone_ID).FirstOrDefault
                If objPhone IsNot Nothing Then
                    With objPhone
                        If Not IsNothing(.TPH_CONTACT_TYPE) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Kategori_Kontak", "Kode", .TPH_CONTACT_TYPE)
                            If drTemp IsNot Nothing Then
                                cmb_Phone_ContactType.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If
                        If Not IsNothing(.TPH_COMMUNICATION_TYPE) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Jenis_Alat_Komunikasi", "Kode", .TPH_COMMUNICATION_TYPE)
                            If drTemp IsNot Nothing Then
                                cmb_Phone_CommunicationType.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If

                        txt_Phone_CountryPrefix.Value = .TPH_COUNTRY_PREFIX
                        txt_Phone_Number.Value = .TPH_NUMBER
                        txt_Phone_Extension.Value = .TPH_EXTENSION
                        txt_Phone_Comment.Value = .COMMENTS
                    End With
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub DeletePhone()
        Try
            If Me.PartySubType = 0 Then     'Level 1 (AccountEntity/Person/Entity)
                If Me.PartyType = enumPartyType._Person Then    'Person 
                    Dim objPhone = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Where(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_PHONE_ID = Me.PK_Phone_ID).FirstOrDefault
                    If objPhone IsNot Nothing Then
                        objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Remove(objPhone)

                        'Bind to gridPanel
                        Dim listPhone As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_PHONE) = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Where(Function(x) x.FK_ACT_PERSON = Me.PK_Party_ID And x.ISEMPLOYER = Me.IsEmployer).ToList
                        If listPhone Is Nothing Then
                            listPhone = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_PHONE)
                        End If

                        Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
                        dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
                        dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
                        Using objdb As New NawaDevDAL.NawaDatadevEntities
                            Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
                            Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                            For Each row In dtPhone.Rows
                                row("PK_Phone_ID") = row("PK_SIPESATGOAML_Act_Person_Phone_ID")
                                If Not IsDBNull(row("tph_contact_type")) Then
                                    Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
                                    If objPhoneType IsNot Nothing Then
                                        row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
                                    End If
                                End If
                            Next
                        End Using

                        If Not Me.IsEmployer Then
                            BindGridPanel(gp_Person_Phone, dtPhone)
                        Else
                            BindGridPanel(gp_Person_EmployerPhone, dtPhone)
                        End If
                    End If
                ElseIf Me.PartyType = enumPartyType._Entity Then    'Entity
                    Dim objPhone = objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Where(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_PHONE = Me.PK_Phone_ID).FirstOrDefault
                    If objPhone IsNot Nothing Then
                        objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Remove(objPhone)

                        'Bind to gridPanel
                        Dim listPhone As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_PHONE) = objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Where(Function(x) x.FK_ACT_ENTITY_ID = Me.PK_Party_ID).ToList
                        If listPhone Is Nothing Then
                            listPhone = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_PHONE)
                        End If

                        Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
                        dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
                        dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
                        Using objdb As New NawaDevDAL.NawaDatadevEntities
                            Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
                            Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                            For Each row In dtPhone.Rows
                                row("PK_Phone_ID") = row("PK_SIPESATGOAML_Act_Entity_Phone")
                                If Not IsDBNull(row("tph_contact_type")) Then
                                    Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
                                    If objPhoneType IsNot Nothing Then
                                        row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
                                    End If
                                End If
                            Next
                        End Using
                        BindGridPanel(gp_Entity_Phone, dtPhone)
                    End If
                End If
            Else    'Level 2 (AccountSignatory/AccountEntityDirector/EntityDirector)
                Dim objPhone = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Where(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_PHONE = Me.PK_Phone_ID).FirstOrDefault
                If objPhone IsNot Nothing Then
                    objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Remove(objPhone)

                    'Bind to gridPanel
                    Dim listPhone As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_PHONE) = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Where(Function(x) x.FK_ACT_DIRECTOR_ID = Me.PK_PartySub_ID And x.ISEMPLOYER = Me.IsEmployer).ToList
                    If listPhone Is Nothing Then
                        listPhone = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_PHONE)
                    End If

                    Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
                    dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
                    dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
                    Using objdb As New NawaDevDAL.NawaDatadevEntities
                        Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                        For Each row In dtPhone.Rows
                            row("PK_Phone_ID") = row("PK_SIPESATGOAML_ACT_DIRECTOR_PHONE")
                            If Not IsDBNull(row("tph_contact_type")) Then
                                Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
                                If objPhoneType IsNot Nothing Then
                                    row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
                                End If
                            End If
                        Next
                    End Using

                    If Not Me.IsEmployer Then
                        BindGridPanel(gp_Person_Phone, dtPhone)
                    Else
                        BindGridPanel(gp_Person_EmployerPhone, dtPhone)
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_Phone_Save_Click(sender As Object, e As DirectEventArgs)
        Try
            ValidatePhone()

            If Me.PartySubType = 0 Then     'Level 1 (AccountEntity/Person/Entity)
                If Me.PartyType = enumPartyType._Person Then    'Person
                    Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Where(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_PHONE_ID = Me.PK_Phone_ID).FirstOrDefault
                    If objCek Is Nothing Then
                        Dim lngPK As Long = 0
                        If objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Count = 0 Then
                            lngPK = -1
                        Else
                            lngPK = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Min(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_PHONE_ID) - 1
                            If lngPK >= 0 Then
                                lngPK = -1
                            End If
                        End If

                        objCek = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_PHONE
                        objCek.PK_SIPESATGOAML_ACT_PERSON_PHONE_ID = lngPK
                        Me.IsNewRecord = True
                    Else
                        Me.IsNewRecord = False
                    End If

                    With objCek
                        .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                        .FK_Act_Person = Me.PK_Party_ID
                        .Tph_Contact_Type = cmb_Phone_ContactType.SelectedItemValue
                        .Tph_Communication_Type = cmb_Phone_CommunicationType.SelectedItemValue
                        .tph_country_prefix = txt_Phone_CountryPrefix.Value
                        .tph_number = txt_Phone_Number.Value
                        .tph_extension = txt_Phone_Extension.Value
                        .comments = txt_Phone_Comment.Value
                        .isEmployer = Me.IsEmployer
                    End With

                    If Me.IsNewRecord Then
                        objCek.Active = True
                        objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        objCek.CreatedDate = DateTime.Now
                        objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Add(objCek)
                    Else
                        objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                        objCek.LastUpdateDate = DateTime.Now
                    End If

                    'Bind to gridPanel
                    Dim listPhone As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_PHONE) = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Where(Function(x) x.FK_ACT_PERSON = Me.PK_Party_ID And x.ISEMPLOYER = Me.IsEmployer).ToList
                    If listPhone Is Nothing Then
                        listPhone = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_PHONE)
                    End If

                    Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
                    dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
                    dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
                    Using objdb As New NawaDevDAL.NawaDatadevEntities
                        Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                        For Each row In dtPhone.Rows
                            row("PK_Phone_ID") = row("PK_SIPESATGOAML_Act_Person_Phone_ID")
                            If Not IsDBNull(row("tph_contact_type")) Then
                                Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
                                If objPhoneType IsNot Nothing Then
                                    row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
                                End If
                            End If
                        Next
                    End Using

                    If Not Me.IsEmployer Then
                        BindGridPanel(gp_Person_Phone, dtPhone)
                    Else
                        BindGridPanel(gp_Person_EmployerPhone, dtPhone)
                    End If
                ElseIf Me.PartyType = enumPartyType._Entity Then    'Entity
                    Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Where(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_PHONE = Me.PK_Phone_ID).FirstOrDefault
                    If objCek Is Nothing Then
                        Dim lngPK As Long = 0
                        If objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Count = 0 Then
                            lngPK = -1
                        Else
                            lngPK = objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Min(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_PHONE) - 1
                            If lngPK >= 0 Then
                                lngPK = -1
                            End If
                        End If

                        objCek = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_PHONE
                        objCek.PK_SIPESATGOAML_ACT_ENTITY_PHONE = lngPK
                        Me.IsNewRecord = True
                    Else
                        Me.IsNewRecord = False
                    End If

                    With objCek
                        .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                        .FK_Act_Entity_ID = Me.PK_Party_ID
                        .Tph_Contact_Type = cmb_Phone_ContactType.SelectedItemValue
                        .Tph_Communication_Type = cmb_Phone_CommunicationType.SelectedItemValue
                        .tph_country_prefix = txt_Phone_CountryPrefix.Value
                        .tph_number = txt_Phone_Number.Value
                        .tph_extension = txt_Phone_Extension.Value
                        .comments = txt_Phone_Comment.Value
                    End With

                    If Me.IsNewRecord Then
                        objCek.Active = True
                        objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        objCek.CreatedDate = DateTime.Now
                        objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Add(objCek)
                    Else
                        objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                        objCek.LastUpdateDate = DateTime.Now
                    End If

                    'Bind to gridPanel
                    Dim listPhone As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_PHONE) = objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Where(Function(x) x.FK_ACT_ENTITY_ID = Me.PK_Party_ID).ToList
                    If listPhone Is Nothing Then
                        listPhone = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_PHONE)
                    End If

                    Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
                    dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
                    dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
                    Using objdb As New NawaDevDAL.NawaDatadevEntities
                        Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                        For Each row In dtPhone.Rows
                            row("PK_Phone_ID") = row("PK_SIPESATGOAML_Act_Entity_Phone")
                            If Not IsDBNull(row("tph_contact_type")) Then
                                Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
                                If objPhoneType IsNot Nothing Then
                                    row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
                                End If
                            End If
                        Next
                    End Using
                    BindGridPanel(gp_Entity_Phone, dtPhone)
                End If
            Else    'Level 2 (AccountSignatory/AccountEntityDirector/EntityDirector) 
                Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Where(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_PHONE = Me.PK_Phone_ID).FirstOrDefault
                If objCek Is Nothing Then
                    Dim lngPK As Long = 0
                    If objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Count = 0 Then
                        lngPK = -1
                    Else
                        lngPK = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Min(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_PHONE) - 1
                        If lngPK >= 0 Then
                            lngPK = -1
                        End If
                    End If

                    objCek = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_PHONE
                    objCek.PK_SIPESATGOAML_ACT_DIRECTOR_PHONE = lngPK
                    Me.IsNewRecord = True
                Else
                    Me.IsNewRecord = False
                End If

                With objCek
                    .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                    .FK_Act_Director_ID = Me.PK_PartySub_ID
                    .Tph_Contact_Type = cmb_Phone_ContactType.SelectedItemValue
                    .Tph_Communication_Type = cmb_Phone_CommunicationType.SelectedItemValue
                    .tph_country_prefix = txt_Phone_CountryPrefix.Value
                    .tph_number = txt_Phone_Number.Value
                    .tph_extension = txt_Phone_Extension.Value
                    .comments = txt_Phone_Comment.Value
                    .isEmployer = Me.IsEmployer
                End With

                If Me.IsNewRecord Then
                    objCek.Active = True
                    objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objCek.CreatedDate = DateTime.Now
                    objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Add(objCek)
                Else
                    objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objCek.LastUpdateDate = DateTime.Now
                End If

                'Bind to gridPanel
                Dim listPhone As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_PHONE) = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Where(Function(x) x.FK_ACT_DIRECTOR_ID = Me.PK_PartySub_ID And x.ISEMPLOYER = Me.IsEmployer).ToList
                If listPhone Is Nothing Then
                    listPhone = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_PHONE)
                End If

                Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
                dtPhone.Columns.Add(New DataColumn("PK_Phone_ID", GetType(String)))
                dtPhone.Columns.Add(New DataColumn("ContactType", GetType(String)))
                Using objdb As New NawaDevDAL.NawaDatadevEntities
                    Dim listContactType = objdb.goAML_Ref_Kategori_Kontak.ToList
                    Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                    For Each row In dtPhone.Rows
                        row("PK_Phone_ID") = row("PK_SIPESATGOAML_ACT_DIRECTOR_PHONE")
                        If Not IsDBNull(row("tph_contact_type")) Then
                            Dim objPhoneType = listContactType.Where(Function(x) x.Kode = row("tph_contact_type")).FirstOrDefault
                            If objPhoneType IsNot Nothing Then
                                row("ContactType") = objPhoneType.Kode & " - " & objPhoneType.Keterangan
                            End If
                        End If
                    Next
                End Using

                If Not Me.IsEmployer Then
                    BindGridPanel(gp_Person_Phone, dtPhone)
                Else
                    BindGridPanel(gp_Person_EmployerPhone, dtPhone)
                End If
            End If
            'Close Window Phone
            window_Phone.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Phone_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            window_Phone.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region


#Region "Address"
    Protected Sub cmb_Address_Country_Changed()
        Try
            'If country is ID (Indonesia), State is mandatory
            If cmb_Address_Country.StringValue = "ID" Then
                txt_Address_State.AllowBlank = False
                txt_Address_State.FieldStyle = "background-color: #FFE4C4;"
            Else
                txt_Address_State.AllowBlank = True
                txt_Address_State.FieldStyle = "background-color: #FFFFFF;"
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Address_Entry_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strCommand As String = e.ExtraParams(1).Value

            If Me.ReadOnlyLevel = 0 Then
                Me.ReadOnlyLevel = 6
            End If

            Me.IsEmployer = False
            If e.ExtraParams(2) IsNot Nothing Then
                If UCase(e.ExtraParams(2).Name) = "ISEMPLOYER" Then
                    Dim strIsEmployer = e.ExtraParams(2).Value
                    If strIsEmployer <> "0" Then
                        Me.IsEmployer = True
                    End If
                End If
            End If

            Me.PK_Address_ID = strID
            If strCommand = "Add" Or strCommand = "Edit" Or strCommand = "Detail" Then
                CleanWindowAddress()

                If strCommand = "Edit" Or strCommand = "Detail" Then
                    LoadAddress()
                End If

                If strCommand = "Add" Or strCommand = "Edit" Then
                    SetReadOnlyWindowAddress(False)
                Else
                    SetReadOnlyWindowAddress(True)
                End If

                Dim strWindowTitle As String = "Address"
                If Me.IsEmployer Then
                    strWindowTitle = strWindowTitle & " - Work Place"
                End If

                'Open windows Address
                window_Address.Title = strWindowTitle
                window_Address.Hidden = False
                window_Address.Body.ScrollTo(Direction.Top, 0)

            ElseIf strCommand = "Delete" Then
                DeleteAddress()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadAddress()
        Try
            If Me.PartySubType = 0 Then     'Level 1 (AccountEntity/Person/Entity)
                If Me.PartyType = enumPartyType._Person Then    'Person 
                    Dim objAddress = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Where(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID = Me.PK_Address_ID).FirstOrDefault
                    If objAddress IsNot Nothing Then
                        With objAddress
                            If Not IsNothing(.ADDRESS_TYPE) Then
                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Kategori_Kontak", "Kode", .ADDRESS_TYPE)
                                If drTemp IsNot Nothing Then
                                    cmb_Address_Type.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                                End If
                            End If

                            txt_Address_Address.Value = .ADDRESS
                            txt_Address_Town.Value = .TOWN
                            txt_Address_City.Value = .CITY
                            txt_Address_Zip.Value = .ZIP

                            If Not IsNothing(.COUNTRY_CODE) Then
                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .COUNTRY_CODE)
                                If drTemp IsNot Nothing Then
                                    cmb_Address_Country.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                                End If
                            End If

                            txt_Address_State.Value = .STATE
                            txt_Address_Comment.Value = .COMMENTS
                        End With
                    End If
                ElseIf Me.PartyType = enumPartyType._Entity Then   'Entity
                    Dim objAddress = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Where(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_Address_ID = Me.PK_Address_ID).FirstOrDefault
                    If objAddress IsNot Nothing Then
                        With objAddress
                            If Not IsNothing(.ADDRESS_TYPE) Then
                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Kategori_Kontak", "Kode", .ADDRESS_TYPE)
                                If drTemp IsNot Nothing Then
                                    cmb_Address_Type.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                                End If
                            End If

                            txt_Address_Address.Value = .ADDRESS
                            txt_Address_Town.Value = .TOWN
                            txt_Address_City.Value = .CITY
                            txt_Address_Zip.Value = .ZIP

                            If Not IsNothing(.COUNTRY_CODE) Then
                                drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .COUNTRY_CODE)
                                If drTemp IsNot Nothing Then
                                    cmb_Address_Country.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                                End If
                            End If

                            txt_Address_State.Value = .STATE
                            txt_Address_Comment.Value = .COMMENTS
                        End With
                    End If
                End If
            Else    'Level 2 (AccountSignatory/AccountEntityDirector/EntityDirector)
                Dim objAddress = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Where(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID = Me.PK_Address_ID).FirstOrDefault
                If objAddress IsNot Nothing Then
                    With objAddress
                        If Not IsNothing(.ADDRESS_Type) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Kategori_Kontak", "Kode", .ADDRESS_Type)
                            If drTemp IsNot Nothing Then
                                cmb_Address_Type.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If

                        txt_Address_Address.Value = .ADDRESS
                        txt_Address_Town.Value = .TOWN
                        txt_Address_City.Value = .CITY
                        txt_Address_Zip.Value = .ZIP

                        If Not IsNothing(.COUNTRY_CODE) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .COUNTRY_CODE)
                            If drTemp IsNot Nothing Then
                                cmb_Address_Country.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If

                        txt_Address_State.Value = .STATE
                        txt_Address_Comment.Value = .COMMENTS
                    End With
                End If
            End If

            'If Country = ID (Indonesia), State is mandatory
            If cmb_Address_Country.StringValue = "ID" Then
                txt_Address_State.AllowBlank = False
                txt_Address_State.FieldStyle = "background-color: #FFE4C4;"
            Else
                txt_Address_State.AllowBlank = True
                txt_Address_State.FieldStyle = "background-color: #FFFFFF;"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub DeleteAddress()
        Try
            If Me.PartySubType = 0 Then     'Level 1 (AccountEntity/Person/Entity)
                If Me.PartyType = enumPartyType._Person Then    'Person
                    Dim objAddress = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Where(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID = Me.PK_Address_ID).FirstOrDefault
                    If objAddress IsNot Nothing Then
                        objGoAML_Activity_Class.list_goAML_Act_Person_Address.Remove(objAddress)

                        'Bind to gridPanel
                        Dim listAddress As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_ADDRESS) = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Where(Function(x) x.FK_ACT_PERSON = Me.PK_Party_ID And x.ISEMPLOYER = Me.IsEmployer).ToList
                        If listAddress Is Nothing Then
                            listAddress = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_ADDRESS)
                        End If
                        Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
                        dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
                        dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
                        dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
                        Using objdb As New NawaDevDAL.NawaDatadevEntities
                            Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
                            Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                            For Each row In dtAddress.Rows
                                row("PK_Address_ID") = row("PK_SIPESATGOAML_Act_Person_Address_ID")
                                If Not IsDBNull(row("Address_Type")) Then
                                    Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
                                    If objAddressType IsNot Nothing Then
                                        row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
                                    End If
                                End If

                                If Not IsDBNull(row("Country_Code")) Then
                                    Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
                                    If objCountry IsNot Nothing Then
                                        row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                                    End If
                                End If
                            Next
                        End Using

                        If Not Me.IsEmployer Then
                            BindGridPanel(gp_Person_Address, dtAddress)
                        Else
                            BindGridPanel(gp_Person_EmployerAddress, dtAddress)
                        End If
                    End If
                ElseIf Me.PartyType = enumPartyType._Entity Then    'Entity
                    Dim objAddress = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Where(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_Address_ID = Me.PK_Address_ID).FirstOrDefault
                    If objAddress IsNot Nothing Then
                        objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Remove(objAddress)

                        'Bind to gridPanel
                        Dim listAddress As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_ADDRESS) = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Where(Function(x) x.FK_ACT_ENTITY_ID = Me.PK_Party_ID).ToList
                        If listAddress Is Nothing Then
                            listAddress = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_ADDRESS)
                        End If
                        Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
                        dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
                        dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
                        dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
                        Using objdb As New NawaDevDAL.NawaDatadevEntities
                            Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
                            Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                            For Each row In dtAddress.Rows
                                row("PK_Address_ID") = row("PK_SIPESATGOAML_Act_Entity_Address_ID")
                                If Not IsDBNull(row("Address_Type")) Then
                                    Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
                                    If objAddressType IsNot Nothing Then
                                        row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
                                    End If
                                End If

                                If Not IsDBNull(row("Country_Code")) Then
                                    Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
                                    If objCountry IsNot Nothing Then
                                        row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                                    End If
                                End If
                            Next
                        End Using
                        BindGridPanel(gp_Entity_Address, dtAddress)
                    End If
                End If
            Else    'Level 2 (AccountSignatory/AccountEntityDirector/EntityDirector)
                Dim objAddress = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Where(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID = Me.PK_Address_ID).FirstOrDefault
                If objAddress IsNot Nothing Then
                    objGoAML_Activity_Class.list_goAML_Act_Director_Address.Remove(objAddress)

                    'Bind to gridPanel
                    Dim listAddress As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_ADDRESS) = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Where(Function(x) x.FK_ACT_DIRECTOR_ID = Me.PK_PartySub_ID And x.ISEMPLOYER = Me.IsEmployer).ToList
                    If listAddress Is Nothing Then
                        listAddress = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_ADDRESS)
                    End If
                    Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
                    dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
                    dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
                    dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
                    Using objdb As New NawaDevDAL.NawaDatadevEntities
                        Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                        For Each row In dtAddress.Rows
                            row("PK_Address_ID") = row("PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID")
                            If Not IsDBNull(row("Address_Type")) Then
                                Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
                                If objAddressType IsNot Nothing Then
                                    row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
                                End If
                            End If

                            If Not IsDBNull(row("Country_Code")) Then
                                Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
                                If objCountry IsNot Nothing Then
                                    row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                                End If
                            End If
                        Next
                    End Using

                    If Not Me.IsEmployer Then
                        BindGridPanel(gp_Person_Address, dtAddress)
                    Else
                        BindGridPanel(gp_Person_EmployerAddress, dtAddress)
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_Address_Save_Click(sender As Object, e As DirectEventArgs)
        Try
            ValidateAddress()


            If Me.PartySubType = 0 Then     'Level 1 (AccountEntity/Person/Entity)
                If Me.PartyType = enumPartyType._Person Then    'Person
                    Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Where(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID = Me.PK_Address_ID).FirstOrDefault
                    If objCek Is Nothing Then
                        Dim lngPK As Long = 0
                        If objGoAML_Activity_Class.list_goAML_Act_Person_Address.Count = 0 Then
                            lngPK = -1
                        Else
                            lngPK = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Min(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID) - 1
                            If lngPK >= 0 Then
                                lngPK = -1
                            End If
                        End If

                        objCek = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_ADDRESS
                        objCek.PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID = lngPK
                        Me.IsNewRecord = True
                    Else
                        Me.IsNewRecord = False
                    End If

                    With objCek
                        .fk_report_id = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                        .FK_Act_Person = Me.PK_Party_ID
                        .Address_Type = cmb_Address_Type.SelectedItemValue
                        .Address = txt_Address_Address.Value
                        .Town = txt_Address_Town.Value
                        .City = txt_Address_City.Value
                        .Zip = txt_Address_Zip.Value
                        .Country_Code = cmb_Address_Country.SelectedItemValue
                        .State = txt_Address_State.Value
                        .Comments = txt_Address_Comment.Value
                        .isEmployer = Me.IsEmployer
                    End With

                    If Me.IsNewRecord Then
                        objCek.Active = True
                        objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        objCek.CreatedDate = DateTime.Now
                        objGoAML_Activity_Class.list_goAML_Act_Person_Address.Add(objCek)
                    Else
                        objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                        objCek.LastUpdateDate = DateTime.Now
                    End If

                    'Bind to gridPanel
                    Dim listAddress As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_ADDRESS) = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Where(Function(x) x.FK_ACT_PERSON = Me.PK_Party_ID And x.ISEMPLOYER = Me.IsEmployer).ToList
                    If listAddress Is Nothing Then
                        listAddress = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_ADDRESS)
                    End If
                    Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
                    dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
                    dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
                    dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
                    Using objdb As New NawaDevDAL.NawaDatadevEntities
                        Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                        For Each row In dtAddress.Rows
                            row("PK_Address_ID") = row("PK_SIPESATGOAML_Act_Person_Address_ID")
                            If Not IsDBNull(row("Address_Type")) Then
                                Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
                                If objAddressType IsNot Nothing Then
                                    row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
                                End If
                            End If

                            If Not IsDBNull(row("Country_Code")) Then
                                Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
                                If objCountry IsNot Nothing Then
                                    row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                                End If
                            End If
                        Next
                    End Using

                    If Not Me.IsEmployer Then
                        BindGridPanel(gp_Person_Address, dtAddress)
                    Else
                        BindGridPanel(gp_Person_EmployerAddress, dtAddress)
                    End If
                ElseIf Me.PartyType = enumPartyType._Entity Then    'Entity
                    Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Where(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_Address_ID = Me.PK_Address_ID).FirstOrDefault
                    If objCek Is Nothing Then
                        Dim lngPK As Long = 0
                        If objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Count = 0 Then
                            lngPK = -1
                        Else
                            lngPK = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Min(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_Address_ID) - 1
                            If lngPK >= 0 Then
                                lngPK = -1
                            End If
                        End If

                        objCek = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_ADDRESS
                        objCek.PK_SIPESATGOAML_ACT_ENTITY_Address_ID = lngPK
                        Me.IsNewRecord = True
                    Else
                        Me.IsNewRecord = False
                    End If

                    With objCek
                        .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                        .FK_Act_Entity_ID = Me.PK_Party_ID
                        .Address_Type = cmb_Address_Type.SelectedItemValue
                        .Address = txt_Address_Address.Value
                        .Town = txt_Address_Town.Value
                        .City = txt_Address_City.Value
                        .Zip = txt_Address_Zip.Value
                        .Country_Code = cmb_Address_Country.SelectedItemValue
                        .State = txt_Address_State.Value
                        .Comments = txt_Address_Comment.Value
                    End With

                    If Me.IsNewRecord Then
                        objCek.Active = True
                        objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        objCek.CreatedDate = DateTime.Now
                        objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Add(objCek)
                    Else
                        objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                        objCek.LastUpdateDate = DateTime.Now
                    End If

                    'Bind to gridPanel
                    Dim listAddress As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_ADDRESS) = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Where(Function(x) x.FK_ACT_ENTITY_ID = Me.PK_Party_ID).ToList
                    If listAddress Is Nothing Then
                        listAddress = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_ADDRESS)
                    End If
                    Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
                    dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
                    dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
                    dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
                    Using objdb As New NawaDevDAL.NawaDatadevEntities
                        Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
                        Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                        For Each row In dtAddress.Rows
                            row("PK_Address_ID") = row("PK_SIPESATGOAML_Act_Entity_Address_ID")
                            If Not IsDBNull(row("Address_Type")) Then
                                Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
                                If objAddressType IsNot Nothing Then
                                    row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
                                End If
                            End If

                            If Not IsDBNull(row("Country_Code")) Then
                                Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
                                If objCountry IsNot Nothing Then
                                    row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                                End If
                            End If
                        Next
                    End Using
                    BindGridPanel(gp_Entity_Address, dtAddress)
                End If
            Else    'Level 2 (AccountSignatory/AccountEntityDirector/EntityDirector)
                Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Where(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID = Me.PK_Address_ID).FirstOrDefault
                If objCek Is Nothing Then
                    Dim lngPK As Long = 0
                    If objGoAML_Activity_Class.list_goAML_Act_Director_Address.Count = 0 Then
                        lngPK = -1
                    Else
                        lngPK = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Min(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID) - 1
                        If lngPK >= 0 Then
                            lngPK = -1
                        End If
                    End If

                    objCek = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_ADDRESS
                    objCek.PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID = lngPK
                    Me.IsNewRecord = True
                Else
                    Me.IsNewRecord = False
                End If

                With objCek
                    .FK_Report_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                    .FK_Act_Director_ID = Me.PK_PartySub_ID
                    .Address_Type = cmb_Address_Type.SelectedItemValue
                    .Address = txt_Address_Address.Value
                    .Town = txt_Address_Town.Value
                    .City = txt_Address_City.Value
                    .Zip = txt_Address_Zip.Value
                    .Country_Code = cmb_Address_Country.SelectedItemValue
                    .State = txt_Address_State.Value
                    .Comments = txt_Address_Comment.Value
                    .isEmployer = Me.IsEmployer
                End With

                If Me.IsNewRecord Then
                    objCek.Active = True
                    objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objCek.CreatedDate = DateTime.Now
                    objGoAML_Activity_Class.list_goAML_Act_Director_Address.Add(objCek)
                Else
                    objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objCek.LastUpdateDate = DateTime.Now
                End If

                'Bind to gridPanel
                Dim listAddress As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_ADDRESS) = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Where(Function(x) x.FK_ACT_DIRECTOR_ID = Me.PK_PartySub_ID And x.ISEMPLOYER = Me.IsEmployer).ToList
                If listAddress Is Nothing Then
                    listAddress = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_ADDRESS)
                End If
                Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddress)
                dtAddress.Columns.Add(New DataColumn("PK_Address_ID", GetType(String)))
                dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
                dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
                Using objdb As New NawaDevDAL.NawaDatadevEntities
                    Dim listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
                    Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                    For Each row In dtAddress.Rows
                        row("PK_Address_ID") = row("PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID")
                        If Not IsDBNull(row("Address_Type")) Then
                            Dim objAddressType = listAddressType.Where(Function(x) x.Kode = row("Address_Type")).FirstOrDefault
                            If objAddressType IsNot Nothing Then
                                row("AddressType") = objAddressType.Kode & " - " & objAddressType.Keterangan
                            End If
                        End If

                        If Not IsDBNull(row("Country_Code")) Then
                            Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Country_Code")).FirstOrDefault
                            If objCountry IsNot Nothing Then
                                row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                            End If
                        End If
                    Next
                End Using

                If Not Me.IsEmployer Then
                    BindGridPanel(gp_Person_Address, dtAddress)
                Else
                    BindGridPanel(gp_Person_EmployerAddress, dtAddress)
                End If
            End If

            'Close Window Address
            window_Address.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Address_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            window_Address.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Identification"
    Protected Sub btn_Identification_Entry_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strCommand As String = e.ExtraParams(1).Value

            If Me.ReadOnlyLevel = 0 Then
                Me.ReadOnlyLevel = 6
            End If

            Me.PK_Identification_ID = strID
            If strCommand = "Add" Or strCommand = "Edit" Or strCommand = "Detail" Then
                CleanWindowIdentification()
                If strCommand = "Edit" Or strCommand = "Detail" Then
                    LoadIdentification()
                End If

                If strCommand = "Add" Or strCommand = "Edit" Then
                    SetReadOnlyWindowIdentification(False)
                Else
                    SetReadOnlyWindowIdentification(True)
                End If

                Dim strWindowTitle As String = "Identification"

                'Open windows Identification
                window_Identification.Title = strWindowTitle
                window_Identification.Hidden = False
                window_Identification.Body.ScrollTo(Direction.Top, 0)

            ElseIf strCommand = "Delete" Then
                DeleteIdentification()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadIdentification()
        Try
            If Me.PartySubType = 0 Then
                Dim objTrnIdentification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.PK_ACTIVITY_PERSON_IDENTIFICATION_ID = Me.PK_Identification_ID).FirstOrDefault
                If objTrnIdentification IsNot Nothing Then
                    With objTrnIdentification
                        If Not IsNothing(.TYPE) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Jenis_Dokumen_Identitas", "Kode", .TYPE)
                            If drTemp IsNot Nothing Then
                                cmb_Identification_Type.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If

                        txt_Identification_Number.Value = .NUMBER
                        If .ISSUED_DATE.HasValue Then
                            txt_Identification_IssueDate.Value = .ISSUED_DATE
                        End If
                        If .EXPIRY_DATE.HasValue Then
                            txt_Identification_ExpiredDate.Value = .EXPIRY_DATE
                        End If
                        txt_Identification_IssueBy.Value = .ISSUED_BY

                        If Not IsNothing(.ISSUED_COUNTRY) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .ISSUED_COUNTRY)
                            If drTemp IsNot Nothing Then
                                cmb_Identification_Country.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If

                        txt_Identification_Comment.Value = .IDENTIFICATION_COMMENT
                    End With
                End If
            Else
                Dim objTrnIdentification = objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Where(Function(x) x.PK_ACT_DIRECTOR_IDENTIFICATION_ID = Me.PK_Identification_ID).FirstOrDefault
                If objTrnIdentification IsNot Nothing Then
                    With objTrnIdentification
                        If Not IsNothing(.TYPE) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Jenis_Dokumen_Identitas", "Kode", .TYPE)
                            If drTemp IsNot Nothing Then
                                cmb_Identification_Type.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If

                        txt_Identification_Number.Value = .NUMBER
                        If .ISSUED_DATE.HasValue Then
                            txt_Identification_IssueDate.Value = .ISSUED_DATE
                        End If
                        If .EXPIRY_DATE.HasValue Then
                            txt_Identification_ExpiredDate.Value = .EXPIRY_DATE
                        End If
                        txt_Identification_IssueBy.Value = .ISSUED_BY

                        If Not IsNothing(.ISSUED_COUNTRY) Then
                            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_goAML_Ref_Nama_Negara", "Kode", .ISSUED_COUNTRY)
                            If drTemp IsNot Nothing Then
                                cmb_Identification_Country.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
                            End If
                        End If

                        txt_Identification_Comment.Value = .IDENTIFICATION_COMMENT
                    End With
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub DeleteIdentification()
        Try
            If Me.PartySubType = 0 Then
                Dim objIdentification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.PK_ACTIVITY_PERSON_IDENTIFICATION_ID = Me.PK_Identification_ID).FirstOrDefault
                If objIdentification IsNot Nothing Then
                    objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Remove(objIdentification)
                End If

                'Bind to gridPanel (Object yang sama karena tablenya sama)
                Dim listIdentification As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION) = Nothing
                If Me.PartySubType = 0 Then     'Level 1 Person
                    listIdentification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_ACT_PERSON = Me.PK_Party_ID).ToList
                Else    'Level 2 (AccountSignatory/AccountEntityDirector/EntityDirector)
                    listIdentification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_ACT_PERSON = Me.PK_PartySub_ID).ToList
                End If

                If listIdentification Is Nothing Then
                    listIdentification = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION)
                End If

                Dim dtIdentification As DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
                dtIdentification.Columns.Add(New DataColumn("PK_Identification_ID", GetType(String)))
                dtIdentification.Columns.Add(New DataColumn("Country", GetType(String)))
                Using objdb As New NawaDevDAL.NawaDatadevEntities
                    Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                    For Each row In dtIdentification.Rows
                        row("PK_Identification_ID") = row("PK_Activity_Person_IDENTIFICATION_ID")
                        If Not IsDBNull(row("Issued_Country")) Then
                            Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Issued_Country")).FirstOrDefault
                            If objCountry IsNot Nothing Then
                                row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                            End If
                        End If
                    Next
                End Using
                BindGridPanel(gp_Person_Identification, dtIdentification)
            Else
                Dim objIdentification = objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Where(Function(x) x.PK_ACT_DIRECTOR_IDENTIFICATION_ID = Me.PK_Identification_ID).FirstOrDefault
                If objIdentification IsNot Nothing Then
                    objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Remove(objIdentification)
                End If

                'Bind to gridPanel (Object yang sama karena tablenya sama)
                Dim listIdentification As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_IDENTIFICATION) = Nothing
                If Me.PartySubType = 0 Then     'Level 1 Person
                    'listIdentification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_ACT_ACC_SIGN_ID = Me.PK_Party_ID).ToList
                Else    'Level 2 (AccountSignatory/AccountEntityDirector/EntityDirector)
                    listIdentification = objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Where(Function(x) x.FK_ACT_DIRECTOR_ID = Me.PK_PartySub_ID).ToList
                End If

                If listIdentification Is Nothing Then
                    listIdentification = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_IDENTIFICATION)
                End If

                Dim dtIdentification As DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
                dtIdentification.Columns.Add(New DataColumn("PK_Identification_ID", GetType(String)))
                dtIdentification.Columns.Add(New DataColumn("Country", GetType(String)))
                Using objdb As New NawaDevDAL.NawaDatadevEntities
                    Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                    For Each row In dtIdentification.Rows
                        row("PK_Identification_ID") = row("PK_ACT_DIRECTOR_IDENTIFICATION_ID")
                        If Not IsDBNull(row("Issued_Country")) Then
                            Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Issued_Country")).FirstOrDefault
                            If objCountry IsNot Nothing Then
                                row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                            End If
                        End If
                    Next
                End Using
                BindGridPanel(gp_Person_Identification, dtIdentification)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_Identification_Save_Click(sender As Object, e As DirectEventArgs)
        Try
            ValidateIdentification()
            If Me.PartySubType = 0 Then

                Dim objCek = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.PK_ACTIVITY_PERSON_IDENTIFICATION_ID = Me.PK_Identification_ID).FirstOrDefault
                If objCek Is Nothing Then
                    Dim lngPK As Long = 0
                    If objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Count = 0 Then
                        lngPK = -1
                    Else
                        lngPK = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Min(Function(x) x.PK_ACTIVITY_PERSON_IDENTIFICATION_ID) - 1
                        If lngPK >= 0 Then
                            lngPK = -1
                        End If
                    End If

                    objCek = New SIPESATGoAMLBLL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION
                    objCek.PK_ACTIVITY_PERSON_IDENTIFICATION_ID = lngPK
                    Me.IsNewRecord = True
                Else
                    Me.IsNewRecord = False
                End If

                With objCek
                    .FK_REPORT_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                    .FK_ACT_PERSON = Me.PK_Party_ID
                    'If Me.PartySubType = 0 Then     'Level 1 (Person)
                    '    .FK_Act_Person_ID = Me.PK_Party_ID
                    '    .FK_Person_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.Person
                    'Else    'Level 2 (AccountSignatory/AccountEntityDirector/EntityDirector) 
                    '    .FK_Act_Person_ID = Me.PK_PartySub_ID
                    '    .FK_Person_Type = Me.PartySubType
                    'End If

                    .TYPE = cmb_Identification_Type.SelectedItemValue
                    .NUMBER = txt_Identification_Number.Value
                    If txt_Identification_IssueDate.SelectedDate <> DateTime.MinValue Then
                        .ISSUED_DATE = txt_Identification_IssueDate.SelectedDate
                    End If
                    If txt_Identification_ExpiredDate.SelectedDate <> DateTime.MinValue Then
                        .EXPIRY_DATE = txt_Identification_ExpiredDate.SelectedDate
                    End If
                    .ISSUED_BY = txt_Identification_IssueBy.Value
                    .ISSUED_COUNTRY = cmb_Identification_Country.SelectedItemValue
                    .IDENTIFICATION_COMMENT = txt_Identification_Comment.Value
                End With

                If Me.IsNewRecord Then
                    objCek.Active = True
                    objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objCek.CreatedDate = DateTime.Now
                    objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Add(objCek)
                Else
                    objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objCek.LastUpdateDate = DateTime.Now
                End If

                'Bind to gridPanel
                Dim listIdentification As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION) = Nothing
                'If Me.PartySubType = 0 Then     'Level 1 (Person)
                '    listIdentification = objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Where(Function(x) x.FK_ACT_ACC_SIGN_ID = Me.PK_Party_ID).ToList
                'Else    'Level 2 (AccountSignatory/AccountEntityDirector/EntityDirector) 
                '    listIdentification = objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Where(Function(x) x.FK_ACT_ACC_SIGN_ID = Me.PK_PartySub_ID).ToList
                'End If
                listIdentification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_ACT_PERSON = Me.PK_Party_ID).ToList

                If listIdentification Is Nothing Then
                    listIdentification = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION)
                End If

                Dim dtIdentification As DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
                dtIdentification.Columns.Add(New DataColumn("PK_Identification_ID", GetType(String)))
                dtIdentification.Columns.Add(New DataColumn("Country", GetType(String)))
                Using objdb As New NawaDevDAL.NawaDatadevEntities
                    Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                    For Each row In dtIdentification.Rows
                        row("PK_Identification_ID") = row("PK_ACTIVITY_PERSON_IDENTIFICATION_ID")
                        If Not IsDBNull(row("Issued_Country")) Then
                            Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Issued_Country")).FirstOrDefault
                            If objCountry IsNot Nothing Then
                                row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                            End If
                        End If
                    Next
                End Using
                BindGridPanel(gp_Person_Identification, dtIdentification)
            Else

                Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Where(Function(x) x.PK_ACT_DIRECTOR_IDENTIFICATION_ID = Me.PK_Identification_ID).FirstOrDefault
                If objCek Is Nothing Then
                    Dim lngPK As Long = 0
                    If objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Count = 0 Then
                        lngPK = -1
                    Else
                        lngPK = objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Min(Function(x) x.PK_ACT_DIRECTOR_IDENTIFICATION_ID) - 1
                        If lngPK >= 0 Then
                            lngPK = -1
                        End If
                    End If

                    objCek = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_IDENTIFICATION
                    objCek.PK_ACT_DIRECTOR_IDENTIFICATION_ID = lngPK
                    Me.IsNewRecord = True
                Else
                    Me.IsNewRecord = False
                End If

                With objCek
                    .FK_REPORT_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                    .FK_ACT_DIRECTOR_ID = Me.PK_PartySub_ID
                    'If Me.PartySubType = 0 Then     'Level 1 (Person)
                    '    .FK_Act_Person_ID = Me.PK_Party_ID
                    '    .FK_Person_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.Person
                    'Else    'Level 2 (AccountSignatory/AccountEntityDirector/EntityDirector) 
                    '    .FK_Act_Person_ID = Me.PK_PartySub_ID
                    '    .FK_Person_Type = Me.PartySubType
                    'End If

                    .TYPE = cmb_Identification_Type.SelectedItemValue
                    .NUMBER = txt_Identification_Number.Value
                    If txt_Identification_IssueDate.SelectedDate <> DateTime.MinValue Then
                        .ISSUED_DATE = txt_Identification_IssueDate.SelectedDate
                    End If
                    If txt_Identification_ExpiredDate.SelectedDate <> DateTime.MinValue Then
                        .EXPIRY_DATE = txt_Identification_ExpiredDate.SelectedDate
                    End If
                    .ISSUED_BY = txt_Identification_IssueBy.Value
                    .ISSUED_COUNTRY = cmb_Identification_Country.SelectedItemValue
                    .IDENTIFICATION_COMMENT = txt_Identification_Comment.Value
                End With

                If Me.IsNewRecord Then
                    objCek.Active = True
                    objCek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objCek.CreatedDate = DateTime.Now
                    objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Add(objCek)
                Else
                    objCek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objCek.LastUpdateDate = DateTime.Now
                End If

                'Bind to gridPanel
                Dim listIdentification As List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_IDENTIFICATION) = Nothing
                'If Me.PartySubType = 0 Then     'Level 1 (Person)
                '    listIdentification = objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Where(Function(x) x.FK_ACT_ACC_SIGN_ID = Me.PK_Party_ID).ToList
                'Else    'Level 2 (AccountSignatory/AccountEntityDirector/EntityDirector) 
                '    listIdentification = objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Where(Function(x) x.FK_ACT_ACC_SIGN_ID = Me.PK_PartySub_ID).ToList
                'End If
                listIdentification = objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Where(Function(x) x.FK_ACT_DIRECTOR_ID = Me.PK_PartySub_ID).ToList

                If listIdentification Is Nothing Then
                    listIdentification = New List(Of SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_IDENTIFICATION)
                End If

                Dim dtIdentification As DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
                dtIdentification.Columns.Add(New DataColumn("PK_Identification_ID", GetType(String)))
                dtIdentification.Columns.Add(New DataColumn("Country", GetType(String)))
                Using objdb As New NawaDevDAL.NawaDatadevEntities
                    Dim listCountry = objdb.goAML_Ref_Nama_Negara.ToList
                    For Each row In dtIdentification.Rows
                        row("PK_Identification_ID") = row("PK_ACT_DIRECTOR_IDENTIFICATION_ID")
                        If Not IsDBNull(row("Issued_Country")) Then
                            Dim objCountry = listCountry.Where(Function(x) x.Kode = row("Issued_Country")).FirstOrDefault
                            If objCountry IsNot Nothing Then
                                row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                            End If
                        End If
                    Next
                End Using
                BindGridPanel(gp_Person_Identification, dtIdentification)
            End If


            'Close Window Identification
            window_Identification.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Identification_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            window_Identification.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Set Mandatory Fields"
    Protected Sub SetMandatoryFieldReport()
        Try
            txt_RentityID.FieldStyle = "background-color: #FFE4C4;"
            cmb_SubmisionCode.AllowBlank = False
            'cmb_ReportType.AllowBlank = False
            txt_CurrencyLocal.FieldStyle = "background-color: #FFE4C4;"
            'cmb_JenisLaporan.AllowBlank = False
            'cmb_JenisLaporan.AllowBlank = False
            cmb_JenisLaporan.FieldStyle = "background-color: #FFE4C4;"
            'txt_ReportUniqueValue.AllowBlank = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SetMandatoryFieldAccount()
        Try
            If Me.IsMyClient Then
                txt_Account_InstitutionName.FieldStyle = "background-color: #FFE4C4"
                txt_Account_Branch.FieldStyle = "background-color: #FFE4C4"
                txt_Account_Number.FieldStyle = "background-color: #FFE4C4"
                cmb_Account_CurrencyCode.AllowBlank = False
                txt_Account_ClientNumber.FieldStyle = "background-color: #FFE4C4"
                cmb_Account_Type.AllowBlank = False
                txt_Account_OpeningDate.FieldStyle = "background-color: #FFE4C4"
                cmb_Account_StatusCode.AllowBlank = False

                'Account Signatory
                gp_Account_Signatory.Title = "Account Signatory (Mandatory)"

                'Account Entity
                txt_Account_Entity_CorporateName.FieldStyle = "background-color: #FFE4C4"

                'Account Entity Address & Director
                gp_Account_Entity_Address.Title = "Address (Mandatory)"
                'gp_Account_Entity_Director.Title = "Director (Mandatory)"
            Else
                txt_Account_InstitutionName.FieldStyle = "background-color: #FFE4C4"
                txt_Account_Number.FieldStyle = "background-color: #FFE4C4"
                txt_Account_Branch.FieldStyle = "background-color: #FFFFFF"
                cmb_Account_CurrencyCode.AllowBlank = True
                txt_Account_ClientNumber.FieldStyle = "background-color: #FFFFFF"
                cmb_Account_Type.AllowBlank = True
                txt_Account_OpeningDate.FieldStyle = "background-color: #FFFFFF"
                cmb_Account_StatusCode.AllowBlank = True

                'Account Signatory
                gp_Account_Signatory.Title = "Account Signatory"

                'Account Entity
                txt_Account_Entity_CorporateName.FieldStyle = "background-color: #FFE4C4"

                'Account Entity Address & Director
                gp_Account_Entity_Address.Title = "Address"
                'gp_Account_Entity_Director.Title = "Director"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SetMandatoryFieldPerson()
        Try
            If Me.IsMyClient Then
                cmb_Person_Gender.AllowBlank = False
                txt_Person_LastName.FieldStyle = "background-color: #FFE4C4"
                txt_Person_BirthDate.FieldStyle = "background-color: #FFE4C4"
                txt_Person_BirthPlace.FieldStyle = "background-color: #FFE4C4"
                cmb_Person_Nationality1.AllowBlank = False
                cmb_Person_Residence.AllowBlank = False
                txt_Person_Occupation.FieldStyle = "background-color: #FFE4C4"
                txt_Person_SourceOfWealth.FieldStyle = "background-color: #FFE4C4"

                'Person Address & Phine
                gp_Person_Address.Title = "Address (Mandatory)"
                gp_Person_Phone.Title = "Phone (Mandatory)"

                'Account Signatory Role
                'cmb_Person_AccountSignatory_Role.AllowBlank = False

                'Director Role
                cmb_Person_Director_Role.AllowBlank = False
            Else
                cmb_Person_Gender.AllowBlank = True
                txt_Person_LastName.FieldStyle = "background-color: #FFE4C4"
                txt_Person_BirthDate.FieldStyle = "background-color: #FFFFFF"
                txt_Person_BirthPlace.FieldStyle = "background-color: #FFFFFF"
                cmb_Person_Nationality1.AllowBlank = True
                cmb_Person_Residence.AllowBlank = True
                txt_Person_Occupation.FieldStyle = "background-color: #FFFFFF"
                txt_Person_SourceOfWealth.FieldStyle = "background-color: #FFFFFF"

                'Person Address & Phine
                gp_Person_Address.Title = "Address"
                gp_Person_Phone.Title = "Phone"

                'Account Signatory Role
                'cmb_Person_AccountSignatory_Role.AllowBlank = False

                'Director Role
                cmb_Person_Director_Role.AllowBlank = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SetMandatoryFieldEntity()
        Try
            If Me.IsMyClient Then
                txt_Entity_CorporateName.FieldStyle = "background-color: #FFE4C4"
                txt_Entity_Business.FieldStyle = "background-color: #FFE4C4"
                cmb_Entity_IncorporationCountryCode.AllowBlank = False

                'Entity Address & Director
                gp_Entity_Address.Title = "Address (Mandatory)"
                gp_Entity_Director.Title = "Director (Mandatory)"
            Else
                txt_Entity_CorporateName.FieldStyle = "background-color: #FFE4C4"
                txt_Entity_Business.FieldStyle = "background-color: #FFFFFF"
                cmb_Entity_IncorporationCountryCode.AllowBlank = True

                'Entity Address & Director
                gp_Entity_Address.Title = "Address"
                gp_Entity_Director.Title = "Director"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SetMandatoryFieldAddressPhoneIdentification()
        Try
            'Address
            cmb_Address_Type.AllowBlank = False
            txt_Address_Address.AllowBlank = False
            txt_Address_City.AllowBlank = False
            cmb_Address_Country.AllowBlank = False

            'Phone
            cmb_Phone_ContactType.AllowBlank = False
            cmb_Phone_CommunicationType.AllowBlank = False
            txt_Phone_Number.AllowBlank = False

            'Identification
            cmb_Identification_Type.AllowBlank = False
            txt_Identification_Number.AllowBlank = False
            cmb_Identification_Country.AllowBlank = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub SetMandatoryFieldActivity()
    '    Try
    '        cmb_Activity_Information.AllowBlank = False
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Protected Sub SetMaxDate()
        Try
            'Report
            txt_SubmissionDate.MaxDate = DateTime.Now

            'Account
            'txt_Account_BalanceDate.MaxDate = DateTime.Now
            txt_Account_OpeningDate.MaxDate = DateTime.Now
            txt_Account_ClosingDate.MaxDate = DateTime.Now

            'Account Entity
            txt_Account_Entity_IncorporationDate.MaxDate = DateTime.Now
            'txt_Account_Entity_ClosingDate.MaxDate = DateTime.Now

            'Person
            txt_Person_BirthDate.MaxDate = DateTime.Now
            'txt_Person_DeceaseDate.MaxDate = DateTime.Now

            'Identification
            txt_Identification_IssueDate.MaxDate = DateTime.Now
            txt_Identification_ExpiredDate.MaxDate = DateTime.Now

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Clean Windows"
    Protected Sub CleanWindowAddress()
        Try
            cmb_Address_Type.SetTextValue("")
            txt_Address_Address.Value = Nothing
            txt_Address_Town.Value = Nothing
            txt_Address_City.Value = Nothing
            txt_Address_Zip.Value = Nothing
            cmb_Address_Country.SetTextValue("")
            txt_Address_State.Value = Nothing
            txt_Address_Comment.Value = Nothing

            cmb_Address_Country_Changed()

            'Set Mandatory Fields
            SetMandatoryFieldAddressPhoneIdentification()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub CleanWindowPhone()
        Try
            cmb_Phone_ContactType.SetTextValue("")
            cmb_Phone_CommunicationType.SetTextValue("")
            txt_Phone_CountryPrefix.Value = Nothing
            txt_Phone_Number.Value = Nothing
            txt_Phone_Extension.Value = Nothing
            txt_Phone_Comment.Value = Nothing

            'Set Mandatory Fields
            SetMandatoryFieldAddressPhoneIdentification()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub CleanWindowIdentification()
        Try
            cmb_Identification_Type.SetTextValue("")
            txt_Identification_Number.Value = Nothing
            txt_Identification_IssueDate.Value = Nothing
            txt_Identification_ExpiredDate.Value = Nothing
            txt_Identification_IssueBy.Value = Nothing
            cmb_Identification_Country.SetTextValue("")
            txt_Identification_Comment.Value = Nothing

            'Set Mandatory Fields
            SetMandatoryFieldAddressPhoneIdentification()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub CleanWindowAccountParty()
        Try
            txt_Account_InstitutionName.Value = Nothing
            txt_Account_InstitutionCode.Value = Nothing
            txt_Account_SwiftCode.Value = Nothing
            'chk_Account_NonBankingInstitution.Checked = False
            txt_Account_Branch.Value = Nothing
            txt_Account_Number.Value = Nothing
            cmb_Account_CurrencyCode.SetTextValue("")
            'txt_Account_Label.Value = Nothing
            'txt_Account_IBAN.Value = Nothing
            txt_Account_ClientNumber.Value = Nothing
            cmb_Account_Type.SetTextValue("")
            txt_Account_OpeningDate.Value = Nothing
            txt_Account_ClosingDate.Value = Nothing
            'txt_Account_Balance.Value = Nothing
            'txt_Account_BalanceDate.Value = Nothing
            cmb_Account_StatusCode.SetTextValue("")
            'txt_Account_Beneficiary.Value = Nothing
            'txt_Account_BeneficiaryComment.Value = Nothing
            txt_Account_Comment.Value = Nothing

            'Entity Account
            'chk_Account_IsEntity.Checked = False
            pnl_Account_Entity.Hidden = True

            txt_Account_Entity_CorporateName.Value = Nothing
            'txt_Account_Entity_CommercialName.Value = Nothing
            'cmb_Account_Entity_IncorporationLegalForm.SetTextValue("")
            'txt_Account_Entity_IncorporationNumber.Value = Nothing
            txt_Account_Entity_Business.Value = Nothing
            'txt_Account_Entity_Email.Value = Nothing
            'txt_Account_Entity_URL.Value = Nothing
            txt_Account_Entity_IncorporationState.Value = Nothing
            cmb_Account_Entity_IncorporationCountryCode.SetTextValue("")
            txt_Account_Entity_IncorporationDate.Value = Nothing
            'chk_Account_Entity_IsClosed.Checked = False
            'txt_Account_Entity_ClosingDate.Hidden = True
            'txt_Account_Entity_ClosingDate.Value = Nothing
            txt_Account_Entity_TaxNumber.Value = Nothing
            'txt_Account_Entity_Comment.Value = Nothing

            'Bind Empty GridPanel
            If Me.IsNewRecord Then
                BindGridPanel(gp_Account_Signatory, New DataTable)
                BindGridPanel(gp_Account_Entity_Phone, New DataTable)
                BindGridPanel(gp_Account_Entity_Address, New DataTable)
                'BindGridPanel(gp_Account_Entity_Director, New DataTable)
            End If

            'Set Mandatory Fields
            SetMandatoryFieldAccount()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub CleanWindowPersonParty()
        Try
            cmb_Person_Gender.SetTextValue("")
            txt_Person_Title.Value = Nothing
            txt_Person_LastName.Value = Nothing
            txt_Person_BirthDate.Value = Nothing
            txt_Person_BirthPlace.Value = Nothing
            txt_Person_MotherName.Value = Nothing
            txt_Person_Alias.Value = Nothing

            txt_Person_SSN.Value = Nothing
            txt_Person_PassportNumber.Value = Nothing
            cmb_Person_PassportCountry.SetTextValue("")
            txt_Person_IDNumber.Value = Nothing

            cmb_Person_Nationality1.SetTextValue("")
            cmb_Person_Nationality2.SetTextValue("")
            cmb_Person_Nationality3.SetTextValue("")
            cmb_Person_Residence.SetTextValue("")

            txt_Person_Email1.Value = Nothing
            txt_Person_Email2.Value = Nothing
            txt_Person_Email3.Value = Nothing
            txt_Person_Email4.Value = Nothing
            txt_Person_Email5.Value = Nothing

            txt_Person_Occupation.Value = Nothing
            txt_Person_EmployerName.Value = Nothing
            chk_Person_IsDecease.Checked = False
            txt_Person_DeceaseDate.Value = Nothing
            txt_Person_DeceaseDate.Hidden = True

            txt_Person_TaxNumber.Value = Nothing
            chk_Person_IsPEP.Checked = False
            txt_Person_SourceOfWealth.Value = Nothing
            txt_Person_Comment.Value = Nothing

            'chk_Person_AccountSignatory_IsPrimary.Checked = False
            'chk_Person_AccountSignatory_IsPrimary.Hidden = True
            'cmb_Person_AccountSignatory_Role.SetTextValue("")
            'cmb_Person_AccountSignatory_Role.IsHidden = True
            cmb_Person_Director_Role.SetTextValue("")
            cmb_Person_Director_Role.IsHidden = True

            'Bind Empty GridPanel
            If Me.IsNewRecord Then
                BindGridPanel(gp_Person_Address, New DataTable)
                BindGridPanel(gp_Person_EmployerAddress, New DataTable)
                BindGridPanel(gp_Person_Phone, New DataTable)
                BindGridPanel(gp_Person_EmployerPhone, New DataTable)
                BindGridPanel(gp_Person_Identification, New DataTable)
            End If

            'Set Mandatory Fields
            SetMandatoryFieldPerson()

            'Hide/Show Role
            'chk_Person_AccountSignatory_IsPrimary.Hidden = True
            'cmb_Person_AccountSignatory_Role.IsHidden = True
            cmb_Person_Director_Role.IsHidden = True

            If Me.PartySubType <> 0 Then
                'If Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.Signatory Then
                '    chk_Person_AccountSignatory_IsPrimary.Hidden = False
                '    cmb_Person_AccountSignatory_Role.IsHidden = False
                'Else
                If Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount Or Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity Then
                    cmb_Person_Director_Role.IsHidden = False
                End If

                btn_Person_Import_Customer.Hidden = True
                'btn_Person_Import_WIC.Hidden = True
            Else
                btn_Person_Import_Customer.Hidden = False
                'btn_Person_Import_WIC.Hidden = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub CleanWindowEntityParty()
        Try
            txt_Entity_CorporateName.Value = Nothing
            txt_Entity_CommercialName.Value = Nothing
            cmb_Entity_IncorporationLegalForm.SetTextValue("")
            txt_Entity_IncorporationNumber.Value = Nothing
            txt_Entity_Business.Value = Nothing
            txt_Entity_Email.Value = Nothing
            txt_Entity_URL.Value = Nothing
            txt_Entity_IncorporationState.Value = Nothing
            cmb_Entity_IncorporationCountryCode.SetTextValue("")
            txt_Entity_IncorporationDate.Value = Nothing
            chk_Entity_IsClosed.Checked = False
            txt_Entity_ClosingDate.Hidden = True
            txt_Entity_ClosingDate.Value = Nothing
            txt_Entity_TaxNumber.Value = Nothing
            txt_Entity_Comment.Value = Nothing

            'Bind Empty GridPanel
            If Me.IsNewRecord Then
                BindGridPanel(gp_Entity_Phone, New DataTable)
                BindGridPanel(gp_Entity_Address, New DataTable)
                BindGridPanel(gp_Entity_Director, New DataTable)
            End If

            'Set Mandatory Fields
            SetMandatoryFieldEntity()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub CleanWindowActivity()
        Try
            'Clean Validation Result
            Dim strValidationResult As String = ""
            SIPESATGoAMLBLL.NawaFramework.extInfoPanelupdate(FormPanelReport, strValidationResult, "info_ValidationResultActivity")
            info_ValidationResultActivity.Hidden = True

            cmb_Activity_Information.SetTextValue("")
            txt_Activity_Significance.Value = 0
            txt_Activity_Reason.Value = Nothing
            txt_Activity_Comment.Value = Nothing

            'Activity Party short information
            txt_Activity_CIFNO_ACCOUNTNO.Value = Nothing
            df_Activity_Name.Text = ""
            txt_Activity_Name.Value = Nothing
            'txt_Activity_Address.Value = Nothing

            'Relationship
            TxtClientNumber_Relationship.Value = Nothing
            txtComments_Relationship.Value = Nothing
            TxtValidFrom_Relationship.Value = Nothing
            TxtValidTo_Relationship.Value = Nothing
            txt_ClientNumber_Relationship_Entity.Value = Nothing
            txt_Comments_Relationship_Entity.Value = Nothing
            txt_Validto_Relationship_Entity.Value = Nothing
            txt_Validfrom_Relationship_Entity.Value = Nothing


            'Set Mandatory Fields
            'SetMandatoryFieldActivity()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Validation"
    Protected Sub ValidateAddress()
        Try
            If String.IsNullOrEmpty(cmb_Address_Type.SelectedItemValue) Then
                Throw New ApplicationException(cmb_Address_Type.Label & " is required.")
            End If
            If String.IsNullOrWhiteSpace(txt_Address_Address.Value) Then
                Throw New ApplicationException(txt_Address_Address.FieldLabel & " is required.")
            End If
            If String.IsNullOrWhiteSpace(txt_Address_City.Value) Then
                Throw New ApplicationException(txt_Address_City.FieldLabel & " is required.")
            End If
            If String.IsNullOrEmpty(cmb_Address_Country.SelectedItemValue) Then
                Throw New ApplicationException(cmb_Address_Country.Label & " is required.")
            End If

            If cmb_Address_Country.SelectedItemValue = "ID" And String.IsNullOrWhiteSpace(txt_Address_State.Value) Then
                Throw New ApplicationException(txt_Address_State.FieldLabel & " is required if Country = ID.")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ValidatePhone()
        Try
            If String.IsNullOrEmpty(cmb_Phone_ContactType.SelectedItemValue) Then
                Throw New ApplicationException(cmb_Phone_ContactType.Label & " is required.")
            End If
            If String.IsNullOrEmpty(cmb_Phone_CommunicationType.SelectedItemValue) Then
                Throw New ApplicationException(cmb_Phone_CommunicationType.Label & " is required.")
            End If
            If String.IsNullOrWhiteSpace(txt_Phone_Number.Value) Then
                Throw New ApplicationException(txt_Phone_Number.FieldLabel & " is required.")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ValidateIdentification()
        Try
            If String.IsNullOrEmpty(cmb_Identification_Type.SelectedItemValue) Then
                Throw New ApplicationException(cmb_Identification_Type.Label & " is required.")
            End If
            If String.IsNullOrWhiteSpace(txt_Identification_Number.Value) Then
                Throw New ApplicationException(txt_Identification_Number.FieldLabel & " is required.")
            End If
            If String.IsNullOrEmpty(cmb_Identification_Country.SelectedItemValue) Then
                Throw New ApplicationException(cmb_Identification_Country.Label & " is required.")
            End If

            'Validate Issue Date and Expired Date
            Dim strMessage As String = "Identification Expired Date must greater than or equal Issued Date"
            Dim IssuedDate As DateTime
            Dim ExpiredDate As DateTime

            IssuedDate = CDate(txt_Identification_IssueDate.Value)
            ExpiredDate = CDate(txt_Identification_ExpiredDate.Value)

            If (IssuedDate <> DateTime.MinValue Or ExpiredDate <> DateTime.MinValue) AndAlso IssuedDate > ExpiredDate Then
                Throw New ApplicationException(strMessage)
            End If

            'Validate KTP must 16 digit numeric
            strMessage = "KTP must contain 16 digit numeric only."
            If cmb_Identification_Type.SelectedItemValue = "KTP" Then
                If Not String.IsNullOrEmpty(txt_Identification_Number.Value) Then
                    Dim strSSN As String = txt_Identification_Number.Value
                    If (Len(strSSN) <> 16 Or Not strSSN.All(AddressOf Char.IsDigit)) Then
                        Throw New ApplicationException(strMessage)
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ValidateAccount()
        Try
            'Validate Email
            'If Not String.IsNullOrWhiteSpace(txt_Account_Entity_Email.Value) And Not ValidateEmail(txt_Account_Entity_Email.Value) Then
            '    Throw New ApplicationException(txt_Account_Entity_Email.FieldLabel & " is not valid.")
            'End If

            'Per 3-Feb 2021 Ada update terkait Instituion Name itu wajib baik MC/NMC, juga Institution Code or Swift Code (Salah satu)
            If String.IsNullOrWhiteSpace(txt_Account_InstitutionName.Value) Then
                Throw New ApplicationException(txt_Account_InstitutionName.FieldLabel & " is required.")
            End If

            'PJK/Swift Code required
            If String.IsNullOrWhiteSpace(txt_Account_InstitutionCode.Value) And String.IsNullOrWhiteSpace(txt_Account_SwiftCode.Value) Then
                Throw New ApplicationException("Institution Code/Swift Code is required.")
            End If

            If Me.IsMyClient Then   'My Client
                If String.IsNullOrWhiteSpace(txt_Account_Branch.Value) Then
                    Throw New ApplicationException(txt_Account_Branch.FieldLabel & " is required.")
                End If
                If String.IsNullOrWhiteSpace(txt_Account_Number.Value) Then
                    Throw New ApplicationException(txt_Account_Number.FieldLabel & " is required.")
                End If
                If String.IsNullOrEmpty(cmb_Account_CurrencyCode.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_Account_CurrencyCode.Label & " is required.")
                End If
                If String.IsNullOrWhiteSpace(txt_Account_ClientNumber.Value) Then
                    Throw New ApplicationException(txt_Account_ClientNumber.FieldLabel & " is required.")
                End If
                If String.IsNullOrEmpty(cmb_Account_Type.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_Account_Type.Label & " is required.")
                End If
                If txt_Account_OpeningDate.SelectedDate = DateTime.MinValue Then
                    Throw New ApplicationException(txt_Account_OpeningDate.FieldLabel & " is required.")
                End If
                If String.IsNullOrEmpty(cmb_Account_StatusCode.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_Account_StatusCode.Label & " is required.")
                End If

                ''Account Entity
                'If chk_Account_IsEntity.Checked Then
                '    If String.IsNullOrWhiteSpace(txt_Account_Entity_CorporateName.Value) Then
                '        Throw New ApplicationException(txt_Account_Entity_CorporateName.FieldLabel & " is required.")
                '    End If
                'End If

                ''================ Account Signatory
                'Dim listSignatory = objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Where(Function(x) x.FK_ACT_ACCOUNT_ID = Me.PK_Party_ID).ToList
                'If listSignatory Is Nothing Then
                '    Throw New ApplicationException("Account Signatory is required.")
                'End If

                ''Account Entity
                'If chk_Account_IsEntity.Checked Then
                '    'Account Entity Address
                '    Dim listAddress = objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Address.Where(Function(x) x.FK_ACT_ENTITY_ACCOUNT_ID = Me.PK_Party_ID).ToList
                '    If listAddress Is Nothing Then
                '        Throw New ApplicationException("Account Entity Address is required.")
                '    End If

                '    'Account Entity Director
                '    'Dim listDirector = objGoAML_Activity_Class.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = Me.PK_Party_ID).ToList
                '    'If listDirector Is Nothing Then
                '    '    Throw New ApplicationException("Account Entity Director is required.")
                '    'End If
                'End If
            Else    'Not My Client
                If String.IsNullOrWhiteSpace(txt_Account_Number.Value) Then
                    Throw New ApplicationException(txt_Account_Number.FieldLabel & " is required.")
                End If

                'Account Entity
                'If chk_Account_IsEntity.Checked Then
                '    If String.IsNullOrWhiteSpace(txt_Account_Entity_CorporateName.Value) Then
                '        Throw New ApplicationException(txt_Account_Entity_CorporateName.FieldLabel & " is required.")
                '    End If
                'End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ValidatePerson()
        Try
            'Validate NIK
            If Not String.IsNullOrWhiteSpace(txt_Person_SSN.Value) Then
                Dim strSSN As String = txt_Person_SSN.Value
                If (Len(strSSN) <> 16 Or Not strSSN.All(AddressOf Char.IsDigit)) Then
                    Throw New ApplicationException("SSN/KTP must 16 digit numeric only.")
                End If
            End If

            If Me.PartyType = enumPartyType._Person Then
                If TxtValidFrom_Relationship.SelectedDate = DateTime.MinValue Then
                    Throw New ApplicationException(TxtValidFrom_Relationship.FieldLabel & " Is Required")
                End If
                If TxtValidTo_Relationship.SelectedDate = DateTime.MinValue Then
                    Throw New ApplicationException(TxtValidTo_Relationship.FieldLabel & " Is Required")
                End If
                Dim strMessage As String = "Relationship Valid To Date must greater than or equal Valid From"
                Dim strMessage2 As String = "Relationship Valid From Date not greater than Valid From"
                Dim strSection As String = Nothing
                Dim OpeningDate As DateTime
                Dim ClosedDate As DateTime

                OpeningDate = CDate(TxtValidFrom_Relationship.Value)
                ClosedDate = CDate(TxtValidTo_Relationship.Value)
                If ClosedDate = DateTime.MinValue Then
                Else
                    If ClosedDate <> DateTime.MinValue AndAlso OpeningDate > ClosedDate Then Throw New ApplicationException(strMessage)
                    If OpeningDate <> DateTime.MinValue AndAlso ClosedDate < OpeningDate Then Throw New ApplicationException(strMessage2)
                End If
            End If

            'Validate Email
            'If Not String.IsNullOrWhiteSpace(txt_Person_Email1.Value) And Not ValidateEmail(txt_Person_Email1.Value) Then
            '    Throw New ApplicationException(txt_Person_Email1.FieldLabel & " is not valid.")
            'End If
            'If Not String.IsNullOrWhiteSpace(txt_Person_Email2.Value) And Not ValidateEmail(txt_Person_Email2.Value) Then
            '    Throw New ApplicationException(txt_Person_Email2.FieldLabel & " is not valid.")
            'End If
            'If Not String.IsNullOrWhiteSpace(txt_Person_Email3.Value) And Not ValidateEmail(txt_Person_Email3.Value) Then
            '    Throw New ApplicationException(txt_Person_Email3.FieldLabel & " is not valid.")
            'End If
            'If Not String.IsNullOrWhiteSpace(txt_Person_Email4.Value) And Not ValidateEmail(txt_Person_Email4.Value) Then
            '    Throw New ApplicationException(txt_Person_Email4.FieldLabel & " is not valid.")
            'End If
            'If Not String.IsNullOrWhiteSpace(txt_Person_Email5.Value) And Not ValidateEmail(txt_Person_Email5.Value) Then
            '    Throw New ApplicationException(txt_Person_Email5.FieldLabel & " is not valid.")
            'End If


            If Me.IsMyClient Then
                If String.IsNullOrEmpty(cmb_Person_Gender.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_Person_Gender.Label & " is required.")
                End If
                If String.IsNullOrWhiteSpace(txt_Person_LastName.Value) Then
                    Throw New ApplicationException(txt_Person_LastName.FieldLabel & " is required.")
                End If
                If txt_Person_BirthDate.SelectedDate = DateTime.MinValue Then
                    Throw New ApplicationException(txt_Person_BirthDate.FieldLabel & " is required.")
                End If
                If String.IsNullOrWhiteSpace(txt_Person_BirthPlace.Value) Then
                    Throw New ApplicationException(txt_Person_BirthPlace.FieldLabel & " is required.")
                End If
                If String.IsNullOrEmpty(cmb_Person_Nationality1.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_Person_Nationality1.Label & " is required.")
                End If
                If String.IsNullOrEmpty(cmb_Person_Residence.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_Person_Residence.Label & " is required.")
                End If
                If String.IsNullOrWhiteSpace(txt_Person_Occupation.Value) Then
                    Throw New ApplicationException(txt_Person_Occupation.FieldLabel & " is required.")
                End If
                If String.IsNullOrWhiteSpace(txt_Person_SourceOfWealth.Value) Then
                    Throw New ApplicationException(txt_Person_SourceOfWealth.FieldLabel & " is required.")
                End If

                ''================ Account Signatory
                'If Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.Signatory Then
                '    'Address
                '    Dim listAddress = objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = Me.PK_PartySub_ID And IsEmployer = 0).ToList
                '    If listAddress Is Nothing Then
                '        Throw New ApplicationException("Address is required.")
                '    End If

                '    'Phone
                '    Dim listPhone = objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = Me.PK_PartySub_ID And IsEmployer = 0).ToList
                '    If listPhone Is Nothing Then
                '        Throw New ApplicationException("Phone is required.")
                '    End If
                'End If

                '''================= Account Entity Director or Entity Director (Tablenya sama untuk Bi-Party Activity)
                'If Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount Then
                '    'Address
                '    Dim listAddress = objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = Me.PK_PartySub_ID And IsEmployer = 0).ToList
                '    If listAddress Is Nothing Then
                '        Throw New ApplicationException("Address is required.")
                '    End If

                '    'Phone
                '    Dim listPhone = objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = Me.PK_PartySub_ID And IsEmployer = 0).ToList
                '    If listPhone Is Nothing Then
                '        Throw New ApplicationException("Phone is required.")
                '    End If
                'ElseIf Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity Then
                '    'Address
                '    Dim listAddress = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Where(Function(x) x.FK_Act_Director_ID = Me.PK_PartySub_ID And IsEmployer = 0).ToList
                '    If listAddress Is Nothing Then
                '        Throw New ApplicationException("Address is required.")
                '    End If

                '    'Phone
                '    Dim listPhone = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Where(Function(x) x.FK_Act_Director_ID = Me.PK_PartySub_ID And IsEmployer = 0).ToList
                '    If listPhone Is Nothing Then
                '        Throw New ApplicationException("Phone is required.")
                '    End If
                'End If

                ''=============  Person
                'If Me.PartyType = enumPartyType._Person Then
                '    'Address
                '    Dim listAddress = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Where(Function(x) x.FK_Act_Person = Me.PK_Party_ID And IsEmployer = 0).ToList
                '    If listAddress Is Nothing Then
                '        Throw New ApplicationException("Address is required.")
                '    End If

                '    'Phone
                '    Dim listPhone = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Where(Function(x) x.FK_Act_Person = Me.PK_Party_ID And IsEmployer = 0).ToList
                '    If listPhone Is Nothing Then
                '        Throw New ApplicationException("Phone is required.")
                '    End If
                'End If
            Else
                If String.IsNullOrWhiteSpace(txt_Person_LastName.Value) Then
                    Throw New ApplicationException(txt_Person_LastName.FieldLabel & " is required.")
                End If
            End If
            'Signatory & Director Role Mandatory both MyClient/Not MyClient
            If Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.Signatory Then
                'Account Signatory Role
                'If String.IsNullOrEmpty(cmb_Person_AccountSignatory_Role.SelectedItemValue) Then
                '    Throw New ApplicationException(cmb_Person_AccountSignatory_Role.Label & " is required.")
                'End If
            ElseIf Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount Or Me.PartySubType = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity Then
                If String.IsNullOrEmpty(cmb_Person_Director_Role.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_Person_Director_Role.Label & " is required.")
                End If
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ValidateEntity()
        Try

            'Validate Email
            'If Not String.IsNullOrWhiteSpace(txt_Entity_Email.Value) And Not ValidateEmail(txt_Entity_Email.Value) Then
            '    Throw New ApplicationException(txt_Entity_Email.FieldLabel & " is not valid.")
            'End If


            If String.IsNullOrWhiteSpace(txt_Entity_CorporateName.Value) Then
                    Throw New ApplicationException(txt_Entity_CorporateName.FieldLabel & " is required.")
                End If
            'If String.IsNullOrWhiteSpace(txt_Entity_Business.Value) Then
            '    Throw New ApplicationException(txt_Entity_Business.FieldLabel & " is required.")
            'End If
            'If String.IsNullOrEmpty(cmb_Entity_IncorporationCountryCode.SelectedItemValue) Then
            '    Throw New ApplicationException(cmb_Entity_IncorporationCountryCode.Label & " is required.")
            'End If


            'Dim listAddress = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Where(Function(x) x.FK_ACT_ENTITY_ID = Me.PK_Party_ID).ToList
            'If listAddress Is Nothing Then
            '    Throw New ApplicationException("Entity Address is required.")
            'End If

            ''Account Entity Director
            'Dim listDirector = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Where(Function(x) x.FK_ACT_DIRECTOR_ID = Me.PK_Party_ID).ToList
            'If listDirector Is Nothing Then
            '    Throw New ApplicationException("Entity Director is required.")
            'End If

            If txt_Validfrom_Relationship_Entity.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException(txt_Validfrom_Relationship_Entity.FieldLabel & " Is Required")
            End If
            If txt_Validto_Relationship_Entity.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException(txt_Validto_Relationship_Entity.FieldLabel & " Is Required")
            End If
            Dim strMessage As String = "Relationship Valid To Date must greater than or equal Valid From"
            Dim strMessage2 As String = "Relationship Valid From Date not greater than Valid From"
            Dim strSection As String = Nothing
            Dim OpeningDate As DateTime
            Dim ClosedDate As DateTime

            OpeningDate = CDate(txt_Validfrom_Relationship_Entity.Value)
            ClosedDate = CDate(txt_Validto_Relationship_Entity.Value)
            If ClosedDate = DateTime.MinValue Then
            Else
                If ClosedDate <> DateTime.MinValue AndAlso OpeningDate > ClosedDate Then Throw New ApplicationException(strMessage)
                If OpeningDate <> DateTime.MinValue AndAlso ClosedDate < OpeningDate Then Throw New ApplicationException(strMessage2)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ValidateEmail(emailAddress As String) As Boolean
        If String.IsNullOrEmpty(emailAddress) Then
            Return True
        End If
        Dim email As New Regex("([\w-+]+(?:\.[\w-+]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7})")
        If email.IsMatch(emailAddress) Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub ValidateActivity()
        Try
            'If String.IsNullOrEmpty(cmb_Activity_Information.SelectedItemValue) Then
            '    Throw New ApplicationException(cmb_Activity_Information.Label + " is required.")
            'End If
            'If objGoAML_Activity_Class.list_goAML_Act_Account.Count < 1 Then
            '    Throw New ApplicationException("Party information is required.")
            'End If
            If String.IsNullOrEmpty(txt_Activity_Name.Value) Then
                Throw New ApplicationException("CIF Not Registered as a Customer.")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ValidateReport()
        Try

            'General Info
            'If String.IsNullOrEmpty(cmb_JenisLaporan.SelectedItemValue) Then
            '    Throw New ApplicationException(cmb_JenisLaporan.Label + " is required.")
            'End If
            'If String.IsNullOrEmpty(cmb_ReportType.SelectedItemValue) Then
            '    Throw New ApplicationException(cmb_ReportType.Label + " is required.")
            'End If
            'If String.IsNullOrEmpty(txt_ReportUniqueValue.Value) Then
            '    Throw New ApplicationException(txt_ReportUniqueValue.FieldLabel + " is required.")
            'End If
            If String.IsNullOrEmpty(df_MonthPeriod.SelectedItemValue) Then
                Throw New ApplicationException(df_MonthPeriod.Label + " is required.")
            End If
            If String.IsNullOrEmpty(cmb_SubmisionCode.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SubmisionCode.Label + " is required.")
            End If
            If txt_SubmissionDate.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException(txt_SubmissionDate.FieldLabel + " is required.")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ValidateTransactionActivityIndicatorCount()
        Try

            If objGoAML_Report_Class.list_goAML_Act_ReportPartyType Is Nothing OrElse objGoAML_Report_Class.list_goAML_Act_ReportPartyType.Count = 0 Then
                Throw New ApplicationException("Minimum 1 Activity is required.")
            End If

            'Validate Indicator
            'If GoAML_Report_BLL_.isRequireIndicator(cmb_JenisLaporan.SelectedItemValue) Then
            '    If objGoAML_Report_Class.list_goAML_Report_Indicator Is Nothing OrElse objGoAML_Report_Class.list_goAML_Report_Indicator.Count = 0 Then
            '        Throw New ApplicationException("Minimum 1 indicator is required.")
            '    End If
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Delete unnecessary party data"
    'Example :
    '1. if user choose Bi-Party, then all Multi-Party Related Data must be deleted
    '2. if user choose Bi-Party Account From, then all Person From and Entity From related data must be deleted

    '=============== Activity
    Protected Sub Delete_Activity_Account_RelatedData()
        Try
            ''Account
            'Dim listAccount = objGoAML_Activity_Class.list_goAML_Act_Account.Where(Function(x) x.FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID).ToList
            'If listAccount IsNot Nothing Then
            '    For Each objAccount In listAccount
            '        objGoAML_Activity_Class.list_goAML_Act_Account.Remove(objAccount)

            '        'Account Signatory
            '        Dim listSignatory = objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Where(Function(x) x.FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID).ToList
            '        For Each objSignatory In listSignatory
            '            objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Remove(objSignatory)

            '            'Address
            '            Dim listAddress = objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = objSignatory.PK_ACT_ACC_SIGNATORY_ID).ToList
            '            For Each objAddress In listAddress
            '                objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Address.Remove(objAddress)
            '            Next

            '            'Phone
            '            Dim listPhone = objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = objSignatory.PK_ACT_ACC_SIGNATORY_ID).ToList
            '            For Each objPhone In listPhone
            '                objGoAML_Activity_Class.list_goAML_Act_Acc_sign_Phone.Remove(objPhone)
            '            Next

            '            'Identification
            '            Dim listIdentification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_ACT_ACC_SIGN_ID = objSignatory.PK_ACT_ACC_SIGNATORY_ID).ToList
            '            For Each objIdentification In listIdentification
            '                objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Remove(objIdentification)
            '            Next
            '        Next

            '        'Account Entity
            '        Dim listAccountEntity = objGoAML_Activity_Class.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID)
            '        If listAccountEntity IsNot Nothing Then
            '            For Each objAccountEntity In listAccountEntity
            '                objGoAML_Activity_Class.list_goAML_Act_Entity_Account.Remove(objAccountEntity)

            '                'Address
            '                Dim listAddress = objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Address.Where(Function(x) x.FK_ACT_ENTITY_ACCOUNT_ID = objAccountEntity.PK_ACT_ENTITY_ACCOUNT_ID).ToList
            '                For Each objAddress In listAddress
            '                    objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Address.Remove(objAddress)
            '                Next

            '                'Phone
            '                Dim listPhone = objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Phone.Where(Function(x) x.FK_ACT_ACC_ENTITY_ID = objAccountEntity.PK_ACT_ENTITY_ACCOUNT_ID).ToList
            '                For Each objPhone In listPhone
            '                    objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Phone.Remove(objPhone)
            '                Next

            '                'Account Entity Director
            '                'Dim listDirector = objGoAML_Activity_Class.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = objAccountEntity.PK_SIPESATGOAML_Act_Entity_account).ToList
            '                'For Each objDirector In listDirector
            '                '    objGoAML_Activity_Class.list_goAML_Act_Acc_Ent_Director.Remove(objDirector)

            '                '    'Address
            '                '    Dim listAddressDirector = objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = objDirector.PK_Act_Acc_Ent_Director_ID).ToList
            '                '    For Each objAddressDirector In listAddressDirector
            '                '        objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Director_address.Remove(objAddressDirector)
            '                '    Next

            '                '    'Phone 
            '                '    Dim listPhoneDirector = objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = objDirector.PK_Act_Acc_Ent_Director_ID).ToList
            '                '    For Each objPhoneDirector In listPhoneDirector
            '                '        objGoAML_Activity_Class.list_goAML_Act_Acc_Entity_Director_Phone.Remove(objPhoneDirector)
            '                '    Next

            '                '    'Identification
            '                '    Dim listIdentification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount And x.FK_Act_Person_ID = objDirector.PK_Act_Acc_Ent_Director_ID).ToList
            '                '    For Each objIdentification In listIdentification
            '                '        objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Remove(objIdentification)
            '                '    Next
            '                'Next
            '            Next
            '        End If
            '    Next
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Delete_Activity_Person_RelatedData()
        Try
            Dim listPerson = objGoAML_Activity_Class.list_goAML_Act_Person.Where(Function(x) x.FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID).ToList
            If listPerson IsNot Nothing Then
                For Each objPerson In listPerson
                    objGoAML_Activity_Class.list_goAML_Act_Person.Remove(objPerson)

                    'Address
                    Dim listAddress = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Where(Function(x) x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID).ToList
                    For Each objAddress In listAddress
                        objGoAML_Activity_Class.list_goAML_Act_Person_Address.Remove(objAddress)
                    Next

                    'Phone
                    Dim listPhone = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Where(Function(x) x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID).ToList
                    For Each objPhone In listPhone
                        objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Remove(objPhone)
                    Next

                    'Identification
                    Dim listIdentification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_ACT_PERSON = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.Person And x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID).ToList
                    For Each objIdentification In listIdentification
                        objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Remove(objIdentification)
                    Next
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Delete_Activity_Entity_RelatedData()
        Try
            Dim listEntity = objGoAML_Activity_Class.list_goAML_Act_Entity.Where(Function(x) x.FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID).ToList
            If listEntity IsNot Nothing Then
                For Each objEntity In listEntity
                    objGoAML_Activity_Class.list_goAML_Act_Entity.Remove(objEntity)

                    'Address
                    Dim listAddress = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
                    For Each objAddress In listAddress
                        objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Remove(objAddress)
                    Next

                    'Phone
                    Dim listPhone = objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
                    For Each objPhone In listPhone
                        objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Remove(objPhone)
                    Next

                    'Entity Director
                    Dim listDirector = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
                    'For Each objDirector In listDirector
                    '    objGoAML_Activity_Class.list_goAML_Act_Director.Remove(objDirector)

                    '    'Address
                    '    Dim listAddressDirector = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objDirector.PK_SIPESATGOAML_Act_Director_ID).ToList
                    '    For Each objAddressDirector In listAddressDirector
                    '        objGoAML_Activity_Class.list_goAML_Act_Director_Address.Remove(objAddressDirector)
                    '    Next

                    '    'Phone 
                    '    Dim listPhoneDirector = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objDirector.PK_SIPESATGOAML_Act_Director_ID).ToList
                    '    For Each objPhoneDirector In listPhoneDirector
                    '        objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Remove(objPhoneDirector)
                    '    Next

                    '    'Identification
                    '    Dim listIdentification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity And x.FK_Act_Person_ID = objDirector.PK_SIPESATGOAML_Act_Director_ID).ToList
                    '    For Each objIdentification In listIdentification
                    '        objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Remove(objIdentification)
                    '    Next
                    'Next
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Get PK for New Record"
    Protected Sub get_Existing_PK_Party_ID()
        Try
            Select Case Me.PartyType
                'Case enumPartyType._Account
                '    Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Account.Where(Function(x) x.FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID).FirstOrDefault
                '    If objCek IsNot Nothing Then
                '        Me.PK_Party_ID = objCek.PK_ACT_ACCOUNT_ID
                '    Else
                '        get_New_PK_Party_ID()
                '    End If

                '    Account Entity
                '    Dim objCekAccEnt = objGoAML_Activity_Class.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_ACT_ACCOUNT_ID = Me.PK_Party_ID).FirstOrDefault
                '    If objCekAccEnt IsNot Nothing Then
                '        Me.PK_Party_AccountEntity_ID = objCekAccEnt.PK_ACT_ENTITY_ACCOUNT_ID
                '    Else
                '        get_New_PK_Party_AccountEntity_ID()
                '    End If
                Case enumPartyType._Person
                    Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Person.Where(Function(x) x.FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID).FirstOrDefault
                    If objCek IsNot Nothing Then
                        Me.PK_Party_ID = objCek.PK_SIPESATGOAML_ACT_PERSON_ID
                    Else
                        get_New_PK_Party_ID()
                    End If
                Case enumPartyType._Entity
                    Dim objCek = objGoAML_Activity_Class.list_goAML_Act_Entity.Where(Function(x) x.FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID).FirstOrDefault
                    If objCek IsNot Nothing Then
                        Me.PK_Party_ID = objCek.PK_SIPESATGOAML_ACT_ENTITY_ID
                    Else
                        get_New_PK_Party_ID()
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub get_New_PK_Party_ID()
        Try
            Me.IsNewRecord = True
            Dim lngPK As Long = 0

            Select Case Me.PartyType
                'Case enumPartyType._Account
                '    If objGoAML_Activity_Class.list_goAML_Act_Account.Count = 0 Then
                '        lngPK = -1
                '    Else
                '        lngPK = objGoAML_Activity_Class.list_goAML_Act_Account.Min(Function(x) x.PK_ACT_ACCOUNT_ID) - 1
                '    End If
                Case enumPartyType._Person
                    If objGoAML_Activity_Class.list_goAML_Act_Person.Count = 0 Then
                        lngPK = -1
                    Else
                        lngPK = objGoAML_Activity_Class.list_goAML_Act_Person.Min(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ID) - 1
                    End If
                Case enumPartyType._Entity
                    If objGoAML_Activity_Class.list_goAML_Act_Entity.Count = 0 Then
                        lngPK = -1
                    Else
                        lngPK = objGoAML_Activity_Class.list_goAML_Act_Entity.Min(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_ID) - 1
                    End If
            End Select

            If lngPK >= 0 Then
                lngPK = -1
            End If

            Me.PK_Party_ID = lngPK

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub get_New_PK_PartySub_ID()
        Try
            Me.IsNewRecord = True
            Dim lngPK As Long = 0

            Select Case Me.PartySubType
                'Case SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.Signatory
                '    If objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Count = 0 Then
                '        lngPK = -1
                '    Else
                '        lngPK = objGoAML_Activity_Class.list_goAML_Act_acc_Signatory.Min(Function(x) x.PK_ACT_ACC_SIGNATORY_ID) - 1
                '    End If
                'Case SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount
                '    If objGoAML_Activity_Class.list_goAML_Act_Acc_Ent_Director.Count = 0 Then
                '        lngPK = -1
                '    Else
                '        lngPK = objGoAML_Activity_Class.list_goAML_Act_Acc_Ent_Director.Min(Function(x) x.PK_Act_Acc_Ent_Director_ID) - 1
                '    End If
                Case SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity
                    If objGoAML_Activity_Class.list_goAML_Act_Director.Count = 0 Then
                        lngPK = -1
                    Else
                        lngPK = objGoAML_Activity_Class.list_goAML_Act_Director.Min(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_ID) - 1
                    End If
            End Select

            If lngPK >= 0 Then
                lngPK = -1
            End If

            Me.PK_PartySub_ID = lngPK

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub get_New_PK_Party_AccountEntity_ID()
        Try
            Dim lngPK As Long = 0

            If Me.PartyType = enumPartyType._Account Then
                If objGoAML_Activity_Class.list_goAML_Act_Entity_Account.Count = 0 Then
                    lngPK = -1
                Else
                    lngPK = objGoAML_Activity_Class.list_goAML_Act_Entity_Account.Min(Function(x) x.PK_ACT_ENTITY_ACCOUNT_ID) - 1
                End If
            End If

            If lngPK >= 0 Then
                lngPK = -1
            End If

            Me.PK_Party_AccountEntity_ID = lngPK

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Import Data from goAML Ref Account or WIC"
    Protected Sub btn_Import_Account_Click()
        Try
            Me.SearchType = "Account"
            SearchParty()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Import_Customer_Click()
        Try
            Me.SearchType = "Customer"
            SearchParty()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Import_WIC_Click()
        Try
            Me.SearchType = "WIC"
            SearchParty()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub SearchParty()
        Try
            window_Import_Party.Title = "Search Customer"

            'gp_Import_Party_Account.Hidden = True
            gp_Import_Party_WIC_Individual.Hidden = True
            gp_Import_Party_WIC_Corporate.Hidden = True
            gp_Import_Party_Customer_Individual.Hidden = True
            gp_Import_Party_Customer_Corporate.Hidden = True

            If Me.SearchType = "Account" Then
                'gp_Import_Party_Account.Hidden = False
            ElseIf Me.SearchType = "Customer" Then
                If Me.PartyType = enumPartyType._Person Then
                    gp_Import_Party_Customer_Individual.Hidden = False
                ElseIf Me.PartyType = enumPartyType._Entity Then
                    gp_Import_Party_Customer_Corporate.Hidden = False
                End If
            ElseIf Me.SearchType = "WIC" Then
                If Me.PartyType = enumPartyType._Person Then
                    gp_Import_Party_WIC_Individual.Hidden = False
                ElseIf Me.PartyType = enumPartyType._Entity Then
                    gp_Import_Party_WIC_Corporate.Hidden = False
                End If
            End If

            window_Import_Party.Hidden = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub store_ReadData_Import_Party_Account(sender As Object, e As StoreReadDataEventArgs)
    '    Try
    '        Dim intStart As Integer = e.Start
    '        Dim intLimit As Int16 = e.Limit
    '        Dim inttotalRecord As Integer
    '        Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

    '        intLimit = 10

    '        'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

    '        Dim strsort As String = ""
    '        For Each item As DataSorter In e.Sort
    '            strsort += item.Property & " " & item.Direction.ToString
    '        Next
    '        'Me.indexStart = intStart
    '        'Me.strWhereClause = strfilter
    '        'Me.strOrder = strsort
    '        If strsort = "" Then
    '            strsort = "CIF asc, NamaLengkap asc"
    '        End If
    '        'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
    '        Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Customer", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
    '        ''-- start paging ------------------------------------------------------------
    '        Dim limit As Integer = e.Limit
    '        If (e.Start + e.Limit) > inttotalRecord Then
    '            limit = inttotalRecord - e.Start
    '        End If
    '        'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
    '        ''-- end paging ------------------------------------------------------------
    '        e.Total = inttotalRecord
    '        store_Import_Party_Account.DataSource = DataPaging
    '        store_Import_Party_Account.DataBind()

    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub store_ReadData_Import_Party_WIC_Individual(sender As Object, e As StoreReadDataEventArgs)
        Try

            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            If String.IsNullOrEmpty(strfilter) Then
                strfilter = "FK_Customer_Type_ID=1"
            Else
                strfilter += " AND FK_Customer_Type_ID=1"
            End If

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")
            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            If strsort = "" Then
                strsort = "NamaLengkap asc"
            End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_WIC", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            store_Import_Party_WIC_Individual.DataSource = DataPaging
            store_Import_Party_WIC_Individual.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_Import_Party_WIC_Corporate(sender As Object, e As StoreReadDataEventArgs)
        Try

            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            If String.IsNullOrEmpty(strfilter) Then
                strfilter = "FK_Customer_Type_ID=2"
            Else
                strfilter += " AND FK_Customer_Type_ID=2"
            End If

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")
            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            If strsort = "" Then
                strsort = "NamaLengkap asc"
            End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_WIC", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            store_Import_Party_WIC_Corporate.DataSource = DataPaging
            store_Import_Party_WIC_Corporate.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_Import_Party_Customer_Individual(sender As Object, e As StoreReadDataEventArgs)
        Try

            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            If String.IsNullOrEmpty(strfilter) Then
                strfilter = "FK_Customer_Type_ID=1"
            Else
                strfilter += " AND FK_Customer_Type_ID=1"
            End If

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")
            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            If strsort = "" Then
                strsort = "NamaLengkap asc"
            End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Customer", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            store_Import_Party_Customer_Individual.DataSource = DataPaging
            store_Import_Party_Customer_Individual.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_Import_Party_Customer_Corporate(sender As Object, e As StoreReadDataEventArgs)
        Try

            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            If String.IsNullOrEmpty(strfilter) Then
                strfilter = "FK_Customer_Type_ID=2"
            Else
                strfilter += " AND FK_Customer_Type_ID=2"
            End If

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")
            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            If strsort = "" Then
                strsort = "NamaLengkap asc"
            End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Customer", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            store_Import_Party_Customer_Corporate.DataSource = DataPaging
            store_Import_Party_Customer_Corporate.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_Import_Party_Account(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim strAccountNo As String = e.ExtraParams(0).Value
            Dim strCIFNo As String = e.ExtraParams(1).Value

            Load_Import_Party_Account(strAccountNo)
            window_Import_Party.Hidden = True

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_Import_Party_WIC(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim strPK_WIC As String = e.ExtraParams(0).Value
            Dim strWICNo As String = e.ExtraParams(1).Value

            Load_Import_Party_WIC(strPK_WIC)
            window_Import_Party.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_Import_Party_Customer(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim strPK_Customer As String = e.ExtraParams(0).Value
            Dim strCustomerNo As String = e.ExtraParams(1).Value

            Load_Import_Party_Customer(strPK_Customer)
            window_Import_Party.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Load_Import_Party_Account(strAccountNo As String)
        Try
            Me.IsNewRecord = False
            CleanWindowAccountParty()

            Using objdb As New NawaDevDAL.NawaDatadevEntities
                Dim PK_Customer_ID As Integer = Convert.ToInt32(strAccountNo)
                Dim objCust As NawaDevDAL.goAML_Ref_Customer = objdb.goAML_Ref_Customer.Where(Function(x) x.PK_Customer_ID = PK_Customer_ID).FirstOrDefault
                'If objCust IsNot Nothing Then
                '    Dim objAcc As New SIPESATGOAMLBLL.SIPESATGOAML_Ref_Account
                '    If objCust.CIF IsNot Nothing Then
                '        objAcc = objDB.goAML_Ref_Account.Where(Function(x) x.client_number = objCust.CIF).OrderBy(Function(x) x.opened).FirstOrDefault
                '    End If

                '    Dim objAccount = objGoAML_Activity_Class.list_goAML_Act_Account.Where(Function(x) x.PK_ACT_ACCOUNT_ID = Me.PK_Party_ID).FirstOrDefault
                '    If objAccount Is Nothing Then
                '        objAccount = New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACCOUNT
                '        objAccount.PK_ACT_ACCOUNT_ID = Me.PK_Party_ID
                '        Me.IsNewRecord = True
                '    Else
                '        Me.IsNewRecord = False
                '    End If

                '    With objAccount
                '        .FK_REPORT_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                '        .FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID

                '        Dim objGP As New SIPESATGoAMLDAL.SIPESATGOAML_GLOBAL_PARAMETER
                '        objGP = objDB.SIPESATGOAML_GLOBAL_PARAMETER.Where(Function(x) x.PK_GLOBAL_PARAMETER_ID = 11).FirstOrDefault
                '        If objGP IsNot Nothing Then
                '            .INSTITUTION_NAME = objGP.ParameterValue
                '        End If

                '        objGP = objDB.SIPESATGOAML_GLOBAL_PARAMETER.Where(Function(x) x.PK_GLOBAL_PARAMETER_ID = 20).FirstOrDefault
                '        If objGP IsNot Nothing Then
                '            .INSTITUTION_CODE = objGP.ParameterValue
                '        End If

                '        objGP = objDB.SIPESATGOAML_GLOBAL_PARAMETER.Where(Function(x) x.PK_GLOBAL_PARAMETER_ID = 12).FirstOrDefault
                '        If objGP IsNot Nothing Then
                '            .SWIFT_CODE = objGP.ParameterValue
                '        End If

                '        If objCust IsNot Nothing Then
                '            .BRANCH = objCust.Opening_Branch_Code       'diganti dengan branch customer
                '            .ACCOUNT = objCust.CIF                      'diganti cif customer
                '            .CLIENT_NUMBER = objCust.CIF                'check customer
                '            .OPENED = objCust.Opening_Date              'check customer
                '            .CLOSED = objCust.Closing_Date              'check customer
                '            .STATUS_CODE = objCust.Status_Code          'check customer
                '            If objCust.FK_Customer_Type_ID IsNot Nothing Then
                '                Dim jenisRekeningType As SIPESATGoAMLDAL.SIPESATGoAML_Jenis_Rekening = objDB.SIPESATGoAML_Jenis_Rekening.Where(Function(x) x.PK_SIPESATGoAML_Jenis_Rekening_ID = objCust.FK_Customer_Type_ID).FirstOrDefault
                '                If jenisRekeningType IsNot Nothing Then
                '                    .PERSONAL_ACCOUNT_TYPE = jenisRekeningType.Kode
                '                End If
                '            End If
                '            If objCust.FK_Customer_Type_ID = 1 Then
                '                .ISREKENINGKORPORASI = False
                '            Else
                '                .ISREKENINGKORPORASI = True
                '            End If
                '        End If

                '        If objAcc IsNot Nothing Then
                '            .CURRENCY_CODE = objAcc.Currency_Code
                '            Dim tempSTRCity As String = ""
                '            Dim tempSTRProvince As String = ""
                '            If objAcc.Branch IsNot Nothing Then
                '                Dim objAMLBranch As SIPESATGoAMLDAL.AML_BRANCH = objDB.AML_BRANCH.Where(Function(x) x.FK_AML_BRANCH_CODE = objAcc.Branch).FirstOrDefault
                '                If objAMLBranch IsNot Nothing Then
                '                    tempSTRCity = objAMLBranch.CITY_NAME
                '                    tempSTRProvince = objAMLBranch.PROVINCE_NAME
                '                    'Dim tempSTRReqNum As String = ""
                '                    'If objAMLBranch.CITY_NAME IsNot Nothing Then
                '                    '
                '                    'End If
                '                    '.COMMENTS = "Kota/Kabupaten : " & tempSTRCity & ", Provinsi : " & tempSTRProvince & " , No Rek : " & tempSTRReqNum
                '                End If
                '            End If
                '            .COMMENTS = "Kota/Kabupaten : " & tempSTRCity & ", Provinsi : " & tempSTRProvince & " , No Rek : " & objAcc.Account_No
                '            'If Not String.IsNullOrEmpty(objAcc.FK_CIF_Entity_ID) Then
                '            '    .ISREKENINGKORPORASI = True
                '            'Else
                '            '    .ISREKENINGKORPORASI = False
                '            'End If
                '        End If

                '        If Me.IsNewRecord Then
                '            .Active = True
                '            .CreatedDate = DateTime.Now
                '            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID

                '            objGoAML_Activity_Class.list_goAML_Act_Account.Add(objAccount)
                '        Else
                '            .LastUpdateDate = DateTime.Now
                '            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                '        End If

                '        'Account Entity
                '        If .ISREKENINGKORPORASI Then
                '            Dim objAccountEntity = objGoAML_Activity_Class.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID).FirstOrDefault
                '            If objAccountEntity Is Nothing Then
                '                objAccountEntity = New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ENTITY_ACCOUNT
                '                objAccountEntity.PK_ACT_ENTITY_ACCOUNT_ID = Me.PK_Party_AccountEntity_ID
                '                Me.IsNewRecord = True
                '            Else
                '                Me.IsNewRecord = False
                '            End If

                '            With objAccountEntity
                '                .FK_REPORT_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                '                .FK_ACTIVITY_REPORTPARTY_ID = Me.PK_Activity_ID
                '                .FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID

                '                'Load Informasi Korporasi
                '                Dim objCIFKorporasi = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.GetCustomerbyCIF(objCust.CIF)
                '                If objCIFKorporasi IsNot Nothing Then
                '                    .NAME = objCIFKorporasi.Corp_Name
                '                    '.COMMERCIAL_NAME = objCIFKorporasi.Corp_Commercial_Name
                '                    '.INCORPORATION_LEGAL_FORM = objCIFKorporasi.Corp_Incorporation_Legal_Form
                '                    '.INCORPORATION_NUMBER = objCIFKorporasi.Corp_Incorporation_Number
                '                    .BUSINESS = objCIFKorporasi.Corp_Business
                '                    '.EMAIL = objCIFKorporasi.Corp_Email
                '                    '.URL = objCIFKorporasi.Corp_Url
                '                    .INCORPORATION_STATE = objCIFKorporasi.Corp_Incorporation_State
                '                    .INCORPORATION_COUNTRY_CODE = objCIFKorporasi.Corp_Incorporation_Country_Code
                '                    .INCORPORATION_DATE = objCIFKorporasi.Corp_Incorporation_Date
                '                    'If Not IsNothing(.BUSINESS_CLOSED) Then
                '                    '    .BUSINESS_CLOSED = objCIFKorporasi.Corp_Business_Closed
                '                    'Else
                '                    '    .BUSINESS_CLOSED = False
                '                    'End If
                '                    '.DATE_BUSINESS_CLOSED = objCIFKorporasi.Corp_Date_Business_Closed
                '                    .TAX_NUMBER = objCIFKorporasi.Corp_Tax_Number
                '                    '.COMMENTS = objCIFKorporasi.Corp_Comments
                '                End If
                '            End With

                '            If Me.IsNewRecord Then
                '                objAccountEntity.Active = True
                '                objAccountEntity.CreatedDate = DateTime.Now
                '                objAccountEntity.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID

                '                objGoAML_Activity_Class.list_goAML_Act_Entity_Account.Add(objAccountEntity)
                '            Else
                '                objAccountEntity.LastUpdateDate = DateTime.Now
                '                objAccountEntity.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                '            End If
                '        Else

                '        End If
                '    End With

                '    '====== (Hati2 ini bukan untuk mengambil objGoAML_Activity_Class dari DB,
                '    'melainkan hanya untuk update Signatory dan Entity saja kemudian kembalikan lagi ke sini objectnya)
                '    If objAccount.ISREKENINGKORPORASI Then
                '        'Account Entity
                '        Dim objGoAML_Activity_Class_Get_Entity_Class = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.GetActivityAccountEntityClassByAccountID(objGoAML_Activity_Class, objAccount.PK_ACT_ACCOUNT_ID)
                '        If objGoAML_Activity_Class_Get_Entity_Class IsNot Nothing Then
                '            objGoAML_Activity_Class = objGoAML_Activity_Class_Get_Entity_Class
                '        End If
                '    Else
                '        'Signatory 
                '        Dim objGoAML_Activity_Class_Get_Signatory_Class = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.GetActivityAccountSignatoryClassByAccountID(objGoAML_Activity_Class, objAccount.PK_ACT_ACCOUNT_ID)
                '        If objGoAML_Activity_Class_Get_Signatory_Class IsNot Nothing Then
                '            objGoAML_Activity_Class = objGoAML_Activity_Class_Get_Signatory_Class
                '        End If
                '    End If
                '    '====== End of (Hati2 ini bukan untuk mengambil objGoAML_Activity_Class dari DB,
                '    'melainkan hanya untuk update Signatory dan Entity saja kemudian kembalikan lagi ke sini objectnya)

                '    'Load Account in window objects
                '    LoadAccount()
                'End If
                'Dim objSelAccount = objDB.goAML_Ref_Account.Where(Function(x) x.Account_No = strAccountNo).FirstOrDefault
                'If objSelAccount IsNot Nothing Then

                '    Dim objAccount = objGoAML_Activity_Class.list_goAML_Act_Account.Where(Function(x) x.PK_ACT_ACCOUNT_ID = Me.PK_Party_ID).FirstOrDefault
                '    If objAccount Is Nothing Then
                '        objAccount = New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACCOUNT
                '        objAccount.PK_ACT_ACCOUNT_ID = Me.PK_Party_ID
                '        Me.IsNewRecord = True
                '    Else
                '        Me.IsNewRecord = False
                '    End If

                '    With objAccount
                '        .FK_REPORT_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                '        .FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID

                '        Dim objGP As New SIPESATGoAMLDAL.SIPESATGOAML_GLOBAL_PARAMETER
                '        objGP = objDB.SIPESATGOAML_GLOBAL_PARAMETER.Where(Function(x) x.PK_GLOBAL_PARAMETER_ID = 11).FirstOrDefault
                '        If objGP IsNot Nothing Then
                '            .INSTITUTION_NAME = objGP.ParameterValue
                '        End If

                '        objGP = objDB.SIPESATGOAML_GLOBAL_PARAMETER.Where(Function(x) x.PK_GLOBAL_PARAMETER_ID = 20).FirstOrDefault
                '        If objGP IsNot Nothing Then
                '            .INSTITUTION_CODE = objGP.ParameterValue
                '        End If

                '        objGP = objDB.SIPESATGOAML_GLOBAL_PARAMETER.Where(Function(x) x.PK_GLOBAL_PARAMETER_ID = 12).FirstOrDefault
                '        If objGP IsNot Nothing Then
                '            .SWIFT_CODE = objGP.ParameterValue
                '        End If

                '        'objGP = objDB.goAML_Ref_ReportGlobalParameter.Where(Function(x) x.PK_GlobalReportParameter_ID = 3).FirstOrDefault
                '        'If objGP IsNot Nothing Then
                '        '    If CInt(objGP.ParameterValue) = 1 Then
                '        '        .Non_Banking_Institution = True
                '        '    Else
                '        '        .Non_Banking_Institution = False
                '        '    End If
                '        'End If
                '        Dim objcustomer As SIPESATGOAMLBLL.SIPESATGOAML_Ref_Customer = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objSelAccount.client_number).FirstOrDefault
                '        If objcustomer IsNot Nothing Then
                '            '.BRANCH = objSelAccount.Branch                 'diganti dengan branch customer
                '            .BRANCH = objcustomer.Opening_Branch_Code       'diganti dengan branch customer
                '            '.ACCOUNT = objSelAccount.Account_No            'diganti cif customer
                '            .ACCOUNT = objcustomer.CIF                      'diganti cif customer
                '            '.CLIENT_NUMBER = objSelAccount.client_number   'check customer
                '            .CLIENT_NUMBER = objcustomer.CIF                'check customer
                '            '.OPENED = objSelAccount.opened                 'check customer
                '            .OPENED = objcustomer.Opening_Date              'check customer
                '            '.CLOSED = objSelAccount.closed                 'check customer
                '            .CLOSED = objcustomer.Closing_Date              'check customer
                '            '.STATUS_CODE = objSelAccount.status_code       'check customer
                '            .STATUS_CODE = objcustomer.Status_Code          'check customer
                '        End If

                '        .CURRENCY_CODE = objSelAccount.Currency_Code   'check customer
                '        .ACCOUNT_NAME = objSelAccount.Account_Name
                '        '.iban = objSelAccount.IBAN
                '        '.PERSONAL_ACCOUNT_TYPE = objSelAccount.personal_account_type    'SIPESAT Perseorangan atau SIPESAT Korporasi
                '        '.DATE_BALANCE = objSelAccount.date_balance
                '        '.BALANCE = objSelAccount.balance
                '        '.BENEFICIARY = objSelAccount.beneficiary
                '        '.BENEFICIARY_COMMENT = objSelAccount.beneficiary_comment
                '        .COMMENTS = objSelAccount.comments

                '        If Not String.IsNullOrEmpty(objSelAccount.FK_CIF_Entity_ID) Then
                '            .ISREKENINGKORPORASI = True
                '            .PERSONAL_ACCOUNT_TYPE = "SIPESAT Korporasi"    'SIPESAT Perseorangan atau SIPESAT Korporasi
                '        Else
                '            .ISREKENINGKORPORASI = False
                '            .PERSONAL_ACCOUNT_TYPE = "SIPESAT Perseorangan"    'SIPESAT Perseorangan atau SIPESAT Korporasi
                '        End If

                '        If Me.IsNewRecord Then
                '            .Active = True
                '            .CreatedDate = DateTime.Now
                '            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID

                '            objGoAML_Activity_Class.list_goAML_Act_Account.Add(objAccount)
                '        Else
                '            .LastUpdateDate = DateTime.Now
                '            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                '        End If

                '        'Account Entity
                '        If .ISREKENINGKORPORASI Then
                '            Dim objAccountEntity = objGoAML_Activity_Class.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID).FirstOrDefault
                '            If objAccountEntity Is Nothing Then
                '                objAccountEntity = New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ENTITY_ACCOUNT
                '                objAccountEntity.PK_ACT_ENTITY_ACCOUNT_ID = Me.PK_Party_AccountEntity_ID
                '                Me.IsNewRecord = True
                '            Else
                '                Me.IsNewRecord = False
                '            End If

                '            With objAccountEntity
                '                .FK_REPORT_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                '                .FK_ACTIVITY_REPORTPARTY_ID = Me.PK_Activity_ID
                '                .FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID

                '                'Load Informasi Korporasi
                '                Dim objCIFKorporasi = SIPESATGoAMLBLL.goAML_CustomerBLL.GetCustomerbyCIF(objSelAccount.FK_CIF_Entity_ID)
                '                If objCIFKorporasi IsNot Nothing Then
                '                    .NAME = objCIFKorporasi.Corp_Name
                '                    .COMMERCIAL_NAME = objCIFKorporasi.Corp_Commercial_Name
                '                    .INCORPORATION_LEGAL_FORM = objCIFKorporasi.Corp_Incorporation_Legal_Form
                '                    .INCORPORATION_NUMBER = objCIFKorporasi.Corp_Incorporation_Number
                '                    .BUSINESS = objCIFKorporasi.Corp_Business
                '                    .EMAIL = objCIFKorporasi.Corp_Email
                '                    .URL = objCIFKorporasi.Corp_Url
                '                    .INCORPORATION_STATE = objCIFKorporasi.Corp_Incorporation_State
                '                    .INCORPORATION_COUNTRY_CODE = objCIFKorporasi.Corp_Incorporation_Country_Code
                '                    .INCORPORATION_DATE = objCIFKorporasi.Corp_Incorporation_Date
                '                    If Not IsNothing(.BUSINESS_CLOSED) Then
                '                        .BUSINESS_CLOSED = objCIFKorporasi.Corp_Business_Closed
                '                    Else
                '                        .BUSINESS_CLOSED = False
                '                    End If
                '                    .DATE_BUSINESS_CLOSED = objCIFKorporasi.Corp_Date_Business_Closed
                '                    .TAX_NUMBER = objCIFKorporasi.Corp_Tax_Number
                '                    .COMMENTS = objCIFKorporasi.Corp_Comments
                '                End If
                '            End With

                '            If Me.IsNewRecord Then
                '                objAccountEntity.Active = True
                '                objAccountEntity.CreatedDate = DateTime.Now
                '                objAccountEntity.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID

                '                objGoAML_Activity_Class.list_goAML_Act_Entity_Account.Add(objAccountEntity)
                '            Else
                '                objAccountEntity.LastUpdateDate = DateTime.Now
                '                objAccountEntity.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                '            End If
                '        End If
                '    End With

                '    '====== (Hati2 ini bukan untuk mengambil objGoAML_Activity_Class dari DB,
                '    'melainkan hanya untuk update Signatory dan Entity saja kemudian kembalikan lagi ke sini objectnya)
                '    'Signatory 
                '    Dim objGoAML_Activity_Class_Get_Signatory_Class = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.GetActivityAccountSignatoryClassByAccountID(objGoAML_Activity_Class, objAccount.PK_ACT_ACCOUNT_ID)
                '    If objGoAML_Activity_Class_Get_Signatory_Class IsNot Nothing Then
                '        objGoAML_Activity_Class = objGoAML_Activity_Class_Get_Signatory_Class
                '    End If

                '    'Account Entity
                '    If objAccount.ISREKENINGKORPORASI Then
                '        Dim objGoAML_Activity_Class_Get_Entity_Class = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.GetActivityAccountEntityClassByAccountID(objGoAML_Activity_Class, objAccount.PK_ACT_ACCOUNT_ID)
                '        If objGoAML_Activity_Class_Get_Entity_Class IsNot Nothing Then
                '            objGoAML_Activity_Class = objGoAML_Activity_Class_Get_Entity_Class
                '        End If
                '    End If
                '    '====== End of (Hati2 ini bukan untuk mengambil objGoAML_Activity_Class dari DB,
                '    'melainkan hanya untuk update Signatory dan Entity saja kemudian kembalikan lagi ke sini objectnya)

                '    'Load Account in window objects
                '    LoadAccount()
                '    'ini harus d tambah if trn or act dan hanya untuk act
                '    'LoadBiPartyShortInfo(Me.PartySide)
                'End If
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Load_Import_Party_WIC(strPK_WIC As String)
        Try

            Dim objSelWIC As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from goaml_Ref_wic where PK_Customer_ID = " & strPK_WIC, Nothing)
            If objSelWIC IsNot Nothing Then
                Dim ObjRefWIC As New SIPESATGoAMLBLL.goAML_Ref_WIC
                For Each item As DataRow In objSelWIC.Rows
                    If Not IsDBNull(item("PK_Customer_ID")) Then
                        ObjRefWIC.PK_Customer_ID = item("PK_Customer_ID")
                    End If
                    If Not IsDBNull(item("INDV_Passport_Country")) Then
                        ObjRefWIC.INDV_Passport_Country = item("INDV_Passport_Country")
                    End If
                    If Not IsDBNull(item("INDV_ID_Number")) Then
                        ObjRefWIC.INDV_ID_Number = item("INDV_ID_Number")
                    End If
                    If Not IsDBNull(item("INDV_Nationality1")) Then
                        ObjRefWIC.INDV_Nationality1 = item("INDV_Nationality1")
                    End If
                    If Not IsDBNull(item("INDV_Nationality2")) Then
                        ObjRefWIC.INDV_Nationality2 = item("INDV_Nationality2")
                    End If
                    If Not IsDBNull(item("INDV_Nationality3")) Then
                        ObjRefWIC.INDV_Nationality3 = item("INDV_Nationality3")
                    End If
                    If Not IsDBNull(item("INDV_Residence")) Then
                        ObjRefWIC.INDV_Residence = item("INDV_Residence")
                    End If
                    If Not IsDBNull(item("INDV_Email")) Then
                        ObjRefWIC.INDV_Email = item("INDV_Email")
                    End If
                    If Not IsDBNull(item("INDV_Passport_Number")) Then
                        ObjRefWIC.INDV_Passport_Number = item("INDV_Passport_Number")
                    End If
                    If Not IsDBNull(item("INDV_SSN")) Then
                        ObjRefWIC.INDV_SSN = item("INDV_SSN")
                    End If
                    If Not IsDBNull(item("INDV_Alias")) Then
                        ObjRefWIC.INDV_Alias = item("INDV_Alias")
                    End If
                    If Not IsDBNull(item("INDV_Mothers_Name")) Then
                        ObjRefWIC.INDV_Mothers_Name = item("INDV_Mothers_Name")
                    End If
                    If Not IsDBNull(item("INDV_Birth_Place")) Then
                        ObjRefWIC.INDV_Birth_Place = item("INDV_Birth_Place")
                    End If
                    If Not IsDBNull(item("INDV_BirthDate")) Then
                        ObjRefWIC.INDV_BirthDate = item("INDV_BirthDate")
                    End If
                    If Not IsDBNull(item("INDV_Last_Name")) Then
                        ObjRefWIC.INDV_Last_Name = item("INDV_Last_Name")
                    End If
                    If Not IsDBNull(item("INDV_Prefix")) Then
                        ObjRefWIC.INDV_Prefix = item("INDV_Prefix")
                    End If
                    If Not IsDBNull(item("INDV_Middle_name")) Then
                        ObjRefWIC.INDV_Middle_Name = item("INDV_Middle_name")
                    End If
                    If Not IsDBNull(item("INDV_First_name")) Then
                        ObjRefWIC.INDV_First_Name = item("INDV_First_name")
                    End If
                    If Not IsDBNull(item("INDV_Title")) Then
                        ObjRefWIC.INDV_Title = item("INDV_Title")
                    End If
                    If Not IsDBNull(item("Country_Of_Birth")) Then
                        ObjRefWIC.Country_Of_Birth = item("Country_Of_Birth")
                    End If
                    If Not IsDBNull(item("WIC_No")) Then
                        ObjRefWIC.WIC_No = item("WIC_No")
                    End If
                    If Not IsDBNull(item("INDV_Occupation")) Then
                        ObjRefWIC.INDV_Occupation = item("INDV_Occupation")
                    End If
                    If Not IsDBNull(item("INDV_Employer_Name")) Then
                        ObjRefWIC.INDV_Employer_Name = item("INDV_Employer_Name")
                    End If
                    If Not IsDBNull(item("Corp_Name")) Then
                        ObjRefWIC.Corp_Name = item("Corp_Name")
                    End If
                    If Not IsDBNull(item("Corp_Commercial_Name")) Then
                        ObjRefWIC.Corp_Commercial_Name = item("Corp_Commercial_Name")
                    End If


                    If Not IsDBNull(item("Corp_Url")) Then
                        ObjRefWIC.Corp_Url = item("Corp_Url")
                    End If
                    If Not IsDBNull(item("Corp_Email")) Then
                        ObjRefWIC.Corp_Email = item("Corp_Email")
                    End If
                    If Not IsDBNull(item("Corp_Business")) Then
                        ObjRefWIC.Corp_Business = item("Corp_Business")
                    End If

                    If Not IsDBNull(item("Corp_Incorporation_Number")) Then
                        ObjRefWIC.Corp_Incorporation_Number = item("Corp_Incorporation_Number")
                    End If
                    If Not IsDBNull(item("Corp_Incorporation_Legal_Form")) Then
                        ObjRefWIC.Corp_Incorporation_Legal_Form = item("Corp_Incorporation_Legal_Form")
                    End If

                    If Not IsDBNull(item("FK_Customer_Type_ID")) Then
                        ObjRefWIC.FK_Customer_Type_ID = item("FK_Customer_Type_ID")
                    End If
                    If Not IsDBNull(item("Corp_Comments")) Then
                        ObjRefWIC.Corp_Comments = item("Corp_Comments")
                    End If
                    If Not IsDBNull(item("Corp_Tax_Registeration_Number")) Then
                        ObjRefWIC.Corp_Tax_Registeration_Number = item("Corp_Tax_Registeration_Number")
                    End If
                    If Not IsDBNull(item("Corp_Tax_Number")) Then
                        ObjRefWIC.Corp_Tax_Number = item("Corp_Tax_Number")
                    End If
                    If Not IsDBNull(item("Corp_Date_Business_Closed")) Then
                        ObjRefWIC.Corp_Date_Business_Closed = item("Corp_Date_Business_Closed")
                    End If
                    If Not IsDBNull(item("Corp_Business_Closed")) Then
                        ObjRefWIC.Corp_Business_Closed = item("Corp_Business_Closed")
                    End If
                    If Not IsDBNull(item("Corp_Incorporation_Date")) Then
                        ObjRefWIC.Corp_Incorporation_Date = item("Corp_Incorporation_Date")
                    End If
                    If Not IsDBNull(item("Corp_Role")) Then
                        ObjRefWIC.Corp_Role = item("Corp_Role")
                    End If
                    If Not IsDBNull(item("Corp_Director_ID")) Then
                        ObjRefWIC.Corp_Director_ID = item("Corp_Director_ID")
                    End If
                    If Not IsDBNull(item("Corp_Incorporation_Country_Code")) Then
                        ObjRefWIC.Corp_Incorporation_Country_Code = item("Corp_Incorporation_Country_Code")
                    End If
                    If Not IsDBNull(item("Corp_Incorporation_State")) Then
                        ObjRefWIC.Corp_Incorporation_State = item("Corp_Incorporation_State")
                    End If
                    If Not IsDBNull(item("Active")) Then
                        ObjRefWIC.Active = item("Active")
                    End If
                    If Not IsDBNull(item("CreatedBy")) Then
                        ObjRefWIC.CreatedBy = item("CreatedBy")
                    End If
                    If Not IsDBNull(item("LastUpdateBy")) Then
                        ObjRefWIC.LastUpdateBy = item("LastUpdateBy")
                    End If
                    If Not IsDBNull(item("ApprovedBy")) Then
                        ObjRefWIC.ApprovedBy = item("ApprovedBy")
                    End If
                    If Not IsDBNull(item("CreatedDate")) Then
                        ObjRefWIC.CreatedDate = item("CreatedDate")
                    End If
                    If Not IsDBNull(item("LastUpdateDate")) Then
                        ObjRefWIC.LastUpdateDate = item("LastUpdateDate")
                    End If
                    If Not IsDBNull(item("ApprovedDate")) Then
                        ObjRefWIC.ApprovedDate = item("ApprovedDate")
                    End If
                    If Not IsDBNull(item("Alternateby")) Then
                        ObjRefWIC.Alternateby = item("Alternateby")
                    End If
                    If Not IsDBNull(item("Full_Name_Frn")) Then
                        ObjRefWIC.Full_Name_Frn = item("Full_Name_Frn")
                    End If
                    If Not IsDBNull(item("Residence_Since")) Then
                        ObjRefWIC.Residence_Since = item("Residence_Since")
                    End If
                    If Not IsDBNull(item("INDV_Gender")) Then
                        ObjRefWIC.INDV_Gender = item("INDV_Gender")
                    End If
                    If Not IsDBNull(item("INDV_Deceased")) Then
                        ObjRefWIC.INDV_Deceased = item("INDV_Deceased")
                    End If
                    If Not IsDBNull(item("INDV_Deceased_Date")) Then
                        ObjRefWIC.INDV_Deceased_Date = item("INDV_Deceased_Date")
                    End If
                    If Not IsDBNull(item("INDV_Tax_Reg_Number")) Then
                        ObjRefWIC.INDV_Tax_Reg_Number = item("INDV_Tax_Reg_Number")
                    End If
                    If Not IsDBNull(item("INDV_Tax_Number")) Then
                        ObjRefWIC.INDV_Tax_Number = item("INDV_Tax_Number")
                    End If
                    If Not IsDBNull(item("Is_Protected")) Then
                        ObjRefWIC.Is_Protected = item("Is_Protected")
                    End If
                    If Not IsDBNull(item("Is_RealWIC")) Then
                        ObjRefWIC.Is_RealWIC = item("Is_RealWIC")
                    End If
                    If Not IsDBNull(item("INDV_Email2")) Then
                        ObjRefWIC.INDV_Email2 = item("INDV_Email2")
                    End If
                    If Not IsDBNull(item("INDV_Email3")) Then
                        ObjRefWIC.INDV_Email3 = item("INDV_Email3")
                    End If
                    If Not IsDBNull(item("INDV_Email4")) Then
                        ObjRefWIC.INDV_Email4 = item("INDV_Email4")
                    End If
                    If Not IsDBNull(item("INDV_Email5")) Then
                        ObjRefWIC.INDV_Email5 = item("INDV_Email5")
                    End If
                    If Not IsDBNull(item("isUpdateFromDataSource")) Then
                        ObjRefWIC.isUpdateFromDataSource = item("isUpdateFromDataSource")
                    End If
                    If Not IsDBNull(item("Entity_Status")) Then
                        ObjRefWIC.Entity_Status = item("Entity_Status")
                    End If
                    If Not IsDBNull(item("Entity_Status_Date")) Then
                        ObjRefWIC.Entity_Status_Date = item("Entity_Status_Date")
                    End If

                Next

                If ObjRefWIC.FK_Customer_Type_ID = 1 Then     'Person
                    Load_Import_Party_WIC_Person(ObjRefWIC)
                Else    'Entity
                    Load_Import_Party_WIC_Entity(ObjRefWIC)
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Load_Import_Party_WIC_Person(objSelPerson As SIPESATGoAMLBLL.goAML_Ref_WIC)
        Try
            Me.IsNewRecord = False
            CleanWindowPersonParty()



            Dim objPerson = objGoAML_Activity_Class.list_goAML_Act_Person.Where(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ID = Me.PK_Party_ID).FirstOrDefault
            If objPerson Is Nothing Then
                objPerson = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON
                objPerson.PK_SIPESATGOAML_ACT_PERSON_ID = Me.PK_Party_ID
                Me.IsNewRecord = True
            Else
                Me.IsNewRecord = False
            End If

            'Delete existing data if any
            'Person Address
            Dim listPersonAddress_Old = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Where(Function(x) x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID).ToList
            If listPersonAddress_Old IsNot Nothing Then
                For Each objAddress_Old In listPersonAddress_Old
                    objGoAML_Activity_Class.list_goAML_Act_Person_Address.Remove(objAddress_Old)
                Next
            End If

            'Person Phone
            Dim listPersonPhone_Old = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Where(Function(x) x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID).ToList
            If listPersonPhone_Old IsNot Nothing Then
                For Each objPhone_Old In listPersonPhone_Old
                    objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Remove(objPhone_Old)
                Next
            End If

            ''Person Identification
            'Dim listPersonIdentification_Old = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.Person And x.FK_Act_Person_ID = objPerson.PK_SIPESATGOAML_Act_Person_ID).ToList
            'If listPersonIdentification_Old IsNot Nothing Then
            '    For Each objIdentification_Old In listPersonIdentification_Old
            '        objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Remove(objIdentification_Old)
            '    Next
            'End If

            'Masukkan data baru
            'Get Last PK
            Dim lngPK_Address As Long = 0
            Dim lngPK_Phone As Long = 0
            Dim lngPK_Identification As Long = 0

            'PK Person Address
            If objGoAML_Activity_Class.list_goAML_Act_Person_Address.Count > 0 Then
                lngPK_Address = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Min(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID)
                If lngPK_Address > 0 Then
                    lngPK_Address = 0
                End If
            End If

            'PK Person Phone
            If objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Count > 0 Then
                lngPK_Phone = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Min(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_PHONE_ID)
                If lngPK_Phone > 0 Then
                    lngPK_Phone = 0
                End If
            End If

            'PK Person Identification
            If objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Count > 0 Then
                lngPK_Identification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Min(Function(x) x.PK_ACTIVITY_PERSON_IDENTIFICATION_ID)
                If lngPK_Identification > 0 Then
                    lngPK_Identification = 0
                End If
            End If

            With objPerson
                .FK_REPORT_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                .FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID

                .GENDER = objSelPerson.INDV_Gender
                .TITLE = objSelPerson.INDV_Title
                .FIRST_NAME = objSelPerson.INDV_First_Name
                .MIDDLE_NAME = objSelPerson.INDV_Middle_Name
                .PREFIX = objSelPerson.INDV_Prefix
                .LAST_NAME = objSelPerson.INDV_Last_Name
                .BIRTHDATE = objSelPerson.INDV_BirthDate
                .BITH_PLACE = objSelPerson.INDV_Birth_Place
                .MOTHERS_NAME = objSelPerson.INDV_Mothers_Name
                .ALIAS = objSelPerson.INDV_Alias
                .SSN = objSelPerson.INDV_SSN
                .PASSPORT_NUMBER = objSelPerson.INDV_Passport_Number
                .PASSPORT_COUNTRY = objSelPerson.INDV_Passport_Country
                .ID_NUMBER = objSelPerson.INDV_ID_Number
                .NATIONALITY1 = objSelPerson.INDV_Nationality1
                .NATIONALITY2 = objSelPerson.INDV_Nationality2
                .NATIONALITY3 = objSelPerson.INDV_Nationality3
                .RESIDENCE = objSelPerson.INDV_Residence
                .EMAIL = objSelPerson.INDV_Email
                .EMAIL2 = objSelPerson.INDV_Email2
                .EMAIL3 = objSelPerson.INDV_Email3
                .EMAIL4 = objSelPerson.INDV_Email4
                .EMAIL5 = objSelPerson.INDV_Email5
                .OCCUPATION = objSelPerson.INDV_Occupation
                .EMPLOYER_NAME = objSelPerson.INDV_Employer_Name
                .DECEASED = objSelPerson.INDV_Deceased
                .DECEASED_DATE = objSelPerson.INDV_Deceased_Date
                .TAX_NUMBER = objSelPerson.INDV_Tax_Number
                .TAX_REG_NUMBER = objSelPerson.INDV_Tax_Reg_Number
                .SOURCE_OF_WEALTH = objSelPerson.INDV_SumberDana
                .COMMENT = objSelPerson.INDV_Comment
            End With

            If Me.IsNewRecord Then
                objPerson.Active = 1
                objPerson.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                objPerson.CreatedDate = DateTime.Now
                objGoAML_Activity_Class.list_goAML_Act_Person.Add(objPerson)
            Else
                objPerson.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                objPerson.LastUpdateDate = DateTime.Now
            End If

            Dim objListAddress As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_Address where (FK_Ref_Detail_Of = 3 or FK_Ref_Detail_Of = 10) and FK_To_Table_ID = " & objSelPerson.PK_Customer_ID, Nothing)
            If objListAddress IsNot Nothing AndAlso objListAddress.Rows.Count > 0 Then
                For Each objAddress As DataRow In objListAddress.Rows
                    Dim objNewPersonAddress As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_ADDRESS
                    With objNewPersonAddress
                        lngPK_Address = lngPK_Address - 1

                        .PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID = lngPK_Address
                        .FK_REPORT_ID = objPerson.FK_REPORT_ID
                        .FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID
                        If Not IsDBNull(objAddress("ADDRESS_TYPE")) Then
                            .ADDRESS_TYPE = objAddress("ADDRESS_TYPE")
                        End If
                        If Not IsDBNull(objAddress("ADDRESS")) Then
                            .ADDRESS = objAddress("ADDRESS")
                        End If
                        If Not IsDBNull(objAddress("TOWN")) Then
                            .TOWN = objAddress("TOWN")
                        End If
                        If Not IsDBNull(objAddress("CITY")) Then
                            .CITY = objAddress("CITY")
                        End If
                        If Not IsDBNull(objAddress("ZIP")) Then
                            .ZIP = objAddress("ZIP")
                        End If
                        If Not IsDBNull(objAddress("COUNTRY_CODE")) Then
                            .COUNTRY_CODE = objAddress("COUNTRY_CODE")
                        End If
                        If Not IsDBNull(objAddress("STATE")) Then
                            .STATE = objAddress("STATE")
                        End If
                        If Not IsDBNull(objAddress("COMMENTS")) Then
                            .COMMENTS = objAddress("COMMENTS")
                        End If
                        If Not IsDBNull(objAddress("FK_Ref_Detail_Of")) Then
                            If objAddress("FK_Ref_Detail_Of") = 3 Then
                                .ISEMPLOYER = False
                            Else
                                .ISEMPLOYER = True
                            End If
                        End If



                        .Active = 1
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedDate = DateTime.Now
                    End With
                    objGoAML_Activity_Class.list_goAML_Act_Person_Address.Add(objNewPersonAddress)
                Next
            End If

            'Person Address(es)  (3=Address, 10=Employer Address)
            'Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_To_Table_ID = objSelPerson.PK_Customer_ID).ToList
            'If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
            '    For Each objAddress In objListAddress
            '        Dim objNewPersonAddress As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_ADDRESS
            '        With objNewPersonAddress
            '            lngPK_Address = lngPK_Address - 1

            '            .PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID = lngPK_Address
            '            .FK_REPORT_ID = objPerson.FK_REPORT_ID
            '            .FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID

            '            .ADDRESS_TYPE = objAddress.Address_Type
            '            .ADDRESS = objAddress.Address
            '            .TOWN = objAddress.Town
            '            .CITY = objAddress.City
            '            .ZIP = objAddress.Zip
            '            .COUNTRY_CODE = objAddress.Country_Code
            '            .STATE = objAddress.State
            '            .COMMENTS = objAddress.Comments

            '            If objAddress.FK_Ref_Detail_Of = 3 Then
            '                .ISEMPLOYER = False
            '            Else
            '                .ISEMPLOYER = True
            '            End If

            '            .Active = 1
            '            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            '            .CreatedDate = DateTime.Now
            '        End With
            '        objGoAML_Activity_Class.list_goAML_Act_Person_Address.Add(objNewPersonAddress)
            '    Next
            'End If

            Dim objListPhone As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_Phone where (FK_Ref_Detail_Of = 3 or FK_Ref_Detail_Of = 10) and FK_for_Table_ID = " & objSelPerson.PK_Customer_ID, Nothing)
            If objListPhone IsNot Nothing AndAlso objListPhone.Rows.Count > 0 Then
                For Each objPhone As DataRow In objListPhone.Rows
                    Dim objNewPersonPhone As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_PHONE
                    With objNewPersonPhone
                        lngPK_Phone = lngPK_Phone - 1

                        .PK_SIPESATGOAML_ACT_PERSON_PHONE_ID = lngPK_Phone
                        .FK_REPORT_ID = objPerson.FK_REPORT_ID
                        .FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID
                        If Not IsDBNull(objPhone("TPH_CONTACT_TYPE")) Then
                            .TPH_CONTACT_TYPE = objPhone("TPH_CONTACT_TYPE")
                        End If
                        If Not IsDBNull(objPhone("TPH_COMMUNICATION_TYPE")) Then
                            .TPH_COMMUNICATION_TYPE = objPhone("TPH_COMMUNICATION_TYPE")
                        End If
                        If Not IsDBNull(objPhone("TPH_COUNTRY_PREFIX")) Then
                            .TPH_COUNTRY_PREFIX = objPhone("TPH_COUNTRY_PREFIX")
                        End If
                        If Not IsDBNull(objPhone("TPH_NUMBER")) Then
                            .TPH_NUMBER = objPhone("TPH_NUMBER")
                        End If
                        If Not IsDBNull(objPhone("TPH_EXTENSION")) Then
                            .TPH_EXTENSION = objPhone("TPH_EXTENSION")
                        End If
                        If Not IsDBNull(objPhone("COMMENTS")) Then
                            .COMMENTS = objPhone("COMMENTS")
                        End If
                        If Not IsDBNull(objPhone("FK_Ref_Detail_Of")) Then
                            If objPhone("FK_Ref_Detail_Of") = 3 Then
                                .ISEMPLOYER = False
                            Else
                                .ISEMPLOYER = True
                            End If

                        End If

                        .Active = 1
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedDate = DateTime.Now
                    End With
                    objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Add(objNewPersonPhone)
                Next
            End If


            'Person Phone(s) (3=Phone, 10=Employer Phone)
            'Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_for_Table_ID = objSelPerson.PK_Customer_ID).ToList
            '    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
            '        For Each objPhone In objListPhone
            '            Dim objNewPersonPhone As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_PHONE
            '            With objNewPersonPhone
            '                lngPK_Phone = lngPK_Phone - 1

            '                .PK_SIPESATGOAML_ACT_PERSON_PHONE_ID = lngPK_Phone
            '                .FK_REPORT_ID = objPerson.FK_REPORT_ID
            '                .FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID

            '                .TPH_CONTACT_TYPE = objPhone.Tph_Contact_Type
            '                .TPH_COMMUNICATION_TYPE = objPhone.Tph_Communication_Type
            '                .TPH_COUNTRY_PREFIX = objPhone.tph_country_prefix
            '                .TPH_NUMBER = objPhone.tph_number
            '                .TPH_EXTENSION = objPhone.tph_extension
            '                .COMMENTS = objPhone.comments

            '                If objPhone.FK_Ref_Detail_Of = 3 Then
            '                    .ISEMPLOYER = False
            '                Else
            '                    .ISEMPLOYER = True
            '                End If

            '                .Active = 1
            '                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            '                .CreatedDate = DateTime.Now
            '            End With
            '            objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Add(objNewPersonPhone)
            '        Next
            '    End If

            'Person Identification(s)
            'Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 8 And x.FK_Person_ID = objSelPerson.PK_Customer_ID).ToList
            'If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
            '    For Each objIdentification In objListIdentification
            '        Dim objNewPersonIdentification As New SIPESATGoAMLBLL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION
            '        With objNewPersonIdentification
            '            lngPK_Identification = lngPK_Identification - 1

            '            .PK_ACTIVITY_PERSON_IDENTIFICATION_ID = lngPK_Identification
            '            .FK_REPORT_ID = objPerson.FK_Report_ID
            '            .FK_Person_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.Person
            '            .FK_Act_Person_ID = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID

            '            .TYPE = objIdentification.Type
            '            .NUMBER = objIdentification.Number
            '            .Issue_Date = objIdentification.Issue_Date
            '            .EXPIRY_DATE = objIdentification.Expiry_Date
            '            .ISSUED_BY = objIdentification.Issued_By
            '            .ISSUED_COUNTRY = objIdentification.Issued_Country
            '            .IDENTIFICATION_COMMENT = objIdentification.Identification_Comment

            '            .Active = 1
            '            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            '            .CreatedDate = DateTime.Now
            '        End With
            '        objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Add(objNewPersonIdentification)
            '    Next
            'End If

            LoadPerson()
            LoadActivityShortInfo()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Load_Import_Party_WIC_Entity(objSelEntity As SIPESATGoAMLBLL.goAML_Ref_WIC)
        Try
            Me.IsNewRecord = False
            CleanWindowEntityParty()



            Dim objEntity = objGoAML_Activity_Class.list_goAML_Act_Entity.Where(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_ID = Me.PK_Party_ID).FirstOrDefault
            If objEntity Is Nothing Then
                objEntity = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY
                objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID = Me.PK_Party_ID
                Me.IsNewRecord = True
            Else
                Me.IsNewRecord = False
            End If

            'Delete existing data if any
            'Entity Address
            Dim listEntityAddress_Old = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
            If listEntityAddress_Old IsNot Nothing Then
                For Each objAddress_Old In listEntityAddress_Old
                    objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Remove(objAddress_Old)
                Next
            End If

            'Entity Phone
            Dim listEntityPhone_Old = objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
            If listEntityPhone_Old IsNot Nothing Then
                For Each objPhone_Old In listEntityPhone_Old
                    objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Remove(objPhone_Old)
                Next
            End If

            'Director
            Dim listDirector_Old = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
            If listDirector_Old IsNot Nothing Then
                For Each objDirector_Old In listDirector_Old
                    objGoAML_Activity_Class.list_goAML_Act_Director.Remove(objDirector_Old)

                    'Director Address
                    Dim listDirectorAddress_Old = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objDirector_Old.PK_SIPESATGOAML_ACT_DIRECTOR_ID).ToList
                    If listDirectorAddress_Old IsNot Nothing Then
                        For Each objAddress_Old In listDirectorAddress_Old
                            objGoAML_Activity_Class.list_goAML_Act_Director_Address.Remove(objAddress_Old)
                        Next
                    End If

                    'Director Phone
                    Dim listDirectorPhone_Old = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objDirector_Old.PK_SIPESATGOAML_ACT_DIRECTOR_ID).ToList
                    If listDirectorPhone_Old IsNot Nothing Then
                        For Each objPhone_Old In listDirectorPhone_Old
                            objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Remove(objPhone_Old)
                        Next
                    End If

                    'Director Identification
                    Dim listDirectorIdentification_Old = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_ACT_ENTITY_ID = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity And x.FK_ACT_ENTITY_ID = objDirector_Old.PK_SIPESATGOAML_ACT_DIRECTOR_ID).ToList
                    If listDirectorIdentification_Old IsNot Nothing Then
                        For Each objIdentification_Old In listDirectorIdentification_Old
                            objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Remove(objIdentification_Old)
                        Next
                    End If
                Next
            End If

            'Masukkan data baru
            'Get Last PK
            Dim lngPK_Entity_Address As Long = 0
            Dim lngPK_Entity_Phone As Long = 0
            Dim lngPK_Director As Long = 0
            Dim lngPK_Director_Address As Long = 0
            Dim lngPK_Director_Phone As Long = 0
            Dim lngPK_Director_Identification As Long = 0

            'PK Entity Address
            If objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Count > 0 Then
                lngPK_Entity_Address = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Min(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_Address_ID)
                If lngPK_Entity_Address > 0 Then
                    lngPK_Entity_Address = 0
                End If
            End If

            'PK Entity Phone
            If objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Count > 0 Then
                lngPK_Entity_Phone = objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Min(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_PHONE)
                If lngPK_Entity_Phone > 0 Then
                    lngPK_Entity_Phone = 0
                End If
            End If

            'PK Director 
            If objGoAML_Activity_Class.list_goAML_Act_Director.Count > 0 Then
                lngPK_Director_Address = objGoAML_Activity_Class.list_goAML_Act_Director.Min(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_ID)
                If lngPK_Director_Address > 0 Then
                    lngPK_Director_Address = 0
                End If
            End If

            'PK Director Address
            If objGoAML_Activity_Class.list_goAML_Act_Director_Address.Count > 0 Then
                lngPK_Director_Address = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Min(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID)
                If lngPK_Director_Address > 0 Then
                    lngPK_Director_Address = 0
                End If
            End If

            'PK Director Phone
            If objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Count > 0 Then
                lngPK_Director_Phone = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Min(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_PHONE)
                If lngPK_Director_Phone > 0 Then
                    lngPK_Director_Phone = 0
                End If
            End If

            'PK Director Identification
            If objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Count > 0 Then
                lngPK_Director_Identification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Min(Function(x) x.PK_ACTIVITY_PERSON_IDENTIFICATION_ID)
                If lngPK_Director_Identification > 0 Then
                    lngPK_Director_Identification = 0
                End If
            End If

            With objEntity
                .FK_REPORT_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                .FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID

                .NAME = objSelEntity.Corp_Name
                .COMMERCIAL_NAME = objSelEntity.Corp_Commercial_Name
                .INCORPORATION_LEGAL_FORM = objSelEntity.Corp_Incorporation_Legal_Form
                .INCORPORATION_NUMBER = objSelEntity.Corp_Incorporation_Number
                .BUSINESS = objSelEntity.Corp_Business
                .EMAIL = objSelEntity.Corp_Email
                .URL = objSelEntity.Corp_Url
                .INCORPORATION_STATE = objSelEntity.Corp_Incorporation_State
                .INCORPORATION_COUNTRY_CODE = objSelEntity.Corp_Incorporation_Country_Code
                .INCORPORATION_DATE = objSelEntity.Corp_Incorporation_Date
                .BUSINESS_CLOSED = objSelEntity.Corp_Business_Closed
                .DATE_BUSINESS_CLOSED = objSelEntity.Corp_Date_Business_Closed
                .TAX_NUMBER = objSelEntity.Corp_Tax_Number
                .COMMENTS = objSelEntity.Corp_Comments
            End With

            If Me.IsNewRecord Then
                objEntity.Active = True
                objEntity.CreatedDate = DateTime.Now
                objEntity.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID

                objGoAML_Activity_Class.list_goAML_Act_Entity.Add(objEntity)
            Else
                objEntity.LastUpdateDate = DateTime.Now
                objEntity.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
            End If

            'Entity Address(es)  (1=Address, 5=Employer Address)
            Dim objListAddress As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_Address where (FK_Ref_Detail_Of = 1 or FK_Ref_Detail_Of = 5) and FK_To_Table_ID = " & objSelEntity.PK_Customer_ID, Nothing)
            If objListAddress IsNot Nothing AndAlso objListAddress.Rows.Count > 0 Then
                For Each objAddress As DataRow In objListAddress.Rows
                    Dim objNewEntityAddress As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_ADDRESS
                    With objNewEntityAddress
                        lngPK_Entity_Address = lngPK_Entity_Address - 1

                        .PK_SIPESATGOAML_ACT_ENTITY_Address_ID = lngPK_Entity_Address
                        .FK_REPORT_ID = objEntity.FK_REPORT_ID
                        .FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID

                        If Not IsDBNull(objAddress("Comments")) Then
                            .COMMENTS = objAddress("Comments")
                        End If
                        If Not IsDBNull(objAddress("State")) Then
                            .STATE = objAddress("State")
                        End If
                        If Not IsDBNull(objAddress("Country_Code")) Then
                            .COUNTRY_CODE = objAddress("Country_Code")
                        End If
                        If Not IsDBNull(objAddress("Zip")) Then
                            .ZIP = objAddress("Zip")
                        End If
                        If Not IsDBNull(objAddress("City")) Then
                            .CITY = objAddress("City")
                        End If
                        If Not IsDBNull(objAddress("Town")) Then
                            .TOWN = objAddress("Town")
                        End If
                        If Not IsDBNull(objAddress("Address")) Then
                            .ADDRESS = objAddress("Address")
                        End If
                        If Not IsDBNull(objAddress("Address_Type")) Then
                            .ADDRESS_TYPE = objAddress("Address_Type")
                        End If

                        'If objAddress("FK_Ref_Detail_Of") = 1 Then
                        '    .is = False
                        'Else
                        '    .ISEMPLOYER = True
                        'End If

                        .Active = 1
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedDate = DateTime.Now
                    End With
                    objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Add(objNewEntityAddress)
                Next
            End If

            'Entity Phone(s) (1=Phone, 5=Employer Phone)
            Dim objListPhone As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_Phone where (FK_Ref_Detail_Of = 1 or FK_Ref_Detail_Of = 5) and FK_for_Table_ID = " & objSelEntity.PK_Customer_ID, Nothing)
            If objListPhone IsNot Nothing AndAlso objListPhone.Rows.Count > 0 Then
                For Each objPhone As DataRow In objListPhone.Rows
                    Dim objNewEntityPhone As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_PHONE
                    With objNewEntityPhone
                        lngPK_Entity_Phone = lngPK_Entity_Phone - 1

                        .PK_SIPESATGOAML_ACT_ENTITY_PHONE = lngPK_Entity_Phone
                        .FK_REPORT_ID = objEntity.FK_REPORT_ID
                        .FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID

                        If Not IsDBNull(objPhone("Tph_Contact_Type")) Then
                            .TPH_CONTACT_TYPE = objPhone("Tph_Contact_Type")
                        End If
                        If Not IsDBNull(objPhone("Tph_Communication_Type")) Then
                            .TPH_COMMUNICATION_TYPE = objPhone("Tph_Communication_Type")
                        End If
                        If Not IsDBNull(objPhone("tph_country_prefix")) Then
                            .TPH_COUNTRY_PREFIX = objPhone("tph_country_prefix")
                        End If
                        If Not IsDBNull(objPhone("tph_number")) Then
                            .TPH_NUMBER = objPhone("tph_number")
                        End If
                        If Not IsDBNull(objPhone("tph_extension")) Then
                            .TPH_EXTENSION = objPhone("tph_extension")
                        End If
                        If Not IsDBNull(objPhone("comments")) Then
                            .COMMENTS = objPhone("comments")
                        End If

                        'If objPhone("FK_Ref_Detail_Of") = 1 Then
                        '    .ISEMPLOYER = False
                        'Else
                        '    .ISEMPLOYER = True
                        'End If

                        .Active = 1
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedDate = DateTime.Now
                    End With
                    objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Add(objNewEntityPhone)
                Next
            End If

            ''Entity Identification(s)
            'Dim objListIdentification As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Entity_Identification where FK_Entity_Type = 1 and FK_Entity_ID = " & objSelEntity.PK_Customer_ID, Nothing)
            'If objListIdentification IsNot Nothing AndAlso objListIdentification.Rows.Count > 0 Then
            '    For Each objIdentification As DataRow In objListIdentification.Rows
            '        Dim objNewEntityIdentification As New SIPESATGoAMLBLL.entity
            '        With objNewEntityIdentification
            '            lngPK_Identification = lngPK_Identification - 1

            '            .PK_ACTIVITY_Entity_IDENTIFICATION_ID = lngPK_Identification
            '            .FK_REPORT_ID = objEntity.FK_REPORT_ID
            '            '.FK_Entity_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationEntityType.Entity
            '            '.FK_Act_Entity_ID = objEntity.PK_SIPESATGOAML_ACT_Entity_ID
            '            If Not IsDBNull(objIdentification("Type")) Then
            '                .TYPE = objIdentification("Type")
            '            End If
            '            'If Not IsDBNull(objIdentification("FK_ACT_ENTITY_ID")) Then
            '            '    .FK_ACT_ENTITY_ID = objIdentification("FK_ACT_ENTITY_ID")
            '            'End If

            '            .FK_ACT_Entity = lngPK_Identification

            '            If Not IsDBNull(objIdentification("Number")) Then
            '                .NUMBER = objIdentification("Number")
            '            End If
            '            If Not IsDBNull(objIdentification("ISSUE_DATE")) Then
            '                .ISSUED_DATE = objIdentification("ISSUE_DATE")
            '            End If
            '            If Not IsDBNull(objIdentification("Expiry_Date")) Then
            '                .EXPIRY_DATE = objIdentification("Expiry_Date")
            '            End If
            '            If Not IsDBNull(objIdentification("ISSUED_BY")) Then
            '                .ISSUED_BY = objIdentification("ISSUED_BY")
            '            End If
            '            If Not IsDBNull(objIdentification("ISSUED_COUNTRY")) Then
            '                .ISSUED_COUNTRY = objIdentification("ISSUED_COUNTRY")
            '            End If
            '            If Not IsDBNull(objIdentification("Identification_Comment")) Then
            '                .IDENTIFICATION_COMMENT = objIdentification("Identification_Comment")
            '            End If

            '            .Active = 1
            '            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            '            .CreatedDate = DateTime.Now
            '        End With
            '        objGoAML_Activity_Class.list_goAML_Activity_Entity_Identification.Add(objNewEntityIdentification)
            '    Next
            'End If

            LoadEntity()
            LoadActivityShortInfo()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Load_Import_Party_Customer(strPK_Customer As String)
        Try

            Dim objSelCustomer As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from goAML_Ref_Customer where PK_Customer_ID = " & strPK_Customer, Nothing)
            If objSelCustomer IsNot Nothing Then
                Dim ObjRefCustomer As New SIPESATGoAMLBLL.goAML_Ref_Customer
                For Each item As DataRow In objSelCustomer.Rows
                    If Not IsDBNull(item("PK_Customer_ID")) Then
                        ObjRefCustomer.PK_Customer_ID = item("PK_Customer_ID")
                    End If
                    If Not IsDBNull(item("INDV_Passport_Country")) Then
                        ObjRefCustomer.INDV_Passport_Country = item("INDV_Passport_Country")
                    End If
                    If Not IsDBNull(item("INDV_ID_Number")) Then
                        ObjRefCustomer.INDV_ID_Number = item("INDV_ID_Number")
                    End If
                    If Not IsDBNull(item("INDV_Nationality1")) Then
                        ObjRefCustomer.INDV_Nationality1 = item("INDV_Nationality1")
                    End If
                    If Not IsDBNull(item("INDV_Nationality2")) Then
                        ObjRefCustomer.INDV_Nationality2 = item("INDV_Nationality2")
                    End If
                    If Not IsDBNull(item("INDV_Nationality3")) Then
                        ObjRefCustomer.INDV_Nationality3 = item("INDV_Nationality3")
                    End If
                    If Not IsDBNull(item("INDV_Residence")) Then
                        ObjRefCustomer.INDV_Residence = item("INDV_Residence")
                    End If
                    If Not IsDBNull(item("INDV_Email")) Then
                        ObjRefCustomer.INDV_Email = item("INDV_Email")
                    End If
                    If Not IsDBNull(item("INDV_Passport_Number")) Then
                        ObjRefCustomer.INDV_Passport_Number = item("INDV_Passport_Number")
                    End If
                    If Not IsDBNull(item("INDV_SSN")) Then
                        ObjRefCustomer.INDV_SSN = item("INDV_SSN")
                    End If
                    If Not IsDBNull(item("INDV_Alias")) Then
                        ObjRefCustomer.INDV_Alias = item("INDV_Alias")
                    End If
                    If Not IsDBNull(item("INDV_Mothers_Name")) Then
                        ObjRefCustomer.INDV_Mothers_Name = item("INDV_Mothers_Name")
                    End If
                    If Not IsDBNull(item("INDV_Birth_Place")) Then
                        ObjRefCustomer.INDV_Birth_Place = item("INDV_Birth_Place")
                    End If
                    If Not IsDBNull(item("INDV_BirthDate")) Then
                        ObjRefCustomer.INDV_BirthDate = item("INDV_BirthDate")
                    End If
                    If Not IsDBNull(item("INDV_Last_Name")) Then
                        ObjRefCustomer.INDV_Last_Name = item("INDV_Last_Name")
                    End If
                    If Not IsDBNull(item("INDV_Prefix")) Then
                        ObjRefCustomer.INDV_Prefix = item("INDV_Prefix")
                    End If
                    If Not IsDBNull(item("INDV_Middle_name")) Then
                        ObjRefCustomer.INDV_Middle_name = item("INDV_Middle_name")
                    End If
                    If Not IsDBNull(item("INDV_First_name")) Then
                        ObjRefCustomer.INDV_First_name = item("INDV_First_name")
                    End If
                    If Not IsDBNull(item("INDV_Title")) Then
                        ObjRefCustomer.INDV_Title = item("INDV_Title")
                    End If
                    If Not IsDBNull(item("Status_Code")) Then
                        ObjRefCustomer.Status_Code = item("Status_Code")
                    End If
                    If Not IsDBNull(item("CIF")) Then
                        ObjRefCustomer.CIF = item("CIF")
                    End If
                    If Not IsDBNull(item("INDV_Occupation")) Then
                        ObjRefCustomer.INDV_Occupation = item("INDV_Occupation")
                    End If
                    If Not IsDBNull(item("INDV_Employer_Name")) Then
                        ObjRefCustomer.INDV_Employer_Name = item("INDV_Employer_Name")
                    End If
                    If Not IsDBNull(item("Corp_Name")) Then
                        ObjRefCustomer.Corp_Name = item("Corp_Name")
                    End If
                    If Not IsDBNull(item("Corp_Commercial_Name")) Then
                        ObjRefCustomer.Corp_Commercial_Name = item("Corp_Commercial_Name")
                    End If


                    If Not IsDBNull(item("Corp_Url")) Then
                        ObjRefCustomer.Corp_Url = item("Corp_Url")
                    End If
                    If Not IsDBNull(item("Corp_Email")) Then
                        ObjRefCustomer.Corp_Email = item("Corp_Email")
                    End If
                    If Not IsDBNull(item("Corp_Business")) Then
                        ObjRefCustomer.Corp_Business = item("Corp_Business")
                    End If

                    If Not IsDBNull(item("Corp_Incorporation_Number")) Then
                        ObjRefCustomer.Corp_Incorporation_Number = item("Corp_Incorporation_Number")
                    End If
                    If Not IsDBNull(item("Corp_Incorporation_Legal_Form")) Then
                        ObjRefCustomer.Corp_Incorporation_Legal_Form = item("Corp_Incorporation_Legal_Form")
                    End If

                    If Not IsDBNull(item("FK_Customer_Type_ID")) Then
                        ObjRefCustomer.FK_Customer_Type_ID = item("FK_Customer_Type_ID")
                    End If
                    If Not IsDBNull(item("Corp_Comments")) Then
                        ObjRefCustomer.Corp_Comments = item("Corp_Comments")
                    End If
                    If Not IsDBNull(item("Corp_Tax_Registeration_Number")) Then
                        ObjRefCustomer.Corp_Tax_Registeration_Number = item("Corp_Tax_Registeration_Number")
                    End If
                    If Not IsDBNull(item("Corp_Tax_Number")) Then
                        ObjRefCustomer.Corp_Tax_Number = item("Corp_Tax_Number")
                    End If
                    If Not IsDBNull(item("Corp_Date_Business_Closed")) Then
                        ObjRefCustomer.Corp_Date_Business_Closed = item("Corp_Date_Business_Closed")
                    End If
                    If Not IsDBNull(item("Corp_Business_Closed")) Then
                        ObjRefCustomer.Corp_Business_Closed = item("Corp_Business_Closed")
                    End If
                    If Not IsDBNull(item("Corp_Incorporation_Date")) Then
                        ObjRefCustomer.Corp_Incorporation_Date = item("Corp_Incorporation_Date")
                    End If
                    If Not IsDBNull(item("Corp_Role")) Then
                        ObjRefCustomer.Corp_Role = item("Corp_Role")
                    End If
                    If Not IsDBNull(item("Corp_Director_ID")) Then
                        ObjRefCustomer.Corp_Director_ID = item("Corp_Director_ID")
                    End If
                    If Not IsDBNull(item("Corp_Incorporation_Country_Code")) Then
                        ObjRefCustomer.Corp_Incorporation_Country_Code = item("Corp_Incorporation_Country_Code")
                    End If
                    If Not IsDBNull(item("Corp_Incorporation_State")) Then
                        ObjRefCustomer.Corp_Incorporation_State = item("Corp_Incorporation_State")
                    End If
                    If Not IsDBNull(item("Active")) Then
                        ObjRefCustomer.Active = item("Active")
                    End If
                    If Not IsDBNull(item("CreatedBy")) Then
                        ObjRefCustomer.CreatedBy = item("CreatedBy")
                    End If
                    If Not IsDBNull(item("LastUpdateBy")) Then
                        ObjRefCustomer.LastUpdateBy = item("LastUpdateBy")
                    End If
                    If Not IsDBNull(item("ApprovedBy")) Then
                        ObjRefCustomer.ApprovedBy = item("ApprovedBy")
                    End If
                    If Not IsDBNull(item("CreatedDate")) Then
                        ObjRefCustomer.CreatedDate = item("CreatedDate")
                    End If
                    If Not IsDBNull(item("LastUpdateDate")) Then
                        ObjRefCustomer.LastUpdateDate = item("LastUpdateDate")
                    End If
                    If Not IsDBNull(item("ApprovedDate")) Then
                        ObjRefCustomer.ApprovedDate = item("ApprovedDate")
                    End If
                    If Not IsDBNull(item("Alternateby")) Then
                        ObjRefCustomer.Alternateby = item("Alternateby")
                    End If
                    If Not IsDBNull(item("isDeleted")) Then
                        ObjRefCustomer.isDeleted = item("isDeleted")
                    End If
                    If Not IsDBNull(item("StatusApproval")) Then
                        ObjRefCustomer.StatusApproval = item("StatusApproval")
                    End If
                    If Not IsDBNull(item("INDV_Gender")) Then
                        ObjRefCustomer.INDV_Gender = item("INDV_Gender")
                    End If
                    If Not IsDBNull(item("INDV_Deceased")) Then
                        ObjRefCustomer.INDV_Deceased = item("INDV_Deceased")
                    End If
                    If Not IsDBNull(item("INDV_Deceased_Date")) Then
                        ObjRefCustomer.INDV_Deceased_Date = item("INDV_Deceased_Date")
                    End If
                    If Not IsDBNull(item("INDV_Tax_Reg_Number")) Then
                        ObjRefCustomer.INDV_Tax_Reg_Number = item("INDV_Tax_Reg_Number")
                    End If
                    If Not IsDBNull(item("INDV_Tax_Number")) Then
                        ObjRefCustomer.INDV_Tax_Number = item("INDV_Tax_Number")
                    End If
                    If Not IsDBNull(item("INDV_Source_of_Wealth")) Then
                        ObjRefCustomer.INDV_Source_of_Wealth = item("INDV_Source_of_Wealth")
                    End If
                    If Not IsDBNull(item("INDV_Comments")) Then
                        ObjRefCustomer.INDV_Comments = item("INDV_Comments")
                    End If
                    If Not IsDBNull(item("INDV_Email2")) Then
                        ObjRefCustomer.INDV_Email2 = item("INDV_Email2")
                    End If
                    If Not IsDBNull(item("INDV_Email3")) Then
                        ObjRefCustomer.INDV_Email3 = item("INDV_Email3")
                    End If
                    If Not IsDBNull(item("INDV_Email4")) Then
                        ObjRefCustomer.INDV_Email4 = item("INDV_Email4")
                    End If
                    If Not IsDBNull(item("INDV_Email5")) Then
                        ObjRefCustomer.INDV_Email5 = item("INDV_Email5")
                    End If
                    If Not IsDBNull(item("isUpdateFromDataSource")) Then
                        ObjRefCustomer.isUpdateFromDataSource = item("isUpdateFromDataSource")
                    End If
                    If Not IsDBNull(item("GCN")) Then
                        ObjRefCustomer.GCN = item("GCN")
                    End If
                    If Not IsDBNull(item("isGCNPrimary")) Then
                        ObjRefCustomer.isGCNPrimary = item("isGCNPrimary")
                    End If
                    If Not IsDBNull(item("Opening_Date")) Then
                        ObjRefCustomer.Opening_Date = item("Opening_Date")
                    End If
                    If Not IsDBNull(item("Opening_Branch_Code")) Then
                        ObjRefCustomer.Opening_Branch_Code = item("Opening_Branch_Code")
                    End If
                    If Not IsDBNull(item("Status")) Then
                        ObjRefCustomer.Status = item("Status")
                    End If
                    If Not IsDBNull(item("Closing_Date")) Then
                        ObjRefCustomer.Closing_Date = item("Closing_Date")
                    End If
                    If Not IsDBNull(item("FK_BU_CODE")) Then
                        ObjRefCustomer.FK_BU_CODE = item("FK_BU_CODE")
                    End If
                    If Not IsDBNull(item("FK_SBU_CODE")) Then
                        ObjRefCustomer.FK_SBU_CODE = item("FK_SBU_CODE")
                    End If
                    If Not IsDBNull(item("FK_RM_CODE")) Then
                        ObjRefCustomer.FK_SBU_CODE = item("FK_RM_CODE")
                    End If
                    'If Not IsDBNull(item("country_of_birth")) Then
                    '    ObjRefCustomer.count = item("country_of_birth")
                    'End If


                    'ObjRefCustomer.count = item("country_of_birth")
                Next

                If ObjRefCustomer.FK_Customer_Type_ID = 1 Then     'Person
                    Load_Import_Party_Customer_Person(ObjRefCustomer)
                Else    'Entity
                    Load_Import_Party_Customer_Entity(ObjRefCustomer)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Load_Import_Party_Customer_Person(objSelPerson As SIPESATGoAMLBLL.goAML_Ref_Customer)
        Try
            Me.IsNewRecord = False
            CleanWindowPersonParty()



            Dim objPerson = objGoAML_Activity_Class.list_goAML_Act_Person.Where(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ID = Me.PK_Party_ID).FirstOrDefault
            If objPerson Is Nothing Then
                objPerson = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON
                objPerson.PK_SIPESATGOAML_ACT_PERSON_ID = Me.PK_Party_ID
                Me.IsNewRecord = True
            Else
                Me.IsNewRecord = False
            End If

            'Delete existing data if any
            'Person Address
            Dim listPersonAddress_Old = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Where(Function(x) x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID).ToList
            If listPersonAddress_Old IsNot Nothing Then
                For Each objAddress_Old In listPersonAddress_Old
                    objGoAML_Activity_Class.list_goAML_Act_Person_Address.Remove(objAddress_Old)
                Next
            End If

            'Person Phone
            Dim listPersonPhone_Old = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Where(Function(x) x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID).ToList
            If listPersonPhone_Old IsNot Nothing Then
                For Each objPhone_Old In listPersonPhone_Old
                    objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Remove(objPhone_Old)
                Next
            End If

            'Person Identification
            Dim listPersonIdentification_Old = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID).ToList
            If listPersonIdentification_Old IsNot Nothing Then
                For Each objIdentification_Old In listPersonIdentification_Old
                    objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Remove(objIdentification_Old)
                Next
            End If

            'Person Relationship
            Dim Relationship_Old = objGoAML_Activity_Class.list_goAML_Act_Person_Relationship.Where(Function(x) x.FK_ACT_PERSON_ID = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID).ToList
            If Relationship_Old IsNot Nothing Then
                For Each objRelationship_Old In Relationship_Old
                    objGoAML_Activity_Class.list_goAML_Act_Person_Relationship.Remove(objRelationship_Old)
                Next
            End If

            'Masukkan data baru
            'Get Last PK
            Dim lngPK_Address As Long = 0
            Dim lngPK_Phone As Long = 0
            Dim lngPK_Identification As Long = 0
            Dim lngPK_Relationship As Long = 0

            'PK Person Address
            If objGoAML_Activity_Class.list_goAML_Act_Person_Address.Count > 0 Then
                lngPK_Address = objGoAML_Activity_Class.list_goAML_Act_Person_Address.Min(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID)
                If lngPK_Address > 0 Then
                    lngPK_Address = 0
                End If
            End If

            'PK Person Phone
            If objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Count > 0 Then
                lngPK_Phone = objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Min(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_PHONE_ID)
                If lngPK_Phone > 0 Then
                    lngPK_Phone = 0
                End If
            End If

            'PK Person Identification
            If objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Count > 0 Then
                lngPK_Identification = objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Min(Function(x) x.PK_ACTIVITY_PERSON_IDENTIFICATION_ID)
                If lngPK_Identification > 0 Then
                    lngPK_Identification = 0
                End If
            End If

            'PK Person Relationship
            If objGoAML_Activity_Class.list_goAML_Act_Person_Relationship.Count > 0 Then
                lngPK_Relationship = objGoAML_Activity_Class.list_goAML_Act_Person_Relationship.Min(Function(x) x.PK_SIPESATGOAML_ACT_PERSON_RELATIONSHIP_ID)
                If lngPK_Relationship > 0 Then
                    lngPK_Relationship = 0
                End If
            End If

            With objPerson
                .FK_REPORT_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                .FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID

                .GENDER = objSelPerson.INDV_Gender
                .TITLE = objSelPerson.INDV_Title
                .FIRST_NAME = objSelPerson.INDV_First_name
                .MIDDLE_NAME = objSelPerson.INDV_Middle_name
                .PREFIX = objSelPerson.INDV_Prefix
                .LAST_NAME = objSelPerson.INDV_Last_Name
                .BIRTHDATE = objSelPerson.INDV_BirthDate
                .BITH_PLACE = objSelPerson.INDV_Birth_Place
                .MOTHERS_NAME = objSelPerson.INDV_Mothers_Name
                .ALIAS = objSelPerson.INDV_Alias
                .SSN = objSelPerson.INDV_SSN
                .PASSPORT_NUMBER = objSelPerson.INDV_Passport_Number
                .PASSPORT_COUNTRY = objSelPerson.INDV_Passport_Country
                .ID_NUMBER = objSelPerson.INDV_ID_Number
                .NATIONALITY1 = objSelPerson.INDV_Nationality1
                .NATIONALITY2 = objSelPerson.INDV_Nationality2
                .NATIONALITY3 = objSelPerson.INDV_Nationality3
                .RESIDENCE = objSelPerson.INDV_Residence
                .EMAIL = objSelPerson.INDV_Email
                .EMAIL2 = objSelPerson.INDV_Email2
                .EMAIL3 = objSelPerson.INDV_Email3
                .EMAIL4 = objSelPerson.INDV_Email4
                .EMAIL5 = objSelPerson.INDV_Email5
                .OCCUPATION = objSelPerson.INDV_Occupation
                .EMPLOYER_NAME = objSelPerson.INDV_Employer_Name
                .DECEASED = objSelPerson.INDV_Deceased
                .DECEASED_DATE = objSelPerson.INDV_Deceased_Date
                .TAX_NUMBER = objSelPerson.INDV_Tax_Number
                .TAX_REG_NUMBER = objSelPerson.INDV_Tax_Reg_Number
                .SOURCE_OF_WEALTH = objSelPerson.INDV_Source_of_Wealth
                .COMMENT = objSelPerson.INDV_Comments
            End With

            If Me.IsNewRecord Then
                objPerson.Active = 1
                objPerson.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                objPerson.CreatedDate = DateTime.Now
                objGoAML_Activity_Class.list_goAML_Act_Person.Add(objPerson)
            Else
                objPerson.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                objPerson.LastUpdateDate = DateTime.Now
            End If

            'Person Address(es)  (1=Address, 5=Employer Address)
            Dim objListAddress As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_Address where (FK_Ref_Detail_Of = 1 or FK_Ref_Detail_Of = 5) and FK_To_Table_ID = " & objSelPerson.PK_Customer_ID, Nothing)
            If objListAddress IsNot Nothing AndAlso objListAddress.Rows.Count > 0 Then
                For Each objAddress As DataRow In objListAddress.Rows
                    Dim objNewPersonAddress As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_ADDRESS
                    With objNewPersonAddress
                        lngPK_Address = lngPK_Address - 1

                        .PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID = lngPK_Address
                        .FK_REPORT_ID = objPerson.FK_REPORT_ID
                        .FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID

                        If Not IsDBNull(objAddress("Comments")) Then
                            .COMMENTS = objAddress("Comments")
                        End If
                        If Not IsDBNull(objAddress("State")) Then
                            .STATE = objAddress("State")
                        End If
                        If Not IsDBNull(objAddress("Country_Code")) Then
                            .COUNTRY_CODE = objAddress("Country_Code")
                        End If
                        If Not IsDBNull(objAddress("Zip")) Then
                            .ZIP = objAddress("Zip")
                        End If
                        If Not IsDBNull(objAddress("City")) Then
                            .CITY = objAddress("City")
                        End If
                        If Not IsDBNull(objAddress("Town")) Then
                            .TOWN = objAddress("Town")
                        End If
                        If Not IsDBNull(objAddress("Address")) Then
                            .ADDRESS = objAddress("Address")
                        End If
                        If Not IsDBNull(objAddress("Address_Type")) Then
                            .ADDRESS_TYPE = objAddress("Address_Type")
                        End If

                        If objAddress("FK_Ref_Detail_Of") = 1 Then
                            .ISEMPLOYER = False
                        Else
                            .ISEMPLOYER = True
                        End If

                        .Active = 1
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedDate = DateTime.Now
                    End With
                    objGoAML_Activity_Class.list_goAML_Act_Person_Address.Add(objNewPersonAddress)
                Next
            End If

            'Person Phone(s) (1=Phone, 5=Employer Phone)
            Dim objListPhone As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_Phone where (FK_Ref_Detail_Of = 1 or FK_Ref_Detail_Of = 5) and FK_for_Table_ID = " & objSelPerson.PK_Customer_ID, Nothing)
            If objListPhone IsNot Nothing AndAlso objListPhone.Rows.Count > 0 Then
                For Each objPhone As DataRow In objListPhone.Rows
                    Dim objNewPersonPhone As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_PHONE
                    With objNewPersonPhone
                        lngPK_Phone = lngPK_Phone - 1

                        .PK_SIPESATGOAML_ACT_PERSON_PHONE_ID = lngPK_Phone
                        .FK_REPORT_ID = objPerson.FK_REPORT_ID
                        .FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID

                        If Not IsDBNull(objPhone("Tph_Contact_Type")) Then
                            .TPH_CONTACT_TYPE = objPhone("Tph_Contact_Type")
                        End If
                        If Not IsDBNull(objPhone("Tph_Communication_Type")) Then
                            .TPH_COMMUNICATION_TYPE = objPhone("Tph_Communication_Type")
                        End If
                        If Not IsDBNull(objPhone("tph_country_prefix")) Then
                            .TPH_COUNTRY_PREFIX = objPhone("tph_country_prefix")
                        End If
                        If Not IsDBNull(objPhone("tph_number")) Then
                            .TPH_NUMBER = objPhone("tph_number")
                        End If
                        If Not IsDBNull(objPhone("tph_extension")) Then
                            .TPH_EXTENSION = objPhone("tph_extension")
                        End If
                        If Not IsDBNull(objPhone("comments")) Then
                            .COMMENTS = objPhone("comments")
                        End If

                        If objPhone("FK_Ref_Detail_Of") = 1 Then
                            .ISEMPLOYER = False
                        Else
                            .ISEMPLOYER = True
                        End If

                        .Active = 1
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedDate = DateTime.Now
                    End With
                    objGoAML_Activity_Class.list_goAML_Act_Person_Phone.Add(objNewPersonPhone)
                Next
            End If



            'Person Identification(s)
            Dim objListIdentification As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Person_Identification where FK_Person_Type = 1 and FK_Person_ID = " & objSelPerson.PK_Customer_ID, Nothing)
            If objListIdentification IsNot Nothing AndAlso objListIdentification.Rows.Count > 0 Then
                For Each objIdentification As DataRow In objListIdentification.Rows
                    Dim objNewPersonIdentification As New SIPESATGoAMLBLL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION
                    With objNewPersonIdentification
                        lngPK_Identification = lngPK_Identification - 1

                        .PK_ACTIVITY_PERSON_IDENTIFICATION_ID = lngPK_Identification
                        .FK_REPORT_ID = objPerson.FK_REPORT_ID
                        '.FK_Person_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationPersonType.Person
                        '.FK_Act_Person_ID = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID
                        If Not IsDBNull(objIdentification("Type")) Then
                            .TYPE = objIdentification("Type")
                        End If
                        'If Not IsDBNull(objIdentification("FK_ACT_ENTITY_ID")) Then
                        '    .FK_ACT_ENTITY_ID = objIdentification("FK_ACT_ENTITY_ID")
                        'End If

                        .FK_ACT_PERSON = lngPK_Identification

                        If Not IsDBNull(objIdentification("Number")) Then
                            .NUMBER = objIdentification("Number")
                        End If
                        If Not IsDBNull(objIdentification("ISSUE_DATE")) Then
                            .ISSUED_DATE = objIdentification("ISSUE_DATE")
                        End If
                        If Not IsDBNull(objIdentification("Expiry_Date")) Then
                            .EXPIRY_DATE = objIdentification("Expiry_Date")
                        End If
                        If Not IsDBNull(objIdentification("ISSUED_BY")) Then
                            .ISSUED_BY = objIdentification("ISSUED_BY")
                        End If
                        If Not IsDBNull(objIdentification("ISSUED_COUNTRY")) Then
                            .ISSUED_COUNTRY = objIdentification("ISSUED_COUNTRY")
                        End If
                        If Not IsDBNull(objIdentification("Identification_Comment")) Then
                            .IDENTIFICATION_COMMENT = objIdentification("Identification_Comment")
                        End If

                        .Active = 1
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedDate = DateTime.Now
                    End With
                    objGoAML_Activity_Class.list_goAML_Activity_Person_Identification.Add(objNewPersonIdentification)
                Next
            End If

            'Person Relationship(s)
            Dim objListRelationship As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goaml_ref_customer where pk_Customer_id = " & objSelPerson.PK_Customer_ID, Nothing)
            If objListRelationship IsNot Nothing AndAlso objListRelationship.Rows.Count > 0 Then
                For Each objRelationship As DataRow In objListRelationship.Rows
                    Dim objNewPersonRelationship As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_PERSON_RELATIONSHIP
                    With objNewPersonRelationship
                        lngPK_Relationship = lngPK_Relationship - 1

                        .PK_SIPESATGOAML_ACT_PERSON_RELATIONSHIP_ID = lngPK_Relationship
                        .FK_REPORT_ID = objPerson.FK_REPORT_ID
                        '.FK_Person_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumRelationshipPersonType.Person
                        '.FK_Act_Person_ID = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID
                        If Not IsDBNull(objRelationship("ApprovedDate")) Then
                            .ApprovedDate = objRelationship("ApprovedDate")
                        End If

                        If Not IsDBNull(objRelationship("CreatedBy")) Then
                            .CreatedBy = objRelationship("CreatedBy")
                        End If
                        If Not IsDBNull(objRelationship("CreatedDate")) Then
                            .CreatedDate = objRelationship("CreatedDate")
                        End If
                        If Not IsDBNull(objRelationship("CIF")) Then
                            .CLIENT_NUMBER = objRelationship("CIF")
                        End If

                        .FK_ACT_PERSON_ID = lngPK_Relationship



                        If Not IsDBNull(objRelationship("Opening_Date")) Then
                            .VALID_FROM = objRelationship("Opening_Date")
                        End If

                        If Not IsDBNull(objRelationship("Closing_Date")) Then
                            .VALID_TO = objRelationship("Closing_Date")
                        End If
                        .Active = 1
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedDate = DateTime.Now
                    End With
                    objGoAML_Activity_Class.list_goAML_Act_Person_Relationship.Add(objNewPersonRelationship)
                Next
            End If


            LoadPerson()
            LoadActivityShortInfo()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Load_Import_Party_Customer_Entity(objSelEntity As SIPESATGoAMLBLL.goAML_Ref_Customer)
        Try
            Me.IsNewRecord = False
            CleanWindowEntityParty()

            Using objdb As New NawaDevDAL.NawaDatadevEntities
                Dim objEntity = objGoAML_Activity_Class.list_goAML_Act_Entity.Where(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_ID = Me.PK_Party_ID).FirstOrDefault
                If objEntity Is Nothing Then
                    objEntity = New SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY
                    objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID = Me.PK_Party_ID
                    Me.IsNewRecord = True
                Else
                    Me.IsNewRecord = False
                End If

                'Delete existing data if any
                'Entity Address
                Dim listEntityAddress_Old = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
                If listEntityAddress_Old IsNot Nothing Then
                    For Each objAddress_Old In listEntityAddress_Old
                        objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Remove(objAddress_Old)
                    Next
                End If

                'Entity Phone
                Dim listEntityPhone_Old = objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
                If listEntityPhone_Old IsNot Nothing Then
                    For Each objPhone_Old In listEntityPhone_Old
                        objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Remove(objPhone_Old)
                    Next
                End If

                'Director
                Dim listDirector_Old = objGoAML_Activity_Class.list_goAML_Act_Director.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
                If listDirector_Old IsNot Nothing Then
                    For Each objDirector_Old In listDirector_Old
                        objGoAML_Activity_Class.list_goAML_Act_Director.Remove(objDirector_Old)

                        'Director Address
                        Dim listDirectorAddress_Old = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objDirector_Old.PK_SIPESATGOAML_ACT_DIRECTOR_ID).ToList
                        If listDirectorAddress_Old IsNot Nothing Then
                            For Each objAddress_Old In listDirectorAddress_Old
                                objGoAML_Activity_Class.list_goAML_Act_Director_Address.Remove(objAddress_Old)
                            Next
                        End If

                        'Director Phone
                        Dim listDirectorPhone_Old = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objDirector_Old.PK_SIPESATGOAML_ACT_DIRECTOR_ID).ToList
                        If listDirectorPhone_Old IsNot Nothing Then
                            For Each objPhone_Old In listDirectorPhone_Old
                                objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Remove(objPhone_Old)
                            Next
                        End If

                        'Director Identification
                        Dim listDirectorIdentification_Old = objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Where(Function(x) x.FK_ACT_DIRECTOR_ID = objDirector_Old.PK_SIPESATGOAML_ACT_DIRECTOR_ID).ToList
                        If listDirectorIdentification_Old IsNot Nothing Then
                            For Each objIdentification_Old In listDirectorIdentification_Old
                                objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Remove(objIdentification_Old)
                            Next
                        End If
                    Next
                End If
                'Entity Relationship
                Dim Relationship_Old = objGoAML_Activity_Class.list_goAML_Act_Entity_Relationship.Where(Function(x) x.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID).ToList
                If Relationship_Old IsNot Nothing Then
                    For Each objRelationship_Old In Relationship_Old
                        objGoAML_Activity_Class.list_goAML_Act_Entity_Relationship.Remove(objRelationship_Old)
                    Next
                End If

                'Masukkan data baru
                'Get Last PK
                Dim lngPK_Entity_Address As Long = 0
                Dim lngPK_Entity_Phone As Long = 0
                Dim lngPK_Relationship As Long = 0
                Dim lngPK_Director As Long = 0
                Dim lngPK_Director_Address As Long = 0
                Dim lngPK_Director_Phone As Long = 0
                Dim lngPK_Director_Identification As Long = 0

                'PK Entity Address
                If objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Count > 0 Then
                    lngPK_Entity_Address = objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Min(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_Address_ID)
                    If lngPK_Entity_Address > 0 Then
                        lngPK_Entity_Address = 0
                    End If
                End If

                'PK Entity Phone
                If objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Count > 0 Then
                    lngPK_Entity_Phone = objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Min(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_PHONE)
                    If lngPK_Entity_Phone > 0 Then
                        lngPK_Entity_Phone = 0
                    End If
                End If

                'PK Director 
                If objGoAML_Activity_Class.list_goAML_Act_Director.Count > 0 Then
                    lngPK_Director_Address = objGoAML_Activity_Class.list_goAML_Act_Director.Min(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_ID)
                    If lngPK_Director_Address > 0 Then
                        lngPK_Director_Address = 0
                    End If
                End If

                'PK Director Address
                If objGoAML_Activity_Class.list_goAML_Act_Director_Address.Count > 0 Then
                    lngPK_Director_Address = objGoAML_Activity_Class.list_goAML_Act_Director_Address.Min(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID)
                    If lngPK_Director_Address > 0 Then
                        lngPK_Director_Address = 0
                    End If
                End If

                'PK Director Phone
                If objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Count > 0 Then
                    lngPK_Director_Phone = objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Min(Function(x) x.PK_SIPESATGOAML_ACT_DIRECTOR_PHONE)
                    If lngPK_Director_Phone > 0 Then
                        lngPK_Director_Phone = 0
                    End If
                End If

                'PK Director Identification
                If objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Count > 0 Then
                    lngPK_Director_Identification = objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Min(Function(x) x.PK_ACT_DIRECTOR_IDENTIFICATION_ID)
                    If lngPK_Director_Identification > 0 Then
                        lngPK_Director_Identification = 0
                    End If
                End If

                'PK entity Relationship
                If objGoAML_Activity_Class.list_goAML_Act_Entity_Relationship.Count > 0 Then
                    lngPK_Relationship = objGoAML_Activity_Class.list_goAML_Act_Entity_Relationship.Min(Function(x) x.PK_SIPESATGOAML_ACT_ENTITY_RELATIONSHIP_ID)
                    If lngPK_Relationship > 0 Then
                        lngPK_Relationship = 0
                    End If
                End If

                With objEntity
                    .FK_REPORT_ID = objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                    .FK_ACT_REPORTPARTY_ID = Me.PK_Activity_ID

                    .NAME = objSelEntity.Corp_Name
                    .COMMERCIAL_NAME = objSelEntity.Corp_Commercial_Name
                    .INCORPORATION_LEGAL_FORM = objSelEntity.Corp_Incorporation_Legal_Form
                    .INCORPORATION_NUMBER = objSelEntity.Corp_Incorporation_Number
                    .BUSINESS = objSelEntity.Corp_Business
                    .EMAIL = objSelEntity.Corp_Email
                    .URL = objSelEntity.Corp_Url
                    .INCORPORATION_STATE = objSelEntity.Corp_Incorporation_State
                    .INCORPORATION_COUNTRY_CODE = objSelEntity.Corp_Incorporation_Country_Code
                    .INCORPORATION_DATE = objSelEntity.Corp_Incorporation_Date
                    .BUSINESS_CLOSED = objSelEntity.Corp_Business_Closed
                    .DATE_BUSINESS_CLOSED = objSelEntity.Corp_Date_Business_Closed
                    .TAX_NUMBER = objSelEntity.Corp_Tax_Number
                    .COMMENTS = objSelEntity.Corp_Comments
                End With

                If Me.IsNewRecord Then
                    objEntity.Active = True
                    objEntity.CreatedDate = DateTime.Now
                    objEntity.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID

                    objGoAML_Activity_Class.list_goAML_Act_Entity.Add(objEntity)
                Else
                    objEntity.LastUpdateDate = DateTime.Now
                    objEntity.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                End If

                'Entity Address(es) 
                Dim objListEntityAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = objSelEntity.PK_Customer_ID).ToList
                If objListEntityAddress IsNot Nothing Then
                    For Each objAddress In objListEntityAddress
                        Dim objNewEntityAddress As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_ADDRESS
                        With objNewEntityAddress
                            lngPK_Entity_Address = lngPK_Entity_Address - 1

                            .PK_SIPESATGOAML_ACT_ENTITY_Address_ID = lngPK_Entity_Address
                            .FK_REPORT_ID = objEntity.FK_REPORT_ID
                            .FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID

                            .ADDRESS_TYPE = objAddress.Address_Type
                            .ADDRESS = objAddress.Address
                            .TOWN = objAddress.Town
                            .CITY = objAddress.City
                            .ZIP = objAddress.Zip
                            .COUNTRY_CODE = objAddress.Country_Code
                            .STATE = objAddress.State
                            .COMMENTS = objAddress.Comments

                            .Active = 1
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                        End With
                        objGoAML_Activity_Class.list_goAML_Act_Entity_Address.Add(objNewEntityAddress)
                    Next
                End If

                'Entity Phone(s) 
                Dim objListEntityPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = objSelEntity.PK_Customer_ID).ToList
                If objListEntityPhone IsNot Nothing Then
                    For Each objPhone In objListEntityPhone
                        Dim objNewEntityPhone As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_PHONE
                        With objNewEntityPhone
                            lngPK_Director_Phone = lngPK_Director_Phone - 1

                            .PK_SIPESATGOAML_ACT_ENTITY_PHONE = lngPK_Director_Phone
                            .FK_REPORT_ID = objEntity.FK_REPORT_ID
                            .FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID

                            .TPH_CONTACT_TYPE = objPhone.Tph_Contact_Type
                            .TPH_COMMUNICATION_TYPE = objPhone.Tph_Communication_Type
                            .TPH_COUNTRY_PREFIX = objPhone.tph_country_prefix
                            .TPH_NUMBER = objPhone.tph_number
                            .TPH_EXTENSION = objPhone.tph_extension
                            .COMMENTS = objPhone.comments

                            .Active = 1
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                        End With
                        objGoAML_Activity_Class.list_goAML_Act_Entity_Phone.Add(objNewEntityPhone)
                    Next
                End If

                'Person Relationship(s)
                Dim objListRelationship As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goaml_ref_customer where pk_Customer_id = " & objSelEntity.PK_Customer_ID, Nothing)
                If objListRelationship IsNot Nothing AndAlso objListRelationship.Rows.Count > 0 Then
                    For Each objRelationship As DataRow In objListRelationship.Rows
                        Dim objNewentityRelationship As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_ENTITY_RELATIONSHIP
                        With objNewentityRelationship
                            lngPK_Relationship = lngPK_Relationship - 1

                            .PK_SIPESATGOAML_ACT_ENTITY_RELATIONSHIP_ID = lngPK_Relationship
                            .FK_REPORT_ID = objEntity.FK_REPORT_ID
                            '.FK_entity_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumRelationshipentityType.entity
                            '.FK_Act_entity_ID = objentity.PK_SIPESATGOAML_ACT_entity_ID
                            If Not IsDBNull(objRelationship("ApprovedDate")) Then
                                .ApprovedDate = objRelationship("ApprovedDate")
                            End If

                            If Not IsDBNull(objRelationship("CreatedBy")) Then
                                .CreatedBy = objRelationship("CreatedBy")
                            End If
                            If Not IsDBNull(objRelationship("CreatedDate")) Then
                                .CreatedDate = objRelationship("CreatedDate")
                            End If
                            If Not IsDBNull(objRelationship("CIF")) Then
                                .CLIENT_NUMBER = objRelationship("CIF")
                            End If

                            .FK_ACT_ENTITY_ID = lngPK_Relationship



                            If Not IsDBNull(objRelationship("Opening_Date")) Then
                                .VALID_FROM = objRelationship("Opening_Date")
                            End If

                            If Not IsDBNull(objRelationship("Closing_Date")) Then
                                .VALID_TO = objRelationship("Closing_Date")
                            End If
                            .Active = 1
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                        End With
                        objGoAML_Activity_Class.list_goAML_Act_Entity_Relationship.Add(objNewentityRelationship)
                    Next
                End If


                'Director
                Dim objListDirector As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_Customer_Entity_Director where FK_Entity_ID = '" & objSelEntity.CIF & "'", Nothing)
                If objListDirector IsNot Nothing AndAlso objListDirector.Rows.Count > 0 Then
                    For Each objDirector As DataRow In objListDirector.Rows
                        Dim objNewDirector_New As New SIPESATGOAML_ACT_DIRECTOR
                        With objNewDirector_New
                            lngPK_Director = lngPK_Director - 1

                            .PK_SIPESATGOAML_ACT_DIRECTOR_ID = lngPK_Director
                            .FK_REPORT_ID = objEntity.FK_REPORT_ID
                            .FK_REPORTPARTY_ID = Me.PK_Activity_ID
                            .FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID
                            .FK_SENDER_INFORMATION = enumPartyType._Entity

                            If Not IsDBNull(objDirector("Gender")) Then
                                .GENDER = objDirector("Gender")
                            End If
                            If Not IsDBNull(objDirector("Title")) Then
                                .TITLE = objDirector("Title")
                            End If
                            If Not IsDBNull(objDirector("First_name")) Then
                                .FIRST_NAME = objDirector("First_name")
                            End If
                            If Not IsDBNull(objDirector("Middle_Name")) Then
                                .MIDDLE_NAME = objDirector("Middle_Name")
                            End If
                            If Not IsDBNull(objDirector("Prefix")) Then
                                .PREFIX = objDirector("Prefix")
                            End If
                            If Not IsDBNull(objDirector("Last_Name")) Then
                                .LAST_NAME = objDirector("Last_Name")
                            End If
                            If Not IsDBNull(objDirector("BirthDate")) Then
                                .BIRTHDATE = objDirector("BirthDate")
                            End If
                            If Not IsDBNull(objDirector("Birth_Place")) Then
                                .BIRTH_PLACE = objDirector("Birth_Place")
                            End If
                            If Not IsDBNull(objDirector("Mothers_Name")) Then
                                .MOTHERS_NAME = objDirector("Mothers_Name")
                            End If
                            If Not IsDBNull(objDirector("Alias")) Then
                                .ALIAS = objDirector("Alias")
                            End If
                            If Not IsDBNull(objDirector("SSN")) Then
                                .SSN = objDirector("SSN")
                            End If
                            If Not IsDBNull(objDirector("Passport_Number")) Then
                                .PASSPORT_NUMBER = objDirector("Passport_Number")
                            End If
                            If Not IsDBNull(objDirector("Passport_Country")) Then
                                .PASSPORT_COUNTRY = objDirector("Passport_Country")
                            End If
                            If Not IsDBNull(objDirector("ID_Number")) Then
                                .ID_NUMBER = objDirector("ID_Number")
                            End If
                            If Not IsDBNull(objDirector("Nationality1")) Then
                                .NATIONALITY1 = objDirector("Nationality1")
                            End If
                            If Not IsDBNull(objDirector("Nationality2")) Then
                                .NATIONALITY2 = objDirector("Nationality2")
                            End If
                            If Not IsDBNull(objDirector("Nationality3")) Then
                                .NATIONALITY3 = objDirector("Nationality3")
                            End If
                            If Not IsDBNull(objDirector("Residence")) Then
                                .RESIDENCE = objDirector("Residence")
                            End If
                            If Not IsDBNull(objDirector("Email")) Then
                                .EMAIL = objDirector("Email")
                            End If
                            If Not IsDBNull(objDirector("Email2")) Then
                                .EMAIL2 = objDirector("Email2")
                            End If
                            If Not IsDBNull(objDirector("Email3")) Then
                                .EMAIL3 = objDirector("Email3")
                            End If
                            If Not IsDBNull(objDirector("Email4")) Then
                                .EMAIL4 = objDirector("Email4")
                            End If
                            If Not IsDBNull(objDirector("Email5")) Then
                                .EMAIL5 = objDirector("Email5")
                            End If
                            If Not IsDBNull(objDirector("Occupation")) Then
                                .OCCUPATION = objDirector("Occupation")
                            End If
                            If Not IsDBNull(objDirector("Employer_Name")) Then
                                .EMPLOYER_NAME = objDirector("Employer_Name")
                            End If
                            If Not IsDBNull(objDirector("Deceased")) Then
                                .DECEASED = objDirector("Deceased")
                            End If
                            If Not IsDBNull(objDirector("Deceased_Date")) Then
                                .DECEASED_DATE = objDirector("Deceased_Date")
                            End If
                            If Not IsDBNull(objDirector("Tax_Number")) Then
                                .TAX_NUMBER = objDirector("Tax_Number")
                            End If
                            If Not IsDBNull(objDirector("Tax_Reg_Number")) Then
                                .TAX_REG_NUMBER = objDirector("Tax_Reg_Number")
                            End If
                            If Not IsDBNull(objDirector("Source_of_Wealth")) Then
                                .SOURCE_OF_WEALTH = objDirector("Source_of_Wealth")
                            End If
                            If Not IsDBNull(objDirector("COMMENTS")) Then
                                .COMMENT = objDirector("COMMENTS")
                            End If
                            If Not IsDBNull(objDirector("Role")) Then
                                .ROLE = objDirector("Role")
                            End If

                            .Active = 1
                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .CreatedDate = DateTime.Now
                        End With
                        objGoAML_Activity_Class.list_goAML_Act_Director.Add(objNewDirector_New)
                        Dim PkDirector As Long
                        If Not IsDBNull(objDirector("PK_goAML_Ref_Customer_Entity_Director_ID")) Then
                            PkDirector = objDirector("PK_goAML_Ref_Customer_Entity_Director_ID")
                        End If
                        'Director Address(es) 
                        Dim objListDirectorAddress = objdb.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_To_Table_ID = PkDirector).ToList
                        If objListDirectorAddress IsNot Nothing Then
                            For Each objAddress In objListDirectorAddress
                                Dim objNewDirectorAddress As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_ADDRESS
                                With objNewDirectorAddress
                                    lngPK_Director_Address = lngPK_Director_Address - 1

                                    .PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID = lngPK_Director_Address
                                    .FK_REPORT_ID = objEntity.FK_REPORT_ID
                                    .FK_ACT_DIRECTOR_ID = lngPK_Director

                                    .ADDRESS_Type = objAddress.Address_Type
                                    .ADDRESS = objAddress.Address
                                    .TOWN = objAddress.Town
                                    .CITY = objAddress.City
                                    .ZIP = objAddress.Zip
                                    .COUNTRY_CODE = objAddress.Country_Code
                                    .STATE = objAddress.State
                                    .COMMENTS = objAddress.Comments
                                    If objAddress.FK_Ref_Detail_Of IsNot Nothing Then
                                        If objAddress.FK_Ref_Detail_Of = 7 Then
                                            .ISEMPLOYER = True
                                        Else
                                            .ISEMPLOYER = False
                                        End If
                                    Else
                                        .ISEMPLOYER = False
                                    End If


                                    .Active = 1
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                End With
                                objGoAML_Activity_Class.list_goAML_Act_Director_Address.Add(objNewDirectorAddress)
                            Next
                        End If

                        'Director Phone(s) 
                        Dim objListDirectorPhone = objdb.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_for_Table_ID = PkDirector).ToList
                        If objListDirectorPhone IsNot Nothing Then
                            For Each objPhone In objListDirectorPhone
                                Dim objNewDirectorPhone As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_PHONE
                                With objNewDirectorPhone
                                    lngPK_Director_Phone = lngPK_Director_Phone - 1

                                    .PK_SIPESATGOAML_ACT_DIRECTOR_PHONE = lngPK_Director_Phone
                                    .FK_REPORT_ID = objEntity.FK_REPORT_ID
                                    .FK_ACT_DIRECTOR_ID = lngPK_Director

                                    .TPH_CONTACT_TYPE = objPhone.Tph_Contact_Type
                                    .TPH_COMMUNICATION_TYPE = objPhone.Tph_Communication_Type
                                    .TPH_COUNTRY_PREFIX = objPhone.tph_country_prefix
                                    .TPH_NUMBER = objPhone.tph_number
                                    .TPH_EXTENSION = objPhone.tph_extension
                                    .COMMENTS = objPhone.comments
                                    If objPhone.FK_Ref_Detail_Of IsNot Nothing Then
                                        If objPhone.FK_Ref_Detail_Of = 7 Then
                                            .ISEMPLOYER = True
                                        Else
                                            .ISEMPLOYER = False
                                        End If
                                    Else
                                        .ISEMPLOYER = False
                                    End If
                                    .Active = 1
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                End With
                                objGoAML_Activity_Class.list_goAML_Act_Director_Phone.Add(objNewDirectorPhone)
                            Next
                        End If

                        Dim intDirectrorAccountPersonType = GoAMLBLL.GoAML_Report_BLL.getDirectorAccountPersonType()

                        'Director Identification(s)
                        Dim objListIdentification = objdb.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = intDirectrorAccountPersonType And x.FK_Person_ID = PkDirector).ToList
                        If objListIdentification IsNot Nothing Then
                            For Each objIdentification In objListIdentification
                                Dim objNewDirectorIdentification As New SIPESATGoAMLBLL.SIPESATGOAML_ACT_DIRECTOR_IDENTIFICATION
                                With objNewDirectorIdentification
                                    lngPK_Director_Identification = lngPK_Director_Identification - 1

                                    .PK_ACT_DIRECTOR_IDENTIFICATION_ID = lngPK_Director_Identification
                                    .FK_REPORT_ID = objEntity.FK_REPORT_ID
                                    .FK_ACT_DIRECTOR_ID = lngPK_Director

                                    '.FK_Director_Type = SIPESATGoAMLBLL.SIPESATGoAML_Report_BLL.enumIdentificationDirectorType.Director
                                    '.FK_Act_Director_ID = objDirector.PK_SIPESATGOAML_ACT_Director_ID
                                    'If Not IsDBNull(objIdentification("Type")) Then
                                    '    .TYPE = objIdentification("Type")
                                    'End If
                                    ''If Not IsDBNull(objIdentification("FK_ACT_Director_ID")) Then
                                    ''    .FK_ACT_Director_ID = objIdentification("FK_ACT_Director_ID")
                                    ''End If

                                    '.FK_ACT_Director = lngPK_Identification

                                    'If Not IsDBNull(objIdentification("Number")) Then
                                    '    .NUMBER = objIdentification("Number")
                                    'End If
                                    'If Not IsDBNull(objIdentification("ISSUE_DATE")) Then
                                    '    .ISSUED_DATE = objIdentification("ISSUE_DATE")
                                    'End If
                                    'If Not IsDBNull(objIdentification("Expiry_Date")) Then
                                    '    .EXPIRY_DATE = objIdentification("Expiry_Date")
                                    'End If
                                    'If Not IsDBNull(objIdentification("ISSUED_BY")) Then
                                    '    .ISSUED_BY = objIdentification("ISSUED_BY")
                                    'End If
                                    'If Not IsDBNull(objIdentification("ISSUED_COUNTRY")) Then
                                    '    .ISSUED_COUNTRY = objIdentification("ISSUED_COUNTRY")
                                    'End If
                                    'If Not IsDBNull(objIdentification("Identification_Comment")) Then
                                    '    .IDENTIFICATION_COMMENT = objIdentification("Identification_Comment")
                                    'End If

                                    '.Active = 1
                                    '.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    '.CreatedDate = DateTime.Now
                                    .TYPE = objIdentification.Type
                                    .NUMBER = objIdentification.Number
                                    .ISSUED_DATE = objIdentification.Issue_Date
                                    .EXPIRY_DATE = objIdentification.Expiry_Date
                                    .ISSUED_BY = objIdentification.Issued_By
                                    .ISSUED_COUNTRY = objIdentification.Issued_Country
                                    .IDENTIFICATION_COMMENT = objIdentification.Identification_Comment

                                    .Active = 1
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                End With

                                objGoAML_Activity_Class.list_goAML_Act_Director_Identification.Add(objNewDirectorIdentification)
                            Next
                        End If
                    Next
                End If

                LoadEntity()
                LoadActivityShortInfo()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_Import_Party_Back_Click()
        Try
            window_Import_Party.Hidden = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Set Form Readonly"
    Protected Sub SetReadOnlyWindowActivity(isReadOnly As Boolean)
        Try
            '11 Jan 2023 Ari 
            cmb_Activity_Information.IsReadOnly = isReadOnly
            txt_Activity_Significance.ReadOnly = isReadOnly
            txt_Activity_Reason.ReadOnly = isReadOnly
            txt_Activity_Comment.ReadOnly = isReadOnly

            'Activity Party short information
            txt_Activity_CIFNO_ACCOUNTNO.ReadOnly = isReadOnly
            txt_Activity_Name.ReadOnly = isReadOnly
            'txt_Activity_Address.ReadOnly = isReadOnly

            btn_Activity_Edit.Hidden = isReadOnly
            btn_Activity_Save.Hidden = isReadOnly
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub SetReadOnlyWindowIndicator(isReadOnly As Boolean)
    '    Try
    '        cmb_Indicator.IsReadOnly = isReadOnly
    '        btn_Indicator_Save.Hidden = isReadOnly
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Protected Sub SetReadOnlyWindowAccount(isReadOnly As Boolean)
        Try
            txt_Account_InstitutionName.ReadOnly = isReadOnly
            txt_Account_InstitutionCode.ReadOnly = isReadOnly
            txt_Account_SwiftCode.ReadOnly = isReadOnly
            'chk_Account_NonBankingInstitution.ReadOnly = isReadOnly
            txt_Account_Branch.ReadOnly = isReadOnly
            txt_Account_Number.ReadOnly = isReadOnly
            cmb_Account_CurrencyCode.IsReadOnly = isReadOnly
            'txt_Account_Label.ReadOnly = isReadOnly
            'txt_Account_IBAN.ReadOnly = isReadOnly
            txt_Account_ClientNumber.ReadOnly = isReadOnly
            cmb_Account_Type.IsReadOnly = isReadOnly
            txt_Account_OpeningDate.ReadOnly = isReadOnly
            txt_Account_ClosingDate.ReadOnly = isReadOnly
            'txt_Account_Balance.ReadOnly = isReadOnly
            'txt_Account_BalanceDate.ReadOnly = isReadOnly
            cmb_Account_StatusCode.IsReadOnly = isReadOnly
            'txt_Account_Beneficiary.ReadOnly = isReadOnly
            'txt_Account_BeneficiaryComment.ReadOnly = isReadOnly
            txt_Account_Comment.ReadOnly = isReadOnly

            'Is Entity Account?
            'chk_Account_IsEntity.ReadOnly = isReadOnly
            txt_Account_Entity_CorporateName.ReadOnly = isReadOnly
            'txt_Account_Entity_CommercialName.ReadOnly = isReadOnly
            'cmb_Account_Entity_IncorporationLegalForm.IsReadOnly = isReadOnly
            'txt_Account_Entity_IncorporationNumber.ReadOnly = isReadOnly
            txt_Account_Entity_Business.ReadOnly = isReadOnly
            'txt_Account_Entity_Email.ReadOnly = isReadOnly
            'txt_Account_Entity_URL.ReadOnly = isReadOnly
            txt_Account_Entity_IncorporationState.ReadOnly = isReadOnly
            cmb_Account_Entity_IncorporationCountryCode.IsReadOnly = isReadOnly
            txt_Account_Entity_IncorporationDate.ReadOnly = isReadOnly
            'txt_Account_Entity_ClosingDate.ReadOnly = isReadOnly
            'txt_Account_Entity_ClosingDate.ReadOnly = isReadOnly
            txt_Account_Entity_TaxNumber.ReadOnly = isReadOnly
            'txt_Account_Entity_Comment.ReadOnly = isReadOnly

            btn_Account_Import_Account.Hidden = isReadOnly
            'btn_Account_Save.Hidden = isReadOnly
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SetReadOnlyWindowPerson(isReadOnly As Boolean)
        Try
            cmb_Person_Gender.IsReadOnly = isReadOnly
            txt_Person_Title.ReadOnly = isReadOnly
            txt_Person_LastName.ReadOnly = isReadOnly
            txt_Person_BirthDate.ReadOnly = isReadOnly
            txt_Person_BirthPlace.ReadOnly = isReadOnly
            txt_Person_MotherName.ReadOnly = isReadOnly
            txt_Person_Alias.ReadOnly = isReadOnly

            txt_Person_SSN.ReadOnly = isReadOnly
            txt_Person_PassportNumber.ReadOnly = isReadOnly
            cmb_Person_PassportCountry.IsReadOnly = isReadOnly
            txt_Person_IDNumber.ReadOnly = isReadOnly

            cmb_Person_Nationality1.IsReadOnly = isReadOnly
            cmb_Person_Nationality2.IsReadOnly = isReadOnly
            cmb_Person_Nationality3.IsReadOnly = isReadOnly
            cmb_Person_Residence.IsReadOnly = isReadOnly

            txt_Person_Email1.ReadOnly = isReadOnly
            txt_Person_Email2.ReadOnly = isReadOnly
            txt_Person_Email3.ReadOnly = isReadOnly
            txt_Person_Email4.ReadOnly = isReadOnly
            txt_Person_Email5.ReadOnly = isReadOnly

            txt_Person_Occupation.ReadOnly = isReadOnly
            txt_Person_EmployerName.ReadOnly = isReadOnly
            chk_Person_IsDecease.ReadOnly = isReadOnly
            txt_Person_DeceaseDate.ReadOnly = isReadOnly

            txt_Person_TaxNumber.ReadOnly = isReadOnly
            chk_Person_IsPEP.ReadOnly = isReadOnly
            txt_Person_SourceOfWealth.ReadOnly = isReadOnly
            txt_Person_Comment.ReadOnly = isReadOnly

            'chk_Person_AccountSignatory_IsPrimary.ReadOnly = isReadOnly
            'cmb_Person_AccountSignatory_Role.IsReadOnly = isReadOnly
            cmb_Person_Director_Role.IsReadOnly = isReadOnly

            'chk_Person_AccountSignatory_IsPrimary.ReadOnly = isReadOnly
            'cmb_Person_AccountSignatory_Role.IsReadOnly = isReadOnly
            'cmb_Person_Director_Role.IsReadOnly = isReadOnly

            btn_Person_Import_Customer.Disabled = isReadOnly
            'btn_Person_Import_WIC.Disabled = isReadOnly
            btn_Person_Save.Hidden = isReadOnly
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SetReadOnlyWindowEntity(isReadOnly As Boolean)
        Try
            txt_Entity_CorporateName.ReadOnly = isReadOnly
            txt_Entity_CommercialName.ReadOnly = isReadOnly
            cmb_Entity_IncorporationLegalForm.IsReadOnly = isReadOnly
            txt_Entity_IncorporationNumber.ReadOnly = isReadOnly
            txt_Entity_Business.ReadOnly = isReadOnly
            txt_Entity_Email.ReadOnly = isReadOnly
            txt_Entity_URL.ReadOnly = isReadOnly
            txt_Entity_IncorporationState.ReadOnly = isReadOnly
            cmb_Entity_IncorporationCountryCode.IsReadOnly = isReadOnly
            txt_Entity_IncorporationDate.ReadOnly = isReadOnly
            chk_Entity_IsClosed.ReadOnly = isReadOnly
            txt_Entity_ClosingDate.ReadOnly = isReadOnly
            txt_Entity_TaxNumber.ReadOnly = isReadOnly
            txt_Entity_Comment.ReadOnly = isReadOnly

            btn_Entity_Import_Customer.Hidden = isReadOnly
            'btn_Entity_Import_WIC.Hidden = isReadOnly
            'btn_Person_Save.Hidden = isReadOnly
            btn_Entity_Save.Hidden = isReadOnly
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SetReadOnlyWindowAddress(isReadOnly As Boolean)
        Try
            cmb_Address_Type.IsReadOnly = isReadOnly
            txt_Address_Address.ReadOnly = isReadOnly
            txt_Address_Town.ReadOnly = isReadOnly
            txt_Address_City.ReadOnly = isReadOnly
            'txt_Address_Zip.ReadOnly = isReadOnly
            cmb_Address_Country.IsReadOnly = isReadOnly
            txt_Address_State.ReadOnly = isReadOnly
            'txt_Address_Comment.ReadOnly = isReadOnly

            btn_Address_Save.Hidden = isReadOnly
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SetReadOnlyWindowPhone(isReadOnly As Boolean)
        Try
            cmb_Phone_ContactType.IsReadOnly = isReadOnly
            cmb_Phone_CommunicationType.IsReadOnly = isReadOnly
            'txt_Phone_CountryPrefix.ReadOnly = isReadOnly
            txt_Phone_Number.ReadOnly = isReadOnly
            'txt_Phone_Extension.ReadOnly = isReadOnly
            'txt_Phone_Comment.ReadOnly = isReadOnly

            btn_Phone_Save.Hidden = isReadOnly
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SetReadOnlyWindowIdentification(isReadOnly As Boolean)
        Try
            cmb_Identification_Type.IsReadOnly = isReadOnly
            txt_Identification_Number.ReadOnly = isReadOnly
            txt_Identification_IssueDate.ReadOnly = isReadOnly
            txt_Identification_ExpiredDate.ReadOnly = isReadOnly
            txt_Identification_IssueBy.ReadOnly = isReadOnly
            cmb_Identification_Country.IsReadOnly = isReadOnly
            txt_Identification_Comment.ReadOnly = isReadOnly

            btn_Identification_Save.Hidden = isReadOnly
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Protected Sub SetInitialReportData()
        Try
            'Create new Report Class
            objGoAML_Report_Class = New SIPESATGoAMLBLL.SIPESATGoAML_Report_Class

            'Clean Report Input
            txt_FK_Report_ID.Value = Nothing
            cmb_SubmisionCode.SetTextValue("")
            'cmb_JenisLaporan.SetTextValue("")
            cmb_JenisLaporan.Text = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID(3)
            'cmb_ReportType.StringFilter = "1=2"
            'txt_FIU_Ref_Number.Hidden = True
            'txt_FIU_Ref_Number.AllowBlank = True
            'cmb_TransmodeCode.StringFilter = "1=2"
            'cmb_ReportType.SetTextValue("")
            txt_ReportUniqueValue.Value = Nothing
            txt_RentityID.Text = Nothing
            'txt_RentityBranch.Value = Nothing
            txt_Entity_Reference.Value = Nothing
            'txt_FIU_Ref_Number.Value = Nothing
            txt_SubmissionDate.Value = Nothing
            txt_CurrencyLocal.Value = Nothing
            'txt_Catatan.Value = Nothing
            'txt_Action.Value = Nothing

            'Hide Report ID and Status
            txt_FK_Report_ID.Hidden = True
            txt_Entity_Reference.Hidden = True
            txt_ReportStatus.Hidden = True

            'Set Default Value of RentityID and RentityBranch
            txt_RentityID.Text = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID(1)
            'txt_RentityBranch.Text = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID(18)
            txt_SubmissionDate.Value = CDate(DateTime.Now.ToString("dd-MMM-yyyy"))
            'df_MonthPeriod.Value = "12-2022"
            txt_CurrencyLocal.Value = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID(4)

            Dim strDefaultSubmissionCode As String = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID(2)
            drTemp = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getDataRowByID("vw_SIPESATGoAML_Submission_Type", "Kode", strDefaultSubmissionCode)
            If drTemp IsNot Nothing Then
                cmb_SubmisionCode.SetTextWithTextValue(drTemp("kode"), drTemp("keterangan"))
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "Update 11-Mar-2022 Adi : Set Valid/Invalid klo gak ada Transaksi/Activity"
    Protected Sub UpdateReportIsValid()
        Try
            'Checked Dtype
            'Simplenya cek apakah ada Transaksi/Activity atau tidak
            Dim isReportValid As Boolean = True
            If objGoAML_Report_Class.list_goAML_Act_ReportPartyType Is Nothing OrElse objGoAML_Report_Class.list_goAML_Act_ReportPartyType.Count = 0 Then
                isReportValid = False
            End If

            'Cek juga count validation Report
            Dim strQuery As String = "SELECT COUNT(1) FROM SIPESATGOAML_REPORT_ERROR_MESSAGE WHERE FK_SIPESATGOAML_REPORT_HEADER_ID = " & objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
            Dim intCountValidation As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            If Not IsNothing(intCountValidation) AndAlso intCountValidation > 0 Then
                isReportValid = False
            End If

            objGoAML_Report_Class.obj_goAML_Report.ISVALID = isReportValid
            'Update flag isValid dan Transaction Date jika NULL disamakan dengan Submission Date di database
            If isReportValid Then
                strQuery = "UPDATE SIPESATGOAML_REPORT SET ISVALID = 1 WHERE PK_Report_ID = " & objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Else
                strQuery = "UPDATE SIPESATGOAML_REPORT SET ISVALID = 0 WHERE PK_Report_ID = " & objGoAML_Report_Class.obj_goAML_Report.PK_REPORT_ID
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

    ' Update 27 Dec 2022 Ari : Tambah Dropdown untuk jenis user
    Protected Sub cmb_Activity_Information_Changed()
        Try
            Me.PartyType = cmb_Activity_Information.SelectedItemValue
            LoadActivityShortInfo()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class