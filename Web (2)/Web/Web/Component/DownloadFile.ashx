﻿<%@ WebHandler Language="VB" Class="DownloadFile" %>

Imports System
Imports System.Web
Imports NawaBLL
Public Class DownloadFile : Implements IHttpHandler, System.Web.SessionState.IReadOnlySessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        If context.Session("ParameterUpload.Upload_Content") IsNot Nothing And Not String.IsNullOrEmpty(context.Session("ParameterUpload.Upload_FileName")) Then

            context.Response.Clear()
            context.Response.ClearHeaders()
            context.Response.AddHeader("content-disposition", "attachment;filename=" & context.Session("ParameterUpload.Upload_FileName").Replace(" ", "") & ".xlsx")
            context.Response.Charset = ""
            context.Response.AddHeader("cache-control", "max-age=0")

            context.Response.ContentType = "application/vnd.ms-excel"
            context.Response.BinaryWrite(context.Session("ParameterUpload.Upload_Content"))
            context.Response.End()
        End If

        'Dim request = System.Web.HttpContext.Current.Request
        'Dim ObjModule As NawaDAL.Module

        'Dim strmodule As String = request.Params("ModuleID")


        'Dim key As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk("5")

        'Dim intmodule As Integer = NawaBLL.Common.DecryptQueryString(strmodule, NawaBLL.Common.EncryptRijndael(key.SettingValue, key.EncriptionKey))
        'ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intmodule)

        'Dim objFormUpload = New NawaBLL.FormModuleUpload
        'objFormUpload.ModuleName = ObjModule.ModuleName
        'objFormUpload.SettingImport()

        'Dim temp = request.QueryString("fileName")
        'Dim strAdvancedFilter = request.QueryString("addFilter")

        'Dim us As NawaDAL.MUser = NawaBLL.Common.SessionCurrentUser
        'Dim strFileName As String

    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class