﻿<%@ WebHandler Language="VB" Class="NDSDownloadFile" %>

Imports System
Imports System.Web

Public Class NDSDownloadFile : Implements IHttpHandler, System.Web.SessionState.IReadOnlySessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        If context.Session("NDS.Upload_Content") IsNot Nothing And Not String.IsNullOrEmpty(context.Session("NDS.Upload_FileName")) Then

            context.Response.Clear()
            context.Response.ClearHeaders()
            context.Response.AddHeader("content-disposition", "attachment;filename=" & context.Session("NDS.Upload_FileName").Replace(" ", ""))
            context.Response.Charset = ""
            context.Response.AddHeader("cache-control", "max-age=0")

            context.Response.ContentType = System.Web.MimeMapping.GetMimeMapping(context.Session("NDS.Upload_FileName"))
            context.Response.BinaryWrite(context.Session("NDS.Upload_Content"))
            context.Response.End()
        End If

    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class