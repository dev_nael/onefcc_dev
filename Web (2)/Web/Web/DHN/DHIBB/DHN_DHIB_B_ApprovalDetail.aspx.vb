
Imports System.Data
Imports System.Data.SqlClient

Partial Class DHN_DHIB_B_ApprovalDetail
    'Inherits System.Web.UI.Page
    Inherits ParentPage

    Public Property StringDateFormat() As String
        Get
            Return Session("DHN_DHIB_B_ApprovalDetail.StringDateFormat")
        End Get
        Set(ByVal value As String)
            Session("DHN_DHIB_B_ApprovalDetail.StringDateFormat") = value
        End Set
    End Property

    Public Property DataID() As String
        Get
            Return Session("DHN_DHIB_B_ApprovalDetail.DataID")
        End Get
        Set(ByVal value As String)
            Session("DHN_DHIB_B_ApprovalDetail.DataID") = value
        End Set
    End Property

    Public Property DataTableRejectedTransaction() As DataTable
        Get
            Return Session("DHN_DHIB_B_ApprovalDetail.DataTableRejectedTransaction")
        End Get
        Set(ByVal value As DataTable)
            Session("DHN_DHIB_B_ApprovalDetail.DataTableRejectedTransaction") = value
        End Set
    End Property

    Protected Sub BtnAccept_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim dataModuleApproval As DataRow = getDataRowBySingleString("select top 1 * from ModuleApproval where ModuleName = '" & ObjModule.ModuleName & "' and ModuleKey = '" & DataID() & "'")
            If dataModuleApproval IsNot Nothing Then
                Dim strQuery As String = ""
                '--ModuleField simpan data pk delete counter
                If Not IsDBNull(dataModuleApproval("ModuleField")) AndAlso Not String.IsNullOrEmpty(dataModuleApproval("ModuleField")) Then
                    Dim strArrayCounter() As String = dataModuleApproval("ModuleField").ToString.Split(", ")
                    For Each item In strArrayCounter
                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES (GETDATE(), 'DHN DHIB-B Transaction Counter AuditTrail Deleted', 'DHN DHIB-B Transaction Counter AuditTrail Deleted PK " & item & " started')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Dim param(4) As SqlParameter
                        param(0) = New SqlParameter
                        param(0).ParameterName = "@PK_Transaction"
                        param(0).Value = Convert.ToInt64(item)
                        param(0).DbType = SqlDbType.BigInt

                        param(1) = New SqlParameter
                        param(1).ParameterName = "@Couter_or_Kliring"
                        param(1).Value = "C"
                        param(1).DbType = SqlDbType.VarChar

                        param(2) = New SqlParameter
                        param(2).ParameterName = "@UserID_creator"
                        If Not IsDBNull(dataModuleApproval("CreatedBy")) Then
                            param(2).Value = dataModuleApproval("CreatedBy")
                        Else
                            param(2).Value = ""
                        End If
                        param(2).DbType = SqlDbType.VarChar

                        param(3) = New SqlParameter
                        param(3).ParameterName = "@UserID_approver"
                        param(3).Value = NawaBLL.Common.SessionCurrentUser.UserID
                        param(3).DbType = SqlDbType.VarChar

                        param(4) = New SqlParameter
                        param(4).ParameterName = "@ModuleLabel"
                        param(4).Value = ObjModule.ModuleLabel
                        param(4).DbType = SqlDbType.VarChar

                        NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_DHN_AuditTrailDeleteTransaction", param)

                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES (GETDATE(), 'DHN DHIB-B Transaction Counter AuditTrail Deleted', 'DHN DHIB-B Transaction Counter AuditTrail Deleted PK " & item & " finished')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES (GETDATE(), 'DHN DHIB-B Transaction Counter Deleted', 'DHN DHIB-B Transaction Counter Deleted PK " & item & " started')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Dim paramDeleteTransaction(0) As SqlParameter
                        paramDeleteTransaction(0) = New SqlParameter
                        paramDeleteTransaction(0).ParameterName = "@PK_DHN_TRANSACTIONCOUNTER_ID"
                        paramDeleteTransaction(0).Value = Convert.ToInt64(item)
                        paramDeleteTransaction(0).DbType = SqlDbType.BigInt

                        NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_DHN_BeforeDelete_TransactionCounter", paramDeleteTransaction)

                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES (GETDATE(), 'DHN DHIB-B Transaction Counter Deleted', 'DHN DHIB-B Transaction Counter Deleted PK " & item & " finished')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        strQuery = "DELETE FROM DHN_TransactionCounter WHERE PK_DHN_TRANSACTIONCOUNTER_ID = " & item
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                    Next
                End If

                '--ModuleFieldBefore simpan data PK delete kliring
                If Not IsDBNull(dataModuleApproval("ModuleFieldBefore")) AndAlso Not String.IsNullOrEmpty(dataModuleApproval("ModuleFieldBefore")) Then
                    Dim strArrayCounter() As String = dataModuleApproval("ModuleFieldBefore").ToString.Split(", ")
                    For Each item In strArrayCounter
                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES (GETDATE(), 'DHN DHIB-B Transaction Kliring AuditTrail Deleted', 'DHN DHIB-B Transaction Kliring AuditTrail Deleted PK " & item & " started')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Dim param(4) As SqlParameter
                        param(0) = New SqlParameter
                        param(0).ParameterName = "@PK_Transaction"
                        param(0).Value = Convert.ToInt64(item)
                        param(0).DbType = SqlDbType.BigInt

                        param(1) = New SqlParameter
                        param(1).ParameterName = "@Couter_or_Kliring"
                        param(1).Value = "K"
                        param(1).DbType = SqlDbType.VarChar

                        param(2) = New SqlParameter
                        param(2).ParameterName = "@UserID_creator"
                        If Not IsDBNull(dataModuleApproval("CreatedBy")) Then
                            param(2).Value = dataModuleApproval("CreatedBy")
                        Else
                            param(2).Value = ""
                        End If
                        param(2).DbType = SqlDbType.VarChar

                        param(3) = New SqlParameter
                        param(3).ParameterName = "@UserID_approver"
                        param(3).Value = NawaBLL.Common.SessionCurrentUser.UserID
                        param(3).DbType = SqlDbType.VarChar

                        param(4) = New SqlParameter
                        param(4).ParameterName = "@ModuleLabel"
                        param(4).Value = ObjModule.ModuleLabel
                        param(4).DbType = SqlDbType.VarChar

                        NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_DHN_AuditTrailDeleteTransaction", param)

                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES (GETDATE(), 'DHN DHIB-B Transaction Kliring AuditTrail Deleted', 'DHN DHIB-B Transaction Kliring AuditTrail Deleted PK " & item & " finished')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES (GETDATE(), 'DHN DHIB-B Transaction Kliring Deleted', 'DHN DHIB-B Transaction Kliring Deleted PK " & item & " started')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Dim paramDeleteTransaction(0) As SqlParameter
                        paramDeleteTransaction(0) = New SqlParameter
                        paramDeleteTransaction(0).ParameterName = "@PK_DHN_TRANSACTIONKLIRING_ID"
                        paramDeleteTransaction(0).Value = Convert.ToInt64(item)
                        paramDeleteTransaction(0).DbType = SqlDbType.BigInt

                        NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_DHN_BeforeDelete_TransactionKliring", paramDeleteTransaction)

                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES (GETDATE(), 'DHN DHIB-B Transaction Kliring Deleted', 'DHN DHIB-B Transaction Kliring Deleted PK " & item & " finished')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        strQuery = "DELETE FROM DHN_TransactionKliring WHERE PK_DHN_TRANSACTIONKLIRING_ID = " & item
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                    Next
                End If

                strQuery = "DELETE FROM ModuleApproval WHERE ModuleName = '" & ObjModule.ModuleName & "' and ModuleKey = '" & DataID() & "'"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                LblConfirmation.Text = "Data Saved into DataBase"
                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
            Else
                Throw New ApplicationException("Approval Data Cannot be Found, Please Report This Issue to Admin")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnReject_Click(sender As Object, e As DirectEventArgs)
        Try

            '' Add 25-Nov-2022, Felix Kosongkan REASON_DELETED
            Dim StringPKCounter As String
            StringPKCounter = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT ModuleField FROM ModuleApproval WHERE ModuleName = '" & ObjModule.ModuleName & "' and ModuleKey = '" & DataID() & "'", Nothing)

            Dim StringPKKliring As String
            StringPKKliring = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT ModuleFieldBefore FROM ModuleApproval WHERE ModuleName = '" & ObjModule.ModuleName & "' and ModuleKey = '" & DataID() & "'", Nothing)


            If Not String.IsNullOrEmpty(StringPKKliring) Then
                Dim strQueryUpdate As String = "Update DHN_TransactionKliring set REASON_DELETED = '' where PK_DHN_TRANSACTIONKLIRING_ID in (" & StringPKKliring & ")"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryUpdate, Nothing)
            End If

            If Not String.IsNullOrEmpty(StringPKCounter) Then
                Dim strQueryUpdate As String = "Update DHN_TransactionCounter set REASON_DELETED = '' where PK_DHN_TRANSACTIONCOUNTER_ID in (" & StringPKCounter & ")"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryUpdate, Nothing)
            End If
            '' End 25-Nov-2022

            Dim strQuery As String = "DELETE FROM ModuleApproval WHERE ModuleName = '" & ObjModule.ModuleName & "' and ModuleKey = '" & DataID() & "'"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            LblConfirmation.Text = "Request Rejected"
            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnBack_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval  & "?ModuleID=" & Moduleid)
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            'Moduleid = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & Moduleid)
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_DetailTransaction_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            window_DetailTransaction.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()
                GetDateFormat()
                SetCommandColumnLocation()
                'SetDateFormat()
                LoadData()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Approval
    End Sub

    'Private Function CheckChange() As Boolean
    '    Try
    '        Dim checker As Boolean = False
    '        Return checker
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Protected Sub GetDateFormat()
        Try
            Dim tempDRSystermParameter As DataRow = getDataRowByID("SystemParameter", "SettingName", "FormatDate")
            If tempDRSystermParameter IsNot Nothing Then
                If Not IsDBNull(tempDRSystermParameter("SettingValue")) Then
                    StringDateFormat = tempDRSystermParameter("SettingValue")
                Else
                    StringDateFormat = "dd-MMM-yyyy"
                End If
            Else
                StringDateFormat = "dd-MMM-yyyy"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub SetDateFormat()
    '    Try
    '        'df_DateUpdated.Format = StringDateFormat
    '        'ColumnDateNote.Format = StringDateFormat
    '        'DatePickerColumnDateNote.Format = StringDateFormat
    '        'DatePickerColumnDateNote.FormatText = "Expected Date Format " & StringDateFormat
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Protected Sub LoadData()
        Try
            Dim dataStr As String = Request.Params("ID")
            DataID() = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            'Dim ID As Integer = Convert.ToInt64(dataID)
            Dim tempDRDataDHIBB As DataRow = getDataRowByID("DHN_DHIB_B", "PK_DHN_DHIB_B_ID", DataID())
            If tempDRDataDHIBB IsNot Nothing Then
                If Not IsDBNull(tempDRDataDHIBB("CIF_REF_NO")) Then
                    txt_CIF_No.Value = tempDRDataDHIBB("CIF_REF_NO")
                End If
                If Not IsDBNull(tempDRDataDHIBB("CUSTOMER_NAME")) Then
                    txt_Customer_Name.Value = tempDRDataDHIBB("CUSTOMER_NAME")
                End If
                If Not IsDBNull(tempDRDataDHIBB("STATUS_TITLE_BUSINESS_TYPE")) Then
                    txt_Title_or_Business_Type.Value = tempDRDataDHIBB("STATUS_TITLE_BUSINESS_TYPE")
                End If
                If Not IsDBNull(tempDRDataDHIBB("IDENTITY_NUMBER")) Then
                    txt_Identity_Number.Value = tempDRDataDHIBB("IDENTITY_NUMBER")
                End If
                If Not IsDBNull(tempDRDataDHIBB("TAX_NUMBER")) Then
                    txt_Tax_Number.Value = tempDRDataDHIBB("TAX_NUMBER")
                End If
                If Not IsDBNull(tempDRDataDHIBB("BIRTH_DATE")) Then
                    Dim tempDate As Date = tempDRDataDHIBB("BIRTH_DATE")
                    txt_Birth_Date.Value = tempDate.ToString(StringDateFormat)
                End If
                If Not IsDBNull(tempDRDataDHIBB("ADDRESS_STREET")) Then
                    txt_Address.Value = tempDRDataDHIBB("ADDRESS_STREET")
                End If
                If Not IsDBNull(tempDRDataDHIBB("RT_NEIGHBORHOOD_UNIT")) Then
                    txt_Neighborhood_Unit.Value = tempDRDataDHIBB("RT_NEIGHBORHOOD_UNIT")
                End If
                If Not IsDBNull(tempDRDataDHIBB("RW_COMMUNITY_UNIT")) Then
                    txt_Community_Unit.Value = tempDRDataDHIBB("RW_COMMUNITY_UNIT")
                End If
                If Not IsDBNull(tempDRDataDHIBB("CITY")) Then
                    txt_City.Value = tempDRDataDHIBB("CITY")
                End If
                If Not IsDBNull(tempDRDataDHIBB("PROVINCE")) Then
                    txt_Province.Value = tempDRDataDHIBB("PROVINCE")
                End If
                If Not IsDBNull(tempDRDataDHIBB("ZIP_CODE")) Then
                    txt_Zip_Code.Value = tempDRDataDHIBB("ZIP_CODE")
                End If
                If Not IsDBNull(tempDRDataDHIBB("CUSTOMER_TYPE")) Then
                    Dim tempDRCustomerType As DataRow = getDataRowByID("DHN_Ref_CustomerType", "CODE", tempDRDataDHIBB("CUSTOMER_TYPE").ToString())
                    If tempDRCustomerType IsNot Nothing Then
                        Dim tempStrCustomerType As String = ""
                        If Not IsDBNull(tempDRCustomerType("CODE")) Then
                            tempStrCustomerType = tempDRCustomerType("CODE")
                        End If
                        If Not IsDBNull(tempDRCustomerType("DESCRIPTION")) Then
                            tempStrCustomerType = tempStrCustomerType & " - " & tempDRCustomerType("DESCRIPTION")
                        End If
                        txt_Customer_Type.Value = tempStrCustomerType
                    Else
                        txt_Customer_Type.Value = tempDRDataDHIBB("CUSTOMER_TYPE")
                    End If
                End If
                If Not IsDBNull(tempDRDataDHIBB("ACCOUNT_NO")) Then
                    txt_Account_No.Value = tempDRDataDHIBB("ACCOUNT_NO")
                End If
                If Not IsDBNull(tempDRDataDHIBB("ACCOUNT_TYPE")) Then
                    Dim tempDRAccountType As DataRow = getDataRowByID("DHN_Ref_AccountType", "CODE", tempDRDataDHIBB("ACCOUNT_TYPE").ToString())
                    If tempDRAccountType IsNot Nothing Then
                        Dim tempStrCustomerType As String = ""
                        If Not IsDBNull(tempDRAccountType("CODE")) Then
                            tempStrCustomerType = tempDRAccountType("CODE")
                        End If
                        If Not IsDBNull(tempDRAccountType("DESCRIPTION")) Then
                            tempStrCustomerType = tempStrCustomerType & " - " & tempDRAccountType("DESCRIPTION")
                        End If
                        txt_Account_Type.Value = tempStrCustomerType
                    Else
                        txt_Account_Type.Value = tempDRDataDHIBB("ACCOUNT_TYPE")
                    End If
                End If
                If Not IsDBNull(tempDRDataDHIBB("LATEST_REJECTION_DATE")) Then
                    Dim tempDate As Date = tempDRDataDHIBB("LATEST_REJECTION_DATE")
                    txt_Last_Rejected_Date.Value = tempDate.ToString(StringDateFormat)
                End If
            Else
                Throw New ApplicationException("DHN DHIB-B Data Cannot be Found, Please Report This Issue to Admin")
            End If

            Dim dataModuleApproval As DataRow = getDataRowBySingleString("select top 1 * from ModuleApproval where ModuleName = '" & ObjModule.ModuleName & "' and ModuleKey = '" & DataID() & "'")
            '--modulefield simpan data pk delete counter
            '--modulefieldbefore simpan data PK delete kliring
            If dataModuleApproval IsNot Nothing Then
                Dim strSQL As String = " select "
                strSQL &= " UnionKliringAndCounter.PK_ID "
                strSQL &= " ,UnionKliringAndCounter.CHEQUE_NUMBER "
                strSQL &= " ,CASE "
                strSQL &= "	WHEN reason.PK_DHN_REF_REASON_ID is null THEN UnionKliringAndCounter.REASON_CHEQUE_FAILED_CODE "
                strSQL &= " Else reason.CODE + ' - ' + reason.DESCRIPTION "
                strSQL &= " End As REASON "
                strSQL &= " ,UnionKliringAndCounter.ACCOUNT_NO "
                strSQL &= " ,UnionKliringAndCounter.CIF_NO "
                strSQL &= " ,CASE "
                strSQL &= " WHEN CAST(UnionKliringAndCounter.CHEQUE_OR_BG_CODE as int) BETWEEN 0 AND 9 THEN 'Cheque' "
                strSQL &= " WHEN CAST(UnionKliringAndCounter.CHEQUE_OR_BG_CODE as int) BETWEEN 10 AND 19 THEN 'Bilyet Giro' "
                strSQL &= " End As CHEQUE_OR_BG "
                strSQL &= " ,UnionKliringAndCounter.TRANSACTION_AMOUNT "
                strSQL &= " ,UnionKliringAndCounter.CURRENCY "
                strSQL &= " ,UnionKliringAndCounter.LOCAL_EQUIVALENT_AMOUNT "
                strSQL &= " ,UnionKliringAndCounter.TRANSACTION_DATE "
                strSQL &= " ,UnionKliringAndCounter.BRANCH_TRN "
                strSQL &= " ,UnionKliringAndCounter.DATASOURCE "
                strSQL &= " ,UnionKliringAndCounter.STATUS_TRANSACTION "
                strSQL &= " ,UnionKliringAndCounter.REASON_DELETED " '' Add 25-Nov-2022, Felix Display Reason Deleted
                strSQL &= " from "
                strSQL &= " ( "
                strSQL &= " Select "
                strSQL &= " 'K_'+CAST(PK_DHN_TRANSACTIONKLIRING_ID as varchar(max)) as PK_ID "
                strSQL &= " ,CHEQUE_NUMBER "
                strSQL &= " ,REASON_CHEQUE_FAILED_CODE "
                strSQL &= " ,ACCOUNT_NO "
                strSQL &= " ,CIF_NO "
                strSQL &= " ,CHEQUE_OR_BG_CODE "
                strSQL &= " ,TRANSACTION_AMOUNT "
                strSQL &= " ,CURRENCY "
                strSQL &= " ,LOCAL_EQUIVALENT_AMOUNT "
                strSQL &= " ,TRANSACTION_DATE "
                strSQL &= " ,BRANCH_TRN "
                strSQL &= " ,'Kliring' as DATASOURCE "
                strSQL &= " ,CASE "
                If Not IsDBNull(dataModuleApproval("ModuleFieldBefore")) AndAlso Not String.IsNullOrEmpty(dataModuleApproval("ModuleFieldBefore")) Then
                    strSQL &= " WHEN PK_DHN_TRANSACTIONKLIRING_ID in (" & dataModuleApproval("ModuleFieldBefore") & ") THEN 'Propose Delete' "
                Else
                    strSQL &= " WHEN PK_DHN_TRANSACTIONKLIRING_ID in (0) THEN 'Propose Delete' "
                End If
                strSQL &= " END AS STATUS_TRANSACTION "
                strSQL &= " ,REASON_DELETED " '' Add 25-Nov-2022, Felix Display Reason Deleted
                strSQL &= " From DHN_TransactionKliring "
                strSQL &= " where FK_DHN_DHIB_B_ID = '" & DataID() & "' "
                'If Not IsDBNull(dataModuleApproval("ModuleFieldBefore")) AndAlso Not String.IsNullOrEmpty(dataModuleApproval("ModuleFieldBefore")) Then
                '    strSQL &= " and PK_DHN_TRANSACTIONKLIRING_ID not in (" & dataModuleApproval("ModuleFieldBefore") & ") "
                'End If
                strSQL &= " union all "
                strSQL &= " Select "
                strSQL &= " 'C_'+CAST(PK_DHN_TRANSACTIONCOUNTER_ID as varchar(max)) "
                strSQL &= " ,CHEQUE_NUMBER "
                strSQL &= " ,REASON_CHEQUE_FAILED_CODE "
                strSQL &= " ,ACCOUNT_NO "
                strSQL &= " ,CIF_NO "
                strSQL &= " ,CHEQUE_OR_BG_CODE "
                strSQL &= " ,TRANSACTION_AMOUNT "
                strSQL &= " ,CURRENCY "
                strSQL &= " ,LOCAL_EQUIVALENT_AMOUNT "
                strSQL &= " ,TRANSACTION_DATE "
                strSQL &= " ,BRANCH_TRN "
                strSQL &= " ,'Counter' as DATASOURCE "
                strSQL &= " ,CASE "
                If Not IsDBNull(dataModuleApproval("ModuleField")) AndAlso Not String.IsNullOrEmpty(dataModuleApproval("ModuleField")) Then
                    strSQL &= " WHEN PK_DHN_TRANSACTIONCOUNTER_ID in (" & dataModuleApproval("ModuleField") & ") THEN 'Propose Delete' "
                Else
                    strSQL &= " WHEN PK_DHN_TRANSACTIONCOUNTER_ID in (0) THEN 'Propose Delete' "
                End If
                strSQL &= " END AS STATUS_TRANSACTION "
                strSQL &= " ,REASON_DELETED " '' Add 25-Nov-2022, Felix Display Reason Deleted
                strSQL &= " From DHN_TransactionCounter "
                strSQL &= " where FK_DHN_DHIB_B_ID = '" & DataID() & "' "
                'If Not IsDBNull(dataModuleApproval("ModuleField")) AndAlso Not String.IsNullOrEmpty(dataModuleApproval("ModuleField")) Then
                '    strSQL &= " and PK_DHN_TRANSACTIONCOUNTER_ID not in (" & dataModuleApproval("ModuleField") & ") "
                'End If
                strSQL &= " ) UnionKliringAndCounter "
                strSQL &= " Left join DHN_Ref_Reason reason "
                strSQL &= " On UnionKliringAndCounter.REASON_CHEQUE_FAILED_CODE = reason.CODE "
                DataTableRejectedTransaction() = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            End If
            store_Rejected_Transactions.DataSource = DataTableRejectedTransaction()
            store_Rejected_Transactions.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function getDataRowByID(strTable As String, strTableKeyField As String, strKey As String) As DataRow
        Try
            Dim strSQL As String = "SELECT TOP 1 * FROM " & strTable & " WHERE " & strTableKeyField & "='" & strKey & "'"
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

            Return drResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Protected Function getDataRowBySingleString(strSQL As String) As DataRow
        Try
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

            Return drResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Protected Sub Gc_Rejected_Transactions(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            Dim arrSplitStrings() As String = ID.Split("_")
            Dim transactionType As String = arrSplitStrings(0)
            Dim transactionID As String = arrSplitStrings(1)

            If strCommandName = "Detail" Then
                LoadDetailTransaction(ID)
                window_DetailTransaction.Hidden = False
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDetailTransaction(ID As String)
        Try
            Dim selectedDataRow As DataRow = DataTableRejectedTransaction.Select("PK_ID = '" & ID & "'").FirstOrDefault
            If selectedDataRow IsNot Nothing Then
                If Not IsDBNull(selectedDataRow("ACCOUNT_NO")) Then
                    displayTransaction_AccountNo.Value = selectedDataRow("ACCOUNT_NO")
                Else
                    displayTransaction_AccountNo.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("CIF_NO")) Then
                    displayTransaction_CIF.Value = selectedDataRow("CIF_NO")
                Else
                    displayTransaction_CIF.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("CHEQUE_NUMBER")) Then
                    displayTransaction_ChequeNumber.Value = selectedDataRow("CHEQUE_NUMBER")
                Else
                    displayTransaction_ChequeNumber.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("CHEQUE_OR_BG")) Then
                    displayTransaction_ChequeType.Value = selectedDataRow("CHEQUE_OR_BG")
                Else
                    displayTransaction_ChequeType.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("TRANSACTION_AMOUNT")) Then
                    displayTransaction_TransactionAmount.Value = selectedDataRow("TRANSACTION_AMOUNT")
                Else
                    displayTransaction_TransactionAmount.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("CURRENCY")) Then
                    displayTransaction_Currency.Value = selectedDataRow("CURRENCY")
                Else
                    displayTransaction_Currency.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("LOCAL_EQUIVALENT_AMOUNT")) Then
                    displayTransaction_LocalEquivalentAmount.Value = selectedDataRow("LOCAL_EQUIVALENT_AMOUNT")
                Else
                    displayTransaction_LocalEquivalentAmount.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("TRANSACTION_DATE")) Then
                    Dim tempDate As Date = selectedDataRow("TRANSACTION_DATE")
                    displayTransaction_TransactionDate.Value = tempDate.ToString(StringDateFormat)
                Else
                    displayTransaction_TransactionDate.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("REASON")) Then
                    displayTransaction_ReasonRejected.Value = selectedDataRow("REASON")
                Else
                    displayTransaction_ReasonRejected.Value = ""
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        If buttonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub

    Protected Sub SetCommandColumnLocation()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim buttonPosition As Integer = 1
        If objParamSettingbutton IsNot Nothing Then
            buttonPosition = objParamSettingbutton.SettingValue
        End If

        'Report Level
        ColumnActionLocation(gp_Rejected_Transactions, cc_Rejected_Transactions, buttonPosition)
    End Sub

    Private Sub ClearSession()
        Try
            StringDateFormat = Nothing
            DataID = ""
            DataTableRejectedTransaction = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
