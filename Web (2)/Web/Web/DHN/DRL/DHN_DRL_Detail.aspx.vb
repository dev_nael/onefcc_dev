
Imports System.Data
Imports System.Data.SqlClient

Partial Class DHN_DRL_Detail
    'Inherits System.Web.UI.Page
    Inherits ParentPage

    Public Property StringDateFormat() As String
        Get
            Return Session("DHN_DRL_Detail.StringDateFormat")
        End Get
        Set(ByVal value As String)
            Session("DHN_DRL_Detail.StringDateFormat") = value
        End Set
    End Property

    Public Property DataID() As String
        Get
            Return Session("DHN_DRL_Detail.DataID")
        End Get
        Set(ByVal value As String)
            Session("DHN_DRL_Detail.DataID") = value
        End Set
    End Property

    Public Property DataTableRejectedTransaction() As DataTable
        Get
            Return Session("DHN_DRL_Detail.DataTableRejectedTransaction")
        End Get
        Set(ByVal value As DataTable)
            Session("DHN_DRL_Detail.DataTableRejectedTransaction") = value
        End Set
    End Property

    Protected Sub BtnCancelReport_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            'Moduleid = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub Btn_DetailTransaction_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            window_DetailTransaction.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()
                GetDateFormat()
                SetCommandColumnLocation()
                'SetDateFormat()
                LoadData()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    'Private Function CheckChange() As Boolean
    '    Try
    '        Dim checker As Boolean = False
    '        Return checker
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Protected Sub GetDateFormat()
        Try
            Dim tempDRSystermParameter As DataRow = getDataRowByID("SystemParameter", "SettingName", "FormatDate")
            If tempDRSystermParameter IsNot Nothing Then
                If Not IsDBNull(tempDRSystermParameter("SettingValue")) Then
                    StringDateFormat = tempDRSystermParameter("SettingValue")
                Else
                    StringDateFormat = "dd-MMM-yyyy"
                End If
            Else
                StringDateFormat = "dd-MMM-yyyy"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub SetDateFormat()
    '    Try
    '        'df_DateUpdated.Format = StringDateFormat
    '        'ColumnDateNote.Format = StringDateFormat
    '        'DatePickerColumnDateNote.Format = StringDateFormat
    '        'DatePickerColumnDateNote.FormatText = "Expected Date Format " & StringDateFormat
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Protected Sub LoadData()
        Try
            Dim dataStr As String = Request.Params("ID")
            DataID() = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            'Dim ID As Integer = Convert.ToInt64(dataID)
            Dim tempDRDataDRL As DataRow = getDataRowByID("DHN_DRL", "PK_DHN_DRL_ID", DataID())
            If tempDRDataDRL IsNot Nothing Then
                If Not IsDBNull(tempDRDataDRL("ACCOUNT_NO")) Then
                    txt_Account_No.Value = tempDRDataDRL("ACCOUNT_NO")
                End If
                If Not IsDBNull(tempDRDataDRL("TRANSACTION_DATE")) Then
                    Dim tempDate As Date = tempDRDataDRL("TRANSACTION_DATE")
                    txt_Transaction_Date.Value = tempDate.ToString(StringDateFormat)
                End If
                If Not IsDBNull(tempDRDataDRL("CHEQUE_NUMBER")) Then
                    txt_Cheque_Number.Value = tempDRDataDRL("CHEQUE_NUMBER")
                End If
                If Not IsDBNull(tempDRDataDRL("REASON")) Then
                    Dim tempDRReason As DataRow = getDataRowByID("DHN_Ref_Reason", "CODE", tempDRDataDRL("REASON").ToString())
                    If tempDRReason IsNot Nothing Then
                        Dim tempStrReason As String = ""
                        If Not IsDBNull(tempDRReason("CODE")) Then
                            tempStrReason = tempDRReason("CODE")
                        End If
                        If Not IsDBNull(tempDRReason("DESCRIPTION")) Then
                            tempStrReason = tempStrReason & " - " & tempDRReason("DESCRIPTION")
                        End If
                        txt_Reason_Cheque_Failed.Value = tempStrReason
                    Else
                        txt_Reason_Cheque_Failed.Value = tempDRDataDRL("REASON")
                    End If
                End If
                If Not IsDBNull(tempDRDataDRL("AMOUNT")) Then
                    txt_Transaction_Amount.Value = tempDRDataDRL("AMOUNT")
                End If
                If Not IsDBNull(tempDRDataDRL("TRANSACTION_CODE")) Then
                    txt_Transaction_Code.Value = tempDRDataDRL("TRANSACTION_CODE")
                End If
                If Not IsDBNull(tempDRDataDRL("CUSTOMER_TYPE")) Then
                    Dim tempDRCustomerType As DataRow = getDataRowByID("DHN_Ref_CustomerType", "CODE", tempDRDataDRL("CUSTOMER_TYPE").ToString())
                    If tempDRCustomerType IsNot Nothing Then
                        Dim tempStrCustomerType As String = ""
                        If Not IsDBNull(tempDRCustomerType("CODE")) Then
                            tempStrCustomerType = tempDRCustomerType("CODE")
                        End If
                        If Not IsDBNull(tempDRCustomerType("DESCRIPTION")) Then
                            tempStrCustomerType = tempStrCustomerType & " - " & tempDRCustomerType("DESCRIPTION")
                        End If
                        txt_Customer_Type.Value = tempStrCustomerType
                    Else
                        txt_Customer_Type.Value = tempDRDataDRL("CUSTOMER_TYPE")
                    End If
                End If
                If Not IsDBNull(tempDRDataDRL("CUSTOMER_NAME")) Then
                    txt_Customer_Name.Value = tempDRDataDRL("CUSTOMER_NAME")
                End If
                If Not IsDBNull(tempDRDataDRL("TAX_NUMBER")) Then
                    txt_NPWP.Value = tempDRDataDRL("TAX_NUMBER")
                End If
                If Not IsDBNull(tempDRDataDRL("BIRTH_DATE")) Then
                    Dim tempDate As Date = tempDRDataDRL("BIRTH_DATE")
                    txt_Birth_Date.Value = tempDate.ToString(StringDateFormat)
                End If
                If Not IsDBNull(tempDRDataDRL("ADDRESS")) Then
                    txt_Address.Value = tempDRDataDRL("ADDRESS")
                End If
                If Not IsDBNull(tempDRDataDRL("RT_NEIGHBORHOOD_UNIT")) Then
                    txt_RT.Value = tempDRDataDRL("RT_NEIGHBORHOOD_UNIT")
                End If
                If Not IsDBNull(tempDRDataDRL("RW_COMMUNITY_UNIT")) Then
                    txt_RW.Value = tempDRDataDRL("RW_COMMUNITY_UNIT")
                End If
                If Not IsDBNull(tempDRDataDRL("CITY")) Then
                    txt_City.Value = tempDRDataDRL("CITY")
                End If
                If Not IsDBNull(tempDRDataDRL("PROVINCE")) Then
                    txt_Province.Value = tempDRDataDRL("PROVINCE")
                End If
                If Not IsDBNull(tempDRDataDRL("ZIP_CODE")) Then
                    txt_ZIP_Code.Value = tempDRDataDRL("ZIP_CODE")
                End If
            Else
                Throw New ApplicationException("DHN DRL Data Cannot be Found, Please Report This Issue to Admin")
            End If
            Dim strSQL As String = " select "
            strSQL &= " 'C_'+CAST(Counter.PK_DHN_TRANSACTIONCOUNTER_ID as varchar(max)) as PK_ID "
            strSQL &= " ,Counter.CHEQUE_NUMBER "
            'strSQL &= " ,Counter.REASON_CHEQUE_FAILED_CODE "
            strSQL &= " ,CASE "
            strSQL &= "	WHEN reason.PK_DHN_REF_REASON_ID is null THEN Counter.REASON_CHEQUE_FAILED_CODE "
            strSQL &= " Else reason.CODE + ' - ' + reason.DESCRIPTION "
            strSQL &= " End As REASON "
            strSQL &= " ,Counter.ACCOUNT_NO "
            strSQL &= " ,Counter.CIF_NO "
            'strSQL &= " ,Counter.CHEQUE_OR_BG_CODE "
            strSQL &= " ,CASE "
            strSQL &= " WHEN CAST(Counter.CHEQUE_OR_BG_CODE as int) BETWEEN 0 AND 9 THEN 'Cheque' "
            strSQL &= " WHEN CAST(Counter.CHEQUE_OR_BG_CODE as int) BETWEEN 10 AND 19 THEN 'Bilyet Giro' "
            strSQL &= " End As CHEQUE_OR_BG "
            strSQL &= " ,Counter.TRANSACTION_AMOUNT "
            strSQL &= " ,Counter.CURRENCY "
            strSQL &= " ,Counter.LOCAL_EQUIVALENT_AMOUNT "
            strSQL &= " ,Counter.TRANSACTION_DATE "
            strSQL &= " ,Counter.BRANCH_TRN "
            strSQL &= " ,'Counter' as DATASOURCE "
            strSQL &= " From DHN_TransactionCounter Counter "
            strSQL &= " Left join DHN_Ref_Reason reason "
            strSQL &= " On Counter.REASON_CHEQUE_FAILED_CODE = reason.CODE "
            strSQL &= " where FK_DHN_DRL_ID = '" & DataID() & "' "
            DataTableRejectedTransaction() = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            store_Rejected_Transactions.DataSource = DataTableRejectedTransaction()
            store_Rejected_Transactions.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function getDataRowByID(strTable As String, strTableKeyField As String, strKey As String) As DataRow
        Try
            Dim strSQL As String = "SELECT TOP 1 * FROM " & strTable & " WHERE " & strTableKeyField & "='" & strKey & "'"
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

            Return drResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Protected Sub Gc_Rejected_Transactions(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            Dim arrSplitStrings() As String = ID.Split("_")
            Dim transactionType As String = arrSplitStrings(0)
            Dim transactionID As String = arrSplitStrings(1)

            If strCommandName = "Detail" Then
                LoadDetailTransaction(ID)
                window_DetailTransaction.Hidden = False
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDetailTransaction(ID As String)
        Try
            Dim selectedDataRow As DataRow = DataTableRejectedTransaction.Select("PK_ID = '" & ID & "'").FirstOrDefault
            If selectedDataRow IsNot Nothing Then
                If Not IsDBNull(selectedDataRow("ACCOUNT_NO")) Then
                    displayTransaction_AccountNo.Value = selectedDataRow("ACCOUNT_NO")
                Else
                    displayTransaction_AccountNo.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("CIF_NO")) Then
                    displayTransaction_CIF.Value = selectedDataRow("CIF_NO")
                Else
                    displayTransaction_CIF.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("CHEQUE_NUMBER")) Then
                    displayTransaction_ChequeNumber.Value = selectedDataRow("CHEQUE_NUMBER")
                Else
                    displayTransaction_ChequeNumber.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("CHEQUE_OR_BG")) Then
                    displayTransaction_ChequeType.Value = selectedDataRow("CHEQUE_OR_BG")
                Else
                    displayTransaction_ChequeType.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("TRANSACTION_AMOUNT")) Then
                    displayTransaction_TransactionAmount.Value = selectedDataRow("TRANSACTION_AMOUNT")
                Else
                    displayTransaction_TransactionAmount.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("CURRENCY")) Then
                    displayTransaction_Currency.Value = selectedDataRow("CURRENCY")
                Else
                    displayTransaction_Currency.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("LOCAL_EQUIVALENT_AMOUNT")) Then
                    displayTransaction_LocalEquivalentAmount.Value = selectedDataRow("LOCAL_EQUIVALENT_AMOUNT")
                Else
                    displayTransaction_LocalEquivalentAmount.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("TRANSACTION_DATE")) Then
                    Dim tempDate As Date = selectedDataRow("TRANSACTION_DATE")
                    displayTransaction_TransactionDate.Value = tempDate.ToString(StringDateFormat)
                Else
                    displayTransaction_TransactionDate.Value = ""
                End If
                If Not IsDBNull(selectedDataRow("REASON")) Then
                    displayTransaction_ReasonRejected.Value = selectedDataRow("REASON")
                Else
                    displayTransaction_ReasonRejected.Value = ""
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        If buttonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub

    Protected Sub SetCommandColumnLocation()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim buttonPosition As Integer = 1
        If objParamSettingbutton IsNot Nothing Then
            buttonPosition = objParamSettingbutton.SettingValue
        End If

        'Report Level
        ColumnActionLocation(gp_Rejected_Transactions, cc_Rejected_Transactions, buttonPosition)
    End Sub

    Private Sub ClearSession()
        Try
            StringDateFormat = Nothing
            DataID = Nothing
            DataTableRejectedTransaction = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
