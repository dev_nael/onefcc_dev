<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="DHN_DRL_Detail.aspx.vb" Inherits="DHN_DRL_Detail" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .x-form-item-label.x-form-item-label-default { width : 230px !important; }
        
        .text-wrapper .x-form-display-field {
    	    word-break: break-word;
    	    word-wrap: break-word;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel ID="FormPanelInput" runat="server" Layout="AnchorLayout" ButtonAlign="Center" Title="Daftar Hitam Negara DRL Detail" BodyStyle="padding:10px" AutoScroll="true" Hidden="false">
        <Items>
            <ext:Panel runat="server" ID="ReportGeneralInformationPanel" Layout="AnchorLayout" ClientIDMode="Static" BodyStyle="padding:10px" Margin="5" Border="True">
                <Items>
                    <ext:DisplayField ID="txt_Account_No" runat="server" FieldLabel="Account No" />
                    <ext:DisplayField ID="txt_Transaction_Date" runat="server" FieldLabel="Transaction Date" />
                    <ext:DisplayField ID="txt_Cheque_Number" runat="server" FieldLabel="Cheque Number" />
                    <ext:DisplayField ID="txt_Reason_Cheque_Failed" runat="server" FieldLabel="Reason Cheque Failed" />
                    <ext:DisplayField ID="txt_Transaction_Amount" runat="server" FieldLabel="Transaction Amount" />
                    <ext:DisplayField ID="txt_Transaction_Code" runat="server" FieldLabel="Transaction Code" />
                    <ext:DisplayField ID="txt_Customer_Type" runat="server" FieldLabel="Customer Type" />
                    <ext:DisplayField ID="txt_Customer_Name" runat="server" FieldLabel="Customer Name" />
                    <ext:DisplayField ID="txt_NPWP" runat="server" FieldLabel="NPWP" />
                    <ext:DisplayField ID="txt_Birth_Date" runat="server" FieldLabel="Birth Date" />
                    <ext:DisplayField ID="txt_Address" runat="server" FieldLabel="Address" />
                    <ext:DisplayField ID="txt_RT" runat="server" FieldLabel="RT" />
                    <ext:DisplayField ID="txt_RW" runat="server" FieldLabel="RW" />
                    <ext:DisplayField ID="txt_City" runat="server" FieldLabel="City" />
                    <ext:DisplayField ID="txt_Province" runat="server" FieldLabel="Province" />
                    <ext:DisplayField ID="txt_ZIP_Code" runat="server" FieldLabel="ZIP Code" />
                    <ext:GridPanel ID="gp_Rejected_Transactions" runat="server" Title="Rejected Transactions" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="store_Rejected_Transactions" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model43" IDProperty="PK_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CHEQUE_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="REASON" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ACCOUNT_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIF_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CHEQUE_OR_BG_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TRANSACTION_AMOUNT" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="CURRENCY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LOCAL_EQUIVALENT_AMOUNT" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="TRANSACTION_DATE" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="BRANCH_TRN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DATASOURCE" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column5" runat="server" DataIndex="CHEQUE_NUMBER" Text="Cheque Number" Width="100" ></ext:Column>
                                <ext:Column ID="Column412" runat="server" DataIndex="REASON" Text="Reason Failed" Width="150" ></ext:Column>
                                <ext:Column ID="Column72" runat="server" DataIndex="ACCOUNT_NO" Text="Account No" Width="150" ></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="CIF_NO" Text="CIF" Width="100"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="CURRENCY" Text="Currency" Width="150"></ext:Column>
                                <ext:NumberColumn ID="Column1" runat="server" DataIndex="TRANSACTION_AMOUNT" Text="Transaction Amount" Width="110" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                <ext:NumberColumn ID="Column3" runat="server" DataIndex="LOCAL_EQUIVALENT_AMOUNT" Text="Local Equivalent Amount" Width="150" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="NumberColumn11" runat="server" DataIndex="DATASOURCE" Text="Data Source" Width="150"></ext:Column>
                                <ext:CommandColumn ID="cc_Rejected_Transactions" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="Gc_Rejected_Transactions">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btnCancelReport" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnCancelReport_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <ext:Window ID="window_DetailTransaction" Title="Detail Transaction" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
            <ext:FormPanel runat="server" ID="pnl_DetailTransaction" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <ext:DisplayField ID="displayTransaction_AccountNo" runat="server" FieldLabel="Account No" />
                    <ext:DisplayField ID="displayTransaction_CIF" runat="server" FieldLabel="CIF" />
                    <ext:DisplayField ID="displayTransaction_ChequeNumber" runat="server" FieldLabel="Cheque Number" />
                    <ext:DisplayField ID="displayTransaction_ChequeType" runat="server" FieldLabel="Cheque Type" />
                    <ext:DisplayField ID="displayTransaction_TransactionAmount" runat="server" FieldLabel="Transaction Amount" />
                    <ext:DisplayField ID="displayTransaction_Currency" runat="server" FieldLabel="Currency" />
                    <ext:DisplayField ID="displayTransaction_LocalEquivalentAmount" runat="server" FieldLabel="Local Equivalent Amount" />
                    <ext:DisplayField ID="displayTransaction_TransactionDate" runat="server" FieldLabel="Transaction Date" />
                    <ext:DisplayField ID="displayTransaction_ReasonRejected" runat="server" FieldLabel="Reason Rejected" />
                </Content>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.85, height: size.height * 0.8});" />
            <Resize Handler="#{window_PersonParty}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="Button2" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="Btn_DetailTransaction_Back_Click">
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
</asp:Content>

