
Imports System.Data
Imports System.Data.SqlClient

Partial Class DHN_DHIB_T_Detail
    'Inherits System.Web.UI.Page
    Inherits ParentPage

    Public Property StringDateFormat() As String
        Get
            Return Session("DHN_DHIB_T_Detail.StringDateFormat")
        End Get
        Set(ByVal value As String)
            Session("DHN_DHIB_T_Detail.StringDateFormat") = value
        End Set
    End Property

    Public Property DataID() As String
        Get
            Return Session("DHN_DHIB_T_Detail.DataID")
        End Get
        Set(ByVal value As String)
            Session("DHN_DHIB_T_Detail.DataID") = value
        End Set
    End Property

    Protected Sub BtnCancelReport_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            'Moduleid = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()
                GetDateFormat()
                'SetDateFormat()
                LoadData()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    'Private Function CheckChange() As Boolean
    '    Try
    '        Dim checker As Boolean = False
    '        Return checker
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Protected Sub GetDateFormat()
        Try
            Dim tempDRSystermParameter As DataRow = getDataRowByID("SystemParameter", "SettingName", "FormatDate")
            If tempDRSystermParameter IsNot Nothing Then
                If Not IsDBNull(tempDRSystermParameter("SettingValue")) Then
                    StringDateFormat = tempDRSystermParameter("SettingValue")
                Else
                    StringDateFormat = "dd-MMM-yyyy"
                End If
            Else
                StringDateFormat = "dd-MMM-yyyy"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub SetDateFormat()
    '    Try
    '        'df_DateUpdated.Format = StringDateFormat
    '        'ColumnDateNote.Format = StringDateFormat
    '        'DatePickerColumnDateNote.Format = StringDateFormat
    '        'DatePickerColumnDateNote.FormatText = "Expected Date Format " & StringDateFormat
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Protected Sub LoadData()
        Try
            Dim dataStr As String = Request.Params("ID")
            DataID() = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            'Dim ID As Integer = Convert.ToInt64(dataID)
            Dim tempDRDataDHIBT As DataRow = getDataRowByID("DHN_DHIB_T", "PK_DHN_DHIB_T_ID", DataID())
            If tempDRDataDHIBT IsNot Nothing Then
                If Not IsDBNull(tempDRDataDHIBT("PERIOD")) Then
                    txt_Period.Value = tempDRDataDHIBT("PERIOD")
                End If
                If Not IsDBNull(tempDRDataDHIBT("CIF_REF_NO")) Then
                    txt_CIF.Value = tempDRDataDHIBT("CIF_REF_NO")

                    ''Edit 13-Sep-2022 , Felix, Field di ODM_Customer berubah
                    'Dim strQuery As String = "SELECT TOP 1 FULL_NAME from ODM_Customer WHERE CIF_NO = '" & tempDRDataDHIBT("CIF_REF_NO") & "'"
                    'Dim tempDRDataCustomer As DataRow = getDataRowBySingleString(strQuery)
                    'If tempDRDataCustomer IsNot Nothing Then
                    '    If Not IsDBNull(tempDRDataCustomer("FULL_NAME")) Then
                    '        txt_Account_Customer_Name.Value = tempDRDataCustomer("FULL_NAME")
                    '    End If
                    'End If
                    Dim strQuery As String = "SELECT TOP 1 Nama_Sesuai_Identitas from ODM_Customer WHERE ID_Pihak_Lawan_CIF = '" & tempDRDataDHIBT("CIF_REF_NO") & "'"
                    Dim tempDRDataCustomer As DataRow = getDataRowBySingleString(strQuery)
                    If tempDRDataCustomer IsNot Nothing Then
                        If Not IsDBNull(tempDRDataCustomer("Nama_Sesuai_Identitas")) Then
                            txt_Account_Customer_Name.Value = tempDRDataCustomer("Nama_Sesuai_Identitas")
                        End If
                    End If
                    '' End Edit 13-Sep-2022
                End If
                If Not IsDBNull(tempDRDataDHIBT("CODE_REPORTER_BANK")) Then
                    txt_Reporter_Bank_Code.Value = tempDRDataDHIBT("CODE_REPORTER_BANK")
                End If
                If Not IsDBNull(tempDRDataDHIBT("REF_NO_REPORTER_BANK")) Then
                    txt_Reporter_Bank_Ref_No.Value = tempDRDataDHIBT("REF_NO_REPORTER_BANK")
                End If
                If Not IsDBNull(tempDRDataDHIBT("ACCOUNT_NO")) Then
                    txt_Account_No.Value = tempDRDataDHIBT("ACCOUNT_NO")
                End If
                If Not IsDBNull(tempDRDataDHIBT("ACCOUNT_TYPE")) Then
                    Dim tempDRAccountType As DataRow = getDataRowByID("DHN_Ref_AccountType", "CODE", tempDRDataDHIBT("ACCOUNT_TYPE").ToString())
                    If tempDRAccountType IsNot Nothing Then
                        Dim tempStrCustomerType As String = ""
                        If Not IsDBNull(tempDRAccountType("CODE")) Then
                            tempStrCustomerType = tempDRAccountType("CODE")
                        End If
                        If Not IsDBNull(tempDRAccountType("DESCRIPTION")) Then
                            tempStrCustomerType = tempStrCustomerType & " - " & tempDRAccountType("DESCRIPTION")
                        End If
                        txt_Account_Type.Value = tempStrCustomerType
                    Else
                        txt_Account_Type.Value = tempDRDataDHIBT("ACCOUNT_TYPE")
                    End If
                End If
                If Not IsDBNull(tempDRDataDHIBT("CLOSING_DATE")) Then
                    Dim tempDate As Date = tempDRDataDHIBT("CLOSING_DATE")
                    txt_Closing_Date.Value = tempDate.ToString(StringDateFormat)
                End If
                If Not IsDBNull(tempDRDataDHIBT("CIF_REF_NO")) AndAlso Not IsDBNull(tempDRDataDHIBT("ACCOUNT_NO")) Then
                    Dim strQuery As String = "SELECT TOP 1 * from DHN_AccountMustBeClosed WHERE CIF_NO = '" & tempDRDataDHIBT("CIF_REF_NO") & "' AND ACCOUNT_NO = '" & tempDRDataDHIBT("ACCOUNT_NO") & "'"
                    Dim tempDRDataAccountMustBeClosed As DataRow = getDataRowBySingleString(strQuery)
                    If tempDRDataAccountMustBeClosed IsNot Nothing Then
                        If Not IsDBNull(tempDRDataAccountMustBeClosed("DHN_REFERENCE")) Then
                            txt_DHN_Reference.Value = tempDRDataAccountMustBeClosed("DHN_REFERENCE")
                        End If
                    End If

                    strQuery = "SELECT TOP 1 CIF_NO, ACCOUNT_NO, BRANCH_CODE + ' - ' + isnull(cabang.label,'') as BRANCH_CODE, CURRENCY, CreatedDate, PRODUCT_CODE from ODM_Current_Account_Giro left join VW_Ref_Cabang_Distinct cabang on BRANCH_CODE = cabang.kode_cabang WHERE CIF_NO = '" & tempDRDataDHIBT("CIF_REF_NO") & "' AND ACCOUNT_NO = '" & tempDRDataDHIBT("ACCOUNT_NO") & "'" '' Edit 14-Sep-2022 Felix, tambah Branch Name setelah Branch Code
                    Dim tempDRDataAccount As DataRow = getDataRowBySingleString(strQuery)
                    If tempDRDataAccount IsNot Nothing Then
                        If Not IsDBNull(tempDRDataAccount("CIF_NO")) Then
                            txt_Account_CIF.Value = tempDRDataAccount("CIF_NO")
                        End If
                        If Not IsDBNull(tempDRDataAccount("ACCOUNT_NO")) Then
                            txt_Account_Account_No.Value = tempDRDataAccount("ACCOUNT_NO")
                        End If
                        If Not IsDBNull(tempDRDataAccount("BRANCH_CODE")) Then
                            txt_Account_Branch_Code.Value = tempDRDataAccount("BRANCH_CODE")
                        End If
                        If Not IsDBNull(tempDRDataAccount("CURRENCY")) Then
                            txt_Account_Currency.Value = tempDRDataAccount("CURRENCY")
                        End If
                        If Not IsDBNull(tempDRDataAccount("CreatedDate")) Then
                            txt_Account_Created_Date.Value = tempDRDataAccount("CreatedDate")
                        End If
                        If Not IsDBNull(tempDRDataAccount("PRODUCT_CODE")) Then
                            txt_Account_Product_Code.Value = tempDRDataAccount("PRODUCT_CODE")
                        End If
                    End If
                End If
            Else
                Throw New ApplicationException("DHN DHIB-T Data Cannot be Found, Please Report This Issue to Admin")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function getDataRowByID(strTable As String, strTableKeyField As String, strKey As String) As DataRow
        Try
            Dim strSQL As String = "SELECT TOP 1 * FROM " & strTable & " WHERE " & strTableKeyField & "='" & strKey & "'"
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

            Return drResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Protected Function getDataRowBySingleString(strSQL As String) As DataRow
        Try
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

            Return drResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Private Sub ClearSession()
        Try
            StringDateFormat = Nothing
            DataID = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
