<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="DHN_DHIB_T_Detail.aspx.vb" Inherits="DHN_DHIB_T_Detail" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .x-form-item-label.x-form-item-label-default { width : 230px !important; }
        
        .text-wrapper .x-form-display-field {
    	    word-break: break-word;
    	    word-wrap: break-word;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel ID="FormPanelInput" runat="server" Layout="AnchorLayout" ButtonAlign="Center" Title="Daftar Hitam Negara DHIB-T Detail" BodyStyle="padding:10px" AutoScroll="true" Hidden="false">
        <Items>
            <ext:Panel runat="server" Layout="HBoxLayout" BodyPadding="10" DefaultAnchor="100%" Frame="false">
                <Items>
                    <ext:Panel runat="server" Flex="1">
                        <Items>
                            <ext:DisplayField ID="txt_CIF" runat="server" FieldLabel="CIF" />
                            <ext:DisplayField ID="txt_Account_No" runat="server" FieldLabel="Account No" />
                            <ext:DisplayField ID="txt_Account_Type" runat="server" FieldLabel="Account Type" />
                            <ext:DisplayField ID="txt_Closing_Date" runat="server" FieldLabel="Closing Date" />
                        </Items>
                    </ext:Panel>
                    <ext:Panel runat="server" Flex="1">
                        <Items>
                            <ext:DisplayField ID="txt_Period" runat="server" FieldLabel="Period" />
                            <ext:DisplayField ID="txt_Reporter_Bank_Code" runat="server" FieldLabel="Reporter Bank Code" />
                            <ext:DisplayField ID="txt_Reporter_Bank_Ref_No" runat="server" FieldLabel="Reporter Bank Ref No" />
                            <ext:DisplayField ID="txt_DHN_Reference" runat="server" FieldLabel="DHN Reference" />
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Panel>
            <ext:Panel runat="server" Layout="AnchorLayout" BodyStyle="padding:10px" Margin="5" Border="True" Collapsible="true" Title="Account Information">
                <Items>
                    <ext:DisplayField ID="txt_Account_CIF" runat="server" FieldLabel="CIF" />
                    <ext:DisplayField ID="txt_Account_Account_No" runat="server" FieldLabel="Account No" />
                    <ext:DisplayField ID="txt_Account_Customer_Name" runat="server" FieldLabel="Customer Name" />
                    <ext:DisplayField ID="txt_Account_Branch_Code" runat="server" FieldLabel="Branch Code" />
                    <ext:DisplayField ID="txt_Account_Currency" runat="server" FieldLabel="Currency" />
                    <ext:DisplayField ID="txt_Account_Created_Date" runat="server" FieldLabel="Created Date" />
                    <ext:DisplayField ID="txt_Account_Product_Code" runat="server" FieldLabel="Product Code" />
                </Items>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btnCancelReport" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnCancelReport_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>

