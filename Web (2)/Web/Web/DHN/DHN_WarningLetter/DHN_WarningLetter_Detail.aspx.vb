﻿Imports System.Net
Imports Microsoft.Reporting.WebForms
Imports System.Security.Principal
Imports ReportingService
Imports System.Reflection
Imports Microsoft.ReportingServices.DataProcessing

Partial Class DHN_DHN_WarningLetter_DHN_WarningLetter_Detail
    Inherits ParentPage
    Public Property IDUnik() As Long
        Get
            Return Session("DHN_WarningLetter_Detail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("DHN_WarningLetter_Detail.IDUnik") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                If Session("DHNWaringLetterIFChecker") = 1 Then
                    btnSave.Hidden = True
                Else
                    btnSave.Hidden = False
                End If
                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If
                IDUnik = Session("PK_WarningLetter_ID")
                Dim StrQueryNamaSSRS As String = "select WL_TYPE from DHN_WarningLetter where PK_DHN_WarningLetter_ID ='" & IDUnik & "'"

                Dim SSRSType As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryNamaSSRS, Nothing)
                Session("SSRS_Type_DHNWarning_Letter") = SSRSType
                FormPanel1.Title = SSRSType & " View"




                Dim objreport As New ReportingService.ReportingService2010
                Dim objreportservercredential As New MyReportServerCredentials
                objreport.Credentials = objreportservercredential.NetworkCredentials


                Dim objitem As CatalogItem()
                Dim strpath As String = ""
                objitem = objreport.ListChildren("/", True)
                For Each item As CatalogItem In objitem
                    If item.TypeName = "Report" And item.Name = SSRSType Then
                        strpath = item.Path
                        'cboReport.Items.Add(New Ext.Net.ListItem(item.Name, item.Path))
                    End If

                Next



                LoadDataReport()

                ReportViewer1.SizeToReportContent = True

                ReportViewer1.ProcessingMode = ProcessingMode.Remote
                ReportViewer1.ServerReport.ReportServerCredentials = New MyReportServerCredentials
                ReportViewer1.ServerReport.ReportServerUrl = New Uri(NawaBLL.SystemParameterBLL.GetSystemParameterByPk(13).SettingValue)
                ReportViewer1.ServerReport.ReportPath = hreport.Value

                'Dim strfilterDataAccess As String = NawaBLL.Nawa.BLL.NawaFramework.GetFilterDataAccess(NawaBLL.Common.SessionCurrentUser.FK_MRole_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view)
                'If strfilterDataAccess <> "" Then
                '    strfilterDataAccess = Replace(strfilterDataAccess, "@userid", NawaBLL.Common.SessionCurrentUser.UserID)
                '    strfilterDataAccess = Replace(strfilterDataAccess, "@pkuserid", NawaBLL.Common.SessionCurrentUser.PK_MUser_ID)

                '    Dim arrQuerystring As String() = strfilterDataAccess.Split("&")
                '    For Each item As String In arrQuerystring

                '        Dim arrresult As String() = item.Split("=")
                '        If arrresult.Length > 0 Then
                '            Dim objparam As New ReportParameter(arrresult(0), arrresult(1))

                '            ReportViewer1.ServerReport.SetParameters(objparam)
                '        End If

                '    Next
                'End If

                'Passing Parameter Case ID
                'CustomizeRV(ReportViewer1)
                ReportViewer1.ShowExportControls = False
                'Dim StrDHNID As String = NawaBLL.Common.DecryptQueryString(IDUnik, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                'IDUnik = StrDHNID
                Dim objParam As New ReportParameter("PK_DHN_WARNINGLETTER_ID", IDUnik)
                ReportViewer1.ServerReport.SetParameters(objParam)

                'ReportViewer1.SizeToReportContent = True
                'ReportViewer1.Width = Unit.Percentage(100)
                'ReportViewer1.Height = Unit.Percentage(100)

                'ReportViewer1
                'saveFileDialog.Filter = “Word Doc (*.doc)|*.doc|PDF (*.pdf)| *.pdf”;

            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataReport()
        Dim objreport As New ReportingService.ReportingService2010
        Dim objreportservercredential As New MyReportServerCredentials
        objreport.Credentials = objreportservercredential.NetworkCredentials

        Dim objitem As CatalogItem()

        objitem = objreport.ListChildren("/", True)
        For Each item As CatalogItem In objitem
            If item.TypeName = "Report" And item.Name.ToLower = Session("SSRS_Type_DHNWarning_Letter").ToLower Then


                hreport.Value = item.Path
                'cboReport.Items.Add(New Ext.Net.ListItem(item.Name, item.Path))
            End If

        Next

    End Sub

    Private Sub Parameter_ReportingSerViceSQLView_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.view

    End Sub

    'Protected Sub btnviewReport_Click(sender As Object, e As EventArgs) Handles btnviewReport.Click
    '    Try
    '        ReportViewer1.SizeToReportContent = True

    '        ReportViewer1.ProcessingMode = ProcessingMode.Remote
    '        ReportViewer1.ServerReport.ReportServerCredentials = New MyReportServerCredentials
    '        ReportViewer1.ServerReport.ReportServerUrl = New Uri(NawaBLL.SystemParameterBLL.GetSystemParameterByPk(13).SettingValue)
    '        ReportViewer1.ServerReport.ReportPath = hreport.Value

    '        Dim strfilterDataAccess As String = NawaBLL.Nawa.BLL.NawaFramework.GetFilterDataAccess(NawaBLL.Common.SessionCurrentUser.FK_MRole_ID, ObjModule.PK_Module_ID)
    '        If strfilterDataAccess <> "" Then
    '            'strfilterDataAccess = Replace(strfilterDataAccess, "@userid", "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'")
    '            strfilterDataAccess = Replace(strfilterDataAccess, "@pkuserid", NawaBLL.Common.SessionCurrentUser.PK_MUser_ID)

    '            Dim arrQuerystring As String() = strfilterDataAccess.Split("&")
    '            For Each item As String In arrQuerystring

    '                Dim arrresult As String() = item.Split("=")
    '                If arrresult.Length > 0 Then
    '                    Dim objparam As New ReportParameter(arrresult(0), arrresult(1))

    '                    ReportViewer1.ServerReport.SetParameters(objparam)
    '                End If

    '            Next


    '        End If


    '        ReportViewer1.ServerReport.Refresh()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub ReportViewer1_PreRender(sender As Object, e As EventArgs)
    '    DisableUnwantedExportFormat(sender(ReportViewer1), "Excel")
    '    DisableUnwantedExportFormat(sender(ReportViewer1), "Word")
    'End Sub
    'Protected Sub DisableUnwantedExportFormat(ReportViewerID As ReportViewer, strFormatName As String)

    '    Dim Info As FieldInfo
    '    For Each item As RenderingExtension In ReportViewerID.LocalReport.ListRenderingExtensions()
    '        If item.Name.Trim().ToUpper() = strFormatName.Trim().ToUpper() Then
    '            Info = item.GetType().GetField("m_isVisible", BindingFlags.Instance Or BindingFlags.NonPublic)
    '            Info.SetValue(item, False)
    '        End If
    '    Next
    'End Sub
    Protected Sub ReportViewer1_Load(sender As Object, e As EventArgs)
        Dim exportOption As String = "PDF"
        Dim extension As RenderingExtension = ReportViewer1.LocalReport.ListRenderingExtensions().ToList().Find(Function(x) x.Name.Equals(exportOption, StringComparison.CurrentCultureIgnoreCase))
        If extension IsNot Nothing Then
            Dim fieldInfo As System.Reflection.FieldInfo = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.NonPublic)
            fieldInfo.SetValue(extension, False)
        End If
    End Sub
    Protected Sub ReportViewer1_PreRender(sender As Object, e As EventArgs)
        Dim exportOption As String = "PDF"
        Dim extension As RenderingExtension = ReportViewer1.LocalReport.ListRenderingExtensions().ToList().Find(Function(x) x.Name.Equals(exportOption, StringComparison.CurrentCultureIgnoreCase))
        If extension IsNot Nothing Then
            Dim fieldInfo As System.Reflection.FieldInfo = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.NonPublic)
            fieldInfo.SetValue(extension, False)
        End If
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            'Cari apakah user id nya pernah request id DHN yang ini di approval
            Dim User As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim JumlahDHN As Integer = 0
            Dim StrJumlahDHN As String = "select count(*) from DHN_WarningLetter_ApprovalPrint where user_id_requester = '" & User & "' and fk_dhn_warningletter_id = " & IDUnik
            JumlahDHN = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrJumlahDHN, Nothing)
            If JumlahDHN > 0 Then
                Throw New Exception("This User ID Exist In DHN WarningLetter Approval Print")
            End If
            ' Cari jika dia gak ada di approval,apakah ada di ready print tapi belum di download
            Dim JumlahDHNRP As Integer = 0
            Dim StrJumlahDHNRP As String = "select count(*) from DHN_WarningLetter_ReadyPrint where user_id_requester = '" & User & "' and fk_dhn_warningletter_id = " & IDUnik & " and is_printed = 0"
            JumlahDHNRP = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrJumlahDHNRP, Nothing)
            If JumlahDHNRP > 0 Then
                Throw New Exception("This User ID Exist In DHN WarningLetter Ready Print And Not Printed")
            End If
            If User <> "Sysadmin" Or NawaBLL.Common.SessionCurrentUser.FK_MRole_ID <> 1 Then
                Dim strQuery1 As String = "INSERT INTO DHN_WarningLetter_ApprovalPrint ("
                strQuery1 += "FK_DHN_WARNINGLETTER_ID, "
                strQuery1 += "USER_ID_REQUESTER, "
                strQuery1 += "CreatedBy, "
                strQuery1 += "CreatedDate) "
                strQuery1 += " SELECT "
                strQuery1 += IDUnik & " , "
                strQuery1 += "'" & User & "' , "
                strQuery1 += "'" & User & "' , "
                strQuery1 += " getdate() "
                strQuery1 += " from DHN_WarningLetter where PK_DHN_WARNINGLETTER_ID in(" & IDUnik & ") "
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery1, Nothing)

                lbl_Confirmation.Text = "Data Saved into Pending Approval"
                FormPanel1.Hidden = True
                pnl_Confirmation.Hidden = False
            Else
                Dim strQuery1 As String = "INSERT INTO DHN_WarningLetter_ReadyPrint ("
                strQuery1 += "FK_DHN_WARNINGLETTER_ID, "
                strQuery1 += "USER_ID_REQUESTER, "
                strQuery1 += "USER_ID_APPROVER, "
                strQuery1 += "IS_PRINTED, "
                strQuery1 += "CreatedBy, "
                strQuery1 += "Request_Date, "
                strQuery1 += "CreatedDate) "
                strQuery1 += " SELECT "
                strQuery1 += IDUnik & " , "
                strQuery1 += "'" & User & "' , "
                strQuery1 += "'" & User & "' , "
                strQuery1 += " 0 , "
                strQuery1 += "'" & User & "' , "
                strQuery1 += " getdate() , "
                strQuery1 += " getdate() "
                strQuery1 += " from DHN_WarningLetter where PK_DHN_WARNINGLETTER_ID in(" & IDUnik & ") "
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery1, Nothing)
                lbl_Confirmation.Text = "Data Saved into Ready Print"
                FormPanel1.Hidden = True
                pnl_Confirmation.Hidden = False
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(320030, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/DHN/DHN_WarningLetter/DHN_WarningLetter_View.aspx" & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Confirmation_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(320030, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/DHN/DHN_WarningLetter/DHN_WarningLetter_View.aspx" & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

End Class


<Serializable()>
Public NotInheritable Class MyReportServerCredentials
    Implements IReportServerCredentials

    Public ReadOnly Property ImpersonationUser() As WindowsIdentity _
            Implements IReportServerCredentials.ImpersonationUser
        Get
            'Use the default windows user.  Credentials will be
            'provided by the NetworkCredentials property.
            Return Nothing

        End Get
    End Property

    Public ReadOnly Property NetworkCredentials() As ICredentials _
            Implements IReportServerCredentials.NetworkCredentials
        Get
            'Read the user information from the web.config file.  
            'By reading the information on demand instead of storing 
            'it, the credentials will not be stored in session, 
            'reducing the vulnerable surface area to the web.config 
            'file, which can be secured with an ACL.

            'User name
            Dim userName As String = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(9).SettingValue


            If (String.IsNullOrEmpty(userName)) Then
                Throw New Exception("Missing user name from Application Parameter")
            End If

            'Password
            Dim password As String = NawaBLL.Common.DecryptRijndael(NawaBLL.SystemParameterBLL.GetSystemParameterByPk(10).SettingValue, NawaBLL.SystemParameterBLL.GetSystemParameterByPk(10).EncriptionKey)

            If (String.IsNullOrEmpty(password)) Then
                Throw New Exception("Missing password from Application Parameter")
            End If

            'Domain
            Dim domain As String = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(12).SettingValue

            'If (String.IsNullOrEmpty(domain)) Then
            '    Throw New Exception("Missing domain from web.config file")
            'End If

            Return New NetworkCredential(userName, password, domain)

        End Get
    End Property




    Public Function GetFormsCredentials(ByRef authCookie As Cookie, ByRef userName As String, ByRef password As String, ByRef authority As String) As Boolean Implements IReportServerCredentials.GetFormsCredentials

        authCookie = Nothing
        userName = Nothing
        password = Nothing
        authority = Nothing

        'Not using form credentials
        Return False

    End Function



End Class


