﻿Imports Ext.Net
Imports OfficeOpenXml
Imports NawaBLL
Imports System.Data
Imports NawaDAL
Imports System.Xml
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Services
Partial Class DHN_DHN_WarningLetter_DHN_WarningLetterApproval_View
    Inherits Parent
    Public objFormModuleApproval As NawaBLL.FormModuleView

    Public Property strWhereClause() As String
        Get
            Return Session("DHN_WarningLetterApproval_View.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("DHN_WarningLetterApproval_View.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("DHN_WarningLetterApproval_View.strSort")
        End Get
        Set(ByVal value As String)
            Session("DHN_WarningLetterApproval_View.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("DHN_WarningLetterApproval_View.indexStart")
        End Get
        Set(ByVal value As String)
            Session("DHN_WarningLetterApproval_View.indexStart") = value
        End Set
    End Property
    'Private Sub Parameterview_Init(sender As Object, e As EventArgs) Handles Me.Init
    '    objFormModuleApproval = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer
            Try
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Approval) Then
                    Dim strIDCode As String = 1
                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If



                objFormModuleApproval.ModuleID = objmodule.PK_Module_ID
                objFormModuleApproval.ModuleName = objmodule.ModuleName
                objFormModuleApproval.AddField("PK_DHN_WARNINGLETTER_APPROVALPRINT_ID", "ID Approval", 1, True, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("PK_DHN_WarningLetter_ID", "ID", 2, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("WL_Type", "Letter Type", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("Account_No", "Account No", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("CIF_NO", "CIF", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("Customer_Name", "Customer Name", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                'objFormModuleApproval.AddField("Tindak_lanjut", "Tindak Lanjut", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("CreatedDate", "Request Date", 7, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                objFormModuleApproval.AddField("USER_ID_REQUESTER", "User ID Request", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue)

                objFormModuleApproval.SettingFormView()

                'Panel Title
                GridpanelView.Title = objmodule.ModuleLabel & " -  Approval"

                'Remove default form View buttons
                Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
                GridpanelView.ColumnModel.Columns.Remove(objcommandcol)


                'Dim stringcommand As String = "prepareCommandCollection"
                'objcommandcol.PrepareToolbar.Fn = stringcommand

                'Add button Approval Detail
                Using objCustomCommandColumn As New CommandColumn
                    objCustomCommandColumn.ID = "columncrudapproval"
                    objCustomCommandColumn.ClientIDMode = Web.UI.ClientIDMode.Static
                    objCustomCommandColumn.Width = 100


                    Dim extparam As New Ext.Net.Parameter
                    extparam.Name = "unikkey"
                    extparam.Value = "record.data.PK_DHN_WARNINGLETTER_APPROVALPRINT_ID"
                    extparam.Mode = ParameterMode.Raw

                    Dim extparamcommand As New Ext.Net.Parameter
                    extparamcommand.Name = "command"
                    extparamcommand.Value = "command"
                    extparamcommand.Mode = ParameterMode.Raw

                    objCustomCommandColumn.DirectEvents.Command.ExtraParams.Add(extparam)
                    objCustomCommandColumn.DirectEvents.Command.ExtraParams.Add(extparamcommand)

                    Dim gcApprovalDetail As New GridCommand
                    gcApprovalDetail.CommandName = "ApprovalDetail"
                    gcApprovalDetail.Icon = Icon.ApplicationEdit
                    gcApprovalDetail.Text = "Detail"
                    gcApprovalDetail.ToolTip.Text = "Detail"
                    objCustomCommandColumn.Commands.Add(gcApprovalDetail)

                    AddHandler objCustomCommandColumn.DirectEvents.Command.Event, AddressOf GridCommandCustom

                    GridpanelView.ColumnModel.Columns.Insert(1, objCustomCommandColumn)
                End Using


            Catch ex As Exception

            End Try


            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"
            End If




        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            'Begin Penambahan Advanced Filter
            If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                Toolbar2.Hidden = True
            Else
                Toolbar2.Hidden = False
            End If
            LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")
            'END Penambahan Advanced Filter

            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleApproval.GetWhereClauseHeader(e)
            Dim strsort As String = " PK_DHN_WarningLetter_ID DESC "
            For Each item As DataSorter In e.Sort
                'strsort += ", " & item.Property & " " & item.Direction.ToString
                strsort = item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter

            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            End If

            Dim users As String = NawaBLL.Common.SessionCurrentUser.UserID

            Dim StrQueryRole As String = "select FK_MRole_ID from muser where USERID ='" & users & "'"

            StrQueryRole = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryRole, Nothing)

            If String.IsNullOrEmpty(strWhereClause) Then
                strWhereClause = "PK_DHN_WARNINGLETTER_APPROVALPRINT_ID is not null and FK_MRole_ID = '" & StrQueryRole & "' and USER_ID_REQUESTER <>  '" & users & "'"
            Else
                strWhereClause &= " AND (PK_DHN_WARNINGLETTER_APPROVALPRINT_ID is not null and FK_MRole_ID = '" & StrQueryRole & "' and USER_ID_REQUESTER <>  '" & users & "')"
            End If



            Me.strOrder = strsort
            Dim DataPaging As Data.DataTable = objFormModuleApproval.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("VW_DHN_WarningLetter_ApprovalPrint", "Doc_Number, Posting_Date, Product_Location_Type, Regional, Location, Doc_Date, Warehouse_By, SPV_Technician_By, Finance_By, ASMAN_Logistic_By, SOM_By, RSM_By, RH_By, Deviation", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleApproval.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleApproval.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("DHN_WarningLetter")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=DHN_WarningLetter.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleApproval.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleApproval.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=DHN_WarningLetter.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("Error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("Error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleApproval.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleApproval.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("DHN_WarningLetter")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=DHN_WarningLetter.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleApproval.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleApproval.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=DHN_WarningLetter.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("Error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("Error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("Error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    'Public Sub BtnAdd_Click()
    <DirectMethod>
    Public Sub BtnAdd_Click()

        Try

            Dim Moduleid As String = Request.Params("ModuleID")


            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleApproval.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Parameterview_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleApproval = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub


    'Begin Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleApproval.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridCommandCustom(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            Dim ModuleID As String = Request.Params("ModuleID")
            Dim intModuleID As Integer

            intModuleID = NawaBLL.Common.DecryptQueryString(ModuleID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleID)

            Dim ID As String = e.ExtraParams(0).Value
            ID = NawaBLL.Common.EncryptQueryString(ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            If e.ExtraParams(1).Value = "ApprovalDetail" Then
                Dim lngCaseID As Long = e.ExtraParams("unikkey")
                'Print Preview
                Dim strModule As String = ""

                Dim StrQueryFKWarningLetter As String = "select FK_DHN_WARNINGLETTER_ID from DHN_WarningLetter_ApprovalPrint where PK_DHN_WARNINGLETTER_APPROVALPRINT_ID ='" & lngCaseID & "'"

                Dim IDWarningLetter As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryFKWarningLetter, Nothing)
                Session("IDWarningLetter_Approval") = Nothing
                Session("IDWarningLetter_Approval") = lngCaseID

                Dim StrSQL As String = "Select TOP 1 * FROM VW_DHN_WarningLetter WHERE PK_DHN_WarningLetter_ID=" & IDWarningLetter
                Dim DHNWL As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrSQL)
                Dim TypeDHN As String = ""
                Session("PK_WarningLetterInApproval_ID") = Nothing
                Session("PK_WarningLetterInApproval_ID") = IDWarningLetter
                If DHNWL IsNot Nothing AndAlso Not IsDBNull(DHNWL("WL_TYPE")) Then
                    TypeDHN = DHNWL("WL_TYPE").ToString

                End If

                strModule = TypeDHN
                Dim cekModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName(strModule)
                If cekModule Is Nothing Then
                    Throw New Exception("Module " & strModule & " Not exists!")
                End If

                Dim strModuleEncrypted As String = NawaBLL.Common.EncryptQueryString(cekModule.PK_Module_ID.ToString, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim strWarningLetterIDEncrypted As String = NawaBLL.Common.EncryptQueryString(IDWarningLetter, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                'Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/DHN/DHN_WarningLetter/DHN_WarningLetterApproval_Detail.aspx?ModuleID=" & strModuleEncrypted & "&PK_DHN_WARNINGLETTER_ID=" & strWarningLetterIDEncrypted, ModuleID), "Loading")
                ''Dim Script As String = "window.open('" & NawaBLL.Common.GetApplicationPath & "/DHN/DHN_WarningLetter/DHN_WarningLetter_Detail.aspx?ModuleID=" & strModuleEncrypted & "&PK_DHN_WARNINGLETTER_ID=" & strCaseIDEncrypted & "');"
                ''GridpanelView.AddScript(Script)
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleApproval.objSchemaModule.UrlApprovalDetail & "?ModuleID={0}", ModuleID), "Loading")
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


End Class
