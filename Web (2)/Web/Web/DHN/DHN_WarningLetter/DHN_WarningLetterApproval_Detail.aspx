﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="DHN_WarningLetterApproval_Detail.aspx.vb" Inherits="DHN_DHN_WarningLetter_DHN_WarningLetterApproval_Detail" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >

    <ext:FormPanel ID="FormPanel1" runat="server" Padding="5" AutoScroll="true" Layout="FormLayout">

        <Items>
            <ext:Container ID="Container1" runat="server" Layout="FormLayout" BodyPadding="5" AnchorHorizontal="100%">

                <Items>
                    
                    <ext:Hidden runat="server" ID="hreport">
                        </ext:Hidden>
                    <%--<ext:ComboBox ID="cboReport" runat="server" FieldLabel="Report" AllowBlank="false" Width="300"></ext:ComboBox>--%>

                    <%--<ext:Button ID="btnviewReport" runat="server" Text="View Report" AutoPostBack="true">
                    </ext:Button>--%>
                </Items>
            </ext:Container>
        </Items>
        <Content>
            <asp:ScriptManager ID="ScriptManager1" runat="server" ViewStateMode="Enabled"></asp:ScriptManager>
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" EnableViewState="True" ShowParameterPrompts="false" OnLoad="ReportViewer1_Load" OnPreRender="ReportViewer1_PreRender"></rsweb:ReportViewer>
        </Content>
       <Buttons>
            <ext:Button runat="server" ID="btnSave" Text="Accept">
             
                <DirectEvents>
                    <Click OnEvent="btnSave_Click">
                        <EventMask Msg="Processing..." MinDelay="500" ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
             <ext:Button runat="server" ID="btnReject" Text="Reject">
             
                <DirectEvents>
                    <Click OnEvent="btnReject_Click">
                        <EventMask Msg="Processing..." MinDelay="500" ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancel" runat="server" Text="Cancel" Icon="Cancel">
            
                <DirectEvents>
                    <Click OnEvent="btnCancel_DirectEvent">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="500">
                        </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
        <%-- ================== CONFIRMATION PANEL ========================== --%>
    <ext:FormPanel ID="pnl_Confirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="lbl_Confirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Confirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <%-- ================== END OF CONFIRMATION PANEL ========================== --%>
</asp:Content>


