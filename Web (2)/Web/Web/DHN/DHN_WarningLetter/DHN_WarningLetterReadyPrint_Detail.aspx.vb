﻿Imports System.Net
Imports Microsoft.Reporting.WebForms
Imports System.Security.Principal
Imports ReportingService
Imports System.Reflection
Imports Microsoft.ReportingServices.DataProcessing
Partial Class DHN_DHN_WarningLetter_DHN_WarningLetterReadyPrint_Detail
    Inherits ParentPage
    Public Property IDUnik() As Long
        Get
            Return Session("DHN_WarningLetterApproval_Detail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("DHN_WarningLetterApproval_Detail.IDUnik") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If
                IDUnik = Session("PK_WarningLetterInReadyPrint_ID")
                Dim StrQueryNamaSSRS As String = "select WL_TYPE from DHN_WarningLetter where PK_DHN_WarningLetter_ID ='" & IDUnik & "'"

                Dim SSRSType As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryNamaSSRS, Nothing)
                Session("SSRS_Type_DHNWarningInReadyPrint_Letter") = SSRSType
                FormPanel1.Title = SSRSType & " View"





                Dim objreport As New ReportingService.ReportingService2010
                Dim objreportservercredential As New MyReportServerCredentialsPrint
                objreport.Credentials = objreportservercredential.NetworkCredentials


                Dim objitem As CatalogItem()
                Dim strpath As String = ""
                objitem = objreport.ListChildren("/", True)
                For Each item As CatalogItem In objitem
                    If item.TypeName = "Report" And item.Name = SSRSType Then
                        strpath = item.Path
                        'cboReport.Items.Add(New Ext.Net.ListItem(item.Name, item.Path))
                    End If

                Next



                LoadDataReport()

                ReportViewer1.SizeToReportContent = True

                ReportViewer1.ProcessingMode = ProcessingMode.Remote
                ReportViewer1.ServerReport.ReportServerCredentials = New MyReportServerCredentialsPrint
                ReportViewer1.ServerReport.ReportServerUrl = New Uri(NawaBLL.SystemParameterBLL.GetSystemParameterByPk(13).SettingValue)
                ReportViewer1.ServerReport.ReportPath = hreport.Value

                'Dim strfilterDataAccess As String = NawaBLL.Nawa.BLL.NawaFramework.GetFilterDataAccess(NawaBLL.Common.SessionCurrentUser.FK_MRole_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view)
                'If strfilterDataAccess <> "" Then
                '    strfilterDataAccess = Replace(strfilterDataAccess, "@userid", NawaBLL.Common.SessionCurrentUser.UserID)
                '    strfilterDataAccess = Replace(strfilterDataAccess, "@pkuserid", NawaBLL.Common.SessionCurrentUser.PK_MUser_ID)

                '    Dim arrQuerystring As String() = strfilterDataAccess.Split("&")
                '    For Each item As String In arrQuerystring

                '        Dim arrresult As String() = item.Split("=")
                '        If arrresult.Length > 0 Then
                '            Dim objparam As New ReportParameter(arrresult(0), arrresult(1))

                '            ReportViewer1.ServerReport.SetParameters(objparam)
                '        End If

                '    Next
                'End If

                'Passing Parameter Case ID
                'CustomizeRV(ReportViewer1)
                ReportViewer1.ShowExportControls = True
                Dim objParam As New ReportParameter("PK_DHN_WARNINGLETTER_ID", IDUnik)
                ReportViewer1.ServerReport.SetParameters(objParam)


                'ReportViewer1.SizeToReportContent = True
                'ReportViewer1.Width = Unit.Percentage(100)
                'ReportViewer1.Height = Unit.Percentage(100)

                'ReportViewer1
                'saveFileDialog.Filter = “Word Doc (*.doc)|*.doc|PDF (*.pdf)| *.pdf”;

            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataReport()
        Dim objreport As New ReportingService.ReportingService2010
        Dim objreportservercredential As New MyReportServerCredentialsPrint
        objreport.Credentials = objreportservercredential.NetworkCredentials

        Dim objitem As CatalogItem()

        objitem = objreport.ListChildren("/", True)
        For Each item As CatalogItem In objitem
            If item.TypeName = "Report" And item.Name.ToLower = Session("SSRS_Type_DHNWarningInReadyPrint_Letter").ToLower Then


                hreport.Value = item.Path
                'cboReport.Items.Add(New Ext.Net.ListItem(item.Name, item.Path))
            End If

        Next

    End Sub

    Private Sub Parameter_ReportingSerViceSQLView_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.view

    End Sub

    'Protected Sub btnviewReport_Click(sender As Object, e As EventArgs) Handles btnviewReport.Click
    '    Try
    '        ReportViewer1.SizeToReportContent = True

    '        ReportViewer1.ProcessingMode = ProcessingMode.Remote
    '        ReportViewer1.ServerReport.ReportServerCredentials = New MyReportServerCredentialsPrint
    '        ReportViewer1.ServerReport.ReportServerUrl = New Uri(NawaBLL.SystemParameterBLL.GetSystemParameterByPk(13).SettingValue)
    '        ReportViewer1.ServerReport.ReportPath = hreport.Value

    '        Dim strfilterDataAccess As String = NawaBLL.Nawa.BLL.NawaFramework.GetFilterDataAccess(NawaBLL.Common.SessionCurrentUser.FK_MRole_ID, ObjModule.PK_Module_ID)
    '        If strfilterDataAccess <> "" Then
    '            'strfilterDataAccess = Replace(strfilterDataAccess, "@userid", "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'")
    '            strfilterDataAccess = Replace(strfilterDataAccess, "@pkuserid", NawaBLL.Common.SessionCurrentUser.PK_MUser_ID)

    '            Dim arrQuerystring As String() = strfilterDataAccess.Split("&")
    '            For Each item As String In arrQuerystring

    '                Dim arrresult As String() = item.Split("=")
    '                If arrresult.Length > 0 Then
    '                    Dim objparam As New ReportParameter(arrresult(0), arrresult(1))

    '                    ReportViewer1.ServerReport.SetParameters(objparam)
    '                End If

    '            Next


    '        End If


    '        ReportViewer1.ServerReport.Refresh()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub ReportViewer1_PreRender(sender As Object, e As EventArgs)
    '    DisableUnwantedExportFormat(sender(ReportViewer1), "Excel")
    '    DisableUnwantedExportFormat(sender(ReportViewer1), "Word")
    'End Sub
    'Protected Sub DisableUnwantedExportFormat(ReportViewerID As ReportViewer, strFormatName As String)

    '    Dim Info As FieldInfo
    '    For Each item As RenderingExtension In ReportViewerID.LocalReport.ListRenderingExtensions()
    '        If item.Name.Trim().ToUpper() = strFormatName.Trim().ToUpper() Then
    '            Info = item.GetType().GetField("m_isVisible", BindingFlags.Instance Or BindingFlags.NonPublic)
    '            Info.SetValue(item, False)
    '        End If
    '    Next
    'End Sub
    Protected Sub ReportViewer1_Load(sender As Object, e As EventArgs)
        Dim exportOption As String = "PDF"
        Dim extension As RenderingExtension = ReportViewer1.LocalReport.ListRenderingExtensions().ToList().Find(Function(x) x.Name.Equals(exportOption, StringComparison.CurrentCultureIgnoreCase))
        If extension IsNot Nothing Then
            Dim fieldInfo As System.Reflection.FieldInfo = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.NonPublic)
            fieldInfo.SetValue(extension, False)
        End If
    End Sub
    Protected Sub ReportViewer1_PreRender(sender As Object, e As EventArgs)
        Dim exportOption As String = "PDF"
        Dim extension As RenderingExtension = ReportViewer1.LocalReport.ListRenderingExtensions().ToList().Find(Function(x) x.Name.Equals(exportOption, StringComparison.CurrentCultureIgnoreCase))
        If extension IsNot Nothing Then
            Dim fieldInfo As System.Reflection.FieldInfo = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.NonPublic)
            fieldInfo.SetValue(extension, False)
        End If
    End Sub


    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(320032, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/DHN/DHN_WarningLetter/DHN_WarningLetterReadyPrint_View.aspx" & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub



End Class


<Serializable()>
Public NotInheritable Class MyReportServerCredentialsPrint
    Implements IReportServerCredentials

    Public ReadOnly Property ImpersonationUser() As WindowsIdentity _
            Implements IReportServerCredentials.ImpersonationUser
        Get
            'Use the default windows user.  Credentials will be
            'provided by the NetworkCredentials property.
            Return Nothing

        End Get
    End Property

    Public ReadOnly Property NetworkCredentials() As ICredentials _
            Implements IReportServerCredentials.NetworkCredentials
        Get
            'Read the user information from the web.config file.  
            'By reading the information on demand instead of storing 
            'it, the credentials will not be stored in session, 
            'reducing the vulnerable surface area to the web.config 
            'file, which can be secured with an ACL.

            'User name
            Dim userName As String = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(9).SettingValue


            If (String.IsNullOrEmpty(userName)) Then
                Throw New Exception("Missing user name from Application Parameter")
            End If

            'Password
            Dim password As String = NawaBLL.Common.DecryptRijndael(NawaBLL.SystemParameterBLL.GetSystemParameterByPk(10).SettingValue, NawaBLL.SystemParameterBLL.GetSystemParameterByPk(10).EncriptionKey)

            If (String.IsNullOrEmpty(password)) Then
                Throw New Exception("Missing password from Application Parameter")
            End If

            'Domain
            Dim domain As String = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(12).SettingValue

            'If (String.IsNullOrEmpty(domain)) Then
            '    Throw New Exception("Missing domain from web.config file")
            'End If

            Return New NetworkCredential(userName, password, domain)

        End Get
    End Property




    Public Function GetFormsCredentials(ByRef authCookie As Cookie, ByRef userName As String, ByRef password As String, ByRef authority As String) As Boolean Implements IReportServerCredentials.GetFormsCredentials

        authCookie = Nothing
        userName = Nothing
        password = Nothing
        authority = Nothing

        'Not using form credentials
        Return False

    End Function



End Class
