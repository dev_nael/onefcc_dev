﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="DHN_WarningLetterReadyPrint_Detail.aspx.vb" Inherits="DHN_DHN_WarningLetter_DHN_WarningLetterReadyPrint_Detail" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >

    <ext:FormPanel ID="FormPanel1" runat="server" Padding="5" AutoScroll="true" Layout="FormLayout">

        <Items>
            <ext:Container ID="Container1" runat="server" Layout="FormLayout" BodyPadding="5" AnchorHorizontal="100%">

                <Items>
                    
                    <ext:Hidden runat="server" ID="hreport">
                        </ext:Hidden>
                    <%--<ext:ComboBox ID="cboReport" runat="server" FieldLabel="Report" AllowBlank="false" Width="300"></ext:ComboBox>--%>

                    <%--<ext:Button ID="btnviewReport" runat="server" Text="View Report" AutoPostBack="true">
                    </ext:Button>--%>
                </Items>
            </ext:Container>
        </Items>
        <Content>
            <asp:ScriptManager ID="ScriptManager1" runat="server" ViewStateMode="Enabled"></asp:ScriptManager>
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" EnableViewState="True" ShowParameterPrompts="false" OnLoad="ReportViewer1_Load" OnPreRender="ReportViewer1_PreRender"></rsweb:ReportViewer>
        </Content>
       <Buttons>
            
            <ext:Button ID="BtnCancel" runat="server" Text="Cancel" Icon="Cancel">
            
                <DirectEvents>
                    <Click OnEvent="btnCancel_DirectEvent">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="500">
                        </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
 
    <%-- ================== END OF CONFIRMATION PANEL ========================== --%>
</asp:Content>

