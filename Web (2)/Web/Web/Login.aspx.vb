﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports NawaBLL
Imports NawaDAL

Partial Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                If Not NawaBLL.Common.SessionCurrentUser Is Nothing Then
                    Ext.Net.X.Redirect("Default.aspx")
                ElseIf Request.Params("ReturnUrl") <> "" Then
                    Ext.Net.X.Js.Call("parent.NawadataDirect.RedirectLogout")
                End If

                '20220614 Login with Authentication SSO SAML
                Using objLoginBll As New NawaBLL.V2.LoginBLL.LoginBLL
                    If objLoginBll.AplicationAuthentication = NawaBLL.V2.LoginBLL.LoginBLL.AplicationAuthenticationEnum.Window Then
                        'btnSSO.Hidden = True
                        btnSSO.Visible = False
                        'Labelor.Hidden = True
                        lblOR.Visible = False
                        'PanelBtnSSO.Hidden = True
                        'Window1.Height = "180"
                        'Window1.Width = "350"
                    ElseIf objLoginBll.AplicationAuthentication = NawaBLL.V2.LoginBLL.LoginBLL.AplicationAuthenticationEnum.Form Then
                        'btnSSO.Hidden = True
                        btnSSO.Visible = False
                        'Labelor.Hidden = True
                        lblOR.Visible = False
                        'PanelBtnSSO.Hidden = True
                        'Window1.Height = "180"
                        'Window1.Width = "350"
                    ElseIf objLoginBll.AplicationAuthentication = NawaBLL.V2.LoginBLL.LoginBLL.AplicationAuthenticationEnum.SSO_SAML Then
                        'btnSSO.Hidden = False
                        btnSSO.Visible = True
                        'Labelor.Hidden = False
                        lblOR.Visible = True
                        'PanelBtnSSO.Hidden = False
                        'Window1.Height = "250"
                        'Window1.Width = "350"

                    End If
                End Using
                'End 20220614 Login With Authentication SSO SAML

                '20-Aug-2021 : Load AML News'
                Load_AML_News()
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub





    'Function AutenticateScorpion(ldapdomain As String, applicationid As System.Guid, userid As String, password As String) As Boolean

    '    Dim objlogin As New Scorpion.Security
    '    Try

    '        Dim objresult As String = objlogin.Login(applicationid, ldapdomain, userid, password)
    '        'If objresult = "" Then


    '        'Else

    '        Dim strgroupmenu As String = objlogin.GetRole(applicationid, ldapdomain, userid)
    '        strgroupmenu = strgroupmenu.Trim

    '        NawaDevBLL.IFTIBLL.Createuser(userid, strgroupmenu)

    '        Dim ouser As MUser = NawaBLL.MUserBLL.GetMuserbyUSerId(userid)
    '        If Not ouser Is Nothing Then
    '            Dim strgroupmenuserver As String = NawaBLL.MGroupMenuBLL.GetGroupMenuNameByID(ouser.FK_MGroupMenu_ID.GetValueOrDefault(0))

    '            If strgroupmenu.ToLower.Trim = strgroupmenuserver.ToLower Then
    '                Return True
    '            Else
    '                Return False
    '            End If
    '        Else
    '            Return False
    '        End If

    '        'End If
    '    Catch ex As Exception
    '        Throw
    '    End Try


    'End Function
    Function AutenticateScorpion(ldapdomain As String, applicationid As System.Guid, userid As String, password As String) As Boolean

        Dim objlogin As New Scorpion.WebService1
        Try
            Dim objresult As String = objlogin.Login(applicationid.ToString, ldapdomain, userid, password)
            If objresult = "" Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Throw
        End Try


    End Function
    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.ServerClick
        Try
            '20-Aug-2021 Agar repeater berjalan maka message untuk username dan password kosong di sini saja
            If String.IsNullOrEmpty(txtUsername.Text.Trim) Or String.IsNullOrEmpty(txtPassword.Text.Trim) Then
                Throw New ApplicationException("Username and Password is required!")
            End If
            '-----------------------------------------------------------------------------------------------

            Dim objsysparam As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(NawaBLL.SystemParameterBLL.SytemParameterEnum.LoginUsedIPAddress)
            Dim IsCheckIPAddress As Boolean = True
            If Not objsysparam Is Nothing Then
                IsCheckIPAddress = objsysparam.SettingValue
            End If
            Using objLoginBll As New LoginBLL
                If objLoginBll.AplicationAuthentication = LoginBLL.AplicationAuthenticationEnum.Window Then
                    'done: cek ke AD
                    '         Dim struseridapp As String = LoginBLL.GetUserAppByUserLDAP(txtUsername.Text.Trim)
                    Dim objUser As MUser = LoginBLL.GetMuserByUserID(txtUsername.Text.Trim)

                    ' Dim strApplicationid As System.Guid = New Guid(SystemParameterBLL.GetSystemParameterByPk(21).SettingValue)
                    Dim strLDAPConnection As String = SystemParameterBLL.GetSystemParameterByPk(21).SettingValue
                    Dim strLDAPDomain As String = SystemParameterBLL.GetSystemParameterByPk(22).SettingValue

                    Dim strerror As String = ""
                    If Not objUser Is Nothing Then
                        If objUser.PK_MUser_ID <> 1 Then
                            Dim dLastLogin As Date
                            If objUser.LastLogin.HasValue Then
                                dLastLogin = objUser.LastLogin
                            Else
                                dLastLogin = objUser.CreatedDate
                            End If
                            'Done: cek last login apakah melebihi batas parameter Account Lock after Not Used (days)
                            NawaBLL.LoginBLL.CekParameterLoginAccountLock(objUser, dLastLogin)
                        End If
                        'Done:Validasi user disabled
                        If objUser.IsDisabled.GetValueOrDefault(False) Then
                            Dim objparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9000)
                            If Not objparam Is Nothing Then
                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparam.SettingValue)
                                Throw New Exception(objparam.SettingValue)
                            Else
                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "User cannot login because user has already been disabled.")
                                Throw New Exception("User cannot login because user has already been disabled.")
                            End If
                        End If
                        If Not objUser.Active.GetValueOrDefault(False) Then
                            Dim objparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9001)
                            If Not objparam Is Nothing Then
                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparam.SettingValue)
                                Throw New Exception(objparam.SettingValue)
                            Else
                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "User cannot login because user has already been inactive.")
                                Throw New Exception("User cannot login because user has already been inactive.")
                            End If
                        End If
                        'Done:Validasi group menu disabled
                        NawaBLL.LoginBLL.IsGroupMenuActive(objUser)
                        'Done:Validasi role disabled
                        NawaBLL.LoginBLL.IsRoleActive(objUser)
                        'done:Validasi n salah password
                        If objUser.PK_MUser_ID = 1 Then
                            Dim Salt As String = objUser.UserPasswordSalt
                            If objUser.UserPasword = NawaBLL.Common.Encrypt(txtPassword.Text.Trim, Salt) Then
                                If objUser.PK_MUser_ID = 1 OrElse (Now.Subtract(Convert.ToDateTime(objUser.LastChangePassword)).Days <= NawaBLL.LoginBLL.GetExpiredPasswordDay) Then
                                    If IsCheckIPAddress Then
                                        If objUser.PK_MUser_ID = 1 Or ((Convert.ToBoolean(objUser.InUsed) And objUser.IPAddress = Me.Request.UserHostAddress) Or Not Convert.ToBoolean(objUser.InUsed)) Then
                                            Dim intjmlalternatetask As Integer = NawaBLL.LoginBLL.IsHaveAlternateTask(objUser.UserID)
                                            If intjmlalternatetask > 0 Then
                                                'ada alternate,jadi harus pilih
                                                NawaBLL.LoginBLL.SetTempAlternate(objUser)
                                                Dim strpath As String = Request.ApplicationPath
                                                If strpath = "/" Then strpath = ""
                                                NawaBLL.Common.GetApplicationPath = strpath
                                                Dim StrRedirectNext As String = FormsAuthentication.GetRedirectUrl("", True).ToLower
                                                Dim StrRedirect As String = ""
                                                If StrRedirectNext <> "" Then
                                                    StrRedirect = "ChooseAlternateView.aspx?RedirectURL=" & StrRedirectNext.Replace(strpath.ToLower, "")
                                                Else
                                                    StrRedirect = "ChooseAlternateView.aspx"
                                                End If
                                                'Me.Response.Redirect(StrRedirect, False)
                                                Ext.Net.X.Redirect(StrRedirect)
                                            Else
                                                NawaBLL.LoginBLL.SetCurrentUserLogin(objUser, Request.UserHostAddress)
                                                Dim strpath As String = Request.ApplicationPath
                                                If strpath = "/" Then strpath = ""
                                                NawaBLL.Common.GetApplicationPath = strpath
                                                Dim StrRedirect As String = FormsAuthentication.GetRedirectUrl(NawaBLL.Common.SessionCurrentUser.UserID, True).ToLower
                                                ''Dim strpathtocheck As String = StrRedirect.Replace(strpath.ToLower, "")  '' Edited by Felix 03 Felix 2020
                                                Dim strpathtocheck As String = ""
                                                If strpath <> "" Then
                                                    strpathtocheck = StrRedirect.Replace(strpath.ToLower, "")
                                                Else

                                                End If
                                                Dim indexquestion As String = strpathtocheck.IndexOf("?")
                                                If indexquestion > 0 Then
                                                    strpathtocheck = strpathtocheck.Substring(0, indexquestion)
                                                End If
                                                If Not NawaBLL.LoginBLL.IsAuthenticatePage(strpathtocheck, NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID) Then
                                                    StrRedirect = "Default.aspx"
                                                Else
                                                    StrRedirect = "Default.aspx?RedirectURL=" & StrRedirect.Replace(strpath.ToLower, "")
                                                End If
                                                FormsAuthentication.SetAuthCookie(NawaBLL.Common.SessionCurrentUser.UserID, False)
                                                'Me.Response.Redirect(StrRedirect, False)
                                                Ext.Net.X.Redirect(StrRedirect)
                                            End If
                                        Else
                                            Dim objparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9002)
                                            If Not objparam Is Nothing Then
                                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparam.SettingValue)
                                                Throw New Exception(objparam.SettingValue)
                                            Else
                                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "Login failed. User Name is being used on a another computer.")
                                                Throw New Exception("Login failed. User Name is being used on a another computer.")
                                            End If
                                        End If
                                    Else
                                        Dim intjmlalternatetask As Integer = NawaBLL.LoginBLL.IsHaveAlternateTask(objUser.UserID)
                                        If intjmlalternatetask > 0 Then
                                            'ada alternate,jadi harus pilih
                                            NawaBLL.LoginBLL.SetTempAlternate(objUser)
                                            Dim strpath As String = Request.ApplicationPath
                                            If strpath = "/" Then strpath = ""
                                            NawaBLL.Common.GetApplicationPath = strpath
                                            Dim StrRedirectNext As String = FormsAuthentication.GetRedirectUrl("", True).ToLower
                                            Dim StrRedirect As String = ""
                                            If StrRedirectNext <> "" Then
                                                StrRedirect = "ChooseAlternateView.aspx?RedirectURL=" & StrRedirectNext.Replace(strpath.ToLower, "")
                                            Else
                                                StrRedirect = "ChooseAlternateView.aspx"
                                            End If
                                            'Me.Response.Redirect(StrRedirect, False)
                                            Ext.Net.X.Redirect(StrRedirect)
                                        Else
                                            NawaBLL.LoginBLL.SetCurrentUserLogin(objUser, Request.UserHostAddress)
                                            Dim strpath As String = Request.ApplicationPath.ToLower
                                            If strpath = "/" Then strpath = ""
                                            NawaBLL.Common.GetApplicationPath = strpath
                                            Dim StrRedirect As String = FormsAuthentication.GetRedirectUrl(NawaBLL.Common.SessionCurrentUser.UserID, True).ToLower
                                            'Dim strpathtocheck As String = StrRedirect.Replace(strpath.ToLower, "")
                                            Dim strpathtocheck As String = ""
                                            If strpath <> "" Then
                                                strpath = StrRedirect.Replace(strpath.ToLower, "")
                                            End If
                                            Dim indexquestion As String = strpathtocheck.IndexOf("?")
                                            If indexquestion > 0 Then
                                                strpathtocheck = strpathtocheck.Substring(0, indexquestion)
                                            End If
                                            If Not NawaBLL.LoginBLL.IsAuthenticatePage(strpathtocheck, NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID) Then
                                                StrRedirect = "Default.aspx"
                                            Else
                                                StrRedirect = "Default.aspx?RedirectURL=" & StrRedirect.Replace(strpath.ToLower, "")
                                            End If
                                            FormsAuthentication.SetAuthCookie(NawaBLL.Common.SessionCurrentUser.UserID, False)
                                            'Me.Response.Redirect(StrRedirect, False)
                                            Ext.Net.X.Redirect(StrRedirect)
                                        End If
                                    End If
                                Else
                                    'Done: force change password
                                    FormsAuthentication.SetAuthCookie("ForceChangePassword", False)
                                    Dim strpkuserid As String = NawaBLL.Common.EncryptQueryString(objUser.PK_MUser_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                                    'Response.Redirect("ForceChangePassword.aspx?PKUserID=" & strpkuserid, False)
                                    Ext.Net.X.Redirect("ForceChangePassword.aspx?PKUserID=" & strpkuserid)
                                End If
                            Else
                                NawaBLL.Common.GetPasswordRetry(objUser.UserID) += 1
                                If objUser.PK_MUser_ID <> 1 AndAlso NawaBLL.Common.GetPasswordRetry(objUser.UserID) >= NawaBLL.LoginBLL.GetAccountLockWrongPassword Then
                                    NawaBLL.LoginBLL.DisabledUser(objUser)
                                    NawaBLL.Common.GetPasswordRetry(objUser.UserID) = 0
                                    Throw New Exception("Password retry limit exceeded. The User has been disabled. Please contact your administrator.")
                                End If
                                Throw New Exception(" User or Password is not valid.")
                            End If
                        Else
                            'kalau bukan sysadmin ,cek AD


                            ' If AutenticateScorpion(strLDAPDomain, strApplicationid, txtUsername.Text.Trim, txtPassword.Text.Trim) Then
                            If NawaDevBLL.ActiveDirectoryBLL.AuthenticateUser(strLDAPDomain, txtUsername.Text.Trim, txtPassword.Text.Trim, strLDAPConnection, strerror) Then
                                If IsCheckIPAddress Then
                                    If ((Convert.ToBoolean(objUser.InUsed) And objUser.IPAddress = Me.Request.UserHostAddress) Or Not Convert.ToBoolean(objUser.InUsed)) Then
                                        'valid login ad
                                        NawaBLL.LoginBLL.SetCurrentUserLogin(objUser, Request.UserHostAddress)
                                        Dim strpath As String = Request.ApplicationPath
                                        If strpath = "/" Then strpath = ""
                                        NawaBLL.Common.GetApplicationPath = strpath
                                        Dim StrRedirect As String = FormsAuthentication.GetRedirectUrl(NawaBLL.Common.SessionCurrentUser.UserID, True).ToLower
                                        'Dim strpathtocheck As String = StrRedirect.Replace(strpath.ToLower, "") '' Edited By Felix 03 Feb 2020
                                        Dim strpathtocheck As String = ""
                                        If strpath <> "" Then
                                            strpathtocheck = StrRedirect.Replace(strpath.ToLower, "")
                                        End If
                                        Dim indexquestion As String = strpathtocheck.IndexOf("?")
                                        If indexquestion > 0 Then
                                            strpathtocheck = strpathtocheck.Substring(0, indexquestion)
                                        End If
                                        If Not NawaBLL.LoginBLL.IsAuthenticatePage(strpathtocheck, NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID) Then
                                            StrRedirect = "Default.aspx"
                                        Else
                                            StrRedirect = "Default.aspx?RedirectURL=" & StrRedirect.Replace(strpath.ToLower, "")
                                        End If
                                        FormsAuthentication.SetAuthCookie(NawaBLL.Common.SessionCurrentUser.UserID, False)
                                        'Me.Response.Redirect(StrRedirect, False)
                                        Ext.Net.X.Redirect(StrRedirect)
                                    Else
                                        LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "Login failed. User Name is being used on a another computer.")
                                        Throw New Exception("Login failed. User Name is being used on a another computer.")
                                    End If
                                Else
                                    'valid login ad
                                    NawaBLL.LoginBLL.SetCurrentUserLogin(objUser, Request.UserHostAddress)
                                    Dim strpath As String = Request.ApplicationPath
                                    If strpath = "/" Then strpath = ""
                                    NawaBLL.Common.GetApplicationPath = strpath
                                    Dim StrRedirect As String = FormsAuthentication.GetRedirectUrl(NawaBLL.Common.SessionCurrentUser.UserID, True).ToLower
                                    'Dim strpathtocheck As String = StrRedirect.Replace(strpath.ToLower, "") '' Edited By Felix 03 Feb 2020
                                    Dim strpathtocheck As String = ""
                                    If strpath <> "" Then
                                        strpathtocheck = StrRedirect.Replace(strpath.ToLower, "")
                                    End If
                                    Dim indexquestion As String = strpathtocheck.IndexOf("?")
                                    If indexquestion > 0 Then
                                        strpathtocheck = strpathtocheck.Substring(0, indexquestion)
                                    End If
                                    If Not NawaBLL.LoginBLL.IsAuthenticatePage(strpathtocheck, NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID) Then
                                        StrRedirect = "Default.aspx"
                                    Else
                                        StrRedirect = "Default.aspx?RedirectURL=" & StrRedirect.Replace(strpath.ToLower, "")
                                    End If
                                    FormsAuthentication.SetAuthCookie(NawaBLL.Common.SessionCurrentUser.UserID, False)
                                    'Me.Response.Redirect(StrRedirect, False)
                                    Ext.Net.X.Redirect(StrRedirect)
                                End If
                            Else
                                'bukan

                                NawaBLL.Common.GetPasswordRetry(objUser.UserID) += 1
                                If objUser.PK_MUser_ID <> 1 AndAlso NawaBLL.Common.GetPasswordRetry(objUser.UserID) >= NawaBLL.LoginBLL.GetAccountLockWrongPassword Then
                                    NawaBLL.LoginBLL.DisabledUser(objUser)
                                    NawaBLL.Common.GetPasswordRetry(objUser.UserID) = 0
                                    Dim objparamx As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9003)
                                    If Not objparamx Is Nothing Then
                                        LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparamx.SettingValue)
                                        Throw New Exception(objparamx.SettingValue)
                                    Else
                                        LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "Password retry limit exceeded. The User has been disabled. Please contact your administrator.")
                                        Throw New Exception("Password retry limit exceeded. The User has been disabled. Please contact your administrator")
                                    End If
                                End If
                                Dim objparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9004)
                                If Not objparam Is Nothing Then
                                    LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparam.SettingValue)
                                    Throw New Exception(objparam.SettingValue)
                                Else
                                    LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "User Or  Password Is Not valid.")
                                    Throw New Exception("User Or  Password Is Not valid")
                                End If


                            End If
                        End If
                    Else

                        '

                        ' If AutenticateScorpion(strLDAPDomain, strApplicationid, txtUsername.Text.Trim, txtPassword.Text.Trim) Then
                        If NawaDevBLL.ActiveDirectoryBLL.AuthenticateUser(strLDAPDomain, txtUsername.Text.Trim, txtPassword.Text.Trim, strLDAPConnection, strerror) Then
                            objUser = LoginBLL.GetMuserByUserID(txtUsername.Text.Trim)

                            If Not objUser Is Nothing Then



                                If IsCheckIPAddress Then
                                    If ((Convert.ToBoolean(objUser.InUsed) And objUser.IPAddress = Me.Request.UserHostAddress) Or Not Convert.ToBoolean(objUser.InUsed)) Then
                                        'valid login ad
                                        NawaBLL.LoginBLL.SetCurrentUserLogin(objUser, Request.UserHostAddress)
                                        Dim strpath As String = Request.ApplicationPath
                                        If strpath = "/" Then strpath = ""
                                        NawaBLL.Common.GetApplicationPath = strpath
                                        Dim StrRedirect As String = FormsAuthentication.GetRedirectUrl(NawaBLL.Common.SessionCurrentUser.UserID, True).ToLower
                                        'Dim strpathtocheck As String = StrRedirect.Replace(strpath.ToLower, "") '' Edited By Felix 3 Feb 2020
                                        Dim strpathtocheck As String = ""
                                        If strpath <> "" Then
                                            strpathtocheck = StrRedirect.Replace(strpath.ToLower, "")
                                        End If
                                        Dim indexquestion As String = strpathtocheck.IndexOf("?")
                                        If indexquestion > 0 Then
                                            strpathtocheck = strpathtocheck.Substring(0, indexquestion)
                                        End If
                                        If Not NawaBLL.LoginBLL.IsAuthenticatePage(strpathtocheck, NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID) Then
                                            StrRedirect = "Default.aspx"
                                        Else
                                            StrRedirect = "Default.aspx?RedirectURL=" & StrRedirect.Replace(strpath.ToLower, "")
                                        End If
                                        FormsAuthentication.SetAuthCookie(NawaBLL.Common.SessionCurrentUser.UserID, False)
                                        'Me.Response.Redirect(StrRedirect, False)
                                        Ext.Net.X.Redirect(StrRedirect)
                                    Else
                                        LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "Login failed. User Name is being used on a another computer.")
                                        Throw New Exception("Login failed. User Name is being used on a another computer.")
                                    End If
                                Else
                                    'valid login ad
                                    NawaBLL.LoginBLL.SetCurrentUserLogin(objUser, Request.UserHostAddress)
                                    Dim strpath As String = Request.ApplicationPath
                                    If strpath = "/" Then strpath = ""
                                    NawaBLL.Common.GetApplicationPath = strpath
                                    Dim StrRedirect As String = FormsAuthentication.GetRedirectUrl(NawaBLL.Common.SessionCurrentUser.UserID, True).ToLower
                                    'Dim strpathtocheck As String = StrRedirect.Replace(strpath.ToLower, "") '' Edited by Felix 03 Feb 2020
                                    Dim strpathtocheck As String = ""
                                    If strpath <> "" Then
                                        strpathtocheck = StrRedirect.Replace(strpath.ToLower, "")
                                    End If
                                    Dim indexquestion As String = strpathtocheck.IndexOf("?")
                                    If indexquestion > 0 Then
                                        strpathtocheck = strpathtocheck.Substring(0, indexquestion)
                                    End If
                                    If Not NawaBLL.LoginBLL.IsAuthenticatePage(strpathtocheck, NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID) Then
                                        StrRedirect = "Default.aspx"
                                    Else
                                        StrRedirect = "Default.aspx?RedirectURL=" & StrRedirect.Replace(strpath.ToLower, "")
                                    End If
                                    FormsAuthentication.SetAuthCookie(NawaBLL.Common.SessionCurrentUser.UserID, False)
                                    'Me.Response.Redirect(StrRedirect, False)
                                    Ext.Net.X.Redirect(StrRedirect)
                                End If
                            Else
                                'bukan

                                NawaBLL.Common.GetPasswordRetry(objUser.UserID) += 1
                                If objUser.PK_MUser_ID <> 1 AndAlso NawaBLL.Common.GetPasswordRetry(objUser.UserID) >= NawaBLL.LoginBLL.GetAccountLockWrongPassword Then
                                    NawaBLL.LoginBLL.DisabledUser(objUser)
                                    NawaBLL.Common.GetPasswordRetry(objUser.UserID) = 0
                                    Dim objparamx As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9003)
                                    If Not objparamx Is Nothing Then
                                        LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparamx.SettingValue)
                                        Throw New Exception(objparamx.SettingValue)
                                    Else
                                        LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "Password retry limit exceeded. The User has been disabled. Please contact your administrator.")
                                        Throw New Exception("Password retry limit exceeded. The User has been disabled. Please contact your administrator")
                                    End If
                                End If
                                Dim objparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9004)
                                If Not objparam Is Nothing Then
                                    LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparam.SettingValue)
                                    Throw New Exception(objparam.SettingValue)
                                Else
                                    LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "User Or  Password Is Not valid.")
                                    Throw New Exception("User Or  Password Is Not valid")
                                End If


                            End If
                        Else

                            Throw New Exception("User " & txtUsername.Text.Trim & " has not been found. Please contact your administrator to create your account first or Configure LDAP first.")

                        End If


                    End If
                ElseIf objLoginBll.AplicationAuthentication = LoginBLL.AplicationAuthenticationEnum.Form Then
                    Dim objUser As MUser = LoginBLL.GetMuserByUserID(txtUsername.Text.Trim)
                    If Not objUser Is Nothing Then
                        If objUser.PK_MUser_ID <> 1 Then
                            Dim dLastLogin As Date
                            If objUser.LastLogin.HasValue Then
                                dLastLogin = objUser.LastLogin
                            Else
                                dLastLogin = objUser.CreatedDate
                            End If
                            'Done: cek last login apakah melebihi batas parameter Account Lock after Not Used (days)
                            NawaBLL.LoginBLL.CekParameterLoginAccountLock(objUser, dLastLogin)
                        End If
                        'Done:Validasi user disabled
                        If objUser.IsDisabled.GetValueOrDefault(False) Then
                            Dim objparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9000)
                            If Not objparam Is Nothing Then
                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparam.SettingValue)
                                Throw New Exception(objparam.SettingValue)
                            Else
                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "User cannot login because user has already been disabled.")
                                Throw New Exception("User cannot login because user has already been disabled.")
                            End If
                        End If
                        'Done:Validasi user activation disabled
                        If Not objUser.Active.GetValueOrDefault(False) Then
                            Dim objparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9001)
                            If Not objparam Is Nothing Then
                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparam.SettingValue)
                                Throw New Exception(objparam.SettingValue)
                            Else
                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "User cannot login because user has already been inactive.")
                                Throw New Exception("User cannot login because user has already been inactive.")
                            End If
                        End If
                        'Done:Validasi group menu disabled
                        NawaBLL.LoginBLL.IsGroupMenuActive(objUser)
                        'Done:Validasi role disabled
                        NawaBLL.LoginBLL.IsRoleActive(objUser)
                        'done:Validasi n salah password
                        If objUser.PK_MUser_ID = 1 OrElse NawaBLL.Common.GetPasswordRetry(objUser.UserID) < NawaBLL.LoginBLL.GetAccountLockWrongPassword Then
                            Dim Salt As String = objUser.UserPasswordSalt
                            If objUser.UserPasword = NawaBLL.Common.Encrypt(txtPassword.Text.Trim, Salt) Then
                                If objUser.PK_MUser_ID = 1 OrElse (Now.Subtract(Convert.ToDateTime(objUser.LastChangePassword)).Days <= NawaBLL.LoginBLL.GetExpiredPasswordDay) Then
                                    If IsCheckIPAddress Then
                                        If objUser.PK_MUser_ID = 1 Or ((Convert.ToBoolean(objUser.InUsed) And objUser.IPAddress = Me.Request.UserHostAddress) Or Not Convert.ToBoolean(objUser.InUsed)) Then
                                            Dim intjmlalternatetask As Integer = NawaBLL.LoginBLL.IsHaveAlternateTask(objUser.UserID)
                                            If intjmlalternatetask > 0 Then
                                                NawaBLL.LoginBLL.SetTempAlternate(objUser)
                                                Dim strpath As String = Request.ApplicationPath
                                                If strpath = "/" Then strpath = ""
                                                NawaBLL.Common.GetApplicationPath = strpath
                                                Dim StrRedirectNext As String = FormsAuthentication.GetRedirectUrl("", True).ToLower
                                                Dim StrRedirect As String = ""
                                                If StrRedirectNext <> "" Then
                                                    StrRedirect = "ChooseAlternateView.aspx?RedirectURL=" & StrRedirectNext.Replace(strpath.ToLower, "")
                                                Else
                                                    StrRedirect = "ChooseAlternateView.aspx"
                                                End If
                                                'Me.Response.Redirect(StrRedirect, False)
                                                Ext.Net.X.Redirect(StrRedirect)
                                            Else
                                                NawaBLL.LoginBLL.SetCurrentUserLogin(objUser, Request.UserHostAddress)
                                                Dim strpath As String = Request.ApplicationPath
                                                If strpath = "/" Then strpath = ""
                                                NawaBLL.Common.GetApplicationPath = strpath
                                                Dim StrRedirect As String = FormsAuthentication.GetRedirectUrl(NawaBLL.Common.SessionCurrentUser.UserID, True).ToLower
                                                'Dim strpathtocheck As String = StrRedirect.Replace(strpath.ToLower, "") '' End of Felix 03 Feb 2020
                                                Dim strpathtocheck As String = ""
                                                If strpath <> "" Then
                                                    strpathtocheck = StrRedirect.Replace(strpath.ToLower, "")
                                                End If

                                                Dim indexquestion As String = strpathtocheck.IndexOf("?")
                                                If indexquestion > 0 Then
                                                    strpathtocheck = strpathtocheck.Substring(0, indexquestion)
                                                End If
                                                If Not NawaBLL.LoginBLL.IsAuthenticatePage(strpathtocheck, NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID) Then
                                                    StrRedirect = "Default.aspx"
                                                Else
                                                    StrRedirect = "Default.aspx?RedirectURL=" & StrRedirect.Replace(strpath.ToLower, "")
                                                End If
                                                FormsAuthentication.SetAuthCookie(NawaBLL.Common.SessionCurrentUser.UserID, False)
                                                'Me.Response.Redirect(StrRedirect, False)
                                                Ext.Net.X.Redirect(StrRedirect)
                                            End If
                                        Else
                                            Dim objparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9002)
                                            If Not objparam Is Nothing Then
                                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparam.SettingValue)
                                                Throw New Exception(objparam.SettingValue)
                                            Else
                                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "Login failed. User Name is being used on a another computer.")
                                                Throw New Exception("Login failed. User Name is being used on a another computer.")
                                            End If
                                        End If
                                    Else
                                        'cek kalau ada alternate,maka redirect pilih user yg mau dipakai
                                        Dim intjmlalternatetask As Integer = NawaBLL.LoginBLL.IsHaveAlternateTask(objUser.UserID)
                                        If intjmlalternatetask > 0 Then
                                            NawaBLL.LoginBLL.SetTempAlternate(objUser)
                                            Dim strpath As String = Request.ApplicationPath
                                            If strpath = "/" Then strpath = ""
                                            NawaBLL.Common.GetApplicationPath = strpath
                                            Dim StrRedirectNext As String = FormsAuthentication.GetRedirectUrl("", True).ToLower
                                            Dim StrRedirect As String = ""
                                            If StrRedirectNext <> "" Then
                                                StrRedirect = "ChooseAlternateView.aspx?RedirectURL=" & StrRedirectNext.Replace(strpath.ToLower, "")
                                            Else
                                                StrRedirect = "ChooseAlternateView.aspx"
                                            End If
                                            'Me.Response.Redirect(StrRedirect, False)
                                            Ext.Net.X.Redirect(StrRedirect)
                                        Else
                                            'tidak ada alternate, jadi langsung login
                                            NawaBLL.LoginBLL.SetCurrentUserLogin(objUser, Request.UserHostAddress)
                                            Dim strpath As String = Request.ApplicationPath
                                            If strpath = "/" Then strpath = ""
                                            NawaBLL.Common.GetApplicationPath = strpath
                                            Dim StrRedirect As String = FormsAuthentication.GetRedirectUrl(NawaBLL.Common.SessionCurrentUser.UserID, True).ToLower
                                            Dim strpathtocheck As String = ""
                                            If strpath <> "" Then
                                                strpathtocheck = StrRedirect.Replace(strpath.ToLower, "")
                                            End If


                                            Dim indexquestion As String = strpathtocheck.IndexOf("?")
                                            If indexquestion > 0 Then
                                                strpathtocheck = strpathtocheck.Substring(0, indexquestion)
                                            End If
                                            If Not NawaBLL.LoginBLL.IsAuthenticatePage(strpathtocheck, NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID) Then
                                                StrRedirect = "Default.aspx"
                                            Else
                                                StrRedirect = "Default.aspx?RedirectURL=" & StrRedirect.Replace(strpath.ToLower, "")
                                            End If
                                            FormsAuthentication.SetAuthCookie(NawaBLL.Common.SessionCurrentUser.UserID, False)
                                            Ext.Net.X.Redirect(StrRedirect)
                                            'Me.Response.Redirect(StrRedirect, False)
                                        End If
                                    End If
                                Else
                                    'Done: force change password
                                    FormsAuthentication.SetAuthCookie("ForceChangePassword", False)
                                    Dim strpkuserid As String = NawaBLL.Common.EncryptQueryString(objUser.PK_MUser_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                                    'Response.Redirect("ForceChangePassword.aspx?PKUserID=" & strpkuserid)
                                    Ext.Net.X.Redirect("ForceChangePassword.aspx?PKUserID=" & strpkuserid)
                                End If
                            Else
                                NawaBLL.Common.GetPasswordRetry(objUser.UserID) += 1
                                If objUser.PK_MUser_ID <> 1 AndAlso NawaBLL.Common.GetPasswordRetry(objUser.UserID) >= NawaBLL.LoginBLL.GetAccountLockWrongPassword Then
                                    NawaBLL.LoginBLL.DisabledUser(objUser)
                                    NawaBLL.Common.GetPasswordRetry(objUser.UserID) = 0
                                    Dim objparamx As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9003)
                                    If Not objparamx Is Nothing Then
                                        LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparamx.SettingValue)
                                        Throw New Exception(objparamx.SettingValue)
                                    Else
                                        LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "Password retry limit exceeded. The User has been disabled. Please contact your administrator.")
                                        Throw New Exception("Password retry limit exceeded. The User has been disabled. Please contact your administrator")
                                    End If
                                End If
                                Dim objparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9004)
                                If Not objparam Is Nothing Then
                                    LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparam.SettingValue)
                                    Throw New Exception(objparam.SettingValue)
                                Else
                                    LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "User Or  Password Is Not valid.")
                                    Throw New Exception("User Or  Password Is Not valid")
                                End If
                            End If
                        End If
                    Else
                        Dim objparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9005)
                        If Not objparam Is Nothing Then
                            LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparam.SettingValue)
                            Throw New Exception(objparam.SettingValue)
                        Else
                            LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "User has Not been found. Please contact your administrator to create your account first.")
                            Throw New Exception("User has Not been found. Please contact your administrator to create your account first.")
                        End If
                    End If
                ElseIf objLoginBll.AplicationAuthentication = NawaBLL.V2.LoginBLL.LoginBLL.AplicationAuthenticationEnum.SSO_SAML Then
                    Dim objUser As MUser = NawaBLL.V2.LoginBLL.LoginBLL.GetMuserByUserID(txtUsername.Text.Trim)
                    If Not objUser Is Nothing Then
                        If objUser.PK_MUser_ID <> 1 Then
                            Throw New Exception("User Login Using Authentication SSO, Please Click Button Login With SSO")
                        End If
                        'Done:Validasi group menu disabled
                        NawaBLL.V2.LoginBLL.LoginBLL.IsGroupMenuActive(objUser)
                        'Done:Validasi role disabled
                        NawaBLL.V2.LoginBLL.LoginBLL.IsRoleActive(objUser)
                        'done:Validasi n salah password
                        If objUser.PK_MUser_ID = 1 OrElse NawaBLL.Common.GetPasswordRetry(objUser.UserID) < NawaBLL.LoginBLL.GetAccountLockWrongPassword Then
                            Dim Salt As String = objUser.UserPasswordSalt
                            If objUser.UserPasword = NawaBLL.Common.Encrypt(txtPassword.Text.Trim, Salt) Then
                                If objUser.PK_MUser_ID = 1 OrElse (Now.Subtract(Convert.ToDateTime(objUser.LastChangePassword)).Days <= NawaBLL.LoginBLL.GetExpiredPasswordDay) Then
                                    If IsCheckIPAddress Then
                                        If objUser.PK_MUser_ID = 1 Or ((Convert.ToBoolean(objUser.InUsed) And objUser.IPAddress = Me.Request.UserHostAddress) Or Not Convert.ToBoolean(objUser.InUsed)) Then
                                            Dim intjmlalternatetask As Integer = NawaBLL.LoginBLL.IsHaveAlternateTask(objUser.UserID)
                                            If intjmlalternatetask > 0 Then
                                                NawaBLL.LoginBLL.SetTempAlternate(objUser)
                                                Dim strpath As String = Request.ApplicationPath
                                                If strpath = "/" Then strpath = ""
                                                NawaBLL.Common.GetApplicationPath = strpath
                                                Dim StrRedirectNext As String = FormsAuthentication.GetRedirectUrl("", True).ToLower
                                                Dim StrRedirect As String = ""
                                                If StrRedirectNext <> "" Then
                                                    StrRedirect = "ChooseAlternateView.aspx?RedirectURL=" & StrRedirectNext.Replace(strpath.ToLower, "")
                                                Else
                                                    StrRedirect = "ChooseAlternateView.aspx"
                                                End If
                                                'Me.Response.Redirect(StrRedirect, False)
                                                Ext.Net.X.Redirect(StrRedirect)
                                            Else
                                                NawaBLL.LoginBLL.SetCurrentUserLogin(objUser, Request.UserHostAddress)
                                                Dim strpath As String = Request.ApplicationPath
                                                If strpath = "/" Then strpath = ""
                                                NawaBLL.Common.GetApplicationPath = strpath
                                                Dim StrRedirect As String = FormsAuthentication.GetRedirectUrl(NawaBLL.Common.SessionCurrentUser.UserID, True).ToLower
                                                'Dim strpathtocheck As String = StrRedirect.Replace(strpath.ToLower, "") '' End of Felix 03 Feb 2020
                                                Dim strpathtocheck As String = ""
                                                If strpath <> "" Then
                                                    strpathtocheck = StrRedirect.Replace(strpath.ToLower, "")
                                                End If

                                                Dim indexquestion As String = strpathtocheck.IndexOf("?")
                                                If indexquestion > 0 Then
                                                    strpathtocheck = strpathtocheck.Substring(0, indexquestion)
                                                End If
                                                If Not NawaBLL.LoginBLL.IsAuthenticatePage(strpathtocheck, NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID) Then
                                                    StrRedirect = "Default.aspx"
                                                Else
                                                    StrRedirect = "Default.aspx?RedirectURL=" & StrRedirect.Replace(strpath.ToLower, "")
                                                End If
                                                FormsAuthentication.SetAuthCookie(NawaBLL.Common.SessionCurrentUser.UserID, False)
                                                'Me.Response.Redirect(StrRedirect, False)
                                                Ext.Net.X.Redirect(StrRedirect)
                                            End If
                                        Else
                                            Dim objparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9002)
                                            If Not objparam Is Nothing Then
                                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparam.SettingValue)
                                                Throw New Exception(objparam.SettingValue)
                                            Else
                                                LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "Login failed. User Name is being used on a another computer.")
                                                Throw New Exception("Login failed. User Name is being used on a another computer.")
                                            End If
                                        End If
                                    Else
                                        'cek kalau ada alternate,maka redirect pilih user yg mau dipakai
                                        Dim intjmlalternatetask As Integer = NawaBLL.LoginBLL.IsHaveAlternateTask(objUser.UserID)
                                        If intjmlalternatetask > 0 Then
                                            NawaBLL.LoginBLL.SetTempAlternate(objUser)
                                            Dim strpath As String = Request.ApplicationPath
                                            If strpath = "/" Then strpath = ""
                                            NawaBLL.Common.GetApplicationPath = strpath
                                            Dim StrRedirectNext As String = FormsAuthentication.GetRedirectUrl("", True).ToLower
                                            Dim StrRedirect As String = ""
                                            If StrRedirectNext <> "" Then
                                                StrRedirect = "ChooseAlternateView.aspx?RedirectURL=" & StrRedirectNext.Replace(strpath.ToLower, "")
                                            Else
                                                StrRedirect = "ChooseAlternateView.aspx"
                                            End If
                                            'Me.Response.Redirect(StrRedirect, False)
                                            Ext.Net.X.Redirect(StrRedirect)
                                        Else
                                            'tidak ada alternate, jadi langsung login
                                            NawaBLL.LoginBLL.SetCurrentUserLogin(objUser, Request.UserHostAddress)
                                            Dim strpath As String = Request.ApplicationPath
                                            If strpath = "/" Then strpath = ""
                                            NawaBLL.Common.GetApplicationPath = strpath
                                            Dim StrRedirect As String = FormsAuthentication.GetRedirectUrl(NawaBLL.Common.SessionCurrentUser.UserID, True).ToLower
                                            Dim strpathtocheck As String = ""
                                            If strpath <> "" Then
                                                strpathtocheck = StrRedirect.Replace(strpath.ToLower, "")
                                            End If


                                            Dim indexquestion As String = strpathtocheck.IndexOf("?")
                                            If indexquestion > 0 Then
                                                strpathtocheck = strpathtocheck.Substring(0, indexquestion)
                                            End If
                                            If Not NawaBLL.LoginBLL.IsAuthenticatePage(strpathtocheck, NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID) Then
                                                StrRedirect = "Default.aspx"
                                            Else
                                                StrRedirect = "Default.aspx?RedirectURL=" & StrRedirect.Replace(strpath.ToLower, "")
                                            End If
                                            FormsAuthentication.SetAuthCookie(NawaBLL.Common.SessionCurrentUser.UserID, False)
                                            Ext.Net.X.Redirect(StrRedirect)
                                            'Me.Response.Redirect(StrRedirect, False)
                                        End If
                                    End If
                                Else
                                    'Done: force change password
                                    FormsAuthentication.SetAuthCookie("ForceChangePassword", False)
                                    Dim strpkuserid As String = NawaBLL.Common.EncryptQueryString(objUser.PK_MUser_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                                    'Response.Redirect("ForceChangePassword.aspx?PKUserID=" & strpkuserid)
                                    Ext.Net.X.Redirect("ForceChangePassword.aspx?PKUserID=" & strpkuserid)
                                End If
                            Else
                                NawaBLL.Common.GetPasswordRetry(objUser.UserID) += 1
                                If objUser.PK_MUser_ID <> 1 AndAlso NawaBLL.Common.GetPasswordRetry(objUser.UserID) >= NawaBLL.LoginBLL.GetAccountLockWrongPassword Then
                                    NawaBLL.LoginBLL.DisabledUser(objUser)
                                    NawaBLL.Common.GetPasswordRetry(objUser.UserID) = 0
                                    Dim objparamx As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9003)
                                    If Not objparamx Is Nothing Then
                                        LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparamx.SettingValue)
                                        Throw New Exception(objparamx.SettingValue)
                                    Else
                                        LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "Password retry limit exceeded. The User has been disabled. Please contact your administrator.")
                                        Throw New Exception("Password retry limit exceeded. The User has been disabled. Please contact your administrator")
                                    End If
                                End If
                                Dim objparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(9004)
                                If Not objparam Is Nothing Then
                                    LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparam.SettingValue)
                                    Throw New Exception(objparam.SettingValue)
                                Else
                                    LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "User Or  Password Is Not valid.")
                                    Throw New Exception("User Or  Password Is Not valid")
                                End If
                            End If
                        End If
                    Else

                        Dim objparam As NawaDAL.SystemParameter = NawaBLL.V2.Basic.SystemParameterBLL.GetSystemParameterByPk(9005)
                        If Not objparam Is Nothing Then
                            NawaBLL.V2.LoginBLL.LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, objparam.SettingValue)
                            Throw New Exception(objparam.SettingValue)

                        Else
                            NawaBLL.V2.LoginBLL.LoginBLL.Setuserfaillogin(txtUsername.Text.Trim, Me.Request.UserHostAddress, "User has Not been found. Please contact your administrator to create your account first.")
                            Throw New Exception("User has Not been found. Please contact your administrator to create your account first.")

                        End If


                    End If
                    'End 20220614 Login with Authentication SSO SAML
                End If
            End Using
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            CustomValidator1.IsValid = False
            CustomValidator1.ErrorMessage = ex.Message
            'Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#Region "Update 20-Aug-2021 : AML News"
    Protected Sub Load_AML_News()
        Try

            Dim query As String = ""
            Dim intNewsPerPage As Integer = 4

            Const PATH_TEMP_DIR As String = "~\temp\"
            'Create Directory if not exists
            If Not Directory.Exists(Server.MapPath(PATH_TEMP_DIR)) Then
                Directory.CreateDirectory(Server.MapPath(PATH_TEMP_DIR))
            End If

            'Get News limit on login form
            query = "SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID=8008"
            Dim intNewsLimit As Integer = SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
            If Not IsNothing(intNewsLimit) Then
                intNewsPerPage = intNewsLimit
            End If

            'Get News Page
            query = "SELECT TOP " & intNewsLimit & " *, IIF(LEN(News_Content_Short)>300, SUBSTRING(News_Content_Short,1,200) + '...', News_Content) as News_Content_Limited, convert(varchar, createddate, 106) as Dates FROM AML_News WHERE IsPublic=1"
            query = query + " AND ValidFrom <= '" & DateTime.Now.ToString("yyyy-MM-dd") & "' AND ValidTo >= '" & DateTime.Now.ToString("yyyy-MM-dd") & "'"
            query = query + " ORDER BY PK_AML_News_ID DESC"
            Dim dtNews As New Data.DataTable
            dtNews = SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)

            '13-Sep-2021 Add column image untuk menampung URL
            'Running the following script for additional columns
            dtNews.Columns.Add(New DataColumn("News_Image_URL", GetType(String)))
            If dtNews.Rows.Count > 0 Then
                For Each item As Data.DataRow In dtNews.Rows
                    If Not IsDBNull(item("News_Image")) Then
                        'Filename and location for output template
                        Dim tempImage As String = "AML_News_" & item("PK_AML_News_ID") & "_" & item("News_ImageName") 'Guid.NewGuid.ToString & .News_ImageName
                        Dim objfileinfo = New FileInfo(Server.MapPath(PATH_TEMP_DIR & tempImage))

                        Dim data() As Byte = item("News_Image")
                        File.WriteAllBytes(objfileinfo.FullName, data)

                        item("News_Image_URL") = "Temp/" & tempImage
                    End If
                Next
            End If

            'Convert to PageDataSource
            If dtNews IsNot Nothing And dtNews.Rows.Count > 0 Then
                'For Each row As DataRow In dtNews.Rows
                '    Dim searchedValue As String = Nothing
                '    searchedValue = row.Item("News_Image").ToString()

                '    If IsNoImage(searchedValue) Then
                '    Else
                '        row.Item("News_Image") = Nothing
                '    End If
                'Next row
                Dim pdsNews As New PagedDataSource
                Dim dvNews As DataView = New DataView(dtNews)
                pdsNews.DataSource = dvNews

                If ViewState("PageNumber") IsNot Nothing Then
                    pdsNews.CurrentPageIndex = Convert.ToInt32(ViewState("PageNumber"))
                Else
                    pdsNews.CurrentPageIndex = 0
                End If

                'Bind to repeater
                rptNews.DataSource = pdsNews
                rptNews.DataBind()
                rowNoNews.Visible = False
                rptNews.Visible = True
            Else
                'rptNews.Visible = False
                rowNoNews.Visible = True
                rowMoreNews.Visible = False
                'Load_No_News()
            End If

        Catch ex As Exception
            'Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            'Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
            Load_No_News()
        End Try
    End Sub

    Protected Sub rptPaging_ItemCommand(ByVal source As Object, ByVal e As RepeaterCommandEventArgs)
        ViewState("PageNumber") = Convert.ToInt32(e.CommandArgument) - 1
        Load_AML_News()
    End Sub

    Protected Sub Load_No_News()
        Try
            rowNoNews.Visible = True
            rowNews.Visible = False
            rowMoreNews.Visible = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Function IsNoImage(source As String) As Boolean
    '    Dim Request As HttpWebRequest = HttpWebRequest.Create(source)
    '    Request.Method = "HEAD"

    '    Dim exists As Boolean
    '    Try

    '        Request.GetResponse()
    '        exists = True
    '    Catch

    '        exists = False


    '    End Try
    '    If exists = True Then
    '        Return True
    '    Else
    '        Return False
    '    End If


    '    exists = Nothing
    'End Function

    Protected Sub btnMoreNews_Click()
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "~/AML/AMLNews/AMLNews_PublicView.aspx"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub rptNews_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptNews.ItemCommand
        Try
            If e.CommandName = "remove" Then
                Dim intNewsID As Integer = Convert.ToInt32(e.CommandArgument)
                Dim strEncNewsID As String = NawaBLL.Common.EncryptQueryString(intNewsID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & "~/AML/AMLNews/AMLNews_PublicDetail.aspx?NewsID=" & strEncNewsID)

            End If

        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


#End Region

#Region "20-Sep-2022 Tambahan untuk SSO"
    'Protected Sub btnSSO_Click(sender As Object, e As EventArgs)
    '    Try
    '        Dim objparamSamlEndpoint As NawaDAL.SystemParameter = NawaBLL.V2.Basic.SystemParameterBLL.GetSystemParameterByPk(300)
    '        Dim objparamSamlEntityID As NawaDAL.SystemParameter = NawaBLL.V2.Basic.SystemParameterBLL.GetSystemParameterByPk(301)
    '        Dim objparamSamlAssertionConsumer As NawaDAL.SystemParameter = NawaBLL.V2.Basic.SystemParameterBLL.GetSystemParameterByPk(302)

    '        'Dim samlEndpoint = "http://saml-provider-that-we-use.com/login/"
    '        'Dim samlEndpoint = "https://dev-61270608.okta.com/home/dev-61270608_samlnf2_1/0oa5d3tchfsNZfUwF5d7/aln5d3w5euKKmG9tc5d7"
    '        Dim samlEndpoint As String = objparamSamlEndpoint.SettingValue
    '        Dim request = New NawaBLL.V2.SSOSAML.Saml.AuthRequest(objparamSamlEntityID.SettingValue, objparamSamlAssertionConsumer.SettingValue)
    '        Ext.Net.X.Redirect(request.GetRedirectUrl(samlEndpoint))
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub btnSSO_Click(sender As Object, e As EventArgs) Handles btnSSO.ServerClick
        Try
            Dim objparamSamlEndpoint As NawaDAL.SystemParameter = NawaBLL.V2.Basic.SystemParameterBLL.GetSystemParameterByPk(300)
            Dim objparamSamlEntityID As NawaDAL.SystemParameter = NawaBLL.V2.Basic.SystemParameterBLL.GetSystemParameterByPk(301)
            Dim objparamSamlAssertionConsumer As NawaDAL.SystemParameter = NawaBLL.V2.Basic.SystemParameterBLL.GetSystemParameterByPk(302)

            'Dim samlEndpoint = "http://saml-provider-that-we-use.com/login/"
            'Dim samlEndpoint = "https://dev-61270608.okta.com/home/dev-61270608_samlnf2_1/0oa5d3tchfsNZfUwF5d7/aln5d3w5euKKmG9tc5d7"
            Dim samlEndpoint As String = objparamSamlEndpoint.SettingValue
            Dim request = New NawaBLL.V2.SSOSAML.Saml.AuthRequest(objparamSamlEntityID.SettingValue, objparamSamlAssertionConsumer.SettingValue)
            Ext.Net.X.Redirect(request.GetRedirectUrl(samlEndpoint))
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            CustomValidator1.IsValid = False
            CustomValidator1.ErrorMessage = ex.Message
            'Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class
