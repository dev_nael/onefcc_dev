﻿
Partial Class NDSDropDownField
    Inherits System.Web.UI.UserControl

    Private m_readonly As Boolean = False
    Private m_hidden As Boolean = False
    Private m_emptyText As String
    Private m_strFieldStyle As String
    Private m_label As String
    Private m_strfield As String
    Private m_strtablequery As String
    Private m_strfilter As String
    Private m_anchorlayout As String = "100%"
    Private m_allowblank As Boolean = True
    Private m_LabelAlign As LabelAlign
    Private m_LabelWidth As Integer
    ' -- 2020 23 07 additional apuy
    Private m_displayField As String
    Private m_valueField As String

    'Update Nandar Tambah Event Mask
    Private m_EventMask As Boolean
    '++++++++++++++++++++++++++++++++++++++


    Public Event OnValueChanged As System.EventHandler
    'Shadows Event OnValueChanged()

    Public Property StringFieldStyle() As String
        Get
            Return m_strFieldStyle
        End Get
        Set(ByVal value As String)
            m_strFieldStyle = value

            ComboBox1.FieldStyle = value
        End Set

    End Property
    Public Property IsReadOnly() As Boolean
        Get
            Return m_readonly
        End Get
        Set(ByVal value As Boolean)
            m_readonly = value
            ComboBox1.ReadOnly = value
            'If value Then
            '    ComboBox1.FieldStyle = "background-color: #ddd"
            'End If

            SetBackgroundColor()
        End Set
    End Property
    Public Property IsHidden() As Boolean
        Get
            Return m_hidden
        End Get
        Set(ByVal value As Boolean)
            m_hidden = value
            ComboBox1.Hidden = value
        End Set
    End Property
    Public Property StringValue() As String
        Get
            Return Session("ComboBox" + Me.ID + ".StringValue")
        End Get
        Set(ByVal value As String)
            Session("ComboBox" + Me.ID + ".StringValue") = value
        End Set

    End Property

    'Update Nandar Tambah Value Untuk RawValue
    Public Property StringRawValue() As String
        Get
            Return Session("RawValueComboBox" + Me.ID + ".StringValue")
        End Get
        Set(value As String)
            Session("RawValueComboBox" + Me.ID + ".StringValue") = value
        End Set
    End Property
    '++++++++++++++++++++++++++++++++++++++++++

    Public Property SelectedItemValue() As String
        Get
            'Updateed Apuy 16092020 untuk apabila kondisi dihapus return NULL
            'If String.IsNullOrEmpty(ComboBox1.Text.Trim) Then
            '    Return ""
            'Else
            Return Session("ComboBox" + Me.ID + ".StringValue")
            'End If
        End Get
        Set(ByVal value As String)
            Session("ComboBox" + Me.ID + ".StringValue") = value
        End Set

    End Property

    Public Property SelectedItemText() As String
        Get
            Return Session("RawValueComboBox" + Me.ID + ".StringValue")
        End Get
        Set(value As String)
            Session("RawValueComboBox" + Me.ID + ".StringValue") = value
        End Set
    End Property

    Public Property Label() As String
        Get
            Return m_label
        End Get
        Set(ByVal value As String)
            m_label = value
        End Set
    End Property

    Public Property StringField() As String
        Get
            Return Session("ComboBox" + Me.ID + ".m_strfield")
        End Get
        Set(ByVal value As String)
            Session("ComboBox" + Me.ID + ".m_strfield") = value
        End Set
        'Get
        '    Return m_strfield
        'End Get
        'Set(ByVal value As String)
        '    m_strfield = value
        'End Set
    End Property

    Public Property StringTable() As String
        Get
            Return Session("ComboBox" + Me.ID + ".m_strtablequery")
        End Get
        Set(ByVal value As String)
            Session("ComboBox" + Me.ID + ".m_strtablequery") = value
        End Set
        'Get
        '    Return m_strtablequery
        'End Get
        'Set(ByVal value As String)
        '    m_strtablequery = value
        'End Set
    End Property

    Public Property StringFilter() As String
        Get
            Return Session("ComboBox" + Me.ID + ".StringFilter")
        End Get
        Set(ByVal value As String)
            Session("ComboBox" + Me.ID + ".StringFilter") = value
        End Set

    End Property
    Public Property AnchorHorizontal() As String
        Get
            Return m_anchorlayout
        End Get
        Set(ByVal value As String)
            m_anchorlayout = value
        End Set
    End Property
    Public Property LabelAlign() As LabelAlign
        Get
            Return m_LabelAlign
        End Get
        Set(ByVal value As LabelAlign)
            m_LabelAlign = value
        End Set
    End Property

    Public Property LabelWidth() As Integer
        Get
            Return m_LabelWidth
        End Get
        Set(ByVal value As Integer)
            m_LabelWidth = value
        End Set
    End Property

    'Public Property AllowBlank() As String
    '    Get
    '        Return m_allowblank
    '    End Get
    '    Set(ByVal value As String)
    '        m_allowblank = value
    '    End Set
    'End Property

    Public Property AllowBlank() As String
        Get
            Return m_allowblank
        End Get
        Set(ByVal value As String)
            m_allowblank = value

            ComboBox1.AllowBlank = value
            'If value Then
            '    If Not m_readonly Then
            '        ComboBox1.FieldStyle = "background-color: #FFFFFF"
            '    End If
            'Else
            '    If Not m_readonly Then
            '        ComboBox1.FieldStyle = "background-color: #FFE4C4"
            '    End If
            'End If

            SetBackgroundColor()

        End Set
    End Property

    Public Property EmptyText() As String
        Get
            Return m_emptyText
        End Get
        Set(ByVal value As String)
            m_emptyText = value
        End Set
    End Property

    Public Property DisplayTextWithValue() As Boolean
        Get
            If Session("ComboBox" + Me.ID + ".m_displaytextwithvalue") Is Nothing Then
                Session("ComboBox" + Me.ID + ".m_displaytextwithvalue") = False
            End If
            Return Session("ComboBox" + Me.ID + ".m_displaytextwithvalue")
        End Get
        Set(ByVal value As Boolean)
            Session("ComboBox" + Me.ID + ".m_displaytextwithvalue") = value
        End Set
    End Property

    'Public ReadOnly Property ReadableDisplayTextWithValue() As String
    '    Get
    '        If m_displaytextwithvalue = True Then
    '            Return "'true'"
    '        Else
    '            Return "'false'"
    '        End If
    '    End Get
    'End Property

    Public ReadOnly Property TextValue() As String
        Get
            ''If Not String.IsNullOrEmpty(ComboBox1.Text) And ComboBox1.Text.Contains("-") Then
            ''    Return ComboBox1.Text.Substring(0, ComboBox1.Text.IndexOf("-"))
            ''Else
            ''    Return ""
            ''End If

            ''-- SCB doesn't want to show the ID, just the name in combobox text
            'If Not String.IsNullOrEmpty(ComboBox1.Value) Then
            '    Return StringValue
            'Else
            '    Return Nothing
            'End If

            Return StringValue

        End Get
    End Property

    Public ReadOnly Property TextRawValue() As String
        Get
            'Return ComboBox1.Text
            Return StringRawValue
        End Get
    End Property

    ' -- 2020 07 23 additional apuy

    Public Property DisplayField() As String

        Get
            Return Session("ComboBox" + Me.ID + ".m_displayField")
        End Get
        Set(ByVal value As String)
            Session("ComboBox" + Me.ID + ".m_displayField") = value
        End Set
    End Property

    Public ReadOnly Property GetDisplayField() As String
        Get
            Return "'" + Session("ComboBox" + Me.ID + ".m_valueField") + "'"
        End Get

    End Property

    Public Property ValueField() As String
        'Get
        '    Return m_valueField
        'End Get
        'Set(ByVal value As String)
        '    m_valueField = value
        'End Set

        Get
            Return Session("ComboBox" + Me.ID + ".m_valueField")
        End Get
        Set(ByVal value As String)
            Session("ComboBox" + Me.ID + ".m_valueField") = value
        End Set
    End Property

    Dim objNawaDataPicker As NawaBLL.NawaDataPicker = New NawaBLL.NawaDataPicker


    Protected Sub storePicker_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            If Not e.Parameters("filterheader") Is Nothing Then
                e.Parameters("filterheader") = e.Parameters("filterheader").Replace("+", "*")
            End If

            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)
            Dim strsort As String = ""
            Dim strProperty As String = ""
            For Each item As DataSorter In e.Sort
                strProperty = "[" + item.Property + "]"
                strsort += strProperty & " " & item.Direction.ToString
            Next
            'Dim objStoredata As Ext.Net.Store = CType(sender, Ext.Net.Store)


            objNawaDataPicker.GetPickerNew(ComboBox1, StringTable, StringField, "", "", "GridClick(#{ComboBox1},#{GridPanel},#{Window});", True, True, 600, 300)

            'ketika pertama kali harus di panggil render supaya di load ulang,tapi nextnya gak boleh di render lagi ,jadi cuma 1 kali 
            If Session("ComboBox" + Me.ID + ".counterrender") Is Nothing Then
                Session("ComboBox" + Me.ID + ".counterrender") = 1
                GridPanel.Render()

            End If



            If Ext.Net.X.IsAjaxRequest Then
                ComboBox1.Show()
            End If


            'Tambahan untuk filter kolom yang mengandung spasi
            'If Not (String.IsNullOrEmpty(strfilter)) Then



            '    Dim posisiLike As Int16 = InStr(strfilter, "like")
            '    Dim fieldFilter As String = Left(strfilter, posisiLike - 1)


            '    Dim filterData As ObjectModel.ReadOnlyCollection(Of FilterHeaderCondition) = New FilterHeaderConditions(e.Parameters("filterheader")).Conditions
            '    For Each filter As FilterHeaderCondition In filterData
            '        fieldFilter = filter.DataIndex

            '        If fieldFilter.Contains(" ") Then
            '            fieldFilter = "[" + Left(fieldFilter.Trim(), posisiLike - 1) + "]"
            '        End If


            '        Dim fields As String() = StringField.Split(",")
            '        Dim field As String

            '        For Each field In fields
            '            If InStr(field, fieldFilter) > 0 Then
            '                field = field.ToUpper

            '                If field.Contains("AS") Then
            '                    Dim posisiAS As Int16 = InStr(field, "AS")
            '                    fieldFilter = Left(field, posisiAS - 1)
            '                End If
            '            End If
            '        Next

            '        If String.IsNullOrEmpty(strfilter) Then
            '            strfilter = fieldFilter + " like '%" + filter.Value(Of String) + "%'"
            '        Else
            '            strfilter = strfilter + " AND " + fieldFilter + " like '%" + filter.Value(Of String) + "%'"
            '        End If
            '    Next

            '    'Return
            'End If

            Dim realFilter As String = StringFilter

            If String.IsNullOrEmpty(StringFilter) Then
                realFilter = strfilter
            ElseIf Not String.IsNullOrEmpty(strfilter) Then
                realFilter = StringFilter & " AND " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(StringTable, StringField, realFilter, strsort, intStart, intLimit, inttotalRecord)
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If


            e.Total = inttotalRecord
            store.DataSource = DataPaging
            store.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Private Sub NDSDropDownField_Load(sender As Object, e As EventArgs) Handles Me.Load

        ComboBox1.AnchorHorizontal = AnchorHorizontal
        SetEventMask(IsEventMask)

        If Not Ext.Net.X.IsAjaxRequest Then
            Session("ComboBox" + Me.ID + ".counterrender") = Nothing
            If DisplayTextWithValue = Nothing Then
                DisplayTextWithValue = False
            End If

            'objNawaDataPicker.GetPickerNew(ComboBox1, StringTable, StringField, "", "", "GridClick(#{ComboBox1},#{GridPanel},#{Window});", True, True, 600, 300)
            ComboBox1.FieldLabel = Label
            ComboBox1.AllowBlank = AllowBlank
            Window.Title = "List of " & Me.Label
            ComboBox1.EmptyText = EmptyText
            If LabelWidth > 0 Then
                ComboBox1.LabelWidth = LabelWidth
            End If
        Else


        End If
    End Sub

    Public Sub SetTextValue(values As String)
        'ComboBox1.Text = values

        If Not String.IsNullOrEmpty(values) And values.Contains("-") Then
            'ComboBox1.Value = values.Substring(values.IndexOf("-") + 1, Len(values) - values.IndexOf("-") - 1)
            ComboBox1.Value = values
            StringValue = values.Substring(0, values.IndexOf("-"))
        ElseIf Not String.IsNullOrEmpty(values) Then
            ComboBox1.Value = values
            StringValue = values
        Else
            ComboBox1.Value = Nothing
            StringValue = Nothing
        End If
    End Sub
    Public Sub SetTextWithTextValue(values As String, displayValue As String)

        ComboBox1.Value = values
        ComboBox1.SetText(displayValue)
        StringValue = values

        StringRawValue = displayValue
    End Sub

    Public Sub GridPanel_OnClick(sender As Object, e As DirectEventArgs)
        StringValue = JSON.Deserialize(e.ExtraParams(0).Value)(ValueField).ToString()
        StringRawValue = JSON.Deserialize(e.ExtraParams(0).Value)(DisplayField).ToString()

        If DisplayTextWithValue Then
            ComboBox1.SetText(StringValue + " - " + StringRawValue)
        Else
            ComboBox1.SetText(StringRawValue)
        End If

        RaiseEvent OnValueChanged(sender, e)
    End Sub

    Public Sub RefreshData(value As String)
        StringFilter = value
    End Sub

    Public Sub SetHidden(values As Boolean)
        ComboBox1.Hidden = values
    End Sub
    Public Sub SetEventMask(value As Boolean)
        Dim objGrid As Ext.Net.GridPanel = GridPanel
        objGrid.DirectEvents.Select.EventMask.ShowMask = value
        objGrid.DirectEvents.Select.EventMask.Msg = "Loading..."
        objGrid.DirectEvents.Select.EventMask.MinDelay = 500
    End Sub

    Private Sub NDSDropDownField_Init(sender As Object, e As EventArgs) Handles Me.Init
        'If String.IsNullOrEmpty(StringTable) Then
        '    Throw New ApplicationException("String table cannot be null")
        'End If

        'If String.IsNullOrEmpty(StringField) Then
        '    Throw New ApplicationException("String field cannot be null")
        'End If

        'If String.IsNullOrEmpty(DisplayField) Then
        '    Throw New ApplicationException("String Display field cannot be null")
        'End If

        'If String.IsNullOrEmpty(ValueField) Then
        '    Throw New ApplicationException("String Value field cannot be null")
        'End If
    End Sub
    'Update Nandar Tambah Event Mask
    Public Property IsEventMask() As Boolean
        Get
            Return m_EventMask
        End Get
        Set(ByVal value As Boolean)
            m_EventMask = value
        End Set
    End Property
    '++++++++++++++++++++++++++++++++++++++

    '01-Nov-2021 Adi : ubah trigger clear jadi Right Button Clear untuk trigger perubahan
    Public Sub btn_Clear_Click(sender As Object, e As DirectEventArgs)
        Me.SetTextValue("")
        RaiseEvent OnValueChanged(sender, e)
    End Sub
    'End of 01-Nov-2021 Adi : ubah trigger clear jadi Right Button Clear untuk trigger perubahan

    Protected Sub SetBackgroundColor()
        If m_readonly Then
            ComboBox1.FieldStyle = "background-color: #f0f0f0"
        Else
            If m_allowblank Then
                ComboBox1.FieldStyle = "background-color: #FFFFFF"
            Else
                ComboBox1.FieldStyle = "background-color: #FFE4C4"
            End If
        End If
    End Sub

End Class
