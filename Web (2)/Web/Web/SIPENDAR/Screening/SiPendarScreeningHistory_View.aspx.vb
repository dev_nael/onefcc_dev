﻿Imports Ext.Net
Imports OfficeOpenXml
Imports NawaBLL
Imports System.Data
Imports NawaDAL

Partial Class SiPendarScreeningHistory_View

    Inherits Parent
    Public objFormModuleView As NawaBLL.FormModuleView

    Public Property strWhereClause() As String
        Get
            Return Session("SiPendarScreeningHistory_View.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("SiPendarScreeningHistory_View.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("SiPendarScreeningHistory_View.strSort")
        End Get
        Set(ByVal value As String)
            Session("SiPendarScreeningHistory_View.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("SiPendarScreeningHistory_View.indexStart")
        End Get
        Set(ByVal value As String)
            Session("SiPendarScreeningHistory_View.indexStart") = value
        End Set
    End Property
    Public Property QueryTable() As String
        Get
            Return Session("SiPendarScreeningHistory_View.Table")
        End Get
        Set(ByVal value As String)
            Session("SiPendarScreeningHistory_View.Table") = value
        End Set
    End Property
    Public Property QueryField() As String
        Get
            Return Session("SiPendarScreeningHistory_View.Field")
        End Get
        Set(ByVal value As String)
            Session("SiPendarScreeningHistory_View.Field") = value
        End Set
    End Property
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                    'Update BSIM 14Oct2020
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("SipendarScreeningHistory")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=SipendarScreeningHistory.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                    'Update BSIM 14Oct2020
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=SipendarScreeningHistorycsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not String.IsNullOrEmpty(cboExportExcel.SelectedItem.Value) Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, SystemParameterBLL.GetPageSize, 0)
                    'Update BSIM - 14Oct2020
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("SipendarScreeningHistory")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=SipendarScreeningHistory.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, SystemParameterBLL.GetPageSize, 0)
                    'Update BSIM - 14Oct2020
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=SipendarScreeningHistorycsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub


    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            'Begin Penambahan Advanced Filter
            If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                Toolbar2.Hidden = True
            Else
                Toolbar2.Hidden = False
            End If
            LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")
            'END Penambahan Advanced Filter


            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

            strfilter = strfilter.Replace("Active", "SipendarScreeningJudgement" & ".Active")


            Dim strsort As String = "ID DESC"
            For Each item As DataSorter In e.Sort
                'strsort += ", " & item.Property & " " & item.Direction.ToString
                strsort = item.Property & " " & item.Direction.ToString '' 04-Oct-2021 Daniel
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter
            'Begin Penambahan Advanced Filter
            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            End If
            'END Penambahan Advanced Filter

            Me.strOrder = strsort

            QueryTable = "vw_SIPENDAR_SCREENING_HISTORY"
            QueryField = "*"
            Dim DataPaging As DataTable = SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)

            'Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = DLL.SQLHelper.ExecuteTabelPaging("CustomerInformation_WebTempTable", "CIFNo, Name, DateOfBirth, AccountOwnerId, OpeningDate, WorkingUnitName, IsCustomerInList", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Begin Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdd_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim sm As RowSelectionModel = TryCast(Me.GridpanelView.GetSelectionModel(), RowSelectionModel)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SiPendarScreeningHistory_View_Init(sender As Object, e As EventArgs) Handles Me.Init
        'objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Nothing)
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Nothing) '' Edited 27-Sep-2021 Felix, biar ga error '' 25 Oct 2021 Btn Export Current Page biar muncul jadi set menjadi nothing
    End Sub

    Private Sub SiPendarScreeningHistory_View_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer
            Try
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)


                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                    Dim strIDCode As String = 1
                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If

                objFormModuleView.ModuleID = objmodule.PK_Module_ID
                objFormModuleView.ModuleName = objmodule.ModuleName

                'objFormModuleView.AddField("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.BIGIDENTITY)
                objFormModuleView.AddField("ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.VARCHARValue) ' Edited on 06 Sep 2021
                objFormModuleView.AddField("Nama", "Name Searched", 2, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("DOB", "DOB Searched", 3, False, True, NawaBLL.Common.MFieldType.DATETIMEValue, "dd-MMM-yyyy")
                objFormModuleView.AddField("Birth_Place", "Birth Place Searched", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE", "ID Type Searched", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Identity_Number", "ID Number Searched", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("FK_WATCHLIST_ID_PPATK", "ID PPATK", 7, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("FK_SIPENDAR_SUBMISSION_TYPE_CODE", "Submission Type", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Result", "Result", 9, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("GCN", "GCN/Unique Key", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("NAMACUSTOMER", "Name Found", 11, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("DOBCustomer", "DOB Found", 12, False, True, NawaBLL.Common.MFieldType.DATETIMEValue, "dd-MMM-yyyy")
                objFormModuleView.AddField("Birth_PlaceCustomer", "BirthPlace Found", 13, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer", "ID Type Found", 14, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Identity_NumberCustomer", "ID Number Found", 15, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Keterangan", "Keterangan", 16, False, True, NawaBLL.Common.MFieldType.VARCHARValue) ''Add 15-Nov-2021 Felix, display Keterangan dr Other_Info kalo ada
                objFormModuleView.AddField("TotalSimilarityPct", "Similarity (%)", 17, False, True, NawaBLL.Common.MFieldType.FLOATValue,,,,, 2)
                objFormModuleView.AddField("UserIDRequest", "Created By", 18, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("CreatedDate", "Created Date", 19, False, True, NawaBLL.Common.MFieldType.DATETIMEValue, "dd-MMM-yyyy hh:mm:ss")
                '' Add 27-Sep-2021 Felix, biar ga error
                objFormModuleView.AddField("ActionApproval", "Action Approval", 20, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Notes", "Notes", 21, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                '' End 27-Sep-2021 
                '' Add 13-Oct-2021 Felix, tambah Status Other Info dan LastUpdate
                objFormModuleView.AddField("StatusJudgement", "Status Judgement", 22, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("LastUpdateJudgement", "Last Update Judgement", 23, False, True, NawaBLL.Common.MFieldType.DATETIMEValue, "dd-MMM-yyyy hh:mm:ss")
                objFormModuleView.AddField("Stakeholder_Role", "Role (Non Customer)", 24, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("customerinformation", "Customer / NonCustomer", 25, False, True, NawaBLL.Common.MFieldType.VARCHARValue)

                objFormModuleView.SettingFormView()

                'Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
                'objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"

            Catch ex As Exception

            End Try

            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class
