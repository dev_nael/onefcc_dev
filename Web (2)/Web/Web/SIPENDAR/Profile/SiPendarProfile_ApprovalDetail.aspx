﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="SiPendarProfile_ApprovalDetail.aspx.vb" Inherits="SiPendarProfile_ApprovalDetail" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }
                wind
            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
    <style type="text/css">
        .text-wrapper .x-form-display-field {
    	    word-break: break-word;
    	    word-wrap: break-word;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="0" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="ColumnLayout" ButtonAlign="Center" AutoScroll="true" Title="Watch List Data">
        <Items>
            <ext:Panel runat="server" Title="Action" Collapsible="true" Layout="AnchorLayout" ColumnWidth="1" ID="pnlAction" Border="true" BodyStyle="padding:10px">
                <Content>
                    <ext:DisplayField ID="txt_ModuleApproval_ID" runat="server" FieldLabel="Approval ID" AnchorHorizontal="100%" Hidden="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_ModuleLabel" runat="server" FieldLabel="Module Label" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:DisplayField ID="txt_ModuleKey" runat="server" FieldLabel="Module Key" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:DisplayField ID="txt_CreatedBy" runat="server" FieldLabel="Created By" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:DisplayField ID="txt_CreatedDate" runat="server" FieldLabel="Created Date" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:DisplayField ID="txt_Action" runat="server" FieldLabel="Action" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:Panel runat="server" ID="PanelScreeningResult" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE"  Title="Screening Result" BodyStyle="padding:10px"  Collapsible="true" Hidden="true">
                        <Items>
                            <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true">
                                <Items>
                                    <ext:Checkbox ID="sar_IsSelectedAll_Result" runat="server" FieldLabel="Pilih Semua Result " LabelWidth="150" AnchorHorizontal="50%" Margin="10"/>
                                    <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export " labelStyle="width:80px" AnchorHorizontal="50%" MarginSpec="0 0 0 30" >
                                        <Items>
                                            <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                                            <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                                        </Items>
                                    </ext:ComboBox>
                                    <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" MarginSpec="0 0 0 10" >
                                        <DirectEvents>
                                            <Click OnEvent="ExportExcel" IsUpload="true">
                                                <ExtraParams>
                                                    <ext:Parameter Name="currentPage" Value="App.gp_ScreeningResult.getStore().currentPage" Mode="Raw" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page"   MarginSpec="0 0 0 10">
                                        <DirectEvents>
                                            <Click OnEvent="ExportAllExcel" IsUpload="true"/>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button runat="server" ID="BtnExportSelected" Text="Export Selected Row"  MarginSpec="0 0 0 10">
                                        <DirectEvents>
                                            <Click OnEvent="ExportSelectedExcel" IsUpload="true"/>
                                        </DirectEvents>
                                    </ext:Button>
                                </items>
                            </ext:Toolbar>
                            <ext:GridPanel ID="gp_ScreeningResult" runat="server" Border="true" MarginSpec="10 0" MinHeight="250" ClientIDMode="Static">
                                <View>
                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                </View>
                                <Store>
                                    <ext:Store ID="Store9" runat="server" IsPagingStore="true" PageSize="10">
                                        <Model>
                                            <ext:Model runat="server" ID="model10" IDProperty="PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID">
                                                <Fields>
                                                    <ext:ModelField Name="PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID" Type="String" ></ext:ModelField>
                                                    <ext:ModelField Name="Nama" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="DOB" Type="Date"></ext:ModelField>
                                                    <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="FK_WATCHLIST_ID_PPATK" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="GCN" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="NAMACUSTOMER" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="DOBCustomer" Type="Date"></ext:ModelField>
                                                    <ext:ModelField Name="Birth_PlaceCustomer" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Identity_NumberCustomer" Type="String"></ext:ModelField>
                                                    <%-- 14-Oct-2021 Daniel : Sementara d Comment karena belum terpakai --%>
                                                    <%--<ext:ModelField Name="_Similarity_Nama" Type="Float"></ext:ModelField>
                                                    <ext:ModelField Name="_Similarity_DOB" Type="Float"></ext:ModelField>--%>
                                                    <ext:ModelField Name="TotalSimilarityPct" Type="Float"></ext:ModelField>
                                                    <ext:ModelField Name="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Type="String"></ext:ModelField>
                                                    <%-- 19-Oct-2021 Daniel : Penambahan Stakeholder_Role dan CustomerInformation--%>
                                                    <ext:ModelField Name="Stakeholder_Role" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="customerinformation" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="ClosedDate" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                                    <%-- 14-Oct-2021 Daniel : Sementara d Comment karena belum terpakai --%>
                                                    <%--<ext:ModelField Name="IsClosed" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="ClosedDate" Type="String"></ext:ModelField>--%>
                                                    <ext:ModelField Name="Account_Name" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Account_No" Type="String"></ext:ModelField>
                                                     <ext:ModelField Name="CIF_No" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="FK_SIPENDAR_SUBMISSION_TYPE_CODE" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column46" runat="server" Text="Data Searched">
                                            <Columns>
                                                <ext:Column ID="Column47" runat="server" DataIndex="Nama" Text="Name" MinWidth="200"></ext:Column>
                                                <ext:DateColumn ID="DateColumn5" runat="server" DataIndex="DOB" Text="DOB" MinWidth="120" format="dd-MMM-yyyy"></ext:DateColumn>
                                                <ext:Column ID="Column48" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column55" runat="server" DataIndex="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Text="ID Type" MinWidth="100"></ext:Column>
                                                <ext:Column ID="Column49" runat="server" DataIndex="Identity_Number" Text="Identity Number" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column50" runat="server" DataIndex="FK_WATCHLIST_ID_PPATK" Text="ID Watchlist<br>PPATK" MinWidth="70"></ext:Column>
                                                <ext:Column ID="Column81" runat="server" DataIndex="FK_SIPENDAR_SUBMISSION_TYPE_CODE" Text="Submission Type" MinWidth="150" Flex="1"></ext:Column>
                                            </Columns>
                                        </ext:Column>

                                        <ext:Column ID="Column51" runat="server" Text="Data Found">
                                            <Columns>
                                                <ext:Column ID="Column52" runat="server" DataIndex="customerinformation" Text="Customer / Non-Customer" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column56" runat="server" DataIndex="CIF" Text="CIF" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column73" runat="server" DataIndex="CIF_No" Text="Link To CIF" Flex="1"></ext:Column>
                                                <ext:Column ID="Column72" runat="server" DataIndex="Account_No" Text="Link To Account" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column74" runat="server" DataIndex="Account_Name" Text="Link To Account Name" Flex="1"></ext:Column>
                                                <ext:Column ID="Column53" runat="server" DataIndex="GCN" Text="GCN / Unique Key" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column54" runat="server" DataIndex="NAMACUSTOMER" Text="Name" MinWidth="200"></ext:Column>
                                                <ext:DateColumn ID="DateColumn6" runat="server" DataIndex="DOBCustomer" Text="DOB" MinWidth="120" format="dd-MMM-yyyy"></ext:DateColumn>
                                                <ext:Column ID="Column57" runat="server" DataIndex="Birth_PlaceCustomer" Text="Birth Place" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column58" runat="server" DataIndex="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer" Text="ID Type" MinWidth="100"></ext:Column>
                                                <ext:Column ID="Column59" runat="server" DataIndex="Identity_NumberCustomer" Text="Identity Number" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column64" runat="server" DataIndex="Stakeholder_Role" Text="Non-Customer Role" MinWidth="150"></ext:Column>
                                                <%-- 1-Oct-2021 Adi : Sementara tidak ditampilkan di sini karena butuh waktu minimal 4 second untuk menampilkan ini per GCN --%>
                                                <%-- Test case bareng BSIM dgn data 4 juta customer dan 6 juta account --%>
                                                <%--<ext:Column ID="Column56" runat="server" DataIndex="IsClosed" Text="Customer Closed?" MinWidth="140"></ext:Column>
                                                <ext:Column ID="Column62" runat="server" DataIndex="ClosedDate" Text="Closing Date" MinWidth="100"></ext:Column>--%>
                                            </Columns>
                                        </ext:Column>

                                        <ext:NumberColumn ID="NumberColumn1" runat="server" DataIndex="TotalSimilarityPct" Text="Similarity" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                                        <ext:DateColumn ID="DateColumn7" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="200" format="dd-MMM-yyyy HH:mm:ss"></ext:DateColumn>
                                        <ext:CommandColumn ID="gp_ScreeningResult_CommandColumn" runat="server" Text="Action" CellWrap="true">
                                            <Commands>
                                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                                    <ToolTip Text="Detail Data"></ToolTip>
                                                </ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="gp_ScreeningResult_Gridcommand">
                                                <EventMask ShowMask="true"></EventMask>
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:CheckboxSelectionModel runat="server" Mode="Multi" CheckOnly="true">
                                        <DirectEvents>
                                            <SelectionChange OnEvent="sm_DownloadFromScreeningResult_change">
                                            </SelectionChange>
                                        </DirectEvents>
                                    </ext:CheckboxSelectionModel>
                                </SelectionModel>
                                <Plugins>
                                <%--<ext:FilterHeader runat="server"></ext:FilterHeader>--%>
                                </Plugins>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="false" >
                                        <Items>  
                                            <%-- <ext:Button runat="server" Text="Close Picker" ID="Button5">
                                            <Listeners>
                                            <Click Handler="#{window_SEARCH_CUSTOMER}.hide()"> </Click>
                                            </Listeners>
                                            </ext:Button>--%>
                                            <ext:Label runat="server" ID="LabelScreeningResultCount">

                                            </ext:Label>
                                        </Items>
                                    </ext:PagingToolbar>
                                </BottomBar>
                            </ext:GridPanel>
                            
                            <ext:GridPanel ID="gp_ScreeningResult_All" runat="server" Border="true" MarginSpec="10 0" Hidden="true" MinHeight="250">
                 
                                <View>
                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                </View>
                                <Store>
                                    <ext:Store ID="Store1All" runat="server" IsPagingStore="true" PageSize="10">
                                        <Model>
                                            <ext:Model runat="server" ID="model12" IDProperty="PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID">
                                                <Fields>
                                                    <ext:ModelField Name="PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID" Type="String" ></ext:ModelField>
                                                    <ext:ModelField Name="Nama" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="DOB" Type="Date"></ext:ModelField>
                                                    <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="FK_WATCHLIST_ID_PPATK" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="GCN" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="NAMACUSTOMER" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="DOBCustomer" Type="Date"></ext:ModelField>
                                                    <ext:ModelField Name="Birth_PlaceCustomer" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Identity_NumberCustomer" Type="String"></ext:ModelField>
                                                    <%-- 14-Oct-2021 Daniel : Sementara d Comment karena belum terpakai --%>
                                                    <%--<ext:ModelField Name="_Similarity_Nama" Type="Float"></ext:ModelField>
                                                    <ext:ModelField Name="_Similarity_DOB" Type="Float"></ext:ModelField>--%>
                                                    <ext:ModelField Name="TotalSimilarityPct" Type="Float"></ext:ModelField>
                                                    <ext:ModelField Name="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Type="String"></ext:ModelField>
                                                    <%-- 19-Oct-2021 Daniel : Penambahan Stakeholder_Role dan CustomerInformation--%>
                                                    <ext:ModelField Name="Stakeholder_Role" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="customerinformation" Type="String"></ext:ModelField>
                                                    <%-- 14-Oct-2021 Daniel : Penyetaraan dengan yang lain agar tidak ada dualisme --%>
                                                    <%--<ext:ModelField Name="IsHaveActiveAccount" Type="String"></ext:ModelField>--%>
                                                     <ext:ModelField Name="Account_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Account_No" Type="String"></ext:ModelField>
                                                             <ext:ModelField Name="CIF_No" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn13" runat="server" Text="No"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column65" runat="server" Text="Data Searched">
                                            <Columns>
                                                <ext:Column ID="Column66" runat="server" DataIndex="Nama" Text="Name" MinWidth="200"></ext:Column>
                                                <ext:DateColumn ID="DateColumn8" runat="server" DataIndex="DOB" Text="DOB" MinWidth="120" format="dd-MMM-yyyy"></ext:DateColumn>
                                                <ext:Column ID="Column67" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column68" runat="server" DataIndex="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Text="ID Type" MinWidth="100"></ext:Column>
                                                <ext:Column ID="Column76" runat="server" DataIndex="Identity_Number" Text="Identity Number" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column77" runat="server" DataIndex="FK_WATCHLIST_ID_PPATK" Text="ID Watchlist<br>PPATK" MinWidth="70"></ext:Column>
                                            </Columns>
                                        </ext:Column>

                                        <ext:Column ID="Column78" runat="server" Text="Data Found">
                                            <Columns>
                                                <ext:Column ID="Column79" runat="server" DataIndex="customerinformation" Text="Customer / Non-Customer" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column80" runat="server" DataIndex="CIF" Text="CIF" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column82" runat="server" DataIndex="CIF_No" Text="Link To CIF" Flex="1"></ext:Column>
                                                <ext:Column ID="Column83" runat="server" DataIndex="Account_No" Text="Link To Account" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column84" runat="server" DataIndex="Account_Name" Text="Link To Account Name" Flex="1"></ext:Column>
                                                <ext:Column ID="Column85" runat="server" DataIndex="GCN" Text="GCN / Unique Key" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column86" runat="server" DataIndex="NAMACUSTOMER" Text="Name" MinWidth="200"></ext:Column>
                                                <ext:DateColumn ID="DateColumn9" runat="server" DataIndex="DOBCustomer" Text="DOB" MinWidth="120" format="dd-MMM-yyyy"></ext:DateColumn>
                                                <ext:Column ID="Column87" runat="server" DataIndex="Birth_PlaceCustomer" Text="Birth Place" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column88" runat="server" DataIndex="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer" Text="ID Type" MinWidth="100"></ext:Column>
                                                <ext:Column ID="Column89" runat="server" DataIndex="Identity_NumberCustomer" Text="Identity Number" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column90" runat="server" DataIndex="Stakeholder_Role" Text="Non-Customer Role" MinWidth="150"></ext:Column>
                                                <%-- 1-Oct-2021 Adi : Sementara tidak ditampilkan di sini karena butuh waktu minimal 4 second untuk menampilkan ini per GCN --%>
                                                <%-- Test case bareng BSIM dgn data 4 juta customer dan 6 juta account --%>
                                                <%--<ext:Column ID="Column56" runat="server" DataIndex="IsClosed" Text="Customer Closed?" MinWidth="140"></ext:Column>
                                                <ext:Column ID="Column62" runat="server" DataIndex="ClosedDate" Text="Closing Date" MinWidth="100"></ext:Column>--%>
                                            </Columns>
                                        </ext:Column>

                                        <ext:NumberColumn ID="NumberColumn2" runat="server" DataIndex="TotalSimilarityPct" Text="Similarity" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                        
                                        <ext:CommandColumn ID="gp_ScreeningResult_All_CommandColumn" runat="server" Text="Action" CellWrap="true">
                                            <Commands>
                                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                                    <ToolTip Text="Detail Data"></ToolTip>
                                                </ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="gp_ScreeningResult_Gridcommand">
                                                    <EventMask ShowMask="true"></EventMask>
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
           
                                <Plugins>
                                    <%--<ext:FilterHeader runat="server"></ext:FilterHeader>--%>
                                </Plugins>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                        </Items>
                    </ext:Panel>

                </Content>
            </ext:Panel>
            <ext:Panel runat="server" Title="Data Before" Collapsible="true" Layout="AnchorLayout" ColumnWidth="0.475" ID="pnlDataBefore" Border="true" BodyStyle="padding:10px" >
                <Items>
         <%-- SiPendar Profile General Information_Before --%>
                    <ext:Panel runat="server" Title="General Information" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlGeneralInfo_Before" Border="true" BodyStyle="padding:10px">
                        <Content>

                            <NDS:NDSDropDownField ID="cmb_SIPENDAR_TYPE_ID_Before" LabelWidth="250" ValueField="PK_SIPENDAR_TYPE_id" DisplayField="SIPENDAR_TYPE_NAME" runat="server" StringField="PK_SIPENDAR_TYPE_id, SIPENDAR_TYPE_NAME" StringTable="SIPENDAR_TYPE" Label="SiPendar Report Type" AnchorHorizontal="80%" AllowBlank="false"/>
                            <NDS:NDSDropDownField ID="cmb_CUSTOMER_TYPE_Before" LabelWidth="250" ValueField="PK_Customer_Type_ID" DisplayField="Description" runat="server" StringField="PK_Customer_Type_ID, Description" StringTable="goAML_Ref_Customer_Type" Label="Customer Type" AnchorHorizontal="80%" AllowBlank="false"/>
                            <NDS:NDSDropDownField ID="cmb_JENIS_WATCHLIST_CODE_Before" LabelWidth="250" ValueField="SIPENDAR_JENIS_WATCHLIST_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_JENIS_WATCHLIST_CODE, KETERANGAN" StringTable="SIPENDAR_JENIS_WATCHLIST" Label="Watchlist Category" AnchorHorizontal="80%" AllowBlank="false"/>
                            <NDS:NDSDropDownField ID="cmb_TINDAK_PIDANA_CODE_Before" LabelWidth="250" ValueField="SIPENDAR_TINDAK_PIDANA_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_TINDAK_PIDANA_CODE, KETERANGAN" StringTable="SIPENDAR_TINDAK_PIDANA" Label="Jenis Tindak Pidana" AnchorHorizontal="80%" AllowBlank="false"/>
                            <NDS:NDSDropDownField ID="cmb_SUMBER_INFORMASI_KHUSUS_CODE_Before" LabelWidth="250" ValueField="SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE, KETERANGAN" StringTable="SIPENDAR_SUMBER_INFORMASI_KHUSUS" Label="Sumber Informasi Khusus" AnchorHorizontal="80%" AllowBlank="false"/>
                            <NDS:NDSDropDownField ID="cmb_ORGANISASI_CODE_Before" LabelWidth="250" ValueField="ORGANISASI_CODE" DisplayField="ORGANISASI_NAME" runat="server" StringField="ORGANISASI_CODE, ORGANISASI_NAME" StringTable="SIPENDAR_ORGANISASI" Label="SiPendar Organization Code" AnchorHorizontal="80%" AllowBlank="false"/>
                            <NDS:NDSDropDownField ID="cmb_SUMBER_TYPE_CODE_Before" LabelWidth="250" ValueField="SUMBER_TYPE_CODE" DisplayField="SUMBER_TYPE_NAME" runat="server" StringField="SUMBER_TYPE_CODE, SUMBER_TYPE_NAME" StringTable="SIPENDAR_SUMBER_TYPE" Label="Sumber Type" AnchorHorizontal="80%" AllowBlank="false" IsHidden="true"/>
                            <ext:NumberField ID="txt_SUMBER_ID_Before" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Sumber ID" AnchorHorizontal="80%" FormatText="#,##0" Hidden="true"></ext:NumberField>
                            <NDS:NDSDropDownField ID="cmb_SUBMISSION_TYPE_CODE_Before" LabelWidth="250" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="vw_SIPENDAR_SUBMISSION_TYPE" Label="Submission Type" AnchorHorizontal="80%" AllowBlank="false"/>
                            <ext:TextField ID="txt_GCN_Before" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="GCN / Unique Key" AnchorHorizontal="80%" MaxLength="50" EnforceMaxLength="true"></ext:TextField>
                            <ext:NumberField ID="txt_Similarity_Before" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Similarity (%)" AnchorHorizontal="80%" FormatText="#,##0.00" Hidden="true"></ext:NumberField>
                            <ext:TextField ID="txt_CustomerType_Before" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Customer / Non-Customer" AnchorHorizontal="80%"></ext:TextField>

                            <%-- Individual Information_Before --%>
                            <ext:FieldSet runat="server" ID="fs_Individual_Before" Collapsible="false" Title="Individual Information" Border="true" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="10 0" Padding="10">
                                <Content>
                                    <ext:TextField ID="txt_INDV_NAME_Before" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                                    <ext:TextField ID="txt_INDV_PLACEOFBIRTH_Before" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                                    <%--<ext:DateField ID="txt_INDV_DOB_Before" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="50%" Format="dd-MMM-yyyy" MaxLength="255"></ext:DateField>--%>
                                    <ext:TextField ID="txt_INDV_DOB_Before" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                                    <ext:TextArea ID="txt_INDV_ADDRESS_Before" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Address" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextArea>
                                    <NDS:NDSDropDownField ID="cmb_INDV_NATIONALITY_Before" LabelWidth="250" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Nationality" AnchorHorizontal="80%" AllowBlank="false"/>
                                    <ext:TextField ID="txt_role_noncustomer_Before" LabelWidth="250" runat="server" FieldLabel="Role (Non-Customer)" AnchorHorizontal="80%"></ext:TextField>
                                    <ext:FieldSet runat="server" Title ="*This Information From Ref Customer" Border="true" ID="fs_Individual_Before_IsHaveActiveAccount_Head">
                                        <Content>
                                            <ext:DisplayField runat="server" ID="fs_Individual_Before_IsHaveActiveAccount" FieldLabel="Customer Account Status" LabelWidth="175"></ext:DisplayField>
                                        </Content>
                                    </ext:FieldSet>
                                </Content>
                            </ext:FieldSet>
                            <%-- End of Individual Information_Before --%>

                            <%-- Corporate Information_Before --%>
                            <ext:FieldSet runat="server" ID="fs_Corporate_Before" Collapsible="false" Title="Corporate Information" Border="true" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="10 0" Padding="10" Hidden="true">
                                <Content>
                                    <ext:TextField ID="txt_CORP_NAME_Before" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Corporate Name" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                                    <ext:TextField ID="txt_CORP_NPWP_Before" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="NPWP" AnchorHorizontal="80%" MaxLength="15" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:TextField>
                                    <ext:TextField ID="txt_CORP_NO_IZIN_USAHA_Before" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Business License No." AnchorHorizontal="80%" MaxLength="50" EnforceMaxLength="true"></ext:TextField>
                                    <NDS:NDSDropDownField ID="cmb_CORP_COUNTRY_Before" LabelWidth="250" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Incorporation Country" AnchorHorizontal="80%" AllowBlank="false"/>
                                    <ext:FieldSet runat="server" Title ="*This Information From Ref Customer" Border="true" ID="fs_Corporate_Before_IsHaveActiveAccount_Head">
                                        <Content>
                                            <ext:DisplayField runat="server" ID="fs_Corporate_Before_IsHaveActiveAccount" FieldLabel="Customer Account Status" LabelWidth="175"></ext:DisplayField>
                                        </Content>
                                    </ext:FieldSet>
                                </Content>
                            </ext:FieldSet>
                            <%-- End of Corporate Information_Before --%>

                            <ext:TextArea ID="txt_Keterangan_Before" AllowBlank="true" LabelWidth="200" runat="server" FieldLabel="Keterangan" AnchorHorizontal="100%" MaxLength="4000" EnforceMaxLength="true"></ext:TextArea>

                        </Content>
                    </ext:Panel>
                    <%-- End of SiPendar Profile General Information_Before --%>


                    <%-- SiPendar Profile ACCOUNT_Before --%>
                    <ext:GridPanel ID="gp_SIPENDAR_PROFILE_ACCOUNT_Before" runat="server" Title="Account" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                        <Store>
                            <ext:Store ID="Store_SIPENDAR_PROFILE_ACCOUNT_Before" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model3" runat="server" IDProperty="PK_SIPENDAR_PROFILE_ACCOUNT_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_PROFILE_ACCOUNT_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIFNO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JENIS_REKENING" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="STATUS_REKENING" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NOREKENING" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="30"></ext:RowNumbererColumn>
                                <ext:Column ID="Column8" runat="server" DataIndex="CIFNO" Text="CIF No." MinWidth="150" Flex="1"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="JENIS_REKENING" Text="Account Type" MinWidth="170" Flex="1"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="STATUS_REKENING" Text="Status" MinWidth="100" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="NOREKENING" Text="Account No." MinWidth="150" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of SiPendar Profile ACCOUNT_Before --%>

                    <%-- SiPendar Profile Address_Before --%>
                    <ext:GridPanel ID="gp_SIPENDAR_PROFILE_ADDRESS_Before" runat="server" Title="Address" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                        <Store>
                            <ext:Store ID="Store3" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model4" runat="server" IDProperty="PK_SIPENDAR_PROFILE_ADDRESS_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_PROFILE_ADDRESS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TOWN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="STATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ZIP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="30"></ext:RowNumbererColumn>
                                <ext:Column ID="Column12" runat="server" DataIndex="ADDRESS_TYPE_NAME" Text="Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="ADDRESS" Text="Address" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="TOWN" Text="Town" MinWidth="100" Flex="1"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="CITY" Text="City" MinWidth="100" Flex="1"></ext:Column>
                                <ext:Column ID="Column25" runat="server" DataIndex="COUNTRY_NAME" Text="Country" Flex="1"></ext:Column>
                                <ext:Column ID="Column34" runat="server" DataIndex="STATE" Text="State" MinWidth="100" Flex="1"></ext:Column>
                                <ext:Column ID="Column35" runat="server" DataIndex="ZIP" Text="Zip" Flex="1"></ext:Column>
                                <ext:Column ID="Column38" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of SiPendar Profile Address_Before --%>

                    <%-- SiPendar Profile PHONE_Before --%>
                    <ext:GridPanel ID="gp_SIPENDAR_PROFILE_PHONE_Before" runat="server" Title="Phone" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                        <Store>
                            <ext:Store ID="Store4" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model5" runat="server" IDProperty="PK_SIPENDAR_PROFILE_PHONE_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_PROFILE_PHONE_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PHONE_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMUNICATION_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_PREFIX" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PHONE_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EXTENSION_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No" MinWidth="30"></ext:RowNumbererColumn>
                                <ext:Column ID="Column26" runat="server" DataIndex="PHONE_TYPE_NAME" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column27" runat="server" DataIndex="COMMUNICATION_TYPE_NAME" Text="Communication Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column28" runat="server" DataIndex="COUNTRY_PREFIX" Text="Country Prefix" Flex="1"></ext:Column>
                                <ext:Column ID="Column29" runat="server" DataIndex="PHONE_NUMBER" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:Column ID="Column42" runat="server" DataIndex="EXTENSION_NUMBER" Text="Extension" Flex="1"></ext:Column>
                                <ext:Column ID="Column43" runat="server" DataIndex="COMMENTS" Text="Comments" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of SiPendar Profile PHONE_Before --%>

                    <%-- SiPendar Profile IDENTIFICATION_Before --%>
                    <ext:GridPanel ID="gp_SIPENDAR_PROFILE_IDENTIFICATION_Before" runat="server" Title="Identification" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                        <Store>
                            <ext:Store ID="Store6" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model7" runat="server" IDProperty="PK_SIPENDAR_PROFILE_IDENTIFICATION_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_PROFILE_IDENTIFICATION_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDENTITY_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUE_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EXPIRED_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUED_BY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUED_COUNTRY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No" MinWidth="30"></ext:RowNumbererColumn>
                                <ext:Column ID="Column30" runat="server" DataIndex="Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Text="Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column31" runat="server" DataIndex="IDENTITY_NUMBER" Text="Identification No." MinWidth="200" Flex="1"></ext:Column>
                                <ext:DateColumn ID="DateColumn3" runat="server" DataIndex="ISSUE_DATE" Text="Issued" Format="dd-MMM-yyyy" Flex="1"></ext:DateColumn>
                                <ext:DateColumn ID="DateColumn4" runat="server" DataIndex="EXPIRED_DATE" Text="Expired" Format="dd-MMM-yyyy" Flex="1"></ext:DateColumn>
                                <ext:Column ID="Column32" runat="server" DataIndex="ISSUED_BY" Text="Issued By" Flex="1"></ext:Column>
                                <ext:Column ID="Column33" runat="server" DataIndex="ISSUED_COUNTRY" Text="Issued Country" Flex="1"></ext:Column>
                                <ext:Column ID="Column44" runat="server" DataIndex="Comments" Text="Comments" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of SiPendar Profile IDENTIFICATION_Before --%>
                </Items>
            </ext:Panel>
            <ext:Panel runat="server" Title="Data After" Collapsible="true" Layout="AnchorLayout" ColumnWidth="0.52" ID="pnlDataAfter" Border="true" BodyStyle="padding:10px">
                <Items>

                    <%--Add 29-Sep-2021 Felix--%>
                    <%--Grid Screening Result--%>
                   
                    <%--Grid Screening Result--%>
                    <%--End 29-Sep-2021--%>

                    <%-- 15-Sep-2021 Adi : SIPENDAR Screening Result based (jika FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID nya tidak NULL --%>
                    <ext:Panel runat="server" ID="pnlScreeningResult" Title="Watchlist Information" MarginSpec="0 0 10 0" Collapsible="true" Hidden="true" Border="true" Layout="ColumnLayout" BodyPadding="10">
                        <Items>
                            <ext:FieldSet runat="server" ColumnWidth="0.5" Border="false" Layout="AnchorLayout" Padding="0">
                                <Items>
                                    <ext:DisplayField runat="server" ID="txt_FK_WATCHLIST_ID_PPATK" AnchorHorizontal="100%" FieldLabel="ID Watchlist PPATK" LabelWidth="250"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_Name_Watchlist" AnchorHorizontal="100%" FieldLabel="Name" LabelWidth="250"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_DOB_Watchlist" AnchorHorizontal="100%" FieldLabel="Date of Birth" LabelWidth="250"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>
                            <ext:FieldSet runat="server" ColumnWidth="0.5" Border="false" Layout="AnchorLayout" Padding="0">
                                <Items>
                                    <ext:DisplayField runat="server" ID="txt_BirthPlace_Watchlist" AnchorHorizontal="100%" FieldLabel="Place of Birth" LabelWidth="250"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_IDNumber_Watchlist" AnchorHorizontal="100%" FieldLabel="Identity Number" LabelWidth="250"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_Similarity_Watchlist" AnchorHorizontal="100%" FieldLabel="Similarity" LabelWidth="250"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>
                        </Items>
                    </ext:Panel>
                    <%-- End of 15-Sep-2021 Adi : SIPENDAR Screening Result based (jika FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID nya tidak NULL --%>

                    <%-- SiPendar Profile General Information --%>
                    <ext:Panel runat="server" Title="General Information" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlGeneralInfo" Border="true" BodyStyle="padding:10px">
                        <Content>

                            <NDS:NDSDropDownField ID="cmb_SIPENDAR_TYPE_ID" LabelWidth="250" ValueField="PK_SIPENDAR_TYPE_id" DisplayField="SIPENDAR_TYPE_NAME" runat="server" StringField="PK_SIPENDAR_TYPE_id, SIPENDAR_TYPE_NAME" StringTable="SIPENDAR_TYPE" Label="SiPendar Report Type" AnchorHorizontal="80%" AllowBlank="false"/>
                            <NDS:NDSDropDownField ID="cmb_CUSTOMER_TYPE" LabelWidth="250" ValueField="PK_Customer_Type_ID" DisplayField="Description" runat="server" StringField="PK_Customer_Type_ID, Description" StringTable="goAML_Ref_Customer_Type" Label="Customer Type" AnchorHorizontal="80%" AllowBlank="false"/>
                            <NDS:NDSDropDownField ID="cmb_JENIS_WATCHLIST_CODE" LabelWidth="250" ValueField="SIPENDAR_JENIS_WATCHLIST_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_JENIS_WATCHLIST_CODE, KETERANGAN" StringTable="SIPENDAR_JENIS_WATCHLIST" Label="Watchlist Category" AnchorHorizontal="80%" AllowBlank="false"/>
                            <NDS:NDSDropDownField ID="cmb_TINDAK_PIDANA_CODE" LabelWidth="250" ValueField="SIPENDAR_TINDAK_PIDANA_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_TINDAK_PIDANA_CODE, KETERANGAN" StringTable="SIPENDAR_TINDAK_PIDANA" Label="Jenis Tindak Pidana" AnchorHorizontal="80%" AllowBlank="false"/>
                            <NDS:NDSDropDownField ID="cmb_SUMBER_INFORMASI_KHUSUS_CODE" LabelWidth="250" ValueField="SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE, KETERANGAN" StringTable="SIPENDAR_SUMBER_INFORMASI_KHUSUS" Label="Sumber Informasi Khusus" AnchorHorizontal="80%" AllowBlank="false"/>
                            <NDS:NDSDropDownField ID="cmb_ORGANISASI_CODE" LabelWidth="250" ValueField="ORGANISASI_CODE" DisplayField="ORGANISASI_NAME" runat="server" StringField="ORGANISASI_CODE, ORGANISASI_NAME" StringTable="SIPENDAR_ORGANISASI" Label="SiPendar Organization Code" AnchorHorizontal="80%" AllowBlank="false"/>
                            <NDS:NDSDropDownField ID="cmb_SUMBER_TYPE_CODE" LabelWidth="250" ValueField="SUMBER_TYPE_CODE" DisplayField="SUMBER_TYPE_NAME" runat="server" StringField="SUMBER_TYPE_CODE, SUMBER_TYPE_NAME" StringTable="SIPENDAR_SUMBER_TYPE" Label="Sumber Type" AnchorHorizontal="80%" AllowBlank="false" IsHidden="true"/>
                            <ext:NumberField ID="txt_SUMBER_ID" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Sumber ID" AnchorHorizontal="80%" FormatText="#,##0" Hidden="true"></ext:NumberField>
                            <NDS:NDSDropDownField ID="cmb_SUBMISSION_TYPE_CODE" LabelWidth="250" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="vw_SIPENDAR_SUBMISSION_TYPE" Label="Submission Type" AnchorHorizontal="80%" AllowBlank="false"/>
                            <ext:TextField ID="txt_GCN" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="GCN / Unique Key" AnchorHorizontal="80%" MaxLength="50" EnforceMaxLength="true"></ext:TextField>
                            <ext:NumberField ID="txt_Similarity" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Similarity (%)" AnchorHorizontal="80%" FormatText="#,##0.00" Hidden="true"></ext:NumberField>
                            <ext:TextField ID="txt_CustomerType" LabelWidth="250" runat="server" FieldLabel="Customer / Non-Customer" AnchorHorizontal="80%"></ext:TextField>

                            <%-- Individual Information --%>
                            <ext:FieldSet runat="server" ID="fs_Individual" Collapsible="false" Title="Individual Information" Border="true" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="10 0" Padding="10">
                                <Content>
                                    <ext:TextField ID="txt_INDV_NAME" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                                    <ext:TextField ID="txt_INDV_PLACEOFBIRTH" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                                    <%--<ext:DateField ID="txt_INDV_DOB" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="50%" Format="dd-MMM-yyyy"></ext:DateField>--%>
                                    <ext:TextField ID="txt_INDV_DOB" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                                    <ext:TextArea ID="txt_INDV_ADDRESS" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Address" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextArea>
                                    <NDS:NDSDropDownField ID="cmb_INDV_NATIONALITY" LabelWidth="250" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Nationality" AnchorHorizontal="80%" AllowBlank="false"/>
                                    <ext:TextField ID="txt_role_noncustomer" LabelWidth="250" runat="server" FieldLabel="Role (Non-Customer)" AnchorHorizontal="80%"></ext:TextField>
                                    <ext:FieldSet runat="server" Title ="*This Information From Ref Customer" Border="true" ID="fs_Individual_After_IsHaveActiveAccount_Head">
                                        <Content>
                                            <ext:DisplayField runat="server" ID="fs_Individual_After_IsHaveActiveAccount" FieldLabel="Customer Account Status" LabelWidth="175"></ext:DisplayField>
                                        </Content>
                                    </ext:FieldSet>
                                </Content>
                            </ext:FieldSet>
                            <%-- End of Individual Information --%>

                            <%-- Corporate Information --%>
                            <ext:FieldSet runat="server" ID="fs_Corporate" Collapsible="false" Title="Corporate Information" Border="true" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="10 0" Padding="10" Hidden="true">
                                <Content>
                                    <ext:TextField ID="txt_CORP_NAME" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Corporate Name" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                                    <ext:TextField ID="txt_CORP_NPWP" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="NPWP" AnchorHorizontal="80%" MaxLength="15" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:TextField>
                                    <ext:TextField ID="txt_CORP_NO_IZIN_USAHA" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Business License No." AnchorHorizontal="80%" MaxLength="50" EnforceMaxLength="true"></ext:TextField>
                                    <NDS:NDSDropDownField ID="cmb_CORP_COUNTRY" LabelWidth="250" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Incorporation Country" AnchorHorizontal="80%" AllowBlank="false"/>
                                    <ext:FieldSet runat="server" Title ="*This Information From Ref Customer" Border="true" ID="fs_Corporate_After_IsHaveActiveAccount_Head">
                                        <Content>
                                            <ext:DisplayField runat="server" ID="fs_Corporate_After_IsHaveActiveAccount" FieldLabel="Customer Account Status" LabelWidth="175"></ext:DisplayField>
                                        </Content>
                                    </ext:FieldSet>
                                </Content>
                            </ext:FieldSet>
                            <%-- End of Corporate Information --%>

                            <%-- 8-Jul-2021 Fitur Generate Transaction --%>
                            <ext:FieldSet runat="server" ID="fs_Generate_Transaction" Border="true" Title="Generate Transaction when Approved (Optional)" Hidden="false" Layout="AnchorLayout" Padding="10">
                                <Content>
                                    <ext:Checkbox ID="chk_IsGenerateTransaction" LabelWidth="250" runat="server" FieldLabel="Include Generate Transaction">
                                        <DirectEvents>
                                            <Change OnEvent="chk_IsGenerateTransaction_Change"></Change>
                                        </DirectEvents>
                                    </ext:Checkbox>
                                    <ext:DateField ID="txt_Transaction_DateFrom" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Transaction Date From" AnchorHorizontal="50%" Format="dd-MMM-yyyy" Hidden="true"></ext:DateField>
                                    <ext:DateField ID="txt_Transaction_DateTo" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Transaction Date To" AnchorHorizontal="50%" Format="dd-MMM-yyyy" Hidden="true"></ext:DateField>
                                    <%--Added by Felix on 14-Sep-2021--%>
                                    <ext:Panel runat="server" ID="PanelReportIndicator" Hidden="true" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Indikator" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelReportIndicator" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                               <%-- <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                            <ext:Button runat="server" ID="BtnAddReportIndikator" Text="Tambah Indicator" Icon="Add">
                                                                <DirectEvents>
                                                                    <Click OnEvent="btn_addIndicator_click">
                                                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="500"></EventMask>
                                                                    </Click>
                                                                </DirectEvents>
                                                            </ext:Button>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>--%>
                                                <Store>
                                                    <ext:Store ID="StoreReportIndicatorData" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" ID="Model355" IDProperty="PK_Report_Indicator">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_Report_Indicator" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="FK_Indicator" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn88" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column312" runat="server" DataIndex="FK_Indicator" Text="Kode" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column313" runat="server" DataIndex="Keterangan" Text="Keterangan" Flex="1"></ext:Column>
                                                        <%--<ext:CommandColumn ID="CommandColumn87" runat="server" Text="Action" Flex="1">

                                                            <Commands>
                                                                <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                                                    <ToolTip Text="Edit"></ToolTip>
                                                                </ext:GridCommand>
                                                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                                                    <ToolTip Text="Detail"></ToolTip>
                                                                </ext:GridCommand>
                                                                <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationViewDetail" MinWidth="70">
                                                                    <ToolTip Text="Delete"></ToolTip>
                                                                </ext:GridCommand>
                                                            </Commands>

                                                            <DirectEvents>

                                                                <Command OnEvent="GridcommandReportIndicator">
                                                                    <EventMask ShowMask="true"></EventMask>
                                                                    <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>

                                                                    <ExtraParams>
                                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Report_Indicator" Mode="Raw"></ext:Parameter>
                                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                    </ExtraParams>
                                                                </Command>
                                                            </DirectEvents>
                                                        </ext:CommandColumn>--%>



                                                    </Columns>
                                                </ColumnModel>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>
                                <%--End of 14-Sep-2021--%>
                                </Content>
                            </ext:FieldSet>
                            <%-- End of 8-Jul-2021 Fitur Generate Transaction --%>

                            <ext:TextArea ID="txt_Keterangan" AllowBlank="true" LabelWidth="200" runat="server" FieldLabel="Keterangan" AnchorHorizontal="100%" MaxLength="4000" EnforceMaxLength="true"></ext:TextArea>

                        </Content>
                    </ext:Panel>
                    <%-- End of SiPendar Profile General Information --%>


                    <%-- SiPendar Profile ACCOUNT --%>
                    <ext:GridPanel ID="gp_SIPENDAR_PROFILE_ACCOUNT" runat="server" Title="Account" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                        <Store>
                            <ext:Store ID="Store_SIPENDAR_PROFILE_ACCOUNT" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model29" runat="server" IDProperty="PK_SIPENDAR_PROFILE_ACCOUNT_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_PROFILE_ACCOUNT_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIFNO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JENIS_REKENING" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="STATUS_REKENING" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NOREKENING" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No" MinWidth="30"></ext:RowNumbererColumn>
                                <ext:Column ID="Column75" runat="server" DataIndex="CIFNO" Text="CIF No." MinWidth="150" Flex="1"></ext:Column>
                                <ext:Column ID="Column1" runat="server" DataIndex="JENIS_REKENING" Text="Account Type" MinWidth="170" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="STATUS_REKENING" Text="Status" MinWidth="100" Flex="1"></ext:Column>
                                <ext:Column ID="Column3" runat="server" DataIndex="NOREKENING" Text="Account No." MinWidth="150" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of SiPendar Profile ACCOUNT --%>

                    <%-- SiPendar Profile Address --%>
                    <ext:GridPanel ID="gp_SIPENDAR_PROFILE_ADDRESS" runat="server" Title="Address" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                        <Store>
                            <ext:Store ID="Store1" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="PK_SIPENDAR_PROFILE_ADDRESS_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_PROFILE_ADDRESS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TOWN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="STATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ZIP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="30"></ext:RowNumbererColumn>
                                <ext:Column ID="Column5" runat="server" DataIndex="ADDRESS_TYPE_NAME" Text="Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="ADDRESS" Text="Address" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="TOWN" Text="Town" MinWidth="100" Flex="1"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="CITY" Text="City" MinWidth="100" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="COUNTRY_NAME" Text="Country" Flex="1"></ext:Column>
                                <ext:Column ID="Column36" runat="server" DataIndex="STATE" Text="State" MinWidth="100" Flex="1"></ext:Column>
                                <ext:Column ID="Column37" runat="server" DataIndex="ZIP" Text="Zip" Flex="1"></ext:Column>
                                <ext:Column ID="Column39" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of SiPendar Profile Address --%>

                    <%-- SiPendar Profile PHONE --%>
                    <ext:GridPanel ID="gp_SIPENDAR_PROFILE_PHONE" runat="server" Title="Phone" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                        <Store>
                            <ext:Store ID="Store2" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model2" runat="server" IDProperty="PK_SIPENDAR_PROFILE_PHONE_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_PROFILE_PHONE_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PHONE_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMUNICATION_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_PREFIX" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PHONE_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EXTENSION_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="30"></ext:RowNumbererColumn>
                                <ext:Column ID="Column17" runat="server" DataIndex="PHONE_TYPE_NAME" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="COMMUNICATION_TYPE_NAME" Text="Communication Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="COUNTRY_PREFIX" Text="Country Prefix" Flex="1"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="PHONE_NUMBER" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:Column ID="Column40" runat="server" DataIndex="EXTENSION_NUMBER" Text="Extension" Flex="1"></ext:Column>
                                <ext:Column ID="Column41" runat="server" DataIndex="COMMENTS" Text="Comments" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of SiPendar Profile PHONE --%>

                    <%-- SiPendar Profile IDENTIFICATION --%>
                    <ext:GridPanel ID="gp_SIPENDAR_PROFILE_IDENTIFICATION" runat="server" Title="Identification" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                        <Store>
                            <ext:Store ID="Store5" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model6" runat="server" IDProperty="PK_SIPENDAR_PROFILE_IDENTIFICATION_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_PROFILE_IDENTIFICATION_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDENTITY_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUE_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EXPIRED_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUED_BY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUED_COUNTRY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="30"></ext:RowNumbererColumn>
                                <ext:Column ID="Column21" runat="server" DataIndex="Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Text="Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="IDENTITY_NUMBER" Text="Identification No." MinWidth="200" Flex="1"></ext:Column>
                                <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="ISSUE_DATE" Text="Issued" Format="dd-MMM-yyyy" Flex="1"></ext:DateColumn>
                                <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="EXPIRED_DATE" Text="Expired" Format="dd-MMM-yyyy" Flex="1"></ext:DateColumn>
                                <ext:Column ID="Column23" runat="server" DataIndex="ISSUED_BY" Text="Issued By" Flex="1"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="ISSUED_COUNTRY" Text="Issued Country" Flex="1"></ext:Column>
                                <ext:Column ID="Column45" runat="server" DataIndex="Comments" Text="Comments" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of SiPendar Profile IDENTIFICATION --%>
                </Items>
            </ext:Panel>
            <%-- Update 20210924 Felix : Penambahan Approval History and Note --%>
        <%--<ext:Panel runat="server" Title="Data After" Collapsible="true" Layout="AnchorLayout" ColumnWidth="0.5" ID="pnlDataAfter" Border="true" BodyStyle="padding:10px"> --%>
            <ext:Panel runat="server" ID="panelApprovalHistory" Title="Approval" Layout="AnchorLayout" ColumnWidth="1" Border="true" Collapsible="true" BodyStyle="padding:10px" Hidden="true">
                <Items>
                    <ext:GridPanel ID="gp_history" runat="server" MarginSpec="0 0 20 0" Title="Approval History">
                        <Store>
                            <ext:Store ID="gpStore_history" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" IDProperty="PK_TX_IFTI_Flow_History_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_TX_IFTI_Flow_History_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Module_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UnikID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="UserID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UserName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Notes" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>

                        <ColumnModel runat="server">
                            <Columns>
                                <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="70"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="User Name" DataIndex="UserName" CellWrap="true" flex="1" />
                                <ext:Column runat="server" Text="Status" DataIndex="Status" CellWrap="true" flex="1" />
                                <ext:DateColumn runat="server" Text="Created Date" DataIndex="CreatedDate" CellWrap="true" flex="1" Format="dd-MMM-yyyy hh:mm:ss" />
                                <ext:Column runat="server" Text="Notes" DataIndex="Notes" CellWrap="true" flex="4" />
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    
                    <ext:TextArea ID="txtApprovalNotes" runat="server" FieldLabel="Approval/Rejection Note" AnchorHorizontal="100%" AllowBlank="false"/>

                </Items>
            </ext:Panel>
            <%-- End of Update 20210924 Felix : Penambahan Approval History and Note --%>
        </Items>

        <Buttons>
            <ext:Button ID="btn_SiPendarProfile_Approve" runat="server" Icon="Accept" Text="Approve">
                <DirectEvents>
                    <Click OnEvent="btn_SiPendarProfile_Approve_Click">
                        <EventMask ShowMask="true" Msg="Approve Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_SiPendarProfile_Reject" runat="server" Icon="Decline" Text="Reject">
                <DirectEvents>
                    <Click OnEvent="btn_SiPendarProfile_Reject_Click">
                        <Confirmation Message="Are you sure to Reject this Data ?" ConfirmRequest="true" Title="Delete"></Confirmation>
                        <EventMask ShowMask="true" Msg="Reject Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_SiPendarProfile_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_SiPendarProfile_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <%--Add 29-Sep-2021 Felix--%>
    <ext:Window runat="server" ID="WindowScreeningDetail" Title="Screening Detail" Modal="true" Layout="ColumnLayout" ButtonAlign="Center" Hidden="true" AutoScroll="true" Scrollable="Vertical" Maximizable="true">
        <Items>
            <ext:FieldSet runat="server" ID="FieldSet0" Border="false" Layout="AnchorLayout" ColumnWidth="1" Padding="10" MarginSpec="10 0">
                <Content>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_Similarity" FieldLabel="Similarity" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_CustomerType" FieldLabel="Customer / Non-Customer" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_IsHaveActiveAccount" FieldLabel="Customer Account Status" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_SubmissionType" FieldLabel="Submission Type" LabelWidth="175"></ext:DisplayField> <%--Add 20-Dec-2021--%>
                </Content>
            </ext:FieldSet>
            <ext:FieldSet runat="server" ID="FieldSet1" Border="true" Layout="AnchorLayout" ColumnWidth="0.45" Padding="10" MarginSpec="0 0 0 10" AutoScroll="true" Title="Search Data">
                <Content>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_IDWatchlistPPATK" FieldLabel="ID Watch List PPATK" LabelWidth="175" ></ext:DisplayField>                    
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_NameSearch" FieldLabel="Name Search" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_DOBSearch" FieldLabel="Birth Date Search" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_POBSearch" FieldLabel="Birth Place Search" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_IdentityTypeSearch" FieldLabel="Identity Type Search" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_IdentityNumberSearch" FieldLabel="Identity Number Search" LabelWidth="175" ></ext:DisplayField>

                    <%-- 29-Sep-2021 Adi : Penambahan list of Identity Watchlist --%>
                    <ext:GridPanel ID="gp_IdentityWatchlist" runat="server" EmptyText="No Available Data" Hidden="true" Title="List of Identity">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store7" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model8">
                                        <Fields>
                                            <ext:ModelField Name="Identity_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column60" runat="server" DataIndex="Identity_Type" Text="ID Type" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column61" runat="server" DataIndex="Identity_Number" Text="ID Number" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                    <%-- End of 29-Sep-2021 Adi : Penambahan list of Identity Watchlist --%>
                </Content>
            </ext:FieldSet>
            <ext:FieldSet runat="server" ID="FieldSet2" Border="true" Layout="AnchorLayout" ColumnWidth="0.5" Padding="10" MarginSpec="0 10 0 10" AutoScroll="true" Title="Customer Data">
                <Content>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_GCN" FieldLabel="GCN" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_CustomerName" FieldLabel="Name" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_CustomerDOB" FieldLabel="Birth Date" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_CustomerPOB" FieldLabel="Birth Place" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_CustomerIdentityType" FieldLabel="Identity Type" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_CustomerIdentityNumber" FieldLabel="Identity Number" LabelWidth="175" ></ext:DisplayField>

                    <%-- 29-Sep-2021 Adi : Penambahan list of Identity Customer --%>
                    <ext:GridPanel ID="gp_IdentityCustomer" runat="server" EmptyText="No Available Data" Hidden="true" Title="List of Identity">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store8" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model9">
                                        <Fields>
                                            <ext:ModelField Name="Identity_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column62" runat="server" DataIndex="Identity_Type" Text="ID Type" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column63" runat="server" DataIndex="Identity_Number" Text="ID Number" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                     <%-- 18-Nov-2021 Ari : Penambahan list of Link CIF --%>
                    <ext:GridPanel ID="GridPanelLinkCIF" runat="server" EmptyText="No Available Data" Hidden="true" Title="Link To CIF / Account">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store10" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model11">
                                        <Fields>
                                            <ext:ModelField Name="Account_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Account_No" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="CIF_No" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column69" runat="server" DataIndex="Account_No" Text="Account No" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column70" runat="server" DataIndex="CIF_No" Text="CIF No" Flex="1"></ext:Column>
                                 <ext:Column ID="Column71" runat="server" DataIndex="Account_Name" Text="Account Name" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- End of 29-Sep-2021 Adi : Penambahan list of Identity Customer --%>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_EmptySpace" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_CIF" FieldLabel="CIF" LabelWidth="175" Cls="text-wrapper"></ext:DisplayField>
                </Content>
            </ext:FieldSet>
        </Items>
        <Buttons>
            <ext:Button runat="server" ID="WindowScreeningDetail_Close" Text="Close" Icon="Cancel" >
                <DirectEvents>
                    <Click OnEvent="WindowScreeningDetail_Close_Click">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="100"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.8, height: size.height * 0.8});" />
            <Resize Handler="#{WindowScreeningDetail}.center()" />
        </Listeners>
    </ext:Window>
    <%--End 29-Sep-2021 Felix--%>
</asp:Content>

