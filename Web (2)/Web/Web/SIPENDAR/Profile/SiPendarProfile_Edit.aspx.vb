﻿Imports Ext
Imports Elmah
Imports System.Data
Imports SiPendarBLL
Imports SiPendarDAL
Imports System.Data.SqlClient

Partial Class SiPendarProfile_Edit
    Inherits ParentPage

    Public Property IDModule() As String
        Get
            Return Session("SiPendarProfile_Edit.IDModule")
        End Get
        Set(ByVal value As String)
            Session("SiPendarProfile_Edit.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("SiPendarProfile_Edit.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("SiPendarProfile_Edit.IDUnik") = value
        End Set
    End Property

    'Session untuk menyimpan Account PK ID untuk Account ATM
    Public Property IDAccount() As Long
        Get
            Return Session("SiPendarProfile_Edit.IDAccount")
        End Get
        Set(ByVal value As Long)
            Session("SiPendarProfile_Edit.IDAccount") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_CLASS() As SIPENDAR_PROFILE_CLASS
        Get
            Return Session("SiPendarProfile_Edit.objSIPENDAR_PROFILE_CLASS")
        End Get
        Set(ByVal value As SIPENDAR_PROFILE_CLASS)
            Session("SiPendarProfile_Edit.objSIPENDAR_PROFILE_CLASS") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_ACCOUNT_Edit() As SIPENDAR_PROFILE_ACCOUNT
        Get
            Return Session("SiPendarProfile_Edit.objSIPENDAR_PROFILE_ACCOUNT_Edit")
        End Get
        Set(ByVal value As SIPENDAR_PROFILE_ACCOUNT)
            Session("SiPendarProfile_Edit.objSIPENDAR_PROFILE_ACCOUNT_Edit") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit() As SIPENDAR_PROFILE_ACCOUNT_ATM
        Get
            Return Session("SiPendarProfile_Edit.objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit")
        End Get
        Set(ByVal value As SIPENDAR_PROFILE_ACCOUNT_ATM)
            Session("SiPendarProfile_Edit.objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_ADDRESS_Edit() As SIPENDAR_PROFILE_ADDRESS
        Get
            Return Session("SiPendarProfile_Edit.objSIPENDAR_PROFILE_ADDRESS_Edit")
        End Get
        Set(ByVal value As SIPENDAR_PROFILE_ADDRESS)
            Session("SiPendarProfile_Edit.objSIPENDAR_PROFILE_ADDRESS_Edit") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_PHONE_Edit() As SIPENDAR_PROFILE_PHONE
        Get
            Return Session("SiPendarProfile_Edit.objSIPENDAR_PROFILE_PHONE_Edit")
        End Get
        Set(ByVal value As SIPENDAR_PROFILE_PHONE)
            Session("SiPendarProfile_Edit.objSIPENDAR_PROFILE_PHONE_Edit") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_IDENTIFICATION_Edit() As SIPENDAR_PROFILE_IDENTIFICATION
        Get
            Return Session("SiPendarProfile_Edit.objSIPENDAR_PROFILE_IDENTIFICATION_Edit")
        End Get
        Set(ByVal value As SIPENDAR_PROFILE_IDENTIFICATION)
            Session("SiPendarProfile_Edit.objSIPENDAR_PROFILE_IDENTIFICATION_Edit") = value
        End Set
    End Property

    Sub ClearSession()
        objSIPENDAR_PROFILE_CLASS = New SIPENDAR_PROFILE_CLASS

        objSIPENDAR_PROFILE_ACCOUNT_Edit = Nothing
        objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit = Nothing
        objSIPENDAR_PROFILE_ADDRESS_Edit = Nothing
        objSIPENDAR_PROFILE_PHONE_Edit = Nothing
        objSIPENDAR_PROFILE_IDENTIFICATION_Edit = Nothing

        IDAccount = 0
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As Integer = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            If Not Ext.Net.X.IsAjaxRequest Then
                'Tampilkan notifikasi jika ada di bucket Pending Approval
                If SiPendarProfile_BLL.IsExistsInApproval(ObjModule.ModuleName, IDUnik) Then
                    LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."

                    Panelconfirmation.Hidden = False
                    FormPanelInput.Hidden = True
                End If

                ClearSession()



                FormPanelInput.Title = ObjModule.ModuleLabel & " - Edit"

                SetCommandColumnLocation()

                'Default Customer Type to Individual
                cmb_SIPENDAR_TYPE_ID.SetTextValue("")
                SetDefaultValue()

                'Load Data
                LoadSiPendarProfile()

                'Set Control Visibility
                SetControlVisibility()
                cmb_SIPENDAR_TYPE_ID.IsReadOnly = True
                'Set command column location (Left/Right)

                ' 22 Nov 2021 Ari show result link to cif
                'Show Screening Result
                Show_ScreeningResult()

                ' 21 Dec 2021 Ari penambahan kondisi jika dia pengayaan submission type nya di hide
                If cmb_SIPENDAR_TYPE_ID.TextValue = 1 Then
                    cmb_SUBMISSION_TYPE_CODE.IsReadOnly = False
                Else
                    cmb_SUBMISSION_TYPE_CODE.IsReadOnly = True
                End If

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub SetDefaultValue()
        Try
            Using objNawa As New SiPendarDAL.SiPendarEntities
                Dim objCustomerType = objNawa.goAML_Ref_Customer_Type.OrderBy(Function(x) x.PK_Customer_Type_ID).FirstOrDefault
                If Not objCustomerType Is Nothing Then
                    cmb_CUSTOMER_TYPE.SetTextWithTextValue(objCustomerType.PK_Customer_Type_ID, objCustomerType.Description)
                End If
            End Using

            Using objSipendar As New SiPendarDAL.SiPendarEntities
                Dim objGlobalParameter = objSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault
                If Not objGlobalParameter Is Nothing Then
                    Dim objOrganisasi = objSipendar.SIPENDAR_ORGANISASI.Where(Function(x) x.ORGANISASI_CODE = objGlobalParameter.ParameterValue).FirstOrDefault
                    If objOrganisasi IsNot Nothing Then
                        cmb_ORGANISASI_CODE.SetTextWithTextValue(objOrganisasi.ORGANISASI_CODE, objOrganisasi.ORGANISASI_NAME)
                    End If
                End If

                Dim objSIPENDARType = objSipendar.SIPENDAR_TYPE.OrderBy(Function(x) x.PK_SIPENDAR_TYPE_id).FirstOrDefault
                If Not objSIPENDARType Is Nothing Then
                    cmb_SIPENDAR_TYPE_ID.SetTextWithTextValue(objSIPENDARType.PK_SIPENDAR_TYPE_id, objSIPENDARType.SIPENDAR_TYPE_NAME)
                End If
            End Using

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub cmb_CUSTOMER_TYPE_Change(sender As Object, e As DirectEventArgs)
        Try
            fs_Individual.Hidden = True
            fs_Corporate.Hidden = True

            If cmb_CUSTOMER_TYPE.SelectedItemValue = 1 Then
                fs_Individual.Hidden = False
                gp_SIPENDAR_PROFILE_PHONE.Hidden = False
                gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = False
            ElseIf cmb_CUSTOMER_TYPE.SelectedItemValue = 2 Then
                fs_Corporate.Hidden = False
                gp_SIPENDAR_PROFILE_PHONE.Hidden = True
                gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = True
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    '------- LOAD PROFILE
    Protected Sub LoadSiPendarProfile()
        Try
            'Load Watchlist
            objSIPENDAR_PROFILE_CLASS = SiPendarProfile_BLL.GetSiPendarProfileClassByID(IDUnik)

            Using objDB As New SiPendarDAL.SiPendarEntities
                'Bind Data Header
                With objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE
                    'Error Message (if Any)
                    If Not String.IsNullOrEmpty(.ErrorMessage) Then
                        SiPendarBLL.NawaFramework.extInfoPanelupdate(FormPanelInput, .ErrorMessage, "infoValidationResultPanel")
                        FormPanelInput.Body.ScrollTo(Direction.Top, 0)
                        infoValidationResultPanel.Hidden = False
                        'Exit Sub
                    End If

                    'General Info
                    If Not IsNothing(.FK_SIPENDAR_TYPE_ID) Then
                        Dim objSiPendarType = objDB.SIPENDAR_TYPE.Where(Function(x) x.PK_SIPENDAR_TYPE_id = .FK_SIPENDAR_TYPE_ID).FirstOrDefault
                        If objSiPendarType IsNot Nothing Then
                            cmb_SIPENDAR_TYPE_ID.SetTextWithTextValue(objSiPendarType.PK_SIPENDAR_TYPE_id, objSiPendarType.SIPENDAR_TYPE_NAME)
                        End If
                    End If
                    If Not IsNothing(.Fk_goAML_Ref_Customer_Type_id) Then
                        Dim objCustomerType = SiPendarProfile_BLL.GetCustomerTypeByID(.Fk_goAML_Ref_Customer_Type_id)
                        If objCustomerType IsNot Nothing Then
                            cmb_CUSTOMER_TYPE.SetTextWithTextValue(objCustomerType.PK_Customer_Type_ID, objCustomerType.Description)
                        End If
                    End If

                    'Tambahan 8-Jul-2021
                    If Not IsNothing(.FK_SIPENDAR_SUBMISSION_TYPE_CODE) Then
                        Dim objSubmissionType = objDB.SIPENDAR_SUBMISSION_TYPE.Where(Function(x) x.Kode = .FK_SIPENDAR_SUBMISSION_TYPE_CODE).FirstOrDefault
                        If objSubmissionType IsNot Nothing Then
                            cmb_SUBMISSION_TYPE_CODE.SetTextWithTextValue(objSubmissionType.Kode, objSubmissionType.Keterangan)
                        End If
                    End If
                    'End of Tambahan 8-Jul-2021

                    'SIPENDAR Proaktif
                    If Not IsNothing(.FK_SIPENDAR_JENIS_WATCHLIST_CODE) Then
                        Dim objJenisWatchlist = objDB.SIPENDAR_JENIS_WATCHLIST.Where(Function(x) x.SIPENDAR_JENIS_WATCHLIST_CODE = .FK_SIPENDAR_JENIS_WATCHLIST_CODE).FirstOrDefault
                        If objJenisWatchlist IsNot Nothing Then
                            cmb_JENIS_WATCHLIST_CODE.SetTextWithTextValue(objJenisWatchlist.SIPENDAR_JENIS_WATCHLIST_CODE, objJenisWatchlist.KETERANGAN)
                        End If
                    End If
                    If Not IsNothing(.FK_SIPENDAR_TINDAK_PIDANA_CODE) Then
                        Dim objTindakPidana = objDB.SiPendar_Tindak_Pidana.Where(Function(x) x.SiPendar_Tindak_Pidana_Code = .FK_SIPENDAR_TINDAK_PIDANA_CODE).FirstOrDefault
                        If objTindakPidana IsNot Nothing Then
                            cmb_TINDAK_PIDANA_CODE.SetTextWithTextValue(objTindakPidana.SiPendar_Tindak_Pidana_Code, objTindakPidana.Keterangan)
                        End If
                    End If
                    If Not IsNothing(.FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE) Then
                        Dim objSumberInformasiKhusus = objDB.SiPendar_Sumber_Informasi_Khusus.Where(Function(x) x.SiPendar_Sumber_Informasi_Khusus_Code = .FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE).FirstOrDefault
                        If objSumberInformasiKhusus IsNot Nothing Then
                            cmb_SUMBER_INFORMASI_KHUSUS_CODE.SetTextWithTextValue(objSumberInformasiKhusus.SiPendar_Sumber_Informasi_Khusus_Code, objSumberInformasiKhusus.Keterangan)
                        End If
                    End If
                    If Not IsNothing(.FK_SIPENDAR_ORGANISASI_CODE) Then
                        Dim objSiPendarOrganisasi = objDB.SIPENDAR_ORGANISASI.Where(Function(x) x.ORGANISASI_CODE = .FK_SIPENDAR_ORGANISASI_CODE).FirstOrDefault
                        If objSiPendarOrganisasi IsNot Nothing Then
                            cmb_ORGANISASI_CODE.SetTextWithTextValue(objSiPendarOrganisasi.ORGANISASI_CODE, objSiPendarOrganisasi.ORGANISASI_NAME)
                        End If
                    End If

                    'SIPENDAR PENGAYAAN
                    If Not IsNothing(.FK_SIPENDAR_SUMBER_TYPE_CODE) Then
                        Dim objSiPendarSumberType = objDB.SIPENDAR_SUMBER_TYPE.Where(Function(x) x.SUMBER_TYPE_CODE = .FK_SIPENDAR_SUMBER_TYPE_CODE).FirstOrDefault
                        If objSiPendarSumberType IsNot Nothing Then
                            cmb_SUMBER_TYPE_CODE.SetTextWithTextValue(objSiPendarSumberType.SUMBER_TYPE_CODE, objSiPendarSumberType.SUMBER_TYPE_NAME)
                        End If
                    End If
                    txt_SUMBER_ID.Value = .SUMBER_ID
                    txt_Similarity.Value = .Similarity
                    ' 22 Nov 2021 Ari tambah session untuk tampung fk sipendar result dan status
                    Session("SipendarOtherInfoStatus") = .Status
                    Session("FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID") = .FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
                    txt_GCN.Value = .GCN
                    txt_Keterangan.Value = .Keterangan
                    If .FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID IsNot Nothing Then
                        Dim dtResult As DataTable


                        If .Status IsNot Nothing Then
                            Dim paramLoadResultGroup(0) As SqlParameter

                            paramLoadResultGroup(0) = New SqlParameter
                            paramLoadResultGroup(0).ParameterName = "@PK"
                            paramLoadResultGroup(0).Value = .FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
                            paramLoadResultGroup(0).DbType = SqlDbType.VarChar
                            Dim data As New DataTable
                            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_GROUP_RESULT_BY_PK_With_Status_Not_Null", paramLoadResultGroup)

                            dtResult = data
                        Else
                            Dim paramLoadResultGroup(0) As SqlParameter

                            paramLoadResultGroup(0) = New SqlParameter
                            paramLoadResultGroup(0).ParameterName = "@PK"
                            paramLoadResultGroup(0).Value = .FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
                            paramLoadResultGroup(0).DbType = SqlDbType.VarChar
                            Dim data As New DataTable
                            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_GROUP_RESULT_BY_PK", paramLoadResultGroup)
                            dtResult = data
                        End If
                        If dtResult IsNot Nothing AndAlso dtResult.Rows.Count > 0 Then
                            Dim drResult As DataRow = dtResult.Rows(0)
                            If drResult IsNot Nothing Then
                                If IsDBNull(drResult("Stakeholder_Role")) Then
                                    txt_CustomerType.Text = "Customer"
                                    txt_role_noncustomer.Hidden = True
                                Else
                                    txt_CustomerType.Text = "Non-Customer"
                                    txt_role_noncustomer.Text = drResult("Stakeholder_Role")
                                    txt_role_noncustomer.Hidden = False
                                    fs_Individual_IsHaveActiveAccount_Head.Hidden = True
                                    fs_Corporate_IsHaveActiveAccount_Head.Hidden = True
                                End If
                            End If
                        End If
                    Else
                        txt_CustomerType.Text = "Customer"
                        txt_role_noncustomer.Hidden = True
                    End If

                    'Customer Information
                    'Hide/Show FieldSet Individual/Korporasi
                    fs_Individual.Hidden = True
                    fs_Corporate.Hidden = True

                    Dim customerstatusclosed As String = ""
                    Dim drCustomerClosed As DataRow = GetCustomerClosedStatus(txt_GCN.Value)
                    If drCustomerClosed IsNot Nothing Then
                        If Not IsDBNull(drCustomerClosed("IsClosed")) Then
                            If drCustomerClosed("IsClosed") = 0 Then
                                customerstatusclosed = "Active"
                            Else
                                If Not IsDBNull(drCustomerClosed("ClosedDate")) Then
                                    customerstatusclosed = "Closed ( " & CDate(drCustomerClosed("ClosedDate")).ToString("dd-MMM-yyyy") & " )"
                                Else
                                    customerstatusclosed = "Closed"
                                End If
                            End If
                        End If
                    Else
                        customerstatusclosed = "Customer GCN Not Found"
                    End If

                    If .Fk_goAML_Ref_Customer_Type_id = 1 Then      'Individual

                        gp_SIPENDAR_PROFILE_PHONE.Hidden = False
                        gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = False
                        fs_Individual.Hidden = False

                        txt_INDV_NAME.Value = .INDV_NAME
                        txt_INDV_PLACEOFBIRTH.Value = .INDV_PLACEOFBIRTH
                        If .INDV_DOB.HasValue Then
                            txt_INDV_DOB.Value = .INDV_DOB
                        End If
                        txt_INDV_ADDRESS.Value = .INDV_ADDRESS

                        If Not IsNothing(.INDV_FK_goAML_Ref_Nama_Negara_CODE) Then
                            Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(.INDV_FK_goAML_Ref_Nama_Negara_CODE)
                            If objCountry IsNot Nothing Then
                                cmb_INDV_NATIONALITY.SetTextWithTextValue(objCountry.Kode, objCountry.Keterangan)
                            End If
                        End If
                        fs_Individual_IsHaveActiveAccount.Text = customerstatusclosed
                        If customerstatusclosed = "Customer GCN Not Found" Then
                            fs_Corporate_IsHaveActiveAccount.Value = customerstatusclosed
                        Else
                            fs_Corporate_IsHaveActiveAccount.Value = "This Customer Registered as Individual"
                        End If
                    Else    'Korporasi
                        gp_SIPENDAR_PROFILE_PHONE.Hidden = True
                        gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = True
                        fs_Corporate.Hidden = False

                        txt_CORP_NAME.Value = .CORP_NAME
                        txt_CORP_NPWP.Value = .CORP_NPWP
                        txt_CORP_NO_IZIN_USAHA.Value = .CORP_NO_IZIN_USAHA

                        If Not IsNothing(.CORP_FK_goAML_Ref_Nama_Negara_CODE) Then
                            Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(.CORP_FK_goAML_Ref_Nama_Negara_CODE)
                            If objCountry IsNot Nothing Then
                                cmb_CORP_COUNTRY.SetTextWithTextValue(objCountry.Kode, objCountry.Keterangan)
                            End If
                        End If
                        fs_Corporate_IsHaveActiveAccount.Text = customerstatusclosed
                        If customerstatusclosed = "Customer GCN Not Found" Then
                            fs_Individual_IsHaveActiveAccount.Value = customerstatusclosed
                        Else
                            fs_Individual_IsHaveActiveAccount.Value = "This Customer Registered as Individual"
                        End If
                    End If
                End With
            End Using


            'Bind Data Detail to Grid Panel
            Bind_SIPENDAR_PROFILE_ACCOUNT()
            Bind_SIPENDAR_PROFILE_AddRESS()
            Bind_SIPENDAR_PROFILE_PHONE()
            Bind_SIPENDAR_PROFILE_IDENTIFICATION()

            'Sesuaikan dengan SIPENDAR_TYPE
            If Not IsNothing(objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID) Then
                If objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID = 1 Then
                    cmb_JENIS_WATCHLIST_CODE.IsHidden = False
                    cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = False
                    cmb_SUMBER_TYPE_CODE.IsHidden = True
                    txt_SUMBER_ID.Hidden = True
                    txt_Similarity.Hidden = True
                Else
                    cmb_JENIS_WATCHLIST_CODE.IsHidden = True
                    cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = True
                    cmb_SUMBER_TYPE_CODE.IsHidden = False
                    txt_SUMBER_ID.Hidden = False
                    txt_Similarity.Hidden = False
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF LOAD PROFILE

    '------- ACCOUNT
    Protected Sub btn_SIPENDAR_PROFILE_ACCOUNT_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objSIPENDAR_PROFILE_ACCOUNT_Edit = Nothing
            Clean_Window_SIPENDAR_PROFILE_ACCOUNT()
            Clean_Window_SIPENDAR_PROFILE_ACCOUNT_ATM()

            'Show window pop up
            Window_SIPENDAR_PROFILE_ACCOUNT.Title = "Account - Add"
            Window_SIPENDAR_PROFILE_ACCOUNT.Hidden = False

            'Set IDAccount to 0
            IDAccount = 0
            Bind_SIPENDAR_PROFILE_ACCOUNT_ATM()

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_SIPENDAR_PROFILE_ACCOUNT(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = strID)
                If objToDelete IsNot Nothing Then
                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Remove(objToDelete)
                End If
                Bind_SIPENDAR_PROFILE_ACCOUNT()

                '9-Sep-2021 hapus jg ATM yang menyangkut account bersangkutan
                Dim listATMToDelete = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = strID).ToList
                For Each item In listATMToDelete
                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Remove(item)
                Next
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objSIPENDAR_PROFILE_ACCOUNT_Edit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = strID)
                Load_Window_SIPENDAR_PROFILE_ACCOUNT(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_ACCOUNT_Cancel_Click()
        Try
            'Hide window pop up
            Window_SIPENDAR_PROFILE_ACCOUNT.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_ACCOUNT_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.FieldLabel & " harus diisi.")
            End If
            If Len(Trim(txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.Value)) < 5 Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.FieldLabel & " minimal 5 karakter.")
            End If

            'Action save here
            Dim intPK As Long = -1

            If objSIPENDAR_PROFILE_ACCOUNT_Edit Is Nothing Then  'Add
                Dim objAdd As New SIPENDAR_PROFILE_ACCOUNT
                If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Count > 0 Then
                    intPK = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Min(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                IDAccount = intPK

                With objAdd
                    .PK_SIPENDAR_PROFILE_ACCOUNT_ID = intPK
                    .CIFNO = txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.Value

                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SelectedItemValue) Then
                        .Fk_goAML_Ref_Jenis_Rekening_CODE = cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SelectedItemValue
                    Else
                        .Fk_goAML_Ref_Jenis_Rekening_CODE = Nothing
                    End If

                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SelectedItemValue) Then
                        .Fk_goAML_Ref_Status_Rekening_CODE = cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SelectedItemValue
                    Else
                        .Fk_goAML_Ref_Status_Rekening_CODE = Nothing
                    End If

                    .NOREKENING = txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.Value
                End With

                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Add(objAdd)
            Else    'Edit
                Dim objEdit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = objSIPENDAR_PROFILE_ACCOUNT_Edit.PK_SIPENDAR_PROFILE_ACCOUNT_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDAccount = .PK_SIPENDAR_PROFILE_ACCOUNT_ID

                        .CIFNO = txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.Value

                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SelectedItemValue) Then
                            .Fk_goAML_Ref_Jenis_Rekening_CODE = cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SelectedItemValue
                        Else
                            .Fk_goAML_Ref_Jenis_Rekening_CODE = Nothing
                        End If

                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SelectedItemValue) Then
                            .Fk_goAML_Ref_Status_Rekening_CODE = cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SelectedItemValue
                        Else
                            .Fk_goAML_Ref_Status_Rekening_CODE = Nothing
                        End If

                        .NOREKENING = txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.Value
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_SIPENDAR_PROFILE_ACCOUNT()

            'Save Account ATM FK_Account_ID
            Dim listATM = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = 0)
            If Not listATM Is Nothing Then
                For Each item In listATM
                    item.FK_SIPENDAR_PROFILE_ACCOUNT_ID = IDAccount
                Next
            End If

            'Hide window popup
            Window_SIPENDAR_PROFILE_ACCOUNT.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_ACCOUNT()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listJenisRekening As List(Of goAML_Ref_Jenis_Rekening) = Nothing
        Dim listStatusRekening As List(Of goAML_Ref_Status_Rekening) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listJenisRekening = objdb.goAML_Ref_Jenis_Rekening.ToList
            listStatusRekening = objdb.goAML_Ref_Status_Rekening.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("JENIS_REKENING", GetType(String)))
        objtable.Columns.Add(New DataColumn("STATUS_REKENING", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objJenisRekening = SiPendarProfile_BLL.GetJenisRekeningByCode(item("Fk_goAML_Ref_Jenis_Rekening_CODE").ToString)
                'If objJenisRekening IsNot Nothing Then
                '    item("JENIS_REKENING") = objJenisRekening.Keterangan
                'End If

                'Dim objStatusRekening = SiPendarProfile_BLL.GetStatusRekeningByCode(item("Fk_goAML_Ref_Status_Rekening_CODE").ToString)
                'If objStatusRekening IsNot Nothing Then
                '    item("STATUS_REKENING") = objStatusRekening.Keterangan
                'End If

                If listJenisRekening IsNot Nothing Then
                    Dim objJenisRekening = listJenisRekening.Find(Function(x) x.Kode = item("Fk_goAML_Ref_Jenis_Rekening_CODE").ToString)
                    If objJenisRekening IsNot Nothing Then
                        item("JENIS_REKENING") = objJenisRekening.Keterangan
                    End If
                End If

                If listStatusRekening IsNot Nothing Then
                    Dim objStatusRekening = listStatusRekening.Find(Function(x) x.Kode = item("Fk_goAML_Ref_Status_Rekening_CODE").ToString)
                    If objStatusRekening IsNot Nothing Then
                        item("STATUS_REKENING") = objStatusRekening.Keterangan
                    End If
                End If
            Next
        End If

        gp_SIPENDAR_PROFILE_ACCOUNT.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_ACCOUNT.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_SIPENDAR_PROFILE_ACCOUNT()
        'Clean fields
        txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.Value = Nothing
        cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SetTextValue("")
        cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SetTextValue("")
        txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.Value = Nothing

        'Set fields' ReadOnly
        txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.ReadOnly = False
        cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.IsReadOnly = False
        cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.IsReadOnly = False
        txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.ReadOnly = False

        'Show Buttons
        btn_SIPENDAR_PROFILE_ACCOUNT_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_SIPENDAR_PROFILE_ACCOUNT(strAction As String)
        'Clean window pop up
        Clean_Window_SIPENDAR_PROFILE_ACCOUNT()

        'Populate fields
        With objSIPENDAR_PROFILE_ACCOUNT_Edit
            txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.Value = .CIFNO
            txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.Value = .NOREKENING

            Dim objJenisRekening = SiPendarProfile_BLL.GetJenisRekeningByCode(.Fk_goAML_Ref_Jenis_Rekening_CODE)
            If Not objJenisRekening Is Nothing Then
                cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SetTextWithTextValue(objJenisRekening.Kode, objJenisRekening.Keterangan)
            End If

            Dim objStatusRekening = SiPendarProfile_BLL.GetStatusRekeningByCode(.Fk_goAML_Ref_Status_Rekening_CODE)
            If Not objStatusRekening Is Nothing Then
                cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SetTextWithTextValue(objStatusRekening.Kode, objStatusRekening.Keterangan)
            End If

        End With

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.ReadOnly = False
            cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.IsReadOnly = False
            cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.IsReadOnly = False
            txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.ReadOnly = False
        Else
            txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.ReadOnly = True
            cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.IsReadOnly = True
            cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.IsReadOnly = True
            txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.ReadOnly = True

            btn_SIPENDAR_PROFILE_ACCOUNT_Save.Hidden = True 'Added on 16 Aug 2021
        End If

        'Bind ATM
        IDAccount = objSIPENDAR_PROFILE_ACCOUNT_Edit.PK_SIPENDAR_PROFILE_ACCOUNT_ID
        Bind_SIPENDAR_PROFILE_ACCOUNT_ATM()

        'Show window pop up
        Window_SIPENDAR_PROFILE_ACCOUNT.Title = "Account - " & strAction
        Window_SIPENDAR_PROFILE_ACCOUNT.Hidden = False
    End Sub
    '------- END OF ACCOUNT

    '------- ADDRESS
    Protected Sub btn_SIPENDAR_PROFILE_AddRESS_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objSIPENDAR_PROFILE_ADDRESS_Edit = Nothing
            Clean_Window_SIPENDAR_PROFILE_AddRESS()

            'Show window pop up
            Window_SIPENDAR_PROFILE_ADDRESS.Title = "Address - Add"
            Window_SIPENDAR_PROFILE_ADDRESS.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_SIPENDAR_PROFILE_AddRESS(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = strID)
                If objToDelete IsNot Nothing Then
                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Remove(objToDelete)
                End If
                Bind_SIPENDAR_PROFILE_AddRESS()
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objSIPENDAR_PROFILE_ADDRESS_Edit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = strID)
                Load_Window_SIPENDAR_PROFILE_AddRESS(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_AddRESS_Cancel_Click()
        Try
            'Hide window pop up
            Window_SIPENDAR_PROFILE_ADDRESS.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_AddRESS_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_ADDRESS_CITY.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_ADDRESS_CITY.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.Label & " harus diisi.")
            End If
            'If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_ADDRESS_ZIP.Value) Then
            '    Throw New ApplicationException(txt_SIPENDAR_PROFILE_ADDRESS_ZIP.FieldLabel & " harus diisi.")
            'End If

            'Action save here
            Dim intPK As Long = -1

            If objSIPENDAR_PROFILE_ADDRESS_Edit Is Nothing Then  'Add
                Dim objAdd As New SIPENDAR_PROFILE_ADDRESS
                If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Count > 0 Then
                    intPK = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Min(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                With objAdd
                    .PK_SIPENDAR_PROFILE_ADDRESS_ID = intPK

                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SelectedItemValue) Then
                        .FK_goAML_Ref_Kategori_Kontak_CODE = cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SelectedItemValue
                    Else
                        .FK_goAML_Ref_Kategori_Kontak_CODE = Nothing
                    End If
                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SelectedItemValue) Then
                        .FK_goAML_Ref_Nama_Negara_CODE = cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SelectedItemValue
                    Else
                        .FK_goAML_Ref_Nama_Negara_CODE = Nothing
                    End If

                    .ADDRESS = Trim(txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.Value)
                    .TOWN = Trim(txt_SIPENDAR_PROFILE_ADDRESS_TOWN.Value)
                    .CITY = Trim(txt_SIPENDAR_PROFILE_ADDRESS_CITY.Value)
                    .STATE = Trim(txt_SIPENDAR_PROFILE_ADDRESS_STATE.Value)
                    .ZIP = Trim(txt_SIPENDAR_PROFILE_ADDRESS_ZIP.Value)
                    .COMMENTS = Trim(txt_SIPENDAR_PROFILE_ADDRESS_COMMENTS.Value)
                End With

                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Add(objAdd)
            Else    'Edit
                Dim objEdit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = objSIPENDAR_PROFILE_ADDRESS_Edit.PK_SIPENDAR_PROFILE_ADDRESS_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SelectedItemValue) Then
                            .FK_goAML_Ref_Kategori_Kontak_CODE = cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SelectedItemValue
                        Else
                            .FK_goAML_Ref_Kategori_Kontak_CODE = Nothing
                        End If
                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SelectedItemValue) Then
                            .FK_goAML_Ref_Nama_Negara_CODE = cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SelectedItemValue
                        Else
                            .FK_goAML_Ref_Nama_Negara_CODE = Nothing
                        End If

                        .ADDRESS = Trim(txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.Value)
                        .TOWN = Trim(txt_SIPENDAR_PROFILE_ADDRESS_TOWN.Value)
                        .CITY = Trim(txt_SIPENDAR_PROFILE_ADDRESS_CITY.Value)
                        .STATE = Trim(txt_SIPENDAR_PROFILE_ADDRESS_STATE.Value)
                        .ZIP = Trim(txt_SIPENDAR_PROFILE_ADDRESS_ZIP.Value)
                        .COMMENTS = Trim(txt_SIPENDAR_PROFILE_ADDRESS_COMMENTS.Value)
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_SIPENDAR_PROFILE_AddRESS()

            'Hide window popup
            Window_SIPENDAR_PROFILE_ADDRESS.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_AddRESS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listAddressType As List(Of goAML_Ref_Kategori_Kontak) = Nothing
        Dim listCountryName As List(Of goAML_Ref_Nama_Negara) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
            listCountryName = objdb.goAML_Ref_Nama_Negara.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("ADDRESS_TYPE_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objAddressType = SiPendarProfile_BLL.GetKategoriKontakByCode(item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                'If objAddressType IsNot Nothing Then
                '    item("ADDRESS_TYPE_NAME") = objAddressType.Keterangan
                'Else
                '    item("ADDRESS_TYPE_NAME") = Nothing
                'End If

                'Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(item("FK_goAML_Ref_Nama_Negara_CODE").ToString)
                'If objCountry IsNot Nothing Then
                '    item("COUNTRY_NAME") = objCountry.Keterangan
                'Else
                '    item("COUNTRY_NAME") = Nothing
                'End If

                If listAddressType IsNot Nothing Then
                    Dim objAddressType = listAddressType.Find(Function(x) x.Kode = item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                    If objAddressType IsNot Nothing Then
                        item("ADDRESS_TYPE_NAME") = objAddressType.Keterangan
                    End If
                End If

                If listCountryName IsNot Nothing Then
                    Dim objCountry = listCountryName.Find(Function(x) x.Kode = item("FK_goAML_Ref_Nama_Negara_CODE").ToString)
                    If objCountry IsNot Nothing Then
                        item("COUNTRY_NAME") = objCountry.Keterangan
                    End If
                End If
            Next
        End If

        gp_SIPENDAR_PROFILE_ADDRESS.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_ADDRESS.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_SIPENDAR_PROFILE_AddRESS()
        'Clean fields
        cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SetTextValue("")
        txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.Value = Nothing
        txt_SIPENDAR_PROFILE_ADDRESS_CITY.Value = Nothing
        cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SetTextValue("")
        txt_SIPENDAR_PROFILE_ADDRESS_ZIP.Value = Nothing

        txt_SIPENDAR_PROFILE_ADDRESS_TOWN.Value = Nothing
        txt_SIPENDAR_PROFILE_ADDRESS_STATE.Value = Nothing
        txt_SIPENDAR_PROFILE_ADDRESS_COMMENTS.Value = Nothing

        'Set fields' ReadOnly
        cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.IsReadOnly = False
        txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.ReadOnly = False
        txt_SIPENDAR_PROFILE_ADDRESS_CITY.ReadOnly = False
        cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.IsReadOnly = False
        txt_SIPENDAR_PROFILE_ADDRESS_ZIP.ReadOnly = False

        txt_SIPENDAR_PROFILE_ADDRESS_TOWN.ReadOnly = False
        txt_SIPENDAR_PROFILE_ADDRESS_STATE.ReadOnly = False
        txt_SIPENDAR_PROFILE_ADDRESS_COMMENTS.ReadOnly = False

        'Show Buttons
        btn_SIPENDAR_PROFILE_ADDRESS_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_SIPENDAR_PROFILE_AddRESS(strAction As String)
        'Clean window pop up
        Clean_Window_SIPENDAR_PROFILE_AddRESS()

        'Populate fields
        With objSIPENDAR_PROFILE_ADDRESS_Edit
            txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.Value = .ADDRESS
            txt_SIPENDAR_PROFILE_ADDRESS_CITY.Value = .CITY
            txt_SIPENDAR_PROFILE_ADDRESS_ZIP.Value = .ZIP

            txt_SIPENDAR_PROFILE_ADDRESS_TOWN.Value = .TOWN
            txt_SIPENDAR_PROFILE_ADDRESS_STATE.Value = .STATE
            txt_SIPENDAR_PROFILE_ADDRESS_COMMENTS.Value = .COMMENTS

            If Not IsNothing(.FK_goAML_Ref_Kategori_Kontak_CODE) Then
                Dim objAddressType = SiPendarProfile_BLL.GetKategoriKontakByCode(.FK_goAML_Ref_Kategori_Kontak_CODE)
                If objAddressType IsNot Nothing Then
                    cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SetTextWithTextValue(objAddressType.Kode, objAddressType.Keterangan)
                End If
            End If

            If Not IsNothing(.FK_goAML_Ref_Nama_Negara_CODE) Then
                Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(.FK_goAML_Ref_Nama_Negara_CODE)
                If objCountry IsNot Nothing Then
                    cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SetTextWithTextValue(objCountry.Kode, objCountry.Keterangan)
                End If
            End If

        End With

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.IsReadOnly = True
            txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.ReadOnly = True
            txt_SIPENDAR_PROFILE_ADDRESS_CITY.ReadOnly = True
            cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.IsReadOnly = True
            txt_SIPENDAR_PROFILE_ADDRESS_ZIP.ReadOnly = True

            txt_SIPENDAR_PROFILE_ADDRESS_TOWN.ReadOnly = True
            txt_SIPENDAR_PROFILE_ADDRESS_STATE.ReadOnly = True
            txt_SIPENDAR_PROFILE_ADDRESS_COMMENTS.ReadOnly = True

            btn_SIPENDAR_PROFILE_ADDRESS_Save.Hidden = True
        End If

        'Show window pop up
        Window_SIPENDAR_PROFILE_ADDRESS.Title = "Address - " & strAction
        Window_SIPENDAR_PROFILE_ADDRESS.Hidden = False
    End Sub
    '------- END OF ADDRESS

    '------- PHONE
    Protected Sub btn_SIPENDAR_PROFILE_PHONE_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objSIPENDAR_PROFILE_PHONE_Edit = Nothing
            Clean_Window_SIPENDAR_PROFILE_PHONE()

            'Show window pop up
            Window_SIPENDAR_PROFILE_PHONE.Title = "PHONE - Add"
            Window_SIPENDAR_PROFILE_PHONE.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_SIPENDAR_PROFILE_PHONE(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = strID)
                If objToDelete IsNot Nothing Then
                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Remove(objToDelete)
                End If
                Bind_SIPENDAR_PROFILE_PHONE()
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objSIPENDAR_PROFILE_PHONE_Edit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = strID)
                Load_Window_SIPENDAR_PROFILE_PHONE(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_PHONE_Cancel_Click()
        Try
            'Hide window pop up
            Window_SIPENDAR_PROFILE_PHONE.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_PHONE_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_PHONE_NUMBER.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_PHONE_NUMBER.FieldLabel & " harus diisi.")
            End If

            'Action save here
            Dim intPK As Long = -1

            If objSIPENDAR_PROFILE_PHONE_Edit Is Nothing Then  'Add
                Dim objAdd As New SIPENDAR_PROFILE_PHONE
                If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Count > 0 Then
                    intPK = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Min(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                With objAdd
                    .PK_SIPENDAR_PROFILE_PHONE_ID = intPK

                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SelectedItemValue) Then
                        .FK_goAML_Ref_Kategori_Kontak_CODE = cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SelectedItemValue
                    Else
                        .FK_goAML_Ref_Kategori_Kontak_CODE = Nothing
                    End If

                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SelectedItemValue) Then
                        .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SelectedItemValue
                    Else
                        .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = Nothing
                    End If

                    .COUNTRY_PREFIX = Trim(txt_SIPENDAR_PROFILE_PHONE_COUNTRY_PREFIX.Value)
                    .PHONE_NUMBER = Trim(txt_SIPENDAR_PROFILE_PHONE_NUMBER.Value)
                    .EXTENSION_NUMBER = Trim(txt_SIPENDAR_PROFILE_PHONE_EXTENSION_NUMBER.Value)
                    .COMMENTS = Trim(txt_SIPENDAR_PROFILE_PHONE_COMMENTS.Value)
                End With

                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Add(objAdd)
            Else    'Edit
                Dim objEdit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = objSIPENDAR_PROFILE_PHONE_Edit.PK_SIPENDAR_PROFILE_PHONE_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SelectedItemValue) Then
                            .FK_goAML_Ref_Kategori_Kontak_CODE = cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SelectedItemValue
                        Else
                            .FK_goAML_Ref_Kategori_Kontak_CODE = Nothing
                        End If

                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SelectedItemValue) Then
                            .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SelectedItemValue
                        Else
                            .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = Nothing
                        End If

                        .COUNTRY_PREFIX = Trim(txt_SIPENDAR_PROFILE_PHONE_COUNTRY_PREFIX.Value)
                        .PHONE_NUMBER = Trim(txt_SIPENDAR_PROFILE_PHONE_NUMBER.Value)
                        .EXTENSION_NUMBER = Trim(txt_SIPENDAR_PROFILE_PHONE_EXTENSION_NUMBER.Value)
                        .COMMENTS = Trim(txt_SIPENDAR_PROFILE_PHONE_COMMENTS.Value)
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_SIPENDAR_PROFILE_PHONE()

            'Hide window popup
            Window_SIPENDAR_PROFILE_PHONE.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_PHONE()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listPhoneType As List(Of goAML_Ref_Kategori_Kontak) = Nothing
        Dim listCommunicationType As List(Of goAML_Ref_Jenis_Alat_Komunikasi) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listPhoneType = objdb.goAML_Ref_Kategori_Kontak.ToList
            listCommunicationType = objdb.goAML_Ref_Jenis_Alat_Komunikasi.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("PHONE_TYPE_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("COMMUNICATION_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objPHONEType = SiPendarProfile_BLL.GetKategoriKontakByCode(item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                'If objPHONEType IsNot Nothing Then
                '    item("PHONE_TYPE_NAME") = objPHONEType.Keterangan
                'Else
                '    item("PHONE_TYPE_NAME") = Nothing
                'End If

                'Dim objCommunicationType = SiPendarProfile_BLL.GetJenisAlatKomunikasiByCode(item("FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE").ToString)
                'If objCommunicationType IsNot Nothing Then
                '    item("COMMUNICATION_TYPE_NAME") = objCommunicationType.Keterangan
                'Else
                '    item("COMMUNICATION_TYPE_NAME") = Nothing
                'End If

                If listPhoneType IsNot Nothing Then
                    Dim objPHONEType = listPhoneType.Find(Function(x) x.Kode = item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                    If objPHONEType IsNot Nothing Then
                        item("PHONE_TYPE_NAME") = objPHONEType.Keterangan
                    End If
                End If

                If listCommunicationType IsNot Nothing Then
                    Dim objCommunicationType = listCommunicationType.Find(Function(x) x.Kode = item("FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE").ToString)
                    If objCommunicationType IsNot Nothing Then
                        item("COMMUNICATION_TYPE_NAME") = objCommunicationType.Keterangan
                    End If
                End If

            Next
        End If

        gp_SIPENDAR_PROFILE_PHONE.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_PHONE.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_SIPENDAR_PROFILE_PHONE()
        'Clean fields
        cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SetTextValue("")
        cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SetTextValue("")
        txt_SIPENDAR_PROFILE_PHONE_COUNTRY_PREFIX.Value = Nothing
        txt_SIPENDAR_PROFILE_PHONE_NUMBER.Value = Nothing

        txt_SIPENDAR_PROFILE_PHONE_EXTENSION_NUMBER.Value = Nothing
        txt_SIPENDAR_PROFILE_PHONE_COMMENTS.Value = Nothing

        'Set fields' ReadOnly
        cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.IsReadOnly = False
        cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.IsReadOnly = False
        txt_SIPENDAR_PROFILE_PHONE_COUNTRY_PREFIX.ReadOnly = False
        txt_SIPENDAR_PROFILE_PHONE_NUMBER.ReadOnly = False

        txt_SIPENDAR_PROFILE_PHONE_EXTENSION_NUMBER.ReadOnly = False
        txt_SIPENDAR_PROFILE_PHONE_COMMENTS.ReadOnly = False

        'Show Buttons
        btn_SIPENDAR_PROFILE_PHONE_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_SIPENDAR_PROFILE_PHONE(strAction As String)
        'Clean window pop up
        Clean_Window_SIPENDAR_PROFILE_PHONE()

        'Populate fields
        With objSIPENDAR_PROFILE_PHONE_Edit
            txt_SIPENDAR_PROFILE_PHONE_COUNTRY_PREFIX.Value = .COUNTRY_PREFIX
            txt_SIPENDAR_PROFILE_PHONE_NUMBER.Value = .PHONE_NUMBER

            txt_SIPENDAR_PROFILE_PHONE_EXTENSION_NUMBER.Value = .EXTENSION_NUMBER
            txt_SIPENDAR_PROFILE_PHONE_COMMENTS.Value = .COMMENTS

            If Not IsNothing(.FK_goAML_Ref_Kategori_Kontak_CODE) Then
                Dim objPHONEType = SiPendarProfile_BLL.GetKategoriKontakByCode(.FK_goAML_Ref_Kategori_Kontak_CODE)
                If objPHONEType IsNot Nothing Then
                    cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SetTextWithTextValue(objPHONEType.Kode, objPHONEType.Keterangan)
                End If

                Dim objCommunicationType = SiPendarProfile_BLL.GetJenisAlatKomunikasiByCode(.FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE)
                If objCommunicationType IsNot Nothing Then
                    cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SetTextWithTextValue(objCommunicationType.Kode, objCommunicationType.Keterangan)
                End If
            End If
        End With

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.IsReadOnly = True
            cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.IsReadOnly = True
            txt_SIPENDAR_PROFILE_PHONE_COUNTRY_PREFIX.ReadOnly = True
            txt_SIPENDAR_PROFILE_PHONE_NUMBER.ReadOnly = True

            txt_SIPENDAR_PROFILE_PHONE_EXTENSION_NUMBER.ReadOnly = True
            txt_SIPENDAR_PROFILE_PHONE_COMMENTS.ReadOnly = True

            btn_SIPENDAR_PROFILE_PHONE_Save.Hidden = True
        End If

        'Show window pop up
        Window_SIPENDAR_PROFILE_PHONE.Title = "PHONE - " & strAction
        Window_SIPENDAR_PROFILE_PHONE.Hidden = False
    End Sub
    '------- END OF PHONE

    '------- IDENTIFICATION
    Protected Sub btn_SIPENDAR_PROFILE_IDENTIFICATION_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objSIPENDAR_PROFILE_IDENTIFICATION_Edit = Nothing
            Clean_Window_SIPENDAR_PROFILE_IDENTIFICATION()

            'Show window pop up
            Window_SIPENDAR_PROFILE_IDENTIFICATION.Title = "IDENTIFICATION - Add"
            Window_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_SIPENDAR_PROFILE_IDENTIFICATION(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = strID)
                If objToDelete IsNot Nothing Then
                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Remove(objToDelete)
                End If
                Bind_SIPENDAR_PROFILE_IDENTIFICATION()
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objSIPENDAR_PROFILE_IDENTIFICATION_Edit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = strID)
                Load_Window_SIPENDAR_PROFILE_IDENTIFICATION(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_IDENTIFICATION_Cancel_Click()
        Try
            'Hide window pop up
            Window_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_IDENTIFICATION_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.FieldLabel & " harus diisi.")
            End If
            'If CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value) = DateTime.MinValue Then
            '    Throw New ApplicationException(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.FieldLabel & " harus diisi.")
            'End If
            'If CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value) = DateTime.MinValue Then
            '    Throw New ApplicationException(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.FieldLabel & " harus diisi.")
            'End If
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.Label & " harus diisi.")
            End If

            If Not String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value) Then
                Dim strIDENTIFICATIONType As String = cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SelectedItemValue
                Dim strSSN As String = txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value
                If strIDENTIFICATIONType = "KTP" And (Len(strSSN) <> 16 Or Not strSSN.All(AddressOf Char.IsDigit)) Then
                    Throw New ApplicationException(txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.FieldLabel & " must 16 digit numeric only for KTP.")
                End If
            End If

            If CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value) <> DateTime.MinValue AndAlso CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value) <> DateTime.MinValue Then
                If CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value) > CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value) Then
                    Throw New ApplicationException(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.FieldLabel & " harus >= dari " & txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.FieldLabel)
                End If
            End If

            'Action save here
            Dim intPK As Long = -1

            If objSIPENDAR_PROFILE_IDENTIFICATION_Edit Is Nothing Then  'Add
                Dim objAdd As New SIPENDAR_PROFILE_IDENTIFICATION
                If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Count > 0 Then
                    intPK = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Min(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                With objAdd
                    .PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = intPK

                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SelectedItemValue) Then
                        .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SelectedItemValue
                    Else
                        .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = Nothing
                    End If
                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SelectedItemValue) Then
                        .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SelectedItemValue
                    Else
                        .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = Nothing
                    End If

                    If Not CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value) = DateTime.MinValue Then
                        .ISSUE_DATE = CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value)
                    Else
                        .ISSUE_DATE = Nothing
                    End If

                    If Not CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value) = DateTime.MinValue Then
                        .EXPIRED_DATE = CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value)
                    Else
                        .EXPIRED_DATE = Nothing
                    End If

                    .IDENTITY_NUMBER = Trim(txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value)
                    .ISSUED_BY = Trim(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUED_BY.Value)
                    .Comments = Trim(txt_SIPENDAR_PROFILE_IDENTIFICATION_COMMENTS.Value)
                End With

                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Add(objAdd)
            Else    'Edit
                Dim objEdit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = objSIPENDAR_PROFILE_IDENTIFICATION_Edit.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SelectedItemValue) Then
                            .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SelectedItemValue
                        Else
                            .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = Nothing
                        End If
                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SelectedItemValue) Then
                            .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SelectedItemValue
                        Else
                            .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = Nothing
                        End If

                        If Not CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value) = DateTime.MinValue Then
                            .ISSUE_DATE = CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value)
                        Else
                            .ISSUE_DATE = Nothing
                        End If

                        If Not CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value) = DateTime.MinValue Then
                            .EXPIRED_DATE = CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value)
                        Else
                            .EXPIRED_DATE = Nothing
                        End If

                        .IDENTITY_NUMBER = Trim(txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value)
                        .ISSUED_BY = Trim(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUED_BY.Value)
                        .Comments = Trim(txt_SIPENDAR_PROFILE_IDENTIFICATION_COMMENTS.Value)
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_SIPENDAR_PROFILE_IDENTIFICATION()

            'Hide window popup
            Window_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_IDENTIFICATION()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listIdentificationType As List(Of goAML_Ref_Jenis_Dokumen_Identitas) = Nothing
        Dim listCountryName As List(Of goAML_Ref_Nama_Negara) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listIdentificationType = objdb.goAML_Ref_Jenis_Dokumen_Identitas.ToList
            listCountryName = objdb.goAML_Ref_Nama_Negara.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("IDENTIFICATION_TYPE_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("ISSUED_COUNTRY", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objIDENTIFICATIONType = SiPendarProfile_BLL.GetJenisDokumenIdentitasByCode(item("Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ToString)
                'If objIDENTIFICATIONType IsNot Nothing Then
                '    item("IDENTIFICATION_TYPE_NAME") = objIDENTIFICATIONType.Keterangan
                'Else
                '    item("IDENTIFICATION_TYPE_NAME") = Nothing
                'End If

                'Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(item("FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY").ToString)
                'If objCountry IsNot Nothing Then
                '    item("ISSUED_COUNTRY") = objCountry.Keterangan
                'Else
                '    item("ISSUED_COUNTRY") = Nothing
                'End If

                If listIdentificationType IsNot Nothing Then
                    Dim objIDENTIFICATIONType = listIdentificationType.Find(Function(x) x.Kode = item("Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ToString)
                    If objIDENTIFICATIONType IsNot Nothing Then
                        item("IDENTIFICATION_TYPE_NAME") = objIDENTIFICATIONType.Keterangan
                    End If
                End If

                If listCountryName IsNot Nothing Then
                    Dim objCountry = listCountryName.Find(Function(x) x.Kode = item("FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY").ToString)
                    If objCountry IsNot Nothing Then
                        item("ISSUED_COUNTRY") = objCountry.Keterangan
                    End If
                End If
            Next
        End If

        gp_SIPENDAR_PROFILE_IDENTIFICATION.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_IDENTIFICATION.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_SIPENDAR_PROFILE_IDENTIFICATION()
        'Clean fields
        cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SetTextValue("")
        txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value = Nothing
        txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value = Nothing
        txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value = Nothing
        txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUED_BY.Value = Nothing
        cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SetTextValue("")

        txt_SIPENDAR_PROFILE_IDENTIFICATION_COMMENTS.Value = Nothing

        'Set fields' ReadOnly
        cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.IsReadOnly = False
        txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.ReadOnly = False
        txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.ReadOnly = False
        txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.ReadOnly = False
        txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUED_BY.ReadOnly = False
        cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.IsReadOnly = False

        txt_SIPENDAR_PROFILE_IDENTIFICATION_COMMENTS.ReadOnly = False

        'Show Buttons
        btn_SIPENDAR_PROFILE_IDENTIFICATION_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_SIPENDAR_PROFILE_IDENTIFICATION(strAction As String)
        'Clean window pop up
        Clean_Window_SIPENDAR_PROFILE_IDENTIFICATION()

        'Populate fields
        With objSIPENDAR_PROFILE_IDENTIFICATION_Edit
            txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value = .IDENTITY_NUMBER

            If .ISSUE_DATE.HasValue() Then
                txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value = .ISSUE_DATE
            End If
            If .EXPIRED_DATE.HasValue() Then
                txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value = .EXPIRED_DATE
            End If

            txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUED_BY.Value = .ISSUED_BY
            txt_SIPENDAR_PROFILE_IDENTIFICATION_COMMENTS.Value = .Comments

            If Not IsNothing(.Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE) Then
                Dim objIDENTIFICATIONType = SiPendarProfile_BLL.GetJenisDokumenIdentitasByCode(.Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE)
                If objIDENTIFICATIONType IsNot Nothing Then
                    cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SetTextWithTextValue(objIDENTIFICATIONType.Kode, objIDENTIFICATIONType.Keterangan)
                End If
            End If
            If Not IsNothing(.FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY) Then
                Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(.FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY)
                If objCountry IsNot Nothing Then
                    cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SetTextWithTextValue(objCountry.Kode, objCountry.Keterangan)
                End If
            End If
        End With

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.IsReadOnly = True
            txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.ReadOnly = True
            txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.ReadOnly = True
            txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.ReadOnly = True
            txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUED_BY.ReadOnly = True
            cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.IsReadOnly = True
            txt_SIPENDAR_PROFILE_IDENTIFICATION_COMMENTS.ReadOnly = True

            btn_SIPENDAR_PROFILE_IDENTIFICATION_Save.Hidden = True 'Added on 16 Aug 2021
        End If

        'Show window pop up
        Window_SIPENDAR_PROFILE_IDENTIFICATION.Title = "IDENTIFICATION - " & strAction
        Window_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = False
    End Sub
    '------- END OF IDENTIFICATION

    '------- ACCOUNT_ATM
    Protected Sub btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit = Nothing
            Clean_Window_SIPENDAR_PROFILE_ACCOUNT_ATM()

            'Show window pop up
            Window_SIPENDAR_PROFILE_ACCOUNT_ATM.Title = "ACCOUNT ATM - Add"
            Window_SIPENDAR_PROFILE_ACCOUNT_ATM.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_SIPENDAR_PROFILE_ACCOUNT_ATM(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = strID)
                If objToDelete IsNot Nothing Then
                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Remove(objToDelete)
                End If
                Bind_SIPENDAR_PROFILE_ACCOUNT_ATM()
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = strID)
                Load_Window_SIPENDAR_PROFILE_ACCOUNT_ATM(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Cancel_Click()
        Try
            'Hide window pop up
            Window_SIPENDAR_PROFILE_ACCOUNT_ATM.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.FieldLabel & " harus diisi.")
            End If

            'Action save here
            Dim intPK As Long = -1

            If objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit Is Nothing Then  'Add
                Dim objAdd As New SIPENDAR_PROFILE_ACCOUNT_ATM
                If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Count > 0 Then
                    intPK = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Min(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                With objAdd
                    .PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = intPK
                    .FK_SIPENDAR_PROFILE_ACCOUNT_ID = IDAccount
                    .NOATM = Trim(txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.Value)
                End With

                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Add(objAdd)
            Else    'Edit
                Dim objEdit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        .NOATM = Trim(txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.Value)
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_SIPENDAR_PROFILE_ACCOUNT_ATM()

            'Hide window popup
            Window_SIPENDAR_PROFILE_ACCOUNT_ATM.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_ACCOUNT_ATM()
        Dim listATM = New List(Of SIPENDAR_PROFILE_ACCOUNT_ATM)

        If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Count > 0 Then
            listATM = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = IDAccount).ToList
            If listATM Is Nothing Then
                listATM = New List(Of SIPENDAR_PROFILE_ACCOUNT_ATM)
            End If

        End If
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listATM)

        'Running the following script for additional columns
        'objtable.Columns.Add(New DataColumn("ACCOUNT_ATM_TYPE_NAME", GetType(String)))

        'If objtable.Rows.Count > 0 Then
        '    For Each item As Data.DataRow In objtable.Rows
        '        Dim objACCOUNT_ATMType = SiPendarProfile_BLL.GetKategoriKontakByCode(item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
        '        If objACCOUNT_ATMType IsNot Nothing Then
        '            item("ACCOUNT_ATM_TYPE_NAME") = objACCOUNT_ATMType.Keterangan
        '        Else
        '            item("ACCOUNT_ATM_TYPE_NAME") = Nothing
        '        End If
        '    Next
        'End If

        gp_SIPENDAR_PROFILE_ACCOUNT_ATM.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_ACCOUNT_ATM.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_SIPENDAR_PROFILE_ACCOUNT_ATM()
        'Clean fields
        txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.Value = Nothing

        'Set fields' ReadOnly
        txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.ReadOnly = False

        'Show Buttons
        btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_SIPENDAR_PROFILE_ACCOUNT_ATM(strAction As String)
        'Clean window pop up
        Clean_Window_SIPENDAR_PROFILE_ACCOUNT_ATM()

        'Populate fields
        With objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit
            txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.Value = .NOATM
        End With

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.ReadOnly = True
            btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Save.Hidden = True
        End If

        'Show window pop up
        Window_SIPENDAR_PROFILE_ACCOUNT_ATM.Title = "ACCOUNT ATM - " & strAction
        Window_SIPENDAR_PROFILE_ACCOUNT_ATM.Hidden = False
    End Sub
    '------- END OF ACCOUNT_ATM

    '------- DOCUMENT SAVE
    Protected Sub btn_SIPENDAR_PROFILE_Submit_Click()
        Try
            'Data Validation
            ValidateData()

            'Populate Data
            PopulateData()

            '2021-07-06 : show message if there are no changes in data.
            If IsNoChanges() Then
                Throw New ApplicationException("There are no changes in data. Please edit data before save.")
            End If

            '1-Jul-2021 Validasi jadi pakai Validation Parameter
            Dim ErrorValidation As String = SiPendarProfile_BLL.checkValidation(objSIPENDAR_PROFILE_CLASS, ObjModule, 2)
            If Not String.IsNullOrEmpty(ErrorValidation) Then
                SiPendarBLL.NawaFramework.extInfoPanelupdate(FormPanelInput, ErrorValidation, "infoValidationResultPanel")
                FormPanelInput.Body.ScrollTo(Direction.Top, 0)
                infoValidationResultPanel.Hidden = False
                Exit Sub
            End If

            ''Jalankan validasi satuan untuk replace IsValid dan Error Message
            'Pindah ke pas simpan saja
            'Dim param(0) As SqlParameter
            'param(0) = New SqlParameter
            'param(0).ParameterName = "@PK_Report_ID"
            'param(0).Value = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
            'param(0).DbType = SqlDbType.BigInt
            'NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_PROFILE_Validate_Satuan", param)

            'Save Data With/Without Approval
            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
                SiPendarProfile_BLL.SaveEditWithoutApproval(ObjModule, objSIPENDAR_PROFILE_CLASS)
                LblConfirmation.Text = "Data Saved into Database"
            Else
                SiPendarProfile_BLL.SaveEditWithApproval(ObjModule, objSIPENDAR_PROFILE_CLASS)
                LblConfirmation.Text = "Data Saved into Pending Approval"
            End If

            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ValidateData()
        Try
            'Validate Header
            If String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_TYPE_ID.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_CUSTOMER_TYPE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_CUSTOMER_TYPE.Label & " harus diisi.")
            End If

            If String.IsNullOrEmpty(txt_GCN.Value) Or String.IsNullOrWhiteSpace(txt_GCN.Value) Then ''Edited by Felix on 18 Aug 2021, pindah ke atas, karena ini perlu
                Throw New ApplicationException(txt_GCN.FieldLabel & " harus diisi.")
            End If
            ' 1 Dec 2021 Ari penambahan untuk validasi keterangan
            If txt_Keterangan.Value IsNot Nothing Then
                If Not String.IsNullOrEmpty(txt_Keterangan.Value) Then
                    Dim keterangan As String = ""
                    keterangan = txt_Keterangan.Value.ToString
                    If keterangan.Length > 500 Then
                        Throw New ApplicationException("Keterangan Tidak Boleh Lebih Dari 500 Kata")
                    End If
                End If

            End If
            '3-Jul-2021 Hanya SIPENDAR_TYPE aja yg perlu karena penting untuk validasi
            Exit Sub

            If String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_JENIS_WATCHLIST_CODE.Label & " harus diisi.")
            End If
            'If String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue) Then
            '    Throw New ApplicationException(cmb_TINDAK_PIDANA_CODE.Label & " harus diisi.")
            'End If
            If String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SUMBER_INFORMASI_KHUSUS_CODE.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_ORGANISASI_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_ORGANISASI_CODE.Label & " harus diisi.")
            End If

            'If String.IsNullOrEmpty(txt_GCN.Value) Then ''Edited by Felix on 18 Aug 2021, pindah ke atas, karena ini perlu
            '    Throw New ApplicationException(txt_GCN.FieldLabel & " harus diisi.")
            'End If

            If String.IsNullOrEmpty(txt_Keterangan.Value) Then
                Throw New ApplicationException(txt_Keterangan.FieldLabel & " harus diisi.")
            End If

            If String.IsNullOrEmpty(txt_Similarity.Text) Or txt_Similarity.Text = "N/A" Or txt_Similarity.Text = "NA" Then
                Throw New ApplicationException(txt_Similarity.FieldLabel & " harus diisi.")
            End If

            'Validate Profile
            If cmb_CUSTOMER_TYPE.SelectedItemValue = 1 Then     'Individual
                If String.IsNullOrEmpty(txt_INDV_NAME.Value) Then
                    Throw New ApplicationException(txt_INDV_NAME.FieldLabel & " harus diisi.")
                End If
                If String.IsNullOrEmpty(txt_INDV_PLACEOFBIRTH.Value) Then
                    Throw New ApplicationException(txt_INDV_PLACEOFBIRTH.FieldLabel & " harus diisi.")
                End If
                If CDate(txt_INDV_DOB.Value) = DateTime.MinValue Then
                    Throw New ApplicationException(txt_INDV_DOB.FieldLabel & " harus diisi.")
                End If
                'If String.IsNullOrEmpty(txt_INDV_ADDRESS.Value) Then
                '    Throw New ApplicationException(txt_INDV_ADDRESS.FieldLabel & " harus diisi.")
                'End If
                If String.IsNullOrEmpty(cmb_INDV_NATIONALITY.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_INDV_NATIONALITY.Label & " harus diisi.")
                End If
            ElseIf cmb_CUSTOMER_TYPE.SelectedItemValue = 2 Then     'Corporate
                If String.IsNullOrEmpty(txt_CORP_NAME.Value) Then
                    Throw New ApplicationException(txt_CORP_NAME.FieldLabel & " harus diisi.")
                End If
                If String.IsNullOrEmpty(txt_CORP_NPWP.Value) Then
                    Throw New ApplicationException(txt_CORP_NPWP.FieldLabel & " harus diisi.")
                End If
                If String.IsNullOrEmpty(txt_CORP_NO_IZIN_USAHA.Value) Then
                    Throw New ApplicationException(txt_CORP_NO_IZIN_USAHA.FieldLabel & " harus diisi.")
                End If
                If String.IsNullOrEmpty(cmb_CORP_COUNTRY.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_CORP_COUNTRY.Label & " harus diisi.")
                End If
            End If

            'Validate data Detail
            'If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT Is Nothing OrElse objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Count = 0 Then
            '    Throw New ApplicationException("Minimal 1 data Account harus diisi.")
            'End If
            'If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS Is Nothing OrElse objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Count = 0 Then
            '    Throw New ApplicationException("Minimal 1 data Address harus diisi.")
            'End If
            'If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE Is Nothing OrElse objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Count = 0 Then
            '    Throw New ApplicationException("Minimal 1 data Phone harus diisi.")
            'End If
            'If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION Is Nothing OrElse objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Count = 0 Then
            '    Throw New ApplicationException("Minimal 1 data Identification harus diisi.")
            'End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub PopulateData()
        Try
            With objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE
                'Populate Data Header
                If Not String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
                    .FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.SelectedItemValue
                End If
                If Not String.IsNullOrEmpty(cmb_CUSTOMER_TYPE.SelectedItemValue) Then
                    .Fk_goAML_Ref_Customer_Type_id = cmb_CUSTOMER_TYPE.SelectedItemValue
                End If

                'SIPENDAR Proaktif
                If Not String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue) Then
                    .FK_SIPENDAR_JENIS_WATCHLIST_CODE = cmb_JENIS_WATCHLIST_CODE.SelectedItemValue
                End If



                If Not String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue) Then
                    .FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE = cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue
                End If

                ' 21 Dec 2021 Ari : set tindak pidana dan organisasi code dengan ada yang di db karena untuk data tersebut sudah tidak dipake untuk profile sehingga tidak bisa di edit
                .FK_SIPENDAR_TINDAK_PIDANA_CODE = .FK_SIPENDAR_TINDAK_PIDANA_CODE

                .FK_SIPENDAR_ORGANISASI_CODE = .FK_SIPENDAR_ORGANISASI_CODE

                'SIPENDAR Pengayaan
                If Not String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue) Then
                    .FK_SIPENDAR_SUMBER_TYPE_CODE = cmb_SUMBER_TYPE_CODE.SelectedItemValue
                End If
                If Not (String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A" Or txt_SUMBER_ID.Text = "NA") And cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Then '' Edited by Felix 18 Aug 2021. Dicheck kalo Pengayaan aja
                    If txt_SUMBER_ID.Text > 2147483647 Then
                        Throw New ApplicationException("Angka yang dimasukan maksimal 2147483647 ")
                    Else

                        .SUMBER_ID = CInt(txt_SUMBER_ID.Value)
                    End If

                Else
                    .SUMBER_ID = Nothing
                End If
                If Not (String.IsNullOrEmpty(txt_Similarity.Text) Or txt_Similarity.Text = "N/A" Or txt_Similarity.Text = "NA") Then
                    .Similarity = CDbl(txt_Similarity.Value)
                Else
                    .Similarity = Nothing
                End If

                .GCN = Trim(txt_GCN.Value)

                'Tambahan 8-Jul-2021
                ' Tambahan 21 Nov 2021 Ari : Untuk Inisiasi Submission Type dan Keterangan jika dia kosong maka di isi submission type yang sudah ada di db
                If .Submission_Date IsNot Nothing Then
                    .Submission_Date = .Submission_Date
                Else
                    .Submission_Date = Nothing
                End If

                ' Tambahan 21 Nov 2021 Ari :  jika txt keterangan isinya empty bukan nothing
                If .Keterangan IsNot Nothing And Not String.IsNullOrEmpty(.Keterangan) Then
                    If txt_Keterangan.Value IsNot Nothing And Not String.IsNullOrEmpty(txt_Keterangan.Value.ToString) Then
                        .Keterangan = Trim(txt_Keterangan.Value)
                    Else
                        If .Keterangan Is Nothing Then
                            .Keterangan = Nothing
                        ElseIf String.IsNullOrEmpty(.Keterangan) Then
                            .Keterangan = Nothing
                        End If

                    End If

                Else
                    If .Keterangan Is Nothing Then
                        .Keterangan = Nothing
                    ElseIf String.IsNullOrEmpty(.Keterangan) Then
                        .Keterangan = Nothing
                    End If

                End If

                If Not String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue) Then
                    .FK_SIPENDAR_SUBMISSION_TYPE_CODE = cmb_SUBMISSION_TYPE_CODE.SelectedItemValue
                End If
                'End of Tambahan 8-Jul-2021

                'Profile Data
                If cmb_CUSTOMER_TYPE.SelectedItemValue = 1 Then     'Individual
                    .INDV_NAME = Trim(txt_INDV_NAME.Value)
                    .INDV_PLACEOFBIRTH = Trim(txt_INDV_PLACEOFBIRTH.Value)

                    If Not CDate(txt_INDV_DOB.Value) = DateTime.MinValue Then
                        .INDV_DOB = CDate(txt_INDV_DOB.Value)
                    Else
                        .INDV_DOB = Nothing
                    End If

                    .INDV_ADDRESS = Trim(txt_INDV_ADDRESS.Value)
                    .INDV_FK_goAML_Ref_Nama_Negara_CODE = cmb_INDV_NATIONALITY.SelectedItemValue

                    'Kosongkan data Corporate
                    ' 21 Dec 2021 Ari : Jika data nya dia empty bukan null di db nya maka ada kondisi set datanya jadi empty juga bukan null
                    If .CORP_NAME Is Nothing Then
                        .CORP_NAME = Nothing
                    ElseIf String.IsNullOrEmpty(.CORP_NAME) Then
                        .CORP_NAME = .CORP_NAME
                    End If
                    ' 21 Dec 2021 Ari : Jika data nya dia empty bukan null di db nya maka ada kondisi set datanya jadi empty juga bukan null
                    If .CORP_NPWP Is Nothing Then
                        .CORP_NPWP = Nothing
                    ElseIf String.IsNullOrEmpty(.CORP_NPWP) Then
                        .CORP_NPWP = .CORP_NPWP
                    End If
                    ' 21 Dec 2021 Ari : Jika data nya dia empty bukan null di db nya maka ada kondisi set datanya jadi empty juga bukan null
                    If .CORP_NO_IZIN_USAHA Is Nothing Then
                        .CORP_NO_IZIN_USAHA = Nothing
                    ElseIf String.IsNullOrEmpty(.CORP_NO_IZIN_USAHA) Then
                        .CORP_NO_IZIN_USAHA = .CORP_NO_IZIN_USAHA
                    End If
                    ' 21 Dec 2021 Ari : Jika data nya dia empty bukan null di db nya maka ada kondisi set datanya jadi empty juga bukan null
                    If .CORP_FK_goAML_Ref_Nama_Negara_CODE Is Nothing Then
                        .CORP_FK_goAML_Ref_Nama_Negara_CODE = Nothing
                    ElseIf String.IsNullOrEmpty(.CORP_FK_goAML_Ref_Nama_Negara_CODE) Then
                        .CORP_FK_goAML_Ref_Nama_Negara_CODE = .CORP_FK_goAML_Ref_Nama_Negara_CODE
                    End If

                ElseIf cmb_CUSTOMER_TYPE.SelectedItemValue = 2 Then     'Corporate
                        .CORP_NAME = Trim(txt_CORP_NAME.Value)
                    .CORP_NPWP = Trim(txt_CORP_NPWP.Value)
                    .CORP_NO_IZIN_USAHA = Trim(txt_CORP_NO_IZIN_USAHA.Value)
                    .CORP_FK_goAML_Ref_Nama_Negara_CODE = cmb_CORP_COUNTRY.SelectedItemValue

                    'Kosongkan data Individual
                    ' 21 Dec 2021 Ari : Jika data nya dia empty bukan null di db nya maka ada kondisi set datanya jadi empty juga bukan null

                    If .INDV_NAME Is Nothing Then
                        .INDV_NAME = Nothing
                    ElseIf String.IsNullOrEmpty(.INDV_NAME) Then
                        .INDV_NAME = ""
                    End If
                    ' 21 Dec 2021 Ari : Jika data nya dia empty bukan null di db nya maka ada kondisi set datanya jadi empty juga bukan null
                    If .INDV_PLACEOFBIRTH Is Nothing Then
                        .INDV_PLACEOFBIRTH = Nothing
                    ElseIf String.IsNullOrEmpty(.INDV_PLACEOFBIRTH) Then
                        .INDV_PLACEOFBIRTH = .INDV_PLACEOFBIRTH
                    End If
                    ' 21 Dec 2021 Ari : Jika data nya dia empty bukan null di db nya maka ada kondisi set datanya jadi empty juga bukan null
                    If .INDV_DOB Is Nothing Then
                        .INDV_DOB = Nothing
                    ElseIf String.IsNullOrEmpty(.INDV_DOB) Then
                        .INDV_DOB = .INDV_DOB
                    End If
                    ' 21 Dec 2021 Ari : Jika data nya dia empty bukan null di db nya maka ada kondisi set datanya jadi empty juga bukan null
                    If .INDV_ADDRESS Is Nothing Then
                        .INDV_ADDRESS = Nothing
                    ElseIf String.IsNullOrEmpty(.INDV_ADDRESS) Then
                        .INDV_ADDRESS = .INDV_ADDRESS
                    End If
                    ' 21 Dec 2021 Ari : Jika data nya dia empty bukan null di db nya maka ada kondisi set datanya jadi empty juga bukan null
                    If .INDV_FK_goAML_Ref_Nama_Negara_CODE Is Nothing Then
                        .INDV_FK_goAML_Ref_Nama_Negara_CODE = Nothing
                    ElseIf String.IsNullOrEmpty(.INDV_FK_goAML_Ref_Nama_Negara_CODE) Then
                        .INDV_FK_goAML_Ref_Nama_Negara_CODE = .INDV_FK_goAML_Ref_Nama_Negara_CODE
                    End If

                End If

            End With

            ' 21 Dec 2021 Ari : get data organsisasi code untuk profile account di comment karena value global parameter tidak ada yang sama dengan organisasi code di db, sedangkan di db data profile account value organsisasi code seperti yang di global parameter
            ''Isi default PJK_ID dengan default value
            'Dim strPJKID As String = Nothing
            'Using objSipendar As New SiPendarDAL.SiPendarEntities
            '    Dim objGlobalParameter = objSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault
            '    If Not objGlobalParameter Is Nothing Then
            '        Dim objOrganisasi = objSipendar.SIPENDAR_ORGANISASI.Where(Function(x) x.ORGANISASI_CODE = objGlobalParameter.ParameterValue).FirstOrDefault
            '        If objOrganisasi IsNot Nothing Then
            '            strPJKID = objOrganisasi.ORGANISASI_CODE
            '        End If
            '    End If
            'End Using

            'Untuk tujuan validasi isikan semua SIPENDAR_PROFILE_ID jadi -1
            With objSIPENDAR_PROFILE_CLASS
                Dim pkProfileID As Long = .objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID

                For Each item1 In .objList_SIPENDAR_PROFILE_ACCOUNT
                    item1.FK_SIPENDAR_PROFILE_ID = pkProfileID
                    ' 21 Dec 2021 Ari : jika fk sipendar organsisasi code di db nya empty/nothing maka akan diisi value db jika tidak diisi dari global param
                    Using objSipendar As New SiPendarDAL.SiPendarEntities
                        Dim objGlobalParameter = objSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault

                        If Not String.IsNullOrEmpty(item1.FK_SIPENDAR_ORGANISASI_CODE) Then
                            item1.FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
                        Else
                            item1.FK_SIPENDAR_ORGANISASI_CODE = item1.FK_SIPENDAR_ORGANISASI_CODE
                        End If
                    End Using
                Next
                For Each item2 In .objList_SIPENDAR_PROFILE_ACCOUNT_ATM
                    item2.FK_SIPENDAR_PROFILE_ID = pkProfileID
                Next
                For Each item3 In .objList_SIPENDAR_PROFILE_ADDRESS
                    item3.FK_SIPENDAR_PROFILE_ID = pkProfileID
                Next
                For Each item4 In .objList_SIPENDAR_PROFILE_PHONE
                    item4.FK_SIPENDAR_PROFILE_ID = pkProfileID
                Next
                For Each item5 In .objList_SIPENDAR_PROFILE_IDENTIFICATION
                    item5.FK_SIPENDAR_PROFILE_ID = pkProfileID
                Next

            End With

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_Back_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF DOCUMENT SAVE

    Protected Sub btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SetCommandColumnLocation()
        ColumnActionLocation(gp_SIPENDAR_PROFILE_ACCOUNT, cc_SIPENDAR_PROFILE_ACCOUNT)
        ColumnActionLocation(gp_SIPENDAR_PROFILE_ACCOUNT_ATM, cc_SIPENDAR_PROFILE_ACCOUNT_ATM)
        ColumnActionLocation(gp_SIPENDAR_PROFILE_ADDRESS, cc_SIPENDAR_PROFILE_ADDRESS)
        ColumnActionLocation(gp_SIPENDAR_PROFILE_PHONE, cc_SIPENDAR_PROFILE_PHONE)
        ColumnActionLocation(gp_SIPENDAR_PROFILE_IDENTIFICATION, cc_SIPENDAR_PROFILE_IDENTIFICATION)
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub SetControlVisibility()
        Try
            '3-Jul-2021 Adi : Hide field yang awalnya ada di XML tapi di XSD tidak ada
            cmb_TINDAK_PIDANA_CODE.IsHidden = True
            cmb_ORGANISASI_CODE.IsHidden = True
            txt_INDV_ADDRESS.Hidden = True


            'Sesuaikan dengan SIPENDAR_TYPE
            If Not String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                    gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = False
                    gp_SIPENDAR_PROFILE_PHONE.Hidden = False
                    cmb_JENIS_WATCHLIST_CODE.IsHidden = False
                    cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = False
                    cmb_SUMBER_TYPE_CODE.IsHidden = True
                    txt_SUMBER_ID.Hidden = True
                    txt_Similarity.Hidden = True
                Else
                    gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = True
                    gp_SIPENDAR_PROFILE_PHONE.Hidden = True
                    cmb_JENIS_WATCHLIST_CODE.IsHidden = True
                    cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = True
                    cmb_SUMBER_TYPE_CODE.IsHidden = False
                    txt_SUMBER_ID.Hidden = False
                    txt_Similarity.Hidden = False
                End If
            End If

            ' 1 Dec 2021 Ari penambahan untuk validasi customer type
            If objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.Fk_goAML_Ref_Customer_Type_id = 2 Then
                gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = True
                gp_SIPENDAR_PROFILE_PHONE.Hidden = True
            Else
                gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = False
                gp_SIPENDAR_PROFILE_PHONE.Hidden = False
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function IsNoChanges() As Boolean
        Dim objSIPENDAR_PROFILE_CLASS_Old = SiPendarProfile_BLL.GetSiPendarProfileClassByID(IDUnik)
        With objSIPENDAR_PROFILE_CLASS_Old.objSIPENDAR_PROFILE

            .INDV_NAME = IIf(Trim(.INDV_NAME) = "", Nothing, Trim(.INDV_NAME))
            .INDV_PLACEOFBIRTH = IIf(Trim(.INDV_PLACEOFBIRTH) = "", Nothing, Trim(.INDV_PLACEOFBIRTH))
            .INDV_ADDRESS = IIf(Trim(.INDV_ADDRESS) = "", Nothing, Trim(.INDV_ADDRESS))

            .CORP_NAME = IIf(Trim(.CORP_NAME) = "", Nothing, Trim(.CORP_NAME))
            .CORP_NO_IZIN_USAHA = IIf(Trim(.CORP_NO_IZIN_USAHA) = "", Nothing, Trim(.CORP_NO_IZIN_USAHA))
            .CORP_NPWP = IIf(Trim(.CORP_NPWP) = "", Nothing, Trim(.CORP_NPWP))

            .Keterangan = IIf(Trim(.Keterangan) = "", Nothing, Trim(.Keterangan))
            Dim similarity As String = Format(.Similarity, "#,##0.00")
            If Not String.IsNullOrEmpty(similarity) Then
                .Similarity = similarity
            Else
                .Similarity = .Similarity
            End If

        End With

        Dim objXMLData As String = NawaBLL.Common.Serialize(objSIPENDAR_PROFILE_CLASS)
        Dim objXMLData_Old As String = NawaBLL.Common.Serialize(objSIPENDAR_PROFILE_CLASS_Old)

        If objXMLData = objXMLData_Old Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub SIPENDAR_PROFILE_Add_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Update
    End Sub

    Protected Function GetCustomerClosedStatus(strGCN As String) As DataRow
        Try

            Dim param(0) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@GCN"
            param(0).Value = strGCN
            param(0).DbType = SqlDbType.VarChar

            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_GetCustomerClosedStatus_ByGCN", param)

            Return drResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    'Protected Function GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID As String) As DataTable
    '    Try
    '        Dim data As New DataTable

    '        Dim stringquery As String = " SELECT result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID, result.Nama, result.DOB, result.Birth_Place, result.Identity_Number, result.FK_WATCHLIST_ID_PPATK, result.GCN, " &
    '        " STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'') as CIF, result.NAMACUSTOMER, result.DOBCustomer, result.Birth_PlaceCustomer, result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer, " &
    '        " result.Identity_NumberCustomer, result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE, (result.TotalSimilarity*100) AS TotalSimilarityPct, result.Stakeholder_Role, " &
    '        " case when result.Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation " &
    '        " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
    '        " WHERE result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = " & PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
    '        stringquery = stringquery & " ORDER BY TotalSimilarityPct Desc, result.GCN, result.Nama"
    '        data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)

    '        Return data
    '    Catch ex As Exception
    '        Throw ex
    '        'Return New DataTable
    '    End Try
    'End Function

#Region "22 Nov 2021 Ari Penambahan Grid Screening Result"

    Protected Sub Show_ScreeningResult()
        Try
            Dim intID As Integer = Convert.ToInt32(Session("FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID"))
            Session("FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID") = Nothing
            If intID > 0 Then
                Dim dtResult As DataTable = New DataTable
                Dim status As String = Session("SipendarOtherInfoStatus")
                Session("SipendarOtherInfoStatus") = Nothing

                If status IsNot Nothing Then
                    Dim paramLoadResultGroup(0) As SqlParameter

                    paramLoadResultGroup(0) = New SqlParameter
                    paramLoadResultGroup(0).ParameterName = "@PK"
                    paramLoadResultGroup(0).Value = intID
                    paramLoadResultGroup(0).DbType = SqlDbType.VarChar
                    Dim data As New DataTable
                    data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_GROUP_RESULT_BY_PK_With_Status_Not_Null", paramLoadResultGroup)
                    For Each row As DataRow In data.Rows
                        If row.Item("Stakeholder_Role") IsNot Nothing And Not IsDBNull(row.Item("Stakeholder_Role")) Then
                            ' 18 Nov 2021 Ari pengambahan grid link CIF
                            GridPanelLinkCIF.Hidden = False
                            Dim stringgcn As String = Convert.ToString(row("GCN"))
                            Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                            Dim dtLinkCIF As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Account(Convert.ToString(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
                            GridPanelLinkCIF.GetStore.DataSource = dtLinkCIF
                            GridPanelLinkCIF.GetStore.DataBind()
                            GridPanelLinkCIF.Hidden = False
                        End If
                    Next
                Else
                    Dim paramLoadResultGroup(0) As SqlParameter

                    paramLoadResultGroup(0) = New SqlParameter
                    paramLoadResultGroup(0).ParameterName = "@PK"
                    paramLoadResultGroup(0).Value = intID
                    paramLoadResultGroup(0).DbType = SqlDbType.VarChar
                    Dim data As New DataTable
                    data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_GROUP_RESULT_BY_PK", paramLoadResultGroup)
                    For Each row As DataRow In data.Rows
                        If Not String.IsNullOrEmpty(row.Item("Stakeholder_Role").ToString) Then
                            ' 18 Nov 2021 Ari pengambahan grid link CIF
                            GridPanelLinkCIF.Hidden = False
                            Dim stringgcn As String = Convert.ToString(row("GCN"))
                            Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                            Dim dtLinkCIF As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Account(Convert.ToString(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
                            GridPanelLinkCIF.GetStore.DataSource = dtLinkCIF
                            GridPanelLinkCIF.GetStore.DataBind()

                        End If
                    Next

                End If
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer(Unique_Key As String) As DataRow
        Try
            Unique_Key = Replace(Unique_Key, "'", "''")
            Dim stringquery As String = " select top 1 * from SIPENDAR_StakeHolder_NonCustomer where Unique_Key = '" & Unique_Key & "' "
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery, Nothing)
            Return drResult
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function
    Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Identifications(FK_SIPENDAR_StakeHolder_NonCustomer_ID As String) As DataTable
        Try
            Dim data As New DataTable
            Dim stringquery As String = " select Type as Identity_Type, Number as Identity_Number, Issue_Date, Expiry_Date, Issued_By, Issued_Country, Comments " &
                " from SIPENDAR_StakeHolder_NonCustomer_Identifications where FK_SIPENDAR_StakeHolder_NonCustomer_ID = " & FK_SIPENDAR_StakeHolder_NonCustomer_ID
            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function

    Protected Function GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID As String) As DataTable
        Try
            Dim data As New DataTable
            'Dim stringquery As String = " SELECT result.*, (result.TotalSimilarity*100) AS TotalSimilarityPct, custaccount.[CountAccount], case when custaccount.CountAccount > 0 " &
            '" then 'Yes' else 'No' end as IsHaveActiveAccount FROM SIPENDAR_SCREENING_REQUEST_RESULT result left join ( select distinct cust.GCN, count(cust.GCN) as [CountAccount] " &
            '" from goAML_Ref_Customer cust join goAML_Ref_Account acc on acc.client_number =  cust.CIF join SIPENDAR_SCREENING_REQUEST_RESULT resultget on resultget.GCN = cust.GCN where " &
            '" resultget.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID=" & PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID & " and acc.status_code <> 'TTP' and resultget.GCN is not null group by cust.GCN ) " &
            '" custaccount on result.GCN = custaccount.GCN WHERE result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID=" & PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID & " ORDER BY TotalSimilarityPct Desc, " &
            '" result.GCN, result.Nama"

            'Dim stringquery As String = ""
            'stringquery = "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct, "
            'stringquery += "custaccount.[CountAccount], "
            'stringquery += "case when custaccount.CountAccount > 0 "
            'stringquery += "then 'Yes' else 'No' end as IsHaveActiveAccount "
            'stringquery += "FROM SIPENDAR_SCREENING_REQUEST_RESULT result "
            'stringquery += "Left Join( "
            'stringquery += " Select distinct cust.GCN, count(cust.GCN) As [CountAccount] "
            'stringquery += " From goAML_Ref_Customer cust "
            'stringquery += " Join goAML_Ref_Account acc "
            'stringquery += " On acc.client_number =  cust.CIF "
            'stringquery += " Join SIPENDAR_SCREENING_REQUEST_RESULT resultget "
            'stringquery += " On resultget.GCN = cust.GCN "
            'stringquery += " where "
            'stringquery += "resultget.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = " & PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID & ""
            'stringquery += " And acc.status_code <> 'TTP'  "
            'stringquery += " And resultget.GCN Is Not null group by cust.GCN ) custaccount "
            'stringquery += " On result.GCN = custaccount.GCN "
            'stringquery += "left join SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO b "
            'stringquery += "On result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = b.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID "
            'stringquery += "WHERE result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID In "
            'stringquery += "(Select distinct PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID "
            'stringquery += "from SIPENDAR_SCREENING_REQUEST_RESULT "
            'stringquery += "where PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID =" & intID & " ) "
            'stringquery += "And result.GCN Is Not null  "
            'stringquery += "And isnull(b.Status,'New') in ('New','Reject','Reject Exclusion') "
            'stringquery += "ORDER BY TotalSimilarityPct Desc, result.GCN, Nama "

            'data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_GROUP_RESULT_BY_PK", paramLoadResultGroup)

            Dim paramLoadResultGroup(0) As SqlParameter

            paramLoadResultGroup(0) = New SqlParameter
            paramLoadResultGroup(0).ParameterName = "@PK"
            paramLoadResultGroup(0).Value = PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
            paramLoadResultGroup(0).DbType = SqlDbType.VarChar

            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_GROUP_RESULT_BY_PK", paramLoadResultGroup)
            data.Columns.Add(New Data.DataColumn("Account_Name", GetType(String)))
            data.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            data.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))
            Dim data2 As New DataTable
            For Each row As Data.DataRow In data.Rows
                If Not String.IsNullOrEmpty(row.Item("Stakeholder_Role").ToString) Then
                    row.Item("Status") = "Non Customer"

                    ' 19 Nov 2021 Penmbahan untuk link CIF
                    Dim stringgcn As String = Convert.ToString(row("GCN"))
                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                    If drResult IsNot Nothing Then
                        Dim query As String = "DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) " &
                                              "INSERT INTO @tblaccount ( Groupaccount ) " &
                                              "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID") &
                                              " Select STUFF((SELECT ','  + cast(Groupaccount as nvarchar) from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_No, " &
                                              " STUFF((SELECT  ','  +  CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),'') WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),'') " &
                                              " Else '' end from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif " &
                                              " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_Name, STUFF((SELECT ','  + isnull(cast(rc.cif as nvarchar),'') from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as CIF_No "
                        data2 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)
                        For Each row2 As DataRow In data2.Rows
                            row("Account_Name") = row2("Account_Name")
                            row("Account_No") = row2("Account_No")
                            row("CIF_No") = row2("CIF_No")
                        Next
                    Else
                        row("Account_Name") = Nothing
                        row("Account_No") = Nothing
                        row("CIF_No") = Nothing
                    End If

                Else
                    row.Item("Status") = "Customer"
                    row("Account_Name") = Nothing
                    row("Account_No") = Nothing
                    row("CIF_No") = Nothing
                End If
            Next
            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function


    Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Account(FK_SIPENDAR_StakeHolder_NonCustomer_ID As String) As DataTable
        Try
            Dim data As New DataTable
            Dim stringquery As String = " DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) INSERT INTO @tblaccount (Groupaccount) " &
                "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & FK_SIPENDAR_StakeHolder_NonCustomer_ID &
                " select Groupaccount as Account_No, rc.cif as CIF_No , CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),null)   WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),null) else null end as Account_Name from @tblaccount sn" &
                " left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif"
            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function


#End Region

End Class
