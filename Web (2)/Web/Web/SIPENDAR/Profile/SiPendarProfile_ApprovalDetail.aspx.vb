﻿Imports Ext
Imports SiPendarDAL
Imports Elmah
Imports System.Data
Imports SiPendarBLL
Imports System.Data.SqlClient '' Felix 24-Sep-2021
Imports OfficeOpenXml
Imports System.Reflection

Partial Class SiPendarProfile_ApprovalDetail
    Inherits ParentPage

    Public Property IDModule() As String
        Get
            Return Session("SiPendarProfile_ApprovalDetail.IDModule")
        End Get
        Set(ByVal value As String)
            Session("SiPendarProfile_ApprovalDetail.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("SiPendarProfile_ApprovalDetail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("SiPendarProfile_ApprovalDetail.IDUnik") = value
        End Set
    End Property

    Public Property objApproval() As NawaDAL.ModuleApproval
        Get
            Return Session("SiPendarProfile_ApprovalDetail.objApproval")
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            Session("SiPendarProfile_ApprovalDetail.objApproval") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_CLASS() As SiPendarBLL.SIPENDAR_PROFILE_CLASS
        Get
            Return Session("SiPendarProfile_ApprovalDetail.objSIPENDAR_PROFILE_CLASS")
        End Get
        Set(ByVal value As SiPendarBLL.SIPENDAR_PROFILE_CLASS)
            Session("SiPendarProfile_ApprovalDetail.objSIPENDAR_PROFILE_CLASS") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_CLASS_BEFORE() As SiPendarBLL.SIPENDAR_PROFILE_CLASS
        Get
            Return Session("SiPendarProfile_ApprovalDetail.objSIPENDAR_PROFILE_CLASS_BEFORE")
        End Get
        Set(ByVal value As SiPendarBLL.SIPENDAR_PROFILE_CLASS)
            Session("SiPendarProfile_ApprovalDetail.objSIPENDAR_PROFILE_CLASS_BEFORE") = value
        End Set
    End Property

    '' Added by Felix 14-Sep-2021
    Public Property ListIndikatorLaporan As List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
        Get
            If Session("SiPendarProfile_ApprovalDetail.ListIndikatorLaporan") Is Nothing Then
                Session("SiPendarProfile_ApprovalDetail.ListIndikatorLaporan") = New List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
            End If
            Return Session("SiPendarProfile_ApprovalDetail.ListIndikatorLaporan")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
            Session("SiPendarProfile_ApprovalDetail.ListIndikatorLaporan") = value
        End Set
    End Property
    '' End of 14-Sep-2021
    '' Start 24-Sep-2021
    Public Property IDScreeningJudgement() As Long
        Get
            Return Session("SiPendarProfile_ApprovalDetail.IDScreeningJudgement")
        End Get
        Set(ByVal value As Long)
            Session("SiPendarProfile_ApprovalDetail.IDScreeningJudgement") = value
        End Set
    End Property
    Public Property objModuleScreeningJudgement() As NawaDAL.Module
        Get
            Return Session("SiPendarProfile_ApprovalDetail.objModuleScreeningJudgement")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("SiPendarProfile_ApprovalDetail.objModuleScreeningJudgement") = value
        End Set
    End Property
    '' End 24-Sep-2021

    '' Start 20-Jul-2022 Penambhan atas Request ANZ
    Public Property TotalScreeningResult As Integer
        Get
            Return Session("SiPendarProfile_ApprovalDetail.TotalScreeningResult")
        End Get
        Set(value As Integer)
            Session("SiPendarProfile_ApprovalDetail.TotalScreeningResult") = value
        End Set
    End Property
    '' End 20-Jul-2022

    Sub ClearSession()
        objApproval = Nothing
        objSIPENDAR_PROFILE_CLASS = New SIPENDAR_PROFILE_CLASS
        objSIPENDAR_PROFILE_CLASS_BEFORE = New SIPENDAR_PROFILE_CLASS
        ListIndikatorLaporan = New List(Of SIPENDAR_Report_Indicator) '' Added by Felix 14-Sep-2021
        IDScreeningJudgement = 0 ''Add 21-Dec-2021
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As New Integer
            If IDModule IsNot Nothing Then
                intModuleID = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()

                FormPanelInput.Title = ObjModule.ModuleLabel & " - Approval"

                LoadModuleApproval()

                If Not String.IsNullOrEmpty(objApproval.ModuleFieldBefore) Then
                    LoadDataBefore()
                End If

                If Not String.IsNullOrEmpty(objApproval.ModuleField) Then
                    LoadDataAfter()
                End If

                'Load Approval History Judgement
                LoadApprovalHistoryJudgement() '' Add 24-Sep-2021 Felix

                'Load Screening Result (If there is any)
                Show_ScreeningResult()

                'Set command column location (Left/Right)
                SetCommandColumnLocation()

                SetControlReadOnly()

                SetControlVisibility()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    '------- LOAD MODULE APPROVAL
    Protected Sub LoadModuleApproval()
        Try
            objApproval = NawaBLL.ModuleApprovalBLL.GetModuleApprovalByID(IDUnik)
            If Not objApproval Is Nothing Then
                txt_ModuleApproval_ID.Value = objApproval.PK_ModuleApproval_ID
                Dim objModuleToApprove = NawaBLL.ModuleBLL.GetModuleByModuleName(objApproval.ModuleName)
                If objModuleToApprove IsNot Nothing Then
                    txt_ModuleLabel.Value = objModuleToApprove.ModuleLabel
                End If
                txt_ModuleKey.Value = objApproval.ModuleKey
                txt_Action.Value = NawaBLL.ModuleBLL.GetModuleActionNamebyID(objApproval.PK_ModuleAction_ID)

                Dim objUser = NawaBLL.MUserBLL.GetMuserbyUSerId(objApproval.CreatedBy)
                If objUser IsNot Nothing Then
                    txt_CreatedBy.Value = objApproval.CreatedBy & " - " & objUser.UserName
                Else
                    txt_CreatedBy.Value = objApproval.CreatedBy
                End If

                'daniel 2021/08/19 tostring tidak dapat menghandle null value
                If objApproval.CreatedDate IsNot Nothing Then
                    txt_CreatedDate.Value = objApproval.CreatedDate.Value.ToString("dd-MMM-yyyy")
                End If

                'Set Panel Width
                If objApproval.PK_ModuleAction_ID = 1 Then
                    pnlDataBefore.Hidden = True
                    pnlDataAfter.ColumnWidth = 1
                    pnlDataAfter.Title = "New Data"
                ElseIf objApproval.PK_ModuleAction_ID = 3 Then
                    pnlDataAfter.Hidden = True
                    pnlDataBefore.ColumnWidth = 1
                End If

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF LOAD MODULE APPROVAL

    '------- LOAD DATA BEFORE
    Protected Sub LoadDataBefore()
        Try
            If Not String.IsNullOrEmpty(objApproval.ModuleFieldBefore) Then
                objSIPENDAR_PROFILE_CLASS_BEFORE = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(SiPendarBLL.SIPENDAR_PROFILE_CLASS))
            End If

            LoadSiPendarProfile_Before()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadSiPendarProfile_Before()
        Try
            Using objDB As New SiPendarDAL.SiPendarEntities
                'Bind Data Header
                'objSIPENDAR_PROFILE_CLASS_BEFORE = Nothing
                'Object reference Not set to an instance of an object.
                'objSIPENDAR_PROFILE_CLASS_BEFORE.objSIPENDAR_PROFILE = Nothing
                'Object reference Not set to an instance of an object.
                If objSIPENDAR_PROFILE_CLASS_BEFORE IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS_BEFORE.objSIPENDAR_PROFILE IsNot Nothing Then
                    With objSIPENDAR_PROFILE_CLASS_BEFORE.objSIPENDAR_PROFILE
                        'General Information
                        If Not IsNothing(.FK_SIPENDAR_TYPE_ID) Then
                            Dim objSiPendarType = objDB.SIPENDAR_TYPE.Where(Function(x) x.PK_SIPENDAR_TYPE_id = .FK_SIPENDAR_TYPE_ID).FirstOrDefault
                            If objSiPendarType IsNot Nothing Then
                                cmb_SIPENDAR_TYPE_ID_Before.SetTextWithTextValue(objSiPendarType.PK_SIPENDAR_TYPE_id, objSiPendarType.SIPENDAR_TYPE_NAME)
                            End If
                        End If
                        If Not IsNothing(.Fk_goAML_Ref_Customer_Type_id) Then
                            Dim objCustomerType = SiPendarProfile_BLL.GetCustomerTypeByID(.Fk_goAML_Ref_Customer_Type_id)
                            If objCustomerType IsNot Nothing Then
                                cmb_CUSTOMER_TYPE_Before.SetTextWithTextValue(objCustomerType.PK_Customer_Type_ID, objCustomerType.Description)
                            End If
                        End If

                        'Tambahan 8-Jul-2021
                        If Not IsNothing(.FK_SIPENDAR_SUBMISSION_TYPE_CODE) Then
                            Dim objSubmissionType = objDB.SIPENDAR_SUBMISSION_TYPE.Where(Function(x) x.Kode = .FK_SIPENDAR_SUBMISSION_TYPE_CODE).FirstOrDefault
                            If objSubmissionType IsNot Nothing Then
                                cmb_SUBMISSION_TYPE_CODE_Before.SetTextWithTextValue(objSubmissionType.Kode, objSubmissionType.Keterangan)
                            End If
                        End If
                        'End of Tambahan 8-Jul-2021

                        'SIPENDAR PROAKTIF
                        If Not IsNothing(.FK_SIPENDAR_JENIS_WATCHLIST_CODE) Then
                            Dim objJenisWatchlist = objDB.SIPENDAR_JENIS_WATCHLIST.Where(Function(x) x.SIPENDAR_JENIS_WATCHLIST_CODE = .FK_SIPENDAR_JENIS_WATCHLIST_CODE).FirstOrDefault
                            If objJenisWatchlist IsNot Nothing Then
                                cmb_JENIS_WATCHLIST_CODE_Before.SetTextWithTextValue(objJenisWatchlist.SIPENDAR_JENIS_WATCHLIST_CODE, objJenisWatchlist.KETERANGAN)
                            End If
                        End If
                        If Not IsNothing(.FK_SIPENDAR_TINDAK_PIDANA_CODE) Then
                            Dim objTindakPidana = objDB.SiPendar_Tindak_Pidana.Where(Function(x) x.SiPendar_Tindak_Pidana_Code = .FK_SIPENDAR_TINDAK_PIDANA_CODE).FirstOrDefault
                            If objTindakPidana IsNot Nothing Then
                                cmb_TINDAK_PIDANA_CODE_Before.SetTextWithTextValue(objTindakPidana.SiPendar_Tindak_Pidana_Code, objTindakPidana.Keterangan)
                            End If
                        End If
                        If Not IsNothing(.FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE) Then
                            Dim objSumberInformasiKhusus = objDB.SiPendar_Sumber_Informasi_Khusus.Where(Function(x) x.SiPendar_Sumber_Informasi_Khusus_Code = .FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE).FirstOrDefault
                            If objSumberInformasiKhusus IsNot Nothing Then
                                cmb_SUMBER_INFORMASI_KHUSUS_CODE_Before.SetTextWithTextValue(objSumberInformasiKhusus.SiPendar_Sumber_Informasi_Khusus_Code, objSumberInformasiKhusus.Keterangan)
                            End If
                        End If
                        If Not IsNothing(.FK_SIPENDAR_ORGANISASI_CODE) Then
                            Dim objSiPendarOrganisasi = objDB.SIPENDAR_ORGANISASI.Where(Function(x) x.ORGANISASI_CODE = .FK_SIPENDAR_ORGANISASI_CODE).FirstOrDefault
                            If objSiPendarOrganisasi IsNot Nothing Then
                                cmb_ORGANISASI_CODE_Before.SetTextWithTextValue(objSiPendarOrganisasi.ORGANISASI_CODE, objSiPendarOrganisasi.ORGANISASI_NAME)
                            End If
                        End If

                        'SIPENDAR PENGAYAAN
                        If Not IsNothing(.FK_SIPENDAR_SUMBER_TYPE_CODE) Then
                            Dim objSiPendarSumberType = objDB.SIPENDAR_SUMBER_TYPE.Where(Function(x) x.SUMBER_TYPE_CODE = .FK_SIPENDAR_SUMBER_TYPE_CODE).FirstOrDefault
                            If objSiPendarSumberType IsNot Nothing Then
                                cmb_SUMBER_TYPE_CODE_Before.SetTextWithTextValue(objSiPendarSumberType.SUMBER_TYPE_CODE, objSiPendarSumberType.SUMBER_TYPE_NAME)
                            End If
                        End If
                        txt_SUMBER_ID_Before.Value = .SUMBER_ID
                        txt_Similarity_Before.Value = .Similarity

                        txt_GCN_Before.Value = .GCN
                        txt_Keterangan_Before.Value = .Keterangan

                        If .FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID IsNot Nothing Then
                            Dim dtResult As DataTable = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID)
                            If dtResult IsNot Nothing AndAlso dtResult.Rows.Count > 0 Then
                                Dim drResult As DataRow = dtResult.Rows(0)
                                If drResult IsNot Nothing Then
                                    If IsDBNull(drResult("Stakeholder_Role")) Then
                                        txt_CustomerType_Before.Text = "Customer"
                                        txt_role_noncustomer_Before.Hidden = True
                                    Else
                                        txt_CustomerType_Before.Text = "Non-Customer"
                                        txt_role_noncustomer_Before.Text = drResult("Stakeholder_Role")
                                        txt_role_noncustomer_Before.Hidden = False
                                        fs_Individual_Before_IsHaveActiveAccount_Head.Hidden = True
                                        fs_Corporate_Before_IsHaveActiveAccount_Head.Hidden = True
                                    End If
                                End If
                            End If
                        Else
                            txt_CustomerType_Before.Text = "Customer"
                            txt_role_noncustomer_Before.Hidden = True
                        End If

                        'Customer Information
                        'Hide/Show FieldSet Individual/Korporasi
                        fs_Individual_Before.Hidden = True
                        fs_Corporate_Before.Hidden = True

                        Dim customerstatusclosed As String = ""
                        Dim drCustomerClosed As DataRow = GetCustomerClosedStatus(txt_GCN_Before.Value)
                        If drCustomerClosed IsNot Nothing Then
                            If Not IsDBNull(drCustomerClosed("IsClosed")) Then
                                If drCustomerClosed("IsClosed") = 0 Then
                                    customerstatusclosed = "Active"
                                Else
                                    If Not IsDBNull(drCustomerClosed("ClosedDate")) Then
                                        customerstatusclosed = "Closed ( " & CDate(drCustomerClosed("ClosedDate")).ToString("dd-MMM-yyyy") & " )"
                                    Else
                                        customerstatusclosed = "Closed"
                                    End If
                                End If
                            End If
                        Else
                            customerstatusclosed = "Customer GCN Not Found"
                        End If

                        If .Fk_goAML_Ref_Customer_Type_id = 1 Then      'Individual
                            fs_Individual_Before.Hidden = False

                            txt_INDV_NAME_Before.Value = .INDV_NAME
                            txt_INDV_PLACEOFBIRTH_Before.Value = .INDV_PLACEOFBIRTH
                            If .INDV_DOB.HasValue Then
                                txt_INDV_DOB_Before.Value = .INDV_DOB.Value.ToString("dd-MMM-yyyy") '' Edited by Felix 23 Aug 2021. Tambah .Value.ToString("dd-MMM-yyyy"
                            End If
                            txt_INDV_ADDRESS_Before.Value = .INDV_ADDRESS

                            If Not IsNothing(.INDV_FK_goAML_Ref_Nama_Negara_CODE) Then
                                Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(.INDV_FK_goAML_Ref_Nama_Negara_CODE)
                                If objCountry IsNot Nothing Then
                                    cmb_INDV_NATIONALITY_Before.SetTextWithTextValue(objCountry.Kode, objCountry.Keterangan)
                                End If
                            End If
                            fs_Individual_Before_IsHaveActiveAccount.Text = customerstatusclosed
                        Else    'Korporasi
                            fs_Corporate_Before.Hidden = False

                            txt_CORP_NAME_Before.Value = .CORP_NAME
                            txt_CORP_NPWP_Before.Value = .CORP_NPWP
                            txt_CORP_NO_IZIN_USAHA_Before.Value = .CORP_NO_IZIN_USAHA

                            If Not IsNothing(.CORP_FK_goAML_Ref_Nama_Negara_CODE) Then
                                Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(.CORP_FK_goAML_Ref_Nama_Negara_CODE)
                                If objCountry IsNot Nothing Then
                                    cmb_CORP_COUNTRY_Before.SetTextWithTextValue(objCountry.Kode, objCountry.Keterangan)
                                End If
                            End If
                            gp_SIPENDAR_PROFILE_PHONE_Before.Hidden = True
                            gp_SIPENDAR_PROFILE_PHONE.Hidden = True
                            gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = True
                            gp_SIPENDAR_PROFILE_IDENTIFICATION_Before.Hidden = True
                            fs_Corporate_Before_IsHaveActiveAccount.Text = customerstatusclosed
                        End If
                    End With
                End If
            End Using


            'Bind Data Detail to Grid Panel
            Bind_SIPENDAR_PROFILE_ACCOUNT_Before()
            Bind_SIPENDAR_PROFILE_AddRESS_Before()
            Bind_SIPENDAR_PROFILE_PHONE_Before()
            Bind_SIPENDAR_PROFILE_IDENTIFICATION_Before()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_ACCOUNT_Before()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS_BEFORE.objList_SIPENDAR_PROFILE_ACCOUNT)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listJenisRekening As List(Of goAML_Ref_Jenis_Rekening) = Nothing
        Dim listStatusRekening As List(Of goAML_Ref_Status_Rekening) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listJenisRekening = objdb.goAML_Ref_Jenis_Rekening.ToList
            listStatusRekening = objdb.goAML_Ref_Status_Rekening.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("JENIS_REKENING", GetType(String)))
        objtable.Columns.Add(New DataColumn("STATUS_REKENING", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objJenisRekening = SiPendarProfile_BLL.GetJenisRekeningByCode(item("Fk_goAML_Ref_Jenis_Rekening_CODE").ToString)
                'If objJenisRekening IsNot Nothing Then
                '    item("JENIS_REKENING") = objJenisRekening.Keterangan
                'End If

                'Dim objStatusRekening = SiPendarProfile_BLL.GetStatusRekeningByCode(item("Fk_goAML_Ref_Status_Rekening_CODE").ToString)
                'If objStatusRekening IsNot Nothing Then
                '    item("STATUS_REKENING") = objStatusRekening.Keterangan
                'End If

                If listJenisRekening IsNot Nothing Then
                    Dim objJenisRekening = listJenisRekening.Find(Function(x) x.Kode = item("Fk_goAML_Ref_Jenis_Rekening_CODE").ToString)
                    If objJenisRekening IsNot Nothing Then
                        item("JENIS_REKENING") = objJenisRekening.Keterangan
                    End If
                End If

                If listStatusRekening IsNot Nothing Then
                    Dim objStatusRekening = listStatusRekening.Find(Function(x) x.Kode = item("Fk_goAML_Ref_Status_Rekening_CODE").ToString)
                    If objStatusRekening IsNot Nothing Then
                        item("STATUS_REKENING") = objStatusRekening.Keterangan
                    End If
                End If
            Next
        End If

        gp_SIPENDAR_PROFILE_ACCOUNT_Before.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_ACCOUNT_Before.GetStore().DataBind()
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_AddRESS_Before()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS_BEFORE.objList_SIPENDAR_PROFILE_ADDRESS)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listAddressType As List(Of goAML_Ref_Kategori_Kontak) = Nothing
        Dim listCountryName As List(Of goAML_Ref_Nama_Negara) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
            listCountryName = objdb.goAML_Ref_Nama_Negara.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("ADDRESS_TYPE_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objAddressType = SiPendarProfile_BLL.GetKategoriKontakByCode(item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                'If objAddressType IsNot Nothing Then
                '    item("ADDRESS_TYPE_NAME") = objAddressType.Keterangan
                'Else
                '    item("ADDRESS_TYPE_NAME") = Nothing
                'End If

                'Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(item("FK_goAML_Ref_Nama_Negara_CODE").ToString)
                'If objCountry IsNot Nothing Then
                '    item("COUNTRY_NAME") = objCountry.Keterangan
                'Else
                '    item("COUNTRY_NAME") = Nothing
                'End If

                If listAddressType IsNot Nothing Then
                    Dim objAddressType = listAddressType.Find(Function(x) x.Kode = item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                    If objAddressType IsNot Nothing Then
                        item("ADDRESS_TYPE_NAME") = objAddressType.Keterangan
                    End If
                End If

                If listCountryName IsNot Nothing Then
                    Dim objCountry = listCountryName.Find(Function(x) x.Kode = item("FK_goAML_Ref_Nama_Negara_CODE").ToString)
                    If objCountry IsNot Nothing Then
                        item("COUNTRY_NAME") = objCountry.Keterangan
                    End If
                End If
            Next
        End If

        gp_SIPENDAR_PROFILE_ADDRESS_Before.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_ADDRESS_Before.GetStore().DataBind()
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_PHONE_Before()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS_BEFORE.objList_SIPENDAR_PROFILE_PHONE)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listPhoneType As List(Of goAML_Ref_Kategori_Kontak) = Nothing
        Dim listCommunicationType As List(Of goAML_Ref_Jenis_Alat_Komunikasi) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listPhoneType = objdb.goAML_Ref_Kategori_Kontak.ToList
            listCommunicationType = objdb.goAML_Ref_Jenis_Alat_Komunikasi.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("PHONE_TYPE_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("COMMUNICATION_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objPHONEType = SiPendarProfile_BLL.GetKategoriKontakByCode(item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                'If objPHONEType IsNot Nothing Then
                '    item("PHONE_TYPE_NAME") = objPHONEType.Keterangan
                'Else
                '    item("PHONE_TYPE_NAME") = Nothing
                'End If

                'Dim objCommunicationType = SiPendarProfile_BLL.GetJenisAlatKomunikasiByCode(item("FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE").ToString)
                'If objCommunicationType IsNot Nothing Then
                '    item("COMMUNICATION_TYPE_NAME") = objCommunicationType.Keterangan
                'Else
                '    item("COMMUNICATION_TYPE_NAME") = Nothing
                'End If

                If listPhoneType IsNot Nothing Then
                    Dim objPHONEType = listPhoneType.Find(Function(x) x.Kode = item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                    If objPHONEType IsNot Nothing Then
                        item("PHONE_TYPE_NAME") = objPHONEType.Keterangan
                    End If
                End If

                If listCommunicationType IsNot Nothing Then
                    Dim objCommunicationType = listCommunicationType.Find(Function(x) x.Kode = item("FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE").ToString)
                    If objCommunicationType IsNot Nothing Then
                        item("COMMUNICATION_TYPE_NAME") = objCommunicationType.Keterangan
                    End If
                End If

            Next
        End If

        gp_SIPENDAR_PROFILE_PHONE_Before.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_PHONE_Before.GetStore().DataBind()
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_IDENTIFICATION_Before()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS_BEFORE.objList_SIPENDAR_PROFILE_IDENTIFICATION)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listIdentificationType As List(Of goAML_Ref_Jenis_Dokumen_Identitas) = Nothing
        Dim listCountryName As List(Of goAML_Ref_Nama_Negara) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listIdentificationType = objdb.goAML_Ref_Jenis_Dokumen_Identitas.ToList
            listCountryName = objdb.goAML_Ref_Nama_Negara.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("IDENTIFICATION_TYPE_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("ISSUED_COUNTRY", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objIDENTIFICATIONType = SiPendarProfile_BLL.GetJenisDokumenIdentitasByCode(item("Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ToString)
                'If objIDENTIFICATIONType IsNot Nothing Then
                '    item("IDENTIFICATION_TYPE_NAME") = objIDENTIFICATIONType.Keterangan
                'Else
                '    item("IDENTIFICATION_TYPE_NAME") = Nothing
                'End If

                'Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(item("FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY").ToString)
                'If objCountry IsNot Nothing Then
                '    item("ISSUED_COUNTRY") = objCountry.Keterangan
                'Else
                '    item("ISSUED_COUNTRY") = Nothing
                'End If

                If listIdentificationType IsNot Nothing Then
                    Dim objIDENTIFICATIONType = listIdentificationType.Find(Function(x) x.Kode = item("Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ToString)
                    If objIDENTIFICATIONType IsNot Nothing Then
                        item("IDENTIFICATION_TYPE_NAME") = objIDENTIFICATIONType.Keterangan
                    End If
                End If

                If listCountryName IsNot Nothing Then
                    Dim objCountry = listCountryName.Find(Function(x) x.Kode = item("FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY").ToString)
                    If objCountry IsNot Nothing Then
                        item("ISSUED_COUNTRY") = objCountry.Keterangan
                    End If
                End If
            Next
        End If

        gp_SIPENDAR_PROFILE_IDENTIFICATION_Before.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_IDENTIFICATION_Before.GetStore().DataBind()

    End Sub
    '------- END OF LOAD DATA BEFORE

    '------- LOAD DATA AFTER
    Protected Sub LoadDataAfter()
        Try
            'objApproval.ModuleField = ""
            'Unexpected end of file.
            'objApproval.ModuleField = Nothing
            'String reference Not set to an instance of a String. Parameter name: s
            If Not String.IsNullOrEmpty(objApproval.ModuleField) Then
                objSIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(SiPendarBLL.SIPENDAR_PROFILE_CLASS))
            End If
            LoadSiPendarProfile()

            ''15-Sep-21 Adi : Load Screening Result (if any)
            'LoadScreeningResult() '' Remark 04-Oct-2021. Katanya di comment aja

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadSiPendarProfile()
        Try
            Using objDB As New SiPendarDAL.SiPendarEntities
                'Bind Data Header
                If objSIPENDAR_PROFILE_CLASS IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE IsNot Nothing Then
                    With objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE
                        'General Info
                        If Not IsNothing(.FK_SIPENDAR_TYPE_ID) Then
                            Dim objSiPendarType = objDB.SIPENDAR_TYPE.Where(Function(x) x.PK_SIPENDAR_TYPE_id = .FK_SIPENDAR_TYPE_ID).FirstOrDefault
                            If objSiPendarType IsNot Nothing Then
                                cmb_SIPENDAR_TYPE_ID.SetTextWithTextValue(objSiPendarType.PK_SIPENDAR_TYPE_id, objSiPendarType.SIPENDAR_TYPE_NAME)
                            End If
                        End If
                        If Not IsNothing(.Fk_goAML_Ref_Customer_Type_id) Then
                            Dim objCustomerType = SiPendarProfile_BLL.GetCustomerTypeByID(.Fk_goAML_Ref_Customer_Type_id)
                            If objCustomerType IsNot Nothing Then
                                cmb_CUSTOMER_TYPE.SetTextWithTextValue(objCustomerType.PK_Customer_Type_ID, objCustomerType.Description)
                            End If
                        End If

                        'Tambahan 8-Jul-2021
                        If Not IsNothing(.FK_SIPENDAR_SUBMISSION_TYPE_CODE) Then
                            Dim objSubmissionType = objDB.SIPENDAR_SUBMISSION_TYPE.Where(Function(x) x.Kode = .FK_SIPENDAR_SUBMISSION_TYPE_CODE).FirstOrDefault
                            If objSubmissionType IsNot Nothing Then
                                cmb_SUBMISSION_TYPE_CODE.SetTextWithTextValue(objSubmissionType.Kode, objSubmissionType.Keterangan)
                            End If
                        End If

                        If .Transaction_DateFrom.HasValue Then
                            chk_IsGenerateTransaction.Checked = True
                            txt_Transaction_DateFrom.Value = .Transaction_DateFrom
                            txt_Transaction_DateFrom.Hidden = False
                        End If
                        If .Transaction_DateTo.HasValue Then
                            txt_Transaction_DateTo.Value = .Transaction_DateTo
                            txt_Transaction_DateTo.Hidden = False
                        End If
                        'End of Tambahan 8-Jul-2021

                        'Added by Felix on 14-Sep-20
                        If Not IsNothing(.Report_Indicator) Then

                            ' Split string based on comma
                            Dim Report_Indicators As String() = .Report_Indicator.Split(New Char() {","c})

                            ' Use For Each loop over words and display them

                            Dim Indicator As String
                            For Each Indicator In Report_Indicators
                                Dim reportIndicator As New SiPendarDAL.SIPENDAR_Report_Indicator

                                If ListIndikatorLaporan.Count = 0 Then
                                    reportIndicator.PK_Report_Indicator = -1
                                ElseIf ListIndikatorLaporan.Count >= 1 Then
                                    reportIndicator.PK_Report_Indicator = ListIndikatorLaporan.Min(Function(x) x.PK_Report_Indicator) - 1
                                End If

                                reportIndicator.FK_Indicator = Indicator

                                ListIndikatorLaporan.Add(reportIndicator)
                            Next
                            bindReportIndicator(StoreReportIndicatorData, ListIndikatorLaporan)
                            PanelReportIndicator.Hidden = False
                        End If
                        'End of 14-Sep-20


                        'SIPENDAR Proaktif
                        If Not IsNothing(.FK_SIPENDAR_JENIS_WATCHLIST_CODE) Then
                            Dim objJenisWatchlist = objDB.SIPENDAR_JENIS_WATCHLIST.Where(Function(x) x.SIPENDAR_JENIS_WATCHLIST_CODE = .FK_SIPENDAR_JENIS_WATCHLIST_CODE).FirstOrDefault
                            If objJenisWatchlist IsNot Nothing Then
                                cmb_JENIS_WATCHLIST_CODE.SetTextWithTextValue(objJenisWatchlist.SIPENDAR_JENIS_WATCHLIST_CODE, objJenisWatchlist.KETERANGAN)
                            End If
                        End If
                        If Not IsNothing(.FK_SIPENDAR_TINDAK_PIDANA_CODE) Then
                            Dim objTindakPidana = objDB.SiPendar_Tindak_Pidana.Where(Function(x) x.SiPendar_Tindak_Pidana_Code = .FK_SIPENDAR_TINDAK_PIDANA_CODE).FirstOrDefault
                            If objTindakPidana IsNot Nothing Then
                                cmb_TINDAK_PIDANA_CODE.SetTextWithTextValue(objTindakPidana.SiPendar_Tindak_Pidana_Code, objTindakPidana.Keterangan)
                            End If
                        End If
                        If Not IsNothing(.FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE) Then
                            Dim objSumberInformasiKhusus = objDB.SiPendar_Sumber_Informasi_Khusus.Where(Function(x) x.SiPendar_Sumber_Informasi_Khusus_Code = .FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE).FirstOrDefault
                            If objSumberInformasiKhusus IsNot Nothing Then
                                cmb_SUMBER_INFORMASI_KHUSUS_CODE.SetTextWithTextValue(objSumberInformasiKhusus.SiPendar_Sumber_Informasi_Khusus_Code, objSumberInformasiKhusus.Keterangan)
                            End If
                        End If
                        If Not IsNothing(.FK_SIPENDAR_ORGANISASI_CODE) Then
                            Dim objSiPendarOrganisasi = objDB.SIPENDAR_ORGANISASI.Where(Function(x) x.ORGANISASI_CODE = .FK_SIPENDAR_ORGANISASI_CODE).FirstOrDefault
                            If objSiPendarOrganisasi IsNot Nothing Then
                                cmb_ORGANISASI_CODE.SetTextWithTextValue(objSiPendarOrganisasi.ORGANISASI_CODE, objSiPendarOrganisasi.ORGANISASI_NAME)
                            End If
                        End If

                        'SIPENDAR PENGAYAAN
                        If Not IsNothing(.FK_SIPENDAR_SUMBER_TYPE_CODE) Then
                            Dim objSiPendarSumberType = objDB.SIPENDAR_SUMBER_TYPE.Where(Function(x) x.SUMBER_TYPE_CODE = .FK_SIPENDAR_SUMBER_TYPE_CODE).FirstOrDefault
                            If objSiPendarSumberType IsNot Nothing Then
                                cmb_SUMBER_TYPE_CODE.SetTextWithTextValue(objSiPendarSumberType.SUMBER_TYPE_CODE, objSiPendarSumberType.SUMBER_TYPE_NAME)
                            End If
                        End If
                        txt_SUMBER_ID.Value = .SUMBER_ID
                        txt_Similarity.Value = .Similarity

                        txt_GCN.Value = .GCN
                        txt_Keterangan.Value = .Keterangan
                        If .FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID IsNot Nothing Then
                            Dim dtResult As DataTable = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID)
                            If dtResult IsNot Nothing AndAlso dtResult.Rows.Count > 0 Then
                                Dim drResult As DataRow = dtResult.Rows(0)
                                If drResult IsNot Nothing Then
                                    If IsDBNull(drResult("Stakeholder_Role")) Then
                                        txt_CustomerType.Text = "Customer"
                                        txt_role_noncustomer.Hidden = True
                                    Else
                                        txt_CustomerType.Text = "Non-Customer"
                                        txt_role_noncustomer.Text = drResult("Stakeholder_Role")
                                        txt_role_noncustomer.Hidden = False
                                        fs_Individual_After_IsHaveActiveAccount_Head.Hidden = True
                                        fs_Corporate_After_IsHaveActiveAccount_Head.Hidden = True
                                    End If
                                End If
                            End If
                        Else
                            txt_CustomerType.Text = "Customer"
                            txt_role_noncustomer.Hidden = True
                        End If

                        'Customer Information
                        'Hide/Show FieldSet Individual/Korporasi
                        fs_Individual.Hidden = True
                        fs_Corporate.Hidden = True

                        Dim customerstatusclosed As String = ""
                        Dim drCustomerClosed As DataRow = GetCustomerClosedStatus(txt_GCN.Value)
                        If drCustomerClosed IsNot Nothing Then
                            If Not IsDBNull(drCustomerClosed("IsClosed")) Then
                                If drCustomerClosed("IsClosed") = 0 Then
                                    customerstatusclosed = "Active"
                                Else
                                    If Not IsDBNull(drCustomerClosed("ClosedDate")) Then
                                        customerstatusclosed = "Closed ( " & CDate(drCustomerClosed("ClosedDate")).ToString("dd-MMM-yyyy") & " )"
                                    Else
                                        customerstatusclosed = "Closed"
                                    End If
                                End If
                            End If
                        Else
                            customerstatusclosed = "Customer GCN Not Found"
                        End If

                        If .Fk_goAML_Ref_Customer_Type_id = 1 Then      'Individual
                            fs_Individual.Hidden = False

                            txt_INDV_NAME.Value = .INDV_NAME
                            txt_INDV_PLACEOFBIRTH.Value = .INDV_PLACEOFBIRTH
                            If .INDV_DOB.HasValue Then
                                txt_INDV_DOB.Value = .INDV_DOB.Value.ToString("dd-MMM-yyyy") '' Edited by Felix 23 Aug 2021. Tambah .Value.ToString("dd-MMM-yyyy"
                            End If
                            txt_INDV_ADDRESS.Value = .INDV_ADDRESS

                            If Not IsNothing(.INDV_FK_goAML_Ref_Nama_Negara_CODE) Then
                                Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(.INDV_FK_goAML_Ref_Nama_Negara_CODE)
                                If objCountry IsNot Nothing Then
                                    cmb_INDV_NATIONALITY.SetTextWithTextValue(objCountry.Kode, objCountry.Keterangan)
                                End If
                            End If
                            fs_Individual_After_IsHaveActiveAccount.Text = customerstatusclosed
                        Else    'Korporasi
                            fs_Corporate.Hidden = False

                            txt_CORP_NAME.Value = .CORP_NAME
                            txt_CORP_NPWP.Value = .CORP_NPWP
                            txt_CORP_NO_IZIN_USAHA.Value = .CORP_NO_IZIN_USAHA

                            If Not IsNothing(.CORP_FK_goAML_Ref_Nama_Negara_CODE) Then
                                Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(.CORP_FK_goAML_Ref_Nama_Negara_CODE)
                                If objCountry IsNot Nothing Then
                                    cmb_CORP_COUNTRY.SetTextWithTextValue(objCountry.Kode, objCountry.Keterangan)
                                End If
                            End If
                            gp_SIPENDAR_PROFILE_PHONE_Before.Hidden = True
                            gp_SIPENDAR_PROFILE_PHONE.Hidden = True
                            gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = True
                            gp_SIPENDAR_PROFILE_IDENTIFICATION_Before.Hidden = True
                            fs_Corporate_After_IsHaveActiveAccount.Text = customerstatusclosed
                        End If
                    End With
                End If
            End Using


            'Bind Data Detail to Grid Panel
            Bind_SIPENDAR_PROFILE_ACCOUNT()
            Bind_SIPENDAR_PROFILE_AddRESS()
            Bind_SIPENDAR_PROFILE_PHONE()
            Bind_SIPENDAR_PROFILE_IDENTIFICATION()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_ACCOUNT()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listJenisRekening As List(Of goAML_Ref_Jenis_Rekening) = Nothing
        Dim listStatusRekening As List(Of goAML_Ref_Status_Rekening) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listJenisRekening = objdb.goAML_Ref_Jenis_Rekening.ToList
            listStatusRekening = objdb.goAML_Ref_Status_Rekening.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("JENIS_REKENING", GetType(String)))
        objtable.Columns.Add(New DataColumn("STATUS_REKENING", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objJenisRekening = SiPendarProfile_BLL.GetJenisRekeningByCode(item("Fk_goAML_Ref_Jenis_Rekening_CODE").ToString)
                'If objJenisRekening IsNot Nothing Then
                '    item("JENIS_REKENING") = objJenisRekening.Keterangan
                'End If

                'Dim objStatusRekening = SiPendarProfile_BLL.GetStatusRekeningByCode(item("Fk_goAML_Ref_Status_Rekening_CODE").ToString)
                'If objStatusRekening IsNot Nothing Then
                '    item("STATUS_REKENING") = objStatusRekening.Keterangan
                'End If

                If listJenisRekening IsNot Nothing Then
                    Dim objJenisRekening = listJenisRekening.Find(Function(x) x.Kode = item("Fk_goAML_Ref_Jenis_Rekening_CODE").ToString)
                    If objJenisRekening IsNot Nothing Then
                        item("JENIS_REKENING") = objJenisRekening.Keterangan
                    End If
                End If

                If listStatusRekening IsNot Nothing Then
                    Dim objStatusRekening = listStatusRekening.Find(Function(x) x.Kode = item("Fk_goAML_Ref_Status_Rekening_CODE").ToString)
                    If objStatusRekening IsNot Nothing Then
                        item("STATUS_REKENING") = objStatusRekening.Keterangan
                    End If
                End If
            Next
        End If

        gp_SIPENDAR_PROFILE_ACCOUNT.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_ACCOUNT.GetStore().DataBind()
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_AddRESS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listAddressType As List(Of goAML_Ref_Kategori_Kontak) = Nothing
        Dim listCountryName As List(Of goAML_Ref_Nama_Negara) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
            listCountryName = objdb.goAML_Ref_Nama_Negara.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("ADDRESS_TYPE_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objAddressType = SiPendarProfile_BLL.GetKategoriKontakByCode(item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                'If objAddressType IsNot Nothing Then
                '    item("ADDRESS_TYPE_NAME") = objAddressType.Keterangan
                'Else
                '    item("ADDRESS_TYPE_NAME") = Nothing
                'End If

                'Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(item("FK_goAML_Ref_Nama_Negara_CODE").ToString)
                'If objCountry IsNot Nothing Then
                '    item("COUNTRY_NAME") = objCountry.Keterangan
                'Else
                '    item("COUNTRY_NAME") = Nothing
                'End If

                If listAddressType IsNot Nothing Then
                    Dim objAddressType = listAddressType.Find(Function(x) x.Kode = item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                    If objAddressType IsNot Nothing Then
                        item("ADDRESS_TYPE_NAME") = objAddressType.Keterangan
                    End If
                End If

                If listCountryName IsNot Nothing Then
                    Dim objCountry = listCountryName.Find(Function(x) x.Kode = item("FK_goAML_Ref_Nama_Negara_CODE").ToString)
                    If objCountry IsNot Nothing Then
                        item("COUNTRY_NAME") = objCountry.Keterangan
                    End If
                End If
            Next
        End If

        gp_SIPENDAR_PROFILE_ADDRESS.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_ADDRESS.GetStore().DataBind()
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_PHONE()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listPhoneType As List(Of goAML_Ref_Kategori_Kontak) = Nothing
        Dim listCommunicationType As List(Of goAML_Ref_Jenis_Alat_Komunikasi) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listPhoneType = objdb.goAML_Ref_Kategori_Kontak.ToList
            listCommunicationType = objdb.goAML_Ref_Jenis_Alat_Komunikasi.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("PHONE_TYPE_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("COMMUNICATION_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objPHONEType = SiPendarProfile_BLL.GetKategoriKontakByCode(item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                'If objPHONEType IsNot Nothing Then
                '    item("PHONE_TYPE_NAME") = objPHONEType.Keterangan
                'Else
                '    item("PHONE_TYPE_NAME") = Nothing
                'End If

                'Dim objCommunicationType = SiPendarProfile_BLL.GetJenisAlatKomunikasiByCode(item("FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE").ToString)
                'If objCommunicationType IsNot Nothing Then
                '    item("COMMUNICATION_TYPE_NAME") = objCommunicationType.Keterangan
                'Else
                '    item("COMMUNICATION_TYPE_NAME") = Nothing
                'End If

                If listPhoneType IsNot Nothing Then
                    Dim objPHONEType = listPhoneType.Find(Function(x) x.Kode = item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                    If objPHONEType IsNot Nothing Then
                        item("PHONE_TYPE_NAME") = objPHONEType.Keterangan
                    End If
                End If

                If listCommunicationType IsNot Nothing Then
                    Dim objCommunicationType = listCommunicationType.Find(Function(x) x.Kode = item("FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE").ToString)
                    If objCommunicationType IsNot Nothing Then
                        item("COMMUNICATION_TYPE_NAME") = objCommunicationType.Keterangan
                    End If
                End If

            Next
        End If

        gp_SIPENDAR_PROFILE_PHONE.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_PHONE.GetStore().DataBind()
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_IDENTIFICATION()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listIdentificationType As List(Of goAML_Ref_Jenis_Dokumen_Identitas) = Nothing
        Dim listCountryName As List(Of goAML_Ref_Nama_Negara) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listIdentificationType = objdb.goAML_Ref_Jenis_Dokumen_Identitas.ToList
            listCountryName = objdb.goAML_Ref_Nama_Negara.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("IDENTIFICATION_TYPE_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("ISSUED_COUNTRY", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objIDENTIFICATIONType = SiPendarProfile_BLL.GetJenisDokumenIdentitasByCode(item("Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ToString)
                'If objIDENTIFICATIONType IsNot Nothing Then
                '    item("IDENTIFICATION_TYPE_NAME") = objIDENTIFICATIONType.Keterangan
                'Else
                '    item("IDENTIFICATION_TYPE_NAME") = Nothing
                'End If

                'Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(item("FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY").ToString)
                'If objCountry IsNot Nothing Then
                '    item("ISSUED_COUNTRY") = objCountry.Keterangan
                'Else
                '    item("ISSUED_COUNTRY") = Nothing
                'End If

                If listIdentificationType IsNot Nothing Then
                    Dim objIDENTIFICATIONType = listIdentificationType.Find(Function(x) x.Kode = item("Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ToString)
                    If objIDENTIFICATIONType IsNot Nothing Then
                        item("IDENTIFICATION_TYPE_NAME") = objIDENTIFICATIONType.Keterangan
                    End If
                End If

                If listCountryName IsNot Nothing Then
                    Dim objCountry = listCountryName.Find(Function(x) x.Kode = item("FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY").ToString)
                    If objCountry IsNot Nothing Then
                        item("ISSUED_COUNTRY") = objCountry.Keterangan
                    End If
                End If
            Next
        End If

        gp_SIPENDAR_PROFILE_IDENTIFICATION.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_IDENTIFICATION.GetStore().DataBind()
    End Sub
    '------- END OF LOAD DATA AFTER

    '------- DOCUMENT APPROVE/REJECT
    Protected Sub btn_SiPendarProfile_Approve_Click()
        Try
            'Update 24-Sep-2021 Felix : 
            'Penambahan Approval History And Note '' Screening Judgement
            If String.IsNullOrEmpty(txtApprovalNotes.Value) And panelApprovalHistory.Hidden = False Then
                Ext.Net.X.Msg.Alert("Information", "Approval/Rejection Note is required!").Show()
                Exit Sub
            End If

            Dim notes As String = txtApprovalNotes.Value
            SiPendarBLL.goAML_NEW_ReportBLL.CreateNotes(objModuleScreeningJudgement.PK_Module_ID, objModuleScreeningJudgement.ModuleLabel, SiPendarBLL.goAML_NEW_ReportBLL.actionApproval.Accept, SiPendarBLL.goAML_NEW_ReportBLL.actionForm.Approval, IDScreeningJudgement, notes)
            '' End of 24-Sep-2021

            'Approve Data
            SiPendarProfile_BLL.Accept(objApproval.PK_ModuleApproval_ID)
            LblConfirmation.Text = "Data Approved"

            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SiPendarProfile_Reject_Click()
        Try
            'Update 24-Sep-2021 Felix : 
            'Penambahan Approval History And Note
            If String.IsNullOrEmpty(txtApprovalNotes.Value) And panelApprovalHistory.Hidden = False Then
                Ext.Net.X.Msg.Alert("Information", "Approval/Rejection Note is required!").Show()
                Exit Sub
            End If

            Dim notes As String = txtApprovalNotes.Value
            SiPendarBLL.goAML_NEW_ReportBLL.CreateNotes(objModuleScreeningJudgement.PK_Module_ID, objModuleScreeningJudgement.ModuleLabel, SiPendarBLL.goAML_NEW_ReportBLL.actionApproval.Reject, SiPendarBLL.goAML_NEW_ReportBLL.actionForm.Approval, IDScreeningJudgement, notes)
            '' End of 24-Sep-2021

            'Reject Data
            SiPendarProfile_BLL.Reject(objApproval.PK_ModuleApproval_ID)
            LblConfirmation.Text = "Data Rejected"

            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SiPendarProfile_Back_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF DOCUMENT SAVE

    Protected Sub btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SetControlReadOnly()
        'DATA BEFORE
        'Read Only property
        cmb_SIPENDAR_TYPE_ID_Before.IsReadOnly = True
        cmb_CUSTOMER_TYPE_Before.IsReadOnly = True
        cmb_JENIS_WATCHLIST_CODE_Before.IsReadOnly = True
        cmb_TINDAK_PIDANA_CODE_Before.IsReadOnly = True
        cmb_SUMBER_INFORMASI_KHUSUS_CODE_Before.IsReadOnly = True
        cmb_ORGANISASI_CODE_Before.IsReadOnly = True
        txt_GCN_Before.ReadOnly = True
        txt_CustomerType_Before.ReadOnly = True
        txt_role_noncustomer_Before.ReadOnly = True
        txt_Keterangan_Before.ReadOnly = True
        txt_Similarity_Before.ReadOnly = True
        cmb_SUMBER_TYPE_CODE_Before.IsReadOnly = True
        txt_SUMBER_ID_Before.ReadOnly = True
        cmb_SUBMISSION_TYPE_CODE_Before.IsReadOnly = True

        txt_INDV_NAME_Before.ReadOnly = True
        txt_INDV_PLACEOFBIRTH_Before.ReadOnly = True
        txt_INDV_DOB_Before.ReadOnly = True
        txt_INDV_ADDRESS_Before.ReadOnly = True
        cmb_INDV_NATIONALITY_Before.IsReadOnly = True

        txt_CORP_NAME_Before.ReadOnly = True
        txt_CORP_NPWP_Before.ReadOnly = True
        txt_CORP_NO_IZIN_USAHA_Before.ReadOnly = True
        cmb_CORP_COUNTRY_Before.IsReadOnly = True

        'Background Color
        txt_GCN_Before.FieldStyle = "background-color:#ddd;"
        txt_CustomerType_Before.FieldStyle = "background-color:#ddd;"
        txt_role_noncustomer_Before.FieldStyle = "background-color:#ddd;"
        txt_Keterangan_Before.FieldStyle = "background-color:#ddd;"
        txt_Similarity_Before.FieldStyle = "background-color:#ddd;"
        txt_SUMBER_ID_Before.FieldStyle = "background-color:#ddd;"

        txt_INDV_NAME_Before.FieldStyle = "background-color:#ddd;"
        txt_INDV_PLACEOFBIRTH_Before.FieldStyle = "background-color:#ddd;"
        txt_INDV_DOB_Before.FieldStyle = "background-color:#ddd;"
        txt_INDV_ADDRESS_Before.FieldStyle = "background-color:#ddd;"

        txt_CORP_NAME_Before.FieldStyle = "background-color:#ddd;"
        txt_CORP_NPWP_Before.FieldStyle = "background-color:#ddd;"
        txt_CORP_NO_IZIN_USAHA_Before.FieldStyle = "background-color:#ddd;"

        'DATA AFTER
        'Read Only property
        cmb_SIPENDAR_TYPE_ID.IsReadOnly = True
        cmb_CUSTOMER_TYPE.IsReadOnly = True
        cmb_JENIS_WATCHLIST_CODE.IsReadOnly = True
        cmb_TINDAK_PIDANA_CODE.IsReadOnly = True
        cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsReadOnly = True
        cmb_ORGANISASI_CODE.IsReadOnly = True
        txt_GCN.ReadOnly = True
        txt_CustomerType.ReadOnly = True
        txt_role_noncustomer.ReadOnly = True
        txt_Keterangan.ReadOnly = True
        txt_Similarity.ReadOnly = True
        cmb_SUMBER_TYPE_CODE.IsReadOnly = True
        txt_SUMBER_ID.ReadOnly = True
        cmb_SUBMISSION_TYPE_CODE.IsReadOnly = True

        txt_INDV_NAME.ReadOnly = True
        txt_INDV_PLACEOFBIRTH.ReadOnly = True
        txt_INDV_DOB.ReadOnly = True
        txt_INDV_ADDRESS.ReadOnly = True
        cmb_INDV_NATIONALITY.IsReadOnly = True

        txt_CORP_NAME.ReadOnly = True
        txt_CORP_NPWP.ReadOnly = True
        txt_CORP_NO_IZIN_USAHA.ReadOnly = True
        cmb_CORP_COUNTRY.IsReadOnly = True

        chk_IsGenerateTransaction.ReadOnly = True
        txt_Transaction_DateFrom.ReadOnly = True
        txt_Transaction_DateTo.ReadOnly = True

        'Background Color
        txt_GCN.FieldStyle = "background-color:#ddd;"
        txt_CustomerType.FieldStyle = "background-color:#ddd;"
        txt_role_noncustomer.FieldStyle = "background-color:#ddd;"
        txt_Keterangan.FieldStyle = "background-color:#ddd;"
        txt_Similarity.FieldStyle = "background-color:#ddd;"
        txt_SUMBER_ID.FieldStyle = "background-color:#ddd;"

        txt_INDV_NAME.FieldStyle = "background-color:#ddd;"
        txt_INDV_PLACEOFBIRTH.FieldStyle = "background-color:#ddd;"
        txt_INDV_DOB.FieldStyle = "background-color:#ddd;"
        txt_INDV_ADDRESS.FieldStyle = "background-color:#ddd;"

        txt_CORP_NAME.FieldStyle = "background-color:#ddd;"
        txt_CORP_NPWP.FieldStyle = "background-color:#ddd;"
        txt_CORP_NO_IZIN_USAHA.FieldStyle = "background-color:#ddd;"

        txt_Transaction_DateFrom.FieldStyle = "background-color:#ddd;"
        txt_Transaction_DateTo.FieldStyle = "background-color:#ddd;"
    End Sub

    Protected Sub SetControlVisibility()
        Try
            '3-Jul-2021 Adi : Hide field yang awalnya ada di XML tapi di XSD tidak ada
            cmb_TINDAK_PIDANA_CODE.IsHidden = True
            cmb_ORGANISASI_CODE.IsHidden = True
            txt_INDV_ADDRESS.Hidden = True

            'Sesuaikan dengan SIPENDAR_TYPE
            If objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID = 1 Then
                cmb_JENIS_WATCHLIST_CODE.IsHidden = False
                cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = False
                cmb_SUMBER_TYPE_CODE.IsHidden = True
                txt_SUMBER_ID.Hidden = True
                txt_Similarity.Hidden = True

                fs_Generate_Transaction.Hidden = True
            Else
                cmb_JENIS_WATCHLIST_CODE.IsHidden = True
                cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = True
                cmb_SUMBER_TYPE_CODE.IsHidden = False
                txt_SUMBER_ID.Hidden = False
                txt_Similarity.Hidden = False

                fs_Generate_Transaction.Hidden = False
            End If


            '3-Jul-2021 Adi : Hide field yang awalnya ada di XML tapi di XSD tidak ada
            cmb_TINDAK_PIDANA_CODE_Before.IsHidden = True
            cmb_ORGANISASI_CODE_Before.IsHidden = True
            txt_INDV_ADDRESS_Before.Hidden = True

            'Sesuaikan dengan SIPENDAR_TYPE
            If objSIPENDAR_PROFILE_CLASS_BEFORE.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID = 1 Then
                cmb_JENIS_WATCHLIST_CODE_Before.IsHidden = False
                cmb_SUMBER_INFORMASI_KHUSUS_CODE_Before.IsHidden = False
                cmb_SUMBER_TYPE_CODE_Before.IsHidden = True
                txt_SUMBER_ID_Before.Hidden = True
                txt_Similarity_Before.Hidden = True
            Else
                cmb_JENIS_WATCHLIST_CODE_Before.IsHidden = True
                cmb_SUMBER_INFORMASI_KHUSUS_CODE_Before.IsHidden = True
                cmb_SUMBER_TYPE_CODE_Before.IsHidden = False
                txt_SUMBER_ID_Before.Hidden = False
                txt_Similarity_Before.Hidden = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "7-Jul-2021 Adi"
    'Penambahan fitur Generate Transaction From - To
    Protected Sub chk_IsGenerateTransaction_Change(sender As Object, e As EventArgs)
        Try
            If chk_IsGenerateTransaction.Checked = True Then
                txt_Transaction_DateFrom.Hidden = False
                txt_Transaction_DateTo.Hidden = False
                PanelReportIndicator.Hidden = False ''Added by Felix on 14 Sep 2021
            Else
                txt_Transaction_DateFrom.Hidden = True
                txt_Transaction_DateTo.Hidden = True
                PanelReportIndicator.Hidden = True ''Added by Felix on 14 Sep 2021
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
#Region "14-Sep-2021 Felix"
    Sub bindReportIndicator(store As Ext.Net.Store, listAddresss As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        '  objtable.Columns.Add(New DataColumn("Kode_Indicator", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                '     item("Kode_Indicator") = item("FK_Indicator")
                item("Keterangan") = getReportIndicatorByKode(item("FK_Indicator").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Shared Function getReportIndicatorByKode(Kode As String) As String
        Dim strKategori As String = ""
        Using objdb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Dim kategori As SiPendarDAL.goAML_Ref_Indikator_Laporan = objdb.goAML_Ref_Indikator_Laporan.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If kategori IsNot Nothing Then
                strKategori = kategori.Keterangan
            End If
        End Using
        Return strKategori
    End Function
#End Region

#Region "15-Sep-2021 Adi : Show screening result (if any)"
    Protected Sub LoadScreeningResult()
        Try
            If objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID IsNot Nothing Then
                Dim PK_Result_ID As Long = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
                Using objdb As New SiPendarDAL.SiPendarEntities
                    Dim objScreeningResult = objdb.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = PK_Result_ID).FirstOrDefault
                    If objScreeningResult IsNot Nothing Then
                        txt_FK_WATCHLIST_ID_PPATK.Value = objScreeningResult.FK_WATCHLIST_ID_PPATK
                        txt_Name_Watchlist.Value = objScreeningResult.Nama
                        If objScreeningResult.DOB.HasValue Then
                            'txt_DOB_Watchlist.Value = objScreeningResult.DOB.ToString("dd-MMM-yyyy")
                            txt_DOB_Watchlist.Value = objScreeningResult.DOB.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        End If
                        txt_BirthPlace_Watchlist.Value = objScreeningResult.Birth_Place
                        txt_IDNumber_Watchlist.Value = objScreeningResult.Identity_Number
                        If objScreeningResult.TotalSimilarity.HasValue Then
                            txt_Similarity_Watchlist.Value = CDbl(objScreeningResult.TotalSimilarity * 100).ToString("#,##0.00") & " %"
                        End If
                    End If
                End Using

                pnlScreeningResult.Hidden = False
            Else
                pnlScreeningResult.Hidden = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub SiPendarProfile_ApprovalDetail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Approval
    End Sub
#Region "24-Sep-2021 Felix"
    Private Sub LoadApprovalHistoryJudgement()
        Try

            Dim EncryptedReportID As String = Request.Params("ID")
            Dim IDModuleApproval = NawaBLL.Common.DecryptQueryString(EncryptedReportID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            objModuleScreeningJudgement = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_SCREENING_JUDGEMENT")

            Using objdbUpdateStatus As New SiPendarDAL.SiPendarEntities
                Dim objApprovalUpdateStatus As SiPendarDAL.ModuleApproval = objdbUpdateStatus.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = IDModuleApproval).FirstOrDefault()
                Dim objDataUpdateStatus As New SIPENDAR_PROFILE_CLASS
                If objApprovalUpdateStatus IsNot Nothing Then
                    If objApproval.ModuleField IsNot Nothing Then
                        objDataUpdateStatus = NawaBLL.Common.Deserialize(objApprovalUpdateStatus.ModuleField, GetType(SIPENDAR_PROFILE_CLASS))
                    ElseIf objApproval.ModuleFieldBefore IsNot Nothing Then
                        objDataUpdateStatus = NawaBLL.Common.Deserialize(objApprovalUpdateStatus.ModuleFieldBefore, GetType(SIPENDAR_PROFILE_CLASS))
                    End If
                End If
                If objDataUpdateStatus IsNot Nothing AndAlso objDataUpdateStatus.objSIPENDAR_PROFILE IsNot Nothing AndAlso objDataUpdateStatus.objSIPENDAR_PROFILE.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID IsNot Nothing Then
                    IDScreeningJudgement = objDataUpdateStatus.objSIPENDAR_PROFILE.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID

                    Dim dtResult As DataTable = New DataTable
                    Dim paramLoadApprovalHistory(1) As SqlParameter

                    paramLoadApprovalHistory(0) = New SqlParameter
                    paramLoadApprovalHistory(0).ParameterName = "@Module_ID"
                    paramLoadApprovalHistory(0).Value = objModuleScreeningJudgement.PK_Module_ID
                    paramLoadApprovalHistory(0).DbType = SqlDbType.VarChar

                    paramLoadApprovalHistory(1) = New SqlParameter
                    paramLoadApprovalHistory(1).ParameterName = "@Result_ID"
                    paramLoadApprovalHistory(1).Value = IDScreeningJudgement
                    paramLoadApprovalHistory(1).DbType = SqlDbType.VarChar

                    dtResult = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_JUDGEMENT_APPROVAL_HISTORY", paramLoadApprovalHistory)
                    'Dim objApprovalHistory = SiPendarBLL.goAML_NEW_ReportBLL.get_ListNotesHistory(ObjModule.PK_Module_ID, IDReport)
                    If dtResult.Rows.Count <> 0 Then
                        gpStore_history.DataSource = dtResult
                        gpStore_history.DataBind()
                    End If

                    panelApprovalHistory.Hidden = False
                End If
            End Using
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
#Region "29-Sep-2021 Felix"
    Protected Sub Show_ScreeningResult()
        Try
            '' Start 20-Jul-2022 Penambhan atas Request ANZ
            cboExportExcel.Value = "Excel"
            '' End 20-Jul-2022
            'Dim PK_Request_ID As String = Request.Params("ID")
            'Dim intID = NawaBLL.Common.DecryptQueryString(PK_Request_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            'Dim intID = NawaBLL.Common.DecryptQueryString(PK_Request_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            'Dim querySelect As String = ""
            'querySelect = "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct "
            'querySelect += "FROM SIPENDAR_SCREENING_REQUEST_RESULT a "
            'querySelect += "left join SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO b "
            'querySelect += "on a.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = b.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID "
            'querySelect += "WHERE a.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID in "
            'querySelect += "(select distinct PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID "
            'querySelect += "from SIPENDAR_SCREENING_REQUEST_RESULT "
            'querySelect += "where PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID =" & intID & " ) "
            'querySelect += "and a.GCN is not null  "
            'querySelect += "and isnull(b.Status,'New') in ('New','Reject','Reject Exclusion') "
            'querySelect += "ORDER BY TotalSimilarityPct Desc, a.GCN, Nama "

            Dim dtResult As DataTable = New DataTable
            dtResult = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT_OtherInfo(IDScreeningJudgement)
            'dtResult = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, querySelect)
            If dtResult.Rows.Count <> 0 Then
                gp_ScreeningResult.GetStore.DataSource = dtResult
                gp_ScreeningResult.GetStore.DataBind()
                '' Start 20-Jul-2022 Penambhan atas Request ANZ
                PanelScreeningResult.Hidden = False
                gp_ScreeningResult_All.GetStore.DataSource = dtResult
                gp_ScreeningResult_All.GetStore.DataBind()

                'gp_ScreeningResult.Hidden = False
                TotalScreeningResult = dtResult.Rows.Count
                '' End 20-Jul-2022 
            Else
                '' Start 20-Jul-2022 Penambhan atas Request ANZ
                'gp_ScreeningResult.Hidden = True
                PanelScreeningResult.Hidden = True
                '' Start 20-Jul-2022
            End If

            'gp_ScreeningResult.Hidden = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT_OtherInfo(PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID As String) As DataTable
        Try
            Dim data As New DataTable

            'Dim stringquery As String = " SELECT result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID, result.Nama, result.DOB, result.Birth_Place, result.Identity_Number, result.FK_WATCHLIST_ID_PPATK, result.GCN, " &
            '" STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'') as CIF, result.NAMACUSTOMER, result.DOBCustomer, result.Birth_PlaceCustomer, result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer, " &
            '" result.Identity_NumberCustomer, result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE, (result.TotalSimilarity*100) AS TotalSimilarityPct, result.Stakeholder_Role, " &
            '" case when result.Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation " &
            '" FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
            '" WHERE result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = " & PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
            'stringquery = stringquery & " ORDER BY TotalSimilarityPct Desc, result.GCN, result.Nama"
            'data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)

            Dim paramLoadResultGroup(0) As SqlParameter

            paramLoadResultGroup(0) = New SqlParameter
            paramLoadResultGroup(0).ParameterName = "@PK"
            paramLoadResultGroup(0).Value = PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
            paramLoadResultGroup(0).DbType = SqlDbType.VarChar

            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_GROUP_RESULT_BY_PK", paramLoadResultGroup)

            data.Columns.Add(New Data.DataColumn("Account_Name", GetType(String)))
            data.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            data.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))
            Dim data2 As New DataTable
            For Each row As Data.DataRow In data.Rows
                If row.Item("Stakeholder_Role") IsNot Nothing And Not IsDBNull(row.Item("Stakeholder_Role")) Then
                    row.Item("Status") = "Non Customer"

                    ' 19 Nov 2021 Penmbahan untuk link CIF
                    Dim stringgcn As String = Convert.ToString(row("GCN"))
                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                    If drResult IsNot Nothing Then
                        Dim query As String = "DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) " &
                                              "INSERT INTO @tblaccount ( Groupaccount ) " &
                                              "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID") &
                                              " Select STUFF((SELECT ','  + cast(Groupaccount as nvarchar) from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_No, " &
                                              " STUFF((SELECT  ','  +  CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),'') WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),'') " &
                                              " Else '' end from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif " &
                                              " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_Name, STUFF((SELECT ','  + isnull(cast(rc.cif as nvarchar),'') from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as CIF_No "
                        data2 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)
                        For Each row2 As DataRow In data2.Rows
                            row("Account_Name") = row2("Account_Name")
                            row("Account_No") = row2("Account_No")
                            row("CIF_No") = row2("CIF_No")
                        Next
                    Else
                        row("Account_Name") = Nothing
                        row("Account_No") = Nothing
                        row("CIF_No") = Nothing
                    End If

                Else
                    row.Item("Status") = "Customer"
                    row("Account_Name") = Nothing
                    row("Account_No") = Nothing
                    row("CIF_No") = Nothing
                End If
            Next

            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function

    Protected Function GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID As String) As DataTable
        Try
            Dim data As New DataTable

            Dim stringquery As String = " SELECT result.Stakeholder_Role " &
            " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
            " WHERE result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = " & PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)

            Return data

            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function




    'Protected Function GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT_OtherInfo(PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID As String) As DataTable
    '    Try
    '        Dim data As New DataTable

    '        'Dim stringquery As String = " SELECT result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID, result.Nama, result.DOB, result.Birth_Place, result.Identity_Number, result.FK_WATCHLIST_ID_PPATK, result.GCN, " &
    '        '" STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'') as CIF, result.NAMACUSTOMER, result.DOBCustomer, result.Birth_PlaceCustomer, result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer, " &
    '        '" result.Identity_NumberCustomer, result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE, (result.TotalSimilarity*100) AS TotalSimilarityPct, result.Stakeholder_Role, " &
    '        '" case when result.Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation " &
    '        '" FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
    '        '" WHERE result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = " & PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
    '        'stringquery = stringquery & " ORDER BY TotalSimilarityPct Desc, result.GCN, result.Nama"
    '        'data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)

    '        Dim paramLoadResultGroup(0) As SqlParameter

    '        paramLoadResultGroup(0) = New SqlParameter
    '        paramLoadResultGroup(0).ParameterName = "@PK"
    '        paramLoadResultGroup(0).Value = PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
    '        paramLoadResultGroup(0).DbType = SqlDbType.VarChar

    '        data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_GROUP_RESULT_BY_PK", paramLoadResultGroup)

    '        Return data

    '        Return data
    '    Catch ex As Exception
    '        Throw ex
    '        'Return New DataTable
    '    End Try
    'End Function

    Protected Sub WindowScreeningDetail_Close_Click()
        WindowScreeningDetail.Hidden = True
    End Sub

    Protected Sub gp_ScreeningResult_Gridcommand(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                Clear_WindowScreeningDetail()
                WindowScreeningDetail.Hidden = False
                LoadData_WindowScreeningDetail(id)
            Else

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Clear_WindowScreeningDetail()
        Try
            WindowScreeningDetail_Similarity.Clear()
            WindowScreeningDetail_IDWatchlistPPATK.Clear()
            WindowScreeningDetail_NameSearch.Clear()
            WindowScreeningDetail_DOBSearch.Clear()
            WindowScreeningDetail_POBSearch.Clear()
            WindowScreeningDetail_IdentityTypeSearch.Clear()
            WindowScreeningDetail_IdentityNumberSearch.Clear()
            WindowScreeningDetail_GCN.Clear()
            WindowScreeningDetail_CustomerName.Clear()
            WindowScreeningDetail_CustomerDOB.Clear()
            WindowScreeningDetail_CustomerPOB.Clear()
            WindowScreeningDetail_CustomerIdentityType.Clear()
            WindowScreeningDetail_CustomerIdentityNumber.Clear()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadColumnCommandScreeningResult()
        Try
            ColumnActionLocation(gp_ScreeningResult, gp_ScreeningResult_CommandColumn)
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Protected Sub LoadData_WindowScreeningDetail(id As String)
        Try
            Dim dtResult As DataTable = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT_OtherInfo(id)
            If dtResult IsNot Nothing Then
                If dtResult.Rows.Count > 0 Then
                    Dim itemrows As Data.DataRow = dtResult.Rows(0)
                    'Data Search
                    If Not IsDBNull(itemrows("FK_WATCHLIST_ID_PPATK")) Then
                        WindowScreeningDetail_IDWatchlistPPATK.Text = Convert.ToString(itemrows("FK_WATCHLIST_ID_PPATK"))
                    End If
                    If Not IsDBNull(itemrows("Nama")) Then
                        WindowScreeningDetail_NameSearch.Text = Convert.ToString(itemrows("Nama"))
                    End If
                    If Not IsDBNull(itemrows("DOB")) Then
                        WindowScreeningDetail_DOBSearch.Text = Convert.ToDateTime(itemrows("DOB")).ToString("dd-MM-yyyy")
                    End If
                    If Not IsDBNull(itemrows("Birth_Place")) Then
                        WindowScreeningDetail_POBSearch.Text = Convert.ToString(itemrows("Birth_Place"))
                    End If
                    If Not IsDBNull(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE")) Then
                        WindowScreeningDetail_IdentityTypeSearch.Text = Convert.ToString(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE"))
                    End If
                    If Not IsDBNull(itemrows("Identity_Number")) Then
                        WindowScreeningDetail_IdentityNumberSearch.Text = Convert.ToString(itemrows("Identity_Number"))
                    End If
                    If Not IsDBNull(itemrows("TotalSimilarityPct")) Then
                        WindowScreeningDetail_Similarity.Text = Convert.ToString(itemrows("TotalSimilarityPct")) & "%"
                    End If
                    '20-Dec-2021 Felix : Tambah info Submission_Type
                    If Not IsDBNull(itemrows("FK_SIPENDAR_SUBMISSION_TYPE_CODE")) Then
                        WindowScreeningDetail_SubmissionType.Text = Convert.ToString(itemrows("FK_SIPENDAR_SUBMISSION_TYPE_CODE"))
                    End If

                    '29-Sep-2021 Adi : Penambahan list of Identity Watchlist
                    If Not IsDBNull(itemrows("FK_WATCHLIST_ID_PPATK")) Then
                        Dim strSQL As String = "SELECT Identity_Type, Identity_Number"
                        strSQL += " FROM SIPENDAR_WATCHLIST sw"
                        strSQL += " UNPIVOT (Identity_Number FOR Identity_Type IN (NPWP,KTP,PAS,KITAS,SUKET,SIM,KITAP, KIMS)) unpvt"
                        strSQL += " WHERE ISNULL(Identity_Number,'')<>'' AND ID_PPATK = '" & itemrows("FK_WATCHLIST_ID_PPATK") & "'"
                        Dim dtIdentityWatchlist As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

                        gp_IdentityWatchlist.GetStore.DataSource = dtIdentityWatchlist
                        gp_IdentityWatchlist.GetStore.DataBind()
                        gp_IdentityWatchlist.Hidden = False

                        'Hide Identity Type Search dan Identity NUmber Search
                        WindowScreeningDetail_IdentityTypeSearch.Hidden = True
                        WindowScreeningDetail_IdentityNumberSearch.Hidden = True
                    Else
                        gp_IdentityWatchlist.Hidden = True
                        'Hide Identity Type Search dan Identity NUmber Search
                        WindowScreeningDetail_IdentityTypeSearch.Hidden = False
                        WindowScreeningDetail_IdentityNumberSearch.Hidden = False
                    End If

                    'Data Found
                    If Not IsDBNull(itemrows("customerinformation")) Then
                        Dim customerinformation As String = Convert.ToString(itemrows("customerinformation"))
                        If Not String.IsNullOrEmpty(customerinformation) Then
                            '29-Sep-2021 Adi : Penambahan list of Identity Customer
                            'Hide Identity Type Search dan Identity NUmber Search
                            WindowScreeningDetail_CustomerIdentityType.Hidden = True
                            WindowScreeningDetail_CustomerIdentityNumber.Hidden = True
                            ''Customer' else 'Non Customer'
                            If customerinformation = "Customer" Then
                                WindowScreeningDetail_CustomerType.Text = "Customer"
                                FieldSet2.Title = "Customer Data"
                                WindowScreeningDetail_IsHaveActiveAccount.Hidden = False
                                WindowScreeningDetail_GCN.FieldLabel = "GCN"
                                WindowScreeningDetail_CIF.FieldLabel = "CIF"
                                '30-Sep-2021 ganti cara dengan panggil SP
                                'If Not IsDBNull(itemrows("IsHaveActiveAccount")) Then
                                '    WindowScreeningDetail_IsHaveActiveAccount.Text = Convert.ToString(itemrows("IsHaveActiveAccount"))
                                'End If
                                If Not IsDBNull(itemrows("GCN")) Then
                                    WindowScreeningDetail_GCN.Text = Convert.ToString(itemrows("GCN"))
                                    Dim drCustomerClosed As DataRow = GetCustomerClosedStatus(itemrows("GCN"))
                                    If drCustomerClosed IsNot Nothing Then
                                        If Not IsDBNull(drCustomerClosed("IsClosed")) Then
                                            If drCustomerClosed("IsClosed") = 0 Then
                                                WindowScreeningDetail_IsHaveActiveAccount.Value = "Active"
                                            Else
                                                If Not IsDBNull(drCustomerClosed("ClosedDate")) Then
                                                    WindowScreeningDetail_IsHaveActiveAccount.Value = "Closed ( " & CDate(drCustomerClosed("ClosedDate")).ToString("dd-MMM-yyyy") & " )"
                                                Else
                                                    WindowScreeningDetail_IsHaveActiveAccount.Value = "Closed"
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                                'End of 30-Sep-2021 ganti cara dengan panggil SP

                                If Not IsDBNull(itemrows("CIF")) Then
                                    WindowScreeningDetail_CIF.Text = Convert.ToString(itemrows("CIF"))
                                End If
                                If Not IsDBNull(itemrows("NAMACUSTOMER")) Then
                                    WindowScreeningDetail_CustomerName.Text = Convert.ToString(itemrows("NAMACUSTOMER"))
                                End If
                                If Not IsDBNull(itemrows("DOBCustomer")) Then
                                    WindowScreeningDetail_CustomerDOB.Text = Convert.ToDateTime(itemrows("DOBCustomer")).ToString("dd-MM-yyyy")
                                End If
                                If Not IsDBNull(itemrows("Birth_PlaceCustomer")) Then
                                    WindowScreeningDetail_CustomerPOB.Text = Convert.ToString(itemrows("Birth_PlaceCustomer"))
                                End If
                                If Not IsDBNull(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer")) Then
                                    WindowScreeningDetail_CustomerIdentityType.Text = Convert.ToString(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer"))
                                End If
                                If Not IsDBNull(itemrows("Identity_NumberCustomer")) Then
                                    WindowScreeningDetail_CustomerIdentityNumber.Text = Convert.ToString(itemrows("Identity_NumberCustomer"))
                                End If

                                If Not IsDBNull(itemrows("GCN")) Then
                                    Dim strSQL As String = "SELECT FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer AS Identity_Type, Identity_NumberCustomer AS Identity_Number"
                                    strSQL += " FROM vw_SIPENDAR_CUSTOMER_IDENTITY sci"
                                    strSQL += " WHERE ISNULL(GCN,'')='" & itemrows("GCN") & "'"
                                    Dim dtIdentityCustomer As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

                                    gp_IdentityCustomer.GetStore.DataSource = dtIdentityCustomer
                                    gp_IdentityCustomer.GetStore.DataBind()
                                    gp_IdentityCustomer.Hidden = False
                                Else
                                    gp_IdentityCustomer.Hidden = True
                                End If

                            ElseIf customerinformation = "Non Customer" Then
                                WindowScreeningDetail_CustomerType.Text = "Non Customer"
                                FieldSet2.Title = "Non Customer Data"
                                WindowScreeningDetail_IsHaveActiveAccount.Hidden = True
                                WindowScreeningDetail_GCN.FieldLabel = "Unique Key"
                                WindowScreeningDetail_CIF.FieldLabel = "Role"
                                If Not IsDBNull(itemrows("GCN")) Then
                                    Dim stringgcn As String = Convert.ToString(itemrows("GCN"))
                                    WindowScreeningDetail_GCN.Text = stringgcn
                                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                                    If drResult IsNot Nothing Then
                                        If Not IsDBNull(drResult("Role")) Then
                                            WindowScreeningDetail_CIF.Text = Convert.ToString(drResult("Role"))
                                        End If
                                        If Not IsDBNull(drResult("Name")) Then
                                            WindowScreeningDetail_CustomerName.Text = Convert.ToString(drResult("Name"))
                                        End If
                                        If Not IsDBNull(drResult("Birth_Date")) Then
                                            WindowScreeningDetail_CustomerDOB.Text = Convert.ToDateTime(drResult("Birth_Date")).ToString("dd-MM-yyyy")
                                        End If
                                        If Not IsDBNull(drResult("Birth_Place")) Then
                                            WindowScreeningDetail_CustomerPOB.Text = Convert.ToString(drResult("Birth_Place"))
                                        End If
                                        'If Not IsDBNull(drResult("Country_Code")) Then
                                        '    WindowScreeningDetail_CustomerIdentityType.FieldLabel = "Negara"
                                        '    WindowScreeningDetail_CustomerIdentityType.Hidden = False
                                        '    WindowScreeningDetail_CustomerIdentityType.Text = GetCountryName(Convert.ToString(drResult("Country_Code")))
                                        'End If
                                        If Not IsDBNull(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")) Then
                                            Dim dtIdentityCustomer As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Identifications(Convert.ToString(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
                                            gp_IdentityCustomer.GetStore.DataSource = dtIdentityCustomer
                                            gp_IdentityCustomer.GetStore.DataBind()
                                            gp_IdentityCustomer.Hidden = False
                                        Else
                                            gp_IdentityCustomer.Hidden = True
                                        End If
                                        ' 18 Nov 2021 Ari pengambahan grid link CIF
                                        GridPanelLinkCIF.Hidden = False
                                        If Not IsDBNull(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")) Then
                                            Dim dtLinkCIF As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Account(Convert.ToString(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
                                            GridPanelLinkCIF.GetStore.DataSource = dtLinkCIF
                                            GridPanelLinkCIF.GetStore.DataBind()
                                            GridPanelLinkCIF.Hidden = False
                                        Else
                                            GridPanelLinkCIF.Hidden = True
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If

            'Using objdb As New SiPendarEntities
            '    Dim objResult As SIPENDAR_SCREENING_REQUEST_RESULT = objdb.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = id).FirstOrDefault
            '    If objResult IsNot Nothing Then
            '        If objResult.TotalSimilarity IsNot Nothing Then
            '            WindowScreeningDetail_Similarity.Text = (objResult.TotalSimilarity * 100).ToString() & "%"
            '        End If
            '        If objResult.FK_WATCHLIST_ID_PPATK IsNot Nothing Then
            '            WindowScreeningDetail_IDWatchlistPPATK.Text = objResult.FK_WATCHLIST_ID_PPATK.ToString()
            '        End If
            '        If objResult.Nama IsNot Nothing Then
            '            WindowScreeningDetail_NameSearch.Text = objResult.Nama.ToString()
            '        End If
            '        If objResult.DOB IsNot Nothing Then
            '            WindowScreeningDetail_DOBSearch.Text = objResult.DOB.Value.ToString("dd-MM-yyyy")
            '        End If
            '        If objResult.Birth_Place IsNot Nothing Then
            '            WindowScreeningDetail_POBSearch.Text = objResult.Birth_Place.ToString()
            '        End If
            '        If objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE IsNot Nothing Then
            '            WindowScreeningDetail_IdentityTypeSearch.Text = objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE.ToString()
            '        End If
            '        If objResult.Identity_Number IsNot Nothing Then
            '            WindowScreeningDetail_IdentityNumberSearch.Text = objResult.Identity_Number.ToString()
            '        End If
            '        If objResult.GCN IsNot Nothing Then
            '            WindowScreeningDetail_GCN.Text = objResult.GCN.ToString()
            '        End If
            '        If objResult.NAMACUSTOMER IsNot Nothing Then
            '            WindowScreeningDetail_CustomerName.Text = objResult.NAMACUSTOMER.ToString()
            '        End If
            '        If objResult.DOBCustomer IsNot Nothing Then
            '            WindowScreeningDetail_CustomerDOB.Text = objResult.DOBCustomer.Value.ToString("dd-MM-yyyy")
            '        End If
            '        If objResult.Birth_PlaceCustomer IsNot Nothing Then
            '            WindowScreeningDetail_CustomerPOB.Text = objResult.Birth_PlaceCustomer.ToString()
            '        End If
            '        If objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer IsNot Nothing Then
            '            WindowScreeningDetail_CustomerIdentityType.Text = objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer.ToString()
            '        End If
            '        If objResult.Identity_NumberCustomer IsNot Nothing Then
            '            WindowScreeningDetail_CustomerIdentityNumber.Text = objResult.Identity_NumberCustomer.ToString()
            '        End If
            '    End If
            'End Using
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SetCommandColumnLocation()
        Try
            ColumnActionLocation(gp_ScreeningResult, gp_ScreeningResult_CommandColumn)

        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
#Region "30-Sep-2021 Adi : Get Customer Closed and Closing Date"
    Protected Function GetCustomerClosedStatus(strGCN As String) As DataRow
        Try

            Dim param(0) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@GCN"
            param(0).Value = strGCN
            param(0).DbType = SqlDbType.VarChar

            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_GetCustomerClosedStatus_ByGCN", param)

            Return drResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function
#End Region

    Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Identifications(FK_SIPENDAR_StakeHolder_NonCustomer_ID As String) As DataTable
        Try
            Dim data As New DataTable
            Dim stringquery As String = " select Type as Identity_Type, Number as Identity_Number, Issue_Date, Expiry_Date, Issued_By, Issued_Country, Comments " &
                " from SIPENDAR_StakeHolder_NonCustomer_Identifications where FK_SIPENDAR_StakeHolder_NonCustomer_ID = " & FK_SIPENDAR_StakeHolder_NonCustomer_ID
            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function

    Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer(Unique_Key As String) As DataRow
        Try
            Dim stringquery As String = " select top 1 * from SIPENDAR_StakeHolder_NonCustomer where Unique_Key = '" & Unique_Key & "' "
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery, Nothing)
            Return drResult
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function

    Protected Function GetCountryName(Kode As String) As String
        Try
            Dim tempstring = ""
            Dim stringquery As String = " select top 1 Keterangan from goAML_Ref_Nama_Negara where Kode = '" & Kode & "' "
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery, Nothing)
            If drResult IsNot Nothing Then
                If Not IsDBNull(drResult("Keterangan")) Then
                    tempstring = Convert.ToString(drResult("Keterangan"))
                End If
            End If
            Return tempstring
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function

#Region "18 Nov Ari tambah Fungsi untuk get Link CIF"
    Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Account(FK_SIPENDAR_StakeHolder_NonCustomer_ID As String) As DataTable
        Try
            Dim data As New DataTable
            Dim stringquery As String = " DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) INSERT INTO @tblaccount (Groupaccount) " &
                "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & FK_SIPENDAR_StakeHolder_NonCustomer_ID &
                " select Groupaccount as Account_No, rc.cif as CIF_No , CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),null)   WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),null) else null end as Account_Name from @tblaccount sn" &
                " left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif"
            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function
#End Region

#Region "20-Jul-2022 Penambhan Export Data Screening atas Request ANZ"
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            'Dim DataScreeningAll As DataTable = New DataTable
            'DataScreeningAll = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT_OtherInfo(IDScreeningJudgement)

            Dim strQuery As String = ""
            'mengikuti sp usp_GET_GROUP_RESULT_BY_PK
            strQuery = " declare @PK as bigint = " & IDScreeningJudgement &
             " ;With ResultGroup as ( " &
             " select PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID,PK_SIPENDAR_SCREENING_REQUEST_ID,GCN,result.FK_WATCHLIST_ID_PPATK " &
             " ,reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
             " inner join SIPENDAR_SCREENING_REQUEST_DETAIL reqDet " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = reqdet.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
             " where PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = @PK ) " &
             " SELECT " &
             " result.Nama, " &
             " result.DOB, " &
             " result.Birth_Place, " &
             " result.Identity_Number, " &
             " result.FK_WATCHLIST_ID_PPATK, " &
             " result.GCN, " &
             " (SELECT STRING_AGG (CIF, ',') AS CIF FROM goAML_Ref_Customer where GCN = result.GCN ) as CIF, " &
             " result.NAMACUSTOMER, " &
             " result.DOBCustomer, " &
             " result.Birth_PlaceCustomer, " &
             " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer, " &
             " result.Identity_NumberCustomer, " &
             " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE, " &
             " result.CreatedDate, " &
             " result.Stakeholder_Role, " &
             " (result.TotalSimilarity*100) AS TotalSimilarityPct, " &
             " case when result.Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation " &
             " ,reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
             " inner join ResultGroup RG " &
             " on result.GCN = RG.GCN " &
             " and isnull(result.FK_WATCHLIST_ID_PPATK,result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID) " &
             " = isnull(RG.FK_WATCHLIST_ID_PPATK,RG.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID) " &
             " left join SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO OI " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = OI.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID " &
             " and isnull(OI.Status,'') in ('Accept','Not Match') " &
             " Inner join SIPENDAR_SCREENING_REQUEST_DETAIL reqDet " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = reqdet.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
             " and reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE = RG.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " where OI.PK_SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO_ID is null " &
             " ORDER BY TotalSimilarityPct Desc, result.GCN, Nama "

            Dim DataScreeningAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery,)

            If DataScreeningAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            End If

            '23 Nov 2021 Ari : Penambahan Link To CIF/Account/Account Name
            DataScreeningAll.Columns.Add(New Data.DataColumn("Account_Name", GetType(String)))
            DataScreeningAll.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            DataScreeningAll.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))

            Dim data2 As New DataTable
            For Each row As DataRow In DataScreeningAll.Rows
                If row.Item("customerinformation") = "Non Customer" Then
                    Dim stringgcn As String = Convert.ToString(row("GCN"))
                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                    If drResult IsNot Nothing Then
                        Dim query As String = "DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) " &
                                              "INSERT INTO @tblaccount ( Groupaccount ) " &
                                              "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID") &
                                              " Select STUFF((SELECT ','  + cast(Groupaccount as nvarchar) from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_No, " &
                                              " STUFF((SELECT  ','  +  CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),'') WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),'') " &
                                              " Else '' end from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif " &
                                              " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_Name, STUFF((SELECT ','  + isnull(cast(rc.cif as nvarchar),'') from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as CIF_No "
                        data2 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)
                        For Each row2 As DataRow In data2.Rows
                            row("Account_Name") = row2("Account_Name")
                            row("Account_No") = row2("Account_No")
                            row("CIF_No") = row2("CIF_No")
                        Next
                    Else
                        row("Account_Name") = Nothing
                        row("Account_No") = Nothing
                        row("CIF_No") = Nothing
                    End If
                Else
                    row("Account_Name") = Nothing
                    row("Account_No") = Nothing
                    row("CIF_No") = Nothing
                End If

            Next


            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))



                    Using objtbl As Data.DataTable = DataScreeningAll

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("SipendarScreeningResult")
                            'objtbl.Columns.Remove("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")
                            'objtbl.Columns.Remove("PK_SIPENDAR_SCREENING_REQUEST_ID")
                            'objtbl.Columns.Remove("UserIDRequest")
                            'objtbl.Columns.Remove("CreatedDate")
                            'objtbl.Columns.Remove("FK_SIPENDAR_SCREENING_REQUEST_TYPE_ID")
                            'objtbl.Columns.Remove("_Similarity")
                            'objtbl.Columns.Remove("_Confidence")
                            'objtbl.Columns.Remove("_Similarity_Nama")
                            'objtbl.Columns.Remove("_Similarity_DOB")
                            'objtbl.Columns.Remove("IsNeedToReportSIPENDAR")
                            'objtbl.Columns.Remove("Approved")
                            'objtbl.Columns.Remove("ApprovedBy")
                            'objtbl.Columns.Remove("Nationality")
                            'objtbl.Columns.Remove("NationalityCustomer")
                            'objtbl.Columns.Remove("_Similarity_Birth_Place")
                            'objtbl.Columns.Remove("_Similarity_Identity_Number")
                            'objtbl.Columns.Remove("_Similarity_Nationality")
                            'objtbl.Columns.Remove("PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID")
                            objtbl.Columns("Nama").SetOrdinal(0)
                            objtbl.Columns("DOB").SetOrdinal(1)
                            objtbl.Columns("Birth_Place").SetOrdinal(2)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                            objtbl.Columns("Identity_Number").SetOrdinal(4)
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                            objtbl.Columns("customerinformation").SetOrdinal(7)
                            objtbl.Columns("CIF").SetOrdinal(8)
                            objtbl.Columns("CIF_No").SetOrdinal(9)
                            objtbl.Columns("Account_No").SetOrdinal(10)
                            objtbl.Columns("Account_Name").SetOrdinal(11)
                            objtbl.Columns("GCN").SetOrdinal(12)
                            objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                            objtbl.Columns("DOBCustomer").SetOrdinal(14)
                            objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                            objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                            objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                            objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)

                            objtbl.Columns("Nama").ColumnName = "Name Search Data"
                            objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                            objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                            objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                            objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                            objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                            objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                            objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                            objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                            objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                            objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                            objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                            objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                            objtbl.Columns("Account_No").ColumnName = "Link To Account"
                            objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = DataScreeningAll                    'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        objtbl.Columns("Nama").SetOrdinal(0)
                        objtbl.Columns("DOB").SetOrdinal(1)
                        objtbl.Columns("Birth_Place").SetOrdinal(2)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                        objtbl.Columns("Identity_Number").SetOrdinal(4)
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                        objtbl.Columns("customerinformation").SetOrdinal(7)
                        objtbl.Columns("CIF").SetOrdinal(8)
                        objtbl.Columns("CIF_No").SetOrdinal(9)
                        objtbl.Columns("Account_No").SetOrdinal(10)
                        objtbl.Columns("Account_Name").SetOrdinal(11)
                        objtbl.Columns("GCN").SetOrdinal(12)
                        objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                        objtbl.Columns("DOBCustomer").SetOrdinal(14)
                        objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                        objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                        objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                        objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)

                        objtbl.Columns("Nama").ColumnName = "Name Search Data"
                        objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                        objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                        objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                        objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                        objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                        objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                        objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                        objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                        objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                        objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                        objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                        objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                        objtbl.Columns("Account_No").ColumnName = "Link To Account"
                        objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            Dim pagecurrent As Integer = e.ExtraParams("currentPage")

            'Dim DataScreeningCurrentPage As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "DECLARE @PageNumber AS INT, @RowspPage AS INT SET @PageNumber =" & pagecurrent & " SET @RowspPage = 10 SELECT Nama,DOB,Birth_Place,Stakeholder_Role,case when Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation,CONVERT(varchar(4000),STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer rf WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'')) as CIF,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE,Identity_Number,FK_WATCHLIST_ID_PPATK,GCN,NAMACUSTOMER,DOBCustomer,Birth_PlaceCustomer,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer,Identity_NumberCustomer, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT result WHERE PK_SIPENDAR_SCREENING_REQUEST_ID=" & PK_Request_ID & " ORDER BY TotalSimilarityPct Desc, GCN, Nama OFFSET ((@PageNumber - 1) * @RowspPage) ROWS FETCH NEXT @RowspPage ROWS ONLY",)

            '' Edit 15-Dec-2021 Felix
            Dim strQuery As String = ""
            'mengikuti sp usp_GET_GROUP_RESULT_BY_PK
            strQuery = "DECLARE @PageNumber AS INT, " &
             " @RowspPage AS INT SET @PageNumber =" & pagecurrent &
             " SET @RowspPage = 10 " &
             " declare @PK as bigint = " & IDScreeningJudgement &
             " ;With ResultGroup as ( " &
             " select PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID,PK_SIPENDAR_SCREENING_REQUEST_ID,GCN,result.FK_WATCHLIST_ID_PPATK " &
             " ,reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
             " inner join SIPENDAR_SCREENING_REQUEST_DETAIL reqDet " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = reqdet.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
             " where PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = @PK ) " &
             " SELECT " &
             " result.Nama, " &
             " result.DOB, " &
             " result.Birth_Place, " &
             " result.Identity_Number, " &
             " result.FK_WATCHLIST_ID_PPATK, " &
             " result.GCN, " &
             " (SELECT STRING_AGG (CIF, ',') AS CIF FROM goAML_Ref_Customer where GCN = result.GCN ) as CIF, " &
             " result.NAMACUSTOMER, " &
             " result.DOBCustomer, " &
             " result.Birth_PlaceCustomer, " &
             " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer, " &
             " result.Identity_NumberCustomer, " &
             " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE, " &
             " result.CreatedDate, " &
             " result.Stakeholder_Role, " &
             " (result.TotalSimilarity*100) AS TotalSimilarityPct, " &
             " case when result.Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation " &
             " ,reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
             " inner join ResultGroup RG " &
             " on result.GCN = RG.GCN " &
             " and isnull(result.FK_WATCHLIST_ID_PPATK,result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID) " &
             " = isnull(RG.FK_WATCHLIST_ID_PPATK,RG.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID) " &
             " left join SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO OI " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = OI.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID " &
             " and isnull(OI.Status,'') in ('Accept','Not Match') " &
             " Inner join SIPENDAR_SCREENING_REQUEST_DETAIL reqDet " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = reqdet.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
             " and reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE = RG.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " where OI.PK_SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO_ID is null " &
             " ORDER BY TotalSimilarityPct Desc, result.GCN, Nama " &
             " OFFSET ((@PageNumber - 1) * @RowspPage) ROWS FETCH NEXT @RowspPage ROWS ONLY"

            'Dim DataScreeningCurrentPage As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "DECLARE @PageNumber AS INT, @RowspPage AS INT SET @PageNumber =" & pagecurrent & " SET @RowspPage = 10 SELECT Nama,DOB,Birth_Place,Stakeholder_Role,case when Stakeholder_Role Is null then 'Customer' else 'Non Customer' end as customerinformation,CONVERT(varchar(4000),STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer rf WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'')) as CIF,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE,Identity_Number,FK_WATCHLIST_ID_PPATK,GCN,NAMACUSTOMER,DOBCustomer,Birth_PlaceCustomer,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer,Identity_NumberCustomer, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT result WHERE PK_SIPENDAR_SCREENING_REQUEST_ID=" & PK_Request_ID & " ORDER BY TotalSimilarityPct Desc, GCN, Nama OFFSET ((@PageNumber - 1) * @RowspPage) ROWS FETCH NEXT @RowspPage ROWS ONLY",)
            Dim DataScreeningCurrentPage As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery,)
            '' 15-Dec-2021
            If DataScreeningCurrentPage.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            End If

            '23 Nov 2021 Ari : Penambahan Link To CIF/Account/Account Name
            DataScreeningCurrentPage.Columns.Add(New Data.DataColumn("Account_Name", GetType(String)))
            DataScreeningCurrentPage.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            DataScreeningCurrentPage.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))

            Dim data2 As New DataTable
            For Each row As DataRow In DataScreeningCurrentPage.Rows
                If row.Item("customerinformation") = "Non Customer" Then
                    Dim stringgcn As String = Convert.ToString(row("GCN"))
                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                    If drResult IsNot Nothing Then
                        Dim query As String = "DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) " &
                                              "INSERT INTO @tblaccount ( Groupaccount ) " &
                                              "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID") &
                                              " Select STUFF((SELECT ','  + cast(Groupaccount as nvarchar) from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_No, " &
                                              " STUFF((SELECT  ','  +  CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),'') WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),'') " &
                                              " Else '' end from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif " &
                                              " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_Name, STUFF((SELECT ','  + isnull(cast(rc.cif as nvarchar),'') from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as CIF_No "
                        data2 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)
                        For Each row2 As DataRow In data2.Rows
                            row("Account_Name") = row2("Account_Name")
                            row("Account_No") = row2("Account_No")
                            row("CIF_No") = row2("CIF_No")
                        Next
                    Else
                        row("Account_Name") = Nothing
                        row("Account_No") = Nothing
                        row("CIF_No") = Nothing
                    End If
                Else
                    row("Account_Name") = Nothing
                    row("Account_No") = Nothing
                    row("CIF_No") = Nothing
                End If

            Next


            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))



                    Using objtbl As Data.DataTable = DataScreeningCurrentPage

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("Sipendar_Screening_Request")
                            objtbl.Columns("Nama").SetOrdinal(0)
                            objtbl.Columns("DOB").SetOrdinal(1)
                            objtbl.Columns("Birth_Place").SetOrdinal(2)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                            objtbl.Columns("Identity_Number").SetOrdinal(4)
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                            objtbl.Columns("customerinformation").SetOrdinal(7)
                            objtbl.Columns("CIF").SetOrdinal(8)
                            objtbl.Columns("CIF_No").SetOrdinal(9)
                            objtbl.Columns("Account_No").SetOrdinal(10)
                            objtbl.Columns("Account_Name").SetOrdinal(11)
                            objtbl.Columns("GCN").SetOrdinal(12)
                            objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                            objtbl.Columns("DOBCustomer").SetOrdinal(14)
                            objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                            objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                            objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                            objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)

                            objtbl.Columns("Nama").ColumnName = "Name Search Data"
                            objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                            objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                            objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                            objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                            objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                            objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                            objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                            objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                            objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                            objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                            objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                            objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                            objtbl.Columns("Account_No").ColumnName = "Link To Account"
                            objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)



                    Using objtbl As Data.DataTable = DataScreeningCurrentPage

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        objtbl.Columns("Nama").SetOrdinal(0)
                        objtbl.Columns("DOB").SetOrdinal(1)
                        objtbl.Columns("Birth_Place").SetOrdinal(2)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                        objtbl.Columns("Identity_Number").SetOrdinal(4)
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                        objtbl.Columns("customerinformation").SetOrdinal(7)
                        objtbl.Columns("CIF").SetOrdinal(8)
                        objtbl.Columns("CIF_No").SetOrdinal(9)
                        objtbl.Columns("Account_No").SetOrdinal(10)
                        objtbl.Columns("Account_Name").SetOrdinal(11)
                        objtbl.Columns("GCN").SetOrdinal(12)
                        objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                        objtbl.Columns("DOBCustomer").SetOrdinal(14)
                        objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                        objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                        objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                        objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)

                        objtbl.Columns("Nama").ColumnName = "Name Search Data"
                        objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                        objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                        objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                        objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                        objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                        objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                        objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                        objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                        objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                        objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                        objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                        objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                        objtbl.Columns("Account_No").ColumnName = "Link To Account"
                        objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    Protected Sub ExportSelectedExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)
            Dim selected As RowSelectionModel = gp_ScreeningResult.SelectionModel.Primary
            If selected.SelectedRows.Count = 0 Then

                Throw New Exception("Minimal 1 data harus dipilih!")
            End If
            Dim listScreeninnigResultPK As New List(Of SIPENDAR_SCREENING_REQUEST_RESULT)
            For Each item As SelectedRow In smScreeningResult.SelectedRows
                Dim recordID = item.RecordID.ToString
                Dim objNew = New SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT
                With objNew
                    .PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID
                End With

                listScreeninnigResultPK.Add(objNew)
            Next
            Dim pkscreeningresultselected As String = ""
            Dim listScreeninnigResult As New List(Of SIPENDAR_SCREENING_REQUEST_RESULT)
            For Each item In listScreeninnigResultPK
                Dim cariscreeningrequest As New SIPENDAR_SCREENING_REQUEST_RESULT
                Using objdb As New SiPendarEntities
                    cariscreeningrequest = objdb.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = item.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID).FirstOrDefault()
                End Using
                If cariscreeningrequest IsNot Nothing Then
                    listScreeninnigResult.Add(New SIPENDAR_SCREENING_REQUEST_RESULT() With {
                  .PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = cariscreeningrequest.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID,
                  .Nama = cariscreeningrequest.Nama,
                  .DOBCustomer = cariscreeningrequest.DOBCustomer,
                  .Birth_PlaceCustomer = cariscreeningrequest.Birth_PlaceCustomer,
                  .Identity_Number = cariscreeningrequest.Identity_Number,
                  .FK_WATCHLIST_ID_PPATK = cariscreeningrequest.FK_WATCHLIST_ID_PPATK,
                  .GCN = cariscreeningrequest.GCN,
                  .NAMACUSTOMER = cariscreeningrequest.NAMACUSTOMER,
                  .DOB = cariscreeningrequest.DOB,
                  .Birth_Place = cariscreeningrequest.Birth_Place,
                  .Identity_NumberCustomer = cariscreeningrequest.Identity_Number,
                  .TotalSimilarity = cariscreeningrequest.TotalSimilarity * 100,
                  .FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer = cariscreeningrequest.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer
                  })
                End If

                pkscreeningresultselected = pkscreeningresultselected & "," & item.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
            Next
            pkscreeningresultselected = pkscreeningresultselected.Remove(0, 1)

            '' Edit 15-Dec-2021 Felix
            'Dim strquery As String = "SELECT Nama,DOB,Birth_Place,Stakeholder_Role,case when Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation,CONVERT(VARCHAR(4000),STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'')) as CIF,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE,Identity_Number,FK_WATCHLIST_ID_PPATK,GCN,NAMACUSTOMER,DOBCustomer,Birth_PlaceCustomer,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer,Identity_NumberCustomer, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT result WHERE PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID in(" & pkscreeningresultselected & ") ORDER BY TotalSimilarityPct Desc, GCN, Nama"
            Dim strquery As String = "SELECT result.Nama,result.DOB,result.Birth_Place,result.Stakeholder_Role, " &
                " case when result.Stakeholder_Role Is null then 'Customer' else 'Non Customer' end as customerinformation,CONVERT(VARCHAR(4000),STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'')) as CIF, " &
                " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE,result.Identity_Number,result.FK_WATCHLIST_ID_PPATK, " &
                " req.FK_SIPENDAR_SUBMISSION_TYPE_CODE, " &
                " result.GCN,result.NAMACUSTOMER,result.DOBCustomer,result.Birth_PlaceCustomer,result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer,result.Identity_NumberCustomer, (TotalSimilarity*100) AS TotalSimilarityPct, result.CreatedDate " &
                " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
                " inner join SIPENDAR_SCREENING_REQUEST_DETAIL req " &
                " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = req.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
                " WHERE PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID in(" & pkscreeningresultselected & ") ORDER BY TotalSimilarityPct Desc, GCN, Nama"


            '' End 15-Dec-2021
            Dim DataScreeningSelectResult As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquery,)

            '23 Nov 2021 Ari : Penambahan Link To CIF/Account/Account Name
            DataScreeningSelectResult.Columns.Add(New Data.DataColumn("Account_Name", GetType(String)))
            DataScreeningSelectResult.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            DataScreeningSelectResult.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))
            Dim data2 As New DataTable
            For Each row As DataRow In DataScreeningSelectResult.Rows
                If row.Item("customerinformation") = "Non Customer" Then
                    Dim stringgcn As String = Convert.ToString(row("GCN"))
                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                    If drResult IsNot Nothing Then
                        Dim query As String = "DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) " &
                                              "INSERT INTO @tblaccount ( Groupaccount ) " &
                                              "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID") &
                                              " Select STUFF((SELECT ','  + cast(Groupaccount as nvarchar) from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_No, " &
                                              " STUFF((SELECT  ','  +  CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),'') WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),'') " &
                                              " Else '' end from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif " &
                                              " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_Name, STUFF((SELECT ','  + isnull(cast(rc.cif as nvarchar),'') from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as CIF_No "
                        data2 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)
                        For Each row2 As DataRow In data2.Rows
                            row("Account_Name") = row2("Account_Name")
                            row("Account_No") = row2("Account_No")
                            row("CIF_No") = row2("CIF_No")
                        Next
                    Else
                        row("Account_Name") = Nothing
                        row("Account_No") = Nothing
                        row("CIF_No") = Nothing
                    End If
                Else
                    row("Account_Name") = Nothing
                    row("Account_No") = Nothing
                    row("CIF_No") = Nothing
                End If

            Next


            Dim tablescreeningresult As New DataTable
            Dim fields() As FieldInfo = GetType(SIPENDAR_SCREENING_REQUEST_RESULT).GetFields(BindingFlags.Instance Or BindingFlags.Static Or BindingFlags.NonPublic Or BindingFlags.Public)
            For Each field As FieldInfo In fields

                tablescreeningresult.Columns.Add(field.Name, System.Type.GetType("System.String"))
            Next
            For Each item As SIPENDAR_SCREENING_REQUEST_RESULT In listScreeninnigResult
                Dim row As DataRow = tablescreeningresult.NewRow()
                For Each field As FieldInfo In fields
                    row(field.Name) = field.GetValue(item)

                Next

                tablescreeningresult.Rows.Add(row)
            Next
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    tablescreeningresult.DefaultView.Sort = "_TotalSimilarity Desc, _GCN"
                    Using objtbl As Data.DataTable = DataScreeningSelectResult

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("SipendarScreeningResult")
                            'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")
                            'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_ID")
                            'objtbl.Columns.Remove("_UserIDRequest")
                            'objtbl.Columns.Remove("_CreatedDate")
                            'objtbl.Columns.Remove("_FK_SIPENDAR_SCREENING_REQUEST_TYPE_ID")
                            'objtbl.Columns.Remove("_C_Similarity")
                            'objtbl.Columns.Remove("_C_Confidence")
                            'objtbl.Columns.Remove("_C_Similarity_Nama")
                            'objtbl.Columns.Remove("_C_Similarity_DOB")
                            'objtbl.Columns.Remove("_IsNeedToReportSIPENDAR")
                            'objtbl.Columns.Remove("_Approved")
                            'objtbl.Columns.Remove("_ApprovedBy")
                            'objtbl.Columns.Remove("_Nationality")
                            'objtbl.Columns.Remove("_NationalityCustomer")
                            'objtbl.Columns.Remove("_C_Similarity_Birth_Place")
                            'objtbl.Columns.Remove("_C_Similarity_Identity_Number")
                            'objtbl.Columns.Remove("_C_Similarity_Nationality")
                            'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID")
                            'objtbl.Columns("_Nama").ColumnName = "Name Search Data"
                            'objtbl.Columns("_DOB").ColumnName = "DOB Search Data"
                            'objtbl.Columns("_GCN").ColumnName = "GCN Search Data"
                            'objtbl.Columns("_NAMACUSTOMER").ColumnName = "Name Customer Data"
                            'objtbl.Columns("_DOBCustomer").ColumnName = "DOB Customer Data"
                            'objtbl.Columns("_TotalSimilarity").ColumnName = "Similarity"
                            'objtbl.Columns("_Identity_Number").ColumnName = "Identity Number Search Data"
                            'objtbl.Columns("_Birth_Place").ColumnName = "Birth Place Search Data"
                            'objtbl.Columns("_Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                            'objtbl.Columns("_FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Search Data"
                            'objtbl.Columns("_Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                            'objtbl.Columns("_FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            objtbl.Columns("Nama").SetOrdinal(0)
                            objtbl.Columns("DOB").SetOrdinal(1)
                            objtbl.Columns("Birth_Place").SetOrdinal(2)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                            objtbl.Columns("Identity_Number").SetOrdinal(4)
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                            objtbl.Columns("customerinformation").SetOrdinal(7)
                            objtbl.Columns("CIF").SetOrdinal(8)
                            objtbl.Columns("CIF_No").SetOrdinal(9)
                            objtbl.Columns("Account_No").SetOrdinal(10)
                            objtbl.Columns("Account_Name").SetOrdinal(11)
                            objtbl.Columns("GCN").SetOrdinal(12)
                            objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                            objtbl.Columns("DOBCustomer").SetOrdinal(14)
                            objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                            objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                            objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                            objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)
                            objtbl.Columns("CreatedDate").SetOrdinal(20)

                            objtbl.Columns("Nama").ColumnName = "Name Search Data"
                            objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                            objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                            objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                            objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                            objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                            objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                            objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                            objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                            objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                            objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                            objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                            objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                            objtbl.Columns("Account_No").ColumnName = "Link To Account"
                            objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = DataScreeningSelectResult

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")
                        'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_ID")
                        'objtbl.Columns.Remove("_UserIDRequest")
                        'objtbl.Columns.Remove("_CreatedDate")
                        'objtbl.Columns.Remove("_FK_SIPENDAR_SCREENING_REQUEST_TYPE_ID")
                        'objtbl.Columns.Remove("_C_Similarity")
                        'objtbl.Columns.Remove("_C_Confidence")
                        'objtbl.Columns.Remove("_C_Similarity_Nama")
                        'objtbl.Columns.Remove("_C_Similarity_DOB")
                        'objtbl.Columns.Remove("_IsNeedToReportSIPENDAR")
                        'objtbl.Columns.Remove("_Approved")
                        'objtbl.Columns.Remove("_ApprovedBy")
                        'objtbl.Columns.Remove("_Nationality")
                        'objtbl.Columns.Remove("_NationalityCustomer")
                        'objtbl.Columns.Remove("_C_Similarity_Birth_Place")
                        'objtbl.Columns.Remove("_C_Similarity_Identity_Number")
                        'objtbl.Columns.Remove("_C_Similarity_Nationality")
                        'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID")
                        'objtbl.Columns("_Nama").ColumnName = "Name Search Data"
                        'objtbl.Columns("_DOB").ColumnName = "DOB Search Data"
                        'objtbl.Columns("_GCN").ColumnName = "GCN Search Data"
                        'objtbl.Columns("_NAMACUSTOMER").ColumnName = "Name Customer Data"
                        'objtbl.Columns("_DOBCustomer").ColumnName = "DOB Customer Data"
                        'objtbl.Columns("_TotalSimilarity").ColumnName = "Similarity"
                        'objtbl.Columns("_Identity_Number").ColumnName = "Identity Number Search Data"
                        'objtbl.Columns("_Birth_Place").ColumnName = "Birth Place Search Data"
                        'objtbl.Columns("_Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                        'objtbl.Columns("_FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Search Data"
                        'objtbl.Columns("_Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                        'objtbl.Columns("_FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        objtbl.Columns("Nama").SetOrdinal(0)
                        objtbl.Columns("DOB").SetOrdinal(1)
                        objtbl.Columns("Birth_Place").SetOrdinal(2)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                        objtbl.Columns("Identity_Number").SetOrdinal(4)
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                        objtbl.Columns("customerinformation").SetOrdinal(7)
                        objtbl.Columns("CIF").SetOrdinal(8)
                        objtbl.Columns("CIF_No").SetOrdinal(9)
                        objtbl.Columns("Account_No").SetOrdinal(10)
                        objtbl.Columns("Account_Name").SetOrdinal(11)
                        objtbl.Columns("GCN").SetOrdinal(12)
                        objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                        objtbl.Columns("DOBCustomer").SetOrdinal(14)
                        objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                        objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                        objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                        objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)
                        objtbl.Columns("CreatedDate").SetOrdinal(20)

                        objtbl.Columns("Nama").ColumnName = "Name Search Data"
                        objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                        objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                        objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                        objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                        objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                        objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                        objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                        objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                        objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                        objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                        objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                        objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                        objtbl.Columns("Account_No").ColumnName = "Link To Account"
                        objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    Protected Sub sar_IsSelectedAll_Result_CheckedChanged(sender As Object, e As EventArgs) Handles sar_IsSelectedAll_Result.DirectCheck
        If sar_IsSelectedAll_Result.Value Then
            'Ext.Net.X.Js.Call("Hide")
            gp_ScreeningResult.Hide()
            BtnExport.Hide()
            BtnExportSelected.Hide()
            gp_ScreeningResult_All.Show()
        Else
            'Ext.Net.X.Js.Call("Show")
            gp_ScreeningResult.Show()
            BtnExport.Show()
            BtnExportSelected.Show()
            gp_ScreeningResult_All.Hide()
        End If
    End Sub

    Protected Sub sm_DownloadFromScreeningResult_change(sender As Object, e As EventArgs)
        Try
            Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)
            LabelScreeningResultCount.Text = "Selected : " & smScreeningResult.SelectedRows.Count & " of " & TotalScreeningResult
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class