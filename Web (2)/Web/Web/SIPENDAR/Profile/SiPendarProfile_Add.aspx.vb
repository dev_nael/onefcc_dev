﻿Imports Ext
Imports Elmah
Imports System.Data
Imports SiPendarBLL
Imports SiPendarDAL
Imports System.Data.SqlClient
Imports System.Data.Entity

Partial Class SiPendarProfile_Add
    Inherits ParentPage

    Public Property IDModule() As String
        Get
            Return Session("SiPendarProfile_Add.IDModule")
        End Get
        Set(ByVal value As String)
            Session("SiPendarProfile_Add.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("SiPendarProfile_Add.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("SiPendarProfile_Add.IDUnik") = value
        End Set
    End Property

    'Session untuk menyimpan Account PK ID untuk Account ATM
    Public Property IDAccount() As Long
        Get
            Return Session("SiPendarProfile_Add.IDAccount")
        End Get
        Set(ByVal value As Long)
            Session("SiPendarProfile_Add.IDAccount") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_CLASS() As SIPENDAR_PROFILE_CLASS
        Get
            Return Session("SiPendarProfile_Add.objSIPENDAR_PROFILE_CLASS")
        End Get
        Set(ByVal value As SIPENDAR_PROFILE_CLASS)
            Session("SiPendarProfile_Add.objSIPENDAR_PROFILE_CLASS") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_ACCOUNT_Edit() As SIPENDAR_PROFILE_ACCOUNT
        Get
            Return Session("SiPendarProfile_Add.objSIPENDAR_PROFILE_ACCOUNT_Edit")
        End Get
        Set(ByVal value As SIPENDAR_PROFILE_ACCOUNT)
            Session("SiPendarProfile_Add.objSIPENDAR_PROFILE_ACCOUNT_Edit") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit() As SIPENDAR_PROFILE_ACCOUNT_ATM
        Get
            Return Session("SiPendarProfile_Add.objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit")
        End Get
        Set(ByVal value As SIPENDAR_PROFILE_ACCOUNT_ATM)
            Session("SiPendarProfile_Add.objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_ADDRESS_Edit() As SIPENDAR_PROFILE_ADDRESS
        Get
            Return Session("SiPendarProfile_Add.objSIPENDAR_PROFILE_ADDRESS_Edit")
        End Get
        Set(ByVal value As SIPENDAR_PROFILE_ADDRESS)
            Session("SiPendarProfile_Add.objSIPENDAR_PROFILE_ADDRESS_Edit") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_PHONE_Edit() As SIPENDAR_PROFILE_PHONE
        Get
            Return Session("SiPendarProfile_Add.objSIPENDAR_PROFILE_PHONE_Edit")
        End Get
        Set(ByVal value As SIPENDAR_PROFILE_PHONE)
            Session("SiPendarProfile_Add.objSIPENDAR_PROFILE_PHONE_Edit") = value
        End Set
    End Property

    Public Property objSIPENDAR_PROFILE_IDENTIFICATION_Edit() As SIPENDAR_PROFILE_IDENTIFICATION
        Get
            Return Session("SiPendarProfile_Add.objSIPENDAR_PROFILE_IDENTIFICATION_Edit")
        End Get
        Set(ByVal value As SIPENDAR_PROFILE_IDENTIFICATION)
            Session("SiPendarProfile_Add.objSIPENDAR_PROFILE_IDENTIFICATION_Edit") = value
        End Set
    End Property

    '' 14-Sep-2021 Felix
    Public Property ListIndicator As List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
        Get
            Return Session("SiPendarProfile_Add.ListIndicator")
        End Get
        Set(value As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
            Session("SiPendarProfile_Add.ListIndicator") = value
        End Set
    End Property

    Public Property IDIndicator As String
        Get
            Return Session("SiPendarProfile_Add.IDIndicator")
        End Get
        Set(value As String)
            Session("SiPendarProfile_Add.IDIndicator") = value
        End Set
    End Property
    '' End 14-Sep-2021

    Sub ClearSession()
        objSIPENDAR_PROFILE_CLASS = New SIPENDAR_PROFILE_CLASS

        objSIPENDAR_PROFILE_ACCOUNT_Edit = Nothing
        objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit = Nothing
        objSIPENDAR_PROFILE_ADDRESS_Edit = Nothing
        objSIPENDAR_PROFILE_PHONE_Edit = Nothing
        objSIPENDAR_PROFILE_IDENTIFICATION_Edit = Nothing

        IDAccount = 0

        ListIndicator = New List(Of SiPendarDAL.SIPENDAR_Report_Indicator) '' Added by Felix 14 Sep 2021
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As Integer = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()

                FormPanelInput.Title = ObjModule.ModuleLabel & " - Add"

                SetCommandColumnLocation()

                'Default Customer Type to Individual
                SetDefaultValue()

                '3-Jul-2021 Adi : Set control Visibility sesuai Report Type
                SetControlVisibility(Nothing, Nothing)

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub SetDefaultValue()
        Try

            Using objNawa As New SiPendarDAL.SiPendarEntities
                Dim objCustomerType = objNawa.goAML_Ref_Customer_Type.OrderBy(Function(x) x.PK_Customer_Type_ID).FirstOrDefault
                If Not objCustomerType Is Nothing Then
                    cmb_CUSTOMER_TYPE.SetTextWithTextValue(objCustomerType.PK_Customer_Type_ID, objCustomerType.Description)
                End If
            End Using

            Using objSipendar As New SiPendarDAL.SiPendarEntities
                Dim objGlobalParameter = objSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault
                If Not objGlobalParameter Is Nothing Then
                    Dim objOrganisasi = objSipendar.SIPENDAR_ORGANISASI.Where(Function(x) x.ORGANISASI_CODE = objGlobalParameter.ParameterValue).FirstOrDefault
                    If objOrganisasi IsNot Nothing Then
                        cmb_ORGANISASI_CODE.SetTextWithTextValue(objOrganisasi.ORGANISASI_CODE, objOrganisasi.ORGANISASI_NAME)
                    End If
                End If

                Dim objSIPENDARType = objSipendar.SIPENDAR_TYPE.OrderBy(Function(x) x.PK_SIPENDAR_TYPE_id).FirstOrDefault
                If Not objSIPENDARType Is Nothing Then
                    cmb_SIPENDAR_TYPE_ID.SetTextWithTextValue(objSIPENDARType.PK_SIPENDAR_TYPE_id, objSIPENDARType.SIPENDAR_TYPE_NAME)
                End If

                'Fixing 6-Jul-2021 Adi : Combo Sipendar Type better dilock berdasarkan module name aja
                If ObjModule.ModuleName = "vw_SIPENDAR_PROFILE_PROAKTIF" Then
                    objSIPENDARType = objSipendar.SIPENDAR_TYPE.Where(Function(x) x.PK_SIPENDAR_TYPE_id = 1).FirstOrDefault
                    If Not objSIPENDARType Is Nothing Then
                        cmb_SIPENDAR_TYPE_ID.SetTextWithTextValue(objSIPENDARType.PK_SIPENDAR_TYPE_id, objSIPENDARType.SIPENDAR_TYPE_NAME)
                    End If

                    'Set Control Visibility
                    cmb_JENIS_WATCHLIST_CODE.IsHidden = False
                    cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = False
                    cmb_SUMBER_TYPE_CODE.IsHidden = True
                    txt_SUMBER_ID.Hidden = True
                    txt_Similarity.Hidden = True

                    '8-Jul-2021 : Fitur Generate Transaction
                    fs_Generate_Transaction.Hidden = True
                Else
                    objSIPENDARType = objSipendar.SIPENDAR_TYPE.Where(Function(x) x.PK_SIPENDAR_TYPE_id = 2).FirstOrDefault
                    If Not objSIPENDARType Is Nothing Then
                        cmb_SIPENDAR_TYPE_ID.SetTextWithTextValue(objSIPENDARType.PK_SIPENDAR_TYPE_id, objSIPENDARType.SIPENDAR_TYPE_NAME)
                    End If

                    'Set Control Visibility
                    cmb_JENIS_WATCHLIST_CODE.IsHidden = True
                    cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = True
                    cmb_SUMBER_TYPE_CODE.IsHidden = False
                    txt_SUMBER_ID.Hidden = False
                    txt_Similarity.Hidden = False

                    '8-Jul-2021 : Fitur Generate Transaction
                    fs_Generate_Transaction.Hidden = False
                End If

                cmb_SIPENDAR_TYPE_ID.IsReadOnly = True
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub cmb_CUSTOMER_TYPE_Change(sender As Object, e As DirectEventArgs)
        Try
            fs_Individual.Hidden = True
            fs_Corporate.Hidden = True

            If cmb_CUSTOMER_TYPE.SelectedItemValue = 1 Then
                fs_Individual.Hidden = False
                gp_SIPENDAR_PROFILE_PHONE.Hidden = False
                gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = False
            ElseIf cmb_CUSTOMER_TYPE.SelectedItemValue = 2 Then
                fs_Corporate.Hidden = False
                gp_SIPENDAR_PROFILE_PHONE.Hidden = True
                gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = True
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    '------- ACCOUNT
    Protected Sub btn_SIPENDAR_PROFILE_ACCOUNT_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objSIPENDAR_PROFILE_ACCOUNT_Edit = Nothing
            Clean_Window_SIPENDAR_PROFILE_ACCOUNT()
            Clean_Window_SIPENDAR_PROFILE_ACCOUNT_ATM()

            'Show window pop up
            Window_SIPENDAR_PROFILE_ACCOUNT.Title = "Account - Add"
            Window_SIPENDAR_PROFILE_ACCOUNT.Hidden = False

            'Set IDAccount to 0
            IDAccount = 0
            Bind_SIPENDAR_PROFILE_ACCOUNT_ATM()

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_SIPENDAR_PROFILE_ACCOUNT(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = strID)
                    If objToDelete IsNot Nothing Then
                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Remove(objToDelete)
                    End If
                    Bind_SIPENDAR_PROFILE_ACCOUNT()

                    '9-Sep-2021 hapus jg ATM yang menyangkut account bersangkutan
                    Dim listATMToDelete = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = strID).ToList
                    For Each item In listATMToDelete
                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Remove(item)
                    Next
                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    objSIPENDAR_PROFILE_ACCOUNT_Edit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = strID)
                    Load_Window_SIPENDAR_PROFILE_ACCOUNT(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_ACCOUNT_Cancel_Click()
        Try
            'Hide window pop up
            Window_SIPENDAR_PROFILE_ACCOUNT.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_ACCOUNT_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.FieldLabel & " harus diisi.")
            End If
            If Len(Trim(txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.Value)) < 5 Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.FieldLabel & " minimal 5 karakter.")
            End If

            'Action save here
            Dim intPK As Long = -1

            If objSIPENDAR_PROFILE_ACCOUNT_Edit Is Nothing Then  'Add
                Dim objAdd As New SIPENDAR_PROFILE_ACCOUNT
                If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Count > 0 Then
                    intPK = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Min(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                IDAccount = intPK

                With objAdd
                    .PK_SIPENDAR_PROFILE_ACCOUNT_ID = intPK
                    .CIFNO = txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.Value

                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SelectedItemValue) Then
                        .Fk_goAML_Ref_Jenis_Rekening_CODE = cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SelectedItemValue
                    Else
                        .Fk_goAML_Ref_Jenis_Rekening_CODE = Nothing
                    End If

                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SelectedItemValue) Then
                        .Fk_goAML_Ref_Status_Rekening_CODE = cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SelectedItemValue
                    Else
                        .Fk_goAML_Ref_Status_Rekening_CODE = Nothing
                    End If

                    .NOREKENING = txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.Value
                End With

                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Add(objAdd)
            Else    'Edit
                Dim objEdit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = objSIPENDAR_PROFILE_ACCOUNT_Edit.PK_SIPENDAR_PROFILE_ACCOUNT_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDAccount = .PK_SIPENDAR_PROFILE_ACCOUNT_ID

                        .CIFNO = txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.Value

                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SelectedItemValue) Then
                            .Fk_goAML_Ref_Jenis_Rekening_CODE = cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SelectedItemValue
                        Else
                            .Fk_goAML_Ref_Jenis_Rekening_CODE = Nothing
                        End If

                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SelectedItemValue) Then
                            .Fk_goAML_Ref_Status_Rekening_CODE = cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SelectedItemValue
                        Else
                            .Fk_goAML_Ref_Status_Rekening_CODE = Nothing
                        End If

                        .NOREKENING = txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.Value
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_SIPENDAR_PROFILE_ACCOUNT()

            'Save Account ATM FK_Account_ID
            Dim listATM = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = 0)
            If Not listATM Is Nothing Then
                For Each item In listATM
                    item.FK_SIPENDAR_PROFILE_ACCOUNT_ID = IDAccount
                Next
            End If

            'Hide window popup
            Window_SIPENDAR_PROFILE_ACCOUNT.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_ACCOUNT()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listJenisRekening As List(Of goAML_Ref_Jenis_Rekening) = Nothing
        Dim listStatusRekening As List(Of goAML_Ref_Status_Rekening) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listJenisRekening = objdb.goAML_Ref_Jenis_Rekening.ToList
            listStatusRekening = objdb.goAML_Ref_Status_Rekening.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("JENIS_REKENING", GetType(String)))
        objtable.Columns.Add(New DataColumn("STATUS_REKENING", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objJenisRekening = SiPendarProfile_BLL.GetJenisRekeningByCode(item("Fk_goAML_Ref_Jenis_Rekening_CODE").ToString)
                'If objJenisRekening IsNot Nothing Then
                '    item("JENIS_REKENING") = objJenisRekening.Keterangan
                'End If

                'Dim objStatusRekening = SiPendarProfile_BLL.GetStatusRekeningByCode(item("Fk_goAML_Ref_Status_Rekening_CODE").ToString)
                'If objStatusRekening IsNot Nothing Then
                '    item("STATUS_REKENING") = objStatusRekening.Keterangan
                'End If

                If listJenisRekening IsNot Nothing Then
                    Dim objJenisRekening = listJenisRekening.Find(Function(x) x.Kode = item("Fk_goAML_Ref_Jenis_Rekening_CODE").ToString)
                    If objJenisRekening IsNot Nothing Then
                        item("JENIS_REKENING") = objJenisRekening.Keterangan
                    End If
                End If

                If listStatusRekening IsNot Nothing Then
                    Dim objStatusRekening = listStatusRekening.Find(Function(x) x.Kode = item("Fk_goAML_Ref_Status_Rekening_CODE").ToString)
                    If objStatusRekening IsNot Nothing Then
                        item("STATUS_REKENING") = objStatusRekening.Keterangan
                    End If
                End If
            Next
        End If

        gp_SIPENDAR_PROFILE_ACCOUNT.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_ACCOUNT.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_SIPENDAR_PROFILE_ACCOUNT()
        'Clean fields
        txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.Value = Nothing
        cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SetTextValue("")
        cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SetTextValue("")
        txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.Value = Nothing

        'Set fields' ReadOnly
        txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.ReadOnly = False
        cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.IsReadOnly = False
        cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.IsReadOnly = False
        txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.ReadOnly = False

        'Show Buttons
        btn_SIPENDAR_PROFILE_ACCOUNT_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_SIPENDAR_PROFILE_ACCOUNT(strAction As String)
        'Clean window pop up
        Clean_Window_SIPENDAR_PROFILE_ACCOUNT()

        If objSIPENDAR_PROFILE_ACCOUNT_Edit IsNot Nothing Then
            'Populate fields
            With objSIPENDAR_PROFILE_ACCOUNT_Edit
                txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.Value = .CIFNO
                txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.Value = .NOREKENING

                If .Fk_goAML_Ref_Jenis_Rekening_CODE IsNot Nothing Then
                    Dim objJenisRekening = SiPendarProfile_BLL.GetJenisRekeningByCode(.Fk_goAML_Ref_Jenis_Rekening_CODE)
                    If Not objJenisRekening Is Nothing Then
                        cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.SetTextWithTextValue(objJenisRekening.Kode, objJenisRekening.Keterangan)
                    End If
                End If

                If .Fk_goAML_Ref_Status_Rekening_CODE IsNot Nothing Then
                    Dim objStatusRekening = SiPendarProfile_BLL.GetStatusRekeningByCode(.Fk_goAML_Ref_Status_Rekening_CODE)
                    If Not objStatusRekening Is Nothing Then
                        cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.SetTextWithTextValue(objStatusRekening.Kode, objStatusRekening.Keterangan)
                    End If
                End If
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.ReadOnly = False
            cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.IsReadOnly = False
            cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.IsReadOnly = False
            txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.ReadOnly = False
        Else
            txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO.ReadOnly = True
            cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING.IsReadOnly = True
            cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING.IsReadOnly = True
            txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING.ReadOnly = True

            btn_SIPENDAR_PROFILE_ACCOUNT_Save.Hidden = True '' Added on 12 Aug 2021
        End If

        'Bind ATM
        IDAccount = objSIPENDAR_PROFILE_ACCOUNT_Edit.PK_SIPENDAR_PROFILE_ACCOUNT_ID
        Bind_SIPENDAR_PROFILE_ACCOUNT_ATM()

        'Show window pop up
        Window_SIPENDAR_PROFILE_ACCOUNT.Title = "Account - " & strAction
        Window_SIPENDAR_PROFILE_ACCOUNT.Hidden = False
    End Sub
    '------- END OF ACCOUNT

    '------- ADDRESS
    Protected Sub btn_SIPENDAR_PROFILE_AddRESS_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objSIPENDAR_PROFILE_ADDRESS_Edit = Nothing
            Clean_Window_SIPENDAR_PROFILE_AddRESS()

            'Show window pop up
            Window_SIPENDAR_PROFILE_ADDRESS.Title = "Address - Add"
            Window_SIPENDAR_PROFILE_ADDRESS.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_SIPENDAR_PROFILE_AddRESS(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value
            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = strID)
                    If objToDelete IsNot Nothing Then
                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Remove(objToDelete)
                    End If
                    Bind_SIPENDAR_PROFILE_AddRESS()
                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    objSIPENDAR_PROFILE_ADDRESS_Edit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = strID)
                    Load_Window_SIPENDAR_PROFILE_AddRESS(strAction)
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_AddRESS_Cancel_Click()
        Try
            'Hide window pop up
            Window_SIPENDAR_PROFILE_ADDRESS.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_AddRESS_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_ADDRESS_CITY.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_ADDRESS_CITY.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.Label & " harus diisi.")
            End If
            'If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_ADDRESS_ZIP.Value) Then
            '    Throw New ApplicationException(txt_SIPENDAR_PROFILE_ADDRESS_ZIP.FieldLabel & " harus diisi.")
            'End If

            'Action save here
            Dim intPK As Long = -1

            If objSIPENDAR_PROFILE_ADDRESS_Edit Is Nothing Then  'Add
                Dim objAdd As New SIPENDAR_PROFILE_ADDRESS
                If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Count > 0 Then
                    intPK = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Min(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                With objAdd
                    .PK_SIPENDAR_PROFILE_ADDRESS_ID = intPK

                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SelectedItemValue) Then
                        .FK_goAML_Ref_Kategori_Kontak_CODE = cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SelectedItemValue
                    Else
                        .FK_goAML_Ref_Kategori_Kontak_CODE = Nothing
                    End If
                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SelectedItemValue) Then
                        .FK_goAML_Ref_Nama_Negara_CODE = cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SelectedItemValue
                    Else
                        .FK_goAML_Ref_Nama_Negara_CODE = Nothing
                    End If

                    .ADDRESS = Trim(txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.Value)
                    .TOWN = Trim(txt_SIPENDAR_PROFILE_ADDRESS_TOWN.Value)
                    .CITY = Trim(txt_SIPENDAR_PROFILE_ADDRESS_CITY.Value)
                    .STATE = Trim(txt_SIPENDAR_PROFILE_ADDRESS_STATE.Value)
                    .ZIP = Trim(txt_SIPENDAR_PROFILE_ADDRESS_ZIP.Value)
                    .COMMENTS = Trim(txt_SIPENDAR_PROFILE_ADDRESS_COMMENTS.Value)
                End With

                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Add(objAdd)
            Else    'Edit
                Dim objEdit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = objSIPENDAR_PROFILE_ADDRESS_Edit.PK_SIPENDAR_PROFILE_ADDRESS_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SelectedItemValue) Then
                            .FK_goAML_Ref_Kategori_Kontak_CODE = cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SelectedItemValue
                        Else
                            .FK_goAML_Ref_Kategori_Kontak_CODE = Nothing
                        End If
                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SelectedItemValue) Then
                            .FK_goAML_Ref_Nama_Negara_CODE = cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SelectedItemValue
                        Else
                            .FK_goAML_Ref_Nama_Negara_CODE = Nothing
                        End If

                        .ADDRESS = Trim(txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.Value)
                        .TOWN = Trim(txt_SIPENDAR_PROFILE_ADDRESS_TOWN.Value)
                        .CITY = Trim(txt_SIPENDAR_PROFILE_ADDRESS_CITY.Value)
                        .STATE = Trim(txt_SIPENDAR_PROFILE_ADDRESS_STATE.Value)
                        .ZIP = Trim(txt_SIPENDAR_PROFILE_ADDRESS_ZIP.Value)
                        .COMMENTS = Trim(txt_SIPENDAR_PROFILE_ADDRESS_COMMENTS.Value)
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_SIPENDAR_PROFILE_AddRESS()

            'Hide window popup
            Window_SIPENDAR_PROFILE_ADDRESS.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_AddRESS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listAddressType As List(Of goAML_Ref_Kategori_Kontak) = Nothing
        Dim listCountryName As List(Of goAML_Ref_Nama_Negara) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listAddressType = objdb.goAML_Ref_Kategori_Kontak.ToList
            listCountryName = objdb.goAML_Ref_Nama_Negara.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("ADDRESS_TYPE_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objAddressType = SiPendarProfile_BLL.GetKategoriKontakByCode(item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                'If objAddressType IsNot Nothing Then
                '    item("ADDRESS_TYPE_NAME") = objAddressType.Keterangan
                'Else
                '    item("ADDRESS_TYPE_NAME") = Nothing
                'End If

                'Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(item("FK_goAML_Ref_Nama_Negara_CODE").ToString)
                'If objCountry IsNot Nothing Then
                '    item("COUNTRY_NAME") = objCountry.Keterangan
                'Else
                '    item("COUNTRY_NAME") = Nothing
                'End If

                If listAddressType IsNot Nothing Then
                    Dim objAddressType = listAddressType.Find(Function(x) x.Kode = item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                    If objAddressType IsNot Nothing Then
                        item("ADDRESS_TYPE_NAME") = objAddressType.Keterangan
                    End If
                End If

                If listCountryName IsNot Nothing Then
                    Dim objCountry = listCountryName.Find(Function(x) x.Kode = item("FK_goAML_Ref_Nama_Negara_CODE").ToString)
                    If objCountry IsNot Nothing Then
                        item("COUNTRY_NAME") = objCountry.Keterangan
                    End If
                End If
            Next
        End If

        gp_SIPENDAR_PROFILE_ADDRESS.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_ADDRESS.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_SIPENDAR_PROFILE_AddRESS()
        'Clean fields
        cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SetTextValue("")
        txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.Value = Nothing
        txt_SIPENDAR_PROFILE_ADDRESS_CITY.Value = Nothing
        cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SetTextValue("")
        txt_SIPENDAR_PROFILE_ADDRESS_ZIP.Value = Nothing

        txt_SIPENDAR_PROFILE_ADDRESS_TOWN.Value = Nothing
        txt_SIPENDAR_PROFILE_ADDRESS_STATE.Value = Nothing
        txt_SIPENDAR_PROFILE_ADDRESS_COMMENTS.Value = Nothing

        'Set fields' ReadOnly
        cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.IsReadOnly = False
        txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.ReadOnly = False
        txt_SIPENDAR_PROFILE_ADDRESS_CITY.ReadOnly = False
        cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.IsReadOnly = False
        txt_SIPENDAR_PROFILE_ADDRESS_ZIP.ReadOnly = False

        txt_SIPENDAR_PROFILE_ADDRESS_TOWN.ReadOnly = False
        txt_SIPENDAR_PROFILE_ADDRESS_STATE.ReadOnly = False
        txt_SIPENDAR_PROFILE_ADDRESS_COMMENTS.ReadOnly = False

        'Show Buttons
        btn_SIPENDAR_PROFILE_ADDRESS_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_SIPENDAR_PROFILE_AddRESS(strAction As String)
        'Clean window pop up
        Clean_Window_SIPENDAR_PROFILE_AddRESS()

        If objSIPENDAR_PROFILE_ADDRESS_Edit IsNot Nothing Then
            'Populate fields
            With objSIPENDAR_PROFILE_ADDRESS_Edit
                txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.Value = .ADDRESS
                txt_SIPENDAR_PROFILE_ADDRESS_CITY.Value = .CITY
                txt_SIPENDAR_PROFILE_ADDRESS_ZIP.Value = .ZIP

                txt_SIPENDAR_PROFILE_ADDRESS_TOWN.Value = .TOWN
                txt_SIPENDAR_PROFILE_ADDRESS_STATE.Value = .STATE
                txt_SIPENDAR_PROFILE_ADDRESS_COMMENTS.Value = .COMMENTS

                If Not IsNothing(.FK_goAML_Ref_Kategori_Kontak_CODE) Then
                    Dim objAddressType = SiPendarProfile_BLL.GetKategoriKontakByCode(.FK_goAML_Ref_Kategori_Kontak_CODE)
                    If objAddressType IsNot Nothing Then
                        cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.SetTextWithTextValue(objAddressType.Kode, objAddressType.Keterangan)
                    End If
                End If

                If Not IsNothing(.FK_goAML_Ref_Nama_Negara_CODE) Then
                    Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(.FK_goAML_Ref_Nama_Negara_CODE)
                    If objCountry IsNot Nothing Then
                        cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.SetTextWithTextValue(objCountry.Kode, objCountry.Keterangan)
                    End If
                End If

            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE.IsReadOnly = True
            txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS.ReadOnly = True
            txt_SIPENDAR_PROFILE_ADDRESS_CITY.ReadOnly = True
            cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE.IsReadOnly = True
            txt_SIPENDAR_PROFILE_ADDRESS_ZIP.ReadOnly = True

            txt_SIPENDAR_PROFILE_ADDRESS_TOWN.ReadOnly = True
            txt_SIPENDAR_PROFILE_ADDRESS_STATE.ReadOnly = True
            txt_SIPENDAR_PROFILE_ADDRESS_COMMENTS.ReadOnly = True

            btn_SIPENDAR_PROFILE_ADDRESS_Save.Hidden = True
        End If

        'Show window pop up
        Window_SIPENDAR_PROFILE_ADDRESS.Title = "Address - " & strAction
        Window_SIPENDAR_PROFILE_ADDRESS.Hidden = False
    End Sub
    '------- END OF ADDRESS

    '------- PHONE
    Protected Sub btn_SIPENDAR_PROFILE_PHONE_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objSIPENDAR_PROFILE_PHONE_Edit = Nothing
            Clean_Window_SIPENDAR_PROFILE_PHONE()

            'Show window pop up
            Window_SIPENDAR_PROFILE_PHONE.Title = "PHONE - Add"
            Window_SIPENDAR_PROFILE_PHONE.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_SIPENDAR_PROFILE_PHONE(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value
            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = strID)
                    If objToDelete IsNot Nothing Then
                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Remove(objToDelete)
                    End If
                    Bind_SIPENDAR_PROFILE_PHONE()
                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    objSIPENDAR_PROFILE_PHONE_Edit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = strID)
                    Load_Window_SIPENDAR_PROFILE_PHONE(strAction)
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_PHONE_Cancel_Click()
        Try
            'Hide window pop up
            Window_SIPENDAR_PROFILE_PHONE.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_PHONE_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_PHONE_NUMBER.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_PHONE_NUMBER.FieldLabel & " harus diisi.")
            End If

            'Action save here
            Dim intPK As Long = -1

            If objSIPENDAR_PROFILE_PHONE_Edit Is Nothing Then  'Add
                Dim objAdd As New SIPENDAR_PROFILE_PHONE
                If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Count > 0 Then
                    intPK = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Min(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                With objAdd
                    .PK_SIPENDAR_PROFILE_PHONE_ID = intPK

                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SelectedItemValue) Then
                        .FK_goAML_Ref_Kategori_Kontak_CODE = cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SelectedItemValue
                    Else
                        .FK_goAML_Ref_Kategori_Kontak_CODE = Nothing
                    End If

                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SelectedItemValue) Then
                        .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SelectedItemValue
                    Else
                        .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = Nothing
                    End If

                    .COUNTRY_PREFIX = Trim(txt_SIPENDAR_PROFILE_PHONE_COUNTRY_PREFIX.Value)
                    .PHONE_NUMBER = Trim(txt_SIPENDAR_PROFILE_PHONE_NUMBER.Value)
                    .EXTENSION_NUMBER = Trim(txt_SIPENDAR_PROFILE_PHONE_EXTENSION_NUMBER.Value)
                    .COMMENTS = Trim(txt_SIPENDAR_PROFILE_PHONE_COMMENTS.Value)
                End With

                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Add(objAdd)
            Else    'Edit
                Dim objEdit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = objSIPENDAR_PROFILE_PHONE_Edit.PK_SIPENDAR_PROFILE_PHONE_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SelectedItemValue) Then
                            .FK_goAML_Ref_Kategori_Kontak_CODE = cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SelectedItemValue
                        Else
                            .FK_goAML_Ref_Kategori_Kontak_CODE = Nothing
                        End If

                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SelectedItemValue) Then
                            .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SelectedItemValue
                        Else
                            .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = Nothing
                        End If

                        .COUNTRY_PREFIX = Trim(txt_SIPENDAR_PROFILE_PHONE_COUNTRY_PREFIX.Value)
                        .PHONE_NUMBER = Trim(txt_SIPENDAR_PROFILE_PHONE_NUMBER.Value)
                        .EXTENSION_NUMBER = Trim(txt_SIPENDAR_PROFILE_PHONE_EXTENSION_NUMBER.Value)
                        .COMMENTS = Trim(txt_SIPENDAR_PROFILE_PHONE_COMMENTS.Value)
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_SIPENDAR_PROFILE_PHONE()

            'Hide window popup
            Window_SIPENDAR_PROFILE_PHONE.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_PHONE()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listPhoneType As List(Of goAML_Ref_Kategori_Kontak) = Nothing
        Dim listCommunicationType As List(Of goAML_Ref_Jenis_Alat_Komunikasi) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listPhoneType = objdb.goAML_Ref_Kategori_Kontak.ToList
            listCommunicationType = objdb.goAML_Ref_Jenis_Alat_Komunikasi.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("PHONE_TYPE_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("COMMUNICATION_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objPHONEType = SiPendarProfile_BLL.GetKategoriKontakByCode(item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                'If objPHONEType IsNot Nothing Then
                '    item("PHONE_TYPE_NAME") = objPHONEType.Keterangan
                'Else
                '    item("PHONE_TYPE_NAME") = Nothing
                'End If

                'Dim objCommunicationType = SiPendarProfile_BLL.GetJenisAlatKomunikasiByCode(item("FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE").ToString)
                'If objCommunicationType IsNot Nothing Then
                '    item("COMMUNICATION_TYPE_NAME") = objCommunicationType.Keterangan
                'Else
                '    item("COMMUNICATION_TYPE_NAME") = Nothing
                'End If

                If listPhoneType IsNot Nothing Then
                    Dim objPHONEType = listPhoneType.Find(Function(x) x.Kode = item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
                    If objPHONEType IsNot Nothing Then
                        item("PHONE_TYPE_NAME") = objPHONEType.Keterangan
                    End If
                End If

                If listCommunicationType IsNot Nothing Then
                    Dim objCommunicationType = listCommunicationType.Find(Function(x) x.Kode = item("FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE").ToString)
                    If objCommunicationType IsNot Nothing Then
                        item("COMMUNICATION_TYPE_NAME") = objCommunicationType.Keterangan
                    End If
                End If

            Next
        End If

        gp_SIPENDAR_PROFILE_PHONE.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_PHONE.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_SIPENDAR_PROFILE_PHONE()
        'Clean fields
        cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SetTextValue("")
        cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SetTextValue("")
        txt_SIPENDAR_PROFILE_PHONE_COUNTRY_PREFIX.Value = Nothing
        txt_SIPENDAR_PROFILE_PHONE_NUMBER.Value = Nothing

        txt_SIPENDAR_PROFILE_PHONE_EXTENSION_NUMBER.Value = Nothing
        txt_SIPENDAR_PROFILE_PHONE_COMMENTS.Value = Nothing

        'Set fields' ReadOnly
        cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.IsReadOnly = False
        cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.IsReadOnly = False
        txt_SIPENDAR_PROFILE_PHONE_COUNTRY_PREFIX.ReadOnly = False
        txt_SIPENDAR_PROFILE_PHONE_NUMBER.ReadOnly = False

        txt_SIPENDAR_PROFILE_PHONE_EXTENSION_NUMBER.ReadOnly = False
        txt_SIPENDAR_PROFILE_PHONE_COMMENTS.ReadOnly = False

        'Show Buttons
        btn_SIPENDAR_PROFILE_PHONE_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_SIPENDAR_PROFILE_PHONE(strAction As String)
        'Clean window pop up
        Clean_Window_SIPENDAR_PROFILE_PHONE()

        If objSIPENDAR_PROFILE_PHONE_Edit IsNot Nothing Then
            'Populate fields
            With objSIPENDAR_PROFILE_PHONE_Edit
                txt_SIPENDAR_PROFILE_PHONE_COUNTRY_PREFIX.Value = .COUNTRY_PREFIX
                txt_SIPENDAR_PROFILE_PHONE_NUMBER.Value = .PHONE_NUMBER

                txt_SIPENDAR_PROFILE_PHONE_EXTENSION_NUMBER.Value = .EXTENSION_NUMBER
                txt_SIPENDAR_PROFILE_PHONE_COMMENTS.Value = .COMMENTS

                If Not IsNothing(.FK_goAML_Ref_Kategori_Kontak_CODE) Then
                    Dim objPHONEType = SiPendarProfile_BLL.GetKategoriKontakByCode(.FK_goAML_Ref_Kategori_Kontak_CODE)
                    If objPHONEType IsNot Nothing Then
                        cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.SetTextWithTextValue(objPHONEType.Kode, objPHONEType.Keterangan)
                    End If
                End If

                If Not IsNothing(.FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE) Then
                    Dim objCommunicationType = SiPendarProfile_BLL.GetJenisAlatKomunikasiByCode(.FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE)
                    If objCommunicationType IsNot Nothing Then
                        cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.SetTextWithTextValue(objCommunicationType.Kode, objCommunicationType.Keterangan)
                    End If
                End If
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE.IsReadOnly = True
            cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE.IsReadOnly = True
            txt_SIPENDAR_PROFILE_PHONE_COUNTRY_PREFIX.ReadOnly = True
            txt_SIPENDAR_PROFILE_PHONE_NUMBER.ReadOnly = True

            txt_SIPENDAR_PROFILE_PHONE_EXTENSION_NUMBER.ReadOnly = True
            txt_SIPENDAR_PROFILE_PHONE_COMMENTS.ReadOnly = True

            btn_SIPENDAR_PROFILE_PHONE_Save.Hidden = True
        End If

        'Show window pop up
        Window_SIPENDAR_PROFILE_PHONE.Title = "PHONE - " & strAction
        Window_SIPENDAR_PROFILE_PHONE.Hidden = False
    End Sub
    '------- END OF PHONE

    '------- IDENTIFICATION
    Protected Sub btn_SIPENDAR_PROFILE_IDENTIFICATION_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objSIPENDAR_PROFILE_IDENTIFICATION_Edit = Nothing
            Clean_Window_SIPENDAR_PROFILE_IDENTIFICATION()

            'Show window pop up
            Window_SIPENDAR_PROFILE_IDENTIFICATION.Title = "IDENTIFICATION - Add"
            Window_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_SIPENDAR_PROFILE_IDENTIFICATION(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value
            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = strID)
                    If objToDelete IsNot Nothing Then
                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Remove(objToDelete)
                    End If
                    Bind_SIPENDAR_PROFILE_IDENTIFICATION()
                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    objSIPENDAR_PROFILE_IDENTIFICATION_Edit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = strID)
                    Load_Window_SIPENDAR_PROFILE_IDENTIFICATION(strAction)
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_IDENTIFICATION_Cancel_Click()
        Try
            'Hide window pop up
            Window_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_IDENTIFICATION_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.FieldLabel & " harus diisi.")
            End If
            'If CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value) = DateTime.MinValue Then
            '    Throw New ApplicationException(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.FieldLabel & " harus diisi.")
            'End If
            'If CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value) = DateTime.MinValue Then
            '    Throw New ApplicationException(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.FieldLabel & " harus diisi.")
            'End If
            If String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.Label & " harus diisi.")
            End If

            If Not String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value) Then
                Dim strIDENTIFICATIONType As String = cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SelectedItemValue
                Dim strSSN As String = txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value
                If strIDENTIFICATIONType = "KTP" And (Len(strSSN) <> 16 Or Not strSSN.All(AddressOf Char.IsDigit)) Then
                    Throw New ApplicationException(txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.FieldLabel & " must 16 digit numeric only for KTP.")
                End If
            End If

            If CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value) <> DateTime.MinValue AndAlso CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value) <> DateTime.MinValue Then
                If CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value) > CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value) Then
                    Throw New ApplicationException(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.FieldLabel & " harus >= dari " & txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.FieldLabel)
                End If
            End If

            'Action save here
            Dim intPK As Long = -1

            If objSIPENDAR_PROFILE_IDENTIFICATION_Edit Is Nothing Then  'Add
                Dim objAdd As New SIPENDAR_PROFILE_IDENTIFICATION
                If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Count > 0 Then
                    intPK = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Min(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                With objAdd
                    .PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = intPK

                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SelectedItemValue) Then
                        .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SelectedItemValue
                    Else
                        .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = Nothing
                    End If
                    If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SelectedItemValue) Then
                        .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SelectedItemValue
                    Else
                        .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = Nothing
                    End If

                    If Not CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value) = DateTime.MinValue Then
                        .ISSUE_DATE = CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value)
                    Else
                        .ISSUE_DATE = Nothing
                    End If

                    If Not CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value) = DateTime.MinValue Then
                        .EXPIRED_DATE = CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value)
                    Else
                        .EXPIRED_DATE = Nothing
                    End If

                    .IDENTITY_NUMBER = Trim(txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value)
                    .ISSUED_BY = Trim(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUED_BY.Value)
                    .Comments = Trim(txt_SIPENDAR_PROFILE_IDENTIFICATION_COMMENTS.Value)
                End With

                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Add(objAdd)
            Else    'Edit
                Dim objEdit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = objSIPENDAR_PROFILE_IDENTIFICATION_Edit.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SelectedItemValue) Then
                            .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SelectedItemValue
                        Else
                            .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = Nothing
                        End If
                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SelectedItemValue) Then
                            .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SelectedItemValue
                        Else
                            .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = Nothing
                        End If

                        If Not CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value) = DateTime.MinValue Then
                            .ISSUE_DATE = CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value)
                        Else
                            .ISSUE_DATE = Nothing
                        End If

                        If Not CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value) = DateTime.MinValue Then
                            .EXPIRED_DATE = CDate(txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value)
                        Else
                            .EXPIRED_DATE = Nothing
                        End If

                        .IDENTITY_NUMBER = Trim(txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value)
                        .ISSUED_BY = Trim(txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUED_BY.Value)
                        .Comments = Trim(txt_SIPENDAR_PROFILE_IDENTIFICATION_COMMENTS.Value)
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_SIPENDAR_PROFILE_IDENTIFICATION()

            'Hide window popup
            Window_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_IDENTIFICATION()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION)

        '01-Sep-2021 : Adi Fixing bind gridpanel lambat. Get the list from database biar cepat searchingnya
        Dim listIdentificationType As List(Of goAML_Ref_Jenis_Dokumen_Identitas) = Nothing
        Dim listCountryName As List(Of goAML_Ref_Nama_Negara) = Nothing

        Using objdb As New SiPendarDAL.SiPendarEntities
            listIdentificationType = objdb.goAML_Ref_Jenis_Dokumen_Identitas.ToList
            listCountryName = objdb.goAML_Ref_Nama_Negara.ToList
        End Using

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("IDENTIFICATION_TYPE_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("ISSUED_COUNTRY", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                'Dim objIDENTIFICATIONType = SiPendarProfile_BLL.GetJenisDokumenIdentitasByCode(item("Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ToString)
                'If objIDENTIFICATIONType IsNot Nothing Then
                '    item("IDENTIFICATION_TYPE_NAME") = objIDENTIFICATIONType.Keterangan
                'Else
                '    item("IDENTIFICATION_TYPE_NAME") = Nothing
                'End If

                'Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(item("FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY").ToString)
                'If objCountry IsNot Nothing Then
                '    item("ISSUED_COUNTRY") = objCountry.Keterangan
                'Else
                '    item("ISSUED_COUNTRY") = Nothing
                'End If

                If listIdentificationType IsNot Nothing Then
                    Dim objIDENTIFICATIONType = listIdentificationType.Find(Function(x) x.Kode = item("Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ToString)
                    If objIDENTIFICATIONType IsNot Nothing Then
                        item("IDENTIFICATION_TYPE_NAME") = objIDENTIFICATIONType.Keterangan
                    End If
                End If

                If listCountryName IsNot Nothing Then
                    Dim objCountry = listCountryName.Find(Function(x) x.Kode = item("FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY").ToString)
                    If objCountry IsNot Nothing Then
                        item("ISSUED_COUNTRY") = objCountry.Keterangan
                    End If
                End If
            Next
        End If

        gp_SIPENDAR_PROFILE_IDENTIFICATION.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_IDENTIFICATION.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_SIPENDAR_PROFILE_IDENTIFICATION()
        'Clean fields
        cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SetTextValue("")
        txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value = Nothing
        txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value = Nothing
        txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value = Nothing
        txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUED_BY.Value = Nothing
        cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SetTextValue("")

        txt_SIPENDAR_PROFILE_IDENTIFICATION_COMMENTS.Value = Nothing

        'Set fields' ReadOnly
        cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.IsReadOnly = False
        txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.ReadOnly = False
        txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.ReadOnly = False
        txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.ReadOnly = False
        txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUED_BY.ReadOnly = False
        cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.IsReadOnly = False

        txt_SIPENDAR_PROFILE_IDENTIFICATION_COMMENTS.ReadOnly = False

        'Show Buttons
        btn_SIPENDAR_PROFILE_IDENTIFICATION_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_SIPENDAR_PROFILE_IDENTIFICATION(strAction As String)
        'Clean window pop up
        Clean_Window_SIPENDAR_PROFILE_IDENTIFICATION()

        If objSIPENDAR_PROFILE_IDENTIFICATION_Edit IsNot Nothing Then
            'Populate fields
            With objSIPENDAR_PROFILE_IDENTIFICATION_Edit
                txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.Value = .IDENTITY_NUMBER

                If .ISSUE_DATE.HasValue() Then
                    txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.Value = .ISSUE_DATE
                End If
                If .EXPIRED_DATE.HasValue() Then
                    txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.Value = .EXPIRED_DATE
                End If

                txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUED_BY.Value = .ISSUED_BY
                txt_SIPENDAR_PROFILE_IDENTIFICATION_COMMENTS.Value = .Comments

                If Not IsNothing(.Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE) Then
                    Dim objIDENTIFICATIONType = SiPendarProfile_BLL.GetJenisDokumenIdentitasByCode(.Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE)
                    If objIDENTIFICATIONType IsNot Nothing Then
                        cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.SetTextWithTextValue(objIDENTIFICATIONType.Kode, objIDENTIFICATIONType.Keterangan)
                    End If
                End If
                If Not IsNothing(.FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY) Then
                    Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(.FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY)
                    If objCountry IsNot Nothing Then
                        cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.SetTextWithTextValue(objCountry.Kode, objCountry.Keterangan)
                    End If
                End If
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE.IsReadOnly = True
            txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER.ReadOnly = True
            txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE.ReadOnly = True
            txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE.ReadOnly = True
            txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUED_BY.ReadOnly = True
            cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY.IsReadOnly = True
            txt_SIPENDAR_PROFILE_IDENTIFICATION_COMMENTS.ReadOnly = True

            btn_SIPENDAR_PROFILE_IDENTIFICATION_Save.Hidden = True '' Added on 12 Aug 2021
        End If

        'Show window pop up
        Window_SIPENDAR_PROFILE_IDENTIFICATION.Title = "IDENTIFICATION - " & strAction
        Window_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = False
    End Sub
    '------- END OF IDENTIFICATION

    '------- ACCOUNT_ATM
    Protected Sub btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit = Nothing
            Clean_Window_SIPENDAR_PROFILE_ACCOUNT_ATM()

            'Show window pop up
            Window_SIPENDAR_PROFILE_ACCOUNT_ATM.Title = "ACCOUNT ATM - Add"
            Window_SIPENDAR_PROFILE_ACCOUNT_ATM.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_SIPENDAR_PROFILE_ACCOUNT_ATM(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value
            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = strID)
                    If objToDelete IsNot Nothing Then
                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Remove(objToDelete)
                    End If
                    Bind_SIPENDAR_PROFILE_ACCOUNT_ATM()
                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = strID)
                    Load_Window_SIPENDAR_PROFILE_ACCOUNT_ATM(strAction)
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Cancel_Click()
        Try
            'Hide window pop up
            Window_SIPENDAR_PROFILE_ACCOUNT_ATM.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.Value) Then
                Throw New ApplicationException(txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.FieldLabel & " harus diisi.")
            End If

            'Action save here
            Dim intPK As Long = -1

            If objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit Is Nothing Then  'Add
                Dim objAdd As New SIPENDAR_PROFILE_ACCOUNT_ATM
                If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Count > 0 Then
                    intPK = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Min(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                With objAdd
                    .PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = intPK
                    .FK_SIPENDAR_PROFILE_ACCOUNT_ID = IDAccount
                    .NOATM = Trim(txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.Value)
                End With

                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Add(objAdd)
            Else    'Edit
                Dim objEdit = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        .NOATM = Trim(txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.Value)
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_SIPENDAR_PROFILE_ACCOUNT_ATM()

            'Hide window popup
            Window_SIPENDAR_PROFILE_ACCOUNT_ATM.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_SIPENDAR_PROFILE_ACCOUNT_ATM()
        Dim listATM = New List(Of SIPENDAR_PROFILE_ACCOUNT_ATM)

        If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Count > 0 Then
            listATM = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = IDAccount).ToList
            If listATM Is Nothing Then
                listATM = New List(Of SIPENDAR_PROFILE_ACCOUNT_ATM)
            End If

        End If
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listATM)

        'Running the following script for additional columns
        'objtable.Columns.Add(New DataColumn("ACCOUNT_ATM_TYPE_NAME", GetType(String)))

        'If objtable.Rows.Count > 0 Then
        '    For Each item As Data.DataRow In objtable.Rows
        '        Dim objACCOUNT_ATMType = SiPendarProfile_BLL.GetKategoriKontakByCode(item("FK_goAML_Ref_Kategori_Kontak_CODE").ToString)
        '        If objACCOUNT_ATMType IsNot Nothing Then
        '            item("ACCOUNT_ATM_TYPE_NAME") = objACCOUNT_ATMType.Keterangan
        '        Else
        '            item("ACCOUNT_ATM_TYPE_NAME") = Nothing
        '        End If
        '    Next
        'End If

        gp_SIPENDAR_PROFILE_ACCOUNT_ATM.GetStore().DataSource = objtable
        gp_SIPENDAR_PROFILE_ACCOUNT_ATM.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_SIPENDAR_PROFILE_ACCOUNT_ATM()
        'Clean fields
        txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.Value = Nothing

        'Set fields' ReadOnly
        txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.ReadOnly = False

        'Show Buttons
        btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_SIPENDAR_PROFILE_ACCOUNT_ATM(strAction As String)
        'Clean window pop up
        Clean_Window_SIPENDAR_PROFILE_ACCOUNT_ATM()

        'Populate fields
        With objSIPENDAR_PROFILE_ACCOUNT_ATM_Edit
            txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.Value = .NOATM
        End With

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM.ReadOnly = True
            btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Save.Hidden = True
        End If

        'Show window pop up
        Window_SIPENDAR_PROFILE_ACCOUNT_ATM.Title = "ACCOUNT ATM - " & strAction
        Window_SIPENDAR_PROFILE_ACCOUNT_ATM.Hidden = False
    End Sub
    '------- END OF ACCOUNT_ATM

    '------- DOCUMENT SAVE
    Protected Sub btn_SIPENDAR_PROFILE_Submit_Click()
        Try
            'Data Validation
            ValidateData()

            'Populate Data
            PopulateData()

            '1-Jul-2021 Validasi jadi pakai Validation Parameter
            Dim ErrorValidation As String = SiPendarProfile_BLL.checkValidation(objSIPENDAR_PROFILE_CLASS, ObjModule, 1)
            If Not String.IsNullOrEmpty(ErrorValidation) Then
                SiPendarBLL.NawaFramework.extInfoPanelupdate(FormPanelInput, ErrorValidation, "infoValidationResultPanel")
                FormPanelInput.Body.ScrollTo(Direction.Top, 0)
                infoValidationResultPanel.Hidden = False
                Exit Sub
            End If

            'Save Data With/Without Approval
            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
                SiPendarProfile_BLL.SaveAddWithoutApproval(ObjModule, objSIPENDAR_PROFILE_CLASS)
                LblConfirmation.Text = "Data Saved into Database"
            Else
                SiPendarProfile_BLL.SaveAddWithApproval(ObjModule, objSIPENDAR_PROFILE_CLASS)
                LblConfirmation.Text = "Data Saved into Pending Approval"
            End If

            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ValidateData()
        Try
            'Validate Header
            If String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_TYPE_ID.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_CUSTOMER_TYPE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_CUSTOMER_TYPE.Label & " harus diisi.")
            End If
            If String.IsNullOrWhiteSpace(txt_GCN.Value) Then
                Throw New ApplicationException("GCN Harus diisi")
            End If
            '' To Do

            '8-Jul-2021 : Fitur Generate Transaction
            If chk_IsGenerateTransaction.Checked Then
                If CDate(txt_Transaction_DateFrom.Value) = DateTime.MinValue Or CDate(txt_Transaction_DateTo.Value) = DateTime.MinValue Then
                    Throw New ApplicationException(txt_SUMBER_ID.FieldLabel & " Tanggal transaksi From dan To harus diisi. Jika pilihan Generate Transaction dicentang.")
                End If
                If CDate(txt_Transaction_DateFrom.Value) <> DateTime.MinValue And CDate(txt_Transaction_DateTo.Value) <> DateTime.MinValue Then
                    If CDate(txt_Transaction_DateFrom.Value) > CDate(txt_Transaction_DateTo.Value) Then
                        Throw New ApplicationException(txt_SUMBER_ID.FieldLabel & " Tanggal transaksi From harus < Tanggal transaksi To.")
                    End If
                End If

                Dim dtFrom As Date = CDate(txt_Transaction_DateFrom.Value)
                Dim dtTo As Date = CDate(txt_Transaction_DateTo.Value)

                Dim objList = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch_GCN(txt_GCN.Value, dtFrom, dtTo)
                If objList Is Nothing OrElse objList.Count <= 0 Then
                    Throw New ApplicationException("GCN " & txt_GCN.Value & " tidak memiliki data transaksi di range tanggal tersebut.")
                End If

                'cek apakah GCN ini sudah ada di SIPENDAR_Report pada tanggal yang sama
                Using objDb As New SiPendarDAL.SiPendarEntities
                    Dim strGCN As String = txt_GCN.Value
                    Dim objCek = objDb.SIPENDAR_Report.Where(Function(x) x.UnikReference = strGCN And DbFunctions.TruncateTime(x.Submission_Date) = DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault
                    If objCek IsNot Nothing Then
                        Throw New ApplicationException("Tidak bisa generate Report Transaction. Customer ini sudah punya Report Transaction di hari ini.")
                    End If
                End Using

                'Indicator wajib diisi minimal 1 Added by Felix on 14-Sep-2021
                If ListIndicator.Count = 0 Then
                    Throw New ApplicationException("Indikator wajib diisi minimal 1")
                End If
                'End of 14 Sep 2021

                '' Added by Felix 01 Sep 2021
                Dim CheckLawanKosong As Long = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER where PK_GlobalReportParameter_ID = 8", Nothing) '' Added by Felix on 30 Aug 2021
                If CheckLawanKosong = 1 Then
                    Dim objCheckLawan = SiPendarBLL.SIPENDARGenerateBLL.CheckCounterPartyExistForTransaction(txt_GCN.Value, txt_Transaction_DateFrom.Value, txt_Transaction_DateTo.Value)
                    If objCheckLawan.Count > 0 Then
                        Throw New ApplicationException("GCN " & txt_GCN.Value & " memiliki transaksi yang tidak mempunyai lawan untuk periode yang dipilih.")
                    End If
                    '' End of Felix 01 Sep 2021
                End If '' End of 30 Aug 2021
            End If

            '' Added on 02 Aug 2021
            'Cek apakah sudah pernah dilaporkan pada hari yang sama

            'Dim objGlobalParameterPerType As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
            Dim objDbSipendar As New SiPendarDAL.SiPendarEntities
            'objGlobalParameterPerType = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 7).FirstOrDefault

            'Dim objprofile As New SiPendarDAL.SIPENDAR_PROFILE
            'Dim GCN As String = txt_GCN.Value
            'Dim submissiontype As String = ""
            'submissiontype = cmb_SUBMISSION_TYPE_CODE.SelectedItemValue
            'Dim sumber_id As String = ""
            'sumber_id = txt_SUMBER_ID.Value
            '' 21 Dec 2021 Ari : Penambahan untuk pengayaan validasi nya akan dicari gcn + sumber id + submission type
            'If objGlobalParameterPerType.ParameterValue = 1 Then
            '    If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
            '        objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue).FirstOrDefault
            '    Else
            '        objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue And x.FK_SIPENDAR_SUBMISSION_TYPE_CODE = submissiontype And x.SUMBER_ID = sumber_id).FirstOrDefault
            '    End If
            'Else
            '    If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
            '        objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault
            '    Else
            '        objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_SUBMISSION_TYPE_CODE = submissiontype And x.SUMBER_ID = sumber_id).FirstOrDefault
            '    End If

            'End If

            '' 21 Dec 2021 Ari : Penambahan untuk keterengan validasi
            'If objprofile IsNot Nothing Then
            '    If Not (txt_INDV_NAME.Value = Nothing Or txt_INDV_NAME.Value = "") Then
            '        If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
            '            Throw New ApplicationException("<br>GCN : " & txt_GCN.Value & " - " & txt_INDV_NAME.Value & " sudah pernah dilaporkan pada hari ini.")
            '        Else
            '            Throw New ApplicationException("<br>GCN-SumberID-SubmissionType : " & txt_GCN.Value & " - " & txt_SUMBER_ID.Value & " - " & cmb_SUBMISSION_TYPE_CODE.SelectedItemValue & " and " & txt_INDV_NAME.Value & " sudah pernah dilaporkan pada hari ini.")
            '        End If
            '    End If

            '    If Not (txt_CORP_NAME.Value = Nothing Or txt_CORP_NAME.Value = "") Then
            '        If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
            '            Throw New ApplicationException("<br>GCN : " & txt_GCN.Value & " - " & txt_CORP_NAME.Value & " sudah pernah dilaporkan pada hari ini.")
            '        Else
            '            Throw New ApplicationException("<br>GCN-SumberID-SubmissionType : " & txt_GCN.Value & " - " & txt_SUMBER_ID.Value & " - " & cmb_SUBMISSION_TYPE_CODE.SelectedItemValue & " and " & txt_CORP_NAME.Value & " sudah pernah dilaporkan pada hari ini.")
            '        End If
            '    End If
            'End If

            Dim objProfileModule As NawaDAL.Module = Nothing
            If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PROAKTIF")
            Else
                objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PENGAYAAN")
            End If

            ' 28 Dec 2021 Ari penambahan validasi gcn dengan format baru :
            Dim objprofile As New SiPendarDAL.SIPENDAR_PROFILE
            Dim GCN As String = txt_GCN.Value
            Dim submissiontype As String = ""
            submissiontype = cmb_SUBMISSION_TYPE_CODE.SelectedItemValue
            Dim sumber_id As String = ""
            sumber_id = txt_SUMBER_ID.Value


            Dim objGlobalParameterPerTypes As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
            objGlobalParameterPerTypes = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 7).FirstOrDefault

            Dim Modulekey As String = ""
            If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                Modulekey = cmb_SIPENDAR_TYPE_ID.SelectedItemValue & "-" & GCN & "-" & Now.ToString("yyyy-MM-dd") & "_0+" & submissiontype
                'ModuleKey = cmb_SIPENDAR_TYPE_ID.SelectedItemValue & "-" & gcn & "-" & Now.ToString("yyyy-MM-dd")
            Else
                Modulekey = cmb_SIPENDAR_TYPE_ID.SelectedItemValue & "-" & GCN & "-" & Now.ToString("yyyy-MM-dd") & "_" & sumber_id & "+" & submissiontype
            End If

            ' 28 Dec 2021 Ari : valiidasi untuk approval
            If SiPendarProfile_BLL.IsExistsInApprovalProfileType(objProfileModule.ModuleName, Modulekey, "1", objGlobalParameterPerTypes.ParameterValue) Then
                'LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."
                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                    Throw New ApplicationException("Sorry, New Profile for GCN " & GCN & " today is already exists in Pending Approval.")
                Else
                    Throw New ApplicationException("Sorry, New Profile for GCN " & GCN & " ,SUMBER ID " & sumber_id & " , and SUBMISSION Type " & submissiontype & " today is already exists in Pending Approval.")
                End If
                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
            End If



            If objGlobalParameterPerTypes.ParameterValue = 1 Then
                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue).FirstOrDefault
                Else
                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue And x.FK_SIPENDAR_SUBMISSION_TYPE_CODE = submissiontype And x.SUMBER_ID = sumber_id).FirstOrDefault
                End If
            Else
                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault
                Else
                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_SUBMISSION_TYPE_CODE = submissiontype And x.SUMBER_ID = sumber_id).FirstOrDefault
                End If
            End If

            ' 21 Dec 2021 Ari : Penambahan untuk keterengan validasi
            If objprofile IsNot Nothing Then
                If Not (txt_INDV_NAME.Value = Nothing Or txt_INDV_NAME.Value = "") Then
                    If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                        Throw New ApplicationException("<br>GCN : " & txt_GCN.Value & " - " & txt_INDV_NAME.Value & " sudah pernah dilaporkan pada hari ini.")
                    Else
                        Throw New ApplicationException("<br>GCN-SumberID-SubmissionType : " & txt_GCN.Value & " - " & txt_SUMBER_ID.Value & " - " & cmb_SUBMISSION_TYPE_CODE.SelectedItemValue & " and " & txt_INDV_NAME.Value & " sudah pernah dilaporkan pada hari ini.")
                    End If
                End If

                If Not (txt_CORP_NAME.Value = Nothing Or txt_CORP_NAME.Value = "") Then
                    If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                        Throw New ApplicationException("<br>GCN : " & txt_GCN.Value & " - " & txt_CORP_NAME.Value & " sudah pernah dilaporkan pada hari ini.")
                    Else
                        Throw New ApplicationException("<br>GCN-SumberID-SubmissionType : " & txt_GCN.Value & " - " & txt_SUMBER_ID.Value & " - " & cmb_SUBMISSION_TYPE_CODE.SelectedItemValue & " and " & txt_CORP_NAME.Value & " sudah pernah dilaporkan pada hari ini.")
                    End If
                End If
            End If



            'If txt_GCN.Value IsNot Nothing And txt_GCN.Value <> "" Then
            '    If SiPendarProfile_BLL.IsExistsInApprovalProfileType(objProfileModule.ModuleName, cmb_SIPENDAR_TYPE_ID.SelectedItemValue & "-" & txt_GCN.Value & "-" & Now.ToString("yyyy-MM-dd"), "1", objGlobalParameterPerType.ParameterValue) Then
            '        'LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."
            '        Throw New ApplicationException("Sorry, New Profile for GCN " & txt_GCN.Value & " today is already exists in Pending Approval.")
            '        'Panelconfirmation.Hidden = False
            '        'FormPanelInput.Hidden = True
            '    End If
            'End If
            ' 1 Dec 2021 Ari penambahan untuk validasi keterangan
            If txt_Keterangan.Value IsNot Nothing Then
                If Not String.IsNullOrEmpty(txt_Keterangan.Value) Then
                    Dim keterangan As String = ""
                    keterangan = txt_Keterangan.Value.ToString
                    If keterangan.Length > 500 Then
                        Throw New ApplicationException("Keterangan Tidak Boleh Lebih Dari 500 Kata")
                    End If
                End If
            End If
            '' End of 02 Aug 2021

            '3-Jul-2021 Hanya SIPENDAR_TYPE aja yg perlu karena penting untuk validasi
            Exit Sub

            If String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_JENIS_WATCHLIST_CODE.Label & " harus diisi.")
            End If
            'If String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue) Then
            '    Throw New ApplicationException(cmb_TINDAK_PIDANA_CODE.Label & " harus diisi.")
            'End If
            If String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SUMBER_INFORMASI_KHUSUS_CODE.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_ORGANISASI_CODE.SelectedItemValue) Then
                Throw New ApplicationException(cmb_ORGANISASI_CODE.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(txt_GCN.Value) Then
                Throw New ApplicationException(txt_GCN.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(txt_Keterangan.Value) Then
                Throw New ApplicationException(txt_Keterangan.FieldLabel & " harus diisi.")
            End If

            If String.IsNullOrEmpty(txt_Similarity.Text) Or txt_Similarity.Text = "N/A" Or txt_Similarity.Text = "NA" Then
                Throw New ApplicationException(txt_Similarity.FieldLabel & " harus diisi.")
            End If

            'Validate Profile
            If cmb_CUSTOMER_TYPE.SelectedItemValue = 1 Then     'Individual
                If String.IsNullOrEmpty(txt_INDV_NAME.Value) Then
                    Throw New ApplicationException(txt_INDV_NAME.FieldLabel & " harus diisi.")
                End If
                If String.IsNullOrEmpty(txt_INDV_PLACEOFBIRTH.Value) Then
                    Throw New ApplicationException(txt_INDV_PLACEOFBIRTH.FieldLabel & " harus diisi.")
                End If
                If CDate(txt_INDV_DOB.Value) = DateTime.MinValue Then
                    Throw New ApplicationException(txt_INDV_DOB.FieldLabel & " harus diisi.")
                End If
                'If String.IsNullOrEmpty(txt_INDV_ADDRESS.Value) Then
                '    Throw New ApplicationException(txt_INDV_ADDRESS.FieldLabel & " harus diisi.")
                'End If
                If String.IsNullOrEmpty(cmb_INDV_NATIONALITY.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_INDV_NATIONALITY.Label & " harus diisi.")
                End If
            ElseIf cmb_CUSTOMER_TYPE.SelectedItemValue = 2 Then     'Corporate
                If String.IsNullOrEmpty(txt_CORP_NAME.Value) Then
                    Throw New ApplicationException(txt_CORP_NAME.FieldLabel & " harus diisi.")
                End If
                If String.IsNullOrEmpty(txt_CORP_NPWP.Value) Then
                    Throw New ApplicationException(txt_CORP_NPWP.FieldLabel & " harus diisi.")
                End If
                If String.IsNullOrEmpty(txt_CORP_NO_IZIN_USAHA.Value) Then
                    Throw New ApplicationException(txt_CORP_NO_IZIN_USAHA.FieldLabel & " harus diisi.")
                End If
                If String.IsNullOrEmpty(cmb_CORP_COUNTRY.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_CORP_COUNTRY.Label & " harus diisi.")
                End If
            End If

            'Validate data Detail
            'If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT Is Nothing OrElse objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Count = 0 Then
            '    Throw New ApplicationException("Minimal 1 data Account harus diisi.")
            'End If
            'If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS Is Nothing OrElse objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Count = 0 Then
            '    Throw New ApplicationException("Minimal 1 data Address harus diisi.")
            'End If
            'If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE Is Nothing OrElse objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Count = 0 Then
            '    Throw New ApplicationException("Minimal 1 data Phone harus diisi.")
            'End If
            'If objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION Is Nothing OrElse objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Count = 0 Then
            '    Throw New ApplicationException("Minimal 1 data Identification harus diisi.")
            'End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub PopulateData()
        Try
            With objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE
                'Populate Data Header
                If Not String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
                    .FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.SelectedItemValue
                End If
                If Not String.IsNullOrEmpty(cmb_CUSTOMER_TYPE.SelectedItemValue) Then
                    .Fk_goAML_Ref_Customer_Type_id = cmb_CUSTOMER_TYPE.SelectedItemValue
                End If

                'SIPENDAR Proaktif
                If Not String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue) Then
                    .FK_SIPENDAR_JENIS_WATCHLIST_CODE = cmb_JENIS_WATCHLIST_CODE.SelectedItemValue
                End If
                If Not String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue) Then
                    .FK_SIPENDAR_TINDAK_PIDANA_CODE = cmb_TINDAK_PIDANA_CODE.SelectedItemValue
                End If
                If Not String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue) Then
                    .FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE = cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue
                End If
                If Not String.IsNullOrEmpty(cmb_ORGANISASI_CODE.SelectedItemValue) Then
                    .FK_SIPENDAR_ORGANISASI_CODE = cmb_ORGANISASI_CODE.SelectedItemValue
                End If

                'SIPENDAR Pengayaan
                If Not String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue) Then
                    .FK_SIPENDAR_SUMBER_TYPE_CODE = cmb_SUMBER_TYPE_CODE.SelectedItemValue
                End If
                'If Not (String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A" Or txt_SUMBER_ID.Text = "NA") Then
                '    .SUMBER_ID = CInt(txt_SUMBER_ID.Value)
                'Else
                '    .SUMBER_ID = Nothing
                'End If
                If Not (String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A" Or txt_SUMBER_ID.Text = "NA") And cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Then
                    If txt_SUMBER_ID.Text > 2147483647 Then
                        Throw New ApplicationException("Angka yang dimasukan maksimal 2147483647 ")
                    Else

                        .SUMBER_ID = CInt(txt_SUMBER_ID.Value)
                    End If

                Else
                    .SUMBER_ID = Nothing
                End If
                If Not (String.IsNullOrEmpty(txt_Similarity.Text) Or txt_Similarity.Text = "N/A" Or txt_Similarity.Text = "NA") Then
                    .Similarity = CDbl(txt_Similarity.Value)
                Else
                    .Similarity = Nothing
                End If

                .GCN = Trim(txt_GCN.Value)
                .Keterangan = Trim(txt_Keterangan.Value)

                'Tambahan 8-Jul-2021
                .Submission_Date = DateTime.Now
                If Not String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue) Then
                    .FK_SIPENDAR_SUBMISSION_TYPE_CODE = cmb_SUBMISSION_TYPE_CODE.SelectedItemValue
                End If
                If chk_IsGenerateTransaction.Checked Then
                    If CDate(txt_Transaction_DateFrom.Value) <> DateTime.MinValue Then
                        .Transaction_DateFrom = CDate(txt_Transaction_DateFrom.Value)
                    End If
                    If CDate(txt_Transaction_DateTo.Value) <> DateTime.MinValue Then
                        .Transaction_DateTo = CDate(txt_Transaction_DateTo.Value)
                    End If

                    'Added by Felix 14-Sep-2021
                    If ListIndicator.Count > 0 Then
                        Dim i As Integer
                        Dim temp As String
                        i = 0
                        temp = ""
                        For Each item In ListIndicator
                            i += 1
                            If ListIndicator.Count = 1 Then
                                'temp = getReportIndicatorByKode(item.FK_Indicator)
                                temp = item.FK_Indicator
                            Else
                                If i = 1 Then
                                    'temp = getReportIndicatorByKode(item.FK_Indicator)
                                    temp = item.FK_Indicator
                                Else
                                    'temp += "," + getReportIndicatorByKode(item.FK_Indicator)
                                    temp += "," + item.FK_Indicator
                                End If
                            End If
                        Next
                        .Report_Indicator = temp
                    End If
                    'End of 14-Sep-2021

                End If
                'End of Tambahan 8-Jul-2021

                'Profile Data
                If cmb_CUSTOMER_TYPE.SelectedItemValue = 1 Then     'Individual
                    .INDV_NAME = Trim(txt_INDV_NAME.Value)
                    .INDV_PLACEOFBIRTH = Trim(txt_INDV_PLACEOFBIRTH.Value)

                    If Not CDate(txt_INDV_DOB.Value) = DateTime.MinValue Then
                        .INDV_DOB = CDate(txt_INDV_DOB.Value)
                    Else
                        .INDV_DOB = Nothing
                    End If

                    .INDV_ADDRESS = Trim(txt_INDV_ADDRESS.Value)
                    .INDV_FK_goAML_Ref_Nama_Negara_CODE = cmb_INDV_NATIONALITY.SelectedItemValue

                    'Kosongkan data Corporate
                    .CORP_NAME = Nothing
                    .CORP_NPWP = Nothing
                    .CORP_NO_IZIN_USAHA = Nothing
                    .CORP_FK_goAML_Ref_Nama_Negara_CODE = Nothing
                ElseIf cmb_CUSTOMER_TYPE.SelectedItemValue = 2 Then     'Corporate
                    .CORP_NAME = Trim(txt_CORP_NAME.Value)
                    .CORP_NPWP = Trim(txt_CORP_NPWP.Value)
                    .CORP_NO_IZIN_USAHA = Trim(txt_CORP_NO_IZIN_USAHA.Value)
                    .CORP_FK_goAML_Ref_Nama_Negara_CODE = cmb_CORP_COUNTRY.SelectedItemValue

                    'Kosongkan data Individual
                    .INDV_NAME = Nothing
                    .INDV_PLACEOFBIRTH = Nothing
                    .INDV_DOB = Nothing
                    .INDV_ADDRESS = Nothing
                    .INDV_FK_goAML_Ref_Nama_Negara_CODE = Nothing
                End If

            End With

            'Isi default PJK_ID dengan default value
            Dim strPJKID As String = Nothing
            Using objSipendar As New SiPendarDAL.SiPendarEntities
                Dim objGlobalParameter = objSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault
                If Not objGlobalParameter Is Nothing Then
                    Dim objOrganisasi = objSipendar.SIPENDAR_ORGANISASI.Where(Function(x) x.ORGANISASI_CODE = objGlobalParameter.ParameterValue).FirstOrDefault
                    If objOrganisasi IsNot Nothing Then
                        strPJKID = objOrganisasi.ORGANISASI_CODE
                    End If
                End If
            End Using

            'Untuk tujuan validasi isikan semua SIPENDAR_PROFILE_ID jadi -1
            With objSIPENDAR_PROFILE_CLASS
                Dim pkProfileID As Long = -1
                .objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID = pkProfileID

                For Each item1 In .objList_SIPENDAR_PROFILE_ACCOUNT
                    item1.FK_SIPENDAR_PROFILE_ID = pkProfileID
                    item1.FK_SIPENDAR_ORGANISASI_CODE = strPJKID
                Next
                For Each item2 In .objList_SIPENDAR_PROFILE_ACCOUNT_ATM
                    item2.FK_SIPENDAR_PROFILE_ID = pkProfileID
                Next
                For Each item3 In .objList_SIPENDAR_PROFILE_ADDRESS
                    item3.FK_SIPENDAR_PROFILE_ID = pkProfileID
                Next
                For Each item4 In .objList_SIPENDAR_PROFILE_PHONE
                    item4.FK_SIPENDAR_PROFILE_ID = pkProfileID
                Next
                For Each item5 In .objList_SIPENDAR_PROFILE_IDENTIFICATION
                    item5.FK_SIPENDAR_PROFILE_ID = pkProfileID
                Next

            End With

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_SIPENDAR_PROFILE_Back_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF DOCUMENT SAVE

    Protected Sub btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SetCommandColumnLocation()
        ColumnActionLocation(gp_SIPENDAR_PROFILE_ACCOUNT, cc_SIPENDAR_PROFILE_ACCOUNT)
        ColumnActionLocation(gp_SIPENDAR_PROFILE_ACCOUNT_ATM, cc_SIPENDAR_PROFILE_ACCOUNT_ATM)
        ColumnActionLocation(gp_SIPENDAR_PROFILE_ADDRESS, cc_SIPENDAR_PROFILE_ADDRESS)
        ColumnActionLocation(gp_SIPENDAR_PROFILE_PHONE, cc_SIPENDAR_PROFILE_PHONE)
        ColumnActionLocation(gp_SIPENDAR_PROFILE_IDENTIFICATION, cc_SIPENDAR_PROFILE_IDENTIFICATION)

        ColumnActionLocation(gp_SEARCH_CUSTOMER, cc_SEARCH_CUSTOMER)

    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub SetControlVisibility(sender As Object, e As DirectEventArgs)
        Try
            '3-Jul-2021 Adi : Hide field yang awalnya ada di XML tapi di XSD tidak ada
            cmb_TINDAK_PIDANA_CODE.IsHidden = True
            cmb_ORGANISASI_CODE.IsHidden = True
            txt_INDV_ADDRESS.Hidden = True

            'Sesuaikan dengan SIPENDAR_TYPE
            If Not String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                    cmb_JENIS_WATCHLIST_CODE.IsHidden = False
                    cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = False
                    cmb_SUMBER_TYPE_CODE.IsHidden = True
                    txt_SUMBER_ID.Hidden = True
                    txt_Similarity.Hidden = True
                Else
                    cmb_JENIS_WATCHLIST_CODE.IsHidden = True
                    cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = True
                    cmb_SUMBER_TYPE_CODE.IsHidden = False
                    txt_SUMBER_ID.Hidden = False
                    txt_Similarity.Hidden = False
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "Update 6 Juli 2021 Adi : Penambahan fitur Search Customer by GCN"
    Protected Sub Store_SEARCH_CUSTOMER_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 20

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")
            '' Remarked on 19 Jul 2021
            'If String.IsNullOrEmpty(strfilter) Then
            '    strfilter = "GCN IS NOT NULL"
            'Else
            '    strfilter += " AND GCN IS NOT NULL"
            'End If

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            If strsort = "" Then
                strsort = "NamaLengkap asc, GCN asc"
            End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Customer_For_SIPENDAR", "GCN, CIF, NamaLengkap, TanggalLahir", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start <0 OrElse limit <0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            StoreSEARCH_CUSTOMER.DataSource = DataPaging
            StoreSEARCH_CUSTOMER.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SearchCustomer_Click()
        Try
            window_SEARCH_CUSTOMER.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_SEARCH_CUSTOMER(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            Dim strGCN = e.ExtraParams(0).Value
            LoadCustomerData(strGCN)

            window_SEARCH_CUSTOMER.Hidden = True

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadCustomerData(strGCN As String)
        Try
            'Ambil data2 dari NawadataDevEntities
            Dim objCustomer As SiPendarDAL.goAML_Ref_Customer = Nothing
            Dim listCustomer As New List(Of SiPendarDAL.goAML_Ref_Customer)      '1 GCN bisa punya banyak CIF

            Dim listProfileAccount As New List(Of SiPendarBLL.ProfileAccountClass)
            'Dim listAccount As New List(Of SiPendarDAL.goAML_Ref_Account)

            Dim listAddress As New List(Of SiPendarDAL.goAML_Ref_Address)
            Dim listPhone As New List(Of SiPendarDAL.goAML_Ref_Phone)
            Dim listIdentification As New List(Of SiPendarDAL.goAML_Person_Identification)
            Dim objGlobalParameter As New SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER

            Using objDb As New SiPendarDAL.SiPendarEntities
                'Get Global Parameter
                objGlobalParameter = objDb.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault

                'Get Data Customer
                objCustomer = objDb.goAML_Ref_Customer.Where(Function(x) x.GCN = strGCN).FirstOrDefault
                listCustomer = objDb.goAML_Ref_Customer.Where(Function(x) x.GCN = strGCN).ToList

                'Get Account, Address, Phone, Identification
                If listCustomer IsNot Nothing AndAlso listCustomer.Count > 0 Then
                    For Each itemCustomer In listCustomer
                        Dim tempListAccount = objDb.goAML_Ref_Account.Where(Function(x) x.client_number = itemCustomer.CIF).ToList
                        Dim tempListAddress = objDb.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = itemCustomer.PK_Customer_ID).ToList
                        Dim tempListPhone = objDb.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = itemCustomer.PK_Customer_ID).ToList
                        Dim tempListIdentification = objDb.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = itemCustomer.PK_Customer_ID).ToList

                        'List Account
                        If tempListAccount IsNot Nothing AndAlso tempListAccount.Count > 0 Then
                            For Each itemAccount In tempListAccount
                                Dim tempprofileaccount As New ProfileAccountClass
                                tempprofileaccount.ObjAccount = itemAccount
                                If itemAccount.Account_No IsNot Nothing Then
                                    tempprofileaccount.ListATMAccount = objDb.AML_ACCOUNT_ATM.Where(Function(x) x.Account_No = itemAccount.Account_No).ToList
                                End If
                                listProfileAccount.Add(tempprofileaccount)
                            Next
                        End If
                        'If tempListAccount IsNot Nothing AndAlso tempListAccount.Count > 0 Then
                        '    For Each itemAccount In tempListAccount
                        '        listAccount.Add(itemAccount)
                        '    Next
                        'End If

                        'List Address
                        If tempListAddress IsNot Nothing AndAlso tempListAddress.Count > 0 Then
                            For Each itemaddress In tempListAddress
                                listAddress.Add(itemaddress)
                            Next
                        End If

                        'List Phone
                        If tempListPhone IsNot Nothing AndAlso tempListPhone.Count > 0 Then
                            For Each itemphone In tempListPhone
                                listPhone.Add(itemphone)
                            Next
                        End If

                        'List identification
                        If itemCustomer.FK_Customer_Type_ID = 1 Then    'Hanya perorangan yg punya Identification
                            If tempListIdentification IsNot Nothing AndAlso tempListIdentification.Count > 0 Then
                                For Each itemidentification In tempListIdentification
                                    listIdentification.Add(itemidentification)
                                Next
                            End If

                            '20210826
                            If Not String.IsNullOrWhiteSpace(itemCustomer.INDV_SSN) Then
                                Dim tempident As New goAML_Person_Identification
                                If itemCustomer.INDV_SSN IsNot Nothing Then
                                    tempident.Number = itemCustomer.INDV_SSN
                                Else
                                    tempident.Number = ""
                                End If
                                tempident.Type = "KTP"
                                If itemCustomer.INDV_Nationality1 IsNot Nothing Then
                                    tempident.Issued_Country = itemCustomer.INDV_Nationality1
                                Else
                                    tempident.Issued_Country = ""
                                End If
                                Dim objcheck As goAML_Person_Identification = listIdentification.Where(Function(x) x.Number = tempident.Number And x.Type = tempident.Type And x.Issued_Country = tempident.Issued_Country).FirstOrDefault
                                If objcheck Is Nothing Then
                                    listIdentification.Add(tempident)
                                End If
                            End If

                            If Not String.IsNullOrWhiteSpace(itemCustomer.INDV_Passport_Number) Then
                                Dim tempident As New goAML_Person_Identification
                                If itemCustomer.INDV_Passport_Number IsNot Nothing Then
                                    tempident.Number = itemCustomer.INDV_Passport_Number
                                Else
                                    tempident.Number = ""
                                End If
                                tempident.Type = "PAS"
                                If itemCustomer.INDV_Passport_Country IsNot Nothing Then
                                    tempident.Issued_Country = itemCustomer.INDV_Passport_Country
                                Else
                                    tempident.Issued_Country = ""
                                End If
                                Dim objcheck As goAML_Person_Identification = listIdentification.Where(Function(x) x.Number = tempident.Number And x.Type = tempident.Type And x.Issued_Country = tempident.Issued_Country).FirstOrDefault
                                If objcheck Is Nothing Then
                                    listIdentification.Add(tempident)
                                End If
                            End If
                        End If
                    Next
                End If
            End Using

            'Load Customer
            If Not objCustomer Is Nothing Then
                Dim objAddress = listAddress.FirstOrDefault

                With objCustomer
                    cmb_CUSTOMER_TYPE.SetTextValue("")
                    If Not IsNothing(.FK_Customer_Type_ID) Then
                        Dim objCustomerType = SiPendarProfile_BLL.GetCustomerTypeByID(.FK_Customer_Type_ID)
                        If objCustomerType IsNot Nothing Then
                            cmb_CUSTOMER_TYPE.SetTextWithTextValue(objCustomerType.PK_Customer_Type_ID, objCustomerType.Description)
                        End If
                    End If

                    fs_Individual.Hidden = True
                    fs_Corporate.Hidden = True

                    If .GCN IsNot Nothing Then
                        txt_GCN.Value = objCustomer.GCN
                    End If

                    Dim customerstatusclosed As String = ""
                    Dim drCustomerClosed As DataRow = GetCustomerClosedStatus(txt_GCN.Value)
                    If drCustomerClosed IsNot Nothing Then
                        If Not IsDBNull(drCustomerClosed("IsClosed")) Then
                            If drCustomerClosed("IsClosed") = 0 Then
                                customerstatusclosed = "Active"
                            Else
                                If Not IsDBNull(drCustomerClosed("ClosedDate")) Then
                                    customerstatusclosed = "Closed ( " & CDate(drCustomerClosed("ClosedDate")).ToString("dd-MMM-yyyy") & " )"
                                Else
                                    customerstatusclosed = "Closed"
                                End If
                            End If
                        End If
                    Else
                        customerstatusclosed = "Customer GCN Not Found"
                    End If

                    If objCustomer.FK_Customer_Type_ID = 1 Then     'Individual
                        Clean_Panel_Information_Individual()
                        fs_Individual.Hidden = False
                        gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = False
                        gp_SIPENDAR_PROFILE_PHONE.Hidden = False
                        If .INDV_Last_Name IsNot Nothing Then
                            txt_INDV_NAME.Value = .INDV_Last_Name
                        End If
                        If objAddress IsNot Nothing AndAlso objAddress.Address IsNot Nothing Then
                            txt_INDV_ADDRESS.Value = objAddress.Address
                        End If
                        If .INDV_BirthDate.HasValue Then
                            txt_INDV_DOB.Value = .INDV_BirthDate
                        End If
                        If .INDV_Birth_Place IsNot Nothing Then
                            txt_INDV_PLACEOFBIRTH.Value = .INDV_Birth_Place
                        End If

                        cmb_INDV_NATIONALITY.SetTextValue("")
                        If Not .INDV_Nationality1 Is Nothing Then
                            Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(.INDV_Nationality1)
                            If objCountry IsNot Nothing Then
                                cmb_INDV_NATIONALITY.SetTextWithTextValue(objCountry.Kode, objCountry.Keterangan)
                            End If
                        End If
                        fs_Individual_IsHaveActiveAccount.Value = customerstatusclosed
                        If customerstatusclosed = "Customer GCN Not Found" Then
                            fs_Corporate_IsHaveActiveAccount.Value = customerstatusclosed
                        Else
                            fs_Corporate_IsHaveActiveAccount.Value = "This Customer Registered as Individual"
                        End If

                        ''20210826
                        'If Not String.IsNullOrWhiteSpace(.INDV_SSN) Then
                        '    Dim tempident As New goAML_Person_Identification
                        '    tempident.Number = .INDV_SSN
                        '    tempident.Type = "KTP"
                        '    tempident.Issued_Country = .INDV_Nationality1
                        '    listIdentification.Add(tempident)
                        'End If

                        'If Not String.IsNullOrWhiteSpace(.INDV_Passport_Number) Then
                        '    Dim tempident As New goAML_Person_Identification
                        '    tempident.Number = .INDV_Passport_Number
                        '    tempident.Type = "PAS"
                        '    tempident.Issued_Country = .INDV_Passport_Country
                        '    listIdentification.Add(tempident)
                        'End If
                        ''20210826

                    Else    'Korporasi
                        Clean_Panel_Information_Korporasi()
                        fs_Corporate.Hidden = False
                        gp_SIPENDAR_PROFILE_IDENTIFICATION.Hidden = True
                        gp_SIPENDAR_PROFILE_PHONE.Hidden = True
                        If .Corp_Name IsNot Nothing Then
                            txt_CORP_NAME.Value = .Corp_Name
                        End If
                        If .Corp_Tax_Number IsNot Nothing Then
                            txt_CORP_NPWP.Value = .Corp_Tax_Number
                        End If
                        If .Corp_Incorporation_Number IsNot Nothing Then
                            txt_CORP_NO_IZIN_USAHA.Value = .Corp_Incorporation_Number
                        End If

                        cmb_CORP_COUNTRY.SetTextValue("")
                        If Not .Corp_Incorporation_Country_Code Is Nothing Then
                            Dim objCountry = SiPendarProfile_BLL.GetNegaraByCode(.Corp_Incorporation_Country_Code)
                            If objCountry IsNot Nothing Then
                                cmb_CORP_COUNTRY.SetTextWithTextValue(objCountry.Kode, objCountry.Keterangan)
                            End If
                        End If
                        fs_Corporate_IsHaveActiveAccount.Value = customerstatusclosed
                        If customerstatusclosed = "Customer GCN Not Found" Then
                            fs_Individual_IsHaveActiveAccount.Value = customerstatusclosed
                        Else
                            fs_Individual_IsHaveActiveAccount.Value = "This Customer Registered as Individual"
                        End If
                    End If
                End With
            End If

            With objSIPENDAR_PROFILE_CLASS
                'Kosongkan dulu object Listnya
                .objList_SIPENDAR_PROFILE_ACCOUNT = New List(Of SIPENDAR_PROFILE_ACCOUNT)
                .objList_SIPENDAR_PROFILE_ACCOUNT_ATM = New List(Of SIPENDAR_PROFILE_ACCOUNT_ATM)
                .objList_SIPENDAR_PROFILE_ADDRESS = New List(Of SIPENDAR_PROFILE_ADDRESS)
                .objList_SIPENDAR_PROFILE_PHONE = New List(Of SIPENDAR_PROFILE_PHONE)
                .objList_SIPENDAR_PROFILE_IDENTIFICATION = New List(Of SIPENDAR_PROFILE_IDENTIFICATION)

                'SiPendarProfile Account
                If listProfileAccount IsNot Nothing AndAlso listProfileAccount.Count > 0 Then
                    Dim PK_ID As Long = 0
                    Dim PK_ATM_ID As Long = 0
                    For Each itemAccount In listProfileAccount
                        PK_ID = PK_ID - 1

                        Dim objAccount As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT
                        With objAccount
                            .PK_SIPENDAR_PROFILE_ACCOUNT_ID = PK_ID
                            .CIFNO = itemAccount.ObjAccount.client_number
                            .Fk_goAML_Ref_Jenis_Rekening_CODE = itemAccount.ObjAccount.personal_account_type
                            .Fk_goAML_Ref_Status_Rekening_CODE = itemAccount.ObjAccount.status_code

                            If objGlobalParameter IsNot Nothing Then
                                .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
                            End If

                            .NOREKENING = itemAccount.ObjAccount.Account_No
                        End With

                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Add(objAccount)

                        For Each itemATM In itemAccount.ListATMAccount
                            PK_ATM_ID = PK_ATM_ID - 1
                            Dim objATM As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT_ATM
                            With objATM
                                .PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = PK_ATM_ID
                                .FK_SIPENDAR_PROFILE_ACCOUNT_ID = objAccount.PK_SIPENDAR_PROFILE_ACCOUNT_ID
                                .NOATM = itemATM.ATM_NO
                            End With
                            objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Add(objATM)
                        Next
                    Next
                End If

                'SiPendarProfile Address
                If listAddress IsNot Nothing AndAlso listAddress.Count > 0 Then
                    Dim PK_ID As Long = 0
                    For Each itemAddress In listAddress
                        PK_ID = PK_ID - 1

                        Dim objAddress As New SiPendarDAL.SIPENDAR_PROFILE_ADDRESS
                        With objAddress
                            .PK_SIPENDAR_PROFILE_ADDRESS_ID = PK_ID
                            .FK_goAML_Ref_Kategori_Kontak_CODE = itemAddress.Address_Type
                            .ADDRESS = itemAddress.Address
                            .CITY = itemAddress.City
                            .FK_goAML_Ref_Nama_Negara_CODE = itemAddress.Country_Code
                            .ZIP = itemAddress.Zip

                            'tambahan 5 Jul 2021
                            .TOWN = itemAddress.Town
                            .STATE = itemAddress.State
                            .COMMENTS = itemAddress.Comments
                        End With

                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Add(objAddress)
                    Next
                End If

                'SiPendarProfile PHONE
                If listPhone IsNot Nothing AndAlso listPhone.Count > 0 Then
                    Dim PK_ID As Long = 0
                    For Each itemPHONE In listPhone
                        PK_ID = PK_ID - 1

                        Dim objPHONE As New SiPendarDAL.SIPENDAR_PROFILE_PHONE
                        With objPHONE
                            .PK_SIPENDAR_PROFILE_PHONE_ID = PK_ID
                            .FK_goAML_Ref_Kategori_Kontak_CODE = itemPHONE.Tph_Contact_Type
                            .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = itemPHONE.Tph_Communication_Type
                            .COUNTRY_PREFIX = itemPHONE.tph_country_prefix
                            .PHONE_NUMBER = itemPHONE.tph_number

                            'tambahan 5 Jul 2021
                            .EXTENSION_NUMBER = itemPHONE.tph_extension
                            .COMMENTS = itemPHONE.comments
                        End With

                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Add(objPHONE)
                    Next
                End If

                'SiPendarProfile IDENTIFICATION
                If listIdentification IsNot Nothing AndAlso listIdentification.Count > 0 Then
                    Dim PK_ID As Long = 0
                    For Each itemIDENTIFICATION In listIdentification
                        Dim objCek = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.IDENTITY_NUMBER = itemIDENTIFICATION.Number)
                        If objCek IsNot Nothing Then
                            Continue For
                        End If
                        PK_ID = PK_ID - 1

                        Dim objIDENTIFICATION As New SiPendarDAL.SIPENDAR_PROFILE_IDENTIFICATION
                        With objIDENTIFICATION
                            .PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = PK_ID
                            .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = itemIDENTIFICATION.Type
                            .IDENTITY_NUMBER = itemIDENTIFICATION.Number
                            .ISSUE_DATE = itemIDENTIFICATION.Issue_Date
                            .EXPIRED_DATE = itemIDENTIFICATION.Expiry_Date
                            .ISSUED_BY = itemIDENTIFICATION.Issued_By
                            .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = itemIDENTIFICATION.Issued_Country

                            'tambahan 5 Jul 2021
                            .Comments = itemIDENTIFICATION.Identification_Comment
                        End With

                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Add(objIDENTIFICATION)
                    Next
                End If
            End With

            'Bind data to GridPanel
            Bind_SIPENDAR_PROFILE_ACCOUNT()
            'Bind_SIPENDAR_PROFILE_ACCOUNT_ATM()
            Bind_SIPENDAR_PROFILE_AddRESS()
            Bind_SIPENDAR_PROFILE_PHONE()
            Bind_SIPENDAR_PROFILE_IDENTIFICATION()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "7-Jul-2021 Adi"
    'Penambahan fitur Generate Transaction From - To
    Protected Sub chk_IsGenerateTransaction_Change(sender As Object, e As EventArgs)
        Try
            If chk_IsGenerateTransaction.Checked = True Then
                txt_Transaction_DateFrom.Hidden = False
                txt_Transaction_DateTo.Hidden = False
                PanelReportIndicator.Hidden = False ''14-Sep-2021 Felix
            Else
                txt_Transaction_DateFrom.Hidden = True
                txt_Transaction_DateTo.Hidden = True
                PanelReportIndicator.Hidden = True ''14-Sep-2021 Felix
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region


#Region "14-Sep-2021 Felix Untuk Report Indicator"
    Protected Sub btn_addIndicator_click(sender As Object, e As DirectEventArgs)
        Try
            sar_reportIndicator.Clear()
            WindowReportIndicator.Hidden = False
            sar_reportIndicator.Selectable = True
            BtnsaveReportIndicator.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandReportIndicator(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = ID).FirstOrDefault)
                bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                editIndicator(ID, "Edit")
                IDIndicator = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editIndicator(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveReportIndicator_Click(sender As Object, e As DirectEventArgs)
        Try
            If String.IsNullOrEmpty(sar_reportIndicator.SelectedItem.Value) Then
                Throw New ApplicationException(sar_reportIndicator.FieldLabel + " Tidak boleh kosong")
            End If

            For Each item In ListIndicator
                If item.FK_Indicator = sar_reportIndicator.SelectedItem.Value Then
                    Throw New ApplicationException(sar_reportIndicator.SelectedItem.Value + " Sudah Ada")
                End If
            Next

            Dim reportIndicator As New SiPendarDAL.SIPENDAR_Report_Indicator
            If IDIndicator IsNot Nothing Or IDIndicator <> "" Then
                ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = IDIndicator).FirstOrDefault)
            End If
            If ListIndicator.Count = 0 Then
                reportIndicator.PK_Report_Indicator = -1
            ElseIf ListIndicator.Count >= 1 Then
                reportIndicator.PK_Report_Indicator = ListIndicator.Min(Function(x) x.PK_Report_Indicator) - 1
            End If

            reportIndicator.FK_Indicator = sar_reportIndicator.SelectedItem.Value
            ListIndicator.Add(reportIndicator)
            bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            WindowReportIndicator.Hidden = True
            IDIndicator = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelreportIndicator_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowReportIndicator.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub bindReportIndicator(store As Ext.Net.Store, listAddresss As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        '  objtable.Columns.Add(New DataColumn("Kode_Indicator", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                '     item("Kode_Indicator") = item("FK_Indicator")
                item("Keterangan") = getReportIndicatorByKode(item("FK_Indicator").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Shared Function getReportIndicatorByKode(Kode As String) As String
        Dim strKategori As String = ""
        Using objdb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Dim kategori As SiPendarDAL.goAML_Ref_Indikator_Laporan = objdb.goAML_Ref_Indikator_Laporan.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If kategori IsNot Nothing Then
                strKategori = kategori.Keterangan
            End If
        End Using
        Return strKategori
    End Function

    Sub editIndicator(id As String, command As String)
        Try
            sar_reportIndicator.Clear()

            WindowReportIndicator.Hidden = False
            Dim reportIndicator As SiPendarDAL.SIPENDAR_Report_Indicator
            reportIndicator = ListIndicator.Where(Function(x) x.PK_Report_Indicator = id).FirstOrDefault


            sar_reportIndicator.SetValue(reportIndicator.FK_Indicator)
            If command = "Detail" Then
                sar_reportIndicator.Selectable = False
                BtnsaveReportIndicator.Hidden = True
            Else
                sar_reportIndicator.Selectable = True
                BtnsaveReportIndicator.Hidden = False
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ReportIndicator_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            'objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goaml_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
#End Region

    Private Sub SIPENDAR_PROFILE_Add_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Insert
    End Sub

    Protected Sub Clean_Panel_Information_Individual()

        txt_INDV_NAME.SetValue("")

        txt_INDV_ADDRESS.SetValue("")

        txt_INDV_DOB.Reset()

        txt_INDV_PLACEOFBIRTH.SetValue("")

        cmb_INDV_NATIONALITY.SetTextValue("")
    End Sub

    Protected Sub Clean_Panel_Information_Korporasi()

        txt_CORP_NAME.SetValue("")

        txt_CORP_NPWP.SetValue("")

        txt_CORP_NO_IZIN_USAHA.SetValue("")

        cmb_CORP_COUNTRY.SetTextValue("")
    End Sub

    Protected Function GetCustomerClosedStatus(strGCN As String) As DataRow
        Try

            Dim param(0) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@GCN"
            param(0).Value = strGCN
            param(0).DbType = SqlDbType.VarChar

            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_GetCustomerClosedStatus_ByGCN", param)

            Return drResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function
End Class