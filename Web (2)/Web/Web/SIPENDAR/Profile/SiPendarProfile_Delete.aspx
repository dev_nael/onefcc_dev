﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="SiPendarProfile_Delete.aspx.vb" Inherits="SiPendarProfile_Delete" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="SiPendar Profile Data">
        <DockedItems>
            <ext:Toolbar ID="ToolbarInput" ClientIDMode="Static" runat="server" EnableOverflow="true" Dock="Top" Layout="FitLayout" MaxHeight="200"  >                
                <Items>
                      <ext:InfoPanel ID="infoValidationResultPanel" ClientIDMode="Static" runat="server" UI="Warning" Layout="AnchorLayout"  Title="Validation Result" AutoScroll="true" IconCls="#Error" MarginSpec="10 0" Border="True" Hidden="true" Collapsible="true"></ext:InfoPanel>
                </Items>
                </ext:Toolbar>                                                     
        </DockedItems>

        <Items>
            <%-- SiPendar Profile General Information --%>
            <ext:Panel runat="server" Title="General Information" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlWATCHLIST" Border="true" BodyStyle="padding:10px">
                <Content>

                    <NDS:NDSDropDownField ID="cmb_SIPENDAR_TYPE_ID" LabelWidth="250" ValueField="PK_SIPENDAR_TYPE_id" DisplayField="SIPENDAR_TYPE_NAME" runat="server" StringField="PK_SIPENDAR_TYPE_id, SIPENDAR_TYPE_NAME" StringTable="SIPENDAR_TYPE" Label="SiPendar Report Type" AnchorHorizontal="80%" AllowBlank="false"/>
                    <NDS:NDSDropDownField ID="cmb_CUSTOMER_TYPE" LabelWidth="250" ValueField="PK_Customer_Type_ID" DisplayField="Description" runat="server" StringField="PK_Customer_Type_ID, Description" StringTable="goAML_Ref_Customer_Type" Label="Customer Type" AnchorHorizontal="80%" AllowBlank="false"/>
                    <NDS:NDSDropDownField ID="cmb_JENIS_WATCHLIST_CODE" LabelWidth="250" ValueField="SIPENDAR_JENIS_WATCHLIST_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_JENIS_WATCHLIST_CODE, KETERANGAN" StringTable="SIPENDAR_JENIS_WATCHLIST" Label="Watchlist Category" AnchorHorizontal="80%" AllowBlank="false"/>
                    <NDS:NDSDropDownField ID="cmb_TINDAK_PIDANA_CODE" LabelWidth="250" ValueField="SIPENDAR_TINDAK_PIDANA_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_TINDAK_PIDANA_CODE, KETERANGAN" StringTable="SIPENDAR_TINDAK_PIDANA" Label="Jenis Tindak Pidana" AnchorHorizontal="80%" AllowBlank="false"/>
                    <NDS:NDSDropDownField ID="cmb_SUMBER_INFORMASI_KHUSUS_CODE" LabelWidth="250" ValueField="SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE, KETERANGAN" StringTable="SIPENDAR_SUMBER_INFORMASI_KHUSUS" Label="Sumber Informasi Khusus" AnchorHorizontal="80%" AllowBlank="false"/>
                    <NDS:NDSDropDownField ID="cmb_ORGANISASI_CODE" LabelWidth="250" ValueField="ORGANISASI_CODE" DisplayField="ORGANISASI_NAME" runat="server" StringField="ORGANISASI_CODE, ORGANISASI_NAME" StringTable="SIPENDAR_ORGANISASI" Label="SiPendar Organization Code" AnchorHorizontal="80%" AllowBlank="false"/>
                    <NDS:NDSDropDownField ID="cmb_SUMBER_TYPE_CODE" LabelWidth="250" ValueField="SUMBER_TYPE_CODE" DisplayField="SUMBER_TYPE_NAME" runat="server" StringField="SUMBER_TYPE_CODE, SUMBER_TYPE_NAME" StringTable="SIPENDAR_SUMBER_TYPE" Label="Sumber Type" AnchorHorizontal="80%" AllowBlank="false" IsHidden="true"/>
                    <ext:NumberField ID="txt_SUMBER_ID" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Sumber ID" AnchorHorizontal="80%" FormatText="#,##0" Hidden="true"></ext:NumberField>
                    <NDS:NDSDropDownField ID="cmb_SUBMISSION_TYPE_CODE" LabelWidth="250" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="vw_SIPENDAR_SUBMISSION_TYPE" Label="Submission Type" AnchorHorizontal="80%" AllowBlank="false"/>
                    <ext:TextField ID="txt_GCN" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="GCN" AnchorHorizontal="80%" MaxLength="50" EnforceMaxLength="true"></ext:TextField>
                    <ext:NumberField ID="txt_Similarity" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Similarity (%)" AnchorHorizontal="80%" FormatText="#,##0.00" Hidden="true"></ext:NumberField>
                    <ext:TextField ID="txt_CustomerType" LabelWidth="250" runat="server" FieldLabel="Customer / Non-Customer" AnchorHorizontal="80%"></ext:TextField>

                    <%-- Individual Information --%>
                    <ext:FieldSet runat="server" ID="fs_Individual" Collapsible="false" Title="Individual Information" Border="true" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="10 0" Padding="10">
                        <Content>
                            <ext:TextField ID="txt_INDV_NAME" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                            <ext:TextField ID="txt_INDV_PLACEOFBIRTH" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                            <ext:DateField ID="txt_INDV_DOB" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="50%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:TextArea ID="txt_INDV_ADDRESS" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Address" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextArea>
                            <NDS:NDSDropDownField ID="cmb_INDV_NATIONALITY" LabelWidth="250" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Nationality" AnchorHorizontal="80%" AllowBlank="false"/>
                            <ext:TextField ID="txt_role_noncustomer" LabelWidth="250" runat="server" FieldLabel="Role (Non-Customer)" AnchorHorizontal="80%" ReadOnly="true" FieldStyle="background-color:#ddd;"></ext:TextField>
                            <ext:FieldSet runat="server" Title ="*This Information From Ref Customer" Border="true" ID="fs_Individual_IsHaveActiveAccount_Head">
                                <Content>
                                    <ext:DisplayField runat="server" ID="fs_Individual_IsHaveActiveAccount" FieldLabel="Customer Account Status" LabelWidth="175"></ext:DisplayField>
                                </Content>
                            </ext:FieldSet>
                             <ext:GridPanel ID="GridPanelLinkCIF" runat="server" EmptyText="No Available Data" Hidden="true" Title="Link To CIF / Account">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store8" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model8">
                                        <Fields>
                                            <ext:ModelField Name="Account_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Account_No" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="CIF_No" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column69" runat="server" DataIndex="Account_No" Text="Account No" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column70" runat="server" DataIndex="CIF_No" Text="CIF No" Flex="1"></ext:Column>
                                 <ext:Column ID="Column71" runat="server" DataIndex="Account_Name" Text="Account Name" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                        </Content>
                    </ext:FieldSet>
                    <%-- End of Individual Information --%>

                    <%-- Corporate Information --%>
                    <ext:FieldSet runat="server" ID="fs_Corporate" Collapsible="false" Title="Corporate Information" Border="true" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="10 0" Padding="10" Hidden="true">
                        <Content>
                            <ext:TextField ID="txt_CORP_NAME" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Corporate Name" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                            <ext:TextField ID="txt_CORP_NPWP" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="NPWP" AnchorHorizontal="80%" MaxLength="15" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:TextField>
                            <ext:TextField ID="txt_CORP_NO_IZIN_USAHA" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Business License No." AnchorHorizontal="80%" MaxLength="50" EnforceMaxLength="true"></ext:TextField>
                            <NDS:NDSDropDownField ID="cmb_CORP_COUNTRY" LabelWidth="250" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Incorporation Country" AnchorHorizontal="80%" AllowBlank="false"/>
                            <ext:FieldSet runat="server" Title ="*This Information From Ref Customer" Border="true" ID="fs_Corporate_IsHaveActiveAccount_Head">
                                <Content>
                                    <ext:DisplayField runat="server" ID="fs_Corporate_IsHaveActiveAccount" FieldLabel="Customer Account Status" LabelWidth="175"></ext:DisplayField>
                                </Content>
                            </ext:FieldSet>
                        </Content>
                    </ext:FieldSet>
                    <%-- End of Corporate Information --%>

                    <%-- 8-Jul-2021 Fitur Generate Transaction --%>
                    <ext:FieldSet runat="server" ID="fs_Generate_Transaction" Border="true" Title="Generate Transaction when Approved (Optional)" Hidden="false" Layout="AnchorLayout" Padding="10">
                        <Content>
                            <ext:Checkbox ID="chk_IsGenerateTransaction" LabelWidth="250" runat="server" FieldLabel="Include Generate Transaction">
                                <DirectEvents>
                                    <Change OnEvent="chk_IsGenerateTransaction_Change"></Change>
                                </DirectEvents>
                            </ext:Checkbox>
                            <ext:DateField ID="txt_Transaction_DateFrom" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Transaction Date From" AnchorHorizontal="50%" Format="dd-MMM-yyyy" Hidden="true"></ext:DateField>
                            <ext:DateField ID="txt_Transaction_DateTo" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Transaction Date To" AnchorHorizontal="50%" Format="dd-MMM-yyyy" Hidden="true"></ext:DateField>
                        </Content>
                    </ext:FieldSet>
                    <%-- End of 8-Jul-2021 Fitur Generate Transaction --%>

                    <ext:TextArea ID="txt_Keterangan" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Keterangan" AnchorHorizontal="80%" MaxLength="4000" EnforceMaxLength="true"></ext:TextArea>

                </Content>
            </ext:Panel>
            <%-- End of SiPendar Profile General Information --%>


            <%-- SiPendar Profile ACCOUNT --%>
            <ext:GridPanel ID="gp_SIPENDAR_PROFILE_ACCOUNT" runat="server" Title="Account" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                <TopBar>
                    <ext:Toolbar ID="toolbar12" runat="server">
                        <Items>
                            <ext:Button ID="btn_SIPENDAR_PROFILE_ACCOUNT_Add" runat="server" Text="Add Account" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btn_SIPENDAR_PROFILE_ACCOUNT_Add_Click">
                                        <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="Store_SIPENDAR_PROFILE_ACCOUNT" runat="server" IsPagingStore="true" PageSize="8">
                        <Model>
                            <ext:Model ID="Model29" runat="server" IDProperty="PK_SIPENDAR_PROFILE_ACCOUNT_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_SIPENDAR_PROFILE_ACCOUNT_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CIFNO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="JENIS_REKENING" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="STATUS_REKENING" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NOREKENING" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                        <ext:Column ID="Column75" runat="server" DataIndex="CIFNO" Text="CIF No." Flex="1"></ext:Column>
                        <ext:Column ID="Column8" runat="server" DataIndex="JENIS_REKENING" Text="Account Type" Flex="1"></ext:Column>
                        <ext:Column ID="Column9" runat="server" DataIndex="STATUS_REKENING" Text="Status" Flex="1"></ext:Column>
                        <ext:Column ID="Column11" runat="server" DataIndex="NOREKENING" Text="Account No." Flex="1"></ext:Column>
                        <ext:CommandColumn ID="cc_SIPENDAR_PROFILE_ACCOUNT" runat="server" Text="Action" MinWidth="200">
                            <Commands>
                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_SIPENDAR_PROFILE_ACCOUNT">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_SIPENDAR_PROFILE_ACCOUNT_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                    </Confirmation>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            <%-- End of SiPendar Profile ACCOUNT --%>

            <%-- SiPendar Profile Address --%>
            <ext:GridPanel ID="gp_SIPENDAR_PROFILE_ADDRESS" runat="server" Title="Address" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                <TopBar>
                    <ext:Toolbar ID="toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="btn_SIPENDAR_PROFILE_ADDRESS_Add" runat="server" Text="Add Address" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btn_SIPENDAR_PROFILE_ADDRESS_Add_Click">
                                        <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="Store1" runat="server" IsPagingStore="true" PageSize="8">
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="PK_SIPENDAR_PROFILE_ADDRESS_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_SIPENDAR_PROFILE_ADDRESS_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="TOWN" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="STATE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ZIP" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                        <ext:Column ID="Column5" runat="server" DataIndex="ADDRESS_TYPE_NAME" Text="Type" Flex="1"></ext:Column>
                        <ext:Column ID="Column1" runat="server" DataIndex="ADDRESS" Text="Address" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                        <ext:Column ID="Column19" runat="server" DataIndex="TOWN" Text="Town" MinWidth="100" Flex="1"></ext:Column>
                        <ext:Column ID="Column2" runat="server" DataIndex="CITY" Text="City" MinWidth="100" Flex="1"></ext:Column>
                        <ext:Column ID="Column3" runat="server" DataIndex="COUNTRY_NAME" Text="Country" Flex="1"></ext:Column>
                        <ext:Column ID="Column20" runat="server" DataIndex="STATE" Text="State" MinWidth="100" Flex="1"></ext:Column>
                        <ext:Column ID="Column4" runat="server" DataIndex="ZIP" Text="Zip" Flex="1"></ext:Column>
                        <ext:Column ID="Column21" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="cc_SIPENDAR_PROFILE_ADDRESS" runat="server" Text="Action" MinWidth="200">
                            <Commands>
                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_SIPENDAR_PROFILE_ADDRESS">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_SIPENDAR_PROFILE_ADDRESS_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                    </Confirmation>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            <%-- End of SiPendar Profile Address --%>

            <%-- SiPendar Profile PHONE --%>
            <ext:GridPanel ID="gp_SIPENDAR_PROFILE_PHONE" runat="server" Title="Phone" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                <TopBar>
                    <ext:Toolbar ID="toolbar3" runat="server">
                        <Items>
                            <ext:Button ID="btn_SIPENDAR_PROFILE_PHONE_Add" runat="server" Text="Add Phone" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btn_SIPENDAR_PROFILE_PHONE_Add_Click">
                                        <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="Store3" runat="server" IsPagingStore="true" PageSize="8">
                        <Model>
                            <ext:Model ID="Model3" runat="server" IDProperty="PK_SIPENDAR_PROFILE_PHONE_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_SIPENDAR_PROFILE_PHONE_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="PHONE_TYPE_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="COMMUNICATION_TYPE_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="COUNTRY_PREFIX" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="PHONE_NUMBER" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="EXTENSION_NUMBER" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                        <ext:Column ID="Column10" runat="server" DataIndex="PHONE_TYPE_NAME" Text="Contact Type" Flex="1"></ext:Column>
                        <ext:Column ID="Column18" runat="server" DataIndex="COMMUNICATION_TYPE_NAME" Text="Communication Type" Flex="1"></ext:Column>
                        <ext:Column ID="Column12" runat="server" DataIndex="COUNTRY_PREFIX" Text="Country Prefix" Flex="1"></ext:Column>
                        <ext:Column ID="Column13" runat="server" DataIndex="PHONE_NUMBER" Text="Phone Number" Flex="1"></ext:Column>
                        <ext:Column ID="Column22" runat="server" DataIndex="EXTENSION_NUMBER" Text="Extension" Flex="1"></ext:Column>
                        <ext:Column ID="Column23" runat="server" DataIndex="COMMENTS" Text="Comments" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="cc_SIPENDAR_PROFILE_PHONE" runat="server" Text="Action" MinWidth="200">
                            <Commands>
                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_SIPENDAR_PROFILE_PHONE">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_SIPENDAR_PROFILE_PHONE_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                    </Confirmation>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            <%-- End of SiPendar Profile PHONE --%>

            <%-- SiPendar Profile IDENTIFICATION --%>
            <ext:GridPanel ID="gp_SIPENDAR_PROFILE_IDENTIFICATION" runat="server" Title="Identification" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                <TopBar>
                    <ext:Toolbar ID="toolbar2" runat="server">
                        <Items>
                            <ext:Button ID="btn_SIPENDAR_PROFILE_IDENTIFICATION_Add" runat="server" Text="Add Identification" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btn_SIPENDAR_PROFILE_IDENTIFICATION_Add_Click">
                                        <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="Store2" runat="server" IsPagingStore="true" PageSize="8">
                        <Model>
                            <ext:Model ID="Model2" runat="server" IDProperty="PK_SIPENDAR_PROFILE_IDENTIFICATION_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_SIPENDAR_PROFILE_IDENTIFICATION_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="IDENTITY_NUMBER" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ISSUE_DATE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="EXPIRED_DATE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ISSUED_BY" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ISSUED_COUNTRY" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                        <ext:Column ID="Column6" runat="server" DataIndex="Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Text="Type" Flex="1"></ext:Column>
                        <ext:Column ID="Column7" runat="server" DataIndex="IDENTITY_NUMBER" Text="Identification No." MinWidth="200" Flex="1"></ext:Column>
                        <ext:DateColumn ID="Column14" runat="server" DataIndex="ISSUE_DATE" Text="Issued" Format="dd-MMM-yyyy" Flex="1"></ext:DateColumn>
                        <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="EXPIRED_DATE" Text="Expired" Format="dd-MMM-yyyy" Flex="1"></ext:DateColumn>
                        <ext:Column ID="Column15" runat="server" DataIndex="ISSUED_BY" Text="Issued By" Flex="1"></ext:Column>
                        <ext:Column ID="Column16" runat="server" DataIndex="ISSUED_COUNTRY" Text="Issued Country" Flex="1"></ext:Column>
                        <ext:Column ID="Column24" runat="server" DataIndex="COMMENTS" Text="Comments" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="cc_SIPENDAR_PROFILE_IDENTIFICATION" runat="server" Text="Action" MinWidth="200">
                            <Commands>
                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_SIPENDAR_PROFILE_IDENTIFICATION">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                    </Confirmation>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            <%-- End of SiPendar Profile IDENTIFICATION --%>

        </Items>

        <Buttons>
            <ext:Button ID="btn_SIPENDAR_PROFILE_Submit" runat="server" Icon="ApplicationDelete" Text="Delete">
                <DirectEvents>
                    <Click OnEvent="btn_SIPENDAR_PROFILE_Submit_Click">
                        <EventMask ShowMask="true" Msg="Saving Change..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_SIPENDAR_PROFILE_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_SIPENDAR_PROFILE_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <%-- Pop Up Windows --%>

    <%-- Pop Up Window ACCOUNT --%>
    <ext:Window ID="Window_SIPENDAR_PROFILE_ACCOUNT" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="Window_Panel_SIPENDAR_PROFILE_ACCOUNT" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Content>
                    <ext:TextField ID="txt_SIPENDAR_PROFILE_ACCOUNT_CIFNO" runat="server" FieldLabel="CIF" AllowBlank="false" AnchorHorizontal="100%" MaxLength="30" EnforceMaxLength="true"></ext:TextField>
                    <NDS:NDSDropDownField ID="cmb_SIPENDAR_PROFILE_ACCOUNT_JENIS_REKENING" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Jenis_Rekening" Label="Account Type" AnchorHorizontal="80%" AllowBlank="false" OnOnValueChanged="cmb_CUSTOMER_TYPE_Change"/>
                    <NDS:NDSDropDownField ID="cmb_SIPENDAR_PROFILE_ACCOUNT_STATUS_REKENING" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Status_Rekening" Label="Account Status" AnchorHorizontal="80%" AllowBlank="false"/>
                    <ext:TextField ID="txt_SIPENDAR_PROFILE_ACCOUNT_NOREKENING" runat="server" FieldLabel="Account No." AllowBlank="false" AnchorHorizontal="100%" MaxLength="50" EnforceMaxLength="true"></ext:TextField>

                    <%-- SiPendar Profile ACCOUNT_ATM --%>
                    <ext:GridPanel ID="gp_SIPENDAR_PROFILE_ACCOUNT_ATM" runat="server" Title="Account ATM" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                        <TopBar>
                            <ext:Toolbar ID="toolbar4" runat="server">
                                <Items>
                                    <ext:Button ID="btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Add" runat="server" Text="Add ATM" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Add_Click">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store4" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model4" runat="server" IDProperty="PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NOATM" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                                <ext:Column ID="Column17" runat="server" DataIndex="NOATM" Text="ATM No." Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_SIPENDAR_PROFILE_ACCOUNT_ATM" runat="server" Text="Action" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_SIPENDAR_PROFILE_ACCOUNT_ATM">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of SiPendar Profile ACCOUNT_ATM --%>
                </Content>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_SIPENDAR_PROFILE_ACCOUNT_Save" runat="server" Icon="Disk" Text="Save" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btn_SIPENDAR_PROFILE_ACCOUNT_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_SIPENDAR_PROFILE_ACCOUNT_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_SIPENDAR_PROFILE_ACCOUNT_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.97});" />

            <Resize Handler="#{Window_SIPENDAR_PROFILE_ACCOUNT}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window ACCOUNT --%>

    <%-- Pop Up Window Address --%>
    <ext:Window ID="Window_SIPENDAR_PROFILE_ADDRESS" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="Window_Panel_SIPENDAR_PROFILE_ADDRESS" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Content>
                    <NDS:NDSDropDownField ID="cmb_SIPENDAR_PROFILE_ADDRESS_TYPE_CODE" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Kategori_Kontak" Label="Address Type" AnchorHorizontal="100%" AllowBlank="false"/>
                    <ext:TextField ID="txt_SIPENDAR_PROFILE_ADDRESS_ADDRESS" runat="server" FieldLabel="Address" AllowBlank="false" AnchorHorizontal="100%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="txt_SIPENDAR_PROFILE_ADDRESS_TOWN" runat="server" FieldLabel="Town" AllowBlank="true" AnchorHorizontal="100%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="txt_SIPENDAR_PROFILE_ADDRESS_CITY" runat="server" FieldLabel="City" AllowBlank="false" AnchorHorizontal="100%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                    <NDS:NDSDropDownField ID="cmb_SIPENDAR_PROFILE_ADDRESS_COUNTRY_CODE" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Country Code" AnchorHorizontal="100%" AllowBlank="false"/>
                    <ext:TextField ID="txt_SIPENDAR_PROFILE_ADDRESS_STATE" runat="server" FieldLabel="State" AllowBlank="true" AnchorHorizontal="100%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="txt_SIPENDAR_PROFILE_ADDRESS_ZIP" runat="server" FieldLabel="Zip" AllowBlank="true" AnchorHorizontal="50%" MaxLength="10" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:TextField>
                    <ext:TextArea ID="txt_SIPENDAR_PROFILE_ADDRESS_COMMENTS" runat="server" FieldLabel="Comments" AllowBlank="true" AnchorHorizontal="100%" MaxLength="4000" EnforceMaxLength="true"></ext:TextArea>
                </Content>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_SIPENDAR_PROFILE_ADDRESS_Save" runat="server" Icon="Disk" Text="Save" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btn_SIPENDAR_PROFILE_ADDRESS_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_SIPENDAR_PROFILE_ADDRESS_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_SIPENDAR_PROFILE_ADDRESS_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.7});" />

            <Resize Handler="#{Window_SIPENDAR_PROFILE_ADDRESS}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Address --%>

    <%-- Pop Up Window PHONE --%>
    <ext:Window ID="Window_SIPENDAR_PROFILE_PHONE" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="FormPanel2" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Content>
                    <NDS:NDSDropDownField ID="cmb_SIPENDAR_PROFILE_PHONE_TYPE_CODE" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Kategori_Kontak" Label="Phone Type" AnchorHorizontal="100%" AllowBlank="false"/>
                    <NDS:NDSDropDownField ID="cmb_SIPENDAR_PROFILE_PHONE_COMMUNICATION_TYPE_CODE" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Jenis_Alat_Komunikasi" Label="Communication Type" AnchorHorizontal="100%" AllowBlank="false"/>
                    <ext:TextField ID="txt_SIPENDAR_PROFILE_PHONE_COUNTRY_PREFIX" runat="server" FieldLabel="Country Prefix" AllowBlank="true" AnchorHorizontal="100%" MaxLength="4" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:TextField>
                    <ext:TextField ID="txt_SIPENDAR_PROFILE_PHONE_NUMBER" runat="server" FieldLabel="Phone Number" AllowBlank="false" AnchorHorizontal="50%" MaxLength="50" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:TextField>
                    <ext:TextField ID="txt_SIPENDAR_PROFILE_PHONE_EXTENSION_NUMBER" runat="server" FieldLabel="Extension Number" AllowBlank="true" AnchorHorizontal="50%" MaxLength="10" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:TextField>
                    <ext:TextArea ID="txt_SIPENDAR_PROFILE_PHONE_COMMENTS" runat="server" FieldLabel="Comments" AllowBlank="true" AnchorHorizontal="100%" MaxLength="4000" EnforceMaxLength="true"></ext:TextArea>
                </Content>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_SIPENDAR_PROFILE_PHONE_Save" runat="server" Icon="Disk" Text="Save" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btn_SIPENDAR_PROFILE_PHONE_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_SIPENDAR_PROFILE_PHONE_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_SIPENDAR_PROFILE_PHONE_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.7});" />

            <Resize Handler="#{Window_SIPENDAR_PROFILE_PHONE}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window PHONE --%>


    <%-- Pop Up Window IDENTIFICATION --%>
    <ext:Window ID="Window_SIPENDAR_PROFILE_IDENTIFICATION" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="FormPanel1" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Content>
                    <NDS:NDSDropDownField ID="cmb_SIPENDAR_PROFILE_IDENTIFICATION_TYPE_CODE" ValueField="Kode" LabelWidth="250" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Jenis_Dokumen_Identitas" Label="Identification Type" AnchorHorizontal="100%" AllowBlank="false"/>
                    <ext:TextField ID="txt_SIPENDAR_PROFILE_IDENTIFICATION_IDENTITY_NUMBER" runat="server" LabelWidth="250" FieldLabel="Identification Number" AllowBlank="false" AnchorHorizontal="100%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                    <ext:DateField ID="txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_DATE" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Issued Date" AnchorHorizontal="50%" Format="dd-MMM-yyyy"></ext:DateField>
                    <ext:DateField ID="txt_SIPENDAR_PROFILE_IDENTIFICATION_EXPIRED_DATE" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Expired Date" AnchorHorizontal="50%" Format="dd-MMM-yyyy"></ext:DateField>
                    <ext:TextField ID="txt_SIPENDAR_PROFILE_IDENTIFICATION_ISSUED_BY" runat="server" LabelWidth="250" FieldLabel="Issued By" AllowBlank="true" AnchorHorizontal="100%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
                    <NDS:NDSDropDownField ID="cmb_SIPENDAR_PROFILE_IDENTIFICATION_ISSUE_COUNTRY" ValueField="Kode" LabelWidth="250" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Issued Country" AnchorHorizontal="100%" AllowBlank="false"/>
                    <ext:TextArea ID="txt_SIPENDAR_PROFILE_IDENTIFICATION_COMMENTS" runat="server" LabelWidth="250" FieldLabel="Comments" AllowBlank="true" AnchorHorizontal="100%" MaxLength="4000" EnforceMaxLength="true"></ext:TextArea>
                </Content>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_SIPENDAR_PROFILE_IDENTIFICATION_Save" runat="server" Icon="Disk" Text="Save" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btn_SIPENDAR_PROFILE_IDENTIFICATION_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_SIPENDAR_PROFILE_IDENTIFICATION_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_SIPENDAR_PROFILE_IDENTIFICATION_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.7});" />

            <Resize Handler="#{Window_SIPENDAR_PROFILE_IDENTIFICATION}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window IDENTIFICATION --%>


    <%-- Pop Up Window ACCOUNT_ATM --%>
    <ext:Window ID="Window_SIPENDAR_PROFILE_ACCOUNT_ATM" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="400" Height="300" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="FormPanel3" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Content>
                    <ext:TextField ID="txt_SIPENDAR_PROFILE_ACCOUNT_ATM_NOATM" runat="server" FieldLabel="ATM No." AllowBlank="false" AnchorHorizontal="100%" MaxLength="50" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:TextField>
                </Content>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Save" runat="server" Icon="Disk" Text="Save" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_SIPENDAR_PROFILE_ACCOUNT_ATM_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.6, height: size.height * 0.5});" />

            <Resize Handler="#{Window_SIPENDAR_PROFILE_ACCOUNT_ATM}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window ACCOUNT_ATM --%>

    <%-- End of Pop Up Windows --%>

</asp:Content>

