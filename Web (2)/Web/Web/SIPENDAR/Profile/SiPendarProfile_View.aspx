﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.master" AutoEventWireup="false" CodeFile="SiPendarProfile_View.aspx.vb" Inherits="SiPendarProfile_View" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };

        var prepareCommandCollection = function (grid, toolbar, rowIndex, record) {
            var GenerateButton = toolbar.items.get(3);
            var EditButton = toolbar.items.get(1);
            var wf = record.data.Status
            var param = record.data.ParameterValue
            if (wf == 8 && param == 1 ) {
                EditButton.setDisabled(false)
            } else if (wf == 8 && param == 0) {
                EditButton.setDisabled(true)
            }

            if (GenerateButton != undefined) {
                if (wf == 1 || wf == 8) {
                    GenerateButton.setDisabled(false)
                } else {
                    GenerateButton.setDisabled(true)
                }
            }

            var RedirectButton = toolbar.items.get(4);
            var wf2 = record.data.FK_Report_ID
            var ProfileTypeID = record.data.ProfileTypeID /*Added on 18 Aug 2021*/
            if (RedirectButton != undefined) {
                if (wf2 !== null) {
                    RedirectButton.setDisabled(false)
                } else {
                    RedirectButton.setDisabled(true)
                }
                /*Added on 18 Aug 2021*/
                if (ProfileTypeID == 1) {
                    RedirectButton.setDisabled(true)
                }
                /*End of 18 Aug 2021*/
            }
        }

        var prepareCommandCollection_Checker = function (grid, toolbar, rowIndex, record) {
            var GenerateButton = toolbar.items.get(1);
            var wf = record.data.Status
            if (GenerateButton != undefined) {
                if (wf == 1 || wf == 8) {
                    GenerateButton.setDisabled(false)
                } else {
                    GenerateButton.setDisabled(true)
                }
            }

            var RedirectButton = toolbar.items.get(2);
            var wf2 = record.data.FK_Report_ID
            if (RedirectButton != undefined) {
                if (wf2 !== null) {
                    RedirectButton.setDisabled(false)
                } else {
                    RedirectButton.setDisabled(true)
                }
            }
        }

        
        var prepareCommandCollection_0 = function (grid, toolbar, rowIndex, record) {
            var GenerateButton = toolbar.items.get(0);
            var wf = record.data.Status
            if (GenerateButton != undefined) {
                if (wf == 1 || wf == 8) {
                    GenerateButton.setDisabled(false)
                } else {
                    GenerateButton.setDisabled(true)
                }
            }

            var RedirectButton = toolbar.items.get(1);
            var wf2 = record.data.FK_Report_ID
            if (RedirectButton != undefined) {
                if (wf2 !== null) {
                    RedirectButton.setDisabled(false)
                } else {
                    RedirectButton.setDisabled(true)
                }
            }
        }
        
        var prepareCommandCollection_1 = function (grid, toolbar, rowIndex, record) {
            var GenerateButton = toolbar.items.get(1);
            var wf = record.data.Status
            if (GenerateButton != undefined) {
                if (wf == 1 || wf == 8) {
                    GenerateButton.setDisabled(false)
                } else {
                    GenerateButton.setDisabled(true)
                }
            }

            var RedirectButton = toolbar.items.get(2);
            var wf2 = record.data.FK_Report_ID
            if (RedirectButton != undefined) {
                if (wf2 !== null) {
                    RedirectButton.setDisabled(false)
                } else {
                    RedirectButton.setDisabled(true)
                }
            }
        }
        
        var prepareCommandCollection_2 = function (grid, toolbar, rowIndex, record) {
            var GenerateButton = toolbar.items.get(2);
            var wf = record.data.Status
            if (GenerateButton != undefined) {
                if (wf == 1 || wf == 8) {
                    GenerateButton.setDisabled(false)
                } else {
                    GenerateButton.setDisabled(true)
                }
            }

            var RedirectButton = toolbar.items.get(3);
            var wf2 = record.data.FK_Report_ID
            if (RedirectButton != undefined) {
                if (wf2 !== null) {
                    RedirectButton.setDisabled(false)
                } else {
                    RedirectButton.setDisabled(true)
                }
            }
        }
        
        var prepareCommandCollection_3 = function (grid, toolbar, rowIndex, record) {
            var GenerateButton = toolbar.items.get(3);
            var wf = record.data.Status
            if (GenerateButton != undefined) {
                if (wf == 1 || wf == 8) {
                    GenerateButton.setDisabled(false)
                } else {
                    GenerateButton.setDisabled(true)
                }
            }

            var RedirectButton = toolbar.items.get(4);
            var wf2 = record.data.FK_Report_ID
            if (RedirectButton != undefined) {
                if (wf2 !== null) {
                    RedirectButton.setDisabled(false)
                } else {
                    RedirectButton.setDisabled(true)
                }
            }
        }
        
        var prepareCommandCollection_4 = function (grid, toolbar, rowIndex, record) {
            var GenerateButton = toolbar.items.get(1);
            var wf = record.data.Status
            if (GenerateButton != undefined) {
                if (wf == 1 || wf == 8) {
                    GenerateButton.setDisabled(false)
                } else {
                    GenerateButton.setDisabled(true)
                }
            }

            var RedirectButton = toolbar.items.get(2);
            var wf2 = record.data.FK_Report_ID
            if (RedirectButton != undefined) {
                if (wf2 !== null) {
                    RedirectButton.setDisabled(false)
                } else {
                    RedirectButton.setDisabled(true)
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <ext:GridPanel ID="GridpanelView" ClientIDMode="Static" runat="server" Title="Title" Width="2000">
        <View>
            <ext:GridView runat="server" EnableTextSelection="true" />
        </View>
        <Store>
            <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadData">

                <Sorters>
                    <%--<ext:DataSorter Property="" Direction="ASC" />--%>
                </Sorters>
                <Proxy>
                    <ext:PageProxy />
                </Proxy>
            </ext:Store>
        </Store>
        <SelectionModel>
            <ext:CheckboxSelectionModel runat="server" Mode="Multi" CheckOnly="true">
            </ext:CheckboxSelectionModel>
        </SelectionModel>
        <Plugins>
            <%--<ext:GridFilters ID="GridFilters1" runat="server" />--%>
            <ext:FilterHeader ID="grdiheaderfilter" runat="server" Remote="true"></ext:FilterHeader>

        </Plugins>
        <BottomBar>

            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />

        </BottomBar>

        <%--Begin Update Penambahan Advance Filter--%>
        <DockedItems>
            <%--End Update Penambahan Advance Filter--%>
            <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                <Items>

                    <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export :">
                        <Items>
                            <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                            <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                        </Items>
                    </ext:ComboBox>
                    <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" AutoPostBack="true" OnClick="ExportExcel" ClientIDMode="Static" />
                    <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page" AutoPostBack="true" OnClick="ExportAllExcel" />
                    <%--<ext:Button ID="Button1" runat="server" Text="Print Current Page" Icon="Printer" Handler="this.up('grid').print();" />--%>
                    <ext:Button ID="BtnAdd" runat="server" Text="Add New Profile" Icon="Add" Handler="NawadataDirect.BtnAdd_Click()"  />
                    <%--Begin Update Penambahan Advance Filter--%>
                    <%--<ext:Button ID="AdvancedFilter" runat="server" Text="Advanced Filter" Icon="Magnifier" Handler="NawadataDirect.BtnAdvancedFilter_Click()" />--%>
                    <%--End Update Penambahan Advance Filter--%>

                    <%-- Generate XML Bulk --%>
                    <ext:Button ID="btn_GenerateXML_Bulk" runat="server" Text="Generate XML Bulk" Icon="DiskDownload">
                        <DirectEvents>
                            <Click OnEvent="GenerateXML_Bulk" Success="NawadataDirect.Download_Bulk({isUpload : true});" >
                                <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <%-- End of Generate XML Bulk --%>
        
                </Items>
            </ext:Toolbar>
            <%--Begin Update Penambahan Advance Filter--%>
            <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true" Dock="Top" Hidden="true">
                <Items>
                    <ext:HyperlinkButton ID="HyperlinkButton1" runat="server" Text="Clear Advanced Filter">
                        <DirectEvents>
                            <Click OnEvent="btnClear_Click"></Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                    <ext:Label ID="LblAdvancedFilter" runat="server" Text="">
                    </ext:Label>
                </Items>
            </ext:Toolbar>
        </DockedItems>
        <%--End Update Penambahan Advance Filter--%>

        <Listeners>
            <ViewReady Handler="columnAutoResize(this);
                    this.getStore().on('load', Ext.bind(columnAutoResize, null, [this]));"
                Delay="10" />
        </Listeners>
        <%--<SelectionModel>
            <ext:CheckboxSelectionModel runat="server" Mode="Single">
            </ext:CheckboxSelectionModel>
        </SelectionModel>--%>
    </ext:GridPanel>
     <%--Begin Update Penambahan Advance Filter--%>
    <ext:Panel ID="Panel1" runat="server" Hidden="true">
        <Content>
            <uc1:AdvancedFilter runat="server" ID="AdvancedFilter1" />
        </Content>
        <Items>
        </Items>
    </ext:Panel>
    <%--End Update Penambahan Advance Filter--%>

    <%-- Pop Up Window Validate XSD Bulk Error Messages --%>
    <ext:Window ID="Window_ValidateXSD_Bulk_ErrorMessage" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout" Title="Validation XSD Schema Error Message">
        <Items>
            <ext:FormPanel ID="fp_ValidateXSD_Bulk_ErrorMessage" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Content>
                    <ext:DisplayField runat="server" Text="Some data that doesn't match the XSD schema."></ext:DisplayField>
                    <ext:GridPanel ID="gp_ValidateXSD_Bulk_ErrorMessage" runat="server" Title="" AutoScroll="true" Border="true" PaddingSpec="10 0" MinHeight="200">
                        <Store>
                            <ext:Store ID="Store4" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model4" runat="server" IDProperty="PK_SIPENDAR_PROFILE_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_PROFILE_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="GCN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Customer_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ErrorMessageXSD" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="30"></ext:RowNumbererColumn>
                                <ext:Column ID="Column17" runat="server" DataIndex="GCN" Text="GCN" MinWidth="100" Flex="1"></ext:Column>
                                <ext:Column ID="Column1" runat="server" DataIndex="Customer_Name" Text="Customer Name" MinWidth="200" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="ErrorMessageXSD" Text="Error Message" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Content>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_ValidateXSD_Bulk_ErrorMessage_Close" runat="server" Icon="Cancel" Text="Close">
                <DirectEvents>
                    <Click OnEvent="btn_ValidateXSD_Bulk_ErrorMessage_Close_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.9, height: size.height * 0.8});" />
            <Resize Handler="#{Window_ValidateXSD_Bulk_ErrorMessage}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Validate XSD Bulk Error Messages --%>

</asp:Content>

