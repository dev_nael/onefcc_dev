﻿Imports Ext.Net
Imports OfficeOpenXml
Imports NawaBLL
Imports System.Data
Imports NawaDAL
Imports System.Xml
Imports System.Data.SqlClient
Imports System.IO

Partial Class SiPendarProfile_View

    Inherits Parent
    Public objFormModuleView As NawaBLL.FormModuleView

    Public Property strWhereClause() As String
        Get
            Return Session("SiPendarProfile_View.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("SiPendarProfile_View.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("SiPendarProfile_View.strSort")
        End Get
        Set(ByVal value As String)
            Session("SiPendarProfile_View.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("SiPendarProfile_View.indexStart")
        End Get
        Set(ByVal value As String)
            Session("SiPendarProfile_View.indexStart") = value
        End Set
    End Property
    Public Property QueryTable() As String
        Get
            Return Session("SiPendarProfile_View.Table")
        End Get
        Set(ByVal value As String)
            Session("SiPendarProfile_View.Table") = value
        End Set
    End Property
    Public Property QueryField() As String
        Get
            Return Session("SiPendarProfile_View.Field")
        End Get
        Set(ByVal value As String)
            Session("SiPendarProfile_View.Field") = value
        End Set
    End Property

    '28-Sep-2021 Adi : show event mask on generate XML
    Public Property Filetodownload() As String
        Get
            Return Session("SiPendarProfile_View.Filetodownload")
        End Get
        Set(ByVal value As String)
            Session("SiPendarProfile_View.Filetodownload") = value
        End Set
    End Property

    Public Property FiletodownloadXML() As String
        Get
            Return Session("SiPendarProfile_View.FiletodownloadXML")
        End Get
        Set(ByVal value As String)
            Session("SiPendarProfile_View.FiletodownloadXML") = value
        End Set
    End Property
    '-------------------------------------------------

    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub


    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            'Begin Penambahan Advanced Filter
            If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                Toolbar2.Hidden = True
            Else
                Toolbar2.Hidden = False
            End If
            LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")
            'END Penambahan Advanced Filter


            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

            strfilter = strfilter.Replace("Active", objFormModuleView.ModuleName & ".Active")


            Dim strsort As String = "PK_SIPENDAR_PROFILE_ID DESC"
            For Each item As DataSorter In e.Sort
                'strsort += ", " & item.Property & " " & item.Direction.ToString
                strsort = item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter
            'Begin Penambahan Advanced Filter
            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            End If
            'END Penambahan Advanced Filter

            Me.strOrder = strsort

            'QueryTable = "vw_SIPENDAR_PROFILE"
            'QueryField = "*"
            'Dim DataPaging As DataTable = SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)

            Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)

            'Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = DLL.SQLHelper.ExecuteTabelPaging("CustomerInformation_WebTempTable", "CIFNo, Name, DateOfBirth, AccountOwnerId, OpeningDate, WorkingUnitName, IsCustomerInList", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Begin Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdd_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim sm As RowSelectionModel = TryCast(Me.GridpanelView.GetSelectionModel(), RowSelectionModel)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SiPendarProfile_View_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub

    Private Sub SiPendarProfile_View_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer
            Try
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)


                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                    Dim strIDCode As String = 1
                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If

                objFormModuleView.ModuleID = objmodule.PK_Module_ID
                objFormModuleView.ModuleName = objmodule.ModuleName

                objFormModuleView.AddField("PK_SIPENDAR_PROFILE_ID", "Profile ID", 1, True, True, NawaBLL.Common.MFieldType.BIGIDENTITY)
                objFormModuleView.AddField("SiPendar_Type", "SiPendar Type", 2, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("StatusDescription", "Status Description", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("AGING", "Due Days", 3, False, True, NawaBLL.Common.MFieldType.INTValue)
                objFormModuleView.AddField("GCN", "GCN", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                '' Add 22-Dec-2021
                If intModuleid = "20203" Then '' 20203 - vw_SIPENDAR_PROFILE_PENGAYAAN	SIPENDAR Pengayaan
                    objFormModuleView.AddField("Sumber_ID", "Sumber ID", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                End If
                '' End 22-Dec-2021
                objFormModuleView.AddField("Customer_Name", "Customer Name", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Customer_Type", "Customer Type", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Keterangan", "Remarks", 7, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("CreatedBy", "Created By", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("CreatedDate", "Created Date", 9, False, True, NawaBLL.Common.MFieldType.DATETIMEValue, "dd-MMM-yyyy")
                objFormModuleView.AddField("LastUpdateDate", "Last Update Date", 10, False, True, NawaBLL.Common.MFieldType.DATETIMEValue, "dd-MMM-yyyy")
                objFormModuleView.AddField("IsValid", "Valid", 11, False, True, NawaBLL.Common.MFieldType.BooleanValue)
                objFormModuleView.AddField("ErrorMessage", "Error Message", 12, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Status", "Status ID", 13, False, False, NawaBLL.Common.MFieldType.INTValue)
                objFormModuleView.AddField("SubmissionType", "Submission Type", 14, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("IsHaveReportTransaction", "Have Report Trx", 15, False, True, NawaBLL.Common.MFieldType.BooleanValue)
                objFormModuleView.AddField("FK_Report_ID", "Report ID", 16, False, True, NawaBLL.Common.MFieldType.BIGINTValue)
                objFormModuleView.AddField("ParameterValue", "ParameterValue", 17, False, False, NawaBLL.Common.MFieldType.BIGINTValue)

                objFormModuleView.SettingFormView()

                'Custom GridCommand
                Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
                Dim gridCommandResponse As New GridCommand

                objcommandcol.Width = 450
                Dim objGenerate As New GridCommand
                objGenerate.CommandName = "Generate"
                objGenerate.Icon = Icon.DiskDownload
                objGenerate.Text = "Generate XML"
                objGenerate.ToolTip.Text = ""
                objcommandcol.Commands.Add(objGenerate)

                Dim objRedirect As New GridCommand
                objRedirect.CommandName = "ViewReportTrx"
                objRedirect.Icon = Icon.ArrowRight
                objRedirect.Text = "View Transaction"
                objRedirect.ToolTip.Text = ""
                'If objmodule.ModuleLabel = "SIPENDAR PROAKTIF" Then
                '    objRedirect.Disabled = True
                'End If
                objcommandcol.Commands.Add(objRedirect)

                Dim extparam As New Ext.Net.Parameter
                extparam.Name = "unikkey"
                extparam.Value = "record.data.PK_SIPENDAR_PROFILE_ID"
                extparam.Mode = ParameterMode.Raw
                objcommandcol.DirectEvents.Command.ExtraParams.Add(extparam)

                objcommandcol.DirectEvents.Command.IsUpload = True
                objcommandcol.DirectEvents.Command.EventMask.ShowMask = True
                objcommandcol.DirectEvents.Command.EventMask.Msg = "Loading..."
                objcommandcol.DirectEvents.Command.Success = "NawadataDirect.Download_XML({isUpload : true});"

                Dim extParamCommandName As New Ext.Net.Parameter
                extparam.Name = "command"
                extparam.Value = "command"
                extparam.Mode = ParameterMode.Raw
                objcommandcol.DirectEvents.Command.ExtraParams.Add(extparam)

                AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf GenerateXML
                AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf RedirectToSiPendarReport

                '18-Aug-2021 Adi : Cek hak akses. Jika tidak ada hak akses Edit maka buttonnya hanya akan terlihat 3. Jadi index nya akan berubah saat disabled
                'If objmodule IsNot Nothing Then
                '    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, Common.ModuleActionEnum.Update) Then
                '        objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection_Checker"
                '    Else
                '        objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"
                '    End If
                'End If

                '29-Nov-2021 Daniel : Count hak akses.
                Dim aksescount As Integer = 0
                If objmodule IsNot Nothing Then
                    If ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, Common.ModuleActionEnum.Update) Then
                        aksescount += 1
                    End If
                    If ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, Common.ModuleActionEnum.Activation) Then
                        aksescount += 1
                    End If
                    If ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, Common.ModuleActionEnum.Delete) Then
                        aksescount += 1
                    End If
                    If ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        aksescount += 1
                    End If
                End If
                Dim stringcommand As String = "prepareCommandCollection_" & aksescount
                objcommandcol.PrepareToolbar.Fn = stringcommand

            Catch ex As Exception

            End Try

            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GenerateXML(sender As Object, e As DirectEventArgs)
        Try
            FiletodownloadXML = ""

            If e.ExtraParams("command") = "Generate" Then

                'Define the parameter for the XML filename
                Dim pkProfileID As Long = e.ExtraParams("unikkey")
                Dim isMustValidToDownload As Integer = 0

                '' Add 18-Oct-2021 Felix, validasi Submission Date harus sama dengan Watchlist.Periode
                '' Remarkd 18-Nov-2021, kembali seperti dulu
                'Dim objParamCheckSubmissionDate(0) As SqlParameter
                'objParamCheckSubmissionDate(0) = New SqlParameter
                'objParamCheckSubmissionDate(0).ParameterName = "@PK_Profile_ID"
                'objParamCheckSubmissionDate(0).Value = pkProfileID

                'Dim str_SubmissionDateValidate As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_Check_SubmissionDate_Valid", objParamCheckSubmissionDate)
                'If str_SubmissionDateValidate IsNot Nothing Then
                '    If str_SubmissionDateValidate <> "" Then
                '        Throw New ApplicationException("" + str_SubmissionDateValidate + "")
                '    End If
                'End If
                '' End 18-Oct-2021

                'Get SIPENDAR TYPE
                Dim intSIPENDARTYPE As Integer = 1
                Dim strSumberID As String = "" '' Add 23-Nov-2021, kalau pengayaan, ditambah SumberID dinama filenya
                Dim gcn As String = "" ' 2 Dec 2021 Ari penambahan GCN untuk nama file
                Dim strSubmissionType As String = "" ' Add 16-Dec-2021 Felix
                Using objdb As New SiPendarDAL.SiPendarEntities
                    Dim objProfile = objdb.SIPENDAR_PROFILE.Where(Function(x) x.PK_SIPENDAR_PROFILE_ID = pkProfileID).FirstOrDefault
                    If objProfile IsNot Nothing Then
                        intSIPENDARTYPE = objProfile.FK_SIPENDAR_TYPE_ID
                        gcn = objProfile.GCN
                        Dim clean As String = ""
                        clean = gcn.Replace("-", "")
                        gcn = clean
                        If objProfile.FK_SIPENDAR_TYPE_ID = 2 Then '' Add 23-Nov-2021, kalau pengayaan, ditambah SumberID dinama filenya
                            strSumberID = objProfile.SUMBER_ID
                        End If
                        strSubmissionType = objProfile.FK_SIPENDAR_SUBMISSION_TYPE_CODE '' Add 16-Dec-2021 Felix
                    End If

                        Dim objGlobalParameter = objdb.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 2).FirstOrDefault
                    If objGlobalParameter IsNot Nothing AndAlso IsNumeric(objGlobalParameter.ParameterValue) Then
                        isMustValidToDownload = CInt(objGlobalParameter.ParameterValue)
                    End If
                End Using

                'Validate XSD
                If isMustValidToDownload = 1 Then
                    ValidateXSD_Satuan(pkProfileID)
                End If

                'Define the temp directory to save the xml
                Dim serverPath As String = Server.MapPath("~\goaml\FolderExport\")
                Dim strDirPath = serverPath & Common.SessionCurrentUser.UserID & "\"
                If Not Directory.Exists(strDirPath) Then
                    Directory.CreateDirectory(strDirPath)
                End If

                Dim strReportCode = "SIPENDAR-PROAKTIF"
                If intSIPENDARTYPE = 2 Then
                    strReportCode = "SIPENDAR-PENGAYAAN"
                End If

                Dim strTanggalGenerate As String = Convert.ToDateTime(Now()).ToString("yyyyMMddHHmmss")
                'Dim strFileName As String = strReportCode & "-" & pkProfileID & "-" & strTanggalGenerate & ".xml"  '' Add 23-Nov-2021, kalau pengayaan, ditambah SumberID dinama filenya
                Dim strFileName As String = ""
                If intSIPENDARTYPE = 2 Then
                    'strFileName = strReportCode & "-" & pkProfileID & "-" & strSumberID & "-" & strTanggalGenerate & "-" & gcn & ".xml"
                    strFileName = strReportCode & "-" & pkProfileID & "-" & strSumberID & "-" & strTanggalGenerate & "-" & gcn & "-" & strSubmissionType & ".xml" '' Edit 16-Dec-2021 tambah SubmissionType
                Else
                    'strFileName = strReportCode & "-" & pkProfileID & "-" & strTanggalGenerate & "-" & gcn & ".xml"  '' Add 23-Nov-2021, kalau pengayaan, ditambah SumberID dinama filenya
                    strFileName = strReportCode & "-" & pkProfileID & "-" & strTanggalGenerate & "-" & gcn & "-" & strSubmissionType & ".xml"  '' Edit 16-Dec-2021 tambah SubmissionType
                End If


                Dim strFilePath As String = strDirPath & strFileName

                'delete kalau sudah pernah ada
                If IO.File.Exists(strFilePath) Then
                    IO.File.Delete(strFilePath)
                End If

                'Get data row to generate to XML
                Dim objParam(2) As SqlParameter
                objParam(0) = New SqlParameter
                objParam(0).ParameterName = "@PK"
                objParam(0).Value = pkProfileID

                objParam(1) = New SqlParameter
                objParam(1).ParameterName = "@session"
                objParam(1).Value = NawaBLL.Common.SessionCurrentUser.UserID

                objParam(2) = New SqlParameter
                objParam(2).ParameterName = "@FK_SIPENDAR_Type"
                objParam(2).Value = intSIPENDARTYPE

                Dim xmlDataRow As DataRow = SQLHelper.ExecuteRow(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_GenerateXML", objParam)
                Dim strResult As String = xmlDataRow(0)

                '' Added by Felix 02 Aug 2021
                If IsDBNull(strResult) Or String.IsNullOrEmpty(strResult) Then
                    Throw New ApplicationException("XML tidak terbentuk. Mohon check query generate XML.")
                End If
                '' End of 02 Aug 2021

                ' Added by Felix 29 Jul 2021
                'Get data row to generate to XML
                Dim objParamUpdateStatus(1) As SqlParameter
                objParamUpdateStatus(0) = New SqlParameter
                objParamUpdateStatus(0).ParameterName = "@PK"
                objParamUpdateStatus(0).Value = pkProfileID

                objParamUpdateStatus(1) = New SqlParameter
                objParamUpdateStatus(1).ParameterName = "@session"
                objParamUpdateStatus(1).Value = NawaBLL.Common.SessionCurrentUser.UserID

                SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_UpdateStatusGenerateXML", objParamUpdateStatus)
                ' End of 29 Jul 2021

                'Save the XML temp file to temp directory
                Dim objDoc As New XmlDocument
                objDoc.LoadXml(strResult)
                objDoc.PreserveWhitespace = False
                objDoc.Save(strFilePath)

                '28-Sep-2021 Adi : Separate Download function to Direct Method to show event mask
                'Download the XML
                'Response.Clear()
                'Response.ContentType = "text/xml"
                ''Response.ContentType = "application/octet-stream"
                'Response.AddHeader("content-disposition", "attachment; filename=" & strFileName)
                'Response.TransmitFile(strFilePath)
                'Response.[End]()

                FiletodownloadXML = strFilePath
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Sub Download_XML()
        If String.IsNullOrEmpty(FiletodownloadXML) Then
            Exit Sub
        End If

        Response.Clear()
        Response.ContentType = "text/xml"
        'Response.ContentType = "application/octet-stream"
        'Response.AddHeader("content-disposition", "attachment; filename=" & Filetodownload)
        Response.AddHeader("content-disposition", "attachment; filename=" & IO.Path.GetFileName(FiletodownloadXML))
        'Response.TransmitFile(Filetodownload)
        Response.BinaryWrite(File.ReadAllBytes(FiletodownloadXML))
        Response.[End]()

        FiletodownloadXML = ""
    End Sub

    Protected Sub GenerateXML_Bulk()
        Try
            Filetodownload = ""

            'Cek minimal 1 profile hasil screening dipilih
            Dim smScreeningResult As RowSelectionModel = TryCast(GridpanelView.GetSelectionModel(), RowSelectionModel)
            If smScreeningResult.SelectedRows.Count = 0 Then
                Throw New Exception("Minimal 1 data harus dipilih!")
            End If

            'Get Parameter
            Dim isMustValidToDownload As Integer = 0
            Dim intCountNotReadyToGenerate As Integer = 0
            Dim gcn As String = "" ' 2 Dec 2021 Ari penambahan untuk penamaan file dengan gcn
            Using objdb As New SiPendarDAL.SiPendarEntities
                Dim objGlobalParameter = objdb.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 2).FirstOrDefault
                If objGlobalParameter IsNot Nothing AndAlso IsNumeric(objGlobalParameter.ParameterValue) Then
                    isMustValidToDownload = CInt(objGlobalParameter.ParameterValue)
                End If

                'Cek Status Sebelum Generate
                For Each item As SelectedRow In smScreeningResult.SelectedRows
                    Dim pkProfileID = item.RecordID.ToString

                    ' Added by Felix 29 Jul 2021
                    'Get data row to generate to XML
                    'Dim objParamUpdateStatus(1) As SqlParameter
                    'objParamUpdateStatus(0) = New SqlParameter
                    'objParamUpdateStatus(0).ParameterName = "@PK"
                    'objParamUpdateStatus(0).Value = pkProfileID

                    'objParamUpdateStatus(1) = New SqlParameter
                    'objParamUpdateStatus(1).ParameterName = "@session"
                    'objParamUpdateStatus(1).Value = NawaBLL.Common.SessionCurrentUser.UserID

                    'SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_UpdateStatusGenerateXML", objParamUpdateStatus)
                    Dim objProfile = objdb.SIPENDAR_PROFILE.Where(Function(x) x.PK_SIPENDAR_PROFILE_ID = pkProfileID).FirstOrDefault
                    If objGlobalParameter IsNot Nothing AndAlso Not (objProfile.Status = "1" Or objProfile.Status = "8") Then
                        intCountNotReadyToGenerate += 1
                    End If

                Next
            End Using

            If intCountNotReadyToGenerate > 0 Then
                Throw New Exception("SIPENDAR yang dipilih harus yang statusnya semua Waiting For Generate.")
            End If

            'Validate Schema XSD
            If isMustValidToDownload = 1 Then
                Dim strError = ""
                strError = ValidateXSD_Bulk()
                If Not String.IsNullOrEmpty(strError) Then
                    Window_ValidateXSD_Bulk_ErrorMessage.Hidden = False
                    Exit Sub
                End If
            End If

            'Save to list SIPENDAR Profile yang mau di generate XML
            Dim listSiPendarProfile = New List(Of SiPendarDAL.SIPENDAR_PROFILE)

            For Each item As SelectedRow In smScreeningResult.SelectedRows
                Dim recordID = item.RecordID.ToString
                Dim objNew = New SiPendarDAL.SIPENDAR_PROFILE
                With objNew
                    .PK_SIPENDAR_PROFILE_ID = recordID
                    Using objdb As New SiPendarDAL.SiPendarEntities
                        Dim objProfile = objdb.SIPENDAR_PROFILE.Where(Function(x) x.PK_SIPENDAR_PROFILE_ID = recordID).FirstOrDefault
                        If objProfile IsNot Nothing Then
                            objNew.GCN = objProfile.GCN
                        End If

                    End Using

                End With

                listSiPendarProfile.Add(objNew)
            Next

            'Define the temp directory to save the xml
            Dim serverPath As String = Server.MapPath("~\goaml\FolderExport\")
            Dim strDirPath = serverPath & Common.SessionCurrentUser.UserID & "\"
            If Not Directory.Exists(strDirPath) Then
                Directory.CreateDirectory(strDirPath)
            End If

            'Generate XML and get the file result in zip
            Dim strFileReturn As String = ""
            'strFileReturn = SiPendarBLL.SiPendarProfile_BLL.GenerateXML_ProAktif(listSiPendarProfile, strDirPath)
            strFileReturn = SiPendarBLL.SiPendarProfile_BLL.GenerateXML_Bulk(listSiPendarProfile, strDirPath, objFormModuleView.ModuleName)
            If strFileReturn Is Nothing Then
                Throw New Exception("Reports Not exist")
            End If

            '' Pindah ke sini on 01 Sept 2021
            For Each item As SelectedRow In smScreeningResult.SelectedRows
                Dim pkProfileID = item.RecordID.ToString

                ' Added by Felix 29 Jul 2021
                'Get data row to generate to XML
                Dim objParamUpdateStatus(1) As SqlParameter
                objParamUpdateStatus(0) = New SqlParameter
                objParamUpdateStatus(0).ParameterName = "@PK"
                objParamUpdateStatus(0).Value = pkProfileID

                objParamUpdateStatus(1) = New SqlParameter
                objParamUpdateStatus(1).ParameterName = "@session"
                objParamUpdateStatus(1).Value = NawaBLL.Common.SessionCurrentUser.UserID

                SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_UpdateStatusGenerateXML", objParamUpdateStatus)
            Next
            '' End of 01 Sept 2021

            '28-Sep-2021 Adi : Separate Download function to Direct Method to show event mask
            'Response.Clear()
            'Response.ClearHeaders()
            'Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(strFileReturn))
            'Response.Charset = ""
            'Response.AddHeader("cache-control", "max-age=0")
            'Me.EnableViewState = False
            'Response.ContentType = "application/zip"
            'Response.BinaryWrite(File.ReadAllBytes(strFileReturn))
            'Response.End()
            'Download(strFileReturn)

            Filetodownload = strFileReturn

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    '28-Sep-2021 Adi : separate function download
    <DirectMethod>
    Sub Download_Bulk()
        If String.IsNullOrEmpty(Filetodownload) Then
            Exit Sub
        End If

        Response.Clear()
        Response.ClearHeaders()
        Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(Filetodownload))
        Response.Charset = ""
        Response.AddHeader("cache-control", "max-age=0")
        Me.EnableViewState = False
        Response.ContentType = "application/zip"
        Response.BinaryWrite(File.ReadAllBytes(Filetodownload))
        Response.End()

        Filetodownload = ""
    End Sub

    Protected Sub ValidateXSD_Satuan(pkProfileID As Long)
        Try
            Dim strUserID = NawaBLL.Common.SessionCurrentUser.UserID

            'Jalankan validasi untuk mengisi Error Message
            SiPendarBLL.SipendarValidateXSDBLL.ValidateSchema(pkProfileID, strUserID)

            'Get error message from database
            Dim errorMessage As String = ""
            errorMessage = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "Select ErrorMessageXSD FROM SIPENDAR_PROFILE WHERE PK_SIPENDAR_PROFILE_ID=" & pkProfileID, Nothing)

            If Not IsDBNull(errorMessage) AndAlso Not String.IsNullOrEmpty(errorMessage) Then
                Throw New ApplicationException("<b>XSD Validation Error :</b><br> " & errorMessage)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function ValidateXSD_Bulk() As String
        Try
            Dim smScreeningResult As RowSelectionModel = TryCast(GridpanelView.GetSelectionModel(), RowSelectionModel)
            Dim strUserID = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strRetErrorMessage As String = ""
            Dim strListProfile As String = ""

            'Save to list SIPENDAR Profile yang mau di generate XML
            For Each item As SelectedRow In smScreeningResult.SelectedRows
                Dim pkProfileID = item.RecordID.ToString
                SiPendarBLL.SipendarValidateXSDBLL.ValidateSchema(pkProfileID, strUserID)

                'Get error message from database
                Dim errorMessage As String = ""
                errorMessage = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT ErrorMessageXSD FROM SIPENDAR_PROFILE WHERE PK_SIPENDAR_PROFILE_ID=" & pkProfileID, Nothing)
                If Not IsDBNull(errorMessage) AndAlso Not String.IsNullOrEmpty(errorMessage) Then
                    strRetErrorMessage = strRetErrorMessage & errorMessage
                End If

                If strListProfile = "" Then
                    strListProfile = strListProfile & pkProfileID
                Else
                    strListProfile = strListProfile & "," & pkProfileID
                End If
            Next

            If Not String.IsNullOrEmpty(strRetErrorMessage) Then
                Dim dtSiPendarProfile As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM vw_SIPENDAR_PROFILE WHERE ISNULL(ErrorMessageXSD,'')<>'' AND PK_SIPENDAR_PROFILE_ID IN (" & strListProfile & ")", Nothing)

                gp_ValidateXSD_Bulk_ErrorMessage.GetStore.DataSource = dtSiPendarProfile
                gp_ValidateXSD_Bulk_ErrorMessage.GetStore.DataBind()
            End If

            Return strRetErrorMessage

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub btn_ValidateXSD_Bulk_ErrorMessage_Close_Click()
        Try
            Window_ValidateXSD_Bulk_ErrorMessage.Hidden = True
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub RedirectToSiPendarReport(sender As Object, e As DirectEventArgs)
        Try
            If e.ExtraParams("command") = "ViewReportTrx" Then
                Dim objModuleSiPendarReport As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SiPendarReport")
                If objModuleSiPendarReport IsNot Nothing Then
                    Dim strEncModuleID As String = NawaBLL.Common.EncryptQueryString(objModuleSiPendarReport.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                    'session untuk parameter pk di view transaction
                    Session("ViewTransactionSIPENDAR_Pengayaan_PK_DeleteMenu") = Nothing
                    Session("ViewTransactionSIPENDAR_Pengayaan_PK_DetailMenu") = Nothing
                    Session("ViewTransactionSIPENDAR_Pengayaan_PK_EditMenu") = Nothing
                    'Get Parameter Profile ID
                    Dim pkProfileID As String = e.ExtraParams("unikkey")
                    Session("ViewTransactionSIPENDAR_Pengayaan_PK") = pkProfileID

                    If InStr(objModuleSiPendarReport.UrlView, "?") > 0 Then
                        Session("ViewTransactionSIPENDAR_Pengayaan") = True
                        Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objModuleSiPendarReport.UrlView & "&ModuleID=" & strEncModuleID & "&ProfileID=" & pkProfileID)
                    Else
                        Session("ViewTransactionSIPENDAR_Pengayaan") = True
                        Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objModuleSiPendarReport.UrlView & "?ModuleID=" & strEncModuleID & "&ProfileID=" & pkProfileID)
                    End If
                Else
                    Throw New ApplicationException("Module vw_SiPendarReport is not exists!")
                End If

            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

End Class
