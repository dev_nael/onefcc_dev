﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/Site1.Master" CodeFile="SIPENDAR_UploadReportView.aspx.vb" Inherits="SIPENDAR_UploadReportView" %>
<%--Begin Update Penambahan Advance Filter--%>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>
<%--End Update Penambahan Advance Filter--%>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <script type="text/javascript" >
           Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
               return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
           };
           Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
               return {
                   type: "string",
                   op: "*",
                   value: value
               };
           };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:GridPanel ID="GridpanelView" runat="server" Title="Title" Layout="FitLayout">
        <Store>
            <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadData">

                <Sorters>
                    <%--<ext:DataSorter Property="" Direction="ASC" />--%>
                </Sorters>
                <Proxy>
                    <ext:PageProxy />
                </Proxy>
            </ext:Store>
        </Store>
        <Plugins>
            <%--<ext:GridFilters ID="GridFilters1" runat="server" />--%>
            <ext:FilterHeader ID="grdiheaderfilter" runat="server" Remote="true"></ext:FilterHeader>

        </Plugins>
        <BottomBar>
            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
        </BottomBar>
        <DockedItems>
        <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                <Items>
                    <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" EmptyText="[Select Format]" FieldLabel="Export :">

                        <Items>
                            <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                            <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                        </Items>

                    </ext:ComboBox>
                    <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" AutoPostBack="true" OnClick="ExportExcel" ClientIDMode="Static" />
                    <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page" AutoPostBack="true" OnClick="ExportAllExcel" />
                    <%--<ext:Button ID="Button1" runat="server" Text="Print Current Page" Icon="Printer" Handler="this.up('grid').print({currentPageOnly : true});" />--%>
                    <ext:Button ID="BtnAdd" runat="server" Text="Add New Record" Icon="Add">
                        <DirectEvents>
                            <Click OnEvent="BtnAdd_Click">
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                       <ext:Button ID="AdvancedFilter" runat="server" Text="Advanced Filter" Icon="Add" Handler="NawadataDirect.BtnAdvancedFilter_Click()">
                        </ext:Button>

                </Items>
            </ext:Toolbar>
         <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true" Dock="Top" Hidden="true">
                <Items>
                    <ext:HyperlinkButton ID="HyperlinkButton1" runat="server" Text="Clear Advanced Filter">
                        <DirectEvents>
                            <Click OnEvent="btnClear_Click"></Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                    <ext:Label ID="LblAdvancedFilter" runat="server" Text="">
                    </ext:Label>
                </Items>
            </ext:Toolbar>
        </DockedItems>

    </ext:GridPanel>


        <%--Begin Update Penambahan Advance Filter--%>
    <ext:Panel ID="Panel1" runat="server" Hidden="true">
        <Content>
            <uc1:AdvancedFilter runat="server" ID="AdvancedFilter1" />
        </Content>
        <Items>
        </Items>
    </ext:Panel>
    <%--End Update Penambahan Advance Filter--%>

</asp:Content>
