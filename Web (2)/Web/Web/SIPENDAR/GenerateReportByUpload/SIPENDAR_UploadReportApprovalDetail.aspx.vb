﻿Imports System.Data
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports SiPendarDAL
Imports NawaBLL
Imports Ext
Imports SiPendarBLL
Imports System.Data.SqlClient
Imports NawaDAL
Imports Elmah
Imports System.IO
Imports OfficeOpenXml

Partial Class SIPENDAR_UploadReportApprovalDetail
    Inherits Parent
    Public UploadReportSTR As New SiPendarBLL.UploadReportSTR


#Region "Session"
    Private Sub ClearSession()
        ListReportValid = New List(Of SIPENDAR_ReportSTR)
        ListIndikatorLaporanValid = New List(Of SIPENDAR_Indikator_LaporanSTR)
        ListTransactionBipartyValid = New List(Of SIPENDAR_Transaction_BiPartySTR)
        ListTransactionMultipartyValid = New List(Of SIPENDAR_Transaction_MultiPartySTR)
        ListActivityValid = New List(Of SIPENDAR_ActivitySTR)
        ListReportNotValid = New List(Of SiPendarDAL.SIPENDAR_ReportSTR)
        ListIndikatorLaporanNotValid = New List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR)
        ListTransactionBipartyNotValid = New List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR)
        ListTransactionMultipartyNotValid = New List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR)
        ListActivityNotValid = New List(Of SiPendarDAL.SIPENDAR_ActivitySTR)
    End Sub


    Private Sub Bindgrid()

        StoreReportData.DataSource = ListReportValid.ToList()
        StoreReportData.DataBind()
        StoreIndikatorLaporanData.DataSource = ListIndikatorLaporanValid.ToList()
        StoreIndikatorLaporanData.DataBind()
        StoreTransactionBiParty.DataSource = ListTransactionBipartyValid.ToList()
        StoreTransactionBiParty.DataBind()
        StoreTransactionMultiParty.DataSource = ListTransactionMultipartyValid.ToList()
        StoreTransactionMultiParty.DataBind()
        StoreActivityAccount.DataSource = ListActivityValid.Where(Function(x) x.SubNodeType = 1).ToList()
        StoreActivityAccount.DataBind()
        StoreActivityPerson.DataSource = ListActivityValid.Where(Function(x) x.SubNodeType = 2).ToList()
        StoreActivityPerson.DataBind()
        StoreActivityEntity.DataSource = ListActivityValid.Where(Function(x) x.SubNodeType = 3).ToList()
        StoreActivityEntity.DataBind()

        If ListReportNotValid.ToList().Count > 0 Then
            GridPanelReportInvalid.Hidden = False
            PanelReportInvalid.Hidden = False
            StoreReportInvalid.DataSource = ListReportNotValid.ToList()
            StoreReportInvalid.DataBind()
        End If
        If ListIndikatorLaporanNotValid.ToList().Count > 0 Then
            GridPanelIndikatorLaporanInvalid.Hidden = False
            PanelIndikatorLaporanInvalid.Hidden = False
            StoreIndikatorLaporanInvalid.DataSource = ListIndikatorLaporanNotValid.ToList()
            StoreIndikatorLaporanInvalid.DataBind()
        End If
        If ListTransactionBipartyNotValid.ToList().Count > 0 Then
            GridPanelTransactionBipartyInvalid.Hidden = False
            PanelTransactionBipartyInvalid.Hidden = False
            StoreTransactionBipartyInvalid.DataSource = ListTransactionBipartyNotValid.ToList()
            StoreTransactionBipartyInvalid.DataBind()
        End If
        If ListTransactionMultipartyNotValid.ToList().Count > 0 Then
            GridPanelTransactionMultiPartyInvalid.Hidden = False
            PanelTransactionMultiPartyInvalid.Hidden = False
            StoreTransactionMultiPartyInvalid.DataSource = ListTransactionMultipartyNotValid.ToList()
            StoreTransactionMultiPartyInvalid.DataBind()
        End If
        If ListActivityNotValid.Where(Function(x) x.SubNodeType = 1).ToList().Count > 0 Then
            GridPanelActivityAccountInvalid.Hidden = False
            PanelActivityAccountInvalid.Hidden = False
            StoreActivityAccountInvalid.DataSource = ListActivityNotValid.Where(Function(x) x.SubNodeType = 1).ToList()
            StoreActivityAccountInvalid.DataBind()
        End If
        If ListActivityNotValid.Where(Function(x) x.SubNodeType = 2).ToList().Count > 0 Then
            GridPanelActivityPersonInvalid.Hidden = False
            PanelActivityPersonInvalid.Hidden = False
            StoreActivityPersonInvalid.DataSource = ListActivityNotValid.Where(Function(x) x.SubNodeType = 2).ToList()
            StoreActivityPersonInvalid.DataBind()
        End If
        If ListActivityNotValid.Where(Function(x) x.SubNodeType = 3).ToList().Count > 0 Then
            GridPanelActivityEntityInvalid.Hidden = False
            PanelActivityEntityInvalid.Hidden = False
            StoreActivityEntityInvalid.DataSource = ListActivityNotValid.Where(Function(x) x.SubNodeType = 3).ToList()
            StoreActivityEntityInvalid.DataBind()
        End If

    End Sub

    Private _IDReq As Long
    Public Property IDReq() As Long
        Get
            Return _IDReq
        End Get
        Set(ByVal value As Long)
            _IDReq = value
        End Set
    End Property

    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("SIPENDAR_UploadReportApprovalDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("SIPENDAR_UploadReportApprovalDetail.ObjModule") = value
        End Set
    End Property

    Public Property ListReportValid As List(Of SiPendarDAL.SIPENDAR_ReportSTR)
        Get
            If Session("SIPENDAR_UploadReportApprovalDetail.ListReportValid") Is Nothing Then
                Session("SIPENDAR_UploadReportApprovalDetail.ListReportValid") = New List(Of SiPendarDAL.SIPENDAR_ReportSTR)
            End If
            Return Session("SIPENDAR_UploadReportApprovalDetail.ListReportValid")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_ReportSTR))
            Session("SIPENDAR_UploadReportApprovalDetail.ListReportValid") = value
        End Set
    End Property

    Public Property ListReportNotValid As List(Of SiPendarDAL.SIPENDAR_ReportSTR)
        Get
            If Session("SIPENDAR_UploadReportApprovalDetail.ListReportNotValid") Is Nothing Then
                Session("SIPENDAR_UploadReportApprovalDetail.ListReportNotValid") = New List(Of SiPendarDAL.SIPENDAR_ReportSTR)
            End If
            Return Session("SIPENDAR_UploadReportApprovalDetail.ListReportNotValid")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_ReportSTR))
            Session("SIPENDAR_UploadReportApprovalDetail.ListReportNotValid") = value
        End Set
    End Property

    Public Property ListIndikatorLaporanValid As List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR)
        Get
            If Session("SIPENDAR_UploadReportApprovalDetail.ListIndikatorLaporanValid") Is Nothing Then
                Session("SIPENDAR_UploadReportApprovalDetail.ListIndikatorLaporanValid") = New List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR)
            End If
            Return Session("SIPENDAR_UploadReportApprovalDetail.ListIndikatorLaporanValid")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR))
            Session("SIPENDAR_UploadReportApprovalDetail.ListIndikatorLaporanValid") = value
        End Set
    End Property

    Public Property ListIndikatorLaporanNotValid As List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR)
        Get
            If Session("SIPENDAR_UploadReportApprovalDetail.ListIndikatorLaporanNotValid") Is Nothing Then
                Session("SIPENDAR_UploadReportApprovalDetail.ListIndikatorLaporanNotValid") = New List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR)
            End If
            Return Session("SIPENDAR_UploadReportApprovalDetail.ListIndikatorLaporanNotValid")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR))
            Session("SIPENDAR_UploadReportApprovalDetail.ListIndikatorLaporanNotValid") = value
        End Set
    End Property

    Public Property ListTransactionBipartyValid As List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR)
        Get
            If Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionBipartyValid") Is Nothing Then
                Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionBipartyValid") = New List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR)
            End If
            Return Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionBipartyValid")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR))
            Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionBipartyValid") = value
        End Set
    End Property

    Public Property ListTransactionBipartyNotValid As List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR)
        Get
            If Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionBipartyNotValid") Is Nothing Then
                Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionBipartyNotValid") = New List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR)
            End If
            Return Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionBipartyNotValid")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR))
            Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionBipartyNotValid") = value
        End Set
    End Property

    Public Property ListTransactionMultipartyValid As List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR)
        Get
            If Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionMultipartyValid") Is Nothing Then
                Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionMultipartyValid") = New List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR)
            End If
            Return Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionMultipartyValid")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR))
            Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionMultipartyValid") = value
        End Set
    End Property

    Public Property ListTransactionMultipartyNotValid As List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR)
        Get
            If Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionMultipartyNotValid") Is Nothing Then
                Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionMultipartyNotValid") = New List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR)
            End If
            Return Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionMultipartyNotValid")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR))
            Session("SIPENDAR_UploadReportApprovalDetail.ListTransactionMultipartyNotValid") = value
        End Set
    End Property

    Public Property ListActivityValid As List(Of SiPendarDAL.SIPENDAR_ActivitySTR)
        Get
            If Session("SIPENDAR_UploadReportApprovalDetail.ListActivityValid") Is Nothing Then
                Session("SIPENDAR_UploadReportApprovalDetail.ListActivityValid") = New List(Of SiPendarDAL.SIPENDAR_ActivitySTR)
            End If
            Return Session("SIPENDAR_UploadReportApprovalDetail.ListActivityValid")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_ActivitySTR))
            Session("SIPENDAR_UploadReportApprovalDetail.ListActivityValid") = value
        End Set
    End Property

    Public Property ListActivityNotValid As List(Of SiPendarDAL.SIPENDAR_ActivitySTR)
        Get
            If Session("SIPENDAR_UploadReportApprovalDetail.ListActivityNotValid") Is Nothing Then
                Session("SIPENDAR_UploadReportApprovalDetail.ListActivityNotValid") = New List(Of SiPendarDAL.SIPENDAR_ActivitySTR)
            End If
            Return Session("SIPENDAR_UploadReportApprovalDetail.ListActivityNotValid")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_ActivitySTR))
            Session("SIPENDAR_UploadReportApprovalDetail.ListActivityNotValid") = value
        End Set
    End Property

    Public Property objSchemaModule() As NawaDAL.Module
        Get
            Return Session("Account_APPROVAL_DETAIL.objSchemaModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("Account_APPROVAL_DETAIL.objSchemaModule") = value
        End Set
    End Property

#End Region

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            GridPanelSetting()
            Paging()
            Dim strModuleid As String = Request.Params("ModuleID")
            Dim strmodule As String = Request.Params("ModuleID")
            Dim strid As String = Request.Params("ID")
            IDReq = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim moduleid As Integer = NawaBLL.Common.DecryptQueryString(strModuleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            objSchemaModule = NawaBLL.ModuleBLL.GetModuleByModuleID(moduleid)
            Dim intmodule As Integer = NawaBLL.Common.DecryptQueryString(strmodule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Me.ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intmodule)

            If Not Ext.Net.X.IsAjaxRequest Then

                ClearSession()

                If Not ObjModule Is Nothing Then
                    'If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Insert) Then
                    If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Approval) Then 'perubahan firman 20201112 mestinya NawaBLL.Common.ModuleActionEnum.Approval agar di menu access tidak perlu di ceklis add nya
                        Dim strIDCode As String = 1
                        strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                        Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    FormPanelInput.Title = "Report STR  - Approval Detail"

                    GridPanelReportInvalid.Hidden = True
                    GridPanelIndikatorLaporanInvalid.Hidden = True
                    GridPanelTransactionBipartyInvalid.Hidden = True
                    GridPanelTransactionMultiPartyInvalid.Hidden = True
                    GridPanelActivityPersonInvalid.Hidden = True
                    GridPanelActivityAccountInvalid.Hidden = True
                    GridPanelActivityEntityInvalid.Hidden = True

                    PanelReportInvalid.Hidden = True
                    PanelIndikatorLaporanInvalid.Hidden = True
                    PanelTransactionBipartyInvalid.Hidden = True
                    PanelTransactionMultiPartyInvalid.Hidden = True
                    PanelActivityPersonInvalid.Hidden = True
                    PanelActivityAccountInvalid.Hidden = True
                    PanelActivityEntityInvalid.Hidden = True

                    Dim objModuleApproval = UploadReportSTR.getApprovalDetail(IDReq)
                    ModuleName.Text = objModuleApproval.ModuleName
                    ModuleKey.Text = objModuleApproval.ModuleKey
                    Action.Text = UploadReportSTR.GetModuleAction(objModuleApproval.PK_ModuleAction_ID).ModuleActionName
                    CreatedBy.Text = objModuleApproval.CreatedBy
                    CreatedDate.Text = objModuleApproval.CreatedDate.ToString

                    Dim objModuledata As UploadReportSTRData = NawaBLL.Common.Deserialize(objModuleApproval.ModuleField, GetType(UploadReportSTRData))
                    ListReportValid = objModuledata.objlistReport
                    ListIndikatorLaporanValid = objModuledata.objlistIndikatorLaporan
                    ListTransactionBipartyValid = objModuledata.objlistTransactionBiparty
                    ListTransactionMultipartyValid = objModuledata.objlistTransactionMultiparty
                    ListActivityValid = objModuledata.objlistActivity

                    Bindgrid()
                Else
                    Throw New Exception("Invalid Module ID")
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
    '    ObjEODTask = New SiPendarBLL.EODTaskBLL(FormPanelInput)
    'End Sub
#End Region

#Region "Method"

    Protected Sub GridCmdTransactionMultiparty(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadPanelTransactionMultipartyDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridCmdTransactionBiparty(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadPanelTransactionBipartyDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadPanelTransactionBipartyDetail(id As String)
        Try

            Dim item = ListTransactionBipartyValid.Where(Function(x) x.TransactionNumber = id).FirstOrDefault() ' HSBC 20201217 agar data ketika show menjadi benar
            If item Is Nothing Then
                item = ListTransactionBipartyNotValid.Where(Function(x) x.TransactionNumber = id).FirstOrDefault() ' HSBC 20201217 agar data ketika show menjadi benar
            End If
            If item IsNot Nothing Then
                windowDetailTransactionBiparty.Hidden = False
                If Not item.Case_ID Is Nothing Then
                    textCaseID.Text = item.Case_ID
                End If
                If Not item.TransactionNumber Is Nothing Then
                    txtTransactionNumber.Text = item.TransactionNumber
                End If
                If Not item.Internal_Ref_Number Is Nothing Then
                    txtInternal_Ref_Number.Text = item.Internal_Ref_Number
                End If
                If Not item.Transaction_Location Is Nothing Then
                    txtTransaction_Location.Text = item.Transaction_Location
                End If
                If Not item.Transaction_Remark Is Nothing Then
                    txtTransaction_Remark.Text = item.Transaction_Remark
                End If
                If Not item.Date_Transaction Is Nothing Then
                    txtDate_Transaction.Text = item.Date_Transaction
                End If
                If Not item.Teller Is Nothing Then
                    txtTeller.Text = item.Teller
                End If
                If Not item.Authorized Is Nothing Then
                    txtAuthorized.Text = item.Authorized
                End If
                If Not item.Late_Deposit Is Nothing Then
                    txtLate_Deposit.Text = item.Late_Deposit
                End If
                If Not item.Date_Posting Is Nothing Then
                    txtDate_Posting.Text = item.Date_Posting
                End If
                If Not item.Transmode_Code Is Nothing Then
                    If Not item.Transmode_Code = "" Then
                        Dim variable = UploadReportSTR.getTransmodecode(item.Transmode_Code)
                        If variable IsNot Nothing Then
                            txtTransmode_Code.Text = UploadReportSTR.getTransmodecode(item.Transmode_Code).Keterangan
                        End If
                    End If
                End If
                'txtValue_Date.Text = item.Value_Date
                If Not item.Transmode_Comment Is Nothing Then
                    txtTransmode_Comment.Text = item.Transmode_Comment
                End If
                If Not item.Amount_Local Is Nothing Then
                    txtAmount_Local.Text = item.Amount_Local
                End If
                If Not item.isMyClient_FROM Is Nothing Then
                    txtisMyClient_FROM.Text = item.isMyClient_FROM
                End If
                If Not item.From_Funds_Code Is Nothing Then
                    If Not item.From_Funds_Code = "" Then
                        Dim variable = UploadReportSTR.getFundsCode(item.From_Funds_Code)
                        If Not variable Is Nothing Then
                            txtFrom_Funds_Code.Text = UploadReportSTR.getFundsCode(item.From_Funds_Code).Keterangan
                        End If
                    End If
                End If
                If Not item.From_Funds_Comment Is Nothing Then
                    txtFrom_Funds_Comment.Text = item.From_Funds_Comment
                End If
                If Not item.From_Foreign_Currency_Code Is Nothing Then
                    If Not item.From_Foreign_Currency_Code = "" Then
                        Dim variable = UploadReportSTR.getMataUang(item.From_Foreign_Currency_Code)
                        If variable IsNot Nothing Then
                            txtFrom_Foreign_Currency_Code.Text = UploadReportSTR.getMataUang(item.From_Foreign_Currency_Code).Keterangan
                        End If
                    End If
                End If
                If Not item.From_Foreign_Currency_Amount Is Nothing Then
                    txtFrom_Foreign_Currency_Amount.Text = item.From_Foreign_Currency_Amount
                End If

                If Not item.From_Foreign_Currency_Exchange_Rate Is Nothing Then
                    txtFrom_Foreign_Currency_Exchange_Rate.Text = item.From_Foreign_Currency_Exchange_Rate
                End If
                If Not item.From_Country Is Nothing Then
                    If Not item.From_Country = "" Then
                        Dim variable = UploadReportSTR.getNamaNegara(item.From_Country)
                        If variable IsNot Nothing Then

                            txtFrom_Country.Text = UploadReportSTR.getNamaNegara(item.From_Country).Keterangan
                        End If
                    End If
                End If
                If Not item.isMyClient_TO Is Nothing Then
                    txtisMyClient_TO.Text = item.isMyClient_TO
                End If
                If Not item.To_Funds_Code Is Nothing Then
                    If Not item.To_Funds_Code = "" Then
                        Dim variable = UploadReportSTR.getFundsCode(item.To_Funds_Code)
                        If variable IsNot Nothing Then
                            txtTo_Funds_Code.Text = UploadReportSTR.getFundsCode(item.To_Funds_Code).Keterangan
                        End If
                    End If
                End If
                If Not item.From_Funds_Comment Is Nothing Then
                    txtTo_Funds_Comment.Text = item.From_Funds_Comment
                End If
                If Not item.To_Foreign_Currency_Code Is Nothing Then
                    If Not item.To_Foreign_Currency_Code = "" Then
                        Dim variable = UploadReportSTR.getMataUang(item.To_Foreign_Currency_Code)
                        If variable IsNot Nothing Then
                            txtTo_Foreign_Currency_Code.Text = UploadReportSTR.getMataUang(item.To_Foreign_Currency_Code).Keterangan
                        End If
                    End If
                End If
                If Not item.To_Foreign_Currency_Amount Is Nothing Then
                    txtTo_Foreign_Currency_Amount.Text = item.To_Foreign_Currency_Amount
                End If
                If Not item.To_Foreign_Currency_Exchange_Rate Is Nothing Then
                    txtTo_Foreign_Currency_Exchange_Rate.Text = item.To_Foreign_Currency_Exchange_Rate
                End If
                If Not item.To_Country Is Nothing Then
                    If Not item.To_Country = "" Then
                        Dim variable = UploadReportSTR.getNamaNegara(item.To_Country)
                        If variable IsNot Nothing Then
                            txtTo_Country.Text = UploadReportSTR.getNamaNegara(item.To_Country).Keterangan
                        End If
                    End If
                End If
                If Not item.Comments Is Nothing Then
                    txtComments.Text = item.Comments
                End If
                If Not item.isSwift Is Nothing Then
                    txtisSwift.Text = item.isSwift
                End If

                'HSBC 20201117 Fachmi -- penambahan field swift code lawan
                If Not item.Swift_Code_Lawan Is Nothing Then
                    txtSwiftCodeLawan.Text = item.Swift_Code_Lawan
                End If

                If Not item.FK_Source_Data_ID Is Nothing Then
                    If Not item.FK_Source_Data_ID < 1 Then 'HSBC 20201117 Fachmi -- Error code int to "" (blank string)
                        Dim variable = UploadReportSTR.getSourceData(item.FK_Source_Data_ID)
                        txtFK_Source_Data_ID.Text = UploadReportSTR.getSourceData(item.FK_Source_Data_ID).Description
                    End If
                End If


                If Not item.FROM_Account_No Is Nothing Then
                    txtFROM_Account_No.Text = item.FROM_Account_No
                End If
                If Not item.FROM_CIFNO Is Nothing Then
                    txtFROM_CIFNO.Text = item.FROM_CIFNO
                End If
                If Not item.From_WIC_NO Is Nothing Then
                    txtFrom_WIC_NO.Text = item.From_WIC_NO
                End If
                If Not item.TO_ACCOUNT_NO Is Nothing Then
                    txtTO_ACCOUNT_NO.Text = item.TO_ACCOUNT_NO
                End If
                If Not item.TO_CIF_NO Is Nothing Then
                    txtTO_CIF_NO.Text = item.TO_CIF_NO
                End If
                If Not item.TO_WIC_NO Is Nothing Then
                    txtTO_WIC_NO.Text = item.TO_WIC_NO
                End If
                If Not item.Condutor_ID Is Nothing Then
                    txtCondutor_ID.Text = item.Condutor_ID

                    txtIsUsedConductor.Text = "True"
                Else
                    txtIsUsedConductor.Text = "False"
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadPanelTransactionMultipartyDetail(id As Long)
        Try
            Dim item = ListTransactionMultipartyValid.Where(Function(x) x.PK_Transaction_ID = id).FirstOrDefault()
            If item Is Nothing Then
                item = ListTransactionMultipartyNotValid.Where(Function(x) x.PK_Transaction_ID = id).FirstOrDefault()
            End If
            If item IsNot Nothing Then
                windowDetailTransactionMultiparty.Hidden = False
                If Not item.Case_ID Is Nothing Then
                    MultitxtCaseID.Text = item.Case_ID
                End If
                If Not item.TransactionNumber Is Nothing Then
                    txtMultiTransactionNumber.Text = item.TransactionNumber
                End If
                If Not item.Internal_Ref_Number Is Nothing Then
                    txtMultiInternal_Ref_Number.Text = item.Internal_Ref_Number
                End If
                If Not item.Transaction_Location Is Nothing Then
                    txtMultiTransaction_Location.Text = item.Transaction_Location
                End If
                If Not item.Transaction_Remark Is Nothing Then
                    txtMultiTransaction_Remark.Text = item.Transaction_Remark
                End If
                If Not item.Date_Transaction Is Nothing Then
                    txtMultiDate_Transaction.Text = item.Date_Transaction
                End If
                If Not item.Teller Is Nothing Then
                    txtMultiTeller.Text = item.Teller
                End If
                If Not item.Authorized Is Nothing Then
                    txtMultiAuthorized.Text = item.Authorized
                End If
                If Not item.Late_Deposit Is Nothing Then
                    txtMultiLate_Deposit.Text = item.Late_Deposit
                End If
                If Not item.Date_Posting Is Nothing Then
                    txtMultiDate_Posting.Text = item.Date_Posting
                End If
                If Not item.Transmode_Code Is Nothing Then
                    If Not item.Transmode_Code = "" Then
                        Dim variable = UploadReportSTR.getTransmodecode(item.Transmode_Code)
                        If variable IsNot Nothing Then
                            txtMultiTransmode_Code.Text = UploadReportSTR.getTransmodecode(item.Transmode_Code).Keterangan
                        End If
                    End If
                End If
                If Not item.Transmode_Comment Is Nothing Then
                    txtMultiTransmode_Comment.Text = item.Transmode_Comment
                End If
                If Not item.Amount_Local Is Nothing Then
                    txtMultiAmount_Local.Text = item.Amount_Local
                End If
                If Not item.isMyClient Is Nothing Then
                    txtMultiisMyClient.Text = item.isMyClient
                End If
                If Not item.Funds_Code Is Nothing Then
                    If Not item.Funds_Code = "" Then
                        Dim variable = UploadReportSTR.getFundsCode(item.Funds_Code)
                        If variable IsNot Nothing Then
                            txtMultiFunds_Code.Text = UploadReportSTR.getFundsCode(item.Funds_Code).Keterangan
                        End If
                    End If
                End If
                If Not item.Funds_Comment Is Nothing Then
                    txtMultiFunds_Comment.Text = item.Funds_Comment
                End If
                If Not item.Foreign_Currency_Code Is Nothing Then
                    If Not item.Foreign_Currency_Code = "" Then
                        Dim variable = UploadReportSTR.getMataUang(item.Foreign_Currency_Code)
                        If variable IsNot Nothing Then
                            txtMultiForeign_Currency_Code.Text = UploadReportSTR.getMataUang(item.Foreign_Currency_Code).Keterangan
                        End If
                    End If
                End If
                If Not item.Foreign_Currency_Amount Is Nothing Then
                    txtMultiForeign_Currency_Amount.Text = item.Foreign_Currency_Amount
                End If
                If Not item.Foreign_Currency_Exchange_Rate Is Nothing Then
                    txtMultiForeign_Currency_Exchange_Rate.Text = item.Foreign_Currency_Exchange_Rate
                End If
                If Not item.Country Is Nothing Then
                    If Not item.Country = "" Then
                        Dim variable = UploadReportSTR.getNamaNegara(item.Country)
                        If variable IsNot Nothing Then
                            txtMultiCountry.Text = UploadReportSTR.getNamaNegara(item.Country).Keterangan
                        End If
                    End If
                End If
                If Not item.Comments Is Nothing Then
                    txtMultiComments.Text = item.Comments
                End If
                If Not item.isSwift Is Nothing Then
                    txtMultiisSwift.Text = item.isSwift
                End If


                If Not item.FK_Source_Data_ID Is Nothing Then
                    If Not item.FK_Source_Data_ID < 1 Then 'HSBC 20201130 Firman -- Error code int to "" (blank string)
                        Dim variable = UploadReportSTR.getSourceData(item.FK_Source_Data_ID)
                        txtMultiFK_Source_Data_ID.Text = UploadReportSTR.getSourceData(item.FK_Source_Data_ID).Description
                    End If
                End If

                If Not item.Account_No Is Nothing Then
                    txtMultiAccount_No.Text = item.Account_No
                End If
                If Not item.CIFNO Is Nothing Then
                    txtMultiCIFNO.Text = item.CIFNO
                End If
                If Not item.WIC_NO Is Nothing Then
                    txtMultiWIC_NO.Text = item.WIC_NO
                End If
                If Not item.Condutor_ID Is Nothing Then
                    txtMultiCondutor_ID.Text = item.IsUsedConductor
                    txtMultiIsUsedConductor.Text = "True"
                Else
                    txtMultiIsUsedConductor.Text = "False"
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Function IsDataValid() As Boolean
        If RemarkReportSTR.Text = "" Then
            Throw New ApplicationException("Remark is required")
        End If
        Return True
    End Function


    Protected Sub BtnSave_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            IsDataValid()
            Dim stringwithsimilargcn As String = ""
            For Each item As SIPENDAR_ReportSTR In ListReportValid
                If item.Case_ID IsNot Nothing AndAlso item.Tanggal_Laporan IsNot Nothing Then
                    If SIPENDARGenerateBLL.CheckReportByGCNandSubmission(item.Case_ID, item.Tanggal_Laporan) Then
                        If String.IsNullOrEmpty(stringwithsimilargcn) Then
                            stringwithsimilargcn = item.Case_ID
                        Else
                            stringwithsimilargcn = stringwithsimilargcn & ", " & item.Case_ID
                        End If
                    End If
                End If
            Next
            If Not String.IsNullOrEmpty(stringwithsimilargcn) Then
                Throw New ApplicationException("Transaksi dengan GCN : " & stringwithsimilargcn & " pada Tanggal yang di Laporkan sudah ada.")
            End If
            SiPendarBLL.UploadReportSTR.Accept(Me.IDReq, RemarkReportSTR.Value)

            LblConfirmation.Text = "Data Approved. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True


        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub BtnReject_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            IsDataValid()
            SiPendarBLL.UploadReportSTR.Reject(IDReq, RemarkReportSTR.Value)

            LblConfirmation.Text = "Data Rejected. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True


        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub


    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub BtnCancel_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Sub GridPanelSetting()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 2 Then
            GridPanelTransactionBiParty.ColumnModel.Columns.RemoveAt(GridPanelTransactionBiParty.ColumnModel.Columns.Count - 1)
            GridPanelTransactionBiParty.ColumnModel.Columns.Insert(1, cmdbipartyvalid)
            GridPanelTransactionBipartyInvalid.ColumnModel.Columns.RemoveAt(GridPanelTransactionBipartyInvalid.ColumnModel.Columns.Count - 1)
            GridPanelTransactionBipartyInvalid.ColumnModel.Columns.Insert(1, cmdBipartyInvalid)
            GridPanelTransactionMultiParty.ColumnModel.Columns.RemoveAt(GridPanelTransactionMultiParty.ColumnModel.Columns.Count - 1)
            GridPanelTransactionMultiParty.ColumnModel.Columns.Insert(1, cmdmultipartyvalid)
            GridPanelTransactionMultiPartyInvalid.ColumnModel.Columns.RemoveAt(GridPanelTransactionMultiPartyInvalid.ColumnModel.Columns.Count - 1)
            GridPanelTransactionMultiPartyInvalid.ColumnModel.Columns.Insert(1, cmdMultipartyinValid)

        End If

    End Sub


    Sub Paging()

        StoreReportData.PageSize = SystemParameterBLL.GetPageSize
        StoreReportInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreIndikatorLaporanData.PageSize = SystemParameterBLL.GetPageSize
        StoreIndikatorLaporanInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreTransactionBiParty.PageSize = SystemParameterBLL.GetPageSize
        StoreTransactionBipartyInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreTransactionMultiParty.PageSize = SystemParameterBLL.GetPageSize
        StoreTransactionMultiPartyInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityAccount.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityAccountInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityEntity.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityEntityInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityPerson.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityPersonInvalid.PageSize = SystemParameterBLL.GetPageSize

    End Sub


#End Region

End Class