﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="SIPENDAR_GenerateDetail.aspx.vb" Inherits="goAML_GenerateSARDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .my-style .x-form-display-field-default {
            color: #FF0000;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:Window ID="WindowAttachment" Layout="AnchorLayout" Title="Dokumen" runat="server" Modal="true" Hidden="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:FileUploadField ID="FileDoc" runat="server" FieldLabel="File Document" AnchorHorizontal="100%">
            </ext:FileUploadField>
            <ext:DisplayField ID="txtFileName" runat="server" FieldLabel="File Name" AnchorHorizontal="100%">
            </ext:DisplayField>
            <ext:TextArea ID="txtKeterangan" runat="server" FieldLabel="Description" AnchorHorizontal="100%">
            </ext:TextArea>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.4, height: size.height * 0.4});" />

            <Resize Handler="#{WindowAttachment}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="BtnsaveAttachment" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">

                <DirectEvents>
                    <Click OnEvent="btnSaveAttachment_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancelAttachment" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancelAttachment_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:FormPanel ID="FormPanelInput" runat="server" Layout="AnchorLayout" ButtonAlign="Center" Title="Generate STR SAR" BodyStyle="padding:20px" AutoScroll="true" Hidden="false">
        <Items>
            <ext:DisplayField ID="sar_PK" runat="server" FieldLabel="ID" AnchorHorizontal="100%" />
            <ext:DisplayField ID="sar_cif" runat="server" FieldLabel="CIF No" AnchorHorizontal="70%" />
            <ext:DisplayField ID="sar_laporan" runat="server" FieldLabel="Laporan" AnchorHorizontal="70%" />
            <ext:DisplayField ID="sar_TanggalLaporan" runat="server" FieldLabel="Tanggal Laporan" AnchorHorizontal="70%" />
            <ext:DisplayField ID="NoRefPPATK" runat="server" FieldLabel="No Ref PPATK" AnchorHorizontal="70%" />
            <ext:DisplayField ID="Alasan" runat="server" FieldLabel="Alasan" AnchorHorizontal="70%" />
            <ext:Panel runat="server" ID="Panel2" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaction Data" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                <Items>
                    <ext:GridPanel ID="GridPaneldetail" runat="server" AutoScroll="true" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreTransaction" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model ID="ModelDetail" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="NO_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="NoTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NoRefTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TipeTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LokasiTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KeteranganTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DateTransaction" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="AccountNo" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NamaTeller" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NamaPejabat" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TglPembukuan" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="CaraTranDilakukan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CaraTranLain" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DebitCredit" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="OriginalAmount" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDR" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn28" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="colStoreProcedure" runat="server" DataIndex="NoTran" Text="Nomor Transaksi" Width="150"></ext:Column>
                                <ext:Column ID="colIsuseProcessDate" runat="server" DataIndex="NoRefTran" Text="No Ref Transaksi" Width="150"></ext:Column>
                                <ext:Column ID="colTipeTran" runat="server" DataIndex="TipeTran" Text="Tipe Transaksi" Width="150"></ext:Column>
                                <ext:Column runat="server" DataIndex="LokasiTran" Text="Lokasi Transaksi" Width="150"></ext:Column>
                                <ext:Column ID="Colorder" runat="server" DataIndex="KeteranganTran" Text="Keterangan Transaksi" Width="170"></ext:Column>
                                <ext:DateColumn ID="Column2" runat="server" DataIndex="DateTransaction" Text="Tanggal Transaksi" Width="150">
                                    <Filter>
                                        <ext:DateFilter />
                                    </Filter>
                                </ext:DateColumn>
                                <ext:Column ID="colAccountNo" runat="server" DataIndex="AccountNo" Text="No Akun" Width="150"></ext:Column>
                                <ext:Column runat="server" DataIndex="NamaTeller" Text="Nama Teller" Width="120"></ext:Column>
                                <ext:Column ID="Column1" runat="server" DataIndex="NamaPejabat" Text="Nama Pejabat" Width="120"></ext:Column>
                                <ext:DateColumn ID="Column3" runat="server" DataIndex="TglPembukuan" Text="Tanggal Pembukuan" Width="160"></ext:DateColumn>
                                <ext:Column runat="server" DataIndex="CaraTranDilakukan" Text="Cara Transaksi Dilakukan" Width="185"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="CaraTranLain" Text="Cara Transaksi Lain" Width="150"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="DebitCredit" Text="Debit Credit" Width="150"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="OriginalAmount" Text="Original Amount" Width="150"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="IDR" Text="Nilai Transaksi(IDR)" Width="150"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server">
                            </ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            <ext:Panel runat="server" ID="PanelReportIndicator" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Indikator" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                <Items>
                    <ext:GridPanel ID="GridPanelReportIndicator" runat="server" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreReportIndicatorData" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model355" IDProperty="PK_Report_Indicator">
                                        <Fields>
                                            <ext:ModelField Name="PK_Report_Indicator" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Indicator" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn88" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column312" runat="server" DataIndex="FK_Indicator" Text="Kode" Flex="1"></ext:Column>
                                <ext:Column ID="Column313" runat="server" DataIndex="Keterangan" Text="Keterangan" Flex="2"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            <ext:Panel runat="server" ID="PanelAttachment" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Attachment Document" BodyStyle="padding:10px" Margin="10" Collapsible="true" Hidden="true">
                <Items>
                    <ext:GridPanel ID="GridPanelAttachment" runat="server" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        
                        <Store>
                            <ext:Store ID="StoreAttachment" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model1" IDProperty="PK_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FileName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column13" runat="server" DataIndex="FileName" Text="File Name" Flex="1">
                                    <Commands>
                                         <ext:ImageCommand Icon="DiskDownload" CommandName="Download" ToolTip-Text="Download Dokumen"></ext:ImageCommand>         
                                     </Commands>
                                     <DirectEvents>
                                         <Command OnEvent="GridcommandAttachment">
                                              <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                         </Command>
                                     </DirectEvents>
                                </ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="Keterangan" Text="Keterangan" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAttachment" runat="server" Text="Action" Flex="1" Hidden="true">

                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>

                                        <Command OnEvent="GridcommandAttachment">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>

                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
        </Items>
        <Items>
            <ext:DisplayField ID="DisplayField1" runat="server" AnchorHorizontal="70%" />
            <ext:Panel runat="server" ID="panelApprovalHistory" ClientIDMode="Static" Title="Approval" Layout="AnchorLayout" Border="true" Collapsible="true" BodyStyle="padding:10px" ComponentCls="xPanelContainer" Hidden="false">
                <Items>
                    <ext:GridPanel ID="gp_history" runat="server" MarginSpec="0 0 20 0" Title="Approval History" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="gpStore_history" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" IDProperty="PK_TX_IFTI_Flow_History_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_TX_IFTI_Flow_History_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Module_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UnikID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="UserID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UserName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Notes" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>

                        <ColumnModel runat="server">
                            <Columns>
                                <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="70"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="User Name" DataIndex="UserName" CellWrap="true" Flex="1" />
                                <ext:Column runat="server" Text="Status" DataIndex="Status" CellWrap="true" Flex="1" />
                                <ext:DateColumn runat="server" Text="Created Date" DataIndex="CreatedDate" CellWrap="true" Flex="1" Format="dd-MMM-yyyy hh:mm:ss" />
                                <ext:Column runat="server" Text="Notes" DataIndex="Notes" CellWrap="true" Flex="4" />
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar24" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="BtnCancel" runat="server" Text="Cancel" Icon="PageBack">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
