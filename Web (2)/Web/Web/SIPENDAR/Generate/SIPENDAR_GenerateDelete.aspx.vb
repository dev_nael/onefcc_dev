﻿Imports System.Data
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports SiPendarDAL
Imports NawaBLL
Imports Ext
Imports SiPendarBLL
Imports System.Data.SqlClient
Imports NawaDAL
Imports Elmah
Partial Class SIPENDAR_GenerateDelete
    Inherits Parent
#Region "Session"
    Private _IDReq As Long
    Public Property IDReq() As Long
        Get
            Return _IDReq
        End Get
        Set(ByVal value As Long)
            _IDReq = value
        End Set
    End Property
    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("SIPENDAR_GenerateDelete.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("SIPENDAR_GenerateDelete.ObjModule") = value
        End Set
    End Property
    Public Property ObjGenerateSAR As SiPendarDAL.SIPENDAR_ODM_Generate_Report
        Get
            Return Session("SIPENDAR_GenerateDelete.ObjGenerateSAR")
        End Get
        Set(ByVal value As SiPendarDAL.SIPENDAR_ODM_Generate_Report)
            Session("SIPENDAR_GenerateDelete.ObjGenerateSAR") = value
        End Set
    End Property
    Public Property ObjSARData As SiPendarBLL.SIPENDARGenerateData
        Get
            Return Session("SIPENDAR_GenerateDelete.ObjSARData")
        End Get
        Set(ByVal value As SiPendarBLL.SIPENDARGenerateData)
            Session("SIPENDAR_GenerateDelete.ObjSARData") = value
        End Set
    End Property
    Public Property ListIndicator As List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
        Get
            Return Session("SIPENDAR_GenerateDelete.ListIndicator")
        End Get
        Set(value As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
            Session("SIPENDAR_GenerateDelete.ListIndicator") = value
        End Set
    End Property
    Public Property ListDokumen As List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment)
        Get
            Return Session("SIPENDAR_GenerateDelete.ListDokumen")
        End Get
        Set(value As List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment))
            Session("SIPENDAR_GenerateDelete.ListDokumen") = value
        End Set
    End Property
    Public Property IDDokumen As String
        Get
            Return Session("SIPENDAR_GenerateDelete.IDDokumen")
        End Get
        Set(value As String)
            Session("SIPENDAR_GenerateDelete.IDDokumen") = value
        End Set
    End Property
#End Region

#Region "Load Data"
    Private Sub SIPENDAR_GenerateSARDelete_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer

            intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)


            If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Delete) Then
                Dim strIDCode As String = 1
                strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                Exit Sub
            End If
            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = ObjModule.ModuleLabel & " Delete"
                clearSession()
                LoadData()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadApprovalHistory(modulId As String, sarId As String)
        Try
            Dim objApprovalHistory As New List(Of goAML_Module_Note_History)
            objApprovalHistory = SiPendarBLL.SIPENDARGenerateBLL.getListApprovalHistory(modulId, sarId)
            If objApprovalHistory.Count > 0 Then
                gpStore_history.DataSource = objApprovalHistory
                gpStore_history.DataBind()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadData()
        Dim IDData As String = Request.Params("ID")
        Dim Arrstr() As String
        Dim num As Integer = 1

        IDReq = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)

        ObjSARData.objGenerateSAR = SiPendarBLL.SIPENDARGenerateBLL.getGenerateSARByPk(IDReq)
        ObjSARData.listObjGenerateSARTransaction = SiPendarBLL.SIPENDARGenerateBLL.getListSarTransactionByFk(IDReq)
        ObjSARData.listObjDokumen = SiPendarBLL.SIPENDARGenerateBLL.getListGenerateSARDokumen(IDReq)
        bindTransaction(StoreTransaction, ObjSARData.listObjGenerateSARTransaction)
        bindAttachment(StoreAttachment, ObjSARData.listObjDokumen)

        With ObjSARData.objGenerateSAR
            sar_PK.Text = .PK_goAML_ODM_Generate_STR_SAR
            sar_cif.Text = .CIF_NO + " - " + SIPENDARGenerateBLL.getCustomerNameByCif(.CIF_NO)
            sar_laporan.Text = SiPendarBLL.GlobalReportFunctionBLL.getJenisLaporanByCode(.Transaction_Code)
            If .Date_Report IsNot Nothing Then
                sar_TanggalLaporan.Text = .Date_Report.Value.ToString("dd-MMM-yyyy")
            End If
            NoRefPPATK.Text = .Fiu_Ref_Number
            Alasan.Text = .Reason
            If .indicator IsNot Nothing Then
                Arrstr = .indicator.Split(",")
                For Each item In Arrstr
                    Dim Indikator As New SiPendarDAL.SIPENDAR_Report_Indicator
                    Indikator.PK_Report_Indicator = num
                    Indikator.FK_Indicator = item
                    ListIndicator.Add(Indikator)
                    num += 1
                Next
                bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            End If
        End With
        LoadApprovalHistory(ObjModule.PK_Module_ID, IDReq)
    End Sub
    Sub bindAttachment(store As Ext.Net.Store, listAttachment As List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAttachment)
        objtable.Columns.Add(New DataColumn("FileName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("FileName") = item("File_Name")
                item("Keterangan") = item("Remarks")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindTransaction(store As Ext.Net.Store, listTransaction As List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Transaction))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listTransaction)
        objtable.Columns.Add(New DataColumn("NoTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoRefTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("TipeTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("LokasiTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("KeteranganTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("DateTransaction", GetType(Date)))
        objtable.Columns.Add(New DataColumn("AccountNo", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaTeller", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaPejabat", GetType(String)))
        objtable.Columns.Add(New DataColumn("TglPembukuan", GetType(Date)))
        objtable.Columns.Add(New DataColumn("CaraTranDilakukan", GetType(String)))
        objtable.Columns.Add(New DataColumn("CaraTranLain", GetType(String)))
        objtable.Columns.Add(New DataColumn("DebitCredit", GetType(String)))
        objtable.Columns.Add(New DataColumn("OriginalAmount", GetType(String)))
        objtable.Columns.Add(New DataColumn("IDR", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("NoTran") = item("Transaction_Number")
                item("NoRefTran") = item("Ref_Num")
                Dim SourceData As String = item("Source_Data").ToString
                Dim SourceDataTemp As Integer
                If Integer.TryParse(SourceData, SourceDataTemp) Then
                    If SourceData <> "" Then
                        item("TipeTran") = SiPendarBLL.SIPENDARGenerateBLL.getSourceData(SourceData)
                    End If
                Else
                    item("TipeTran") = SourceData
                End If
                item("LokasiTran") = item("Transaction_Location")
                item("KeteranganTran") = item("Transaction_Remark")
                item("DateTransaction") = item("Date_Transaction")
                item("AccountNo") = item("Account_NO")
                item("NamaTeller") = item("Teller")
                item("NamaPejabat") = item("Authorized")
                item("TglPembukuan") = item("Date_Posting")
                item("CaraTranDilakukan") = item("Transmode_Code")
                item("CaraTranLain") = item("Transmode_Comment")
                item("DebitCredit") = item("Debit_Credit")
                If item("Original_Amount").ToString <> "" Then
                    Dim OriginalAmount As Decimal = item("Original_Amount").ToString
                    item("OriginalAmount") = OriginalAmount.ToString("#,###.00")
                End If
                If item("IDR_Amount").ToString <> "" Then
                    Dim IDR As Decimal = item("IDR_Amount").ToString
                    item("IDR") = IDR.ToString("#,###.00")
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindReportIndicator(store As Ext.Net.Store, listAddresss As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        '  objtable.Columns.Add(New DataColumn("Kode_Indicator", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                '     item("Kode_Indicator") = item("FK_Indicator")
                item("Keterangan") = SiPendarBLL.SIPENDARGenerateBLL.getReportIndicatorByKode(item("FK_Indicator").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
#End Region

#Region "Method"
    Sub clearSession()
        ObjGenerateSAR = New SiPendarDAL.SIPENDAR_ODM_Generate_Report
        ObjSARData = New SiPendarBLL.SIPENDARGenerateData
        ListIndicator = New List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
        ListDokumen = New List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment)
    End Sub
#End Region

#Region "Direct Event"
    Protected Sub BtnDelete_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim listApproval As New List(Of SiPendarDAL.ModuleApproval)
            listApproval = SiPendarBLL.SIPENDARGenerateBLL.getListModuleApproval(ObjModule.ModuleName, ObjSARData.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR).ToList

            If listApproval.Count > 0 Then
                Throw New ApplicationException("There is Already Pending Approval Please Confirm Approval First Before Do Another Change For This Data")
            End If

            If Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                'Dim path As String = ConfigurationManager.AppSettings("FilePathGenerateSARAttachment").ToString
                'Dim path As String = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(9018).SettingValue
                'SiPendarBLL.SIPENDARGenerateBLL.SaveGenerateSarTanpaApproval(ObjSARData, ObjModule, 3, path)

                Dim DateFrom As New DateTime
                Dim DateTo As New DateTime

                SiPendarBLL.SIPENDARGenerateBLL.SaveGenerateSTRSarTanpaApproval(ObjSARData, ObjModule, 3, DateFrom, DateTo)
                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Saved into Database."
            Else
                'SiPendarBLL.SIPENDARGenerateBLL.SaveGenerateSarApproval(ObjSARData, ObjModule, 3)

                SiPendarBLL.SIPENDARGenerateBLL.SaveGenerateSTRSarApproval(ObjSARData, ObjModule, 3)
                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Saved into Pending Approval"
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Download" Then
                DownloadFile(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DownloadFile(id As Long)
        Dim objdownload As SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment = ObjSARData.listObjDokumen.Find(Function(x) x.PK_ID = id)
        If Not objdownload Is Nothing Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & objdownload.File_Name)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "ContentType"
            Response.BinaryWrite(objdownload.File_Doc)
            Response.End()
        End If
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub btnSaveAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            If Not FileDoc.HasFile Then
                Throw New Exception("Please Upload File")
            End If

            Dim Dokumen As New SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment
            If IDDokumen IsNot Nothing Or IDDokumen <> "" Then
                ListDokumen.Remove(ListDokumen.Where(Function(x) x.PK_ID = IDDokumen).FirstOrDefault)
            End If

            If ListDokumen.Count = 0 Then
                Dokumen.PK_ID = -1
            ElseIf ListIndicator.Count >= 1 Then
                Dokumen.PK_ID = ListDokumen.Min(Function(x) x.PK_ID) - 1
            End If

            Dokumen.File_Name = IO.Path.GetFileName(FileDoc.FileName)
            Dokumen.File_Doc = FileDoc.FileBytes
            Dokumen.Remarks = txtKeterangan.Text.Trim

            Dokumen.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            Dokumen.CreatedDate = DateTime.Now

            ListDokumen.Add(Dokumen)

            bindAttachment(StoreAttachment, ListDokumen)
            WindowAttachment.Hidden = True
            IDDokumen = Nothing

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAttachment.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class