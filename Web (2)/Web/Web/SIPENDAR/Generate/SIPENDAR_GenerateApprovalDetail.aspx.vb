﻿Imports Ext
Imports System.Data
Imports NawaBLL
Imports SiPendarDAL
Imports SiPendarBLL
Imports SiPendarBLL.SIPENDARGenerateBLL
Imports Microsoft.VisualBasic.CompilerServices
Imports NawaBLL.Nawa
Imports NawaDAL
Imports System.Data.Entity
Imports System.Data.SqlClient

Partial Class SIPENDAR_GenerateApprovalDetail
    Inherits Parent
    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("SIPENDAR_GenerateApprovalDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("SIPENDAR_GenerateApprovalDetail.ObjModule") = value
        End Set
    End Property
    Private _IDReq As Long
    Public Property IDReq() As Long
        Get
            Return _IDReq
        End Get
        Set(ByVal value As Long)
            _IDReq = value
        End Set
    End Property

    Private _objApproval As NawaDAL.ModuleApproval
    Private _objGenerateSarNew As SiPendarDAL.SIPENDAR_ODM_Generate_Report
    Private _objGenerateSarOld As SiPendarDAL.SIPENDAR_ODM_Generate_Report
    Public Property ObjApproval() As NawaDAL.ModuleApproval
        Get
            Return _objApproval
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            _objApproval = value
        End Set
    End Property
    Public Property objGenerateSarNew() As SiPendarBLL.SIPENDARGenerateData
        Get
            Return Session("SIPENDAR_GenerateApprovalDetail.objGenerateSarNew")
        End Get
        Set(value As SiPendarBLL.SIPENDARGenerateData)
            Session("SIPENDAR_GenerateApprovalDetail.objGenerateSarNew") = value
        End Set
    End Property
    Public Property objGenerateSarOld() As SiPendarBLL.SIPENDARGenerateData
        Get
            Return Session("SIPENDAR_GenerateApprovalDetail.objGenerateSarOld")
        End Get
        Set(value As SiPendarBLL.SIPENDARGenerateData)
            Session("SIPENDAR_GenerateApprovalDetail.objGenerateSarOld") = value
        End Set
    End Property
    Public Property ListIndicatorNew As List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
        Get
            Return Session("SIPENDAR_GenerateApprovalDetail.ListIndicatorNew")
        End Get
        Set(value As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
            Session("SIPENDAR_GenerateApprovalDetail.ListIndicatorNew") = value
        End Set
    End Property
    Public Property ListIndicatorOld As List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
        Get
            Return Session("SIPENDAR_GenerateApprovalDetail.ListIndicatorOld")
        End Get
        Set(value As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
            Session("SIPENDAR_GenerateApprovalDetail.ListIndicatorOld") = value
        End Set
    End Property

    Public Property IsUseFilterFromGlobal As String
        Get
            Return Session("SIPENDAR_GenerateApprovalDetail.IsUseFilterFromGlobal")
        End Get
        Set(value As String)
            Session("SIPENDAR_GenerateApprovalDetail.IsUseFilterFromGlobal") = value
        End Set
    End Property

    Private Sub SIPENDAR_ReportApprovalDetailt_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not NawaBLL.Common.SessionCurrentUser Is Nothing Then
                TextAreaReasonApproval.FieldStyle = "background-color: #FFE4C4"
                Dim strid As String = Request.Params("ID")
                Dim strModuleid As String = Request.Params("ModuleID")
                Try

                    IDReq = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Catch ex As Exception
                    Throw New Exception("Invalid ID Approval.")
                End Try

                ObjApproval = NawaBLL.ModuleApprovalBLL.GetModuleApprovalByID(IDReq)

                If Not Ext.Net.X.IsAjaxRequest Then
                    clearSession()

                    If Not ObjApproval Is Nothing Then


                        ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleName(ObjApproval.ModuleName)
                        Dim strmodulelabel As String = ""
                        If Not ObjModule Is Nothing Then
                            strmodulelabel = ObjModule.ModuleLabel
                        End If
                        PanelInfo.Title = strmodulelabel & " Approval Detail"

                        lblModuleName.Text = strmodulelabel
                        lblModuleKey.Text = ObjApproval.ModuleKey
                        lblAction.Text = NawaBLL.ModuleBLL.GetModuleActionNamebyID(ObjApproval.PK_ModuleAction_ID)
                        LblCreatedBy.Text = ObjApproval.CreatedBy
                        lblCreatedDate.Text = ObjApproval.CreatedDate.Value.ToString("dd-MMM-yyyy")

                        BtnSave.Visible = ObjApproval.CreatedBy <> NawaBLL.Common.SessionCurrentUser.UserID
                        BtnReject.Visible = ObjApproval.CreatedBy <> NawaBLL.Common.SessionCurrentUser.UserID

                    End If

                    Using objdb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
                        Dim objglobalparam As SIPENDAR_GLOBAL_PARAMETER = objdb.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 101).FirstOrDefault
                        If objglobalparam IsNot Nothing AndAlso objglobalparam.ParameterValue IsNot Nothing Then
                            IsUseFilterFromGlobal = objglobalparam.ParameterValue
                        End If
                    End Using

                    Dim Arrstr() As String
                    Dim num As Integer = 1

                    Select Case ObjApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert

                            objGenerateSarNew = NawaBLL.Common.Deserialize(ObjApproval.ModuleField, GetType(SiPendarBLL.SIPENDARGenerateData))

                            With objGenerateSarNew.objGenerateSAR
                                sar_PK.Text = .PK_goAML_ODM_Generate_STR_SAR
                                sar_cif.Text = .CIF_NO
                                sar_laporan.Text = SiPendarBLL.GlobalReportFunctionBLL.getJenisLaporanByCode(.Transaction_Code)
                                If .Date_Report IsNot Nothing Then
                                    sar_TanggalLaporan.Text = .Date_Report.Value.ToString("dd-MMM-yyyy")
                                End If
                                NoRefPPATK.Text = .Fiu_Ref_Number
                                Alasan.Text = .Reason
                                If .indicator IsNot Nothing Then
                                    Arrstr = .indicator.Split(",")
                                    For Each item In Arrstr
                                        Dim Indikator As New SiPendarDAL.SIPENDAR_Report_Indicator
                                        Indikator.PK_Report_Indicator = num
                                        'Indikator.FK_Indicator = SiPendarBLL.SIPENDARGenerateBLL.getIndicatorKode(item)
                                        Indikator.FK_Indicator = item
                                        ListIndicatorNew.Add(Indikator)
                                        num += 1
                                    Next
                                    bindReportIndicator(StoreReportIndicatorData, ListIndicatorNew)
                                End If

                                If objGenerateSarNew.objGenerateSAR.IsSelectedAll Then
                                    Panel5.Hidden = False
                                    Panel2.Hidden = True

                                    Dim listSarTransaction As New List(Of goAML_ODM_Transaksi)
                                    Dim DateFrom As DateTime
                                    Dim DateTo As DateTime

                                    For Each item In objGenerateSarNew.listObjGenerateSARTransaction
                                        DateFrom = item.Date_Transaction
                                        DateTo = item.Date_Posting
                                    Next
                                    If IsUseFilterFromGlobal IsNot Nothing AndAlso IsUseFilterFromGlobal = "1" Then
                                        listSarTransaction = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch_GCN_WithFilter(objGenerateSarNew.objGenerateSAR.CIF_NO, DateFrom, DateTo)
                                    Else
                                        listSarTransaction = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch_GCN(objGenerateSarNew.objGenerateSAR.CIF_NO, DateFrom, DateTo)
                                    End If

                                    If listSarTransaction.Count > 0 Then
                                        bindOdmTransaksi(StoreTransactionOdm, listSarTransaction)
                                    End If
                                Else
                                    Panel5.Hidden = True
                                    Panel2.Hidden = False
                                    If objGenerateSarNew.listObjGenerateSARTransaction.Count > 0 Then
                                        bindTransaction(StoreTransaction, objGenerateSarNew.listObjGenerateSARTransaction)
                                    End If
                                End If
                                If objGenerateSarNew.listObjDokumen.Count > 0 Then
                                    bindAttachment(StoreAttachment, objGenerateSarNew.listObjDokumen)
                                End If
                            End With

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            FormPanelOld.Hidden = False

                            objGenerateSarNew = NawaBLL.Common.Deserialize(ObjApproval.ModuleField, GetType(SiPendarBLL.SIPENDARGenerateData))
                            objGenerateSarOld = NawaBLL.Common.Deserialize(ObjApproval.ModuleFieldBefore, GetType(SiPendarBLL.SIPENDARGenerateData))

                            With objGenerateSarNew.objGenerateSAR
                                sar_PK.Text = .PK_goAML_ODM_Generate_STR_SAR
                                sar_cif.Text = .CIF_NO
                                sar_laporan.Text = SiPendarBLL.GlobalReportFunctionBLL.getJenisLaporanByCode(.Transaction_Code)
                                If .Date_Report IsNot Nothing Then
                                    sar_TanggalLaporan.Text = .Date_Report.Value.ToString("dd-MMM-yyyy")
                                End If
                                NoRefPPATK.Text = .Fiu_Ref_Number
                                Alasan.Text = .Reason
                                If .indicator IsNot Nothing Then
                                    Arrstr = .indicator.Split(",")
                                    For Each item In Arrstr
                                        Dim Indikator As New SiPendarDAL.SIPENDAR_Report_Indicator
                                        Indikator.PK_Report_Indicator = num
                                        'Indikator.FK_Indicator = SiPendarBLL.SIPENDARGenerateBLL.getIndicatorKode(item)
                                        Indikator.FK_Indicator = item
                                        ListIndicatorNew.Add(Indikator)
                                        num += 1
                                    Next
                                    bindReportIndicator(StoreReportIndicatorData, ListIndicatorNew)
                                End If
                                bindTransaction(StoreTransaction, objGenerateSarNew.listObjGenerateSARTransaction)
                                If objGenerateSarNew.listObjDokumen.Count > 0 Then
                                    bindAttachment(StoreAttachment, objGenerateSarNew.listObjDokumen)
                                End If
                            End With

                            With objGenerateSarOld.objGenerateSAR
                                sar_PkOld.Text = .PK_goAML_ODM_Generate_STR_SAR
                                sar_cifOld.Text = .CIF_NO
                                sar_laporanOld.Text = SiPendarBLL.GlobalReportFunctionBLL.getJenisLaporanByCode(.Transaction_Code)
                                If .Date_Report IsNot Nothing Then
                                    sar_TanggalLaporanOld.Text = .Date_Report.Value.ToString("dd-MMM-yyyy")
                                End If
                                NoRefPPATKOld.Text = .Fiu_Ref_Number
                                AlasanOld.Text = .Reason
                                If .indicator IsNot Nothing Then
                                    Arrstr = .indicator.Split(",")
                                    For Each item In Arrstr
                                        Dim Indikator As New SiPendarDAL.SIPENDAR_Report_Indicator
                                        Indikator.PK_Report_Indicator = num
                                        'Indikator.FK_Indicator = SiPendarBLL.SIPENDARGenerateBLL.getIndicatorKode(item)
                                        Indikator.FK_Indicator = item
                                        ListIndicatorOld.Add(Indikator)
                                        num += 1
                                    Next
                                    bindReportIndicator(StoreIndicatorOld, ListIndicatorOld)
                                End If
                                bindTransaction(StoreTransactionOld, objGenerateSarOld.listObjGenerateSARTransaction)
                                If objGenerateSarOld.listObjDokumen.Count > 0 Then
                                    bindAttachment(StoreAttachmentOld, objGenerateSarOld.listObjDokumen)
                                End If
                            End With
                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            objGenerateSarNew = NawaBLL.Common.Deserialize(ObjApproval.ModuleField, GetType(SiPendarBLL.SIPENDARGenerateData))

                            With objGenerateSarNew.objGenerateSAR
                                sar_PK.Text = .PK_goAML_ODM_Generate_STR_SAR
                                sar_cif.Text = .CIF_NO
                                sar_laporan.Text = SiPendarBLL.GlobalReportFunctionBLL.getJenisLaporanByCode(.Transaction_Code)
                                If .Date_Report IsNot Nothing Then
                                    sar_TanggalLaporan.Text = .Date_Report.Value.ToString("dd-MMM-yyyy")
                                End If
                                NoRefPPATK.Text = .Fiu_Ref_Number
                                Alasan.Text = .Reason
                                If .indicator IsNot Nothing Then
                                    Arrstr = .indicator.Split(",")
                                    For Each item In Arrstr
                                        Dim Indikator As New SiPendarDAL.SIPENDAR_Report_Indicator
                                        Indikator.PK_Report_Indicator = num
                                        'Indikator.FK_Indicator = SiPendarBLL.SIPENDARGenerateBLL.getIndicatorKode(item)
                                        Indikator.FK_Indicator = item
                                        ListIndicatorNew.Add(Indikator)
                                        num += 1
                                    Next
                                    bindReportIndicator(StoreReportIndicatorData, ListIndicatorNew)
                                End If
                                bindTransaction(StoreTransaction, objGenerateSarNew.listObjGenerateSARTransaction)
                                If objGenerateSarNew.listObjDokumen.Count > 0 Then
                                    bindAttachment(StoreAttachment, objGenerateSarNew.listObjDokumen)
                                End If
                            End With
                    End Select

                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAttachmentOld(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editDokumen(ID, "Detail")
            ElseIf e.ExtraParams(1).Value = "Download" Then
                DownloadFileOld(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DownloadFileOld(id As Long)
        Dim objdownload As SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment = objGenerateSarOld.listObjDokumen.Find(Function(x) x.PK_ID = id)
        If Not objdownload Is Nothing Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & objdownload.File_Name)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "ContentType"
            Response.BinaryWrite(objdownload.File_Doc)
            Response.End()
        End If
    End Sub

    Protected Sub GridcommandAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editDokumen(ID, "Detail")
            ElseIf e.ExtraParams(1).Value = "Download" Then
                DownloadFile(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editDokumen(id As String, command As String)
        Try
            'FileDoc.Reset()
            'txtFileName.Clear()
            'txtKeterangan.Clear()

            'WindowAttachment.Hidden = False
            'Dim Dokumen As New SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment
            'Dokumen = ListDokumen.Where(Function(x) x.PK_ID = id).FirstOrDefault

            'txtFileName.Text = Dokumen.File_Name
            'txtKeterangan.Text = Dokumen.Remarks

            'If command = "Detail" Then
            '    FileDoc.Hidden = True
            '    BtnsaveAttachment.Hidden = True
            '    txtKeterangan.Editable = False
            'Else
            '    FileDoc.Hidden = False
            '    BtnsaveAttachment.Hidden = False
            '    txtKeterangan.Editable = True
            'End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAttachment.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            'If Not FileDoc.HasFile Then
            '    Throw New Exception("Please Upload File")
            'End If

            'Dim Dokumen As New SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment
            'If IDDokumen IsNot Nothing Or IDDokumen <> "" Then
            '    ListDokumen.Remove(ListDokumen.Where(Function(x) x.PK_ID = IDDokumen).FirstOrDefault)
            'End If

            'If ListDokumen.Count = 0 Then
            '    Dokumen.PK_ID = -1
            'ElseIf ListIndicator.Count >= 1 Then
            '    Dokumen.PK_ID = ListDokumen.Min(Function(x) x.PK_ID) - 1
            'End If

            'Dokumen.File_Name = IO.Path.GetFileName(FileDoc.FileName)
            'Dokumen.File_Doc = FileDoc.FileBytes
            'Dokumen.Remarks = txtKeterangan.Text.Trim

            'Dokumen.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            'Dokumen.CreatedDate = DateTime.Now

            'ListDokumen.Add(Dokumen)

            'bindAttachment(StoreAttachment, ListDokumen)
            'WindowAttachment.Hidden = True
            'IDDokumen = Nothing

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DownloadFile(id As Long)
        Dim objdownload As SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment = objGenerateSarNew.listObjDokumen.Find(Function(x) x.PK_ID = id)
        If Not objdownload Is Nothing Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & objdownload.File_Name)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "ContentType"
            Response.BinaryWrite(objdownload.File_Doc)
            Response.End()
        End If
    End Sub

    Sub clearSession()
        objGenerateSarNew = New SiPendarBLL.SIPENDARGenerateData
        objGenerateSarOld = New SiPendarBLL.SIPENDARGenerateData
        ListIndicatorNew = New List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
        ListIndicatorOld = New List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
    End Sub

    Sub bindOdmTransaksi(store As Ext.Net.Store, listTransaction As List(Of SiPendarDAL.goAML_ODM_Transaksi))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listTransaction)
        objtable.Columns.Add(New DataColumn("NoTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoRefTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("TipeTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("LokasiTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("KeteranganTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("DateTransaction", GetType(Date)))
        objtable.Columns.Add(New DataColumn("AccountNo", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaTeller", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaPejabat", GetType(String)))
        objtable.Columns.Add(New DataColumn("TglPembukuan", GetType(Date)))
        objtable.Columns.Add(New DataColumn("CaraTranDilakukan", GetType(String)))
        objtable.Columns.Add(New DataColumn("CaraTranLain", GetType(String)))
        objtable.Columns.Add(New DataColumn("DebitCredit", GetType(String)))
        objtable.Columns.Add(New DataColumn("OriginalAmount", GetType(String)))
        objtable.Columns.Add(New DataColumn("IDR", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("NoTran") = item("Transaction_Number")
                item("NoRefTran") = item("Ref_Num")
                Dim SourceData As String = item("Source_Data").ToString
                Dim SourceDataTemp As Integer
                If Integer.TryParse(SourceData, SourceDataTemp) Then
                    If SourceData <> "" Then
                        item("TipeTran") = SiPendarBLL.SIPENDARGenerateBLL.getSourceData(SourceData)
                    End If
                Else
                    item("TipeTran") = SourceData
                End If
                item("LokasiTran") = item("Transaction_Location")
                item("KeteranganTran") = item("Transaction_Remark")
                item("DateTransaction") = item("Date_Transaction")
                item("AccountNo") = item("Account_NO")
                item("NamaTeller") = item("Teller")
                item("NamaPejabat") = item("Authorized")
                item("TglPembukuan") = item("Date_Posting")
                item("CaraTranDilakukan") = item("Transmode_Code")
                item("CaraTranLain") = item("Transmode_Comment")
                item("DebitCredit") = item("Debit_Credit")
                If item("Original_Amount").ToString <> "" Then
                    Dim OriginalAmount As Decimal = item("Original_Amount").ToString
                    item("OriginalAmount") = OriginalAmount.ToString("#,###.00")
                End If
                If item("IDR_Amount").ToString <> "" Then
                    Dim IDR As Decimal = item("IDR_Amount").ToString
                    item("IDR") = IDR.ToString("#,###.00")
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub


    Sub bindTransaction(store As Ext.Net.Store, listTransaction As List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Transaction))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listTransaction)
        objtable.Columns.Add(New DataColumn("NoTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoRefTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("TipeTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("LokasiTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("KeteranganTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("DateTransaction", GetType(Date)))
        objtable.Columns.Add(New DataColumn("AccountNo", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaTeller", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaPejabat", GetType(String)))
        objtable.Columns.Add(New DataColumn("TglPembukuan", GetType(Date)))
        objtable.Columns.Add(New DataColumn("CaraTranDilakukan", GetType(String)))
        objtable.Columns.Add(New DataColumn("CaraTranLain", GetType(String)))
        objtable.Columns.Add(New DataColumn("DebitCredit", GetType(String)))
        objtable.Columns.Add(New DataColumn("OriginalAmount", GetType(String)))
        objtable.Columns.Add(New DataColumn("IDR", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("NoTran") = item("Transaction_Number")
                item("NoRefTran") = item("Ref_Num")
                Dim SourceData As String = item("Source_Data").ToString
                Dim SourceDataTemp As Integer
                If Integer.TryParse(SourceData, SourceDataTemp) Then
                    If SourceData <> "" Then
                        item("TipeTran") = SiPendarBLL.SIPENDARGenerateBLL.getSourceData(SourceData)
                    End If
                Else
                    item("TipeTran") = SourceData
                End If
                item("LokasiTran") = item("Transaction_Location")
                item("KeteranganTran") = item("Transaction_Remark")
                item("DateTransaction") = item("Date_Transaction")
                item("AccountNo") = item("Account_NO")
                item("NamaTeller") = item("Teller")
                item("NamaPejabat") = item("Authorized")
                item("TglPembukuan") = item("Date_Posting")
                item("CaraTranDilakukan") = item("Transmode_Code")
                item("CaraTranLain") = item("Transmode_Comment")
                item("DebitCredit") = item("Debit_Credit")
                If item("Original_Amount").ToString <> "" Then
                    Dim OriginalAmount As Decimal = item("Original_Amount").ToString
                    item("OriginalAmount") = OriginalAmount.ToString("#,###.00")
                End If
                If item("IDR_Amount").ToString <> "" Then
                    Dim IDR As Decimal = item("IDR_Amount").ToString
                    item("IDR") = IDR.ToString("#,###.00")
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub bindAttachment(store As Ext.Net.Store, listAttachment As List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAttachment)
        objtable.Columns.Add(New DataColumn("FileName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("FileName") = item("File_Name")
                item("Keterangan") = item("Remarks")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub bindReportIndicator(store As Ext.Net.Store, listAddresss As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        '  objtable.Columns.Add(New DataColumn("Kode_Indicator", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                '     item("Kode_Indicator") = item("FK_Indicator")
                item("Keterangan") = SiPendarBLL.SIPENDARGenerateBLL.getReportIndicatorByKode(item("FK_Indicator").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Protected Sub BtnReject_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim SAR As New SIPENDARGenerateBLL
            If TextAreaReasonApproval.Text = "" Then
                Throw New ApplicationException("Approval/Rejection Note Tidak Boleh Kosong")
            End If

            Dim Approval_Reason As String
            Approval_Reason = TextAreaReasonApproval.Text

            SAR.Reject(ObjApproval.PK_ModuleApproval_ID, Approval_Reason)

            LblConfirmation.Text = "Data Rejected. Click Ok to Back To " & ObjModule.ModuleLabel & " Approval."

            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnSave_Click(sneder As Object, e As DirectEventArgs)
        Try
            Dim SAR As New SIPENDARGenerateBLL

            If TextAreaReasonApproval.Text = "" Then
                Throw New ApplicationException("Approval/Rejection Note Tidak Boleh Kosong")
            End If

            Dim Approval_Reason As String
            Approval_Reason = TextAreaReasonApproval.Text
            Select Case ObjApproval.PK_ModuleAction_ID
                Case NawaBLL.Common.ModuleActionEnum.Insert
                    Dim ListSar As New List(Of SIPENDAR_ODM_Generate_Report)

                    'ListSar = SiPendarBLL.SIPENDARGenerateBLL.getListSar(objGenerateSarNew.objGenerateSAR.CIF_NO, objGenerateSarNew.objGenerateSAR.Transaction_Code, objGenerateSarNew.objGenerateSAR.Date_Report)
                    'If ListSar.Count > 0 Then
                    '    Throw New ApplicationException("Laporan Dengan GCN: " & objGenerateSarNew.objGenerateSAR.CIF_NO & ", Jenis Laporan: " & objGenerateSarNew.objGenerateSAR.Transaction_Code & " dan Tanggal Laporan: " & objGenerateSarNew.objGenerateSAR.Date_Report.Value.ToString("dd-MMM-yyyy") & " Sudah Ada.")
                    'End If
                    'Remark 18-Jan-2022, tidak validasi karena boleh Gerate lagi untuk profile berbeda
                    'If SIPENDARGenerateBLL.CheckReportByGCNandSubmission(objGenerateSarNew.objGenerateSAR.CIF_NO, objGenerateSarNew.objGenerateSAR.Date_Report) Then
                    '    Throw New ApplicationException("Transaksi dengan GCN : " & objGenerateSarNew.objGenerateSAR.CIF_NO & " pada Tanggal Laporan : " & objGenerateSarNew.objGenerateSAR.Date_Report & " sudah ada.")
                    'End If
                Case NawaBLL.Common.ModuleActionEnum.Update
                    'Remark 18-Jan-2022, tidak validasi karena boleh Gerate lagi untuk profile berbeda
                    'If SIPENDARGenerateBLL.CheckReportByGCNandSubmission(objGenerateSarNew.objGenerateSAR.CIF_NO, objGenerateSarNew.objGenerateSAR.Date_Report) Then
                    '    Throw New ApplicationException("Transaksi dengan GCN : " & objGenerateSarNew.objGenerateSAR.CIF_NO & " pada Tanggal Laporan : " & objGenerateSarNew.objGenerateSAR.Date_Report & " sudah ada.")
                    'End If
            End Select


            SIPENDARGenerateBLL.Accept(ObjApproval.PK_ModuleApproval_ID, Approval_Reason)

            LblConfirmation.Text = "Data Approved. Click Ok to Back To " & ObjModule.ModuleLabel & " Approval."

            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs) Handles BtnConfirmation.DirectClick
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class
