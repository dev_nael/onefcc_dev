﻿Imports System.Data
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports SiPendarDAL
Imports NawaBLL
Imports Ext
Imports SiPendarBLL
Imports System.Data.SqlClient
Imports NawaDAL
Imports Elmah
Imports System.IO
Imports OfficeOpenXml
'Update Product
Imports SiPendarBLL.GlobalReportFunctionBLL
Imports System.Text
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Data.Entity

Partial Class SIPENDAR_GenerateAdd
    Inherits Parent

#Region "Session"
    Public objFormModuleView As FormModuleView
    Public Property strWhereClause() As String
        Get
            Return Session("SIPENDAR_GenerateAdd.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("SIPENDAR_GenerateAdd.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("SIPENDAR_GenerateAdd.strSort")
        End Get
        Set(ByVal value As String)
            Session("SIPENDAR_GenerateAdd.strSort") = value
        End Set
    End Property

    Public Property indexStart() As String
        Get
            Return Session("SIPENDAR_GenerateAdd.indexStart")
        End Get
        Set(ByVal value As String)
            Session("SIPENDAR_GenerateAdd.indexStart") = value
        End Set
    End Property
    'Private ObjEODTask As SiPendarBLL.EODTaskBLL
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("SIPENDAR_GenerateAdd.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("SIPENDAR_GenerateAdd.ObjModule") = value
        End Set
    End Property
    Public Property ListTransaction As List(Of SiPendarDAL.goAML_ODM_Transaksi)
        Get
            If Session("SIPENDAR_GenerateAdd.ListTransaction") Is Nothing Then
                Session("SIPENDAR_GenerateAdd.ListTransaction") = New List(Of SiPendarDAL.goAML_ODM_Transaksi)
            End If
            Return Session("SIPENDAR_GenerateAdd.ListTransaction")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.goAML_ODM_Transaksi))
            Session("SIPENDAR_GenerateAdd.ListTransaction") = value
        End Set
    End Property
    Public Property ListSelectedTransaction As List(Of SiPendarDAL.goAML_ODM_Transaksi)
        Get
            If Session("SIPENDAR_GenerateAdd.ListSelectedTransaction") Is Nothing Then
                Session("SIPENDAR_GenerateAdd.ListSelectedTransaction") = New List(Of SiPendarDAL.goAML_ODM_Transaksi)
            End If
            Return Session("SIPENDAR_GenerateAdd.ListSelectedTransaction")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.goAML_ODM_Transaksi))
            Session("SIPENDAR_GenerateAdd.ListSelectedTransaction") = value
        End Set
    End Property
    Public Property ListSendTransaction As List(Of SiPendarDAL.goAML_ODM_Transaksi)
        Get
            If Session("SIPENDAR_GenerateAdd.ListSendTransaction") Is Nothing Then
                Session("SIPENDAR_GenerateAdd.ListSendTransaction") = New List(Of SiPendarDAL.goAML_ODM_Transaksi)
            End If
            Return Session("SIPENDAR_GenerateAdd.ListSendTransaction")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.goAML_ODM_Transaksi))
            Session("SIPENDAR_GenerateAdd.ListSendTransaction") = value
        End Set
    End Property
    Public Property ListGenerateSarTransaction As List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Transaction)
        Get
            If Session("SIPENDAR_GenerateAdd.ListGenerateSarTransaction") Is Nothing Then
                Session("SIPENDAR_GenerateAdd.ListGenerateSarTransaction") = New List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Transaction)
            End If
            Return Session("SIPENDAR_GenerateAdd.ListGenerateSarTransaction")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Transaction))
            Session("SIPENDAR_GenerateAdd.ListGenerateSarTransaction") = value
        End Set
    End Property
    Public Property objGenerateSAR As List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report)
        Get
            If Session("SIPENDAR_GenerateAdd.objGenerateSAR") Is Nothing Then
                Session("SIPENDAR_GenerateAdd.objGenerateSAR") = New List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report)
            End If
            Return Session("SIPENDAR_GenerateAdd.objGenerateSAR")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report))
            Session("SIPENDAR_GenerateAdd.objGenerateSAR") = value
        End Set
    End Property
    Public Property ObjSARData As SiPendarBLL.SIPENDARGenerateData
        Get
            Return Session("SIPENDAR_GenerateAdd.ObjSARData")
        End Get
        Set(ByVal value As SiPendarBLL.SIPENDARGenerateData)
            Session("SIPENDAR_GenerateAdd.ObjSARData") = value
        End Set
    End Property
    Public Property ListIndicator As List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
        Get
            Return Session("SIPENDAR_GenerateAdd.ListIndicator")
        End Get
        Set(value As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
            Session("SIPENDAR_GenerateAdd.ListIndicator") = value
        End Set
    End Property
    Public Property ListDokumen As List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment)
        Get
            Return Session("SIPENDAR_GenerateAdd.ListDokumen")
        End Get
        Set(value As List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment))
            Session("SIPENDAR_GenerateAdd.ListDokumen") = value
        End Set
    End Property

    Public Property IDIndicator As String
        Get
            Return Session("SIPENDAR_GenerateAdd.IDIndicator")
        End Get
        Set(value As String)
            Session("SIPENDAR_GenerateAdd.IDIndicator") = value
        End Set
    End Property

    Public Property IDDokumen As String
        Get
            Return Session("SIPENDAR_GenerateAdd.IDDokumen")
        End Get
        Set(value As String)
            Session("SIPENDAR_GenerateAdd.IDDokumen") = value
        End Set
    End Property

    Public Property IsUseFilterFromGlobal As String
        Get
            Return Session("SIPENDAR_GenerateAdd.IsUseFilterFromGlobal")
        End Get
        Set(value As String)
            Session("SIPENDAR_GenerateAdd.IsUseFilterFromGlobal") = value
        End Set
    End Property

#End Region

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                Dim strmodule As String = Request.Params("ModuleID")
                ClearSession()
                Dim intmodule As Integer = NawaBLL.Common.DecryptQueryString(strmodule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intmodule)

                If Not ObjModule Is Nothing Then
                    If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Insert) Then
                        Dim strIDCode As String = 1
                        strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                        Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    Using objdb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
                        Dim objglobalparam As SIPENDAR_GLOBAL_PARAMETER = objdb.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 101).FirstOrDefault
                        If objglobalparam IsNot Nothing AndAlso objglobalparam.ParameterValue IsNot Nothing Then
                            IsUseFilterFromGlobal = objglobalparam.ParameterValue
                        End If
                    End Using
                    FormPanelInput.Title = ObjModule.ModuleLabel & " Add"
                    'Panelconfirmation.Title = ObjModule.ModuleLabel & " Add"
                    'StoreDetailType.Reload()

                    'Dim objrand As New Random
                    'ObjTask.PK_EODTask_ID = objrand.Next

                    LoadStoreComboBox()
                    sar_DateFrom.Value = Date.Now
                    sar_DateTo.Value = Date.Now
                    FileDoc.FieldStyle = "background-color: #FFE4C4"
                    NoRefPPATK.FieldStyle = "background-color: #FFE4C4"
                    sar_reportIndicator.FieldStyle = "background-color: #FFE4C4"
                    ColumnActionLocation()
                    PopupMaximizale()

                    '4-Jul-2021 Adi : Tanggal laporan disesuaikan dengan Profile, jadi didisable
                    TanggalLaporan.ReadOnly = True
                    TanggalLaporan.FieldStyle = "background-color: #FFE4C4"
                    TanggalLaporan.Hidden = True
                Else
                    Throw New Exception("Invalid Module ID")
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
    '    ObjEODTask = New SiPendarBLL.EODTaskBLL(FormPanelInput)
    'End Sub
#End Region

#Region "Method"
    Sub ColumnActionLocation()
        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 1 Then
            'GridPanelReportIndicator.ColumnModel.Columns.Add(CommandColumn87)
        ElseIf bsettingRight = 2 Then
            GridPanelReportIndicator.ColumnModel.Columns.RemoveAt(GridPanelReportIndicator.ColumnModel.Columns.Count - 1)
            GridPanelReportIndicator.ColumnModel.Columns.Insert(1, CommandColumn87)

            GridPanelAttachment.ColumnModel.Columns.RemoveAt(GridPanelAttachment.ColumnModel.Columns.Count - 1)
            GridPanelAttachment.ColumnModel.Columns.Insert(1, CommandColumnAttachment)
        End If

    End Sub

    Sub PopupMaximizale()
        WindowReportIndicator.Maximizable = True
        WindowAttachment.Maximizable = True
    End Sub
    Private Sub ClearSession()
        ListTransaction = New List(Of SiPendarDAL.goAML_ODM_Transaksi)
        ListSelectedTransaction = New List(Of SiPendarDAL.goAML_ODM_Transaksi)
        ListSendTransaction = New List(Of SiPendarDAL.goAML_ODM_Transaksi)
        objGenerateSAR = New List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report)
        ListIndicator = New List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
        ObjSARData = New SiPendarBLL.SIPENDARGenerateData
        ListGenerateSarTransaction = New List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Transaction)
        ListDokumen = New List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment)
    End Sub
    Sub LoadStoreComboBox()
        sar_jenisLaporan.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreJenisLaporan.Reload()

        sar_reportIndicator.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreReportIndicator.Reload()

        sar_CIF.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreCIF.Reload()
    End Sub
    Protected Sub CIF_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " GCN_Customer_Name like '%" & query & "%'"

            End If
            'If strfilter.Length > 0 Then
            '    strfilter += " and active=1"
            'Else
            '    strfilter += "active=1"
            'End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            'objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_PENGAYAAN_WaitingGenerated", "GCN, GCN_Customer_Name", strfilter, "GCN_Customer_Name", e.Start, e.Limit, e.Total) '' Edit 17 Dec 2021
            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_PENGAYAAN_WaitingGenerated", "GCN, GCN_Customer_Name, PK_SIPENDAR_PROFILE_ID", strfilter, "GCN_Customer_Name", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub ReportIndicator_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            'objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goaml_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub JenisLaporan_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            'objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Jenis_Laporan_STR", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_Ref_Jenis_Laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Sub bindTransaction(store As Ext.Net.Store, listTransaction As List(Of SiPendarDAL.goAML_ODM_Transaksi))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listTransaction)
        objtable.Columns.Add(New DataColumn("Valid", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoRefTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("TipeTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("LokasiTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("KeteranganTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("DateTransaction", GetType(Date)))
        objtable.Columns.Add(New DataColumn("AccountNo", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaTeller", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaPejabat", GetType(String)))
        objtable.Columns.Add(New DataColumn("TglPembukuan", GetType(Date)))
        objtable.Columns.Add(New DataColumn("CaraTranDilakukan", GetType(String)))
        objtable.Columns.Add(New DataColumn("CaraTranLain", GetType(String)))
        objtable.Columns.Add(New DataColumn("DebitCredit", GetType(String)))
        objtable.Columns.Add(New DataColumn("OriginalAmount", GetType(String)))
        objtable.Columns.Add(New DataColumn("IDR", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim CifLawan As String = item("CIF_No_Lawan").ToString
                Dim WicLawan As String = item("WIC_No_Lawan").ToString
                Dim AccountLawan As String = item("ACCOUNT_No_Lawan").ToString
                Dim BiMulti As String = item("BiMultiParty").ToString
                If (CifLawan = "" And WicLawan = "" And AccountLawan = "") Or BiMulti = "" Then
                    item("Valid") = "Tidak"
                Else
                    item("Valid") = "Ya"
                End If
                item("NoTran") = item("Transaction_Number")
                item("NoRefTran") = item("Ref_Num")
                Dim SourceData As String = item("Source_Data").ToString
                Dim SourceDataTemp As Integer
                If Integer.TryParse(SourceData, SourceDataTemp) Then
                    If SourceData <> "" Then
                        item("TipeTran") = SiPendarBLL.SIPENDARGenerateBLL.getSourceData(SourceData)
                    End If
                Else
                    item("TipeTran") = SourceData
                End If
                item("LokasiTran") = item("Transaction_Location")
                item("KeteranganTran") = item("Transaction_Remark")
                item("DateTransaction") = item("Date_Transaction")
                item("AccountNo") = item("Account_NO")
                item("NamaTeller") = item("Teller")
                item("NamaPejabat") = item("Authorized")
                item("TglPembukuan") = item("Date_Posting")
                item("CaraTranDilakukan") = item("Transmode_Code")
                item("CaraTranLain") = item("Transmode_Comment")
                item("DebitCredit") = item("Debit_Credit")
                If item("Original_Amount").ToString <> "" Then
                    Dim OriginalAmount As Decimal = item("Original_Amount").ToString
                    item("OriginalAmount") = OriginalAmount.ToString("#,###.00")
                End If
                If item("IDR_Amount").ToString <> "" Then
                    Dim IDR As Decimal = item("IDR_Amount").ToString
                    item("IDR") = IDR.ToString("#,###.00")
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAttachment(store As Ext.Net.Store, listAttachment As List(Of SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAttachment)
        objtable.Columns.Add(New DataColumn("FileName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("FileName") = item("File_Name")
                item("Keterangan") = item("Remarks")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindReportIndicator(store As Ext.Net.Store, listAddresss As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        '  objtable.Columns.Add(New DataColumn("Kode_Indicator", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                '     item("Kode_Indicator") = item("FK_Indicator")
                item("Keterangan") = getReportIndicatorByKode(item("FK_Indicator").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Shared Function getReportIndicatorByKode(Kode As String) As String
        Dim strKategori As String = ""
        Using objdb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Dim kategori As SiPendarDAL.goAML_Ref_Indikator_Laporan = objdb.goAML_Ref_Indikator_Laporan.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If kategori IsNot Nothing Then
                strKategori = kategori.Keterangan
            End If
        End Using
        Return strKategori
    End Function


    Sub editIndicator(id As String, command As String)
        Try
            sar_reportIndicator.Clear()

            WindowReportIndicator.Hidden = False
            Dim reportIndicator As SiPendarDAL.SIPENDAR_Report_Indicator
            reportIndicator = ListIndicator.Where(Function(x) x.PK_Report_Indicator = id).FirstOrDefault


            sar_reportIndicator.SetValue(reportIndicator.FK_Indicator)
            If command = "Detail" Then
                sar_reportIndicator.Selectable = False
                BtnsaveReportIndicator.Hidden = True
            Else
                sar_reportIndicator.Selectable = True
                BtnsaveReportIndicator.Hidden = False
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub editDokumen(id As String, command As String)
        Try
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()

            WindowAttachment.Hidden = False
            Dim Dokumen As New SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment
            Dokumen = ListDokumen.Where(Function(x) x.PK_ID = id).FirstOrDefault

            txtFileName.Text = Dokumen.File_Name
            txtKeterangan.Text = Dokumen.Remarks

            If command = "Detail" Then
                FileDoc.Hidden = True
                BtnsaveAttachment.Hidden = True
                txtKeterangan.Editable = False
            Else
                FileDoc.Hidden = False
                BtnsaveAttachment.Hidden = False
                txtKeterangan.Editable = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DownloadFile(id As Long)
        Dim objdownload As SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment = ListDokumen.Find(Function(x) x.PK_ID = id)
        If Not objdownload Is Nothing Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & objdownload.File_Name)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            'Response.ContentType = "ContentType"
            Response.ContentType = MimeMapping.GetMimeMapping(objdownload.File_Name)
            Response.BinaryWrite(objdownload.File_Doc)
            Response.End()
        End If
    End Sub

    Protected Sub CreateExcel2007WithData(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try


            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            'Dim intTotalRow As Long
            Dim objSchemaModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(12226)
            Dim objSchemaModuleField As List(Of NawaDAL.ModuleField) = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(12226)
            Dim objSchemaModuleFieldDefault As New List(Of NawaDAL.ModuleFieldDefault)
            Dim objmdl As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(12226)

            Dim cif As String = GetGCNbyPKProfile(sar_CIF.SelectedItem.Value) '' sar_CIF.SelectedItem.Value '' Edit 17-Dec-2021
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate

            'Dim objList = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch(cif, datefrom, dateto)
            '4-Jul-2021 ubah by GCN ListTransaction
            'comment karena tidak terpakai
            'Dim objList = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch_GCN(cif, datefrom, dateto)



            objFormModuleView = New FormModuleView(GridPaneldetail, BtnExport)
            objFormModuleView.ModuleID = objmdl.PK_Module_ID
            objFormModuleView.ModuleName = objmdl.ModuleName

            Dim delimitercheckboxparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(39)
            Dim strdelimiter As String = ";"
            If Not delimitercheckboxparam Is Nothing Then
                strdelimiter = delimitercheckboxparam.SettingValue
            End If

            Dim objtb As DataTable = NawaBLL.Common.CopyGenericToDataTable(ListTransaction)
            Dim intmaxrow As Integer
            If objtb.Rows.Count + 1000 > 1048575 Then
                intmaxrow = 1048575
            Else
                intmaxrow = objtb.Rows.Count + 1000
            End If


            Using objtbl As Data.DataTable = objtb
                'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                objtbl.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                Dim intTotalPage As Integer = (intmaxrow + 1048575 - 1) / 1048575
                Dim dsTable As New Data.DataSet

                If intTotalPage = 1 Then
                    'objFormModuleView.changeHeader(objtbl)

                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    DataSetToExcel(dsTable, objfileinfo)

                    dsTable.Dispose()
                    dsTable = Nothing



                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If



                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")



                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()
                ElseIf intTotalPage > 1 Then
                    'objFormModuleView.changeHeader(objtbl)



                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    'ambil page 2 dst sampe page terakhir
                    For index = 1 To intTotalPage - 2
                        Using objtbltemp As Data.DataTable = objtb
                            objtbltemp.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                            dsTable.Tables.Add(objtbltemp.Copy)
                        End Using
                    Next
                    DataSetToExcel(dsTable, objfileinfo)
                    dsTable.Dispose()
                    dsTable = Nothing

                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If
                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()

                End If
            End Using

        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub
    Protected Sub CreateExcel2007WithDataNoChecked(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try


            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            'Dim intTotalRow As Long
            Dim objSchemaModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(12226)
            Dim objSchemaModuleField As List(Of NawaDAL.ModuleField) = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(12226)
            Dim objSchemaModuleFieldDefault As New List(Of NawaDAL.ModuleFieldDefault)
            Dim objmdl As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(12226)

            Dim cif As String = GetGCNbyPKProfile(sar_CIF.SelectedItem.Value) '' sar_CIF.SelectedItem.Value ''Edit 17-Dec-2021
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate

            'Dim objList = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch(cif, datefrom, dateto)
            '4-Jul-2021 ubah by GCN
            'comment karena tidak terpakai
            'Dim objList = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch_GCN(cif, datefrom, dateto)



            objFormModuleView = New FormModuleView(GridPanel1, BtnExportNoChecked)
            objFormModuleView.ModuleID = objmdl.PK_Module_ID
            objFormModuleView.ModuleName = objmdl.ModuleName

            Dim delimitercheckboxparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(39)
            Dim strdelimiter As String = ";"
            If Not delimitercheckboxparam Is Nothing Then
                strdelimiter = delimitercheckboxparam.SettingValue
            End If

            Dim objtb As DataTable = NawaBLL.Common.CopyGenericToDataTable(ListTransaction)
            Dim intmaxrow As Integer
            If objtb.Rows.Count + 1000 > 1048575 Then
                intmaxrow = 1048575
            Else
                intmaxrow = objtb.Rows.Count + 1000
            End If


            Using objtbl As Data.DataTable = objtb
                'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                objtbl.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                Dim intTotalPage As Integer = (intmaxrow + 1048575 - 1) / 1048575
                Dim dsTable As New Data.DataSet

                If intTotalPage = 1 Then
                    'objFormModuleView.changeHeader(objtbl)

                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    DataSetToExcel(dsTable, objfileinfo)

                    dsTable.Dispose()
                    dsTable = Nothing



                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If



                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")



                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()
                ElseIf intTotalPage > 1 Then
                    'objFormModuleView.changeHeader(objtbl)



                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    'ambil page 2 dst sampe page terakhir
                    For index = 1 To intTotalPage - 2
                        Using objtbltemp As Data.DataTable = objtb
                            objtbltemp.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                            dsTable.Tables.Add(objtbltemp.Copy)
                        End Using
                    Next
                    DataSetToExcel(dsTable, objfileinfo)
                    dsTable.Dispose()
                    dsTable = Nothing

                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If
                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()

                End If
            End Using

        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub
    Private Shared Sub DataSetToExcel(ByVal dataSet As DataSet, ByVal objfileinfo As FileInfo)
        Using pck As ExcelPackage = New ExcelPackage()

            For Each dataTable As DataTable In dataSet.Tables
                Dim workSheet As ExcelWorksheet = pck.Workbook.Worksheets.Add(dataTable.TableName)
                workSheet.Cells("A1").LoadFromDataTable(dataTable, True)

                Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                Dim intcolnumber As Integer = 1
                For Each item As System.Data.DataColumn In dataTable.Columns
                    If item.DataType = GetType(Date) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = dateformat
                    End If
                    If item.DataType = GetType(Integer) Or item.DataType = GetType(Double) Or item.DataType = GetType(Long) Or item.DataType = GetType(Decimal) Or item.DataType = GetType(Single) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = "#,##0.00"
                    End If

                    intcolnumber = intcolnumber + 1
                Next
                workSheet.Cells(workSheet.Dimension.Address).AutoFitColumns()
            Next

            pck.SaveAs(objfileinfo)
        End Using
    End Sub

    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

            'strfilter = strfilter.Replace("Active", objFormModuleView.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter

            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            End If

            Me.strOrder = strsort
            If strsort = "" Then
                strsort = "NO_ID asc"
            End If

            Dim DataPaging As DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)

            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If

            e.Total = inttotalRecord
            GridPaneldetail.GetStore.DataSource = DataPaging
            GridPaneldetail.GetStore.DataBind()


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Direct Event"
    Protected Sub OnSelect_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim data As New goAML_ODM_Transaksi
            Dim idx As String
            idx = e.ExtraParams(0).Value

            data.NO_ID = e.ExtraParams(0).Value

            ListSelectedTransaction.Add(data)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub OnDeSelect_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim data As New goAML_ODM_Transaksi

            For Each item In ListSelectedTransaction
                If item.NO_ID = CInt(e.ExtraParams(0).Value) Then
                    data = item
                    Exit For
                End If
            Next

            ListSelectedTransaction.Remove(data)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnGenerate_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim ListSar As New List(Of SIPENDAR_ODM_Generate_Report)
            Dim ListODMTransactionIncomplete As New List(Of goAML_ODM_Transaksi)
            Dim ListTransactionIncomplete As New List(Of SIPENDAR_ODM_Generate_Report_Transaction)
            Dim getIsGenerateSAR As String = SiPendarBLL.SIPENDARGenerateBLL.getIsGenerateSAR("IsGenerateSAR")

            'Update Product Remark Validasi Cara Transaksi Dilakukan/Transmode Code
            'Dim ListTransactionNoTrnMode As New List(Of SIPENDAR_ODM_Generate_Report_Transaction)
            'Dim ListODMTransactionNoTrnMode As New List(Of goAML_ODM_Transaksi)

            'Update Product untuk No ref PPATK/Fiu_Ref_Number
            If checkjenislaporan() = True And NoRefPPATK.Text = "" Then
                Throw New ApplicationException("Jika Jenis Laporannya LTKMP maka No Ref PPATK Tidak Boleh Kosong")
            End If
            If ListIndicator.Count = 0 Then
                Throw New ApplicationException("indikator tidak boleh kosong")
            End If

            Dim rsm As RowSelectionModel = GridPaneldetail.SelectionModel.Primary

            ListSelectedTransaction.Clear()
            For Each itemrow As SelectedRow In rsm.SelectedRows
                If itemrow.RecordID IsNot Nothing Then
                    Dim data As New goAML_ODM_Transaksi
                    data.NO_ID = itemrow.RecordID
                    ListSelectedTransaction.Add(data)
                End If
            Next

            Dim GenerateSAR As New SIPENDAR_ODM_Generate_Report
            With GenerateSAR
                '.CIF_NO = sar_CIF.SelectedItem.Value '' Edit 17-Dec-2021
                .CIF_NO = GetGCNbyPKProfile(sar_CIF.SelectedItem.Value)
                .Transaction_Code = sar_jenisLaporan.SelectedItem.Value
                .Date_Report = TanggalLaporan.Value
                .Fiu_Ref_Number = NoRefPPATK.Text
                .Reason = alasan.Text
                .total_transaction = ListSelectedTransaction.Count
                .Fk_Report_ID = 0
                .status = 1
                If ListTransaction.Count = ListSelectedTransaction.Count OrElse sar_IsSelectedAll.Value OrElse rsm.SelectedRows.Count = ListTransaction.Count Then
                    .IsSelectedAll = True
                Else
                    .IsSelectedAll = False
                End If

                .FK_Sipendar_Profile_ID = sar_CIF.SelectedItem.Value '' Add 20-Dec-2021

                '' Edit 17-Dec-2021
                'If SIPENDARGenerateBLL.CheckReportByGCNandSubmission(.CIF_NO, .Date_Report) Then
                '    Throw New ApplicationException("Transaksi dengan GCN : " & .CIF_NO & " pada Tanggal Laporan : " & .Date_Report & " sudah ada.")
                'End If

                ''Remark 18-Jan-2022, boleh bikin Report yang sama
                'Dim unikReference As String = GetUnikReferencebyPKProfile(sar_CIF.SelectedItem.Value)
                'If CheckReportByUnikReference(unikReference) Then
                '    Throw New ApplicationException("Transaksi dengan Unik Refence : " & unikReference & " sudah ada.")
                'End If
                '' End 17-Dec-2021

                If ListIndicator.Count > 0 Then
                    Dim i As Integer
                    Dim temp As String
                    i = 0
                    temp = ""
                    For Each item In ListIndicator
                        i += 1
                        If ListIndicator.Count = 1 Then
                            'temp = getReportIndicatorByKode(item.FK_Indicator)
                            temp = item.FK_Indicator
                        Else
                            If i = 1 Then
                                'temp = getReportIndicatorByKode(item.FK_Indicator)
                                temp = item.FK_Indicator
                            Else
                                'temp += "," + getReportIndicatorByKode(item.FK_Indicator)
                                temp += "," + item.FK_Indicator
                            End If
                        End If
                    Next
                    .indicator = temp
                End If
            End With

            ObjSARData.objGenerateSAR = GenerateSAR

            ' Remarked 20-Dec-2021. Karena sekarang bisa 1 GCN banyak Profile.
            'ListSar = SiPendarBLL.SIPENDARGenerateBLL.getListSar(GenerateSAR.CIF_NO, GenerateSAR.Transaction_Code, GenerateSAR.Date_Report)
            'If ListSar.Count > 0 Then
            '    Throw New ApplicationException("Laporan Dengan GCN: " & GenerateSAR.CIF_NO & ", Jenis Laporan: " & GenerateSAR.Transaction_Code & " dan Tanggal Laporan: " & GenerateSAR.Date_Report.Value.ToString("dd-MMM-yyyy") & " Sudah Ada.")
            'End If

            For Each item In ListDokumen
                ObjSARData.listObjDokumen.Add(item)
            Next

            If GenerateSAR.IsSelectedAll = False Then
                If ListSelectedTransaction.Count = 0 Then
                    Throw New ApplicationException("Tidak Ada Transaksi yang dipilih")
                End If

                Dim objODMTransaksi As New goAML_ODM_Transaksi
                Dim objSARTransaksi As New SIPENDAR_ODM_Generate_Report_Transaction

                ObjSARData.listObjGenerateSARTransaction = New List(Of SIPENDAR_ODM_Generate_Report_Transaction)

                For Each item In ListSelectedTransaction
                    objODMTransaksi = ListTransaction.Where(Function(x) x.NO_ID = item.NO_ID).FirstOrDefault
                    'daniel 20210903 validasi objODMTransaksi not nothing
                    If objODMTransaksi IsNot Nothing Then
                        With objSARTransaksi
                            .Date_Transaction = objODMTransaksi.Date_Transaction
                            .CIF_NO = objODMTransaksi.CIF_NO
                            .Account_NO = objODMTransaksi.Account_NO
                            .WIC_NO = objODMTransaksi.WIC_No
                            .Ref_Num = objODMTransaksi.Ref_Num
                            .Debit_Credit = objODMTransaksi.Debit_Credit
                            .Original_Amount = objODMTransaksi.Original_Amount
                            .Currency = objODMTransaksi.Currency
                            .Exchange_Rate = objODMTransaksi.Exchange_Rate
                            .IDR_Amount = objODMTransaksi.IDR_Amount
                            .Transaction_Code = objODMTransaksi.Transaction_Code
                            .Source_Data = objODMTransaksi.Source_Data
                            .Transaction_Remark = objODMTransaksi.Transaction_Remark
                            .Transaction_Number = objODMTransaksi.Transaction_Number
                            .Transaction_Location = objODMTransaksi.Transaction_Location
                            .Teller = objODMTransaksi.Teller
                            .Authorized = objODMTransaksi.Authorized
                            .Date_Posting = objODMTransaksi.Date_Posting
                            .Transmode_Code = objODMTransaksi.Transmode_Code
                            .Transmode_Comment = objODMTransaksi.Transmode_Comment
                            .Comments = objODMTransaksi.Comments
                            .CIF_No_Lawan = objODMTransaksi.CIF_No_Lawan
                            .ACCOUNT_No_Lawan = objODMTransaksi.ACCOUNT_No_Lawan
                            .WIC_No_Lawan = objODMTransaksi.WIC_No_Lawan
                            .Conductor_ID = objODMTransaksi.Conductor_ID
                            .Swift_Code_Lawan = objODMTransaksi.Swift_Code_Lawan
                            .country_code_lawan = objODMTransaksi.Country_Code_Lawan
                            .MsgTypeSwift = objODMTransaksi.MsgSwiftType
                            .From_Funds_Code = objODMTransaksi.From_Funds_Code
                            .To_Funds_Code = objODMTransaksi.To_Funds_Code
                            .country_code = objODMTransaksi.Country_Code
                            .Currency_Lawan = objODMTransaksi.Currency_Lawan
                            .BiMultiParty = objODMTransaksi.BiMultiParty
                            .GCN = objODMTransaksi.GCN
                            .GCN_Lawan = objODMTransaksi.GCN_Lawan
                            .Active = 1
                            .Business_date = objODMTransaksi.Business_date
                            .From_Type = objODMTransaksi.From_Type
                            .To_Type = objODMTransaksi.To_Type
                        End With
                        ObjSARData.listObjGenerateSARTransaction.Add(objSARTransaksi)
                    End If
                    objODMTransaksi = New goAML_ODM_Transaksi
                    objSARTransaksi = New SIPENDAR_ODM_Generate_Report_Transaction
                Next

                '' Add 23 Nov 2021 , baca ke global Parameter PK 8
                Dim isCheckCounterParty As String = 0
                Using objdb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
                    Dim objglobalparam As SIPENDAR_GLOBAL_PARAMETER = objdb.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 8).FirstOrDefault
                    If objglobalparam IsNot Nothing AndAlso objglobalparam.ParameterValue IsNot Nothing Then
                        isCheckCounterParty = objglobalparam.ParameterValue
                    End If
                End Using
                '' End 23 Nov 2021

                'If getIsGenerateSAR = "1" Then
                If getIsGenerateSAR = "1" And isCheckCounterParty = 1 Then '' Add 23 Nov 2021
                    ListTransactionIncomplete = ObjSARData.listObjGenerateSARTransaction.Where(Function(x) x.BiMultiParty = 1 And x.CIF_No_Lawan Is Nothing And x.WIC_No_Lawan Is Nothing And x.ACCOUNT_No_Lawan Is Nothing).ToList
                    If ListTransactionIncomplete.Count > 0 Then
                        Throw New ApplicationException("Any Counter Party of Transaction is not completed")
                    End If
                End If

                'Update Product Remark Validasi Cara Transaksi Dilakukan/Transmode Code
                'ListTransactionNoTrnMode = ObjSARData.listObjGenerateSARTransaction.Where(Function(x) x.Transmode_Code = "" Or x.Transmode_Code Is Nothing).ToList
                'If ListTransactionNoTrnMode.Count > 0 Then
                '    Throw New ApplicationException("Cara Tranaksi Dilakukan Kosong Di " + ListTransactionNoTrnMode.Count.ToString + " Transaksi")
                'End If

            ElseIf GenerateSAR.IsSelectedAll = True Then
                If ListTransaction.Count = 0 Then
                    Throw New ApplicationException("Tidak Ada Transaksi")
                End If

                '' Add 23 Nov 2021 , baca ke global Parameter PK 8
                Dim isCheckCounterParty As String = 0
                Using objdb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
                    Dim objglobalparam As SIPENDAR_GLOBAL_PARAMETER = objdb.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 8).FirstOrDefault
                    If objglobalparam IsNot Nothing AndAlso objglobalparam.ParameterValue IsNot Nothing Then
                        isCheckCounterParty = objglobalparam.ParameterValue
                    End If
                End Using
                '' End 23 Nov 2021

                'If getIsGenerateSAR = "1" Then
                If getIsGenerateSAR = "1" And isCheckCounterParty = 1 Then '' Add 23 Nov 2021
                    ListODMTransactionIncomplete = ListTransaction.Where(Function(x) x.BiMultiParty = 1 And x.CIF_No_Lawan Is Nothing And x.WIC_No_Lawan Is Nothing And x.ACCOUNT_No_Lawan Is Nothing).ToList
                    If ListODMTransactionIncomplete.Count > 0 Then
                        Throw New ApplicationException("Any Counter Party of Transaction is not completed")
                    End If
                End If

                'Update Product Remark Validasi Cara Transaksi Dilakukan/Transmode Code
                'ListODMTransactionNoTrnMode = ListTransaction.Where(Function(x) x.Transmode_Code = "" Or x.Transmode_Code Is Nothing).ToList
                'If ListODMTransactionNoTrnMode.Count > 0 Then
                '    Throw New ApplicationException("Cara Tranaksi Dilakukan Kosong Di " + ListODMTransactionNoTrnMode.Count.ToString + " Transaksi")
                'End If
            End If

            If Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                'Dim dirPath As String = Server.MapPath(Request.ServerVariables("PATH_INFO")).Substring(0, Server.MapPath(Request.ServerVariables("PATH_INFO")).LastIndexOf("\") + 1) & "DikumenSAR\"
                'Dim dirPath As String = Server.MapPath(Request.ServerVariables("PATH_INFO")).Substring(0, Server.MapPath(Request.ServerVariables("PATH_INFO")).LastIndexOf("\") + 1) & "FolderSAR\"
                'Dim dirPathTemplate As String = Server.MapPath(Request.ServerVariables("PATH_INFO")).Substring(0, Server.MapPath(Request.ServerVariables("PATH_INFO")).LastIndexOf("\") + 1) & "FolderTemplate\"
                'Dim FileReturn As String = ""
                'dirPath = dirPath & Common.SessionCurrentUser.UserID & "\"
                'If Not Directory.Exists(dirPath) Then
                '    Directory.CreateDirectory(dirPath)
                'End If
                'Dim path As String = ConfigurationManager.AppSettings("FilePathGenerateSARAttachment").ToString
                'Dim path As String = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(9018).SettingValue
                'SiPendarBLL.SIPENDARGenerateBLL.SaveGenerateSarTanpaApproval(ObjSARData, ObjModule, 1, path)



                Dim DateFrom As DateTime = sar_DateFrom.Value
                Dim DateTo As DateTime = sar_DateTo.Value
                SiPendarBLL.SIPENDARGenerateBLL.SaveGenerateSTRSarTanpaApproval(ObjSARData, ObjModule, 1, DateFrom, DateTo)
                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Saved into Database."
            Else
                If ObjSARData.objGenerateSAR.IsSelectedAll Then
                    Dim SarTransaction As New SIPENDAR_ODM_Generate_Report_Transaction
                    SarTransaction.Date_Transaction = sar_DateFrom.Value
                    SarTransaction.Date_Posting = sar_DateTo.Value

                    ObjSARData.listObjGenerateSARTransaction.Add(SarTransaction)
                End If
                ObjSARData.objGenerateSAR.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                ObjSARData.objGenerateSAR.CreatedDate = DateTime.Now

                'SiPendarBLL.SIPENDARGenerateBLL.SaveGenerateSarApproval(ObjSARData, ObjModule, 1)
                SiPendarBLL.SIPENDARGenerateBLL.SaveGenerateSTRSarApproval(ObjSARData, ObjModule, 1)
                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Saved into Pending Approval"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSearch_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim cif As String = GetGCNbyPKProfile(sar_CIF.SelectedItem.Value) '' sar_CIF.SelectedItem.Value '' Edit 17-Dec-2021
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate

            If cif Is Nothing Then
                Throw New ApplicationException("GCN No Tidak boleh kosong")
            ElseIf sar_DateFrom.Text = "1/1/0001 12:00:00 AM" Or sar_DateTo.Text = "1/1/0001 12:00:00 AM" Then
                Throw New ApplicationException("Tidak Ada Transaksi")
            End If

            'ListTransaction = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch(cif, datefrom, dateto)
            '4-Jul-2021 ubah by GCN
            If IsUseFilterFromGlobal IsNot Nothing AndAlso IsUseFilterFromGlobal = "1" Then
                ListTransaction = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch_GCN_WithFilter(cif, datefrom, dateto)
            Else
                ListTransaction = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch_GCN(cif, datefrom, dateto)
            End If

            'If ListTransaction.Count > 0 Then
            bindTransaction(StoreTransaction, ListTransaction)
            bindTransaction(StoreTransaksiNoChecked, ListTransaction)
            'End If
            If ListTransaction.Count = 0 Then
                Ext.Net.X.Msg.Alert("Message", "Tidak Ada Transaksi").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub btn_addIndicator_click(sender As Object, e As DirectEventArgs)
        Try
            sar_reportIndicator.Clear()
            WindowReportIndicator.Hidden = False
            sar_reportIndicator.Selectable = True
            BtnsaveReportIndicator.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_addAttachment_click(sender As Object, e As DirectEventArgs)
        Try
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()
            WindowAttachment.Hidden = False
            FileDoc.Hidden = False
            BtnsaveAttachment.Hidden = False
            txtKeterangan.Editable = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandReportIndicator(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = ID).FirstOrDefault)
                bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                editIndicator(ID, "Edit")
                IDIndicator = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editIndicator(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ListDokumen.Remove(ListDokumen.Where(Function(x) x.PK_ID = ID).FirstOrDefault)
                bindAttachment(StoreAttachment, ListDokumen)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                editDokumen(ID, "Edit")
                IDDokumen = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editDokumen(ID, "Detail")
            ElseIf e.ExtraParams(1).Value = "Download" Then
                DownloadFile(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveReportIndicator_Click(sender As Object, e As DirectEventArgs)
        Try
            If String.IsNullOrEmpty(sar_reportIndicator.SelectedItem.Value) Then
                Throw New ApplicationException(sar_reportIndicator.FieldLabel + " Tidak boleh kosong")
            End If

            For Each item In ListIndicator
                If item.FK_Indicator = sar_reportIndicator.SelectedItem.Value Then
                    Throw New ApplicationException(sar_reportIndicator.SelectedItem.Value + " Sudah Ada")
                End If
            Next

            Dim reportIndicator As New SiPendarDAL.SIPENDAR_Report_Indicator
            If IDIndicator IsNot Nothing Or IDIndicator <> "" Then
                ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = IDIndicator).FirstOrDefault)
            End If
            If ListIndicator.Count = 0 Then
                reportIndicator.PK_Report_Indicator = -1
            ElseIf ListIndicator.Count >= 1 Then
                reportIndicator.PK_Report_Indicator = ListIndicator.Min(Function(x) x.PK_Report_Indicator) - 1
            End If

            reportIndicator.FK_Indicator = sar_reportIndicator.SelectedItem.Value
            ListIndicator.Add(reportIndicator)
            bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            WindowReportIndicator.Hidden = True
            IDIndicator = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelreportIndicator_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowReportIndicator.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim status As Boolean = False
            Dim DokumenOld As New SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment
            Dim Dokumen As New SiPendarDAL.SIPENDAR_ODM_Generate_Report_Attachment
            If IDDokumen IsNot Nothing Or IDDokumen <> "" Then
                status = True
                DokumenOld = ListDokumen.Where(Function(x) x.PK_ID = IDDokumen).FirstOrDefault
                ListDokumen.Remove(ListDokumen.Where(Function(x) x.PK_ID = IDDokumen).FirstOrDefault)
            ElseIf Not FileDoc.HasFile Then
                Throw New Exception("Please Upload File")
            End If

            Dim docName As String = IO.Path.GetFileName(FileDoc.FileName)
            For Each item In ListDokumen
                If item.File_Name = docName Then
                    Throw New ApplicationException(docName + " Sudah Ada")
                End If
            Next

            If ListDokumen.Count = 0 Then
                Dokumen.PK_ID = -1
            ElseIf ListDokumen.Count >= 1 Then
                Dokumen.PK_ID = ListDokumen.Min(Function(x) x.PK_ID) - 1
            End If

            If status = True And Not FileDoc.HasFile Then
                Dokumen.File_Name = DokumenOld.File_Name
                Dokumen.File_Doc = DokumenOld.File_Doc
            Else
                Dokumen.File_Name = IO.Path.GetFileName(FileDoc.FileName)
                Dokumen.File_Doc = FileDoc.FileBytes
            End If
            Dokumen.Remarks = txtKeterangan.Text.Trim

            Dokumen.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            Dokumen.CreatedDate = DateTime.Now

            ListDokumen.Add(Dokumen)

            bindAttachment(StoreAttachment, ListDokumen)
            WindowAttachment.Hidden = True
            IDDokumen = Nothing

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAttachment.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub sar_jenisLaporan_DirectSelect(sender As Object, e As DirectEventArgs)
        Try
            'If sar_jenisLaporan.SelectedItem.Value = "LTKMT" Or sar_jenisLaporan.SelectedItem.Value = "LAPT" Or sar_jenisLaporan.SelectedItem.Value = "LTKM" Then
            If sar_jenisLaporan.SelectedItem.Value IsNot Nothing Then
                PanelReportIndicator.Hidden = False
            Else
                PanelReportIndicator.Hidden = True
            End If

            'Update Product untuk No ref PPATK/Fiu_Ref_Number
            If checkjenislaporan() = True Then
                NoRefPPATK.Hidden = False
            Else
                NoRefPPATK.Hidden = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub sar_IsSelectedAll_CheckedChanged(sender As Object, e As EventArgs) Handles sar_IsSelectedAll.DirectCheck
        If sar_IsSelectedAll.Value Then
            PanelChecked.Hide()
            PanelnoCheked.Show()
        Else
            PanelChecked.Show()
            PanelnoCheked.Hide()
        End If
    End Sub

    'Protected Sub sar_IsSelectedAll_CheckedChanged(sender As Object, e As EventArgs) Handles sar_IsSelectedAll.DirectCheck
    '    If sar_IsSelectedAll.Value Then
    '        'pake yg ini
    '        'GridPaneldetail.Selectable = False

    '        'CheckboxSelectionModel1.SelectAll()
    '        'GridPaneldetail.SelectionModel.Clear() 
    '        'CheckboxSelectionModel1.SetLocked(True)
    '        CheckboxSelectionModel1.Visible = False
    '        'Dim selmodel = New CheckboxSelectionModel()

    '        'With selmodel
    '        '    .CheckOnly = True
    '        '    .SelectAll()
    '        'End With
    '        'GridPaneldetail.SelectionModel.Add(selmodel)
    '    Else
    '        'GridPaneldetail.DisableSelection = False
    '        CheckboxSelectionModel1.Visible = True
    '        'CheckboxSelectionModel1.SetLocked(False)
    '        'CheckboxSelectionModel1.DeselectAll()
    '    End If

    'End Sub

    'Update Product untuk No ref PPATK/Fiu_Ref_Number
    Function checkjenislaporan()
        Try
            Using objdb As New SiPendarEntities
                Dim jenisLaporan As String = sar_jenisLaporan.Value
                If objdb.GoAML_Laporan_Req_PPATK.Where(Function(x) x.Kode = jenisLaporan And x.Active = True).FirstOrDefault IsNot Nothing Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            Throw New ApplicationException("There is an error occurred on checkjenislaporan() function")
        End Try
    End Function

#End Region

    Protected Sub sar_CIF_Change()
        Try
            Dim strComboCIF As String = sar_CIF.SelectedItem.Text
            Dim strDate As String = Right(strComboCIF, 10)

            TanggalLaporan.Value = CDate(strDate)
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#Region "17-Dec-2021 Felix"
    Private Function GetGCNbyPKProfile(PK As String) As String
        Try

            Dim strSQLGetGCN As String = "select GCN  " &
                                    " FROM Sipendar_profile " &
                                    " where PK_SIPENDAR_PROFILE_ID = '" & PK & "'"

            Dim drGCN As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQLGetGCN)

            If drGCN IsNot Nothing Then
                If Not IsDBNull(drGCN("GCN")) Then
                    Return Convert.ToString(drGCN("GCN"))
                End If
            End If

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''Remark 18-Jan-2022, boleh bikin Report yang sama
    'Public Shared Function CheckReportByUnikReference(UnikReference As String) As Boolean
    '    Try
    '        Using objDB As New SiPendarDAL.SiPendarEntities
    '            Dim objgenerate As SIPENDAR_Report = objDB.SIPENDAR_Report.Where(Function(x) x.UnikReference = UnikReference).FirstOrDefault
    '            If objgenerate IsNot Nothing Then
    '                Return True
    '            Else
    '                Return False
    '            End If
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Private Function GetUnikReferencebyPKProfile(PK As String) As String
        Try

            Dim strSQLGetGCN As String = "select cast (GCN as varchar(500)) + '-' + cast (Sumber_ID as varchar(500)) + '-' + FK_SIPENDAR_SUBMISSION_TYPE_CODE as UnikReference " &
                                    " FROM Sipendar_profile " &
                                    " where PK_SIPENDAR_PROFILE_ID = '" & PK & "'"

            Dim drGCN As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQLGetGCN)

            If drGCN IsNot Nothing Then
                If Not IsDBNull(drGCN("UnikReference")) Then
                    Return Convert.ToString(drGCN("UnikReference"))
                End If
            End If

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
End Class
