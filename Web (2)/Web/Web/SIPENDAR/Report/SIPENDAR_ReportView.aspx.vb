﻿Imports Ext.Net
Imports OfficeOpenXml
Partial Class SIPENDAR_Report_ReportView
    Inherits Parent
    Public objFormModuleView As NawaBLL.FormModuleView
    Public Property strWhereClause() As String
        Get
            Return Session("SIPENDAR_Report_ReportView.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("SIPENDAR_Report_ReportView.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("SIPENDAR_Report_ReportView.strSort")
        End Get
        Set(ByVal value As String)
            Session("SIPENDAR_Report_ReportView.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("SIPENDAR_Report_ReportView.indexStart")
        End Get
        Set(ByVal value As String)
            Session("SIPENDAR_Report_ReportView.indexStart") = value
        End Set
    End Property
    'Update: Zikri 19102020
    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("SIPENDAR_Report_ReportView.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("SIPENDAR_Report_ReportView.ObjModule") = value
        End Set
    End Property
    'End of Update
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub
    <DirectMethod>
    Public Sub BtnAdd_Click()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            'Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")

            '----------------25/01/2020 BSIM
            Dim tempUrl As String = ""

            If Request.Url.ToString().Contains("?") Then

                tempUrl = Request.Url.ToString().Split("?")(1)

            End If

            If String.IsNullOrEmpty(tempUrl) Then
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")
            Else
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?{0}", tempUrl), "Loading")
            End If
            '-----------------------------

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SIPENDAR_Report_ReportView_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer

            Try
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                'Update: Zikri 19102020
                'Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)
                ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                    Dim strIDCode As String = 1
                    BtnAdd.Hidden = True

                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If

                objFormModuleView.ModuleID = ObjModule.PK_Module_ID
                objFormModuleView.ModuleName = ObjModule.ModuleName

                objFormModuleView.AddField("PK_Report_ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.INTValue, , , , , )
                objFormModuleView.AddField("Aging", "Aging", 2, False, True, NawaBLL.Common.MFieldType.INTValue, , , , , )
                objFormModuleView.AddField("submission_date", "Tanggal Laporan", 3, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )
                objFormModuleView.AddField("report_code", "Kode Jenis Laporan", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("Report", "Jenis Laporan", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("submission", "Cara Penyampaian Laporan", 6, False, False, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("transaction_date", "Tanggal Transaksi", 7, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,,)
                objFormModuleView.AddField("TransaksiUnik", "Transaksi Unik", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleView.AddField("ValueTransaksiUnik", "Unik Reference", 9, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleView.AddField("submit_date", "Tanggal Submit", 10, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,,)
                objFormModuleView.AddField("ID_Laporan_PPATK_Result", "ID PPATK Result", 11, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,) '' Added By Felix 20 Nov 2020
                objFormModuleView.AddField("PPATKNo", "No PPATK", 12, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleView.AddField("statusPPATK", "Status", 13, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleView.AddField("isValid", "Valid", 14, False, True, NawaBLL.Common.MFieldType.BooleanValue,,,,, )
                objFormModuleView.AddField("lastUpdateDate", "Tanggal Last Update", 15, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,,)
                objFormModuleView.AddField("statusID", "StatusID", 16, False, False, NawaBLL.Common.MFieldType.INTValue,,,,,)
                objFormModuleView.AddField("ErrorMessage", "Error Message", 17, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleView.SettingFormView()


                Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
                objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"
                AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf GridCommand_DirectEvent 'Update: Zikri 19102020
                'Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")

                'Dim gridCommandResponse As New GridCommand

                'objcommandcol.Width = 200
                'Dim objDrop As New GridCommand
                'objDrop.CommandName = "Delete"
                'objDrop.Icon = Icon.PencilDelete
                'objDrop.Text = "Delete"
                'objDrop.ToolTip.Text = ""
                'objcommandcol.Commands.Add(objDrop)

                'AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf Delete



            Catch ex As Exception
            End Try

            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            Using objdb As New NawaDevDAL.NawaDatadevEntities
                'Begin Penambahan Advanced Filter
                If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                    Toolbar2.Hidden = True
                Else
                    Toolbar2.Hidden = False
                End If
                LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")
                'END Penambahan Advanced Filter

                '' added by Apuy 20200902
                Dim ReportType As String = Request.Params("ReportType")

                '' Added By Felix 09 Jul 2020
                Dim ReportCode As String = Request.Params("ReportCode")
                Dim TanggalTransaksi As String = Request.Params("TanggalTransaksi")
                Dim isValid As String = Request.Params("Valid")
                '' End of Felix 09 Jul 2020

                Dim intStart As Integer = e.Start
                Dim intLimit As Int16 = e.Limit
                Dim inttotalRecord As Integer
                Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

                '' added by Apuy 20200902
                If Not String.IsNullOrEmpty(ReportType) Then
                    If String.IsNullOrEmpty(strfilter) Then
                        strfilter += "ReportType like ('%" & ReportType & "%')"
                    Else
                        strfilter += " AND ReportType like ('%" & ReportType & "%')"
                    End If
                End If

                '' Added By Adi 12 Jul 2021 : Filtering based on GCN dan ReportDate untuk hubungkan ke SIPENDAR Profile
                Dim pkProfileID As String = Request.Params("ProfileID")
                Dim strFilterProfile As String = ""
                If Not String.IsNullOrEmpty(pkProfileID) Then
                    Using objDbSipendar As New SiPendarDAL.SiPendarEntities
                        Dim objProfile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.PK_SIPENDAR_PROFILE_ID = pkProfileID).FirstOrDefault
                        'strFilterProfile = "ValueTransaksiUnik = '" & objProfile.GCN & "' AND DATEDIFF(DAY, submission_date, '" & CDate(objProfile.CreatedDate).ToString("yyyy-MM-dd") & "')=0" '' Edit 17-Dec-2021
                        'strFilterProfile = "ValueTransaksiUnik = '" & objProfile.GCN & "-" & objProfile.SUMBER_ID & "-" & objProfile.FK_SIPENDAR_SUBMISSION_TYPE_CODE & "' AND DATEDIFF(DAY, submission_date, '" & CDate(objProfile.CreatedDate).ToString("yyyy-MM-dd") & "')=0"
                        strFilterProfile = "PK_Report_ID = '" & objProfile.FK_Report_ID & "'"

                        '' Remark 18-Nov-2021, karena Submission_Date balik ke sebelumnya
                        'Dim objWatchlist = objDbSipendar.SIPENDAR_WATCHLIST.Where(Function(x) x.ID_PPATK = objProfile.SUMBER_ID).FirstOrDefault
                        'If objProfile IsNot Nothing And objWatchlist IsNot Nothing Then
                        '    'strFilterProfile = "ValueTransaksiUnik = '" & objProfile.GCN & "' AND DATEDIFF(DAY, submission_date, '" & CDate(objProfile.CreatedDate).ToString("yyyy-MM-dd") & "')=0"

                        '    strFilterProfile = "ValueTransaksiUnik = '" & objProfile.GCN & "' AND DATEDIFF(DAY, submission_date, '" & CDate(objWatchlist.PERIODE).ToString("yyyy-MM-dd") & "')=0"
                        'End If
                    End Using
                End If

                If Not String.IsNullOrEmpty(strFilterProfile) Then
                    If String.IsNullOrEmpty(strfilter) Then
                        strfilter += strFilterProfile
                    Else
                        strfilter += " AND " & strFilterProfile
                    End If
                End If
                '' End of Added By Adi 12 Jul 2021 : Filtering based on GCN dan ReportDate untuk hubungkan ke SIPENDAR Profile

                '  strfilter = strfilter.Replace("Active", objFormModuleView.ModuleName & ".Active")
                '' Added By Felix 09 Jul 2020

                If Not String.IsNullOrEmpty(ReportCode) Then

                    '' added by Apuy 20200902
                    If String.IsNullOrEmpty(strfilter) Then
                        'objFormModuleView.ModuleName = "Testing aja" ''objmodule.ModuleName
                        strfilter += "report_code='" & ReportCode & "' and transaction_date='" & TanggalTransaksi _
                                & "' and IsValid=" & isValid & ""
                    Else
                        strfilter += " AND report_code='" & ReportCode & "' and transaction_date='" & TanggalTransaksi _
                               & "' and IsValid=" & isValid & ""
                    End If

                End If
                '' End of Felix 09 Jul 2020


                Dim strsort As String = ""
                For Each item As DataSorter In e.Sort
                    strsort += item.Property & " " & item.Direction.ToString
                Next
                Me.indexStart = intStart
                Me.strWhereClause = strfilter
                'Fix eror export 'Edited CIMB 15 Oct 2020
                'Me.strOrder = strsort
                If strsort = "" Then
                    strsort = "transaction_date Desc"
                End If


                'Begin Update Penambahan Advance Filter
                If strWhereClause.Length > 0 Then
                    If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                        strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                        intStart = 0
                    End If
                Else
                    If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                        strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                        intStart = 0
                    End If
                End If



                Me.strOrder = strsort

                Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
                'Dim DataPaging As Data.DataTable = DLL.SQLHelper.ExecuteTabelPaging("CustomerInformation_WebTempTable", "CIFNo, Name, DateOfBirth, AccountOwnerId, OpeningDate, WorkingUnitName, IsCustomerInList", strfilter, strsort, intStart, intLimit, inttotalRecord)
                ''-- start paging ------------------------------------------------------------
                Dim limit As Integer = e.Limit
                If (e.Start + e.Limit) > inttotalRecord Then
                    limit = inttotalRecord - e.Start
                End If
                'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
                ''-- end paging ------------------------------------------------------------
                e.Total = inttotalRecord
                GridpanelView.GetStore.DataSource = DataPaging
                GridpanelView.GetStore.DataBind()
            End Using
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Update: Zikri 19102020
    Private Sub GridCommand_DirectEvent(sender As Object, e As DirectEventArgs)
        Dim ID As String = e.ExtraParams(0).Value
        Dim objoperation As String = e.ExtraParams(1).Value
        Dim tempUrl As String = ""
        If Request.Url.ToString().Contains("?") Then
            tempUrl = Request.Url.ToString().Split("?")(1)
        End If


        Select Case objoperation
            Case "Edit"
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlEdit & "?" & tempUrl & "&ID=" & NawaBLL.Common.EncryptQueryString(ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)))
            Case "Delete"
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlDelete & "?" & tempUrl & "&ID=" & NawaBLL.Common.EncryptQueryString(ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)))
            Case "Detail"
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlDetail & "?" & tempUrl & "&ID=" & NawaBLL.Common.EncryptQueryString(ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)))
            Case "Activation"
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlActivation & "?" & tempUrl & "&ID=" & NawaBLL.Common.EncryptQueryString(ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)))
        End Select
    End Sub
    'End of Update
    '<DirectMethod>
    'Public Sub BtnAdvancedFilter_Click()
    '    Try

    '        Dim Moduleid As String = Request.Params("ModuleID")

    '        Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
    '        Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

    '        AdvancedFilter1.TableName = "Web_ValidationReport_RowComplate"
    '        AdvancedFilter1.objListModuleField = Nothing

    '        Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
    '        objwindow.Hidden = False
    '        AdvancedFilter1.BindData()
    '        ' AdvancedFilter1.ClearFilter()
    '        AdvancedFilter1.StoreToReleoad = "StoreView"
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    'Begin Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update Penambahan Advance Filter
End Class
