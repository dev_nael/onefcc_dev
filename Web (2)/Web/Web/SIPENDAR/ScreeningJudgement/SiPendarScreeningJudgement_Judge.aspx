﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="SiPendarScreeningJudgement_Judge.aspx.vb" Inherits="SiPendarScreeningJudgement_Judge" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        var updateMask = function (text) {
            App.df_ScreeningStatus.setValue(text);
        };
    </script>
    <style type="text/css">
        .text-wrapper .x-form-display-field {
    	    word-break: break-word;
    	    word-wrap: break-word;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <%-- Pop Up Window Report Indicator Ari 15-09-21 --%>
      <ext:Window ID="WindowReportIndicator" Layout="AnchorLayout" Title="Indikator" runat="server" Modal="true" Hidden="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
           <%-- <ext:ComboBox ID="sar_reportIndicator" runat="server" FieldLabel="Indikator" DisplayField="Keterangan" ValueField="Kode" EmptyText="Pilih Salah Satu" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreReportIndicator" OnReadData="ReportIndicator_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server" ID="Model356">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
            </ext:ComboBox>--%>
            <ext:Panel ID="Pnl_cmbW_ReportIndicator" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField ID="cmbW_ReportIndicator" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="vw_SIPENDAR_ref_indicator_laporan" Label="Indikator" AnchorHorizontal="100%" />
                </Content>
            </ext:Panel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.4, height: size.height * 0.4});" />

            <Resize Handler="#{WindowReportIndicator}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="BtnsaveReportIndicator" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">

                <DirectEvents>
                    <Click OnEvent="btnSaveReportIndicator_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancelReportIndicator" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancelreportIndicator_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
	<%-- End Pop Up Window Report Indicator Ari 15-09-21 --%>

    <ext:FormPanel ID="FormPanelInput" BodyPadding="5" AutoScroll="true" Layout="AnchorLayout" AnchorHorizontal="90%" ButtonAlign="Center" runat="server" Height="300" Title="">
        <Items>
             <ext:DisplayField ID="nontesnoncustomer" runat="server" Text="Notes : Untuk Data Non Customer Sipendar Type hanya bisa Proaktif." MarginSpec="10 0 0 10" Hidden="true"></ext:DisplayField>
            <%--Added by Felix 16-Sep-2021--%>
                <%--<ext:Checkbox ID="cb_IsSelectAllResult" runat="server" FieldLabel="Pilih Semua Result" LabelWidth="200" AnchorHorizontal="100%"/>--%>
            <%--ENd of 16-Sep-2021--%>
            
            <ext:Panel runat="server" ID="PanelScreeningResult" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE"  Title="Screening Result" BodyStyle="padding:10px"  Collapsible="true">
                <Items>
                    <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true">
                        <Items>
                            <ext:Checkbox ID="sar_IsSelectedAll_Result" runat="server" FieldLabel="Pilih Semua Result " LabelWidth="150" AnchorHorizontal="50%" Margin="10"/>
                            <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export " labelStyle="width:80px" AnchorHorizontal="50%" MarginSpec="0 0 0 30" >
                                <Items>
                                    <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                                    <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                                </Items>
                            </ext:ComboBox>
                            <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" MarginSpec="0 0 0 10" >
                                <DirectEvents>
                                    <Click OnEvent="ExportExcel" IsUpload="true">
                                        <ExtraParams>
                                            <ext:Parameter Name="currentPage" Value="App.gp_ScreeningResult.getStore().currentPage" Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page"   MarginSpec="0 0 0 10">
                                <DirectEvents>
                                    <Click OnEvent="ExportAllExcel" IsUpload="true"/>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="BtnExportSelected" Text="Export Selected Row"  MarginSpec="0 0 0 10">
                                <DirectEvents>
                                    <Click OnEvent="ExportSelectedExcel" IsUpload="true"/>
                                </DirectEvents>
                            </ext:Button>
                        </items>
                    </ext:Toolbar>

                    <%--Grid Screening Result--%>
                    <ext:GridPanel ID="gp_ScreeningResult" runat="server" Border="true" MarginSpec="10 0" MinHeight="250" ClientIDMode="Static">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store1" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="model1" IDProperty="PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID" Type="String" ></ext:ModelField>
                                            <ext:ModelField Name="Nama" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DOB" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_WATCHLIST_ID_PPATK" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="GCN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMACUSTOMER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DOBCustomer" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Birth_PlaceCustomer" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_NumberCustomer" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Submission_Type" Type="String"></ext:ModelField>
                                            <%-- 14-Oct-2021 Daniel : Sementara d Comment karena belum terpakai --%>
                                            <%--<ext:ModelField Name="_Similarity_Nama" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="_Similarity_DOB" Type="Float"></ext:ModelField>--%>
                                            <ext:ModelField Name="TotalSimilarityPct" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Type="String"></ext:ModelField>
                                            <%-- 19-Oct-2021 Daniel : Penambahan Stakeholder_Role dan CustomerInformation--%>
                                            <ext:ModelField Name="Stakeholder_Role" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="customerinformation" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ClosedDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                            <%-- 14-Oct-2021 Daniel : Sementara d Comment karena belum terpakai --%>
                                            <%--<ext:ModelField Name="IsClosed" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ClosedDate" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="Account_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Account_No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIF_No" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                            <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                            <ext:Column ID="Column1" runat="server" Text="Data Searched">
                                <Columns>
                                    <ext:Column ID="Column2" runat="server" DataIndex="Nama" Text="Name" MinWidth="200"></ext:Column>
                                    <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="DOB" Text="DOB" MinWidth="120" format="dd-MMM-yyyy"></ext:DateColumn>
                                    <ext:Column ID="Column6" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column55" runat="server" DataIndex="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Text="ID Type" MinWidth="100"></ext:Column>
                                    <ext:Column ID="Column3" runat="server" DataIndex="Identity_Number" Text="Identity Number" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column13" runat="server" DataIndex="FK_WATCHLIST_ID_PPATK" Text="ID Watchlist<br>PPATK" MinWidth="70"></ext:Column>
                                    <ext:Column ID="Column4" runat="server" DataIndex="Submission_Type" Text="Submission Type" Flex="1"></ext:Column>
                                </Columns>
                            </ext:Column>

                            <ext:Column ID="Column5" runat="server" Text="Data Found">
                                <Columns>
                                    <ext:Column ID="Column63" runat="server" DataIndex="customerinformation" Text="Customer / Non-Customer" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column56" runat="server" DataIndex="CIF" Text="CIF" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column73" runat="server" DataIndex="CIF_No" Text="Link To CIF" Flex="1"></ext:Column>
                                    <ext:Column ID="Column72" runat="server" DataIndex="Account_No" Text="Link To Account" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column74" runat="server" DataIndex="Account_Name" Text="Link To Account Name" Flex="1"></ext:Column>
                                    <ext:Column ID="Column14" runat="server" DataIndex="GCN" Text="GCN / Unique Key" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column15" runat="server" DataIndex="NAMACUSTOMER" Text="Name" MinWidth="200"></ext:Column>
                                    <ext:DateColumn ID="DateColumn3" runat="server" DataIndex="DOBCustomer" Text="DOB" MinWidth="120" format="dd-MMM-yyyy"></ext:DateColumn>
                                    <ext:Column ID="Column16" runat="server" DataIndex="Birth_PlaceCustomer" Text="Birth Place" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column17" runat="server" DataIndex="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer" Text="ID Type" MinWidth="100"></ext:Column>
                                    <ext:Column ID="Column19" runat="server" DataIndex="Identity_NumberCustomer" Text="Identity Number" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column64" runat="server" DataIndex="Stakeholder_Role" Text="Non-Customer Role" MinWidth="150"></ext:Column>
                                    <%-- 1-Oct-2021 Adi : Sementara tidak ditampilkan di sini karena butuh waktu minimal 4 second untuk menampilkan ini per GCN --%>
                                    <%-- Test case bareng BSIM dgn data 4 juta customer dan 6 juta account --%>
                                    <%--<ext:Column ID="Column56" runat="server" DataIndex="IsClosed" Text="Customer Closed?" MinWidth="140"></ext:Column>
                                    <ext:Column ID="Column62" runat="server" DataIndex="ClosedDate" Text="Closing Date" MinWidth="100"></ext:Column>--%>
                                </Columns>
                            </ext:Column>

                            <ext:NumberColumn ID="NumberColumn1" runat="server" DataIndex="TotalSimilarityPct" Text="Similarity" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                            <ext:DateColumn ID="DateColumn5" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="200" format="dd-MMM-yyyy HH:mm:ss"></ext:DateColumn>
                            <ext:CommandColumn ID="gp_ScreeningResult_CommandColumn" runat="server" Text="Action" CellWrap="true">
                                <Commands>
                                    <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                        <ToolTip Text="Detail Data"></ToolTip>
                                    </ext:GridCommand>
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gp_ScreeningResult_Gridcommand">
                                        <EventMask ShowMask="true"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="record.data.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                            </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:CheckboxSelectionModel runat="server" Mode="Multi" CheckOnly="true">
                                <DirectEvents>
                                    <SelectionChange OnEvent="sm_ScreeningResult_change">
                                    </SelectionChange>
                                </DirectEvents>
                            </ext:CheckboxSelectionModel>
                        </SelectionModel>
                        <Plugins>
                            <%--<ext:FilterHeader runat="server"></ext:FilterHeader>--%>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="false" >
                                <Items>  
                                    <%-- <ext:Button runat="server" Text="Close Picker" ID="Button5">
                                    <Listeners>
                                    <Click Handler="#{window_SEARCH_CUSTOMER}.hide()"> </Click>
                                    </Listeners>
                                    </ext:Button>--%>
                                    <ext:Label runat="server" ID="LabelScreeningResultCount">

                                    </ext:Label>
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
                    </ext:GridPanel>
                    <%--Grid Screening Result--%>
                    
                    <%--Grid Screening Result All--%>
                    <ext:GridPanel ID="gp_ScreeningResultAll" runat="server" Border="true" MarginSpec="10 0" Hidden="true">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store3" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="model2" IDProperty="PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID" Type="String" ></ext:ModelField>
                                            <ext:ModelField Name="Nama" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DOB" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_WATCHLIST_ID_PPATK" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="GCN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMACUSTOMER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DOBCustomer" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Birth_PlaceCustomer" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_NumberCustomer" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Submission_Type" Type="String"></ext:ModelField>
                                            <%-- 14-Oct-2021 Daniel : Sementara d Comment karena belum terpakai --%>
                                            <%--<ext:ModelField Name="_Similarity_Nama" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="_Similarity_DOB" Type="Float"></ext:ModelField>--%>
                                            <ext:ModelField Name="TotalSimilarityPct" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Type="String"></ext:ModelField>
                                            <%-- 19-Oct-2021 Daniel : Penambahan Stakeholder_Role dan CustomerInformation--%>
                                            <ext:ModelField Name="Stakeholder_Role" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="customerinformation" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ClosedDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                            <%-- 14-Oct-2021 Daniel : Sementara d Comment karena belum terpakai --%>
                                            <%--<ext:ModelField Name="IsClosed" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ClosedDate" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="Account_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Account_No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIF_No" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                            <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                            <ext:Column ID="Column7" runat="server" Text="Data Searched">
                                <Columns>
                                    <ext:Column ID="Column8" runat="server" DataIndex="Nama" Text="Name" MinWidth="200"></ext:Column>
                                    <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="DOB" Text="DOB" MinWidth="120" format="dd-MMM-yyyy"></ext:DateColumn>
                                    <ext:Column ID="Column9" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column10" runat="server" DataIndex="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Text="ID Type" MinWidth="100"></ext:Column>
                                    <ext:Column ID="Column11" runat="server" DataIndex="Identity_Number" Text="Identity Number" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column12" runat="server" DataIndex="FK_WATCHLIST_ID_PPATK" Text="ID Watchlist<br>PPATK" MinWidth="70"></ext:Column>
                                    <ext:Column ID="Column18" runat="server" DataIndex="Submission_Type" Text="Submission Type" Flex="1"></ext:Column>
                                </Columns>
                            </ext:Column>

                            <ext:Column ID="Column20" runat="server" Text="Data Found">
                                <Columns>
                                    <ext:Column ID="Column21" runat="server" DataIndex="customerinformation" Text="Customer / Non-Customer" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column22" runat="server" DataIndex="CIF" Text="CIF" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column23" runat="server" DataIndex="CIF_No" Text="Link To CIF" Flex="1"></ext:Column>
                                    <ext:Column ID="Column24" runat="server" DataIndex="Account_No" Text="Link To Account" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column25" runat="server" DataIndex="Account_Name" Text="Link To Account Name" Flex="1"></ext:Column>
                                    <ext:Column ID="Column26" runat="server" DataIndex="GCN" Text="GCN / Unique Key" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column27" runat="server" DataIndex="NAMACUSTOMER" Text="Name" MinWidth="200"></ext:Column>
                                    <ext:DateColumn ID="DateColumn4" runat="server" DataIndex="DOBCustomer" Text="DOB" MinWidth="120" format="dd-MMM-yyyy"></ext:DateColumn>
                                    <ext:Column ID="Column28" runat="server" DataIndex="Birth_PlaceCustomer" Text="Birth Place" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column29" runat="server" DataIndex="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer" Text="ID Type" MinWidth="100"></ext:Column>
                                    <ext:Column ID="Column30" runat="server" DataIndex="Identity_NumberCustomer" Text="Identity Number" MinWidth="150"></ext:Column>
                                    <ext:Column ID="Column31" runat="server" DataIndex="Stakeholder_Role" Text="Non-Customer Role" MinWidth="150"></ext:Column>
                                    <%-- 1-Oct-2021 Adi : Sementara tidak ditampilkan di sini karena butuh waktu minimal 4 second untuk menampilkan ini per GCN --%>
                                    <%-- Test case bareng BSIM dgn data 4 juta customer dan 6 juta account --%>
                                    <%--<ext:Column ID="Column56" runat="server" DataIndex="IsClosed" Text="Customer Closed?" MinWidth="140"></ext:Column>
                                    <ext:Column ID="Column62" runat="server" DataIndex="ClosedDate" Text="Closing Date" MinWidth="100"></ext:Column>--%>
                                </Columns>
                            </ext:Column>

                            <ext:NumberColumn ID="NumberColumn2" runat="server" DataIndex="TotalSimilarityPct" Text="Similarity" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                            <ext:DateColumn ID="DateColumn6" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="200" format="dd-MMM-yyyy HH:mm:ss"></ext:DateColumn>
                            <ext:CommandColumn ID="gp_ScreeningResult_All_CommandColumn" runat="server" Text="Action" CellWrap="true">
                                <Commands>
                                    <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                        <ToolTip Text="Detail Data"></ToolTip>
                                    </ext:GridCommand>
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gp_ScreeningResult_Gridcommand">
                                        <EventMask ShowMask="true"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="record.data.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                            </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                        </SelectionModel>
                        <Plugins>
                        <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_ScreeningResultAll" runat="server" HideRefresh="True">
                            </ext:PagingToolbar>
                    
                        </BottomBar>
                    </ext:GridPanel>
                    <%--Grid Screening Result All--%>
                </Items>
            </ext:Panel>


            <%--Added by Felix 16-Sep-2021--%>
            <%--End of 16-Sep-2021--%>

            <%-- SiPendar Profile General Information (Harus dilengkapi kalau mau dilaporkan) --%>
            <ext:Panel runat="server" Title="SiPendar Report General Information" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlSiPendarReport" Border="true" BodyStyle="padding:10px" Hidden="false">
                <Content>

                    <NDS:NDSDropDownField ID="cmb_SIPENDAR_TYPE_ID" LabelWidth="250" ValueField="PK_SIPENDAR_TYPE_id" DisplayField="SIPENDAR_TYPE_NAME" runat="server" StringField="PK_SIPENDAR_TYPE_id, SIPENDAR_TYPE_NAME" StringTable="SIPENDAR_TYPE" Label="SiPendar Report Type" AnchorHorizontal="80%" AllowBlank="false" OnOnValueChanged="SetControlVisibility"/>
                    <NDS:NDSDropDownField ID="cmb_JENIS_WATCHLIST_CODE" LabelWidth="250" ValueField="SIPENDAR_JENIS_WATCHLIST_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_JENIS_WATCHLIST_CODE, KETERANGAN" StringTable="SIPENDAR_JENIS_WATCHLIST" Label="Watchlist Category" AnchorHorizontal="80%" AllowBlank="false"/>
                    <NDS:NDSDropDownField ID="cmb_TINDAK_PIDANA_CODE" LabelWidth="250" ValueField="SIPENDAR_TINDAK_PIDANA_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_TINDAK_PIDANA_CODE, KETERANGAN" StringTable="SIPENDAR_TINDAK_PIDANA" Label="Jenis Tindak Pidana" AnchorHorizontal="80%" AllowBlank="false" IsHidden="true"/>
                    <NDS:NDSDropDownField ID="cmb_SUMBER_INFORMASI_KHUSUS_CODE" LabelWidth="250" ValueField="SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE, KETERANGAN" StringTable="SIPENDAR_SUMBER_INFORMASI_KHUSUS" Label="Sumber Informasi Khusus" AnchorHorizontal="80%" AllowBlank="false"/>
                    <NDS:NDSDropDownField ID="cmb_SUMBER_TYPE_CODE" LabelWidth="250" ValueField="SUMBER_TYPE_CODE" DisplayField="SUMBER_TYPE_NAME" runat="server" StringField="SUMBER_TYPE_CODE, SUMBER_TYPE_NAME" StringTable="SIPENDAR_SUMBER_TYPE" Label="Sumber Type" AnchorHorizontal="80%" AllowBlank="false" IsHidden="true"/>
                    <ext:NumberField ID="txt_SUMBER_ID" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Sumber ID" AnchorHorizontal="80%" FormatText="#,##0" Hidden="true" MaxValue="2147483646"></ext:NumberField>
                    <NDS:NDSDropDownField ID="cmb_SUBMISSION_TYPE_CODE" LabelWidth="250" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="vw_SIPENDAR_SUBMISSION_TYPE" Label="Submission Type" AnchorHorizontal="80%" AllowBlank="false" visible="false"/>
                    <ext:TextArea ID="txt_Keterangan" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Keterangan" AnchorHorizontal="80%" MaxLength="500" EnforceMaxLength="true"></ext:TextArea>

                    <ext:DisplayField runat="server" ID="df_blank"></ext:DisplayField>

                    <ext:FieldSet runat="server" ID="fs_Generate_Transaction" Border="true" Title="Generate Transaction" Hidden="true" Layout="AnchorLayout" Padding="10">
                        <Content>
                            <ext:Checkbox ID="chk_IsGenerateTransaction" LabelWidth="250" runat="server" FieldLabel="Include Generate Transaction">
                                <DirectEvents>
                                    <Change OnEvent="chk_IsGenerateTransaction_Change"></Change>
                                </DirectEvents>
                            </ext:Checkbox>
                            <ext:DateField ID="txt_Transaction_DateFrom" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Transaction Date From" AnchorHorizontal="50%" Format="dd-MMM-yyyy" Hidden="true"></ext:DateField>
                            <ext:DateField ID="txt_Transaction_DateTo" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Transaction Date To" AnchorHorizontal="50%" Format="dd-MMM-yyyy" Hidden="true"></ext:DateField>
                        </Content>
                    </ext:FieldSet>
					<%-- Panel Report Indicator Ari 15-09-21 --%>
                    <ext:Panel runat="server" ID="PanelReportIndicator" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" Border="true" BodyStyle="padding:10px" Hidden="true" Title="Indikator" Margin="10">
	                <Items>
	                    <ext:GridPanel ID="gp_ReportIndicator" runat="server" EmptyText="No Available Data">
	                        <View>
	                            <ext:GridView runat="server" EnableTextSelection="true" />
	                        </View>
	                        <TopBar>
	                            <ext:Toolbar runat="server">
	                                <Items>
	                                    <ext:Button runat="server" ID="BtnAddReportIndikator" Text="Tambah Indicator" Icon="Add">
	                                        <DirectEvents>
	                                            <Click OnEvent="btn_addIndicator_click">
	                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="500"></EventMask>
	                                            </Click>
	                                        </DirectEvents>
	                                    </ext:Button>
	                                </Items>
	                            </ext:Toolbar>
	                        </TopBar>
	                        <Store>
	                            <ext:Store ID="StoreReportIndicatorData" runat="server">
	                                <Model>
	                                    <ext:Model runat="server" ID="Model355" IDProperty="PK_Report_Indicator">
	                                        <Fields>
	                                            <ext:ModelField Name="PK_Report_Indicator" Type="Int"></ext:ModelField>
	                                            <ext:ModelField Name="FK_Indicator" Type="String"></ext:ModelField>
	                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
	                                        </Fields>
	                                    </ext:Model>
	                                </Model>
	                            </ext:Store>
	                        </Store>
	                        <ColumnModel>
	                            <Columns>
	                                <ext:RowNumbererColumn ID="RowNumbererColumn88" runat="server" Text="No"></ext:RowNumbererColumn>
	                                <ext:Column ID="Column312" runat="server" DataIndex="FK_Indicator" Text="Kode" Flex="1"></ext:Column>
	                                <ext:Column ID="Column313" runat="server" DataIndex="Keterangan" Text="Keterangan" Flex="1"></ext:Column>
	                                <ext:CommandColumn ID="cc_ReportIndicator" runat="server" Text="Action" Flex="1">

	                                    <Commands>
	                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
	                                            <ToolTip Text="Edit"></ToolTip>
	                                        </ext:GridCommand>
	                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
	                                            <ToolTip Text="Detail"></ToolTip>
	                                        </ext:GridCommand>
	                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationViewDetail" MinWidth="70">
	                                            <ToolTip Text="Delete"></ToolTip>
	                                        </ext:GridCommand>
	                                    </Commands>

	                                    <DirectEvents>

	                                        <Command OnEvent="GridcommandReportIndicator">
	                                            <EventMask ShowMask="true"></EventMask>
	                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>

	                                            <ExtraParams>
	                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Report_Indicator" Mode="Raw"></ext:Parameter>
	                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
	                                            </ExtraParams>
	                                        </Command>
	                                    </DirectEvents>
	                                </ext:CommandColumn>



		                            </Columns>
		                        </ColumnModel>
		                    </ext:GridPanel>
		                </Items>
	            	</ext:Panel>
					<%-- End Panel Report Indicator Ari 15-09-21 --%>
                 </Content>

            </ext:Panel>
                  
            <%-- End of SiPendar Profile General Information --%>

            <%--<ext:Panel ID="panel_TaskManager" runat="server" MarginSpec="10 0" Border="true" BodyPadding="10" Hidden="true">
                <Content>
                    <ext:TaskManager ID="TaskManager1" runat="server">
                        <Tasks>
                            <ext:Task
                                TaskID="refreshScreeningStatus"
                                Interval="1000"
                                AutoRun="false"
                                >
                                <DirectEvents>
                                    <Update OnEvent="Screening_CheckStatus">--%>
                                        <%--<EventMask Msg="Loading..." ShowMask="true" MinDelay="200"></EventMask>--%>
                                   <%-- </Update>
                                </DirectEvents>
                            </ext:Task>
                        </Tasks>
                    </ext:TaskManager>
                </Content>
            </ext:Panel>--%>

            <%-- Update 20210921 Felix : Penambahan Approval History and Note --%>
            <ext:Panel runat="server" ID="panelApprovalHistory" ClientIDMode="Static" Title="Approval" Layout="AnchorLayout" Border="true" Collapsible="true" BodyStyle="padding:10px" ComponentCls="xPanelContainer" Hidden="true">
                <Items>
                    <ext:GridPanel ID="gp_history" runat="server" MarginSpec="0 0 20 0" Title="Approval History">
                        <Store>
                            <ext:Store ID="gpStore_history" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" IDProperty="PK_TX_IFTI_Flow_History_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_TX_IFTI_Flow_History_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Module_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UnikID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="UserID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UserName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Notes" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>

                        <ColumnModel runat="server">
                            <Columns>
                                <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="70"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="User Name" DataIndex="UserName" CellWrap="true" Flex="1" />
                                <ext:Column runat="server" Text="Status" DataIndex="Status" CellWrap="true" Flex="1" />
                                <ext:DateColumn runat="server" Text="Created Date" DataIndex="CreatedDate" CellWrap="true" Flex="1" Format="dd-MMM-yyyy hh:mm:ss" />
                                <ext:Column runat="server" Text="Notes" DataIndex="Notes" CellWrap="true" Flex="4" />
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar24" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            <%-- End of Update 20210921 Felix : Penambahan Approval History and Note --%>

        </Items>
        <Buttons>
            <%--<ext:Button ID="btn_Screening" ClientIDMode="Static" runat="server" Text="Start Screening" Enabled="false" Icon="PlayGreen">
                <DirectEvents>
                    <Click OnEvent="btn_Screening_Click">--%>
                        <%--<EventMask ShowMask="true"></EventMask>--%>
                   <%-- </Click>
                </DirectEvents>
            </ext:Button>--%>
            <%--Added on 23 Aug 2021--%>
             <ext:Button ID="btn_Exclude" ClientIDMode="Static" runat="server" Text="Judge as Not Match" Enabled="false" Icon="BulletCross" Hidden="false">
                <DirectEvents>
                    <Click OnEvent="btn_Exclude_Click">
                        <EventMask ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <%--End of 23 Aug 2021--%>
            <%--<ext:Button ID="btn_Save" ClientIDMode="Static" runat="server" Text="Save Selected to SiPendar Report" Enabled="false" Icon="DiskBlack" Hidden="false">--%>
            <ext:Button ID="btn_Save" ClientIDMode="Static" runat="server" Text="Save to SiPendar Report" Enabled="false" Icon="DiskBlack" Hidden="false">
                <DirectEvents>
                    <Click OnEvent="btn_Save_Click">
                        <EventMask ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Cancel" runat="server" Text="Cancel" Icon="PictureEmpty">
                <DirectEvents>
                    <%--<Click OnEvent="btn_Cancel_Click"></Click>--%>
                    <Click OnEvent="btn_SIPENDAR_JUDGEMENT_Back_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="Accept">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    
    <ext:Window runat="server" ID="WindowScreeningDetail" Title="Screening Detail" Modal="true" Layout="ColumnLayout" ButtonAlign="Center" Hidden="true" AutoScroll="true" Scrollable="Vertical" Maximizable="true">
        <Items>
            <ext:FieldSet runat="server" ID="FieldSet0" Border="false" Layout="AnchorLayout" ColumnWidth="1" Padding="10" MarginSpec="10 0">
                <Content>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_Similarity" FieldLabel="Similarity" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_IsHaveActiveAccount" FieldLabel="Customer Account Status" LabelWidth="175"></ext:DisplayField>
                     <ext:DisplayField runat="server" ID="WindowScreeningRoles" FieldLabel="Customer / NonCustomer" LabelWidth="175" Hidden="true"></ext:DisplayField>
                 <ext:DisplayField runat="server" ID="WindowScreeningDetail_SubmissionType" FieldLabel="Submission Type" LabelWidth="175"></ext:DisplayField> <%--Add 20-Dec-2021--%>
                </Content>
            </ext:FieldSet>
            <ext:FieldSet runat="server" ID="FieldSet1" Border="true" Layout="AnchorLayout" ColumnWidth="0.45" Padding="10" MarginSpec="0 0 0 10" AutoScroll="true" Title="Search Data">
                <Content>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_IDWatchlistPPATK" FieldLabel="ID Watch List PPATK" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_NameSearch" FieldLabel="Name Search" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_DOBSearch" FieldLabel="Birth Date Search" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_POBSearch" FieldLabel="Birth Place Search" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_IdentityTypeSearch" FieldLabel="Identity Type Search" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_IdentityNumberSearch" FieldLabel="Identity Number Search" LabelWidth="175" ></ext:DisplayField>

                    <%-- 29-Sep-2021 Adi : Penambahan list of Identity Watchlist --%>
                    <ext:GridPanel ID="gp_IdentityWatchlist" runat="server" EmptyText="No Available Data" Hidden="true" Title="List of Identity">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model5">
                                        <Fields>
                                            <ext:ModelField Name="Identity_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column58" runat="server" DataIndex="Identity_Type" Text="ID Type" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column59" runat="server" DataIndex="Identity_Number" Text="ID Number" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                    <%-- End of 29-Sep-2021 Adi : Penambahan list of Identity Watchlist --%>
                </Content>
            </ext:FieldSet>
            <ext:FieldSet runat="server" ID="FieldSet2" Border="true" Layout="AnchorLayout" ColumnWidth="0.5" Padding="10" MarginSpec="0 10 0 10" AutoScroll="true" Title="Customer Data">
                <Content>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_GCN" FieldLabel="GCN" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_CustomerName" FieldLabel="Name" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_CustomerDOB" FieldLabel="Birth Date" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_CustomerPOB" FieldLabel="Birth Place" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_CustomerIdentityType" FieldLabel="Customer Identity Type" LabelWidth="175" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_CustomerIdentityNumber" FieldLabel="Customer Identity Number" LabelWidth="175" ></ext:DisplayField>
                  <ext:DisplayField runat="server" ID="WindowScreeningUniqueKey" FieldLabel="Unique Key" LabelWidth="175" hidden="true" ></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningName" FieldLabel="Name" LabelWidth="175" Cls="text-wrapper" hidden="true"></ext:DisplayField>
                          <ext:DisplayField runat="server" ID="WindowScreeningBirthDate" FieldLabel="Birth Date" LabelWidth="175" hidden="true"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningBirthPlace" FieldLabel="Birth Place" LabelWidth="175" hidden="true"></ext:DisplayField>
              
                          

                    <%-- 29-Sep-2021 Adi : Penambahan list of Identity Customer --%>
                    <ext:GridPanel ID="gp_IdentityCustomer" runat="server" EmptyText="No Available Data" Hidden="true" Title="List of Identity">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store4" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model6">
                                        <Fields>
                                            <ext:ModelField Name="Identity_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column60" runat="server" DataIndex="Identity_Type" Text="ID Type" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column61" runat="server" DataIndex="Identity_Number" Text="ID Number" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                            <%-- 19-Nov-2021 Ari : Penambahan list of Link CIF --%>
                    <ext:GridPanel ID="GridPanelLinkCIF" runat="server" EmptyText="No Available Data" Hidden="true" Title="Link To CIF / Account">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store5" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model7">
                                        <Fields>
                                            <ext:ModelField Name="Account_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Account_No" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="CIF_No" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column69" runat="server" DataIndex="Account_No" Text="Account No" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column70" runat="server" DataIndex="CIF_No" Text="CIF No" Flex="1"></ext:Column>
                                 <ext:Column ID="Column71" runat="server" DataIndex="Account_Name" Text="Account Name" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- End of 29-Sep-2021 Adi : Penambahan list of Identity Customer --%>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_EmptySpace" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_CIF" FieldLabel="CIF" LabelWidth="175" Cls="text-wrapper"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningRole" FieldLabel="Role" LabelWidth="175" hidden="true"></ext:DisplayField>
                </Content>
            </ext:FieldSet>
        </Items>
        <Buttons>
            <ext:Button runat="server" ID="WindowScreeningDetail_Close" Text="Close" Icon="Cancel" >
                <DirectEvents>
                    <Click OnEvent="WindowScreeningDetail_Close_Click">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="100"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.8, height: size.height * 0.8});" />
            <Resize Handler="#{WindowScreeningDetail}.center()" />
        </Listeners>
    </ext:Window>
</asp:Content>