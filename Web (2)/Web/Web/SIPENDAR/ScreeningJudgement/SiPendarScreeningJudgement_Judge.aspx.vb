﻿Imports System.Data
Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Drawing
Imports System.IO
Imports System.Reflection
Imports System.Threading
Imports Elmah
Imports OfficeOpenXml
Imports OfficeOpenXml.Style
Imports SiPendarBLL


Partial Class SiPendarScreeningJudgement_Judge
    Inherits ParentPage

    Const PATH_TEMP_DIR As String = "~\temp\ScreeningTemplate\"

    Public Property intID() As String
        Get
            Return Session("SiPendarScreeningJudgement_Judge.intID")
        End Get
        Set(ByVal value As String)
            Session("SiPendarScreeningJudgement_Judge.intID") = value
        End Set
    End Property

    Public Property IDModule() As String
        Get
            Return Session("SiPendarProfile_Add.IDModule")
        End Get
        Set(ByVal value As String)
            Session("SiPendarProfile_Add.IDModule") = value
        End Set
    End Property

    Public Property ScreeningStatus() As String
        Get
            Return Session("SiPendarScreeningJudgement_Judge.ScreeningStatus")
        End Get
        Set(ByVal value As String)
            Session("SiPendarScreeningJudgement_Judge.ScreeningStatus") = value
        End Set
    End Property

    Public Property objSIPENDAR_SCREENING_REQUEST() As SiPendarDAL.SIPENDAR_SCREENING_REQUEST
        Get
            Return Session("SiPendarScreeningJudgement_Judge.objSIPENDAR_SCREENING_REQUEST")
        End Get
        Set(ByVal value As SiPendarDAL.SIPENDAR_SCREENING_REQUEST)
            Session("SiPendarScreeningJudgement_Judge.objSIPENDAR_SCREENING_REQUEST") = value
        End Set
    End Property

    Public Property listSIPENDAR_SCREENING_REQUEST_DETAIL() As List(Of SiPendarDAL.SIPENDAR_SCREENING_REQUEST_DETAIL)
        Get
            Return Session("SiPendarScreeningJudgement_Judge.listSIPENDAR_SCREENING_REQUEST_DETAIL")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_SCREENING_REQUEST_DETAIL))
            Session("SiPendarScreeningJudgement_Judge.listSIPENDAR_SCREENING_REQUEST_DETAIL") = value
        End Set
    End Property

    Public Property objSIPENDAR_SCREENING_REQUEST_DETAIL_Edit() As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_DETAIL
        Get
            Return Session("SiPendarScreeningJudgement_Judge.objSIPENDAR_SCREENING_REQUEST_DETAIL_Edit")
        End Get
        Set(ByVal value As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_DETAIL)
            Session("SiPendarScreeningJudgement_Judge.objSIPENDAR_SCREENING_REQUEST_DETAIL_Edit") = value
        End Set
    End Property

    ' Added by Felix 16-Sep-2021
    Public Property TotalResult As Integer
        Get
            Return Session("SiPendarScreeningJudgement_Judge.TotalResult")
        End Get
        Set(value As Integer)
            Session("SiPendarScreeningJudgement_Judge.TotalResult") = value
        End Set
    End Property

    Public Property dt_ScreeningResult() As DataTable
        Get
            Return Session("SiPendarScreeningJudgement_Judge.dt_ScreeningResult")

        End Get
        Set(ByVal value As DataTable)
            Session("SiPendarScreeningJudgement_Judge.dt_ScreeningResult") = value
        End Set

    End Property
    ' End of 16-Sep-2021

    Private Sub SiPendarScreening_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        Me.ActionType = NawaBLL.Common.ModuleActionEnum.view
    End Sub

    Private Sub SiPendarScreening_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = Me.ObjModule.ModuleLabel

                Dim ModuleID As String = Request.Params("ModuleID")
                IDModule = NawaBLL.Common.DecryptQueryString(ModuleID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                'Clear Session
                ClearSession()
                'Date Format sesuai System Parameter
                'txt_DOB.Format = NawaBLL.SystemParameterBLL.GetDateFormat
                'sar_reportIndicator.FieldStyle = "background-color: #FFE4C4"  'Ari 15-09-2021 set warna dari dropdown di indikator
                ''Edit 30-Sep-2021 Felix , Ganti NDSDropdown
                cmbW_ReportIndicator.AllowBlank = False

                'Set command column location (Left/Right)
                SetCommandColumnLocation() 'Ari 15-09-2021

                'Show Result
                Show_ScreeningResult()

                'Show History Approval
                LoadApprovalHistoryJudgement()
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ClearSession()
        Try
            objSIPENDAR_SCREENING_REQUEST = New SiPendarDAL.SIPENDAR_SCREENING_REQUEST
            listSIPENDAR_SCREENING_REQUEST_DETAIL = New List(Of SiPendarDAL.SIPENDAR_SCREENING_REQUEST_DETAIL)

            ListIndicator = New List(Of SiPendarDAL.SIPENDAR_Report_Indicator) 'Ari 15-09-2021 clear data untuk listindicator
            bindReportIndicator(StoreReportIndicatorData, ListIndicator)

            objSIPENDAR_SCREENING_REQUEST_DETAIL_Edit = Nothing

            ''Added by Felix 16-Sep-2021
            dt_ScreeningResult = New DataTable
            gp_ScreeningResult.GetStore.DataSource = dt_ScreeningResult
            gp_ScreeningResult.GetStore.DataBind()

            gp_ScreeningResultAll.GetStore.DataSource = dt_ScreeningResult
            gp_ScreeningResultAll.GetStore.DataBind()
            TotalResult = 0
            '' End of 16-Sep-2021


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "General"
    Protected Sub btn_Cancel_Click()
        Try
            'Clear Screening Request
            listSIPENDAR_SCREENING_REQUEST_DETAIL = New List(Of SiPendarDAL.SIPENDAR_SCREENING_REQUEST_DETAIL)

            'Hide Grid Screening Result

            gp_ScreeningResult.Hidden = True
            gp_ScreeningResult.GetSelectionModel.ClearSelection()

            pnlSiPendarReport.Hidden = True

            'Clean SiPendar Report fields
            cmb_JENIS_WATCHLIST_CODE.SetTextValue("")
            cmb_SUMBER_INFORMASI_KHUSUS_CODE.SetTextValue("")
            cmb_TINDAK_PIDANA_CODE.SetTextValue("")

            cmb_SUMBER_TYPE_CODE.SetTextValue("")
            txt_SUMBER_ID.Value = Nothing
            txt_Keterangan.Value = Nothing

            '8-Jul-2021 Adi : Tamabahan permintaan BSIM
            cmb_SUBMISSION_TYPE_CODE.SetTextValue("")

            chk_IsGenerateTransaction.Checked = False
            txt_Transaction_DateFrom.Value = Nothing
            txt_Transaction_DateTo.Value = Nothing

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    ' Ari 15-09-2021 menambahkan fungsi set command untuk panel indikator
    Sub SetCommandColumnLocation()
        Try
            ColumnActionLocation(gp_ScreeningResult, gp_ScreeningResult_CommandColumn)
            ColumnActionLocation(gp_ScreeningResultAll, gp_ScreeningResult_All_CommandColumn)
            ColumnActionLocation(gp_ReportIndicator, cc_ReportIndicator)

            '' Remark 30-Sep-2021 Felix , ganti NDSDropdown
            'sar_reportIndicator.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
            'StoreReportIndicator.Reload()
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If

            If bsettingRight = 1 Then
                'GridPanelReportIndicator.ColumnModel.Columns.Add(CommandColumn87)
            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Screening by Upload"
    Protected Sub btn_UploadScreeningRequest_Click()
        Try
            'Window_ScreeningUpload.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub btn_DownloadTemplate_Click()
        Try

            'Create Directory if not exists
            If Not Directory.Exists(Server.MapPath(PATH_TEMP_DIR)) Then
                Directory.CreateDirectory(Server.MapPath(PATH_TEMP_DIR))
            End If

            'Filename and location for output template
            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            Dim objfileinfo = New FileInfo(Server.MapPath(PATH_TEMP_DIR & tempfilexls))

            'Object to generate excel template
            Dim objScreeningRequest As Data.DataTable = New Data.DataTable()
            objScreeningRequest.Columns.Add(New Data.DataColumn("Name", GetType(String)))
            objScreeningRequest.Columns.Add(New Data.DataColumn("Date of Birth", GetType(Date)))

            'Generate excel template
            Using resource As New ExcelPackage(objfileinfo)
                'Membuath work sheet
                Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("ScreeningRequest")

                'Mengisi row 1 dengan header template
                ws.Cells("A1").LoadFromDataTable(objScreeningRequest, True)

                Dim rng As ExcelRange
                Dim comment As ExcelComment
                Dim strComment As String

                'Kolom B1 Name Mandatory
                rng = ws.Cells("A1")
                'rng.Value = "Everyday Be Coding"
                strComment = "NAWADATA: " & (vbCrLf) & "[MANDATORY] Diisi dengan Nama yang ingin di screening."
                comment = rng.AddComment(strComment, "NawaData")
                comment.AutoFit = True

                'Kolom C1 Datetime format dd-MMM-yyyy
                rng = ws.Cells("B1")
                strComment = "NAWADATA: " & (vbCrLf) & "[OPTIONAL] Data Type : Date." & (vbCrLf) & "Format : dd-MMM-yyyy."
                comment = rng.AddComment(strComment, "NawaData")
                comment.AutoFit = True

                'Kolom C1 set cell format to dd-MMM-yyyy
                rng = ws.Cells("B1:B" & (Int16.MaxValue) - 1)
                rng.Style.Numberformat.Format = "dd-MMM-yyyy"

                'Background and Font Color
                rng = ws.Cells("A1:B1")
                rng.Style.Font.Color.SetColor(Color.White)
                rng.Style.Font.Bold = True
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid
                rng.Style.Fill.BackgroundColor.SetColor(Color.Blue)

                ws.Cells(ws.Dimension.Address).AutoFitColumns()
                resource.Save()
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("content-disposition", "attachment;filename=" & "SiPendarScreeningRequest.xlsx")
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                Response.End()
            End Using

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_SIPENDAR_JUDGEMENT_Back_Click()
        Try
            IDModule = NawaBLL.Common.EncryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ReadSheetScreening(ByVal sheet As ExcelWorksheet)
        Try
            Dim PK_ID As Long = 0
            If listSIPENDAR_SCREENING_REQUEST_DETAIL.Count > 0 Then
                PK_ID = listSIPENDAR_SCREENING_REQUEST_DETAIL.Min(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID) - 1
            End If
            If PK_ID >= 0 Then
                PK_ID = -1
            End If

            Dim IRow As Integer = 2

            While IRow <> 0
                Dim strName As String = ""

                'Gunakan Name Sebagai Key... 
                'Cari Name, jika kosong diasumsikan bahwa itu row terakhir
                strName = sheet.Cells(IRow, 1).Text.Trim
                If Not String.IsNullOrEmpty(strName) Then
                    Dim ObjData As New SiPendarDAL.SIPENDAR_SCREENING_REQUEST_DETAIL
                    With ObjData
                        .PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = PK_ID
                        If Not String.IsNullOrEmpty(sheet.Cells(IRow, 1).Text.Trim) Then
                            .Nama = sheet.Cells(IRow, 1).Text.Trim
                        End If
                        If Not String.IsNullOrEmpty(sheet.Cells(IRow, 2).Text.Trim) Then
                            .DOB = sheet.Cells(IRow, 2).Text.Trim
                        End If

                        PK_ID = PK_ID - 1
                    End With

                    listSIPENDAR_SCREENING_REQUEST_DETAIL.Add(ObjData)
                    IRow = IRow + 1
                Else
                    IRow = 0
                End If
            End While

            Dim str As String = ""

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region


#Region "Start Screening"
    Private Const Interval = 10 ' In seconds (ie: 10 seconds)
    Private mdtmTarget As Date

    Protected Sub btn_Confirmation_Click()
        Try
            ''Clean Screening Request
            'btn_Cancel_Click()

            'Hide Panel Confirmation
            Panelconfirmation.Hidden = True
            FormPanelInput.Hidden = False

            Try
                IDModule = NawaBLL.Common.EncryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                If InStr(ObjModule.UrlView, "?") > 0 Then
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
                Else
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
                End If
            Catch ex As Exception When TypeOf ex Is ApplicationException
                Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
            Catch ex As Exception
                Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
                Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
            End Try

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Show_ScreeningResult()
        Try
            '' Start 20-Jul-2022 Penambhan atas Request ANZ
            cboExportExcel.Value = "Excel"
            '' End 20-Jul-2022
            Dim PK_Request_ID As String = Request.Params("ID")
            intID = NawaBLL.Common.DecryptQueryString(PK_Request_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            'Dim intID = NawaBLL.Common.DecryptQueryString(PK_Request_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            'Dim querySelect As String = ""
            'querySelect = "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct "
            'querySelect += "FROM SIPENDAR_SCREENING_REQUEST_RESULT a "
            'querySelect += "left join SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO b "
            'querySelect += "on a.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = b.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID "
            'querySelect += "WHERE a.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID in "
            'querySelect += "(select distinct PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID "
            'querySelect += "from SIPENDAR_SCREENING_REQUEST_RESULT "
            'querySelect += "where PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID =" & intID & " ) "
            'querySelect += "and a.GCN is not null  "
            'querySelect += "and isnull(b.Status,'New') in ('New','Reject','Reject Exclusion') "
            'querySelect += "ORDER BY TotalSimilarityPct Desc, a.GCN, Nama "

            Dim dtResult As DataTable = New DataTable
            dtResult = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(intID)
            'dtResult = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, querySelect)

            gp_ScreeningResult.GetStore.DataSource = dtResult
            gp_ScreeningResult.GetStore.DataBind()

            ''Added by Felix 16-Sep-2021
            dt_ScreeningResult = dtResult
            gp_ScreeningResultAll.GetStore.DataSource = dtResult
            gp_ScreeningResultAll.GetStore.DataBind()
            TotalResult = dt_ScreeningResult.Rows.Count
            '' End of 16-Sep-2021
            If dtResult.Rows.Count <> 0 Then
                PanelScreeningResult.Hidden = False
            Else
                PanelScreeningResult.Hidden = True
            End If
            'gp_ScreeningResult.Hidden = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Report to SiPendar"
    Protected Sub Btn_Save_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)

            ''Added by Felix 16-Sep-2021 Kalau Select All = false, Validasi
            'If cb_IsSelectAllResult.Checked = False Then
            '    Cek minimal 1 profile hasil screening dipilih
            If smScreeningResult.SelectedRows.Count = 0 Then ''Unremarked 19-Oct-2022, sepertinya tidak sengaja keremark saat penambahan fitur export
                Throw New Exception("Minimal 1 Profile Hasil Screening harus dipilih!")
            End If
            'End If
            '' End 16-Sep-2021

            'Validasi kelengkapan data untuk disimpan sebagai SiPendar Report
            ValidateSiPendarReport()

            'Set flag IsNeetToReportSIPENDAR
            Using objDb As New SiPendarDAL.SiPendarEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDb.Database.BeginTransaction()
                    Try
                        'Edit 29-Sep-2021 Felix, ambil 1 aja di grid

                        'For Each item As SelectedRow In smScreeningResult.SelectedRows
                        '    Dim recordID = item.RecordID.ToString
                        '    Dim objScreeningResult = objDb.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID).FirstOrDefault
                        '    If Not objScreeningResult Is Nothing Then
                        '        With objScreeningResult
                        '            .IsNeedToReportSIPENDAR = True
                        '            objDb.Entry(objScreeningResult).State = Entity.EntityState.Modified
                        '            objDb.SaveChanges()
                        '        End With
                        '    End If
                        'Next

                        Dim recordID = intID
                        Dim objScreeningResult = objDb.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID).FirstOrDefault
                        If Not objScreeningResult Is Nothing Then
                            With objScreeningResult
                                .IsNeedToReportSIPENDAR = True
                                objDb.Entry(objScreeningResult).State = Entity.EntityState.Modified
                                objDb.SaveChanges()
                            End With
                        End If
                        'End 29-Sep-2021 

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                    End Try
                End Using
            End Using

            'Populate Data to save to SiPendar Report
            SaveDataSiPendar()

            'Save ke Other Info
            SaveScreeningResultOtherInfo()

            ClearSession() '' Felix 21-Sep-2021 Pindah ke luar, dari dalam SaveDataSiPendar

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    '' Added on 23 Aug 2021
    Protected Sub btn_Exclude_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            SaveExcludeScreeningResultOtherInfo()

            Dim objProfileModule As NawaDAL.Module = Nothing

            objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_SCREENING_JUDGEMENT_Approval_Exclusion")

            '' Add by Felix 13-Oct-2021, kalau value = 1 , ga perlu approval
            Dim objDbSipendar As New SiPendarDAL.SiPendarEntities
            Dim objGlobalParameterPK10 As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
            objGlobalParameterPK10 = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 10).FirstOrDefault

            'Save Data With/Without Approval
            If objProfileModule IsNot Nothing Then
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsSupportView = False Or objGlobalParameterPK10.ParameterValue = 1 Then
                    Try

                        Dim PK_Request_ID As String = Request.Params("ID")
                        Dim intID = NawaBLL.Common.DecryptQueryString(PK_Request_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                        Dim dtResult As DataTable = New DataTable
                        'dtResult = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT WHERE PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID in (select distinct PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID from SIPENDAR_SCREENING_REQUEST_RESULT where PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID =" & intID & " ) and GCN is not null ORDER BY TotalSimilarityPct Desc, GCN, Nama")
                        dtResult = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(intID) '' Edit 01-Oct-2021 Felix

                        'Set flag IsNeetToReportSIPENDAR
                        Using objDb As New SiPendarDAL.SiPendarEntities
                            Using objTrans As System.Data.Entity.DbContextTransaction = objDb.Database.BeginTransaction()
                                Try
                                    For Each item As DataRow In dtResult.Rows
                                        'Dim recordID = item("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")
                                        Dim recordID = intID '' Edit  04-Oct-2021 Felix. Biar statusnya berubah yg di PK itu.

                                        Dim paramChangeStatus(1) As SqlParameter

                                        paramChangeStatus(0) = New SqlParameter
                                        paramChangeStatus(0).ParameterName = "@PK"
                                        paramChangeStatus(0).Value = recordID
                                        paramChangeStatus(0).DbType = SqlDbType.VarChar

                                        paramChangeStatus(1) = New SqlParameter
                                        paramChangeStatus(1).ParameterName = "@Status"
                                        'paramChangeStatus(1).Value = "Excluded"
                                        paramChangeStatus(1).Value = "Not Match" '' Edit 01-Oct-2021
                                        paramChangeStatus(1).DbType = SqlDbType.VarChar

                                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_UPDATE_STATUS_OTHER_INFO", paramChangeStatus)


                                    Next

                                    objTrans.Commit()
                                Catch ex As Exception
                                    objTrans.Rollback()
                                End Try
                            End Using
                        End Using

                        'LblConfirmation.Text = "Data Excluded"
                        'LblConfirmation.Text = "Data Not Match" '' Edit 01-Oct-2021 Felix
                        LblConfirmation.Text = "Data Judged as Not Match And Saved to Database" '' Edit 01-Oct-2021 Felix

                        Panelconfirmation.Hidden = False
                        FormPanelInput.Hidden = True

                    Catch ex As Exception
                        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
                        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
                    End Try
                Else
                    LblConfirmation.Text = "Data Saved into Pending Approval"
                End If
            Else
                LblConfirmation.Text = "Data Saved into Database"
            End If

            'Tampilkan confirmation
            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '' End of 23 Aug 2021 

    Protected Sub ValidateSiPendarReport()
        Try

            'Validate Header
            If String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
                Throw New ApplicationException(cmb_SIPENDAR_TYPE_ID.Label & " harus diisi.")
            End If
            ' 22 Dec 2021 Ari : hide submission type jadi tidak perlu di validasi
            'If String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue) Then
            '    Throw New ApplicationException(cmb_SUBMISSION_TYPE_CODE.Label & " harus diisi.")
            'End If
            ' 1 Dec 2021 Ari penambahan untuk validasi keterangan
            If txt_Keterangan.Value IsNot Nothing Then
                Dim keterangan As String = ""
                keterangan = txt_Keterangan.Value.ToString
                If keterangan.Length > 500 Then
                    Throw New ApplicationException("Keterangan Tidak Boleh Lebih Dari 500 Kata")
                End If
            End If

            If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then      'SIPENDAR Proaktif
                If String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_JENIS_WATCHLIST_CODE.Label & " harus diisi.")
                End If
                'If String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue) Then
                '    Throw New ApplicationException(cmb_TINDAK_PIDANA_CODE.Label & " harus diisi.")
                'End If
                If String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_SUMBER_INFORMASI_KHUSUS_CODE.Label & " harus diisi.")
                End If
            Else    'SIPENDAR Pengayaan
                If String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_SUMBER_TYPE_CODE.Label & " harus diisi.")
                End If
                'If String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A" Or txt_SUMBER_ID.Text = "NA" Then
                '    Throw New ApplicationException(txt_SUMBER_ID.FieldLabel & " harus diisi.")
                'Else
                '    If CLng(txt_SUMBER_ID.Value) < 1 Or CLng(txt_SUMBER_ID.Value) > 2147483646 Then
                '        Throw New ApplicationException(txt_SUMBER_ID.FieldLabel & " harus diisi >= 1 dan <= 2147483646.")
                '    End If
                'End If

                If chk_IsGenerateTransaction.Checked Then
                    If CDate(txt_Transaction_DateFrom.Value) = DateTime.MinValue Or CDate(txt_Transaction_DateTo.Value) = DateTime.MinValue Then
                        Throw New ApplicationException("Tanggal transaksi From dan To harus diisi. Jika pilihan Generate Transaction dicentang.")
                    End If
                    If CDate(txt_Transaction_DateFrom.Value) <> DateTime.MinValue And CDate(txt_Transaction_DateTo.Value) <> DateTime.MinValue Then
                        If CDate(txt_Transaction_DateFrom.Value) > CDate(txt_Transaction_DateTo.Value) Then
                            Throw New ApplicationException("Tanggal transaksi From harus < Tanggal transaksi To.")
                        End If
                    End If
                    'Ari 15-09-2021 Cek apakah list indikator ada atau tidak
                    If ListIndicator.Count = 0 Then
                        Throw New ApplicationException(" Minimal isi 1 indikator. Jika pilihan Generate Transaction dicentang.")
                    End If
                End If
            End If

            'If String.IsNullOrEmpty(txt_Keterangan.Value) Then
            '    Throw New ApplicationException(txt_Keterangan.FieldLabel & " harus diisi.")
            'End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SaveDataSiPendar()
        Try

            'Edited by Felix on 16-Sep-2021 Tambah IF untuk SelectAll 
            Dim strMessage As String = ""
            Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)
            'If cb_IsSelectAllResult.Checked Then

            'For Each row As DataRow In dt_ScreeningResult.Rows
            '    Dim recordID = row.Item("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID").ToString()
            Dim recordID = intID
            Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT = Nothing
            Using objDbSipendar As New SiPendarDAL.SiPendarEntities
                objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID).FirstOrDefault
                If objScreeningResult IsNot Nothing Then
                    'Cek apakah GCN kosong
                    If objScreeningResult.GCN Is Nothing OrElse objScreeningResult.GCN = "" Then
                        Throw New Exception("GCN tidak boleh kosong untuk bisa dilaporkan sebagai SIPENDAR")
                    End If


                    '' Added on 21 Jul 2021
                    'Cek apakah ID Watchlist PPATK kosong (khusus pengayaan)
                    If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 AndAlso (objScreeningResult.FK_WATCHLIST_ID_PPATK Is Nothing OrElse objScreeningResult.FK_WATCHLIST_ID_PPATK = "") Then
                        Throw New Exception("ID Watchlist PPATK tidak boleh kosong untuk bisa dilaporkan sebagai SIPENDAR PENGAYAAN")
                    End If
                    '' End of on 21 Jul 2021

                    'Cek apakah sudah pernah dilaporkan pada hari yang sama
                    'Dim objProfile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = objScreeningResult.GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue).FirstOrDefault

                    ' 28 Dec 2021 Ari : Di hide karena sudah ada validasi gcn berdasarkan format baru
                    ''Edited by Felix 27 Jul 2021
                    'Dim objGlobalParameterPerType As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
                    'objGlobalParameterPerType = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 7).FirstOrDefault

                    ''Dim objprofile As New SiPendarDAL.SIPENDAR_PROFILE
                    ''If objGlobalParameterPerType.ParameterValue = 1 Then
                    ''    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = objScreeningResult.GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue).FirstOrDefault
                    ''Else
                    ''    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = objScreeningResult.GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault
                    ''End If
                    '''End of 27 Jul 2021
                    ''If objprofile IsNot Nothing Then
                    ''    strMessage += "<br>GCN : " & objScreeningResult.GCN & " - " & objScreeningResult.NAMACUSTOMER & " sudah pernah dilaporkan pada hari ini."
                    ''End If

                    ' 10 Januari 2022 Ari penambahan untuk objprofile

                    'Cek juga apakah ada transaksi di range tanggal yang diinput untuk GCN-GCN yang dipilih
                    If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Then
                        If chk_IsGenerateTransaction.Checked Then
                            Dim dtFrom As Date = CDate(txt_Transaction_DateFrom.Value)
                            Dim dtTo As Date = CDate(txt_Transaction_DateTo.Value)

                            Dim objList = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch_GCN(objScreeningResult.GCN, dtFrom, dtTo)
                            If objList Is Nothing OrElse objList.Count <= 0 Then
                                strMessage += "<br>GCN : " & objScreeningResult.GCN & " - " & objScreeningResult.NAMACUSTOMER & " tidak memiliki data transaksi di range tanggal tersebut."
                            End If

                            '' Added by Felix 06 Aug 2021
                            Dim CheckLawanKosong As Long = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER where PK_GlobalReportParameter_ID = 8", Nothing) '' Added by Felix on 30 Aug 2021
                            If CheckLawanKosong = 1 Then
                                Dim objCheckLawan = SiPendarBLL.SIPENDARGenerateBLL.CheckCounterPartyExistForTransaction(objScreeningResult.GCN, dtFrom, dtTo)
                                If objCheckLawan.Count > 0 Then
                                    strMessage += "<br>GCN : " & objScreeningResult.GCN & " - " & objScreeningResult.NAMACUSTOMER & " memiliki transaksi yang tidak mempunyai lawan untuk periode yang dipilih."
                                End If
                                '' End of Felix 06 Aug 2021
                            End If '' End of 30 Aug 2021


                            Dim objCek = objDbSipendar.SIPENDAR_Report.Where(Function(x) x.UnikReference = objScreeningResult.GCN And DbFunctions.TruncateTime(x.Submission_Date) = DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault
                            If objCek IsNot Nothing Then
                                Throw New ApplicationException("Tidak bisa generate Report Transaction. Customer ini sudah punya Report Transaction di hari ini.")
                            End If

                        End If
                    End If

                End If
            End Using
            'Next
            ''Remark 29-Sep-2021 Felix karena skrng sudah di group by Request ID, GCN, PPATK ID, jd ambil 1 aja karena sama
            'Else
            '    'Save by looping selected item
            '    'Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)

            '    'Cek dulu apakah sudah pernah dilapor pada hari yang sama dan GCN yang sama

            '    For Each item As SelectedRow In smScreeningResult.SelectedRows
            '        Dim recordID = item.RecordID.ToString
            '        Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT = Nothing
            '        Using objDbSipendar As New SiPendarDAL.SiPendarEntities
            '            objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID).FirstOrDefault
            '            If objScreeningResult IsNot Nothing Then
            '                'Cek apakah GCN kosong
            '                If objScreeningResult.GCN Is Nothing OrElse objScreeningResult.GCN = "" Then
            '                    Throw New Exception("GCN tidak boleh kosong untuk bisa dilaporkan sebagai SIPENDAR")
            '                End If

            '                '' Added on 21 Jul 2021
            '                'Cek apakah ID Watchlist PPATK kosong (khusus pengayaan)
            '                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 AndAlso (objScreeningResult.FK_WATCHLIST_ID_PPATK Is Nothing OrElse objScreeningResult.FK_WATCHLIST_ID_PPATK = "") Then
            '                    Throw New Exception("ID Watchlist PPATK tidak boleh kosong untuk bisa dilaporkan sebagai SIPENDAR PENGAYAAN")
            '                End If
            '                '' End of on 21 Jul 2021

            '                'Cek apakah sudah pernah dilaporkan pada hari yang sama
            '                'Dim objProfile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = objScreeningResult.GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue).FirstOrDefault

            '                ' Edited by Felix 27 Jul 2021
            '                Dim objGlobalParameterPerType As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
            '                objGlobalParameterPerType = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 7).FirstOrDefault

            '                Dim objprofile As New SiPendarDAL.SIPENDAR_PROFILE
            '                If objGlobalParameterPerType.ParameterValue = 1 Then
            '                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = objScreeningResult.GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue).FirstOrDefault
            '                Else
            '                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = objScreeningResult.GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault
            '                End If
            '                'End of 27 Jul 2021
            '                If objprofile IsNot Nothing Then
            '                    strMessage += "<br>GCN : " & objScreeningResult.GCN & " - " & objScreeningResult.NAMACUSTOMER & " sudah pernah dilaporkan pada hari ini."
            '                End If

            '                'Cek juga apakah ada transaksi di range tanggal yang diinput untuk GCN-GCN yang dipilih
            '                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Then
            '                    If chk_IsGenerateTransaction.Checked Then
            '                        Dim dtFrom As Date = CDate(txt_Transaction_DateFrom.Value)
            '                        Dim dtTo As Date = CDate(txt_Transaction_DateTo.Value)

            '                        Dim objList = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch_GCN(objScreeningResult.GCN, dtFrom, dtTo)
            '                        If objList Is Nothing OrElse objList.Count <= 0 Then
            '                            strMessage += "<br>GCN : " & objScreeningResult.GCN & " - " & objScreeningResult.NAMACUSTOMER & " tidak memiliki data transaksi di range tanggal tersebut."
            '                        End If

            '                        '' Added by Felix 06 Aug 2021
            '                        Dim CheckLawanKosong As Long = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER where PK_GlobalReportParameter_ID = 8", Nothing) '' Added by Felix on 30 Aug 2021
            '                        If CheckLawanKosong = 1 Then
            '                            Dim objCheckLawan = SiPendarBLL.SIPENDARGenerateBLL.CheckCounterPartyExistForTransaction(objScreeningResult.GCN, dtFrom, dtTo)
            '                            If objCheckLawan.Count > 0 Then
            '                                strMessage += "<br>GCN : " & objScreeningResult.GCN & " - " & objScreeningResult.NAMACUSTOMER & " memiliki transaksi yang tidak mempunyai lawan untuk periode yang dipilih."
            '                            End If
            '                            '' End of Felix 06 Aug 2021
            '                        End If '' End of 30 Aug 2021


            '                        Dim objCek = objDbSipendar.SIPENDAR_Report.Where(Function(x) x.UnikReference = objScreeningResult.GCN And DbFunctions.TruncateTime(x.Submission_Date) = DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault
            '                        If objCek IsNot Nothing Then
            '                            Throw New ApplicationException("Tidak bisa generate Report Transaction. Customer ini sudah punya Report Transaction di hari ini.")
            '                        End If

            '                    End If
            '                End If

            '            End If
            '        End Using
            '    Next
            'End If
            '' End Remark 29-Sep-2021

            If Not String.IsNullOrEmpty(strMessage) Then
                Throw New ApplicationException(strMessage)
            End If

            '6-Jul-2021 Adi Get objModule
            Dim objProfileModule As NawaDAL.Module = Nothing
            If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PROAKTIF")
            Else
                objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PENGAYAAN")
            End If

            'Save to Database (With/Without Approval)
            Dim previousGCN As String = ""

            'Remarked 29-Sep-2021 Felix
            'Edited by Felix on 16-Sep-2021 Tambah If untuk SelectAll 
            'If cb_IsSelectAllResult.Checked Then
            '    For Each row As DataRow In dt_ScreeningResult.Rows
            '        Dim recordID = row.Item("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID").ToString()

            '        'Buat object baru untuk list SIPENDAR_PROFILE
            '        Dim objSIPENDAR_PROFILE_CLASS As New SiPendarBLL.SIPENDAR_PROFILE_CLASS

            '        'Ambil data2 dari Sipendar Entity
            '        Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT = Nothing
            '        Dim objGlobalParameter As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
            '        Using objDbSipendar As New SiPendarDAL.SiPendarEntities
            '            objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID).FirstOrDefault
            '            objGlobalParameter = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault
            '        End Using

            '        'Skip jika ada 2 GCN yang sama dipilih
            '        If objScreeningResult.GCN = previousGCN Then
            '            Continue For
            '        End If

            '        'Ambil data2 dari NawadataDevEntities
            '        Dim objCustomer As SiPendarDAL.goAML_Ref_Customer = Nothing
            '        Dim listCustomer As New List(Of SiPendarDAL.goAML_Ref_Customer)      '1 GCN bisa punya banyak CIF
            '        'Dim listAccount As New List(Of SiPendarDAL.goAML_Ref_Account)
            '        Dim listAddress As New List(Of SiPendarDAL.goAML_Ref_Address)
            '        Dim listPhone As New List(Of SiPendarDAL.goAML_Ref_Phone)
            '        Dim listIdentification As New List(Of SiPendarDAL.goAML_Person_Identification)

            '        Dim listProfileAccount As New List(Of SiPendarBLL.ProfileAccountClass) '' Added 30 Jul 2021

            '        Using objDbNawa As New SiPendarDAL.SiPendarEntities
            '            'Get Data Customer
            '            If Not objScreeningResult Is Nothing Then
            '                objCustomer = objDbNawa.goAML_Ref_Customer.Where(Function(x) x.GCN = objScreeningResult.GCN).FirstOrDefault
            '                listCustomer = objDbNawa.goAML_Ref_Customer.Where(Function(x) x.GCN = objScreeningResult.GCN).ToList
            '            End If

            '            'Get Account, Address, Phone, Identification
            '            If listCustomer IsNot Nothing AndAlso listCustomer.Count > 0 Then
            '                For Each itemCustomer In listCustomer
            '                    Dim tempListAccount = objDbNawa.goAML_Ref_Account.Where(Function(x) x.client_number = itemCustomer.CIF).ToList
            '                    Dim tempListAddress = objDbNawa.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = itemCustomer.PK_Customer_ID).ToList
            '                    Dim tempListPhone = objDbNawa.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = itemCustomer.PK_Customer_ID).ToList
            '                    Dim tempListIdentification = objDbNawa.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = itemCustomer.PK_Customer_ID).ToList

            '                    'List Account
            '                    If tempListAccount IsNot Nothing AndAlso tempListAccount.Count > 0 Then
            '                        For Each itemAccount In tempListAccount

            '                            Dim tempprofileaccount As New ProfileAccountClass
            '                            tempprofileaccount.ObjAccount = itemAccount
            '                            If itemAccount.Account_No IsNot Nothing Then
            '                                tempprofileaccount.ListATMAccount = objDbNawa.AML_ACCOUNT_ATM.Where(Function(x) x.Account_No = itemAccount.Account_No).ToList
            '                            End If

            '                            'listAccount.Add(itemAccount)
            '                            listProfileAccount.Add(tempprofileaccount)
            '                        Next
            '                    End If

            '                    'List Address
            '                    If tempListAddress IsNot Nothing AndAlso tempListAddress.Count > 0 Then
            '                        For Each itemaddress In tempListAddress
            '                            listAddress.Add(itemaddress)
            '                        Next
            '                    End If

            '                    'List Phone
            '                    If tempListPhone IsNot Nothing AndAlso tempListPhone.Count > 0 Then
            '                        For Each itemphone In tempListPhone
            '                            listPhone.Add(itemphone)
            '                        Next
            '                    End If

            '                    'List identification
            '                    If itemCustomer.FK_Customer_Type_ID = 1 Then    'Hanya perorangan yg punya Identification
            '                        If tempListIdentification IsNot Nothing AndAlso tempListIdentification.Count > 0 Then
            '                            For Each itemidentification In tempListIdentification
            '                                listIdentification.Add(itemidentification)
            '                            Next
            '                        End If
            '                    End If
            '                    ''Added on 26 Aug 2021
            '                    If Not objCustomer Is Nothing Then
            '                        If Not String.IsNullOrWhiteSpace(itemCustomer.INDV_SSN) Then
            '                            Dim tempident As New SiPendarDAL.goAML_Person_Identification
            '                            tempident.Number = itemCustomer.INDV_SSN
            '                            tempident.Type = "KTP"
            '                            tempident.Issued_Country = itemCustomer.INDV_Nationality1
            '                            listIdentification.Add(tempident)
            '                        End If

            '                        If Not String.IsNullOrWhiteSpace(itemCustomer.INDV_Passport_Number) Then
            '                            Dim tempident As New SiPendarDAL.goAML_Person_Identification
            '                            tempident.Number = itemCustomer.INDV_Passport_Number
            '                            tempident.Type = "PAS"
            '                            tempident.Issued_Country = itemCustomer.INDV_Passport_Country
            '                            listIdentification.Add(tempident)
            '                        End If
            '                    End If
            '                    '' End of 26 Aug 2021
            '                Next
            '            End If
            '        End Using

            '        With objSIPENDAR_PROFILE_CLASS
            '            'SiPendar Profile
            '            With objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE

            '                'Populate Data Header
            '                If Not String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
            '                    .FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.SelectedItemValue
            '                End If
            '                If Not objCustomer Is Nothing Then
            '                    .Fk_goAML_Ref_Customer_Type_id = objCustomer.FK_Customer_Type_ID
            '                End If
            '                If Not String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue) Then
            '                    .FK_SIPENDAR_JENIS_WATCHLIST_CODE = cmb_JENIS_WATCHLIST_CODE.SelectedItemValue
            '                End If
            '                If Not String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue) Then
            '                    .FK_SIPENDAR_TINDAK_PIDANA_CODE = cmb_TINDAK_PIDANA_CODE.SelectedItemValue
            '                End If
            '                If Not String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue) Then
            '                    .FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE = cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue
            '                End If
            '                If Not objGlobalParameter Is Nothing Then
            '                    .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
            '                End If

            '                'tambahan 5-Jul-2021
            '                If Not String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue) Then
            '                    .FK_SIPENDAR_SUMBER_TYPE_CODE = cmb_SUMBER_TYPE_CODE.SelectedItemValue
            '                End If

            '                'If Not (String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A" Or txt_SUMBER_ID.Text = "NA") Then
            '                '    .SUMBER_ID = CInt(txt_SUMBER_ID.Value)
            '                'Else
            '                '    .SUMBER_ID = Nothing
            '                'End If
            '                'end of tambahan 5-Jul-2021

            '                '21-Jul-2021 Adi : Sumber ID diambil dari ID Watchlist PPATK
            '                If objScreeningResult.FK_WATCHLIST_ID_PPATK IsNot Nothing AndAlso objScreeningResult.FK_WATCHLIST_ID_PPATK <> "" Then
            '                    .SUMBER_ID = objScreeningResult.FK_WATCHLIST_ID_PPATK
            '                Else
            '                    .SUMBER_ID = Nothing
            '                End If
            '                'End of 21-Jul-2021 Adi : Sumber ID diambil dari ID Watchlist PPATK

            '                'tambahan 8-Jul-2021
            '                If Not String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue) Then
            '                    .FK_SIPENDAR_SUBMISSION_TYPE_CODE = cmb_SUBMISSION_TYPE_CODE.SelectedItemValue
            '                End If
            '                .Submission_Date = DateTime.Now()

            '                If (chk_IsGenerateTransaction.Checked) Then
            '                    .Transaction_DateFrom = CDate(txt_Transaction_DateFrom.Value)
            '                    .Transaction_DateTo = CDate(txt_Transaction_DateTo.Value)
            '                    'Ari 15-09-2021 menambahkan fungsi input ke field report indicator dari list indikator
            '                    If ListIndicator.Count > 0 Then
            '                        Dim i As Integer
            '                        Dim temp As String
            '                        i = 0
            '                        temp = ""
            '                        For Each itemindicator In ListIndicator
            '                            i += 1
            '                            If ListIndicator.Count = 1 Then
            '                                'temp = getReportIndicatorByKode(item.FK_Indicator)
            '                                temp = itemindicator.FK_Indicator
            '                            Else
            '                                If i = 1 Then
            '                                    'temp = getReportIndicatorByKode(item.FK_Indicator)
            '                                    temp = itemindicator.FK_Indicator
            '                                Else
            '                                    'temp += "," + getReportIndicatorByKode(item.FK_Indicator)
            '                                    temp += "," + itemindicator.FK_Indicator
            '                                End If
            '                            End If
            '                        Next
            '                        .Report_Indicator = temp
            '                    End If
            '                    'End Ari 15-09-2021
            '                End If
            '                ''end of tambahan 8-Jul-2021
            '                '' Added by Felix 15-Sep-2021
            '                .FK_SIPENDAR_SCREENING_REQUEST_ID = objScreeningResult.PK_SIPENDAR_SCREENING_REQUEST_ID
            '                .FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = objScreeningResult.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
            '                ''End of 15-Sep-2021


            '                If Not objCustomer Is Nothing Then
            '                    Dim objAddress = listAddress.FirstOrDefault

            '                    .GCN = objCustomer.GCN

            '                    If objCustomer.FK_Customer_Type_ID = 1 Then     'Individual
            '                        .INDV_NAME = objCustomer.INDV_Last_Name
            '                        .INDV_FK_goAML_Ref_Nama_Negara_CODE = objCustomer.INDV_Nationality1

            '                        If objAddress IsNot Nothing Then
            '                            .INDV_ADDRESS = objAddress.Address
            '                        End If

            '                        .INDV_PLACEOFBIRTH = objCustomer.INDV_Birth_Place
            '                        .INDV_DOB = objCustomer.INDV_BirthDate
            '                    Else    'Korporasi
            '                        .CORP_NAME = objCustomer.Corp_Name
            '                        .CORP_FK_goAML_Ref_Nama_Negara_CODE = objCustomer.Corp_Incorporation_Country_Code
            '                        .CORP_NPWP = objCustomer.Corp_Tax_Number
            '                        .CORP_NO_IZIN_USAHA = objCustomer.Corp_Incorporation_Number
            '                    End If
            '                End If

            '                .Keterangan = Trim(txt_Keterangan.Value)

            '                If Not objScreeningResult Is Nothing Then
            '                    If Not IsNothing(objScreeningResult.TotalSimilarity) Then
            '                        .Similarity = CDbl(objScreeningResult.TotalSimilarity) * 100
            '                    End If
            '                End If
            '            End With

            '            'SiPendarProfile Account
            '            If listProfileAccount IsNot Nothing AndAlso listProfileAccount.Count > 0 Then
            '                Dim PK_ID As Long = 0
            '                Dim PK_ATM_ID As Long = 0
            '                For Each itemAccount In listProfileAccount
            '                    PK_ID = PK_ID - 1
            '                    Dim objAccount As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT
            '                    With objAccount
            '                        .PK_SIPENDAR_PROFILE_ACCOUNT_ID = PK_ID
            '                        .CIFNO = itemAccount.ObjAccount.client_number
            '                        .Fk_goAML_Ref_Jenis_Rekening_CODE = itemAccount.ObjAccount.personal_account_type
            '                        .Fk_goAML_Ref_Status_Rekening_CODE = itemAccount.ObjAccount.status_code

            '                        If objGlobalParameter IsNot Nothing Then
            '                            .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
            '                        End If

            '                        .NOREKENING = itemAccount.ObjAccount.Account_No
            '                    End With

            '                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Add(objAccount)

            '                    '' Added on 30 Jul 2021

            '                    For Each itemATM In itemAccount.ListATMAccount
            '                        PK_ATM_ID = PK_ATM_ID - 1
            '                        Dim objATM As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT_ATM
            '                        With objATM
            '                            .PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = PK_ATM_ID
            '                            .FK_SIPENDAR_PROFILE_ACCOUNT_ID = objAccount.PK_SIPENDAR_PROFILE_ACCOUNT_ID
            '                            .NOATM = itemATM.ATM_NO
            '                        End With
            '                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Add(objATM)
            '                    Next
            '                    '' end of 30 Jul 2021
            '                Next
            '            End If

            '            'SiPendarProfile Address
            '            If listAddress IsNot Nothing AndAlso listAddress.Count > 0 Then
            '                Dim PK_ID As Long = 0
            '                For Each itemAddress In listAddress
            '                    PK_ID = PK_ID - 1
            '                    Dim objAddress As New SiPendarDAL.SIPENDAR_PROFILE_ADDRESS
            '                    With objAddress
            '                        .PK_SIPENDAR_PROFILE_ADDRESS_ID = PK_ID
            '                        .FK_goAML_Ref_Kategori_Kontak_CODE = itemAddress.Address_Type
            '                        .ADDRESS = itemAddress.Address
            '                        .CITY = itemAddress.City
            '                        .FK_goAML_Ref_Nama_Negara_CODE = itemAddress.Country_Code
            '                        .ZIP = itemAddress.Zip

            '                        'tambahan 5 Jul 2021
            '                        .TOWN = itemAddress.Town
            '                        .STATE = itemAddress.State
            '                        .COMMENTS = itemAddress.Comments
            '                    End With

            '                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Add(objAddress)
            '                Next
            '            End If

            '            'SiPendarProfile PHONE
            '            If listPhone IsNot Nothing AndAlso listPhone.Count > 0 Then
            '                Dim PK_ID As Long = 0
            '                For Each itemPHONE In listPhone
            '                    PK_ID = PK_ID - 1
            '                    Dim objPHONE As New SiPendarDAL.SIPENDAR_PROFILE_PHONE
            '                    With objPHONE
            '                        .PK_SIPENDAR_PROFILE_PHONE_ID = PK_ID
            '                        .FK_goAML_Ref_Kategori_Kontak_CODE = itemPHONE.Tph_Contact_Type
            '                        .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = itemPHONE.Tph_Communication_Type
            '                        .COUNTRY_PREFIX = itemPHONE.tph_country_prefix
            '                        .PHONE_NUMBER = itemPHONE.tph_number

            '                        'tambahan 5 Jul 2021
            '                        .EXTENSION_NUMBER = itemPHONE.tph_extension
            '                        .COMMENTS = itemPHONE.comments
            '                    End With

            '                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Add(objPHONE)
            '                Next
            '            End If



            '            'SiPendarProfile IDENTIFICATION
            '            If listIdentification IsNot Nothing AndAlso listIdentification.Count > 0 Then
            '                Dim PK_ID As Long = 0
            '                For Each itemIDENTIFICATION In listIdentification
            '                    Dim objCek = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.IDENTITY_NUMBER = itemIDENTIFICATION.Number)
            '                    If objCek IsNot Nothing Then
            '                        Continue For
            '                    End If
            '                    PK_ID = PK_ID - 1
            '                    Dim objIDENTIFICATION As New SiPendarDAL.SIPENDAR_PROFILE_IDENTIFICATION
            '                    With objIDENTIFICATION
            '                        '.Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = objIDENTIFICATION.Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE
            '                        '.IDENTITY_NUMBER = objIDENTIFICATION.IDENTITY_NUMBER
            '                        '.ISSUE_DATE = objIDENTIFICATION.ISSUE_DATE
            '                        '.EXPIRED_DATE = objIDENTIFICATION.EXPIRED_DATE
            '                        '.ISSUED_BY = objIDENTIFICATION.ISSUED_BY
            '                        '.FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = objIDENTIFICATION.FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY
            '                        .PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = PK_ID
            '                        .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = itemIDENTIFICATION.Type
            '                        .IDENTITY_NUMBER = itemIDENTIFICATION.Number
            '                        .ISSUE_DATE = itemIDENTIFICATION.Issue_Date
            '                        .EXPIRED_DATE = itemIDENTIFICATION.Expiry_Date
            '                        .ISSUED_BY = itemIDENTIFICATION.Issued_By
            '                        .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = itemIDENTIFICATION.Issued_Country

            '                        'tambahan 5 Jul 2021
            '                        .Comments = itemIDENTIFICATION.Identification_Comment
            '                    End With

            '                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Add(objIDENTIFICATION)
            '                Next
            '            End If
            '        End With

            '        'Save Data With/Without Approval
            '        'If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
            '        '6-Jul-2021 Adi : get Module apakah perlu approval atau tidak sesuai SIPENDAR TYPE yang diisi
            '        If objProfileModule IsNot Nothing Then
            '            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
            '                SiPendarProfile_BLL.SaveAddWithoutApproval(objProfileModule, objSIPENDAR_PROFILE_CLASS)
            '            Else
            '                SiPendarProfile_BLL.SaveAddWithApproval(objProfileModule, objSIPENDAR_PROFILE_CLASS)
            '            End If
            '        Else
            '            SiPendarProfile_BLL.SaveAddWithoutApproval(ObjModule, objSIPENDAR_PROFILE_CLASS)
            '        End If

            '        'Set previousGCN
            '        previousGCN = objScreeningResult.GCN
            '    Next
            '    ''Remark 29-Sep-2021 Felix karena skrng sudah di group by Request ID, GCN, PPATK ID, jd ambil 1 aja karena sama
            'Else
            '    For Each item As SelectedRow In smScreeningResult.SelectedRows
            '        Dim recordID = item.RecordID.ToString

            '        'Buat object baru untuk list SIPENDAR_PROFILE
            '        Dim objSIPENDAR_PROFILE_CLASS As New SiPendarBLL.SIPENDAR_PROFILE_CLASS

            '        'Ambil data2 dari Sipendar Entity
            '        Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT = Nothing
            '        Dim objGlobalParameter As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
            '        Using objDbSipendar As New SiPendarDAL.SiPendarEntities
            '            objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID).FirstOrDefault
            '            objGlobalParameter = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault
            '        End Using

            '        'Skip jika ada 2 GCN yang sama dipilih
            '        If objScreeningResult.GCN = previousGCN Then
            '            Continue For
            '        End If

            '        'Ambil data2 dari NawadataDevEntities
            '        Dim objCustomer As SiPendarDAL.goAML_Ref_Customer = Nothing
            '        Dim listCustomer As New List(Of SiPendarDAL.goAML_Ref_Customer)      '1 GCN bisa punya banyak CIF
            '        'Dim listAccount As New List(Of SiPendarDAL.goAML_Ref_Account)
            '        Dim listAddress As New List(Of SiPendarDAL.goAML_Ref_Address)
            '        Dim listPhone As New List(Of SiPendarDAL.goAML_Ref_Phone)
            '        Dim listIdentification As New List(Of SiPendarDAL.goAML_Person_Identification)

            '        Dim listProfileAccount As New List(Of SiPendarBLL.ProfileAccountClass) '' Added 30 Jul 2021

            '        Using objDbNawa As New SiPendarDAL.SiPendarEntities
            '            'Get Data Customer
            '            If Not objScreeningResult Is Nothing Then
            '                objCustomer = objDbNawa.goAML_Ref_Customer.Where(Function(x) x.GCN = objScreeningResult.GCN).FirstOrDefault
            '                listCustomer = objDbNawa.goAML_Ref_Customer.Where(Function(x) x.GCN = objScreeningResult.GCN).ToList
            '            End If

            '            'Get Account, Address, Phone, Identification
            '            If listCustomer IsNot Nothing AndAlso listCustomer.Count > 0 Then
            '                For Each itemCustomer In listCustomer
            '                    Dim tempListAccount = objDbNawa.goAML_Ref_Account.Where(Function(x) x.client_number = itemCustomer.CIF).ToList
            '                    Dim tempListAddress = objDbNawa.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = itemCustomer.PK_Customer_ID).ToList
            '                    Dim tempListPhone = objDbNawa.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = itemCustomer.PK_Customer_ID).ToList
            '                    Dim tempListIdentification = objDbNawa.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = itemCustomer.PK_Customer_ID).ToList

            '                    'List Account
            '                    If tempListAccount IsNot Nothing AndAlso tempListAccount.Count > 0 Then
            '                        For Each itemAccount In tempListAccount

            '                            Dim tempprofileaccount As New ProfileAccountClass
            '                            tempprofileaccount.ObjAccount = itemAccount
            '                            If itemAccount.Account_No IsNot Nothing Then
            '                                tempprofileaccount.ListATMAccount = objDbNawa.AML_ACCOUNT_ATM.Where(Function(x) x.Account_No = itemAccount.Account_No).ToList
            '                            End If

            '                            'listAccount.Add(itemAccount)
            '                            listProfileAccount.Add(tempprofileaccount)
            '                        Next
            '                    End If

            '                    'List Address
            '                    If tempListAddress IsNot Nothing AndAlso tempListAddress.Count > 0 Then
            '                        For Each itemaddress In tempListAddress
            '                            listAddress.Add(itemaddress)
            '                        Next
            '                    End If

            '                    'List Phone
            '                    If tempListPhone IsNot Nothing AndAlso tempListPhone.Count > 0 Then
            '                        For Each itemphone In tempListPhone
            '                            listPhone.Add(itemphone)
            '                        Next
            '                    End If

            '                    'List identification
            '                    If itemCustomer.FK_Customer_Type_ID = 1 Then    'Hanya perorangan yg punya Identification
            '                        If tempListIdentification IsNot Nothing AndAlso tempListIdentification.Count > 0 Then
            '                            For Each itemidentification In tempListIdentification
            '                                listIdentification.Add(itemidentification)
            '                            Next
            '                        End If
            '                    End If
            '                    ''Added on 26 Aug 2021
            '                    If Not objCustomer Is Nothing Then
            '                        If Not String.IsNullOrWhiteSpace(itemCustomer.INDV_SSN) Then
            '                            Dim tempident As New SiPendarDAL.goAML_Person_Identification
            '                            tempident.Number = itemCustomer.INDV_SSN
            '                            tempident.Type = "KTP"
            '                            tempident.Issued_Country = itemCustomer.INDV_Nationality1
            '                            listIdentification.Add(tempident)
            '                        End If

            '                        If Not String.IsNullOrWhiteSpace(itemCustomer.INDV_Passport_Number) Then
            '                            Dim tempident As New SiPendarDAL.goAML_Person_Identification
            '                            tempident.Number = itemCustomer.INDV_Passport_Number
            '                            tempident.Type = "PAS"
            '                            tempident.Issued_Country = itemCustomer.INDV_Passport_Country
            '                            listIdentification.Add(tempident)
            '                        End If
            '                    End If
            '                    '' End of 26 Aug 2021
            '                Next
            '            End If
            '        End Using

            '        With objSIPENDAR_PROFILE_CLASS
            '            'SiPendar Profile
            '            With objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE

            '                'Populate Data Header
            '                If Not String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
            '                    .FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.SelectedItemValue
            '                End If
            '                If Not objCustomer Is Nothing Then
            '                    .Fk_goAML_Ref_Customer_Type_id = objCustomer.FK_Customer_Type_ID
            '                End If
            '                If Not String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue) Then
            '                    .FK_SIPENDAR_JENIS_WATCHLIST_CODE = cmb_JENIS_WATCHLIST_CODE.SelectedItemValue
            '                End If
            '                If Not String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue) Then
            '                    .FK_SIPENDAR_TINDAK_PIDANA_CODE = cmb_TINDAK_PIDANA_CODE.SelectedItemValue
            '                End If
            '                If Not String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue) Then
            '                    .FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE = cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue
            '                End If
            '                If Not objGlobalParameter Is Nothing Then
            '                    .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
            '                End If

            '                'tambahan 5-Jul-2021
            '                If Not String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue) Then
            '                    .FK_SIPENDAR_SUMBER_TYPE_CODE = cmb_SUMBER_TYPE_CODE.SelectedItemValue
            '                End If

            '                'If Not (String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A" Or txt_SUMBER_ID.Text = "NA") Then
            '                '    .SUMBER_ID = CInt(txt_SUMBER_ID.Value)
            '                'Else
            '                '    .SUMBER_ID = Nothing
            '                'End If
            '                'end of tambahan 5-Jul-2021

            '                '21-Jul-2021 Adi : Sumber ID diambil dari ID Watchlist PPATK
            '                If objScreeningResult.FK_WATCHLIST_ID_PPATK IsNot Nothing AndAlso objScreeningResult.FK_WATCHLIST_ID_PPATK <> "" Then
            '                    .SUMBER_ID = objScreeningResult.FK_WATCHLIST_ID_PPATK
            '                Else
            '                    .SUMBER_ID = Nothing
            '                End If
            '                'End of 21-Jul-2021 Adi : Sumber ID diambil dari ID Watchlist PPATK

            '                'tambahan 8-Jul-2021
            '                If Not String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue) Then
            '                    .FK_SIPENDAR_SUBMISSION_TYPE_CODE = cmb_SUBMISSION_TYPE_CODE.SelectedItemValue
            '                End If
            '                .Submission_Date = DateTime.Now()

            '                If (chk_IsGenerateTransaction.Checked) Then
            '                    .Transaction_DateFrom = CDate(txt_Transaction_DateFrom.Value)
            '                    .Transaction_DateTo = CDate(txt_Transaction_DateTo.Value)
            '                    'Ari 15-09-2021 menambahkan fungsi input ke field report indicator dari list indikator
            '                    If ListIndicator.Count > 0 Then
            '                        Dim i As Integer
            '                        Dim temp As String
            '                        i = 0
            '                        temp = ""
            '                        For Each itemindicator In ListIndicator
            '                            i += 1
            '                            If ListIndicator.Count = 1 Then
            '                                'temp = getReportIndicatorByKode(item.FK_Indicator)
            '                                temp = itemindicator.FK_Indicator
            '                            Else
            '                                If i = 1 Then
            '                                    'temp = getReportIndicatorByKode(item.FK_Indicator)
            '                                    temp = itemindicator.FK_Indicator
            '                                Else
            '                                    'temp += "," + getReportIndicatorByKode(item.FK_Indicator)
            '                                    temp += "," + itemindicator.FK_Indicator
            '                                End If
            '                            End If
            '                        Next
            '                        .Report_Indicator = temp
            '                    End If
            '                    'End Ari 15-09-2021
            '                End If
            '                ''end of tambahan 8-Jul-2021
            '                '' Added by Felix 15-Sep-2021
            '                .FK_SIPENDAR_SCREENING_REQUEST_ID = objScreeningResult.PK_SIPENDAR_SCREENING_REQUEST_ID
            '                .FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = objScreeningResult.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
            '                ''End of 15-Sep-2021


            '                If Not objCustomer Is Nothing Then
            '                    Dim objAddress = listAddress.FirstOrDefault

            '                    .GCN = objCustomer.GCN

            '                    If objCustomer.FK_Customer_Type_ID = 1 Then     'Individual
            '                        .INDV_NAME = objCustomer.INDV_Last_Name
            '                        .INDV_FK_goAML_Ref_Nama_Negara_CODE = objCustomer.INDV_Nationality1

            '                        If objAddress IsNot Nothing Then
            '                            .INDV_ADDRESS = objAddress.Address
            '                        End If

            '                        .INDV_PLACEOFBIRTH = objCustomer.INDV_Birth_Place
            '                        .INDV_DOB = objCustomer.INDV_BirthDate
            '                    Else    'Korporasi
            '                        .CORP_NAME = objCustomer.Corp_Name
            '                        .CORP_FK_goAML_Ref_Nama_Negara_CODE = objCustomer.Corp_Incorporation_Country_Code
            '                        .CORP_NPWP = objCustomer.Corp_Tax_Number
            '                        .CORP_NO_IZIN_USAHA = objCustomer.Corp_Incorporation_Number
            '                    End If
            '                End If

            '                .Keterangan = Trim(txt_Keterangan.Value)

            '                If Not objScreeningResult Is Nothing Then
            '                    If Not IsNothing(objScreeningResult.TotalSimilarity) Then
            '                        .Similarity = CDbl(objScreeningResult.TotalSimilarity) * 100
            '                    End If
            '                End If
            '            End With

            '            'SiPendarProfile Account
            '            If listProfileAccount IsNot Nothing AndAlso listProfileAccount.Count > 0 Then
            '                Dim PK_ID As Long = 0
            '                Dim PK_ATM_ID As Long = 0
            '                For Each itemAccount In listProfileAccount
            '                    PK_ID = PK_ID - 1
            '                    Dim objAccount As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT
            '                    With objAccount
            '                        .PK_SIPENDAR_PROFILE_ACCOUNT_ID = PK_ID
            '                        .CIFNO = itemAccount.ObjAccount.client_number
            '                        .Fk_goAML_Ref_Jenis_Rekening_CODE = itemAccount.ObjAccount.personal_account_type
            '                        .Fk_goAML_Ref_Status_Rekening_CODE = itemAccount.ObjAccount.status_code

            '                        If objGlobalParameter IsNot Nothing Then
            '                            .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
            '                        End If

            '                        .NOREKENING = itemAccount.ObjAccount.Account_No
            '                    End With

            '                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Add(objAccount)

            '                    '' Added on 30 Jul 2021

            '                    For Each itemATM In itemAccount.ListATMAccount
            '                        PK_ATM_ID = PK_ATM_ID - 1
            '                        Dim objATM As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT_ATM
            '                        With objATM
            '                            .PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = PK_ATM_ID
            '                            .FK_SIPENDAR_PROFILE_ACCOUNT_ID = objAccount.PK_SIPENDAR_PROFILE_ACCOUNT_ID
            '                            .NOATM = itemATM.ATM_NO
            '                        End With
            '                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Add(objATM)
            '                    Next
            '                    '' end of 30 Jul 2021
            '                Next
            '            End If

            '            'SiPendarProfile Address
            '            If listAddress IsNot Nothing AndAlso listAddress.Count > 0 Then
            '                Dim PK_ID As Long = 0
            '                For Each itemAddress In listAddress
            '                    PK_ID = PK_ID - 1
            '                    Dim objAddress As New SiPendarDAL.SIPENDAR_PROFILE_ADDRESS
            '                    With objAddress
            '                        .PK_SIPENDAR_PROFILE_ADDRESS_ID = PK_ID
            '                        .FK_goAML_Ref_Kategori_Kontak_CODE = itemAddress.Address_Type
            '                        .ADDRESS = itemAddress.Address
            '                        .CITY = itemAddress.City
            '                        .FK_goAML_Ref_Nama_Negara_CODE = itemAddress.Country_Code
            '                        .ZIP = itemAddress.Zip

            '                        'tambahan 5 Jul 2021
            '                        .TOWN = itemAddress.Town
            '                        .STATE = itemAddress.State
            '                        .COMMENTS = itemAddress.Comments
            '                    End With

            '                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Add(objAddress)
            '                Next
            '            End If

            '            'SiPendarProfile PHONE
            '            If listPhone IsNot Nothing AndAlso listPhone.Count > 0 Then
            '                Dim PK_ID As Long = 0
            '                For Each itemPHONE In listPhone
            '                    PK_ID = PK_ID - 1
            '                    Dim objPHONE As New SiPendarDAL.SIPENDAR_PROFILE_PHONE
            '                    With objPHONE
            '                        .PK_SIPENDAR_PROFILE_PHONE_ID = PK_ID
            '                        .FK_goAML_Ref_Kategori_Kontak_CODE = itemPHONE.Tph_Contact_Type
            '                        .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = itemPHONE.Tph_Communication_Type
            '                        .COUNTRY_PREFIX = itemPHONE.tph_country_prefix
            '                        .PHONE_NUMBER = itemPHONE.tph_number

            '                        'tambahan 5 Jul 2021
            '                        .EXTENSION_NUMBER = itemPHONE.tph_extension
            '                        .COMMENTS = itemPHONE.comments
            '                    End With

            '                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Add(objPHONE)
            '                Next
            '            End If



            '            'SiPendarProfile IDENTIFICATION
            '            If listIdentification IsNot Nothing AndAlso listIdentification.Count > 0 Then
            '                Dim PK_ID As Long = 0
            '                For Each itemIDENTIFICATION In listIdentification
            '                    Dim objCek = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.IDENTITY_NUMBER = itemIDENTIFICATION.Number)
            '                    If objCek IsNot Nothing Then
            '                        Continue For
            '                    End If
            '                    PK_ID = PK_ID - 1
            '                    Dim objIDENTIFICATION As New SiPendarDAL.SIPENDAR_PROFILE_IDENTIFICATION
            '                    With objIDENTIFICATION
            '                        '.Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = objIDENTIFICATION.Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE
            '                        '.IDENTITY_NUMBER = objIDENTIFICATION.IDENTITY_NUMBER
            '                        '.ISSUE_DATE = objIDENTIFICATION.ISSUE_DATE
            '                        '.EXPIRED_DATE = objIDENTIFICATION.EXPIRED_DATE
            '                        '.ISSUED_BY = objIDENTIFICATION.ISSUED_BY
            '                        '.FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = objIDENTIFICATION.FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY
            '                        .PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = PK_ID
            '                        .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = itemIDENTIFICATION.Type
            '                        .IDENTITY_NUMBER = itemIDENTIFICATION.Number
            '                        .ISSUE_DATE = itemIDENTIFICATION.Issue_Date
            '                        .EXPIRED_DATE = itemIDENTIFICATION.Expiry_Date
            '                        .ISSUED_BY = itemIDENTIFICATION.Issued_By
            '                        .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = itemIDENTIFICATION.Issued_Country

            '                        'tambahan 5 Jul 2021
            '                        .Comments = itemIDENTIFICATION.Identification_Comment
            '                    End With

            '                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Add(objIDENTIFICATION)
            '                Next
            '            End If
            '        End With

            '        'Save Data With/Without Approval
            '        'If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
            '        '6-Jul-2021 Adi : get Module apakah perlu approval atau tidak sesuai SIPENDAR TYPE yang diisi
            '        If objProfileModule IsNot Nothing Then
            '            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
            '                SiPendarProfile_BLL.SaveAddWithoutApproval(objProfileModule, objSIPENDAR_PROFILE_CLASS)
            '            Else
            '                SiPendarProfile_BLL.SaveAddWithApproval(objProfileModule, objSIPENDAR_PROFILE_CLASS)
            '            End If
            '        Else
            '            SiPendarProfile_BLL.SaveAddWithoutApproval(ObjModule, objSIPENDAR_PROFILE_CLASS)
            '        End If

            '        'Set previousGCN
            '        previousGCN = objScreeningResult.GCN
            '    Next
            'End If
            'End of 16-Sep-2021
            'End of 29-Sep-2021

            'Buat object baru untuk list SIPENDAR_PROFILE
            Dim objSIPENDAR_PROFILE_CLASS As New SiPendarBLL.SIPENDAR_PROFILE_CLASS

            'Ambil data2 dari Sipendar Entity
            objScreeningResult = Nothing
            ' 23 Dec 2021 Ari : buat objglobalparam apakah user ingin satu profile dengan gcn tertentu dibuat tidak bisa sama gcn dibuat lebih daris satu dalam satu hari
            Dim objGlobalParameterPerTypes As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing

            Dim objGlobalParameter As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
            Using objDbSipendar As New SiPendarDAL.SiPendarEntities
                objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = intID).FirstOrDefault
                objGlobalParameter = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault
                objGlobalParameterPerTypes = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 7).FirstOrDefault
            End Using
            'Ambil data2 dari NawadataDevEntities
            Dim objCustomer As SiPendarDAL.goAML_Ref_Customer = Nothing
            Dim objAccounts As SiPendarDAL.goAML_Ref_Account = Nothing
            Dim listCustomer As New List(Of SiPendarDAL.goAML_Ref_Customer)      '1 GCN bisa punya banyak CIF
            Dim listAccount As New List(Of NawaDevDAL.goAML_Ref_Account)
            Dim listAddress As New List(Of SiPendarDAL.goAML_Ref_Address)
            Dim listPhone As New List(Of SiPendarDAL.goAML_Ref_Phone)
            Dim listIdentification As New List(Of SiPendarDAL.goAML_Person_Identification)
            Dim listAddressStakeholder As New List(Of SiPendarDAL.SIPENDAR_StakeHolder_NonCustomer_Addresses)
            Dim listPhoneStakeholder As New List(Of SiPendarDAL.SIPENDAR_StakeHolder_NonCustomer_Phones)
            Dim listIdentificationStakeholder As New List(Of SiPendarDAL.SIPENDAR_StakeHolder_NonCustomer_Identifications)

            Dim listProfileAccount As New List(Of SiPendarBLL.ProfileAccountClass) '' Added 30 Jul 2021

            Using objDbNawa As New SiPendarDAL.SiPendarEntities
                'Ari 25 Oct 2021 Save Sipendar dan Cek apakah dia Non Customer atau tidak
                If objScreeningResult.Stakeholder_Role Is Nothing Then
                    'Get Data Customer
                    If Not objScreeningResult Is Nothing Then
                        objCustomer = objDbNawa.goAML_Ref_Customer.Where(Function(x) x.GCN = objScreeningResult.GCN).FirstOrDefault
                        listCustomer = objDbNawa.goAML_Ref_Customer.Where(Function(x) x.GCN = objScreeningResult.GCN).ToList
                    End If

                    'Get Account, Address, Phone, Identification
                    If listCustomer IsNot Nothing AndAlso listCustomer.Count > 0 Then
                        For Each itemCustomer In listCustomer
                            Dim tempListAccount = objDbNawa.goAML_Ref_Account.Where(Function(x) x.client_number = itemCustomer.CIF).ToList
                            Dim tempListAddress = objDbNawa.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = itemCustomer.PK_Customer_ID).ToList
                            Dim tempListPhone = objDbNawa.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = itemCustomer.PK_Customer_ID).ToList
                            Dim tempListIdentification = objDbNawa.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = itemCustomer.PK_Customer_ID).ToList

                            'List Account
                            If tempListAccount IsNot Nothing AndAlso tempListAccount.Count > 0 Then
                                For Each itemAccount In tempListAccount

                                    Dim tempprofileaccount As New ProfileAccountClass
                                    tempprofileaccount.ObjAccount = itemAccount
                                    If itemAccount.Account_No IsNot Nothing Then
                                        tempprofileaccount.ListATMAccount = objDbNawa.AML_ACCOUNT_ATM.Where(Function(x) x.Account_No = itemAccount.Account_No).ToList
                                    End If

                                    'listAccount.Add(itemAccount)
                                    listProfileAccount.Add(tempprofileaccount)
                                Next
                            End If

                            'List Address
                            If tempListAddress IsNot Nothing AndAlso tempListAddress.Count > 0 Then
                                For Each itemaddress In tempListAddress
                                    listAddress.Add(itemaddress)
                                Next
                            End If

                            'List Phone
                            If tempListPhone IsNot Nothing AndAlso tempListPhone.Count > 0 Then
                                For Each itemphone In tempListPhone
                                    listPhone.Add(itemphone)
                                Next
                            End If

                            'List identification
                            If itemCustomer.FK_Customer_Type_ID = 1 Then    'Hanya perorangan yg punya Identification
                                If tempListIdentification IsNot Nothing AndAlso tempListIdentification.Count > 0 Then
                                    For Each itemidentification In tempListIdentification
                                        listIdentification.Add(itemidentification)
                                    Next
                                End If
                            End If
                            ''Added on 26 Aug 2021
                            If Not objCustomer Is Nothing Then
                                If Not String.IsNullOrWhiteSpace(itemCustomer.INDV_SSN) Then
                                    Dim tempident As New SiPendarDAL.goAML_Person_Identification
                                    tempident.Number = itemCustomer.INDV_SSN
                                    tempident.Type = "KTP"
                                    tempident.Issued_Country = itemCustomer.INDV_Nationality1
                                    listIdentification.Add(tempident)
                                End If

                                If Not String.IsNullOrWhiteSpace(itemCustomer.INDV_Passport_Number) Then
                                    Dim tempident As New SiPendarDAL.goAML_Person_Identification
                                    tempident.Number = itemCustomer.INDV_Passport_Number
                                    tempident.Type = "PAS"
                                    tempident.Issued_Country = itemCustomer.INDV_Passport_Country
                                    listIdentification.Add(tempident)
                                End If
                            End If
                            '' End of 26 Aug 2021
                        Next
                    Else
                        Throw New ApplicationException("Sorry, There is no Customer with GCN" & objScreeningResult.GCN & ", Please contact admin to check Customer Data.")
                    End If
                    With objSIPENDAR_PROFILE_CLASS
                        'SiPendar Profile
                        With objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE


                            'Populate Data Header
                            If Not String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
                                .FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.SelectedItemValue
                            End If
                            If Not objCustomer Is Nothing Then
                                .Fk_goAML_Ref_Customer_Type_id = objCustomer.FK_Customer_Type_ID
                            End If
                            If Not String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue) Then
                                .FK_SIPENDAR_JENIS_WATCHLIST_CODE = cmb_JENIS_WATCHLIST_CODE.SelectedItemValue
                            End If
                            If Not String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue) Then
                                .FK_SIPENDAR_TINDAK_PIDANA_CODE = cmb_TINDAK_PIDANA_CODE.SelectedItemValue
                            End If
                            If Not String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue) Then
                                .FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE = cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue
                            End If
                            If Not objGlobalParameter Is Nothing Then
                                .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
                            End If

                            'tambahan 5-Jul-2021
                            If Not String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue) Then
                                .FK_SIPENDAR_SUMBER_TYPE_CODE = cmb_SUMBER_TYPE_CODE.SelectedItemValue
                            End If

                            'If Not (String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A" Or txt_SUMBER_ID.Text = "NA") Then
                            '    .SUMBER_ID = CInt(txt_SUMBER_ID.Value)
                            'Else
                            '    .SUMBER_ID = Nothing
                            'End If
                            'end of tambahan 5-Jul-2021

                            '21-Jul-2021 Adi : Sumber ID diambil dari ID Watchlist PPATK
                            If objScreeningResult.FK_WATCHLIST_ID_PPATK IsNot Nothing AndAlso objScreeningResult.FK_WATCHLIST_ID_PPATK <> "" Then
                                .SUMBER_ID = objScreeningResult.FK_WATCHLIST_ID_PPATK
                            Else
                                .SUMBER_ID = Nothing
                            End If
                            'End of 21-Jul-2021 Adi : Sumber ID diambil dari ID Watchlist PPATK

                            'tambahan 8-Jul-2021
                            ' 22 Dec 2021 Ari menggambil data submission type di grid result
                            Dim query2 As String = "select b.FK_SIPENDAR_SUBMISSION_TYPE_CODE FROM SIPENDAR_SCREENING_REQUEST_RESULT a inner join SIPENDAR_SCREENING_REQUEST_DETAIL b on a.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = b.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID where a.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = " & objScreeningResult.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
                            Dim data3 As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query2)
                            For Each row3 As DataRow In data3.Rows
                                .FK_SIPENDAR_SUBMISSION_TYPE_CODE = row3("FK_SIPENDAR_SUBMISSION_TYPE_CODE")
                            Next
                            If Not String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue) Then
                                .FK_SIPENDAR_SUBMISSION_TYPE_CODE = cmb_SUBMISSION_TYPE_CODE.SelectedItemValue
                            End If
                            .Submission_Date = DateTime.Now()

                            If (chk_IsGenerateTransaction.Checked) Then
                                .Transaction_DateFrom = CDate(txt_Transaction_DateFrom.Value)
                                .Transaction_DateTo = CDate(txt_Transaction_DateTo.Value)
                                'Ari 15-09-2021 menambahkan fungsi input ke field report indicator dari list indikator
                                If ListIndicator.Count > 0 Then
                                    Dim i As Integer
                                    Dim temp As String
                                    i = 0
                                    temp = ""
                                    For Each itemindicator In ListIndicator
                                        i += 1
                                        If ListIndicator.Count = 1 Then
                                            'temp = getReportIndicatorByKode(item.FK_Indicator)
                                            temp = itemindicator.FK_Indicator
                                        Else
                                            If i = 1 Then
                                                'temp = getReportIndicatorByKode(item.FK_Indicator)
                                                temp = itemindicator.FK_Indicator
                                            Else
                                                'temp += "," + getReportIndicatorByKode(item.FK_Indicator)
                                                temp += "," + itemindicator.FK_Indicator
                                            End If
                                        End If
                                    Next
                                    .Report_Indicator = temp
                                End If
                                'End Ari 15-09-2021
                            End If
                            ''end of tambahan 8-Jul-2021
                            '' Added by Felix 15-Sep-2021
                            .FK_SIPENDAR_SCREENING_REQUEST_ID = objScreeningResult.PK_SIPENDAR_SCREENING_REQUEST_ID
                            .FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = objScreeningResult.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
                            ''End of 15-Sep-2021


                            If Not objCustomer Is Nothing Then
                                Dim objAddress = listAddress.FirstOrDefault

                                .GCN = objCustomer.GCN

                                If objCustomer.FK_Customer_Type_ID = 1 Then     'Individual
                                    .INDV_NAME = objCustomer.INDV_Last_Name
                                    .INDV_FK_goAML_Ref_Nama_Negara_CODE = objCustomer.INDV_Nationality1

                                    If objAddress IsNot Nothing Then
                                        .INDV_ADDRESS = objAddress.Address
                                    End If

                                    .INDV_PLACEOFBIRTH = objCustomer.INDV_Birth_Place
                                    .INDV_DOB = objCustomer.INDV_BirthDate
                                Else    'Korporasi
                                    .CORP_NAME = objCustomer.Corp_Name
                                    .CORP_FK_goAML_Ref_Nama_Negara_CODE = objCustomer.Corp_Incorporation_Country_Code
                                    .CORP_NPWP = objCustomer.Corp_Tax_Number
                                    .CORP_NO_IZIN_USAHA = objCustomer.Corp_Incorporation_Number
                                End If
                            End If

                            .Keterangan = Trim(txt_Keterangan.Value)

                            If Not objScreeningResult Is Nothing Then
                                If Not IsNothing(objScreeningResult.TotalSimilarity) Then
                                    .Similarity = CDbl(objScreeningResult.TotalSimilarity) * 100
                                End If
                            End If
                        End With

                        'SiPendarProfile Account
                        If listProfileAccount IsNot Nothing AndAlso listProfileAccount.Count > 0 Then
                            Dim PK_ID As Long = 0
                            Dim PK_ATM_ID As Long = 0
                            For Each itemAccount In listProfileAccount
                                PK_ID = PK_ID - 1
                                Dim objAccount As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT
                                With objAccount
                                    .PK_SIPENDAR_PROFILE_ACCOUNT_ID = PK_ID
                                    .CIFNO = itemAccount.ObjAccount.client_number
                                    .Fk_goAML_Ref_Jenis_Rekening_CODE = itemAccount.ObjAccount.personal_account_type
                                    .Fk_goAML_Ref_Status_Rekening_CODE = itemAccount.ObjAccount.status_code

                                    If objGlobalParameter IsNot Nothing Then
                                        .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
                                    End If

                                    .NOREKENING = itemAccount.ObjAccount.Account_No
                                End With

                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Add(objAccount)

                                '' Added on 30 Jul 2021

                                For Each itemATM In itemAccount.ListATMAccount
                                    PK_ATM_ID = PK_ATM_ID - 1
                                    Dim objATM As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT_ATM
                                    With objATM
                                        .PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = PK_ATM_ID
                                        .FK_SIPENDAR_PROFILE_ACCOUNT_ID = objAccount.PK_SIPENDAR_PROFILE_ACCOUNT_ID
                                        .NOATM = itemATM.ATM_NO
                                    End With
                                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Add(objATM)
                                Next
                                '' end of 30 Jul 2021
                            Next
                        End If

                        'SiPendarProfile Address
                        If listAddress IsNot Nothing AndAlso listAddress.Count > 0 Then
                            Dim PK_ID As Long = 0
                            For Each itemAddress In listAddress
                                PK_ID = PK_ID - 1
                                Dim objAddress As New SiPendarDAL.SIPENDAR_PROFILE_ADDRESS
                                With objAddress
                                    .PK_SIPENDAR_PROFILE_ADDRESS_ID = PK_ID
                                    .FK_goAML_Ref_Kategori_Kontak_CODE = itemAddress.Address_Type
                                    .ADDRESS = itemAddress.Address
                                    .CITY = itemAddress.City
                                    .FK_goAML_Ref_Nama_Negara_CODE = itemAddress.Country_Code
                                    .ZIP = itemAddress.Zip

                                    'tambahan 5 Jul 2021
                                    .TOWN = itemAddress.Town
                                    .STATE = itemAddress.State
                                    .COMMENTS = itemAddress.Comments
                                End With

                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Add(objAddress)
                            Next
                        End If

                        'SiPendarProfile PHONE
                        If listPhone IsNot Nothing AndAlso listPhone.Count > 0 Then
                            Dim PK_ID As Long = 0
                            For Each itemPHONE In listPhone
                                PK_ID = PK_ID - 1
                                Dim objPHONE As New SiPendarDAL.SIPENDAR_PROFILE_PHONE
                                With objPHONE
                                    .PK_SIPENDAR_PROFILE_PHONE_ID = PK_ID
                                    .FK_goAML_Ref_Kategori_Kontak_CODE = itemPHONE.Tph_Contact_Type
                                    .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = itemPHONE.Tph_Communication_Type
                                    .COUNTRY_PREFIX = itemPHONE.tph_country_prefix
                                    .PHONE_NUMBER = itemPHONE.tph_number

                                    'tambahan 5 Jul 2021
                                    .EXTENSION_NUMBER = itemPHONE.tph_extension
                                    .COMMENTS = itemPHONE.comments
                                End With

                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Add(objPHONE)
                            Next
                        End If



                        'SiPendarProfile IDENTIFICATION
                        If listIdentification IsNot Nothing AndAlso listIdentification.Count > 0 Then
                            Dim PK_ID As Long = 0
                            For Each itemIDENTIFICATION In listIdentification
                                Dim objCek = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.IDENTITY_NUMBER = itemIDENTIFICATION.Number)
                                If objCek IsNot Nothing Then
                                    Continue For
                                End If
                                PK_ID = PK_ID - 1
                                Dim objIDENTIFICATION As New SiPendarDAL.SIPENDAR_PROFILE_IDENTIFICATION
                                With objIDENTIFICATION
                                    '.Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = objIDENTIFICATION.Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE
                                    '.IDENTITY_NUMBER = objIDENTIFICATION.IDENTITY_NUMBER
                                    '.ISSUE_DATE = objIDENTIFICATION.ISSUE_DATE
                                    '.EXPIRED_DATE = objIDENTIFICATION.EXPIRED_DATE
                                    '.ISSUED_BY = objIDENTIFICATION.ISSUED_BY
                                    '.FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = objIDENTIFICATION.FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY
                                    .PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = PK_ID
                                    .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = itemIDENTIFICATION.Type
                                    .IDENTITY_NUMBER = itemIDENTIFICATION.Number
                                    .ISSUE_DATE = itemIDENTIFICATION.Issue_Date
                                    .EXPIRED_DATE = itemIDENTIFICATION.Expiry_Date
                                    .ISSUED_BY = itemIDENTIFICATION.Issued_By
                                    .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = itemIDENTIFICATION.Issued_Country

                                    'tambahan 5 Jul 2021
                                    .Comments = itemIDENTIFICATION.Identification_Comment
                                End With

                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Add(objIDENTIFICATION)
                            Next
                        End If
                        ' 23 Dec 2021 Ari : Penambahan Validasi jika gcn tersebut ada di approval profile
                        Dim Modulekey As String = ""
                        If objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID = 1 Then
                            Modulekey = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID & "-" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & "-" & Now.ToString("yyyy-MM-dd") & "_0+" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE
                            'ModuleKey = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID & "-" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & "-" & Now.ToString("yyyy-MM-dd")
                        Else
                            Modulekey = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID & "-" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & "-" & Now.ToString("yyyy-MM-dd") & "_" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID & "+" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE
                        End If

                        If SiPendarProfile_BLL.IsExistsInApprovalProfileType(objProfileModule.ModuleName, Modulekey, "1", objGlobalParameterPerTypes.ParameterValue) Then
                            'LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."
                            If objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID = 1 Then
                                Throw New ApplicationException("Sorry, New Profile for GCN " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & " today is already exists in Pending Approval.")
                            Else
                                Throw New ApplicationException("Sorry, New Profile for GCN " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & " ,SUMBER ID " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID & " , and SUBMISSION Type " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE & " today is already exists in Pending Approval.")
                            End If
                            Panelconfirmation.Hidden = False
                            FormPanelInput.Hidden = True
                        End If

                        ' 10 Jan 2022 Ari penambahan validasi gcn dengan format baru :
                        Dim GCN As String = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN
                        Dim submissiontype As String = ""
                        submissiontype = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE
                        Dim sumber_id As String = ""
                        If objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID.ToString <> "" Then
                            sumber_id = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID
                        End If

                        Dim objGlobalParameterPerTypes2 As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
                        Dim objprofile As New SiPendarDAL.SIPENDAR_PROFILE
                        Using objDbSipendar As New SiPendarDAL.SiPendarEntities
                            objGlobalParameterPerTypes2 = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 7).FirstOrDefault
                            If objGlobalParameterPerTypes2.ParameterValue = 1 Then
                                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue).FirstOrDefault
                                Else
                                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue And x.FK_SIPENDAR_SUBMISSION_TYPE_CODE = submissiontype And x.SUMBER_ID = sumber_id).FirstOrDefault
                                End If
                            Else
                                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault
                                Else
                                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_SUBMISSION_TYPE_CODE = submissiontype And x.SUMBER_ID = sumber_id).FirstOrDefault
                                End If

                            End If

                        End Using

                        ' 21 Dec 2021 Ari : Penambahan untuk keterengan validasi
                        If objprofile IsNot Nothing Then
                            If Not (objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.INDV_NAME = Nothing Or objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.INDV_NAME = "") Then
                                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                                    Throw New ApplicationException("<br>GCN : " & GCN & " - " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.INDV_NAME & " sudah pernah dilaporkan pada hari ini.")
                                Else
                                    Throw New ApplicationException("<br>GCN-SumberID-SubmissionType : " & GCN & " - " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID & " - " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE & " and " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.INDV_NAME & " sudah pernah dilaporkan pada hari ini.")
                                End If
                            End If

                            If Not (objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.CORP_NAME = Nothing Or objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.CORP_NAME = "") Then
                                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                                    Throw New ApplicationException("<br>GCN : " & GCN & " - " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.CORP_NAME & " sudah pernah dilaporkan pada hari ini.")
                                Else
                                    Throw New ApplicationException("<br>GCN-SumberID-SubmissionType : " & GCN & " - " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID & " - " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE & " and " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.CORP_NAME & " sudah pernah dilaporkan pada hari ini.")
                                End If
                            End If
                        End If



                    End With

                Else
                    Dim objstakeholder As New SiPendarDAL.SIPENDAR_StakeHolder_NonCustomer
                    Dim liststakeholder As New List(Of SiPendarDAL.SIPENDAR_StakeHolder_NonCustomer)
                    If Not objScreeningResult Is Nothing Then
                        objstakeholder = objDbNawa.SIPENDAR_StakeHolder_NonCustomer.Where(Function(x) x.Unique_Key = objScreeningResult.GCN).FirstOrDefault
                        liststakeholder = objDbNawa.SIPENDAR_StakeHolder_NonCustomer.Where(Function(x) x.Unique_Key = objScreeningResult.GCN).ToList
                    End If

                    'Get Account, Address, Phone, Identification
                    If liststakeholder IsNot Nothing AndAlso liststakeholder.Count > 0 Then
                        For Each itemStakeholder In liststakeholder

                            Dim tempListAddress = objDbNawa.SIPENDAR_StakeHolder_NonCustomer_Addresses.Where(Function(x) x.FK_SIPENDAR_StakeHolder_NonCustomer_ID = itemStakeholder.PK_SIPENDAR_StakeHolder_NonCustomer_ID).ToList
                            Dim tempListPhone = objDbNawa.SIPENDAR_StakeHolder_NonCustomer_Phones.Where(Function(x) x.FK_SIPENDAR_StakeHolder_NonCustomer_ID = itemStakeholder.PK_SIPENDAR_StakeHolder_NonCustomer_ID).ToList
                            Dim tempListIdentification = objDbNawa.SIPENDAR_StakeHolder_NonCustomer_Identifications.Where(Function(x) x.FK_SIPENDAR_StakeHolder_NonCustomer_ID = itemStakeholder.PK_SIPENDAR_StakeHolder_NonCustomer_ID).ToList

                            'List Address
                            If tempListAddress IsNot Nothing AndAlso tempListAddress.Count > 0 Then
                                For Each itemaddress In tempListAddress
                                    listAddressStakeholder.Add(itemaddress)
                                Next
                            End If

                            'List Phone
                            If tempListPhone IsNot Nothing AndAlso tempListPhone.Count > 0 Then
                                For Each itemphone In tempListPhone
                                    listPhoneStakeholder.Add(itemphone)
                                Next
                            End If

                            'List identification
                            If tempListIdentification IsNot Nothing AndAlso tempListIdentification.Count > 0 Then
                                For Each itemidentification In tempListIdentification
                                    listIdentificationStakeholder.Add(itemidentification)
                                Next
                            End If
                        Next
                    Else
                        Throw New ApplicationException("Sorry, There is no StakeHolder with UniqueKey " & objScreeningResult.GCN & ", Please contact admin to check StakeHolder Data.")
                    End If

                    With objSIPENDAR_PROFILE_CLASS
                        'SiPendar Profile
                        With objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE

                            'Populate Data Header
                            If Not String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
                                .FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.SelectedItemValue
                            End If

                            .Fk_goAML_Ref_Customer_Type_id = 1

                            If Not String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue) Then
                                .FK_SIPENDAR_JENIS_WATCHLIST_CODE = cmb_JENIS_WATCHLIST_CODE.SelectedItemValue
                            End If
                            If Not String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue) Then
                                .FK_SIPENDAR_TINDAK_PIDANA_CODE = cmb_TINDAK_PIDANA_CODE.SelectedItemValue
                            End If
                            If Not String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue) Then
                                .FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE = cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue
                            End If
                            If Not objGlobalParameter Is Nothing Then
                                .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
                            End If

                            'tambahan 5-Jul-2021
                            If Not String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue) Then
                                .FK_SIPENDAR_SUMBER_TYPE_CODE = cmb_SUMBER_TYPE_CODE.SelectedItemValue
                            End If

                            'If Not (String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A" Or txt_SUMBER_ID.Text = "NA") Then
                            '    .SUMBER_ID = CInt(txt_SUMBER_ID.Value)
                            'Else
                            '    .SUMBER_ID = Nothing
                            'End If
                            'end of tambahan 5-Jul-2021

                            '21-Jul-2021 Adi : Sumber ID diambil dari ID Watchlist PPATK
                            If objScreeningResult.FK_WATCHLIST_ID_PPATK IsNot Nothing AndAlso objScreeningResult.FK_WATCHLIST_ID_PPATK <> "" Then
                                .SUMBER_ID = objScreeningResult.FK_WATCHLIST_ID_PPATK
                            Else
                                .SUMBER_ID = Nothing
                            End If
                            'End of 21-Jul-2021 Adi : Sumber ID diambil dari ID Watchlist PPATK

                            'tambahan 8-Jul-2021
                            ' 22 Dec 2021 Ari menggambil data submission type di grid result
                            Dim query2 As String = "select b.FK_SIPENDAR_SUBMISSION_TYPE_CODE FROM SIPENDAR_SCREENING_REQUEST_RESULT a inner join SIPENDAR_SCREENING_REQUEST_DETAIL b on a.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = b.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID where a.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = " & objScreeningResult.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
                            Dim data3 As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query2)
                            For Each row3 As DataRow In data3.Rows
                                .FK_SIPENDAR_SUBMISSION_TYPE_CODE = row3("FK_SIPENDAR_SUBMISSION_TYPE_CODE")
                            Next
                            'If Not String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue) Then
                            '    .FK_SIPENDAR_SUBMISSION_TYPE_CODE = cmb_SUBMISSION_TYPE_CODE.SelectedItemValue
                            'End If
                            .Submission_Date = DateTime.Now()

                            If (chk_IsGenerateTransaction.Checked) Then
                                .Transaction_DateFrom = CDate(txt_Transaction_DateFrom.Value)
                                .Transaction_DateTo = CDate(txt_Transaction_DateTo.Value)
                                'Ari 15-09-2021 menambahkan fungsi input ke field report indicator dari list indikator
                                If ListIndicator.Count > 0 Then
                                    Dim i As Integer
                                    Dim temp As String
                                    i = 0
                                    temp = ""
                                    For Each itemindicator In ListIndicator
                                        i += 1
                                        If ListIndicator.Count = 1 Then
                                            'temp = getReportIndicatorByKode(item.FK_Indicator)
                                            temp = itemindicator.FK_Indicator
                                        Else
                                            If i = 1 Then
                                                'temp = getReportIndicatorByKode(item.FK_Indicator)
                                                temp = itemindicator.FK_Indicator
                                            Else
                                                'temp += "," + getReportIndicatorByKode(item.FK_Indicator)
                                                temp += "," + itemindicator.FK_Indicator
                                            End If
                                        End If
                                    Next
                                    .Report_Indicator = temp
                                End If
                                'End Ari 15-09-2021
                            End If
                            ''end of tambahan 8-Jul-2021
                            '' Added by Felix 15-Sep-2021
                            .FK_SIPENDAR_SCREENING_REQUEST_ID = objScreeningResult.PK_SIPENDAR_SCREENING_REQUEST_ID
                            .FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = objScreeningResult.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
                            ''End of 15-Sep-2021


                            If Not objstakeholder Is Nothing Then
                                Dim objAddressstak = listAddressStakeholder.FirstOrDefault

                                .GCN = objstakeholder.Unique_Key
                                .INDV_NAME = objstakeholder.Name
                                .INDV_DOB = objstakeholder.Birth_Date
                                .INDV_PLACEOFBIRTH = objstakeholder.Birth_Place
                                .INDV_FK_goAML_Ref_Nama_Negara_CODE = objstakeholder.Country_Code
                                If objAddressstak IsNot Nothing Then
                                    .INDV_ADDRESS = objAddressstak.Address
                                End If



                            End If

                            .Keterangan = Trim(txt_Keterangan.Value)

                            If Not objScreeningResult Is Nothing Then
                                If Not IsNothing(objScreeningResult.TotalSimilarity) Then
                                    .Similarity = CDbl(objScreeningResult.TotalSimilarity) * 100
                                End If
                            End If
                        End With

                        'SiPendarProfile Account
                        If listProfileAccount IsNot Nothing AndAlso listProfileAccount.Count > 0 Then
                            Dim PK_ID As Long = 0
                            Dim PK_ATM_ID As Long = 0
                            For Each itemAccount In listProfileAccount
                                PK_ID = PK_ID - 1
                                Dim objAccount As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT
                                With objAccount
                                    .PK_SIPENDAR_PROFILE_ACCOUNT_ID = PK_ID
                                    .CIFNO = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER where PK_GlobalReportParameter_ID = 104", Nothing)
                                    .Fk_goAML_Ref_Jenis_Rekening_CODE = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER where PK_GlobalReportParameter_ID = 107", Nothing)
                                    .Fk_goAML_Ref_Status_Rekening_CODE = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER where PK_GlobalReportParameter_ID = 106", Nothing)

                                    If objGlobalParameter IsNot Nothing Then
                                        .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
                                    End If

                                    .NOREKENING = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER where PK_GlobalReportParameter_ID = 105", Nothing)
                                End With

                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Add(objAccount)

                                '' Added on 30 Jul 2021

                                For Each itemATM In itemAccount.ListATMAccount
                                    PK_ATM_ID = PK_ATM_ID - 1
                                    Dim objATM As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT_ATM
                                    With objATM
                                        .PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = PK_ATM_ID
                                        .FK_SIPENDAR_PROFILE_ACCOUNT_ID = objAccount.PK_SIPENDAR_PROFILE_ACCOUNT_ID
                                        .NOATM = itemATM.ATM_NO
                                    End With
                                    objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Add(objATM)
                                Next
                                '' end of 30 Jul 2021
                            Next
                        Else
                            Dim PK_ID As Long = 0
                            Dim PK_ATM_ID As Long = 0

                            PK_ID = PK_ID - 1
                            Dim objAccount As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT
                            With objAccount
                                .PK_SIPENDAR_PROFILE_ACCOUNT_ID = PK_ID
                                .CIFNO = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER where PK_GlobalReportParameter_ID = 104", Nothing)
                                .Fk_goAML_Ref_Jenis_Rekening_CODE = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER where PK_GlobalReportParameter_ID = 107", Nothing)
                                .Fk_goAML_Ref_Status_Rekening_CODE = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER where PK_GlobalReportParameter_ID = 106", Nothing)

                                If objGlobalParameter IsNot Nothing Then
                                    .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
                                End If

                                .NOREKENING = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER where PK_GlobalReportParameter_ID = 105", Nothing)
                            End With

                            objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Add(objAccount)




                        End If

                        'SiPendarProfile Address
                        If listAddressStakeholder IsNot Nothing AndAlso listAddressStakeholder.Count > 0 Then
                            Dim PK_ID As Long = 0
                            For Each itemAddress In listAddressStakeholder
                                PK_ID = PK_ID - 1
                                Dim objAddress As New SiPendarDAL.SIPENDAR_PROFILE_ADDRESS
                                With objAddress
                                    .PK_SIPENDAR_PROFILE_ADDRESS_ID = PK_ID
                                    .FK_goAML_Ref_Kategori_Kontak_CODE = itemAddress.Address_Type
                                    .ADDRESS = itemAddress.Address
                                    .CITY = itemAddress.City
                                    .FK_goAML_Ref_Nama_Negara_CODE = itemAddress.Country_code
                                    .ZIP = itemAddress.Zip

                                    'tambahan 5 Jul 2021
                                    .TOWN = itemAddress.Town
                                    .STATE = itemAddress.State
                                    .COMMENTS = itemAddress.Comments
                                End With

                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Add(objAddress)
                            Next
                        End If

                        'SiPendarProfile PHONE
                        If listPhoneStakeholder IsNot Nothing AndAlso listPhoneStakeholder.Count > 0 Then
                            Dim PK_ID As Long = 0
                            For Each itemPHONE In listPhoneStakeholder
                                PK_ID = PK_ID - 1
                                Dim objPHONE As New SiPendarDAL.SIPENDAR_PROFILE_PHONE
                                With objPHONE
                                    .PK_SIPENDAR_PROFILE_PHONE_ID = PK_ID
                                    .FK_goAML_Ref_Kategori_Kontak_CODE = itemPHONE.Tph_Contact_Type
                                    .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = itemPHONE.Tph_Communication_Type
                                    .COUNTRY_PREFIX = itemPHONE.Tph_Country_Prefix
                                    .PHONE_NUMBER = itemPHONE.Tph_Number

                                    'tambahan 5 Jul 2021
                                    .EXTENSION_NUMBER = itemPHONE.Tph_Extension
                                    .COMMENTS = itemPHONE.Comments
                                End With

                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Add(objPHONE)
                            Next
                        End If



                        'SiPendarProfile IDENTIFICATION
                        If listIdentificationStakeholder IsNot Nothing AndAlso listIdentificationStakeholder.Count > 0 Then
                            Dim PK_ID As Long = 0
                            For Each itemIDENTIFICATION In listIdentificationStakeholder
                                Dim objCek = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.IDENTITY_NUMBER = itemIDENTIFICATION.Number)
                                If objCek IsNot Nothing Then
                                    Continue For
                                End If
                                PK_ID = PK_ID - 1
                                Dim objIDENTIFICATION As New SiPendarDAL.SIPENDAR_PROFILE_IDENTIFICATION
                                With objIDENTIFICATION
                                    '.Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = objIDENTIFICATION.Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE
                                    '.IDENTITY_NUMBER = objIDENTIFICATION.IDENTITY_NUMBER
                                    '.ISSUE_DATE = objIDENTIFICATION.ISSUE_DATE
                                    '.EXPIRED_DATE = objIDENTIFICATION.EXPIRED_DATE
                                    '.ISSUED_BY = objIDENTIFICATION.ISSUED_BY
                                    '.FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = objIDENTIFICATION.FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY
                                    .PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = PK_ID
                                    .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = itemIDENTIFICATION.Type
                                    .IDENTITY_NUMBER = itemIDENTIFICATION.Number
                                    .ISSUE_DATE = itemIDENTIFICATION.Issue_Date
                                    .EXPIRED_DATE = itemIDENTIFICATION.Expiry_Date_
                                    .ISSUED_BY = itemIDENTIFICATION.Issued_By
                                    .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = itemIDENTIFICATION.Issued_Country_

                                    ''tambahan 5 Jul 2021
                                    .Comments = itemIDENTIFICATION.Comments
                                End With

                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Add(objIDENTIFICATION)
                            Next
                        End If
                        ' 23 Dec 2021 Ari : Penambahan Validasi jika gcn tersebut ada di approval profile
                        Dim newModuleKey As String = ""
                        If objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID = 1 Then
                            newModuleKey = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID & "-" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & "-" & Now.ToString("yyyy-MM-dd") & "_0+" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE
                            'newModuleKey = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID & "-" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & "-" & Now.ToString("yyyy-MM-dd")
                        ElseIf objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID = 2 Then
                            newModuleKey = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID & "-" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & "-" & Now.ToString("yyyy-MM-dd") & "_" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID & "+" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE
                        End If
                        '' Added by Felix 02 Aug 2021
                        If SiPendarProfile_BLL.IsExistsInApprovalProfileType(objProfileModule.ModuleName, newModuleKey, "1", objGlobalParameterPerTypes.ParameterValue) Then
                            'LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."
                            If objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID = 1 Then
                                Throw New ApplicationException("Sorry, New Profile for GCN " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & " today is already exists in Pending Approval.")
                            Else
                                Throw New ApplicationException("Sorry, New Profile for GCN " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & " ,SUMBER ID " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID & " , and SUBMISSION Type " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE & " today is already exists in Pending Approval.")

                            End If
                            Panelconfirmation.Hidden = False
                            FormPanelInput.Hidden = True
                        End If

                        ' 10 Jan 2022 Ari penambahan validasi gcn dengan format baru :
                        Dim GCN As String = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN
                        Dim submissiontype As String = ""
                        submissiontype = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE
                        Dim sumber_id As String = ""
                        If objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID IsNot Nothing AndAlso objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID <> "" Then
                            sumber_id = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID
                        End If


                        Dim objGlobalParameterPerTypes2 As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
                        Dim objprofile As New SiPendarDAL.SIPENDAR_PROFILE
                        Using objDbSipendar As New SiPendarDAL.SiPendarEntities
                            objGlobalParameterPerTypes2 = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 7).FirstOrDefault
                            If objGlobalParameterPerTypes2.ParameterValue = 1 Then
                                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue).FirstOrDefault
                                Else
                                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue And x.FK_SIPENDAR_SUBMISSION_TYPE_CODE = submissiontype And x.SUMBER_ID = sumber_id).FirstOrDefault
                                End If
                            Else
                                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault
                                Else
                                    objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_SUBMISSION_TYPE_CODE = submissiontype And x.SUMBER_ID = sumber_id).FirstOrDefault
                                End If

                            End If

                        End Using

                        ' 21 Dec 2021 Ari : Penambahan untuk keterengan validasi
                        If objprofile IsNot Nothing Then
                            If Not (objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.INDV_NAME = Nothing Or objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.INDV_NAME = "") Then
                                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                                    Throw New ApplicationException("<br>GCN : " & GCN & " - " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.INDV_NAME & " sudah pernah dilaporkan pada hari ini.")
                                Else
                                    Throw New ApplicationException("<br>GCN-SumberID-SubmissionType : " & GCN & " - " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID & " - " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE & " and " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.INDV_NAME & " sudah pernah dilaporkan pada hari ini.")
                                End If
                            End If

                            If Not (objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.CORP_NAME = Nothing Or objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.CORP_NAME = "") Then
                                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                                    Throw New ApplicationException("<br>GCN : " & GCN & " - " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.CORP_NAME & " sudah pernah dilaporkan pada hari ini.")
                                Else
                                    Throw New ApplicationException("<br>GCN-SumberID-SubmissionType : " & GCN & " - " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID & " - " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE & " and " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.CORP_NAME & " sudah pernah dilaporkan pada hari ini.")
                                End If
                            End If
                        End If



                    End With
                End If
            End Using


            'Save Data With/Without Approval
            'If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
            '6-Jul-2021 Adi : get Module apakah perlu approval atau tidak sesuai SIPENDAR TYPE yang diisi
            If objProfileModule IsNot Nothing Then
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
                    SiPendarProfile_BLL.SaveAddWithoutApproval(objProfileModule, objSIPENDAR_PROFILE_CLASS)
                Else
                    SiPendarProfile_BLL.SaveAddWithApproval(objProfileModule, objSIPENDAR_PROFILE_CLASS)
                End If
            Else
                SiPendarProfile_BLL.SaveAddWithoutApproval(ObjModule, objSIPENDAR_PROFILE_CLASS)
            End If

            'Set previousGCN
            previousGCN = objScreeningResult.GCN

            'Save Data With/Without Approval
            If objProfileModule IsNot Nothing Then
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
                    LblConfirmation.Text = "Data Saved into Database"
                Else
                    LblConfirmation.Text = "Data Saved into Pending Approval"
                End If
            Else
                LblConfirmation.Text = "Data Saved into Database"
            End If

            'Tampilkan confirmation
            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True
            'ClearSession() 'Ari 15-09-2021 -- Pindah ke luar, setelah Save Other Info
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Protected Sub SetControlVisibility(sender As Object, e As DirectEventArgs)
        Try
            '3-Jul-2021 Adi : Hide field yang awalnya ada di XML tapi di XSD tidak ada
            cmb_TINDAK_PIDANA_CODE.IsHidden = True

            'Sesuaikan dengan SIPENDAR_TYPE
            If Not String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                    cmb_JENIS_WATCHLIST_CODE.IsHidden = False
                    cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = False
                    cmb_SUMBER_TYPE_CODE.IsHidden = True
                    txt_SUMBER_ID.Hidden = True

                    fs_Generate_Transaction.Hidden = True
                    chk_IsGenerateTransaction.Checked = False
                    txt_Transaction_DateFrom.Value = Nothing
                    txt_Transaction_DateTo.Value = Nothing
                Else
                    cmb_JENIS_WATCHLIST_CODE.IsHidden = True
                    cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = True
                    cmb_SUMBER_TYPE_CODE.IsHidden = False
                    'txt_SUMBER_ID.Hidden = False

                    fs_Generate_Transaction.Hidden = False
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "7-Jul-2021 Adi"
    'Penambahan fitur Generate Transaction From - To
    Protected Sub chk_IsGenerateTransaction_Change(sender As Object, e As EventArgs)
        Try
            If chk_IsGenerateTransaction.Checked = True Then
                txt_Transaction_DateFrom.Hidden = False
                txt_Transaction_DateTo.Hidden = False
                PanelReportIndicator.Hidden = False 'Ari 15-09-2021
            Else
                txt_Transaction_DateFrom.Hidden = True
                txt_Transaction_DateTo.Hidden = True
                PanelReportIndicator.Hidden = True 'Ari 15-09-2021
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub SaveScreeningResultOtherInfo()
        Try

            Dim strQuery As String = ""
            Dim strQuery1 As String = "INSERT INTO SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO ("
            strQuery1 += "FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID, "
            strQuery1 += "FK_SIPENDAR_TYPE_ID, "
            strQuery1 += "FK_SIPENDAR_JENIS_WATCHLIST_CODE, "
            strQuery1 += "FK_SIPENDAR_TINDAK_PIDANA_CODE, "
            strQuery1 += "FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE, "
            strQuery1 += "FK_SIPENDAR_ORGANISASI_CODE, "
            strQuery1 += "Keterangan, "
            strQuery1 += "FK_SIPENDAR_SUMBER_TYPE_CODE, "
            strQuery1 += "SUMBER_ID, "
            strQuery1 += "FK_SIPENDAR_SUBMISSION_TYPE_CODE, "
            strQuery1 += "IsGenerateTransaction, "
            strQuery1 += "Transaction_DateFrom, "
            strQuery1 += "Transaction_DateTo, "
            strQuery1 += "GCN, "
            strQuery1 += "Status, " ''Added by Felix 21 Jul 2021, tambah Status
            strQuery1 += "CreatedBy, "
            strQuery1 += "CreatedDate,"
            strQuery1 += "LastUpdateDate)"

            Dim strQuery2 As String = ""

            'Edited by Felix on 16-Sep-2021 Tambah IF untuk SelectAll 
            'If cb_IsSelectAllResult.Checked Then
            'Save to Database
            'Using objDbSipendar As New SiPendarDAL.SiPendarEntities
            '        Dim previousGCN As String = ""
            '        For Each row As DataRow In dt_ScreeningResult.Rows
            '            Dim recordID = row.Item("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID").ToString()

            '            'Ambil data2 dari Sipendar Screening Result
            '            Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT = Nothing
            '            Dim objGlobalParameter As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
            '            objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID).FirstOrDefault
            '            objGlobalParameter = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault

            '            If objScreeningResult IsNot Nothing Then
            '                'Skip jika 2 GCN yang sama dipilih
            '                If objScreeningResult.GCN = previousGCN Then
            '                    Continue For
            '                End If

            '                '' Added by Felix 21-Sep-2021
            '                '' Delete dlu Other_Info nya sebelum Insert
            '                Dim strQueryDelete = "Delete SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO WHERE FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = '" & recordID & "'"
            '                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryDelete, Nothing)
            '                '' End of 21-Sep-2021

            '                strQuery2 = "VALUES ("
            '                strQuery2 += recordID & ","
            '                strQuery2 += IIf(String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue), "NULL", cmb_SIPENDAR_TYPE_ID.SelectedItemValue) & ","

            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue), "NULL", "'" & cmb_JENIS_WATCHLIST_CODE.SelectedItemValue & "'") & ","
            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue), "NULL", "'" & cmb_TINDAK_PIDANA_CODE.SelectedItemValue & "'") & ","
            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue), "NULL", "'" & cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue & "'") & ","

            '                strQuery2 += IIf(objGlobalParameter Is Nothing, "NULL", "'" & objGlobalParameter.ParameterValue & "'") & ","
            '                strQuery2 += IIf(String.IsNullOrEmpty(txt_Keterangan.Value), "NULL", "'" & txt_Keterangan.Value & "'") & ","

            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue), "NULL", "'" & cmb_SUMBER_TYPE_CODE.SelectedItemValue & "'") & ","
            '                'strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A", "NULL", CLng(txt_SUMBER_ID.Value).ToString) & ","
            '                strQuery2 += row.Item("FK_WATCHLIST_ID_PPATK").ToString() & "," '' Edited by Felix 21-Sep-2021

            '                strQuery2 += IIf(String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue), "NULL", "'" & cmb_SUBMISSION_TYPE_CODE.SelectedItemValue & "'") & ","
            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "0", "1") & ","
            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "NULL", "'" & CDate(txt_Transaction_DateFrom.Value).ToString("yyyy-MM-dd HH:mm:ss") & "'") & ","
            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "NULL", "'" & CDate(txt_Transaction_DateTo.Value).ToString("yyyy-MM-dd HH:mm:ss") & "'") & ","

            '                strQuery2 += "'" & objScreeningResult.GCN & "',"

            '                ''Added by Felix 21 Jul 2021, tambah Status
            '                Dim objProfileModule As NawaDAL.Module = Nothing
            '                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
            '                    objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PROAKTIF")
            '                Else
            '                    objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PENGAYAAN")
            '                End If
            '                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
            '                    strQuery2 += "'Accept',"
            '                Else
            '                    strQuery2 += "'Waiting For Approval'," ''Added by Felix 21 Jul 2021, tambah Status
            '                End If
            '                ''Added by Felix 21 Jul 2021, tambah Status

            '                strQuery2 += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
            '                strQuery2 += "'" & CDate(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss") & "'"
            '                strQuery2 += ")"

            '                strQuery = strQuery1 & strQuery2
            '                'Throw New Exception(strQuery)

            '                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            '                'Set previousGCN
            '                previousGCN = objScreeningResult.GCN
            '            End If

            '        Next
            '    End Using
            'Else
            '    'Save by looping selected item
            '    Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)

            '    'Save to Database
            '    Using objDbSipendar As New SiPendarDAL.SiPendarEntities
            '        Dim previousGCN As String = ""
            '        For Each item As SelectedRow In smScreeningResult.SelectedRows
            '            Dim recordID = item.RecordID.ToString

            '            'Ambil data2 dari Sipendar Screening Result
            '            Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT = Nothing
            '            Dim objGlobalParameter As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
            '            objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID).FirstOrDefault
            '            objGlobalParameter = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault

            '            If objScreeningResult IsNot Nothing Then
            '                'Skip jika 2 GCN yang sama dipilih
            '                If objScreeningResult.GCN = previousGCN Then
            '                    Continue For
            '                End If

            '                '' Added by Felix 21-Sep-2021
            '                '' Delete dlu Other_Info nya sebelum Insert
            '                Dim strQueryDelete = "Delete SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO WHERE FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = '" & recordID & "'"
            '                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryDelete, Nothing)
            '                '' End of 21-Sep-2021

            '                strQuery2 = "VALUES ("
            '                strQuery2 += recordID & ","
            '                strQuery2 += IIf(String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue), "NULL", cmb_SIPENDAR_TYPE_ID.SelectedItemValue) & ","

            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue), "NULL", "'" & cmb_JENIS_WATCHLIST_CODE.SelectedItemValue & "'") & ","
            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue), "NULL", "'" & cmb_TINDAK_PIDANA_CODE.SelectedItemValue & "'") & ","
            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue), "NULL", "'" & cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue & "'") & ","

            '                strQuery2 += IIf(objGlobalParameter Is Nothing, "NULL", "'" & objGlobalParameter.ParameterValue & "'") & ","
            '                strQuery2 += IIf(String.IsNullOrEmpty(txt_Keterangan.Value), "NULL", "'" & txt_Keterangan.Value & "'") & ","

            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue), "NULL", "'" & cmb_SUMBER_TYPE_CODE.SelectedItemValue & "'") & ","
            '                'strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A", "NULL", CLng(txt_SUMBER_ID.Value).ToString) & "," '' Edited by Felix 21-Sep-2021, kita isi ID_PPATK_Watchlistnya
            '                'strQuery2 += objScreeningResult.FK_WATCHLIST_ID_PPATK & ","
            '                If objScreeningResult.FK_WATCHLIST_ID_PPATK IsNot Nothing Then
            '                    strQuery2 += "'" & objScreeningResult.FK_WATCHLIST_ID_PPATK & "',"
            '                Else
            '                    strQuery2 += "NULL,"
            '                End If

            '                strQuery2 += IIf(String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue), "NULL", "'" & cmb_SUBMISSION_TYPE_CODE.SelectedItemValue & "'") & ","
            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "0", "1") & ","
            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "NULL", "'" & CDate(txt_Transaction_DateFrom.Value).ToString("yyyy-MM-dd HH:mm:ss") & "'") & ","
            '                strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "NULL", "'" & CDate(txt_Transaction_DateTo.Value).ToString("yyyy-MM-dd HH:mm:ss") & "'") & ","

            '                strQuery2 += "'" & objScreeningResult.GCN & "',"

            '                ''Added by Felix 21 Jul 2021, tambah Status
            '                Dim objProfileModule As NawaDAL.Module = Nothing
            '                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
            '                    objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PROAKTIF")
            '                Else
            '                    objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PENGAYAAN")
            '                End If
            '                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
            '                    strQuery2 += "'Accept',"
            '                Else
            '                    strQuery2 += "'Waiting For Approval'," ''Added by Felix 21 Jul 2021, tambah Status
            '                End If
            '                ''Added by Felix 21 Jul 2021, tambah Status

            '                strQuery2 += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
            '                strQuery2 += "'" & CDate(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss") & "'"
            '                strQuery2 += ")"

            '                strQuery = strQuery1 & strQuery2
            '                'Throw New Exception(strQuery)

            '                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            '                'Set previousGCN
            '                previousGCN = objScreeningResult.GCN
            '            End If

            '        Next
            '    End Using
            'End If
            '' End of 16-Sep-2021
            Using objDbSipendar As New SiPendarDAL.SiPendarEntities
                'Dim recordID = intID

                'Ambil data2 dari Sipendar Screening Result
                Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT = Nothing
                Dim objGlobalParameter As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
                objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = intID).FirstOrDefault
                objGlobalParameter = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault

                If objScreeningResult IsNot Nothing Then
                    'Skip jika 2 GCN yang sama dipilih
                    'If objScreeningResult.GCN = previousGCN Then
                    '    Continue For
                    'End If

                    '' Added by Felix 21-Sep-2021
                    '' Delete dlu Other_Info nya sebelum Insert
                    Dim strQueryDelete = "Delete SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO WHERE FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = '" & intID & "'"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryDelete, Nothing)
                    '' End of 21-Sep-2021

                    strQuery2 = "VALUES ("
                    strQuery2 += intID & ","
                    strQuery2 += IIf(String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue), "NULL", cmb_SIPENDAR_TYPE_ID.SelectedItemValue) & ","

                    strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue), "NULL", "'" & cmb_JENIS_WATCHLIST_CODE.SelectedItemValue & "'") & ","
                    strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue), "NULL", "'" & cmb_TINDAK_PIDANA_CODE.SelectedItemValue & "'") & ","
                    strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue), "NULL", "'" & cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue & "'") & ","

                    strQuery2 += IIf(objGlobalParameter Is Nothing, "NULL", "'" & objGlobalParameter.ParameterValue & "'") & ","
                    strQuery2 += IIf(String.IsNullOrEmpty(txt_Keterangan.Value), "NULL", "'" & txt_Keterangan.Value & "'") & ","

                    strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue), "NULL", "'" & cmb_SUMBER_TYPE_CODE.SelectedItemValue & "'") & ","
                    'strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A", "NULL", CLng(txt_SUMBER_ID.Value).ToString) & ","
                    'strQuery2 += objScreeningResult.FK_WATCHLIST_ID_PPATK.ToString() & "," '' Edited by Felix 21-Sep-2021

                    If objScreeningResult.FK_WATCHLIST_ID_PPATK IsNot Nothing Then
                        strQuery2 += "'" & objScreeningResult.FK_WATCHLIST_ID_PPATK.ToString() & "',"
                    Else
                        strQuery2 += "NULL,"
                    End If

                    Dim FK_SIPENDAR_SUBMISSION_TYPE_CODE As String = ""
                    ' 22 Dec 2021 Ari menggambil data submission type di grid result
                    Dim query2 As String = "select b.FK_SIPENDAR_SUBMISSION_TYPE_CODE FROM SIPENDAR_SCREENING_REQUEST_RESULT a inner join SIPENDAR_SCREENING_REQUEST_DETAIL b on a.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = b.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID where a.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = " & objScreeningResult.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
                    Dim data3 As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query2)
                    For Each row3 As DataRow In data3.Rows
                        FK_SIPENDAR_SUBMISSION_TYPE_CODE = row3("FK_SIPENDAR_SUBMISSION_TYPE_CODE")
                    Next

                    ' 22 Dec 2021 Ari ganti cmb_submmision_type jadi variebel dari submission type di atas
                    strQuery2 += IIf(String.IsNullOrEmpty(FK_SIPENDAR_SUBMISSION_TYPE_CODE), "NULL", "'" & FK_SIPENDAR_SUBMISSION_TYPE_CODE & "'") & ","
                    strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "0", "1") & ","
                    strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "NULL", "'" & CDate(txt_Transaction_DateFrom.Value).ToString("yyyy-MM-dd HH:mm:ss") & "'") & ","
                    strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "NULL", "'" & CDate(txt_Transaction_DateTo.Value).ToString("yyyy-MM-dd HH:mm:ss") & "'") & ","

                    strQuery2 += "'" & objScreeningResult.GCN & "',"

                    ''Added by Felix 21 Jul 2021, tambah Status
                    Dim objProfileModule As NawaDAL.Module = Nothing
                    If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                        objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PROAKTIF")
                    Else
                        objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PENGAYAAN")
                    End If
                    If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
                        strQuery2 += "'Accept',"
                    Else
                        strQuery2 += "'Waiting For Approval'," ''Added by Felix 21 Jul 2021, tambah Status
                    End If
                    ''Added by Felix 21 Jul 2021, tambah Status

                    strQuery2 += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
                    strQuery2 += "'" & CDate(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss") & "',"
                    strQuery2 += "'" & CDate(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    strQuery2 += ")"

                    strQuery = strQuery1 & strQuery2
                    'Throw New Exception(strQuery)

                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                End If
            End Using


            '' Added by Felix 20-Sep-2021, Update status juga untuk Screening Result yang sama
            'If cb_IsSelectAllResult.Checked Then
            '    'Save to Database
            '    Using objDbSipendar As New SiPendarDAL.SiPendarEntities
            '        Dim previousGCN As String = ""
            '        For Each row As DataRow In dt_ScreeningResult.Rows
            '            Dim recordID = row.Item("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID").ToString()

            '            Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT = Nothing
            '            objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID).FirstOrDefault

            '            If objScreeningResult IsNot Nothing Then
            '                'Skip jika 2 GCN yang sama dipilih
            '                If objScreeningResult.GCN = previousGCN Then
            '                    Continue For
            '                End If

            '                Dim objProfileModule As NawaDAL.Module = Nothing
            '                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
            '                    objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PROAKTIF")
            '                Else
            '                    objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PENGAYAAN")
            '                End If

            '                Dim paramChangeStatus(3) As SqlParameter

            '                paramChangeStatus(0) = New SqlParameter
            '                paramChangeStatus(0).ParameterName = "@PK_Result_ID"
            '                paramChangeStatus(0).Value = recordID
            '                paramChangeStatus(0).DbType = SqlDbType.VarChar

            '                paramChangeStatus(1) = New SqlParameter
            '                paramChangeStatus(1).ParameterName = "@Sumber_ID"
            '                'paramChangeStatus(1).Value = row.Item("FK_WATCHLIST_ID_PPATK").ToString()
            '                If objScreeningResult.FK_WATCHLIST_ID_PPATK IsNot Nothing Then
            '                    paramChangeStatus(1).Value = row.Item("FK_WATCHLIST_ID_PPATK").ToString()
            '                Else
            '                    paramChangeStatus(1).Value = Nothing
            '                End If
            '                paramChangeStatus(1).DbType = SqlDbType.VarChar

            '                paramChangeStatus(2) = New SqlParameter
            '                paramChangeStatus(2).ParameterName = "@GCN"
            '                paramChangeStatus(2).Value = objScreeningResult.GCN
            '                paramChangeStatus(2).DbType = SqlDbType.VarChar

            '                paramChangeStatus(3) = New SqlParameter
            '                paramChangeStatus(3).ParameterName = "@Status"
            '                paramChangeStatus(3).DbType = SqlDbType.VarChar
            '                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
            '                    paramChangeStatus(3).Value = "Group Accept"
            '                Else
            '                    paramChangeStatus(3).Value = "Group Waiting For Approval"
            '                End If

            '                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_UPDATE_STATUS_OTHER_INFO_ForSameResult", paramChangeStatus)

            '                'Set previousGCN
            '                previousGCN = objScreeningResult.GCN
            '            End If
            '        Next
            '    End Using
            'Else

            '    'Save by looping selected item
            '    Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)

            '    'Save to Database
            '    Using objDbSipendar As New SiPendarDAL.SiPendarEntities
            '        Dim previousGCN As String = ""
            '        For Each item As SelectedRow In smScreeningResult.SelectedRows
            '            Dim recordID = item.RecordID.ToString()

            '            Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT = Nothing
            '            objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID).FirstOrDefault

            '            If objScreeningResult IsNot Nothing Then
            '                'Skip jika 2 GCN yang sama dipilih
            '                If objScreeningResult.GCN = previousGCN Then
            '                    Continue For
            '                End If

            '                Dim objProfileModule As NawaDAL.Module = Nothing
            '                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
            '                    objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PROAKTIF")
            '                Else
            '                    objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PENGAYAAN")
            '                End If

            '                Dim paramChangeStatus(3) As SqlParameter

            '                paramChangeStatus(0) = New SqlParameter
            '                paramChangeStatus(0).ParameterName = "@PK_Result_ID"
            '                paramChangeStatus(0).Value = recordID
            '                paramChangeStatus(0).DbType = SqlDbType.VarChar

            '                paramChangeStatus(1) = New SqlParameter
            '                paramChangeStatus(1).ParameterName = "@Sumber_ID"
            '                'paramChangeStatus(1).Value = IIf(String.IsNullOrEmpty(objScreeningResult.FK_WATCHLIST_ID_PPATK), "NULL", objScreeningResult.FK_WATCHLIST_ID_PPATK.ToString())
            '                If objScreeningResult.FK_WATCHLIST_ID_PPATK IsNot Nothing Then
            '                    paramChangeStatus(1).Value = objScreeningResult.FK_WATCHLIST_ID_PPATK
            '                Else
            '                    paramChangeStatus(1).Value = Nothing
            '                End If
            '                paramChangeStatus(1).DbType = SqlDbType.VarChar

            '                paramChangeStatus(2) = New SqlParameter
            '                paramChangeStatus(2).ParameterName = "@GCN"
            '                paramChangeStatus(2).Value = objScreeningResult.GCN
            '                paramChangeStatus(2).DbType = SqlDbType.VarChar

            '                paramChangeStatus(3) = New SqlParameter
            '                paramChangeStatus(3).ParameterName = "@Status"
            '                paramChangeStatus(3).DbType = SqlDbType.VarChar
            '                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
            '                    paramChangeStatus(3).Value = "Group Accept"
            '                Else
            '                    paramChangeStatus(3).Value = "Group Waiting For Approval"
            '                End If

            '                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_UPDATE_STATUS_OTHER_INFO_ForSameResult", paramChangeStatus)

            '                'Set previousGCN
            '                previousGCN = objScreeningResult.GCN
            '            End If
            '        Next
            '    End Using
            'End If

            '' End of 28-Sep-2021

            Using objDbSipendar As New SiPendarDAL.SiPendarEntities

                Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT = Nothing
                objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = intID).FirstOrDefault

                If objScreeningResult IsNot Nothing Then

                    Dim objProfileModule As NawaDAL.Module = Nothing
                    If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
                        objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PROAKTIF")
                    Else
                        objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PENGAYAAN")
                    End If

                    Dim paramChangeStatus(3) As SqlParameter

                    paramChangeStatus(0) = New SqlParameter
                    paramChangeStatus(0).ParameterName = "@PK_Result_ID"
                    paramChangeStatus(0).Value = intID
                    paramChangeStatus(0).DbType = SqlDbType.VarChar

                    paramChangeStatus(1) = New SqlParameter
                    paramChangeStatus(1).ParameterName = "@Sumber_ID"
                    'paramChangeStatus(1).Value = row.Item("FK_WATCHLIST_ID_PPATK").ToString()
                    If objScreeningResult.FK_WATCHLIST_ID_PPATK IsNot Nothing Then
                        paramChangeStatus(1).Value = objScreeningResult.FK_WATCHLIST_ID_PPATK.ToString()
                    Else
                        paramChangeStatus(1).Value = Nothing
                    End If
                    paramChangeStatus(1).DbType = SqlDbType.VarChar

                    paramChangeStatus(2) = New SqlParameter
                    paramChangeStatus(2).ParameterName = "@GCN"
                    paramChangeStatus(2).Value = objScreeningResult.GCN
                    paramChangeStatus(2).DbType = SqlDbType.VarChar

                    paramChangeStatus(3) = New SqlParameter
                    paramChangeStatus(3).ParameterName = "@Status"
                    paramChangeStatus(3).DbType = SqlDbType.VarChar
                    If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
                        paramChangeStatus(3).Value = "Group Accept"
                    Else
                        paramChangeStatus(3).Value = "Group Waiting For Approval"
                    End If

                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_UPDATE_STATUS_OTHER_INFO_ForSameResult", paramChangeStatus)

                    'Set previousGCN
                End If

            End Using

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    '' Added on 23 Aug 2021 by Felix
    Protected Sub SaveExcludeScreeningResultOtherInfo()
        Try
            Dim strQuery As String = ""
            Dim strQuery1 As String = "INSERT INTO SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO ("
            strQuery1 += "FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID, "
            'strQuery1 += "FK_SIPENDAR_TYPE_ID, "
            'strQuery1 += "FK_SIPENDAR_JENIS_WATCHLIST_CODE, "
            'strQuery1 += "FK_SIPENDAR_TINDAK_PIDANA_CODE, "
            'strQuery1 += "FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE, "
            'strQuery1 += "FK_SIPENDAR_ORGANISASI_CODE, "
            'strQuery1 += "Keterangan, "
            'strQuery1 += "FK_SIPENDAR_SUMBER_TYPE_CODE, "
            strQuery1 += "SUMBER_ID, "
            'strQuery1 += "FK_SIPENDAR_SUBMISSION_TYPE_CODE, "
            strQuery1 += "IsGenerateTransaction, "
            'strQuery1 += "Transaction_DateFrom, "
            'strQuery1 += "Transaction_DateTo, "
            strQuery1 += "GCN, "
            strQuery1 += "Status, " ''Added by Felix 21 Jul 2021, tambah Status
            strQuery1 += "CreatedBy, "
            strQuery1 += "CreatedDate,"
            strQuery1 += "Lastupdatedate)" '' Add Felix 14-Oct-2021, tambah LastUpdateDate

            Dim strQuery2 As String = ""

            'Save by looping selected item
            Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)

            Dim PK_Request_ID As String = Request.Params("ID")
            Dim intID = NawaBLL.Common.DecryptQueryString(PK_Request_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim dtResult As DataTable = New DataTable
            'dtResult = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT WHERE PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID in (select distinct PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID from SIPENDAR_SCREENING_REQUEST_RESULT where PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID =" & intID & " ) and GCN is not null ORDER BY TotalSimilarityPct Desc, GCN, Nama")
            dtResult = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(intID) '' Edit 01-Oct-2021 Felix

            'Save to Database
            Using objDbSipendar As New SiPendarDAL.SiPendarEntities
                Dim previousGCN As String = ""
                For Each item As DataRow In dtResult.Rows
                    'Dim recordID = item("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID").ToString
                    Dim recordID = intID '' Edit  04-Oct-2021 Felix. Biar statusnya berubah yg di PK itu.

                    'Ambil data2 dari Sipendar Screening Result
                    Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT = Nothing
                    Dim objGlobalParameter As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
                    objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID).FirstOrDefault
                    objGlobalParameter = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault

                    '' Add by Felix 13-Oct-2021, kalau value = 1 , ga perlu approval
                    Dim objGlobalParameterPK10 As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
                    objGlobalParameterPK10 = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 10).FirstOrDefault

                    If objScreeningResult IsNot Nothing Then
                        'Skip jika 2 GCN yang sama dipilih
                        If objScreeningResult.GCN = previousGCN Then
                            Continue For
                        End If

                        '' Added by Felix 21-Sep-2021
                        '' Delete dlu Other_Info nya sebelum Insert
                        Dim strQueryDelete = "Delete SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO WHERE FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = '" & recordID & "'"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryDelete, Nothing)
                        '' End of 21-Sep-2021

                        strQuery2 = "VALUES ("
                        strQuery2 += recordID & ","
                        'strQuery2 += IIf(String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue), "NULL", cmb_SIPENDAR_TYPE_ID.SelectedItemValue) & ","

                        'strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue), "NULL", "'" & cmb_JENIS_WATCHLIST_CODE.SelectedItemValue & "'") & ","
                        'strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue), "NULL", "'" & cmb_TINDAK_PIDANA_CODE.SelectedItemValue & "'") & ","
                        'strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue), "NULL", "'" & cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue & "'") & ","

                        'strQuery2 += IIf(objGlobalParameter Is Nothing, "NULL", "'" & objGlobalParameter.ParameterValue & "'") & ","
                        'strQuery2 += IIf(String.IsNullOrEmpty(txt_Keterangan.Value), "NULL", "'" & txt_Keterangan.Value & "'") & ","

                        'strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue), "NULL", "'" & cmb_SUMBER_TYPE_CODE.SelectedItemValue & "'") & ","
                        'strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A", "NULL", CLng(txt_SUMBER_ID.Value).ToString) & "," '' Edited by Felix 21-Sep-2021, kita isi ID_PPATK_Watchlistnya
                        'strQuery2 += objScreeningResult.FK_WATCHLIST_ID_PPATK & ","
                        'strQuery2 += IIf(String.IsNullOrEmpty(objScreeningResult.FK_WATCHLIST_ID_PPATK), "NULL", objScreeningResult.FK_WATCHLIST_ID_PPATK.ToString()) & "," '' Edited by Felix 21-Sep-2021, kita isi ID_PPATK_Watchlistnya
                        If objScreeningResult.FK_WATCHLIST_ID_PPATK IsNot Nothing Then
                            strQuery2 += "'" & objScreeningResult.FK_WATCHLIST_ID_PPATK & "',"
                        Else
                            strQuery2 += "NULL," '' Edited by Felix 21-Sep-2021, kita isi ID_PPATK_Watchlistnya
                        End If

                        'strQuery2 += IIf(String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue), "NULL", "'" & cmb_SUBMISSION_TYPE_CODE.SelectedItemValue & "'") & ","
                        strQuery2 += "0,"
                        'strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "NULL", "'" & CDate(txt_Transaction_DateFrom.Value).ToString("yyyy-MM-dd") & "'") & ","
                        'strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "NULL", "'" & CDate(txt_Transaction_DateTo.Value).ToString("yyyy-MM-dd") & "'") & ","

                        strQuery2 += "'" & item("GCN") & "',"

                        ''Added by Felix 21 Jul 2021, tambah Status
                        Dim objProfileModule As NawaDAL.Module = Nothing
                        objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_SCREENING_JUDGEMENT_Approval_Exclusion")

                        If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsSupportView = False Or objGlobalParameterPK10.ParameterValue = 1 Then
                            'strQuery2 += "'Excluded',"
                            strQuery2 += "'Not Match'," ''Edit 01-Oct-2021 Felix, ganti jd Not Match
                        Else
                            'strQuery2 += "'Waiting For Approval Exclusion'," ''Added by Felix 21 Jul 2021, tambah Status
                            strQuery2 += "'Waiting For Approval Not Match'," ''Edit 01-Oct-2021 Felix, ganti jd Not Match
                        End If
                        ''Added by Felix 21 Jul 2021, tambah Status

                        strQuery2 += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
                        strQuery2 += "'" & CDate(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss") & "',"
                        strQuery2 += "'" & CDate(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        strQuery2 += ")"

                        strQuery = strQuery1 & strQuery2
                        'Throw New Exception(strQuery)

                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        'Set previousGCN
                        previousGCN = objScreeningResult.GCN
                    End If

                    '' Added by Felix 20-Sep-2021
                    Dim objProfileModule2 As NawaDAL.Module = Nothing
                    objProfileModule2 = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_SCREENING_JUDGEMENT_Approval_Exclusion")

                    Dim paramChangeStatus(3) As SqlParameter

                    paramChangeStatus(0) = New SqlParameter
                    paramChangeStatus(0).ParameterName = "@PK_Result_ID"
                    paramChangeStatus(0).Value = recordID
                    paramChangeStatus(0).DbType = SqlDbType.VarChar

                    paramChangeStatus(1) = New SqlParameter
                    paramChangeStatus(1).ParameterName = "@Sumber_ID"
                    'paramChangeStatus(1).Value = IIf(String.IsNullOrEmpty(objScreeningResult.FK_WATCHLIST_ID_PPATK), "NULL", objScreeningResult.FK_WATCHLIST_ID_PPATK.ToString()) '' Edited by Felix 21-Sep-2021, kita isi ID_PPATK_Watchlistnya
                    If objScreeningResult.FK_WATCHLIST_ID_PPATK IsNot Nothing Then
                        paramChangeStatus(1).Value = objScreeningResult.FK_WATCHLIST_ID_PPATK '' Edited by Felix 21-Sep-2021, kita isi ID_PPATK_Watchlistnya
                    Else
                        paramChangeStatus(1).Value = Nothing
                    End If
                    paramChangeStatus(1).DbType = SqlDbType.VarChar

                    paramChangeStatus(2) = New SqlParameter
                    paramChangeStatus(2).ParameterName = "@GCN"
                    paramChangeStatus(2).Value = objScreeningResult.GCN
                    paramChangeStatus(2).DbType = SqlDbType.VarChar

                    paramChangeStatus(3) = New SqlParameter
                    paramChangeStatus(3).ParameterName = "@Status"
                    paramChangeStatus(3).DbType = SqlDbType.VarChar
                    If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule2.IsSupportView = False Or objGlobalParameterPK10.ParameterValue = 1 Then
                        'paramChangeStatus(3).Value = "Group Excluded"
                        paramChangeStatus(3).Value = "Group Not Match" '' Edit 01-Oct-2021 Felix
                    Else
                        'paramChangeStatus(3).Value = "Group Waiting For Approval Exclusion"
                        paramChangeStatus(3).Value = "Group Waiting For Approval Not Match" '' Edit 01-Oct-2021 Felix
                    End If

                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_UPDATE_STATUS_OTHER_INFO_ForSameResult", paramChangeStatus)
                    '' End of 20-Sep-2021

                Next
            End Using

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    '' End of 23 Aug 2021
#End Region

#Region "14-Sep-2021 Ariswara"

    Public Property ListIndicator As List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
        Get
            Return Session("SiPendarScreeningJudgement.ListIndicator")
        End Get
        Set(value As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
            Session("SiPendarScreeningJudgement.ListIndicator") = value
        End Set
    End Property

    Public Property IDIndicator As String
        Get
            Return Session("SiPendarScreeningJudgement.IDIndicator")
        End Get
        Set(value As String)
            Session("SiPendarScreeningJudgement.IDIndicator") = value
        End Set
    End Property

    Protected Sub ReportIndicator_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            'objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goaml_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Sub bindReportIndicator(store As Ext.Net.Store, listIndicator As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listIndicator)
        '  objtable.Columns.Add(New DataColumn("Kode_Indicator", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                '     item("Kode_Indicator") = item("FK_Indicator")
                item("Keterangan") = getReportIndicatorByKode(item("FK_Indicator").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Shared Function getReportIndicatorByKode(Kode As String) As String
        Dim strKategori As String = ""
        Using objdb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Dim kategori As SiPendarDAL.goAML_Ref_Indikator_Laporan = objdb.goAML_Ref_Indikator_Laporan.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If kategori IsNot Nothing Then
                strKategori = kategori.Keterangan
            End If
        End Using
        Return strKategori
    End Function


    Sub editIndicator(id As String, command As String)
        Try
            'sar_reportIndicator.Clear()
            cmbW_ReportIndicator.SetTextValue("")

            WindowReportIndicator.Hidden = False
            'Dim reportIndicator As SiPendarDAL.SIPENDAR_Report_Indicator
            'reportIndicator = ListIndicator.Where(Function(x) x.PK_Report_Indicator = id).FirstOrDefault


            'sar_reportIndicator.SetValue(reportIndicator.FK_Indicator)
            If command = "Detail" Then
                'sar_reportIndicator.Selectable = False
                cmbW_ReportIndicator.IsReadOnly = True
                BtnsaveReportIndicator.Hidden = True
            Else
                'sar_reportIndicator.Selectable = True
                cmbW_ReportIndicator.IsReadOnly = False
                BtnsaveReportIndicator.Hidden = False
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandReportIndicator(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = ID).FirstOrDefault)
                bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                editIndicator(ID, "Edit")
                IDIndicator = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editIndicator(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveReportIndicator_Click(sender As Object, e As DirectEventArgs)
        Try
            ''Edit 30-Sep-2021 Felix , Ganti NDSDropdown
            'If String.IsNullOrEmpty(sar_reportIndicator.SelectedItem.Value) Then
            '    Throw New ApplicationException(sar_reportIndicator.FieldLabel + " Tidak boleh kosong")
            'End If
            If cmbW_ReportIndicator.SelectedItemValue = "" Or cmbW_ReportIndicator.SelectedItemValue = Nothing Then
                Throw New ApplicationException(cmbW_ReportIndicator.Label + " Tidak boleh kosong")
            End If

            'For Each item In ListIndicator
            '    If item.FK_Indicator = sar_reportIndicator.SelectedItem.Value Then
            '        Throw New ApplicationException(sar_reportIndicator.SelectedItem.Value + " Sudah Ada")
            '    End If
            'Next

            For Each item In ListIndicator
                If item.FK_Indicator = cmbW_ReportIndicator.SelectedItemValue Then
                    Throw New ApplicationException(cmbW_ReportIndicator.SelectedItemValue + " Sudah Ada")
                End If
            Next
            '' End 30-Sep-2021

            Dim reportIndicator As New SiPendarDAL.SIPENDAR_Report_Indicator
            If IDIndicator IsNot Nothing Or IDIndicator <> "" Then
                ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = IDIndicator).FirstOrDefault)
            End If
            If ListIndicator.Count = 0 Then
                reportIndicator.PK_Report_Indicator = -1
            ElseIf ListIndicator.Count >= 1 Then
                reportIndicator.PK_Report_Indicator = ListIndicator.Min(Function(x) x.PK_Report_Indicator) - 1
            End If

            'reportIndicator.FK_Indicator = sar_reportIndicator.SelectedItem.Value
            reportIndicator.FK_Indicator = cmbW_ReportIndicator.SelectedItemValue ''Edit 30-Sep-2021 Felix , Ganti NDSDropdown
            ListIndicator.Add(reportIndicator)
            bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            WindowReportIndicator.Hidden = True
            IDIndicator = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelreportIndicator_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowReportIndicator.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_addIndicator_click(sender As Object, e As DirectEventArgs)
        Try
            'sar_reportIndicator.Clear() ''Edit 30-Sep-2021 Felix , Ganti NDSDropdown
            cmbW_ReportIndicator.SetTextValue("")
            WindowReportIndicator.Hidden = False
            'sar_reportIndicator.Selectable = True ''Edit 30-Sep-2021 Felix , Ganti NDSDropdown
            cmbW_ReportIndicator.IsReadOnly = False
            BtnsaveReportIndicator.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


#End Region

#Region "16-Sep-2021 Felix"
    Protected Sub sm_ScreeningResult_change(sender As Object, e As EventArgs)
        Try
            Dim smWatchlist As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)
            LabelScreeningResultCount.Text = "Selected : " & smWatchlist.SelectedRows.Count & " of " & TotalResult
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub cb_IsSelectAllResult_Changed(sender As Object, e As EventArgs) Handles cb_IsSelectAllResult.DirectCheck
    '    If cb_IsSelectAllResult.Value Then
    '        gp_ScreeningResult.Hide()
    '        gp_ScreeningResultAll.Show()
    '    Else
    '        gp_ScreeningResult.Show()
    '        gp_ScreeningResultAll.Hide()
    '    End If
    'End Sub
#End Region

#Region "20-Sep-2021 Daniel"
    Protected Sub WindowScreeningDetail_Close_Click()
        WindowScreeningDetail.Hidden = True
    End Sub

    Protected Sub gp_ScreeningResult_Gridcommand(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                Clear_WindowScreeningDetail()
                WindowScreeningDetail.Hidden = False
                LoadData_WindowScreeningDetail(id)
            Else

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadData_WindowScreeningDetail(id As String)
        Try
            Dim dtResult As DataTable = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(id)
            If dtResult IsNot Nothing Then
                If dtResult.Rows.Count > 0 Then
                    Dim itemrows As Data.DataRow = dtResult.Rows(0)
                    'Data Search
                    If Not IsDBNull(itemrows("FK_WATCHLIST_ID_PPATK")) Then
                        WindowScreeningDetail_IDWatchlistPPATK.Text = Convert.ToString(itemrows("FK_WATCHLIST_ID_PPATK"))
                    End If
                    If Not IsDBNull(itemrows("Nama")) Then
                        WindowScreeningDetail_NameSearch.Text = Convert.ToString(itemrows("Nama"))
                    End If
                    If Not IsDBNull(itemrows("DOB")) Then
                        WindowScreeningDetail_DOBSearch.Text = Convert.ToDateTime(itemrows("DOB")).ToString("dd-MM-yyyy")
                    End If
                    If Not IsDBNull(itemrows("Birth_Place")) Then
                        WindowScreeningDetail_POBSearch.Text = Convert.ToString(itemrows("Birth_Place"))
                    End If
                    If Not IsDBNull(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE")) Then
                        WindowScreeningDetail_IdentityTypeSearch.Text = Convert.ToString(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE"))
                    End If
                    If Not IsDBNull(itemrows("Identity_Number")) Then
                        WindowScreeningDetail_IdentityNumberSearch.Text = Convert.ToString(itemrows("Identity_Number"))
                    End If
                    If Not IsDBNull(itemrows("TotalSimilarityPct")) Then
                        WindowScreeningDetail_Similarity.Text = Convert.ToString(itemrows("TotalSimilarityPct")) & "%"
                    End If
                    '20-Dec-2021 Felix : Tambah info Submission_Type
                    If Not IsDBNull(itemrows("FK_SIPENDAR_SUBMISSION_TYPE_CODE")) Then
                        WindowScreeningDetail_SubmissionType.Text = Convert.ToString(itemrows("FK_SIPENDAR_SUBMISSION_TYPE_CODE"))
                    End If

                    '29-Sep-2021 Adi : Penambahan list of Identity Watchlist
                    If Not IsDBNull(itemrows("FK_WATCHLIST_ID_PPATK")) Then
                        Dim strSQL As String = "SELECT Identity_Type, Identity_Number"
                        strSQL += " FROM SIPENDAR_WATCHLIST sw"
                        strSQL += " UNPIVOT (Identity_Number FOR Identity_Type IN (NPWP,KTP,PAS,KITAS,SUKET,SIM,KITAP, KIMS)) unpvt"
                        strSQL += " WHERE ISNULL(Identity_Number,'')<>'' AND ID_PPATK = '" & itemrows("FK_WATCHLIST_ID_PPATK") & "'"
                        Dim dtIdentityWatchlist As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

                        gp_IdentityWatchlist.GetStore.DataSource = dtIdentityWatchlist
                        gp_IdentityWatchlist.GetStore.DataBind()
                        gp_IdentityWatchlist.Hidden = False

                        'Hide Identity Type Search dan Identity NUmber Search
                        WindowScreeningDetail_IdentityTypeSearch.Hidden = True
                        WindowScreeningDetail_IdentityNumberSearch.Hidden = True
                    Else
                        gp_IdentityWatchlist.Hidden = True
                        'Hide Identity Type Search dan Identity NUmber Search
                        WindowScreeningDetail_IdentityTypeSearch.Hidden = False
                        WindowScreeningDetail_IdentityNumberSearch.Hidden = False
                    End If

                    'Data Found
                    If Not IsDBNull(itemrows("customerinformation")) Then
                        Dim customerinformation As String = Convert.ToString(itemrows("customerinformation"))
                        If customerinformation IsNot Nothing Then
                            '29-Sep-2021 Adi : Penambahan list of Identity Customer
                            'Hide Identity Type Search dan Identity NUmber Search
                            WindowScreeningDetail_CustomerIdentityType.Hidden = True
                            WindowScreeningDetail_CustomerIdentityNumber.Hidden = True
                            ''Customer' else 'Non Customer'
                            If customerinformation = "Customer" Then
                                WindowScreeningRoles.Value = "Customer"
                                FieldSet2.Title = "Customer Data"
                                WindowScreeningDetail_IsHaveActiveAccount.Hidden = False
                                WindowScreeningDetail_GCN.FieldLabel = "GCN"
                                WindowScreeningDetail_CIF.FieldLabel = "CIF"
                                '30-Sep-2021 ganti cara dengan panggil SP
                                'If Not IsDBNull(itemrows("IsHaveActiveAccount")) Then
                                '    WindowScreeningDetail_IsHaveActiveAccount.Text = Convert.ToString(itemrows("IsHaveActiveAccount"))
                                'End If
                                If Not IsDBNull(itemrows("GCN")) Then
                                    WindowScreeningDetail_GCN.Text = Convert.ToString(itemrows("GCN"))
                                    Dim drCustomerClosed As DataRow = GetCustomerClosedStatus(itemrows("GCN"))
                                    If drCustomerClosed IsNot Nothing Then
                                        If Not IsDBNull(drCustomerClosed("IsClosed")) Then
                                            If drCustomerClosed("IsClosed") = 0 Then
                                                WindowScreeningDetail_IsHaveActiveAccount.Value = "Active"
                                            Else
                                                If Not IsDBNull(drCustomerClosed("ClosedDate")) Then
                                                    WindowScreeningDetail_IsHaveActiveAccount.Value = "Closed ( " & CDate(drCustomerClosed("ClosedDate")).ToString("dd-MMM-yyyy") & " )"
                                                Else
                                                    WindowScreeningDetail_IsHaveActiveAccount.Value = "Closed"
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                                'End of 30-Sep-2021 ganti cara dengan panggil SP

                                If Not IsDBNull(itemrows("CIF")) Then
                                    WindowScreeningDetail_CIF.Text = Convert.ToString(itemrows("CIF"))
                                End If
                                If Not IsDBNull(itemrows("NAMACUSTOMER")) Then
                                    WindowScreeningDetail_CustomerName.Text = Convert.ToString(itemrows("NAMACUSTOMER"))
                                End If
                                If Not IsDBNull(itemrows("DOBCustomer")) Then
                                    WindowScreeningDetail_CustomerDOB.Text = Convert.ToDateTime(itemrows("DOBCustomer")).ToString("dd-MM-yyyy")
                                End If
                                If Not IsDBNull(itemrows("Birth_PlaceCustomer")) Then
                                    WindowScreeningDetail_CustomerPOB.Text = Convert.ToString(itemrows("Birth_PlaceCustomer"))
                                End If
                                If Not IsDBNull(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer")) Then
                                    WindowScreeningDetail_CustomerIdentityType.Text = Convert.ToString(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer"))
                                End If
                                If Not IsDBNull(itemrows("Identity_NumberCustomer")) Then
                                    WindowScreeningDetail_CustomerIdentityNumber.Text = Convert.ToString(itemrows("Identity_NumberCustomer"))
                                End If

                                If Not IsDBNull(itemrows("GCN")) Then
                                    Dim strSQL As String = "SELECT FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer AS Identity_Type, Identity_NumberCustomer AS Identity_Number"
                                    strSQL += " FROM vw_SIPENDAR_CUSTOMER_IDENTITY sci"
                                    strSQL += " WHERE ISNULL(GCN,'')='" & itemrows("GCN") & "'"
                                    Dim dtIdentityCustomer As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

                                    gp_IdentityCustomer.GetStore.DataSource = dtIdentityCustomer
                                    gp_IdentityCustomer.GetStore.DataBind()
                                    gp_IdentityCustomer.Hidden = False
                                Else
                                    gp_IdentityCustomer.Hidden = True
                                End If

                            ElseIf customerinformation = "Non Customer" Then
                                WindowScreeningRoles.Value = "Non Customer"
                                FieldSet2.Title = "Non Customer Data"
                                WindowScreeningDetail_IsHaveActiveAccount.Hidden = True
                                WindowScreeningDetail_GCN.FieldLabel = "Unique Key"
                                WindowScreeningDetail_CIF.FieldLabel = "Role"
                                If Not IsDBNull(itemrows("GCN")) Then
                                    Dim stringgcn As String = Convert.ToString(itemrows("GCN"))
                                    WindowScreeningDetail_GCN.Text = stringgcn
                                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                                    If drResult IsNot Nothing Then
                                        If Not IsDBNull(drResult("Role")) Then
                                            WindowScreeningDetail_CIF.Text = Convert.ToString(drResult("Role"))
                                        End If
                                        If Not IsDBNull(drResult("Name")) Then
                                            WindowScreeningDetail_CustomerName.Text = Convert.ToString(drResult("Name"))
                                        End If
                                        If Not IsDBNull(drResult("Birth_Date")) Then
                                            WindowScreeningDetail_CustomerDOB.Text = Convert.ToDateTime(drResult("Birth_Date")).ToString("dd-MM-yyyy")
                                        End If
                                        If Not IsDBNull(drResult("Birth_Place")) Then
                                            WindowScreeningDetail_CustomerPOB.Text = Convert.ToString(drResult("Birth_Place"))
                                        End If
                                        'If Not IsDBNull(drResult("Country_Code")) Then
                                        '    WindowScreeningDetail_CustomerIdentityType.FieldLabel = "Negara"
                                        '    WindowScreeningDetail_CustomerIdentityType.Hidden = False
                                        '    WindowScreeningDetail_CustomerIdentityType.Text = GetCountryName(Convert.ToString(drResult("Country_Code")))
                                        'End If
                                        If Not IsDBNull(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")) Then
                                            Dim dtIdentityCustomer As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Identifications(Convert.ToString(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
                                            gp_IdentityCustomer.GetStore.DataSource = dtIdentityCustomer
                                            gp_IdentityCustomer.GetStore.DataBind()
                                            gp_IdentityCustomer.Hidden = False
                                        Else
                                            gp_IdentityCustomer.Hidden = True
                                        End If
                                        ' 18 Nov 2021 Ari pengambahan grid link CIF
                                        GridPanelLinkCIF.Hidden = False
                                        If Not IsDBNull(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")) Then
                                            Dim dtLinkCIF As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Account(Convert.ToString(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
                                            GridPanelLinkCIF.GetStore.DataSource = dtLinkCIF
                                            GridPanelLinkCIF.GetStore.DataBind()
                                            GridPanelLinkCIF.Hidden = False
                                        Else
                                            GridPanelLinkCIF.Hidden = True
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If

            'Using objdb As New SiPendarEntities
            '    Dim objResult As SIPENDAR_SCREENING_REQUEST_RESULT = objdb.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = id).FirstOrDefault
            '    If objResult IsNot Nothing Then
            '        If objResult.TotalSimilarity IsNot Nothing Then
            '            WindowScreeningDetail_Similarity.Text = (objResult.TotalSimilarity * 100).ToString() & "%"
            '        End If
            '        If objResult.FK_WATCHLIST_ID_PPATK IsNot Nothing Then
            '            WindowScreeningDetail_IDWatchlistPPATK.Text = objResult.FK_WATCHLIST_ID_PPATK.ToString()
            '        End If
            '        If objResult.Nama IsNot Nothing Then
            '            WindowScreeningDetail_NameSearch.Text = objResult.Nama.ToString()
            '        End If
            '        If objResult.DOB IsNot Nothing Then
            '            WindowScreeningDetail_DOBSearch.Text = objResult.DOB.Value.ToString("dd-MM-yyyy")
            '        End If
            '        If objResult.Birth_Place IsNot Nothing Then
            '            WindowScreeningDetail_POBSearch.Text = objResult.Birth_Place.ToString()
            '        End If
            '        If objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE IsNot Nothing Then
            '            WindowScreeningDetail_IdentityTypeSearch.Text = objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE.ToString()
            '        End If
            '        If objResult.Identity_Number IsNot Nothing Then
            '            WindowScreeningDetail_IdentityNumberSearch.Text = objResult.Identity_Number.ToString()
            '        End If
            '        If objResult.GCN IsNot Nothing Then
            '            WindowScreeningDetail_GCN.Text = objResult.GCN.ToString()
            '        End If
            '        If objResult.NAMACUSTOMER IsNot Nothing Then
            '            WindowScreeningDetail_CustomerName.Text = objResult.NAMACUSTOMER.ToString()
            '        End If
            '        If objResult.DOBCustomer IsNot Nothing Then
            '            WindowScreeningDetail_CustomerDOB.Text = objResult.DOBCustomer.Value.ToString("dd-MM-yyyy")
            '        End If
            '        If objResult.Birth_PlaceCustomer IsNot Nothing Then
            '            WindowScreeningDetail_CustomerPOB.Text = objResult.Birth_PlaceCustomer.ToString()
            '        End If
            '        If objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer IsNot Nothing Then
            '            WindowScreeningDetail_CustomerIdentityType.Text = objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer.ToString()
            '        End If
            '        If objResult.Identity_NumberCustomer IsNot Nothing Then
            '            WindowScreeningDetail_CustomerIdentityNumber.Text = objResult.Identity_NumberCustomer.ToString()
            '        End If
            '    End If
            'End Using

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub Clear_WindowScreeningDetail()
        Try
            WindowScreeningDetail_Similarity.Clear()
            WindowScreeningDetail_IDWatchlistPPATK.Clear()
            WindowScreeningDetail_NameSearch.Clear()
            WindowScreeningDetail_DOBSearch.Clear()
            WindowScreeningDetail_POBSearch.Clear()
            WindowScreeningDetail_IdentityTypeSearch.Clear()
            WindowScreeningDetail_IdentityNumberSearch.Clear()
            WindowScreeningDetail_GCN.Clear()
            WindowScreeningDetail_CustomerName.Clear()
            WindowScreeningDetail_CustomerDOB.Clear()
            WindowScreeningDetail_CustomerPOB.Clear()
            WindowScreeningDetail_CustomerIdentityType.Clear()
            WindowScreeningDetail_CustomerIdentityNumber.Clear()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadColumnCommandScreeningResult()
        Try
            ColumnActionLocation(gp_ScreeningResult, gp_ScreeningResult_CommandColumn)
            ColumnActionLocation(gp_ScreeningResultAll, gp_ScreeningResult_All_CommandColumn)
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

#End Region
#Region "24-Sep-2021 Daniel"
    ''' <summary>
    ''' Function For Get SIPENDAR_SCREENING_REQUEST_RESULT as DataTabel
    ''' </summary>
    ''' <param name="PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID">PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID Value (Primary Key Of SIPENDAR_SCREENING_REQUEST_RESULT)</param>
    ''' <returns></returns>
    Protected Function GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID As String) As DataTable
        Try
            Dim data As New DataTable
            'Dim stringquery As String = " SELECT result.*, (result.TotalSimilarity*100) AS TotalSimilarityPct, custaccount.[CountAccount], case when custaccount.CountAccount > 0 " &
            '" then 'Yes' else 'No' end as IsHaveActiveAccount FROM SIPENDAR_SCREENING_REQUEST_RESULT result left join ( select distinct cust.GCN, count(cust.GCN) as [CountAccount] " &
            '" from goAML_Ref_Customer cust join goAML_Ref_Account acc on acc.client_number =  cust.CIF join SIPENDAR_SCREENING_REQUEST_RESULT resultget on resultget.GCN = cust.GCN where " &
            '" resultget.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID=" & PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID & " and acc.status_code <> 'TTP' and resultget.GCN is not null group by cust.GCN ) " &
            '" custaccount on result.GCN = custaccount.GCN WHERE result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID=" & PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID & " ORDER BY TotalSimilarityPct Desc, " &
            '" result.GCN, result.Nama"

            'Dim stringquery As String = ""
            'stringquery = "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct, "
            'stringquery += "custaccount.[CountAccount], "
            'stringquery += "case when custaccount.CountAccount > 0 "
            'stringquery += "then 'Yes' else 'No' end as IsHaveActiveAccount "
            'stringquery += "FROM SIPENDAR_SCREENING_REQUEST_RESULT result "
            'stringquery += "Left Join( "
            'stringquery += " Select distinct cust.GCN, count(cust.GCN) As [CountAccount] "
            'stringquery += " From goAML_Ref_Customer cust "
            'stringquery += " Join goAML_Ref_Account acc "
            'stringquery += " On acc.client_number =  cust.CIF "
            'stringquery += " Join SIPENDAR_SCREENING_REQUEST_RESULT resultget "
            'stringquery += " On resultget.GCN = cust.GCN "
            'stringquery += " where "
            'stringquery += "resultget.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = " & PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID & ""
            'stringquery += " And acc.status_code <> 'TTP'  "
            'stringquery += " And resultget.GCN Is Not null group by cust.GCN ) custaccount "
            'stringquery += " On result.GCN = custaccount.GCN "
            'stringquery += "left join SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO b "
            'stringquery += "On result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = b.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID "
            'stringquery += "WHERE result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID In "
            'stringquery += "(Select distinct PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID "
            'stringquery += "from SIPENDAR_SCREENING_REQUEST_RESULT "
            'stringquery += "where PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID =" & intID & " ) "
            'stringquery += "And result.GCN Is Not null  "
            'stringquery += "And isnull(b.Status,'New') in ('New','Reject','Reject Exclusion') "
            'stringquery += "ORDER BY TotalSimilarityPct Desc, result.GCN, Nama "

            'data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_GROUP_RESULT_BY_PK", paramLoadResultGroup)

            Dim paramLoadResultGroup(0) As SqlParameter

            paramLoadResultGroup(0) = New SqlParameter
            paramLoadResultGroup(0).ParameterName = "@PK"
            paramLoadResultGroup(0).Value = PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
            paramLoadResultGroup(0).DbType = SqlDbType.VarChar

            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_GROUP_RESULT_BY_PK", paramLoadResultGroup)
            ' 19 Nov Ari : Penambahan column untuk link cif
            data.Columns.Add(New Data.DataColumn("Account_Name", GetType(String)))
            data.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            data.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))
            ' 22 Dec Ari : Penambahan column submission type
            data.Columns.Add(New Data.DataColumn("Submission_Type", GetType(String)))
            Dim data2 As New DataTable
            Dim data3 As New DataTable
            For Each row As Data.DataRow In data.Rows
                If row.Item("Stakeholder_Role") IsNot Nothing And Not IsDBNull(row.Item("Stakeholder_Role")) Then
                    row.Item("Status") = "Non Customer"
                    ' 22 Dec 2021 Ari set sipendar type jika non customer dan munculkan note untuk non customer
                    nontesnoncustomer.Hidden = False
                    cmb_SIPENDAR_TYPE_ID.SetTextWithTextValue(1, "Sipendar Proaktif")
                    cmb_SIPENDAR_TYPE_ID.IsReadOnly = True
                    ' 19 Nov 2021 Penmbahan untuk link CIF
                    Dim stringgcn As String = Convert.ToString(row("GCN"))
                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                    If drResult IsNot Nothing Then
                        Dim query As String = "DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) " &
                                              "INSERT INTO @tblaccount ( Groupaccount ) " &
                                              "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID") &
                                              " Select STUFF((SELECT ','  + cast(Groupaccount as nvarchar) from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_No, " &
                                              " STUFF((SELECT  ','  +  CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),'') WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),'') " &
                                              " Else '' end from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif " &
                                              " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_Name, STUFF((SELECT ','  + isnull(cast(rc.cif as nvarchar),'') from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as CIF_No "
                        data2 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)
                        For Each row2 As DataRow In data2.Rows
                            row("Account_Name") = row2("Account_Name")
                            row("Account_No") = row2("Account_No")
                            row("CIF_No") = row2("CIF_No")
                        Next
                    Else
                        row("Account_Name") = Nothing
                        row("Account_No") = Nothing
                        row("CIF_No") = Nothing
                    End If

                Else
                    row.Item("Status") = "Customer"
                    row("Account_Name") = Nothing
                    row("Account_No") = Nothing
                    row("CIF_No") = Nothing
                End If
                ' 22 Dec Ari : Panggil query untuk dapatkan submission type
                Dim query2 As String = "select b.FK_SIPENDAR_SUBMISSION_TYPE_CODE FROM SIPENDAR_SCREENING_REQUEST_RESULT a inner join SIPENDAR_SCREENING_REQUEST_DETAIL b on a.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = b.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID where a.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = " & row.Item("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")
                data3 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query2)
                For Each row3 As DataRow In data3.Rows
                    row("Submission_Type") = row3("FK_SIPENDAR_SUBMISSION_TYPE_CODE")
                Next
            Next
            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function
#End Region
#Region "2021-Sep-23 Felix"
    Private Sub LoadApprovalHistoryJudgement()
        Try
            Dim EncryptedReportID As String = Request.Params("ID")
            Dim IDResult = NawaBLL.Common.DecryptQueryString(EncryptedReportID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            Dim dtResult As DataTable = New DataTable
            Dim paramLoadApprovalHistory(1) As SqlParameter

            paramLoadApprovalHistory(0) = New SqlParameter
            paramLoadApprovalHistory(0).ParameterName = "@Module_ID"
            paramLoadApprovalHistory(0).Value = ObjModule.PK_Module_ID
            paramLoadApprovalHistory(0).DbType = SqlDbType.VarChar

            paramLoadApprovalHistory(1) = New SqlParameter
            paramLoadApprovalHistory(1).ParameterName = "@Result_ID"
            paramLoadApprovalHistory(1).Value = IDResult
            paramLoadApprovalHistory(1).DbType = SqlDbType.VarChar

            dtResult = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_JUDGEMENT_APPROVAL_HISTORY", paramLoadApprovalHistory)
            'Dim objApprovalHistory = SiPendarBLL.goAML_NEW_ReportBLL.get_ListNotesHistory(ObjModule.PK_Module_ID, IDReport)
            If dtResult.Rows.Count <> 0 Then
                gpStore_history.DataSource = dtResult
                gpStore_history.DataBind()
            End If


            panelApprovalHistory.Hidden = False

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
#Region "30-Sep-2021 Adi : Get Customer Closed and Closing Date"
    Protected Function GetCustomerClosedStatus(strGCN As String) As DataRow
        Try

            Dim param(0) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@GCN"
            param(0).Value = strGCN
            param(0).DbType = SqlDbType.VarChar

            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_GetCustomerClosedStatus_ByGCN", param)

            Return drResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function
#End Region
#Region "28 Oct 2021 Ari : Get Non Customer Data"
    Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer(Unique_Key As String) As DataRow
        Try
            Unique_Key = Replace(Unique_Key, "'", "''")
            Dim stringquery As String = " select top 1 * from SIPENDAR_StakeHolder_NonCustomer where Unique_Key = '" & Unique_Key & "' "
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery, Nothing)
            Return drResult
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function
    Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Identifications(FK_SIPENDAR_StakeHolder_NonCustomer_ID As String) As DataTable
        Try
            Dim data As New DataTable
            Dim stringquery As String = " select Type as Identity_Type, Number as Identity_Number, Issue_Date, Expiry_Date, Issued_By, Issued_Country, Comments " &
                " from SIPENDAR_StakeHolder_NonCustomer_Identifications where FK_SIPENDAR_StakeHolder_NonCustomer_ID = " & FK_SIPENDAR_StakeHolder_NonCustomer_ID
            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function
#End Region

#Region "19 Nov Ari tambah Fungsi untuk get Link CIF"
    Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Account(FK_SIPENDAR_StakeHolder_NonCustomer_ID As String) As DataTable
        Try
            Dim data As New DataTable
            Dim stringquery As String = " DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) INSERT INTO @tblaccount (Groupaccount) " &
                "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & FK_SIPENDAR_StakeHolder_NonCustomer_ID &
                " select Groupaccount as Account_No, rc.cif as CIF_No , CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),null)   WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),null) else null end as Account_Name from @tblaccount sn" &
                " left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif"
            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function
#End Region


#Region "20-Jul-2022 Penambhan Export Data Screening atas Request ANZ"
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            Dim PK_Request_ID As String = Request.Params("ID")
            intID = NawaBLL.Common.DecryptQueryString(PK_Request_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            'Dim DataScreeningAll As DataTable = New DataTable
            'DataScreeningAll = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(intID)

            Dim strQuery As String = ""
            'mengikuti sp usp_GET_GROUP_RESULT_BY_PK
            strQuery = " declare @PK as bigint = " & intID &
             " ;With ResultGroup as ( " &
             " select PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID,PK_SIPENDAR_SCREENING_REQUEST_ID,GCN,result.FK_WATCHLIST_ID_PPATK " &
             " ,reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
             " inner join SIPENDAR_SCREENING_REQUEST_DETAIL reqDet " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = reqdet.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
             " where PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = @PK ) " &
             " SELECT " &
             " result.Nama, " &
             " result.DOB, " &
             " result.Birth_Place, " &
             " result.Identity_Number, " &
             " result.FK_WATCHLIST_ID_PPATK, " &
             " result.GCN, " &
             " (SELECT STRING_AGG (CIF, ',') AS CIF FROM goAML_Ref_Customer where GCN = result.GCN ) as CIF, " &
             " result.NAMACUSTOMER, " &
             " result.DOBCustomer, " &
             " result.Birth_PlaceCustomer, " &
             " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer, " &
             " result.Identity_NumberCustomer, " &
             " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE, " &
             " result.CreatedDate, " &
             " result.Stakeholder_Role, " &
             " (result.TotalSimilarity*100) AS TotalSimilarityPct, " &
             " case when result.Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation " &
             " ,reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
             " inner join ResultGroup RG " &
             " on result.GCN = RG.GCN " &
             " and isnull(result.FK_WATCHLIST_ID_PPATK,result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID) " &
             " = isnull(RG.FK_WATCHLIST_ID_PPATK,RG.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID) " &
             " left join SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO OI " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = OI.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID " &
             " and isnull(OI.Status,'') in ('Accept','Not Match') " &
             " Inner join SIPENDAR_SCREENING_REQUEST_DETAIL reqDet " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = reqdet.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
             " and reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE = RG.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " where OI.PK_SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO_ID is null " &
             " ORDER BY TotalSimilarityPct Desc, result.GCN, Nama "

            Dim DataScreeningAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery,)

            If DataScreeningAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            End If

            '23 Nov 2021 Ari : Penambahan Link To CIF/Account/Account Name
            DataScreeningAll.Columns.Add(New Data.DataColumn("Account_Name", GetType(String)))
            DataScreeningAll.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            DataScreeningAll.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))

            Dim data2 As New DataTable
            For Each row As DataRow In DataScreeningAll.Rows
                If row.Item("customerinformation") = "Non Customer" Then
                    Dim stringgcn As String = Convert.ToString(row("GCN"))
                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                    If drResult IsNot Nothing Then
                        Dim query As String = "DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) " &
                                              "INSERT INTO @tblaccount ( Groupaccount ) " &
                                              "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID") &
                                              " Select STUFF((SELECT ','  + cast(Groupaccount as nvarchar) from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_No, " &
                                              " STUFF((SELECT  ','  +  CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),'') WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),'') " &
                                              " Else '' end from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif " &
                                              " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_Name, STUFF((SELECT ','  + isnull(cast(rc.cif as nvarchar),'') from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as CIF_No "
                        data2 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)
                        For Each row2 As DataRow In data2.Rows
                            row("Account_Name") = row2("Account_Name")
                            row("Account_No") = row2("Account_No")
                            row("CIF_No") = row2("CIF_No")
                        Next
                    Else
                        row("Account_Name") = Nothing
                        row("Account_No") = Nothing
                        row("CIF_No") = Nothing
                    End If
                Else
                    row("Account_Name") = Nothing
                    row("Account_No") = Nothing
                    row("CIF_No") = Nothing
                End If

            Next

            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))



                    Using objtbl As Data.DataTable = DataScreeningAll

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("SipendarScreeningResult")
                            'objtbl.Columns.Remove("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")
                            'objtbl.Columns.Remove("PK_SIPENDAR_SCREENING_REQUEST_ID")
                            'objtbl.Columns.Remove("UserIDRequest")
                            'objtbl.Columns.Remove("CreatedDate")
                            'objtbl.Columns.Remove("FK_SIPENDAR_SCREENING_REQUEST_TYPE_ID")
                            'objtbl.Columns.Remove("_Similarity")
                            'objtbl.Columns.Remove("_Confidence")
                            'objtbl.Columns.Remove("_Similarity_Nama")
                            'objtbl.Columns.Remove("_Similarity_DOB")
                            'objtbl.Columns.Remove("IsNeedToReportSIPENDAR")
                            'objtbl.Columns.Remove("Approved")
                            'objtbl.Columns.Remove("ApprovedBy")
                            'objtbl.Columns.Remove("Nationality")
                            'objtbl.Columns.Remove("NationalityCustomer")
                            'objtbl.Columns.Remove("_Similarity_Birth_Place")
                            'objtbl.Columns.Remove("_Similarity_Identity_Number")
                            'objtbl.Columns.Remove("_Similarity_Nationality")
                            'objtbl.Columns.Remove("PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID")
                            objtbl.Columns("Nama").SetOrdinal(0)
                            objtbl.Columns("DOB").SetOrdinal(1)
                            objtbl.Columns("Birth_Place").SetOrdinal(2)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                            objtbl.Columns("Identity_Number").SetOrdinal(4)
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                            objtbl.Columns("customerinformation").SetOrdinal(7)
                            objtbl.Columns("CIF").SetOrdinal(8)
                            objtbl.Columns("CIF_No").SetOrdinal(9)
                            objtbl.Columns("Account_No").SetOrdinal(10)
                            objtbl.Columns("Account_Name").SetOrdinal(11)
                            objtbl.Columns("GCN").SetOrdinal(12)
                            objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                            objtbl.Columns("DOBCustomer").SetOrdinal(14)
                            objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                            objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                            objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                            objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)

                            objtbl.Columns("Nama").ColumnName = "Name Search Data"
                            objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                            objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                            objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                            objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                            objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                            objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                            objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                            objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                            objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                            objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                            objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                            objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                            objtbl.Columns("Account_No").ColumnName = "Link To Account"
                            objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = DataScreeningAll                    'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        objtbl.Columns("Nama").SetOrdinal(0)
                        objtbl.Columns("DOB").SetOrdinal(1)
                        objtbl.Columns("Birth_Place").SetOrdinal(2)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                        objtbl.Columns("Identity_Number").SetOrdinal(4)
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                        objtbl.Columns("customerinformation").SetOrdinal(7)
                        objtbl.Columns("CIF").SetOrdinal(8)
                        objtbl.Columns("CIF_No").SetOrdinal(9)
                        objtbl.Columns("Account_No").SetOrdinal(10)
                        objtbl.Columns("Account_Name").SetOrdinal(11)
                        objtbl.Columns("GCN").SetOrdinal(12)
                        objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                        objtbl.Columns("DOBCustomer").SetOrdinal(14)
                        objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                        objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                        objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                        objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)

                        objtbl.Columns("Nama").ColumnName = "Name Search Data"
                        objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                        objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                        objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                        objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                        objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                        objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                        objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                        objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                        objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                        objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                        objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                        objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                        objtbl.Columns("Account_No").ColumnName = "Link To Account"
                        objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            Dim PK_Request_ID As String = Request.Params("ID")
            intID = NawaBLL.Common.DecryptQueryString(PK_Request_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim pagecurrent As Integer = e.ExtraParams("currentPage")

            'Dim DataScreeningCurrentPage As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "DECLARE @PageNumber AS INT, @RowspPage AS INT SET @PageNumber =" & pagecurrent & " SET @RowspPage = 10 SELECT Nama,DOB,Birth_Place,Stakeholder_Role,case when Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation,CONVERT(varchar(4000),STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer rf WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'')) as CIF,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE,Identity_Number,FK_WATCHLIST_ID_PPATK,GCN,NAMACUSTOMER,DOBCustomer,Birth_PlaceCustomer,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer,Identity_NumberCustomer, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT result WHERE PK_SIPENDAR_SCREENING_REQUEST_ID=" & PK_Request_ID & " ORDER BY TotalSimilarityPct Desc, GCN, Nama OFFSET ((@PageNumber - 1) * @RowspPage) ROWS FETCH NEXT @RowspPage ROWS ONLY",)

            '' Edit 15-Dec-2021 Felix
            Dim strQuery As String = ""
            'mengikuti sp usp_GET_GROUP_RESULT_BY_PK
            strQuery = "DECLARE @PageNumber AS INT, " &
             " @RowspPage AS INT SET @PageNumber =" & pagecurrent &
             " SET @RowspPage = 10 " &
             " declare @PK as bigint = " & intID &
             " ;With ResultGroup as ( " &
             " select PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID,PK_SIPENDAR_SCREENING_REQUEST_ID,GCN,result.FK_WATCHLIST_ID_PPATK " &
             " ,reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
             " inner join SIPENDAR_SCREENING_REQUEST_DETAIL reqDet " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = reqdet.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
             " where PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = @PK ) " &
             " SELECT " &
             " result.Nama, " &
             " result.DOB, " &
             " result.Birth_Place, " &
             " result.Identity_Number, " &
             " result.FK_WATCHLIST_ID_PPATK, " &
             " result.GCN, " &
             " (SELECT STRING_AGG (CIF, ',') AS CIF FROM goAML_Ref_Customer where GCN = result.GCN ) as CIF, " &
             " result.NAMACUSTOMER, " &
             " result.DOBCustomer, " &
             " result.Birth_PlaceCustomer, " &
             " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer, " &
             " result.Identity_NumberCustomer, " &
             " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE, " &
             " result.CreatedDate, " &
             " result.Stakeholder_Role, " &
             " (result.TotalSimilarity*100) AS TotalSimilarityPct, " &
             " case when result.Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation " &
             " ,reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
             " inner join ResultGroup RG " &
             " on result.GCN = RG.GCN " &
             " and isnull(result.FK_WATCHLIST_ID_PPATK,result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID) " &
             " = isnull(RG.FK_WATCHLIST_ID_PPATK,RG.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID) " &
             " left join SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO OI " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = OI.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID " &
             " and isnull(OI.Status,'') in ('Accept','Not Match') " &
             " Inner join SIPENDAR_SCREENING_REQUEST_DETAIL reqDet " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = reqdet.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
             " and reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE = RG.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " where OI.PK_SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO_ID is null " &
             " ORDER BY TotalSimilarityPct Desc, result.GCN, Nama " &
             " OFFSET ((@PageNumber - 1) * @RowspPage) ROWS FETCH NEXT @RowspPage ROWS ONLY"

            'Dim DataScreeningCurrentPage As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "DECLARE @PageNumber AS INT, @RowspPage AS INT SET @PageNumber =" & pagecurrent & " SET @RowspPage = 10 SELECT Nama,DOB,Birth_Place,Stakeholder_Role,case when Stakeholder_Role Is null then 'Customer' else 'Non Customer' end as customerinformation,CONVERT(varchar(4000),STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer rf WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'')) as CIF,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE,Identity_Number,FK_WATCHLIST_ID_PPATK,GCN,NAMACUSTOMER,DOBCustomer,Birth_PlaceCustomer,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer,Identity_NumberCustomer, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT result WHERE PK_SIPENDAR_SCREENING_REQUEST_ID=" & PK_Request_ID & " ORDER BY TotalSimilarityPct Desc, GCN, Nama OFFSET ((@PageNumber - 1) * @RowspPage) ROWS FETCH NEXT @RowspPage ROWS ONLY",)
            Dim DataScreeningCurrentPage As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery,)
            '' 15-Dec-2021
            If DataScreeningCurrentPage.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            End If

            '23 Nov 2021 Ari : Penambahan Link To CIF/Account/Account Name
            DataScreeningCurrentPage.Columns.Add(New Data.DataColumn("Account_Name", GetType(String)))
            DataScreeningCurrentPage.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            DataScreeningCurrentPage.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))

            Dim data2 As New DataTable
            For Each row As DataRow In DataScreeningCurrentPage.Rows
                If row.Item("customerinformation") = "Non Customer" Then
                    Dim stringgcn As String = Convert.ToString(row("GCN"))
                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                    If drResult IsNot Nothing Then
                        Dim query As String = "DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) " &
                                              "INSERT INTO @tblaccount ( Groupaccount ) " &
                                              "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID") &
                                              " Select STUFF((SELECT ','  + cast(Groupaccount as nvarchar) from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_No, " &
                                              " STUFF((SELECT  ','  +  CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),'') WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),'') " &
                                              " Else '' end from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif " &
                                              " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_Name, STUFF((SELECT ','  + isnull(cast(rc.cif as nvarchar),'') from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as CIF_No "
                        data2 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)
                        For Each row2 As DataRow In data2.Rows
                            row("Account_Name") = row2("Account_Name")
                            row("Account_No") = row2("Account_No")
                            row("CIF_No") = row2("CIF_No")
                        Next
                    Else
                        row("Account_Name") = Nothing
                        row("Account_No") = Nothing
                        row("CIF_No") = Nothing
                    End If
                Else
                    row("Account_Name") = Nothing
                    row("Account_No") = Nothing
                    row("CIF_No") = Nothing
                End If

            Next


            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))



                    Using objtbl As Data.DataTable = DataScreeningCurrentPage

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("Sipendar_Screening_Request")
                            objtbl.Columns("Nama").SetOrdinal(0)
                            objtbl.Columns("DOB").SetOrdinal(1)
                            objtbl.Columns("Birth_Place").SetOrdinal(2)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                            objtbl.Columns("Identity_Number").SetOrdinal(4)
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                            objtbl.Columns("customerinformation").SetOrdinal(7)
                            objtbl.Columns("CIF").SetOrdinal(8)
                            objtbl.Columns("CIF_No").SetOrdinal(9)
                            objtbl.Columns("Account_No").SetOrdinal(10)
                            objtbl.Columns("Account_Name").SetOrdinal(11)
                            objtbl.Columns("GCN").SetOrdinal(12)
                            objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                            objtbl.Columns("DOBCustomer").SetOrdinal(14)
                            objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                            objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                            objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                            objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)

                            objtbl.Columns("Nama").ColumnName = "Name Search Data"
                            objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                            objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                            objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                            objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                            objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                            objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                            objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                            objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                            objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                            objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                            objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                            objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                            objtbl.Columns("Account_No").ColumnName = "Link To Account"
                            objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)



                    Using objtbl As Data.DataTable = DataScreeningCurrentPage

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        objtbl.Columns("Nama").SetOrdinal(0)
                        objtbl.Columns("DOB").SetOrdinal(1)
                        objtbl.Columns("Birth_Place").SetOrdinal(2)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                        objtbl.Columns("Identity_Number").SetOrdinal(4)
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                        objtbl.Columns("customerinformation").SetOrdinal(7)
                        objtbl.Columns("CIF").SetOrdinal(8)
                        objtbl.Columns("CIF_No").SetOrdinal(9)
                        objtbl.Columns("Account_No").SetOrdinal(10)
                        objtbl.Columns("Account_Name").SetOrdinal(11)
                        objtbl.Columns("GCN").SetOrdinal(12)
                        objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                        objtbl.Columns("DOBCustomer").SetOrdinal(14)
                        objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                        objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                        objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                        objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)

                        objtbl.Columns("Nama").ColumnName = "Name Search Data"
                        objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                        objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                        objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                        objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                        objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                        objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                        objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                        objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                        objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                        objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                        objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                        objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                        objtbl.Columns("Account_No").ColumnName = "Link To Account"
                        objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    Protected Sub ExportSelectedExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)
            Dim selected As RowSelectionModel = gp_ScreeningResult.SelectionModel.Primary
            If selected.SelectedRows.Count = 0 Then

                Throw New Exception("Minimal 1 data harus dipilih!")
            End If
            Dim listScreeninnigResultPK As New List(Of SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT)
            For Each item As SelectedRow In smScreeningResult.SelectedRows
                Dim recordID = item.RecordID.ToString
                Dim objNew = New SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT
                With objNew
                    .PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID
                End With

                listScreeninnigResultPK.Add(objNew)
            Next
            Dim pkscreeningresultselected As String = ""
            Dim listScreeninnigResult As New List(Of SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT)
            For Each item In listScreeninnigResultPK
                Dim cariscreeningrequest As New SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT
                Using objdb As New SiPendarDAL.SiPendarEntities
                    cariscreeningrequest = objdb.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = item.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID).FirstOrDefault()
                End Using
                If cariscreeningrequest IsNot Nothing Then
                    listScreeninnigResult.Add(New SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT() With {
                  .PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = cariscreeningrequest.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID,
                  .Nama = cariscreeningrequest.Nama,
                  .DOBCustomer = cariscreeningrequest.DOBCustomer,
                  .Birth_PlaceCustomer = cariscreeningrequest.Birth_PlaceCustomer,
                  .Identity_Number = cariscreeningrequest.Identity_Number,
                  .FK_WATCHLIST_ID_PPATK = cariscreeningrequest.FK_WATCHLIST_ID_PPATK,
                  .GCN = cariscreeningrequest.GCN,
                  .NAMACUSTOMER = cariscreeningrequest.NAMACUSTOMER,
                  .DOB = cariscreeningrequest.DOB,
                  .Birth_Place = cariscreeningrequest.Birth_Place,
                  .Identity_NumberCustomer = cariscreeningrequest.Identity_Number,
                  .TotalSimilarity = cariscreeningrequest.TotalSimilarity * 100,
                  .FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer = cariscreeningrequest.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer
                  })
                End If

                pkscreeningresultselected = pkscreeningresultselected & "," & item.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
            Next
            pkscreeningresultselected = pkscreeningresultselected.Remove(0, 1)

            '' Edit 15-Dec-2021 Felix
            'Dim strquery As String = "SELECT Nama,DOB,Birth_Place,Stakeholder_Role,case when Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation,CONVERT(VARCHAR(4000),STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'')) as CIF,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE,Identity_Number,FK_WATCHLIST_ID_PPATK,GCN,NAMACUSTOMER,DOBCustomer,Birth_PlaceCustomer,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer,Identity_NumberCustomer, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT result WHERE PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID in(" & pkscreeningresultselected & ") ORDER BY TotalSimilarityPct Desc, GCN, Nama"
            Dim strquery As String = "SELECT result.Nama,result.DOB,result.Birth_Place,result.Stakeholder_Role, " &
                " case when result.Stakeholder_Role Is null then 'Customer' else 'Non Customer' end as customerinformation,CONVERT(VARCHAR(4000),STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'')) as CIF, " &
                " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE,result.Identity_Number,result.FK_WATCHLIST_ID_PPATK, " &
                " req.FK_SIPENDAR_SUBMISSION_TYPE_CODE, " &
                " result.GCN,result.NAMACUSTOMER,result.DOBCustomer,result.Birth_PlaceCustomer,result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer,result.Identity_NumberCustomer, (TotalSimilarity*100) AS TotalSimilarityPct, result.CreatedDate " &
                " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
                " inner join SIPENDAR_SCREENING_REQUEST_DETAIL req " &
                " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = req.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
                " WHERE PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID in(" & pkscreeningresultselected & ") ORDER BY TotalSimilarityPct Desc, GCN, Nama"


            '' End 15-Dec-2021
            Dim DataScreeningSelectResult As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquery,)

            '23 Nov 2021 Ari : Penambahan Link To CIF/Account/Account Name
            DataScreeningSelectResult.Columns.Add(New Data.DataColumn("Account_Name", GetType(String)))
            DataScreeningSelectResult.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            DataScreeningSelectResult.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))
            Dim data2 As New DataTable
            For Each row As DataRow In DataScreeningSelectResult.Rows
                If row.Item("customerinformation") = "Non Customer" Then
                    Dim stringgcn As String = Convert.ToString(row("GCN"))
                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                    If drResult IsNot Nothing Then
                        Dim query As String = "DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) " &
                                              "INSERT INTO @tblaccount ( Groupaccount ) " &
                                              "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID") &
                                              " Select STUFF((SELECT ','  + cast(Groupaccount as nvarchar) from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_No, " &
                                              " STUFF((SELECT  ','  +  CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),'') WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),'') " &
                                              " Else '' end from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif " &
                                              " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_Name, STUFF((SELECT ','  + isnull(cast(rc.cif as nvarchar),'') from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as CIF_No "
                        data2 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)
                        For Each row2 As DataRow In data2.Rows
                            row("Account_Name") = row2("Account_Name")
                            row("Account_No") = row2("Account_No")
                            row("CIF_No") = row2("CIF_No")
                        Next
                    Else
                        row("Account_Name") = Nothing
                        row("Account_No") = Nothing
                        row("CIF_No") = Nothing
                    End If
                Else
                    row("Account_Name") = Nothing
                    row("Account_No") = Nothing
                    row("CIF_No") = Nothing
                End If

            Next


            Dim tablescreeningresult As New DataTable
            Dim fields() As FieldInfo = GetType(SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT).GetFields(BindingFlags.Instance Or BindingFlags.Static Or BindingFlags.NonPublic Or BindingFlags.Public)
            For Each field As FieldInfo In fields

                tablescreeningresult.Columns.Add(field.Name, System.Type.GetType("System.String"))
            Next
            For Each item As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT In listScreeninnigResult
                Dim row As DataRow = tablescreeningresult.NewRow()
                For Each field As FieldInfo In fields
                    row(field.Name) = field.GetValue(item)

                Next

                tablescreeningresult.Rows.Add(row)
            Next
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    tablescreeningresult.DefaultView.Sort = "_TotalSimilarity Desc, _GCN"
                    Using objtbl As Data.DataTable = DataScreeningSelectResult

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("SipendarScreeningResult")
                            'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")
                            'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_ID")
                            'objtbl.Columns.Remove("_UserIDRequest")
                            'objtbl.Columns.Remove("_CreatedDate")
                            'objtbl.Columns.Remove("_FK_SIPENDAR_SCREENING_REQUEST_TYPE_ID")
                            'objtbl.Columns.Remove("_C_Similarity")
                            'objtbl.Columns.Remove("_C_Confidence")
                            'objtbl.Columns.Remove("_C_Similarity_Nama")
                            'objtbl.Columns.Remove("_C_Similarity_DOB")
                            'objtbl.Columns.Remove("_IsNeedToReportSIPENDAR")
                            'objtbl.Columns.Remove("_Approved")
                            'objtbl.Columns.Remove("_ApprovedBy")
                            'objtbl.Columns.Remove("_Nationality")
                            'objtbl.Columns.Remove("_NationalityCustomer")
                            'objtbl.Columns.Remove("_C_Similarity_Birth_Place")
                            'objtbl.Columns.Remove("_C_Similarity_Identity_Number")
                            'objtbl.Columns.Remove("_C_Similarity_Nationality")
                            'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID")
                            'objtbl.Columns("_Nama").ColumnName = "Name Search Data"
                            'objtbl.Columns("_DOB").ColumnName = "DOB Search Data"
                            'objtbl.Columns("_GCN").ColumnName = "GCN Search Data"
                            'objtbl.Columns("_NAMACUSTOMER").ColumnName = "Name Customer Data"
                            'objtbl.Columns("_DOBCustomer").ColumnName = "DOB Customer Data"
                            'objtbl.Columns("_TotalSimilarity").ColumnName = "Similarity"
                            'objtbl.Columns("_Identity_Number").ColumnName = "Identity Number Search Data"
                            'objtbl.Columns("_Birth_Place").ColumnName = "Birth Place Search Data"
                            'objtbl.Columns("_Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                            'objtbl.Columns("_FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Search Data"
                            'objtbl.Columns("_Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                            'objtbl.Columns("_FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            objtbl.Columns("Nama").SetOrdinal(0)
                            objtbl.Columns("DOB").SetOrdinal(1)
                            objtbl.Columns("Birth_Place").SetOrdinal(2)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                            objtbl.Columns("Identity_Number").SetOrdinal(4)
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                            objtbl.Columns("customerinformation").SetOrdinal(7)
                            objtbl.Columns("CIF").SetOrdinal(8)
                            objtbl.Columns("CIF_No").SetOrdinal(9)
                            objtbl.Columns("Account_No").SetOrdinal(10)
                            objtbl.Columns("Account_Name").SetOrdinal(11)
                            objtbl.Columns("GCN").SetOrdinal(12)
                            objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                            objtbl.Columns("DOBCustomer").SetOrdinal(14)
                            objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                            objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                            objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                            objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)
                            objtbl.Columns("CreatedDate").SetOrdinal(20)

                            objtbl.Columns("Nama").ColumnName = "Name Search Data"
                            objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                            objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                            objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                            objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                            objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                            objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                            objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                            objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                            objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                            objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                            objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                            objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                            objtbl.Columns("Account_No").ColumnName = "Link To Account"
                            objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = DataScreeningSelectResult

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")
                        'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_ID")
                        'objtbl.Columns.Remove("_UserIDRequest")
                        'objtbl.Columns.Remove("_CreatedDate")
                        'objtbl.Columns.Remove("_FK_SIPENDAR_SCREENING_REQUEST_TYPE_ID")
                        'objtbl.Columns.Remove("_C_Similarity")
                        'objtbl.Columns.Remove("_C_Confidence")
                        'objtbl.Columns.Remove("_C_Similarity_Nama")
                        'objtbl.Columns.Remove("_C_Similarity_DOB")
                        'objtbl.Columns.Remove("_IsNeedToReportSIPENDAR")
                        'objtbl.Columns.Remove("_Approved")
                        'objtbl.Columns.Remove("_ApprovedBy")
                        'objtbl.Columns.Remove("_Nationality")
                        'objtbl.Columns.Remove("_NationalityCustomer")
                        'objtbl.Columns.Remove("_C_Similarity_Birth_Place")
                        'objtbl.Columns.Remove("_C_Similarity_Identity_Number")
                        'objtbl.Columns.Remove("_C_Similarity_Nationality")
                        'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID")
                        'objtbl.Columns("_Nama").ColumnName = "Name Search Data"
                        'objtbl.Columns("_DOB").ColumnName = "DOB Search Data"
                        'objtbl.Columns("_GCN").ColumnName = "GCN Search Data"
                        'objtbl.Columns("_NAMACUSTOMER").ColumnName = "Name Customer Data"
                        'objtbl.Columns("_DOBCustomer").ColumnName = "DOB Customer Data"
                        'objtbl.Columns("_TotalSimilarity").ColumnName = "Similarity"
                        'objtbl.Columns("_Identity_Number").ColumnName = "Identity Number Search Data"
                        'objtbl.Columns("_Birth_Place").ColumnName = "Birth Place Search Data"
                        'objtbl.Columns("_Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                        'objtbl.Columns("_FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Search Data"
                        'objtbl.Columns("_Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                        'objtbl.Columns("_FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        objtbl.Columns("Nama").SetOrdinal(0)
                        objtbl.Columns("DOB").SetOrdinal(1)
                        objtbl.Columns("Birth_Place").SetOrdinal(2)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                        objtbl.Columns("Identity_Number").SetOrdinal(4)
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                        objtbl.Columns("customerinformation").SetOrdinal(7)
                        objtbl.Columns("CIF").SetOrdinal(8)
                        objtbl.Columns("CIF_No").SetOrdinal(9)
                        objtbl.Columns("Account_No").SetOrdinal(10)
                        objtbl.Columns("Account_Name").SetOrdinal(11)
                        objtbl.Columns("GCN").SetOrdinal(12)
                        objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                        objtbl.Columns("DOBCustomer").SetOrdinal(14)
                        objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                        objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                        objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                        objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)
                        objtbl.Columns("CreatedDate").SetOrdinal(20)

                        objtbl.Columns("Nama").ColumnName = "Name Search Data"
                        objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                        objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                        objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                        objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                        objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                        objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                        objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                        objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                        objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                        objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                        objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                        objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                        objtbl.Columns("Account_No").ColumnName = "Link To Account"
                        objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    Protected Sub sar_IsSelectedAll_Result_CheckedChanged(sender As Object, e As EventArgs) Handles sar_IsSelectedAll_Result.DirectCheck
        If sar_IsSelectedAll_Result.Value Then
            'Ext.Net.X.Js.Call("Hide")
            gp_ScreeningResult.Hide()
            BtnExport.Hide()
            BtnExportSelected.Hide()
            gp_ScreeningResultAll.Show()
        Else
            'Ext.Net.X.Js.Call("Show")
            gp_ScreeningResult.Show()
            BtnExport.Show()
            BtnExportSelected.Show()
            gp_ScreeningResultAll.Hide()
        End If
    End Sub
#End Region
End Class

