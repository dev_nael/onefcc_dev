﻿Imports System.Data
Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Drawing
Imports System.IO
Imports System.Reflection
Imports System.Threading
Imports Elmah
Imports OfficeOpenXml
Imports OfficeOpenXml.Style
Imports SiPendarBLL
Imports SiPendarDAL

Partial Class SiPendarExclusionScreeningJudgement_Detail
    Inherits ParentPage

    Const PATH_TEMP_DIR As String = "~\temp\ScreeningTemplate\"

    Public Property intID() As String
        Get
            Return Session("SiPendarExclusionScreeningJudgement_Detail.intID")
        End Get
        Set(ByVal value As String)
            Session("SiPendarExclusionScreeningJudgement_Detail.intID") = value
        End Set
    End Property

    Public Property IDModule() As String
        Get
            Return Session("SiPendarProfile_Add.IDModule")
        End Get
        Set(ByVal value As String)
            Session("SiPendarProfile_Add.IDModule") = value
        End Set
    End Property

    Public Property ScreeningStatus() As String
        Get
            Return Session("SiPendarExclusionScreeningJudgement_Detail.ScreeningStatus")
        End Get
        Set(ByVal value As String)
            Session("SiPendarExclusionScreeningJudgement_Detail.ScreeningStatus") = value
        End Set
    End Property

    Public Property objSIPENDAR_SCREENING_REQUEST() As SiPendarDAL.SIPENDAR_SCREENING_REQUEST
        Get
            Return Session("SiPendarExclusionScreeningJudgement_Detail.objSIPENDAR_SCREENING_REQUEST")
        End Get
        Set(ByVal value As SiPendarDAL.SIPENDAR_SCREENING_REQUEST)
            Session("SiPendarExclusionScreeningJudgement_Detail.objSIPENDAR_SCREENING_REQUEST") = value
        End Set
    End Property

    Public Property listSIPENDAR_SCREENING_REQUEST_DETAIL() As List(Of SiPendarDAL.SIPENDAR_SCREENING_REQUEST_DETAIL)
        Get
            Return Session("SiPendarExclusionScreeningJudgement_Detail.listSIPENDAR_SCREENING_REQUEST_DETAIL")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_SCREENING_REQUEST_DETAIL))
            Session("SiPendarExclusionScreeningJudgement_Detail.listSIPENDAR_SCREENING_REQUEST_DETAIL") = value
        End Set
    End Property

    Public Property objSIPENDAR_SCREENING_REQUEST_DETAIL_Edit() As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_DETAIL
        Get
            Return Session("SiPendarExclusionScreeningJudgement_Detail.objSIPENDAR_SCREENING_REQUEST_DETAIL_Edit")
        End Get
        Set(ByVal value As SiPendarDAL.SIPENDAR_SCREENING_REQUEST_DETAIL)
            Session("SiPendarExclusionScreeningJudgement_Detail.objSIPENDAR_SCREENING_REQUEST_DETAIL_Edit") = value
        End Set
    End Property

    '' 2021-Sep-24 Felix
    Public Property IDScreeningJudgement() As String
        Get
            Return Session("SiPendarExclusionScreeningJudgement_Detail.IDScreeningJudgement")
        End Get
        Set(ByVal value As String)
            Session("SiPendarExclusionScreeningJudgement_Detail.IDScreeningJudgement") = value
        End Set
    End Property
    '' End 2021-Sep-24

    '' Start 20-Jul-2022 Penambhan atas Request ANZ
    Public Property TotalScreeningResult As Integer
        Get
            Return Session("SiPendarProfile_ApprovalDetail.TotalScreeningResult")
        End Get
        Set(value As Integer)
            Session("SiPendarProfile_ApprovalDetail.TotalScreeningResult") = value
        End Set
    End Property
    '' End 20-Jul-2022


    Private Sub SiPendarScreening_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        Me.ActionType = NawaBLL.Common.ModuleActionEnum.view
    End Sub

    Private Sub SiPendarScreening_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = Me.ObjModule.ModuleLabel

                Dim ModuleID As String = Request.Params("ModuleID")
                IDModule = NawaBLL.Common.DecryptQueryString(ModuleID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                '' Added 24-Sep-2021 Felix
                Dim EncryptedReportID As String = Request.Params("ID")
                IDScreeningJudgement = NawaBLL.Common.DecryptQueryString(EncryptedReportID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                '' End 24-Sep-2021 Felix

                'Date Format sesuai System Parameter
                'txt_DOB.Format = NawaBLL.SystemParameterBLL.GetDateFormat

                'Set command column location (Left/Right)
                LoadColumnCommandScreeningResult()

                'Show Screening Result
                Show_ScreeningResult()

                'Load Approval History
                LoadApprovalHistoryJudgement()

                'Clear Session
                ClearSession()


            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ClearSession()
        Try
            objSIPENDAR_SCREENING_REQUEST = New SiPendarDAL.SIPENDAR_SCREENING_REQUEST
            listSIPENDAR_SCREENING_REQUEST_DETAIL = New List(Of SiPendarDAL.SIPENDAR_SCREENING_REQUEST_DETAIL)

            objSIPENDAR_SCREENING_REQUEST_DETAIL_Edit = Nothing

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "General"
    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If

            If bsettingRight = 1 Then
                'GridPanelReportIndicator.ColumnModel.Columns.Add(CommandColumn87)
            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Screening by Upload"
    Protected Sub btn_UploadScreeningRequest_Click()
        Try
            'Window_ScreeningUpload.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub btn_DownloadTemplate_Click()
        Try

            'Create Directory if not exists
            If Not Directory.Exists(Server.MapPath(PATH_TEMP_DIR)) Then
                Directory.CreateDirectory(Server.MapPath(PATH_TEMP_DIR))
            End If

            'Filename and location for output template
            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            Dim objfileinfo = New FileInfo(Server.MapPath(PATH_TEMP_DIR & tempfilexls))

            'Object to generate excel template
            Dim objScreeningRequest As Data.DataTable = New Data.DataTable()
            objScreeningRequest.Columns.Add(New Data.DataColumn("Name", GetType(String)))
            objScreeningRequest.Columns.Add(New Data.DataColumn("Date of Birth", GetType(Date)))

            'Generate excel template
            Using resource As New ExcelPackage(objfileinfo)
                'Membuath work sheet
                Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("ScreeningRequest")

                'Mengisi row 1 dengan header template
                ws.Cells("A1").LoadFromDataTable(objScreeningRequest, True)

                Dim rng As ExcelRange
                Dim comment As ExcelComment
                Dim strComment As String

                'Kolom B1 Name Mandatory
                rng = ws.Cells("A1")
                'rng.Value = "Everyday Be Coding"
                strComment = "NAWADATA: " & (vbCrLf) & "[MANDATORY] Diisi dengan Nama yang ingin di screening."
                comment = rng.AddComment(strComment, "NawaData")
                comment.AutoFit = True

                'Kolom C1 Datetime format dd-MMM-yyyy
                rng = ws.Cells("B1")
                strComment = "NAWADATA: " & (vbCrLf) & "[OPTIONAL] Data Type : Date." & (vbCrLf) & "Format : dd-MMM-yyyy."
                comment = rng.AddComment(strComment, "NawaData")
                comment.AutoFit = True

                'Kolom C1 set cell format to dd-MMM-yyyy
                rng = ws.Cells("B1:B" & (Int16.MaxValue) - 1)
                rng.Style.Numberformat.Format = "dd-MMM-yyyy"

                'Background and Font Color
                rng = ws.Cells("A1:B1")
                rng.Style.Font.Color.SetColor(Color.White)
                rng.Style.Font.Bold = True
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid
                rng.Style.Fill.BackgroundColor.SetColor(Color.Blue)

                ws.Cells(ws.Dimension.Address).AutoFitColumns()
                resource.Save()
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("content-disposition", "attachment;filename=" & "SiPendarScreeningRequest.xlsx")
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                Response.End()
            End Using

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_Cancel_Click()
        Try
            IDModule = NawaBLL.Common.EncryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ReadSheetScreening(ByVal sheet As ExcelWorksheet)
        Try
            Dim PK_ID As Long = 0
            If listSIPENDAR_SCREENING_REQUEST_DETAIL.Count > 0 Then
                PK_ID = listSIPENDAR_SCREENING_REQUEST_DETAIL.Min(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID) - 1
            End If
            If PK_ID >= 0 Then
                PK_ID = -1
            End If

            Dim IRow As Integer = 2

            While IRow <> 0
                Dim strName As String = ""

                'Gunakan Name Sebagai Key... 
                'Cari Name, jika kosong diasumsikan bahwa itu row terakhir
                strName = sheet.Cells(IRow, 1).Text.Trim
                If Not String.IsNullOrEmpty(strName) Then
                    Dim ObjData As New SiPendarDAL.SIPENDAR_SCREENING_REQUEST_DETAIL
                    With ObjData
                        .PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = PK_ID
                        If Not String.IsNullOrEmpty(sheet.Cells(IRow, 1).Text.Trim) Then
                            .Nama = sheet.Cells(IRow, 1).Text.Trim
                        End If
                        If Not String.IsNullOrEmpty(sheet.Cells(IRow, 2).Text.Trim) Then
                            .DOB = sheet.Cells(IRow, 2).Text.Trim
                        End If

                        PK_ID = PK_ID - 1
                    End With

                    listSIPENDAR_SCREENING_REQUEST_DETAIL.Add(ObjData)
                    IRow = IRow + 1
                Else
                    IRow = 0
                End If
            End While

            Dim str As String = ""

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Start Screening"
    Private Const Interval = 10 ' In seconds (ie: 10 seconds)
    Private mdtmTarget As Date

    Protected Sub btn_Confirmation_Click()
        Try
            ''Clean Screening Request
            'btn_Cancel_Click()

            'Hide Panel Confirmation
            Panelconfirmation.Hidden = True
            FormPanelInput.Hidden = False

            Try
                IDModule = NawaBLL.Common.EncryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                If InStr(ObjModule.UrlView, "?") > 0 Then
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
                Else
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
                End If
            Catch ex As Exception When TypeOf ex Is ApplicationException
                Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
            Catch ex As Exception
                Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
                Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
            End Try

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub Show_ScreeningResult()
        Try
            '' Start 20-Jul-2022 Penambhan atas Request ANZ
            cboExportExcel.Value = "Excel"
            '' End 20-Jul-2022
            Dim PK_Request_ID As String = Request.Params("ID")
            intID = NawaBLL.Common.DecryptQueryString(PK_Request_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim dtResult As DataTable = New DataTable
            dtResult = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(intID)
            'dtResult = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT WHERE PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID =" & intID & " and GCN is not null ORDER BY TotalSimilarityPct Desc, GCN, Nama")
            gp_ScreeningResult.GetStore.DataSource = dtResult
            gp_ScreeningResult.GetStore.DataBind()

            '' Start 20-Jul-2022 Penambhan atas Request ANZ
            gp_ScreeningResult_All.GetStore.DataSource = dtResult
            gp_ScreeningResult_All.GetStore.DataBind()
            '' End 20-Jul-2022
            'For Each item As Data.DataRow In dtResult.Rows
            '    If Not IsDBNull(item("FK_WATCHLIST_ID_PPATK")) Then
            '        txt_SUMBER_ID.Value = item("FK_WATCHLIST_ID_PPATK").ToString
            '    End If
            'Next

            'gp_ScreeningRequest.Hidden = True
            'gp_ScreeningResult.Hidden = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Report to SiPendar"
    Protected Sub BtnApproveReport_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            'Update 24-Sep-2021 Felix : 
            'Penambahan Approval History And Note
            If String.IsNullOrEmpty(txtApprovalNotes.Value) Then
                Ext.Net.X.Msg.Alert("Information", "Approval/Rejection Note is required!").Show()
                Exit Sub
            End If

            Dim objModuleScreeningJudgement As NawaDAL.Module = Nothing
            objModuleScreeningJudgement = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_SCREENING_JUDGEMENT")
            Dim notes As String = txtApprovalNotes.Value
            SiPendarBLL.goAML_NEW_ReportBLL.CreateNotes(objModuleScreeningJudgement.PK_Module_ID, objModuleScreeningJudgement.ModuleLabel, SiPendarBLL.goAML_NEW_ReportBLL.actionApproval.Accept, SiPendarBLL.goAML_NEW_ReportBLL.actionForm.Approval, IDScreeningJudgement, notes)
            '' End of 24-Sep-2021

            'Dim PK_Request_ID As String = Request.Params("ID")
            'Dim intID = NawaBLL.Common.DecryptQueryString(PK_Request_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim dtResult As DataTable = New DataTable
            dtResult = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(intID)
            'dtResult = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT WHERE PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID =" & intID & " and GCN is not null ORDER BY TotalSimilarityPct Desc, GCN, Nama")

            'Set flag IsNeetToReportSIPENDAR
            Using objDb As New SiPendarDAL.SiPendarEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDb.Database.BeginTransaction()
                    Try
                        For Each item As DataRow In dtResult.Rows
                            Dim recordID = item("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")

                            Dim paramChangeStatus(1) As SqlParameter

                            paramChangeStatus(0) = New SqlParameter
                            paramChangeStatus(0).ParameterName = "@PK"
                            paramChangeStatus(0).Value = recordID
                            paramChangeStatus(0).DbType = SqlDbType.VarChar

                            paramChangeStatus(1) = New SqlParameter
                            paramChangeStatus(1).ParameterName = "@Status"
                            'paramChangeStatus(1).Value = "Excluded"
                            paramChangeStatus(1).Value = "Not Match" '' Edit 01-Oct-2021 Felix
                            paramChangeStatus(1).DbType = SqlDbType.VarChar

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_UPDATE_STATUS_OTHER_INFO", paramChangeStatus)


                        Next

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                    End Try
                End Using
            End Using

            'LblConfirmation.Text = "Data Exclude Approved"
            LblConfirmation.Text = "Data Judge Approved" '' 01-Sep-2021 Felix, saran dari Retno

            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    '' Added on 23 Aug 2021
    Protected Sub BtnRejectReport_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            'Update 24-Sep-2021 Felix : 
            'Penambahan Approval History And Note
            If String.IsNullOrEmpty(txtApprovalNotes.Value) Then
                Ext.Net.X.Msg.Alert("Information", "Approval/Rejection Note is required!").Show()
                Exit Sub
            End If

            Dim objModuleScreeningJudgement As NawaDAL.Module = Nothing
            objModuleScreeningJudgement = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_SCREENING_JUDGEMENT")
            Dim notes As String = txtApprovalNotes.Value
            SiPendarBLL.goAML_NEW_ReportBLL.CreateNotes(objModuleScreeningJudgement.PK_Module_ID, objModuleScreeningJudgement.ModuleLabel, SiPendarBLL.goAML_NEW_ReportBLL.actionApproval.Reject, SiPendarBLL.goAML_NEW_ReportBLL.actionForm.Approval, IDScreeningJudgement, notes)
            '' End of 24-Sep-2021

            'Dim PK_Request_ID As String = Request.Params("ID")
            'Dim intID = NawaBLL.Common.DecryptQueryString(PK_Request_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim dtResult As DataTable = New DataTable
            dtResult = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(intID)
            'dtResult = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT WHERE PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID  =" & intID & " and GCN is not null ORDER BY TotalSimilarityPct Desc, GCN, Nama")

            'Set flag IsNeetToReportSIPENDAR
            Using objDb As New SiPendarDAL.SiPendarEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDb.Database.BeginTransaction()
                    Try
                        For Each item As DataRow In dtResult.Rows
                            Dim recordID = item("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")

                            Dim paramChangeStatus(1) As SqlParameter

                            paramChangeStatus(0) = New SqlParameter
                            paramChangeStatus(0).ParameterName = "@PK"
                            paramChangeStatus(0).Value = recordID
                            paramChangeStatus(0).DbType = SqlDbType.VarChar

                            paramChangeStatus(1) = New SqlParameter
                            paramChangeStatus(1).ParameterName = "@Status"
                            'paramChangeStatus(1).Value = "Reject Exclusion"
                            paramChangeStatus(1).Value = "Reject Not Match" '' Edit 01-Oct-2021 Ganti jadi Not Match
                            paramChangeStatus(1).DbType = SqlDbType.VarChar

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_UPDATE_STATUS_OTHER_INFO", paramChangeStatus)


                        Next

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                    End Try
                End Using
            End Using

            'LblConfirmation.Text = "Data Exclude Rejected" 
            LblConfirmation.Text = "Data Judge Rejected" '' 01-Sep-2021 Felix, saran dari Retno

            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '' End of 23 Aug 2021 


#End Region

#Region "20-Sep-2021 Daniel"
    Protected Sub WindowScreeningDetail_Close_Click()
        WindowScreeningDetail.Hidden = True
    End Sub

    Protected Sub gp_ScreeningResult_Gridcommand(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                Clear_WindowScreeningDetail()
                WindowScreeningDetail.Hidden = False
                LoadData_WindowScreeningDetail(id)
            Else

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadData_WindowScreeningDetail(id As String)
        Try
            Dim dtResult As DataTable = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(id)
            If dtResult IsNot Nothing Then
                If dtResult.Rows.Count > 0 Then
                    Dim itemrows As Data.DataRow = dtResult.Rows(0)
                    'Data Search
                    If Not IsDBNull(itemrows("FK_WATCHLIST_ID_PPATK")) Then
                        WindowScreeningDetail_IDWatchlistPPATK.Text = Convert.ToString(itemrows("FK_WATCHLIST_ID_PPATK"))
                    End If
                    If Not IsDBNull(itemrows("Nama")) Then
                        WindowScreeningDetail_NameSearch.Text = Convert.ToString(itemrows("Nama"))
                    End If
                    If Not IsDBNull(itemrows("DOB")) Then
                        WindowScreeningDetail_DOBSearch.Text = Convert.ToDateTime(itemrows("DOB")).ToString("dd-MM-yyyy")
                    End If
                    If Not IsDBNull(itemrows("Birth_Place")) Then
                        WindowScreeningDetail_POBSearch.Text = Convert.ToString(itemrows("Birth_Place"))
                    End If
                    If Not IsDBNull(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE")) Then
                        WindowScreeningDetail_IdentityTypeSearch.Text = Convert.ToString(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE"))
                    End If
                    If Not IsDBNull(itemrows("Identity_Number")) Then
                        WindowScreeningDetail_IdentityNumberSearch.Text = Convert.ToString(itemrows("Identity_Number"))
                    End If
                    If Not IsDBNull(itemrows("TotalSimilarityPct")) Then
                        WindowScreeningDetail_Similarity.Text = Convert.ToString(itemrows("TotalSimilarityPct")) & "%"
                    End If
                    '20-Dec-2021 Felix : Tambah info Submission_Type
                    If Not IsDBNull(itemrows("FK_SIPENDAR_SUBMISSION_TYPE_CODE")) Then
                        WindowScreeningDetail_SubmissionType.Text = Convert.ToString(itemrows("FK_SIPENDAR_SUBMISSION_TYPE_CODE"))
                    End If

                    '29-Sep-2021 Adi : Penambahan list of Identity Watchlist
                    If Not IsDBNull(itemrows("FK_WATCHLIST_ID_PPATK")) Then
                        Dim strSQL As String = "SELECT Identity_Type, Identity_Number"
                        strSQL += " FROM SIPENDAR_WATCHLIST sw"
                        strSQL += " UNPIVOT (Identity_Number FOR Identity_Type IN (NPWP,KTP,PAS,KITAS,SUKET,SIM,KITAP, KIMS)) unpvt"
                        strSQL += " WHERE ISNULL(Identity_Number,'')<>'' AND ID_PPATK = '" & itemrows("FK_WATCHLIST_ID_PPATK") & "'"
                        Dim dtIdentityWatchlist As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

                        gp_IdentityWatchlist.GetStore.DataSource = dtIdentityWatchlist
                        gp_IdentityWatchlist.GetStore.DataBind()
                        gp_IdentityWatchlist.Hidden = False

                        'Hide Identity Type Search dan Identity NUmber Search
                        WindowScreeningDetail_IdentityTypeSearch.Hidden = True
                        WindowScreeningDetail_IdentityNumberSearch.Hidden = True
                    Else
                        gp_IdentityWatchlist.Hidden = True
                        'Hide Identity Type Search dan Identity NUmber Search
                        WindowScreeningDetail_IdentityTypeSearch.Hidden = False
                        WindowScreeningDetail_IdentityNumberSearch.Hidden = False
                    End If

                    'Data Found
                    If Not IsDBNull(itemrows("customerinformation")) Then
                        Dim customerinformation As String = Convert.ToString(itemrows("customerinformation"))
                        If customerinformation IsNot Nothing Then
                            '29-Sep-2021 Adi : Penambahan list of Identity Customer
                            'Hide Identity Type Search dan Identity NUmber Search
                            WindowScreeningDetail_CustomerIdentityType.Hidden = True
                            WindowScreeningDetail_CustomerIdentityNumber.Hidden = True
                            ''Customer' else 'Non Customer'
                            If customerinformation = "Customer" Then
                                WindowScreeningRoles.Value = "Customer"
                                FieldSet2.Title = "Customer Data"
                                WindowScreeningDetail_IsHaveActiveAccount.Hidden = False
                                WindowScreeningDetail_GCN.FieldLabel = "GCN"
                                WindowScreeningDetail_CIF.FieldLabel = "CIF"
                                '30-Sep-2021 ganti cara dengan panggil SP
                                'If Not IsDBNull(itemrows("IsHaveActiveAccount")) Then
                                '    WindowScreeningDetail_IsHaveActiveAccount.Text = Convert.ToString(itemrows("IsHaveActiveAccount"))
                                'End If
                                If Not IsDBNull(itemrows("GCN")) Then
                                    WindowScreeningDetail_GCN.Text = Convert.ToString(itemrows("GCN"))
                                    Dim drCustomerClosed As DataRow = GetCustomerClosedStatus(itemrows("GCN"))
                                    If drCustomerClosed IsNot Nothing Then
                                        If Not IsDBNull(drCustomerClosed("IsClosed")) Then
                                            If drCustomerClosed("IsClosed") = 0 Then
                                                WindowScreeningDetail_IsHaveActiveAccount.Value = "Active"
                                            Else
                                                If Not IsDBNull(drCustomerClosed("ClosedDate")) Then
                                                    WindowScreeningDetail_IsHaveActiveAccount.Value = "Closed ( " & CDate(drCustomerClosed("ClosedDate")).ToString("dd-MMM-yyyy") & " )"
                                                Else
                                                    WindowScreeningDetail_IsHaveActiveAccount.Value = "Closed"
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                                'End of 30-Sep-2021 ganti cara dengan panggil SP

                                If Not IsDBNull(itemrows("CIF")) Then
                                    WindowScreeningDetail_CIF.Text = Convert.ToString(itemrows("CIF"))
                                End If
                                If Not IsDBNull(itemrows("NAMACUSTOMER")) Then
                                    WindowScreeningDetail_CustomerName.Text = Convert.ToString(itemrows("NAMACUSTOMER"))
                                End If
                                If Not IsDBNull(itemrows("DOBCustomer")) Then
                                    WindowScreeningDetail_CustomerDOB.Text = Convert.ToDateTime(itemrows("DOBCustomer")).ToString("dd-MM-yyyy")
                                End If
                                If Not IsDBNull(itemrows("Birth_PlaceCustomer")) Then
                                    WindowScreeningDetail_CustomerPOB.Text = Convert.ToString(itemrows("Birth_PlaceCustomer"))
                                End If
                                If Not IsDBNull(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer")) Then
                                    WindowScreeningDetail_CustomerIdentityType.Text = Convert.ToString(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer"))
                                End If
                                If Not IsDBNull(itemrows("Identity_NumberCustomer")) Then
                                    WindowScreeningDetail_CustomerIdentityNumber.Text = Convert.ToString(itemrows("Identity_NumberCustomer"))
                                End If

                                If Not IsDBNull(itemrows("GCN")) Then
                                    Dim strSQL As String = "SELECT FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer AS Identity_Type, Identity_NumberCustomer AS Identity_Number"
                                    strSQL += " FROM vw_SIPENDAR_CUSTOMER_IDENTITY sci"
                                    strSQL += " WHERE ISNULL(GCN,'')='" & itemrows("GCN") & "'"
                                    Dim dtIdentityCustomer As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

                                    gp_IdentityCustomer.GetStore.DataSource = dtIdentityCustomer
                                    gp_IdentityCustomer.GetStore.DataBind()
                                    gp_IdentityCustomer.Hidden = False
                                Else
                                    gp_IdentityCustomer.Hidden = True
                                End If

                            ElseIf customerinformation = "Non Customer" Then
                                WindowScreeningRoles.Value = "Non Customer"
                                FieldSet2.Title = "Non Customer Data"
                                WindowScreeningDetail_IsHaveActiveAccount.Hidden = True
                                WindowScreeningDetail_GCN.FieldLabel = "Unique Key"
                                WindowScreeningDetail_CIF.FieldLabel = "Role"
                                If Not IsDBNull(itemrows("GCN")) Then
                                    Dim stringgcn As String = Convert.ToString(itemrows("GCN"))
                                    WindowScreeningDetail_GCN.Text = stringgcn
                                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                                    If drResult IsNot Nothing Then
                                        If Not IsDBNull(drResult("Role")) Then
                                            WindowScreeningDetail_CIF.Text = Convert.ToString(drResult("Role"))
                                        End If
                                        If Not IsDBNull(drResult("Name")) Then
                                            WindowScreeningDetail_CustomerName.Text = Convert.ToString(drResult("Name"))
                                        End If
                                        If Not IsDBNull(drResult("Birth_Date")) Then
                                            WindowScreeningDetail_CustomerDOB.Text = Convert.ToDateTime(drResult("Birth_Date")).ToString("dd-MM-yyyy")
                                        End If
                                        If Not IsDBNull(drResult("Birth_Place")) Then
                                            WindowScreeningDetail_CustomerPOB.Text = Convert.ToString(drResult("Birth_Place"))
                                        End If
                                        'If Not IsDBNull(drResult("Country_Code")) Then
                                        '    WindowScreeningDetail_CustomerIdentityType.FieldLabel = "Negara"
                                        '    WindowScreeningDetail_CustomerIdentityType.Hidden = False
                                        '    WindowScreeningDetail_CustomerIdentityType.Text = GetCountryName(Convert.ToString(drResult("Country_Code")))
                                        'End If
                                        If Not IsDBNull(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")) Then
                                            Dim dtIdentityCustomer As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Identifications(Convert.ToString(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
                                            gp_IdentityCustomer.GetStore.DataSource = dtIdentityCustomer
                                            gp_IdentityCustomer.GetStore.DataBind()
                                            gp_IdentityCustomer.Hidden = False
                                        Else
                                            gp_IdentityCustomer.Hidden = True
                                        End If
                                        ' 18 Nov 2021 Ari pengambahan grid link CIF
                                        GridPanelLinkCIF.Hidden = False
                                        If Not IsDBNull(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")) Then
                                            Dim dtLinkCIF As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Account(Convert.ToString(drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
                                            GridPanelLinkCIF.GetStore.DataSource = dtLinkCIF
                                            GridPanelLinkCIF.GetStore.DataBind()
                                            GridPanelLinkCIF.Hidden = False
                                        Else
                                            GridPanelLinkCIF.Hidden = True
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If

            'Using objdb As New SiPendarEntities
            '    Dim objResult As SIPENDAR_SCREENING_REQUEST_RESULT = objdb.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = id).FirstOrDefault
            '    If objResult IsNot Nothing Then
            '        If objResult.TotalSimilarity IsNot Nothing Then
            '            WindowScreeningDetail_Similarity.Text = (objResult.TotalSimilarity * 100).ToString() & "%"
            '        End If
            '        If objResult.FK_WATCHLIST_ID_PPATK IsNot Nothing Then
            '            WindowScreeningDetail_IDWatchlistPPATK.Text = objResult.FK_WATCHLIST_ID_PPATK.ToString()
            '        End If
            '        If objResult.Nama IsNot Nothing Then
            '            WindowScreeningDetail_NameSearch.Text = objResult.Nama.ToString()
            '        End If
            '        If objResult.DOB IsNot Nothing Then
            '            WindowScreeningDetail_DOBSearch.Text = objResult.DOB.Value.ToString("dd-MM-yyyy")
            '        End If
            '        If objResult.Birth_Place IsNot Nothing Then
            '            WindowScreeningDetail_POBSearch.Text = objResult.Birth_Place.ToString()
            '        End If
            '        If objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE IsNot Nothing Then
            '            WindowScreeningDetail_IdentityTypeSearch.Text = objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE.ToString()
            '        End If
            '        If objResult.Identity_Number IsNot Nothing Then
            '            WindowScreeningDetail_IdentityNumberSearch.Text = objResult.Identity_Number.ToString()
            '        End If
            '        If objResult.GCN IsNot Nothing Then
            '            WindowScreeningDetail_GCN.Text = objResult.GCN.ToString()
            '        End If
            '        If objResult.NAMACUSTOMER IsNot Nothing Then
            '            WindowScreeningDetail_CustomerName.Text = objResult.NAMACUSTOMER.ToString()
            '        End If
            '        If objResult.DOBCustomer IsNot Nothing Then
            '            WindowScreeningDetail_CustomerDOB.Text = objResult.DOBCustomer.Value.ToString("dd-MM-yyyy")
            '        End If
            '        If objResult.Birth_PlaceCustomer IsNot Nothing Then
            '            WindowScreeningDetail_CustomerPOB.Text = objResult.Birth_PlaceCustomer.ToString()
            '        End If
            '        If objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer IsNot Nothing Then
            '            WindowScreeningDetail_CustomerIdentityType.Text = objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer.ToString()
            '        End If
            '        If objResult.Identity_NumberCustomer IsNot Nothing Then
            '            WindowScreeningDetail_CustomerIdentityNumber.Text = objResult.Identity_NumberCustomer.ToString()
            '        End If
            '    End If
            'End Using

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Clear_WindowScreeningDetail()
        Try
            WindowScreeningDetail_Similarity.Clear()
            WindowScreeningDetail_IDWatchlistPPATK.Clear()
            WindowScreeningDetail_NameSearch.Clear()
            WindowScreeningDetail_DOBSearch.Clear()
            WindowScreeningDetail_POBSearch.Clear()
            WindowScreeningDetail_IdentityTypeSearch.Clear()
            WindowScreeningDetail_IdentityNumberSearch.Clear()
            WindowScreeningDetail_GCN.Clear()
            WindowScreeningDetail_CustomerName.Clear()
            WindowScreeningDetail_CustomerDOB.Clear()
            WindowScreeningDetail_CustomerPOB.Clear()
            WindowScreeningDetail_CustomerIdentityType.Clear()
            WindowScreeningDetail_CustomerIdentityNumber.Clear()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadColumnCommandScreeningResult()
        Try
            ColumnActionLocation(gp_ScreeningResult, gp_ScreeningResult_CommandColumn)
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

#End Region
#Region "24-Sep-2021 Daniel"
    ''' <summary>
    ''' Function For Get SIPENDAR_SCREENING_REQUEST_RESULT as DataTabel
    ''' </summary>
    ''' <param name="PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID">PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID Value (Primary Key Of SIPENDAR_SCREENING_REQUEST_RESULT)</param>
    ''' <returns></returns>
    Protected Function GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID As String) As DataTable
        Try
            Dim data As New DataTable
            'Dim stringquery As String = " SELECT result.*, (result.TotalSimilarity*100) AS TotalSimilarityPct, custaccount.[CountAccount], case when custaccount.CountAccount > 0 " &
            '" then 'Yes' else 'No' end as IsHaveActiveAccount FROM SIPENDAR_SCREENING_REQUEST_RESULT result left join ( select distinct cust.GCN, count(cust.GCN) as [CountAccount] " &
            '" from goAML_Ref_Customer cust join goAML_Ref_Account acc on acc.client_number =  cust.CIF join SIPENDAR_SCREENING_REQUEST_RESULT resultget on resultget.GCN = cust.GCN where " &
            '" resultget.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID=" & PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID & " and acc.status_code <> 'TTP' and resultget.GCN is not null group by cust.GCN ) " &
            '" custaccount on result.GCN = custaccount.GCN WHERE result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID=" & PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID & " ORDER BY TotalSimilarityPct Desc, " &
            '" result.GCN, result.Nama"

            'Dim stringquery As String = ""
            'stringquery = "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct, "
            'stringquery += "custaccount.[CountAccount], "
            'stringquery += "case when custaccount.CountAccount > 0 "
            'stringquery += "then 'Yes' else 'No' end as IsHaveActiveAccount "
            'stringquery += "FROM SIPENDAR_SCREENING_REQUEST_RESULT result "
            'stringquery += "Left Join( "
            'stringquery += " Select distinct cust.GCN, count(cust.GCN) As [CountAccount] "
            'stringquery += " From goAML_Ref_Customer cust "
            'stringquery += " Join goAML_Ref_Account acc "
            'stringquery += " On acc.client_number =  cust.CIF "
            'stringquery += " Join SIPENDAR_SCREENING_REQUEST_RESULT resultget "
            'stringquery += " On resultget.GCN = cust.GCN "
            'stringquery += " where "
            'stringquery += "resultget.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = " & PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID & ""
            'stringquery += " And acc.status_code <> 'TTP'  "
            'stringquery += " And resultget.GCN Is Not null group by cust.GCN ) custaccount "
            'stringquery += " On result.GCN = custaccount.GCN "
            'stringquery += "left join SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO b "
            'stringquery += "On result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = b.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID "
            'stringquery += "WHERE result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID In "
            'stringquery += "(Select distinct PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID "
            'stringquery += "from SIPENDAR_SCREENING_REQUEST_RESULT "
            'stringquery += "where PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID =" & intID & " ) "
            'stringquery += "And result.GCN Is Not null  "
            'stringquery += "And isnull(b.Status,'New') in ('New','Reject','Reject Exclusion') "
            'stringquery += "ORDER BY TotalSimilarityPct Desc, result.GCN, Nama "

            'data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_GROUP_RESULT_BY_PK", paramLoadResultGroup)

            Dim paramLoadResultGroup(0) As SqlParameter

            paramLoadResultGroup(0) = New SqlParameter
            paramLoadResultGroup(0).ParameterName = "@PK"
            paramLoadResultGroup(0).Value = PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
            paramLoadResultGroup(0).DbType = SqlDbType.VarChar

            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_GROUP_RESULT_BY_PK", paramLoadResultGroup)
            ' 19 Nov Ari : Penambahan column untuk link cif
            data.Columns.Add(New Data.DataColumn("Account_Name", GetType(String)))
            data.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            data.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))
            ' 22 Dec Ari : Penambahan column submission type
            data.Columns.Add(New Data.DataColumn("Submission_Type", GetType(String)))
            Dim data2 As New DataTable
            Dim data3 As New DataTable
            For Each row As Data.DataRow In data.Rows
                If row.Item("Stakeholder_Role") IsNot Nothing And Not IsDBNull(row.Item("Stakeholder_Role")) Then
                    row.Item("Status") = "Non Customer"

                    ' 19 Nov 2021 Penmbahan untuk link CIF
                    Dim stringgcn As String = Convert.ToString(row("GCN"))
                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                    If drResult IsNot Nothing Then
                        Dim query As String = "DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) " &
                                              "INSERT INTO @tblaccount ( Groupaccount ) " &
                                              "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID") &
                                              " Select STUFF((SELECT ','  + cast(Groupaccount as nvarchar) from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_No, " &
                                              " STUFF((SELECT  ','  +  CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),'') WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),'') " &
                                              " Else '' end from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif " &
                                              " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_Name, STUFF((SELECT ','  + isnull(cast(rc.cif as nvarchar),'') from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as CIF_No "
                        data2 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)
                        For Each row2 As DataRow In data2.Rows
                            row("Account_Name") = row2("Account_Name")
                            row("Account_No") = row2("Account_No")
                            row("CIF_No") = row2("CIF_No")
                        Next
                    Else
                        row("Account_Name") = Nothing
                        row("Account_No") = Nothing
                        row("CIF_No") = Nothing
                    End If

                Else
                    row.Item("Status") = "Customer"
                    row("Account_Name") = Nothing
                    row("Account_No") = Nothing
                    row("CIF_No") = Nothing
                End If
                ' 22 Dec Ari : Panggil query untuk dapatkan submission type
                Dim query2 As String = "select b.FK_SIPENDAR_SUBMISSION_TYPE_CODE FROM SIPENDAR_SCREENING_REQUEST_RESULT a inner join SIPENDAR_SCREENING_REQUEST_DETAIL b on a.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = b.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID where a.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = " & row.Item("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")
                data3 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query2)
                For Each row3 As DataRow In data3.Rows
                    row("Submission_Type") = row3("FK_SIPENDAR_SUBMISSION_TYPE_CODE")
                Next
            Next
            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function
#End Region
#Region "24-Sep-2021 Felix"
    Private Sub LoadApprovalHistoryJudgement()
        Try

            Dim EncryptedReportID As String = Request.Params("ID")
            Dim IDResult = NawaBLL.Common.DecryptQueryString(EncryptedReportID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            Dim objModuleScreeningJudgement As NawaDAL.Module = Nothing
            objModuleScreeningJudgement = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_SCREENING_JUDGEMENT")

            Dim dtResult As DataTable = New DataTable
            Dim paramLoadApprovalHistory(1) As SqlParameter

            paramLoadApprovalHistory(0) = New SqlParameter
            paramLoadApprovalHistory(0).ParameterName = "@Module_ID"
            paramLoadApprovalHistory(0).Value = objModuleScreeningJudgement.PK_Module_ID
            paramLoadApprovalHistory(0).DbType = SqlDbType.VarChar

            paramLoadApprovalHistory(1) = New SqlParameter
            paramLoadApprovalHistory(1).ParameterName = "@Result_ID"
            paramLoadApprovalHistory(1).Value = IDResult
            paramLoadApprovalHistory(1).DbType = SqlDbType.VarChar

            dtResult = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GET_JUDGEMENT_APPROVAL_HISTORY", paramLoadApprovalHistory)
            'Dim objApprovalHistory = SiPendarBLL.goAML_NEW_ReportBLL.get_ListNotesHistory(ObjModule.PK_Module_ID, IDReport)
            If dtResult.Rows.Count <> 0 Then
                gpStore_history.DataSource = dtResult
                gpStore_history.DataBind()
            End If

            panelApprovalHistory.Hidden = False

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
#Region "30-Sep-2021 Adi : Get Customer Closed and Closing Date"
    Protected Function GetCustomerClosedStatus(strGCN As String) As DataRow
        Try

            Dim param(0) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@GCN"
            param(0).Value = strGCN
            param(0).DbType = SqlDbType.VarChar

            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_GetCustomerClosedStatus_ByGCN", param)

            Return drResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function
#End Region
#Region "28 Oct 2021 Ari : Get Non Customer Data"
    Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer(Unique_Key As String) As DataRow
        Try
            Unique_Key = Replace(Unique_Key, "'", "''")
            Dim stringquery As String = " select top 1 * from SIPENDAR_StakeHolder_NonCustomer where Unique_Key = '" & Unique_Key & "' "
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery, Nothing)
            Return drResult
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function
    Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Identifications(FK_SIPENDAR_StakeHolder_NonCustomer_ID As String) As DataTable
        Try
            Dim data As New DataTable
            Dim stringquery As String = " select Type as Identity_Type, Number as Identity_Number, Issue_Date, Expiry_Date, Issued_By, Issued_Country, Comments " &
                " from SIPENDAR_StakeHolder_NonCustomer_Identifications where FK_SIPENDAR_StakeHolder_NonCustomer_ID = " & FK_SIPENDAR_StakeHolder_NonCustomer_ID
            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function
#End Region

#Region "19 Nov Ari tambah Fungsi untuk get Link CIF"
    Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Account(FK_SIPENDAR_StakeHolder_NonCustomer_ID As String) As DataTable
        Try
            Dim data As New DataTable
            Dim stringquery As String = " DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) INSERT INTO @tblaccount (Groupaccount) " &
                "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & FK_SIPENDAR_StakeHolder_NonCustomer_ID &
                " select Groupaccount as Account_No, rc.cif as CIF_No , CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),null)   WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),null) else null end as Account_Name from @tblaccount sn" &
                " left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif"
            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function
#End Region


#Region "20-Jul-2022 Penambhan Export Data Screening atas Request ANZ"
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            Dim PK_Request_ID As String = Request.Params("ID")
            intID = NawaBLL.Common.DecryptQueryString(PK_Request_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            'Dim DataScreeningAll As DataTable = New DataTable
            'DataScreeningAll = GetDataTabelSIPENDAR_SCREENING_REQUEST_RESULT(intID)

            Dim strQuery As String = ""
            'mengikuti sp usp_GET_GROUP_RESULT_BY_PK
            strQuery = " declare @PK as bigint = " & intID &
             " ;With ResultGroup as ( " &
             " select PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID,PK_SIPENDAR_SCREENING_REQUEST_ID,GCN,result.FK_WATCHLIST_ID_PPATK " &
             " ,reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
             " inner join SIPENDAR_SCREENING_REQUEST_DETAIL reqDet " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = reqdet.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
             " where PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = @PK ) " &
             " SELECT " &
             " result.Nama, " &
             " result.DOB, " &
             " result.Birth_Place, " &
             " result.Identity_Number, " &
             " result.FK_WATCHLIST_ID_PPATK, " &
             " result.GCN, " &
             " (SELECT STRING_AGG (CIF, ',') AS CIF FROM goAML_Ref_Customer where GCN = result.GCN ) as CIF, " &
             " result.NAMACUSTOMER, " &
             " result.DOBCustomer, " &
             " result.Birth_PlaceCustomer, " &
             " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer, " &
             " result.Identity_NumberCustomer, " &
             " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE, " &
             " result.CreatedDate, " &
             " result.Stakeholder_Role, " &
             " (result.TotalSimilarity*100) AS TotalSimilarityPct, " &
             " case when result.Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation " &
             " ,reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
             " inner join ResultGroup RG " &
             " on result.GCN = RG.GCN " &
             " and isnull(result.FK_WATCHLIST_ID_PPATK,result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID) " &
             " = isnull(RG.FK_WATCHLIST_ID_PPATK,RG.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID) " &
             " left join SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO OI " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = OI.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID " &
             " and isnull(OI.Status,'') in ('Accept','Not Match') " &
             " Inner join SIPENDAR_SCREENING_REQUEST_DETAIL reqDet " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = reqdet.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
             " and reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE = RG.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " where OI.PK_SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO_ID is null " &
             " ORDER BY TotalSimilarityPct Desc, result.GCN, Nama "

            Dim DataScreeningAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery,)

            If DataScreeningAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            End If

            '23 Nov 2021 Ari : Penambahan Link To CIF/Account/Account Name
            DataScreeningAll.Columns.Add(New Data.DataColumn("Account_Name", GetType(String)))
            DataScreeningAll.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            DataScreeningAll.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))

            Dim data2 As New DataTable
            For Each row As DataRow In DataScreeningAll.Rows
                If row.Item("customerinformation") = "Non Customer" Then
                    Dim stringgcn As String = Convert.ToString(row("GCN"))
                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                    If drResult IsNot Nothing Then
                        Dim query As String = "DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) " &
                                              "INSERT INTO @tblaccount ( Groupaccount ) " &
                                              "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID") &
                                              " Select STUFF((SELECT ','  + cast(Groupaccount as nvarchar) from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_No, " &
                                              " STUFF((SELECT  ','  +  CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),'') WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),'') " &
                                              " Else '' end from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif " &
                                              " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_Name, STUFF((SELECT ','  + isnull(cast(rc.cif as nvarchar),'') from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as CIF_No "
                        data2 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)
                        For Each row2 As DataRow In data2.Rows
                            row("Account_Name") = row2("Account_Name")
                            row("Account_No") = row2("Account_No")
                            row("CIF_No") = row2("CIF_No")
                        Next
                    Else
                        row("Account_Name") = Nothing
                        row("Account_No") = Nothing
                        row("CIF_No") = Nothing
                    End If
                Else
                    row("Account_Name") = Nothing
                    row("Account_No") = Nothing
                    row("CIF_No") = Nothing
                End If

            Next

            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))



                    Using objtbl As Data.DataTable = DataScreeningAll

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("SipendarScreeningResult")
                            'objtbl.Columns.Remove("PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")
                            'objtbl.Columns.Remove("PK_SIPENDAR_SCREENING_REQUEST_ID")
                            'objtbl.Columns.Remove("UserIDRequest")
                            'objtbl.Columns.Remove("CreatedDate")
                            'objtbl.Columns.Remove("FK_SIPENDAR_SCREENING_REQUEST_TYPE_ID")
                            'objtbl.Columns.Remove("_Similarity")
                            'objtbl.Columns.Remove("_Confidence")
                            'objtbl.Columns.Remove("_Similarity_Nama")
                            'objtbl.Columns.Remove("_Similarity_DOB")
                            'objtbl.Columns.Remove("IsNeedToReportSIPENDAR")
                            'objtbl.Columns.Remove("Approved")
                            'objtbl.Columns.Remove("ApprovedBy")
                            'objtbl.Columns.Remove("Nationality")
                            'objtbl.Columns.Remove("NationalityCustomer")
                            'objtbl.Columns.Remove("_Similarity_Birth_Place")
                            'objtbl.Columns.Remove("_Similarity_Identity_Number")
                            'objtbl.Columns.Remove("_Similarity_Nationality")
                            'objtbl.Columns.Remove("PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID")
                            objtbl.Columns("Nama").SetOrdinal(0)
                            objtbl.Columns("DOB").SetOrdinal(1)
                            objtbl.Columns("Birth_Place").SetOrdinal(2)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                            objtbl.Columns("Identity_Number").SetOrdinal(4)
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                            objtbl.Columns("customerinformation").SetOrdinal(7)
                            objtbl.Columns("CIF").SetOrdinal(8)
                            objtbl.Columns("CIF_No").SetOrdinal(9)
                            objtbl.Columns("Account_No").SetOrdinal(10)
                            objtbl.Columns("Account_Name").SetOrdinal(11)
                            objtbl.Columns("GCN").SetOrdinal(12)
                            objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                            objtbl.Columns("DOBCustomer").SetOrdinal(14)
                            objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                            objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                            objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                            objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)

                            objtbl.Columns("Nama").ColumnName = "Name Search Data"
                            objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                            objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                            objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                            objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                            objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                            objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                            objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                            objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                            objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                            objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                            objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                            objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                            objtbl.Columns("Account_No").ColumnName = "Link To Account"
                            objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = DataScreeningAll                    'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        objtbl.Columns("Nama").SetOrdinal(0)
                        objtbl.Columns("DOB").SetOrdinal(1)
                        objtbl.Columns("Birth_Place").SetOrdinal(2)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                        objtbl.Columns("Identity_Number").SetOrdinal(4)
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                        objtbl.Columns("customerinformation").SetOrdinal(7)
                        objtbl.Columns("CIF").SetOrdinal(8)
                        objtbl.Columns("CIF_No").SetOrdinal(9)
                        objtbl.Columns("Account_No").SetOrdinal(10)
                        objtbl.Columns("Account_Name").SetOrdinal(11)
                        objtbl.Columns("GCN").SetOrdinal(12)
                        objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                        objtbl.Columns("DOBCustomer").SetOrdinal(14)
                        objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                        objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                        objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                        objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)

                        objtbl.Columns("Nama").ColumnName = "Name Search Data"
                        objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                        objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                        objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                        objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                        objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                        objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                        objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                        objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                        objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                        objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                        objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                        objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                        objtbl.Columns("Account_No").ColumnName = "Link To Account"
                        objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            Dim pagecurrent As Integer = e.ExtraParams("currentPage")

            'Dim DataScreeningCurrentPage As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "DECLARE @PageNumber AS INT, @RowspPage AS INT SET @PageNumber =" & pagecurrent & " SET @RowspPage = 10 SELECT Nama,DOB,Birth_Place,Stakeholder_Role,case when Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation,CONVERT(varchar(4000),STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer rf WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'')) as CIF,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE,Identity_Number,FK_WATCHLIST_ID_PPATK,GCN,NAMACUSTOMER,DOBCustomer,Birth_PlaceCustomer,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer,Identity_NumberCustomer, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT result WHERE PK_SIPENDAR_SCREENING_REQUEST_ID=" & PK_Request_ID & " ORDER BY TotalSimilarityPct Desc, GCN, Nama OFFSET ((@PageNumber - 1) * @RowspPage) ROWS FETCH NEXT @RowspPage ROWS ONLY",)

            '' Edit 15-Dec-2021 Felix
            Dim strQuery As String = ""
            'mengikuti sp usp_GET_GROUP_RESULT_BY_PK
            strQuery = "DECLARE @PageNumber AS INT, " &
             " @RowspPage AS INT SET @PageNumber =" & pagecurrent &
             " SET @RowspPage = 10 " &
             " declare @PK as bigint = " & IDScreeningJudgement &
             " ;With ResultGroup as ( " &
             " select PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID,PK_SIPENDAR_SCREENING_REQUEST_ID,GCN,result.FK_WATCHLIST_ID_PPATK " &
             " ,reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
             " inner join SIPENDAR_SCREENING_REQUEST_DETAIL reqDet " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = reqdet.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
             " where PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = @PK ) " &
             " SELECT " &
             " result.Nama, " &
             " result.DOB, " &
             " result.Birth_Place, " &
             " result.Identity_Number, " &
             " result.FK_WATCHLIST_ID_PPATK, " &
             " result.GCN, " &
             " (SELECT STRING_AGG (CIF, ',') AS CIF FROM goAML_Ref_Customer where GCN = result.GCN ) as CIF, " &
             " result.NAMACUSTOMER, " &
             " result.DOBCustomer, " &
             " result.Birth_PlaceCustomer, " &
             " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer, " &
             " result.Identity_NumberCustomer, " &
             " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE, " &
             " result.CreatedDate, " &
             " result.Stakeholder_Role, " &
             " (result.TotalSimilarity*100) AS TotalSimilarityPct, " &
             " case when result.Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation " &
             " ,reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
             " inner join ResultGroup RG " &
             " on result.GCN = RG.GCN " &
             " and isnull(result.FK_WATCHLIST_ID_PPATK,result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID) " &
             " = isnull(RG.FK_WATCHLIST_ID_PPATK,RG.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID) " &
             " left join SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO OI " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = OI.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID " &
             " and isnull(OI.Status,'') in ('Accept','Not Match') " &
             " Inner join SIPENDAR_SCREENING_REQUEST_DETAIL reqDet " &
             " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = reqdet.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
             " and reqDet.FK_SIPENDAR_SUBMISSION_TYPE_CODE = RG.FK_SIPENDAR_SUBMISSION_TYPE_CODE " &
             " where OI.PK_SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO_ID is null " &
             " ORDER BY TotalSimilarityPct Desc, result.GCN, Nama " &
             " OFFSET ((@PageNumber - 1) * @RowspPage) ROWS FETCH NEXT @RowspPage ROWS ONLY"

            'Dim DataScreeningCurrentPage As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "DECLARE @PageNumber AS INT, @RowspPage AS INT SET @PageNumber =" & pagecurrent & " SET @RowspPage = 10 SELECT Nama,DOB,Birth_Place,Stakeholder_Role,case when Stakeholder_Role Is null then 'Customer' else 'Non Customer' end as customerinformation,CONVERT(varchar(4000),STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer rf WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'')) as CIF,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE,Identity_Number,FK_WATCHLIST_ID_PPATK,GCN,NAMACUSTOMER,DOBCustomer,Birth_PlaceCustomer,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer,Identity_NumberCustomer, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT result WHERE PK_SIPENDAR_SCREENING_REQUEST_ID=" & PK_Request_ID & " ORDER BY TotalSimilarityPct Desc, GCN, Nama OFFSET ((@PageNumber - 1) * @RowspPage) ROWS FETCH NEXT @RowspPage ROWS ONLY",)
            Dim DataScreeningCurrentPage As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery,)
            '' 15-Dec-2021
            If DataScreeningCurrentPage.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            End If

            '23 Nov 2021 Ari : Penambahan Link To CIF/Account/Account Name
            DataScreeningCurrentPage.Columns.Add(New Data.DataColumn("Account_Name", GetType(String)))
            DataScreeningCurrentPage.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            DataScreeningCurrentPage.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))

            Dim data2 As New DataTable
            For Each row As DataRow In DataScreeningCurrentPage.Rows
                If row.Item("customerinformation") = "Non Customer" Then
                    Dim stringgcn As String = Convert.ToString(row("GCN"))
                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                    If drResult IsNot Nothing Then
                        Dim query As String = "DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) " &
                                              "INSERT INTO @tblaccount ( Groupaccount ) " &
                                              "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID") &
                                              " Select STUFF((SELECT ','  + cast(Groupaccount as nvarchar) from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_No, " &
                                              " STUFF((SELECT  ','  +  CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),'') WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),'') " &
                                              " Else '' end from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif " &
                                              " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_Name, STUFF((SELECT ','  + isnull(cast(rc.cif as nvarchar),'') from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as CIF_No "
                        data2 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)
                        For Each row2 As DataRow In data2.Rows
                            row("Account_Name") = row2("Account_Name")
                            row("Account_No") = row2("Account_No")
                            row("CIF_No") = row2("CIF_No")
                        Next
                    Else
                        row("Account_Name") = Nothing
                        row("Account_No") = Nothing
                        row("CIF_No") = Nothing
                    End If
                Else
                    row("Account_Name") = Nothing
                    row("Account_No") = Nothing
                    row("CIF_No") = Nothing
                End If

            Next


            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))



                    Using objtbl As Data.DataTable = DataScreeningCurrentPage

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("Sipendar_Screening_Request")
                            objtbl.Columns("Nama").SetOrdinal(0)
                            objtbl.Columns("DOB").SetOrdinal(1)
                            objtbl.Columns("Birth_Place").SetOrdinal(2)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                            objtbl.Columns("Identity_Number").SetOrdinal(4)
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                            objtbl.Columns("customerinformation").SetOrdinal(7)
                            objtbl.Columns("CIF").SetOrdinal(8)
                            objtbl.Columns("CIF_No").SetOrdinal(9)
                            objtbl.Columns("Account_No").SetOrdinal(10)
                            objtbl.Columns("Account_Name").SetOrdinal(11)
                            objtbl.Columns("GCN").SetOrdinal(12)
                            objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                            objtbl.Columns("DOBCustomer").SetOrdinal(14)
                            objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                            objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                            objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                            objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)

                            objtbl.Columns("Nama").ColumnName = "Name Search Data"
                            objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                            objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                            objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                            objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                            objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                            objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                            objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                            objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                            objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                            objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                            objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                            objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                            objtbl.Columns("Account_No").ColumnName = "Link To Account"
                            objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)



                    Using objtbl As Data.DataTable = DataScreeningCurrentPage

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        objtbl.Columns("Nama").SetOrdinal(0)
                        objtbl.Columns("DOB").SetOrdinal(1)
                        objtbl.Columns("Birth_Place").SetOrdinal(2)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                        objtbl.Columns("Identity_Number").SetOrdinal(4)
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                        objtbl.Columns("customerinformation").SetOrdinal(7)
                        objtbl.Columns("CIF").SetOrdinal(8)
                        objtbl.Columns("CIF_No").SetOrdinal(9)
                        objtbl.Columns("Account_No").SetOrdinal(10)
                        objtbl.Columns("Account_Name").SetOrdinal(11)
                        objtbl.Columns("GCN").SetOrdinal(12)
                        objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                        objtbl.Columns("DOBCustomer").SetOrdinal(14)
                        objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                        objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                        objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                        objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)

                        objtbl.Columns("Nama").ColumnName = "Name Search Data"
                        objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                        objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                        objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                        objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                        objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                        objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                        objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                        objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                        objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                        objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                        objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                        objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                        objtbl.Columns("Account_No").ColumnName = "Link To Account"
                        objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    Protected Sub ExportSelectedExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)
            Dim selected As RowSelectionModel = gp_ScreeningResult.SelectionModel.Primary
            If selected.SelectedRows.Count = 0 Then

                Throw New Exception("Minimal 1 data harus dipilih!")
            End If
            Dim listScreeninnigResultPK As New List(Of SIPENDAR_SCREENING_REQUEST_RESULT)
            For Each item As SelectedRow In smScreeningResult.SelectedRows
                Dim recordID = item.RecordID.ToString
                Dim objNew = New SiPendarDAL.SIPENDAR_SCREENING_REQUEST_RESULT
                With objNew
                    .PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = recordID
                End With

                listScreeninnigResultPK.Add(objNew)
            Next
            Dim pkscreeningresultselected As String = ""
            Dim listScreeninnigResult As New List(Of SIPENDAR_SCREENING_REQUEST_RESULT)
            For Each item In listScreeninnigResultPK
                Dim cariscreeningrequest As New SIPENDAR_SCREENING_REQUEST_RESULT
                Using objdb As New SiPendarEntities
                    cariscreeningrequest = objdb.SIPENDAR_SCREENING_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = item.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID).FirstOrDefault()
                End Using
                If cariscreeningrequest IsNot Nothing Then
                    listScreeninnigResult.Add(New SIPENDAR_SCREENING_REQUEST_RESULT() With {
                  .PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = cariscreeningrequest.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID,
                  .Nama = cariscreeningrequest.Nama,
                  .DOBCustomer = cariscreeningrequest.DOBCustomer,
                  .Birth_PlaceCustomer = cariscreeningrequest.Birth_PlaceCustomer,
                  .Identity_Number = cariscreeningrequest.Identity_Number,
                  .FK_WATCHLIST_ID_PPATK = cariscreeningrequest.FK_WATCHLIST_ID_PPATK,
                  .GCN = cariscreeningrequest.GCN,
                  .NAMACUSTOMER = cariscreeningrequest.NAMACUSTOMER,
                  .DOB = cariscreeningrequest.DOB,
                  .Birth_Place = cariscreeningrequest.Birth_Place,
                  .Identity_NumberCustomer = cariscreeningrequest.Identity_Number,
                  .TotalSimilarity = cariscreeningrequest.TotalSimilarity * 100,
                  .FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer = cariscreeningrequest.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer
                  })
                End If

                pkscreeningresultselected = pkscreeningresultselected & "," & item.PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID
            Next
            pkscreeningresultselected = pkscreeningresultselected.Remove(0, 1)

            '' Edit 15-Dec-2021 Felix
            'Dim strquery As String = "SELECT Nama,DOB,Birth_Place,Stakeholder_Role,case when Stakeholder_Role is null then 'Customer' else 'Non Customer' end as customerinformation,CONVERT(VARCHAR(4000),STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'')) as CIF,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE,Identity_Number,FK_WATCHLIST_ID_PPATK,GCN,NAMACUSTOMER,DOBCustomer,Birth_PlaceCustomer,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer,Identity_NumberCustomer, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT result WHERE PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID in(" & pkscreeningresultselected & ") ORDER BY TotalSimilarityPct Desc, GCN, Nama"
            Dim strquery As String = "SELECT result.Nama,result.DOB,result.Birth_Place,result.Stakeholder_Role, " &
                " case when result.Stakeholder_Role Is null then 'Customer' else 'Non Customer' end as customerinformation,CONVERT(VARCHAR(4000),STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'')) as CIF, " &
                " result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE,result.Identity_Number,result.FK_WATCHLIST_ID_PPATK, " &
                " req.FK_SIPENDAR_SUBMISSION_TYPE_CODE, " &
                " result.GCN,result.NAMACUSTOMER,result.DOBCustomer,result.Birth_PlaceCustomer,result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer,result.Identity_NumberCustomer, (TotalSimilarity*100) AS TotalSimilarityPct, result.CreatedDate " &
                " FROM SIPENDAR_SCREENING_REQUEST_RESULT result " &
                " inner join SIPENDAR_SCREENING_REQUEST_DETAIL req " &
                " on result.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID = req.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID " &
                " WHERE PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID in(" & pkscreeningresultselected & ") ORDER BY TotalSimilarityPct Desc, GCN, Nama"


            '' End 15-Dec-2021
            Dim DataScreeningSelectResult As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquery,)

            '23 Nov 2021 Ari : Penambahan Link To CIF/Account/Account Name
            DataScreeningSelectResult.Columns.Add(New Data.DataColumn("Account_Name", GetType(String)))
            DataScreeningSelectResult.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            DataScreeningSelectResult.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))
            Dim data2 As New DataTable
            For Each row As DataRow In DataScreeningSelectResult.Rows
                If row.Item("customerinformation") = "Non Customer" Then
                    Dim stringgcn As String = Convert.ToString(row("GCN"))
                    Dim drResult As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
                    If drResult IsNot Nothing Then
                        Dim query As String = "DECLARE @tblaccount TABLE (Groupaccount varchar(4000)) " &
                                              "INSERT INTO @tblaccount ( Groupaccount ) " &
                                              "select value from sipendar_stakeholder_noncustomer sp cross apply STRING_SPLIT(sp.account,',') where sp.pk_sipendar_stakeholder_noncustomer_id = " & drResult("PK_SIPENDAR_StakeHolder_NonCustomer_ID") &
                                              " Select STUFF((SELECT ','  + cast(Groupaccount as nvarchar) from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_No, " &
                                              " STUFF((SELECT  ','  +  CASE WHEN FK_CUSTOMER_TYPE_ID = 1 THEN isnull(cast(rc.indv_last_name as nvarchar),'') WHEN FK_CUSTOMER_TYPE_ID = 2 THEN isnull(cast(rc.corp_name as nvarchar),'') " &
                                              " Else '' end from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no left join goAML_ref_customer rc on rf.client_number = rc.cif " &
                                              " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as Account_Name, STUFF((SELECT ','  + isnull(cast(rc.cif as nvarchar),'') from @tblaccount sn left join goAML_Ref_Account rf on Groupaccount = rf.account_no " &
                                              " left join goAML_ref_customer rc on rf.client_number = rc.cif FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(4000)'), 1, 1, '') as CIF_No "
                        data2 = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)
                        For Each row2 As DataRow In data2.Rows
                            row("Account_Name") = row2("Account_Name")
                            row("Account_No") = row2("Account_No")
                            row("CIF_No") = row2("CIF_No")
                        Next
                    Else
                        row("Account_Name") = Nothing
                        row("Account_No") = Nothing
                        row("CIF_No") = Nothing
                    End If
                Else
                    row("Account_Name") = Nothing
                    row("Account_No") = Nothing
                    row("CIF_No") = Nothing
                End If

            Next


            Dim tablescreeningresult As New DataTable
            Dim fields() As FieldInfo = GetType(SIPENDAR_SCREENING_REQUEST_RESULT).GetFields(BindingFlags.Instance Or BindingFlags.Static Or BindingFlags.NonPublic Or BindingFlags.Public)
            For Each field As FieldInfo In fields

                tablescreeningresult.Columns.Add(field.Name, System.Type.GetType("System.String"))
            Next
            For Each item As SIPENDAR_SCREENING_REQUEST_RESULT In listScreeninnigResult
                Dim row As DataRow = tablescreeningresult.NewRow()
                For Each field As FieldInfo In fields
                    row(field.Name) = field.GetValue(item)

                Next

                tablescreeningresult.Rows.Add(row)
            Next
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    tablescreeningresult.DefaultView.Sort = "_TotalSimilarity Desc, _GCN"
                    Using objtbl As Data.DataTable = DataScreeningSelectResult

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("SipendarScreeningResult")
                            'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")
                            'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_ID")
                            'objtbl.Columns.Remove("_UserIDRequest")
                            'objtbl.Columns.Remove("_CreatedDate")
                            'objtbl.Columns.Remove("_FK_SIPENDAR_SCREENING_REQUEST_TYPE_ID")
                            'objtbl.Columns.Remove("_C_Similarity")
                            'objtbl.Columns.Remove("_C_Confidence")
                            'objtbl.Columns.Remove("_C_Similarity_Nama")
                            'objtbl.Columns.Remove("_C_Similarity_DOB")
                            'objtbl.Columns.Remove("_IsNeedToReportSIPENDAR")
                            'objtbl.Columns.Remove("_Approved")
                            'objtbl.Columns.Remove("_ApprovedBy")
                            'objtbl.Columns.Remove("_Nationality")
                            'objtbl.Columns.Remove("_NationalityCustomer")
                            'objtbl.Columns.Remove("_C_Similarity_Birth_Place")
                            'objtbl.Columns.Remove("_C_Similarity_Identity_Number")
                            'objtbl.Columns.Remove("_C_Similarity_Nationality")
                            'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID")
                            'objtbl.Columns("_Nama").ColumnName = "Name Search Data"
                            'objtbl.Columns("_DOB").ColumnName = "DOB Search Data"
                            'objtbl.Columns("_GCN").ColumnName = "GCN Search Data"
                            'objtbl.Columns("_NAMACUSTOMER").ColumnName = "Name Customer Data"
                            'objtbl.Columns("_DOBCustomer").ColumnName = "DOB Customer Data"
                            'objtbl.Columns("_TotalSimilarity").ColumnName = "Similarity"
                            'objtbl.Columns("_Identity_Number").ColumnName = "Identity Number Search Data"
                            'objtbl.Columns("_Birth_Place").ColumnName = "Birth Place Search Data"
                            'objtbl.Columns("_Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                            'objtbl.Columns("_FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Search Data"
                            'objtbl.Columns("_Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                            'objtbl.Columns("_FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            objtbl.Columns("Nama").SetOrdinal(0)
                            objtbl.Columns("DOB").SetOrdinal(1)
                            objtbl.Columns("Birth_Place").SetOrdinal(2)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                            objtbl.Columns("Identity_Number").SetOrdinal(4)
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                            objtbl.Columns("customerinformation").SetOrdinal(7)
                            objtbl.Columns("CIF").SetOrdinal(8)
                            objtbl.Columns("CIF_No").SetOrdinal(9)
                            objtbl.Columns("Account_No").SetOrdinal(10)
                            objtbl.Columns("Account_Name").SetOrdinal(11)
                            objtbl.Columns("GCN").SetOrdinal(12)
                            objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                            objtbl.Columns("DOBCustomer").SetOrdinal(14)
                            objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                            objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                            objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                            objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)
                            objtbl.Columns("CreatedDate").SetOrdinal(20)

                            objtbl.Columns("Nama").ColumnName = "Name Search Data"
                            objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                            objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                            objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                            objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                            objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                            objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                            objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                            objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                            objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                            objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                            objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                            objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                            objtbl.Columns("Account_No").ColumnName = "Link To Account"
                            objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = DataScreeningSelectResult

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_RESULT_ID")
                        'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_ID")
                        'objtbl.Columns.Remove("_UserIDRequest")
                        'objtbl.Columns.Remove("_CreatedDate")
                        'objtbl.Columns.Remove("_FK_SIPENDAR_SCREENING_REQUEST_TYPE_ID")
                        'objtbl.Columns.Remove("_C_Similarity")
                        'objtbl.Columns.Remove("_C_Confidence")
                        'objtbl.Columns.Remove("_C_Similarity_Nama")
                        'objtbl.Columns.Remove("_C_Similarity_DOB")
                        'objtbl.Columns.Remove("_IsNeedToReportSIPENDAR")
                        'objtbl.Columns.Remove("_Approved")
                        'objtbl.Columns.Remove("_ApprovedBy")
                        'objtbl.Columns.Remove("_Nationality")
                        'objtbl.Columns.Remove("_NationalityCustomer")
                        'objtbl.Columns.Remove("_C_Similarity_Birth_Place")
                        'objtbl.Columns.Remove("_C_Similarity_Identity_Number")
                        'objtbl.Columns.Remove("_C_Similarity_Nationality")
                        'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID")
                        'objtbl.Columns("_Nama").ColumnName = "Name Search Data"
                        'objtbl.Columns("_DOB").ColumnName = "DOB Search Data"
                        'objtbl.Columns("_GCN").ColumnName = "GCN Search Data"
                        'objtbl.Columns("_NAMACUSTOMER").ColumnName = "Name Customer Data"
                        'objtbl.Columns("_DOBCustomer").ColumnName = "DOB Customer Data"
                        'objtbl.Columns("_TotalSimilarity").ColumnName = "Similarity"
                        'objtbl.Columns("_Identity_Number").ColumnName = "Identity Number Search Data"
                        'objtbl.Columns("_Birth_Place").ColumnName = "Birth Place Search Data"
                        'objtbl.Columns("_Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                        'objtbl.Columns("_FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Search Data"
                        'objtbl.Columns("_Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                        'objtbl.Columns("_FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        objtbl.Columns("Nama").SetOrdinal(0)
                        objtbl.Columns("DOB").SetOrdinal(1)
                        objtbl.Columns("Birth_Place").SetOrdinal(2)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").SetOrdinal(3)
                        objtbl.Columns("Identity_Number").SetOrdinal(4)
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(5)
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").SetOrdinal(6) ''Add 15-Dec-2021 Felix
                        objtbl.Columns("customerinformation").SetOrdinal(7)
                        objtbl.Columns("CIF").SetOrdinal(8)
                        objtbl.Columns("CIF_No").SetOrdinal(9)
                        objtbl.Columns("Account_No").SetOrdinal(10)
                        objtbl.Columns("Account_Name").SetOrdinal(11)
                        objtbl.Columns("GCN").SetOrdinal(12)
                        objtbl.Columns("NAMACUSTOMER").SetOrdinal(13)
                        objtbl.Columns("DOBCustomer").SetOrdinal(14)
                        objtbl.Columns("Birth_PlaceCustomer").SetOrdinal(15)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").SetOrdinal(16)
                        objtbl.Columns("Identity_NumberCustomer").SetOrdinal(17)
                        objtbl.Columns("Stakeholder_Role").SetOrdinal(18)
                        objtbl.Columns("TotalSimilarityPct").SetOrdinal(19)
                        objtbl.Columns("CreatedDate").SetOrdinal(20)

                        objtbl.Columns("Nama").ColumnName = "Name Search Data"
                        objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                        objtbl.Columns("GCN").ColumnName = "GCN Customer Data"
                        objtbl.Columns("NAMACUSTOMER").ColumnName = "Name Customer Data"
                        objtbl.Columns("DOBCustomer").ColumnName = "DOB Customer Data"
                        objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                        objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                        objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                        objtbl.Columns("Identity_NumberCustomer").ColumnName = "Identity Number Customer Data"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer").ColumnName = "ID Type Customer"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE").ColumnName = "ID Type Search Data"
                        objtbl.Columns("Birth_PlaceCustomer").ColumnName = "Birth Place Customer Data"
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        objtbl.Columns("FK_SIPENDAR_SUBMISSION_TYPE_CODE").ColumnName = "Submission Type" ''Add 15-Dec-2021 Felix
                        objtbl.Columns("Stakeholder_Role").ColumnName = "Non-Customer Role"
                        objtbl.Columns("customerinformation").ColumnName = "Customer / Non-Customer"
                        objtbl.Columns("CIF_No").ColumnName = "Link To CIF"
                        objtbl.Columns("Account_No").ColumnName = "Link To Account"
                        objtbl.Columns("Account_Name").ColumnName = "Link To Account Name"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    Protected Sub sar_IsSelectedAll_Result_CheckedChanged(sender As Object, e As EventArgs) Handles sar_IsSelectedAll_Result.DirectCheck
        If sar_IsSelectedAll_Result.Value Then
            'Ext.Net.X.Js.Call("Hide")
            gp_ScreeningResult.Hide()
            BtnExport.Hide()
            BtnExportSelected.Hide()
            gp_ScreeningResult_All.Show()
        Else
            'Ext.Net.X.Js.Call("Show")
            gp_ScreeningResult.Show()
            BtnExport.Show()
            BtnExportSelected.Show()
            gp_ScreeningResult_All.Hide()
        End If
    End Sub

    Protected Sub sm_DownloadFromScreeningResult_change(sender As Object, e As EventArgs)
        Try
            Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)
            LabelScreeningResultCount.Text = "Selected : " & smScreeningResult.SelectedRows.Count & " of " & TotalScreeningResult
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class
