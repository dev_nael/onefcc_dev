﻿Imports System.Data
Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Drawing
Imports System.IO
Imports System.Reflection
Imports System.Threading
Imports Elmah
Imports NawaDAL
Imports OfficeOpenXml
Imports OfficeOpenXml.Style
Imports SiPendarBLL
Imports SiPendarDAL

Partial Class SIPENDAR_ScreeningCustomerProspect_SipendarScreeningCustomerProspect
    Inherits ParentPage

    Const PATH_TEMP_DIR As String = "~\temp\"


    Public Property PK_Request_ID() As Long
        Get
            Return Session("SipendarScreeningCustomerProspect.PK_Request_ID")
        End Get
        Set(ByVal value As Long)
            Session("SipendarScreeningCustomerProspect.PK_Request_ID") = value
        End Set
    End Property

    Public Property ScreeningStatus() As String
        Get
            Return Session("SipendarScreeningCustomerProspect.ScreeningStatus")
        End Get
        Set(ByVal value As String)
            Session("SipendarScreeningCustomerProspect.ScreeningStatus") = value
        End Set
    End Property

    Public Property objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST() As SiPendarDAL.SIPENDAR_Screening_Customer_Prospect_Request
        Get
            Return Session("SipendarScreeningCustomerProspect.objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST")
        End Get
        Set(ByVal value As SiPendarDAL.SIPENDAR_Screening_Customer_Prospect_Request)
            Session("SipendarScreeningCustomerProspect.objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST") = value
        End Set
    End Property

    Public Property listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL() As List(Of SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL)
        Get
            Return Session("SipendarScreeningCustomerProspect.listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL))
            Session("SipendarScreeningCustomerProspect.listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL") = value
        End Set
    End Property

    Public Property objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_Edit() As SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL
        Get
            Return Session("SipendarScreeningCustomerProspect.objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_Edit")
        End Get
        Set(ByVal value As SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL)
            Session("SipendarScreeningCustomerProspect.objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_Edit") = value
        End Set
    End Property

    Private Sub SipendarScreeningCustomerProspect_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        Me.ActionType = NawaBLL.Common.ModuleActionEnum.view
    End Sub

    Private Sub SipendarScreeningCustomerProspect_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = Me.ObjModule.ModuleLabel

                'Date Format sesuai System Parameter
                txt_DOB.Format = NawaBLL.SystemParameterBLL.GetDateFormat
                'sar_reportIndicator.FieldStyle = "background-color: #FFE4C4" 'Ari 15-09-2021   '29-Sep-2021 ganti jadi NDS Dropdown
                'Set command column location (Left/Right)
                SetCommandColumnLocation()
                LoadColumnCommandScreeningResult()

                'Clear Session
                ClearSession()

                ' Pak Adi 01 Oktober 2021
                Ext.Net.X.Call("showSMWatchlist")
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ClearSession()
        Try
            objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST = New SiPendarDAL.SIPENDAR_Screening_Customer_Prospect_Request
            listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL = New List(Of SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL)
            'ListIndicator = New List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
            objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_Edit = Nothing

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "General"
    Protected Sub btn_Cancel_Click()
        Try
            ''Clear Screening Request
            'listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL = New List(Of SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL)
            'BindScreeningRequest()

            ''Hide Grid Screening Result
            'gp_ScreeningRequest.Hidden = False
            'gp_ScreeningResult.Hidden = True
            'gp_ScreeningResult.GetSelectionModel.ClearSelection()

            ''Clear Screening Status
            'df_ScreeningStart.Text = ""
            'df_ScreeningStatus.Text = ""
            'df_ScreeningFinished.Text = ""

            ''Show button Screening
            'btn_Screening.Hidden = False
            'btn_Save.Hidden = True
            'pnlSiPendarReport.Hidden = True

            ''Clean SiPendar Report fields
            'cmb_JENIS_WATCHLIST_CODE.SetTextValue("")
            'cmb_SUMBER_INFORMASI_KHUSUS_CODE.SetTextValue("")
            'cmb_TINDAK_PIDANA_CODE.SetTextValue("")

            'cmb_SUMBER_TYPE_CODE.SetTextValue("")
            'txt_SUMBER_ID.Value = Nothing
            'txt_Keterangan.Value = Nothing

            ''8-Jul-2021 Adi : Tamabahan permintaan BSIM
            'cmb_SUBMISSION_TYPE_CODE.SetTextValue("")

            'chk_IsGenerateTransaction.Checked = False
            'txt_Transaction_DateFrom.Value = Nothing
            'txt_Transaction_DateTo.Value = Nothing

            Clear_Search()

            ''Added by Felix on 27 Aug 2021

            'Dim paramChangeStatus(1) As SqlParameter

            'paramChangeStatus(0) = New SqlParameter
            'paramChangeStatus(0).ParameterName = "@PK"
            'paramChangeStatus(0).Value = PK_Request_ID
            'paramChangeStatus(0).DbType = SqlDbType.VarChar

            'paramChangeStatus(1) = New SqlParameter
            'paramChangeStatus(1).ParameterName = "@Status"
            'paramChangeStatus(1).Value = "Excluded"
            'paramChangeStatus(1).DbType = SqlDbType.VarChar

            'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_INSERT_STATUS_OTHER_INFO", paramChangeStatus)
            '''End of 27 Aug 2021

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Sub SetCommandColumnLocation()
        Try
            ColumnActionLocation(gp_ScreeningRequest, cc_ScreeningRequest)
            'ColumnActionLocation(gp_DownloadFromWatchlist, cc_DownloadFromWatchlist)
            'ColumnActionLocation(GridPanelReportIndicator, cc_ReportIndicator)
            'ColumnActionLocation(gp_DownloadFromWatchlistAllCheck, cc_DownloadFromWatchlistAllCheck)

            '29-Sep-2021 ganti jadi NDS Dropdown
            'sar_reportIndicator.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
            'StoreReportIndicator.Reload()
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If

            If bsettingRight = 1 Then
                'GridPanelReportIndicator.ColumnModel.Columns.Add(CommandColumn87)
            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#End Region

#Region "Screening by Upload"
    Protected Sub btn_UploadScreeningRequest_Click()
        Try
            Window_ScreeningUpload.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub btn_DownloadTemplate_Click()
        Try

            'Create Directory if not exists
            If Not Directory.Exists(Server.MapPath(PATH_TEMP_DIR)) Then
                Directory.CreateDirectory(Server.MapPath(PATH_TEMP_DIR))
            End If

            'Filename and location for output template
            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            Dim objfileinfo = New FileInfo(Server.MapPath(PATH_TEMP_DIR & tempfilexls))

            'Object to generate excel template
            Dim objScreeningRequest As Data.DataTable = New Data.DataTable()
            objScreeningRequest.Columns.Add(New Data.DataColumn("Name", GetType(String)))
            objScreeningRequest.Columns.Add(New Data.DataColumn("Date of Birth", GetType(Date)))

            '21-Jul-2021 Adi : Penambahan Screening Birth Place dan Identity Number
            objScreeningRequest.Columns.Add(New Data.DataColumn("Birth Place", GetType(String)))
            objScreeningRequest.Columns.Add(New Data.DataColumn("Identity Number", GetType(String)))
            '4 Nov 21 Ari hide gak ada id ppatk
            'objScreeningRequest.Columns.Add(New Data.DataColumn("ID Watchlist PPATK", GetType(Integer)))
            'End of 21-Jul-2021 Adi : Penambahan Screening Birth Place dan Identity Number


            'Generate excel template
            Using resource As New ExcelPackage(objfileinfo)
                'Membuath work sheet
                '4 Nov 21 Ganti Worksheet Name jadi Customer Prospect
                Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("ScreeningRequestCustProspect")

                'Mengisi row 1 dengan header template
                ws.Cells("A1").LoadFromDataTable(objScreeningRequest, True)

                Dim rng As ExcelRange
                Dim comment As ExcelComment
                Dim strComment As String

                'Kolom A1 Name Mandatory
                rng = ws.Cells("A1")
                'rng.Value = "Everyday Be Coding"
                strComment = "NAWADATA: " & (vbCrLf) & "[MANDATORY] Diisi dengan Nama yang ingin di screening."
                comment = rng.AddComment(strComment, "NawaData")
                comment.AutoFit = True

                'Kolom B1 Datetime format dd-MMM-yyyy
                rng = ws.Cells("B1")
                strComment = "NAWADATA: " & (vbCrLf) & "[OPTIONAL] Data Type : Date." & (vbCrLf) & "Format : dd-MMM-yyyy."
                comment = rng.AddComment(strComment, "NawaData")
                comment.AutoFit = True

                'Kolom B1 set cell format to dd-MMM-yyyy
                rng = ws.Cells("B1:B" & (Int16.MaxValue) - 1)
                rng.Style.Numberformat.Format = "dd-MMM-yyyy"

                '21-Jul-2021 Adi : Penambahan Screening Birth Place dan Identity Number
                'Kolom C1 Birth Place Optional
                rng = ws.Cells("C1")
                strComment = "NAWADATA: " & (vbCrLf) & "[OPTIONAL] Diisi dengan Birth Place yang ingin di screening."
                comment = rng.AddComment(strComment, "NawaData")
                comment.AutoFit = True

                'Kolom D1 Identity Number Optional
                rng = ws.Cells("D1")
                strComment = "NAWADATA: " & (vbCrLf) & "[OPTIONAL] Diisi dengan Identity Number yang ingin di screening."
                comment = rng.AddComment(strComment, "NawaData")
                comment.AutoFit = True

                'Kolom D1 set cell format to Text
                rng = ws.Cells("D1:D" & (Int16.MaxValue) - 1)
                rng.Style.Numberformat.Format = "@"

                '4 Nov 21 ID Ppatk di hide
                ''Kolom E1 ID Watchlist PPATK Optional
                'rng = ws.Cells("E1")
                'strComment = "NAWADATA: " & (vbCrLf) & "[OPTIONAL/MANDAROTY] Diisi dengan ID Watchlist PPATK (Mandatory jika akan dilaporkan sebagai SIPENDAR PENGAYAAN)."
                'comment = rng.AddComment(strComment, "NawaData")
                'comment.AutoFit = True
                ''End of 21-Jul-2021 Adi : Penambahan Screening Birth Place dan Identity Number


                'Background and Font Color
                rng = ws.Cells("A1:D1")
                rng.Style.Font.Color.SetColor(Color.White)
                rng.Style.Font.Bold = True
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid
                rng.Style.Fill.BackgroundColor.SetColor(Color.Blue)

                ws.Cells(ws.Dimension.Address).AutoFitColumns()
                resource.Save()
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("content-disposition", "attachment;filename=" & "SipendarScreeningCustomerProspectRequest.xlsx")
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                Response.End()
            End Using

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_ScreeningUpload_Cancel_Click()
        Try
            Window_ScreeningUpload.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_ScreeningUpload_Save_Click()
        Try
            'Your code to upload data
            'Validasi jika belum ada file yang diupload
            If Not fileTemplateScreening.HasFile Then
                Throw New ApplicationException("Belum ada file yang diupload.")
            End If

            'Validasi hanya terima xlsx
            If Not fileTemplateScreening.FileName.EndsWith(".xlsx") Then
                Throw New ApplicationException("Format yang valid hanya .xlsx.")
            End If

            'Import data from excel template
            Using objdb As New NawaDAL.NawaDataEntities
                If fileTemplateScreening.HasFile Then
                    'Directory file fisik yang di-write di server
                    Dim strFilePath As String = Server.MapPath(PATH_TEMP_DIR)
                    Dim strFileName As String = Guid.NewGuid.ToString & ".xlsx"

                    'Write file uploaded to server
                    System.IO.File.WriteAllBytes(strFilePath & strFileName, fileTemplateScreening.FileBytes)

                    Dim br As BinaryReader
                    Dim bData As Byte()
                    br = New BinaryReader(System.IO.File.OpenRead(strFilePath & strFileName))
                    bData = br.ReadBytes(br.BaseStream.Length)
                    Dim ms As MemoryStream = New MemoryStream(bData, 0, bData.Length)
                    ms.Write(bData, 0, bData.Length)

                    Using excelFile As New ExcelPackage(ms)
                        If excelFile.Workbook.Worksheets("ScreeningRequestCustProspect") IsNot Nothing Then
                            ReadSheetScreening(excelFile.Workbook.Worksheets("ScreeningRequestCustProspect"))
                        End If
                    End Using

                    ms.Flush()
                    ms.Close()
                End If

                'Bind to Grid before Screening
                BindScreeningRequest()

            End Using

            Window_ScreeningUpload.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ReadSheetScreening(ByVal sheet As ExcelWorksheet)
        Try
            Dim PK_ID As Long = 0
            If listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Count > 0 Then
                PK_ID = listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Min(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID) - 1
            End If
            If PK_ID >= 0 Then
                PK_ID = -1
            End If

            Dim IRow As Integer = 2

            'Dim IDPPATKNotMatch As String = ""
            'While IRow <> 0
            '    Dim strName As String = ""
            '    strName = sheet.Cells(IRow, 1).Text.Trim
            '    If Not String.IsNullOrEmpty(strName) Then
            '        If Not String.IsNullOrEmpty(sheet.Cells(IRow, 5).Text.Trim) Then
            '            If GetDataWatchlistByIDPPATK(sheet.Cells(IRow, 5).Text.Trim) Is Nothing Then
            '                If IRow = 2 Then
            '                    IDPPATKNotMatch = sheet.Cells(IRow, 5).Text.Trim
            '                Else
            '                    IDPPATKNotMatch = IDPPATKNotMatch & ", " & sheet.Cells(IRow, 5).Text.Trim
            '                End If
            '            End If
            '        End If
            '        IRow = IRow + 1
            '    Else
            '        IRow = 0
            '    End If
            'End While

            'If Not String.IsNullOrEmpty(IDPPATKNotMatch) Then
            '    Throw New ApplicationException("Couldn't Find ID PPATK (" & IDPPATKNotMatch & ") on SIPENDAR WachList")
            'End If

            IRow = 2

            While IRow <> 0
                Dim strName As String = ""

                'Gunakan Name Sebagai Key... 
                'Cari Name, jika kosong diasumsikan bahwa itu row terakhir
                strName = sheet.Cells(IRow, 1).Text.Trim
                If Not String.IsNullOrEmpty(strName) Then


                    Dim ObjData As New SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL
                    With ObjData
                        .PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID = PK_ID
                        If Not String.IsNullOrEmpty(sheet.Cells(IRow, 1).Text.Trim) Then
                            .Nama = sheet.Cells(IRow, 1).Text.Trim
                        End If
                        If Not String.IsNullOrEmpty(sheet.Cells(IRow, 2).Text.Trim) Then
                            .DOB = sheet.Cells(IRow, 2).Text.Trim
                        End If

                        '21-Jul-2021 Adi : Penambahan Screening Birth Place dan Identity Number
                        If Not String.IsNullOrEmpty(sheet.Cells(IRow, 3).Text.Trim) Then
                            .Birth_Place = sheet.Cells(IRow, 3).Text.Trim
                        End If
                        If Not String.IsNullOrEmpty(sheet.Cells(IRow, 4).Text.Trim) Then
                            .Identity_Number = sheet.Cells(IRow, 4).Text.Trim
                        End If
                        '4 Nov 21 Ari : Hide ID PPATK
                        'If Not String.IsNullOrEmpty(sheet.Cells(IRow, 5).Text.Trim) Then
                        '    .FK_WATCHLIST_ID_PPATK = sheet.Cells(IRow, 5).Text.Trim
                        'End If
                        'End of 21-Jul-2021 Adi : Penambahan Screening Birth Place dan Identity Number

                        PK_ID = PK_ID - 1
                    End With

                    listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Add(ObjData)
                    IRow = IRow + 1
                Else
                    IRow = 0
                End If
            End While

            Dim str As String = ""

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindScreeningRequest()
        Try
            'Convert object to Datatable
            Dim dt As DataTable = NawaBLL.Common.CopyGenericToDataTable(listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.ToList)

            'Bind to Grid
            gp_ScreeningRequest.GetStore.DataSource = dt
            gp_ScreeningRequest.GetStore.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Screening Request Satuan"

    Protected Sub CleanFormScreeningRequest()
        Try
            txt_Nama.Value = Nothing
            txt_DOB.Value = Nothing

            '21-Jul-2021 Adi
            txt_Birth_Place.Value = Nothing
            txt_Identity_Number.Value = Nothing
            'txt_FK_WATCHLIST_ID_PPATK.Value = Nothing

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_AddScreeningRequest_Click()
        Try
            objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_Edit = Nothing

            CleanFormScreeningRequest()
            Window_ScreeningAdd.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_ScreeningAdd_Save_Click()
        Try
            'Validate Data
            If String.IsNullOrEmpty(txt_Nama.Value) Then
                Throw New ApplicationException("Name is required!")
            End If

            'If Not (String.IsNullOrEmpty(txt_FK_WATCHLIST_ID_PPATK.Text) Or txt_FK_WATCHLIST_ID_PPATK.Text = "N/A" Or txt_FK_WATCHLIST_ID_PPATK.Text = "NA") Then
            '    If GetDataWatchlistByIDPPATK(txt_FK_WATCHLIST_ID_PPATK.Text) Is Nothing Then
            '        Throw New ApplicationException("Couldn't Find ID PPATK on SIPENDAR WachList")
            '    End If
            'End If
            'Your code to save to grid panel here
            Dim PK_ID As Long = 0

            If objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_Edit Is Nothing Then    'Add New
                Dim objNew = New SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL

                If listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Count > 0 Then
                    PK_ID = listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Min(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID) - 1
                End If
                If PK_ID >= 0 Then
                    PK_ID = -1
                End If

                With objNew
                    .PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID = PK_ID
                    .Nama = txt_Nama.Value.ToString().Trim()
                    If CDate(txt_DOB.Value) <> DateTime.MinValue Then
                        .DOB = txt_DOB.Value
                    End If

                    '21-Jul-2021 Adi
                    .Birth_Place = txt_Birth_Place.Value
                    .Identity_Number = txt_Identity_Number.Value

                    'If Not (String.IsNullOrEmpty(txt_FK_WATCHLIST_ID_PPATK.Text) Or txt_FK_WATCHLIST_ID_PPATK.Text = "N/A" Or txt_FK_WATCHLIST_ID_PPATK.Text = "NA") Then
                    '    .FK_WATCHLIST_ID_PPATK = txt_FK_WATCHLIST_ID_PPATK.Value
                    'End If

                End With

                listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Add(objNew)
            Else    'Update Existing
                PK_ID = objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_Edit.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID
                Dim objUpdate = listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Where(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID = PK_ID).FirstOrDefault
                If objUpdate IsNot Nothing Then
                    With objUpdate
                        .Nama = txt_Nama.Value.ToString().Trim()
                        If CDate(txt_DOB.Value) <> DateTime.MinValue Then
                            .DOB = txt_DOB.Value
                        End If

                        '21-Jul-2021 Adi
                        .Birth_Place = txt_Birth_Place.Value
                        .Identity_Number = txt_Identity_Number.Value

                        'If Not (String.IsNullOrEmpty(txt_FK_WATCHLIST_ID_PPATK.Text) Or txt_FK_WATCHLIST_ID_PPATK.Text = "N/A" Or txt_FK_WATCHLIST_ID_PPATK.Text = "NA") Then
                        '    .FK_WATCHLIST_ID_PPATK = txt_FK_WATCHLIST_ID_PPATK.Value
                        'End If

                    End With
                End If
            End If

            'Bind to Gridpanel
            BindScreeningRequest()

            Window_ScreeningAdd.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_ScreeningAdd_Cancel_Click()
        Try
            Window_ScreeningAdd.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_ScreeningRequest(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Edit" Then
                'Clear Form 
                CleanFormScreeningRequest()

                'Get the data object to edit
                objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_Edit = listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Where(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID = strID).FirstOrDefault

                'Fill in the form
                If objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_Edit IsNot Nothing Then
                    With objSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_Edit
                        txt_Nama.Value = .Nama
                        txt_DOB.Value = .DOB

                        '21-Jul-2021 Adi
                        txt_Birth_Place.Value = .Birth_Place
                        txt_Identity_Number.Value = .Identity_Number
                        'txt_FK_WATCHLIST_ID_PPATK.Value = .FK_WATCHLIST_ID_PPATK
                    End With
                End If

                'Show the window
                Window_ScreeningAdd.Hidden = False
            ElseIf strAction = "Delete" Then
                'Get the data object to delete
                Dim objToDelete = listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Find(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID = strID)

                'Delete the data
                If objToDelete IsNot Nothing Then
                    listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Remove(objToDelete)
                End If

                'Binding to gridpanel
                BindScreeningRequest()
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Start Screening"
    Private Const Interval = 10 ' In seconds (ie: 10 seconds)
    Private mdtmTarget As Date

    Protected Sub btn_Screening_Click()
        Try
            'Hide Grid Screening Result
            sar_IsSelectedAll_Result.Hidden = True
            gp_ScreeningResult.Hidden = True
            gp_ScreeningResult_All.Hidden = True
            PanelScreeningResult.Hidden = True
            df_ScreeningStart.Text = ""
            df_ScreeningStatus.Text = ""
            df_ScreeningFinished.Text = ""

            'Validate data to screening
            If listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL Is Nothing OrElse listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Count <= 0 Then
                Throw New ApplicationException("Add/Upload data to Screening Request to continue.")
            End If

            'Save to Database
            PK_Request_ID = SaveScreeningRequest()

            'Save to EOD Scheduler and get the PK_EODTaskDetailLog_ID for check the status
            Dim objParamRequest(1) As SqlParameter
            objParamRequest(0) = New SqlParameter
            objParamRequest(0).ParameterName = "@PK_Request_ID"
            objParamRequest(0).Value = PK_Request_ID

            objParamRequest(1) = New SqlParameter
            objParamRequest(1).ParameterName = "@User_ID"
            objParamRequest(1).Value = NawaBLL.Common.SessionCurrentUser.UserID

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_SaveEOD", objParamRequest) '' Edited on 27 Aug 2021

            'Show status
            'df_ScreeningStatus.Hidden = False
            'df_ScreeningStatus.Text = "Screening is in progress. Please wait..."

            'Show Confirmation
            'LblConfirmation.Text = "Screening Request has been saved to Database and will be processed by Scheduler."
            'FormPanelInput.Hidden = True
            'Panelconfirmation.Hidden = False

            'Check status periodically
            btn_Screening.Hidden = True
            'btn_Save.Hidden = True
            df_ScreeningStart.Text = DateTime.Now.ToString("dd-MMM-yyyy, HH:mm:ss")
            panel_ScreeningStatus.Hidden = False
            Dim cfg As New MaskConfig
            cfg.Msg = "Screening in Progress..."
            cfg.El = "Ext.getBody()"
            Ext.Net.X.Mask.Show(cfg)
            TaskManager1.StartTask("refreshScreeningStatus")


        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Confirmation_Click()
        Try
            'Clean Screening Request
            'btn_Cancel_Click()
            Clear_Search()

            'Hide Panel Confirmation
            Panelconfirmation.Hidden = True
            FormPanelInput.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Function SaveScreeningRequest() As Long

        PK_Request_ID = Nothing

        Using objDb As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDb.Database.BeginTransaction()
                Try
                    'Object Screening Request
                    Dim objRequest = New SiPendarDAL.SIPENDAR_Screening_Customer_Prospect_Request
                    With objRequest
                        .CreatedDate = DateTime.Now()
                        .UserIDRequest = NawaBLL.Common.SessionCurrentUser.UserID
                        .IsAlreadyScreening = False
                    End With

                    'Save to database
                    objDb.Entry(objRequest).State = Entity.EntityState.Added
                    objDb.SaveChanges()

                    'Save detail request
                    For Each item In listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL
                        item.FK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID = objRequest.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID

                        objDb.Entry(item).State = Entity.EntityState.Added
                        objDb.SaveChanges()
                    Next

                    'Commit Transaction
                    objTrans.Commit()

                    'Return the PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID
                    PK_Request_ID = objRequest.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID

                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using

        Return PK_Request_ID

    End Function

    Protected Sub Screening_CheckStatus()
        Dim strStatus As String = ""
        strStatus = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT MsEODStatusName FROM vw_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_RESULT WHERE PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID = " & PK_Request_ID, Nothing)
        df_ScreeningStatus.Text = strStatus

        If Not (strStatus = "In Progress" Or strStatus = "OnQueue") Then
            TaskManager1.StopTask("refreshScreeningStatus")
            Ext.Net.X.Mask.Hide()
            df_ScreeningFinished.Text = DateTime.Now.ToString("dd-MMM-yyyy, HH:mm:ss")

            '' Added by Felix 15-Sep-2021
            'Get Total based on Screening Request
            Dim tblTotal As DataTable

            'Dim totalScreening As Long
            'Dim totalFound As Long
            'Dim totalNotFound As Long

            Dim objCountTotalScreening(0) As SqlParameter
            objCountTotalScreening(0) = New SqlParameter
            objCountTotalScreening(0).ParameterName = "@PK_Request_ID"
            objCountTotalScreening(0).Value = PK_Request_ID

            tblTotal = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_CountScreeningTotal_ByRequestID", objCountTotalScreening)

            For Each row As DataRow In tblTotal.Rows
                df_TotalScreening.Text = row.Item("TotalScreening")
                df_TotalFound.Text = row.Item("TotalFound")
                df_TotalNotFound.Text = row.Item("TotalNotFound")
            Next

            '' End of 15-Sep-2021

            'Show Screening Result
            Show_ScreeningResult()

            'Show Button Save
            'pnlSiPendarReport.Hidden = False
            'btn_Save.Hidden = False
        End If
    End Sub

    Protected Sub Show_ScreeningResult()
        Try
            Dim dtResult As DataTable = New DataTable
            dtResult = GetDataTabelSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT(PK_Request_ID)
            'dtResult = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT WHERE PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID=" & PK_Request_ID & " ORDER BY TotalSimilarityPct Desc, GCN, Nama")
            gp_ScreeningResult.GetStore.DataSource = dtResult
            gp_ScreeningResult.GetStore.DataBind()
            TotalScreeningResult = dtResult.Rows.Count 'Ari 16-09-2021 menghitung total screening result

            '24Sept2021 Daniel bagian ini tidak diperlukan karena query ny sama saja
            'Dim dtResultAll As DataTable = New DataTable
            'dtResultAll = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT WHERE PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID=" & PK_Request_ID & " ORDER BY TotalSimilarityPct Desc, GCN, Nama")
            '24Sept2021 Daniel
            gp_ScreeningResult_All.GetStore.DataSource = dtResult
            gp_ScreeningResult_All.GetStore.DataBind()

            sar_IsSelectedAll_Result.Hidden = False

            gp_ScreeningRequest.Hidden = True
            gp_ScreeningResult.Hidden = False
            cboExportExcel.Value = "Excel"
            PanelScreeningResult.Hidden = False
            'Ari 15-09-2021 Menambahkan fungsi untuk bind data screening all result
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

    '#Region "Report to SiPendar"
    '    Protected Sub Btn_Save_Click(sender As Object, e As Ext.Net.DirectEventArgs)
    '        Try
    '            'Ari 15-09-2021 cek apakah combo box untuk semua data di centang
    '            If sar_IsSelectedAll_Result.Checked Then
    '                'Cek minimal 1 profile hasil screening dipilih

    '                Dim result As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT WHERE PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID=" & PK_Request_ID & " ORDER BY TotalSimilarityPct Desc, GCN, Nama", Nothing)
    '                Sipendar_Screening_Result_AllChecked = result
    '                'Validasi kelengkapan data untuk disimpan sebagai SiPendar Report
    '                'ValidateSiPendarReport()

    '                'Set flag IsNeetToReportSIPENDAR
    '                Using objDb As New SiPendarDAL.SiPendarEntities
    '                    Using objTrans As System.Data.Entity.DbContextTransaction = objDb.Database.BeginTransaction()
    '                        Try
    '                            For Each row As DataRow In Sipendar_Screening_Result_AllChecked.Rows
    '                                Dim recordID As String = row.Item("PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID").ToString().ToString

    '                                Dim objScreeningResult = objDb.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID = recordID).FirstOrDefault
    '                                If Not objScreeningResult Is Nothing Then
    '                                    With objScreeningResult
    '                                        .IsNeedToReportSIPENDAR = True
    '                                        objDb.Entry(objScreeningResult).State = Entity.EntityState.Modified
    '                                        objDb.SaveChanges()
    '                                    End With
    '                                End If
    '                            Next

    '                            objTrans.Commit()
    '                        Catch ex As Exception
    '                            objTrans.Rollback()
    '                        End Try
    '                    End Using
    '                End Using
    '            Else
    '                'Cek minimal 1 profile hasil screening dipilih
    '                Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)
    '                If smScreeningResult.SelectedRows.Count = 0 Then
    '                    Throw New Exception("Minimal 1 Profile Hasil Screening harus dipilih!")
    '                End If
    '                'Validasi kelengkapan data untuk disimpan sebagai SiPendar Report
    '                'ValidateSiPendarReport()

    '                'Set flag IsNeetToReportSIPENDAR
    '                Using objDb As New SiPendarDAL.SiPendarEntities
    '                    Using objTrans As System.Data.Entity.DbContextTransaction = objDb.Database.BeginTransaction()
    '                        Try
    '                            For Each item As SelectedRow In smScreeningResult.SelectedRows
    '                                Dim recordID = item.RecordID.ToString
    '                                Dim objScreeningResult = objDb.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = recordID).FirstOrDefault
    '                                If Not objScreeningResult Is Nothing Then
    '                                    With objScreeningResult
    '                                        .IsNeedToReportSIPENDAR = True
    '                                        objDb.Entry(objScreeningResult).State = Entity.EntityState.Modified
    '                                        objDb.SaveChanges()
    '                                    End With
    '                                End If
    '                            Next

    '                            objTrans.Commit()
    '                        Catch ex As Exception
    '                            objTrans.Rollback()
    '                        End Try
    '                    End Using
    '                End Using
    '            End If





    '            ''Populate Data to save to SiPendar Report
    '            'SaveDataSiPendar()

    '            ''Save ke Other Info
    '            'SaveScreeningResultOtherInfo()


    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    'Protected Sub ValidateSiPendarReport()
    '    '    Try
    '    '        'Validate Header
    '    '        If String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
    '    '            Throw New ApplicationException(cmb_SIPENDAR_TYPE_ID.Label & " harus diisi.")
    '    '        End If
    '    '        If String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue) Then
    '    '            Throw New ApplicationException(cmb_SUBMISSION_TYPE_CODE.Label & " harus diisi.")
    '    '        End If

    '    '        If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then      'SIPENDAR Proaktif
    '    '            If String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue) Then
    '    '                Throw New ApplicationException(cmb_JENIS_WATCHLIST_CODE.Label & " harus diisi.")
    '    '            End If
    '    '            'If String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue) Then
    '    '            '    Throw New ApplicationException(cmb_TINDAK_PIDANA_CODE.Label & " harus diisi.")
    '    '            'End If
    '    '            If String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue) Then
    '    '                Throw New ApplicationException(cmb_SUMBER_INFORMASI_KHUSUS_CODE.Label & " harus diisi.")
    '    '            End If
    '    '        Else    'SIPENDAR Pengayaan
    '    '            If String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue) Then
    '    '                Throw New ApplicationException(cmb_SUMBER_TYPE_CODE.Label & " harus diisi.")
    '    '            End If

    '    '            '21-Jul-2021 Adi : dicomment karena sudah ambil langsung dari ID Watchlist PPATK
    '    '            'If String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A" Or txt_SUMBER_ID.Text = "NA" Then
    '    '            '    Throw New ApplicationException(txt_SUMBER_ID.FieldLabel & " harus diisi.")
    '    '            'Else
    '    '            '    If CLng(txt_SUMBER_ID.Value) < 1 Or CLng(txt_SUMBER_ID.Value) > 2147483646 Then
    '    '            '        Throw New ApplicationException(txt_SUMBER_ID.FieldLabel & " harus diisi >= 1 dan <= 2147483646.")
    '    '            '    End If
    '    '            'End If

    '    '            If chk_IsGenerateTransaction.Checked Then
    '    '                If CDate(txt_Transaction_DateFrom.Value) = DateTime.MinValue Or CDate(txt_Transaction_DateTo.Value) = DateTime.MinValue Then
    '    '                    Throw New ApplicationException("Tanggal transaksi From dan To harus diisi. Jika pilihan Generate Transaction dicentang.")
    '    '                End If
    '    '                If CDate(txt_Transaction_DateFrom.Value) <> DateTime.MinValue And CDate(txt_Transaction_DateTo.Value) <> DateTime.MinValue Then
    '    '                    If CDate(txt_Transaction_DateFrom.Value) > CDate(txt_Transaction_DateTo.Value) Then
    '    '                        Throw New ApplicationException("Tanggal transaksi From harus < Tanggal transaksi To.")
    '    '                    End If
    '    '                End If
    '    '                If ListIndicator.Count = 0 Then
    '    '                    Throw New ApplicationException(" Minimal isi 1 indikator. Jika pilihan Generate Transaction dicentang.")
    '    '                End If
    '    '            End If
    '    '        End If



    '    '        'If String.IsNullOrEmpty(txt_Keterangan.Value) Then
    '    '        '    Throw New ApplicationException(txt_Keterangan.FieldLabel & " harus diisi.")
    '    '        'End If

    '    '    Catch ex As Exception
    '    '        Throw ex
    '    '    End Try
    '    'End Sub

    '    'Protected Sub SaveDataSiPendar()
    '    '    Try
    '    '        'Ari 15-09-2021 jika customer pilih semua data di grid buat kondisinya
    '    '        If sar_IsSelectedAll_Result.Checked Then
    '    '            'Save by looping selected item
    '    '            Dim smScreeningResult As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT WHERE PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID=" & PK_Request_ID & " ORDER BY GCN ", Nothing)

    '    '            '            '' Felix 27 Jul 2021
    '    '            Dim objGlobalParameterPerType As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing

    '    '            'Cek dulu apakah sudah pernah dilapor pada hari yang sama dan GCN yang sama
    '    '            Dim strMessage As String = ""
    '    '            For Each row As DataRow In smScreeningResult.Rows

    '    '                If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 AndAlso Not IsDBNull(row("Stakeholder_Role")) Then
    '    '                    Throw New Exception("Non Customer hanya boleh dilaporkan sebagai SIPENDAR PROAKTIF")
    '    '                End If

    '    '                Dim recordID As String = row.Item("PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID").ToString

    '    '                Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT = Nothing
    '    '                Using objDbSipendar As New SiPendarDAL.SiPendarEntities
    '    '                    objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = recordID).FirstOrDefault
    '    '                    If objScreeningResult IsNot Nothing Then
    '    '                        'Cek apakah GCN kosong
    '    '                        If objScreeningResult.GCN Is Nothing OrElse objScreeningResult.GCN = "" Then
    '    '                            Throw New Exception("GCN tidak boleh kosong untuk bisa dilaporkan sebagai SIPENDAR")
    '    '                        End If

    '    '                        'Cek apakah ID Watchlist PPATK kosong (khusus pengayaan)
    '    '                        If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 AndAlso (objScreeningResult.FK_WATCHLIST_ID_PPATK Is Nothing OrElse objScreeningResult.FK_WATCHLIST_ID_PPATK = "") Then
    '    '                            Throw New Exception("ID Watchlist PPATK tidak boleh kosong untuk bisa dilaporkan sebagai SIPENDAR PENGAYAAN")
    '    '                        End If


    '    '                        'Cek apakah sudah pernah dilaporkan pada hari yang sama
    '    '                        'Dim objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = objScreeningResult.GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue).FirstOrDefault
    '    '                        ' Edited by Felix 27 Jul 2021

    '    '                        objGlobalParameterPerType = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 7).FirstOrDefault

    '    '                        Dim objprofile As New SiPendarDAL.SIPENDAR_PROFILE
    '    '                        If objGlobalParameterPerType.ParameterValue = 1 Then
    '    '                            objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = objScreeningResult.GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue).FirstOrDefault
    '    '                        Else
    '    '                            objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = objScreeningResult.GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault
    '    '                        End If
    '    '                        'End of 27 Jul 2021

    '    '                        If objprofile IsNot Nothing Then
    '    '                            strMessage += "<br>GCN : " & objScreeningResult.GCN & " - " & objScreeningResult.NAMAWATCHLIST & " sudah pernah dilaporkan pada hari ini."
    '    '                        End If

    '    '                        'Cek juga apakah ada transaksi di range tanggal yang diinput untuk GCN-GCN yang dipilih
    '    '                        If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Then
    '    '                            If chk_IsGenerateTransaction.Checked Then
    '    '                                Dim dtFrom As Date = CDate(txt_Transaction_DateFrom.Value)
    '    '                                Dim dtTo As Date = CDate(txt_Transaction_DateTo.Value)

    '    '                                Dim objList = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch_GCN(objScreeningResult.GCN, dtFrom, dtTo)
    '    '                                If objList Is Nothing OrElse objList.Count <= 0 Then
    '    '                                    strMessage += "<br>GCN : " & objScreeningResult.GCN & " - " & objScreeningResult.NAMAWATCHLIST & " tidak memiliki data transaksi di range tanggal tersebut."
    '    '                                End If

    '    '                                '' Added by Felix 06 Aug 2021
    '    '                                Dim CheckLawanKosong As Long = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER where PK_GlobalReportParameter_ID = 8", Nothing) '' Added by Felix on 30 Aug 2021
    '    '                                If CheckLawanKosong = 1 Then
    '    '                                    Dim objCheckLawan = SiPendarBLL.SIPENDARGenerateBLL.CheckCounterPartyExistForTransaction(objScreeningResult.GCN, dtFrom, dtTo)
    '    '                                    If objCheckLawan.Count > 0 Then
    '    '                                        strMessage += "<br>GCN : " & objScreeningResult.GCN & " - " & objScreeningResult.NAMAWATCHLIST & " memiliki transaksi yang tidak mempunyai lawan untuk periode yang dipilih."
    '    '                                    End If
    '    '                                    '' End of Felix 06 Aug 2021
    '    '                                End If '' End of 30 Aug 2021

    '    '                                Dim objCek = objDbSipendar.SIPENDAR_Report.Where(Function(x) x.UnikReference = objScreeningResult.GCN And DbFunctions.TruncateTime(x.Submission_Date) = DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault
    '    '                                If objCek IsNot Nothing Then
    '    '                                    Throw New ApplicationException("Tidak bisa generate Report Transaction. Customer ini sudah punya Report Transaction di hari ini.")
    '    '                                End If

    '    '                            End If

    '    '                        End If

    '    '                    End If
    '    '                End Using
    '    '            Next
    '    '            If Not String.IsNullOrEmpty(strMessage) Then
    '    '                Throw New ApplicationException(strMessage)
    '    '            End If

    '    '            '6-Jul-2021 Adi Get objModule
    '    '            Dim objProfileModule As NawaDAL.Module = Nothing
    '    '            If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
    '    '                objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PROAKTIF")
    '    '            Else
    '    '                objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PENGAYAAN")
    '    '            End If

    '    '            'Save to Database (With/Without Approval)
    '    '            Dim previousGCN As String = ""
    '    '            For Each row As Data.DataRow In smScreeningResult.Rows
    '    '                Dim recordID As String = row.Item("PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID").ToString

    '    '                Dim temprole As String = ""

    '    '                If Not IsDBNull(row("Stakeholder_Role")) Then
    '    '                    temprole = Convert.ToString(row("Stakeholder_Role"))
    '    '                End If

    '    '                'Buat object baru untuk list SIPENDAR_PROFILE
    '    '                Dim objSIPENDAR_PROFILE_CLASS As New SiPendarBLL.SIPENDAR_PROFILE_CLASS

    '    '                'Ambil data2 dari Sipendar Entity
    '    '                Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT = Nothing
    '    '                Dim objGlobalParameter As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
    '    '                Using objDbSipendar As New SiPendarDAL.SiPendarEntities
    '    '                    objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = recordID).FirstOrDefault
    '    '                    objGlobalParameter = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault
    '    '                End Using

    '    '                'Skip jika ada 2 GCN yang sama dipilih
    '    '                If objScreeningResult.GCN = previousGCN Then
    '    '                    Continue For
    '    '                End If

    '    '                'Ambil data2 dari NawadataDevEntities
    '    '                Dim objCustomer As SiPendarDAL.goAML_Ref_Customer = Nothing
    '    '                Dim listCustomer As New List(Of SiPendarDAL.goAML_Ref_Customer)      '1 GCN bisa punya banyak CIF
    '    '                'Dim listAccount As New List(Of SiPendarDAL.goAML_Ref_Account)
    '    '                Dim listAddress As New List(Of SiPendarDAL.goAML_Ref_Address)
    '    '                Dim listPhone As New List(Of SiPendarDAL.goAML_Ref_Phone)
    '    '                Dim listIdentification As New List(Of SiPendarDAL.goAML_Person_Identification)

    '    '                Dim listProfileAccount As New List(Of SiPendarBLL.ProfileAccountClass) '' Added 30 Jul 2021

    '    '                If String.IsNullOrEmpty(temprole) Then
    '    '                    Using objDbNawa As New SiPendarDAL.SiPendarEntities
    '    '                        'Get Data Customer
    '    '                        If Not objScreeningResult Is Nothing Then
    '    '                            objCustomer = objDbNawa.goAML_Ref_Customer.Where(Function(x) x.GCN = objScreeningResult.GCN).FirstOrDefault
    '    '                            listCustomer = objDbNawa.goAML_Ref_Customer.Where(Function(x) x.GCN = objScreeningResult.GCN).ToList
    '    '                        End If

    '    '                        'Get Account, Address, Phone, Identification
    '    '                        If listCustomer IsNot Nothing AndAlso listCustomer.Count > 0 Then
    '    '                            For Each itemCustomer In listCustomer
    '    '                                Dim tempListAccount = objDbNawa.goAML_Ref_Account.Where(Function(x) x.client_number = itemCustomer.CIF).ToList
    '    '                                Dim tempListAddress = objDbNawa.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = itemCustomer.PK_Customer_ID).ToList
    '    '                                Dim tempListPhone = objDbNawa.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = itemCustomer.PK_Customer_ID).ToList
    '    '                                Dim tempListIdentification = objDbNawa.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = itemCustomer.PK_Customer_ID).ToList

    '    '                                'List Account
    '    '                                If tempListAccount IsNot Nothing AndAlso tempListAccount.Count > 0 Then
    '    '                                    For Each itemAccount In tempListAccount

    '    '                                        Dim tempprofileaccount As New ProfileAccountClass
    '    '                                        tempprofileaccount.ObjAccount = itemAccount
    '    '                                        If itemAccount.Account_No IsNot Nothing Then
    '    '                                            tempprofileaccount.ListATMAccount = objDbNawa.AML_ACCOUNT_ATM.Where(Function(x) x.Account_No = itemAccount.Account_No).ToList
    '    '                                        End If

    '    '                                        'listAccount.Add(itemAccount)
    '    '                                        listProfileAccount.Add(tempprofileaccount)
    '    '                                    Next
    '    '                                End If

    '    '                                'List Address
    '    '                                If tempListAddress IsNot Nothing AndAlso tempListAddress.Count > 0 Then
    '    '                                    For Each itemaddress In tempListAddress
    '    '                                        listAddress.Add(itemaddress)
    '    '                                    Next
    '    '                                End If

    '    '                                'List Phone
    '    '                                If tempListPhone IsNot Nothing AndAlso tempListPhone.Count > 0 Then
    '    '                                    For Each itemphone In tempListPhone
    '    '                                        listPhone.Add(itemphone)
    '    '                                    Next
    '    '                                End If

    '    '                                'List identification
    '    '                                If itemCustomer.FK_Customer_Type_ID = 1 Then    'Hanya perorangan yg punya Identification
    '    '                                    If tempListIdentification IsNot Nothing AndAlso tempListIdentification.Count > 0 Then
    '    '                                        For Each itemidentification In tempListIdentification
    '    '                                            listIdentification.Add(itemidentification)
    '    '                                        Next
    '    '                                    End If

    '    '                                    ''Added on 26 Aug 2021
    '    '                                    If Not objCustomer Is Nothing Then
    '    '                                        If Not String.IsNullOrWhiteSpace(itemCustomer.INDV_SSN) Then
    '    '                                            Dim tempident As New SiPendarDAL.goAML_Person_Identification
    '    '                                            tempident.Number = itemCustomer.INDV_SSN
    '    '                                            tempident.Type = "KTP"
    '    '                                            tempident.Issued_Country = itemCustomer.INDV_Nationality1
    '    '                                            listIdentification.Add(tempident)
    '    '                                        End If

    '    '                                        If Not String.IsNullOrWhiteSpace(itemCustomer.INDV_Passport_Number) Then
    '    '                                            Dim tempident As New SiPendarDAL.goAML_Person_Identification
    '    '                                            tempident.Number = itemCustomer.INDV_Passport_Number
    '    '                                            tempident.Type = "PAS"
    '    '                                            tempident.Issued_Country = itemCustomer.INDV_Passport_Country
    '    '                                            listIdentification.Add(tempident)
    '    '                                        End If
    '    '                                    End If
    '    '                                    '' End of 26 Aug 2021
    '    '                                End If
    '    '                            Next
    '    '                        End If
    '    '                    End Using
    '    '                Else
    '    '                    If Not IsDBNull(row("GCN")) Then
    '    '                        Dim stringgcn As String = Convert.ToString(row("GCN"))
    '    '                        Dim drSIPENDAR_StakeHolder_NonCustomer As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)

    '    '                        If drSIPENDAR_StakeHolder_NonCustomer IsNot Nothing Then
    '    '                            If Not IsDBNull(drSIPENDAR_StakeHolder_NonCustomer("PK_SIPENDAR_StakeHolder_NonCustomer_ID")) Then

    '    '                                'SiPendarProfile Account
    '    '                                Dim PK_IDAccount As Long = -1
    '    '                                Dim objAccount As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT
    '    '                                With objAccount
    '    '                                    .PK_SIPENDAR_PROFILE_ACCOUNT_ID = PK_IDAccount
    '    '                                    Dim tempparameter As New SIPENDAR_GLOBAL_PARAMETER

    '    '                                    tempparameter = GetDataSIPENDAR_GLOBAL_PARAMETER(104)
    '    '                                    If tempparameter IsNot Nothing Then
    '    '                                        .CIFNO = tempparameter.ParameterValue
    '    '                                    End If

    '    '                                    tempparameter = GetDataSIPENDAR_GLOBAL_PARAMETER(105)
    '    '                                    If tempparameter IsNot Nothing Then
    '    '                                        .NOREKENING = tempparameter.ParameterValue
    '    '                                    End If

    '    '                                    tempparameter = GetDataSIPENDAR_GLOBAL_PARAMETER(106)
    '    '                                    If tempparameter IsNot Nothing Then
    '    '                                        .Fk_goAML_Ref_Status_Rekening_CODE = tempparameter.ParameterValue
    '    '                                    End If

    '    '                                    tempparameter = GetDataSIPENDAR_GLOBAL_PARAMETER(107)
    '    '                                    If tempparameter IsNot Nothing Then
    '    '                                        .Fk_goAML_Ref_Jenis_Rekening_CODE = tempparameter.ParameterValue
    '    '                                    End If

    '    '                                    If objGlobalParameter IsNot Nothing Then
    '    '                                        .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
    '    '                                    End If

    '    '                                End With
    '    '                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Add(objAccount)

    '    '                                'SiPendarProfile IDENTIFICATION
    '    '                                Dim dtIdentity As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Identifications(Convert.ToString(drSIPENDAR_StakeHolder_NonCustomer("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
    '    '                                If dtIdentity IsNot Nothing AndAlso dtIdentity.Rows.Count > 0 Then
    '    '                                    Dim PK_ID As Long = 0
    '    '                                    For Each rowsIdentity As Data.DataRow In dtIdentity.Rows
    '    '                                        Dim tempidentitynumber As String = ""
    '    '                                        If Not IsDBNull(rowsIdentity("Identity_Number")) Then
    '    '                                            tempidentitynumber = Convert.ToString(rowsIdentity("Identity_Number"))
    '    '                                        End If
    '    '                                        Dim objCek = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.IDENTITY_NUMBER = tempidentitynumber)
    '    '                                        If objCek IsNot Nothing Then
    '    '                                            Continue For
    '    '                                        End If
    '    '                                        PK_ID = PK_ID - 1
    '    '                                        Dim objIDENTIFICATION As New SiPendarDAL.SIPENDAR_PROFILE_IDENTIFICATION
    '    '                                        With objIDENTIFICATION
    '    '                                            .PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = PK_ID
    '    '                                            If Not IsDBNull(rowsIdentity("Identity_Type")) Then
    '    '                                                .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = Convert.ToString(rowsIdentity("Identity_Type"))
    '    '                                            End If
    '    '                                            .IDENTITY_NUMBER = tempidentitynumber
    '    '                                            If Not IsDBNull(rowsIdentity("Issue_Date")) Then
    '    '                                                .ISSUE_DATE = rowsIdentity("Issue_Date")
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsIdentity("Expiry_Date")) Then
    '    '                                                .EXPIRED_DATE = rowsIdentity("Expiry_Date")
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsIdentity("Issued_By")) Then
    '    '                                                .ISSUED_BY = Convert.ToString(rowsIdentity("Issued_By"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsIdentity("Issued_Country")) Then
    '    '                                                .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = Convert.ToString(rowsIdentity("Issued_Country"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsIdentity("Comments")) Then
    '    '                                                .Comments = Convert.ToString(rowsIdentity("Comments"))
    '    '                                            End If
    '    '                                        End With
    '    '                                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Add(objIDENTIFICATION)
    '    '                                    Next
    '    '                                End If

    '    '                                'SiPendarProfile Address
    '    '                                Dim dtAddress As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Addresses(Convert.ToString(drSIPENDAR_StakeHolder_NonCustomer("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
    '    '                                If dtAddress IsNot Nothing AndAlso dtAddress.Rows.Count > 0 Then
    '    '                                    Dim PK_ID As Long = 0
    '    '                                    For Each rowsAddress As Data.DataRow In dtAddress.Rows
    '    '                                        PK_ID = PK_ID - 1
    '    '                                        Dim objAddress As New SiPendarDAL.SIPENDAR_PROFILE_ADDRESS
    '    '                                        With objAddress
    '    '                                            .PK_SIPENDAR_PROFILE_ADDRESS_ID = PK_ID
    '    '                                            If Not IsDBNull(rowsAddress("Address_Type")) Then
    '    '                                                .FK_goAML_Ref_Kategori_Kontak_CODE = Convert.ToString(rowsAddress("Address_Type"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsAddress("Address")) Then
    '    '                                                .ADDRESS = Convert.ToString(rowsAddress("Address"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsAddress("City")) Then
    '    '                                                .CITY = Convert.ToString(rowsAddress("City"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsAddress("Country_code")) Then
    '    '                                                .FK_goAML_Ref_Nama_Negara_CODE = Convert.ToString(rowsAddress("Country_code"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsAddress("Zip")) Then
    '    '                                                .ZIP = Convert.ToString(rowsAddress("Zip"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsAddress("Town")) Then
    '    '                                                .TOWN = Convert.ToString(rowsAddress("Town"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsAddress("State")) Then
    '    '                                                .STATE = Convert.ToString(rowsAddress("State"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsAddress("Comments")) Then
    '    '                                                .COMMENTS = Convert.ToString(rowsAddress("Comments"))
    '    '                                            End If
    '    '                                        End With
    '    '                                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Add(objAddress)
    '    '                                    Next
    '    '                                End If

    '    '                                'SiPendarProfile phone
    '    '                                Dim dtPhone As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Phones(Convert.ToString(drSIPENDAR_StakeHolder_NonCustomer("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
    '    '                                If dtPhone IsNot Nothing AndAlso dtPhone.Rows.Count > 0 Then
    '    '                                    Dim PK_ID As Long = 0
    '    '                                    For Each rowsPhone As Data.DataRow In dtPhone.Rows
    '    '                                        PK_ID = PK_ID - 1
    '    '                                        Dim objPHONE As New SiPendarDAL.SIPENDAR_PROFILE_PHONE
    '    '                                        With objPHONE
    '    '                                            .PK_SIPENDAR_PROFILE_PHONE_ID = PK_ID
    '    '                                            If Not IsDBNull(rowsPhone("Tph_Contact_Type")) Then
    '    '                                                .FK_goAML_Ref_Kategori_Kontak_CODE = Convert.ToString(rowsPhone("Tph_Contact_Type"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsPhone("Tph_Communication_Type")) Then
    '    '                                                .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = Convert.ToString(rowsPhone("Tph_Communication_Type"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsPhone("Tph_Country_Prefix")) Then
    '    '                                                .COUNTRY_PREFIX = Convert.ToString(rowsPhone("Tph_Country_Prefix"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsPhone("Tph_Number")) Then
    '    '                                                .PHONE_NUMBER = Convert.ToString(rowsPhone("Tph_Number"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsPhone("Tph_Extension")) Then
    '    '                                                .EXTENSION_NUMBER = Convert.ToString(rowsPhone("Tph_Extension"))
    '    '                                            End If
    '    '                                            If Not IsDBNull(rowsPhone("Comments")) Then
    '    '                                                .COMMENTS = Convert.ToString(rowsPhone("Comments"))
    '    '                                            End If
    '    '                                        End With
    '    '                                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Add(objPHONE)
    '    '                                    Next
    '    '                                End If
    '    '                            End If
    '    '                        End If
    '    '                    End If
    '    '                End If

    '    '                With objSIPENDAR_PROFILE_CLASS
    '    '                    'SiPendar Profile
    '    '                    With objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE

    '    '                        'Populate Data Header
    '    '                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
    '    '                            .FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.SelectedItemValue
    '    '                        End If
    '    '                        If Not String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue) Then
    '    '                            .FK_SIPENDAR_JENIS_WATCHLIST_CODE = cmb_JENIS_WATCHLIST_CODE.SelectedItemValue
    '    '                        End If
    '    '                        If Not String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue) Then
    '    '                            .FK_SIPENDAR_TINDAK_PIDANA_CODE = cmb_TINDAK_PIDANA_CODE.SelectedItemValue
    '    '                        End If
    '    '                        If Not String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue) Then
    '    '                            .FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE = cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue
    '    '                        End If
    '    '                        If Not objGlobalParameter Is Nothing Then
    '    '                            .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
    '    '                        End If

    '    '                        'tambahan 5-Jul-2021
    '    '                        If Not String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue) Then
    '    '                            .FK_SIPENDAR_SUMBER_TYPE_CODE = cmb_SUMBER_TYPE_CODE.SelectedItemValue
    '    '                        End If

    '    '                        'tambahan 8-Jul-2021
    '    '                        If Not String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue) Then
    '    '                            .FK_SIPENDAR_SUBMISSION_TYPE_CODE = cmb_SUBMISSION_TYPE_CODE.SelectedItemValue
    '    '                        End If
    '    '                        .Submission_Date = DateTime.Now()

    '    '                        '21-Jul-2021 Adi : Sumber ID diambil dari ID Watchlist PPATK
    '    '                        If objScreeningResult.FK_WATCHLIST_ID_PPATK IsNot Nothing AndAlso objScreeningResult.FK_WATCHLIST_ID_PPATK <> "" And cmb_SIPENDAR_TYPE_ID.SelectedItemValue = "2" Then '' Edited by Felix 19 Aug 2021, kalau Pengayaan aja baru diisi
    '    '                            .SUMBER_ID = objScreeningResult.FK_WATCHLIST_ID_PPATK
    '    '                        Else
    '    '                            .SUMBER_ID = Nothing
    '    '                        End If
    '    '                        'End of 21-Jul-2021 Adi : Sumber ID diambil dari ID Watchlist PPATK

    '    '                        'Ari 16-09-2021 inisiasi untuk insert value field fk sipendar screening request dan request result
    '    '                        .FK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID = objScreeningResult.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID
    '    '                        .FK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = recordID

    '    '                        If (chk_IsGenerateTransaction.Checked) Then
    '    '                            .Transaction_DateFrom = CDate(txt_Transaction_DateFrom.Value)
    '    '                            .Transaction_DateTo = CDate(txt_Transaction_DateTo.Value)
    '    '                            If ListIndicator.Count > 0 Then
    '    '                                Dim i As Integer
    '    '                                Dim temp As String
    '    '                                i = 0
    '    '                                temp = ""
    '    '                                For Each itemindicator In ListIndicator
    '    '                                    i += 1
    '    '                                    If ListIndicator.Count = 1 Then
    '    '                                        'temp = getReportIndicatorByKode(item.FK_Indicator)
    '    '                                        temp = itemindicator.FK_Indicator
    '    '                                    Else
    '    '                                        If i = 1 Then
    '    '                                            'temp = getReportIndicatorByKode(item.FK_Indicator)
    '    '                                            temp = itemindicator.FK_Indicator
    '    '                                        Else
    '    '                                            'temp += "," + getReportIndicatorByKode(item.FK_Indicator)
    '    '                                            temp += "," + itemindicator.FK_Indicator
    '    '                                        End If
    '    '                                    End If
    '    '                                Next
    '    '                                .Report_Indicator = temp
    '    '                                Session("Report_Indicator_SIpendar_SCREENING_CUSTOMER_PROSPECT_REQUEST") = temp
    '    '                            End If
    '    '                        End If
    '    '                        .Keterangan = Trim(txt_Keterangan.Value)
    '    '                        'end of tambahan 8-Jul-2021

    '    '                        If Not objScreeningResult Is Nothing Then
    '    '                            If Not IsNothing(objScreeningResult.TotalSimilarity) Then
    '    '                                .Similarity = CDbl(objScreeningResult.TotalSimilarity) * 100
    '    '                            End If
    '    '                        End If

    '    '                        'If Not (String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A" Or txt_SUMBER_ID.Text = "NA") Then
    '    '                        '    .SUMBER_ID = CInt(txt_SUMBER_ID.Value)
    '    '                        'Else
    '    '                        '    .SUMBER_ID = Nothing
    '    '                        'End If
    '    '                        'end of tambahan 5-Jul-2021

    '    '                        If String.IsNullOrEmpty(temprole) Then
    '    '                            If Not objCustomer Is Nothing Then
    '    '                                .Fk_goAML_Ref_Customer_Type_id = objCustomer.FK_Customer_Type_ID
    '    '                                Dim objAddress = listAddress.FirstOrDefault

    '    '                                .GCN = objCustomer.GCN

    '    '                                If objCustomer.FK_Customer_Type_ID = 1 Then     'Individual
    '    '                                    .INDV_NAME = objCustomer.INDV_Last_Name
    '    '                                    .INDV_FK_goAML_Ref_Nama_Negara_CODE = objCustomer.INDV_Nationality1

    '    '                                    If objAddress IsNot Nothing Then
    '    '                                        .INDV_ADDRESS = objAddress.Address
    '    '                                    End If

    '    '                                    .INDV_PLACEOFBIRTH = objCustomer.INDV_Birth_Place
    '    '                                    .INDV_DOB = objCustomer.INDV_BirthDate
    '    '                                Else    'Korporasi
    '    '                                    .CORP_NAME = objCustomer.Corp_Name
    '    '                                    .CORP_FK_goAML_Ref_Nama_Negara_CODE = objCustomer.Corp_Incorporation_Country_Code
    '    '                                    .CORP_NPWP = objCustomer.Corp_Tax_Number
    '    '                                    .CORP_NO_IZIN_USAHA = objCustomer.Corp_Incorporation_Number
    '    '                                End If
    '    '                            End If
    '    '                        Else
    '    '                            If Not IsDBNull(row("GCN")) Then
    '    '                                Dim stringgcn As String = Convert.ToString(row("GCN"))
    '    '                                .GCN = stringgcn
    '    '                                .Fk_goAML_Ref_Customer_Type_id = 1
    '    '                                Dim objAddress = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.FirstOrDefault
    '    '                                If objAddress IsNot Nothing Then
    '    '                                    .INDV_ADDRESS = objAddress.ADDRESS
    '    '                                End If
    '    '                                Dim drSIPENDAR_StakeHolder_NonCustomer As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
    '    '                                If drSIPENDAR_StakeHolder_NonCustomer IsNot Nothing Then
    '    '                                    If Not IsDBNull(drSIPENDAR_StakeHolder_NonCustomer("Name")) Then
    '    '                                        .INDV_NAME = drSIPENDAR_StakeHolder_NonCustomer("Name")
    '    '                                    End If
    '    '                                    If Not IsDBNull(drSIPENDAR_StakeHolder_NonCustomer("Country_Code")) Then
    '    '                                        .INDV_FK_goAML_Ref_Nama_Negara_CODE = drSIPENDAR_StakeHolder_NonCustomer("Country_Code")
    '    '                                    End If
    '    '                                    If Not IsDBNull(drSIPENDAR_StakeHolder_NonCustomer("Birth_Place")) Then
    '    '                                        .INDV_PLACEOFBIRTH = drSIPENDAR_StakeHolder_NonCustomer("Birth_Place")
    '    '                                    End If
    '    '                                    If Not IsDBNull(drSIPENDAR_StakeHolder_NonCustomer("Birth_Date")) Then
    '    '                                        .INDV_DOB = drSIPENDAR_StakeHolder_NonCustomer("Birth_Date")
    '    '                                    End If
    '    '                                End If
    '    '                            End If
    '    '                        End If
    '    '                    End With

    '    '                    'SiPendarProfile Account
    '    '                    If listProfileAccount IsNot Nothing AndAlso listProfileAccount.Count > 0 Then
    '    '                        Dim PK_ID As Long = 0
    '    '                        Dim PK_ATM_ID As Long = 0
    '    '                        For Each itemAccount In listProfileAccount
    '    '                            PK_ID = PK_ID - 1
    '    '                            Dim objAccount As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT
    '    '                            With objAccount
    '    '                                .PK_SIPENDAR_PROFILE_ACCOUNT_ID = PK_ID
    '    '                                .CIFNO = itemAccount.ObjAccount.client_number
    '    '                                .Fk_goAML_Ref_Jenis_Rekening_CODE = itemAccount.ObjAccount.personal_account_type
    '    '                                .Fk_goAML_Ref_Status_Rekening_CODE = itemAccount.ObjAccount.status_code

    '    '                                If objGlobalParameter IsNot Nothing Then
    '    '                                    .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
    '    '                                End If

    '    '                                .NOREKENING = itemAccount.ObjAccount.Account_No
    '    '                            End With

    '    '                            objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Add(objAccount)

    '    '                            '' Added on 30 Jul 2021

    '    '                            For Each itemATM In itemAccount.ListATMAccount
    '    '                                PK_ATM_ID = PK_ATM_ID - 1
    '    '                                Dim objATM As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT_ATM
    '    '                                With objATM
    '    '                                    .PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = PK_ATM_ID
    '    '                                    .FK_SIPENDAR_PROFILE_ACCOUNT_ID = objAccount.PK_SIPENDAR_PROFILE_ACCOUNT_ID
    '    '                                    .NOATM = itemATM.ATM_NO
    '    '                                End With
    '    '                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Add(objATM)
    '    '                            Next
    '    '                            '' end of 30 Jul 2021
    '    '                        Next
    '    '                    End If

    '    '                    'SiPendarProfile Address
    '    '                    If listAddress IsNot Nothing AndAlso listAddress.Count > 0 Then
    '    '                        Dim PK_ID As Long = 0
    '    '                        For Each itemAddress In listAddress
    '    '                            PK_ID = PK_ID - 1
    '    '                            Dim objAddress As New SiPendarDAL.SIPENDAR_PROFILE_ADDRESS
    '    '                            With objAddress
    '    '                                .PK_SIPENDAR_PROFILE_ADDRESS_ID = PK_ID
    '    '                                .FK_goAML_Ref_Kategori_Kontak_CODE = itemAddress.Address_Type
    '    '                                .ADDRESS = itemAddress.Address
    '    '                                .CITY = itemAddress.City
    '    '                                .FK_goAML_Ref_Nama_Negara_CODE = itemAddress.Country_Code
    '    '                                .ZIP = itemAddress.Zip

    '    '                                'tambahan 5 Jul 2021
    '    '                                .TOWN = itemAddress.Town
    '    '                                .STATE = itemAddress.State
    '    '                                .COMMENTS = itemAddress.Comments
    '    '                            End With

    '    '                            objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Add(objAddress)
    '    '                        Next
    '    '                    End If

    '    '                    'SiPendarProfile PHONE
    '    '                    If listPhone IsNot Nothing AndAlso listPhone.Count > 0 Then
    '    '                        Dim PK_ID As Long = 0
    '    '                        For Each itemPHONE In listPhone
    '    '                            PK_ID = PK_ID - 1
    '    '                            Dim objPHONE As New SiPendarDAL.SIPENDAR_PROFILE_PHONE
    '    '                            With objPHONE
    '    '                                .PK_SIPENDAR_PROFILE_PHONE_ID = PK_ID
    '    '                                .FK_goAML_Ref_Kategori_Kontak_CODE = itemPHONE.Tph_Contact_Type
    '    '                                .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = itemPHONE.Tph_Communication_Type
    '    '                                .COUNTRY_PREFIX = itemPHONE.tph_country_prefix
    '    '                                .PHONE_NUMBER = itemPHONE.tph_number

    '    '                                'tambahan 5 Jul 2021
    '    '                                .EXTENSION_NUMBER = itemPHONE.tph_extension
    '    '                                .COMMENTS = itemPHONE.comments
    '    '                            End With

    '    '                            objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Add(objPHONE)
    '    '                        Next
    '    '                    End If

    '    '                    'SiPendarProfile IDENTIFICATION
    '    '                    If listIdentification IsNot Nothing AndAlso listIdentification.Count > 0 Then
    '    '                        Dim PK_ID As Long = 0
    '    '                        For Each itemIDENTIFICATION In listIdentification
    '    '                            Dim objCek = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.IDENTITY_NUMBER = itemIDENTIFICATION.Number)
    '    '                            If objCek IsNot Nothing Then
    '    '                                Continue For
    '    '                            End If
    '    '                            PK_ID = PK_ID - 1
    '    '                            Dim objIDENTIFICATION As New SiPendarDAL.SIPENDAR_PROFILE_IDENTIFICATION
    '    '                            With objIDENTIFICATION
    '    '                                .PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = PK_ID
    '    '                                .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = itemIDENTIFICATION.Type
    '    '                                .IDENTITY_NUMBER = itemIDENTIFICATION.Number
    '    '                                .ISSUE_DATE = itemIDENTIFICATION.Issue_Date
    '    '                                .EXPIRED_DATE = itemIDENTIFICATION.Expiry_Date
    '    '                                .ISSUED_BY = itemIDENTIFICATION.Issued_By
    '    '                                .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = itemIDENTIFICATION.Issued_Country

    '    '                                'tambahan 5 Jul 2021
    '    '                                .Comments = itemIDENTIFICATION.Identification_Comment
    '    '                            End With

    '    '                            objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Add(objIDENTIFICATION)
    '    '                        Next
    '    '                    End If
    '    '                End With

    '    '                '' Added by Felix 02 Aug 2021

    '    '                If SiPendarProfile_BLL.IsExistsInApprovalProfileType(objProfileModule.ModuleName, objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID & "-" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & "-" & Now.ToString("yyyy-MM-dd"), "1", objGlobalParameterPerType.ParameterValue) Then
    '    '                    'LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."
    '    '                    Throw New ApplicationException("Sorry, New Profile for GCN " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & " today is already exists in Pending Approval.")
    '    '                    Panelconfirmation.Hidden = False
    '    '                    FormPanelInput.Hidden = True
    '    '                End If
    '    '                '' End of 02 Aug 2021

    '    '                'Save Data With/Without Approval
    '    '                'If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
    '    '                '6-Jul-2021 Adi : get Module apakah perlu approval atau tidak sesuai SIPENDAR TYPE yang diisi
    '    '                If objProfileModule IsNot Nothing Then
    '    '                    If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
    '    '                        SiPendarProfile_BLL.SaveAddWithoutApproval(objProfileModule, objSIPENDAR_PROFILE_CLASS)
    '    '                    Else
    '    '                        SiPendarProfile_BLL.SaveAddWithApproval(objProfileModule, objSIPENDAR_PROFILE_CLASS)
    '    '                    End If
    '    '                Else
    '    '                    SiPendarProfile_BLL.SaveAddWithoutApproval(ObjModule, objSIPENDAR_PROFILE_CLASS)
    '    '                End If

    '    '                'Set previousGCN
    '    '                previousGCN = objScreeningResult.GCN
    '    '            Next

    '    '            'Save Data With/Without Approval
    '    '            If objProfileModule IsNot Nothing Then
    '    '                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
    '    '                    LblConfirmation.Text = "Data Saved into Database"
    '    '                Else
    '    '                    LblConfirmation.Text = "Data Saved into Pending Approval"
    '    '                End If
    '    '            Else
    '    '                LblConfirmation.Text = "Data Saved into Database"
    '    '            End If
    '    '        Else
    '    '            'Save by looping selected item
    '    '            Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)

    '    '            '' Felix 27 Jul 2021
    '    '            Dim objGlobalParameterPerType As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing

    '    '            'Cek dulu apakah sudah pernah dilapor pada hari yang sama dan GCN yang sama
    '    '            Dim strMessage As String = ""
    '    '            For Each item As SelectedRow In smScreeningResult.SelectedRows
    '    '                Dim recordID = item.RecordID.ToString
    '    '                Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT = Nothing
    '    '                Using objDbSipendar As New SiPendarDAL.SiPendarEntities
    '    '                    objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = recordID).FirstOrDefault
    '    '                    Dim dtScreeningResult As New DataTable
    '    '                    If objScreeningResult IsNot Nothing Then
    '    '                        dtScreeningResult = GetDataTabelSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT(objScreeningResult.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID, objScreeningResult.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID)
    '    '                    End If
    '    '                    If dtScreeningResult IsNot Nothing AndAlso dtScreeningResult.Rows.Count > 0 Then
    '    '                        Dim drScreeningResult As DataRow = dtScreeningResult.Rows(0)
    '    '                        If drScreeningResult IsNot Nothing Then
    '    '                            If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 AndAlso Not IsDBNull(drScreeningResult("Stakeholder_Role")) Then
    '    '                                Throw New Exception("Non Customer hanya boleh dilaporkan sebagai SIPENDAR PROAKTIF")
    '    '                            End If
    '    '                        End If
    '    '                    End If

    '    '                    If objScreeningResult IsNot Nothing Then
    '    '                        'Cek apakah GCN kosong
    '    '                        If objScreeningResult.GCN Is Nothing OrElse objScreeningResult.GCN = "" Then
    '    '                            Throw New Exception("GCN tidak boleh kosong untuk bisa dilaporkan sebagai SIPENDAR")
    '    '                        End If

    '    '                        'Cek apakah ID Watchlist PPATK kosong (khusus pengayaan)
    '    '                        If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 AndAlso (objScreeningResult.FK_WATCHLIST_ID_PPATK Is Nothing OrElse objScreeningResult.FK_WATCHLIST_ID_PPATK = "") Then
    '    '                            Throw New Exception("ID Watchlist PPATK tidak boleh kosong untuk bisa dilaporkan sebagai SIPENDAR PENGAYAAN")
    '    '                        End If

    '    '                        'Cek apakah sudah pernah dilaporkan pada hari yang sama
    '    '                        'Dim objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = objScreeningResult.GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue).FirstOrDefault
    '    '                        ' Edited by Felix 27 Jul 2021

    '    '                        objGlobalParameterPerType = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 7).FirstOrDefault

    '    '                        Dim objprofile As New SiPendarDAL.SIPENDAR_PROFILE
    '    '                        If objGlobalParameterPerType.ParameterValue = 1 Then
    '    '                            objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = objScreeningResult.GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now) And x.FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.TextValue).FirstOrDefault
    '    '                        Else
    '    '                            objprofile = objDbSipendar.SIPENDAR_PROFILE.Where(Function(x) x.GCN = objScreeningResult.GCN And DbFunctions.TruncateTime(x.CreatedDate) = DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault
    '    '                        End If
    '    '                        'End of 27 Jul 2021

    '    '                        If objprofile IsNot Nothing Then
    '    '                            strMessage += "<br>GCN : " & objScreeningResult.GCN & " - " & objScreeningResult.NAMAWATCHLIST & " sudah pernah dilaporkan pada hari ini."
    '    '                        End If

    '    '                        'Cek juga apakah ada transaksi di range tanggal yang diinput untuk GCN-GCN yang dipilih
    '    '                        If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Then
    '    '                            If chk_IsGenerateTransaction.Checked Then
    '    '                                Dim dtFrom As Date = CDate(txt_Transaction_DateFrom.Value)
    '    '                                Dim dtTo As Date = CDate(txt_Transaction_DateTo.Value)

    '    '                                Dim objList = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch_GCN(objScreeningResult.GCN, dtFrom, dtTo)
    '    '                                If objList Is Nothing OrElse objList.Count <= 0 Then
    '    '                                    strMessage += "<br>GCN : " & objScreeningResult.GCN & " - " & objScreeningResult.NAMAWATCHLIST & " tidak memiliki data transaksi di range tanggal tersebut."
    '    '                                End If

    '    '                                '' Added by Felix 06 Aug 2021
    '    '                                Dim CheckLawanKosong As Long = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER where PK_GlobalReportParameter_ID = 8", Nothing) '' Added by Felix on 30 Aug 2021
    '    '                                If CheckLawanKosong = 1 Then
    '    '                                    Dim objCheckLawan = SiPendarBLL.SIPENDARGenerateBLL.CheckCounterPartyExistForTransaction(objScreeningResult.GCN, dtFrom, dtTo)
    '    '                                    If objCheckLawan.Count > 0 Then
    '    '                                        strMessage += "<br>GCN : " & objScreeningResult.GCN & " - " & objScreeningResult.NAMAWATCHLIST & " memiliki transaksi yang tidak mempunyai lawan untuk periode yang dipilih."
    '    '                                    End If
    '    '                                    '' End of Felix 06 Aug 2021
    '    '                                End If '' End of 30 Aug 2021

    '    '                                Dim objCek = objDbSipendar.SIPENDAR_Report.Where(Function(x) x.UnikReference = objScreeningResult.GCN And DbFunctions.TruncateTime(x.Submission_Date) = DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault
    '    '                                If objCek IsNot Nothing Then
    '    '                                    Throw New ApplicationException("Tidak bisa generate Report Transaction. Customer ini sudah punya Report Transaction di hari ini.")
    '    '                                End If

    '    '                            End If

    '    '                        End If

    '    '                    End If
    '    '                End Using
    '    '            Next
    '    '            If Not String.IsNullOrEmpty(strMessage) Then
    '    '                Throw New ApplicationException(strMessage)
    '    '            End If

    '    '            '6-Jul-2021 Adi Get objModule
    '    '            Dim objProfileModule As NawaDAL.Module = Nothing
    '    '            If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
    '    '                objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PROAKTIF")
    '    '            Else
    '    '                objProfileModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_PROFILE_PENGAYAAN")
    '    '            End If

    '    '            'Save to Database (With/Without Approval)
    '    '            Dim previousGCN As String = ""
    '    '            For Each item As SelectedRow In smScreeningResult.SelectedRows
    '    '                Dim recordID = item.RecordID.ToString

    '    '                Dim temprole As String = ""

    '    '                'If Not IsDBNull(Row("Stakeholder_Role")) Then
    '    '                '    temprole = Convert.ToString(Row("Stakeholder_Role"))
    '    '                'End If

    '    '                'Buat object baru untuk list SIPENDAR_PROFILE
    '    '                Dim objSIPENDAR_PROFILE_CLASS As New SiPendarBLL.SIPENDAR_PROFILE_CLASS

    '    '                'Ambil data2 dari Sipendar Entity
    '    '                Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT = Nothing
    '    '                Dim objGlobalParameter As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
    '    '                Using objDbSipendar As New SiPendarDAL.SiPendarEntities
    '    '                    objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = recordID).FirstOrDefault
    '    '                    objGlobalParameter = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault
    '    '                End Using

    '    '                'Skip jika ada 2 GCN yang sama dipilih
    '    '                If objScreeningResult.GCN = previousGCN Then
    '    '                    Continue For
    '    '                End If

    '    '                'Ambil data2 dari NawadataDevEntities
    '    '                Dim objCustomer As SiPendarDAL.goAML_Ref_Customer = Nothing
    '    '                Dim listCustomer As New List(Of SiPendarDAL.goAML_Ref_Customer)      '1 GCN bisa punya banyak CIF
    '    '                'Dim listAccount As New List(Of SiPendarDAL.goAML_Ref_Account)
    '    '                Dim listAddress As New List(Of SiPendarDAL.goAML_Ref_Address)
    '    '                Dim listPhone As New List(Of SiPendarDAL.goAML_Ref_Phone)
    '    '                Dim listIdentification As New List(Of SiPendarDAL.goAML_Person_Identification)

    '    '                Dim listProfileAccount As New List(Of SiPendarBLL.ProfileAccountClass) '' Added 30 Jul 2021

    '    '                ''Here
    '    '                Dim dtScreeningResult As New DataTable
    '    '                If objScreeningResult IsNot Nothing Then
    '    '                    dtScreeningResult = GetDataTabelSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT(objScreeningResult.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID, objScreeningResult.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID)
    '    '                End If

    '    '                If dtScreeningResult IsNot Nothing AndAlso dtScreeningResult.Rows.Count > 0 Then
    '    '                    Dim drScreeningResult As DataRow = dtScreeningResult.Rows(0)
    '    '                    If drScreeningResult IsNot Nothing Then
    '    '                        If IsDBNull(drScreeningResult("Stakeholder_Role")) Then
    '    '                            Using objDbNawa As New SiPendarDAL.SiPendarEntities
    '    '                                'Get Data Customer
    '    '                                If Not objScreeningResult Is Nothing Then
    '    '                                    objCustomer = objDbNawa.goAML_Ref_Customer.Where(Function(x) x.GCN = objScreeningResult.GCN).FirstOrDefault
    '    '                                    listCustomer = objDbNawa.goAML_Ref_Customer.Where(Function(x) x.GCN = objScreeningResult.GCN).ToList
    '    '                                End If

    '    '                                'Get Account, Address, Phone, Identification
    '    '                                If listCustomer IsNot Nothing AndAlso listCustomer.Count > 0 Then
    '    '                                    For Each itemCustomer In listCustomer
    '    '                                        Dim tempListAccount = objDbNawa.goAML_Ref_Account.Where(Function(x) x.client_number = itemCustomer.CIF).ToList
    '    '                                        Dim tempListAddress = objDbNawa.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = itemCustomer.PK_Customer_ID).ToList
    '    '                                        Dim tempListPhone = objDbNawa.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = itemCustomer.PK_Customer_ID).ToList
    '    '                                        Dim tempListIdentification = objDbNawa.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = itemCustomer.PK_Customer_ID).ToList

    '    '                                        'List Account
    '    '                                        If tempListAccount IsNot Nothing AndAlso tempListAccount.Count > 0 Then
    '    '                                            For Each itemAccount In tempListAccount

    '    '                                                Dim tempprofileaccount As New ProfileAccountClass
    '    '                                                tempprofileaccount.ObjAccount = itemAccount
    '    '                                                If itemAccount.Account_No IsNot Nothing Then
    '    '                                                    tempprofileaccount.ListATMAccount = objDbNawa.AML_ACCOUNT_ATM.Where(Function(x) x.Account_No = itemAccount.Account_No).ToList
    '    '                                                End If

    '    '                                                'listAccount.Add(itemAccount)
    '    '                                                listProfileAccount.Add(tempprofileaccount)
    '    '                                            Next
    '    '                                        End If

    '    '                                        'List Address
    '    '                                        If tempListAddress IsNot Nothing AndAlso tempListAddress.Count > 0 Then
    '    '                                            For Each itemaddress In tempListAddress
    '    '                                                listAddress.Add(itemaddress)
    '    '                                            Next
    '    '                                        End If

    '    '                                        'List Phone
    '    '                                        If tempListPhone IsNot Nothing AndAlso tempListPhone.Count > 0 Then
    '    '                                            For Each itemphone In tempListPhone
    '    '                                                listPhone.Add(itemphone)
    '    '                                            Next
    '    '                                        End If

    '    '                                        'List identification
    '    '                                        If itemCustomer.FK_Customer_Type_ID = 1 Then    'Hanya perorangan yg punya Identification
    '    '                                            If tempListIdentification IsNot Nothing AndAlso tempListIdentification.Count > 0 Then
    '    '                                                For Each itemidentification In tempListIdentification
    '    '                                                    listIdentification.Add(itemidentification)
    '    '                                                Next
    '    '                                            End If

    '    '                                            ''Added on 26 Aug 2021
    '    '                                            If Not objCustomer Is Nothing Then
    '    '                                                If Not String.IsNullOrWhiteSpace(itemCustomer.INDV_SSN) Then
    '    '                                                    Dim tempident As New SiPendarDAL.goAML_Person_Identification
    '    '                                                    tempident.Number = itemCustomer.INDV_SSN
    '    '                                                    tempident.Type = "KTP"
    '    '                                                    tempident.Issued_Country = itemCustomer.INDV_Nationality1
    '    '                                                    listIdentification.Add(tempident)
    '    '                                                End If

    '    '                                                If Not String.IsNullOrWhiteSpace(itemCustomer.INDV_Passport_Number) Then
    '    '                                                    Dim tempident As New SiPendarDAL.goAML_Person_Identification
    '    '                                                    tempident.Number = itemCustomer.INDV_Passport_Number
    '    '                                                    tempident.Type = "PAS"
    '    '                                                    tempident.Issued_Country = itemCustomer.INDV_Passport_Country
    '    '                                                    listIdentification.Add(tempident)
    '    '                                                End If
    '    '                                            End If
    '    '                                            '' End of 26 Aug 2021
    '    '                                        End If
    '    '                                    Next
    '    '                                End If
    '    '                            End Using
    '    '                        Else
    '    '                            If Not IsDBNull(drScreeningResult("GCN")) Then
    '    '                                Dim stringgcn As String = Convert.ToString(drScreeningResult("GCN"))
    '    '                                Dim drSIPENDAR_StakeHolder_NonCustomer As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)

    '    '                                If drSIPENDAR_StakeHolder_NonCustomer IsNot Nothing Then
    '    '                                    If Not IsDBNull(drSIPENDAR_StakeHolder_NonCustomer("PK_SIPENDAR_StakeHolder_NonCustomer_ID")) Then

    '    '                                        'SiPendarProfile Account
    '    '                                        Dim PK_IDAccount As Long = -1
    '    '                                        Dim objAccount As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT
    '    '                                        With objAccount
    '    '                                            .PK_SIPENDAR_PROFILE_ACCOUNT_ID = PK_IDAccount
    '    '                                            Dim tempparameter As New SIPENDAR_GLOBAL_PARAMETER

    '    '                                            tempparameter = GetDataSIPENDAR_GLOBAL_PARAMETER(104)
    '    '                                            If tempparameter IsNot Nothing Then
    '    '                                                .CIFNO = tempparameter.ParameterValue
    '    '                                            End If

    '    '                                            tempparameter = GetDataSIPENDAR_GLOBAL_PARAMETER(105)
    '    '                                            If tempparameter IsNot Nothing Then
    '    '                                                .NOREKENING = tempparameter.ParameterValue
    '    '                                            End If

    '    '                                            tempparameter = GetDataSIPENDAR_GLOBAL_PARAMETER(106)
    '    '                                            If tempparameter IsNot Nothing Then
    '    '                                                .Fk_goAML_Ref_Status_Rekening_CODE = tempparameter.ParameterValue
    '    '                                            End If

    '    '                                            tempparameter = GetDataSIPENDAR_GLOBAL_PARAMETER(107)
    '    '                                            If tempparameter IsNot Nothing Then
    '    '                                                .Fk_goAML_Ref_Jenis_Rekening_CODE = tempparameter.ParameterValue
    '    '                                            End If

    '    '                                            If objGlobalParameter IsNot Nothing Then
    '    '                                                .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
    '    '                                            End If

    '    '                                        End With
    '    '                                        objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Add(objAccount)

    '    '                                        'SiPendarProfile IDENTIFICATION
    '    '                                        Dim dtIdentity As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Identifications(Convert.ToString(drSIPENDAR_StakeHolder_NonCustomer("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
    '    '                                        If dtIdentity IsNot Nothing AndAlso dtIdentity.Rows.Count > 0 Then
    '    '                                            Dim PK_ID As Long = 0
    '    '                                            For Each rowsIdentity As Data.DataRow In dtIdentity.Rows
    '    '                                                Dim tempidentitynumber As String = ""
    '    '                                                If Not IsDBNull(rowsIdentity("Identity_Number")) Then
    '    '                                                    tempidentitynumber = Convert.ToString(rowsIdentity("Identity_Number"))
    '    '                                                End If
    '    '                                                Dim objCek = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.IDENTITY_NUMBER = tempidentitynumber)
    '    '                                                If objCek IsNot Nothing Then
    '    '                                                    Continue For
    '    '                                                End If
    '    '                                                PK_ID = PK_ID - 1
    '    '                                                Dim objIDENTIFICATION As New SiPendarDAL.SIPENDAR_PROFILE_IDENTIFICATION
    '    '                                                With objIDENTIFICATION
    '    '                                                    .PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = PK_ID
    '    '                                                    If Not IsDBNull(rowsIdentity("Identity_Type")) Then
    '    '                                                        .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = Convert.ToString(rowsIdentity("Identity_Type"))
    '    '                                                    End If
    '    '                                                    .IDENTITY_NUMBER = tempidentitynumber
    '    '                                                    If Not IsDBNull(rowsIdentity("Issue_Date")) Then
    '    '                                                        .ISSUE_DATE = rowsIdentity("Issue_Date")
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsIdentity("Expiry_Date")) Then
    '    '                                                        .EXPIRED_DATE = rowsIdentity("Expiry_Date")
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsIdentity("Issued_By")) Then
    '    '                                                        .ISSUED_BY = Convert.ToString(rowsIdentity("Issued_By"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsIdentity("Issued_Country")) Then
    '    '                                                        .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = Convert.ToString(rowsIdentity("Issued_Country"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsIdentity("Comments")) Then
    '    '                                                        .Comments = Convert.ToString(rowsIdentity("Comments"))
    '    '                                                    End If
    '    '                                                End With
    '    '                                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Add(objIDENTIFICATION)
    '    '                                            Next
    '    '                                        End If

    '    '                                        'SiPendarProfile Address
    '    '                                        Dim dtAddress As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Addresses(Convert.ToString(drSIPENDAR_StakeHolder_NonCustomer("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
    '    '                                        If dtAddress IsNot Nothing AndAlso dtAddress.Rows.Count > 0 Then
    '    '                                            Dim PK_ID As Long = 0
    '    '                                            For Each rowsAddress As Data.DataRow In dtAddress.Rows
    '    '                                                PK_ID = PK_ID - 1
    '    '                                                Dim objAddress As New SiPendarDAL.SIPENDAR_PROFILE_ADDRESS
    '    '                                                With objAddress
    '    '                                                    .PK_SIPENDAR_PROFILE_ADDRESS_ID = PK_ID
    '    '                                                    If Not IsDBNull(rowsAddress("Address_Type")) Then
    '    '                                                        .FK_goAML_Ref_Kategori_Kontak_CODE = Convert.ToString(rowsAddress("Address_Type"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsAddress("Address")) Then
    '    '                                                        .ADDRESS = Convert.ToString(rowsAddress("Address"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsAddress("City")) Then
    '    '                                                        .CITY = Convert.ToString(rowsAddress("City"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsAddress("Country_code")) Then
    '    '                                                        .FK_goAML_Ref_Nama_Negara_CODE = Convert.ToString(rowsAddress("Country_code"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsAddress("Zip")) Then
    '    '                                                        .ZIP = Convert.ToString(rowsAddress("Zip"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsAddress("Town")) Then
    '    '                                                        .TOWN = Convert.ToString(rowsAddress("Town"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsAddress("State")) Then
    '    '                                                        .STATE = Convert.ToString(rowsAddress("State"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsAddress("Comments")) Then
    '    '                                                        .COMMENTS = Convert.ToString(rowsAddress("Comments"))
    '    '                                                    End If
    '    '                                                End With
    '    '                                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Add(objAddress)
    '    '                                            Next
    '    '                                        End If

    '    '                                        'SiPendarProfile Address
    '    '                                        Dim dtPhone As DataTable = GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Phones(Convert.ToString(drSIPENDAR_StakeHolder_NonCustomer("PK_SIPENDAR_StakeHolder_NonCustomer_ID")))
    '    '                                        If dtPhone IsNot Nothing AndAlso dtPhone.Rows.Count > 0 Then
    '    '                                            Dim PK_ID As Long = 0
    '    '                                            For Each rowsPhone As Data.DataRow In dtPhone.Rows
    '    '                                                PK_ID = PK_ID - 1
    '    '                                                Dim objPHONE As New SiPendarDAL.SIPENDAR_PROFILE_PHONE
    '    '                                                With objPHONE
    '    '                                                    .PK_SIPENDAR_PROFILE_PHONE_ID = PK_ID
    '    '                                                    If Not IsDBNull(rowsPhone("Tph_Contact_Type")) Then
    '    '                                                        .FK_goAML_Ref_Kategori_Kontak_CODE = Convert.ToString(rowsPhone("Tph_Contact_Type"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsPhone("Tph_Communication_Type")) Then
    '    '                                                        .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = Convert.ToString(rowsPhone("Tph_Communication_Type"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsPhone("Tph_Country_Prefix")) Then
    '    '                                                        .COUNTRY_PREFIX = Convert.ToString(rowsPhone("Tph_Country_Prefix"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsPhone("Tph_Number")) Then
    '    '                                                        .PHONE_NUMBER = Convert.ToString(rowsPhone("Tph_Number"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsPhone("Tph_Extension")) Then
    '    '                                                        .EXTENSION_NUMBER = Convert.ToString(rowsPhone("Tph_Extension"))
    '    '                                                    End If
    '    '                                                    If Not IsDBNull(rowsPhone("Comments")) Then
    '    '                                                        .COMMENTS = Convert.ToString(rowsPhone("Comments"))
    '    '                                                    End If
    '    '                                                End With
    '    '                                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Add(objPHONE)
    '    '                                            Next
    '    '                                        End If
    '    '                                    End If
    '    '                                End If
    '    '                            End If
    '    '                        End If
    '    '                    End If
    '    '                End If

    '    '                With objSIPENDAR_PROFILE_CLASS
    '    '                    'SiPendar Profile
    '    '                    With objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE

    '    '                        'Populate Data Header
    '    '                        If Not String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
    '    '                            .FK_SIPENDAR_TYPE_ID = cmb_SIPENDAR_TYPE_ID.SelectedItemValue
    '    '                        End If
    '    '                        If Not String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue) Then
    '    '                            .FK_SIPENDAR_JENIS_WATCHLIST_CODE = cmb_JENIS_WATCHLIST_CODE.SelectedItemValue
    '    '                        End If
    '    '                        If Not String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue) Then
    '    '                            .FK_SIPENDAR_TINDAK_PIDANA_CODE = cmb_TINDAK_PIDANA_CODE.SelectedItemValue
    '    '                        End If
    '    '                        If Not String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue) Then
    '    '                            .FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE = cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue
    '    '                        End If
    '    '                        If Not objGlobalParameter Is Nothing Then
    '    '                            .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
    '    '                        End If

    '    '                        'tambahan 5-Jul-2021
    '    '                        If Not String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue) Then
    '    '                            .FK_SIPENDAR_SUMBER_TYPE_CODE = cmb_SUMBER_TYPE_CODE.SelectedItemValue
    '    '                        End If

    '    '                        'If Not (String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A" Or txt_SUMBER_ID.Text = "NA") Then
    '    '                        '    .SUMBER_ID = CInt(txt_SUMBER_ID.Value)
    '    '                        'Else
    '    '                        '    .SUMBER_ID = Nothing
    '    '                        'End If
    '    '                        'end of tambahan 5-Jul-2021
    '    '                        'tambahan 8-Jul-2021
    '    '                        If Not String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue) Then
    '    '                            .FK_SIPENDAR_SUBMISSION_TYPE_CODE = cmb_SUBMISSION_TYPE_CODE.SelectedItemValue
    '    '                        End If
    '    '                        .Submission_Date = DateTime.Now()

    '    '                        If (chk_IsGenerateTransaction.Checked) Then
    '    '                            .Transaction_DateFrom = CDate(txt_Transaction_DateFrom.Value)
    '    '                            .Transaction_DateTo = CDate(txt_Transaction_DateTo.Value)
    '    '                            If ListIndicator.Count > 0 Then
    '    '                                Dim i As Integer
    '    '                                Dim temp As String
    '    '                                i = 0
    '    '                                temp = ""
    '    '                                For Each itemindicator In ListIndicator
    '    '                                    i += 1
    '    '                                    If ListIndicator.Count = 1 Then
    '    '                                        'temp = getReportIndicatorByKode(item.FK_Indicator)
    '    '                                        temp = itemindicator.FK_Indicator
    '    '                                    Else
    '    '                                        If i = 1 Then
    '    '                                            'temp = getReportIndicatorByKode(item.FK_Indicator)
    '    '                                            temp = itemindicator.FK_Indicator
    '    '                                        Else
    '    '                                            'temp += "," + getReportIndicatorByKode(item.FK_Indicator)
    '    '                                            temp += "," + itemindicator.FK_Indicator
    '    '                                        End If
    '    '                                    End If
    '    '                                Next
    '    '                                .Report_Indicator = temp
    '    '                                Session("Report_Indicator_SIpendar_SCREENING_CUSTOMER_PROSPECT_REQUEST") = temp
    '    '                            End If
    '    '                        End If
    '    '                        'end of tambahan 8-Jul-2021

    '    '                        .Keterangan = Trim(txt_Keterangan.Value)



    '    '                        '21-Jul-2021 Adi : Sumber ID diambil dari ID Watchlist PPATK
    '    '                        If objScreeningResult.FK_WATCHLIST_ID_PPATK IsNot Nothing AndAlso objScreeningResult.FK_WATCHLIST_ID_PPATK <> "" And cmb_SIPENDAR_TYPE_ID.SelectedItemValue = "2" Then '' Edited by Felix 19 Aug 2021, kalau Pengayaan aja baru diisi
    '    '                            .SUMBER_ID = objScreeningResult.FK_WATCHLIST_ID_PPATK
    '    '                        Else
    '    '                            .SUMBER_ID = Nothing
    '    '                        End If
    '    '                        'End of 21-Jul-2021 Adi : Sumber ID diambil dari ID Watchlist PPATK

    '    '                        'Ari 16-09-2021 inisiasi untuk insert value field fk sipendar screening request dan request result
    '    '                        .FK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID = objScreeningResult.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID
    '    '                        .FK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = recordID

    '    '                        If Not objScreeningResult Is Nothing Then
    '    '                            If Not IsNothing(objScreeningResult.TotalSimilarity) Then
    '    '                                .Similarity = CDbl(objScreeningResult.TotalSimilarity) * 100
    '    '                            End If
    '    '                        End If


    '    '                        If dtScreeningResult IsNot Nothing AndAlso dtScreeningResult.Rows.Count > 0 Then
    '    '                            Dim drScreeningResult As DataRow = dtScreeningResult.Rows(0)
    '    '                            If drScreeningResult IsNot Nothing Then
    '    '                                If IsDBNull(drScreeningResult("Stakeholder_Role")) Then
    '    '                                    If Not objCustomer Is Nothing Then
    '    '                                        .Fk_goAML_Ref_Customer_Type_id = objCustomer.FK_Customer_Type_ID
    '    '                                        Dim objAddress = listAddress.FirstOrDefault

    '    '                                        .GCN = objCustomer.GCN

    '    '                                        If objCustomer.FK_Customer_Type_ID = 1 Then     'Individual
    '    '                                            .INDV_NAME = objCustomer.INDV_Last_Name
    '    '                                            .INDV_FK_goAML_Ref_Nama_Negara_CODE = objCustomer.INDV_Nationality1

    '    '                                            If objAddress IsNot Nothing Then
    '    '                                                .INDV_ADDRESS = objAddress.Address
    '    '                                            End If

    '    '                                            .INDV_PLACEOFBIRTH = objCustomer.INDV_Birth_Place
    '    '                                            .INDV_DOB = objCustomer.INDV_BirthDate
    '    '                                        Else    'Korporasi
    '    '                                            .CORP_NAME = objCustomer.Corp_Name
    '    '                                            .CORP_FK_goAML_Ref_Nama_Negara_CODE = objCustomer.Corp_Incorporation_Country_Code
    '    '                                            .CORP_NPWP = objCustomer.Corp_Tax_Number
    '    '                                            .CORP_NO_IZIN_USAHA = objCustomer.Corp_Incorporation_Number
    '    '                                        End If
    '    '                                    End If
    '    '                                Else
    '    '                                    If Not IsDBNull(drScreeningResult("GCN")) Then
    '    '                                        Dim stringgcn As String = Convert.ToString(drScreeningResult("GCN"))
    '    '                                        .GCN = stringgcn
    '    '                                        .Fk_goAML_Ref_Customer_Type_id = 1
    '    '                                        Dim objAddress = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.FirstOrDefault
    '    '                                        If objAddress IsNot Nothing Then
    '    '                                            .INDV_ADDRESS = objAddress.ADDRESS
    '    '                                        End If
    '    '                                        Dim drSIPENDAR_StakeHolder_NonCustomer As DataRow = GetDataTabelSIPENDAR_StakeHolder_NonCustomer(stringgcn)
    '    '                                        If drSIPENDAR_StakeHolder_NonCustomer IsNot Nothing Then
    '    '                                            If Not IsDBNull(drSIPENDAR_StakeHolder_NonCustomer("Name")) Then
    '    '                                                .INDV_NAME = drSIPENDAR_StakeHolder_NonCustomer("Name")
    '    '                                            End If
    '    '                                            If Not IsDBNull(drSIPENDAR_StakeHolder_NonCustomer("Country_Code")) Then
    '    '                                                .INDV_FK_goAML_Ref_Nama_Negara_CODE = drSIPENDAR_StakeHolder_NonCustomer("Country_Code")
    '    '                                            End If
    '    '                                            If Not IsDBNull(drSIPENDAR_StakeHolder_NonCustomer("Birth_Place")) Then
    '    '                                                .INDV_PLACEOFBIRTH = drSIPENDAR_StakeHolder_NonCustomer("Birth_Place")
    '    '                                            End If
    '    '                                            If Not IsDBNull(drSIPENDAR_StakeHolder_NonCustomer("Birth_Date")) Then
    '    '                                                .INDV_DOB = drSIPENDAR_StakeHolder_NonCustomer("Birth_Date")
    '    '                                            End If
    '    '                                        End If
    '    '                                    End If
    '    '                                End If
    '    '                            End If
    '    '                        End If
    '    '                    End With

    '    '                    'SiPendarProfile Account
    '    '                    If listProfileAccount IsNot Nothing AndAlso listProfileAccount.Count > 0 Then
    '    '                        Dim PK_ID As Long = 0
    '    '                        Dim PK_ATM_ID As Long = 0
    '    '                        For Each itemAccount In listProfileAccount
    '    '                            PK_ID = PK_ID - 1
    '    '                            Dim objAccount As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT
    '    '                            With objAccount
    '    '                                .PK_SIPENDAR_PROFILE_ACCOUNT_ID = PK_ID
    '    '                                .CIFNO = itemAccount.ObjAccount.client_number
    '    '                                .Fk_goAML_Ref_Jenis_Rekening_CODE = itemAccount.ObjAccount.personal_account_type
    '    '                                .Fk_goAML_Ref_Status_Rekening_CODE = itemAccount.ObjAccount.status_code

    '    '                                If objGlobalParameter IsNot Nothing Then
    '    '                                    .FK_SIPENDAR_ORGANISASI_CODE = objGlobalParameter.ParameterValue
    '    '                                End If

    '    '                                .NOREKENING = itemAccount.ObjAccount.Account_No
    '    '                            End With

    '    '                            objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT.Add(objAccount)

    '    '                            '' Added on 30 Jul 2021

    '    '                            For Each itemATM In itemAccount.ListATMAccount
    '    '                                PK_ATM_ID = PK_ATM_ID - 1
    '    '                                Dim objATM As New SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT_ATM
    '    '                                With objATM
    '    '                                    .PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = PK_ATM_ID
    '    '                                    .FK_SIPENDAR_PROFILE_ACCOUNT_ID = objAccount.PK_SIPENDAR_PROFILE_ACCOUNT_ID
    '    '                                    .NOATM = itemATM.ATM_NO
    '    '                                End With
    '    '                                objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Add(objATM)
    '    '                            Next
    '    '                            '' end of 30 Jul 2021
    '    '                        Next
    '    '                    End If

    '    '                    'SiPendarProfile Address
    '    '                    If listAddress IsNot Nothing AndAlso listAddress.Count > 0 Then
    '    '                        Dim PK_ID As Long = 0
    '    '                        For Each itemAddress In listAddress
    '    '                            PK_ID = PK_ID - 1
    '    '                            Dim objAddress As New SiPendarDAL.SIPENDAR_PROFILE_ADDRESS
    '    '                            With objAddress
    '    '                                .PK_SIPENDAR_PROFILE_ADDRESS_ID = PK_ID
    '    '                                .FK_goAML_Ref_Kategori_Kontak_CODE = itemAddress.Address_Type
    '    '                                .ADDRESS = itemAddress.Address
    '    '                                .CITY = itemAddress.City
    '    '                                .FK_goAML_Ref_Nama_Negara_CODE = itemAddress.Country_Code
    '    '                                .ZIP = itemAddress.Zip

    '    '                                'tambahan 5 Jul 2021
    '    '                                .TOWN = itemAddress.Town
    '    '                                .STATE = itemAddress.State
    '    '                                .COMMENTS = itemAddress.Comments
    '    '                            End With

    '    '                            objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_ADDRESS.Add(objAddress)
    '    '                        Next
    '    '                    End If

    '    '                    'SiPendarProfile PHONE
    '    '                    If listPhone IsNot Nothing AndAlso listPhone.Count > 0 Then
    '    '                        Dim PK_ID As Long = 0
    '    '                        For Each itemPHONE In listPhone
    '    '                            PK_ID = PK_ID - 1
    '    '                            Dim objPHONE As New SiPendarDAL.SIPENDAR_PROFILE_PHONE
    '    '                            With objPHONE
    '    '                                .PK_SIPENDAR_PROFILE_PHONE_ID = PK_ID
    '    '                                .FK_goAML_Ref_Kategori_Kontak_CODE = itemPHONE.Tph_Contact_Type
    '    '                                .FK_goAML_Ref_Jenis_Alat_Komunikasi_CODE = itemPHONE.Tph_Communication_Type
    '    '                                .COUNTRY_PREFIX = itemPHONE.tph_country_prefix
    '    '                                .PHONE_NUMBER = itemPHONE.tph_number

    '    '                                'tambahan 5 Jul 2021
    '    '                                .EXTENSION_NUMBER = itemPHONE.tph_extension
    '    '                                .COMMENTS = itemPHONE.comments
    '    '                            End With

    '    '                            objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_PHONE.Add(objPHONE)
    '    '                        Next
    '    '                    End If

    '    '                    'SiPendarProfile IDENTIFICATION
    '    '                    If listIdentification IsNot Nothing AndAlso listIdentification.Count > 0 Then
    '    '                        Dim PK_ID As Long = 0
    '    '                        For Each itemIDENTIFICATION In listIdentification
    '    '                            Dim objCek = objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.IDENTITY_NUMBER = itemIDENTIFICATION.Number)
    '    '                            If objCek IsNot Nothing Then
    '    '                                Continue For
    '    '                            End If
    '    '                            PK_ID = PK_ID - 1
    '    '                            Dim objIDENTIFICATION As New SiPendarDAL.SIPENDAR_PROFILE_IDENTIFICATION
    '    '                            With objIDENTIFICATION
    '    '                                .PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = PK_ID
    '    '                                .Fk_goAML_Ref_Jenis_Dokumen_Identitas_CODE = itemIDENTIFICATION.Type
    '    '                                .IDENTITY_NUMBER = itemIDENTIFICATION.Number
    '    '                                .ISSUE_DATE = itemIDENTIFICATION.Issue_Date
    '    '                                .EXPIRED_DATE = itemIDENTIFICATION.Expiry_Date
    '    '                                .ISSUED_BY = itemIDENTIFICATION.Issued_By
    '    '                                .FK_goAML_Ref_Nama_Negara_CODE_ISSUE_COUNTRY = itemIDENTIFICATION.Issued_Country

    '    '                                'tambahan 5 Jul 2021
    '    '                                .Comments = itemIDENTIFICATION.Identification_Comment
    '    '                            End With

    '    '                            objSIPENDAR_PROFILE_CLASS.objList_SIPENDAR_PROFILE_IDENTIFICATION.Add(objIDENTIFICATION)
    '    '                        Next
    '    '                    End If
    '    '                End With

    '    '                '' Added by Felix 02 Aug 2021

    '    '                If SiPendarProfile_BLL.IsExistsInApprovalProfileType(objProfileModule.ModuleName, objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID & "-" & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & "-" & Now.ToString("yyyy-MM-dd"), "1", objGlobalParameterPerType.ParameterValue) Then
    '    '                    'LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."
    '    '                    Throw New ApplicationException("Sorry, New Profile for GCN " & objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN & " today is already exists in Pending Approval.")
    '    '                    Panelconfirmation.Hidden = False
    '    '                    FormPanelInput.Hidden = True
    '    '                End If
    '    '                '' End of 02 Aug 2021

    '    '                'Save Data With/Without Approval
    '    '                'If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
    '    '                '6-Jul-2021 Adi : get Module apakah perlu approval atau tidak sesuai SIPENDAR TYPE yang diisi
    '    '                If objProfileModule IsNot Nothing Then
    '    '                    If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
    '    '                        SiPendarProfile_BLL.SaveAddWithoutApproval(objProfileModule, objSIPENDAR_PROFILE_CLASS)
    '    '                    Else
    '    '                        SiPendarProfile_BLL.SaveAddWithApproval(objProfileModule, objSIPENDAR_PROFILE_CLASS)
    '    '                    End If
    '    '                Else
    '    '                    SiPendarProfile_BLL.SaveAddWithoutApproval(ObjModule, objSIPENDAR_PROFILE_CLASS)
    '    '                End If

    '    '                'Set previousGCN
    '    '                previousGCN = objScreeningResult.GCN
    '    '            Next

    '    '            'Save Data With/Without Approval
    '    '            If objProfileModule IsNot Nothing Then
    '    '                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse objProfileModule.IsUseApproval = False Then
    '    '                    LblConfirmation.Text = "Data Saved into Database"
    '    '                Else
    '    '                    LblConfirmation.Text = "Data Saved into Pending Approval"
    '    '                End If
    '    '            Else
    '    '                LblConfirmation.Text = "Data Saved into Database"
    '    '            End If
    '    '        End If


    '    '        'Tampilkan confirmation
    '    '        Panelconfirmation.Hidden = False
    '    '        FormPanelInput.Hidden = True

    '    '    Catch ex As Exception
    '    '        Throw ex
    '    '    End Try
    '    'End Sub


    '#End Region

    'Protected Sub SetControlVisibility(sender As Object, e As DirectEventArgs)
    '    Try
    '        '3-Jul-2021 Adi : Hide field yang awalnya ada di XML tapi di XSD tidak ada
    '        cmb_TINDAK_PIDANA_CODE.IsHidden = True

    '        'Sesuaikan dengan SIPENDAR_TYPE
    '        If Not String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue) Then
    '            If cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Then
    '                cmb_JENIS_WATCHLIST_CODE.IsHidden = False
    '                cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = False
    '                cmb_SUMBER_TYPE_CODE.IsHidden = True
    '                txt_SUMBER_ID.Hidden = True

    '                fs_Generate_Transaction.Hidden = True
    '                chk_IsGenerateTransaction.Checked = False
    '                txt_Transaction_DateFrom.Value = Nothing
    '                txt_Transaction_DateTo.Value = Nothing
    '            Else
    '                cmb_JENIS_WATCHLIST_CODE.IsHidden = True
    '                cmb_SUMBER_INFORMASI_KHUSUS_CODE.IsHidden = True
    '                cmb_SUMBER_TYPE_CODE.IsHidden = False

    '                '21-Jul-2021 Adi : Sumber ID digantikan dengan 
    '                'txt_SUMBER_ID.Hidden = False
    '                txt_SUMBER_ID.Hidden = True

    '                fs_Generate_Transaction.Hidden = False
    '            End If
    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub


#Region "14-Sep-2021 Ariswara"

    Public Property Sipendar_Screening_Result_AllChecked() As DataTable
        Get

            'If Session("SIPENDAR_Screening.Sipendar_Screening_Watchlist_AllChecked") Is Nothing Then
            '    'Dim objdt As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select kode,keterangan from vw_goAML_Ref_Kategori_Kontak", Nothing)
            '    Session("SIPENDAR_Screening.Sipendar_Screening_Watchlist_AllChecked") = Nothing
            'End If
            Return Session("SIPENDAR_Screening.Sipendar_Screening_Result_AllChecked")

        End Get
        Set(ByVal value As DataTable)
            Session("SIPENDAR_Screening.Sipendar_Screening_Result_AllChecked") = value
        End Set

    End Property

    Public Property Sipendar_Screening_Watchlist_AllChecked() As DataTable
        Get

            'If Session("SIPENDAR_Screening.Sipendar_Screening_Watchlist_AllChecked") Is Nothing Then
            '    'Dim objdt As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select kode,keterangan from vw_goAML_Ref_Kategori_Kontak", Nothing)
            '    Session("SIPENDAR_Screening.Sipendar_Screening_Watchlist_AllChecked") = Nothing
            'End If
            Return Session("SIPENDAR_Screening.Sipendar_Screening_Watchlist_AllChecked")

        End Get
        Set(ByVal value As DataTable)
            Session("SIPENDAR_Screening.Sipendar_Screening_Watchlist_AllChecked") = value
        End Set

    End Property

    'Public Property ListIndicator As List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
    '    Get
    '        Return Session("SipendarScreeningCustomerProspect.ListIndicator")
    '    End Get
    '    Set(value As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
    '        Session("SipendarScreeningCustomerProspect.ListIndicator") = value
    '    End Set
    'End Property

    'Public Property IDIndicator As String
    '    Get
    '        Return Session("SipendarScreeningCustomerProspect.IDIndicator")
    '    End Get
    '    Set(value As String)
    '        Session("SipendarScreeningCustomerProspect.IDIndicator") = value
    '    End Set
    'End Property

    Public Property TotalWatchlist As Integer
        Get
            Return Session("SipendarScreeningCustomerProspect.TotalWatchlist")
        End Get
        Set(value As Integer)
            Session("SipendarScreeningCustomerProspect.TotalWatchlist") = value
        End Set
    End Property

    Public Property TotalScreeningResult As Integer
        Get
            Return Session("SipendarScreeningCustomerProspect.TotalScreeningResult")
        End Get
        Set(value As Integer)
            Session("SipendarScreeningCustomerProspect.TotalScreeningResult") = value
        End Set
    End Property

    'Protected Sub ReportIndicator_readData(sender As Object, e As StoreReadDataEventArgs)
    '    Try
    '        Dim query As String = e.Parameters("query")
    '        If query Is Nothing Then query = ""

    '        Dim strfilter As String = ""
    '        If query.Length > 0 Then

    '            strfilter = " Keterangan Like '%" & query & "%'"

    '        End If
    '        If strfilter.Length > 0 Then
    '            strfilter += " and active=1"
    '        Else
    '            strfilter += "active=1"
    '        End If

    '        Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

    '        'objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goaml_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
    '        objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
    '        objstore.DataBind()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

    '    End Try
    'End Sub

    'Sub bindReportIndicator(store As Ext.Net.Store, listAddresss As List(Of SiPendarDAL.SIPENDAR_Report_Indicator))
    '    Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
    '    '  objtable.Columns.Add(New DataColumn("Kode_Indicator", GetType(String)))
    '    objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
    '    If objtable.Rows.Count > 0 Then
    '        For Each item As Data.DataRow In objtable.Rows
    '            '     item("Kode_Indicator") = item("FK_Indicator")
    '            item("Keterangan") = getReportIndicatorByKode(item("FK_Indicator").ToString)
    '        Next
    '    End If
    '    store.DataSource = objtable
    '    store.DataBind()
    'End Sub

    'Shared Function getReportIndicatorByKode(Kode As String) As String
    '    Dim strKategori As String = ""
    '    Using objdb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
    '        Dim kategori As SiPendarDAL.goAML_Ref_Indikator_Laporan = objdb.goAML_Ref_Indikator_Laporan.Where(Function(x) x.Kode = Kode).FirstOrDefault
    '        If kategori IsNot Nothing Then
    '            strKategori = kategori.Keterangan
    '        End If
    '    End Using
    '    Return strKategori
    'End Function

    'Sub editIndicator(id As String, command As String)
    '    Try
    '        '29-Sep-2021 ganti jadi NDS Dropdown
    '        'sar_reportIndicator.Clear()

    '        WindowReportIndicator.Hidden = False
    '        Dim reportIndicator As SiPendarDAL.SIPENDAR_Report_Indicator
    '        reportIndicator = ListIndicator.Where(Function(x) x.PK_Report_Indicator = id).FirstOrDefault

    '        '29-Sep-2021 ganti jadi NDS Dropdown
    '        'sar_reportIndicator.SetValue(reportIndicator.FK_Indicator)
    '        If reportIndicator IsNot Nothing AndAlso reportIndicator.FK_Indicator IsNot Nothing Then
    '            Dim strSQL As String = "SELECT TOP 1 * FROM vw_SIPENDAR_ref_indicator_laporan WHERE Kode='" & reportIndicator.FK_Indicator & "'"
    '            Dim drIndicator As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

    '            If drIndicator IsNot Nothing Then
    '                sar_reportIndicator.SetTextWithTextValue(drIndicator("kode"), drIndicator("keterangan"))
    '            End If
    '        End If

    '        If command = "Detail" Then
    '            '29-Sep-2021 ganti jadi NDS Dropdown
    '            'sar_reportIndicator.Selectable = False
    '            sar_reportIndicator.IsReadOnly = True
    '            BtnsaveReportIndicator.Hidden = True
    '        Else
    '            '29-Sep-2021 ganti jadi NDS Dropdown
    '            'sar_reportIndicator.Selectable = True
    '            sar_reportIndicator.IsReadOnly = False
    '            BtnsaveReportIndicator.Hidden = False
    '        End If

    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub GridcommandReportIndicator(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Delete" Then
    '            ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = ID).FirstOrDefault)
    '            bindReportIndicator(StoreReportIndicatorData, ListIndicator)
    '        ElseIf e.ExtraParams(1).Value = "Edit" Then
    '            editIndicator(ID, "Edit")
    '            IDIndicator = ID
    '        ElseIf e.ExtraParams(1).Value = "Detail" Then
    '            editIndicator(ID, "Detail")
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub btnSaveReportIndicator_Click(sender As Object, e As DirectEventArgs)
    '    Try
    '        '29-Sep-2021 ganti jadi NDS Dropdown
    '        'If String.IsNullOrEmpty(sar_reportIndicator.SelectedItem.Value) Then
    '        If String.IsNullOrEmpty(sar_reportIndicator.SelectedItemValue) Then
    '            'Throw New ApplicationException(sar_reportIndicator.FieldLabel + " Tidak boleh kosong")
    '            Throw New ApplicationException(sar_reportIndicator.Label + " Tidak boleh kosong")
    '        End If

    '        For Each item In ListIndicator
    '            If item.FK_Indicator = sar_reportIndicator.SelectedItemValue Then      'sar_reportIndicator.SelectedItem.Value Then
    '                'Throw New ApplicationException(sar_reportIndicator.SelectedItem.Value + " Sudah Ada")
    '                Throw New ApplicationException(sar_reportIndicator.SelectedItemValue + " Sudah Ada")
    '            End If
    '        Next

    '        Dim reportIndicator As New SiPendarDAL.SIPENDAR_Report_Indicator
    '        If IDIndicator IsNot Nothing Or IDIndicator <> "" Then
    '            ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = IDIndicator).FirstOrDefault)
    '        End If
    '        If ListIndicator.Count = 0 Then
    '            reportIndicator.PK_Report_Indicator = -1
    '        ElseIf ListIndicator.Count >= 1 Then
    '            reportIndicator.PK_Report_Indicator = ListIndicator.Min(Function(x) x.PK_Report_Indicator) - 1
    '        End If

    '        reportIndicator.FK_Indicator = sar_reportIndicator.SelectedItemValue       'sar_reportIndicator.SelectedItem.Value
    '        ListIndicator.Add(reportIndicator)
    '        bindReportIndicator(StoreReportIndicatorData, ListIndicator)
    '        WindowReportIndicator.Hidden = True
    '        IDIndicator = Nothing
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    'Protected Sub BtnCancelreportIndicator_Click(sender As Object, e As DirectEventArgs)
    '    Try
    '        WindowReportIndicator.Hidden = True
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub btn_addIndicator_click(sender As Object, e As DirectEventArgs)
    '    Try
    '        '29-Sep-2021 ganti jadi NDS Dropdown
    '        'sar_reportIndicator.Clear()
    '        sar_reportIndicator.SetTextValue("")

    '        WindowReportIndicator.Hidden = False
    '        'sar_reportIndicator.Selectable = True
    '        sar_reportIndicator.IsReadOnly = False
    '        BtnsaveReportIndicator.Hidden = False
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub sar_IsSelectedAll_CheckedChanged(sender As Object, e As EventArgs) Handles sar_IsSelectedAll.DirectCheck

    '    'If sar_IsSelectedAll.Value Then
    '    If sar_IsSelectedAll.Checked Then
    '        '1-Oct-2021 Adi : jangan hide-show gridpanel, hide-show selection model saja
    '        'gp_DownloadFromWatchlist.Hide()
    '        'gp_DownloadFromWatchlistAllCheck.Show()



    '        Ext.Net.X.Call("hideSMWatchlist")
    '    Else
    '        'gp_DownloadFromWatchlist.Show()
    '        'gp_DownloadFromWatchlistAllCheck.Hide()



    '        Ext.Net.X.Call("showSMWatchlist")
    '    End If
    'End Sub

    Protected Sub sar_IsSelectedAll_Result_CheckedChanged(sender As Object, e As EventArgs) Handles sar_IsSelectedAll_Result.DirectCheck
        If sar_IsSelectedAll_Result.Value Then
            gp_ScreeningResult.Hide()
            BtnExport.Hide()
            BtnExportSelected.Hide()
            gp_ScreeningResult_All.Show()
        Else
            gp_ScreeningResult.Show()
            BtnExport.Show()
            BtnExportSelected.Show()


            gp_ScreeningResult_All.Hide()
        End If
    End Sub

    'Protected Sub store_DownloadFromWatchlistAllCheck_ReadData(sender As Object, e As StoreReadDataEventArgs)
    '    Try
    '        Dim intStart As Integer = e.Start
    '        Dim intLimit As Int16 = e.Limit
    '        Dim inttotalRecord As Integer
    '        Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)



    '        Dim strsort As String = ""
    '        For Each item As DataSorter In e.Sort
    '            strsort += item.Property & " " & item.Direction.ToString
    '        Next
    '        'Me.indexStart = intStart
    '        'Me.strWhereClause = strfilter
    '        'Me.strOrder = strsort
    '        If strsort = "" Then
    '            strsort = "NAMA_ASLI asc"
    '        End If

    '        'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
    '        Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_WATCHLIST", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
    '        ''-- start paging ------------------------------------------------------------
    '        Dim limit As Integer = e.Limit
    '        If (e.Start + e.Limit) > inttotalRecord Then
    '            limit = inttotalRecord - e.Start
    '        End If
    '        'Dim rangeData As List(Of Object) = If((e.Start <0 OrElse limit <0), data, data.GetRange(e.Start, limit))
    '        ''-- end paging ------------------------------------------------------------
    '        e.Total = inttotalRecord
    '        store_DownloadAllFromWatchlist.DataSource = DataPaging
    '        store_DownloadAllFromWatchlist.DataBind()

    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub btn_DownloadAllFromWatchlist_Cancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
    '    Try
    '        window_DownloadFromWatchlist.Hidden = True
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub btn_DownloadAllFromWatchlist_Save_Click()
    '    Try
    '        window_DownloadFromWatchlist.Hidden = False
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub gc_DownloadAllFromWatchlist(sender As Object, e As Ext.Net.DirectEventArgs)
    '    Try

    '        Dim strID = e.ExtraParams(0).Value
    '        'LoadWatchlist(strID)

    '        window_DownloadFromWatchlist.Hidden = True

    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub sm_DownloadFromWatchlist_change(sender As Object, e As EventArgs)
    '    Try
    '        If sar_IsSelectedAll.Checked Then
    '        Else
    '            Dim smWatchlist As RowSelectionModel = TryCast(gp_DownloadFromWatchlist.GetSelectionModel(), RowSelectionModel)
    '            LblSelectedWatchlistCount.Text = "Selected : " & smWatchlist.SelectedRows.Count & " of " & TotalWatchlist
    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Protected Sub sm_DownloadFromScreeningResult_change(sender As Object, e As EventArgs)
        Try
            Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)
            LabelScreeningResultCount.Text = "Selected : " & smScreeningResult.SelectedRows.Count & " of " & TotalScreeningResult
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function GetDataWatchlistByIDPPATK(IDPPATK As String) As DataRow
        Try
            Dim strSQL As String = "select top 1 * from SIPENDAR_WATCHLIST where ID_PPATK ='" & IDPPATK & "'"
            Dim tempdatarow As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            Return tempdatarow
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

#Region "7-Jul-2021 Adi"
    'Penambahan fitur Generate Transaction From - To
    'Protected Sub chk_IsGenerateTransaction_Change(sender As Object, e As EventArgs)
    '    Try
    '        If chk_IsGenerateTransaction.Checked = True Then
    '            txt_Transaction_DateFrom.Hidden = False
    '            txt_Transaction_DateTo.Hidden = False
    '            PanelReportIndicator.Hidden = False
    '        Else
    '            txt_Transaction_DateFrom.Hidden = True
    '            txt_Transaction_DateTo.Hidden = True
    '            PanelReportIndicator.Hidden = True
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    ''Protected Sub SaveScreeningResultOtherInfo()
    ''    Try
    ''        Dim strQuery As String = ""
    ''        Dim strQuery1 As String = "INSERT INTO SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_OTHER_INFO ("
    ''        strQuery1 += "FK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID, "
    ''        strQuery1 += "FK_SIPENDAR_TYPE_ID, "
    ''        strQuery1 += "FK_SIPENDAR_JENIS_WATCHLIST_CODE, "
    ''        strQuery1 += "FK_SIPENDAR_TINDAK_PIDANA_CODE, "
    ''        strQuery1 += "FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE, "
    ''        strQuery1 += "FK_SIPENDAR_ORGANISASI_CODE, "
    ''        strQuery1 += "Keterangan, "
    ''        strQuery1 += "FK_SIPENDAR_SUMBER_TYPE_CODE, "
    ''        strQuery1 += "SUMBER_ID, "
    ''        strQuery1 += "FK_SIPENDAR_SUBMISSION_TYPE_CODE, "
    ''        strQuery1 += "IsGenerateTransaction, "
    ''        strQuery1 += "Transaction_DateFrom, "
    ''        strQuery1 += "Transaction_DateTo, "
    ''        strQuery1 += "GCN, "
    ''        strQuery1 += "CreatedBy, "
    ''        strQuery1 += "CreatedDate, "
    ''        strQuery1 += "Status, " ' Felix 14-Oct-2021 biar masuk jd waiting for approval
    ''        strQuery1 += "LastUpdateDate, " ' Felix 01-Nov-2021 isi lastupdateDate
    ''        strQuery1 += "Report_Indicator)" 'Ari 15-09-2021 menambahakn field untuk report indikator

    ''        Dim strQuery2 As String = ""
    ''        'Ari 15-06-2021 tambah kondisi dimana user klik select all data result
    ''        If sar_IsSelectedAll_Result.Checked Then
    ''            'Save by looping selected item
    ''            Dim smScreeningResult As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT *, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT WHERE PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID=" & PK_Request_ID & " ORDER BY TotalSimilarityPct Desc, GCN, Nama", Nothing)

    ''            'Save to Database
    ''            Using objDbSipendar As New SiPendarDAL.SiPendarEntities
    ''                Dim previousGCN As String = ""
    ''                For Each row As DataRow In smScreeningResult.Rows
    ''                    Dim recordID As String = row.Item("PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID").ToString().ToString

    ''                    'Ambil data2 dari Sipendar Screening Result
    ''                    Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT = Nothing
    ''                    Dim objGlobalParameter As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
    ''                    objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = recordID).FirstOrDefault
    ''                    objGlobalParameter = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault

    ''                    If objScreeningResult IsNot Nothing Then
    ''                        'Skip jika 2 GCN yang sama dipilih
    ''                        If objScreeningResult.GCN = previousGCN Then
    ''                            Continue For
    ''                        End If

    ''                        strQuery2 = "VALUES ("
    ''                        strQuery2 += recordID & ","
    ''                        strQuery2 += IIf(String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue), "NULL", cmb_SIPENDAR_TYPE_ID.SelectedItemValue) & ","

    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue), "NULL", "'" & cmb_JENIS_WATCHLIST_CODE.SelectedItemValue & "'") & ","
    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue), "NULL", "'" & cmb_TINDAK_PIDANA_CODE.SelectedItemValue & "'") & ","
    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue), "NULL", "'" & cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue & "'") & ","

    ''                        strQuery2 += IIf(objGlobalParameter Is Nothing, "NULL", "'" & objGlobalParameter.ParameterValue & "'") & ","
    ''                        strQuery2 += IIf(String.IsNullOrEmpty(txt_Keterangan.Value), "NULL", "'" & txt_Keterangan.Value & "'") & ","

    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue), "NULL", "'" & cmb_SUMBER_TYPE_CODE.SelectedItemValue & "'") & ","
    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A", "NULL", CLng(txt_SUMBER_ID.Value).ToString) & ","

    ''                        strQuery2 += IIf(String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue), "NULL", "'" & cmb_SUBMISSION_TYPE_CODE.SelectedItemValue & "'") & ","
    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "0", "1") & ","
    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "NULL", "'" & CDate(txt_Transaction_DateFrom.Value).ToString("yyyy-MM-dd") & "'") & ","
    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "NULL", "'" & CDate(txt_Transaction_DateTo.Value).ToString("yyyy-MM-dd") & "'") & ","

    ''                        strQuery2 += "'" & objScreeningResult.GCN & "',"
    ''                        strQuery2 += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
    ''                        strQuery2 += "'" & CDate(DateTime.Now).ToString("yyyy-MM-dd") & "',"
    ''                        strQuery2 += "'Waiting For Approval'," ''Felix 14-Oct-2021, biar ilang dr screening Judgement pas save.
    ''                        strQuery2 += "'" & CDate(DateTime.Now).ToString("yyyy-MM-dd") & "'," ' Felix 01-Nov-2021 isi lastupdateDate
    ''                        strQuery2 += "'" & Session("Report_Indicator_SIpendar_SCREENING_CUSTOMER_PROSPECT_REQUEST") & "'" & ")"  ' Ari 15-09-2021 list indikator ini sudah ditampung dalam session


    ''                        strQuery = strQuery1 & strQuery2
    ''                        'Throw New Exception(strQuery)
    ''                        Session("Report_Indicator_SIpendar_SCREENING_CUSTOMER_PROSPECT_REQUEST") = Nothing
    ''                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

    ''                    End If

    ''                Next
    ''            End Using
    ''        Else
    ''            'Save by looping selected item
    ''            Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)

    ''            'Save to Database
    ''            Using objDbSipendar As New SiPendarDAL.SiPendarEntities
    ''                Dim previousGCN As String = ""
    ''                For Each item As SelectedRow In smScreeningResult.SelectedRows
    ''                    Dim recordID = item.RecordID.ToString

    ''                    'Ambil data2 dari Sipendar Screening Result
    ''                    Dim objScreeningResult As SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT = Nothing
    ''                    Dim objGlobalParameter As SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER = Nothing
    ''                    objScreeningResult = objDbSipendar.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = recordID).FirstOrDefault
    ''                    objGlobalParameter = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault

    ''                    If objScreeningResult IsNot Nothing Then
    ''                        'Skip jika 2 GCN yang sama dipilih
    ''                        If objScreeningResult.GCN = previousGCN Then
    ''                            Continue For
    ''                        End If

    ''                        strQuery2 = "VALUES ("
    ''                        strQuery2 += recordID & ","
    ''                        strQuery2 += IIf(String.IsNullOrEmpty(cmb_SIPENDAR_TYPE_ID.SelectedItemValue), "NULL", cmb_SIPENDAR_TYPE_ID.SelectedItemValue) & ","

    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_JENIS_WATCHLIST_CODE.SelectedItemValue), "NULL", "'" & cmb_JENIS_WATCHLIST_CODE.SelectedItemValue & "'") & ","
    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_TINDAK_PIDANA_CODE.SelectedItemValue), "NULL", "'" & cmb_TINDAK_PIDANA_CODE.SelectedItemValue & "'") & ","
    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 2 Or String.IsNullOrEmpty(cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue), "NULL", "'" & cmb_SUMBER_INFORMASI_KHUSUS_CODE.SelectedItemValue & "'") & ","

    ''                        strQuery2 += IIf(objGlobalParameter Is Nothing, "NULL", "'" & objGlobalParameter.ParameterValue & "'") & ","
    ''                        strQuery2 += IIf(String.IsNullOrEmpty(txt_Keterangan.Value), "NULL", "'" & txt_Keterangan.Value & "'") & ","

    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or String.IsNullOrEmpty(cmb_SUMBER_TYPE_CODE.SelectedItemValue), "NULL", "'" & cmb_SUMBER_TYPE_CODE.SelectedItemValue & "'") & ","
    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or String.IsNullOrEmpty(txt_SUMBER_ID.Text) Or txt_SUMBER_ID.Text = "N/A", "NULL", CLng(txt_SUMBER_ID.Value).ToString) & ","

    ''                        strQuery2 += IIf(String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_CODE.SelectedItemValue), "NULL", "'" & cmb_SUBMISSION_TYPE_CODE.SelectedItemValue & "'") & ","
    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "0", "1") & ","
    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "NULL", "'" & CDate(txt_Transaction_DateFrom.Value).ToString("yyyy-MM-dd") & "'") & ","
    ''                        strQuery2 += IIf(cmb_SIPENDAR_TYPE_ID.SelectedItemValue = 1 Or chk_IsGenerateTransaction.Checked = False, "NULL", "'" & CDate(txt_Transaction_DateTo.Value).ToString("yyyy-MM-dd") & "'") & ","

    ''                        strQuery2 += "'" & objScreeningResult.GCN & "',"
    ''                        strQuery2 += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
    ''                        strQuery2 += "'" & CDate(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss") & "',"
    ''                        strQuery2 += "'Waiting For Approval'," ''Felix 14-Oct-2021, biar ilang dr screening Judgement pas save.
    ''                        strQuery2 += "'" & CDate(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss") & "'," ' Felix 01-Nov-2021 isi lastupdateDate
    ''                        strQuery2 += "'" & Session("Report_Indicator_SIpendar_SCREENING_CUSTOMER_PROSPECT_REQUEST") & "'" & ")"  ' Ari 15-09-2021 list indikator ini sudah ditampung dalam session


    ''                        strQuery = strQuery1 & strQuery2
    ''                        'Throw New Exception(strQuery)
    ''                        Session("Report_Indicator_SIpendar_SCREENING_CUSTOMER_PROSPECT_REQUEST") = Nothing
    ''                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

    ''                    End If

    ''                Next
    ''            End Using
    ''        End If


    ''    Catch ex As Exception
    ''        Throw ex
    ''    End Try
    ''End Sub
#End Region

    '#Region "Update 21-Jul-2021 Adi : Screening Request download from SIPENDAR Watchlist"
    '    Protected Sub Store_DownloadFromWatchlist_ReadData(sender As Object, e As StoreReadDataEventArgs)
    '        Try
    '            Dim intStart As Integer = e.Start
    '            Dim intLimit As Int16 = e.Limit
    '            Dim inttotalRecord As Integer
    '            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

    '            intLimit = 10

    '            Dim strsort As String = ""
    '            For Each item As DataSorter In e.Sort
    '                strsort += item.Property & " " & item.Direction.ToString
    '            Next
    '            'Me.indexStart = intStart
    '            'Me.strWhereClause = strfilter
    '            'Me.strOrder = strsort
    '            If strsort = "" Then
    '                strsort = "NAMA_ASLI asc"
    '            End If
    '            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
    '            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_WATCHLIST", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
    '            ''-- start paging ------------------------------------------------------------
    '            Dim limit As Integer = e.Limit
    '            If (e.Start + e.Limit) > inttotalRecord Then
    '                limit = inttotalRecord - e.Start
    '            End If
    '            'Dim rangeData As List(Of Object) = If((e.Start <0 OrElse limit <0), data, data.GetRange(e.Start, limit))
    '            ''-- end paging ------------------------------------------------------------
    '            e.Total = inttotalRecord
    '            store_DownloadFromWatchlist.DataSource = DataPaging
    '            Session("filterstring") = strfilter



    '            store_DownloadFromWatchlist.DataBind()
    '            'Ari 15-09-2021 Inisiasi total watchlist dari store read data
    '            TotalWatchlist = inttotalRecord

    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub btn_DownloadFromWatchlist_Click()
    '        Try
    '            window_DownloadFromWatchlist.Hidden = False
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub


    '    Protected Sub gc_DownloadFromWatchlist(sender As Object, e As Ext.Net.DirectEventArgs)
    '        Try

    '            Dim strID = e.ExtraParams(0).Value
    '            'LoadWatchlist(strID)

    '            window_DownloadFromWatchlist.Hidden = True

    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub btn_DownloadFromWatchlist_Save_Click(sender As Object, e As Ext.Net.DirectEventArgs)
    '        Try
    '            'Cek minimal 1 data watchlist dipilih
    '            Dim smWatchlist As RowSelectionModel = TryCast(gp_DownloadFromWatchlist.GetSelectionModel(), RowSelectionModel)
    '            If sar_IsSelectedAll.Checked Then
    '            Else
    '                If smWatchlist.SelectedRows.Count = 0 Then
    '                    Throw New Exception("Minimal 1 data Watchlist harus dipilih!")
    '                End If
    '            End If



    '            'Load data Watchlist to Screening Request
    '            LoadDataWatchlist()

    '            window_DownloadFromWatchlist.Hidden = True

    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub


    '    Protected Sub btn_DownloadFromWatchlist_Cancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
    '        Try
    '            window_DownloadFromWatchlist.Hidden = True
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub LoadDataWatchlist()
    '        Try
    '            'Ari 15-09-2021 menambahkan kondisi dimana jika user ceklis semua data di grid
    '            If sar_IsSelectedAll.Checked Then
    '                Dim PK_ID As Long = 0

    '                If Not String.IsNullOrEmpty(Session("filterstring")) Then

    '                    Dim query As String = "SELECT * FROM vw_SIPENDAR_WATCHLIST WHERE " & Session("filterstring").ToString

    '                    Dim DataPagings As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
    '                    Sipendar_Screening_Watchlist_AllChecked = Nothing
    '                    Sipendar_Screening_Watchlist_AllChecked = DataPagings
    '                    Session("filterstring") = Nothing

    '                Else
    '                    Dim watchlistall As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT * from vw_SIPENDAR_WATCHLIST", Nothing)
    '                    'Dim watchlistall As DataTable = store_DownloadFromWatchlist.DataSource
    '                    Sipendar_Screening_Watchlist_AllChecked = watchlistall

    '                End If

    '                For Each row As DataRow In Sipendar_Screening_Watchlist_AllChecked.Rows
    '                    Dim recordID As String = row.Item("pk_sipendar_watchlist_id").ToString()
    '                    Dim datawatchlist As DataRow = GetDataWatchlistByPKWatchlist(recordID)
    '                    If datawatchlist IsNot Nothing Then
    '                        'Buat array untuk menampung identity_number
    '                        Dim countIdentity As Integer = 0
    '                        Dim arrIdentity(7, 1) As String

    '                        'Define Jenis Identitas
    '                        arrIdentity(0, 0) = "NPWP"
    '                        arrIdentity(1, 0) = "KTP"
    '                        arrIdentity(2, 0) = "PAS"
    '                        arrIdentity(3, 0) = "KITAS"
    '                        arrIdentity(4, 0) = "SUKET"
    '                        arrIdentity(5, 0) = "SIM"
    '                        arrIdentity(6, 0) = "KITAP"
    '                        arrIdentity(7, 0) = "KIMS"

    '                        'Define Nomor Identitas
    '                        For i = 0 To 7
    '                            If IsDBNull(datawatchlist(arrIdentity(i, 0))) Then
    '                                arrIdentity(i, 1) = Nothing
    '                            Else
    '                                arrIdentity(i, 1) = Convert.ToString(datawatchlist(arrIdentity(i, 0)))
    '                                countIdentity += 1
    '                            End If
    '                        Next

    '                        'Jika identitas tidak ada yang terisi maka buat hanya 1 baris
    '                        If countIdentity = 0 Then
    '                            Dim objNew = New SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL

    '                            If listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Count > 0 Then
    '                                PK_ID = listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Min(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID) - 1
    '                            End If
    '                            If PK_ID >= 0 Then
    '                                PK_ID = -1
    '                            End If

    '                            With objNew
    '                                .PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID = PK_ID
    '                                If Not IsDBNull(datawatchlist("PARAMETER_PENCARIAN_NAMA")) Then
    '                                    .Nama = Convert.ToString(datawatchlist("PARAMETER_PENCARIAN_NAMA"))
    '                                End If
    '                                If Not IsDBNull(datawatchlist("TANGGAL_LAHIR")) AndAlso CDate(datawatchlist("TANGGAL_LAHIR")) <> DateTime.MinValue Then
    '                                    .DOB = CDate(datawatchlist("TANGGAL_LAHIR"))
    '                                End If
    '                                If Not IsDBNull(datawatchlist("TEMPAT_LAHIR")) Then
    '                                    .Birth_Place = Convert.ToString(datawatchlist("TEMPAT_LAHIR"))
    '                                End If
    '                                .FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE = Nothing
    '                                .Identity_Number = Nothing
    '                                If Not IsDBNull(datawatchlist("ID_PPATK")) Then
    '                                    If Not String.IsNullOrEmpty(Convert.ToString(datawatchlist("ID_PPATK"))) Then
    '                                        .FK_WATCHLIST_ID_PPATK = Convert.ToString(datawatchlist("ID_PPATK"))
    '                                    End If
    '                                End If
    '                            End With

    '                            listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Add(objNew)
    '                        Else
    '                            For i = 0 To 7
    '                                If arrIdentity(i, 1) IsNot Nothing Then
    '                                    Dim objNew = New SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL

    '                                    If listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Count > 0 Then
    '                                        PK_ID = listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Min(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID) - 1
    '                                    End If
    '                                    If PK_ID >= 0 Then
    '                                        PK_ID = -1
    '                                    End If

    '                                    With objNew
    '                                        .PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID = PK_ID
    '                                        If Not IsDBNull(datawatchlist("PARAMETER_PENCARIAN_NAMA")) Then
    '                                            .Nama = Convert.ToString(datawatchlist("PARAMETER_PENCARIAN_NAMA"))
    '                                        End If
    '                                        If Not IsDBNull(datawatchlist("TANGGAL_LAHIR")) AndAlso CDate(datawatchlist("TANGGAL_LAHIR")) <> DateTime.MinValue Then
    '                                            .DOB = CDate(datawatchlist("TANGGAL_LAHIR"))
    '                                        End If
    '                                        If Not IsDBNull(datawatchlist("TEMPAT_LAHIR")) Then
    '                                            .Birth_Place = Convert.ToString(datawatchlist("TEMPAT_LAHIR"))
    '                                        End If
    '                                        .FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE = arrIdentity(i, 0)
    '                                        .Identity_Number = arrIdentity(i, 1)
    '                                        If Not IsDBNull(datawatchlist("ID_PPATK")) Then
    '                                            If Not String.IsNullOrEmpty(Convert.ToString(datawatchlist("ID_PPATK"))) Then
    '                                                .FK_WATCHLIST_ID_PPATK = Convert.ToString(datawatchlist("ID_PPATK"))
    '                                            End If
    '                                        End If
    '                                    End With
    '                                    listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Add(objNew)
    '                                End If
    '                            Next
    '                        End If
    '                    End If
    '                Next
    '                'Using objdb As New SiPendarDAL.SiPendarEntities
    '                '    For Each row As DataRow In Sipendar_Screening_Watchlist_AllChecked.Rows
    '                '        Dim recordID As String = row.Item("pk_sipendar_watchlist_id").ToString()
    '                '        Dim objWatchlist = objdb.SIPENDAR_WATCHLIST.Where(Function(x) x.PK_SIPENDAR_WATCHLIST_ID = recordID).FirstOrDefault
    '                '        If objWatchlist IsNot Nothing Then

    '                '            'Buat array untuk menampung identity_number
    '                '            Dim countIdentity As Integer = 0
    '                '            Dim arrIdentity(7, 1) As String

    '                '            'Define Jenis Identitas
    '                '            arrIdentity(0, 0) = "NPWP"
    '                '            arrIdentity(1, 0) = "KTP"
    '                '            arrIdentity(2, 0) = "PAS"
    '                '            arrIdentity(3, 0) = "KITAS"
    '                '            arrIdentity(4, 0) = "SUKET"
    '                '            arrIdentity(5, 0) = "SIM"
    '                '            arrIdentity(6, 0) = "KITAP"
    '                '            arrIdentity(7, 0) = "KIMS"

    '                '            'Define Nomor Identitas
    '                '            arrIdentity(0, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.NPWP), objWatchlist.NPWP, Nothing)
    '                '            arrIdentity(1, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.KTP), objWatchlist.KTP, Nothing)
    '                '            arrIdentity(2, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.PAS), objWatchlist.PAS, Nothing)
    '                '            arrIdentity(3, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.KITAS), objWatchlist.KITAS, Nothing)
    '                '            arrIdentity(4, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.SUKET), objWatchlist.SUKET, Nothing)
    '                '            arrIdentity(5, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.SIM), objWatchlist.SIM, Nothing)
    '                '            arrIdentity(6, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.KITAP), objWatchlist.KITAP, Nothing)

    '                '            'Define count identity
    '                '            For i = 0 To 6
    '                '                If arrIdentity(i, 1) IsNot Nothing Then
    '                '                    countIdentity += 1
    '                '                End If
    '                '            Next

    '                '            'Jika identitas tidak ada yang terisi maka buat hanya 1 baris
    '                '            If countIdentity = 0 Then
    '                '                Dim objNew = New SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL

    '                '                If listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Count > 0 Then
    '                '                    PK_ID = listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Min(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID) - 1
    '                '                End If
    '                '                If PK_ID >= 0 Then
    '                '                    PK_ID = -1
    '                '                End If

    '                '                With objNew
    '                '                    .PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID = PK_ID
    '                '                    .Nama = objWatchlist.PARAMETER_PENCARIAN_NAMA
    '                '                    If Not String.IsNullOrEmpty(objWatchlist.TANGGAL_LAHIR) AndAlso CDate(objWatchlist.TANGGAL_LAHIR) <> DateTime.MinValue Then
    '                '                        .DOB = objWatchlist.TANGGAL_LAHIR
    '                '                    End If

    '                '                    .Birth_Place = objWatchlist.TEMPAT_LAHIR
    '                '                    .FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE = Nothing
    '                '                    .Identity_Number = Nothing

    '                '                    If Not String.IsNullOrEmpty(objWatchlist.ID_PPATK) Then
    '                '                        .FK_WATCHLIST_ID_PPATK = objWatchlist.ID_PPATK
    '                '                    End If

    '                '                End With

    '                '                listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Add(objNew)
    '                '            Else
    '                '                For i = 0 To 6
    '                '                    If arrIdentity(i, 1) IsNot Nothing Then
    '                '                        Dim objNew = New SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL

    '                '                        If listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Count > 0 Then
    '                '                            PK_ID = listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Min(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID) - 1
    '                '                        End If
    '                '                        If PK_ID >= 0 Then
    '                '                            PK_ID = -1
    '                '                        End If

    '                '                        With objNew
    '                '                            .PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID = PK_ID
    '                '                            .Nama = objWatchlist.PARAMETER_PENCARIAN_NAMA
    '                '                            If Not String.IsNullOrEmpty(objWatchlist.TANGGAL_LAHIR) AndAlso CDate(objWatchlist.TANGGAL_LAHIR) <> DateTime.MinValue Then
    '                '                                .DOB = objWatchlist.TANGGAL_LAHIR
    '                '                            End If

    '                '                            .Birth_Place = objWatchlist.TEMPAT_LAHIR
    '                '                            .FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE = arrIdentity(i, 0)
    '                '                            .Identity_Number = arrIdentity(i, 1)

    '                '                            If Not String.IsNullOrEmpty(objWatchlist.ID_PPATK) Then
    '                '                                .FK_WATCHLIST_ID_PPATK = objWatchlist.ID_PPATK
    '                '                            End If

    '                '                        End With

    '                '                        listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Add(objNew)
    '                '                    End If
    '                '                Next
    '                '            End If
    '                '        End If
    '                '    Next
    '                'End Using
    '            Else
    '                Dim smWatchlist As RowSelectionModel = TryCast(gp_DownloadFromWatchlist.GetSelectionModel(), RowSelectionModel)

    '                Dim PK_ID As Long = 0

    '                For Each item As SelectedRow In smWatchlist.SelectedRows
    '                    Dim recordID = item.RecordID.ToString

    '                    Dim datawatchlist As DataRow = GetDataWatchlistByPKWatchlist(recordID)
    '                    If datawatchlist IsNot Nothing Then
    '                        'Buat array untuk menampung identity_number
    '                        Dim countIdentity As Integer = 0
    '                        Dim arrIdentity(7, 1) As String

    '                        'Define Jenis Identitas
    '                        arrIdentity(0, 0) = "NPWP"
    '                        arrIdentity(1, 0) = "KTP"
    '                        arrIdentity(2, 0) = "PAS"
    '                        arrIdentity(3, 0) = "KITAS"
    '                        arrIdentity(4, 0) = "SUKET"
    '                        arrIdentity(5, 0) = "SIM"
    '                        arrIdentity(6, 0) = "KITAP"
    '                        arrIdentity(7, 0) = "KIMS"

    '                        'Define Nomor Identitas
    '                        For i = 0 To 7
    '                            If IsDBNull(datawatchlist(arrIdentity(i, 0))) Then
    '                                arrIdentity(i, 1) = Nothing
    '                            Else
    '                                arrIdentity(i, 1) = Convert.ToString(datawatchlist(arrIdentity(i, 0)))
    '                                countIdentity += 1
    '                            End If
    '                        Next

    '                        'Jika identitas tidak ada yang terisi maka buat hanya 1 baris
    '                        If countIdentity = 0 Then
    '                            Dim objNew = New SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL

    '                            If listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Count > 0 Then
    '                                PK_ID = listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Min(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID) - 1
    '                            End If
    '                            If PK_ID >= 0 Then
    '                                PK_ID = -1
    '                            End If

    '                            With objNew
    '                                .PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID = PK_ID
    '                                If Not IsDBNull(datawatchlist("PARAMETER_PENCARIAN_NAMA")) Then
    '                                    .Nama = Convert.ToString(datawatchlist("PARAMETER_PENCARIAN_NAMA"))
    '                                End If
    '                                If Not IsDBNull(datawatchlist("TANGGAL_LAHIR")) AndAlso CDate(datawatchlist("TANGGAL_LAHIR")) <> DateTime.MinValue Then
    '                                    .DOB = CDate(datawatchlist("TANGGAL_LAHIR"))
    '                                End If
    '                                If Not IsDBNull(datawatchlist("TEMPAT_LAHIR")) Then
    '                                    .Birth_Place = Convert.ToString(datawatchlist("TEMPAT_LAHIR"))
    '                                End If
    '                                .FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE = Nothing
    '                                .Identity_Number = Nothing
    '                                If Not IsDBNull(datawatchlist("ID_PPATK")) Then
    '                                    If Not String.IsNullOrEmpty(Convert.ToString(datawatchlist("ID_PPATK"))) Then
    '                                        .FK_WATCHLIST_ID_PPATK = Convert.ToString(datawatchlist("ID_PPATK"))
    '                                    End If
    '                                End If
    '                            End With

    '                            listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Add(objNew)
    '                        Else
    '                            For i = 0 To 7
    '                                If arrIdentity(i, 1) IsNot Nothing Then
    '                                    Dim objNew = New SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL

    '                                    If listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Count > 0 Then
    '                                        PK_ID = listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Min(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID) - 1
    '                                    End If
    '                                    If PK_ID >= 0 Then
    '                                        PK_ID = -1
    '                                    End If

    '                                    With objNew
    '                                        .PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID = PK_ID
    '                                        If Not IsDBNull(datawatchlist("PARAMETER_PENCARIAN_NAMA")) Then
    '                                            .Nama = Convert.ToString(datawatchlist("PARAMETER_PENCARIAN_NAMA"))
    '                                        End If
    '                                        If Not IsDBNull(datawatchlist("TANGGAL_LAHIR")) AndAlso CDate(datawatchlist("TANGGAL_LAHIR")) <> DateTime.MinValue Then
    '                                            .DOB = CDate(datawatchlist("TANGGAL_LAHIR"))
    '                                        End If
    '                                        If Not IsDBNull(datawatchlist("TEMPAT_LAHIR")) Then
    '                                            .Birth_Place = Convert.ToString(datawatchlist("TEMPAT_LAHIR"))
    '                                        End If
    '                                        .FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE = arrIdentity(i, 0)
    '                                        .Identity_Number = arrIdentity(i, 1)
    '                                        If Not IsDBNull(datawatchlist("ID_PPATK")) Then
    '                                            If Not String.IsNullOrEmpty(Convert.ToString(datawatchlist("ID_PPATK"))) Then
    '                                                .FK_WATCHLIST_ID_PPATK = Convert.ToString(datawatchlist("ID_PPATK"))
    '                                            End If
    '                                        End If
    '                                    End With
    '                                    listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Add(objNew)
    '                                End If
    '                            Next
    '                        End If
    '                    End If
    '                Next
    '                'Using objdb As New SiPendarDAL.SiPendarEntities
    '                '    Dim PK_ID As Long = 0

    '                '    For Each item As SelectedRow In smWatchlist.SelectedRows
    '                '        Dim recordID = item.RecordID.ToString
    '                '        Dim objWatchlist = objdb.SIPENDAR_WATCHLIST.Where(Function(x) x.PK_SIPENDAR_WATCHLIST_ID = recordID).FirstOrDefault
    '                '        If objWatchlist IsNot Nothing Then

    '                '            'Buat array untuk menampung identity_number
    '                '            Dim countIdentity As Integer = 0
    '                '            Dim arrIdentity(6, 1) As String

    '                '            'Define Jenis Identitas
    '                '            arrIdentity(0, 0) = "NPWP"
    '                '            arrIdentity(1, 0) = "KTP"
    '                '            arrIdentity(2, 0) = "PAS"
    '                '            arrIdentity(3, 0) = "KITAS"
    '                '            arrIdentity(4, 0) = "SUKET"
    '                '            arrIdentity(5, 0) = "SIM"
    '                '            arrIdentity(6, 0) = "KITAP"

    '                '            'Define Nomor Identitas
    '                '            arrIdentity(0, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.NPWP), objWatchlist.NPWP, Nothing)
    '                '            arrIdentity(1, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.KTP), objWatchlist.KTP, Nothing)
    '                '            arrIdentity(2, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.PAS), objWatchlist.PAS, Nothing)
    '                '            arrIdentity(3, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.KITAS), objWatchlist.KITAS, Nothing)
    '                '            arrIdentity(4, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.SUKET), objWatchlist.SUKET, Nothing)
    '                '            arrIdentity(5, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.SIM), objWatchlist.SIM, Nothing)
    '                '            arrIdentity(6, 1) = IIf(Not String.IsNullOrEmpty(objWatchlist.KITAP), objWatchlist.KITAP, Nothing)

    '                '            'Define count identity
    '                '            For i = 0 To 6
    '                '                If arrIdentity(i, 1) IsNot Nothing Then
    '                '                    countIdentity += 1
    '                '                End If
    '                '            Next

    '                '            'Jika identitas tidak ada yang terisi maka buat hanya 1 baris
    '                '            If countIdentity = 0 Then
    '                '                Dim objNew = New SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL

    '                '                If listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Count > 0 Then
    '                '                    PK_ID = listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Min(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID) - 1
    '                '                End If
    '                '                If PK_ID >= 0 Then
    '                '                    PK_ID = -1
    '                '                End If

    '                '                With objNew
    '                '                    .PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID = PK_ID
    '                '                    .Nama = objWatchlist.PARAMETER_PENCARIAN_NAMA
    '                '                    If Not String.IsNullOrEmpty(objWatchlist.TANGGAL_LAHIR) AndAlso CDate(objWatchlist.TANGGAL_LAHIR) <> DateTime.MinValue Then
    '                '                        .DOB = objWatchlist.TANGGAL_LAHIR
    '                '                    End If

    '                '                    .Birth_Place = objWatchlist.TEMPAT_LAHIR
    '                '                    .FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE = Nothing
    '                '                    .Identity_Number = Nothing

    '                '                    If Not String.IsNullOrEmpty(objWatchlist.ID_PPATK) Then
    '                '                        .FK_WATCHLIST_ID_PPATK = objWatchlist.ID_PPATK
    '                '                    End If

    '                '                End With

    '                '                listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Add(objNew)
    '                '            Else
    '                '                For i = 0 To 6
    '                '                    If arrIdentity(i, 1) IsNot Nothing Then
    '                '                        Dim objNew = New SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL

    '                '                        If listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Count > 0 Then
    '                '                            PK_ID = listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Min(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID) - 1
    '                '                        End If
    '                '                        If PK_ID >= 0 Then
    '                '                            PK_ID = -1
    '                '                        End If

    '                '                        With objNew
    '                '                            .PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID = PK_ID
    '                '                            .Nama = objWatchlist.PARAMETER_PENCARIAN_NAMA
    '                '                            If Not String.IsNullOrEmpty(objWatchlist.TANGGAL_LAHIR) AndAlso CDate(objWatchlist.TANGGAL_LAHIR) <> DateTime.MinValue Then
    '                '                                .DOB = objWatchlist.TANGGAL_LAHIR
    '                '                            End If

    '                '                            .Birth_Place = objWatchlist.TEMPAT_LAHIR
    '                '                            .FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE = arrIdentity(i, 0)
    '                '                            .Identity_Number = arrIdentity(i, 1)

    '                '                            If Not String.IsNullOrEmpty(objWatchlist.ID_PPATK) Then
    '                '                                .FK_WATCHLIST_ID_PPATK = objWatchlist.ID_PPATK
    '                '                            End If

    '                '                        End With

    '                '                        listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL.Add(objNew)
    '                '                    End If
    '                '                Next
    '                '            End If
    '                '        End If
    '                '    Next
    '                'End Using
    '            End If


    '            'Bind to Gridpanel
    '            BindScreeningRequest()

    '            'Clear selection
    '            gp_DownloadFromWatchlist.GetSelectionModel.ClearSelection()

    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub
    '#End Region

#Region "15-Sep-2021 Felix"
    Protected Sub Clear_Search()
        'Clear Screening Request
        listSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL = New List(Of SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL)
        BindScreeningRequest()
        'ListIndicator = New List(Of SiPendarDAL.SIPENDAR_Report_Indicator)
        'bindReportIndicator(StoreReportIndicatorData, ListIndicator)
        'sar_IsSelectedAll.Checked = Nothing
        'Hide Grid Screening Result
        gp_ScreeningRequest.Hidden = False
        gp_ScreeningResult.Hidden = True
        gp_ScreeningResult_All.Hidden = True
        PanelScreeningResult.Hidden = True
        gp_ScreeningResult.GetSelectionModel.ClearSelection()
        'Ari 5 Oct 2021 untuk reset grid di import watchlist
        'gp_DownloadFromWatchlist.GetSelectionModel.ClearSelection()
        'gp_DownloadFromWatchlist.Refresh()
        'filter_DownloadFromWatchlist.ClearFilter()
        'gp_DownloadFromWatchlist.GetStore().ClearFilter()
        fileTemplateScreening.Reset()
        'Clear Screening Status
        df_ScreeningStart.Text = ""
        df_ScreeningStatus.Text = ""
        df_ScreeningFinished.Text = ""
        df_TotalScreening.Text = ""
        df_TotalFound.Text = ""
        df_TotalNotFound.Text = ""

        '29-Sep-2021 Adi
        panel_ScreeningStatus.Hidden = True

        'Show button Screening
        btn_Screening.Hidden = False
        'btn_Save.Hidden = True
        'pnlSiPendarReport.Hidden = True

        ''Clean SiPendar Report fields
        'cmb_JENIS_WATCHLIST_CODE.SetTextValue("")
        'cmb_SUMBER_INFORMASI_KHUSUS_CODE.SetTextValue("")
        'cmb_TINDAK_PIDANA_CODE.SetTextValue("")

        'cmb_SUMBER_TYPE_CODE.SetTextValue("")
        'txt_SUMBER_ID.Value = Nothing
        'txt_Keterangan.Value = Nothing

        ''8-Jul-2021 Adi : Tamabahan permintaan BSIM
        'cmb_SUBMISSION_TYPE_CODE.SetTextValue("")

        'chk_IsGenerateTransaction.Checked = False
        'txt_Transaction_DateFrom.Value = Nothing
        'txt_Transaction_DateTo.Value = Nothing
    End Sub
#End Region

#Region "20-Sep-2021 Daniel"
    Protected Sub WindowScreeningDetail_Close_Click()
        WindowScreeningDetail.Hidden = True
    End Sub

    Protected Sub gp_ScreeningResult_Gridcommand(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                Clear_WindowScreeningDetail()
                WindowScreeningDetail.Hidden = False
                LoadData_WindowScreeningDetail(id)
            Else

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadData_WindowScreeningDetail(id As String)
        Try
            Dim dtResult As DataTable = GetDataTabelSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT(PK_Request_ID, id)
            If dtResult IsNot Nothing Then
                If dtResult.Rows.Count > 0 Then
                    Dim itemrows As Data.DataRow = dtResult.Rows(0)
                    'Data Search
                    If Not IsDBNull(itemrows("FK_WATCHLIST_ID_PPATK")) Then
                        WindowScreeningDetail_IDWatchlistPPATK.Text = Convert.ToString(itemrows("FK_WATCHLIST_ID_PPATK"))
                    End If
                    If Not IsDBNull(itemrows("Nama")) Then
                        WindowScreeningDetail_NameSearch.Text = Convert.ToString(itemrows("Nama"))
                    End If
                    If Not IsDBNull(itemrows("DOB")) Then
                        WindowScreeningDetail_DOBSearch.Text = Convert.ToDateTime(itemrows("DOB")).ToString("dd-MM-yyyy")
                    End If
                    If Not IsDBNull(itemrows("Birth_Place")) Then
                        WindowScreeningDetail_POBSearch.Text = Convert.ToString(itemrows("Birth_Place"))
                    End If
                    'If Not IsDBNull(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE")) Then
                    '    WindowScreeningDetail_IdentityTypeSearch.Text = Convert.ToString(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE"))
                    'End If
                    If Not IsDBNull(itemrows("Identity_Number")) Then
                        WindowScreeningDetail_IdentityNumberSearch.Text = Convert.ToString(itemrows("Identity_Number"))
                    End If
                    If Not IsDBNull(itemrows("TotalSimilarityPct")) Then
                        WindowScreeningDetail_Similarity.Text = Convert.ToString(itemrows("TotalSimilarityPct")) & "%"
                    End If
                    '29-Sep-2021 Adi : Penambahan list of Identity Watchlist
                    'If Not IsDBNull(itemrows("FK_WATCHLIST_ID_PPATK")) Then
                    '    Dim strSQL As String = "SELECT Identity_Type, Identity_Number"
                    '    strSQL += " FROM SIPENDAR_WATCHLIST sw"
                    '    strSQL += " UNPIVOT (Identity_Number FOR Identity_Type IN (NPWP,KTP,PAS,KITAS,SUKET,SIM,KITAP, KIMS)) unpvt"
                    '    strSQL += " WHERE ISNULL(Identity_Number,'')<>'' AND ID_PPATK = '" & itemrows("FK_WATCHLIST_ID_PPATK") & "'"
                    '    Dim dtIdentityWatchlist As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

                    '    gp_IdentityWatchlist.GetStore.DataSource = dtIdentityWatchlist
                    '    gp_IdentityWatchlist.GetStore.DataBind()
                    '    gp_IdentityWatchlist.Hidden = False

                    '    'Hide Identity Type Search dan Identity NUmber Search
                    '    WindowScreeningDetail_IdentityTypeSearch.Hidden = True
                    '    WindowScreeningDetail_IdentityNumberSearch.Hidden = True
                    'Else
                    gp_IdentityWatchlist.Hidden = True
                    'Hide Identity Type Search dan Identity NUmber Search
                    'WindowScreeningDetail_IdentityTypeSearch.Hidden = False
                    WindowScreeningDetail_IdentityNumberSearch.Hidden = False
                    'End If

                    'Data Found

                    'Dim customerinformation As String = Convert.ToString(itemrows("customerinformation"))
                    'If Not String.IsNullOrEmpty(customerinformation) Then
                    '29-Sep-2021 Adi : Penambahan list of Identity Customer
                    'Hide Identity Type Search dan Identity NUmber Search
                    'WindowScreeningDetail_CustomerIdentityType.Hidden = True
                    'WindowScreeningDetail_CustomerIdentityNumber.Hidden = True
                    '''Customer' else 'Non Customer'
                    'If customerinformation = "Customer" Then
                    '    'WindowScreeningDetail_CustomerType.Text = "Customer"
                    '    FieldSet2.Title = "Watchlist Data"
                    '    'WindowScreeningDetail_IsHaveActiveAccount.Hidden = False
                    '    WindowScreeningDetail_GCN.FieldLabel = "GCN"
                    '    WindowScreeningDetail_CIF.FieldLabel = "CIF"
                    '30-Sep-2021 ganti cara dengan panggil SP
                    'If Not IsDBNull(itemrows("IsHaveActiveAccount")) Then
                    '    WindowScreeningDetail_IsHaveActiveAccount.Text = Convert.ToString(itemrows("IsHaveActiveAccount"))
                    'End If
                    'If Not IsDBNull(itemrows("GCN")) Then
                    '    WindowScreeningDetail_GCN.Text = Convert.ToString(itemrows("GCN"))
                    '    Dim drCustomerClosed As DataRow = GetCustomerClosedStatus(itemrows("GCN"))
                    '    If drCustomerClosed IsNot Nothing Then
                    '        If Not IsDBNull(drCustomerClosed("IsClosed")) Then
                    '            If drCustomerClosed("IsClosed") = 0 Then
                    '                WindowScreeningDetail_IsHaveActiveAccount.Value = "Active"
                    '            Else
                    '                If Not IsDBNull(drCustomerClosed("ClosedDate")) Then
                    '                    WindowScreeningDetail_IsHaveActiveAccount.Value = "Closed ( " & CDate(drCustomerClosed("ClosedDate")).ToString("dd-MMM-yyyy") & " )"
                    '                Else
                    '                    WindowScreeningDetail_IsHaveActiveAccount.Value = "Closed"
                    '                End If
                    '            End If
                    '        End If
                    '    End If
                    'End If
                    ''End of 30-Sep-2021 ganti cara dengan panggil SP


                    If Not IsDBNull(itemrows("NAMAWATCHLIST")) Then
                        WindowScreeningDetail_WatchlistName.Text = Convert.ToString(itemrows("NAMAWATCHLIST"))
                    End If
                    If Not IsDBNull(itemrows("DOBWatchlist")) Then
                        WindowScreeningDetail_WatchlistDOB.Text = Convert.ToDateTime(itemrows("DOBWatchlist")).ToString("dd-MM-yyyy")
                    End If
                    If Not IsDBNull(itemrows("Birth_PlaceWatchlist")) Then
                        WindowScreeningDetail_WatchlistPOB.Text = Convert.ToString(itemrows("Birth_PlaceWatchlist"))
                    End If
                    'If Not IsDBNull(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist")) Then
                    '    WindowScreeningDetail_WatchlistIdentityType.Text = Convert.ToString(itemrows("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist"))
                    'End If
                    'If Not IsDBNull(itemrows("Identity_NumberWatchlist")) Then
                    '    WindowScreeningDetail_WatchlistIdentityNumber.Text = Convert.ToString(itemrows("Identity_NumberWatchlist"))
                    'End If

                    '29-Sep-2021 Adi : Penambahan List of Identity Watchlist
                    If Not IsDBNull(itemrows("FK_WATCHLIST_ID_PPATK")) Then
                        Dim strSQL As String = "SELECT Identity_Type, Identity_Number"
                        strSQL += " FROM SIPENDAR_WATCHLIST sw"
                        strSQL += " UNPIVOT (Identity_Number FOR Identity_Type IN (NPWP,KTP,PAS,KITAS,SUKET,SIM,KITAP, KIMS)) unpvt"
                        strSQL += " WHERE ISNULL(Identity_Number,'')<>'' AND ID_PPATK = '" & itemrows("FK_WATCHLIST_ID_PPATK") & "'"
                        Dim dtIdentityWatchlist As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

                        gp_IdentityWatchlistResult.GetStore.DataSource = dtIdentityWatchlist
                        gp_IdentityWatchlistResult.GetStore.DataBind()
                        gp_IdentityWatchlistResult.Hidden = False

                        ''Hide Identity Type Search dan Identity NUmber Search
                        'WindowScreeningDetail_IdentityTypeSearch.Hidden = True
                        'WindowScreeningDetail_IdentityNumberSearch.Hidden = True
                    Else
                        gp_IdentityWatchlistResult.Hidden = True
                        'Hide Identity Type Search dan Identity NUmber Search
                        'WindowScreeningDetail_IdentityTypeSearch.Hidden = False
                        'WindowScreeningDetail_WatchlistIdentityNumber.Hidden = False
                    End If

                End If
            End If
            '    End If
            'End If

            'Using objdb As New SiPendarEntities
            '    Dim objResult As SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT = objdb.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = id).FirstOrDefault
            '    If objResult IsNot Nothing Then
            '        If objResult.TotalSimilarity IsNot Nothing Then
            '            WindowScreeningDetail_Similarity.Text = (objResult.TotalSimilarity * 100).ToString() & "%"
            '        End If
            '        If objResult.FK_WATCHLIST_ID_PPATK IsNot Nothing Then
            '            WindowScreeningDetail_IDWatchlistPPATK.Text = objResult.FK_WATCHLIST_ID_PPATK.ToString()
            '        End If
            '        If objResult.Nama IsNot Nothing Then
            '            WindowScreeningDetail_NameSearch.Text = objResult.Nama.ToString()
            '        End If
            '        If objResult.DOB IsNot Nothing Then
            '            WindowScreeningDetail_DOBSearch.Text = objResult.DOB.Value.ToString("dd-MM-yyyy")
            '        End If
            '        If objResult.Birth_Place IsNot Nothing Then
            '            WindowScreeningDetail_POBSearch.Text = objResult.Birth_Place.ToString()
            '        End If
            '        If objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE IsNot Nothing Then
            '            WindowScreeningDetail_IdentityTypeSearch.Text = objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE.ToString()
            '        End If
            '        If objResult.Identity_Number IsNot Nothing Then
            '            WindowScreeningDetail_IdentityNumberSearch.Text = objResult.Identity_Number.ToString()
            '        End If
            '        If objResult.GCN IsNot Nothing Then
            '            WindowScreeningDetail_GCN.Text = objResult.GCN.ToString()
            '        End If
            '        If objResult.NAMAWATCHLIST IsNot Nothing Then
            '            WindowScreeningDetail_CustomerName.Text = objResult.NAMAWATCHLIST.ToString()
            '        End If
            '        If objResult.DOBWatchlist IsNot Nothing Then
            '            WindowScreeningDetail_CustomerDOB.Text = objResult.DOBWatchlist.Value.ToString("dd-MM-yyyy")
            '        End If
            '        If objResult.Birth_PlaceWatchlist IsNot Nothing Then
            '            WindowScreeningDetail_CustomerPOB.Text = objResult.Birth_PlaceWatchlist.ToString()
            '        End If
            '        If objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist IsNot Nothing Then
            '            WindowScreeningDetail_CustomerIdentityType.Text = objResult.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist.ToString()
            '        End If
            '        If objResult.Identity_NumberWatchlist IsNot Nothing Then
            '            WindowScreeningDetail_CustomerIdentityNumber.Text = objResult.Identity_NumberWatchlist.ToString()
            '        End If
            '    End If
            'End Using

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Clear_WindowScreeningDetail()
        Try
            WindowScreeningDetail_Similarity.Clear()
            WindowScreeningDetail_IDWatchlistPPATK.Clear()
            WindowScreeningDetail_NameSearch.Clear()
            WindowScreeningDetail_DOBSearch.Clear()
            WindowScreeningDetail_POBSearch.Clear()
            'WindowScreeningDetail_IdentityTypeSearch.Clear()
            WindowScreeningDetail_IdentityNumberSearch.Clear()
            WindowScreeningDetail_IDWatchlistPPATK.Clear()
            WindowScreeningDetail_WatchlistName.Clear()
            WindowScreeningDetail_WatchlistDOB.Clear()
            WindowScreeningDetail_WatchlistPOB.Clear()
            'WindowScreeningDetail_WatchlistIdentityType.Clear()
            'WindowScreeningDetail_WatchlistIdentityNumber.Clear()
            'WindowScreeningDetail_GCN.Clear()
            'WindowScreeningDetail_CustomerName.Clear()
            'WindowScreeningDetail_CustomerDOB.Clear()
            'WindowScreeningDetail_CustomerPOB.Clear()
            'WindowScreeningDetail_CustomerIdentityType.Clear()
            'WindowScreeningDetail_CustomerIdentityNumber.Clear()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadColumnCommandScreeningResult()
        Try
            ColumnActionLocation(gp_ScreeningResult, gp_ScreeningResult_CommandColumn)
            ColumnActionLocation(gp_ScreeningResult_All, gp_ScreeningResult_All_CommandColumn)
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub
#End Region

#Region "24-Sep-2021 Daniel"
    ''' <summary>
    ''' Function For Get SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT as DataTabel
    ''' </summary>
    ''' <param name="PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID">PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID Value</param>
    ''' <param name="PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID">PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID Value, This Value Optional For Get Specific Result Data (Primary Key Of SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT)</param>
    ''' <returns></returns>
    Protected Function GetDataTabelSIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT(PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID As Long, Optional PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID As String = "") As DataTable
        Try
            Dim data As New DataTable
            'Dim stringquery As String = " SELECT result.*, (result.TotalSimilarity*100) AS TotalSimilarityPct, custaccount.[CountAccount], case when custaccount.CountAccount > 0 then 'Yes' else 'No' " &
            '    " end as IsHaveActiveAccount FROM SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT result left join ( select distinct cust.GCN, count(cust.GCN) as [CountAccount] from goAML_Ref_Customer cust join " &
            '    " goAML_Ref_Account acc on acc.client_number =  cust.CIF join SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT resultget on resultget.GCN = cust.GCN where resultget.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID= " &
            '    PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID & " and acc.status_code <> 'TTP' and resultget.GCN is not null group by cust.GCN ) custaccount on result.GCN = custaccount.GCN WHERE " &
            '    " result.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID= " & PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID
            Dim stringquery As String = " SELECT result.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID, result.Nama, result.DOB, result.Birth_Place, result.Identity_Number, result.FK_WATCHLIST_ID_PPATK,  " &
            " result.NAMAWATCHLIST, result.DOBWatchlist, result.Birth_PlaceWatchlist, result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist, " &
            " result.Identity_NumberWatchlist, result.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE, (result.TotalSimilarity*100) AS TotalSimilarityPct " &
            " FROM SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT result " &
            " WHERE result.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID = " & PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID
            If Not String.IsNullOrEmpty(PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID) Then
                stringquery = stringquery & " and result.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = " & PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID
            End If
            stringquery = stringquery & " ORDER BY TotalSimilarityPct Desc, result.Nama"
            data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
            Return data
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function
#End Region

    '#Region "30-Sep-2021 Adi : Get Customer Closed and Closing Date"
    '    Protected Function GetCustomerClosedStatus(strGCN As String) As DataRow
    '        Try

    '            Dim param(0) As SqlParameter

    '            param(0) = New SqlParameter
    '            param(0).ParameterName = "@GCN"
    '            param(0).Value = strGCN
    '            param(0).DbType = SqlDbType.VarChar

    '            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_GetCustomerClosedStatus_ByGCN", param)

    '            Return drResult
    '        Catch ex As Exception
    '            Throw ex
    '            Return Nothing
    '        End Try
    '    End Function
    '#End Region

#Region "Ariswara 21 Sept 2021"


    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            Dim DataScreeningAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT Nama,DOB,Birth_Place,Identity_Number,FK_WATCHLIST_ID_PPATK,NAMAWATCHLIST,DOBWatchlist,Birth_PlaceWatchlist,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist,Identity_NumberWatchlist, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT WHERE PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID=" & PK_Request_ID & " ORDER BY TotalSimilarityPct Desc,  Nama",)
            If DataScreeningAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            End If
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = DataScreeningAll

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("SipendarScreeningCustomerProspectResult")
                            'objtbl.Columns.Remove("PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID")
                            'objtbl.Columns.Remove("PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID")
                            'objtbl.Columns.Remove("UserIDRequest")
                            'objtbl.Columns.Remove("CreatedDate")
                            'objtbl.Columns.Remove("FK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_TYPE_ID")
                            'objtbl.Columns.Remove("_Similarity")
                            'objtbl.Columns.Remove("_Confidence")
                            'objtbl.Columns.Remove("_Similarity_Nama")
                            'objtbl.Columns.Remove("_Similarity_DOB")
                            'objtbl.Columns.Remove("IsNeedToReportSIPENDAR")
                            'objtbl.Columns.Remove("Approved")
                            'objtbl.Columns.Remove("ApprovedBy")
                            'objtbl.Columns.Remove("Nationality")
                            'objtbl.Columns.Remove("NationalityCustomer")
                            'objtbl.Columns.Remove("_Similarity_Birth_Place")
                            'objtbl.Columns.Remove("_Similarity_Identity_Number")
                            'objtbl.Columns.Remove("_Similarity_Nationality")
                            'objtbl.Columns.Remove("PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID")
                            objtbl.Columns("Nama").SetOrdinal(0)
                            objtbl.Columns("DOB").SetOrdinal(1)
                            objtbl.Columns("Birth_Place").SetOrdinal(2)
                            objtbl.Columns("Identity_Number").SetOrdinal(3)
                            'objtbl.Columns("GCN").SetOrdinal(6)
                            objtbl.Columns("NAMAWATCHLIST").SetOrdinal(4)
                            objtbl.Columns("DOBWatchlist").SetOrdinal(5)
                            objtbl.Columns("Birth_PlaceWatchlist").SetOrdinal(6)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").SetOrdinal(7)
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(8)
                            objtbl.Columns("Identity_NumberWatchlist").SetOrdinal(9)
                            objtbl.Columns("TotalSimilarityPct").SetOrdinal(10)
                            objtbl.Columns("Nama").ColumnName = "Name Search Data"
                            objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                            'objtbl.Columns("GCN").ColumnName = "GCN Watchlist Data"
                            objtbl.Columns("NAMAWATCHLIST").ColumnName = "Name Watchlist Data"
                            objtbl.Columns("DOBWatchlist").ColumnName = "DOB Watchlist Data"
                            objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                            objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                            objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                            objtbl.Columns("Identity_NumberWatchlist").ColumnName = "Identity Number Watchlist Data"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").ColumnName = "ID Type Watchlist"

                            objtbl.Columns("Birth_PlaceWatchlist").ColumnName = "Birth Place Watchlist Data"
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Sipendar_SCREENING_CUSTOMER_PROSPECT_REQUEST.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = DataScreeningAll

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        objtbl.Columns("Nama").SetOrdinal(0)
                        objtbl.Columns("DOB").SetOrdinal(1)
                        objtbl.Columns("Birth_Place").SetOrdinal(2)
                        objtbl.Columns("Identity_Number").SetOrdinal(3)
                        'objtbl.Columns("GCN").SetOrdinal(6)
                        objtbl.Columns("NAMAWATCHLIST").SetOrdinal(4)
                        objtbl.Columns("DOBWatchlist").SetOrdinal(5)
                        objtbl.Columns("Birth_PlaceWatchlist").SetOrdinal(6)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").SetOrdinal(7)
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(8)
                        objtbl.Columns("Identity_NumberWatchlist").SetOrdinal(9)
                        objtbl.Columns("TotalSimilarityPct").SetOrdinal(10)
                        objtbl.Columns("Nama").ColumnName = "Name Search Data"
                        objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                        'objtbl.Columns("GCN").ColumnName = "GCN Watchlist Data"
                        objtbl.Columns("NAMAWATCHLIST").ColumnName = "Name Watchlist Data"
                        objtbl.Columns("DOBWatchlist").ColumnName = "DOB Watchlist Data"
                        objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                        objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                        objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                        objtbl.Columns("Identity_NumberWatchlist").ColumnName = "Identity Number Watchlist Data"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").ColumnName = "ID Type Watchlist"

                        objtbl.Columns("Birth_PlaceWatchlist").ColumnName = "Birth Place Watchlist Data"
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Sipendar_SCREENING_CUSTOMER_PROSPECT_REQUEST.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            Dim pagecurrent As Integer = e.ExtraParams("currentPage")

            Dim DataScreeningCurrentPage As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "DECLARE @PageNumber AS INT, @RowspPage AS INT SET @PageNumber =" & pagecurrent & " SET @RowspPage = 10 SELECT Nama,DOB,Birth_Place,Identity_Number,FK_WATCHLIST_ID_PPATK,NAMAWATCHLIST,DOBWatchlist,Birth_PlaceWatchlist,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist,Identity_NumberWatchlist, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT WHERE PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID=" & PK_Request_ID & " ORDER BY TotalSimilarityPct Desc, Nama OFFSET ((@PageNumber - 1) * @RowspPage) ROWS FETCH NEXT @RowspPage ROWS ONLY",)
            If DataScreeningCurrentPage.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            End If
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = DataScreeningCurrentPage

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("Sipendar_SCREENING_CUSTOMER_PROSPECT_REQUEST")
                            objtbl.Columns("Nama").SetOrdinal(0)
                            objtbl.Columns("DOB").SetOrdinal(1)
                            objtbl.Columns("Birth_Place").SetOrdinal(2)
                            objtbl.Columns("Identity_Number").SetOrdinal(3)
                            'objtbl.Columns("GCN").SetOrdinal(6)
                            objtbl.Columns("NAMAWATCHLIST").SetOrdinal(4)
                            objtbl.Columns("DOBWatchlist").SetOrdinal(5)
                            objtbl.Columns("Birth_PlaceWatchlist").SetOrdinal(6)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").SetOrdinal(7)
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(8)
                            objtbl.Columns("Identity_NumberWatchlist").SetOrdinal(9)
                            objtbl.Columns("TotalSimilarityPct").SetOrdinal(10)
                            objtbl.Columns("Nama").ColumnName = "Name Search Data"
                            objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                            'objtbl.Columns("GCN").ColumnName = "GCN Watchlist Data"
                            objtbl.Columns("NAMAWATCHLIST").ColumnName = "Name Watchlist Data"
                            objtbl.Columns("DOBWatchlist").ColumnName = "DOB Watchlist Data"
                            objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                            objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                            objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                            objtbl.Columns("Identity_NumberWatchlist").ColumnName = "Identity Number Watchlist Data"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").ColumnName = "ID Type Watchlist"

                            objtbl.Columns("Birth_PlaceWatchlist").ColumnName = "Birth Place Watchlist Data"
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Sipendar_SCREENING_CUSTOMER_PROSPECT_REQUEST.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = DataScreeningCurrentPage

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        objtbl.Columns("Nama").SetOrdinal(0)
                        objtbl.Columns("DOB").SetOrdinal(1)
                        objtbl.Columns("Birth_Place").SetOrdinal(2)
                        objtbl.Columns("Identity_Number").SetOrdinal(3)
                        'objtbl.Columns("GCN").SetOrdinal(6)
                        objtbl.Columns("NAMAWATCHLIST").SetOrdinal(4)
                        objtbl.Columns("DOBWatchlist").SetOrdinal(5)
                        objtbl.Columns("Birth_PlaceWatchlist").SetOrdinal(6)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").SetOrdinal(7)
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(8)
                        objtbl.Columns("Identity_NumberWatchlist").SetOrdinal(9)
                        objtbl.Columns("TotalSimilarityPct").SetOrdinal(10)
                        objtbl.Columns("Nama").ColumnName = "Name Search Data"
                        objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                        'objtbl.Columns("GCN").ColumnName = "GCN Watchlist Data"
                        objtbl.Columns("NAMAWATCHLIST").ColumnName = "Name Watchlist Data"
                        objtbl.Columns("DOBWatchlist").ColumnName = "DOB Watchlist Data"
                        objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                        objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                        objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                        objtbl.Columns("Identity_NumberWatchlist").ColumnName = "Identity Number Watchlist Data"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").ColumnName = "ID Type Watchlist"

                        objtbl.Columns("Birth_PlaceWatchlist").ColumnName = "Birth Place Watchlist Data"
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Sipendar_SCREENING_CUSTOMER_PROSPECT_REQUEST.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    Protected Sub ExportSelectedExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            Dim smScreeningResult As RowSelectionModel = TryCast(gp_ScreeningResult.GetSelectionModel(), RowSelectionModel)
            Dim selected As RowSelectionModel = gp_ScreeningResult.SelectionModel.Primary
            If selected.SelectedRows.Count = 0 Then

                Throw New Exception("Minimal 1 data harus dipilih!")
            End If
            Dim listScreeninnigResultPK As New List(Of SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT)
            For Each item As SelectedRow In smScreeningResult.SelectedRows
                Dim recordID = item.RecordID.ToString
                Dim objNew = New SiPendarDAL.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT
                With objNew
                    .PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = recordID
                End With

                listScreeninnigResultPK.Add(objNew)
            Next
            Dim pkscreeningresultselected As String = ""
            Dim listScreeninnigResult As New List(Of SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT)
            For Each item In listScreeninnigResultPK
                Dim cariscreeningrequest As New SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT
                Using objdb As New SiPendarEntities
                    cariscreeningrequest = objdb.SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT.Where(Function(x) x.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = item.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID).FirstOrDefault()
                End Using
                If cariscreeningrequest IsNot Nothing Then
                    listScreeninnigResult.Add(New SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT() With {
                  .PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID = cariscreeningrequest.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID,
                  .Nama = cariscreeningrequest.Nama,
                  .DOBWatchlist = cariscreeningrequest.DOBWatchlist,
                  .Birth_PlaceWatchlist = cariscreeningrequest.Birth_PlaceWatchlist,
                  .Identity_Number = cariscreeningrequest.Identity_Number,
                  .FK_WATCHLIST_ID_PPATK = cariscreeningrequest.FK_WATCHLIST_ID_PPATK,
                  .NAMAWATCHLIST = cariscreeningrequest.NAMAWATCHLIST,
                  .DOB = cariscreeningrequest.DOB,
                  .Birth_Place = cariscreeningrequest.Birth_Place,
                  .Identity_NumberWatchlist = cariscreeningrequest.Identity_Number,
                  .TotalSimilarity = cariscreeningrequest.TotalSimilarity * 100,
                  .FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist = cariscreeningrequest.FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist
                  })
                End If

                pkscreeningresultselected = pkscreeningresultselected & "," & item.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID
            Next
            pkscreeningresultselected = pkscreeningresultselected.Remove(0, 1)

            Dim strquery As String = "SELECT Nama,DOB,Birth_Place,Identity_Number,FK_WATCHLIST_ID_PPATK,NAMAWATCHLIST,DOBWatchlist,Birth_PlaceWatchlist,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist,Identity_NumberWatchlist, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT WHERE PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID in(" & pkscreeningresultselected & ") ORDER BY TotalSimilarityPct Desc,  Nama"
            Dim DataScreeningSelectResult As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquery,)
            Dim tablescreeningresult As New DataTable
            Dim fields() As FieldInfo = GetType(SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT).GetFields(BindingFlags.Instance Or BindingFlags.Static Or BindingFlags.NonPublic Or BindingFlags.Public)
            For Each field As FieldInfo In fields

                tablescreeningresult.Columns.Add(field.Name, System.Type.GetType("System.String"))
            Next
            For Each item As SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT In listScreeninnigResult
                Dim row As DataRow = tablescreeningresult.NewRow()
                For Each field As FieldInfo In fields
                    row(field.Name) = field.GetValue(item)

                Next

                tablescreeningresult.Rows.Add(row)
            Next
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    tablescreeningresult.DefaultView.Sort = "_TotalSimilarity Desc"
                    Using objtbl As Data.DataTable = DataScreeningSelectResult

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("SipendarScreeningCustomerProspectResult")
                            'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID")
                            'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID")
                            'objtbl.Columns.Remove("_UserIDRequest")
                            'objtbl.Columns.Remove("_CreatedDate")
                            'objtbl.Columns.Remove("_FK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_TYPE_ID")
                            'objtbl.Columns.Remove("_C_Similarity")
                            'objtbl.Columns.Remove("_C_Confidence")
                            'objtbl.Columns.Remove("_C_Similarity_Nama")
                            'objtbl.Columns.Remove("_C_Similarity_DOB")
                            'objtbl.Columns.Remove("_IsNeedToReportSIPENDAR")
                            'objtbl.Columns.Remove("_Approved")
                            'objtbl.Columns.Remove("_ApprovedBy")
                            'objtbl.Columns.Remove("_Nationality")
                            'objtbl.Columns.Remove("_NationalityCustomer")
                            'objtbl.Columns.Remove("_C_Similarity_Birth_Place")
                            'objtbl.Columns.Remove("_C_Similarity_Identity_Number")
                            'objtbl.Columns.Remove("_C_Similarity_Nationality")
                            'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID")
                            'objtbl.Columns("_Nama").ColumnName = "Name Search Data"
                            'objtbl.Columns("_DOB").ColumnName = "DOB Search Data"
                            'objtbl.Columns("_GCN").ColumnName = "GCN Search Data"
                            'objtbl.Columns("_NAMAWATCHLIST").ColumnName = "Name Watchlist Data"
                            'objtbl.Columns("_DOBWatchlist").ColumnName = "DOB Watchlist Data"
                            'objtbl.Columns("_TotalSimilarity").ColumnName = "Similarity"
                            'objtbl.Columns("_Identity_Number").ColumnName = "Identity Number Search Data"
                            'objtbl.Columns("_Birth_Place").ColumnName = "Birth Place Search Data"
                            'objtbl.Columns("_Identity_NumberWatchlist").ColumnName = "Identity Number Watchlist Data"
                            'objtbl.Columns("_FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").ColumnName = "ID Type Search Data"
                            'objtbl.Columns("_Birth_PlaceWatchlist").ColumnName = "Birth Place Watchlist Data"
                            'objtbl.Columns("_FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            objtbl.Columns("Nama").SetOrdinal(0)
                            objtbl.Columns("DOB").SetOrdinal(1)
                            objtbl.Columns("Birth_Place").SetOrdinal(2)
                            objtbl.Columns("Identity_Number").SetOrdinal(3)
                            'objtbl.Columns("GCN").SetOrdinal(6)
                            objtbl.Columns("NAMAWATCHLIST").SetOrdinal(4)
                            objtbl.Columns("DOBWatchlist").SetOrdinal(5)
                            objtbl.Columns("Birth_PlaceWatchlist").SetOrdinal(6)
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").SetOrdinal(7)
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(8)
                            objtbl.Columns("Identity_NumberWatchlist").SetOrdinal(9)
                            objtbl.Columns("TotalSimilarityPct").SetOrdinal(10)
                            objtbl.Columns("Nama").ColumnName = "Name Search Data"
                            objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                            'objtbl.Columns("GCN").ColumnName = "GCN Watchlist Data"
                            objtbl.Columns("NAMAWATCHLIST").ColumnName = "Name Watchlist Data"
                            objtbl.Columns("DOBWatchlist").ColumnName = "DOB Watchlist Data"
                            objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                            objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                            objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                            objtbl.Columns("Identity_NumberWatchlist").ColumnName = "Identity Number Watchlist Data"
                            objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").ColumnName = "ID Type Watchlist"

                            objtbl.Columns("Birth_PlaceWatchlist").ColumnName = "Birth Place Watchlist Data"
                            objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Sipendar_SCREENING_CUSTOMER_PROSPECT_REQUEST.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = DataScreeningSelectResult

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_ScreeningResult.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID")
                        'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID")
                        'objtbl.Columns.Remove("_UserIDRequest")
                        'objtbl.Columns.Remove("_CreatedDate")
                        'objtbl.Columns.Remove("_FK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_TYPE_ID")
                        'objtbl.Columns.Remove("_C_Similarity")
                        'objtbl.Columns.Remove("_C_Confidence")
                        'objtbl.Columns.Remove("_C_Similarity_Nama")
                        'objtbl.Columns.Remove("_C_Similarity_DOB")
                        'objtbl.Columns.Remove("_IsNeedToReportSIPENDAR")
                        'objtbl.Columns.Remove("_Approved")
                        'objtbl.Columns.Remove("_ApprovedBy")
                        'objtbl.Columns.Remove("_Nationality")
                        'objtbl.Columns.Remove("_NationalityCustomer")
                        'objtbl.Columns.Remove("_C_Similarity_Birth_Place")
                        'objtbl.Columns.Remove("_C_Similarity_Identity_Number")
                        'objtbl.Columns.Remove("_C_Similarity_Nationality")
                        'objtbl.Columns.Remove("_PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID")
                        'objtbl.Columns("_Nama").ColumnName = "Name Search Data"
                        'objtbl.Columns("_DOB").ColumnName = "DOB Search Data"
                        'objtbl.Columns("_GCN").ColumnName = "GCN Search Data"
                        'objtbl.Columns("_NAMAWATCHLIST").ColumnName = "Name Watchlist Data"
                        'objtbl.Columns("_DOBWatchlist").ColumnName = "DOB Watchlist Data"
                        'objtbl.Columns("_TotalSimilarity").ColumnName = "Similarity"
                        'objtbl.Columns("_Identity_Number").ColumnName = "Identity Number Search Data"
                        'objtbl.Columns("_Birth_Place").ColumnName = "Birth Place Search Data"
                        'objtbl.Columns("_Identity_NumberWatchlist").ColumnName = "Identity Number Watchlist Data"
                        'objtbl.Columns("_FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").ColumnName = "ID Type Search Data"
                        'objtbl.Columns("_Birth_PlaceWatchlist").ColumnName = "Birth Place Watchlist Data"
                        'objtbl.Columns("_FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        objtbl.Columns("Nama").SetOrdinal(0)
                        objtbl.Columns("DOB").SetOrdinal(1)
                        objtbl.Columns("Birth_Place").SetOrdinal(2)
                        objtbl.Columns("Identity_Number").SetOrdinal(3)
                        'objtbl.Columns("GCN").SetOrdinal(6)
                        objtbl.Columns("NAMAWATCHLIST").SetOrdinal(4)
                        objtbl.Columns("DOBWatchlist").SetOrdinal(5)
                        objtbl.Columns("Birth_PlaceWatchlist").SetOrdinal(6)
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").SetOrdinal(7)
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").SetOrdinal(8)
                        objtbl.Columns("Identity_NumberWatchlist").SetOrdinal(9)
                        objtbl.Columns("TotalSimilarityPct").SetOrdinal(10)
                        objtbl.Columns("Nama").ColumnName = "Name Search Data"
                        objtbl.Columns("DOB").ColumnName = "DOB Search Data"
                        'objtbl.Columns("GCN").ColumnName = "GCN Watchlist Data"
                        objtbl.Columns("NAMAWATCHLIST").ColumnName = "Name Watchlist Data"
                        objtbl.Columns("DOBWatchlist").ColumnName = "DOB Watchlist Data"
                        objtbl.Columns("TotalSimilarityPct").ColumnName = "Similarity"
                        objtbl.Columns("Identity_Number").ColumnName = "Identity Number Search Data"
                        objtbl.Columns("Birth_Place").ColumnName = "Birth Place Search Data"
                        objtbl.Columns("Identity_NumberWatchlist").ColumnName = "Identity Number Watchlist Data"
                        objtbl.Columns("FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist").ColumnName = "ID Type Watchlist"

                        objtbl.Columns("Birth_PlaceWatchlist").ColumnName = "Birth Place Watchlist Data"
                        objtbl.Columns("FK_WATCHLIST_ID_PPATK").ColumnName = "ID Watchlist PPATK"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Sipendar_SCREENING_CUSTOMER_PROSPECT_REQUEST.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub


    Protected Sub GetCurrentPage(sender As Object, e As DirectEventArgs)
        Ext.Net.X.Msg.Alert("Current Page", e.ExtraParams("currentPage")).Show()

    End Sub

#End Region

    'Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Identifications(FK_SIPENDAR_StakeHolder_NonCustomer_ID As String) As DataTable
    '    Try
    '        Dim data As New DataTable
    '        Dim stringquery As String = " select Type as Identity_Type, Number as Identity_Number, Issue_Date, Expiry_Date, Issued_By, Issued_Country, Comments " &
    '            " from SIPENDAR_StakeHolder_NonCustomer_Identifications where FK_SIPENDAR_StakeHolder_NonCustomer_ID = " & FK_SIPENDAR_StakeHolder_NonCustomer_ID
    '        data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
    '        Return data
    '    Catch ex As Exception
    '        Throw ex
    '        'Return New DataTable
    '    End Try
    'End Function

    'Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Addresses(FK_SIPENDAR_StakeHolder_NonCustomer_ID As String) As DataTable
    '    Try
    '        Dim data As New DataTable
    '        Dim stringquery As String = " select * from SIPENDAR_StakeHolder_NonCustomer_Addresses where FK_SIPENDAR_StakeHolder_NonCustomer_ID = " & FK_SIPENDAR_StakeHolder_NonCustomer_ID
    '        data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
    '        Return data
    '    Catch ex As Exception
    '        Throw ex
    '        'Return New DataTable
    '    End Try
    'End Function

    'Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer_Phones(FK_SIPENDAR_StakeHolder_NonCustomer_ID As String) As DataTable
    '    Try
    '        Dim data As New DataTable
    '        Dim stringquery As String = " select * from SIPENDAR_StakeHolder_NonCustomer_Phones where FK_SIPENDAR_StakeHolder_NonCustomer_ID = " & FK_SIPENDAR_StakeHolder_NonCustomer_ID
    '        data = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
    '        Return data
    '    Catch ex As Exception
    '        Throw ex
    '        'Return New DataTable
    '    End Try
    'End Function

    'Protected Function GetDataTabelSIPENDAR_StakeHolder_NonCustomer(Unique_Key As String) As DataRow
    '    Try
    '        Unique_Key = Replace(Unique_Key, "'", "''")
    '        Dim stringquery As String = " select top 1 * from SIPENDAR_StakeHolder_NonCustomer where Unique_Key = '" & Unique_Key & "' "
    '        Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery, Nothing)
    '        Return drResult
    '    Catch ex As Exception
    '        Throw ex
    '        'Return New DataTable
    '    End Try
    'End Function

    Protected Function GetCountryName(Kode As String) As String
        Try
            Dim tempstring = ""
            Kode = Replace(Kode, "'", "''")
            Dim stringquery As String = " select top 1 Keterangan from goAML_Ref_Nama_Negara where Kode = '" & Kode & "' "
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery, Nothing)
            If drResult IsNot Nothing Then
                If Not IsDBNull(drResult("Keterangan")) Then
                    tempstring = Convert.ToString(drResult("Keterangan"))
                End If
            End If
            Return tempstring
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function

    Protected Function GetDataSIPENDAR_GLOBAL_PARAMETER(ID_Paramter As Long) As SIPENDAR_GLOBAL_PARAMETER
        Try

            Dim objGlobalParameter As New SiPendarDAL.SIPENDAR_GLOBAL_PARAMETER
            Using objDbSipendar As New SiPendarDAL.SiPendarEntities
                objGlobalParameter = objDbSipendar.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = ID_Paramter).FirstOrDefault
            End Using
            Return objGlobalParameter
        Catch ex As Exception
            Throw ex
            'Return New DataTable
        End Try
    End Function

    Protected Function GetDataWatchlistByPKWatchlist(PK As String) As DataRow
        Try
            Dim strSQL As String = "select top 1 * from SIPENDAR_WATCHLIST where PK_SIPENDAR_WATCHLIST_ID =" & PK
            Dim tempdatarow As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            Return tempdatarow
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
End Class



