﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="SipendarScreeningCustomerProspect.aspx.vb" Inherits="SIPENDAR_ScreeningCustomerProspect_SipendarScreeningCustomerProspect" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        //Show or hide Selection Model gridpanel watchlist
        var showSMWatchlist = function () {
            var grid = App.gp_DownloadFromWatchlist,
                sm = grid.getSelectionModel();



            if (sm.injectCheckboxWatchlist === false) {
                sm.injectCheckboxWatchlist = 0;
                sm.addCheckbox(grid.getView());
            } else {
                grid.getView().headerCt.items.getAt(0).show();
            }
        };



        var hideSMWatchlist = function () {
            App.gp_DownloadFromWatchlist.getView().headerCt.items.getAt(0).hide();
        };
        var updateMask = function (text) {
            App.df_ScreeningStatus.setValue(text);
        };
    </script>
    <style type="text/css">
        .text-wrapper .x-form-display-field {
            word-break: break-word;
            word-wrap: break-word;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

   



    <%-- Pop Up Window Screening Upload --%>
    <ext:Window ID="Window_ScreeningUpload" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" Layout="FitLayout" Title="Upload Screening Request">
        <Items>
            <ext:FormPanel ID="Window_Panel_AML_WATCHLIST_ALIAS" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:HyperlinkButton ID="btn_DownloadTemplate" MarginSpec="10 0" runat="server" Text="Export Template">
                        <DirectEvents>
                            <Click OnEvent="btn_DownloadTemplate_Click"></Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                    <ext:FileUploadField runat="server" ID="fileTemplateScreening" FieldLabel="Input File" Accept=".xlsx" AnchorHorizontal="100%" AllowBlank="false" />
                  <%--  <ext:DisplayField runat="server" FieldLabel="Note" Text="* Imported Data With Wrong ID PPATK Will Not Be Added on List" AnchorHorizontal="100%"></ext:DisplayField>
  --%>              </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_ScreeningUpload_Save" runat="server" Icon="DiskUpload" Text="Import">
                <DirectEvents>
                    <Click OnEvent="btn_ScreeningUpload_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_ScreeningUpload_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_ScreeningUpload_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.5});" />

            <Resize Handler="#{Window_ScreeningUpload}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Screening Upload --%>

    <%-- Pop Up Window Screening Add --%>
    <ext:Window ID="Window_ScreeningAdd" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout" Title="Add Screening Request">
        <Items>
            <ext:FormPanel ID="FormPanel1" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:TextField ID="txt_Nama" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Name" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <ext:DateField ID="txt_DOB" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="50%" Format="dd-MMM-yyyy"></ext:DateField>
                    <ext:TextField ID="txt_Birth_Place" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="80%" MaxLength="500" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="txt_Identity_Number" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Identity Number" AnchorHorizontal="80%" MaxLength="500" EnforceMaxLength="true"></ext:TextField>
                    <%--  <ext:NumberField ID="txt_FK_WATCHLIST_ID_PPATK" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="ID Watchlist PPATK *" AnchorHorizontal="80%" MaxLength="500" EnforceMaxLength="true"></ext:NumberField>--%>

                    <%-- <ext:FieldSet runat="server" AnchorHorizontal="100%" Layout="AnchorLayout" MarginSpec="10 0">
                        <Items>
                           <ext:DisplayField ID="txt_Info" runat="server" AnchorHorizontal="100%" Text="*) Notes : ID Watchlist PPATK wajib diisi jika ingin membuat Profile Pengayaan" MarginSpec="10 0"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>--%>
                </Items>
            </ext:FormPanel>

        </Items>
        <Buttons>
            <ext:Button ID="btn_ScreeningAdd_Save" runat="server" Icon="Accept" Text="OK">
                <DirectEvents>
                    <Click OnEvent="btn_ScreeningAdd_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_ScreeningAdd_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_ScreeningAdd_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.7});" />

            <Resize Handler="#{Window_ScreeningAdd}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Screening Upload --%>

    <ext:FormPanel ID="FormPanelInput" BodyPadding="5" AutoScroll="true" Layout="AnchorLayout" AnchorHorizontal="90%" ButtonAlign="Center" runat="server" Height="300" Title="">
        <Items>


            <%--Grid Screening Request--%>
            <ext:GridPanel ID="gp_ScreeningRequest" runat="server" Title="Screening Request" Border="true" MinHeight="250">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <TopBar>
                    <ext:Toolbar runat="server" ID="Toolbar1">
                        <Items>
                            <ext:Button ID="btn_AddScreeningRequest" runat="server" Icon="Add" Text="Add Screening Request">
                                <DirectEvents>
                                    <Click OnEvent="btn_AddScreeningRequest_Click">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="200"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btn_UploadScreeningRequest" runat="server" Icon="DiskUpload" Text="Upload Screening Request">
                                <DirectEvents>
                                    <Click OnEvent="btn_UploadScreeningRequest_Click">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="200"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="Store3" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="model3" IDProperty="PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID">
                                <Fields>
                                    <ext:ModelField Name="Nama" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="DOB" Type="Date"></ext:ModelField>
                                    <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_WATCHLIST_ID_PPATK" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No." MinWidth="60"></ext:RowNumbererColumn>
                        <ext:Column ID="Column16" runat="server" DataIndex="Nama" Text="Name *" MinWidth="200" Flex="1"></ext:Column>
                        <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="DOB" Text="Date Of Birth" MinWidth="120" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column4" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="150" Flex="1"></ext:Column>
                        <ext:Column ID="Column5" runat="server" DataIndex="Identity_Number" Text="Identity Number" MinWidth="150" Flex="1"></ext:Column>
                        <%--    <ext:Column ID="Column14" runat="server" DataIndex="FK_WATCHLIST_ID_PPATK" Text="ID Watchlist PPATK" MinWidth="70" Flex="1"></ext:Column>--%>
                        <ext:CommandColumn ID="cc_ScreeningRequest" runat="server" Text="Action" MinWidth="200">
                            <Commands>
                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_ScreeningRequest">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                    </Confirmation>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader runat="server"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            <%--Grid Screening Request--%>

            <ext:Panel ID="panel_ScreeningStatus" runat="server" MarginSpec="10 0" Border="true" BodyPadding="5" Hidden="true" Layout="ColumnLayout">
                <Content>
                    <ext:FieldSet runat="server" Border="false" ColumnWidth="0.33">
                        <Items>
                            <ext:DisplayField ID="df_ScreeningStart" runat="server" AnchorHorizontal="100%" FieldLabel="Time Started"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet runat="server" Border="false" ColumnWidth="0.3">
                        <Items>
                            <ext:DisplayField ID="df_ScreeningStatus" runat="server" AnchorHorizontal="100%" FieldLabel="Status"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet runat="server" Border="false" ColumnWidth="0.33">
                        <Items>
                            <ext:DisplayField ID="df_ScreeningFinished" runat="server" AnchorHorizontal="100%" FieldLabel="Time Finished"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>
                </Content>
                <%--Added by Felix 15-Sep-2021--%>
                <Content>
                    <ext:FieldSet runat="server" Border="false" ColumnWidth="0.33">
                        <Items>
                            <ext:DisplayField ID="df_TotalScreening" runat="server" AnchorHorizontal="100%" FieldLabel="Total Screening"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet runat="server" Border="false" ColumnWidth="0.3">
                        <Items>
                            <ext:DisplayField ID="df_TotalFound" runat="server" AnchorHorizontal="100%" FieldLabel="Found"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet runat="server" Border="false" ColumnWidth="0.33">
                        <Items>
                            <ext:DisplayField ID="df_TotalNotFound" runat="server" AnchorHorizontal="100%" FieldLabel="Not Found"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>
                </Content>
                <%--End of 15-Sep-2021--%>
            </ext:Panel>
            <ext:Panel runat="server" ID="PanelScreeningResult" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Screening Result" BodyStyle="padding:10px" Collapsible="true" Hidden="true">
                <Items>
                    <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true">
                        <Items>
                            <ext:Checkbox ID="sar_IsSelectedAll_Result" runat="server" FieldLabel="Pilih Semua Result " LabelWidth="150" AnchorHorizontal="50%" Margin="10" Hidden="true" />
                            <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export " LabelStyle="width:80px" AnchorHorizontal="50%">
                                <Items>
                                    <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                                    <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                                </Items>
                            </ext:ComboBox>
                            <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" MarginSpec="0 0 0 10">
                                <DirectEvents>
                                    <Click OnEvent="ExportExcel" IsUpload="true">
                                        <ExtraParams>
                                            <ext:Parameter Name="currentPage" Value="App.gp_ScreeningResult.getStore().currentPage" Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page" MarginSpec="0 0 0 10">
                                <DirectEvents>
                                    <Click OnEvent="ExportAllExcel" IsUpload="true" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="BtnExportSelected" Text="Export Selected Row" MarginSpec="0 0 0 10">
                                <DirectEvents>
                                    <Click OnEvent="ExportSelectedExcel" IsUpload="true" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                    <%--Grid Screening Result--%>
                    <ext:GridPanel ID="gp_ScreeningResult" runat="server" Border="true" MarginSpec="10 0" Hidden="true" MinHeight="250" ClientIDMode="Static">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store1" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="model1" IDProperty="PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nama" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DOB" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_WATCHLIST_ID_PPATK" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMAWATCHLIST" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DOBWatchlist" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Birth_PlaceWatchlist" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_NumberWatchlist" Type="String"></ext:ModelField>
                                            <%-- 14-Oct-2021 Daniel : Sementara d Comment karena belum terpakai --%>
                                            <%--<ext:ModelField Name="_Similarity_Nama" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="_Similarity_DOB" Type="Float"></ext:ModelField>--%>
                                            <ext:ModelField Name="TotalSimilarityPct" Type="Float"></ext:ModelField>
                                  <%--          <ext:ModelField Name="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Type="String"></ext:ModelField>--%>
                                            <%-- 19-Oct-2021 Daniel : Penambahan Stakeholder_Role dan CustomerInformation--%>
                                        <%--    <ext:ModelField Name="Stakeholder_Role" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="customerinformation" Type="String"></ext:ModelField>--%>
                                            <%-- 14-Oct-2021 Daniel : Sementara d Comment karena belum terpakai --%>
                                            <%--<ext:ModelField Name="IsClosed" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ClosedDate" Type="String"></ext:ModelField>--%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column12" runat="server" Text="Data Searched">
                                    <Columns>
                                        <ext:Column ID="Column1" runat="server" DataIndex="Nama" Text="Name" MinWidth="200"></ext:Column>
                                        <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="DOB" Text="DOB" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                                        <ext:Column ID="Column6" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="150"></ext:Column>
                                       <%-- <ext:Column ID="Column55" runat="server" DataIndex="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Text="ID Type" MinWidth="100"></ext:Column>--%>
                                        <ext:Column ID="Column7" runat="server" DataIndex="Identity_Number" Text="Identity Number" MinWidth="150"></ext:Column>
                                        <%-- 1-Oct-2021 Adi : Sementara tidak ditampilkan di sini karena butuh waktu minimal 4 second untuk menampilkan ini per GCN --%>
                                        <%-- Test case bareng BSIM dgn data 4 juta customer dan 6 juta account --%>
                                        <%--<ext:Column ID="Column56" runat="server" DataIndex="IsClosed" Text="Customer Closed?" MinWidth="140"></ext:Column>
                                        <ext:Column ID="Column62" runat="server" DataIndex="ClosedDate" Text="Closing Date" MinWidth="100"></ext:Column>--%>
                                    </Columns>
                                </ext:Column>
                                <ext:Column ID="Column11" runat="server" Text="Data Found">
                                    <Columns>
                                      
                                          <ext:Column ID="Column2" runat="server" DataIndex="NAMAWATCHLIST" Text="Name" MinWidth="200"></ext:Column>
                                        <ext:DateColumn ID="DateColumn3" runat="server" DataIndex="DOBWatchlist" Text="DOB" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                                        <ext:Column ID="Column8" runat="server" DataIndex="Birth_PlaceWatchlist" Text="Birth Place" MinWidth="150"></ext:Column>
                                            <ext:Column ID="Column55" runat="server" DataIndex="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist" Text="ID Type" MinWidth="100"></ext:Column>
                                        <ext:Column ID="Column9" runat="server" DataIndex="Identity_NumberWatchlist" Text="Identity Number" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column13" runat="server" DataIndex="FK_WATCHLIST_ID_PPATK" Text="ID Watchlist<br>PPATK" MinWidth="70"></ext:Column>
                                    </Columns>
                                </ext:Column>



                                <ext:NumberColumn ID="NumberColumn1" runat="server" DataIndex="TotalSimilarityPct" Text="Similarity" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>

                                <ext:CommandColumn ID="gp_ScreeningResult_CommandColumn" runat="server" Text="Action" CellWrap="true">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail Data"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gp_ScreeningResult_Gridcommand">
                                            <EventMask ShowMask="true"></EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:CheckboxSelectionModel runat="server" Mode="Multi" CheckOnly="true">
                                <DirectEvents>
                                    <SelectionChange OnEvent="sm_DownloadFromScreeningResult_change">
                                    </SelectionChange>
                                </DirectEvents>
                            </ext:CheckboxSelectionModel>
                        </SelectionModel>
                        <Plugins>
                            <%--<ext:FilterHeader runat="server"></ext:FilterHeader>--%>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="false">
                                <Items>
                                    <%-- <ext:Button runat="server" Text="Close Picker" ID="Button5">
                                    <Listeners>
                                    <Click Handler="#{window_SEARCH_CUSTOMER}.hide()"> </Click>
                                    </Listeners>
                                    </ext:Button>--%>
                                    <ext:Label runat="server" ID="LabelScreeningResultCount">
                                    </ext:Label>
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="gp_ScreeningResult_All" runat="server" Border="true" MarginSpec="10 0" Hidden="true" MinHeight="250">

                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store1All" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="model4" IDProperty="PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nama" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DOB" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_WATCHLIST_ID_PPATK" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMAWATCHLIST" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DOBWatchlist" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Birth_PlaceWatchlist" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_NumberWatchlist" Type="String"></ext:ModelField>
                                            <%-- 14-Oct-2021 Daniel : Sementara d Comment karena belum terpakai --%>
                                            <%--<ext:ModelField Name="_Similarity_Nama" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="_Similarity_DOB" Type="Float"></ext:ModelField>--%>
                                            <ext:ModelField Name="TotalSimilarityPct" Type="Float"></ext:ModelField>
                                          <%--  <ext:ModelField Name="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Type="String"></ext:ModelField>--%>
                                            <%-- 19-Oct-2021 Daniel : Penambahan Stakeholder_Role dan CustomerInformation--%>
                                           <%-- <ext:ModelField Name="Stakeholder_Role" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="customerinformation" Type="String"></ext:ModelField>--%>
                                            <%-- 14-Oct-2021 Daniel : Penyetaraan dengan yang lain agar tidak ada dualisme --%>
                                            <%--<ext:ModelField Name="IsHaveActiveAccount" Type="String"></ext:ModelField>--%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column3" runat="server" Text="Data Searched">
                                    <Columns>
                                        <ext:Column ID="Column10" runat="server" DataIndex="Nama" Text="Name" MinWidth="200"></ext:Column>
                                        <ext:DateColumn ID="DateColumn4" runat="server" DataIndex="DOB" Text="DOB" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                                        <ext:Column ID="Column14" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="150"></ext:Column>
                                       <%-- <ext:Column ID="Column55" runat="server" DataIndex="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE" Text="ID Type" MinWidth="100"></ext:Column>--%>
                                        <ext:Column ID="Column15" runat="server" DataIndex="Identity_Number" Text="Identity Number" MinWidth="150"></ext:Column>
                                        <%-- 1-Oct-2021 Adi : Sementara tidak ditampilkan di sini karena butuh waktu minimal 4 second untuk menampilkan ini per GCN --%>
                                        <%-- Test case bareng BSIM dgn data 4 juta customer dan 6 juta account --%>
                                        <%--<ext:Column ID="Column56" runat="server" DataIndex="IsClosed" Text="Customer Closed?" MinWidth="140"></ext:Column>
                                        <ext:Column ID="Column62" runat="server" DataIndex="ClosedDate" Text="Closing Date" MinWidth="100"></ext:Column>--%>
                                    </Columns>
                                </ext:Column>
                                <ext:Column ID="Column17" runat="server" Text="Data Found">
                                    <Columns>
                                      
                                          <ext:Column ID="Column18" runat="server" DataIndex="NAMAWATCHLIST" Text="Name" MinWidth="200"></ext:Column>
                                        <ext:DateColumn ID="DateColumn5" runat="server" DataIndex="DOBWatchlist" Text="DOB" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                                        <ext:Column ID="Column19" runat="server" DataIndex="Birth_PlaceWatchlist" Text="Birth Place" MinWidth="150"></ext:Column>
                                            <ext:Column ID="Column20" runat="server" DataIndex="FK_goAML_Ref_Jenis_Dokumen_Identitas_CODEWatchlist" Text="ID Type" MinWidth="100"></ext:Column>
                                        <ext:Column ID="Column21" runat="server" DataIndex="Identity_NumberWatchlist" Text="Identity Number" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column22" runat="server" DataIndex="FK_WATCHLIST_ID_PPATK" Text="ID Watchlist<br>PPATK" MinWidth="70"></ext:Column>
                                    </Columns>
                                </ext:Column>


                                <ext:NumberColumn ID="NumberColumn2" runat="server" DataIndex="TotalSimilarityPct" Text="Similarity" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>

                                <ext:CommandColumn ID="gp_ScreeningResult_All_CommandColumn" runat="server" Text="Action" CellWrap="true">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail Data"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gp_ScreeningResult_Gridcommand">
                                            <EventMask ShowMask="true"></EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_RESULT_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>

                        <Plugins>
                            <%--<ext:FilterHeader runat="server"></ext:FilterHeader>--%>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            <%--Grid Screening Result--%>

            <%-- SiPendar Profile General Information (Harus dilengkapi kalau mau dilaporkan) --%>
       <%--     <ext:Panel runat="server" Title="SiPendar Report General Information" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlSiPendarReport" Border="true" BodyStyle="padding:10px" Hidden="true">
                <Content>

                    <nds:NDSDropDownField ID="cmb_SIPENDAR_TYPE_ID" LabelWidth="250" ValueField="PK_SIPENDAR_TYPE_id" DisplayField="SIPENDAR_TYPE_NAME" runat="server" StringField="PK_SIPENDAR_TYPE_id, SIPENDAR_TYPE_NAME" StringTable="SIPENDAR_TYPE" Label="SiPendar Report Type" AnchorHorizontal="80%" AllowBlank="false" OnOnValueChanged="SetControlVisibility" />
                    <nds:NDSDropDownField ID="cmb_JENIS_WATCHLIST_CODE" LabelWidth="250" ValueField="SIPENDAR_JENIS_WATCHLIST_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_JENIS_WATCHLIST_CODE, KETERANGAN" StringTable="SIPENDAR_JENIS_WATCHLIST" Label="Watchlist Category" AnchorHorizontal="80%" AllowBlank="false" />
                    <nds:NDSDropDownField ID="cmb_TINDAK_PIDANA_CODE" LabelWidth="250" ValueField="SIPENDAR_TINDAK_PIDANA_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_TINDAK_PIDANA_CODE, KETERANGAN" StringTable="SIPENDAR_TINDAK_PIDANA" Label="Jenis Tindak Pidana" AnchorHorizontal="80%" AllowBlank="false" IsHidden="true" />
                    <nds:NDSDropDownField ID="cmb_SUMBER_INFORMASI_KHUSUS_CODE" LabelWidth="250" ValueField="SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE" DisplayField="KETERANGAN" runat="server" StringField="SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE, KETERANGAN" StringTable="SIPENDAR_SUMBER_INFORMASI_KHUSUS" Label="Sumber Informasi Khusus" AnchorHorizontal="80%" AllowBlank="false" />
                    <nds:NDSDropDownField ID="cmb_SUMBER_TYPE_CODE" LabelWidth="250" ValueField="SUMBER_TYPE_CODE" DisplayField="SUMBER_TYPE_NAME" runat="server" StringField="SUMBER_TYPE_CODE, SUMBER_TYPE_NAME" StringTable="SIPENDAR_SUMBER_TYPE" Label="Sumber Type" AnchorHorizontal="80%" AllowBlank="false" IsHidden="true" />
                    <ext:NumberField ID="txt_SUMBER_ID" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Sumber ID" AnchorHorizontal="80%" FormatText="#,##0" Hidden="true" MaxValue="2147483646"></ext:NumberField>
                    <nds:NDSDropDownField ID="cmb_SUBMISSION_TYPE_CODE" LabelWidth="250" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="vw_SIPENDAR_SUBMISSION_TYPE" Label="Submission Type" AnchorHorizontal="80%" AllowBlank="false" />
                    <ext:TextArea ID="txt_Keterangan" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Keterangan" AnchorHorizontal="80%" MaxLength="4000" EnforceMaxLength="true"></ext:TextArea>

                    <ext:DisplayField runat="server" ID="df_blank"></ext:DisplayField>

                    <ext:FieldSet runat="server" ID="fs_Generate_Transaction" Border="true" Title="Generate Transaction" Hidden="true" Layout="AnchorLayout" Padding="10">
                        <Content>
                            <ext:Checkbox ID="chk_IsGenerateTransaction" LabelWidth="250" runat="server" FieldLabel="Include Generate Transaction">
                                <DirectEvents>
                                    <Change OnEvent="chk_IsGenerateTransaction_Change"></Change>
                                </DirectEvents>
                            </ext:Checkbox>
                            <ext:DateField ID="txt_Transaction_DateFrom" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Transaction Date From" AnchorHorizontal="50%" Format="dd-MMM-yyyy" Hidden="true"></ext:DateField>
                            <ext:DateField ID="txt_Transaction_DateTo" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Transaction Date To" AnchorHorizontal="50%" Format="dd-MMM-yyyy" Hidden="true"></ext:DateField>
                            <ext:Panel runat="server" ID="PanelReportIndicator" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" Border="true" BodyStyle="padding:10px" Hidden="true" Title="Indikator" Margin="10">
                                <Items>
                                    <ext:GridPanel ID="GridPanelReportIndicator" runat="server" EmptyText="No Available Data">
                                        <View>
                                            <ext:GridView runat="server" EnableTextSelection="true" />
                                        </View>
                                        <TopBar>
                                            <ext:Toolbar runat="server">
                                                <Items>
                                                    <ext:Button runat="server" ID="BtnAddReportIndikator" Text="Tambah Indicator" Icon="Add">
                                                        <DirectEvents>
                                                            <Click OnEvent="btn_addIndicator_click">
                                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="500"></EventMask>
                                                            </Click>
                                                        </DirectEvents>
                                                    </ext:Button>
                                                </Items>
                                            </ext:Toolbar>
                                        </TopBar>
                                        <Store>
                                            <ext:Store ID="StoreReportIndicatorData" runat="server">
                                                <Model>
                                                    <ext:Model runat="server" ID="Model355" IDProperty="PK_Report_Indicator">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Report_Indicator" Type="Int"></ext:ModelField>
                                                            <ext:ModelField Name="FK_Indicator" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn88" runat="server" Text="No"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column312" runat="server" DataIndex="FK_Indicator" Text="Kode" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column313" runat="server" DataIndex="Keterangan" Text="Keterangan" Flex="1" CellWrap="true"></ext:Column>
                                                <ext:CommandColumn ID="cc_ReportIndicator" runat="server" Text="Action" MinWidth="220">

                                                    <Commands>
                                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                                            <ToolTip Text="Edit"></ToolTip>
                                                        </ext:GridCommand>
                                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                                            <ToolTip Text="Detail"></ToolTip>
                                                        </ext:GridCommand>
                                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationViewDetail" MinWidth="70">
                                                            <ToolTip Text="Delete"></ToolTip>
                                                        </ext:GridCommand>
                                                    </Commands>

                                                    <DirectEvents>

                                                        <Command OnEvent="GridcommandReportIndicator">
                                                            <EventMask ShowMask="true"></EventMask>
                                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>

                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Report_Indicator" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>



                                            </Columns>
                                        </ColumnModel>
                                    </ext:GridPanel>
                                </Items>
                            </ext:Panel>
                        </Content>
                    </ext:FieldSet>

                </Content>
            </ext:Panel>--%>
            <%-- End of SiPendar Profile General Information --%>

            <ext:Panel ID="panel_TaskManager" runat="server" MarginSpec="10 0" Border="true" BodyPadding="10" Hidden="true">
                <Content>
                    <ext:TaskManager ID="TaskManager1" runat="server">
                        <Tasks>
                            <ext:Task
                                TaskID="refreshScreeningStatus"
                                Interval="1000"
                                AutoRun="false">
                                <DirectEvents>
                                    <Update OnEvent="Screening_CheckStatus">
                                        <%--<EventMask Msg="Loading..." ShowMask="true" MinDelay="200"></EventMask>--%>
                                    </Update>
                                </DirectEvents>
                            </ext:Task>
                        </Tasks>
                    </ext:TaskManager>
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Screening" ClientIDMode="Static" runat="server" Text="Start Screening" Enabled="false" Icon="PlayGreen">
                <DirectEvents>
                    <Click OnEvent="btn_Screening_Click">
                        <%--<EventMask ShowMask="true"></EventMask>--%>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <%-- <ext:Button ID="btn_Save" ClientIDMode="Static" runat="server" Text="Save Selected to SiPendar Report" Enabled="false" Icon="DiskBlack" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btn_Save_Click">
                        <EventMask ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>--%>
            <ext:Button ID="btn_Cancel" runat="server" Text="Clear Search" Icon="PictureEmpty">
                <DirectEvents>
                    <Click OnEvent="btn_Cancel_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="Accept">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <ext:Window runat="server" ID="WindowScreeningDetail" Title="Screening Detail" Modal="true" Layout="ColumnLayout" ButtonAlign="Center" Hidden="true" AutoScroll="true" Scrollable="Vertical" Maximizable="true">
        <Items>
            <ext:FieldSet runat="server" ID="FieldSet0" Border="false" Layout="AnchorLayout" ColumnWidth="1" Padding="10" MarginSpec="10 0">
                <Content>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_Similarity" FieldLabel="Similarity" LabelWidth="175"></ext:DisplayField>
                    <%--  <ext:DisplayField runat="server" ID="WindowScreeningDetail_CustomerType" FieldLabel="Customer / Non-Customer" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_IsHaveActiveAccount" FieldLabel="Customer Account Status" LabelWidth="175"></ext:DisplayField>--%>
                </Content>
            </ext:FieldSet>
            <ext:FieldSet runat="server" ID="FieldSet1" Border="true" Layout="AnchorLayout" ColumnWidth="0.45" Padding="10" MarginSpec="0 0 0 10" AutoScroll="true" Title="Search Data">
                <Content>
                     <ext:DisplayField runat="server" ID="WindowScreeningDetail_NameSearch" FieldLabel="Name Search" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_DOBSearch" FieldLabel="Birth Date Search" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_POBSearch" FieldLabel="Birth Place Search" LabelWidth="175"></ext:DisplayField>
                  <%--  <ext:DisplayField runat="server" ID="WindowScreeningDetail_IdentityTypeSearch" FieldLabel="Identity Type Search" LabelWidth="175"></ext:DisplayField>
                  --%>  <ext:DisplayField runat="server" ID="WindowScreeningDetail_IdentityNumberSearch" FieldLabel="Identity Number Search" LabelWidth="175"></ext:DisplayField>

                    <%-- 29-Sep-2021 Adi : Penambahan list of Identity Watchlist --%>
                    <ext:GridPanel ID="gp_IdentityWatchlist" runat="server" EmptyText="No Available Data" Hidden="true" Title="List of Identity">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model5">
                                        <Fields>
                                            <ext:ModelField Name="Identity_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column58" runat="server" DataIndex="Identity_Type" Text="ID Type" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column59" runat="server" DataIndex="Identity_Number" Text="ID Number" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                    <%-- End of 29-Sep-2021 Adi : Penambahan list of Identity Watchlist --%>
                </Content>
            </ext:FieldSet>
            <ext:FieldSet runat="server" ID="FieldSet2" Border="true" Layout="AnchorLayout" ColumnWidth="0.5" Padding="10" MarginSpec="0 10 0 10" AutoScroll="true" Title="Watchlist Data">
                <Content>
                     <ext:DisplayField runat="server" ID="WindowScreeningDetail_IDWatchlistPPATK" FieldLabel="ID Watch List PPATK" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_WatchlistName" FieldLabel="Name" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_WatchlistDOB" FieldLabel="Birth Date" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_WatchlistPOB" FieldLabel="Birth Place" LabelWidth="175"></ext:DisplayField>
                 <%--   <ext:DisplayField runat="server" ID="WindowScreeningDetail_WatchlistIdentityType" FieldLabel="Identity Type" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="WindowScreeningDetail_WatchlistIdentityNumber" FieldLabel="Identity Number" LabelWidth="175"></ext:DisplayField>--%>

                    <%-- 29-Sep-2021 Adi : Penambahan list of Identity Customer --%>
                    <ext:GridPanel ID="gp_IdentityWatchlistResult" runat="server" EmptyText="No Available Data" Hidden="true" Title="List of Identity">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store4" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model6">
                                        <Fields>
                                            <ext:ModelField Name="Identity_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column60" runat="server" DataIndex="Identity_Type" Text="ID Type" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column61" runat="server" DataIndex="Identity_Number" Text="ID Number" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                    <%-- End of 29-Sep-2021 Adi : Penambahan list of Identity Customer --%>
                </Content>
            </ext:FieldSet>
        </Items>
        <Buttons>
            <ext:Button runat="server" ID="WindowScreeningDetail_Close" Text="Close" Icon="Cancel">
                <DirectEvents>
                    <Click OnEvent="WindowScreeningDetail_Close_Click">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="100"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.8, height: size.height * 0.8});" />
            <Resize Handler="#{WindowScreeningDetail}.center()" />
        </Listeners>
    </ext:Window>

</asp:Content>
