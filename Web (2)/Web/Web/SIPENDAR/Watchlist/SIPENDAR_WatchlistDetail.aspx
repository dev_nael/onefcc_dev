﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="SIPENDAR_WatchlistDetail.aspx.vb" Inherits="SIPENDAR_WatchlistDetail" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .x-form-item-label.x-form-item-label-default { width : 230px !important; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel ID="FormPanelInput" runat="server" Layout="AnchorLayout" ButtonAlign="Center" Title="SIPENDAR Watchlist DETAIL" BodyStyle="padding:10px" AutoScroll="true" Hidden="false">
        <Items>
            <ext:InfoPanel ID="infoValidationResultPanel" ClientIDMode="Static" runat="server" UI="Warning" Layout="AnchorLayout" Title="Validation Result" IconCls="#Error" MarginSpec="10 0" Border="True" Hidden="true"></ext:InfoPanel>
            <ext:Panel runat="server" ID="ReportGeneralInformationPanel" Layout="AnchorLayout" ClientIDMode="Static" BodyStyle="padding:10px" Margin="5" Border="True">
                <Items>
                    <ext:DateField ID="PERIODEField" runat="server" FieldLabel="Periode" Format="dd-MMM-yyyy" AnchorHorizontal="70%" AllowBlank="false" Editable="false" Selectable="false"/>

                    <ext:TextField ID="ID_PPATKField" runat="server" FieldLabel="ID PPATK" AnchorHorizontal="70%" AllowBlank="false" EnableKeyEvents="true" MaskRe="[0-9]" Selectable="false" Editable="false" />
                    <ext:TextField ID="KODE_WATCHLISTField" runat="server" FieldLabel="Kode Watchlist" AnchorHorizontal="70%" AllowBlank="false" Selectable="false" Editable="false"/>
                    <ext:TextField ID="JENIS_PELAKUField" runat="server" FieldLabel="Jenis Pelaku" AnchorHorizontal="70%" AllowBlank="false" Selectable="false" Editable="false"/>
                    <ext:TextField ID="NAMA_ASLIField" runat="server" FieldLabel="Nama Asli" AnchorHorizontal="70%" AllowBlank="false" Selectable="false" Editable="false"/>
                    <ext:TextField ID="PARAMETER_PENCARIAN_NAMAField" runat="server" FieldLabel="Parameter Pencarian" AnchorHorizontal="70%" AllowBlank="false" Selectable="false" Editable="false"/>
                    <ext:TextField ID="TEMPAT_LAHIRField" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="70%" AllowBlank="true" Selectable="false" Editable="false"/>

                    <ext:DateField ID="TANGGAL_LAHIRField" runat="server" FieldLabel="Tanggal Lahir" Format="dd-MMM-yyyy" AnchorHorizontal="70%" AllowBlank="true" Selectable="false" Editable="false"/>

                    <ext:TextField ID="NPWPField" runat="server" FieldLabel="NPWP" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]" Selectable="false" Editable="false"/>
                    <ext:TextField ID="KTPField" runat="server" FieldLabel="KTP" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]" Selectable="false" Editable="false"/>
                    <ext:TextField ID="PASField" runat="server" FieldLabel="PAS" AnchorHorizontal="70%" AllowBlank="true" Selectable="false" Editable="false"/>
                    <ext:TextField ID="KITASField" runat="server" FieldLabel="KITAS" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]" Selectable="false" Editable="false"/>
                    <ext:TextField ID="SUKETField" runat="server" FieldLabel="SUKET" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]" Selectable="false" Editable="false"/>
                    <ext:TextField ID="SIMField" runat="server" FieldLabel="SIM" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]" Selectable="false" Editable="false"/>
                    <ext:TextField ID="KITAPField" runat="server" FieldLabel="KITAP" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]" Selectable="false" Editable="false"/>
                    <ext:TextField ID="KIMSField" runat="server" FieldLabel="KIMS" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]" Selectable="false" Editable="false"/>
                    <ext:TextField ID="WatchlistField" runat="server" FieldLabel="Watchlist" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]" Selectable="false" Editable="false"/>
                    <ext:TextField ID="Submission_Type" runat="server" FieldLabel="Submission Type" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]" Selectable="false" Editable="false"/>
                </Items>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btnCancel" runat="server" Icon="PageBack"  Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="100"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>

