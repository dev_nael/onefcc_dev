﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.master" AutoEventWireup="false" CodeFile="SIPENDAR_WatchlistView.aspx.vb" Inherits="SIPENDAR_WatchlistView" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };
        
        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
        

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <ext:GridPanel ID="GridpanelView" ClientIDMode="Static" runat="server" Title="Title" Width="2000">
        <View>
            <ext:GridView runat="server"/>
        </View>
        <Store>
            <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadData">
               <%-- <Model>
                    <ext:Model runat="server" ID="ModelWatchlistView" IDProperty="PK_SIPENDAR_WATCHLIST_ID">
                        <Fields>
                            <ext:ModelField Name="PK_SIPENDAR_WATCHLIST_ID" Type="Int"></ext:ModelField>
                            <ext:ModelField Name="PERIODE" Type="Date"></ext:ModelField>
                            <ext:ModelField Name="ID_PPATK" Type="String"></ext:ModelField>
                            <ext:ModelField Name="KODE_WATCHLIST" Type="String"></ext:ModelField>
                            <ext:ModelField Name="JENIS_PELAKU" Type="String"></ext:ModelField>
                            <ext:ModelField Name="NAMA_ASLI" Type="String"></ext:ModelField>
                            <ext:ModelField Name="PARAMETER_PENCARIAN_NAMA" Type="String"></ext:ModelField>
                            <ext:ModelField Name="TEMPAT_LAHIR" Type="String"></ext:ModelField>
                            <ext:ModelField Name="TANGGAL_LAHIR" Type="Date"></ext:ModelField>
                            <ext:ModelField Name="NPWP" Type="String"></ext:ModelField>
                            <ext:ModelField Name="KTP" Type="String"></ext:ModelField>
                            <ext:ModelField Name="PAS" Type="String"></ext:ModelField>
                            <ext:ModelField Name="KITAS" Type="String"></ext:ModelField>
                            <ext:ModelField Name="SUKET" Type="String"></ext:ModelField>
                            <ext:ModelField Name="SIM" Type="String"></ext:ModelField>
                            <ext:ModelField Name="KITAP" Type="String"></ext:ModelField>
                            <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                            <ext:ModelField Name="LastUpdateDate" Type="Date"></ext:ModelField>
                        </Fields>
                    </ext:Model>
                </Model>--%>
                
                <Sorters>
                    <%--<ext:DataSorter Property="" Direction="ASC" />--%>
                </Sorters>
                <Proxy>
                    <ext:PageProxy />
                </Proxy>
            </ext:Store>
        </Store>
     <%--   <ColumnModel>
            <Columns>
                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No." MinWidth="60"></ext:RowNumbererColumn>
                <ext:DateColumn ID="DateColumn3" runat="server" DataIndex="PERIODE" Text="Periode" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>--%>
                <%--<ext:Column ID="Column16" runat="server" DataIndex="PERIODE" Text="Periode" MinWidth="120"></ext:Column>--%>
                <%--<ext:NumberColumn ID="Column4" runat="server" DataIndex="ID_PPATK" Text="Id" MinWidth="40" Format="#,##0" Align="Right"></ext:NumberColumn>
                <ext:Column ID="Column5" runat="server" DataIndex="KODE_WATCHLIST" Text="Kode Watchlist" MinWidth="100"></ext:Column>
                <ext:Column ID="Column6" runat="server" DataIndex="JENIS_PELAKU" Text="Jenis Pelaku" MinWidth="100"></ext:Column>
                <ext:Column ID="Column7" runat="server" DataIndex="NAMA_ASLI" Text="Nama Asli" MinWidth="150"></ext:Column>
                <ext:Column ID="Column8" runat="server" DataIndex="PARAMETER_PENCARIAN_NAMA" Text="Parameter Pencarian Nama" MinWidth="200"></ext:Column>
                <ext:Column ID="Column9" runat="server" DataIndex="TEMPAT_LAHIR" Text="Tempat Lahir" MinWidth="150"></ext:Column>
                <ext:DateColumn ID="Column10" runat="server" DataIndex="TANGGAL_LAHIR" Text="Tanggal Lahir" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                <ext:Column ID="Column11" runat="server" DataIndex="NPWP" Text="NPWP" MinWidth="150"></ext:Column>
                <ext:Column ID="Column12" runat="server" DataIndex="KTP" Text="KTP" MinWidth="150"></ext:Column>
                <ext:Column ID="Column13" runat="server" DataIndex="PAS" Text="PAS" MinWidth="150"></ext:Column>
                <ext:Column ID="Column14" runat="server" DataIndex="KITAS" Text="KITAS" MinWidth="150"></ext:Column>
                <ext:Column ID="Column15" runat="server" DataIndex="SUKET" Text="SUKET" MinWidth="150"></ext:Column>
                <ext:Column ID="Column17" runat="server" DataIndex="SIM" Text="SIM" MinWidth="150"></ext:Column>
                <ext:Column ID="Column18" runat="server" DataIndex="KITAP" Text="KITAP" MinWidth="150"></ext:Column>
                <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="LastUpdateDate" Text="Last Update Date" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                --%>
<%--                <ext:CommandColumn ID="CommandColumnView" runat="server" Text="Action" Flex="2">

                    <Commands>
                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                            <ToolTip Text="Edit"></ToolTip>
                        </ext:GridCommand>
                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                            <ToolTip Text="Detail"></ToolTip>
                        </ext:GridCommand>
                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                            <ToolTip Text="Delete"></ToolTip>
                        </ext:GridCommand>
                    </Commands>

                    <DirectEvents>

                        <Command OnEvent="GridcommandWatchlistView">
                            <EventMask ShowMask="true"></EventMask>
                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                            <ExtraParams>
                                <ext:Parameter Name="unikkey" Value="record.data.PK_SIPENDAR_WATCHLIST_ID" Mode="Raw"></ext:Parameter>
                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                            </ExtraParams>
                        </Command>
                    </DirectEvents>
                </ext:CommandColumn>--%>
          <%--  </Columns>
        </ColumnModel>--%>
        <%--<ColumnModel>
            <Columns>
                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                <ext:Column ID="Column1" runat="server" DataIndex="PK_SIPENDAR_WATCHLIST_ID" Text="ID" Flex="1"></ext:Column>
                <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="PERIODE" Text="Periode" Flex="1"></ext:DateColumn>
            </Columns>
        </ColumnModel>--%>
        <Plugins>
            <%--<ext:GridFilters ID="GridFilters1" runat="server" />--%>
            <ext:FilterHeader ID="grdiheaderfilter" runat="server" Remote="true"></ext:FilterHeader>

        </Plugins>
        <BottomBar>

            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />

        </BottomBar>

        <%--Begin Update Penambahan Advance Filter--%>
        <DockedItems>
            <%--End Update Penambahan Advance Filter--%>
            <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                <Items>

                    <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export :">
                        <Items>
                            <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                            <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                        </Items>
                    </ext:ComboBox>
                    <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" AutoPostBack="true" OnClick="ExportExcel" ClientIDMode="Static" />
                    <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page" AutoPostBack="true" OnClick="ExportAllExcel" />
                    <%--<ext:Button ID="Button1" runat="server" Text="Print Current Page" Icon="Printer" Handler="this.up('grid').print();" />--%>
                    <ext:Button ID="BtnAdd" runat="server" Text="Add New Data" Icon="Add" Handler="NawadataDirect.BtnAdd_Click()"  />
                    <%--Begin Update Penambahan Advance Filter--%>
                    <ext:Button ID="AdvancedFilter" runat="server" Text="Advanced Filter" Icon="Magnifier" Handler="NawadataDirect.BtnAdvancedFilter_Click()" />
                    <%--End Update Penambahan Advance Filter--%>
                </Items>
            </ext:Toolbar>
            <%--Begin Update Penambahan Advance Filter--%>
            <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true" Dock="Top" Hidden="true">
                <Items>
                    <ext:HyperlinkButton ID="HyperlinkButton1" runat="server" Text="Clear Advanced Filter">
                        <DirectEvents>
                            <Click OnEvent="btnClear_Click"></Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                    <ext:Label ID="LblAdvancedFilter" runat="server" Text="">
                    </ext:Label>
                </Items>
            </ext:Toolbar>
        </DockedItems>
        <%--End Update Penambahan Advance Filter--%>

        <Listeners>
            <ViewReady Handler="columnAutoResize(this);
                    this.getStore().on('load', Ext.bind(columnAutoResize, null, [this]));"
                Delay="10" />
        </Listeners>
        <%--<SelectionModel>
            <ext:CheckboxSelectionModel runat="server" Mode="Single">
            </ext:CheckboxSelectionModel>
        </SelectionModel>--%>
    </ext:GridPanel>
     <%--Begin Update Penambahan Advance Filter--%>
    <ext:Panel ID="Panel1" runat="server" Hidden="true">
        <Content>
            <uc1:AdvancedFilter runat="server" ID="AdvancedFilter1" />
        </Content>
        <Items>
        </Items>
    </ext:Panel>
    <%--End Update Penambahan Advance Filter--%>

</asp:Content>

