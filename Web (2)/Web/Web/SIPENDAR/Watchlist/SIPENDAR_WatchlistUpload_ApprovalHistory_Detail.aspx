﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="SIPENDAR_WatchlistUpload_ApprovalHistory_Detail.aspx.vb" Inherits="SIPENDAR_Watchlist_SIPENDAR_WatchlistUpload_ApprovalHistory_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        var onKeyUp = function (combo, e) {
            var v = combo.getRawValue();
            combo.store.filter(combo.displayField, new RegExp(v, "i"));
            combo.onLoad();
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };
        
        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel runat="server" ID="fpMain" BodyPadding="10" ButtonAlign="Center" Scrollable="Both" Title="Sipendar Watchlist Approval History Detail">
        <Items>
           <ext:FormPanel runat="server" Flex="1">
               <Items>
                   <ext:FormPanel runat="server">
                       <Items>
                             <ext:DisplayField ID="txtPK_Sipendar_Watchlist_Approval_History_ID" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Watchlist Approval ID" />
                            <ext:DisplayField ID="txtModuleName" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Module Name" />
                            <ext:DisplayField ID="txtModuleKey" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Module Key" />
                           <%-- <ext:DisplayField ID="txtModuleField" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Module Field" />
                            <ext:DisplayField ID="txtModuleFieldBefore" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Module Field Before" />--%>
                            <ext:DisplayField ID="txtPK_ModuleAction_ID" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Module Action" />
                            <ext:DisplayField ID="txtFK_MRole_ID" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="FK Mrole ID" />
                       </Items>
                   </ext:FormPanel>
                   <ext:FormPanel runat="server">
                       <Items>
                            <ext:DisplayField ID="txtUserID" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="User ID" />
                            <ext:DisplayField ID="txtStatus" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Status" />
                            <ext:DisplayField ID="txtNotes" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Notes" />
                            <ext:DisplayField ID="txtCreatedBy" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Created By" />
                            <ext:DisplayField ID="txtCreatedDate" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Created Date" />
                       </Items>
                   </ext:FormPanel>
                  
               </Items>
           </ext:FormPanel>
           
            <ext:FormPanel runat="server"  Layout="HBoxLayout">
                <Items>
                    <ext:FormPanel runat="server" Flex="1" title="Sipendar Watchlist Old Data" ID="formwatchlistold" Hidden="true">
                        <Items>

                        </Items>
                    </ext:FormPanel>
                    <ext:FormPanel runat="server" Flex="1" title="Sipendar Watchlist New Data" ID="formwatchlistnew"  Hidden="true" >
                        <Items>

                        </Items>
                    </ext:FormPanel>
                </Items>
            </ext:FormPanel>
            
            <ext:Panel runat="server" ID="PanelCleanedData" Layout="AnchorLayout" Border="TRUE" Title="Sipendar Watchlist Saved Data" Margin="10" Collapsible="true" AnchorHorizontal="100%" hidden="true">
                <Items>
                    <ext:GridPanel ID="GridPanelWatchlisCleansing" runat="server" EmptyText="No Available Data"  AnchorHorizontal="100%">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreWatchlistCleansing" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model2" IDProperty="PK_SIPENDAR_WATCHLIST_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_WATCHLIST_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="nawaaction" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PERIODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_PPATK" Type="int"></ext:ModelField>
                                            <ext:ModelField Name="KODE_WATCHLIST" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JENIS_PELAKU" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMA_ASLI" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PARAMETER_PENCARIAN_NAMA" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TEMPAT_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TANGGAL_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NPWP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KTP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SUKET" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SIM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KIMS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Watchlist" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastUpdateDate" Type="String"></ext:ModelField>
                                            <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                            <ext:ModelField Name="Submission_Type" Type="String"></ext:ModelField>
                                            <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters>
                                    <ext:DataSorter Property="ID_PPATK" Direction="ASC" />
                                </Sorters>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No." MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column16" runat="server" DataIndex="nawaaction" Text="Action" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column1" runat="server" DataIndex="PERIODE" Text="Periode" MinWidth="120" Format="dd-MMM-yyyy"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn2" runat="server" DataIndex="ID_PPATK" Text="ID" MinWidth="40" Format="#,##0" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column2" runat="server" DataIndex="KODE_WATCHLIST" Text="Kode Watchlist" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column3" runat="server" DataIndex="JENIS_PELAKU" Text="Jenis Pelaku" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="NAMA_ASLI" Text="Nama Asli" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column5" runat="server" DataIndex="PARAMETER_PENCARIAN_NAMA" Text="Parameter Pencarian Nama" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="TEMPAT_LAHIR" Text="Tempat Lahir" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="TANGGAL_LAHIR" Text="Tanggal Lahir" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column7" runat="server" DataIndex="NPWP" Text="NPWP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="KTP" Text="KTP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="PAS" Text="PAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="KITAS" Text="KITAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="SUKET" Text="SUKET" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="SIM" Text="SIM" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="KITAP" Text="KITAP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="KIMS" Text="KIMS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="Watchlist" Text="Watchlist" MinWidth="150"></ext:Column>
                                <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                <ext:Column ID="Column52" runat="server" DataIndex="Submission_Type" Text="Submission Type" MinWidth="150"></ext:Column>
                                <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            
            <ext:Panel runat="server" ID="PanelOldData" Layout="AnchorLayout" Border="TRUE" Title="Sipendar Watchlist Old Data" Margin="10" Collapsible="true" AnchorHorizontal="100%" hidden="true">
                <Items>
                    <ext:GridPanel ID="GridPanelWatchlisOld" runat="server" EmptyText="No Available Data"  AnchorHorizontal="100%" >
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreWatchlistOld" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model3" IDProperty="PK_SIPENDAR_WATCHLIST_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_WATCHLIST_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="PERIODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_PPATK" Type="int"></ext:ModelField>
                                            <ext:ModelField Name="KODE_WATCHLIST" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JENIS_PELAKU" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMA_ASLI" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PARAMETER_PENCARIAN_NAMA" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TEMPAT_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TANGGAL_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NPWP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KTP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SUKET" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SIM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KIMS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Watchlist" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastUpdateDate" Type="String"></ext:ModelField>
                                            <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                            <ext:ModelField Name="Submission_Type" Type="String"></ext:ModelField>
                                            <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters>
                                    <ext:DataSorter Property="ID_PPATK" Direction="ASC" />
                                </Sorters>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No." MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column30" runat="server" DataIndex="PERIODE" Text="Periode" MinWidth="120" Format="dd-MMM-yyyy"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn3" runat="server" DataIndex="ID_PPATK" Text="ID" MinWidth="40" Format="#,##0" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column31" runat="server" DataIndex="KODE_WATCHLIST" Text="Kode Watchlist" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column32" runat="server" DataIndex="JENIS_PELAKU" Text="Jenis Pelaku" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column33" runat="server" DataIndex="NAMA_ASLI" Text="Nama Asli" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column34" runat="server" DataIndex="PARAMETER_PENCARIAN_NAMA" Text="Parameter Pencarian Nama" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column35" runat="server" DataIndex="TEMPAT_LAHIR" Text="Tempat Lahir" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="DateColumn3" runat="server" DataIndex="TANGGAL_LAHIR" Text="Tanggal Lahir" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column38" runat="server" DataIndex="NPWP" Text="NPWP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column39" runat="server" DataIndex="KTP" Text="KTP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column40" runat="server" DataIndex="PAS" Text="PAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column41" runat="server" DataIndex="KITAS" Text="KITAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column42" runat="server" DataIndex="SUKET" Text="SUKET" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column43" runat="server" DataIndex="SIM" Text="SIM" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column44" runat="server" DataIndex="KITAP" Text="KITAP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column45" runat="server" DataIndex="KIMS" Text="KIMS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column46" runat="server" DataIndex="Watchlist" Text="Watchlist" MinWidth="150"></ext:Column>
                                <%--<ext:Column ID="Column30" runat="server" DataIndex="CreatedBy" Text="CreatedBy" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="Column31" runat="server" DataIndex="LastUpdateDate" Text="LastUpdateDate" MinWidth="150" Format="dd-MMM-yyyy"></ext:DateColumn>--%>
                                <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                <ext:Column ID="Column47" runat="server" DataIndex="Submission_Type" Text="Submission Type" MinWidth="150"></ext:Column>
                                <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>  
            
            <ext:Panel runat="server" ID="PanelRawData" Layout="AnchorLayout" Border="TRUE" Title="Sipendar Watchlist Upload Data" Margin="10" Collapsible="true" AnchorHorizontal="100%" hidden="true">
                <Items>
                    <ext:GridPanel ID="GridPanelWatchlistNew" runat="server" EmptyText="No Available Data"  AnchorHorizontal="100%" >
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreWatchlistNew" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model1" IDProperty="PK_SIPENDAR_WATCHLIST_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_WATCHLIST_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="PERIODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_PPATK" Type="int"></ext:ModelField>
                                            <ext:ModelField Name="KODE_WATCHLIST" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JENIS_PELAKU" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMA_ASLI" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PARAMETER_PENCARIAN_NAMA" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TEMPAT_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TANGGAL_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NPWP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KTP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SUKET" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SIM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KIMS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Watchlist" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastUpdateDate" Type="String"></ext:ModelField>
                                            <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                            <ext:ModelField Name="Submission_Type" Type="String"></ext:ModelField>
                                            <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters>
                                    <ext:DataSorter Property="ID_PPATK" Direction="ASC" />
                                </Sorters>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No." MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column17" runat="server" DataIndex="PERIODE" Text="Periode" MinWidth="120" Format="dd-MMM-yyyy"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn1" runat="server" DataIndex="ID_PPATK" Text="ID" MinWidth="40" Format="#,##0" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column18" runat="server" DataIndex="KODE_WATCHLIST" Text="Kode Watchlist" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="JENIS_PELAKU" Text="Jenis Pelaku" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="NAMA_ASLI" Text="Nama Asli" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column21" runat="server" DataIndex="PARAMETER_PENCARIAN_NAMA" Text="Parameter Pencarian Nama" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="TEMPAT_LAHIR" Text="Tempat Lahir" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="TANGGAL_LAHIR" Text="Tanggal Lahir" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column23" runat="server" DataIndex="NPWP" Text="NPWP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="KTP" Text="KTP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column25" runat="server" DataIndex="PAS" Text="PAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="KITAS" Text="KITAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column27" runat="server" DataIndex="SUKET" Text="SUKET" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column28" runat="server" DataIndex="SIM" Text="SIM" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column29" runat="server" DataIndex="KITAP" Text="KITAP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column36" runat="server" DataIndex="KIMS" Text="KIMS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column37" runat="server" DataIndex="Watchlist" Text="Watchlist" MinWidth="150"></ext:Column>
                                <%--<ext:Column ID="Column30" runat="server" DataIndex="CreatedBy" Text="CreatedBy" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="Column31" runat="server" DataIndex="LastUpdateDate" Text="LastUpdateDate" MinWidth="150" Format="dd-MMM-yyyy"></ext:DateColumn>--%>
                                <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                <ext:Column ID="Column48" runat="server" DataIndex="Submission_Type" Text="Submission Type" MinWidth="150"></ext:Column>
                                <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>    
        </Items> 
        <Buttons>
          
            <ext:Button ID="BtnCancel" runat="server" Text="Back" Icon="PageBack">
                <Listeners>
                    <Click Handler="#{BtnCancel}.disable()" />
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="btnCancel_DirectEvent">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="500">
                        </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
