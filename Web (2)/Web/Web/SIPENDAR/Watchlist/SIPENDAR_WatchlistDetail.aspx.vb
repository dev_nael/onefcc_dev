﻿
Imports System.Data
Imports System.Data.Entity

Partial Class SIPENDAR_WatchlistDetail
    Inherits System.Web.UI.Page

    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("SIPENDAR_WatchlistDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("SIPENDAR_WatchlistDetail.ObjModule") = value
        End Set
    End Property

    Public Property ErrorValidation() As String
        Get
            Return Session("SIPENDAR_WatchlistDetail.ErrorValidation")
        End Get
        Set(ByVal value As String)
            Session("SIPENDAR_WatchlistDetail.ErrorValidation") = value
        End Set
    End Property
    Public Property DataID As String
        Get
            Return Session("SIPENDAR_WatchlistDetail.DataID")
        End Get
        Set(ByVal value As String)
            Session("SIPENDAR_WatchlistDetail.DataID") = value
        End Set
    End Property
    Public Property DataOld As SiPendarDAL.SIPENDAR_WATCHLIST
        Get
            Return Session("SIPENDAR_WatchlistDetail.DataOld")
        End Get
        Set(ByVal value As SiPendarDAL.SIPENDAR_WATCHLIST)
            Session("SIPENDAR_WatchlistDetail.DataOld") = value
        End Set
    End Property

    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SIPENDAR_WatchlistDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer

            intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)


            If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Detail) Then
                Dim strIDCode As String = 1
                strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                Exit Sub
            End If
            If Not Ext.Net.X.IsAjaxRequest Then
                LoadDataFromDatabase()
                If DataOld IsNot Nothing AndAlso DataOld.ID_PPATK IsNot Nothing AndAlso DataOld.Submission_Type IsNot Nothing Then
                    ValidateIDPPATKInApproval(DataOld.ID_PPATK, DataOld.Submission_Type)
                End If
                If ErrorValidation <> "" Then
                        SiPendarBLL.NawaFramework.extInfoPanelupdate(FormPanelInput, ErrorValidation, "infoValidationResultPanel")
                        FormPanelInput.Body.ScrollTo(Direction.Top, 0)
                        infoValidationResultPanel.Hidden = False
                    End If
                End If
                ErrorValidation = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataFromDatabase()
        Try
            Dim dataStr As String = Request.Params("ID")
            DataID = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            DataOld = New SiPendarDAL.SIPENDAR_WATCHLIST
            DataOld.PK_SIPENDAR_WATCHLIST_ID = CLng(DataID)
            Dim stringquery As String = "select top 1 * from SIPENDAR_WATCHLIST where PK_SIPENDAR_WATCHLIST_ID = " & DataID
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery, Nothing)
            If drResult IsNot Nothing Then
                If Not IsDBNull(drResult("PERIODE")) Then
                    DataOld.PERIODE = drResult("PERIODE")
                    PERIODEField.SelectedDate = drResult("PERIODE")
                End If
                If Not IsDBNull(drResult("ID_PPATK")) Then
                    DataOld.ID_PPATK = drResult("ID_PPATK")
                    ID_PPATKField.Text = drResult("ID_PPATK")
                End If
                If Not IsDBNull(drResult("KODE_WATCHLIST")) Then
                    DataOld.KODE_WATCHLIST = drResult("KODE_WATCHLIST")
                    KODE_WATCHLISTField.Text = drResult("KODE_WATCHLIST")
                End If
                If Not IsDBNull(drResult("JENIS_PELAKU")) Then
                    DataOld.JENIS_PELAKU = drResult("JENIS_PELAKU")
                    JENIS_PELAKUField.Text = drResult("JENIS_PELAKU")
                End If
                If Not IsDBNull(drResult("NAMA_ASLI")) Then
                    DataOld.NAMA_ASLI = drResult("NAMA_ASLI")
                    NAMA_ASLIField.Text = drResult("NAMA_ASLI")
                End If
                If Not IsDBNull(drResult("PARAMETER_PENCARIAN_NAMA")) Then
                    DataOld.PARAMETER_PENCARIAN_NAMA = drResult("PARAMETER_PENCARIAN_NAMA")
                    PARAMETER_PENCARIAN_NAMAField.Text = drResult("PARAMETER_PENCARIAN_NAMA")
                End If
                If Not IsDBNull(drResult("TEMPAT_LAHIR")) Then
                    DataOld.TEMPAT_LAHIR = drResult("TEMPAT_LAHIR")
                    TEMPAT_LAHIRField.Text = drResult("TEMPAT_LAHIR")
                End If
                If Not IsDBNull(drResult("TANGGAL_LAHIR")) Then
                    DataOld.TANGGAL_LAHIR = drResult("TANGGAL_LAHIR")
                    TANGGAL_LAHIRField.SelectedDate = drResult("TANGGAL_LAHIR")
                End If
                If Not IsDBNull(drResult("NPWP")) Then
                    DataOld.NPWP = drResult("NPWP")
                    NPWPField.Text = drResult("NPWP")
                End If
                If Not IsDBNull(drResult("KTP")) Then
                    DataOld.KTP = drResult("KTP")
                    KTPField.Text = drResult("KTP")
                End If
                If Not IsDBNull(drResult("PAS")) Then
                    DataOld.PAS = drResult("PAS")
                    PASField.Text = drResult("PAS")
                End If
                If Not IsDBNull(drResult("KITAS")) Then
                    DataOld.KITAS = drResult("KITAS")
                    KITASField.Text = drResult("KITAS")
                End If
                If Not IsDBNull(drResult("SUKET")) Then
                    DataOld.SUKET = drResult("SUKET")
                    SUKETField.Text = drResult("SUKET")
                End If
                If Not IsDBNull(drResult("SIM")) Then
                    DataOld.SIM = drResult("SIM")
                    SIMField.Text = drResult("SIM")
                End If
                If Not IsDBNull(drResult("KITAP")) Then
                    DataOld.KITAP = drResult("KITAP")
                    KITAPField.Text = drResult("KITAP")
                End If

                'Ari 25 Oct 2021 Menambahn KIMS dan Watchlist
                If Not IsDBNull(drResult("KIMS")) Then
                    DataOld.KIMS = drResult("KIMS")
                    KIMSField.Text = drResult("KIMS")
                End If

                If Not IsDBNull(drResult("Watchlist")) Then
                    If drResult("Watchlist") = "True" Then
                        WatchlistField.Text = drResult("Watchlist")
                        DataOld.Watchlist = drResult("Watchlist")
                    Else
                        WatchlistField.Text = drResult("Watchlist")
                        DataOld.Watchlist = drResult("Watchlist")
                    End If
                End If

                If Not IsDBNull(drResult("Submission_Type")) Then
                    DataOld.Submission_Type = drResult("Submission_Type")
                    Dim strQuery As String = "select top 1 Kode, Keterangan from SIPENDAR_SUBMISSION_TYPE where Kode = '" & DataOld.Submission_Type & "'"
                    Dim drResultSubmission_Type As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                    If drResultSubmission_Type IsNot Nothing AndAlso Not IsDBNull(drResultSubmission_Type("Kode")) AndAlso Not IsDBNull(drResultSubmission_Type("Keterangan")) Then
                        Submission_Type.Text = drResultSubmission_Type("Keterangan")
                    Else
                        Submission_Type.Text = DataOld.Submission_Type
                    End If
                End If

                If Not IsDBNull(drResult("Active")) Then
                    DataOld.Active = drResult("Active")
                End If
                If Not IsDBNull(drResult("CreatedBy")) Then
                    DataOld.CreatedBy = drResult("CreatedBy")
                End If
                If Not IsDBNull(drResult("CreatedDate")) Then
                    DataOld.CreatedDate = drResult("CreatedDate")
                End If
                If Not IsDBNull(drResult("LastUpdateBy")) Then
                    DataOld.LastUpdateBy = drResult("LastUpdateBy")
                End If
                If Not IsDBNull(drResult("LastUpdateDate")) Then
                    DataOld.LastUpdateDate = drResult("LastUpdateDate")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ValidateIDPPATKInApproval(ID_PPATK As String, Submission_Type As String)
        Dim strQuery As String = "select top 1 ID_PPATK from SIPENDAR_WATCHLIST_Upload_approval where ID_PPATK = '" & ID_PPATK & "' and Submission_Type = '" & Submission_Type & "'"
        Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        If drResult IsNot Nothing Then
            ErrorValidation = ErrorValidation & "<br> Data With ID PPATK " & ID_PPATK & " and Submission Type " & Submission_Type & " Already Exist in Approval Upload. "
        End If

        strQuery = "select top 1 PK_ModuleApproval_ID  from ModuleApproval"
        strQuery = strQuery & " where ModuleName = '" & ObjModule.ModuleName & "'"
        strQuery = strQuery & " and ('" & Submission_Type & "-" & ID_PPATK & "') = ModuleKey"
        strQuery = strQuery & " and PK_ModuleAction_ID <> 7"
        drResult = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        If drResult IsNot Nothing Then
            ErrorValidation = ErrorValidation & "<br> Data With ID PPATK " & ID_PPATK & " and Submission Type " & Submission_Type & " Already Exist in Approval. "
        End If
    End Sub

    Private Function CheckIsDataDate(Data As String) As Boolean
        Try
            Dim tempdate As DateTime = CDate(Data)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function CheckIsDataNumberic(Data As String) As Boolean
        Try
            Return (Data.All(AddressOf Char.IsDigit))
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class
