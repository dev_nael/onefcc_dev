﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="SIPENDAR_WatchlistUpload.aspx.vb" Inherits="SIPENDAR_WatchlistUpload" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script  type="text/javascript">
        
        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };

        var updateMask = function (text) {
            App.df_ScreeningStatus.setValue(text);
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%-- Pop Up Window Upload Excel --%>
    <ext:Window ID="Window_UploadExcel" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" Layout="FitLayout" Title="Upload Watchlist from Excel File">
        <Items>
            <ext:FormPanel ID="FormPanel2" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:FileUploadField runat="server" ID="FileUploadExcel"  FieldLabel="Input File" Accept=".xlsx" AnchorHorizontal="100%" AllowBlank="false"/>
                    <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                    <ext:Panel runat="server" ID="Panel1" Layout="AnchorLayout">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_SUBMISSION_TYPE_Excel" ValueField="Kode" DisplayField="Keterangan" runat="server" 
                                StringField="Kode, Keterangan" StringTable="SIPENDAR_SUBMISSION_TYPE" Label="Submission Type" AllowBlank="false"/>
                        </Content>
                    </ext:Panel>
                    <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_UploadExcel_Save" runat="server" Icon="DiskUpload" Text="Import">
                <DirectEvents>
                    <Click OnEvent="btn_UploadExcel_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_UploadExcel_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_UploadExcel_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.5});" />
            <Resize Handler="#{Window_UploadExcel}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Upload Excel --%>

    <%-- Pop Up Window Upload XML --%>
    <ext:Window ID="Window_UploadXML" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" Layout="FitLayout" Title="Upload Watchlist from XML File">
        <Items>
            <ext:FormPanel ID="FormPanel1" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:FileUploadField runat="server" ID="FileUploadXML"  FieldLabel="Input File" Accept=".xml" AnchorHorizontal="100%" AllowBlank="false"/>
                    <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                    <ext:Panel runat="server" ID="Panel2" Layout="AnchorLayout">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_SUBMISSION_TYPE_XML" ValueField="Kode" DisplayField="Keterangan" runat="server" 
                                StringField="Kode, Keterangan" StringTable="SIPENDAR_SUBMISSION_TYPE" Label="Submission Type" AllowBlank="false"/>
                        </Content>
                    </ext:Panel>
                    <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_UploadXML_Save" runat="server" Icon="DiskUpload" Text="Import">
                <DirectEvents>
                    <Click OnEvent="btn_UploadXML_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_UploadXML_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_UploadXML_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.5});" />
            <Resize Handler="#{Window_UploadXML}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Upload XML --%>

    <ext:FormPanel ID="FormPanelInput" AutoScroll="true" Layout="AnchorLayout" AnchorHorizontal="90%" ButtonAlign="Center" runat="server" Height="300" Title="">
        <Items>
            <ext:DisplayField runat="server" Text="Notes : Data Watchlist akan di Update apabila ID PPATK yang diupload dan Submission Type yang dipilih sama seperti data yang sudah ada di database." MarginSpec="10 0 0 10"></ext:DisplayField>
            <ext:Button ID="btn_UploadExcel" runat="server" Icon="DiskUpload" Text="Upload Excel" MarginSpec="10 10 0 10">
                <DirectEvents>
                    <Click OnEvent="btn_UploadExcel_Click">
                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="200"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_UploadXML" runat="server" Icon="DiskUpload" Text="Upload XML" MarginSpec="10 0 0 0">
                <DirectEvents>
                    <Click OnEvent="btn_UploadXML_Click">
                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="200"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Panel runat="server" ID="PanelValid" Layout="AnchorLayout" Border="TRUE" Title="Data Valid" Margin="10" Collapsible="true">
                <Items>
                    <%--Grid Upload Result Valid--%>
                    <ext:GridPanel ID="gp_UploadResult" runat="server" Border="false">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="store_Watchlist_Valid" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" PageSize="10"
                                OnReadData="store_Watchlist_Valid_ReadData" RemotePaging="true" ClientIDMode="Static">
                                <Model>
                                    <ext:Model runat="server" ID="model3" IDProperty="PK_Upload_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Upload_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="nawa_Action" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PERIODE_DT" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="ID_PPATK_INT" Type="Int" ></ext:ModelField>
                                            <ext:ModelField Name="KODE_WATCHLIST" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JENIS_PELAKU" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMA_ASLI" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PARAMETER_PENCARIAN_NAMA" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TEMPAT_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TANGGAL_LAHIR_DT" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="NPWP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KTP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SUKET" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SIM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAP" Type="String"></ext:ModelField>

                                            <%-- 26-Oct-2021 Penambahan 2 Field dari PPATK --%>
                                            <ext:ModelField Name="KIMS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Watchlist" Type="String"></ext:ModelField>
                                            <%-- End of 26-Oct-2021 Penambahan 2 Field dari PPATK --%>
                                            
                                            <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                            <ext:ModelField Name="Submission_Type" Type="String"></ext:ModelField>
                                            <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
<%--                                   <Sorters>
                                        <ext:DataSorter Property="ID_PPATK" Direction="ASC" />
                                    </Sorters>--%>
                                <Proxy>
                                    <ext:PageProxy />
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No."></ext:RowNumbererColumn>
                                <ext:Column ID="Column31" runat="server" DataIndex="nawa_Action" Text="Action" MinWidth="60"></ext:Column>
                                <ext:DateColumn ID="Column16" runat="server" DataIndex="PERIODE_DT" Text="Periode" MinWidth="120" Format="dd-MMM-yyyy" ClientIDMode="Static">
                                    <Items>
                                        <ext:DateField ID="Column16_DateField1" runat="server" Format="dd-MMM-yyyy" FormatText="Expected Date Format dd-MMM-yyyy">
                                            <Plugins>
                                                <ext:ClearButton ID="Column16_ClearButton1">
                                                </ext:ClearButton>
                                            </Plugins>
                                        </ext:DateField>
                                    </Items>
                                    <Filter>
                                        <ext:DateFilter >
                                        </ext:DateFilter>
                                    </Filter>
								</ext:DateColumn>
                                <ext:NumberColumn ID="Column4" runat="server" DataIndex="ID_PPATK_INT" Text="Id" MinWidth="40" Format="#,##0" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column5" runat="server" DataIndex="KODE_WATCHLIST" Text="Kode Watchlist" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="JENIS_PELAKU" Text="Jenis Pelaku" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="NAMA_ASLI" Text="Nama Asli" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="PARAMETER_PENCARIAN_NAMA" Text="Parameter Pencarian Nama" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="TEMPAT_LAHIR" Text="Tempat Lahir" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="Column10" runat="server" DataIndex="TANGGAL_LAHIR_DT" Text="Tanggal Lahir" MinWidth="120" Format="dd-MMM-yyyy">
                                    <Items>
                                        <ext:DateField ID="DateField1" runat="server" Format="dd-MMM-yyyy" FormatText="Expected Date Format dd-MMM-yyyy">
                                            <Plugins>
                                                <ext:ClearButton ID="Column10_ClearButton1">
                                                </ext:ClearButton>
                                            </Plugins>
                                        </ext:DateField>
                                    </Items>
                                    <Filter>
                                        <ext:DateFilter >
                                        </ext:DateFilter>
                                    </Filter>
								</ext:DateColumn>
                                <ext:Column ID="Column11" runat="server" DataIndex="NPWP" Text="NPWP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="KTP" Text="KTP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="PAS" Text="PAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="KITAS" Text="KITAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="SUKET" Text="SUKET" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="SIM" Text="SIM" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="KITAP" Text="KITAP" MinWidth="150"></ext:Column>

                                <%-- 26-Oct-2021 Penambahan 2 Field dari PPATK --%>
                                <ext:Column ID="Column34" runat="server" DataIndex="KIMS" Text="KIMS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column35" runat="server" DataIndex="Watchlist" Text="Watchlist" MinWidth="100"></ext:Column>
                                <%-- End of 26-Oct-2021 Penambahan 2 Field dari PPATK --%>
                                
                                <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                <ext:Column ID="Column52" runat="server" DataIndex="Submission_Type" Text="Submission Type" MinWidth="150"></ext:Column>
                                <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>

                            </Columns>

                        </ColumnModel>

                        <Plugins>
                            <ext:FilterHeader runat="server" ID="FilterHeader_Valid" Remote="true" ClientIDMode="Static"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                        <Listeners>
                            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width, height: size.height});" />
                            <Resize Handler="#{gp_UploadResult}.center()" />
                        </Listeners>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            <ext:Panel runat="server" ID="PanelInValid" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Data Invalid" MarginSpec="0 10" Collapsible="true" >
                <Items>
                     <%--Grid Invalid--%>
                    <ext:GridPanel ID="GridPanelInvalid" runat="server" Title="" Border="false">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <%--<ext:Store ID="StoreInvalidData" runat="server" IsPagingStore="true" PageSize="10">--%>
                            <ext:Store ID="store_Watchlist_Invalid" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" PageSize="10" 
                                OnReadData="store_Watchlist_Invalid_ReadData" RemotePaging="true" ClientIDMode="Static">
                                <Model>
                                    <ext:Model runat="server" ID="model1" IDProperty="PK_Upload_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Upload_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PERIODE_DT" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="ID_PPATK_INT" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="KODE_WATCHLIST" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JENIS_PELAKU" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMA_ASLI" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PARAMETER_PENCARIAN_NAMA" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TEMPAT_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TANGGAL_LAHIR_DT" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="NPWP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KTP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SUKET" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SIM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAP" Type="String"></ext:ModelField>

                                            <%-- 26-Oct-2021 Penambahan 2 Field dari PPATK --%>
                                            <ext:ModelField Name="KIMS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Watchlist" Type="String"></ext:ModelField>
                                            <%-- End of 26-Oct-2021 Penambahan 2 Field dari PPATK --%>
                                            
                                            <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                            <ext:ModelField Name="Submission_Type" Type="String"></ext:ModelField>
                                            <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>

                                            <%--Strart New Changed Data Show--%>
                                            <ext:ModelField Name="KeteranganError" Type="String"></ext:ModelField>
                                            <%--End--%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                 <%--<Sorters>
                                    <ext:DataSorter Property="ID_PPATK" Direction="ASC" />
                                </Sorters>--%>
                                <Proxy>
                                    <ext:PageProxy />
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No."></ext:RowNumbererColumn>
                                <ext:DateColumn ID="Column1" runat="server" DataIndex="PERIODE_DT" Text="Periode" MinWidth="120" Format="dd-MMM-yyyy">
                                    <Items>
                                        <ext:DateField ID="DateField2" runat="server" Format="dd-MMM-yyyy" FormatText="Expected Date Format dd-MMM-yyyy">
                                            <Plugins>
                                                <ext:ClearButton ID="Column1_ClearButton1">
                                                </ext:ClearButton>
                                            </Plugins>
                                        </ext:DateField>
                                    </Items>
                                    <Filter>
                                        <ext:DateFilter >
                                        </ext:DateFilter>
                                    </Filter>
								</ext:DateColumn>
                                <ext:NumberColumn  ID="Column2" runat="server" DataIndex="ID_PPATK_INT" Text="Id" MinWidth="40" Format="#,##0" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column3" runat="server" DataIndex="KODE_WATCHLIST" Text="Kode Watchlist" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="JENIS_PELAKU" Text="Jenis Pelaku" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="NAMA_ASLI" Text="Nama Asli" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column21" runat="server" DataIndex="PARAMETER_PENCARIAN_NAMA" Text="Parameter Pencarian Nama" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="TEMPAT_LAHIR" Text="Tempat Lahir" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="Column23" runat="server" DataIndex="TANGGAL_LAHIR_DT" Text="Tanggal Lahir" MinWidth="120" Format="dd-MMM-yyyy">
                                    <Items>
                                        <ext:DateField ID="DateField3" runat="server" Format="dd-MMM-yyyy" FormatText="Expected Date Format dd-MMM-yyyy">
                                            <Plugins>
                                                <ext:ClearButton ID="Column23_ClearButton1">
                                                </ext:ClearButton>
                                            </Plugins>
                                        </ext:DateField>
                                    </Items>
                                    <Filter>
                                        <ext:DateFilter >
                                        </ext:DateFilter>
                                    </Filter>
								</ext:DateColumn>
                                <ext:Column ID="Column24" runat="server" DataIndex="NPWP" Text="NPWP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column25" runat="server" DataIndex="KTP" Text="KTP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="PAS" Text="PAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column27" runat="server" DataIndex="KITAS" Text="KITAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column28" runat="server" DataIndex="SUKET" Text="SUKET" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column29" runat="server" DataIndex="SIM" Text="SIM" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column30" runat="server" DataIndex="KITAP" Text="KITAP" MinWidth="150"></ext:Column>

                                <%-- 26-Oct-2021 Penambahan 2 Field dari PPATK --%>
                                <ext:Column ID="Column32" runat="server" DataIndex="KIMS" Text="KIMS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column33" runat="server" DataIndex="Watchlist" Text="Watchlist" MinWidth="100"></ext:Column>
                                <%-- End of 26-Oct-2021 Penambahan 2 Field dari PPATK --%>
                                
                                <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                <ext:Column ID="Column53" runat="server" DataIndex="Submission_Type" Text="Submission Type" MinWidth="150"></ext:Column>
                                <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server" ID="FilterHeader_Invalid" Remote="true" ClientIDMode="Static"></ext:FilterHeader>
                            <%--Strart New Changed Data Show--%>
                            <ext:RowExpander ID="RowExpander1" runat="server">
                                <Template ID="Template1" runat="server">
                                    <Html>
                                        <p><br>Error List:</br> {KeteranganError}</p>
                                        <br />
                                    </Html>
                                </Template>
                            </ext:RowExpander>
                            <%--End--%>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            <ext:Panel runat="server" ID="PanelRawData" Layout="AnchorLayout" Border="TRUE" Title="Raw Data" Margin="10" Collapsible="true">
                <Items>
                    <%--Grid Upload Result Valid--%>
                    <ext:GridPanel ID="gp_UploadRawData" runat="server" Border="false" >
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="store_Watchlist_RawData" runat="server" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="model2" >
                                        <Fields>
                                            <ext:ModelField Name="PK_Upload_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PERIODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_PPATK" Type="String" ></ext:ModelField>
                                            <ext:ModelField Name="KODE_WATCHLIST" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JENIS_PELAKU" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMA_ASLI" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PARAMETER_PENCARIAN_NAMA" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TEMPAT_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TANGGAL_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NPWP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KTP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SUKET" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SIM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KIMS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Watchlist" Type="String"></ext:ModelField>
                                            <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                            <ext:ModelField Name="Submission_Type" Type="String"></ext:ModelField>
                                            <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No."></ext:RowNumbererColumn>
                                <ext:Column ID="Column51" runat="server" DataIndex="PERIODE" Text="Periode" MinWidth="150"></ext:Column>
                                <ext:Column ID="NumberColumn1" runat="server" DataIndex="ID_PPATK" Text="Id" MinWidth="40" Align="Right"></ext:Column>
                                <ext:Column ID="Column37" runat="server" DataIndex="KODE_WATCHLIST" Text="Kode Watchlist" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column38" runat="server" DataIndex="JENIS_PELAKU" Text="Jenis Pelaku" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column39" runat="server" DataIndex="NAMA_ASLI" Text="Nama Asli" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column40" runat="server" DataIndex="PARAMETER_PENCARIAN_NAMA" Text="Parameter Pencarian Nama" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column41" runat="server" DataIndex="TEMPAT_LAHIR" Text="Tempat Lahir" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column36" runat="server" DataIndex="TANGGAL_LAHIR" Text="Tanggal Lahir" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column42" runat="server" DataIndex="NPWP" Text="NPWP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column43" runat="server" DataIndex="KTP" Text="KTP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column44" runat="server" DataIndex="PAS" Text="PAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column45" runat="server" DataIndex="KITAS" Text="KITAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column46" runat="server" DataIndex="SUKET" Text="SUKET" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column47" runat="server" DataIndex="SIM" Text="SIM" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column48" runat="server" DataIndex="KITAP" Text="KITAP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column49" runat="server" DataIndex="KIMS" Text="KIMS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column50" runat="server" DataIndex="Watchlist" Text="Watchlist" MinWidth="100"></ext:Column>
                                <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                <ext:Column ID="Column54" runat="server" DataIndex="Submission_Type" Text="Submission Type" MinWidth="150"></ext:Column>
                                <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                            </Columns>

                        </ColumnModel>

                        <Plugins>
                            <ext:FilterHeader runat="server" ID="FilterHeader_RawData" ></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                        <Listeners>
                            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width, height: size.height});" />
                            <Resize Handler="#{gp_UploadRawData}.center()" />
                        </Listeners>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Save" ClientIDMode="Static" runat="server" Text="Save" Enabled="false" Icon="DiskBlack" Hidden="false">
                <DirectEvents>
                    <Click OnEvent="btn_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Cancel" runat="server" Text="Cancel" Icon="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Cancel_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="Accept">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>