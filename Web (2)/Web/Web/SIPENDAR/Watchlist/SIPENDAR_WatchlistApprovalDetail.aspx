﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="SIPENDAR_WatchlistApprovalDetail.aspx.vb" Inherits="SIPENDAR_Watchlist_SIPENDAR_WatchlistUpload_ApprovalDetail" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Title="SIPENDAR Watchlist Approval" Layout="AnchorLayout" ButtonAlign="Center" autoscroll="true" Hidden="false" >
        <Items>
            <ext:FormPanel ID="PanelInfo" runat="server" Title="" BodyPadding="0" Layout="ColumnLayout" AnchorHorizontal="100%">
                <Items>
                    <ext:FieldSet runat="server" ColumnWidth="0.5" Border="false">
                        <Items>
                            <ext:DisplayField ID="lblModuleName" runat="server" FieldLabel="Module Name">
                            </ext:DisplayField>
                            <ext:DisplayField ID="lblModuleKey" runat="server" FieldLabel="Module Key">
                            </ext:DisplayField>
                            <ext:DisplayField ID="lblAction" runat="server" FieldLabel="Action">
                            </ext:DisplayField>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet runat="server" ColumnWidth="0.45" Border="false">
                        <Items>
                            <ext:DisplayField ID="LblCreatedBy" runat="server" FieldLabel="Created By">
                            </ext:DisplayField>
                            <ext:DisplayField ID="lblCreatedDate" runat="server" FieldLabel="Created Date">
                            </ext:DisplayField>
                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:FormPanel>
            
            <ext:Panel runat="server" ID="PanelCleanedData" Layout="AnchorLayout" Border="TRUE" Title="Sipendar Watchlist Saved Data" Margin="10" Collapsible="true" AnchorHorizontal="100%" hidden="true">
                <Items>
                    <ext:GridPanel ID="GridPanelWatchlisCleansing" runat="server" EmptyText="No Available Data"  AnchorHorizontal="100%">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreWatchlistCleansing" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model2" IDProperty="PK_SIPENDAR_WATCHLIST_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_WATCHLIST_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="nawaaction" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PERIODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_PPATK" Type="int"></ext:ModelField>
                                            <ext:ModelField Name="KODE_WATCHLIST" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JENIS_PELAKU" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMA_ASLI" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PARAMETER_PENCARIAN_NAMA" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TEMPAT_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TANGGAL_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NPWP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KTP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SUKET" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SIM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KIMS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Watchlist" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastUpdateDate" Type="String"></ext:ModelField>
                                            <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                            <ext:ModelField Name="Submission_Type" Type="String"></ext:ModelField>
                                            <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters>
                                    <ext:DataSorter Property="ID_PPATK" Direction="ASC" />
                                </Sorters>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No." MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column38" runat="server" DataIndex="nawaaction" Text="Action" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column39" runat="server" DataIndex="PERIODE" Text="Periode" MinWidth="120" Format="dd-MMM-yyyy"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn2" runat="server" DataIndex="ID_PPATK" Text="ID" MinWidth="40" Format="#,##0" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column40" runat="server" DataIndex="KODE_WATCHLIST" Text="Kode Watchlist" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column41" runat="server" DataIndex="JENIS_PELAKU" Text="Jenis Pelaku" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column42" runat="server" DataIndex="NAMA_ASLI" Text="Nama Asli" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column43" runat="server" DataIndex="PARAMETER_PENCARIAN_NAMA" Text="Parameter Pencarian Nama" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column44" runat="server" DataIndex="TEMPAT_LAHIR" Text="Tempat Lahir" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="TANGGAL_LAHIR" Text="Tanggal Lahir" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column45" runat="server" DataIndex="NPWP" Text="NPWP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column46" runat="server" DataIndex="KTP" Text="KTP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column47" runat="server" DataIndex="PAS" Text="PAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column48" runat="server" DataIndex="KITAS" Text="KITAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column49" runat="server" DataIndex="SUKET" Text="SUKET" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column50" runat="server" DataIndex="SIM" Text="SIM" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column51" runat="server" DataIndex="KITAP" Text="KITAP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column52" runat="server" DataIndex="KIMS" Text="KIMS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column53" runat="server" DataIndex="Watchlist" Text="Watchlist" MinWidth="150"></ext:Column>
                                <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                <ext:Column ID="Column1" runat="server" DataIndex="Submission_Type" Text="Submission Type" MinWidth="150"></ext:Column>
                                <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            
            <ext:Panel runat="server" ID="PanelOldData" Layout="AnchorLayout" Border="TRUE" Title="Sipendar Watchlist Old Data" Margin="10" Collapsible="true" AnchorHorizontal="100%" hidden="true">
                <Items>
                    <ext:GridPanel ID="GridPanelWatchlisOld" runat="server" EmptyText="No Available Data"  AnchorHorizontal="100%" >
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreWatchlistOld" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model3" IDProperty="PK_SIPENDAR_WATCHLIST_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_WATCHLIST_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="PERIODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_PPATK" Type="int"></ext:ModelField>
                                            <ext:ModelField Name="KODE_WATCHLIST" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JENIS_PELAKU" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMA_ASLI" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PARAMETER_PENCARIAN_NAMA" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TEMPAT_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TANGGAL_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NPWP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KTP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SUKET" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SIM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KIMS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Watchlist" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastUpdateDate" Type="String"></ext:ModelField>
                                            <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                            <ext:ModelField Name="Submission_Type" Type="String"></ext:ModelField>
                                            <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters>
                                    <ext:DataSorter Property="ID_PPATK" Direction="ASC" />
                                </Sorters>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No." MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column54" runat="server" DataIndex="PERIODE" Text="Periode" MinWidth="120" Format="dd-MMM-yyyy"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn3" runat="server" DataIndex="ID_PPATK" Text="ID" MinWidth="40" Format="#,##0" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column55" runat="server" DataIndex="KODE_WATCHLIST" Text="Kode Watchlist" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column56" runat="server" DataIndex="JENIS_PELAKU" Text="Jenis Pelaku" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column57" runat="server" DataIndex="NAMA_ASLI" Text="Nama Asli" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column58" runat="server" DataIndex="PARAMETER_PENCARIAN_NAMA" Text="Parameter Pencarian Nama" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column59" runat="server" DataIndex="TEMPAT_LAHIR" Text="Tempat Lahir" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="DateColumn3" runat="server" DataIndex="TANGGAL_LAHIR" Text="Tanggal Lahir" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column60" runat="server" DataIndex="NPWP" Text="NPWP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column61" runat="server" DataIndex="KTP" Text="KTP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column62" runat="server" DataIndex="PAS" Text="PAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column63" runat="server" DataIndex="KITAS" Text="KITAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column64" runat="server" DataIndex="SUKET" Text="SUKET" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column65" runat="server" DataIndex="SIM" Text="SIM" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column66" runat="server" DataIndex="KITAP" Text="KITAP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column67" runat="server" DataIndex="KIMS" Text="KIMS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column68" runat="server" DataIndex="Watchlist" Text="Watchlist" MinWidth="150"></ext:Column>
                                <%--<ext:Column ID="Column30" runat="server" DataIndex="CreatedBy" Text="CreatedBy" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="Column31" runat="server" DataIndex="LastUpdateDate" Text="LastUpdateDate" MinWidth="150" Format="dd-MMM-yyyy"></ext:DateColumn>--%>
                                <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                <ext:Column ID="Column2" runat="server" DataIndex="Submission_Type" Text="Submission Type" MinWidth="150"></ext:Column>
                                <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>  
            
            <ext:Panel runat="server" ID="PanelRawData" Layout="AnchorLayout" Border="TRUE" Title="Sipendar Watchlist Upload Data" Margin="10" Collapsible="true" AnchorHorizontal="100%" hidden="true">
                <Items>
                    <ext:GridPanel ID="GridPanel1" runat="server" EmptyText="No Available Data"  AnchorHorizontal="100%" >
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreWatchlistNew" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model4" IDProperty="PK_SIPENDAR_WATCHLIST_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_WATCHLIST_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="PERIODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_PPATK" Type="int"></ext:ModelField>
                                            <ext:ModelField Name="KODE_WATCHLIST" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JENIS_PELAKU" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMA_ASLI" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PARAMETER_PENCARIAN_NAMA" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TEMPAT_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TANGGAL_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NPWP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KTP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SUKET" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SIM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KIMS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Watchlist" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastUpdateDate" Type="String"></ext:ModelField>
                                            <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                            <ext:ModelField Name="Submission_Type" Type="String"></ext:ModelField>
                                            <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters>
                                    <ext:DataSorter Property="ID_PPATK" Direction="ASC" />
                                </Sorters>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No." MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column69" runat="server" DataIndex="PERIODE" Text="Periode" MinWidth="120" Format="dd-MMM-yyyy"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn4" runat="server" DataIndex="ID_PPATK" Text="ID" MinWidth="40" Format="#,##0" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column70" runat="server" DataIndex="KODE_WATCHLIST" Text="Kode Watchlist" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column71" runat="server" DataIndex="JENIS_PELAKU" Text="Jenis Pelaku" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column72" runat="server" DataIndex="NAMA_ASLI" Text="Nama Asli" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column73" runat="server" DataIndex="PARAMETER_PENCARIAN_NAMA" Text="Parameter Pencarian Nama" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column74" runat="server" DataIndex="TEMPAT_LAHIR" Text="Tempat Lahir" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="DateColumn4" runat="server" DataIndex="TANGGAL_LAHIR" Text="Tanggal Lahir" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column75" runat="server" DataIndex="NPWP" Text="NPWP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column76" runat="server" DataIndex="KTP" Text="KTP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column77" runat="server" DataIndex="PAS" Text="PAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column78" runat="server" DataIndex="KITAS" Text="KITAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column79" runat="server" DataIndex="SUKET" Text="SUKET" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column80" runat="server" DataIndex="SIM" Text="SIM" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column81" runat="server" DataIndex="KITAP" Text="KITAP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column82" runat="server" DataIndex="KIMS" Text="KIMS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column83" runat="server" DataIndex="Watchlist" Text="Watchlist" MinWidth="150"></ext:Column>
                                <%--<ext:Column ID="Column30" runat="server" DataIndex="CreatedBy" Text="CreatedBy" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="Column31" runat="server" DataIndex="LastUpdateDate" Text="LastUpdateDate" MinWidth="150" Format="dd-MMM-yyyy"></ext:DateColumn>--%>
                                <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                                <ext:Column ID="Column3" runat="server" DataIndex="Submission_Type" Text="Submission Type" MinWidth="150"></ext:Column>
                                <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>    

            <%--<ext:Panel ID="pnl_Upload" runat="server" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" AnchorHorizontal="100%" BodyPadding="5">
                <Items>
                    <ext:GridPanel ID="GridPanelWatchlistOld" runat="server" EmptyText="No Available Data"  Title="Sipendar Watchlist Old" AnchorHorizontal="100%">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreWatchlistOld" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model355" IDProperty="PK_SIPENDAR_WATCHLIST_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_WATCHLIST_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="PERIODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_PPATK" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KODE_WATCHLIST" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JENIS_PELAKU" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMA_ASLI" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PARAMETER_PENCARIAN_NAMA" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TEMPAT_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TANGGAL_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NPWP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KTP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SUKET" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SIM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAP" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="KIMS" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="Watchlist" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastUpdateDate" Type="DATE"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No." MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column16" runat="server" DataIndex="PERIODE" Text="Periode" MinWidth="120"></ext:Column>
                                <ext:NumberColumn ID="Column4" runat="server" DataIndex="ID_PPATK" Text="Id" MinWidth="40" Format="#,##0" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column5" runat="server" DataIndex="KODE_WATCHLIST" Text="Kode Watchlist" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="JENIS_PELAKU" Text="Jenis Pelaku" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="NAMA_ASLI" Text="Nama Asli" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="PARAMETER_PENCARIAN_NAMA" Text="Parameter Pencarian Nama" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="TEMPAT_LAHIR" Text="Tempat Lahir" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="Column10" runat="server" DataIndex="TANGGAL_LAHIR" Text="Tanggal Lahir" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column11" runat="server" DataIndex="NPWP" Text="NPWP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="KTP" Text="KTP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="PAS" Text="PAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="KITAS" Text="KITAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="SUKET" Text="SUKET" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column1" runat="server" DataIndex="SIM" Text="SIM" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="KITAP" Text="KITAP" MinWidth="150"></ext:Column>
                                  <ext:Column ID="Column33" runat="server" DataIndex="KIMS" Text="KIMS" MinWidth="150"></ext:Column>
                                  <ext:Column ID="Column34" runat="server" DataIndex="Watchlist" Text="Watchlist" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column3" runat="server" DataIndex="CreatedBy" Text="CreatedBy" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column35" runat="server" DataIndex="LastUpdateDate" Text="LastUpdateDate" MinWidth="150"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelWatchlistNew" runat="server" EmptyText="No Available Data" Title="Sipendar Watchlist Uploaded" ColumnWidth="1">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreWatchlistNew" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model1" IDProperty="PK_SIPENDAR_WATCHLIST_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_SIPENDAR_WATCHLIST_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="PERIODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_PPATK" Type="int"></ext:ModelField>
                                            <ext:ModelField Name="KODE_WATCHLIST" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JENIS_PELAKU" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAMA_ASLI" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PARAMETER_PENCARIAN_NAMA" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TEMPAT_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TANGGAL_LAHIR" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NPWP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KTP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SUKET" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SIM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KITAP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KIMS" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="WatchList" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastUpdateDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="nawa_Action" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                 <Sorters>
                    <ext:DataSorter Property="ID_PPATK" Direction="ASC" />
                </Sorters>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No."></ext:RowNumbererColumn>
                                <ext:Column ID="Column32" runat="server" DataIndex="nawa_Action" Text="Action" MinWidth="60"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="PERIODE" Text="Periode" MinWidth="120"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn1" runat="server" DataIndex="ID_PPATK" Text="Id" MinWidth="40" Format="#,##0" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column18" runat="server" DataIndex="KODE_WATCHLIST" Text="Kode Watchlist" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="JENIS_PELAKU" Text="Jenis Pelaku" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="NAMA_ASLI" Text="Nama Asli" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column21" runat="server" DataIndex="PARAMETER_PENCARIAN_NAMA" Text="Parameter Pencarian Nama" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="TEMPAT_LAHIR" Text="Tempat Lahir" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="TANGGAL_LAHIR" Text="Tanggal Lahir" MinWidth="120" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column23" runat="server" DataIndex="NPWP" Text="NPWP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="KTP" Text="KTP" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column25" runat="server" DataIndex="PAS" Text="PAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="KITAS" Text="KITAS" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column27" runat="server" DataIndex="SUKET" Text="SUKET" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column28" runat="server" DataIndex="SIM" Text="SIM" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column29" runat="server" DataIndex="KITAP" Text="KITAP" MinWidth="150"></ext:Column>
                                  <ext:Column ID="Column36" runat="server" DataIndex="KIMS" Text="KIMS" MinWidth="150"></ext:Column>
                                  <ext:Column ID="Column37" runat="server" DataIndex="WatchList" Text="WatchList" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column30" runat="server" DataIndex="CreatedBy" Text="CreatedBy" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column31" runat="server" DataIndex="LastUpdateDate" Text="LastUpdateDate" MinWidth="150"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                </Items>
            </ext:Panel>--%>

            <ext:FormPanel runat="server" ID="fp_NonUpload" Layout="HBoxLayout" AnchorHorizontal="100%">
                <Items>
                    <ext:FormPanel runat="server" ID="FormPanelOld" Hidden="true" Title="Watchlist Old" Flex="1" BodyPadding="10">
                        <Items>
                        </Items>
                    </ext:FormPanel>

                    <ext:FormPanel runat="server" ID="FormPanelNew" Hidden="true"  Title="Watchlist New" Flex="1" BodyPadding="10">
                        <Items>
                        </Items>
                    </ext:FormPanel>
                </Items>
            </ext:FormPanel>


            <ext:formpanel runat="server" AnchorHorizontal="100%" Layout="AnchorLayout" MarginSpec="10 0 0 0" BodyPadding="10">
                <Items>
                        <ext:TextArea ID="txtApprovalNotes" runat="server" FieldLabel="Approval/Rejection Note" AllowBlank="false" AnchorHorizontal="100%"/>
                </Items>
            </ext:formpanel>

        </Items>
        <Buttons>
            <ext:Button ID="BtnSave" runat="server" Text="Approve" Icon="DiskBlack">
                <DirectEvents>
                    <Click OnEvent="BtnSave_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"> </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnReject" runat="server" Text="Reject" Icon="Decline">
                <DirectEvents>
                    <Click OnEvent="BtnReject_Click">
                        <EventMask ShowMask="true" Msg="Saving Reject Data..." MinDelay="500"> </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancel" runat="server" Text="Cancel" Icon="PageBack">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"> </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

     <ext:FormPanel ID="Panelconfirmation" runat="server" ClientIDMode="Static" Title="Confirmation" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
                <Defaults>
                    <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
                </Defaults>
                <LayoutConfig>
                    <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
                </LayoutConfig>
                <Items>
                    <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel" Tex="aa">
                    </ext:Label>
                </Items>

                <Buttons>
                    <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
</asp:Content>


