﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="SIPENDAR_WatchlistAdd.aspx.vb" Inherits="SIPENDAR_WatchlistAdd" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .x-form-item-label.x-form-item-label-default { width : 230px !important; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel ID="FormPanelInput" runat="server" Layout="AnchorLayout" ButtonAlign="Center" Title="SIPENDAR Watchlist ADD" BodyStyle="padding:10px" AutoScroll="true" Hidden="false">
        <Items>
            <ext:InfoPanel ID="infoValidationResultPanel" ClientIDMode="Static" runat="server" UI="Warning" Layout="AnchorLayout" Title="Validation Result" IconCls="#Error" MarginSpec="10 0" Border="True" Hidden="true"></ext:InfoPanel>
            <ext:Panel runat="server" ID="ReportGeneralInformationPanel" Layout="AnchorLayout" ClientIDMode="Static" BodyStyle="padding:10px" Margin="5" Border="True">
                <Items>
                    <ext:DateField ID="PERIODEField" runat="server" FieldLabel="Periode" Format="dd-MMM-yy" AnchorHorizontal="70%" AllowBlank="false"/>

                    <ext:TextField ID="ID_PPATKField" runat="server" FieldLabel="ID PPATK" AnchorHorizontal="70%" AllowBlank="false" EnableKeyEvents="true" MaskRe="[0-9]"/>
                    <ext:TextField ID="KODE_WATCHLISTField" runat="server" FieldLabel="Kode Watchlist" AnchorHorizontal="70%" AllowBlank="false"/>
                    <ext:TextField ID="JENIS_PELAKUField" runat="server" FieldLabel="Jenis Pelaku" AnchorHorizontal="70%" AllowBlank="false"/>
                    <ext:TextField ID="NAMA_ASLIField" runat="server" FieldLabel="Nama Asli" AnchorHorizontal="70%" AllowBlank="false"/>
                    <ext:TextField ID="PARAMETER_PENCARIAN_NAMAField" runat="server" FieldLabel="Parameter Pencarian" AnchorHorizontal="70%" AllowBlank="false"/>
                    <ext:TextField ID="TEMPAT_LAHIRField" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="70%" AllowBlank="true"/>

                    <ext:DateField ID="TANGGAL_LAHIRField" runat="server" FieldLabel="Tanggal Lahir" Format="dd-MMM-yy" AnchorHorizontal="70%" AllowBlank="true"/>

                    <ext:TextField ID="NPWPField" runat="server" FieldLabel="NPWP" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]"/>
                    <ext:TextField ID="KTPField" runat="server" FieldLabel="KTP" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]"/>
                    <ext:TextField ID="PASField" runat="server" FieldLabel="PAS" AnchorHorizontal="70%" AllowBlank="true" />
                    <ext:TextField ID="KITASField" runat="server" FieldLabel="KITAS" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]"/>
                    <ext:TextField ID="SUKETField" runat="server" FieldLabel="SUKET" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]"/>
                    <ext:TextField ID="SIMField" runat="server" FieldLabel="SIM" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]"/>
                    <ext:TextField ID="KITAPField" runat="server" FieldLabel="KITAP" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]"/>
                    <ext:TextField ID="KIMSField" runat="server" FieldLabel="KIMS" AnchorHorizontal="70%" AllowBlank="true" EnableKeyEvents="true" MaskRe="[0-9]"/>
                      <ext:Checkbox ID="chk_IsWatchlist" AnchorHorizontal="70%" runat="server" FieldLabel="Watchlist">
                              <%--  <DirectEvents>
                                    <Change OnEvent="chk_IsWatchlist_Change"></Change>
                                </DirectEvents>--%>
                            </ext:Checkbox>
                    <%-- 14-Des-2021 Penambahan Field Submission_Type --%>
                    <ext:Panel runat="server" ID="Panel1" Layout="AnchorLayout" AnchorHorizontal="70%">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_SUBMISSION_TYPE" ValueField="Kode" DisplayField="Keterangan" runat="server" 
                                StringField="Kode, Keterangan" StringTable="SIPENDAR_SUBMISSION_TYPE" Label="Submission Type" AllowBlank="false"/>
                        </Content>
                    </ext:Panel>
                    <%-- End of 14-Des-2021 Penambahan Field Submission_Type --%>
                </Items>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btnSave" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="BtnSave_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="100"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="100"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>

