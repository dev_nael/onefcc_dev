﻿Imports System.Data
Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Drawing
Imports System.IO
Imports System.Threading
Imports OfficeOpenXml
Imports OfficeOpenXml.Style
Imports SiPendarBLL

'===================== Changes Log : ================================================
'30-Sep-2021 Adi    : Ubah konsep penyimpanan XML menjadi baris saja ke table SIPENDAR_WATCHLIST_Upload_approval karena lambat saat serialize-deserialize XML
'13-Oct-2021 Adi    : Tambah datetime picker filtering di kolom PERIODE dan TANGGAL_LAHIR
'21-Jun-2022 Riyan  : sync sipendar watchlist to AML Watchlist
'====================================================================================

Partial Class SIPENDAR_WatchlistUpload
    Inherits ParentPage

    Const PATH_TEMP_DIR As String = "~\temp\"

    Public Property listSIPENDAR_WATCHLIST() As List(Of SiPendarDAL.SIPENDAR_WATCHLIST)
        Get
            Return Session("SIPENDAR_WatchlistUpload.listSIPENDAR_WATCHLIST")
        End Get
        Set(ByVal value As List(Of SiPendarDAL.SIPENDAR_WATCHLIST))
            Session("SIPENDAR_WatchlistUpload.listSIPENDAR_WATCHLIST") = value
        End Set
    End Property

    'Public Property nullablevalidate As Boolean
    '    Get
    '        Return Session("SIPENDAR_WatchlistUpload.nullablevalidate")
    '    End Get
    '    Set(ByVal value As Boolean)
    '        Session("SIPENDAR_WatchlistUpload.nullablevalidate") = value
    '    End Set
    'End Property

    Public Property ErrorValidation As Boolean
        Get
            Return Session("SIPENDAR_WatchlistUpload.ErrorValidation")
        End Get
        Set(ByVal value As Boolean)
            Session("SIPENDAR_WatchlistUpload.ErrorValidation") = value
        End Set
    End Property

    Public Property PKUploadHeder As Long
        Get
            Return Session("SIPENDAR_WatchlistUpload.ErrorValidation")
        End Get
        Set(ByVal value As Long)
            Session("SIPENDAR_WatchlistUpload.ErrorValidation") = value
        End Set
    End Property

    Public Property DataTabelSIPENDAR_WATCHLIST_Upload_Header As DataTable
        Get
            Return Session("SIPENDAR_WatchlistUpload.DataTabelSIPENDAR_WATCHLIST_Upload_Header")
        End Get
        Set(ByVal value As DataTable)
            Session("SIPENDAR_WatchlistUpload.DataTabelSIPENDAR_WATCHLIST_Upload_Header") = value
        End Set
    End Property

    Public Property DataTabelSIPENDAR_WATCHLIST_Upload_Detail As DataTable
        Get
            Return Session("SIPENDAR_WatchlistUpload.DataTabelSIPENDAR_WATCHLIST_Upload_Detail")
        End Get
        Set(ByVal value As DataTable)
            Session("SIPENDAR_WatchlistUpload.DataTabelSIPENDAR_WATCHLIST_Upload_Detail") = value
        End Set
    End Property

    Private Sub SIPENDAR_WatchlistUpload_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        Me.ActionType = NawaBLL.Common.ModuleActionEnum.Upload
    End Sub

    Private Sub SIPENDAR_WatchlistUpload_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = Me.ObjModule.ModuleLabel & " - Upload"
                'nullablevalidate = True
                ErrorValidation = True

                'Set command column location (Left/Right)
                SetCommandColumnLocation()

                'Clear Session
                ClearSession()

                '14-Oct-2021 Adi : Hapus data di table SIPENDAR_WATCHLIST_Upload karena menggunakan remote store
                DeleteDataFromUploadBySP()

            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ClearSession()
        Try
            'Session("ComboBox" + cmb_SUBMISSION_TYPE_Excel.ID + ".StringValue") = Nothing
            'Session("ComboBox" + cmb_SUBMISSION_TYPE_XML.ID + ".StringValue") = Nothing
            cmb_SUBMISSION_TYPE_Excel.SetTextValue("")
            cmb_SUBMISSION_TYPE_XML.SetTextValue("")
            DataTabelSIPENDAR_WATCHLIST_Upload_Header = New DataTable
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail = New DataTable
            LoadColumnDataTableHeaderAndDetail()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadColumnDataTableHeaderAndDetail()
        Try
            'Header
            DataTabelSIPENDAR_WATCHLIST_Upload_Header.Columns.Add(New DataColumn("PK_SIPENDAR_WATCHLIST_Upload_Header_ID", GetType(Long)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Header.Columns.Add(New DataColumn("Data_Status", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Header.Columns.Add(New DataColumn("Active", GetType(Boolean)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Header.Columns.Add(New DataColumn("CreatedBy", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Header.Columns.Add(New DataColumn("LastUpdateBy", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Header.Columns.Add(New DataColumn("ApprovedBy", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Header.Columns.Add(New DataColumn("CreatedDate", GetType(Date)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Header.Columns.Add(New DataColumn("LastUpdateDate", GetType(Date)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Header.Columns.Add(New DataColumn("ApprovedDate", GetType(Date)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Header.Columns.Add(New DataColumn("Alternateby", GetType(String)))

            'Detail
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("PK_SIPENDAR_WATCHLIST_Upload_Detail_ID", GetType(Long)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("FK_SIPENDAR_WATCHLIST_Upload_Header_ID", GetType(Long)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("Record_Number", GetType(Long)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("PERIODE", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("ID_PPATK", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("KODE_WATCHLIST", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("JENIS_PELAKU", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("NAMA_ASLI", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("PARAMETER_PENCARIAN_NAMA", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("TEMPAT_LAHIR", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("TANGGAL_LAHIR", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("NPWP", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("KTP", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("PAS", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("KITAS", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("SUKET", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("SIM", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("KITAP", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("KIMS", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("Watchlist", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("Submission_Type", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("Active", GetType(Boolean)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("CreatedBy", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("LastUpdateBy", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("ApprovedBy", GetType(String)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("CreatedDate", GetType(Date)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("LastUpdateDate", GetType(Date)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("ApprovedDate", GetType(Date)))
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Columns.Add(New DataColumn("Alternateby", GetType(String)))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


#Region "General"
    Protected Sub btn_Cancel_Click()
        Try
            Dim strModuleID = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            If InStr(ObjModule.UrlUpload, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlUpload & "&ModuleID=" & strModuleID)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlUpload & "?ModuleID=" & strModuleID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Confirmation_Click()
        Try
            'Dim strModuleID = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            'If InStr(ObjModule.UrlView, "?") > 0 Then
            '    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & strModuleID)
            'Else
            '    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & strModuleID)
            'End If

            FormPanelInput.Hidden = False
            Panelconfirmation.Hidden = True
            'listSIPENDAR_WATCHLIST = New List(Of SiPendarDAL.SIPENDAR_WATCHLIST)
            ClearSession()
            BindUploadResult()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SetCommandColumnLocation()
        Try
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If

            If bsettingRight = 1 Then
                'GridPanelReportIndicator.ColumnModel.Columns.Add(CommandColumn87)
            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Save Watchlist"
    Protected Sub Btn_Save_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If Not ErrorValidation Then
                Throw New ApplicationException("There's Invalid Data, Please Update Data First Before Re-Upload")
            End If
            'If listSIPENDAR_WATCHLIST IsNot Nothing AndAlso listSIPENDAR_WATCHLIST.Count > 0 Then
            If DataTabelSIPENDAR_WATCHLIST_Upload_Detail IsNot Nothing AndAlso DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Rows.Count > 0 Then
                'If nullablevalidate = False Then
                '    Throw New Exception("Field Periode, Id, Kode Watchlist, Jenis Pelaku, Nama Asli, dan Parameter Pencarian tidak boleh kosong")
                'End If

                DataTabelSIPENDAR_WATCHLIST_Upload_Header.Rows.Clear()
                Dim objdatarow As DataRow = DataTabelSIPENDAR_WATCHLIST_Upload_Header.NewRow()
                objdatarow("Active") = True
                objdatarow("CreatedBy") = NawaBLL.Common.SessionCurrentUser.UserID
                objdatarow("CreatedDate") = Now
                objdatarow("LastUpdateBy") = NawaBLL.Common.SessionCurrentUser.UserID
                objdatarow("LastUpdateDate") = Now
                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                    objdatarow("Alternateby") = NawaBLL.Common.SessionAlternateUser.UserID
                End If

                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
                    objdatarow("Data_Status") = "Data Saved Without Approval"
                    DataTabelSIPENDAR_WATCHLIST_Upload_Header.Rows.Add(objdatarow)
                    SaveWithoutApproval()
                    LblConfirmation.Text = "Data Saved into Database"
                Else
                    objdatarow("Data_Status") = "Waiting In Approval"
                    DataTabelSIPENDAR_WATCHLIST_Upload_Header.Rows.Add(objdatarow)
                    SaveToModuleApproval()
                    LblConfirmation.Text = "Data Saved into Pending Approval"
                End If

                FormPanelInput.Hidden = True
                Panelconfirmation.Hidden = False
            Else
                Throw New ApplicationException("Belum ada file yang diupload atau isi file kosong.")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub SaveWithoutApproval()
        'Dim listDataOld As List(Of SiPendarDAL.SIPENDAR_WATCHLIST) = Nothing
        'Using objDB_Old As New SiPendarDAL.SiPendarEntities
        '    listDataOld = objDB_Old.SIPENDAR_WATCHLIST.ToList
        'End Using

        'Using objDB As New SiPendarDAL.SiPendarEntities
        '    Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
        '        Try

        '            'Define local variables
        '            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
        '            Dim strAlternateID As String = Nothing
        '            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
        '            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
        '            Dim strModuleName As String = ObjModule.ModuleLabel

        '            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
        '                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
        '            End If

        '            'Save objData to Table SIPENDAR_WATCHLIST
        '            For Each item As SiPendarDAL.SIPENDAR_WATCHLIST In listSIPENDAR_WATCHLIST
        '                'Get data lama
        '                Dim objDataOld As New SiPendarDAL.SIPENDAR_WATCHLIST
        '                objDataOld = listDataOld.Where(Function(x) x.ID_PPATK = item.ID_PPATK).FirstOrDefault

        '                'Cek apakah data dengan ID PPATK yang sama sudah ada
        '                Dim objCek As SiPendarDAL.SIPENDAR_WATCHLIST = Nothing
        '                If listDataOld IsNot Nothing Then
        '                    objCek = listDataOld.Where(Function(x) x.ID_PPATK = item.ID_PPATK).FirstOrDefault
        '                End If

        '                If objCek Is Nothing Then       'Add data baru
        '                    With item
        '                        .Active = True
        '                        .CreatedBy = strUserID
        '                        .CreatedDate = Now
        '                        .Alternateby = strAlternateID
        '                    End With

        '                    'Save objData
        '                    objDB.Entry(item).State = Entity.EntityState.Added
        '                    objDB.SaveChanges()

        '                    'Audit Trail Header
        '                    Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
        '                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
        '                Else
        '                    'Edit existing data
        '                    With objCek
        '                        .PERIODE = item.PERIODE
        '                        .KODE_WATCHLIST = item.KODE_WATCHLIST
        '                        .JENIS_PELAKU = item.JENIS_PELAKU
        '                        .NAMA_ASLI = item.NAMA_ASLI
        '                        .PARAMETER_PENCARIAN_NAMA = item.PARAMETER_PENCARIAN_NAMA
        '                        .TEMPAT_LAHIR = item.TEMPAT_LAHIR
        '                        .TANGGAL_LAHIR = item.TANGGAL_LAHIR
        '                        .NPWP = item.NPWP
        '                        .KTP = item.KTP
        '                        .PAS = item.PAS
        '                        .KITAS = item.KITAS
        '                        .SUKET = item.SUKET
        '                        .SIM = item.SIM
        '                        .KITAP = item.KITAP
        '                        .Active = True
        '                        .LastUpdateBy = strUserID
        '                        .LastUpdateDate = Now
        '                        .Alternateby = strAlternateID
        '                    End With

        '                    'Save objData
        '                    objDB.Entry(objCek).State = Entity.EntityState.Modified
        '                    objDB.SaveChanges()

        '                    'Audit Trail Header
        '                    Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
        '                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, objCek, objDataOld)
        '                End If
        '            Next

        '            objTrans.Commit()
        '        Catch ex As Exception
        '            objTrans.Rollback()
        '            Throw ex
        '        End Try
        '    End Using
        'End Using

        Try
            SaveDataHeaderAndDetail()
            'Save objData to Table SIPENDAR_WATCHLIST_Upload
            Dim strQuery As String = ""

            strQuery = "INSERT INTO SIPENDAR_WATCHLIST "
            strQuery += "("
            strQuery += "      PERIODE,"
            strQuery += "      ID_PPATK,"
            strQuery += "      KODE_WATCHLIST,"
            strQuery += "      JENIS_PELAKU,"
            strQuery += "      NAMA_ASLI,"
            strQuery += "      PARAMETER_PENCARIAN_NAMA,"
            strQuery += "      TEMPAT_LAHIR,"
            strQuery += "      TANGGAL_LAHIR,"
            strQuery += "      NPWP,"
            strQuery += "      KTP,"
            strQuery += "      PAS,"
            strQuery += "      KITAS,"
            strQuery += "      SUKET,"
            strQuery += "      SIM,"
            strQuery += "      KITAP,"
            strQuery += "      KIMS,"           '26-Oct-2021 Adi
            strQuery += "      Watchlist,"      '26-Oct-2021 Adi
            strQuery += "      Submission_Type,"
            strQuery += "      [Active],"
            strQuery += "      CreatedBy,"
            strQuery += "      CreatedDate"
            strQuery += ") "
            strQuery += "SELECT "
            strQuery += "      PERIODE,"
            strQuery += "      ID_PPATK,"
            strQuery += "      KODE_WATCHLIST,"
            strQuery += "      JENIS_PELAKU,"
            strQuery += "      NAMA_ASLI,"
            strQuery += "      PARAMETER_PENCARIAN_NAMA,"
            strQuery += "      TEMPAT_LAHIR,"
            strQuery += "      TANGGAL_LAHIR,"
            strQuery += "      NPWP,"
            strQuery += "      KTP,"
            strQuery += "      PAS,"
            strQuery += "      KITAS,"
            strQuery += "      SUKET,"
            strQuery += "      SIM,"
            strQuery += "      KITAP,"
            strQuery += "      KIMS,"           '26-Oct-2021 Adi
            strQuery += "      Watchlist,"      '26-Oct-2021 Adi
            strQuery += "      Submission_Type,"
            strQuery += "      [Active],"
            strQuery += "      '" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
            strQuery += "      GETDATE()"
            strQuery += " FROM SIPENDAR_WATCHLIST_Upload WHERE nawa_userid='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery += " AND nawa_Action='Insert'"

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            strQuery = "UPDATE SIPENDAR_WATCHLIST SET"
            strQuery += "      PERIODE = b.PERIODE,"
            strQuery += "      ID_PPATK = b.ID_PPATK,"
            strQuery += "      KODE_WATCHLIST = b.KODE_WATCHLIST,"
            strQuery += "      JENIS_PELAKU = b.JENIS_PELAKU,"
            strQuery += "      NAMA_ASLI = b.NAMA_ASLI,"
            strQuery += "      PARAMETER_PENCARIAN_NAMA = b.PARAMETER_PENCARIAN_NAMA,"
            strQuery += "      TEMPAT_LAHIR = b.TEMPAT_LAHIR,"
            strQuery += "      TANGGAL_LAHIR = b.TANGGAL_LAHIR,"
            strQuery += "      NPWP = b.NPWP,"
            strQuery += "      KTP = b.KTP,"
            strQuery += "      PAS = b.PAS,"
            strQuery += "      KITAS = b.KITAS,"
            strQuery += "      SUKET = b.SUKET,"
            strQuery += "      SIM = b.SIM,"
            strQuery += "      KITAP = b.KITAP,"
            strQuery += "      KIMS = b.KIMS,"              '26-Oct-2021 Adi
            strQuery += "      Watchlist = b.Watchlist,"    '26-Oct-2021 Adi
            strQuery += "      Submission_Type = b.Submission_Type,"
            strQuery += "      LastUpdateBy = '" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
            strQuery += "      LastUpdateDate = GETDATE()"
            strQuery += " FROM SIPENDAR_WATCHLIST a"
            strQuery += " JOIN SIPENDAR_WATCHLIST_Upload b"
            strQuery += " ON a.ID_PPATK = b.ID_PPATK"
            strQuery += " WHERE b.nawa_userid='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery += " AND b.nawa_Action='Update'"

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        Catch ex As Exception
            Throw ex
        End Try

        '21-June-2022 Riyan sync sipendar watchlist to AML Watchlist
        Dim objParamAPI(0) As SqlParameter
        objParamAPI(0) = New SqlParameter
        objParamAPI(0).ParameterName = "@UserID"
        objParamAPI(0).Value = NawaBLL.Common.SessionCurrentUser.UserID

        NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_WATCHLIST_AfterSave", objParamAPI)
        'End sync sipendar watchlist to AML Watchlist

    End Sub

    Protected Sub SaveWithApproval()
        Try
            'Save objData to Table SIPENDAR_WATCHLIST_Upload
            'Dim strQuery As String = ""
            'Dim strQuery1 As String = ""
            'Dim strQuery2 As String = ""
            'Dim lngRecordNo As Long = 0

            'strQuery1 += "INSERT INTO SIPENDAR_WATCHLIST_Upload "
            'strQuery1 += "("
            'strQuery1 += "      nawa_userid,"
            'strQuery1 += "      nawa_recordnumber,"
            'strQuery1 += "      KeteranganError,"
            'strQuery1 += "      nawa_Action,"
            'strQuery1 += "      PK_SIPENDAR_WATCHLIST_ID,"
            'strQuery1 += "      PERIODE,"
            'strQuery1 += "      ID_PPATK,"
            'strQuery1 += "      KODE_WATCHLIST,"
            'strQuery1 += "      JENIS_PELAKU,"
            'strQuery1 += "      NAMA_ASLI,"
            'strQuery1 += "      PARAMETER_PENCARIAN_NAMA,"
            'strQuery1 += "      TEMPAT_LAHIR,"
            'strQuery1 += "      TANGGAL_LAHIR,"
            'strQuery1 += "      NPWP,"
            'strQuery1 += "      KTP,"
            'strQuery1 += "      PAS,"
            'strQuery1 += "      KITAS,"
            'strQuery1 += "      SUKET,"
            'strQuery1 += "      SIM,"
            'strQuery1 += "      KITAP,"
            'strQuery1 += "      [Active]"
            'strQuery1 += ") "

            'Using objDb As New SiPendarDAL.SiPendarEntities
            '    For Each item As SiPendarDAL.SIPENDAR_WATCHLIST In listSIPENDAR_WATCHLIST
            '        'Cek apakah data dengan ID PPATK yang sama sudah ada
            '        Dim strAction As String = "Insert"
            '        Dim lngPK As Long = 0
            '        Dim objCek As SiPendarDAL.SIPENDAR_WATCHLIST = Nothing
            '        objCek = objDb.SIPENDAR_WATCHLIST.Where(Function(x) x.ID_PPATK = item.ID_PPATK).FirstOrDefault
            '        If objCek IsNot Nothing Then
            '            strAction = "update"
            '            lngPK = objCek.PK_SIPENDAR_WATCHLIST_ID
            '        End If

            '        lngRecordNo += 1

            '        strQuery2 = "VALUES ("
            '        strQuery2 += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
            '        strQuery2 += lngRecordNo.ToString & ","
            '        strQuery2 += "'',"
            '        strQuery2 += "'" & strAction & "',"
            '        strQuery2 += lngPK & ","
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.PERIODE), "NULL,", "'" & item.PERIODE & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.ID_PPATK), "NULL,", "'" & item.ID_PPATK & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.KODE_WATCHLIST), "NULL,", "'" & item.KODE_WATCHLIST & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.JENIS_PELAKU), "NULL,", "'" & item.JENIS_PELAKU & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.NAMA_ASLI), "NULL,", "'" & item.NAMA_ASLI & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.PARAMETER_PENCARIAN_NAMA), "NULL,", "'" & item.PARAMETER_PENCARIAN_NAMA & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.TEMPAT_LAHIR), "NULL,", "'" & item.TEMPAT_LAHIR & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.TANGGAL_LAHIR), "NULL,", "'" & item.TANGGAL_LAHIR & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.NPWP), "NULL,", "'" & item.NPWP & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.KTP), "NULL,", "'" & item.KTP & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.PAS), "NULL,", "'" & item.PAS & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.KITAS), "NULL,", "'" & item.KITAS & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.SUKET), "NULL,", "'" & item.SUKET & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.SIM), "NULL,", "'" & item.SIM & "',")
            '        strQuery2 += IIf(String.IsNullOrEmpty(item.KITAP), "NULL,", "'" & item.KITAP & "',")
            '        strQuery2 += "1"
            '        strQuery2 += ")"

            '        strQuery = strQuery1 & strQuery2
            '        'Throw New Exception(strQuery)
            '        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            '    Next
            'End Using

            'Using objDb As New SiPendarDAL.SiPendarEntities
            '    'Save objData to Module Approval
            '    Dim oparam(2) As SqlClient.SqlParameter
            '    oparam(0) = New SqlParameter
            '    oparam(0).ParameterName = "@Modeid"     'ModuleKey kalau di SP
            '    oparam(0).DbType = DbType.Int32
            '    oparam(0).Value = 0     'intmode

            '    oparam(1) = New SqlParameter
            '    oparam(1).ParameterName = "@userid"
            '    oparam(1).DbType = DbType.String
            '    oparam(1).Value = NawaBLL.Common.SessionCurrentUser.UserID

            '    oparam(2) = New SqlParameter
            '    oparam(2).ParameterName = "@ModuleID"
            '    oparam(2).DbType = DbType.Int32
            '    oparam(2).Value = ObjModule.PK_Module_ID

            '    'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SaveUploadApproval", oparam)
            '    Dim objresult As Decimal = objDb.Database.SqlQuery(Of Decimal)("exec usp_SaveUploadApproval @Modeid,@userid,@ModuleID", oparam).First

            '    ' objaudittrailheader.PK_ModuleApproval_ID = objresult
            '    Dim strunik As String = objresult & "import"

            '    'objDb.SaveChanges()
            '    'objtrans.Commit()
            '    'Nawa.BLL.NawaFramework.SendEmailModuleApproval(objresult)
            '    SaveWorkflowReviewData(ObjModule.PK_Module_ID, NawaBLL.Common.SessionCurrentUser.UserID, strunik, objresult)
            'End Using

            '30-Sep-2021 Adi : ganti jadi konsep masuk table SIPENDAR_WATCHLIST_Upload_approval
            SaveToModuleApproval()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Upload Watchlist from Excel"
    Protected Sub btn_UploadExcel_Click()
        Try
            Window_UploadExcel.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_UploadExcel_Cancel_Click()
        Try
            Window_UploadExcel.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_UploadExcel_Save_Click()
        Try
            'Your code to upload data
            'Validasi jika belum ada file yang diupload
            If Not FileUploadExcel.HasFile Then
                Throw New ApplicationException("Belum ada file yang diupload.")
            End If

            'Validasi hanya terima xlsx
            If Not FileUploadExcel.FileName.EndsWith(".xlsx") Then
                Throw New ApplicationException("Format yang valid hanya .xlsx.")
            End If

            '14-Des-2021 Penambahan Field Submission_Type
            If String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_Excel.SelectedItemValue) Then
                Throw New ApplicationException("Submission Type Wajib Dipilih")
            End If
            'End of 14-Des-2021 Penambahan Field Submission_Type

            'Import data from excel template
            Using objdb As New NawaDAL.NawaDataEntities
                If FileUploadExcel.HasFile Then
                    'Directory file fisik yang di-write di server
                    Dim strFilePath As String = Server.MapPath(PATH_TEMP_DIR)
                    Dim strFileName As String = Guid.NewGuid.ToString & ".xlsx"

                    'Write file uploaded to server
                    System.IO.File.WriteAllBytes(strFilePath & strFileName, FileUploadExcel.FileBytes)

                    Dim br As BinaryReader
                    Dim bData As Byte()
                    br = New BinaryReader(System.IO.File.OpenRead(strFilePath & strFileName))
                    bData = br.ReadBytes(br.BaseStream.Length)
                    Dim ms As MemoryStream = New MemoryStream(bData, 0, bData.Length)
                    ms.Write(bData, 0, bData.Length)

                    Using excelFile As New ExcelPackage(ms)
                        If excelFile.Workbook.Worksheets(1) IsNot Nothing Then
                            ReadExcelUpload(excelFile.Workbook.Worksheets(1))
                        End If
                    End Using

                    ms.Flush()
                    ms.Close()
                End If

                'Bind to Grid before Screening
                BindUploadResult()

            End Using

            Window_UploadExcel.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ReadExcelUpload(ByVal sheet As ExcelWorksheet)
        Try

            '3-Aug-2021 : Validasi jika file excel kosong
            Dim strCheck As String = sheet.Cells(1, 1).Text.Trim
            Dim strCheck_Baris2 As String = sheet.Cells(2, 1).Text.Trim     'Case ada field namenya tapi gak ada data.
            If String.IsNullOrEmpty(strCheck) OrElse String.IsNullOrEmpty(strCheck_Baris2) Then
                Throw New ApplicationException("Excel file is empty or has no data row or has invalid template.")
            End If

            '14-Des-2021 Penambahan Field Submission_Type
            Dim submissionType As String = cmb_SUBMISSION_TYPE_Excel.SelectedItemValue
            'End of 14-Des-2021 Penambahan Field Submission_Type

            Dim PK_ID As Long = -1
            Dim RecordNo As Long = 1
            'listSIPENDAR_WATCHLIST = New List(Of SiPendarDAL.SIPENDAR_WATCHLIST)
            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Rows.Clear()

            'If listSIPENDAR_SCREENING_REQUEST_DETAIL.Count > 0 Then
            '    PK_ID = listSIPENDAR_SCREENING_REQUEST_DETAIL.Min(Function(x) x.PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID) - 1
            'End If
            'If PK_ID >= 0 Then
            '    PK_ID = -1
            'End If

            Dim IRow As Integer = 2

            While IRow <> 0
                Dim strName As String = ""

                'Gunakan ID Sebagai Key... 
                'Cari ID, jika kosong maka looping diakhiri
                strName = sheet.Cells(IRow, 1).Text.Trim
                If Not String.IsNullOrEmpty(strName) Then

                    Dim objdatarow As DataRow = DataTabelSIPENDAR_WATCHLIST_Upload_Detail.NewRow()

                    objdatarow("Record_Number") = RecordNo
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 1).Text.Trim) Then
                        objdatarow("PERIODE") = sheet.Cells(IRow, 1).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 2).Text.Trim) Then
                        objdatarow("ID_PPATK") = sheet.Cells(IRow, 2).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 3).Text.Trim) Then
                        objdatarow("KODE_WATCHLIST") = sheet.Cells(IRow, 3).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 4).Text.Trim) Then
                        objdatarow("JENIS_PELAKU") = sheet.Cells(IRow, 4).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 5).Text.Trim) Then
                        objdatarow("NAMA_ASLI") = sheet.Cells(IRow, 5).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 6).Text.Trim) Then
                        objdatarow("PARAMETER_PENCARIAN_NAMA") = sheet.Cells(IRow, 6).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 7).Text.Trim) Then
                        objdatarow("TEMPAT_LAHIR") = sheet.Cells(IRow, 7).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 8).Text.Trim) Then
                        objdatarow("TANGGAL_LAHIR") = sheet.Cells(IRow, 8).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 9).Text.Trim) Then
                        objdatarow("NPWP") = sheet.Cells(IRow, 9).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 10).Text.Trim) Then
                        objdatarow("KTP") = sheet.Cells(IRow, 10).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 11).Text.Trim) Then
                        objdatarow("PAS") = sheet.Cells(IRow, 11).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 12).Text.Trim) Then
                        objdatarow("KITAS") = sheet.Cells(IRow, 12).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 13).Text.Trim) Then
                        objdatarow("SUKET") = sheet.Cells(IRow, 13).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 14).Text.Trim) Then
                        objdatarow("SIM") = sheet.Cells(IRow, 14).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 15).Text.Trim) Then
                        objdatarow("KITAP") = sheet.Cells(IRow, 15).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 16).Text.Trim) Then
                        objdatarow("KIMS") = sheet.Cells(IRow, 16).Text.Trim
                    End If
                    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 17).Text.Trim) Then
                        objdatarow("Watchlist") = sheet.Cells(IRow, 17).Text.Trim
                    End If
                    '14-Des-2021 Penambahan Field Submission_Type
                    objdatarow("Submission_Type") = submissionType
                    'End of 14-Des-2021 Penambahan Field Submission_Type

                    DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Rows.Add(objdatarow)

                    'Dim ObjData As New SiPendarDAL.SIPENDAR_WATCHLIST
                    'With ObjData
                    '    .PK_SIPENDAR_WATCHLIST_ID = PK_ID

                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 1).Text.Trim) Then
                    '        .PERIODE = sheet.Cells(IRow, 1).Text.Trim
                    '    Else
                    '        nullablevalidate = False
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 2).Text.Trim) Then
                    '        .ID_PPATK = sheet.Cells(IRow, 2).Text.Trim
                    '    Else
                    '        nullablevalidate = False
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 3).Text.Trim) Then
                    '        .KODE_WATCHLIST = sheet.Cells(IRow, 3).Text.Trim
                    '    Else
                    '        nullablevalidate = False
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 4).Text.Trim) Then
                    '        .JENIS_PELAKU = sheet.Cells(IRow, 4).Text.Trim
                    '    Else
                    '        nullablevalidate = False
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 5).Text.Trim) Then
                    '        .NAMA_ASLI = sheet.Cells(IRow, 5).Text.Trim
                    '    Else
                    '        nullablevalidate = False
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 6).Text.Trim) Then
                    '        .PARAMETER_PENCARIAN_NAMA = sheet.Cells(IRow, 6).Text.Trim
                    '    Else
                    '        nullablevalidate = False
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 7).Text.Trim) Then
                    '        .TEMPAT_LAHIR = sheet.Cells(IRow, 7).Text.Trim
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 8).Text.Trim) Then
                    '        .TANGGAL_LAHIR = sheet.Cells(IRow, 8).Text.Trim
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 9).Text.Trim) Then
                    '        .NPWP = sheet.Cells(IRow, 9).Text.Trim
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 10).Text.Trim) Then
                    '        .KTP = sheet.Cells(IRow, 10).Text.Trim
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 11).Text.Trim) Then
                    '        .PAS = sheet.Cells(IRow, 11).Text.Trim
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 12).Text.Trim) Then
                    '        .KITAS = sheet.Cells(IRow, 12).Text.Trim
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 13).Text.Trim) Then
                    '        .SUKET = sheet.Cells(IRow, 13).Text.Trim
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 14).Text.Trim) Then
                    '        .SIM = sheet.Cells(IRow, 14).Text.Trim
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 15).Text.Trim) Then
                    '        .KITAP = sheet.Cells(IRow, 15).Text.Trim
                    '    End If

                    '    '26-Oct-2021 Adi
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 16).Text.Trim) Then
                    '        .KIMS = sheet.Cells(IRow, 16).Text.Trim
                    '    End If
                    '    If Not String.IsNullOrEmpty(sheet.Cells(IRow, 17).Text.Trim) Then
                    '        .Watchlist = sheet.Cells(IRow, 17).Text.Trim
                    '    End If
                    '    'End 26-Oct-2021 Adi


                    'End With

                    'listSIPENDAR_WATCHLIST.Add(ObjData)

                    'PK_ID = PK_ID - 1

                    RecordNo = RecordNo + 1
                    IRow = IRow + 1
                Else
                    IRow = 0
                End If
            End While

            Dim str As String = ""

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindUploadResult()
        Try
            If DataTabelSIPENDAR_WATCHLIST_Upload_Detail IsNot Nothing AndAlso DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Rows.Count > 0 Then
                DeleteDataFromUploadBySP()

                '30-Sep-2021 Adi : digabung dengan SP Validasi saat insert ke table _Upload karena lambat
                'SaveDataIntoWatchlistUpload()
                'End of 30-Sep-2021 Adi : digabung dengan SP Validasi saat insert ke table _Upload karena lambat

                ValidateDataUploadBySP()

                'Dim objtblvalid As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select a.nawa_Action, format(try_convert(datetime, a.PERIODE),'dd-MMM-yyyy') as PERIODE, a.ID_PPATK, a.KODE_WATCHLIST, a.JENIS_PELAKU," &
                '                                                                   " a.NAMA_ASLI, a.PARAMETER_PENCARIAN_NAMA, a.TEMPAT_LAHIR, a.TANGGAL_LAHIR, a.NPWP, a.KTP, a.PAS, a.KITAS, a.SUKET, a.SIM," &
                '                                                                   " a.KITAP from SIPENDAR_WATCHLIST_Upload a where a.KeteranganError = '' and a.nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'", Nothing)
                ''Dim objtblvalid As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("SIPENDAR_WATCHLIST_Upload a",
                ''                                                                         "a.PERIODE, a.ID_PPATK, a.KODE_WATCHLIST, a.JENIS_PELAKU, a.NAMA_ASLI, a.PARAMETER_PENCARIAN_NAMA, a.TEMPAT_LAHIR, a.TANGGAL_LAHIR, a.NPWP, a.KTP, a.PAS, a.KITAS, a.SUKET, a.SIM, a.KITAP",
                ''                                                                         "a.KeteranganError = '' and a.nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'",
                ''                                                                         "",
                ''                                                                         0,
                ''                                                                         NawaBLL.SystemParameterBLL.GetPageSize,
                ''                                                                         0)
                ''Bind to Grid Valid
                'objtblvalid.DefaultView.Sort = "ID_ppatk ASC"
                'gp_UploadResult.GetStore.DataSource = objtblvalid
                'gp_UploadResult.GetStore.DataBind()

                '14-Oct-2021 Adi : change to remote store
                FilterHeader_Valid.ClearFilter()
                store_Watchlist_Valid.Reload()

                Dim objtblinvalid As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select a.PERIODE, a.ID_PPATK, a.KODE_WATCHLIST, a.JENIS_PELAKU," &
                                                                                   " a.NAMA_ASLI, a.PARAMETER_PENCARIAN_NAMA, a.TEMPAT_LAHIR, a.TANGGAL_LAHIR, a.NPWP, a.KTP, a.PAS, a.KITAS, a.SUKET, a.SIM," &
                                                                                   " a.KITAP, a.KIMS, a.Watchlist, a.KeteranganError from SIPENDAR_WATCHLIST_Upload a where a.KeteranganError <> '' and a.nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'", Nothing)
                'Dim objtblinvalid As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("SIPENDAR_WATCHLIST_Upload a",
                '                                                                         "a.PERIODE, a.ID_PPATK, a.KODE_WATCHLIST, a.JENIS_PELAKU, a.NAMA_ASLI, a.PARAMETER_PENCARIAN_NAMA, a.TEMPAT_LAHIR, a.TANGGAL_LAHIR, a.NPWP, a.KTP, a.PAS, a.KITAS, a.SUKET, a.SIM, a.KITAP, a.KeteranganError ",
                '                                                                         "a.KeteranganError <>'' and a.nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'",
                '                                                                         "",
                '                                                                         0,
                '                                                                         NawaBLL.SystemParameterBLL.GetPageSize,
                '                                                                         0)
                'Bind to Grid Invalid
                If objtblinvalid.Rows.Count > 0 Then
                    ErrorValidation = False
                    'PanelInValid.Hidden = False
                Else
                    ErrorValidation = True
                End If
                'objtblinvalid.DefaultView.Sort = "ID_PPATK ASC"
                'GridPanelInvalid.GetStore.DataSource = objtblinvalid
                'GridPanelInvalid.GetStore.DataBind()

                '14-Oct-2021 Adi : change to remote store
                FilterHeader_Invalid.ClearFilter()
                store_Watchlist_Invalid.Reload()

                FilterHeader_RawData.ClearFilter()
                'store_Watchlist_RawData.Reload()
                store_Watchlist_RawData.DataSource = DataTabelSIPENDAR_WATCHLIST_Upload_Detail
                store_Watchlist_RawData.DataBind()
                'store_Watchlist_RawData.DataSource = DataTabelSIPENDAR_WATCHLIST_Upload_Detail
                'store_Watchlist_RawData.DataBind()
            Else
                'Convert object to Datatable
                'listSIPENDAR_WATCHLIST = New List(Of SiPendarDAL.SIPENDAR_WATCHLIST)
                'Dim dt As DataTable = NawaBLL.Common.CopyGenericToDataTable(listSIPENDAR_WATCHLIST.ToList)

                ''Bind to Grid
                'dt.DefaultView.Sort = "ID_PPATK ASC"
                'gp_UploadResult.GetStore.DataSource = dt
                'gp_UploadResult.GetStore.DataBind()
                'DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Rows.Clear()
                'gp_UploadResult.GetStore.DataSource = DataTabelSIPENDAR_WATCHLIST_Upload_Detail
                'gp_UploadResult.GetStore.DataBind()
                DeleteDataFromUploadBySP()

                FilterHeader_Valid.ClearFilter()
                store_Watchlist_Valid.Reload()

                '14-Oct-2021 Adi : change to remote store
                FilterHeader_Invalid.ClearFilter()
                store_Watchlist_Invalid.Reload()

                FilterHeader_RawData.ClearFilter()
                'store_Watchlist_RawData.Reload()
                store_Watchlist_RawData.DataSource = DataTabelSIPENDAR_WATCHLIST_Upload_Detail
                store_Watchlist_RawData.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Upload Watchlist from XML"
    Protected Sub btn_UploadXML_Click()
        Try
            Window_UploadXML.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_UploadXML_Cancel_Click()
        Try
            Window_UploadXML.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_UploadXML_Save_Click()
        Try
            'Your code to upload data
            'Validasi jika belum ada file yang diupload
            If Not FileUploadXML.HasFile Then
                Throw New ApplicationException("Belum ada file yang diupload.")
            End If

            'Validasi hanya terima xlsx
            If Not FileUploadXML.FileName.EndsWith(".xml") Then
                Throw New ApplicationException("Format yang valid hanya .xml")
            End If

            '14-Des-2021 Penambahan Field Submission_Type
            If String.IsNullOrEmpty(cmb_SUBMISSION_TYPE_XML.SelectedItemValue) Then
                Throw New ApplicationException("Submission Type Wajib Dipilih")
            End If
            'End of 14-Des-2021 Penambahan Field Submission_Type

            DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Rows.Clear()
            'Import data from XML template
            Using objdb As New NawaDAL.NawaDataEntities
                If FileUploadXML.HasFile Then
                    'Directory file fisik yang di-write di server
                    Dim strFilePath As String = Server.MapPath(PATH_TEMP_DIR)
                    Dim strFileName As String = Guid.NewGuid.ToString & ".xml"
                    '14-Des-2021 Penambahan Field Submission_Type
                    Dim submissionType As String = cmb_SUBMISSION_TYPE_XML.SelectedItemValue
                    'End of 14-Des-2021 Penambahan Field Submission_Type

                    'Write file uploaded to server
                    System.IO.File.WriteAllBytes(strFilePath & strFileName, FileUploadXML.FileBytes)

                    'Read file uploaded to XML string
                    Dim xml As String = System.IO.File.ReadAllText(strFilePath & strFileName)

                    'Jalankan SP untuk memperoleh datatable from XML
                    Dim oparam(0) As SqlClient.SqlParameter
                    oparam(0) = New SqlParameter
                    oparam(0).ParameterName = "@xml"
                    oparam(0).Value = xml
                    Dim dtUploadXML As New DataTable
                    Try
                        dtUploadXML = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_WATCHLIST_ReadXML", oparam)
                    Catch ex As Exception
                        Throw New Exception("Data XML kosong atau tidak sesuai format PPATK.")
                    End Try

                    If dtUploadXML Is Nothing Or dtUploadXML.Rows.Count = 0 Then
                        Throw New Exception("Data XML kosong atau tidak sesuai format PPATK.")
                    End If

                    'From DataTable convert to object
                    listSIPENDAR_WATCHLIST = New List(Of SiPendarDAL.SIPENDAR_WATCHLIST)
                    Dim PK_ID As Long = 0
                    Dim RecordNo As Long = 1
                    For Each row As DataRow In dtUploadXML.Rows
                        PK_ID = PK_ID - 1
                        Dim objdatarow As DataRow = DataTabelSIPENDAR_WATCHLIST_Upload_Detail.NewRow()

                        objdatarow("Record_Number") = RecordNo
                        If Not IsDBNull(row.Item("PERIODE")) Then
                            objdatarow("PERIODE") = row.Item("PERIODE")
                        End If
                        If Not IsDBNull(row.Item("ID_PPATK")) Then
                            objdatarow("ID_PPATK") = row.Item("ID_PPATK")
                        End If
                        If Not IsDBNull(row.Item("KODE_WATCHLIST")) Then
                            objdatarow("KODE_WATCHLIST") = row.Item("KODE_WATCHLIST")
                        End If
                        If Not IsDBNull(row.Item("JENIS_PELAKU")) Then
                            objdatarow("JENIS_PELAKU") = row.Item("JENIS_PELAKU")
                        End If
                        If Not IsDBNull(row.Item("NAMA_ASLI")) Then
                            objdatarow("NAMA_ASLI") = row.Item("NAMA_ASLI")
                        End If
                        If Not IsDBNull(row.Item("PARAMETER_PENCARIAN_NAMA")) Then
                            objdatarow("PARAMETER_PENCARIAN_NAMA") = row.Item("PARAMETER_PENCARIAN_NAMA")
                        End If
                        If Not IsDBNull(row.Item("TEMPAT_LAHIR")) Then
                            objdatarow("TEMPAT_LAHIR") = row.Item("TEMPAT_LAHIR")
                        End If
                        If Not IsDBNull(row.Item("TANGGAL_LAHIR")) Then
                            objdatarow("TANGGAL_LAHIR") = row.Item("TANGGAL_LAHIR")
                        End If
                        If Not IsDBNull(row.Item("NPWP")) Then
                            objdatarow("NPWP") = row.Item("NPWP")
                        End If
                        If Not IsDBNull(row.Item("KTP")) Then
                            objdatarow("KTP") = row.Item("KTP")
                        End If
                        If Not IsDBNull(row.Item("PAS")) Then
                            objdatarow("PAS") = row.Item("PAS")
                        End If
                        If Not IsDBNull(row.Item("KITAS")) Then
                            objdatarow("KITAS") = row.Item("KITAS")
                        End If
                        If Not IsDBNull(row.Item("SUKET")) Then
                            objdatarow("SUKET") = row.Item("SUKET")
                        End If
                        If Not IsDBNull(row.Item("SIM")) Then
                            objdatarow("SIM") = row.Item("SIM")
                        End If
                        If Not IsDBNull(row.Item("KITAP")) Then
                            objdatarow("KITAP") = row.Item("KITAP")
                        End If
                        If Not IsDBNull(row.Item("KIMS")) Then
                            objdatarow("KIMS") = row.Item("KIMS")
                        End If
                        If Not IsDBNull(row.Item("Watchlist")) Then
                            objdatarow("Watchlist") = row.Item("Watchlist")
                        End If
                        '14-Des-2021 Penambahan Field Submission_Type
                        objdatarow("Submission_Type") = submissionType
                        'End of 14-Des-2021 Penambahan Field Submission_Type

                        DataTabelSIPENDAR_WATCHLIST_Upload_Detail.Rows.Add(objdatarow)

                        'Dim objData As New SiPendarDAL.SIPENDAR_WATCHLIST
                        'With objData
                        '    .PK_SIPENDAR_WATCHLIST_ID = PK_ID
                        '    'perlu validasi data kosong
                        '    .PERIODE = IIf(IsDBNull(row.Item("PERIODE")), Nothing, row.Item("PERIODE"))
                        '    .ID_PPATK = IIf(IsDBNull(row.Item("ID_PPATK")), Nothing, row.Item("ID_PPATK"))
                        '    .KODE_WATCHLIST = IIf(IsDBNull(row.Item("KODE_WATCHLIST")), Nothing, row.Item("KODE_WATCHLIST"))
                        '    .JENIS_PELAKU = IIf(IsDBNull(row.Item("JENIS_PELAKU")), Nothing, row.Item("JENIS_PELAKU"))
                        '    .NAMA_ASLI = IIf(IsDBNull(row.Item("NAMA_ASLI")), Nothing, row.Item("NAMA_ASLI"))
                        '    .PARAMETER_PENCARIAN_NAMA = IIf(IsDBNull(row.Item("PARAMETER_PENCARIAN_NAMA")), Nothing, row.Item("PARAMETER_PENCARIAN_NAMA"))

                        '    If String.IsNullOrWhiteSpace(.PERIODE) OrElse String.IsNullOrWhiteSpace(.ID_PPATK) OrElse String.IsNullOrWhiteSpace(.KODE_WATCHLIST) OrElse
                        '        String.IsNullOrWhiteSpace(.JENIS_PELAKU) OrElse String.IsNullOrWhiteSpace(.NAMA_ASLI) OrElse String.IsNullOrWhiteSpace(.PARAMETER_PENCARIAN_NAMA) Then
                        '        nullablevalidate = False
                        '    End If

                        '    .TEMPAT_LAHIR = IIf(IsDBNull(row.Item("TEMPAT_LAHIR")), Nothing, row.Item("TEMPAT_LAHIR"))
                        '    .TANGGAL_LAHIR = IIf(IsDBNull(row.Item("TANGGAL_LAHIR")), Nothing, row.Item("TANGGAL_LAHIR"))
                        '    .NPWP = IIf(IsDBNull(row.Item("NPWP")), Nothing, row.Item("NPWP"))
                        '    .KTP = IIf(IsDBNull(row.Item("KTP")), Nothing, row.Item("KTP"))
                        '    .PAS = IIf(IsDBNull(row.Item("PAS")), Nothing, row.Item("PAS"))
                        '    .KITAS = IIf(IsDBNull(row.Item("KITAS")), Nothing, row.Item("KITAS"))
                        '    .SUKET = IIf(IsDBNull(row.Item("SUKET")), Nothing, row.Item("SUKET"))
                        '    .SIM = IIf(IsDBNull(row.Item("SIM")), Nothing, row.Item("SIM"))
                        '    .KITAP = IIf(IsDBNull(row.Item("KITAP")), Nothing, row.Item("KITAP"))
                        '    .KIMS = IIf(IsDBNull(row.Item("KIMS")), Nothing, row.Item("KIMS"))
                        '    .Watchlist = IIf(IsDBNull(row.Item("Watchlist")), Nothing, row.Item("Watchlist"))
                        'End With

                        'listSIPENDAR_WATCHLIST.Add(objData)
                        RecordNo = RecordNo + 1
                    Next

                    'Bind to Grid before Screening
                    BindUploadResult()
                End If
            End Using

            Window_UploadXML.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ReadXMLUpload()
        Try


        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Dtype New Reformation for Upload 2021/08/20"
    Protected Sub SaveDataIntoWatchlistUpload()
        Try
            'Save objData to Table SIPENDAR_WATCHLIST_Upload
            Dim strQuery As String = ""
            Dim strQuery1 As String = ""
            Dim strQuery2 As String = ""
            Dim lngRecordNo As Long = 0

            strQuery1 += "INSERT INTO SIPENDAR_WATCHLIST_Upload "
            strQuery1 += "("
            strQuery1 += "      nawa_userid,"
            strQuery1 += "      nawa_recordnumber,"
            strQuery1 += "      KeteranganError,"
            strQuery1 += "      nawa_Action,"
            strQuery1 += "      PK_SIPENDAR_WATCHLIST_ID,"
            strQuery1 += "      PERIODE,"
            strQuery1 += "      ID_PPATK,"
            strQuery1 += "      KODE_WATCHLIST,"
            strQuery1 += "      JENIS_PELAKU,"
            strQuery1 += "      NAMA_ASLI,"
            strQuery1 += "      PARAMETER_PENCARIAN_NAMA,"
            strQuery1 += "      TEMPAT_LAHIR,"
            strQuery1 += "      TANGGAL_LAHIR,"
            strQuery1 += "      NPWP,"
            strQuery1 += "      KTP,"
            strQuery1 += "      PAS,"
            strQuery1 += "      KITAS,"
            strQuery1 += "      SUKET,"
            strQuery1 += "      SIM,"
            strQuery1 += "      KITAP,"
            strQuery1 += "      KIMS,"          '26-Oct-2021
            strQuery1 += "      Watchlist,"     '26-Oct-2021
            strQuery1 += "      [Active]"
            strQuery1 += ") "

            Using objDb As New SiPendarDAL.SiPendarEntities
                For Each item As SiPendarDAL.SIPENDAR_WATCHLIST In listSIPENDAR_WATCHLIST
                    'Cek apakah data dengan ID PPATK yang sama sudah ada
                    Dim strAction As String = "Insert"
                    Dim lngPK As Long = 0
                    Dim objCek As SiPendarDAL.SIPENDAR_WATCHLIST = Nothing
                    objCek = objDb.SIPENDAR_WATCHLIST.Where(Function(x) x.ID_PPATK = item.ID_PPATK).FirstOrDefault
                    If objCek IsNot Nothing Then
                        strAction = "update"
                        lngPK = objCek.PK_SIPENDAR_WATCHLIST_ID
                    End If

                    lngRecordNo += 1

                    strQuery2 = "VALUES ("
                    strQuery2 += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
                    strQuery2 += lngRecordNo.ToString & ","
                    strQuery2 += "'',"
                    strQuery2 += "'" & strAction & "',"
                    strQuery2 += lngPK & ","
                    strQuery2 += IIf(String.IsNullOrEmpty(item.PERIODE), "NULL,", "'" & item.PERIODE & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.ID_PPATK), "NULL,", "'" & item.ID_PPATK & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.KODE_WATCHLIST), "NULL,", "'" & item.KODE_WATCHLIST & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.JENIS_PELAKU), "NULL,", "'" & item.JENIS_PELAKU & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.NAMA_ASLI), "NULL,", "'" & item.NAMA_ASLI & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.PARAMETER_PENCARIAN_NAMA), "NULL,", "'" & item.PARAMETER_PENCARIAN_NAMA & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.TEMPAT_LAHIR), "NULL,", "'" & item.TEMPAT_LAHIR & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.TANGGAL_LAHIR), "NULL,", "'" & item.TANGGAL_LAHIR & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.NPWP), "NULL,", "'" & item.NPWP & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.KTP), "NULL,", "'" & item.KTP & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.PAS), "NULL,", "'" & item.PAS & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.KITAS), "NULL,", "'" & item.KITAS & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.SUKET), "NULL,", "'" & item.SUKET & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.SIM), "NULL,", "'" & item.SIM & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.KITAP), "NULL,", "'" & item.KITAP & "',")
                    strQuery2 += IIf(String.IsNullOrEmpty(item.KIMS), "NULL,", "'" & item.KIMS & "',")                  '26-Oct-2021
                    strQuery2 += IIf(String.IsNullOrEmpty(item.Watchlist), "NULL,", "'" & item.Watchlist & "',")        '26-Oct-2021
                    strQuery2 += "1"
                    strQuery2 += ")"

                    strQuery = strQuery1 & strQuery2
                    'Throw New Exception(strQuery)
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Next
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub DeleteDataFromUploadBySP()
        Try
            Dim oparam(1) As SqlClient.SqlParameter

            oparam(0) = New SqlParameter
            oparam(0).ParameterName = "@userid"
            oparam(0).DbType = DbType.String
            oparam(0).Value = NawaBLL.Common.SessionCurrentUser.UserID

            oparam(1) = New SqlParameter
            oparam(1).ParameterName = "@ModuleID"
            oparam(1).DbType = DbType.Int32
            oparam(1).Value = ObjModule.PK_Module_ID

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_DeleteDataUserUpload", oparam)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ValidateDataUploadBySP()
        Try
            '30-Sep-2021 Adi : Parsing by XML to fill SIPENDAR_WATCHLIST_Upload table
            'Test case 9000 data take times to insert 1 by 1 with SQLHelper
            Dim objdt As New DataTable
            Dim objdataset = New DataSet()
            Dim objXMLData As String = ""
            Dim byteData As Byte() = Nothing

            'If listSIPENDAR_WATCHLIST IsNot Nothing Then
            '    objdt = NawaBLL.Common.CopyGenericToDataTable(listSIPENDAR_WATCHLIST)
            '    objdataset.Tables.Add(objdt)
            '    objXMLData = objdataset.GetXml()

            '    '22-Oct-2021 Adi : Cleansing data XML
            '    'objXMLData = Regex.Replace(objXMLData, "[ ](?=[ ])|[^<>/-_,A-Za-z0-9 ]+", "")
            '    Dim strRegex As String = "[ ](?=[ ])|[^<>/-_,A-Za-z0-9 ]+"
            '    Dim drRegex As Data.DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT TOP 1 ParameterValue FROM SIPENDAR_GLOBAL_PARAMETER WHERE PK_GlobalReportParameter_ID=12")
            '    If drRegex IsNot Nothing AndAlso Not IsDBNull(drRegex("ParameterValue")) Then
            '        strRegex = drRegex("ParameterValue")
            '    End If
            '    objXMLData = Regex.Replace(objXMLData, strRegex, "")
            '    '---------------------------------------------------------------------------------

            '    byteData = System.Text.Encoding.Default.GetBytes(objXMLData)
            'End If

            'Dim oparam(0) As SqlClient.SqlParameter
            Dim oparam(1) As SqlClient.SqlParameter

            oparam(0) = New SqlParameter
            oparam(0).ParameterName = "@userid"
            oparam(0).DbType = DbType.String
            oparam(0).Value = NawaBLL.Common.SessionCurrentUser.UserID


            oparam(1) = New SqlParameter
            oparam(1).ParameterName = "@TMP_udt_SIPENDAR_WATCHLIST_Upload_Detail"
            oparam(1).Value = DataTabelSIPENDAR_WATCHLIST_Upload_Detail
            oparam(1).SqlDbType = SqlDbType.Structured
            oparam(1).TypeName = "dbo.udt_SIPENDAR_WATCHLIST_Upload_Detail"

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_WatchListUpload_Validation", oparam)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Dtype Update for WorkFlow 2021/08/30"
    Protected Sub SaveWorkflowReviewData(intModule As Integer, struserid As String, strunik As String, pkmoduleapprovalid As Long)
        Try
            Dim objListParam(2) As System.Data.SqlClient.SqlParameter
            objListParam(0) = New System.Data.SqlClient.SqlParameter
            objListParam(1) = New System.Data.SqlClient.SqlParameter
            objListParam(2) = New System.Data.SqlClient.SqlParameter

            objListParam(0).ParameterName = "@pkmoduleid"
            objListParam(0).Value = intModule
            'di isi karena pas mau save, pasti sudah ada isinya
            objListParam(1).ParameterName = "@pkunikid"
            objListParam(1).Value = strunik

            objListParam(2).ParameterName = "@userId"
            objListParam(2).Value = struserid

            Dim pkworkflowid As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.StoredProcedure, "usp_ValidateMWorkflow", objListParam)

            If pkworkflowid > 0 Then

                ' Dim objtxtReview As Ext.Net.TextArea = FormPanelInput.FindControl("infoReviewNotes")
                Dim strReviewNotes As String = ""
                'If Not objtxtReview Is Nothing Then
                ' strReviewNotes = objtxtReview.Text
                'End If

                Using objdb As New NawaDAL.NawaDataEntities
                    objdb.Database.ExecuteSqlCommand("usp_CreateInitWorkflowModuleDesigner '" & pkworkflowid & "','" & strunik & "','" & NawaBLL.Common.SessionCurrentUser.PK_MUser_ID & "','" & intModule & "','" & NawaBLL.Common.SessionCurrentUser.PK_MUser_ID & "','" & strReviewNotes & "','3'," & pkmoduleapprovalid & ", '4'")
                End Using

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Update 29-Sep-2021 Adi : Konsep Serialize-Deserialize"

    Protected Sub SaveToModuleApproval()
        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    SaveDataHeaderAndDetail()
                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Upload
                    Dim strModuleName As String = "SIPENDAR_WATCHLIST"

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If


                    'Additional Information for Header
                    'For Each item In listSIPENDAR_WATCHLIST
                    '    With item
                    '        .Active = True
                    '        .CreatedBy = strUserID
                    '        .CreatedDate = Now
                    '        .Alternateby = strAlternateID
                    '    End With
                    'Next

                    'Save objData to Module Approval
                    'Dim objXMLData As String = NawaBLL.Common.Serialize(listSIPENDAR_WATCHLIST)
                    Dim objModuleApproval As New SiPendarDAL.ModuleApproval
                    With objModuleApproval
                        '.ModuleKey = 0
                        .ModuleKey = PKUploadHeder
                        .ModuleName = strModuleName
                        '.ModuleField = objXMLData
                        '.ModuleFieldBefore = Nothing
                        .PK_ModuleAction_ID = 7
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objTrans.Commit()

                    SaveToModuleApprovalUpload(objModuleApproval.PK_ModuleApproval_ID)
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Protected Sub SaveToModuleApprovalUpload(lngModuleApprovalID As Long)
        Try
            'Save objData to Table SIPENDAR_WATCHLIST_Upload
            Dim strQuery As String = ""

            strQuery += "INSERT INTO SIPENDAR_WATCHLIST_Upload_Approval "
            strQuery += "("
            strQuery += "      FK_ModuleApproval_ID,"
            strQuery += "      nawa_userid,"
            strQuery += "      nawa_recordnumber,"
            strQuery += "      KeteranganError,"
            strQuery += "      nawa_Action,"
            strQuery += "      PK_SIPENDAR_WATCHLIST_ID,"
            strQuery += "      PERIODE,"
            strQuery += "      ID_PPATK,"
            strQuery += "      KODE_WATCHLIST,"
            strQuery += "      JENIS_PELAKU,"
            strQuery += "      NAMA_ASLI,"
            strQuery += "      PARAMETER_PENCARIAN_NAMA,"
            strQuery += "      TEMPAT_LAHIR,"
            strQuery += "      TANGGAL_LAHIR,"
            strQuery += "      NPWP,"
            strQuery += "      KTP,"
            strQuery += "      PAS,"
            strQuery += "      KITAS,"
            strQuery += "      SUKET,"
            strQuery += "      SIM,"
            strQuery += "      KITAP,"
            strQuery += "      KIMS,"           '26-Oct-2021 Adi
            strQuery += "      Watchlist,"      '26-Oct-2021 Adi
            strQuery += "      Submission_Type,"      '15-Des-2021 Daniel
            strQuery += "      [Active]"
            strQuery += ") "
            strQuery += "SELECT " & lngModuleApprovalID & ","
            strQuery += "      nawa_userid,"
            strQuery += "      nawa_recordnumber,"
            strQuery += "      KeteranganError,"
            strQuery += "      nawa_Action,"
            strQuery += "      PK_SIPENDAR_WATCHLIST_ID,"
            strQuery += "      PERIODE,"
            strQuery += "      ID_PPATK,"
            strQuery += "      KODE_WATCHLIST,"
            strQuery += "      JENIS_PELAKU,"
            strQuery += "      NAMA_ASLI,"
            strQuery += "      PARAMETER_PENCARIAN_NAMA,"
            strQuery += "      TEMPAT_LAHIR,"
            strQuery += "      TANGGAL_LAHIR,"
            strQuery += "      NPWP,"
            strQuery += "      KTP,"
            strQuery += "      PAS,"
            strQuery += "      KITAS,"
            strQuery += "      SUKET,"
            strQuery += "      SIM,"
            strQuery += "      KITAP,"
            strQuery += "      KIMS,"           '26-Oct-2021 Adi
            strQuery += "      Watchlist,"      '26-Oct-2021 Adi
            strQuery += "      Submission_Type,"      '15-Des-2021 Daniel
            strQuery += "      [Active]"
            strQuery += " FROM SIPENDAR_WATCHLIST_Upload WHERE nawa_userid='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Update 13-Oct-2021 Adi : Tambah filter with datetime picker jadi untuk bind ke gridpanel diubah jadi remote paging"
    Protected Sub store_Watchlist_Valid_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Integer = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            If strsort = "" Then
                strsort = "ID_PPATK_INT asc"
            End If

            'Dim strSQL As String = "try_cast(a.PERIODE as DATETIME) as PERIODE, a.ID_PPATK, a.KODE_WATCHLIST, a.JENIS_PELAKU," &
            '                       " a.NAMA_ASLI, a.PARAMETER_PENCARIAN_NAMA, a.TEMPAT_LAHIR, a.TANGGAL_LAHIR, a.NPWP, a.KTP, a.PAS, a.KITAS, a.SUKET, a.SIM," &
            '                       " a.KITAP from SIPENDAR_WATCHLIST_Upload a where a.KeteranganError = '' and a.nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'"

            Dim strFields As String = "a.PK_Upload_ID, a.nawa_Action, a.PERIODE_DT, a.ID_PPATK_INT, a.KODE_WATCHLIST," &
                                      "a.JENIS_PELAKU,a.NAMA_ASLI, a.PARAMETER_PENCARIAN_NAMA, a.TEMPAT_LAHIR," &
                                      "a.TANGGAL_LAHIR_DT, a.NPWP, a.KTP," &
                                      "a.PAS, a.KITAS, a.SUKET, a.SIM, a.KITAP, a.KIMS, a.Watchlist, a.Submission_Type"

            If String.IsNullOrEmpty(strfilter) Then
                strfilter = "ISNULL(a.KeteranganError,'') = '' and a.nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            Else
                strfilter += " AND ISNULL(a.KeteranganError,'') = '' and a.nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            End If

            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_WATCHLIST_Upload a", strFields, strfilter, strsort, intStart, NawaBLL.SystemParameterBLL.GetPageSize, inttotalRecord)
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_WATCHLIST_Upload a", strFields, strfilter, strsort, intStart, intLimit, inttotalRecord)

            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_WATCHLIST", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start <0 OrElse limit <0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord

            store_Watchlist_Valid.DataSource = DataPaging
            store_Watchlist_Valid.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_Watchlist_Invalid_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Integer = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            If strsort = "" Then
                strsort = "ID_PPATK_INT asc"
            End If

            'Dim strSQL As String = "try_cast(a.PERIODE as DATETIME) as PERIODE, a.ID_PPATK, a.KODE_WATCHLIST, a.JENIS_PELAKU," &
            '                       " a.NAMA_ASLI, a.PARAMETER_PENCARIAN_NAMA, a.TEMPAT_LAHIR, a.TANGGAL_LAHIR, a.NPWP, a.KTP, a.PAS, a.KITAS, a.SUKET, a.SIM," &
            '                       " a.KITAP from SIPENDAR_WATCHLIST_Upload a where a.KeteranganError = '' and a.nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'"

            Dim strFields As String = "a.PK_Upload_ID, a.nawa_Action, a.PERIODE_DT, a.ID_PPATK_INT, a.KODE_WATCHLIST," &
                                      "a.JENIS_PELAKU,a.NAMA_ASLI, a.PARAMETER_PENCARIAN_NAMA, a.TEMPAT_LAHIR," &
                                      "a.TANGGAL_LAHIR_DT, a.NPWP, a.KTP," &
                                      "a.PAS, a.KITAS, a.SUKET, a.SIM, a.KITAP, a.KIMS, a.Watchlist, a.KeteranganError, a.Submission_Type"

            If String.IsNullOrEmpty(strfilter) Then
                strfilter = "ISNULL(a.KeteranganError,'') <> '' and a.nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            Else
                strfilter += " AND ISNULL(a.KeteranganError,'') <> '' and a.nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            End If

            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_WATCHLIST_Upload a", strFields, strfilter, strsort, intStart, NawaBLL.SystemParameterBLL.GetPageSize, inttotalRecord)
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_WATCHLIST_Upload a", strFields, strfilter, strsort, intStart, intLimit, inttotalRecord)

            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_WATCHLIST", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start <0 OrElse limit <0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord

            store_Watchlist_Invalid.DataSource = DataPaging
            store_Watchlist_Invalid.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Update 3-Nov-2021 Daniel : Perubahan Methode Save Data"
    Protected Sub SaveDataHeaderAndDetail()
        Try
            Dim oparam(2) As SqlClient.SqlParameter

            oparam(0) = New SqlParameter
            oparam(0).ParameterName = "@userid"
            oparam(0).DbType = DbType.String
            oparam(0).Value = NawaBLL.Common.SessionCurrentUser.UserID

            oparam(1) = New SqlParameter
            oparam(1).ParameterName = "@TMP_udt_SIPENDAR_WATCHLIST_Upload_Header"
            oparam(1).Value = DataTabelSIPENDAR_WATCHLIST_Upload_Header
            oparam(1).SqlDbType = SqlDbType.Structured
            oparam(1).TypeName = "dbo.udt_SIPENDAR_WATCHLIST_Upload_Header"

            oparam(2) = New SqlParameter
            oparam(2).ParameterName = "@TMP_udt_SIPENDAR_WATCHLIST_Upload_Detail"
            oparam(2).Value = DataTabelSIPENDAR_WATCHLIST_Upload_Detail
            oparam(2).SqlDbType = SqlDbType.Structured
            oparam(2).TypeName = "dbo.udt_SIPENDAR_WATCHLIST_Upload_Detail"

            'PKUploadHeder = NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_WatchList_SaveDataRaw", oparam)
            Dim temprow As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_WatchList_SaveDataRaw", oparam)
            If Not IsDBNull(temprow("PKUploadHeader")) Then
                PKUploadHeder = temprow("PKUploadHeader")
            End If
            'PKUploadHeder = PKUploadHeder
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub store_Watchlist_RawData_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Integer = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            'Dim DataPaging As Data.DataTable = DataTabelSIPENDAR_WATCHLIST_Upload_Detail

            Dim dataviewuploaddetail As DataView = DataTabelSIPENDAR_WATCHLIST_Upload_Detail.DefaultView
            dataviewuploaddetail.Sort = strsort
            dataviewuploaddetail.RowFilter = strfilter

            '-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            '-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord

            store_Watchlist_RawData.DataSource = dataviewuploaddetail
            store_Watchlist_RawData.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class
