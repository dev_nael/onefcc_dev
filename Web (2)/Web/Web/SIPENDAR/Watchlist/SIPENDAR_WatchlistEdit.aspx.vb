﻿
Imports System.Data
Imports System.Data.Entity
Imports System.Data.SqlClient

Partial Class SIPENDAR_WatchlistEdit
    Inherits System.Web.UI.Page

    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("SIPENDAR_WatchlistEdit.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("SIPENDAR_WatchlistEdit.ObjModule") = value
        End Set
    End Property

    Public Property ErrorValidation() As String
        Get
            Return Session("SIPENDAR_WatchlistEdit.ErrorValidation")
        End Get
        Set(ByVal value As String)
            Session("SIPENDAR_WatchlistEdit.ErrorValidation") = value
        End Set
    End Property
    Public Property DataID As String
        Get
            Return Session("SIPENDAR_WatchlistEdit.DataID")
        End Get
        Set(ByVal value As String)
            Session("SIPENDAR_WatchlistEdit.DataID") = value
        End Set
    End Property
    Public Property DataOld As SiPendarDAL.SIPENDAR_WATCHLIST
        Get
            Return Session("SIPENDAR_WatchlistEdit.DataOld")
        End Get
        Set(ByVal value As SiPendarDAL.SIPENDAR_WATCHLIST)
            Session("SIPENDAR_WatchlistEdit.DataOld") = value
        End Set
    End Property

    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            ErrorValidation = ""

            If PERIODEField.SelectedDate = DateTime.MinValue Then
                ErrorValidation = ErrorValidation & "<br> " & PERIODEField.FieldLabel & " is Mandatory. "
            Else
                ValidateDataDate(PERIODEField.Text, PERIODEField.FieldLabel)
            End If

            If String.IsNullOrEmpty(ID_PPATKField.Text) Then
                ErrorValidation = ErrorValidation & "<br> " & ID_PPATKField.FieldLabel & " is Mandatory. "
            Else
                ValidateDataNumberic(ID_PPATKField.Text, ID_PPATKField.FieldLabel)
            End If

            If String.IsNullOrEmpty(KODE_WATCHLISTField.Text) Then
                ErrorValidation = ErrorValidation & "<br> " & KODE_WATCHLISTField.FieldLabel & " is Mandatory. "
            End If

            If String.IsNullOrEmpty(JENIS_PELAKUField.Text) Then
                ErrorValidation = ErrorValidation & "<br> " & JENIS_PELAKUField.FieldLabel & " is Mandatory. "
            End If

            If String.IsNullOrEmpty(NAMA_ASLIField.Text) Then
                ErrorValidation = ErrorValidation & "<br> " & NAMA_ASLIField.FieldLabel & " is Mandatory. "
            End If

            If String.IsNullOrEmpty(PARAMETER_PENCARIAN_NAMAField.Text) Then
                ErrorValidation = ErrorValidation & "<br> " & PARAMETER_PENCARIAN_NAMAField.FieldLabel & " is Mandatory. "
            End If

            Dim tempsubmissiontype As String = ""
            If Not String.IsNullOrEmpty(cmb_SUBMISSION_TYPE.SelectedItemValue) Then
                tempsubmissiontype = cmb_SUBMISSION_TYPE.SelectedItemValue
            ElseIf Not String.IsNullOrEmpty(cmb_SUBMISSION_TYPE.TextValue) Then
                tempsubmissiontype = cmb_SUBMISSION_TYPE.TextValue
            End If

            If String.IsNullOrEmpty(tempsubmissiontype) Then
                ErrorValidation = ErrorValidation & "<br> " & cmb_SUBMISSION_TYPE.Label & " is Mandatory. "
            End If

            If Not String.IsNullOrEmpty(ID_PPATKField.Text) AndAlso Not String.IsNullOrEmpty(tempsubmissiontype) Then
                Dim isDataChange As Boolean = False
                If DataOld.ID_PPATK <> ID_PPATKField.Text Then
                    isDataChange = True
                End If
                If DataOld.Submission_Type <> tempsubmissiontype Then
                    isDataChange = True
                End If
                ValidateIDPPATKInApproval(ID_PPATKField.Text, tempsubmissiontype, isDataChange)
            End If

            If String.IsNullOrEmpty(NPWPField.Text) Then
                ValidateDataNumberic(NPWPField.Text, NPWPField.FieldLabel)
            End If

            If String.IsNullOrEmpty(KTPField.Text) Then
                ValidateDataNumberic(KTPField.Text, KTPField.FieldLabel)
            End If

            'If String.IsNullOrEmpty(PASField.Text) Then
            '    ValidateDataNumberic(PASField.Text, PASField.FieldLabel)
            'End If

            If String.IsNullOrEmpty(KITASField.Text) Then
                ValidateDataNumberic(KITASField.Text, KITASField.FieldLabel)
            End If

            If String.IsNullOrEmpty(SUKETField.Text) Then
                ValidateDataNumberic(SUKETField.Text, SUKETField.FieldLabel)
            End If

            If String.IsNullOrEmpty(SIMField.Text) Then
                ValidateDataNumberic(SIMField.Text, SIMField.FieldLabel)
            End If

            If String.IsNullOrEmpty(KITAPField.Text) Then
                ValidateDataNumberic(KITAPField.Text, KITAPField.FieldLabel)
            End If

            If ErrorValidation <> "" Then
                SiPendarBLL.NawaFramework.extInfoPanelupdate(FormPanelInput, ErrorValidation, "infoValidationResultPanel")
                FormPanelInput.Body.ScrollTo(Direction.Top, 0)
                infoValidationResultPanel.Hidden = False
            Else
                Dim objwatchlist As New SiPendarDAL.SIPENDAR_WATCHLIST
                'objwatchlist = DataOld
                Dim isDataChange As Boolean = False
                With objwatchlist
                    .PERIODE = PERIODEField.SelectedDate.ToString("yyyy-MM-dd")
                    If DataOld.PERIODE <> .PERIODE Then
                        isDataChange = True
                    End If
                    .ID_PPATK = ID_PPATKField.Value
                    If DataOld.ID_PPATK <> .ID_PPATK Then
                        isDataChange = True
                    End If
                    .KODE_WATCHLIST = KODE_WATCHLISTField.Value
                    If DataOld.KODE_WATCHLIST <> .KODE_WATCHLIST Then
                        isDataChange = True
                    End If
                    .JENIS_PELAKU = JENIS_PELAKUField.Value
                    If DataOld.JENIS_PELAKU <> .JENIS_PELAKU Then
                        isDataChange = True
                    End If
                    .NAMA_ASLI = NAMA_ASLIField.Value
                    If DataOld.NAMA_ASLI <> .NAMA_ASLI Then
                        isDataChange = True
                    End If
                    .PARAMETER_PENCARIAN_NAMA = PARAMETER_PENCARIAN_NAMAField.Value
                    If DataOld.PARAMETER_PENCARIAN_NAMA <> .PARAMETER_PENCARIAN_NAMA Then
                        isDataChange = True
                    End If
                    .TEMPAT_LAHIR = TEMPAT_LAHIRField.Value
                    If DataOld.TEMPAT_LAHIR <> .TEMPAT_LAHIR Then
                        isDataChange = True
                    End If
                    If TANGGAL_LAHIRField.SelectedDate <> DateTime.MinValue Then
                        .TANGGAL_LAHIR = TANGGAL_LAHIRField.SelectedDate.ToString("yyyy-MM-dd")
                        If DataOld.TANGGAL_LAHIR <> .TANGGAL_LAHIR Then
                            isDataChange = True
                        End If
                    End If
                    If NPWPField.Value IsNot Nothing Then
                        .NPWP = NPWPField.Value
                        If DataOld.NPWP <> .NPWP Then
                            isDataChange = True
                        End If
                    End If
                    If KTPField.Value IsNot Nothing Then
                        .KTP = KTPField.Value
                        If DataOld.KTP <> .KTP Then
                            isDataChange = True
                        End If
                    End If
                    If PASField.Value IsNot Nothing Then
                        .PAS = PASField.Value
                        If DataOld.PAS <> .PAS Then
                            isDataChange = True
                        End If
                    End If
                    If KITASField.Value IsNot Nothing Then
                        .KITAS = KITASField.Value
                        If DataOld.KITAS <> .KITAS Then
                            isDataChange = True
                        End If
                    End If
                    If SUKETField.Value IsNot Nothing Then
                        .SUKET = SUKETField.Value
                        If DataOld.SUKET <> .SUKET Then
                            isDataChange = True
                        End If
                    End If
                    If SIMField.Value IsNot Nothing Then
                        .SIM = SIMField.Value
                        If DataOld.SIM <> .SIM Then
                            isDataChange = True
                        End If
                    End If
                    If KITAPField.Value IsNot Nothing Then
                        .KITAP = KITAPField.Value
                        If DataOld.KITAP <> .KITAP Then
                            isDataChange = True
                        End If
                    End If
                    'Ari 25 Oct 2021 Menambah 2 Field KIMS dan Watchlist
                    If KIMSField.Value IsNot Nothing Then
                        .KIMS = KIMSField.Value
                        If DataOld.KIMS <> .KIMS Then
                            isDataChange = True
                        End If
                    End If

                    Dim cekwatclist As String
                    If chk_IsWatchlist.Checked = True Then
                        cekwatclist = "True"
                    Else
                        cekwatclist = "False"
                    End If

                    Dim iswatchlist As String
                    If DataOld.Watchlist = "True" Then
                        iswatchlist = "True"
                    Else
                        iswatchlist = "False"
                    End If
                    If cekwatclist = iswatchlist Then
                        .Watchlist = cekwatclist
                    Else
                        .Watchlist = cekwatclist
                        isDataChange = True
                    End If
                    'If NawaBLL.Common.SessionAlternateUser.UserID IsNot Nothing Then
                    '    .Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    'End If

                    .Submission_Type = tempsubmissiontype
                    If DataOld.Submission_Type <> .Submission_Type Then
                        isDataChange = True
                    End If

                    .PK_SIPENDAR_WATCHLIST_ID = DataOld.PK_SIPENDAR_WATCHLIST_ID
                    .Active = DataOld.Active
                    .ApprovedBy = DataOld.ApprovedBy
                    .ApprovedDate = DataOld.ApprovedDate
                    .CreatedBy = DataOld.CreatedBy
                    .CreatedDate = DataOld.CreatedDate
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateDate = DateTime.Now
                End With
                If Not isDataChange Then
                    Throw New ApplicationException("Please Do Some Data Change Before Save")
                End If
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not ObjModule.IsUseApproval Then
                    SaveDataWithoutApproval(objwatchlist)
                    Panelconfirmation.Hidden = False
                    FormPanelInput.Hidden = True
                    LblConfirmation.Text = "Data Save into Database ."
                Else
                    SaveDataWithApproval(objwatchlist)
                    Panelconfirmation.Hidden = False
                    FormPanelInput.Hidden = True
                    LblConfirmation.Text = "Data Saved into Pending Approval"
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Moduleid = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub SIPENDAR_WatchlistEdit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer

            intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)


            If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Update) Then
                Dim strIDCode As String = 1
                strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                Exit Sub
            End If
            If Not Ext.Net.X.IsAjaxRequest Then
                'Session("ComboBox" + cmb_SUBMISSION_TYPE.ID + ".StringValue") = Nothing
                cmb_SUBMISSION_TYPE.SetTextValue("")
                LoadDataFromDatabase()
                If DataOld IsNot Nothing AndAlso DataOld.ID_PPATK IsNot Nothing AndAlso DataOld.Submission_Type IsNot Nothing Then
                    ValidateIDPPATKInApproval(DataOld.ID_PPATK, DataOld.Submission_Type, False)
                End If
                If ErrorValidation <> "" Then
                    SiPendarBLL.NawaFramework.extInfoPanelupdate(FormPanelInput, ErrorValidation, "infoValidationResultPanel")
                    FormPanelInput.Body.ScrollTo(Direction.Top, 0)
                    infoValidationResultPanel.Hidden = False
                End If
            End If
            ErrorValidation = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataFromDatabase()
        Try
            Dim dataStr As String = Request.Params("ID")
            DataID = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            DataOld = New SiPendarDAL.SIPENDAR_WATCHLIST
            DataOld.PK_SIPENDAR_WATCHLIST_ID = CLng(DataID)
            Dim stringquery As String = "select top 1 * from SIPENDAR_WATCHLIST where PK_SIPENDAR_WATCHLIST_ID = " & DataID
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery, Nothing)
            If drResult IsNot Nothing Then
                If Not IsDBNull(drResult("PERIODE")) Then
                    DataOld.PERIODE = drResult("PERIODE")
                    PERIODEField.SelectedDate = drResult("PERIODE")
                End If
                If Not IsDBNull(drResult("ID_PPATK")) Then
                    DataOld.ID_PPATK = drResult("ID_PPATK")
                    ID_PPATKField.Text = drResult("ID_PPATK")
                End If
                If Not IsDBNull(drResult("KODE_WATCHLIST")) Then
                    DataOld.KODE_WATCHLIST = drResult("KODE_WATCHLIST")
                    KODE_WATCHLISTField.Text = drResult("KODE_WATCHLIST")
                End If
                If Not IsDBNull(drResult("JENIS_PELAKU")) Then
                    DataOld.JENIS_PELAKU = drResult("JENIS_PELAKU")
                    JENIS_PELAKUField.Text = drResult("JENIS_PELAKU")
                End If
                If Not IsDBNull(drResult("NAMA_ASLI")) Then
                    DataOld.NAMA_ASLI = drResult("NAMA_ASLI")
                    NAMA_ASLIField.Text = drResult("NAMA_ASLI")
                End If
                If Not IsDBNull(drResult("PARAMETER_PENCARIAN_NAMA")) Then
                    DataOld.PARAMETER_PENCARIAN_NAMA = drResult("PARAMETER_PENCARIAN_NAMA")
                    PARAMETER_PENCARIAN_NAMAField.Text = drResult("PARAMETER_PENCARIAN_NAMA")
                End If
                If Not IsDBNull(drResult("TEMPAT_LAHIR")) Then
                    DataOld.TEMPAT_LAHIR = drResult("TEMPAT_LAHIR")
                    TEMPAT_LAHIRField.Text = drResult("TEMPAT_LAHIR")
                End If
                If Not IsDBNull(drResult("TANGGAL_LAHIR")) Then
                    DataOld.TANGGAL_LAHIR = drResult("TANGGAL_LAHIR")
                    TANGGAL_LAHIRField.SelectedDate = drResult("TANGGAL_LAHIR")
                End If
                If Not IsDBNull(drResult("NPWP")) Then
                    DataOld.NPWP = drResult("NPWP")
                    NPWPField.Text = drResult("NPWP")
                End If
                If Not IsDBNull(drResult("KTP")) Then
                    DataOld.KTP = drResult("KTP")
                    KTPField.Text = drResult("KTP")
                End If
                If Not IsDBNull(drResult("PAS")) Then
                    DataOld.PAS = drResult("PAS")
                    PASField.Text = drResult("PAS")
                End If
                If Not IsDBNull(drResult("KITAS")) Then
                    DataOld.KITAS = drResult("KITAS")
                    KITASField.Text = drResult("KITAS")
                End If
                If Not IsDBNull(drResult("SUKET")) Then
                    DataOld.SUKET = drResult("SUKET")
                    SUKETField.Text = drResult("SUKET")
                End If
                If Not IsDBNull(drResult("SIM")) Then
                    DataOld.SIM = drResult("SIM")
                    SIMField.Text = drResult("SIM")
                End If

                'Ari 25 Oct 2021 Menambahn KIMS dan Watchlist
                If Not IsDBNull(drResult("KIMS")) Then
                    DataOld.KIMS = drResult("KIMS")
                    KIMSField.Text = drResult("KIMS")
                End If

                If Not IsDBNull(drResult("Watchlist")) Then
                    If drResult("Watchlist") = "True" Then
                        chk_IsWatchlist.Checked = True
                        DataOld.Watchlist = drResult("Watchlist")
                    Else
                        chk_IsWatchlist.Checked = False
                        DataOld.Watchlist = drResult("Watchlist")
                    End If
                End If

                If Not IsDBNull(drResult("Submission_Type")) Then
                    DataOld.Submission_Type = drResult("Submission_Type")
                    Dim strQuery As String = "select top 1 Kode, Keterangan from SIPENDAR_SUBMISSION_TYPE where Kode = '" & DataOld.Submission_Type & "'"
                    Dim drResultSubmission_Type As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                    If drResultSubmission_Type IsNot Nothing AndAlso Not IsDBNull(drResultSubmission_Type("Kode")) AndAlso Not IsDBNull(drResultSubmission_Type("Keterangan")) Then
                        cmb_SUBMISSION_TYPE.SetTextWithTextValue(drResultSubmission_Type("Kode"), drResultSubmission_Type("Keterangan"))
                    End If
                End If

                If Not IsDBNull(drResult("KITAP")) Then
                    DataOld.KITAP = drResult("KITAP")
                    KITAPField.Text = drResult("KITAP")
                End If
                If Not IsDBNull(drResult("Active")) Then
                    DataOld.Active = drResult("Active")
                End If
                If Not IsDBNull(drResult("CreatedBy")) Then
                    DataOld.CreatedBy = drResult("CreatedBy")
                End If
                If Not IsDBNull(drResult("CreatedDate")) Then
                    DataOld.CreatedDate = drResult("CreatedDate")
                End If
                If Not IsDBNull(drResult("LastUpdateBy")) Then
                    DataOld.LastUpdateBy = drResult("LastUpdateBy")
                End If
                If Not IsDBNull(drResult("LastUpdateDate")) Then
                    DataOld.LastUpdateDate = drResult("LastUpdateDate")
                End If
                If Not IsDBNull(drResult("ApprovedBy")) Then
                    DataOld.ApprovedBy = drResult("ApprovedBy")
                End If
                If Not IsDBNull(drResult("ApprovedDate")) Then
                    DataOld.ApprovedDate = drResult("ApprovedDate")
                End If
                If Not IsDBNull(drResult("Alternateby")) Then
                    DataOld.Alternateby = drResult("Alternateby")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SaveDataWithApproval(objwatchlist As SiPendarDAL.SIPENDAR_WATCHLIST)
        Using objdb As New SiPendarDAL.SiPendarEntities
            Using objtrans As DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    If objwatchlist IsNot Nothing Then
                        Dim dataXML As String = NawaBLL.Common.Serialize(objwatchlist)
                        Dim dataXMLOLD As String = NawaBLL.Common.Serialize(DataOld)
                        Dim objModuleApproval As New SiPendarDAL.ModuleApproval
                        With objModuleApproval
                            .ModuleName = ObjModule.ModuleName
                            .ModuleKey = objwatchlist.ID_PPATK
                            .ModuleField = dataXML
                            .ModuleFieldBefore = dataXMLOLD
                            .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                        End With
                        objdb.Entry(objModuleApproval).State = EntityState.Added
                        objdb.SaveChanges()

                        Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                        Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                        Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                        Dim modulename As String = ObjModule.ModuleLabel
                        Dim objATHeader As SiPendarDAL.AuditTrailHeader = SiPendarBLL.NawaFramework.CreateAuditTrail(objdb, user, act, acts, modulename)
                        SiPendarBLL.NawaFramework.CreateAuditTrailDetailEdit(objdb, objATHeader.PK_AuditTrail_ID, objwatchlist, DataOld)
                        objdb.SaveChanges()
                    End If
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Private Sub SaveDataWithoutApproval(objwatchlist As SiPendarDAL.SIPENDAR_WATCHLIST)
        Using objdb As New SiPendarDAL.SiPendarEntities
            Using objtrans As DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    If objwatchlist IsNot Nothing Then
                        objdb.Entry(objwatchlist).State = EntityState.Modified
                        objdb.SaveChanges()

                        Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                        Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                        Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                        Dim modulename As String = ObjModule.ModuleLabel
                        Dim ObjAuditTrailHeader As SiPendarDAL.AuditTrailHeader = SiPendarBLL.NawaFramework.CreateAuditTrail(objdb, user, act, acts, modulename)
                        SiPendarBLL.NawaFramework.CreateAuditTrailDetailEdit(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, objwatchlist, DataOld)
                        objdb.SaveChanges()
                    End If
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using

        '21-June-2022 Riyan sync sipendar watchlist to AML Watchlist
        Dim objParamAPI(0) As SqlParameter
        objParamAPI(0) = New SqlParameter
        objParamAPI(0).ParameterName = "@UserID"
        objParamAPI(0).Value = NawaBLL.Common.SessionCurrentUser.UserID

        NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_WATCHLIST_AfterSave", objParamAPI)
        'End sync sipendar watchlist to AML Watchlist

    End Sub

    Private Sub ValidateDataNumberic(data As String, objname As String)
        Try
            If Not (data.All(AddressOf Char.IsDigit)) Then
                ErrorValidation = ErrorValidation & "<br> " & objname & " Must Numberic. "
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ValidateDataDate(data As String, objname As String)
        Try
            Dim dtemp As Date = CDate(data)
        Catch ex As Exception
            ErrorValidation = ErrorValidation & "<br> " & objname & " Value Not a Date. "
        End Try
    End Sub

    Private Sub ValidateIDPPATKInApproval(ID_PPATK As String, Submission_Type As String, keyChange As Boolean)
        Dim strQuery As String = "select top 1 ID_PPATK from SIPENDAR_WATCHLIST_Upload_approval where ID_PPATK = '" & ID_PPATK & "' and Submission_Type = '" & Submission_Type & "'"
        Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        If drResult IsNot Nothing Then
            ErrorValidation = ErrorValidation & "<br> Data With ID PPATK " & ID_PPATK & " and Submission Type " & Submission_Type & " Already Exist in Approval Upload. "
        End If

        strQuery = "select top 1 PK_ModuleApproval_ID  from ModuleApproval"
        strQuery = strQuery & " where ModuleName = '" & ObjModule.ModuleName & "'"
        strQuery = strQuery & " and ('" & Submission_Type & "-" & ID_PPATK & "') = ModuleKey"
        strQuery = strQuery & " and PK_ModuleAction_ID <> 7"
        drResult = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        If drResult IsNot Nothing Then
            ErrorValidation = ErrorValidation & "<br> Data With ID PPATK " & ID_PPATK & " and Submission Type " & Submission_Type & " Already Exist in Approval. "
        End If
        If keyChange Then
            strQuery = "select top 1 PK_SIPENDAR_WATCHLIST_ID from SIPENDAR_WATCHLIST"
            strQuery = strQuery & " where ID_PPATK = '" & ID_PPATK & "'"
            strQuery = strQuery & " and Submission_Type = '" & Submission_Type & "'"
            drResult = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            If drResult IsNot Nothing Then
                ErrorValidation = ErrorValidation & "<br> Data With ID PPATK " & ID_PPATK & " and Submission Type " & Submission_Type & " Already Exist. "
            End If
        End If
    End Sub

    Private Function CheckIsDataDate(Data As String) As Boolean
        Try
            Dim tempdate As DateTime = CDate(Data)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function CheckIsDataNumberic(Data As String) As Boolean
        Try
            Return (Data.All(AddressOf Char.IsDigit))
        Catch ex As Exception
            Return False
        End Try
    End Function

End Class
