﻿Imports SiPendarDAL
Imports SiPendarBLL
Imports System.IO
Imports System.Data
Imports NawaDAL
Imports System.Globalization

Partial Class SIPENDAR_Watchlist_SIPENDAR_WatchlistUpload_ApprovalHistory_Detail
    Inherits ParentPage
    Public objFormModuleDetail As NawaBLL.FormModuleDetail
    Const PATH_TEMP_DIR As String = "~\temp\"



    Public Property IDUnik() As Long
        Get
            Return Session("SIPENDAR_WatchlistUpload_ApprovalHistory_Detail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("SIPENDAR_WatchlistUpload_ApprovalHistory_Detail.IDUnik") = value
        End Set
    End Property

    Private Sub SIPENDAR_WatchlistUpload_ApprovalHistory_Detail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                'Clear Session


                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If

                Load_Watchlist_Approval()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Load_Watchlist_Approval()
        Dim objWatchlist As New SiPendarDAL.SIPENDAR_WATCHLIST_APPROVAL_HISTORY
        Using objdb As New SiPendarDAL.SiPendarEntities
            objWatchlist = objdb.SIPENDAR_WATCHLIST_APPROVAL_HISTORY.Where(Function(x) x.PK_Sipendar_Watchlist_Approval_History_ID = IDUnik).FirstOrDefault
        End Using

        If objWatchlist IsNot Nothing Then
            If objWatchlist.ModuleField = "" Then
                objWatchlist.ModuleField = Nothing
            End If
            If objWatchlist.ModuleFieldBefore = "" Then
                objWatchlist.ModuleFieldBefore = Nothing
            End If



            With objWatchlist
                ' jika modulefieldnya kosong keduanya
                If objWatchlist.ModuleField Is Nothing And objWatchlist.ModuleFieldBefore Is Nothing Then
                    txtPK_Sipendar_Watchlist_Approval_History_ID.Value = .PK_Sipendar_Watchlist_Approval_History_ID
                    txtModuleName.Value = "Sipendar Watchlist"
                    txtModuleKey.Value = .ModuleKey
                    'txtModuleField.Value = .ModuleField
                    'txtModuleFieldBefore.Value = .ModuleFieldBefore
                    txtPK_ModuleAction_ID.Value = .PK_ModuleAction_ID & " - " & GetModuleActionName(.PK_ModuleAction_ID)
                    txtFK_MRole_ID.Value = .FK_MRole_ID & " - " & GetMRoleName(.FK_MRole_ID)
                    Using objdb As New NawaDAL.NawaDataEntities
                        Dim userids As Integer = objWatchlist.UserID
                        Dim username = objdb.MUsers.Where(Function(x) x.PK_MUser_ID = userids).FirstOrDefault
                        If username IsNot Nothing AndAlso username.UserID IsNot Nothing Then
                            txtUserID.Value = username.UserID
                        End If

                    End Using
                    txtStatus.Value = .Status
                    txtNotes.Value = .Notes
                    txtCreatedBy.Value = .CreatedBy
                    txtCreatedDate.Value = .CreatedDate.Value.ToString("dd-MMM-yyyy")
                    If .PK_ModuleAction_ID = 7 AndAlso .ModuleKey IsNot Nothing Then
                        Dim stringquery As String = ""
                        'SIPENDAR_WATCHLIST_Upload_Detail (RawData)
                        PanelRawData.Title = "Sipendar Watchlist Upload Raw Data"
                        stringquery = " select * from SIPENDAR_WATCHLIST_Upload_Detail where FK_SIPENDAR_WATCHLIST_Upload_Header_ID = " & .ModuleKey
                        StoreWatchlistNew.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
                        StoreWatchlistNew.DataBind()
                        PanelRawData.Hidden = False

                        'SIPENDAR_WATCHLIST_Upload_Detail_Cleansing (RawData)
                        stringquery = " select "
                        stringquery += " case when olddata.ID_PPATK is null then 'Insert' "
                        stringquery += " else 'Update' "
                        stringquery += " end as nawaaction, "
                        stringquery += " cleansing.*  "
                        stringquery += " from SIPENDAR_WATCHLIST_Upload_Detail_Cleansing cleansing "
                        stringquery += " left join SIPENDAR_WATCHLIST_Upload_Detail_OldData olddata "
                        stringquery += " on olddata.FK_SIPENDAR_WATCHLIST_Upload_Header_ID = cleansing.FK_SIPENDAR_WATCHLIST_Upload_Header_ID "
                        stringquery += " and olddata.ID_PPATK = cleansing.ID_PPATK "
                        stringquery += " where cleansing.FK_SIPENDAR_WATCHLIST_Upload_Header_ID = " & .ModuleKey
                        StoreWatchlistCleansing.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
                        StoreWatchlistCleansing.DataBind()
                        PanelCleanedData.Hidden = False

                        'SIPENDAR_WATCHLIST_Upload_Detail_OldData (RawData)
                        stringquery = " select * from SIPENDAR_WATCHLIST_Upload_Detail_OldData where FK_SIPENDAR_WATCHLIST_Upload_Header_ID = " & .ModuleKey
                        Dim datatabeloldwatchlist As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
                        If datatabeloldwatchlist IsNot Nothing AndAlso datatabeloldwatchlist.Rows.Count > 0 Then
                            StoreWatchlistOld.DataSource = datatabeloldwatchlist
                            StoreWatchlistOld.DataBind()
                            PanelOldData.Hidden = False
                        End If
                    End If
                ElseIf objWatchlist.ModuleField IsNot Nothing And objWatchlist.ModuleFieldBefore IsNot Nothing Then 'jika actionnya edit
                    txtPK_Sipendar_Watchlist_Approval_History_ID.Value = .PK_Sipendar_Watchlist_Approval_History_ID
                    txtModuleName.Value = "Sipendar Watchlist"
                    txtModuleKey.Value = .ModuleKey
                    'txtModuleField.Hidden = True
                    'txtModuleFieldBefore.Hidden = True
                    txtPK_ModuleAction_ID.Value = .PK_ModuleAction_ID & " - " & GetModuleActionName(.PK_ModuleAction_ID)
                    txtFK_MRole_ID.Value = .FK_MRole_ID & " - " & GetMRoleName(.FK_MRole_ID)
                    Using objdb As New NawaDAL.NawaDataEntities
                        Dim userids As Integer = objWatchlist.UserID
                        Dim username = objdb.MUsers.Where(Function(x) x.PK_MUser_ID = userids).FirstOrDefault
                        If username IsNot Nothing Then
                            txtUserID.Value = username.UserID
                        End If

                    End Using
                    txtStatus.Value = .Status
                    txtNotes.Value = .Notes
                    txtCreatedBy.Value = .CreatedBy
                    txtCreatedDate.Value = .CreatedDate.Value.ToString("dd-MMM-yyyy")
                    formwatchlistnew.Hidden = False
                    formwatchlistold.Hidden = False
                    Dim unikkeyOld As String = Guid.NewGuid.ToString
                    Dim unikkeyNew As String = Guid.NewGuid.ToString



                    SiPendarBLL.SipendarWatchlistBLL.LoadPanel(formwatchlistnew, objWatchlist.ModuleField, objWatchlist.ModuleName, unikkeyNew)
                    SiPendarBLL.SipendarWatchlistBLL.LoadPanel(formwatchlistold, objWatchlist.ModuleFieldBefore, objWatchlist.ModuleName, unikkeyOld)
                    SiPendarBLL.SipendarWatchlistBLL.SettingColor(formwatchlistold, formwatchlistnew, unikkeyOld, unikkeyNew)

                ElseIf objWatchlist.ModuleField IsNot Nothing And objWatchlist.ModuleFieldBefore Is Nothing Then 'jika actionnya add / upload / delete
                    If objWatchlist.PK_ModuleAction_ID = 1 Then ' untuk add
                        txtPK_Sipendar_Watchlist_Approval_History_ID.Value = .PK_Sipendar_Watchlist_Approval_History_ID
                        txtModuleName.Value = "Sipendar Watchlist"
                        txtModuleKey.Value = .ModuleKey
                        'txtModuleField.Hidden = True
                        'txtModuleFieldBefore.Hidden = False
                        txtPK_ModuleAction_ID.Value = .PK_ModuleAction_ID & " - " & GetModuleActionName(.PK_ModuleAction_ID)
                        txtFK_MRole_ID.Value = .FK_MRole_ID & " - " & GetMRoleName(.FK_MRole_ID)
                        Using objdb As New NawaDAL.NawaDataEntities
                            Dim userids As Integer = objWatchlist.UserID
                            Dim username = objdb.MUsers.Where(Function(x) x.PK_MUser_ID = userids).FirstOrDefault
                            If username IsNot Nothing Then
                                txtUserID.Value = username.UserID
                            End If

                        End Using
                        txtStatus.Value = .Status
                        txtNotes.Value = .Notes
                        txtCreatedBy.Value = .CreatedBy
                        txtCreatedDate.Value = .CreatedDate.Value.ToString("dd-MMM-yyyy")
                        formwatchlistnew.Hidden = False
                        Dim unikkey As String = Guid.NewGuid.ToString

                        SiPendarBLL.SipendarWatchlistBLL.LoadPanel(formwatchlistnew, objWatchlist.ModuleField, objWatchlist.ModuleName, unikkey)
                    ElseIf objWatchlist.PK_ModuleAction_ID = 3 Then 'untuk delete
                        txtPK_Sipendar_Watchlist_Approval_History_ID.Value = .PK_Sipendar_Watchlist_Approval_History_ID
                        txtModuleName.Value = "Sipendar Watchlist"
                        txtModuleKey.Value = .ModuleKey
                        'txtModuleField.Hidden = True
                        'txtModuleFieldBefore.Hidden = False
                        txtPK_ModuleAction_ID.Value = .PK_ModuleAction_ID & " - " & GetModuleActionName(.PK_ModuleAction_ID)
                        txtFK_MRole_ID.Value = .FK_MRole_ID & " - " & GetMRoleName(.FK_MRole_ID)
                        Using objdb As New NawaDAL.NawaDataEntities
                            Dim userids As Integer = objWatchlist.UserID
                            Dim username = objdb.MUsers.Where(Function(x) x.PK_MUser_ID = userids).FirstOrDefault
                            If username IsNot Nothing Then
                                txtUserID.Value = username.UserID
                            End If

                        End Using
                        txtStatus.Value = .Status
                        txtNotes.Value = .Notes
                        txtCreatedBy.Value = .CreatedBy
                        txtCreatedDate.Value = .CreatedDate.Value.ToString("dd-MMM-yyyy")
                        formwatchlistold.Hidden = False
                        Dim unikkey As String = Guid.NewGuid.ToString

                        SiPendarBLL.SipendarWatchlistBLL.LoadPanel(formwatchlistold, objWatchlist.ModuleField, objWatchlist.ModuleName, unikkey)
                        '2021/12/16 Daniel Bagian ('objWatchlist.PK_ModuleAction_ID = 7') sudah tidak diperlukan dan dapat dihapus/comment kapan saja apabila diperlukan
                    ElseIf objWatchlist.PK_ModuleAction_ID = 7 Then 'untuk upload 
                        txtPK_Sipendar_Watchlist_Approval_History_ID.Value = .PK_Sipendar_Watchlist_Approval_History_ID
                        txtModuleName.Value = "Sipendar Watchlist"
                        txtModuleKey.Value = .ModuleKey
                        'txtModuleField.Hidden = True
                        'txtModuleFieldBefore.Hidden = False
                        txtPK_ModuleAction_ID.Value = .PK_ModuleAction_ID & " - " & GetModuleActionName(.PK_ModuleAction_ID)
                        txtFK_MRole_ID.Value = .FK_MRole_ID & " - " & GetMRoleName(.FK_MRole_ID)
                        Using objdb As New NawaDAL.NawaDataEntities
                            Dim userids As Integer = objWatchlist.UserID
                            Dim username = objdb.MUsers.Where(Function(x) x.PK_MUser_ID = userids).FirstOrDefault
                            If username IsNot Nothing Then
                                txtUserID.Value = username.UserID
                            End If

                        End Using
                        txtStatus.Value = .Status
                        txtNotes.Value = .Notes
                        txtCreatedBy.Value = .CreatedBy
                        txtCreatedDate.Value = .CreatedDate.Value.ToString("dd-MMM-yyyy")
                        PanelRawData.Hidden = False

                        Dim getmodulefield As New SiPendarDAL.SIPENDAR_WATCHLIST_APPROVAL_HISTORY
                        Using objdb As New SiPendarDAL.SiPendarEntities
                            getmodulefield = objdb.SIPENDAR_WATCHLIST_APPROVAL_HISTORY.Where(Function(x) x.PK_Sipendar_Watchlist_Approval_History_ID = objWatchlist.PK_Sipendar_Watchlist_Approval_History_ID).FirstOrDefault()
                        End Using
                        ListUpload = New List(Of SiPendarDAL.SIPENDAR_WATCHLIST)
                        Dim modulefield As String = getmodulefield.ModuleField

                        Dim objupload As List(Of SiPendarDAL.SIPENDAR_WATCHLIST) = NawaBLL.Common.Deserialize(modulefield, GetType(List(Of SiPendarDAL.SIPENDAR_WATCHLIST)))

                        For Each row In objupload
                            Dim dates As DateTime = Nothing
                            Using objdbs As New SiPendarDAL.SiPendarEntities
                                Dim id As Integer = 0

                                id = row.ID_PPATK
                                Dim objawatchlist As SIPENDAR_WATCHLIST = objdbs.SIPENDAR_WATCHLIST.Where(Function(x) x.ID_PPATK = id).FirstOrDefault
                                If objWatchlist IsNot Nothing AndAlso objawatchlist.LastUpdateDate IsNot Nothing Then
                                    dates = objawatchlist.LastUpdateDate
                                Else
                                    dates = DateTime.Now
                                End If
                            End Using

                            ListUpload.Add(New SiPendarDAL.SIPENDAR_WATCHLIST() With {
                         .PK_SIPENDAR_WATCHLIST_ID = row.PK_SIPENDAR_WATCHLIST_ID,
                        .PERIODE = row.PERIODE,
                        .NAMA_ASLI = row.NAMA_ASLI,
                        .PARAMETER_PENCARIAN_NAMA = row.PARAMETER_PENCARIAN_NAMA,
                        .ID_PPATK = row.ID_PPATK,
                        .JENIS_PELAKU = row.JENIS_PELAKU,
                        .KODE_WATCHLIST = row.KODE_WATCHLIST,
                        .TANGGAL_LAHIR = row.TANGGAL_LAHIR,
                        .TEMPAT_LAHIR = row.TEMPAT_LAHIR,
                        .KITAP = row.KITAP,
                        .KITAS = row.KITAS,
                        .KTP = row.KTP,
                        .NPWP = row.NPWP,
                        .PAS = row.PAS,
                        .SIM = row.SIM,
                        .SUKET = row.SUKET,
                        .KIMS = row.KIMS,
                        .Watchlist = row.Watchlist,
                        .CreatedBy = row.CreatedBy,
                        .LastUpdateDate = dates
                        })

                        Next
                        For Each itemupload In ListUpload
                            If itemupload.Watchlist = "True" Then
                                itemupload.Watchlist = itemupload.Watchlist
                            ElseIf itemupload.Watchlist = "False" Then
                                itemupload.Watchlist = itemupload.Watchlist
                            ElseIf itemupload.Watchlist = "1" Then
                                itemupload.Watchlist = "True"
                            ElseIf itemupload.Watchlist = "0" Then
                                itemupload.Watchlist = "False"
                            Else
                                itemupload.Watchlist = Nothing
                            End If
                            Dim asDateTime As DateTime

                            asDateTime = DateTime.Parse(itemupload.PERIODE)


                            Dim toDateStr = asDateTime.ToString("dd-MMM-yyyy")

                            itemupload.PERIODE = toDateStr

                        Next
                        bindWatchlistUpload(StoreWatchlistNew, ListUpload)
                    End If
                End If
            End With
        End If
    End Sub


    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AML_SIPENDAR_WatchlistUpload_ApprovalHistory_Detail_SIPENDAR_WatchlistUpload_ApprovalHistory_Detail_Detail_Init(sender As Object, e As EventArgs) Handles Me.Init
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    Private Sub AML_SIPENDAR_WatchlistUpload_ApprovalHistory_Detail_SIPENDAR_WatchlistUpload_ApprovalHistory_Detail_Detail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        objFormModuleDetail = New NawaBLL.FormModuleDetail(fpMain, Nothing, Nothing, Nothing)
    End Sub

    'Protected Sub StoreListStock_ReadData(sender As Object, e As StoreReadDataEventArgs)
    '    StoreListStock.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM TV_WH_Master_Product as x where x.active = 1", Nothing)
    '    StoreListStock.DataBind()
    'End Sub

    Public Property ListUpload As List(Of SiPendarDAL.SIPENDAR_WATCHLIST)
        Get
            Return Session("Sipendar_Watchlist_ApprovalDetail.ListUpload")
        End Get
        Set(value As List(Of SiPendarDAL.SIPENDAR_WATCHLIST))
            Session("Sipendar_Watchlist_ApprovalDetail.ListUpload") = value
        End Set
    End Property

    Sub bindWatchlistUpload(store As Ext.Net.Store, listUpload As List(Of SiPendarDAL.SIPENDAR_WATCHLIST))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listUpload)
        ''  objtable.Columns.Add(New DataColumn("Kode_Indicator", GetType(String)))
        'For Each row As DataRow In objtable.Rows
        '    row("LastUpdateDate") = CDate(row("LastUpdateDate")).ToString("dd-MM-yyy")
        'Next
        objtable.DefaultView.Sort = "ID_PPATK ASC"

        store.DataSource = objtable
        store.DataBind()
    End Sub

    Protected Function GetModuleActionName(PK_ModuleAction_ID As Integer) As String
        Try
            Dim ActionName As String = ""
            Dim stringquery As String = " select top 1 ModuleActionName from ModuleAction where PK_ModuleAction_ID = " & PK_ModuleAction_ID
            ActionName = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery, Nothing)
            Return ActionName
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Function GetMRoleName(FK_MRole_ID As Integer) As String
        Try
            Dim RoleName As String = ""
            Dim stringquery As String = " select top 1 RoleName from MRole where PK_MRole_ID = " & FK_MRole_ID
            RoleName = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery, Nothing)
            Return RoleName
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
