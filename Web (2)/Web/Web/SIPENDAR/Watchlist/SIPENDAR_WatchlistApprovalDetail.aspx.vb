﻿Imports System.Data
Imports SiPendarDAL
Imports SiPendarBLL
Imports NawaDAL
Imports OfficeOpenXml
Imports System.IO
Imports System.Data.SqlClient
Imports SiPendarBLL.SipendarWatchlistBLL

Partial Class SIPENDAR_Watchlist_SIPENDAR_WatchlistUpload_ApprovalDetail
    Inherits ParentPage
    Public objFormModuleApprovalDetail As NawaBLL.FormModuleApprovalDetail

    Private _IDReq As Long
    Const PATH_TEMP_DIR As String = "~\temp\"

    Public Property IDReq() As Long
        Get
            Return _IDReq
        End Get
        Set(ByVal value As Long)
            _IDReq = value
        End Set
    End Property

    Public Property objSchemaModule() As NawaDAL.Module
        Get
            Return Session("SIPENDAR_WatchlistUpload_ApprovalDetail.objSchemaModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("SIPENDAR_WatchlistUpload_ApprovalDetail.objSchemaModule") = value
        End Set
    End Property

    Private _objApproval As NawaDAL.ModuleApproval

    Public Property ObjApproval() As NawaDAL.ModuleApproval
        Get
            Return _objApproval
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            _objApproval = value
        End Set
    End Property

    Public Property ObjWachlistApproval() As NawaDAL.ModuleApproval
        Get
            Return Session("Sipendar_Watchlist_ApprovalDetail.ObjWachlistApproval")
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            Session("Sipendar_Watchlist_ApprovalDetail.ObjWachlistApproval") = value
        End Set
    End Property

    Private Sub SIPENDAR_WatchlistUpload_ApprovalDetail(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim strid As String = Request.Params("ID")
            Dim strModuleid As String = Request.Params("ModuleID")
            Try
                IDReq = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim moduleid As Integer = NawaBLL.Common.DecryptQueryString(strModuleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                objSchemaModule = NawaBLL.ModuleBLL.GetModuleByModuleID(moduleid)
            Catch ex As Exception
                Throw New Exception("Invalid ID Approval.")
            End Try

            ListUpload = New List(Of SiPendarDAL.SIPENDAR_WATCHLIST)

            '30-Sep-2021 Adi : commented. Pakai parameter saja. Tidak usah pakai session
            'Session("Sipendar_Watchlist_Upload_Notes") = Nothing
            'Dim notes As String = txtApprovalNotes.Value
            'Session("Sipendar_Watchlist_Upload_Notes") = notes

            If Not Ext.Net.X.IsAjaxRequest Then

                ObjApproval = NawaBLL.ModuleApprovalBLL.GetModuleApprovalByID(IDReq)
                If Not ObjApproval Is Nothing Then
                    'PanelInfo.Title = "Sipendar Watchlist Approval"
                    lblModuleName.Text = "Sipendar Watchlist"
                    lblModuleKey.Text = ObjApproval.ModuleKey
                    lblAction.Text = NawaBLL.ModuleBLL.GetModuleActionNamebyID(ObjApproval.PK_ModuleAction_ID)
                    LblCreatedBy.Text = ObjApproval.CreatedBy
                    lblCreatedDate.Text = ObjApproval.CreatedDate.Value.ToString("dd-MMM-yyyy")
                End If
                Using objdb As New SiPendarEntities
                    If ObjApproval.PK_ModuleAction_ID = 7 AndAlso ObjApproval.ModuleKey IsNot Nothing Then
                        'Dim getmodulefield = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = IDReq).FirstOrDefault()
                        'Dim modulefield As String = getmodulefield.ModuleField

                        'Dim objupload As List(Of SiPendarDAL.SIPENDAR_WATCHLIST) = NawaBLL.Common.Deserialize(modulefield, GetType(List(Of SiPendarDAL.SIPENDAR_WATCHLIST)))

                        'For Each row In objupload

                        '    ListUpload.Add(New SiPendarDAL.SIPENDAR_WATCHLIST() With {
                        ' .PK_SIPENDAR_WATCHLIST_ID = row.PK_SIPENDAR_WATCHLIST_ID,
                        '.PERIODE = row.PERIODE,
                        '.NAMA_ASLI = row.NAMA_ASLI,
                        '.PARAMETER_PENCARIAN_NAMA = row.PARAMETER_PENCARIAN_NAMA,
                        '.ID_PPATK = row.ID_PPATK,
                        '.JENIS_PELAKU = row.JENIS_PELAKU,
                        '.KODE_WATCHLIST = row.KODE_WATCHLIST,
                        '.TANGGAL_LAHIR = row.TANGGAL_LAHIR,
                        '.TEMPAT_LAHIR = .TEMPAT_LAHIR,
                        '.KITAP = row.KITAP,
                        '.KITAS = row.KITAS,
                        '.KTP = row.KTP,
                        '.NPWP = row.NPWP,
                        '.PAS = row.PAS,
                        '.SIM = row.SIM,
                        '.SUKET = row.SUKET
                        '})

                        'Next

                        'bindWatchlistUpload(StoreWatchlistNew, ListUpload)

                        '30-Sep-2021 Adi : Bind langsung dari table SIPENDAR_WATCHLIST_Upload_Approval
                        'Dim strQuery As String = "SELECT * FROM SIPENDAR_WATCHLIST_Upload_Approval WHERE FK_ModuleApproval_ID=" & ObjApproval.PK_ModuleApproval_ID & " order by ID_PPATK asc"
                        'Dim dtUploadApproval As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        'If dtUploadApproval IsNot Nothing Then

                        '    For Each row As DataRow In dtUploadApproval.Rows
                        '        Using objdbs As New SiPendarDAL.SiPendarEntities
                        '            Dim id As Integer = 0
                        '            id = Convert.ToInt32(row.Item("ID_PPATK"))
                        '            Dim objawatchlist As SIPENDAR_WATCHLIST = objdbs.SIPENDAR_WATCHLIST.Where(Function(x) x.ID_PPATK = id).FirstOrDefault
                        '            If objawatchlist IsNot Nothing Then
                        '                Dim dates As String = ""
                        '                If objawatchlist.LastUpdateDate IsNot Nothing Then
                        '                    dates = objawatchlist.LastUpdateDate.Value.ToString("dd-MMM-yyyy")
                        '                End If


                        '                If String.IsNullOrEmpty(dates) Or dates Is Nothing Then
                        '                    row.Item("CreatedBy") = objawatchlist.CreatedBy
                        '                    row.Item("LastUpdateDate") = DateTime.Now.ToString("dd-MMM-yyyy")
                        '                Else
                        '                    row.Item("CreatedBy") = objawatchlist.CreatedBy
                        '                    row.Item("LastUpdateDate") = dates
                        '                End If
                        '            Else
                        '                Dim userids As String = ""
                        '                Dim modulename = "SIPENDAR_WATCHLIST"

                        '                Using objdbuser As New NawaDAL.NawaDataEntities
                        '                    Dim userid = objdbuser.ModuleApprovals.Where(Function(x) x.ModuleName = modulename And x.PK_ModuleApproval_ID = IDReq).FirstOrDefault
                        '                    userids = userid.CreatedBy.ToString
                        '                End Using
                        '                row.Item("CreatedBy") = userids
                        '                row.Item("LastUpdateDate") = DateTime.Now.ToString("dd-MMM-yyyy")

                        '            End If
                        '        End Using

                        '    Next
                        '    'GridPanelWatchlistNew.GetStore.DataSource = dtUploadApproval
                        '    'GridPanelWatchlistNew.GetStore.DataBind()
                        'End If

                        'GridPanelWatchlistOld.Visible = False

                        Dim stringquery As String = ""
                        'SIPENDAR_WATCHLIST_Upload_Detail (RawData)
                        PanelRawData.Title = "Sipendar Watchlist Upload Raw Data"
                        stringquery = " select * from SIPENDAR_WATCHLIST_Upload_Detail where FK_SIPENDAR_WATCHLIST_Upload_Header_ID = " & ObjApproval.ModuleKey
                        StoreWatchlistNew.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
                        StoreWatchlistNew.DataBind()
                        PanelRawData.Hidden = False

                        'SIPENDAR_WATCHLIST_Upload_Detail_Cleansing
                        stringquery = " select "
                        stringquery += " case when olddata.ID_PPATK is null then 'Insert' "
                        stringquery += " else 'Update' "
                        stringquery += " end as nawaaction, "
                        stringquery += " cleansing.*  "
                        stringquery += " from SIPENDAR_WATCHLIST_Upload_Detail_Cleansing cleansing "
                        stringquery += " left join SIPENDAR_WATCHLIST_Upload_Detail_OldData olddata "
                        stringquery += " on olddata.FK_SIPENDAR_WATCHLIST_Upload_Header_ID = cleansing.FK_SIPENDAR_WATCHLIST_Upload_Header_ID "
                        stringquery += " and olddata.ID_PPATK = cleansing.ID_PPATK "
                        stringquery += " where cleansing.FK_SIPENDAR_WATCHLIST_Upload_Header_ID = " & ObjApproval.ModuleKey
                        StoreWatchlistCleansing.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
                        StoreWatchlistCleansing.DataBind()
                        PanelCleanedData.Hidden = False

                        'SIPENDAR_WATCHLIST_Upload_Detail_OldData
                        stringquery = " select * from SIPENDAR_WATCHLIST_Upload_Detail_OldData where FK_SIPENDAR_WATCHLIST_Upload_Header_ID = " & ObjApproval.ModuleKey
                        Dim datatabeloldwatchlist As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringquery)
                        If datatabeloldwatchlist IsNot Nothing AndAlso datatabeloldwatchlist.Rows.Count > 0 Then
                            StoreWatchlistOld.DataSource = datatabeloldwatchlist
                            StoreWatchlistOld.DataBind()
                            PanelOldData.Hidden = False
                        End If
                    Else
                        'GridPanelWatchlistNew.Visible = False
                        'GridPanelWatchlistOld.Visible = False
                        Select Case ObjApproval.PK_ModuleAction_ID
                            Case NawaBLL.Common.ModuleActionEnum.Insert
                                FormPanelNew.Hidden = False
                                Dim unikkey As String = Guid.NewGuid.ToString
                                If Not String.IsNullOrEmpty(ObjApproval.ModuleField) Then
                                    SiPendarBLL.SipendarWatchlistBLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkey)
                                End If
                                FormPanelOld.Visible = False
                            Case NawaBLL.Common.ModuleActionEnum.Delete
                                FormPanelNew.Hidden = False
                                Dim unikkeyNew As String = Guid.NewGuid.ToString
                                If Not String.IsNullOrEmpty(ObjApproval.ModuleField) Then
                                    SiPendarBLL.SipendarWatchlistBLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkeyNew)
                                End If
                                FormPanelOld.Visible = False
                            Case NawaBLL.Common.ModuleActionEnum.Update
                                FormPanelNew.Hidden = False
                                FormPanelOld.Hidden = False
                                Dim unikkeyOld As String = Guid.NewGuid.ToString
                                Dim unikkeyNew As String = Guid.NewGuid.ToString
                                If Not String.IsNullOrEmpty(ObjApproval.ModuleField) Then
                                    SiPendarBLL.SipendarWatchlistBLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkeyNew)
                                End If
                                If Not String.IsNullOrEmpty(ObjApproval.ModuleFieldBefore) Then
                                    SiPendarBLL.SipendarWatchlistBLL.LoadPanel(FormPanelOld, ObjApproval.ModuleFieldBefore, ObjApproval.ModuleName, unikkeyOld)
                                End If
                                SettingColor(FormPanelOld, FormPanelNew, unikkeyOld, unikkeyNew)
                            Case NawaBLL.Common.ModuleActionEnum.Activation
                                Dim unikkeyNew As String = Guid.NewGuid.ToString
                        End Select
                    End If
                End Using
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSave_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim notes As String = txtApprovalNotes.Value
            If txtApprovalNotes.Value Is Nothing Then
                Throw New ApplicationException("Notes Harus Diisi")
            End If

            SiPendarBLL.SipendarWatchlistBLL.Accept(Me.IDReq, notes)
            LblConfirmation.Text = "Data Approved. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnReject_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim notes As String = txtApprovalNotes.Value
            If txtApprovalNotes.Value Is Nothing Then
                Throw New ApplicationException("Notes Harus Diisi")
            End If

            SiPendarBLL.SipendarWatchlistBLL.Reject(Me.IDReq, notes)
            LblConfirmation.Text = "Data Rejected. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objSchemaModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs) Handles BtnConfirmation.DirectClick
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objSchemaModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Shared Function GetObjApproval(objApproval As NawaDAL.ModuleApproval) As NawaDAL.ModuleApproval
        Return objApproval
    End Function




    Public Property ListUpload As List(Of SiPendarDAL.SIPENDAR_WATCHLIST)
        Get
            Return Session("Sipendar_Watchlist_ApprovalDetail.ListUpload")
        End Get
        Set(value As List(Of SiPendarDAL.SIPENDAR_WATCHLIST))
            Session("Sipendar_Watchlist_ApprovalDetail.ListUpload") = value
        End Set
    End Property

    Sub bindWatchlistUpload(store As Ext.Net.Store, listUpload As List(Of SiPendarDAL.SIPENDAR_WATCHLIST))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listUpload)
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Private Sub SIPENDAR_Watchlist_SIPENDAR_WatchlistUpload_ApprovalDetail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        objFormModuleApprovalDetail = New NawaBLL.FormModuleApprovalDetail()
    End Sub

    Private Sub SIPENDAR_Watchlist_SIPENDAR_WatchlistUpload_ApprovalDetail_Init(sender As Object, e As EventArgs) Handles Me.Init
        ActionType = NawaBLL.Common.ModuleActionEnum.Approval
    End Sub


End Class