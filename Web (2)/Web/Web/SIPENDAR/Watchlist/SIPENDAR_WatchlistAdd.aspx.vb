﻿
Imports System.Data
Imports System.Data.Entity
Imports System.Data.SqlClient

Partial Class SIPENDAR_WatchlistAdd
    Inherits System.Web.UI.Page

    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("SIPENDAR_WatchlistAdd.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("SIPENDAR_WatchlistAdd.ObjModule") = value
        End Set
    End Property

    Public Property ErrorValidation() As String
        Get
            Return Session("SIPENDAR_WatchlistAdd.ErrorValidation")
        End Get
        Set(ByVal value As String)
            Session("SIPENDAR_WatchlistAdd.ErrorValidation") = value
        End Set
    End Property

    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            ErrorValidation = ""

            If PERIODEField.SelectedDate = DateTime.MinValue Then
                ErrorValidation = ErrorValidation & "<br> " & PERIODEField.FieldLabel & " is Mandatory. "
            Else
                ValidateDataDate(PERIODEField.Text, PERIODEField.FieldLabel)
            End If

            If String.IsNullOrEmpty(KODE_WATCHLISTField.Text) Then
                ErrorValidation = ErrorValidation & "<br> " & KODE_WATCHLISTField.FieldLabel & " is Mandatory. "
            End If

            If String.IsNullOrEmpty(JENIS_PELAKUField.Text) Then
                ErrorValidation = ErrorValidation & "<br> " & JENIS_PELAKUField.FieldLabel & " is Mandatory. "
            End If

            If String.IsNullOrEmpty(NAMA_ASLIField.Text) Then
                ErrorValidation = ErrorValidation & "<br> " & NAMA_ASLIField.FieldLabel & " is Mandatory. "
            End If

            If String.IsNullOrEmpty(PARAMETER_PENCARIAN_NAMAField.Text) Then
                ErrorValidation = ErrorValidation & "<br> " & PARAMETER_PENCARIAN_NAMAField.FieldLabel & " is Mandatory. "
            End If

            If String.IsNullOrEmpty(ID_PPATKField.Text) Then
                ErrorValidation = ErrorValidation & "<br> " & ID_PPATKField.FieldLabel & " is Mandatory. "
            Else
                ValidateDataNumberic(ID_PPATKField.Text, ID_PPATKField.FieldLabel)
            End If

            If String.IsNullOrEmpty(cmb_SUBMISSION_TYPE.SelectedItemValue) Then
                ErrorValidation = ErrorValidation & "<br> " & cmb_SUBMISSION_TYPE.Label & " is Mandatory. "
            End If

            If Not String.IsNullOrEmpty(ID_PPATKField.Text) AndAlso Not String.IsNullOrEmpty(cmb_SUBMISSION_TYPE.SelectedItemValue) Then
                ValidateIDPPATKInApproval(ID_PPATKField.Text, cmb_SUBMISSION_TYPE.SelectedItemValue)
            End If

            If String.IsNullOrEmpty(NPWPField.Text) Then
                ValidateDataNumberic(NPWPField.Text, NPWPField.FieldLabel)
            End If

            If String.IsNullOrEmpty(KTPField.Text) Then
                ValidateDataNumberic(KTPField.Text, KTPField.FieldLabel)
            End If

            'If String.IsNullOrEmpty(PASField.Text) Then
            '    ValidateDataNumberic(PASField.Text, PASField.FieldLabel)
            'End If

            If String.IsNullOrEmpty(KITASField.Text) Then
                ValidateDataNumberic(KITASField.Text, KITASField.FieldLabel)
            End If

            If String.IsNullOrEmpty(SUKETField.Text) Then
                ValidateDataNumberic(SUKETField.Text, SUKETField.FieldLabel)
            End If

            If String.IsNullOrEmpty(SIMField.Text) Then
                ValidateDataNumberic(SIMField.Text, SIMField.FieldLabel)
            End If

            If String.IsNullOrEmpty(KITAPField.Text) Then
                ValidateDataNumberic(KITAPField.Text, KITAPField.FieldLabel)
            End If

            If ErrorValidation <> "" Then
                SiPendarBLL.NawaFramework.extInfoPanelupdate(FormPanelInput, ErrorValidation, "infoValidationResultPanel")
                FormPanelInput.Body.ScrollTo(Direction.Top, 0)
                infoValidationResultPanel.Hidden = False
            Else
                Dim objwatchlist As New SiPendarDAL.SIPENDAR_WATCHLIST
                With objwatchlist
                    .PERIODE = PERIODEField.SelectedDate.ToString("yyyy-MM-dd")
                    .ID_PPATK = ID_PPATKField.Value
                    .KODE_WATCHLIST = KODE_WATCHLISTField.Value
                    .JENIS_PELAKU = JENIS_PELAKUField.Value
                    .NAMA_ASLI = NAMA_ASLIField.Value
                    .PARAMETER_PENCARIAN_NAMA = PARAMETER_PENCARIAN_NAMAField.Value
                    .TEMPAT_LAHIR = TEMPAT_LAHIRField.Value
                    If TANGGAL_LAHIRField.SelectedDate <> DateTime.MinValue Then
                        .TANGGAL_LAHIR = TANGGAL_LAHIRField.SelectedDate.ToString("yyyy-MM-dd")
                    End If
                    If NPWPField.Value IsNot Nothing Then
                        .NPWP = NPWPField.Value
                    End If
                    If KTPField.Value IsNot Nothing Then
                        .KTP = KTPField.Value
                    End If
                    If PASField.Value IsNot Nothing Then
                        .PAS = PASField.Value
                    End If
                    If KITASField.Value IsNot Nothing Then
                        .KITAS = KITASField.Value
                    End If
                    If SUKETField.Value IsNot Nothing Then
                        .SUKET = SUKETField.Value
                    End If
                    If SIMField.Value IsNot Nothing Then
                        .SIM = SIMField.Value
                    End If
                    If KITAPField.Value IsNot Nothing Then
                        .KITAP = KITAPField.Value
                    End If

                    'Ari 25 Oct 2021 Menambah KMIS dan Watchlist
                    If KIMSField.Value IsNot Nothing Then
                        .KIMS = KIMSField.Value
                    End If
                    If chk_IsWatchlist.Checked Then
                        .Watchlist = "True"
                    Else
                        .Watchlist = "False"
                    End If
                    .Submission_Type = cmb_SUBMISSION_TYPE.SelectedItemValue
                    'If NawaBLL.Common.SessionAlternateUser.UserID IsNot Nothing Then
                    '    .Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    'End If
                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .CreatedDate = DateTime.Now
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateDate = DateTime.Now
                End With
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not ObjModule.IsUseApproval Then
                    SaveDataWithoutApproval(objwatchlist)
                    Panelconfirmation.Hidden = False
                    FormPanelInput.Hidden = True
                    LblConfirmation.Text = "Data Save into Database ."
                Else
                    SaveDataWithApproval(objwatchlist)
                    Panelconfirmation.Hidden = False
                    FormPanelInput.Hidden = True
                    LblConfirmation.Text = "Data Saved into Pending Approval"
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Moduleid = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub SIPENDAR_WatchlistAdd_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer

            intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Insert) Then
                Dim strIDCode As String = 1
                strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                Exit Sub
            End If
            If Not Ext.Net.X.IsAjaxRequest Then

                'Session("ComboBox" + cmb_SUBMISSION_TYPE.ID + ".StringValue") = Nothing
                cmb_SUBMISSION_TYPE.SetTextValue("")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveDataWithApproval(objwatchlist As SiPendarDAL.SIPENDAR_WATCHLIST)
        Using objdb As New SiPendarDAL.SiPendarEntities
            Using objtrans As DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    If objwatchlist IsNot Nothing Then
                        Dim dataXML As String = NawaBLL.Common.Serialize(objwatchlist)
                        Dim objModuleApproval As New SiPendarDAL.ModuleApproval
                        With objModuleApproval
                            .ModuleName = ObjModule.ModuleName
                            .ModuleKey = objwatchlist.ID_PPATK
                            .ModuleField = dataXML
                            .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                        End With
                        objdb.Entry(objModuleApproval).State = EntityState.Added
                        objdb.SaveChanges()

                        Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                        Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                        Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                        Dim modulename As String = ObjModule.ModuleLabel
                        Dim objATHeader As SiPendarDAL.AuditTrailHeader = SiPendarBLL.NawaFramework.CreateAuditTrail(objdb, user, act, acts, modulename)
                        SiPendarBLL.NawaFramework.CreateAuditTrailDetailAdd(objdb, objATHeader.PK_AuditTrail_ID, objModuleApproval)
                        objdb.SaveChanges()
                    End If
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Private Sub SaveDataWithoutApproval(objwatchlist As SiPendarDAL.SIPENDAR_WATCHLIST)
        Using objdb As New SiPendarDAL.SiPendarEntities
            Using objtrans As DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    If objwatchlist IsNot Nothing Then
                        objdb.Entry(objwatchlist).State = EntityState.Added
                        objdb.SaveChanges()

                        Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                        Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                        Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                        Dim modulename As String = ObjModule.ModuleLabel
                        Dim ObjAuditTrailHeader As SiPendarDAL.AuditTrailHeader = SiPendarBLL.NawaFramework.CreateAuditTrail(objdb, user, act, acts, modulename)
                        SiPendarBLL.NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, objwatchlist)
                        objdb.SaveChanges()
                    End If
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using

        '21-June-2022 Riyan sync sipendar watchlist to AML Watchlist
        Dim objParamAPI(0) As SqlParameter
        objParamAPI(0) = New SqlParameter
        objParamAPI(0).ParameterName = "@UserID"
        objParamAPI(0).Value = NawaBLL.Common.SessionCurrentUser.UserID

        NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_WATCHLIST_AfterSave", objParamAPI)
        'End sync sipendar watchlist to AML Watchlist

    End Sub

    Private Sub ValidateDataNumberic(data As String, objname As String)
        Try
            If Not (data.All(AddressOf Char.IsDigit)) Then
                ErrorValidation = ErrorValidation & "<br> " & objname & " Must Numberic. "
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ValidateDataDate(data As String, objname As String)
        Try
            Dim dtemp As Date = CDate(data)
        Catch ex As Exception
            ErrorValidation = ErrorValidation & "<br> " & objname & " Value Not a Date. "
        End Try
    End Sub

    Private Sub ValidateIDPPATKInApproval(ID_PPATK As String, Submission_Type As String)
        Dim strQuery As String = "select top 1 ID_PPATK from SIPENDAR_WATCHLIST_Upload_approval where ID_PPATK = '" & ID_PPATK & "' and Submission_Type = '" & Submission_Type & "'"
        Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        If drResult IsNot Nothing Then
            ErrorValidation = ErrorValidation & "<br> Data With ID PPATK " & ID_PPATK & " and Submission Type " & Submission_Type & " Already Exist in Approval Upload. "
        End If

        strQuery = "select top 1 PK_ModuleApproval_ID  from ModuleApproval"
        strQuery = strQuery & " where ModuleName = '" & ObjModule.ModuleName & "'"
        strQuery = strQuery & " and ('" & Submission_Type & "-" & ID_PPATK & "') = ModuleKey"
        strQuery = strQuery & " and PK_ModuleAction_ID <> 7"
        drResult = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        If drResult IsNot Nothing Then
            ErrorValidation = ErrorValidation & "<br> Data With ID PPATK " & ID_PPATK & " and Submission Type " & Submission_Type & " Already Exist in Approval. "
        End If
        strQuery = "select top 1 PK_SIPENDAR_WATCHLIST_ID from SIPENDAR_WATCHLIST"
        strQuery = strQuery & " where ID_PPATK = '" & ID_PPATK & "'"
        strQuery = strQuery & " and Submission_Type = '" & Submission_Type & "'"
        drResult = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        If drResult IsNot Nothing Then
            ErrorValidation = ErrorValidation & "<br> Data With ID PPATK " & ID_PPATK & " and Submission Type " & Submission_Type & " Already Exist. "
        End If
    End Sub

End Class
