﻿<%@ page title="" language="vb" autoeventwireup="false" masterpagefile="~/Site1.Master" inherits="ParameterUpload, App_Web_parameterupload.aspx.252c98" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
         var uncheckAll = function () {

        };

        var renameFormPanelInput = function (formpanelinput, btnImport, btnImportWithoutValidate) {

            formpanelinput.setTitle(formpanelinput.title.replace("Import", "Upload"));

            btnImport.setText(btnImport.text.replace("Import", "Upload"));

            if (btnImportWithoutValidate) {
                btnImportWithoutValidate.setText(btnImportWithoutValidate.text.replace("Import", "Upload"));
            }
        }

        var renameFormPanelSave = function (formpanelsave) {
            formpanelsave.setTitle(formpanelsave.title.replace("Import", "Upload"));
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <ext:Window ID="WindowUploadFilter" runat="server" Icon="ApplicationViewDetail" ButtonAlign="Center" Title="Upload Filter" Hidden="true" Layout="FitLayout" Modal="true">

        <Items>

            <ext:FormPanel ID="FormPanelUploadFIlter" runat="server" Padding="5" ButtonAlign="Center" AnchorHorizontal="100%" Hidden="false" AutoScroll="true">

                <Items>
                    <ext:HyperlinkButton ID="hyperLinkBtnExportTemplate" Text="Export Template" runat="server">
                        <DirectEvents>
                            <Click OnEvent="exportTemplate_DirectClick">
                            </Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>

                    <ext:FileUploadField ID="FileUploadField2" runat="server" FieldLabel="Input File For Filter">
                    </ext:FileUploadField>

                    <%--Coba Multi Selector--%>
                    <ext:MultiSelector
                        runat="server"
                        ID="Selector1"
                        Width="400"
                        Height="300"
                        Title="Selected Fields"
                        ClientIDMode="Static"
                        FieldName="FieldLabel">
                        <Store>
                            <ext:Store ID="StoreFiltered" runat="server">
                                <Model>
                                    <ext:Model ID="ModelData" runat="server" IDProperty="FieldName">
                                        <Fields>
                                            <ext:ModelField Name="FieldName" Type="string"></ext:ModelField>
                                            <ext:ModelField Name="FieldLabel" Type="string"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ViewConfig DeferEmptyText="false" EmptyText="No field selected" />
                        <Search>
                            <ext:MultiSelectorSearch ID="multiSelectorSearch" runat="server" Field="FieldLabel" ClientIDMode="Static">

                                <SearchGridConfig runat="server" BufferedRenderer="false" />
                                <Store>
                                    <ext:Store ID="StoreSource" runat="server">
                                        <Model>
                                            <ext:Model ID="ModelSource" runat="server" IDProperty="FieldName">
                                                <Fields>
                                                    <ext:ModelField Name="FieldName" Type="string"></ext:ModelField>
                                                    <ext:ModelField Name="FieldLabel" Type="string"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Sorters>
                                            <ext:DataSorter Property="FieldLabel" />
                                        </Sorters>
                                    </ext:Store>
                                </Store>
                            </ext:MultiSelectorSearch>
                        </Search>

                        <%--<Buttons>
                                    <ext:Button runat="server" Text="Show value" OnDirectClick="OnClick" />
                                </Buttons>--%>
                    </ext:MultiSelector>
                </Items>

                <Buttons>
                    <ext:Button ID="btnExportFilter" runat="server" Icon="Magnifier" Text="Export Filtered">

                        <DirectEvents>

                            <Click OnEvent="btnExportFilterWithUpload_DirectEvent">
                                <EventMask Msg="Loading..." MinDelay="500" ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.8});" />

            <Resize Handler="#{WindowUploadFilter}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Panel ID="Panel1" runat="server" Hidden="true">
        <Content>
            <uc1:AdvancedFilter runat="server" ID="AdvancedFilter1" IsUpload="true" OnSubmitFilterClicked="AdvancedFilter1_SubmitFilterClicked" />
        </Content>
        <Items>
        </Items>
    </ext:Panel>

    <ext:FormPanel ID="FormPanelInput" runat="server" ButtonAlign="Center" Title="Title" Layout="formLayout" Hidden="false">
        <Listeners>
            <BeforeRender Handler="renameFormPanelInput(#{FormPanelInput},#{BtnSave},#{btnImportWithoutValidate})" />
        </Listeners>
        <DockedItems>
            <ext:Toolbar runat="server">
                <Items>
                    <ext:HyperlinkButton ID="exportDataTemplate" Text="Export Data and Template" runat="server">
                        <Menu>
                            <ext:Menu runat="server">
                                <Items>
                                    <ext:MenuItem runat="server" Text="Export Without Filter" Icon="DiskDownload">
                                        <DirectEvents>
                                            <Click OnEvent="exportDataTemplate_DirectClick">
                                            </Click>
                                        </DirectEvents>
                                    </ext:MenuItem>
                                    <ext:MenuItem runat="server" Text="Export With Filter" Icon="DiskMagnify">
                                        <DirectEvents>
                                            <Click OnEvent="exportDataTemplateWithFilter_DirectClick">
                                            </Click>
                                        </DirectEvents>
                                    </ext:MenuItem>
                                    <ext:MenuItem runat="server" Text="Export With Upload Filter" Icon="DiskBlackMagnify">
                                        <DirectEvents>
                                            <Click OnEvent="exportDataTemplateWithUploadFilter_DirectClick">
                                            </Click>
                                        </DirectEvents>
                                    </ext:MenuItem>
                                </Items>
                            </ext:Menu>
                        </Menu>
                    </ext:HyperlinkButton>
                    <ext:HyperlinkButton ID="exportTemplate" Text="Export Template" runat="server">

                        <DirectEvents>
                            <Click OnEvent="exportTemplate_DirectClick">
                            </Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                </Items>
            </ext:Toolbar>

            <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true" Dock="Top" Hidden="true">
                <Items>
                    <ext:HyperlinkButton ID="btnClear" runat="server" Text="Clear Advanced Filter">
                        <DirectEvents>
                            <Click OnEvent="btnClear_Click"></Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                    <ext:Label ID="LblAdvancedFilter" runat="server" Text="">
                    </ext:Label>
                </Items>
            </ext:Toolbar>
        </DockedItems>
        <Items>
            <ext:FileUploadField ID="FileUploadField1" runat="server" FieldLabel="Input File ">
            </ext:FileUploadField>
            <ext:ComboBox ID="CboMode" runat="server" FieldLabel="Import Mode" ForceSelection="True" AllowBlank="false" BlankText="Please Select Import Mode">

                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Select Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:ComboBox>
        </Items>
        <Buttons>
            <ext:Button ID="BtnSave" runat="server" Icon="Disk" Text="Import">

                <DirectEvents>

                    <Click IsUpload="true"
                        OnEvent="BtnSave_DirectClick"
                        Before="if (!#{FormPanelInput}.getForm().isValid()) { return false; }
                                "
                        Failure="Ext.Msg.show({
                                title   : 'Error',
                                msg     : 'Error during uploading',
                                minWidth: 200,
                                modal   : true,
                                icon    : Ext.Msg.ERROR,
                                buttons : Ext.Msg.OK
                            });">
                        <EventMask ShowMask="true" Msg="Uploading File...."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnImportWithoutValidate" runat="server" Icon="Disk" Text="Import Without Validate">
                <DirectEvents>
                    <Click IsUpload="true"
                        OnEvent="BtnSaveWithoutValidate_DirectClick"
                        Before="if (!#{FormPanelInput}.getForm().isValid()) { return false; }
                                "
                        Failure="Ext.Msg.show({
                                title   : 'Error',
                                msg     : 'Error during uploading',
                                minWidth: 200,
                                modal   : true,
                                icon    : Ext.Msg.ERROR,
                                buttons : Ext.Msg.OK
                            });">
                        <EventMask ShowMask="true" Msg="Uploading File...."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="FormPanelsave" runat="server" ButtonAlign="Center" Title="Title" Hidden="true" AutoScroll="true">

        <Items>
            <ext:DisplayField ID="txtMode" runat="server" FieldLabel="Upload Mode"></ext:DisplayField>

            <ext:GridPanel ID="GridPanelValid" runat="server" Title="" Height="400" AutoScroll="true">
                <Listeners>
                    <ViewReady Handler="renameFormPanelSave(#{FormPanelsave})" />
                </Listeners>
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="StoreViewValid" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="StoreValid_ReadData" IsPagingStore="true">

                        <Sorters>
                        </Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:GridFilters ID="GridFiltersValid" runat="server" />
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbarValid" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

            <ext:GridPanel ID="GridPanelInValid" runat="server" Title="" Height="400" AutoScroll="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                        <Items>
                            <%--<ext:Button runat="server" ID="BtnExport" Text="Export Invalid Data" AutoPostBack="true" OnClick="ExportInvalidData" ClientIDMode="Static" />--%>
                            <ext:Button runat="server" ID="BtnExport" Text="Export Invalid Data"   ClientIDMode="Static">
                                <DirectEvents>
                                    <Click OnEvent="ExportInvalidData" IsUpload="true"></Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="StoreInvalid" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="StoreInValid_ReadData" IsPagingStore="true">
                        <Sorters>
                        </Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:GridFilters ID="GridFiltersInValid" runat="server" />

                    <ext:RowExpander ID="RowExpander1" runat="server">

                        <Template ID="Template1" runat="server">
                            <Html>
                                <p><br>Error List:</br> {KeteranganError}</p>
                                <br />
                            </Html>
                        </Template>
                    </ext:RowExpander>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbarInValid" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

            <%-- <ext:Panel ID="Panel1" runat="server" Layout="FitLayout" Height="200">
                <Items>
                    <ext:TextArea ID="txtErrorupload" runat="server" FieldLabel="Validation Result" AutoScroll="true">
                    </ext:TextArea>
                </Items>
            </ext:Panel>--%>
        </Items>

        <Buttons>
            <ext:Button ID="btnSaveUpload" runat="server" Text="Save">

                <DirectEvents>
                    <Click OnEvent="btnSaveUpload_Click">
                        <EventMask Msg="Loading.." MinDelay="500" ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancelUpload" runat="server" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btnCancelUpload_Click">
                        <EventMask Msg="Loading..." MinDelay="500" ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>

        <Buttons>

            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>