﻿<%@ page title="" language="vb" autoeventwireup="false" masterpagefile="~/Site1.Master" inherits="BackgroundProcessView, App_Web_backgroundprocessview.aspx.252c98" %>

<%--<%@ Register Assembly="CodeEffects.Rule" Namespace="CodeEffects.Rule.Asp" TagPrefix="cc1" %>--%>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <ext:ResourcePlaceHolder runat="server" Mode="ScriptFiles" />
    <script type="text/javascript">

        var fnRowClass = function (record, rowIndex, c, store) {
            if ((record.store.data.length - 1) == rowIndex) {
                return 'NawaSummaryLabel'
            }
        }

        var summaryrenderboolean = function (value, metaData, record, rowIndex, colIndex, store) {
            if (store.data.length - 1 == rowIndex) {

            } else {
                return value
            }
        }

        var prepareCommandCollectionHideButton = function (grid, toolbar, rowIndex, record) {

            var detailButton = toolbar.items.items[0];
            //console.log(record.data.FK_ProcessType_ID);

            //20220620 DevOps ID 3587 [NF2] Background Process User Matrix - Export Result -> penambahan validasi jika FK_ProcessType_ID NULL
            if (record.data.FK_ProcessType_ID != null) {
                var processType = record.data.FK_ProcessType_ID.split("-")[0].trim();
                if (processType == "1" || processType == "2") detailButton.setText("Detail");
                else if (processType == "3" || processType == "4") detailButton.setHidden(true);
                else if (processType == "5" || processType == "6" || processType == "7" || processType == "50" || processType == "51" || processType == "52") detailButton.setText("Download");
                else detailButton.setText("Result");

                if (record.data.FK_Status_ID.split("-")[0].trim() != "3" || processType == "3" || processType == "4") {
                    detailButton.setDisabled(true);
                };

                if (toolbar.items.items.length > 1) {
                    var cancelButton = toolbar.items.items[1];
                    //console.log(parseInt(record.data.FK_Status_ID.split("-")[0].trim()));
                    //if (record.data.FK_Status_ID.split("-")[0].trim() != "1") {
                    //    cancelButton.setDisabled(true);
                    //};

                    //if (parseInt(record.data.FK_Status_ID.split("-")[0].trim()) > parseInt(2)) {
                    //    cancelButton.setDisabled(true);
                    //};

                    //20220926 DevOps ID 4046 Export All Excel Report Query to Background Process
                    var valueStrStatusDisabled = App.txtStatusDisabled.getValue();
                    //console.log(parseInt(record.data.FK_Status_ID.split("-")[0].trim()));
                    //console.log(parseInt(valueStrStatusDisabled));
                    if (parseInt(record.data.FK_Status_ID.split("-")[0].trim()) > parseInt(valueStrStatusDisabled)) {
                        cancelButton.setDisabled(true);
                    };

                };

                //20220620 DevOps ID 3587 [NF2] Background Process User Matrix - Export Result
                var valueStrCurrentuser = App.txtCurrentUser.getValue();
                var executeBy = record.data.ExecuteBy;
                if (executeBy != null) {
                    var valueStrCurrentuserUpper = valueStrCurrentuser.toUpperCase();
                    var executeByUpper = executeBy.toUpperCase();
                    //console.log(valueStrCurrentuserUpper);
                    //console.log(executeByUpper);
                    if (processType == "5" || processType == "6" || processType == "7" || processType == "50" || processType == "51" || processType == "52") {
                        if (valueStrCurrentuserUpper != executeByUpper) {
                            detailButton.setDisabled(true);
                        };
                    };
                }
                else {
                    if (processType == "5" || processType == "6" || processType == "7" || processType == "50" || processType == "51" || processType == "52") {
                        detailButton.setDisabled(true);
                    };
                };
                //End 20220620 DevOps ID 3587 [NF2] Background Process User Matrix - Export Result

            };
            //End 20220620 DevOps ID 3587 [NF2] Background Process User Matrix - Export Result -> penambahan validasi jika FK_ProcessType_ID NULL

            


        }


        var prepareCommandCollection = function (grid, toolbar, rowIndex, record) {

            prepareCommandCollectionHideButton(grid, toolbar, rowIndex, record)

            toolbar.add({ xtype: "displayfield", fieldCls: 'NawaLabel' })

            if (rowIndex == grid.store.data.length - 1) {

                for (i = 0; i < toolbar.items.length; i++) {
                    if (i != toolbar.items.length - 1) {
                        var toolbartemp = toolbar.items.get(i)
                        toolbartemp.setHidden(true);
                    } else {
                        var toolbartemp = toolbar.items.get(i)
                        toolbartemp.setValue(" TOTAL");
                    }
                }
            }
        }

        var columnAutoResize = function (grid) {

            grid.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {
                    col.autoSize();
                }
            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };

        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };

        var filterSession = function (objSession) {
            window.App.GridpanelView.filterHeader.setValue({
                PK_BackgroundProcess_ID: '' + objSession.PK_BackgroundProcess_ID + ''
            });
        };

        function getColumnValue() {
            var value = this.getRawValue();

            //console.log(value[0])

            //console.log(Ext.net.FilterHeader.behaviour.getBehaviour("string", value))
            if (value && !(value[0] == "*" || value[0] == "=" || value[0] == "+")) {

                //if(Ext.net.FilterHeader.behaviour.getBehaviour("string", value)){console.log('ini itu string')}else{console.log('bukan string')}

                //console.log('masuk kondisi, harus return *')

                //console.log(value)

                return "*" + value;
            }
            //else {

            //    console.log('tidak masuk kondisi, harus return *')
            //    return value;
            //}


            //if (value && value[0] !== "*") {
            //    //console.log('if confition' + value)

            //    return "*" + value;
            //} 

            //console.log('else confition')
            return value;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>

        <ext:GridPanel ID="GridpanelView" ClientIDMode="Static" runat="server" Title="Title" EmptyText="Record is empty">

            <Store>
                <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadData">

                    <Sorters>
                    </Sorters>
                    <Proxy>

                        <ext:PageProxy />
                    </Proxy>
                    <Reader>
                    </Reader>
                </ext:Store>
            </Store>

            <Plugins>

                <ext:FilterHeader ID="GridHeader1" runat="server" Remote="true" ClientIDMode="Static" OnCreateFilterableField="GridHeader1_CreateFilterableField" />
            

            </Plugins>

            <BottomBar>

                <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True">
                    <Items>
                        <ext:Button runat="server" ID="BtnExportForImport" ClientIDMode="Static" Text="Export Selected For Import" AutoPostBack="true" OnClick="ExportExcelForImport" />
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
            <DockedItems>
                <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true" Dock="Top" ClientIDMode="Static">
                    <Items>

                        <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" EmptyText="[Select Format]" FieldLabel="Export :">

                            <Items>
                                <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                                <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                            </Items>
                        </ext:ComboBox>
                        <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" AutoPostBack="true" OnClick="ExportExcel" ClientIDMode="Static">
                        </ext:Button>
                        <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page" AutoPostBack="true" OnClick="ExportAllExcel">
                        </ext:Button>

                        <%--<ext:Button ID="btnprint" runat="server" Text="Print Current Page" Icon="Printer" Handler="this.up('grid').print({currentPageOnly : true});">
                           </ext:Button> --%>

                        <ext:Button ID="BtnAdd" ClientIDMode="Static" runat="server" Text="Add New Record" Icon="Add" Visible="false" Handler="NawadataDirect.BtnAdd_Click()">
                        </ext:Button>
                        <ext:Button ID="AdvancedFilter" runat="server" Text="Advanced Filter" Icon="Add" Handler="NawadataDirect.BtnAdvancedFilter_Click()">
                        </ext:Button>

                        <%--20220620 DevOps ID 3587 [NF2] Background Process User Matrix - Export Result--%>
                        <ext:TextField ID="txtCurrentUser" runat="server" AnchorHorizontal="100%" ClientIDMode="Static" Hidden="true">
                     </ext:TextField>

                        <%--20220926 DevOps ID 4046 Export All Excel Report Query to Background Process--%>
                        <ext:TextField ID="txtStatusDisabled" runat="server" AnchorHorizontal="100%" ClientIDMode="Static" Hidden="true" />

                    </Items>
                </ext:Toolbar>

                <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true" Dock="Top" Hidden="true">
                    <Items>
                        <ext:HyperlinkButton ID="btnClear" runat="server" Text="Clear Advanced Filter">
                            <DirectEvents>
                                <Click OnEvent="btnClear_Click"></Click>
                            </DirectEvents>
                        </ext:HyperlinkButton>
                        <ext:Label ID="LblAdvancedFilter" runat="server" Text="">
                        </ext:Label>
                    </Items>
                </ext:Toolbar>
            </DockedItems>
            <Listeners>
                <ViewReady Handler="columnAutoResize(this);this.getStore().on('load', Ext.bind(columnAutoResize, null, [this]));"
                    Delay="10" />
            </Listeners>
            <View>
                <ext:GridView runat="server" EnableTextSelection="true" />
            </View>

            <SelectionModel>
                <ext:CheckboxSelectionModel runat="server" Mode="Multi">
                </ext:CheckboxSelectionModel>
            </SelectionModel>
        </ext:GridPanel>

        <ext:Panel ID="Panel1" runat="server" Hidden="true">
            <Content>
                <uc1:AdvancedFilter runat="server" ID="AdvancedFilter1" />
            </Content>
            <Items>
            </Items>
        </ext:Panel>
    </h1>
</asp:Content>
