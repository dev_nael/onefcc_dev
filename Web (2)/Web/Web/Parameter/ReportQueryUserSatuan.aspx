﻿<%@ page title="" language="VB" masterpagefile="~/Site1.master" autoeventwireup="false" inherits="ReportQueryUserSatuan, App_Web_reportqueryusersatuan.aspx.252c98" %>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
            <ext:GridPanel ID="GridPreview" runat="server" ClientIDMode="Static" Title="Preview" Height="300">
                <DockedItems>
                    <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true"  Dock="Top">
                        <Items>

                            <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" EmptyText="[Select Format]" FieldLabel="Export :" >
                                
                                <Items>
                                    <ext:ListItem Text="Excel" Value="Excel"   ></ext:ListItem>
                                    <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                                </Items>

                            </ext:ComboBox>
                            <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" AutoPostBack="true" OnClick="ExportExcel" ClientIDMode="Static" />
                            <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page" AutoPostBack="true" OnClick="ExportAllExcel" />
                             <ext:Button ID="AdvancedFilter" runat="server" Text="Advanced Filter" Icon="Add" Handler="NawadataDirect.BtnAdvancedFilter_Click()">
                        </ext:Button>
                            <%--<ext:Button ID="Button1" runat="server" Text="Print Current Page" Icon="Printer" Handler="this.up('grid').print({currentPageOnly : true});" />--%>

                        </Items>
                    </ext:Toolbar>
                     <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true" Dock="Top" Hidden="true" >
                 <Items>
                     <ext:HyperlinkButton ID="btnClear" runat="server" Text="Clear Advanced Filter">
                         <DirectEvents>
                             <Click OnEvent="btnClear_Click"> </Click>
                         </DirectEvents>
                     </ext:HyperlinkButton> <ext:Label ID="LblAdvancedFilter" runat="server" Text="" >
                    </ext:Label>    
                 </Items>
                 </ext:Toolbar>
                </DockedItems>
                <View>
                <ext:GridView runat="server" EnableTextSelection="true" />
            </View>
                <Store>
                    <ext:Store ID="storeview" runat="server"  RemoteFilter="true" RemoteSort="true" OnReadData="storePreview_Readdata" RemotePaging="true" ClientIDMode="Static">
                        <Sorters>
                        </Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                         
                    </ext:Store>
                   
                </Store>

                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
        
    <ext:Panel ID="Panel1" runat="server" Hidden="true">
            <Content>
                <uc1:AdvancedFilter runat="server" ID="AdvancedFilter1" />
            </Content>
            <Items>
            </Items>
        </ext:Panel>
</asp:Content>


