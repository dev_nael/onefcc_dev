﻿<%@ page title="" language="vb" autoeventwireup="false" masterpagefile="~/Site1.Master" inherits="ParameterAdd, App_Web_parameteradd.aspx.252c98" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <script type="text/javascript">
         var isHurufAngkaKeyAndChar = function (field, e) { 
             return;

            //var charCode = e.getKey();
            // if ((charCode > 37 && charCode < 42) || charCode == 47  || charCode == 59) {
            //     console.log(charCode);
            //    window.event.returnValue = false;
            //} else {
            //    return;
            //}
        }
 
         function selectcontrol(objcontrol) {  
            objcontrol.focus();
         }

         function reworkHTML(valHtml, valId) { 
             var newHtml = "";

             var id = valId.substring(6)
             
             newHtml = valHtml.replace('href="#"', 'href=# onclick="selectcontrol(App.' + id + ')"')

             return newHtml;
         }

         
         function validation(validationStatus, FormPanelInput, infoPanelEdit, FormStatusBar) {
             var htmlInfoPanel = "";

             var listNumberField = Ext.ComponentQuery.query('numberfield');

             validationStatus.errors.items.forEach(function (val) {
                 if (val.field.config.xtype == "numberfield") {
                     listNumberField.forEach(function (selectedVal) {
                         if (selectedVal.id == val.field.id && val.field.activeErrors) {
                             htmlInfoPanel += '<li id="x-err-' + val.field.id + '">'
                             htmlInfoPanel += '<a href=# onclick = "selectcontrol(App.' + val.field.id + ')" > ' + val.field.activeErrors + '</a >'
                             htmlInfoPanel += '</li>'
                         }
                     })
                 } else {
                     htmlInfoPanel += '<li id="x-err-' + val.field.id + '">'
                     htmlInfoPanel += '<a href=# onclick = "selectcontrol(App.' + val.field.id + ')" > ' + val.msg + '</a >'
                     htmlInfoPanel += '</li>'
                 }
             })

             if (validationStatus.msgEl.el.dom.innerHTML) {
                 infoPanelEdit.setVisible(true);
             }

             infoPanelEdit.update(htmlInfoPanel)

             FormPanelInput.body.scrollTo('top', 0)
         }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <ext:Panel runat="server" Layout="FitLayout" AutoScroll="true" ID="outerPanel">
        <Items>
               <ext:FormPanel ID="FormPanelInput" BodyPadding="20" runat="server" ClientIDMode="Static" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true">

                <DockedItems>
                    <ext:Toolbar ID="ToolbarInput" ClientIDMode="Static" runat="server" EnableOverflow="true" Dock="Top" Layout="FitLayout" MaxHeight="150">
                        <Items>  
                         <%--<ext:InfoPanel ID="InfoPanel1" runat="server" Html="Testing</br>Testing</br>Testing</br>Testing</br>Testing</br>Testing" Hidden="false"  AutoScroll="true">                        
                    </ext:InfoPanel>--%>
                        </Items>
                    </ext:Toolbar>
                </DockedItems>
                <Items>
                </Items>
                <Buttons>
                    <ext:Button ID="btnSave" ClientIDMode="Static" runat="server" Text="Save" Enabled="false" Icon="DiskBlack">
                        <Listeners>
                            <Click Handler="
                                    if (!#{FormPanelInput}.getForm().isValid()) 
                                {   validation(#{validationStatus},#{FormPanelInput},#{infoPanelEdit} ); return false;}
                                else return true;"></Click>
                            
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="Callback">
                                <ExtraParams>
                                    <ext:Parameter Name="command" Value="New" Mode="Value" /> 
                                </ExtraParams>
                                <EventMask ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="btnCancel" runat="server" Text="Cancel" Icon="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancel_Click"></Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>

            </ext:FormPanel>
           
        </Items>
        <BottomBar>
             <ext:StatusBar ID="FormStatusBar" runat="server" DefaultText="Ready" Hidden="true">
                
                    <Plugins>
                        <ext:ValidationStatus ID="validationStatus" ClientIDMode="Static"
                            runat="server"
                            FormPanelID="FormPanelInput"
                            ValidIcon="Accept"
                            ErrorIcon="Exclamation" 
                            > 
                          
                        </ext:ValidationStatus>
                    </Plugins>
                </ext:StatusBar>
        </BottomBar>


    </ext:Panel>
     <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Visible="false">
                <Defaults>
                    <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
                </Defaults>
                <LayoutConfig>
                    <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
                </LayoutConfig>
                <Items>
                    <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
                    </ext:Label>

                </Items>

                <Buttons>

                    <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
</asp:Content>
