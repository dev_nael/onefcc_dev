﻿Imports System.Data
Imports System.IO
Imports OfficeOpenXml

Partial Class Parameterview
    Inherits Parent

    '  Public objFormModuleView As NawaBLL.V2.FormModuleView.FormModuleView

    Private Shared Sub DataSetToExcel(ByVal dataSet As DataSet, ByVal objfileinfo As FileInfo)
        Using pck As ExcelPackage = New ExcelPackage()

            For Each dataTable As DataTable In dataSet.Tables
                Dim workSheet As ExcelWorksheet = pck.Workbook.Worksheets.Add(dataTable.TableName)
                workSheet.Cells("A1").LoadFromDataTable(dataTable, True)

                Dim dateformat As String = NawaBLL.V2.Basic.SystemParameterBLL.GetDateFormat
                Dim intcolnumber As Integer = 1
                For Each item As System.Data.DataColumn In dataTable.Columns
                    If item.DataType = GetType(Date) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = dateformat
                    End If
                    If item.DataType = GetType(Integer) Or item.DataType = GetType(Double) Or item.DataType = GetType(Long) Or item.DataType = GetType(Decimal) Or item.DataType = GetType(Single) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = "#,##0.00"
                    End If

                    intcolnumber = intcolnumber + 1
                Next

                'workSheet.Cells(workSheet.Dimension.Address).AutoFitColumns()

            Next

            pck.SaveAs(objfileinfo)
        End Using
    End Sub

    Public Property objFormModuleView() As NawaBLL.V2.FormModuleView.FormModuleView
        Get
            Return Session("Parameterview.objFormModuleView")
        End Get
        Set(ByVal value As NawaBLL.V2.FormModuleView.FormModuleView)
            Session("Parameterview.objFormModuleView") = value
        End Set
    End Property

    Public Property strWhereClause() As String
        Get
            Return Session("Parameterview.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("Parameterview.strWhereClause") = value
        End Set
    End Property

    Public Property strOrder() As String
        Get
            Return Session("Parameterview.strSort")
        End Get
        Set(ByVal value As String)
            Session("Parameterview.strSort") = value
        End Set
    End Property

    Public Property indexStart() As String
        Get
            Return Session("Parameterview.indexStart")
        End Get
        Set(ByVal value As String)
            Session("Parameterview.indexStart") = value
        End Set
    End Property

    Public Property totalRow() As Integer
        Get
            Return Session("Parameterview.totalRow")
        End Get
        Set(ByVal value As Integer)
            Session("Parameterview.totalRow") = value
        End Set
    End Property

    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("Parameterview.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("Parameterview.ObjModule") = value
        End Set
    End Property

    Public Property ObjDtModuleSummary() As DataTable
        Get
            If Session("Parameterview.ObjDtModuleSummary") Is Nothing Then

                Dim dt As New DataTable

                Session("Parameterview.ObjDtModuleSummary") = dt
            End If

            Return Session("Parameterview.ObjDtModuleSummary")
        End Get
        Set(ByVal value As DataTable)
            Session("Parameterview.ObjDtModuleSummary") = value
        End Set
    End Property
    Public Property PK_BackgroundProcess_ID() As Long
        Get
            Return Session("Parameterview.PK_BackgroundProcess_ID")
        End Get
        Set(ByVal value As Long)
            Session("Parameterview.PK_BackgroundProcess_ID") = value
        End Set
    End Property

    Private Sub Parameterview_Init(sender As Object, e As EventArgs) Handles Me.Init

        objFormModuleView = New NawaBLL.V2.FormModuleView.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer
            Try
                intModuleid = NawaBLL.V2.Common.Common.DecryptQueryString(Moduleid, NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey)
                ObjModule = NawaBLL.V2.ModuleBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

                If Not NawaBLL.V2.ModuleBLL.ModuleBLL.GetHakAkses(NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.V2.Common.Common.ModuleActionEnum.view) Then
                    Dim strIDCode As String = 1
                    strIDCode = NawaBLL.V2.Common.Common.EncryptQueryString(strIDCode, NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.V2.Common.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If

                objFormModuleView.ModuleID = ObjModule.PK_Module_ID
                objFormModuleView.ModuleName = ObjModule.ModuleName
                AddHandler objFormModuleView.DownloadFile, AddressOf downloadfile
                objFormModuleView.SettingFormView()

                If Not ObjModule.IsSupportUpload Then
                    BtnExportForImport.Hide()

                End If
            Catch ex As Exception

            End Try

            If Not Ext.Net.X.IsAjaxRequest Then

                cboExportExcel.SelectedItem.Text = "Excel"
                Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
                Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
                Session("Component_AdvancedFilter.objTableFilter") = Nothing
            End If

            'For Each item As String In SelectionID

            '    sm.SelectedRows.Add(New SelectedRow(item))
            'Next

            ' 2 juni 2020
            ' apuy Summary row
            ' TODOAPUY : Check exist module summary
            ' Changes to sql query dont use dal
            'objFormModuleView.GetModuleSummary
            ObjDtModuleSummary = NawaBLL.V2.SQLHelper.ExecuteTable(NawaBLL.V2.SQLHelper.strConnectionString, CommandType.Text, "IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME = 'MModuleSummary' ) BEGIN  SELECT * FROM MModuleSummary where FK_Module_ID =@ModuleID END ", New SqlClient.SqlParameter() {New SqlClient.SqlParameter("@ModuleID", ObjModule.PK_Module_ID)})

            Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")

            If ObjDtModuleSummary.Rows.Count > 0 Then

                objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"

                GridpanelView.GetView.GetRowClass.Fn = "fnRowClass"

                For Each item In GridpanelView.ColumnModel.Columns
                    If item.XType = "booleancolumn" Then
                        item.Renderer.Fn = "summaryrenderboolean"
                    End If
                Next
            Else
                objcommandcol.PrepareToolbar.Fn = "prepareCommandCollectionHideButton"
            End If


        Catch ex As Exception

            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub downloadfile(IDData As String, ModuleID As String, strfilename As String, strFieldname As String)

        Try
            Dim intModuleid As Integer = NawaBLL.V2.Common.Common.DecryptQueryString(ModuleID, NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey)
            Dim strIDdata As String = NawaBLL.V2.Common.Common.DecryptQueryString(IDData, NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey)

            Dim objModule As NawaDAL.Module = NawaBLL.V2.ModuleBLL.ModuleBLL.GetModuleByModuleID(intModuleid)
            Dim strtablename As String = objFormModuleView.ModuleName
            If Not objModule Is Nothing Then
                If objModule.IsUseDesigner Then
                    strtablename = objModule.ModuleName
                End If
            End If

            Dim strPrimarykey As String
            If objModule.IsUseDesigner Then
                strPrimarykey = NawaBLL.V2.ModuleBLL.ModuleBLL.GetPrimaryKeyField(objModule.PK_Module_ID)
            Else
                strPrimarykey = objFormModuleView.objSchemaModuleField.Where(Function(item) item.IsPrimaryKey = True).Select(Function(item) item.FieldName).FirstOrDefault()
            End If

            'Ext.Net.X.Msg.Alert(strtablename & "TEST" & strPrimarykey, String.Format(NawaBLL.V2.Common.Common.GetApplicationPath & "/Parameter/DownloadFile.ashx?ID={0}&ModuleID={1}&Filename={2}&FieldName={3}", IDData, ModuleID, strfilename, strFieldname)).Show()
            If strtablename <> "" And strPrimarykey <> "" Then
                Dim strsql As String = "select " & strFieldname & " from " & strtablename & " where  " & strPrimarykey & " =@PrimaryData"

                Dim arrdata As Byte() = Nothing
                Dim result As Object = Nothing
                result = NawaBLL.V2.SQLHelper.ExecuteScalar(NawaBLL.V2.SQLHelper.strConnectionString, Data.CommandType.Text, strsql, New SqlClient.SqlParameter() {New SqlClient.SqlParameter("@PrimaryData", strIDdata)})
                If result.GetType.FullName <> "System.DBNull" Then
                    arrdata = result
                End If

                'Dim strEncodeStrFileName As String = HttpUtility.HtmlEncode(strfilename)
                '20220726 fix jika file mengandung & agar bisa di download
                Dim strReplaceFileName As String = Replace(strfilename, "&", "___===")
                If Not arrdata Is Nothing Then

                    Response.Redirect(String.Format(NawaBLL.V2.Common.Common.GetApplicationPath & "/Parameter/DownloadFile.ashx?ID={0}&ModuleID={1}&Filename={2}&FieldName={3}&TableName={4}&PrimaryKey={5}", IDData, ModuleID, strReplaceFileName, strFieldname, strtablename, strPrimarykey), True)

                End If

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                Toolbar2.Hidden = True
            Else
                Toolbar2.Hidden = False
            End If
            LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")

            Dim intStart As Integer = e.Start
            'If intStart = 0 Then intStart = 1
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)
            Dim strsort As String = ""
            '  strfilter = strfilter.Replace("Active", objFormModuleView.ModuleName & ".Active")

            If NawaBLL.V2.Common.Common.SessionSQLVersion >= 2012 Then
                For Each item As DataSorter In e.Sort
                    strsort += item.Property & " " & item.Direction.ToString
                Next
            Else
                For Each item As DataSorter In e.Sort
                    strsort += objFormModuleView.getsortTable(item.Property) & " " & item.Direction.ToString
                Next
            End If

            Me.indexStart = intStart
            Me.strWhereClause = strfilter

            Dim strfilterDataAccess As String = NawaBLL.V2.NawaFramework.Nawa.BLL.NawaFramework.GetFilterDataAccess(NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID, objFormModuleView.objSchemaModule.PK_Module_ID, NawaBLL.V2.Common.Common.ModuleActionEnum.view)

            If strfilterDataAccess <> "" Then
                If strWhereClause = "" Then
                    strWhereClause += strfilterDataAccess
                Else
                    strWhereClause += " and " & strfilterDataAccess
                End If
            End If

            Dim AdditionalFilterQueryString As String = Request.Params("AdditionalFilter")
            If AdditionalFilterQueryString <> "" Then
                If strWhereClause = "" Then
                    strWhereClause += AdditionalFilterQueryString
                Else
                    strWhereClause += " and " & AdditionalFilterQueryString
                End If
            End If

            Dim AdditonalFilterPerUser As String = NawaBLL.V2.NawaFramework.Nawa.BLL.NawaFramework.GetFilterAdditonalPerUser(NawaBLL.V2.Common.Common.SessionCurrentUser.UserID, objFormModuleView.objSchemaModule.PK_Module_ID)
            If AdditonalFilterPerUser <> "" Then
                If strWhereClause = "" Then
                    strWhereClause += AdditonalFilterPerUser
                Else
                    strWhereClause += " and " & AdditonalFilterPerUser
                End If
            End If

            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                End If
            End If

            Me.strOrder = strsort
            ' apuy penambahan
            ' 04 06 2020
            Dim query = ""

            Dim DataPaging As Data.DataTable = objFormModuleView.getDataPagingNew(strWhereClause, strsort, intStart, intLimit, inttotalRecord, query)

            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            'objFormModuleView.objGridPanel.GetStore.DataSource = DataPaging
            'objFormModuleView.objGridPanel.DataBind()

            If inttotalRecord <> totalRow Then
                totalRow = inttotalRecord
            End If

            If ObjDtModuleSummary.Rows.Count > 0 And DataPaging.Rows.Count > 0 Then
                Dim objSqlParam(1) As SqlClient.SqlParameter

                objSqlParam(0) = New SqlClient.SqlParameter
                objSqlParam(0).ParameterName = "@moduleId"
                objSqlParam(0).Value = ObjModule.PK_Module_ID

                objSqlParam(1) = New SqlClient.SqlParameter
                objSqlParam(1).ParameterName = "@query"
                objSqlParam(1).Value = query


                Dim summaryRow As DataRow = NawaBLL.V2.SQLHelper.ExecuteRow(NawaBLL.V2.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_getModuleSummary", objSqlParam)

                If summaryRow IsNot Nothing Then

                    DataPaging.Rows.Add(summaryRow.ItemArray)

                End If

            End If
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.V2.Common.Common.DecryptQueryString(Moduleid, NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.V2.ModuleBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            '20220907 3996 Remove filter field type varbinary in advanced filter
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField.Where(Function(item) item.IsShowInView And item.FK_FieldType_ID <> 14).ToList()
            'End 20220907 3996 Remove filter field type varbinary in advanced filter
            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Public Sub GoToBackgroundProcess()
        Dim Moduleid As String = NawaBLL.V2.Common.Common.EncryptQueryString(NawaBLL.V2.BackgroundProcessBLL.BackgroundProcessBLL.GetModuleID(), NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey)
        Dim dataid As String = NawaBLL.V2.Common.Common.EncryptQueryString(PK_BackgroundProcess_ID, NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey)
        Ext.Net.X.Redirect(NawaBLL.V2.Common.Common.GetApplicationPath & "/Parameter/BackgroundProcessView.aspx?ModuleID=" & Moduleid & "&PK_BackgroundProcess_ID=" & dataid)
    End Sub

    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then

                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    If totalRow > CInt(NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetSystemParameterByPk(61).SettingValue) And CInt(NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetSystemParameterByPk(61).SettingValue) > 0 Then
                        PK_BackgroundProcess_ID = objFormModuleView.BackgroundExport(Me.strWhereClause, Me.strOrder, 5)
                        'Ext.Net.X.Msg.Alert("Info", "File will be Generated in Background Process").Show()
                        Ext.Net.X.Msg.Show(New MessageBoxConfig() With
                        {
                            .Title = "Info",
                            .Message = "File will be Generated in Background Process",
                            .Buttons = MessageBox.Button.OK,
                            .Icon = MessageBox.Icon.INFO,
                            .MessageBoxButtonsConfig = New MessageBoxButtonsConfig() With {
                                .Ok = New MessageBoxButtonConfig() With {.Handler = "NawadataDirect.GoToBackgroundProcess();", .Text = "OK"}
                            }
                        })
                    Else

                        Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                        objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                        Dim intTotalRow As Long
                        '1048575

                        Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, 1048575, intTotalRow)
                            'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                            objtbl.TableName = objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                            Dim intTotalPage As Integer = (intTotalRow + 1048575 - 1) / 1048575
                            Dim dsTable As New Data.DataSet

                            objFormModuleView.changeHeader(objtbl)

                            If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                                objtbl.Columns.Remove("pk_moduleapproval_id")
                            End If
                            For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                                If item.Hidden Then
                                    If objtbl.Columns.Contains(item.DataIndex) Then
                                        objtbl.Columns.Remove(item.DataIndex)
                                    End If

                                End If
                            Next


                            dsTable.Tables.Add(objtbl.Copy())

                            If intTotalPage > 1 Then
                                'ambil page 2 dst sampe page terakhir
                                'For index = 1 To intTotalPage - 2


                                For index = 1 To intTotalPage - 1 '20220914
                                    Using objtbltemp As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, (index * 1048575), 1048575, intTotalRow)

                                        '20220914

                                        objFormModuleView.changeHeader(objtbltemp)

                                        If objtbltemp.Columns.Contains("pk_moduleapproval_id") Then
                                            objtbltemp.Columns.Remove("pk_moduleapproval_id")
                                        End If

                                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                                            If item.Hidden Then
                                                If objtbltemp.Columns.Contains(item.DataIndex) Then
                                                    objtbltemp.Columns.Remove(item.DataIndex)
                                                End If

                                            End If
                                        Next
                                        'End 20220914
                                        objtbltemp.TableName = objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                                        dsTable.Tables.Add(objtbltemp.Copy)

                                    End Using
                                Next
                            End If

                            Dim Breakpointvar = 0


                            DataSetToExcel(dsTable, objfileinfo)
                            'NawaBLL.V2.FormModuleView.Exporter.DatasetToExcel(dsTable, objfileinfo)
                            dsTable.Dispose()
                            dsTable = Nothing

                            If NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetSystemParameterByPk(67).SettingValue Then

                                Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                                If IO.File.Exists(strfilezipexcel) Then
                                    objfileinfo = New IO.FileInfo(strfilezipexcel)
                                End If

                                Dim arrByte As Byte() = IO.File.ReadAllBytes(objfileinfo.FullName)
                                If objfileinfo.Exists Then objfileinfo.Delete()

                                Response.Clear()
                                Response.ClearHeaders()
                                '  Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")

                                Response.ContentType = "application/octet-stream"
                                Response.AddHeader("content-disposition", "attachment;filename=" & objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                                Response.Charset = ""
                                Response.AddHeader("cache-control", "max-age=0")
                                Me.EnableViewState = False
                                Response.BinaryWrite(arrByte)
                                Response.End()
                            Else

                                Dim arrByte As Byte() = IO.File.ReadAllBytes(objfileinfo.FullName)
                                If objfileinfo.Exists Then objfileinfo.Delete()

                                Response.Clear()
                                Response.ClearHeaders()
                                '  Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")

                                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                Response.AddHeader("content-disposition", "attachment;filename=" & objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")
                                Response.Charset = ""
                                Response.AddHeader("cache-control", "max-age=0")
                                Me.EnableViewState = False
                                Response.BinaryWrite(arrByte)
                                Response.End()
                            End If

                        End Using
                    End If



                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    If totalRow > CInt(NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetSystemParameterByPk(61).SettingValue) And CInt(NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetSystemParameterByPk(61).SettingValue) > 0 Then
                        PK_BackgroundProcess_ID = objFormModuleView.BackgroundExport(Me.strWhereClause, Me.strOrder, 6)
                        'Ext.Net.X.Msg.Alert("Info", "File will be Generated in Background Process").Show()
                        Ext.Net.X.Msg.Show(New MessageBoxConfig() With
                        {
                            .Title = "Info",
                            .Message = "File will be Generated in Background Process",
                            .Buttons = MessageBox.Button.OK,
                            .Icon = MessageBox.Icon.INFO,
                            .MessageBoxButtonsConfig = New MessageBoxButtonsConfig() With {
                                .Ok = New MessageBoxButtonConfig() With {.Handler = "NawadataDirect.GoToBackgroundProcess();", .Text = "OK"}
                            }
                        })
                        'Else
                        '    If totalRow > CInt(NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetSystemParameterByPk(61).SettingValue) Then
                        '    objFormModuleView.BackgroundExport(Me.strWhereClause, Me.strOrder, 6)
                        '    Ext.Net.X.Msg.Alert("Info", "File will be Generated in Background Process").Show()
                    Else
                        Dim tempfilecsv As String = Guid.NewGuid.ToString & ".csv"
                        Dim strpathcsv As String = Server.MapPath("~\temp\" & tempfilecsv)

                        Dim strzipfile As String = objFormModuleView.GenerateCSV(Me.strWhereClause, strpathcsv, objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                        objfileinfo = New IO.FileInfo(strzipfile)

                        Dim arrByte As Byte() = IO.File.ReadAllBytes(objfileinfo.FullName)
                        If objfileinfo.Exists Then objfileinfo.Delete()

                        If NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetSystemParameterByPk(67).SettingValue Then
                            Response.Clear()
                            Response.AddHeader("content-disposition", "attachment;filename=" & objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                            Response.Charset = ""
                            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                            Me.EnableViewState = False
                            'Response.ContentType = "application/ms-excel"
                            Response.ContentType = "application/octet-stream"
                            Response.BinaryWrite(arrByte)
                            Response.End()
                        Else
                            Response.Clear()
                            Response.AddHeader("content-disposition", "attachment;filename=" & objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".csv")
                            Response.Charset = ""
                            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                            Me.EnableViewState = False
                            'Response.ContentType = "application/ms-excel"
                            Response.ContentType = "text/csv"
                            Response.BinaryWrite(arrByte)
                            Response.End()
                        End If
                    End If
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#Region "Backup Before export csv"

    'Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
    'objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
    'Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
    'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

    '    objFormModuleView.changeHeader(objtbl)
    '    For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

    '        If item.Hidden Then
    '            If objtbl.Columns.Contains(item.DataIndex) Then
    '                objtbl.Columns.Remove(item.DataIndex)
    '            End If
    '        End If
    '    Next
    '    For k As Integer = 0 To objtbl.Columns.Count - 1
    '        'add separator
    '        stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
    '    Next
    '    'append new line
    '    stringWriter_Temp.Write(vbCr & vbLf)
    '    For i As Integer = 0 To objtbl.Rows.Count - 1
    '        For k As Integer = 0 To objtbl.Columns.Count - 1
    '            'add separator
    '            stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
    '        Next
    '        'append new line
    '        stringWriter_Temp.Write(vbCr & vbLf)
    '    Next
    '    stringWriter_Temp.Close()
    '    Response.Clear()
    '    Response.AddHeader("content-disposition", "attachment;filename=" & objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".csv")
    '    Response.Charset = ""
    '    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '    Me.EnableViewState = False
    '    'Response.ContentType = "application/ms-excel"
    '    Response.ContentType = "text/csv"
    '    Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
    '    Response.End()
    'End Using

#End Region

    Protected Sub ExportExcelForImport(sender As Object, e As EventArgs)
        Try
            Dim sm As RowSelectionModel = TryCast(Me.GridpanelView.GetSelectionModel(), RowSelectionModel)
            If sm.SelectedRows.Count > 0 Then

                Dim SelectionID As ArrayList = New ArrayList

                For Each item As SelectedRow In sm.SelectedRows
                    If IsNumeric(item.RecordID) Then
                        SelectionID.Add(item.RecordID)
                    End If
                Next

                Dim strFileName As String = objFormModuleView.CreateExcel2007WithData(Server.MapPath("~/Temp"), SelectionID)

                Dim fileContents As Byte()
                fileContents = My.Computer.FileSystem.ReadAllBytes(strFileName)
                If IO.File.Exists(strFileName) Then IO.File.Delete(strFileName)

                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                'Response.ContentType = "application/vnd.ms-excel"
                Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                Response.BinaryWrite(fileContents)
                Response.End()
            Else
                Ext.Net.X.Msg.Alert("Error", "Please Select at least one item to export.").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try


    End Sub

    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.V2.Basic.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)

                        If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                            objtbl.Columns.Remove("pk_moduleapproval_id")
                        End If

                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.V2.Basic.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                If item.DataType = GetType(Integer) Or item.DataType = GetType(Double) Or item.DataType = GetType(Long) Or item.DataType = GetType(Decimal) Or item.DataType = GetType(Single) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = "#,##0.00"
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                        End Using
                    End Using

                    Dim arrByte As Byte() = IO.File.ReadAllBytes(objfileinfo.FullName)
                    If objfileinfo.Exists Then objfileinfo.Delete()

                    Response.Clear()
                    Response.ClearHeaders()

                    Response.AddHeader("content-disposition", "attachment;filename=" & objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")
                    Response.BinaryWrite(arrByte)
                    Response.End()
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))

                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)

                    Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.V2.Basic.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                            objtbl.Columns.Remove("pk_moduleapproval_id")
                        End If

                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                'If objtbl.Columns.Contains(item.Text) Then
                                '    objtbl.Columns.Remove(item.Text)
                                'End If
                                '20220916 fix column csv current page
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                    End Using

                    Dim arrByte As Byte() = IO.File.ReadAllBytes(objfileinfo.FullName)
                    If objfileinfo.Exists Then objfileinfo.Delete()

                    Response.Clear()
                    Response.AddHeader("content-disposition", "attachment;filename=" & objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".csv")
                    Response.Charset = ""
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Me.EnableViewState = False
                    'Response.ContentType = "application/ms-excel"
                    Response.ContentType = "text/csv"
                    Response.BinaryWrite(arrByte)
                    Response.End()
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    If objfileinfo.Exists Then objfileinfo.Delete()
                End If

            End If
        End Try
    End Sub

    'Protected Sub Check_Select(sender As Object, e As DirectEventArgs)
    '    Try

    '        'RowSelectionModel sm = this.GridPanel1.GetSelectionModel() as RowSelectionModel;

    '        '   sm.SelectedRows.Add(new SelectedRow(2));
    '        '   sm.SelectedRows.Add(new SelectedRow("11"));

    '        Dim sm As RowSelectionModel = TryCast(Me.GridpanelView.GetSelectionModel(), RowSelectionModel)

    '        For Each item As Ext.Net.SelectedRow In sm.SelectedRows
    '            If Not SelectionID.Contains(item.RecordID) Then
    '                SelectionID.Add(item.RecordID)
    '            End If
    '        Next

    '    Catch ex As Exception
    '                    Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try

    'End Sub

    '<DirectMethod>
    'Public Sub Coba(strcolumn As String, bvalue As Boolean)

    '    For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns
    '        Dim data As String = item.Text
    '        Dim value As Boolean = item.Hidden
    '    Next

    'End Sub
    <DirectMethod>
    Public Sub BtnAdd_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(String.Format(NawaBLL.V2.Common.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Function GridHeader1_CreateFilterableField(sender As Object, column As ColumnBase, defaultField As Field) As Field

        If String.IsNullOrEmpty(column.XType) Then

            defaultField.CustomConfig.Add(New ConfigItem("getValue", "getColumnValue", ParameterMode.Raw))

            'ElseIf column.XType = "booleancolumn" Then

            '    defaultField.CustomConfig.Add(New ConfigItem("getValue", "getColumnValue", ParameterMode.Raw))

        End If

        Return defaultField

    End Function


End Class