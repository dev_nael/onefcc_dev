﻿Imports Ext.Net
Imports OfficeOpenXml
Partial Class ParameterApproval
    Inherits Parent
    Public objFormModuleApproval As NawaBLL.FormModuleApproval
    Public Property strWhereClause() As String
        Get
            Return Session("ParameterApproval.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("ParameterApproval.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("ParameterApproval.strSort")
        End Get
        Set(ByVal value As String)
            Session("ParameterApproval.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("ParameterApproval.indexStart")
        End Get
        Set(ByVal value As String)
            Session("ParameterApproval.indexStart") = value
        End Set
    End Property

    Public Property objSchemaModule() As NawaDAL.Module
        Get
            Return Session("ParameterApproval.objSchemaModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("ParameterApproval.objSchemaModule") = value
        End Set
    End Property

    Private Sub Parameterview_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleApproval = New NawaBLL.FormModuleApproval(Me.GridpanelView)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not NawaBLL.V2.Common.Common.SessionCurrentUser Is Nothing Then
                Dim Moduleid As String = Request.Params("ModuleID")
                Dim intModuleid As Integer
                Try
                    intModuleid = NawaBLL.V2.Common.Common.DecryptQueryString(Moduleid, NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey)
                    objSchemaModule = NawaBLL.V2.ModuleBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

                    If Not NawaBLL.V2.ModuleBLL.ModuleBLL.GetHakAkses(NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MGroupMenu_ID, objSchemaModule.PK_Module_ID, NawaBLL.V2.Common.Common.ModuleActionEnum.Approval) Then
                        Dim strIDCode As String = 1
                        strIDCode = NawaBLL.V2.Common.Common.EncryptQueryString(strIDCode, NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey)

                        Ext.Net.X.Redirect(String.Format(NawaBLL.V2.Common.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    objFormModuleApproval.SettingFormApproval(intModuleid)
                Catch ex As Exception
                    Throw ex
                End Try

                If Not Ext.Net.X.IsAjaxRequest Then

                    cboExportExcel.SelectedItem.Text = "Excel"
                    Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")

                    If objcommandcol IsNot Nothing Then
                        objcommandcol.PrepareToolbar.Fn = "preparecommandcolumn"
                    End If
                End If


            End If


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try



            Dim strModulename As String = ""
            Try

                strModulename = objSchemaModule.ModuleName

            Catch ex As Exception

            End Try

            Dim intStart As Integer = e.Start
            'If intStart = 0 Then intStart = 1
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer

            Dim strfilter As String = objFormModuleApproval.GetWhereClauseHeader(e)
            Dim bHasworkflow As Boolean = False

            strfilter = strfilter.Replace("CreatedDate", "ModuleApproval.CreatedDate")
            strfilter = strfilter.Replace("CreatedBy", "ModuleApproval.CreatedBy")
            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter

            '20221021 Fixing Uncomment function workflow
            Dim intjml As Integer = NawaBLL.FormModuleApprovalDetail.IsModuleHasWorkflow(objSchemaModule.PK_Module_ID)
            If intjml > 0 Then
                bHasworkflow = True
            End If
            'End 20221021 Fixing Uncomment function workflow


            Dim strparam As String = Request.Params("ExtraParam")

            '----- Setting Approval -----
            Dim settingApproval As Boolean = False
            Dim sql As String = "SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'NDSSettingApproval'"
            Dim querySettingApproval As String = ""
            Dim filterSettingApproval As String = ""

            If NawaBLL.V2.SQLHelper.ExecuteScalar(NawaBLL.V2.SQLHelper.strConnectionString, Data.CommandType.Text, sql, Nothing) Then
                sql = "SELECT * FROM NDSSettingApproval WHERE Active = 1 AND FK_Module_ID = @PK_Module_ID"
                Dim NDSSettingApproval As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTable(NawaBLL.V2.SQLHelper.strConnectionString, Data.CommandType.Text, sql, New Data.SqlClient.SqlParameter() {New Data.SqlClient.SqlParameter("@PK_Module_ID", Data.SqlDbType.Int) With {.Value = objSchemaModule.PK_Module_ID}})
                If NDSSettingApproval.Rows.Count <> 0 Then
                    For Each row In NDSSettingApproval.Rows
                        querySettingApproval = " " + row("QueryApproval")
                        filterSettingApproval = row("FilterApproval")

                        filterSettingApproval = filterSettingApproval.ToLower()
                        filterSettingApproval = filterSettingApproval.Replace("@userid", "'" + NawaBLL.V2.Common.Common.SessionCurrentUser.UserID + "'")
                        filterSettingApproval = filterSettingApproval.Replace("@modulename", "'" + strModulename + "'")
                        filterSettingApproval = filterSettingApproval.Replace("@roleid", NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID)
                        filterSettingApproval = filterSettingApproval.Replace("@groupid", NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MGroupMenu_ID)
                    Next

                    settingApproval = True
                End If
            End If
            '----- End Setting Approval -----


            If settingApproval Then
                If strWhereClause.Trim.Length = 0 Then
                    strWhereClause += filterSettingApproval
                Else
                    strWhereClause += filterSettingApproval
                End If
            Else
                If bHasworkflow Then
                    'kalau punya workflow

                    If strWhereClause.Trim.Length = 0 Then


                        strWhereClause += " ModuleApproval.ModuleName='" & strModulename & "'  " & vbCrLf _
    & "AND ( (( FK_MWorkFlowUserType_ID=1 OR FK_MWorkFlowUserType_ID=2) AND FK_MWorkflowUser_ID= '" & NawaBLL.V2.Common.Common.SessionCurrentUser.PK_MUser_ID & "' AND FK_MWorkflow_Status_ID=3) " & vbCrLf _
    & "OR (FK_MWorkFlowUserType_ID=4 AND fk_mworkflowrole_id='" & NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID & "' AND FK_MWorkflow_Status_ID=3) )"

                    Else

                        strWhereClause += " and ModuleApproval.ModuleName='" & strModulename & "'  " & vbCrLf _
   & "AND ( (( FK_MWorkFlowUserType_ID=1 OR FK_MWorkFlowUserType_ID=2) AND FK_MWorkflowUser_ID= '" & NawaBLL.V2.Common.Common.SessionCurrentUser.PK_MUser_ID & "' AND FK_MWorkflow_Status_ID=3) " & vbCrLf _
   & "OR (FK_MWorkFlowUserType_ID=4 AND fk_mworkflowrole_id='" & NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID & "' AND FK_MWorkflow_Status_ID=3) )"

                    End If
                Else

                    If (Not strparam Is Nothing) AndAlso strparam.ToLower = "upline" Then
                        If strWhereClause.Trim.Length = 0 Then

                            strWhereClause += " ModuleApproval.modulename='" & strModulename & "' and ModuleApproval.createdby in ( select a.UserID from MUserStructure a join muser b on a.UserID = b.UserID where a.ParentUserID = '" & NawaBLL.V2.Common.Common.SessionCurrentUser.UserID & "')"


                        Else
                            strWhereClause += " and ModuleApproval.modulename='" & strModulename & "' and ModuleApproval.createdby in ( select a.UserID from MUserStructure a join muser b on a.UserID = b.UserID where a.ParentUserID = '" & NawaBLL.V2.Common.Common.SessionCurrentUser.UserID & "')"


                        End If
                    Else

                        'cek apakah ada table NDSSettingApproval 
                        ' if true, cek, apakah current module exist di table NDSSettingApproval
                        ' querytable
                        ' queryfilter
                        'else

                        If NawaBLL.V2.Common.Common.checkAdditionalUser Then
                            If strWhereClause.Trim.Length = 0 Then
                                'Bug Fix DevOps ID 3492 Error Parameter Approval Jika FK_MRole_ID NULL
                                'strWhereClause += " ModuleApproval.modulename='" & strModulename & "' and ModuleApproval.createdby in ( SELECT m.UserID FROM vw_AdditionalUser m WHERE m.PK_MRole_ID = " & NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID & " AND m.UserID<>'" & NawaBLL.V2.Common.Common.SessionCurrentUser.UserID & "') AND ModuleApproval.FK_MRole_ID = " & NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID
                                strWhereClause += " ModuleApproval.modulename='" & strModulename & "' and ModuleApproval.createdby in ( SELECT m.UserID FROM vw_AdditionalUser m WHERE m.PK_MRole_ID = " & NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID & " AND m.UserID<>'" & NawaBLL.V2.Common.Common.SessionCurrentUser.UserID & "') AND ISNULL(moduleapproval.FK_MRole_ID,MUser.FK_MRole_ID) = " & NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID
                            Else
                                'Bug Fix DevOps ID 3492 Error Parameter Approval Jika FK_MRole_ID NULL
                                'strWhereClause += " AND ModuleApproval.modulename='" & strModulename & "' and ModuleApproval.createdby in ( SELECT m.UserID FROM vw_AdditionalUser m WHERE m.PK_MRole_ID = " & NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID & " AND m.UserID<>'" & NawaBLL.V2.Common.Common.SessionCurrentUser.UserID & "') AND ModuleApproval.FK_MRole_ID = " & NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID
                                strWhereClause += " AND ModuleApproval.modulename='" & strModulename & "' and ModuleApproval.createdby in ( SELECT m.UserID FROM vw_AdditionalUser m WHERE m.PK_MRole_ID = " & NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID & " AND m.UserID<>'" & NawaBLL.V2.Common.Common.SessionCurrentUser.UserID & "') AND ISNULL(moduleapproval.FK_MRole_ID,MUser.FK_MRole_ID) = " & NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID
                            End If
                        Else
                            If strWhereClause.Trim.Length = 0 Then
                                strWhereClause += " ModuleApproval.modulename='" & strModulename & "' and ModuleApproval.createdby in ( SELECT m.UserID FROM MUser m WHERE m.FK_MRole_ID IN ( SELECT c.FK_MRole_ID FROM muser c WHERE c.UserID='" & NawaBLL.V2.Common.Common.SessionCurrentUser.UserID & "') AND m.UserID<>'" & NawaBLL.V2.Common.Common.SessionCurrentUser.UserID & "')"
                            Else
                                strWhereClause += " and  ModuleApproval.modulename='" & strModulename & "' and ModuleApproval.createdby in ( SELECT m.UserID FROM MUser m WHERE m.FK_MRole_ID IN ( SELECT c.FK_MRole_ID FROM muser c WHERE c.UserID='" & NawaBLL.V2.Common.Common.SessionCurrentUser.UserID & "') AND m.UserID<>'" & NawaBLL.V2.Common.Common.SessionCurrentUser.UserID & "')"
                            End If
                        End If

                    End If


                End If
            End If






            Me.strOrder = strsort
            ' Dim DataPaging As Data.DataTable = objFormModuleApproval.getDataPaging(strWhereClause, strsort, intStart - 1, intLimit, inttotalRecord)



            Dim DataPaging As Data.DataTable
            Dim strTable = ""
            Dim strField = "PK_ModuleApproval_ID, ModuleLabel, ModuleKey,moduleapproval.CreatedDate, moduleaction.ModuleActionName   ,ModuleApproval.CreatedBy + ' - ' +MUser.UserName AS CreatedBy"

            'If bHasworkflow Then
            '    DataPaging = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(" ModuleApproval INNER JOIN Module AS m ON ModuleApproval.ModuleName=m.ModuleName INNER JOIN MWorkFlow_Progress AS mfp ON mfp.FK_Module_ID=m.PK_Module_ID AND (mfp.FK_Unik_ID = ModuleApproval.ModuleKey OR mfp.FK_Unik_ID=convert(varchar(50),ModuleApproval.PK_ModuleApproval_ID)+'Import') INNER JOIN moduleaction	 ON moduleaction.PK_ModuleAction_ID=ModuleApproval.PK_ModuleAction_ID LEFT JOIN muser ON muser.userid = ModuleApproval.CreatedBy", "PK_ModuleApproval_ID, ModuleLabel, ModuleKey,moduleapproval.CreatedDate, moduleaction.ModuleActionName   ,ModuleApproval.CreatedBy + ' - ' +MUser.UserName AS CreatedBy", strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            'Else
            '    DataPaging = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(" ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID inner join module on module.modulename=moduleapproval.modulename LEFT JOIN muser ON muser.userid = ModuleApproval.CreatedBy", "PK_ModuleApproval_ID, ModuleLabel, ModuleKey,moduleapproval.CreatedDate , moduleaction.ModuleActionName   ,ModuleApproval.CreatedBy + ' - ' +MUser.UserName AS CreatedBy", strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            'End If

            If settingApproval Then
                strTable = querySettingApproval
            Else
                If bHasworkflow Then
                    strTable = " ModuleApproval INNER JOIN Module AS m ON ModuleApproval.ModuleName=m.ModuleName INNER JOIN MWorkFlow_Progress AS mfp ON mfp.FK_Module_ID=m.PK_Module_ID AND (mfp.FK_Unik_ID = ModuleApproval.ModuleKey OR mfp.FK_Unik_ID=convert(varchar(50),ModuleApproval.PK_ModuleApproval_ID)+'Import') INNER JOIN moduleaction	 ON moduleaction.PK_ModuleAction_ID=ModuleApproval.PK_ModuleAction_ID LEFT JOIN muser ON muser.userid = ModuleApproval.CreatedBy"
                Else
                    strTable = " ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID inner join module on module.modulename=moduleapproval.modulename LEFT JOIN muser ON muser.userid = ModuleApproval.CreatedBy"
                End If
            End If

            If settingApproval = False Then
                If NawaBLL.V2.Common.Common.checkAdditionalUser Then
                    If bHasworkflow Then
                        strTable += " LEFT JOIN vw_AdditionalUser vau ON ModuleApproval.CreatedBy = vau.UserID AND ModuleApproval.FK_MRole_ID = vau.PK_MRole_ID AND ModuleApproval.FK_MRole_ID = " & NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID & " "
                        strWhereClause += " AND ModuleApproval.CreatedBy <> '" & NawaBLL.V2.Common.Common.SessionCurrentUser.UserID & "' "
                    Else
                        strTable += " LEFT JOIN vw_AdditionalUser vau ON ModuleApproval.CreatedBy = vau.UserID AND ModuleApproval.FK_MRole_ID = vau.PK_MRole_ID AND ModuleApproval.FK_MRole_ID = " & NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MRole_ID & " AND vau.PK_MGroupMenu_ID = " & NawaBLL.V2.Common.Common.SessionCurrentUser.FK_MGroupMenu_ID & " AND ModuleApproval.CreatedBy <> '" & NawaBLL.V2.Common.Common.SessionCurrentUser.UserID & "'"
                    End If
                End If
            End If

            DataPaging = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(strTable, strField, strWhereClause, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    ' 20220715 3745 Fix Error Parameter Approval jika user terdaftar di table NDSSettingAdditionalUser
                    'Using objtbl As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(" ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID inner join module on module.modulename=moduleapproval.modulename LEFT JOIN muser ON muser.userid = ModuleApproval.CreatedBy ", "PK_ModuleApproval_ID as Id, ModuleLabel, ModuleKey,moduleapproval.CreatedDate, moduleaction.ModuleActionName   ,moduleapproval.CreatedBy", Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                    ' 20221027 Fix export because has workflow
                    Using objtbl As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(" ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID inner join module on module.modulename=moduleapproval.modulename LEFT JOIN muser ON muser.userid = ModuleApproval.CreatedBy LEFT JOIN MWorkFlow_Progress  on ModuleApproval.ModuleKey = MWorkFlow_Progress.FK_Unik_ID  OR CAST(ModuleApproval.PK_ModuleApproval_ID AS VARCHAR) + 'Import' = MWorkFlow_Progress.FK_Unik_ID ", "PK_ModuleApproval_ID as Id, ModuleLabel, ModuleKey,moduleapproval.CreatedDate, moduleaction.ModuleActionName   ,moduleapproval.CreatedBy", Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)





                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("Approval")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.V2.Basic.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 0
                            For Each item As System.Data.DataColumn In objtbl.Columns

                                intcolnumber = intcolnumber + 1
                                If item.DataType = GetType(Date) Then

                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                If item.DataType = GetType(Integer) Or item.DataType = GetType(Double) Or item.DataType = GetType(Long) Or item.DataType = GetType(Decimal) Or item.DataType = GetType(Single) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = "#,##0.00"
                                End If

                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                        End Using
                    End Using

                    Dim arrByte As Byte() = IO.File.ReadAllBytes(objfileinfo.FullName)
                    If objfileinfo.Exists Then objfileinfo.Delete()

                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objFormModuleApproval.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(arrByte)
                    Response.End()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)

                    ' 20220715 3745 Fix Error Parameter Approval jika user terdaftar di table NDSSettingAdditionalUser
                    ''Using objtbl As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(" ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID inner join module on module.modulename=moduleapproval.modulename LEFT JOIN muser ON muser.userid = ModuleApproval.CreatedBy ", "PK_ModuleApproval_ID as Id, ModuleLabel, ModuleKey,moduleapproval.CreatedDate, moduleaction.ModuleActionName   ,moduleapproval.CreatedBy", Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                    ' 20221027 Fix export because has workflow
                    Using objtbl As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(" ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID inner join module on module.modulename=moduleapproval.modulename LEFT JOIN muser ON muser.userid = ModuleApproval.CreatedBy LEFT JOIN MWorkFlow_Progress  on ModuleApproval.ModuleKey = MWorkFlow_Progress.FK_Unik_ID  OR CAST(ModuleApproval.PK_ModuleApproval_ID AS VARCHAR) + 'Import' = MWorkFlow_Progress.FK_Unik_ID ", "PK_ModuleApproval_ID as Id, ModuleLabel, ModuleKey,moduleapproval.CreatedDate, moduleaction.ModuleActionName   ,moduleapproval.CreatedBy", Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                    End Using

                    Dim arrByte As Byte() = IO.File.ReadAllBytes(objfileinfo.FullName)
                    If objfileinfo.Exists Then objfileinfo.Delete()

                    Response.Clear()
                    Response.AddHeader("content-disposition", "attachment;filename=" & objFormModuleApproval.objSchemaModule.ModuleLabel.Replace(" ", "") & ".csv")
                    Response.Charset = ""
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Me.EnableViewState = False
                    'Response.ContentType = "application/ms-excel"
                    Response.ContentType = "text/csv"
                    Response.BinaryWrite(arrByte)
                    Response.End()
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))

                    ' 20220715 3745 Fix Error Parameter Approval jika user terdaftar di table NDSSettingAdditionalUser
                    'Using objtbl As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(" ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID inner join module on module.modulename=moduleapproval.modulename LEFT JOIN muser ON muser.userid = ModuleApproval.CreatedBy ", "PK_ModuleApproval_ID as Id, ModuleLabel, ModuleKey,moduleapproval.CreatedDate, moduleaction.ModuleActionName   ,moduleapproval.CreatedBy", Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.V2.Basic.SystemParameterBLL.GetPageSize, 0)
                    ' 20221027 Fix export because has workflow
                    Using objtbl As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(" ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID inner join module on module.modulename=moduleapproval.modulename LEFT JOIN muser ON muser.userid = ModuleApproval.CreatedBy LEFT JOIN MWorkFlow_Progress  on ModuleApproval.ModuleKey = MWorkFlow_Progress.FK_Unik_ID  OR CAST(ModuleApproval.PK_ModuleApproval_ID AS VARCHAR) + 'Import' = MWorkFlow_Progress.FK_Unik_ID ", "PK_ModuleApproval_ID as Id, ModuleLabel, ModuleKey,moduleapproval.CreatedDate, moduleaction.ModuleActionName   ,moduleapproval.CreatedBy", Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.V2.Basic.SystemParameterBLL.GetPageSize, 0)
                        '                        objFormModuleView.changeheaderName(objtbl)
                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("Approval")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.V2.Basic.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 0
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                intcolnumber = intcolnumber + 1
                                If item.DataType = GetType(Date) Then

                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                If item.DataType = GetType(Integer) Or item.DataType = GetType(Double) Or item.DataType = GetType(Long) Or item.DataType = GetType(Decimal) Or item.DataType = GetType(Single) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = "#,##0.00"
                                End If

                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                        End Using
                    End Using

                    Dim arrByte As Byte() = IO.File.ReadAllBytes(objfileinfo.FullName)
                    If objfileinfo.Exists Then objfileinfo.Delete()

                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objFormModuleApproval.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(arrByte)
                    Response.End()
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)

                    ' 20220715 3745 Fix Error Parameter Approval jika user terdaftar di table NDSSettingAdditionalUser
                    'Using objtbl As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(" ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID inner join module on module.modulename=moduleapproval.modulename LEFT JOIN muser ON muser.userid = ModuleApproval.CreatedBy ", "PK_ModuleApproval_ID as Id, ModuleLabel, ModuleKey,moduleapproval.CreatedDate, moduleaction.ModuleActionName   ,moduleapproval.CreatedBy", Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.V2.Basic.SystemParameterBLL.GetPageSize, 0)
                    ' 20221027 Fix export because has workflow
                    Using objtbl As Data.DataTable = NawaBLL.V2.SQLHelper.ExecuteTabelPaging(" ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID inner join module on module.modulename=moduleapproval.modulename LEFT JOIN muser ON muser.userid = ModuleApproval.CreatedBy LEFT JOIN MWorkFlow_Progress  on ModuleApproval.ModuleKey = MWorkFlow_Progress.FK_Unik_ID  OR CAST(ModuleApproval.PK_ModuleApproval_ID AS VARCHAR) + 'Import' = MWorkFlow_Progress.FK_Unik_ID ", "PK_ModuleApproval_ID as Id, ModuleLabel, ModuleKey,moduleapproval.CreatedDate, moduleaction.ModuleActionName   ,moduleapproval.CreatedBy", Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.V2.Basic.SystemParameterBLL.GetPageSize, 0)
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                    End Using

                    Dim arrByte As Byte() = IO.File.ReadAllBytes(objfileinfo.FullName)
                    If objfileinfo.Exists Then objfileinfo.Delete()

                    Response.Clear()
                    Response.AddHeader("content-disposition", "attachment;filename=" & objFormModuleApproval.objSchemaModule.ModuleLabel.Replace(" ", "") & ".csv")
                    Response.Charset = ""
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Me.EnableViewState = False
                    'Response.ContentType = "application/ms-excel"
                    Response.ContentType = "text/csv"
                    Response.BinaryWrite(arrByte)
                    Response.End()
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            'Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        Finally
            If Not objfileinfo Is Nothing Then
                If objfileinfo.Exists Then objfileinfo.Delete()
            End If
        End Try
    End Sub

    Protected Function GridHeader1_CreateFilterableField(sender As Object, column As ColumnBase, defaultField As Field) As Field

        If String.IsNullOrEmpty(column.XType) Then

            defaultField.CustomConfig.Add(New ConfigItem("getValue", "getColumnValue", ParameterMode.Raw))

            'ElseIf column.XType = "booleancolumn" Then

            '    defaultField.CustomConfig.Add(New ConfigItem("getValue", "getColumnValue", ParameterMode.Raw))

        End If

        Return defaultField

    End Function

End Class

