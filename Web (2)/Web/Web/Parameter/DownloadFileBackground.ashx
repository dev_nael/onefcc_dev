﻿<%@ WebHandler Language="VB" Class="DownloadFileBackground"  %>

Imports System
Imports System.Web
Imports NawaDAL

Public Class DownloadFileBackground : Implements IHttpHandler, System.Web.SessionState.IReadOnlySessionState
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Try
            Dim sProcessID As String = HttpUtility.HtmlEncode(context.Request.Params("ProcessID"))
            Dim processID As Long = Convert.ToInt64(NawaBLL.V2.Common.Common.DecryptQueryString(sProcessID, NawaBLL.V2.SystemParameterBLL.SystemParameterBLL.GetEncriptionKey()))

            Dim message As String = NawaBLL.V2.BackgroundProcessBLL.BackgroundProcessBLL.GetBackgroundProcessMessage(processID)
            Dim byteArray As Byte() = NawaBLL.V2.BackgroundProcessBLL.BackgroundProcessBLL.GetBackgroundProcessData(processID)

            message = message.Split(New String() {"Generate File "}, StringSplitOptions.None)(1).Split(".")(0) & "." & message.Split(New String() {"Generate File "}, StringSplitOptions.None)(1).Split(".")(1)

            context.Response.Clear()
            context.Response.ClearHeaders()
            context.Response.AddHeader("content-disposition", "attachment;filename=" & message)
            context.Response.Charset = ""
            context.Response.AddHeader("cache-control", "max-age=0")

            context.Response.ContentType = System.Web.MimeMapping.GetMimeMapping(message)
            context.Response.BinaryWrite(byteArray)
            context.Response.End()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try



    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class