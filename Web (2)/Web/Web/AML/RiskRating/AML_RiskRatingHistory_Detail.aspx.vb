﻿
Imports System.Data
Imports System.Globalization
Imports Elmah
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL
Imports OfficeOpenXml

Partial Class AML_RiskRatingHistory_RiskRatingHistoryDetail
    Inherits ParentPage
    Public objFormModuleView As NawaBLL.FormModuleView

    Public Property IDModule() As String
        Get
            Return Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.IDModule")
        End Get
        Set(ByVal value As String)
            Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.IDModule") = value
        End Set
    End Property



    Public Property IDUnik() As Long
        Get
            Return Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.IDUnik") = value
        End Set
    End Property

    Private Sub ClearSession()
        Try
            IDModule = Nothing
            IDUnik = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub AML_RiskRatingHistory_RiskRatingHistoryDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim strmodule As String = Request.Params("ModuleID")
            Dim intmodule As Integer = Common.DecryptQueryString(strmodule, SystemParameterBLL.GetEncriptionKey)
            Me.ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)
            If ObjModule IsNot Nothing Then
                If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                    Dim strIDCode As String = 1
                    strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If
            Else
                Throw New Exception("Invalid Module ID")
            End If
            Dim dataStr As String = Request.Params("ID")
            If dataStr IsNot Nothing Then
                IDUnik = Common.DecryptQueryString(dataStr, SystemParameterBLL.GetEncriptionKey)
            End If
            If Not IDUnik = 0 Then
                LoadData(IDUnik)
            Else
                Throw New ApplicationException("An Error Occurred While Loading the Data With Id")
            End If

            'objFormModuleView.ModuleName = "vw_AML_RISK_RATING_HISTORY_DETAIL_REPORT"

            'objFormModuleView.AddField("PK_AML_RISK_RATING_RESULT_HISTORY_ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.BIGINTValue, , , , , )
            'objFormModuleView.AddField("FK_AML_Creation_Branch_Code", "KODE CABANG", 2, True, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
            'objFormModuleView.AddField("REGION_NAME", "NAMA CABANG", 3, True, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
            'objFormModuleView.AddField("REGION_CODE", "KANTOR WILAYAH", 4, True, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
            'objFormModuleView.AddField("CIF_NO", "CIF No", 5, True, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
            'objFormModuleView.AddField("CUSTOMERNAME", "Nama Nasabah", 6, True, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
            'objFormModuleView.AddField("Customer_Type_Name", "Tipe Nasabah", 7, True, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
            'objFormModuleView.AddField("AML_PEKERJAAN_Name", "Pekerjaan", 8, True, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
            'objFormModuleView.AddField("AML_BIDANG_USAHA_Name", "Bidang Usaha", 9, True, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
            'objFormModuleView.AddField("AML_COUNTRY_Name", "Negara", 10, True, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
            'objFormModuleView.AddField("PROPINSI", "Wilayah", 11, True, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
            'objFormModuleView.AddField("AML_DELIVERY_CHANNELS_NAME", "Jaringan Distribusi", 12, True, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
            'objFormModuleView.AddField("SCORE_AWAL", "Score Awal", 13, True, True, NawaBLL.Common.MFieldType.INTValue, , , , , )
            'objFormModuleView.AddField("SCORE_AKHIR", "Score Akhir", 14, True, True, NawaBLL.Common.MFieldType.INTValue, , , , , )
            'objFormModuleView.AddField("RikRatingNew", "Profil Risiko Awal", 15, True, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
            'objFormModuleView.AddField("RikRatingOld", "Profil Risiko Akhir", 16, True, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
            'objFormModuleView.AddField("TANGGAL_PERUBAHAN_RISIKO", "Tanggal Perubahan Risiko", 17, True, True, NawaBLL.Common.MFieldType.DATETIMEValue, , , , , )
            'objFormModuleView.AddField("TANGGAL_PEMBUATAN_CIF", "Created Date CIF", 18, True, True, NawaBLL.Common.MFieldType.DATETIMEValue, , , , , )
            'objFormModuleView.AddField("LastUpdateDate", "Last Maintenance Date", 19, True, True, NawaBLL.Common.MFieldType.DATETIMEValue, , , , , )
            'objFormModuleView.SettingFormView()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Property listreportriskratingdetail As List(Of NawaDevDAL.AML_RISK_RATING_RESULT_HISTORY)
        Get
            Return Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listreportriskratingproduct")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_RISK_RATING_RESULT_HISTORY))
            Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listreportriskratingproduct") = value
        End Set
    End Property
    Public Property listAccount As List(Of NawaDevDAL.AML_ACCOUNT)
        Get
            Return Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listaccount")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_ACCOUNT))
            Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listaccount") = value
        End Set
    End Property

    Public Property listreportriskrating As List(Of NawaDevDAL.AML_WATCHLIST)
        Get
            Return Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listreportriskrating")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_WATCHLIST))
            Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listreportriskrating") = value
        End Set
    End Property

    Public Property listreportanalisa As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
        Get
            Return Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listreportanalisa")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL))
            Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listreportanalisa") = value
        End Set
    End Property

    Public Property listreportriskratingdelivery As List(Of NawaDevDAL.AML_DELIVERY_CHANNELS)
        Get
            Return Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listreportriskratingdelivery")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_DELIVERY_CHANNELS))
            Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listreportriskratingdelivery") = value
        End Set
    End Property

    Public Property listreportriskratingproduct As List(Of NawaDevDAL.AML_PRODUCT)
        Get
            Return Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listreportriskratingproduct")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_PRODUCT))
            Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listreportriskratingproduct") = value
        End Set
    End Property


    Private Sub LoadData(ids As Long)
        Try

            'Using objdb As New NawaDatadevEntities
            '    'Karena 1 CIF bisa banyak Risk Rating History cari dulu CIF No ke tabel Risk Rating
            '    Dim objRisk = objdb.AML_RISK_RATING_RESULT_HISTORY.Where(Function(x) x.PK_AML_RISK_RATING_RESULT_HISTORY_ID = ids).FirstOrDefault
            '    If Not objRisk Is Nothing Then
            '        Dim objCustomer = objdb.AML_CUSTOMER.Where(Function(x) x.CIFNo = objRisk.CIF_NO).FirstOrDefault

            '        'Show data customer
            '        If Not objCustomer Is Nothing Then
            '            df_CIFNo.Text = objCustomer.CIFNo
            '            df_CustomerName.Text = objCustomer.CUSTOMERNAME

            '            'Show data Cabang
            '            Dim objCabang = objdb.AML_BRANCH.Where(Function(x) x.FK_AML_BRANCH_CODE = objCustomer.FK_AML_Creation_Branch_Code).FirstOrDefault
            '            If Not objCabang Is Nothing Then
            '                df_Branch.Text = (objCustomer.FK_AML_Creation_Branch_Code & " - " & objCabang.BRANCH_NAME).Trim
            '            End If

            '            'Show data Alamat dan Propinsi
            '            Dim objAddress = objdb.AML_CUSTOMER_ADDRESS.Where(Function(x) x.CIFNO = objCustomer.CIFNo).FirstOrDefault
            '            If Not objAddress Is Nothing Then
            '                df_Province.Text = objAddress.PROPINSI
            '                df_Address.Text = objAddress.CUSTOMER_ADDRESS
            '            End If

            '            'Show data Identitas
            '            Dim objIdentity = objdb.AML_CUSTOMER_IDENTITY.Where(Function(x) x.CIFNO = objCustomer.CIFNo).FirstOrDefault
            '            If Not objIdentity Is Nothing Then
            '                df_Identity.Text = objIdentity.CUSTOMER_IDENTITY_NUMBER
            '            End If

            '            'Show Birth Place dan Birth Date
            '            If Not String.IsNullOrEmpty(objCustomer.FK_AML_Customer_Type_Code) AndAlso objCustomer.FK_AML_Customer_Type_Code.Contains("individu") Then
            '                df_BirthPlace.Text = objCustomer.PLACEOFBIRTH
            '                df_BirthDate.Text = CDate(objCustomer.DATEOFBIRTH).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture)

            '                If Not String.IsNullOrEmpty(objCustomer.FK_AML_PEKERJAAN_CODE) Then
            '                    Dim objOccupation = objdb.AML_PEKERJAAN.Where(Function(x) x.FK_AML_PEKERJAAN_Code = objCustomer.FK_AML_PEKERJAAN_CODE).FirstOrDefault
            '                    If Not objOccupation Is Nothing Then
            '                        df_Occupation.Text = objOccupation.AML_PEKERJAAN_Name
            '                    End If
            '                End If

            '                df_BirthPlace.Hidden = False
            '                df_BirthDate.Hidden = False
            '                df_Occupation.Hidden = False
            '            Else
            '                df_BirthPlace.Hidden = True
            '                df_BirthDate.Hidden = True
            '                df_Occupation.Hidden = True
            '            End If

            '            'Show data Citizenship
            '            If Not String.IsNullOrEmpty(objCustomer.FK_AML_CITIZENSHIP_CODE) Then
            '                Dim objCitizenship = objdb.AML_COUNTRY.Where(Function(x) x.FK_AML_COUNTRY_Code = objCustomer.FK_AML_CITIZENSHIP_CODE).FirstOrDefault
            '                If Not objCitizenship Is Nothing Then
            '                    df_Citizenship.Text = objCitizenship.AML_COUNTRY_Name
            '                End If
            '            End If
            '        End If

            '        'Load Risk Rating History
            '        Dim dtHistory As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM AML_RISK_RATING_RESULT_HISTORY WHERE CIF_NO='" & objCustomer.CIFNo.Trim & "'")
            '        gp_RiskRatingHistory.GetStore.DataSource = dtHistory
            '        gp_RiskRatingHistory.GetStore.DataBind()
            '    End If
            'End Using
            Session("PK_RISK_RATING_RESULT_HISTORY") = ids
            Dim Customer As AML_RISK_RATING_RESULT_HISTORY = NawaDevBLL.AML_RiskRatingHistoryBLL.GetAMLRiskHistoryHeader(ids)
            With Customer
                listAccount = NawaDevBLL.AMLReportRiskRating.GetAMLAccountList(.CIF_NO)

                'listreportriskratingproduct = AMLReportRiskRating.GetAMLProductList(.FK_AML_PRODUCT_CODE)
                '    If listreportriskratingproduct IsNot Nothing Then
                '        BindProduct(StoreProductItem, listreportriskratingproduct)
                '    End If
                Dim productlist As New List(Of AML_PRODUCT)
                For Each item In listAccount
                    Dim aList As AML_PRODUCT = NawaDevBLL.AMLReportRiskRating.GetProductByCIF(item.FK_AML_PRODUCT_CODE)
                    productlist.Add(aList)

                Next
                If productlist IsNot Nothing Then
                    BindProduct(StoreProductItem, productlist)
                End If

                Dim Customer2 As AML_CUSTOMER = NawaDevBLL.AMLReportRiskRating.GetCustomerBYCIFNO(.CIF_NO)
                If Customer2 IsNot Nothing Then
                    With Customer2
                        Dim branch As AML_BRANCH = NawaDevBLL.AMLReportRiskRating.GetBranchBYFK(.FK_AML_Creation_Branch_Code)
                        If branch IsNot Nothing Then
                            DisplayKodeCabang.Value = .FK_AML_Creation_Branch_Code
                            DisplayNamaCabang.Value = branch.REGION_NAME
                            DisplayKantorWilayah.Value = branch.REGION_CODE
                        Else
                            DisplayKodeCabang.Value = Nothing
                            DisplayNamaCabang.Value = Nothing
                            DisplayKantorWilayah.Value = Nothing
                        End If
                        DisplayCIFNO.Value = .CIFNo
                        DisplayCustomerName.Value = .CUSTOMERNAME
                        Dim customertype As AML_CUSTOMER_TYPE = NawaDevBLL.AMLReportRiskRating.GetCustomerTypeFK(.FK_AML_Customer_Type_Code)
                        If customertype IsNot Nothing Then
                            DisplayTipeNasabah.Value = customertype.Customer_Type_Name
                        Else
                            DisplayTipeNasabah.Value = Nothing
                        End If

                        Dim pekerjaan As AML_PEKERJAAN = NawaDevBLL.AMLReportRiskRating.GetPekerjaanFK(.FK_AML_PEKERJAAN_CODE)
                        If pekerjaan IsNot Nothing Then
                            If pekerjaan.IS_PEP = 0 Then
                                DisplayPEP.Value = "Tidak"
                            Else
                                DisplayPEP.Value = "Ya"
                            End If
                            DisplayPekerjaan.Value = pekerjaan.AML_PEKERJAAN_Name
                        Else
                            DisplayPEP.Value = Nothing
                            DisplayPekerjaan = Nothing
                        End If
                        DisplayJumlahNominal = Nothing
                        Dim bidangusaha As AML_BIDANG_USAHA = NawaDevBLL.AMLReportRiskRating.GetBidangUsahaByFK(.FK_AML_INDUSTRY_CODE)
                        If bidangusaha IsNot Nothing Then
                            DisplayBidangUsaha.Value = bidangusaha.AML_BIDANG_USAHA_Name
                        Else
                            DisplayBidangUsaha.Value = Nothing
                        End If
                        Dim country As AML_COUNTRY = NawaDevBLL.AMLReportRiskRating.GetCountryByFK(.FK_AML_CITIZENSHIP_CODE)
                        If country IsNot Nothing Then
                            DisplayNegara.Value = country.AML_COUNTRY_Name
                        Else
                            DisplayNegara.Value = Nothing
                        End If
                        Dim prov As AML_CUSTOMER_ADDRESS = NawaDevBLL.AMLReportRiskRating.GetAdressBYCIFNO(.CIFNo)
                        If prov IsNot Nothing Then
                            DisplayWilayah.Value = prov.PROPINSI
                        Else
                            DisplayWilayah.Value = Nothing
                        End If
                        If .CreatedDate IsNot Nothing Then
                            DisplayFieldCIFDateCreate.Value = .CreatedDate.Value.ToString("dd-MMM-yyyy")
                        Else
                            DisplayFieldCIFDateCreate.Value = Nothing
                        End If

                    End With
                End If


                Dim stops As Integer = 0
                Dim Hitungs As Integer = 0
                Dim HitungsCifstop As Integer = 1
                listreportriskratingdetail = NawaDevBLL.AML_RiskRatingHistoryBLL.GetAMLHistoryList(.CIF_NO)
                If listreportriskratingdetail IsNot Nothing Then
                    For Each item In listreportriskratingdetail
                        If item.CIF_NO = .CIF_NO Then
                            If stops < 1 Then
                                DisplayFieldScoreAwal.Value = item.RISK_RATING_TOTAL_SCORE
                                DisplayFieldProfilResikoAwal.Value = item.RISK_RATING_NAME
                                stops = stops + 1
                            End If
                        End If
                    Next
                    For Each item In listreportriskratingdetail
                        If item.CIF_NO = .CIF_NO Then
                            Hitungs = Hitungs + 1
                        End If
                    Next
                    For Each item In listreportriskratingdetail
                        If item.CIF_NO = .CIF_NO Then
                            If HitungsCifstop = Hitungs Then
                                DisplayFieldScoreAkhir.Value = item.RISK_RATING_TOTAL_SCORE
                                DisplayFieldTanggalPerubahanRisiko.Value = item.CreatedDate.Value.ToString("dd-MMM-yyyy")
                                If item.PK_AML_RISK_RATING_RESULT_HISTORY_ID = .PK_AML_RISK_RATING_RESULT_HISTORY_ID Then
                                    DisplayFieldProfilResikoAkhir.Value = .RISK_RATING_NAME
                                End If

                            End If
                            HitungsCifstop = HitungsCifstop + 1
                        End If
                    Next
                Else
                    DisplayFieldScoreAwal.Value = Nothing
                    DisplayFieldProfilResikoAwal.Value = Nothing
                    DisplayFieldScoreAkhir.Value = Nothing
                    DisplayFieldProfilResikoAkhir.Value = Nothing
                End If
                DisplayFieldProfilRisikoT24.Value = Nothing

                If .LastUpdateDate IsNot Nothing Then
                    DisplayFieldLastMaintananceDate.Value = .LastUpdateDate.Value.ToString("dd-MMM-yyyy")
                Else
                    DisplayFieldLastMaintananceDate.Value = Nothing
                End If

                Dim stops2 As Integer = 0
                listreportriskratingdelivery = AMLReportRiskRating.GetAMLDeliveryList(.CIF_NO)
                If listreportriskratingdelivery IsNot Nothing Then
                    For Each item In listreportriskratingdelivery
                        If item.FK_AML_AML_DELIVERY_CHANNELS_CODE = Customer2.FK_AML_DELIVERY_CHANNELS_CODE Then
                            If stops2 < 1 Then
                                DisplayDeliveryChannel.Text = item.AML_DELIVERY_CHANNELS_NAME
                                stops2 = stops2 + 1
                            End If
                        End If
                    Next
                End If
                stops2 = 0
                listreportriskrating = AMLReportRiskRating.GetAMLScreeningList(.CIF_NO)
                If listreportriskrating IsNot Nothing Then
                    BindJudgement(StoreWatchlist, listreportriskrating)
                End If
                Dim screening = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementHeaderByCIFNO(.CIF_NO)
                If screening IsNot Nothing Then
                    listreportanalisa = AMLJudgementBLL.GetAMLJudgementList(screening.PK_AML_SCREENING_RESULT_ID)
                    If listreportanalisa IsNot Nothing Then
                        BindJudgementAnalisa(StoreAnlisaTransaksi, listreportanalisa)
                    End If
                End If

                'listreportriskratingdelivery = AMLReportRiskRating.GetAMLDeliveryList(idheader)
                'If listreportriskratingdelivery IsNot Nothing Then
                '    BindDevlivery(Delivery_Channel_Item, listreportriskratingdelivery)
                'End If

                stops = 0
                Hitungs = 0
                HitungsCifstop = 0
                listreportriskratingdetail = Nothing
                listreportriskratingproduct = Nothing
                listreportriskrating = Nothing
                Dim dtHistory As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM AML_RISK_RATING_RESULT_HISTORY WHERE CIF_NO='" & .CIF_NO.Trim & "'")
                gp_RiskRatingHistory.GetStore.DataSource = dtHistory
                gp_RiskRatingHistory.GetStore.DataBind()
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Shared Function GetAList(aList As List(Of AML_PRODUCT)) As List(Of AML_PRODUCT)
        Return aList
    End Function

    Private Sub BindJudgement(store As Store, list As List(Of AML_WATCHLIST))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_CATEGORY", GetType(String)))
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_TYPE", GetType(String)))

            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    If Not IsDBNull(item("FK_AML_WATCHLIST_CATEGORY_ID")) Then
                        item("AML_WATCHLIST_CATEGORY") = AMLJudgementBLL.GetWatchlistCategoryByID(item("FK_AML_WATCHLIST_CATEGORY_ID"))
                    Else
                        item("AML_WATCHLIST_CATEGORY") = ""
                    End If
                    If Not IsDBNull(item("FK_AML_WATCHLIST_TYPE_ID")) Then
                        item("AML_WATCHLIST_TYPE") = AMLJudgementBLL.GetWatchlistTypeByID(item("FK_AML_WATCHLIST_TYPE_ID"))
                    Else
                        item("AML_WATCHLIST_TYPE") = ""
                    End If

                    If Not IsDBNull(item("NAME")) Then
                        item("NAME") = item("NAME")
                    Else
                        item("NAMEE") = ""
                    End If

                    If Not IsDBNull(item("PK_AML_WATCHLIST_ID")) Then
                        item("PK_AML_WATCHLIST_ID") = item("PK_AML_WATCHLIST_ID")
                    Else
                        item("PK_AML_WATCHLIST_ID") = ""
                    End If
                Next
            End If
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindJudgementAnalisa(store As Store, list As List(Of AML_SCREENING_RESULT_DETAIL))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_CATEGORY", GetType(String)))
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_TYPE", GetType(String)))
            objtable.Columns.Add(New DataColumn("ISMATCH", GetType(String)))
            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    If Not IsDBNull(item("FK_AML_WATCHLIST_CATEGORY_ID")) Then
                        item("AML_WATCHLIST_CATEGORY") = AMLJudgementBLL.GetWatchlistCategoryByID(item("FK_AML_WATCHLIST_CATEGORY_ID"))
                    Else
                        item("AML_WATCHLIST_CATEGORY") = ""
                    End If
                    If Not IsDBNull(item("FK_AML_WATCHLIST_TYPE_ID")) Then
                        item("AML_WATCHLIST_TYPE") = AMLJudgementBLL.GetWatchlistTypeByID(item("FK_AML_WATCHLIST_TYPE_ID"))
                    Else
                        item("AML_WATCHLIST_TYPE") = ""
                    End If

                    If Not IsDBNull(item("NAME")) Then
                        item("NAME") = item("NAME")
                    Else
                        item("NAMEE") = ""
                    End If
                    If Not IsDBNull(item("JUDGEMENT_ISMATCH")) Then
                        If item("JUDGEMENT_ISMATCH") Then
                            item("ISMATCH") = "Match"
                        Else
                            item("ISMATCH") = "Not Match"
                        End If
                    Else
                        item("ISMATCH") = ""
                    End If

                Next
            End If
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindDevlivery(store As Store, list As List(Of AML_DELIVERY_CHANNELS))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub BindProduct(store As Store, list As List(Of AML_PRODUCT))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)

            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Private Sub AML_RiskRatingHistory_RiskRatingHistoryDetail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, bsettingRight As Integer)
        Try
            If bsettingRight = 1 Then
                'GridPanelReportIndicator.ColumnModel.Columns.Add(CommandColumn87)
            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
                'GridPanelReportIndicator.ColumnModel.Columns.RemoveAt(GridPanelReportIndicator.ColumnModel.Columns.Count - 1)
                'GridPanelReportIndicator.ColumnModel.Columns.Insert(1, CommandColumn87)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SetCommandColumnLocation()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        ColumnActionLocation(gp_RiskRatingHistory, CommandColumnRiskRatingHistoryItem, bsettingRight)
    End Sub

    Private Sub ClearOBJDetail()
        Try
            df_CIFNo_Detail.Clear()
            df_CustomerName_Detail.Clear()
            df_RISK_RATING_NAME.Clear()
            df_RISK_RATING_TOTAL_SCORE.Clear()
            df_CreatedDate.Clear()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Protected Sub GridcommandRiskItem(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                ClearOBJDetail()
                LoadRiskHistoryDetail(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadRiskHistoryDetail(id As String)
        Try
            'Show data Risk Rating History
            Using objdb As New NawaDatadevEntities
                Dim objRiskRating = objdb.AML_RISK_RATING_RESULT_HISTORY.Where(Function(x) x.PK_AML_RISK_RATING_RESULT_HISTORY_ID = id).FirstOrDefault
                If Not objRiskRating Is Nothing Then
                    df_CIFNo_Detail.Text = objRiskRating.CIF_NO

                    Dim objCustomer = objdb.AML_CUSTOMER.Where(Function(x) x.CIFNo = objRiskRating.CIF_NO).FirstOrDefault
                    If Not objCustomer Is Nothing Then
                        df_CustomerName_Detail.Text = objCustomer.CUSTOMERNAME
                    End If

                    df_RISK_RATING_NAME.Text = objRiskRating.RISK_RATING_NAME
                    df_RISK_RATING_TOTAL_SCORE.Text = CDbl(objRiskRating.RISK_RATING_TOTAL_SCORE).ToString("#,##0")
                    df_CreatedDate.Text = CDate(objRiskRating.CreatedDate).ToString("dd-MMM-yyyy HH:mm:ss", CultureInfo.InvariantCulture)
                End If
            End Using

            Dim dtRiskDetail As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT arrq.RISK_RATING_QUERY_NAME, arrd.RISK_RATING_DETAIL_VALUE, arrd.RISK_RATING_SCORE FROM AML_RISK_RATING_RESULT_DETAIL_HISTORY arrd JOIN AML_RISK_RATING_QUERY AS arrq ON arrd.FK_RISK_RATING_QUERY_ID = arrq.PK_AML_RISK_RATING_QUERY_ID WHERE FK_AML_RISK_RATING_RESULT_HISTORY=" & id)
            If dtRiskDetail Is Nothing Then
                dtRiskDetail = New DataTable
            End If
            gp_RiskRatingDetail.GetStore.DataSource = dtRiskDetail
            gp_RiskRatingDetail.GetStore.DataBind()

            WindowRiskRatingHistoryDetail.Hidden = False

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnRiskCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowRiskRatingHistoryDetail.Hidden = True
            ClearOBJDetail()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM vw_AML_RISK_RATING_HISTORY_DETAIL_REPORT where PK_AML_RISK_RATING_RESULT_HISTORY_ID = '" & Session("PK_RISK_RATING_RESULT_HISTORY").ToString & "'",)

                        'objFormModuleView.changeHeader(objtbl)
                        'For Each item As Ext.Net.ColumnBase In objtbl.Columns

                        '    If item.Hidden Then 
                        '        If objtbl.Columns.Contains(item.DataIndex) Then
                        '            objtbl.Columns.Remove(item.DataIndex)
                        '        End If

                        '    End If
                        'Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("Data")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM vw_AML_RISK_RATING_HISTORY_DETAIL_REPORT where PK_AML_RISK_RATING_RESULT_HISTORY_ID = '" & Session("PK_RISK_RATING_RESULT_HISTORY").ToString & "'",)

                        'objFormModuleView.changeHeader(objtbl)
                        'For Each item As Ext.Net.ColumnBase In objtbl.Columns

                        '    If item.Hidden Then
                        '        If objtbl.Columns.Contains(item.DataIndex) Then
                        '            objtbl.Columns.Remove(item.DataIndex)
                        '        End If

                        '    End If
                        'Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

End Class
