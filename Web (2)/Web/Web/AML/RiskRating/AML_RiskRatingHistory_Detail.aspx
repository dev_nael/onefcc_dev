﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AML_RiskRatingHistory_Detail.aspx.vb" Inherits="AML_RiskRatingHistory_RiskRatingHistoryDetail" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <%--Begin Update Penambahan Advance Filter--%>
       
    
     <ext:FormPanel ID="PanelInfo" runat="server" Title="Risk Rating History" BodyStyle="padding:10px"  ButtonAlign="Center" Scrollable="Both">
        <Items>
            <ext:GridPanel ID="GridPanel1" runat="server" MarginSpec="0 0 0 0">
             <DockedItems>
           
            <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                <Items>
                    <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export :">
                        <Items>
                            <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                            <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                        </Items>
                    </ext:ComboBox>
                    <ext:Button runat="server" ID="BtnExportAll" Text="Download Detail" AutoPostBack="true" Icon="Disk" OnClick="ExportAllExcel" />
                    </Items>
                </ext:Toolbar>
            </DockedItems>
                </ext:GridPanel>
           
            <ext:Panel ID="panelCustomerInfo" runat="server" Collapsible="true" BodyPadding="10" Title="Customer Information" Layout="ColumnLayout">
                <Items>
                    <ext:FieldSet runat="server" ColumnWidth="0.5" Border="false" Layout="AnchorLayout">
                        <Items>
                             <ext:DisplayField ID="DisplayKodeCabang" runat="server" FieldLabel="Kode Cabang">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayNamaCabang" runat="server" FieldLabel="Nama Cabang">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayKantorWilayah" runat="server" FieldLabel="Kantor Wilayah">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayCIFNO" runat="server" FieldLabel="CIF No">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayCustomerName" runat="server" FieldLabel="Nama Nasabah">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayTipeNasabah" runat="server" FieldLabel="Tipe Nasabah">
                                    </ext:DisplayField>
                             <ext:DisplayField ID="DisplayPekerjaan" runat="server" FieldLabel="Pekerjaan">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayBidangUsaha" runat="server" FieldLabel="Bidang Usaha">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayNegara" runat="server" FieldLabel="Negara">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayWilayah" runat="server" FieldLabel="Wilayah">
                                    </ext:DisplayField>
                            <ext:DisplayField ID="DisplayDeliveryChannel" runat="server" FieldLabel="Jaringan Distribusi">
                                    </ext:DisplayField>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet runat="server" ColumnWidth="0.5" Border="false" Layout="AnchorLayout">
                        <Items>
                             <ext:DisplayField ID="DisplayPEP" runat="server" FieldLabel="PEP" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayJumlahNominal" runat="server" FieldLabel="Jumlah Nominal DPK" Hidden="true">
                                    </ext:DisplayField>
                                    
                            <ext:DisplayField ID="DisplayFieldScoreAwal" runat="server" FieldLabel="Score Awal">
                                    </ext:DisplayField>
                            <ext:DisplayField ID="DisplayFieldScoreAkhir" runat="server" FieldLabel="Score Akhir">
                                    </ext:DisplayField>
                            <ext:DisplayField ID="DisplayFieldProfilResikoAwal" runat="server" FieldLabel="Profil Risiko Awal">
                                    </ext:DisplayField>
                            <ext:DisplayField ID="DisplayFieldProfilResikoAkhir" runat="server" FieldLabel="Profil Risiko Akhir">
                                    </ext:DisplayField>
                            <ext:DisplayField ID="DisplayFieldTanggalPerubahanRisiko" runat="server" FieldLabel="Tanggal Perubahaan Profil Risiko">
                                    </ext:DisplayField>
                            <ext:DisplayField ID="DisplayFieldPerubahanRisiko" runat="server" FieldLabel="Penyebab Perubahaan Profil Risiko">
                                    </ext:DisplayField>
                            <ext:DisplayField ID="DisplayFieldProfilRisikoT24" runat="server" FieldLabel="Profil Risiko T24">
                                    </ext:DisplayField>
                            <ext:DisplayField ID="DisplayFieldCIFDateCreate" runat="server" FieldLabel="Created Date CIF">
                                    </ext:DisplayField>
                            <ext:DisplayField ID="DisplayFieldLastMaintananceDate" runat="server" FieldLabel="Last Maintenance Date">
                                    </ext:DisplayField>
                        </Items>
                    </ext:FieldSet>
                </Items>
          </ext:Panel>
             <ext:GridPanel ID="GridPanelProductBank" runat="server" MarginSpec="0 0 0 0" Title="Produk Bank">
                <Store>
                    <ext:Store ID="StoreProductItem" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" IDProperty="PK_AML_PRODUCT_CODE_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_PRODUCT_CODE_ID" Type="Int"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_PRODUCT_CODE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="PRODUCT_NAME" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                        <ext:Column runat="server" Text="Product Code" DataIndex="FK_AML_PRODUCT_CODE" CellWrap="true" flex="1" />
                        <ext:Column runat="server" Text="Product Name" DataIndex="PRODUCT_NAME" CellWrap="true" flex="2"/>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
             <ext:GridPanel ID="GridPanelWatchlist" runat="server" MarginSpec="0 0 0 0" Title="Sumber Watchlist">
                <Store>
                    <ext:Store ID="StoreWatchlist" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                           <ext:Model runat="server" IDProperty="PK_AML_WATCHLIST_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_WATCHLIST_ID" Type="Int"></ext:ModelField>
                                    <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="AML_WATCHLIST_CATEGORY" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                        <ext:Column runat="server" Text="ID Watch List" DataIndex="PK_AML_WATCHLIST_ID" CellWrap="true" flex="1" />
                        <ext:Column runat="server" Text="Name Watch List" DataIndex="NAME" CellWrap="true" flex="1" />
                        <ext:Column runat="server" Text="Watch List Category" DataIndex="AML_WATCHLIST_CATEGORY" CellWrap="true" flex="1" />
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
              <ext:GridPanel ID="GridPanelAnalisaTransaksi" runat="server" MarginSpec="0 0 0 0" Title="Analisa Transaksi">
                <Store>
                    <ext:Store ID="StoreAnlisaTransaksi" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                           <ext:Model runat="server" IDProperty="PK_AML_SCREENING_RESULT_DETAIL_ID ">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_SCREENING_RESULT_DETAIL_ID" Type="Int"></ext:ModelField>
                                    <ext:ModelField Name="NAME_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="AML_WATCHLIST_CATEGORY" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ISMATCH" Type="String"></ext:ModelField>
                                     <ext:ModelField Name="FK_AML_WATCHLIST_ID" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                         <ext:Column ID="Column6" runat="server" DataIndex="FK_AML_WATCHLIST_ID" Text="Watchlist ID" Flex="1"></ext:Column>
                        <ext:Column runat="server" Text="Name Watch List" DataIndex="NAME_WATCHLIST" CellWrap="true" flex="1" />
                        <ext:Column runat="server" Text="Watch List Category" DataIndex="AML_WATCHLIST_CATEGORY" CellWrap="true" flex="1" />
                         <ext:Column runat="server" Text="Judgement Result" DataIndex="ISMATCH" CellWrap="true" flex="1" />
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
          <ext:GridPanel runat="server" ID="gp_RiskRatingHistory" Title="Risk Rating History">  
                <Store>
                    <ext:Store ID="StoreRiskRatingHistory" runat="server">
                        <Model>
                            <ext:Model runat="server" IDProperty="PK_AML_RISK_RATING_RESULT_HISTORY_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_RISK_RATING_RESULT_HISTORY_ID" Type="Auto"></ext:ModelField>
                                    <ext:ModelField Name="RISK_RATING_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="RISK_RATING_TOTAL_SCORE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedDate" Type="String"></ext:ModelField>                             
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                        <ext:Column runat="server" Text="Risk Rating" DataIndex="RISK_RATING_NAME" CellWrap="true" flex="1" />
                        <ext:Column runat="server" Text="Risk Rating Score" DataIndex="RISK_RATING_TOTAL_SCORE" CellWrap="true" flex="2"/>
                        <ext:DateColumn runat="server" Text="Created Date" DataIndex="CreatedDate" CellWrap="true" flex="1"  Format="dd-MMM-yyyy hh:mm:ss"/>
                          <ext:CommandColumn ID="CommandColumnRiskRatingHistoryItem" runat="server" Text="Action" CellWrap="true">
                            <Commands>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridcommandRiskItem">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_AML_RISK_RATING_RESULT_HISTORY_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                  <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            
        </Items>       
          <Buttons>
            <ext:Button ID="btnCancelReport" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>

    </ext:FormPanel>        
  
   

    <ext:Window ID="WindowRiskRatingHistoryDetail" Title="Risk Rating History Detail" runat="server" Modal="true" Maximizable="true"  Hidden="true" BodyPadding="10" Layout="AnchorLayout" BodyStyle="padding:10" Scrollable="Both" ButtonAlign="Center">
        <Items>
            <ext:FormPanel ID="GeneralInformation" runat="server" Collapsible="true" AnchorHorizontal ="100%" BodyPadding="10" Title="General Information" Layout="ColumnLayout">
                <Items>
                    <ext:FieldSet runat="server" Border="false" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField ID="df_CIFNo_Detail" runat="server" FieldLabel="CIF"></ext:DisplayField>
                            <ext:DisplayField ID="df_CustomerName_Detail" runat="server" FieldLabel="Customer Name"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet runat="server" Border="false" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField ID="df_RISK_RATING_NAME" runat="server" FieldLabel="Risk Rating"></ext:DisplayField>
                            <ext:DisplayField ID="df_RISK_RATING_TOTAL_SCORE" runat="server" FieldLabel="Risk Rating Score"></ext:DisplayField>
                            <ext:DisplayField ID="df_CreatedDate" runat="server" FieldLabel="Created Date"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:FormPanel>

            <ext:GridPanel ID="gp_RiskRatingDetail" runat="server" Title="Risk Rating Detail"   AnchorHorizontal ="100%" AutoScroll="true" Border="true" PaddingSpec="10 0">
                <Store>
                    <ext:Store ID="StoreRiskRatingDetail" runat="server" IsPagingStore="true" PageSize="4">
                        <Model>
                            <ext:Model ID="Model4" runat="server">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_RISK_RATING_RESULT_DETAIL_HISTORY_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="RISK_RATING_QUERY_NAME" Type="String"></ext:ModelField>                                       
                                    <ext:ModelField Name="RISK_RATING_DETAIL_VALUE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="RISK_RATING_SCORE" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                            <ext:Column ID="Column1" runat="server" DataIndex="RISK_RATING_QUERY_NAME" Text="Risk Rating Name" MinWidth="100" CellWrap="true" flex="1" ></ext:Column>                               
                            <ext:Column ID="Column9" runat="server" DataIndex="RISK_RATING_DETAIL_VALUE" Text="Risk Rating Value" MinWidth="100" CellWrap="true" flex="1" ></ext:Column>
                        <ext:Column ID="Column10" runat="server" DataIndex="RISK_RATING_SCORE" Text="Risk Rating Score" MinWidth="250" CellWrap="true" Flex="1"></ext:Column>
                        </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
        </Items>
      <Buttons>
            <ext:Button ID="BtnRiskCancel" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnRiskCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.9, height: size.height * 0.9});" />

            <Resize Handler="#{WindowRiskRatingHistoryDetail}.center()" />
        </Listeners>
    </ext:Window>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
       <%-- <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>--%>
    </ext:FormPanel>
   
</asp:Content>





