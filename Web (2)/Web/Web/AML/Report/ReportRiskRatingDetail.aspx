﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="ReportRiskRatingDetail.aspx.vb" Inherits="AML_Report_ReportRiskRatingDetail" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel ID="PanelInfo" runat="server" Title="Report Hasil Scoring dan Rating Profil Risiko Nasabah Detail" BodyStyle="padding:10px"  ButtonAlign="Center" Scrollable="Both">
        <Items>
            <ext:Panel runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="DisplayKodeCabang" runat="server" FieldLabel="Kode Cabang">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayNamaCabang" runat="server" FieldLabel="Nama Cabang">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayKantorWilayah" runat="server" FieldLabel="Kantor Wilayah">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayCIFNO" runat="server" FieldLabel="CIF No">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayCustomerName" runat="server" FieldLabel="Nama Nasabah">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayTipeNasabah" runat="server" FieldLabel="Tipe Nasabah">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayTujuanPembukaan" runat="server" FieldLabel="Tujuan Pembukaan Rekening">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="DisplayPEP" runat="server" FieldLabel="PEP">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayJumlahNominal" runat="server" FieldLabel="Jumlah Nominal DPK">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayPekerjaan" runat="server" FieldLabel="Pekerjaan">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayBidangUsaha" runat="server" FieldLabel="Bidang Usaha">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayNegara" runat="server" FieldLabel="Negara">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayWilayah" runat="server" FieldLabel="Wilayah">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayDeliveryChannel" runat="server" FieldLabel="Delivery Channel">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
          
            <ext:FormPanel ID="FormResponse" runat="server" Collapsible="true" BodyPadding="10" Title="Product Bank">
              <Items>
                    <ext:GridPanel ID="GridPanelProductBank" runat="server" MarginSpec="0 0 0 0">
                <Store>
                    <ext:Store ID="StoreProductItem" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" IDProperty="PK_AML_PRODUCT_CODE_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_PRODUCT_CODE_ID" Type="Int"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_PRODUCT_CODE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="PRODUCT_NAME" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                        <ext:Column runat="server" Text="Product Code" DataIndex="FK_AML_PRODUCT_CODE" CellWrap="true" flex="1" />
                        <ext:Column runat="server" Text="Product Name" DataIndex="PRODUCT_NAME" CellWrap="true" flex="2"/>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
                </Items>
      
    </ext:FormPanel>
             <ext:FormPanel ID="FormPanel1" runat="server" Collapsible="true" BodyPadding="10" Title="Sumber Watch List">
                 
              <Items>
                   <ext:GridPanel ID="GridPanelWatchlist" runat="server" MarginSpec="0 0 0 0">
                <Store>
                    <ext:Store ID="StoreWatchlist" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                           <ext:Model runat="server" IDProperty="PK_AML_WATCHLIST_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_WATCHLIST_ID" Type="Int"></ext:ModelField>
                                    <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="AML_WATCHLIST_CATEGORY" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                        <ext:Column runat="server" Text="ID Watch List" DataIndex="PK_AML_WATCHLIST_ID" CellWrap="true" flex="1" />
                        <ext:Column runat="server" Text="Name Watch List" DataIndex="NAME" CellWrap="true" flex="1" />
                        <ext:Column runat="server" Text="Watch List Category" DataIndex="AML_WATCHLIST_CATEGORY" CellWrap="true" flex="1" />
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
                    <ext:Panel runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="DisplayFieldSTR" runat="server" FieldLabel="STR">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldScoreAKhir" runat="server" FieldLabel="Score Akhir">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldResikoONEFCC" runat="server" FieldLabel="Profil Resiko OneFCC">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldResikoT24" runat="server" FieldLabel="Profil Resiko T24">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldCIFCreatedDate" runat="server" FieldLabel="Created Date CIF">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldLastMaintance" runat="server" FieldLabel="Last Maintenance Date">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                </Items>
               
      
    </ext:FormPanel>
            </Items>
        <Buttons >
         
            <ext:Button ID="btnCancelReport" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>

        
    </ext:FormPanel>
</asp:Content>




