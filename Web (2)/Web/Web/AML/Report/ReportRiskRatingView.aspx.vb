﻿Imports Ext.Net
Imports OfficeOpenXml
Imports NawaBLL
Imports System.Data
Imports NawaDAL

Partial Class AML_Report_ReportRiskRatingView

    Inherits Parent
    Public objFormModuleView As NawaBLL.FormModuleView
    Public Property strWhereClause() As String
        Get
            Return Session("AML_Report_ReportRiskRatingView.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("AML_Report_ReportRiskRatingView.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("AML_Report_ReportRiskRatingView.strSort")
        End Get
        Set(ByVal value As String)
            Session("AML_Report_ReportRiskRatingView.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("AML_Report_ReportRiskRatingView.indexStart")
        End Get
        Set(ByVal value As String)
            Session("AML_Report_ReportRiskRatingView.indexStart") = value
        End Set
    End Property
    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("AML_Report_ReportRiskRatingView.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AML_Report_ReportRiskRatingView.ObjModule") = value
        End Set
    End Property
    Public Property QueryTable() As String
        Get
            Return Session("AML_Report_ReportRiskRatingView.Table")
        End Get
        Set(ByVal value As String)
            Session("AML_Report_ReportRiskRatingView.Table") = value
        End Set
    End Property
    Public Property QueryField() As String
        Get
            Return Session("AML_Report_ReportRiskRatingView.Field")
        End Get
        Set(ByVal value As String)
            Session("AML_Report_ReportRiskRatingView.Field") = value
        End Set
    End Property


    'Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
    '    Dim objfileinfo As IO.FileInfo = Nothing
    '    Try
    '        If Not cboExportExcel.SelectedItem Is Nothing Then
    '            If cboExportExcel.SelectedItem.Value = "Excel" Then
    '                Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
    '                objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
    '                'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
    '                'Update BSIM 14Oct2020
    '                Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

    '                    objFormModuleView.changeHeader(objtbl)
    '                    For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

    '                        If item.Hidden Then
    '                            If objtbl.Columns.Contains(item.DataIndex) Then
    '                                objtbl.Columns.Remove(item.DataIndex)
    '                            End If

    '                        End If
    '                    Next

    '                    Using resource As New ExcelPackage(objfileinfo)
    '                        Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
    '                        ws.Cells("A1").LoadFromDataTable(objtbl, True)
    '                        Dim dateformat As String = SystemParameterBLL.GetDateFormat
    '                        Dim intcolnumber As Integer = 1
    '                        For Each item As DataColumn In objtbl.Columns
    '                            If item.DataType = GetType(Date) Then
    '                                ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
    '                            End If
    '                            intcolnumber = intcolnumber + 1
    '                        Next
    '                        ws.Cells(ws.Dimension.Address).AutoFitColumns()
    '                        resource.Save()
    '                        Response.Clear()
    '                        Response.ClearHeaders()
    '                        Response.ContentType = "application/vnd.ms-excel"
    '                        Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
    '                        Response.Charset = ""
    '                        Response.AddHeader("cache-control", "max-age=0")
    '                        Me.EnableViewState = False
    '                        Response.ContentType = "ContentType"
    '                        Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
    '                        Response.End()
    '                    End Using
    '                End Using
    '            ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
    '                Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
    '                objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
    '                Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
    '                'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
    '                'Update BSIM 14Oct2020
    '                Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

    '                    objFormModuleView.changeHeader(objtbl)
    '                    For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

    '                        If item.Hidden Then
    '                            If objtbl.Columns.Contains(item.DataIndex) Then
    '                                objtbl.Columns.Remove(item.DataIndex)
    '                            End If

    '                        End If
    '                    Next

    '                    For k As Integer = 0 To objtbl.Columns.Count - 1
    '                        'add separator
    '                        stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
    '                    Next
    '                    'append new line
    '                    stringWriter_Temp.Write(vbCr & vbLf)
    '                    For i As Integer = 0 To objtbl.Rows.Count - 1
    '                        For k As Integer = 0 To objtbl.Columns.Count - 1
    '                            'add separator
    '                            stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
    '                        Next
    '                        'append new line
    '                        stringWriter_Temp.Write(vbCr & vbLf)
    '                    Next
    '                    stringWriter_Temp.Close()
    '                    Response.Clear()
    '                    Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
    '                    Response.Charset = ""
    '                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '                    Me.EnableViewState = False
    '                    'Response.ContentType = "application/ms-excel"
    '                    Response.ContentType = "text/csv"
    '                    Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
    '                    Response.End()
    '                End Using
    '            Else
    '                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
    '            End If
    '        Else
    '            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    'Protected Sub ExportExcel(sender As Object, e As EventArgs)
    '    Dim objfileinfo As IO.FileInfo = Nothing
    '    Try
    '        If Not String.IsNullOrEmpty(cboExportExcel.SelectedItem.Value) Then
    '            If cboExportExcel.SelectedItem.Value = "Excel" Then
    '                Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
    '                objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
    '                'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, SystemParameterBLL.GetPageSize, 0)
    '                'Update BSIM - 14Oct2020
    '                Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

    '                    objFormModuleView.changeHeader(objtbl)
    '                    For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

    '                        If item.Hidden Then
    '                            If objtbl.Columns.Contains(item.DataIndex) Then
    '                                objtbl.Columns.Remove(item.DataIndex)
    '                            End If

    '                        End If
    '                    Next


    '                    Using resource As New ExcelPackage(objfileinfo)
    '                        Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
    '                        ws.Cells("A1").LoadFromDataTable(objtbl, True)
    '                        Dim dateformat As String = SystemParameterBLL.GetDateFormat
    '                        Dim intcolnumber As Integer = 1
    '                        For Each item As DataColumn In objtbl.Columns
    '                            If item.DataType = GetType(Date) Then
    '                                ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
    '                            End If
    '                            intcolnumber = intcolnumber + 1
    '                        Next
    '                        ws.Cells(ws.Dimension.Address).AutoFitColumns()
    '                        resource.Save()
    '                        Response.Clear()
    '                        Response.ClearHeaders()
    '                        Response.ContentType = "application/vnd.ms-excel"
    '                        Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
    '                        Response.Charset = ""
    '                        Response.AddHeader("cache-control", "max-age=0")
    '                        Me.EnableViewState = False
    '                        Response.ContentType = "ContentType"
    '                        Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
    '                        Response.End()
    '                    End Using
    '                End Using
    '                'Dim json As String = Me.Hidden1.Value.ToString()
    '                'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
    '                'Dim xml As XmlNode = eSubmit.Xml
    '                'Me.Response.Clear()
    '                'Me.Response.ContentType = "application/vnd.ms-excel"
    '                'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
    '                'Dim xtExcel As New Xsl.XslCompiledTransform()
    '                'xtExcel.Load(Server.MapPath("Excel.xsl"))
    '                'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
    '                'Me.Response.[End]()
    '            ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
    '                Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
    '                objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
    '                Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
    '                'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, SystemParameterBLL.GetPageSize, 0)
    '                'Update BSIM - 14Oct2020
    '                Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

    '                    objFormModuleView.changeHeader(objtbl)
    '                    For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

    '                        If item.Hidden Then
    '                            If objtbl.Columns.Contains(item.DataIndex) Then
    '                                objtbl.Columns.Remove(item.DataIndex)
    '                            End If

    '                        End If
    '                    Next
    '                    For k As Integer = 0 To objtbl.Columns.Count - 1
    '                        'add separator
    '                        stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
    '                    Next
    '                    'append new line
    '                    stringWriter_Temp.Write(vbCr & vbLf)
    '                    For i As Integer = 0 To objtbl.Rows.Count - 1
    '                        For k As Integer = 0 To objtbl.Columns.Count - 1
    '                            'add separator
    '                            stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
    '                        Next
    '                        'append new line
    '                        stringWriter_Temp.Write(vbCr & vbLf)
    '                    Next
    '                    stringWriter_Temp.Close()
    '                    Response.Clear()
    '                    Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
    '                    Response.Charset = ""
    '                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '                    Me.EnableViewState = False
    '                    'Response.ContentType = "application/ms-excel"
    '                    Response.ContentType = "text/csv"
    '                    Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
    '                    Response.End()
    '                End Using
    '            Else
    '                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
    '            End If
    '        Else
    '            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
    '        End If
    '    Catch ex As Exception
    '        Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
    '    Finally
    '        If Not objfileinfo Is Nothing Then
    '            objfileinfo.Delete()
    '        End If
    '    End Try
    'End Sub
    '<DirectMethod>
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub
    <DirectMethod>
    Public Sub BtnAdd_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim sm As RowSelectionModel = TryCast(Me.GridpanelView.GetSelectionModel(), RowSelectionModel)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AML_Report_ReportRiskRatingView_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub


    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer

            Try
                'Session untuk sequence data di menu questioneredit
                Session("IDquestionDetail") = Nothing
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)
                BtnAdd.Hidden = False

                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                    Dim strIDCode As String = 1
                    BtnAdd.Hidden = False

                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If

                objFormModuleView.ModuleID = ObjModule.PK_Module_ID
                objFormModuleView.ModuleName = "vw_AML_HASIL_SCORING_DAN_RISK_RATING"

                objFormModuleView.AddField("CIFNO", "CIF No", 1, True, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("CUSTOMERNAME", "Nama Nasabah", 2, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Date_Of_Birth", "Tanggal Lahir", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Opening_Date", "Tanggal Pembukaan CIF", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Tipe_Nasabah", "Tipe Nasabah", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("RISK_RATING", "Risk Rating", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("REGION_CODE", "Kanwil", 7, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("REGION_NAME", "Cabang", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("fk_aml_creation_branch_code", "Kode Cabang", 9, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.SettingFormView()

                Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
                'objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"
            Catch ex As Exception
            End Try

            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            Using objdb As New NawaDevDAL.NawaDatadevEntities
                'Begin Penambahan Advanced Filter
                If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                    Toolbar2.Hidden = True
                Else
                    Toolbar2.Hidden = False
                End If
                LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")

                Dim intStart As Integer = e.Start
                Dim intLimit As Int16 = e.Limit
                Dim inttotalRecord As Integer
                Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

                Dim strsort As String = ""
                For Each item As DataSorter In e.Sort
                    strsort += item.Property & " " & item.Direction.ToString
                Next
                Me.indexStart = intStart
                Me.strWhereClause = strfilter
                If strsort = "" Then
                    strsort = "CIFNO Desc"
                End If


                'Begin Update Penambahan Advance Filter
                If strWhereClause.Length > 0 Then
                    If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                        strWhereClause &= "And " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                        intStart = 0
                    End If
                Else
                    If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                        strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                        intStart = 0
                    End If
                End If



                Me.strOrder = strsort

                Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
                'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(" ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID inner join module on module.modulename=moduleapproval.modulename", "PK_ModuleApproval_ID, ModuleLabel, ModuleKey, moduleaction.ModuleActionName, ModuleApproval.CreatedBy, ModuleApproval.CreatedDate", strWhereClause, strsort, intStart, intLimit, inttotalRecord)
                ''-- start paging ------------------------------------------------------------
                Dim limit As Integer = e.Limit
                If (e.Start + e.Limit) > inttotalRecord Then
                    limit = inttotalRecord - e.Start
                End If
                'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
                ''-- end paging ------------------------------------------------------------
                e.Total = inttotalRecord
                GridpanelView.GetStore.DataSource = DataPaging
                GridpanelView.GetStore.DataBind()
            End Using
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


End Class

