﻿Imports System.Data
Imports System.Data.SqlClient
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDAL
Imports NawaDevBLL
Imports NawaDevDAL
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevBLL.GlobalReportFunctionBLL
Imports System.Text
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.IO

Public Module Extenders
    <System.Runtime.CompilerServices.Extension>
    Public Function ToDataTable(Of T)(collection As IEnumerable(Of T), tableName As String) As DataTable
        Dim tbl As DataTable = ToDataTable(collection)
        tbl.TableName = tableName
        Return tbl
    End Function

    <System.Runtime.CompilerServices.Extension>
    Public Function ToDataTable(Of T)(collection As IEnumerable(Of T)) As DataTable
        Dim dt As New DataTable()
        Dim tt As Type = GetType(T)
        Dim pia As PropertyInfo() = tt.GetProperties()
        'Create the columns in the DataTable
        For Each pi As PropertyInfo In pia
            Dim a =
            If(Nullable.GetUnderlyingType(pi.PropertyType), pi.PropertyType)
            dt.Columns.Add(pi.Name, If(Nullable.GetUnderlyingType(pi.PropertyType), pi.PropertyType))
            'If pi.PropertyType Is GetType(DateTime) Then

            '    ' pi.Columns.Add("columnName", TypeOf (datetime2)
            'Else

            'End If
        Next
        'Populate the table
        For Each item As T In collection
            Dim dr As DataRow = dt.NewRow()
            dr.BeginEdit()
            For Each pi As PropertyInfo In pia
                ' cek value apakah null?
                ' If pi.GetValue(item, Nothing) = Nothing Then
                If pi.GetValue(item, Nothing) Is Nothing Then
                    dr(pi.Name) = DBNull.Value
                Else
                    dr(pi.Name) = pi.GetValue(item, Nothing)
                End If

            Next
            dr.EndEdit()
            dt.Rows.Add(dr)
        Next
        Return dt
    End Function


End Module

Partial Class AML_Report_ReportRiskRatingDetail
    Inherits Parent

#Region "Session"
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AML_Report_ReportRiskRatingDetail.ObjModule") = value
        End Set
    End Property

    Public Property listreportriskrating As List(Of NawaDevDAL.AML_WATCHLIST)
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.listreportriskrating")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_WATCHLIST))
            Session("AML_Report_ReportRiskRatingDetail.listreportriskrating") = value
        End Set
    End Property

    Public Property listreportriskratingdelivery As List(Of NawaDevDAL.AML_DELIVERY_CHANNELS)
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.listreportriskratingdelivery")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_DELIVERY_CHANNELS))
            Session("AML_Report_ReportRiskRatingDetail.listreportriskratingdelivery") = value
        End Set
    End Property

    Public Property listreportriskratingproduct As List(Of NawaDevDAL.AML_PRODUCT)
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.listreportriskratingproduct")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_PRODUCT))
            Session("AML_Report_ReportRiskRatingDetail.listreportriskratingproduct") = value
        End Set
    End Property

    Public Property reportriskratingheader As NawaDevDAL.AML_SCREENING_RESULT
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.reportriskratingheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT)
            Session("AML_Report_ReportRiskRatingDetail.reportriskratingheader") = value
        End Set
    End Property
    Public Property reportriskratingrequestforheader As NawaDevDAL.AML_SCREENING_REQUEST
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.reportriskratingrequestforheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_REQUEST)
            Session("AML_Report_ReportRiskRatingDetail.reportriskratingrequestforheader") = value
        End Set
    End Property
    Public Property judgementdetail As NawaDevDAL.AML_SCREENING_RESULT_DETAIL
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.judgementdetail")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            Session("AML_Report_ReportRiskRatingDetail.judgementdetail") = value
        End Set
    End Property
    Public Property judgementdetailchange As NawaDevDAL.AML_SCREENING_RESULT_DETAIL
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.judgementdetailchange")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            Session("AML_Report_ReportRiskRatingDetail.judgementdetailchange") = value
        End Set
    End Property
    Public Property checkerjudgeitems As NawaDevDAL.AML_SCREENING_RESULT_DETAIL
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.checkerjudgeitems")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            Session("AML_Report_ReportRiskRatingDetail.checkerjudgeitems") = value
        End Set
    End Property
    Public Property indexofjudgementitems As Integer
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.indexofjudgementitems")
        End Get
        Set(ByVal value As Integer)
            Session("AML_Report_ReportRiskRatingDetail.indexofjudgementitems") = value
        End Set
    End Property

    Public Property indexofjudgementitemschange As Integer
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.indexofjudgementitemschange")
        End Get
        Set(ByVal value As Integer)
            Session("AML_Report_ReportRiskRatingDetail.indexofjudgementitemschange") = value
        End Set
    End Property
    Public Property dataID As String
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.dataID")
        End Get
        Set(ByVal value As String)
            Session("AML_Report_ReportRiskRatingDetail.dataID") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_CLASS() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.objAML_WATCHLIST_CLASS")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AML_Report_ReportRiskRatingDetail.objAML_WATCHLIST_CLASS") = value
        End Set
    End Property

    Public Property judgementClass() As NawaDevBLL.AMLJudgementClass
        Get
            Return Session("AML_Report_ReportRiskRatingDetail.judgementClass")
        End Get
        Set(ByVal value As NawaDevBLL.AMLJudgementClass)
            Session("AML_Report_ReportRiskRatingDetail.judgementClass") = value
        End Set
    End Property

    Public Property listAccount As List(Of NawaDevDAL.AML_ACCOUNT)
        Get
            Return Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listaccount")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_ACCOUNT))
            Session("AML_RiskRatingHistory_RiskRatingHistoryDetail.listaccount") = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then

                Dim strmodule As String = Request.Params("ModuleID")
                Dim intmodule As Integer = Common.DecryptQueryString(strmodule, SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)
                If ObjModule IsNot Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If
                Dim dataStr As String = Request.Params("ID")
                If dataStr IsNot Nothing Then
                    dataID = Common.DecryptQueryString(dataStr, SystemParameterBLL.GetEncriptionKey)
                End If
                If dataID IsNot Nothing Then
                    loaddata(dataID)
                Else
                    Throw New ApplicationException("An Error Occurred While Loading the Data With Id")
                End If

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub loaddata(idheader As String)
        'reportriskratingheader = AMLJudgementBLL.GetAMLreportriskratingheader(idheader)
        'If Not IsDBNull(reportriskratingheader.FK_AML_SCREENING_REQUEST_ID) Then
        '    reportriskratingrequestforheader = AMLJudgementBLL.GetRequestForHeader(reportriskratingheader.FK_AML_SCREENING_REQUEST_ID)
        'End If
        'If reportriskratingrequestforheader IsNot Nothing Then
        '    If reportriskratingrequestforheader.REQUEST_ID IsNot Nothing Then
        '        display_Request_ID.Text = reportriskratingrequestforheader.REQUEST_ID
        '    End If
        '    If reportriskratingrequestforheader.SOURCE IsNot Nothing Then
        '        display_Request_Source.Text = reportriskratingrequestforheader.SOURCE
        '    End If
        '    If reportriskratingrequestforheader.SERVICE IsNot Nothing Then
        '        display_Request_Service.Text = reportriskratingrequestforheader.SERVICE
        '    End If
        '    If reportriskratingrequestforheader.OPERATION IsNot Nothing Then
        '        display_Request_Operation.Text = reportriskratingrequestforheader.OPERATION
        '    End If
        '    If reportriskratingheader.RESPONSE_ID IsNot Nothing Then
        '        display_Response_ID.Text = reportriskratingheader.RESPONSE_ID
        '    End If
        '    If reportriskratingrequestforheader.CreatedDate IsNot Nothing Then
        '        display_Request_Date.Text = reportriskratingrequestforheader.CreatedDate.Value.ToString("dd-MMM-yyyy, hh:mm:ss tt")
        '    End If
        '    If reportriskratingheader.FK_AML_RESPONSE_CODE_ID IsNot Nothing Then
        '        If reportriskratingheader.FK_AML_RESPONSE_CODE_ID = 1 Then
        '            display_Response_Code.Text = "1 - Found"
        '        ElseIf reportriskratingheader.FK_AML_RESPONSE_CODE_ID = 2 Then
        '            display_Response_Code.Text = "2 - Not Found"
        '        Else
        '            display_Response_Code.Text = "3 - Error"
        '        End If
        '    End If
        '    If reportriskratingrequestforheader.RESPONSE_DESCRIPTION IsNot Nothing Then
        '        display_Response_Description.Text = reportriskratingrequestforheader.RESPONSE_DESCRIPTION
        '    End If
        '    If reportriskratingrequestforheader.RESPONSE_DATE IsNot Nothing Then
        '        display_Response_Date.Text = reportriskratingrequestforheader.RESPONSE_DATE.Value.ToString("dd-MMM-yyyy, hh:mm:ss tt")
        '    End If
        '    If reportriskratingrequestforheader.CIF_NO IsNot Nothing Then
        '        display_Request_CIF.Text = reportriskratingrequestforheader.CIF_NO
        '    End If
        '    If reportriskratingrequestforheader.NAME IsNot Nothing Then
        '        display_Request_Name.Text = reportriskratingrequestforheader.NAME
        '    End If
        '    If reportriskratingrequestforheader.DOB IsNot Nothing Then
        '        display_Request_DOB.Text = reportriskratingrequestforheader.DOB.Value.Date.ToString("dd-MMM-yyyy")
        '    End If
        '    If reportriskratingrequestforheader.NATIONALITY IsNot Nothing Then
        '        display_Request_Nationality.Text = reportriskratingrequestforheader.NATIONALITY
        '    End If
        '    If reportriskratingrequestforheader.IDENTITY_NUMBER IsNot Nothing Then
        '        display_Request_Identity_Number.Text = reportriskratingrequestforheader.IDENTITY_NUMBER
        '    End If
        'End If
        'Dim strid As String = Request.Params("ID")
        'Dim id As String = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
        Dim Customer As AML_CUSTOMER = NawaDevBLL.AMLReportRiskRating.GetCustomerBYCIFNO(idheader)
        With Customer
            listAccount = NawaDevBLL.AMLReportRiskRating.GetAMLAccountList(.CIFNo)

            'listreportriskratingproduct = AMLReportRiskRating.GetAMLProductList(.FK_AML_PRODUCT_CODE)
            '    If listreportriskratingproduct IsNot Nothing Then
            '        BindProduct(StoreProductItem, listreportriskratingproduct)
            '    End If
            Dim productlist As New List(Of AML_PRODUCT)
            For Each item In listAccount
                Dim aList As AML_PRODUCT = NawaDevBLL.AMLReportRiskRating.GetProductByCIF(item.FK_AML_PRODUCT_CODE)
                productlist.Add(aList)

            Next
            If productlist IsNot Nothing Then
                BindProduct(StoreProductItem, productlist)
            End If
            Dim branch As AML_BRANCH = NawaDevBLL.AMLReportRiskRating.GetBranchBYFK(.FK_AML_Creation_Branch_Code)
            If branch IsNot Nothing Then
                DisplayKodeCabang.Value = .FK_AML_Creation_Branch_Code
                DisplayNamaCabang.Value = branch.REGION_NAME
                DisplayKantorWilayah.Value = branch.REGION_CODE
            Else
                DisplayKodeCabang.Value = Nothing
                DisplayNamaCabang.Value = Nothing
                DisplayKantorWilayah.Value = Nothing
            End If
            DisplayCIFNO.Value = .CIFNo
            DisplayCustomerName.Value = .CUSTOMERNAME
            Dim customertype As AML_CUSTOMER_TYPE = NawaDevBLL.AMLReportRiskRating.GetCustomerTypeFK(.FK_AML_Customer_Type_Code)
            If customertype IsNot Nothing Then
                DisplayTipeNasabah.Value = customertype.Customer_Type_Name
            Else
                DisplayTipeNasabah.Value = Nothing
            End If
            DisplayTujuanPembukaan.Value = Nothing
            Dim pekerjaan As AML_PEKERJAAN = NawaDevBLL.AMLReportRiskRating.GetPekerjaanFK(.FK_AML_PEKERJAAN_CODE)
            If pekerjaan IsNot Nothing Then
                If pekerjaan.IS_PEP = 0 Then
                    DisplayPEP.Value = "Tidak"
                Else
                    DisplayPEP.Value = "Ya"
                End If
                DisplayPekerjaan.Value = pekerjaan.AML_PEKERJAAN_Name
            Else
                DisplayPEP.Value = Nothing
                DisplayPekerjaan = Nothing
            End If
            DisplayJumlahNominal = Nothing
            Dim bidangusaha As AML_BIDANG_USAHA = NawaDevBLL.AMLReportRiskRating.GetBidangUsahaByFK(.FK_AML_INDUSTRY_CODE)
            If bidangusaha IsNot Nothing Then
                DisplayBidangUsaha.Value = bidangusaha.AML_BIDANG_USAHA_Name
            Else
                DisplayBidangUsaha.Value = Nothing
            End If
            Dim country As AML_COUNTRY = NawaDevBLL.AMLReportRiskRating.GetCountryByFK(.FK_AML_CITIZENSHIP_CODE)
            If country IsNot Nothing Then
                DisplayNegara.Value = country.AML_COUNTRY_Name
            Else
                DisplayNegara.Value = Nothing
            End If
            Dim prov As AML_CUSTOMER_ADDRESS = NawaDevBLL.AMLReportRiskRating.GetAdressBYCIFNO(.CIFNo)
            If prov IsNot Nothing Then
                DisplayWilayah.Value = prov.PROPINSI
            Else
                DisplayWilayah.Value = Nothing
            End If
            DisplayFieldSTR.Value = Nothing

            Dim risk As AML_RISK_RATING = NawaDevBLL.AMLReportRiskRating.GetRiskRatingByFK(.FK_AML_RISK_CODE)
            If risk IsNot Nothing Then
                DisplayFieldResikoONEFCC.Value = risk.RISK_RATING_NAME
            Else
                DisplayFieldResikoONEFCC.Value = Nothing
            End If

            DisplayFieldScoreAKhir.Value = Nothing
            DisplayFieldResikoT24.Value = Nothing
            If .OpeningDate IsNot Nothing Then
                DisplayFieldCIFCreatedDate.Value = .OpeningDate
            Else
                DisplayFieldCIFCreatedDate.Value = Nothing
            End If

            If .DATEOFLASTDUEDILIGENCE IsNot Nothing Then
                DisplayFieldLastMaintance.Value = .DATEOFLASTDUEDILIGENCE
            Else
                DisplayFieldLastMaintance.Value = Nothing
            End If
            Dim screening_result As AML_SCREENING_RESULT = NawaDevBLL.AMLReportRiskRating.GetScreeningResultByFK(.CIFNo)
            'If screening_result IsNot Nothing Then
            '    If screening_result.FK_AML_RESPONSE_CODE_ID = 1 Then
            '        display_Response_Code.Text = "1 - Found"
            '    ElseIf screening_result.FK_AML_RESPONSE_CODE_ID = 2 Then
            '        display_Response_Code.Text = "2 - Not Found"
            '    Else
            '        display_Response_Code.Text = "3 - Error"
            '    End If
            'Else
            '    display_Response_Code.Text = Nothing
            'End If
            'Dim screening_request As AML_SCREENING_REQUEST = NawaDevBLL.AMLReportRiskRating.GetScreeningRequestByFK(.CIFNo)
            'If screening_request IsNot Nothing Then
            '    display_Response_Description.Text = screening_request.RESPONSE_DESCRIPTION
            '    display_Response_Date.Text = screening_request.RESPONSE_DATE.Value.ToString("dd-MMM-yyyy, hh:mm:ss tt")
            '    display_Request_Date.Text = screening_request.CreatedDate.Value.ToString("dd-MMM-yyyy, hh:mm:ss tt")
            'Else

            'End If
            Dim stops As Integer = 0
            listreportriskratingdelivery = AMLReportRiskRating.GetAMLDeliveryList(idheader)
            If listreportriskratingdelivery IsNot Nothing Then
                For Each item In listreportriskratingdelivery
                    If item.FK_AML_AML_DELIVERY_CHANNELS_CODE = .FK_AML_DELIVERY_CHANNELS_CODE Then
                        If stops < 1 Then
                            DisplayDeliveryChannel.Text = item.AML_DELIVERY_CHANNELS_NAME
                            stops = stops + 1
                        End If
                    End If
                Next
            End If
            stops = 0
            listreportriskrating = AMLReportRiskRating.GetAMLScreeningList(.CIFNo)
            If listreportriskrating IsNot Nothing Then
                BindJudgement(StoreWatchlist, listreportriskrating)
            End If
            'listreportriskratingproduct = AMLReportRiskRating.GetAMLProductList(.FK_AML_PRODUCT_CODE)
            'If listreportriskratingproduct IsNot Nothing Then
            '    BindProduct(StoreProductItem, listreportriskratingproduct)
            'End If
        End With


        'listreportriskratingdelivery = AMLReportRiskRating.GetAMLDeliveryList(idheader)
        'If listreportriskratingdelivery IsNot Nothing Then
        '    BindDevlivery(Delivery_Channel_Item, listreportriskratingdelivery)
        'End If

    End Sub

    Private Sub BindJudgement(store As Store, list As List(Of AML_WATCHLIST))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_CATEGORY", GetType(String)))
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_TYPE", GetType(String)))

            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    If Not IsDBNull(item("FK_AML_WATCHLIST_CATEGORY_ID")) Then
                        item("AML_WATCHLIST_CATEGORY") = AMLJudgementBLL.GetWatchlistCategoryByID(item("FK_AML_WATCHLIST_CATEGORY_ID"))
                    Else
                        item("AML_WATCHLIST_CATEGORY") = ""
                    End If
                    If Not IsDBNull(item("FK_AML_WATCHLIST_TYPE_ID")) Then
                        item("AML_WATCHLIST_TYPE") = AMLJudgementBLL.GetWatchlistTypeByID(item("FK_AML_WATCHLIST_TYPE_ID"))
                    Else
                        item("AML_WATCHLIST_TYPE") = ""
                    End If
                Next
            End If
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindDevlivery(store As Store, list As List(Of AML_DELIVERY_CHANNELS))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub BindProduct(store As Store, list As List(Of AML_PRODUCT))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)

            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Protected Sub Bind_AML_WATCHLIST_ALIAS()
    '    Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS)
    'End Sub

    'Protected Sub Bind_AML_WATCHLIST_ADDRESS()
    '    Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS)

    '    objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
    '    objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

    '    If objtable.Rows.Count > 0 Then
    '        For Each item As Data.DataRow In objtable.Rows
    '            Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
    '            If objCountry IsNot Nothing Then
    '                item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
    '            Else
    '                item("COUNTRY_NAME") = Nothing
    '            End If

    '            Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
    '            If objAddressType IsNot Nothing Then
    '                item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
    '            Else
    '                item("AML_ADDRESS_TYPE_NAME") = Nothing
    '            End If
    '        Next
    '    End If


    'End Sub
    'Protected Sub Bind_AML_WATCHLIST_IDENTITY()
    '    Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY)

    '    objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

    '    If objtable.Rows.Count > 0 Then
    '        For Each item As Data.DataRow In objtable.Rows
    '            Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
    '            If objIDENTITYType IsNot Nothing Then
    '                item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
    '            Else
    '                item("AML_IDENTITY_TYPE_NAME") = Nothing
    '            End If
    '        Next
    '    End If


    'End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Protected Sub loadjudgementitemchange(items As AML_SCREENING_RESULT_DETAIL)
    '    judgementdetailchange = listreportriskratingchange.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = items.PK_AML_SCREENING_RESULT_DETAIL_ID).FirstOrDefault
    '    If judgementdetailchange IsNot Nothing Then
    '        indexofjudgementitemschange = listreportriskratingchange.IndexOf(judgementdetailchange)
    '        listreportriskratingchange(indexofjudgementitemschange) = items
    '    Else
    '        listreportriskratingchange.Add(items)
    '    End If
    'End Sub

    'Protected Sub rgjudgement_click(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim objradio As Ext.Net.Radio = CType(sender, Ext.Net.Radio)
    '        If JudgementComment.Text Is Nothing Or JudgementComment.Text = "" Or JudgementComment.Text = "This Data is Judged Match" Or JudgementComment.Text = "This Data is Judged Not Match" Then
    '            If objradio.InputValue = "1" And objradio.Checked Then
    '                JudgementComment.Text = "This Data is Judged Match"
    '            ElseIf objradio.InputValue = "0" And objradio.Checked Then
    '                JudgementComment.Text = "This Data is Judged Not Match"
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub BtnJudgementSave_Click(sender As Object, e As DirectEventArgs)
    '    Try
    '        If RBJudgementMatch.Checked Or RBJudgementNotMatch.Checked Then
    '            If judgementdetail IsNot Nothing Then
    '                If judgementdetail.JUDGEMENT_ISMATCH <> RBJudgementMatch.Checked Or judgementdetail.JUDGEMENT_COMMENT <> JudgementComment.Text Or judgementdetail.JUDGEMENT_ISMATCH Is Nothing Then
    '                    If RBJudgementMatch.Checked Then
    '                        judgementdetail.JUDGEMENT_ISMATCH = True
    '                    Else
    '                        judgementdetail.JUDGEMENT_ISMATCH = False
    '                    End If
    '                    If JudgementComment.Text IsNot Nothing Then
    '                        judgementdetail.JUDGEMENT_COMMENT = JudgementComment.Text
    '                    End If
    '                    If Common.SessionCurrentUser.UserName IsNot Nothing Then
    '                        judgementdetail.JUDGEMENT_BY = Common.SessionCurrentUser.UserID
    '                    End If
    '                    judgementdetail.JUDGEMENT_DATE = Now()
    '                    If judgementdetail.PK_AML_SCREENING_RESULT_DETAIL_ID <> Nothing And listreportriskrating IsNot Nothing Then
    '                        loadjudgementitemchange(judgementdetail)
    '                        listreportriskrating(indexofjudgementitems) = judgementdetail
    '                        BindJudgement(judgement_Item, listreportriskrating)
    '                    Else
    '                        Throw New ApplicationException("Something Wrong While Saving Data")
    '                    End If
    '                Else
    '                    Throw New ApplicationException("No Data Has Changed, Please Change Some Data Before Saving or Click the Cancel Button to Cancel the Process")
    '                End If
    '            End If
    '        Else
    '            Throw New ApplicationException("Must Conduct Judgment Before Saving Data")
    '        End If

    '    Catch ex As Exception
    '        ErrorSignal.FromCurrentContext.Raise(ex)
    '        Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub BtnJudgementCancel_Click(sender As Object, e As DirectEventArgs)
    '    Try
    '        ClearOBJDetail()
    '    Catch ex As Exception
    '        ErrorSignal.FromCurrentContext.Raise(ex)
    '        Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub BtnConfirmation_DirectClick()
    '    Try
    '        Dim Moduleid As String = Request.Params("ModuleID")
    '        Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
    '    Catch ex As Exception
    '        ErrorSignal.FromCurrentContext.Raise(ex)
    '        Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

End Class
