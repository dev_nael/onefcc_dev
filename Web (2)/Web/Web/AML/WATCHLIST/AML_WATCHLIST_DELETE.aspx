﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AML_WATCHLIST_DELETE.aspx.vb" Inherits="AML_WATCHLIST_DELETE" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="Watch List Data">
        <Items>
            <%-- Watch List General Information --%>
            <ext:Panel runat="server" Title="General Information" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlWATCHLIST" Border="true" BodyStyle="padding:10px">
                <Content>

                    <ext:DisplayField ID="txt_AML_WATCHLIST_ID" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Watch List ID" AnchorHorizontal="80%"></ext:DisplayField>
                    <NDS:NDSDropDownField ID="cmb_AML_WATCHLIST_CATEGORY" LabelWidth="250" ValueField="PK_AML_WATCHLIST_CATEGORY_ID" DisplayField="CATEGORY_NAME" runat="server" StringField="PK_AML_WATCHLIST_CATEGORY_ID, CATEGORY_NAME" StringTable="AML_WATCHLIST_CATEGORY" Label="Watch List Category" AnchorHorizontal="80%" AllowBlank="false"/>
                    <NDS:NDSDropDownField ID="cmb_AML_WATCHLIST_TYPE" LabelWidth="250" ValueField="PK_AML_WATCHLIST_TYPE_ID" DisplayField="TYPE_NAME" runat="server" StringField="PK_AML_WATCHLIST_TYPE_ID, TYPE_NAME" StringTable="AML_WATCHLIST_TYPE" Label="Watch List Type" AnchorHorizontal="80%" AllowBlank="true"/>
                    <ext:TextField ID="txt_NAME" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="txt_PLACE_OF_BIRTH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:TextField>
                    <ext:DateField ID="txt_DATE_OF_BIRTH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="50%" Format="dd-MMM-yyyy"></ext:DateField>
                    <ext:TextField ID="txt_YOB" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Year of Birth" AnchorHorizontal="50%" MaxLength="8000" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:TextField>
                    <ext:TextField ID="txt_NATIONALITY" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Nationality" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:TextField>

                    <%-- Watch List Alias --%>
                    <ext:GridPanel ID="gp_AML_WATCHLIST_ALIAS" runat="server" Title="Alias(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <TopBar>
                            <ext:Toolbar ID="toolbar12" runat="server">
                                <Items>
                                    <ext:Button ID="btn_AML_WATCHLIST_ALIAS_Add" runat="server" Text="Add Alias" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_AML_WATCHLIST_ALIAS_Add_Click">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_AML_WATCHLIST_ALIAS" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model29" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ALIAS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ALIAS_NAME" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                                <ext:Column ID="Column75" runat="server" DataIndex="ALIAS_NAME" Text="Alias Name" MinWidth="300" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_AML_WATCHLIST_ALIAS" runat="server" Text="Action" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_AML_WATCHLIST_ALIAS">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_WATCHLIST_ALIAS_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Alias --%>

                    <%-- Watch List Address --%>
                    <ext:GridPanel ID="gp_AML_WATCHLIST_ADDRESS" runat="server" Title="Address(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <TopBar>
                            <ext:Toolbar ID="toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btn_AML_WATCHLIST_ADDRESS_Add" runat="server" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_AML_WATCHLIST_ADDRESS_Add_Click">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store1" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ADDRESS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_AML_COUNTRY_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="POSTAL_CODE" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                                <ext:Column ID="Column5" runat="server" DataIndex="AML_ADDRESS_TYPE_NAME" Text="Type" MinWidth="50"></ext:Column>
                                <ext:Column ID="Column1" runat="server" DataIndex="ADDRESS" Text="Address" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="FK_AML_COUNTRY_CODE" Text="Country Code" MinWidth="100" Hidden="true"></ext:Column>
                                <ext:Column ID="Column3" runat="server" DataIndex="COUNTRY_NAME" Text="Country Name" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="POSTAL_CODE" Text="Postal Code" MinWidth="100"></ext:Column>
                                <ext:CommandColumn ID="cc_AML_WATCHLIST_ADDRESS" runat="server" Text="Action" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_AML_WATCHLIST_ADDRESS">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_WATCHLIST_ADDRESS_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Address --%>

                    <%-- Watch List Identity --%>
                    <ext:GridPanel ID="gp_AML_WATCHLIST_IDENTITY" runat="server" Title="Identity(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <TopBar>
                            <ext:Toolbar ID="toolbar2" runat="server">
                                <Items>
                                    <ext:Button ID="btn_AML_WATCHLIST_IDENTITY_Add" runat="server" Text="Add Identity" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_AML_WATCHLIST_IDENTITY_Add_Click">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store2" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_IDENTITY_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_IDENTITY_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDENTITYNUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                                <ext:Column ID="Column6" runat="server" DataIndex="AML_IDENTITY_TYPE_NAME" Text="Type" MinWidth="200" Flex="1"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="IDENTITYNUMBER" Text="Identity" MinWidth="300"></ext:Column>
                                <ext:CommandColumn ID="cc_AML_WATCHLIST_IDENTITY" runat="server" Text="Action" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_AML_WATCHLIST_IDENTITY">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_WATCHLIST_IDENTITY_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Identity --%>

                </Content>
            </ext:Panel>
            <%-- End of Watch List General Information --%>

            <%-- Watch List Remarks --%>
            <ext:Panel runat="server" Title="Remarks" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="Panel1" Border="true" BodyStyle="padding:10px" PaddingSpec="10 0">
                <Content>

                    <ext:TextArea ID="txt_CUSTOM_REMARK_1" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 1" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:TextArea>
                    <ext:TextArea ID="txt_CUSTOM_REMARK_2" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 2" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:TextArea>
                    <ext:TextArea ID="txt_CUSTOM_REMARK_3" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 3" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:TextArea>
                    <ext:TextArea ID="txt_CUSTOM_REMARK_4" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 4" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:TextArea>
                    <ext:TextArea ID="txt_CUSTOM_REMARK_5" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 5" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:TextArea>

                </Content>
            </ext:Panel>
            <%-- End of Watch List Remarks --%>


        </Items>

        <Buttons>
            <ext:Button ID="btn_WATCHLIST_Submit" runat="server" Icon="Delete" Text="Delete">
                <DirectEvents>
                    <Click OnEvent="btn_WATCHLIST_Submit_Click">
                        <Confirmation Message="Are You Sure To Delete This Data ?" ConfirmRequest="true" Title="Delete" />
                        <EventMask ShowMask="true" Msg="Deleting Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_WATCHLIST_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_WATCHLIST_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <%-- Pop Up Windows --%>

    <%-- Pop Up Window Alias --%>
    <ext:Window ID="Window_AML_WATCHLIST_ALIAS" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="Window_Panel_AML_WATCHLIST_ALIAS" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:TextField ID="txt_AML_WATCHLIST_ALIAS_NAME" runat="server" FieldLabel="Alias Name" AllowBlank="false" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:TextField>
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_AML_WATCHLIST_ALIAS_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_ALIAS_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_AML_WATCHLIST_ALIAS_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_ALIAS_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.5});" />

            <Resize Handler="#{Window_AML_WATCHLIST_ALIAS}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Alias --%>

    <%-- Pop Up Window Address --%>
    <ext:Window ID="Window_AML_WATCHLIST_ADDRESS" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="Window_Panel_AML_WATCHLIST_ADDRESS" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Content>
                    <NDS:NDSDropDownField ID="cmb_AML_WATCHLIST_ADDRESS_TYPE_CODE" ValueField="FK_AML_ADDRES_TYPE_CODE" DisplayField="ADDRESS_TYPE_NAME" runat="server" StringField="FK_AML_ADDRES_TYPE_CODE, ADDRESS_TYPE_NAME" StringTable="AML_ADDRESS_TYPE" Label="Address Type" AnchorHorizontal="100%" AllowBlank="true"/>
                    <ext:TextField ID="txt_AML_WATCHLIST_ADDRESS" runat="server" FieldLabel="Address" AllowBlank="false" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:TextField>
                    <NDS:NDSDropDownField ID="cmb_AML_WATCHLIST_ADDRESS_COUNTRY_CODE" ValueField="FK_AML_COUNTRY_Code" DisplayField="AML_COUNTRY_Name" runat="server" StringField="FK_AML_COUNTRY_Code, AML_COUNTRY_Name" StringTable="AML_COUNTRY" Label="Country Code" AnchorHorizontal="100%" AllowBlank="true"/>
                    <ext:TextField ID="txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE" runat="server" FieldLabel="Postal Code" AllowBlank="true" AnchorHorizontal="50%" MaxLength="8000" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:TextField>
                </Content>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_AML_WATCHLIST_ADDRESS_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_ADDRESS_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_AML_WATCHLIST_ADDRESS_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_ADDRESS_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.5});" />

            <Resize Handler="#{Window_AML_WATCHLIST_ADDRESS}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Address --%>


    <%-- Pop Up Window Identity --%>
    <ext:Window ID="Window_AML_WATCHLIST_IDENTITY" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="FormPanel1" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Content>
                    <NDS:NDSDropDownField ID="cmb_AML_WATCHLIST_IDENTITY_TYPE_CODE" ValueField="FK_AML_IDENTITY_TYPE_CODE" DisplayField="IDENTITY_TYPE_NAME" runat="server" StringField="FK_AML_IDENTITY_TYPE_CODE, IDENTITY_TYPE_NAME" StringTable="AML_IDENTITY_TYPE" Label="Identity Type" AnchorHorizontal="100%" AllowBlank="true"/>
                    <ext:TextField ID="txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER" runat="server" FieldLabel="Identity Number" AllowBlank="false" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:TextField>
                </Content>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_AML_WATCHLIST_IDENTITY_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_IDENTITY_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_AML_WATCHLIST_IDENTITY_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_IDENTITY_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.5});" />

            <Resize Handler="#{Window_AML_WATCHLIST_IDENTITY}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Identity --%>

    <%-- End of Pop Up Windows --%>

</asp:Content>

