﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AML_WATCHLIST_APPROVALDETAIL.aspx.vb" Inherits="AML_WATCHLIST_APPROVALDETAIL" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="0" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="ColumnLayout" ButtonAlign="Center" AutoScroll="true" Title="Watch List Data">
        <Items>
            <ext:Panel runat="server" Title="Action" Collapsible="true" Layout="AnchorLayout" ColumnWidth="1" ID="pnlAction" Border="true" BodyStyle="padding:10px">
                <Content>
                    <ext:DisplayField ID="txt_ModuleApproval_ID" runat="server" FieldLabel="Approval ID" AnchorHorizontal="100%" Hidden="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_ModuleLabel" runat="server" FieldLabel="Module Label" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:DisplayField ID="txt_ModuleKey" runat="server" FieldLabel="Module Key" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:DisplayField ID="txt_CreatedBy" runat="server" FieldLabel="Created By" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:DisplayField ID="txt_CreatedDate" runat="server" FieldLabel="Created Date" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:DisplayField ID="txt_Action" runat="server" FieldLabel="Action" AnchorHorizontal="100%"></ext:DisplayField>
                </Content>
            </ext:Panel>
            <ext:Panel runat="server" Title="Data Before" Collapsible="true" Layout="AnchorLayout" ColumnWidth="0.5" ID="pnlDataBefore" Border="true" BodyStyle="padding:10px">
                <Items>
                    <%-- Watch List General Information --%>
                    <ext:Panel runat="server" Title="General Information" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlWATCHLIST_Before" Border="true" BodyStyle="padding:10px">
                        <Content>

                            <ext:DisplayField ID="txt_AML_WATCHLIST_ID_Before" AllowBlank="false" runat="server" FieldLabel="Watch List ID" AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:DisplayField ID="txt_AML_WATCHLIST_CATEGORY_Before" AllowBlank="false" runat="server" FieldLabel="Watch List Category" AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:DisplayField ID="txt_AML_WATCHLIST_TYPE_Before" AllowBlank="false" runat="server" FieldLabel="Watch List Type" AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:DisplayField ID="txt_NAME_Before" AllowBlank="false" runat="server" FieldLabel="Full Name" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                            <ext:DisplayField ID="txt_PLACE_OF_BIRTH_Before" AllowBlank="true" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                            <ext:DisplayField ID="txt_DATE_OF_BIRTH_Before" AllowBlank="true" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:DisplayField ID="txt_YOB_Before" AllowBlank="true" runat="server" FieldLabel="Year of Birth" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:DisplayField>
                            <ext:DisplayField ID="txt_NATIONALITY_Before" AllowBlank="true" runat="server" FieldLabel="Nationality" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>

                            <%-- Watch List Alias --%>
                            <ext:GridPanel ID="gp_AML_WATCHLIST_ALIAS_Before" runat="server" Title="Alias(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                                <Store>
                                    <ext:Store ID="Store_AML_WATCHLIST_ALIAS_Before" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model3" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_AML_WATCHLIST_ALIAS_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="ALIAS_NAME" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column8" runat="server" DataIndex="ALIAS_NAME" Text="Alias Name" MinWidth="300" Flex="1"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%-- End of Watch List Alias --%>

                            <%-- Watch List Address --%>
                            <ext:GridPanel ID="gp_AML_WATCHLIST_ADDRESS_Before" runat="server" Title="Address(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                                <Store>
                                    <ext:Store ID="Store3" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model4" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_AML_WATCHLIST_ADDRESS_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="AML_ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="FK_AML_COUNTRY_CODE" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="POSTAL_CODE" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column9" runat="server" DataIndex="AML_ADDRESS_TYPE_NAME" Text="Type" MinWidth="50"></ext:Column>
                                        <ext:Column ID="Column10" runat="server" DataIndex="ADDRESS" Text="Address" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                                        <ext:Column ID="Column11" runat="server" DataIndex="FK_AML_COUNTRY_CODE" Text="Country Code" MinWidth="100" Hidden="true"></ext:Column>
                                        <ext:Column ID="Column12" runat="server" DataIndex="COUNTRY_NAME" Text="Country Name" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column13" runat="server" DataIndex="POSTAL_CODE" Text="Postal Code" MinWidth="100"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%-- End of Watch List Address --%>

                            <%-- Watch List Identity --%>
                            <ext:GridPanel ID="gp_AML_WATCHLIST_IDENTITY_Before" runat="server" Title="Identity(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                                <Store>
                                    <ext:Store ID="Store4" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model5" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_AML_WATCHLIST_IDENTITY_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="AML_IDENTITY_TYPE_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="IDENTITYNUMBER" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column14" runat="server" DataIndex="AML_IDENTITY_TYPE_NAME" Text="Type" MinWidth="200" Flex="1"></ext:Column>
                                        <ext:Column ID="Column15" runat="server" DataIndex="IDENTITYNUMBER" Text="Identity" MinWidth="300"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%-- End of Watch List Identity --%>
                        </Content>
                    </ext:Panel>
                    <%-- End of Watch List General Information --%>

                    <%-- Watch List Remarks --%>
                    <ext:Panel runat="server" Title="Remarks" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlWatchListRemarks_Before" Border="true" BodyStyle="padding:10px" PaddingSpec="10 0">
                        <Content>
                            <ext:DisplayField ID="txt_CUSTOM_REMARK_1_Before" AllowBlank="true" runat="server" FieldLabel="Remark 1" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                            <ext:DisplayField ID="txt_CUSTOM_REMARK_2_Before" AllowBlank="true" runat="server" FieldLabel="Remark 2" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                            <ext:DisplayField ID="txt_CUSTOM_REMARK_3_Before" AllowBlank="true" runat="server" FieldLabel="Remark 3" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                            <ext:DisplayField ID="txt_CUSTOM_REMARK_4_Before" AllowBlank="true" runat="server" FieldLabel="Remark 4" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                            <ext:DisplayField ID="txt_CUSTOM_REMARK_5_Before" AllowBlank="true" runat="server" FieldLabel="Remark 5" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>

                        </Content>
                    </ext:Panel>
                    <%-- End of Watch List Remarks --%>
                </Items>
            </ext:Panel>
            <ext:Panel runat="server" Title="Data After" Collapsible="true" Layout="AnchorLayout" ColumnWidth="0.5" ID="pnlDataAfter" Border="true" BodyStyle="padding:10px">
                <Items>
                    <%-- Watch List General Information --%>
                    <ext:Panel runat="server" Title="General Information" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlWATCHLIST" Border="true" BodyStyle="padding:10px">
                        <Content>

                            <ext:DisplayField ID="txt_AML_WATCHLIST_ID" AllowBlank="false" runat="server" FieldLabel="Watch List ID" AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:DisplayField ID="txt_AML_WATCHLIST_CATEGORY" AllowBlank="false" runat="server" FieldLabel="Watch List Category" AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:DisplayField ID="txt_AML_WATCHLIST_TYPE" AllowBlank="false" runat="server" FieldLabel="Watch List Type" AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:DisplayField ID="txt_NAME" AllowBlank="false" runat="server" FieldLabel="Full Name" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                            <ext:DisplayField ID="txt_PLACE_OF_BIRTH" AllowBlank="true" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                            <ext:DisplayField ID="txt_DATE_OF_BIRTH" AllowBlank="true" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:DisplayField ID="txt_YOB" AllowBlank="true" runat="server" FieldLabel="Year of Birth" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:DisplayField>
                            <ext:DisplayField ID="txt_NATIONALITY" AllowBlank="true" runat="server" FieldLabel="Nationality" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>

                            <%-- Watch List Alias --%>
                            <ext:GridPanel ID="gp_AML_WATCHLIST_ALIAS" runat="server" Title="Alias(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                                <Store>
                                    <ext:Store ID="Store_AML_WATCHLIST_ALIAS" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model29" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_AML_WATCHLIST_ALIAS_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="ALIAS_NAME" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column75" runat="server" DataIndex="ALIAS_NAME" Text="Alias Name" MinWidth="300" Flex="1"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%-- End of Watch List Alias --%>

                            <%-- Watch List Address --%>
                            <ext:GridPanel ID="gp_AML_WATCHLIST_ADDRESS" runat="server" Title="Address(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                                <Store>
                                    <ext:Store ID="Store1" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_AML_WATCHLIST_ADDRESS_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="AML_ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="FK_AML_COUNTRY_CODE" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="POSTAL_CODE" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column5" runat="server" DataIndex="AML_ADDRESS_TYPE_NAME" Text="Type" MinWidth="50"></ext:Column>
                                        <ext:Column ID="Column1" runat="server" DataIndex="ADDRESS" Text="Address" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                                        <ext:Column ID="Column2" runat="server" DataIndex="FK_AML_COUNTRY_CODE" Text="Country Code" MinWidth="100" Hidden="true"></ext:Column>
                                        <ext:Column ID="Column3" runat="server" DataIndex="COUNTRY_NAME" Text="Country Name" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column4" runat="server" DataIndex="POSTAL_CODE" Text="Postal Code" MinWidth="100"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%-- End of Watch List Address --%>

                            <%-- Watch List Identity --%>
                            <ext:GridPanel ID="gp_AML_WATCHLIST_IDENTITY" runat="server" Title="Identity(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                                <Store>
                                    <ext:Store ID="Store2" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model2" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_AML_WATCHLIST_IDENTITY_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="AML_IDENTITY_TYPE_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="IDENTITYNUMBER" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column6" runat="server" DataIndex="AML_IDENTITY_TYPE_NAME" Text="Type" MinWidth="200" Flex="1"></ext:Column>
                                        <ext:Column ID="Column7" runat="server" DataIndex="IDENTITYNUMBER" Text="Identity" MinWidth="300"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%-- End of Watch List Identity --%>

                        </Content>
                    </ext:Panel>
                    <%-- End of Watch List General Information --%>

                    <%-- Watch List Remarks --%>
                    <ext:Panel runat="server" Title="Remarks" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="Panel1" Border="true" BodyStyle="padding:10px" PaddingSpec="10 0">
                        <Content>

                            <ext:DisplayField ID="txt_CUSTOM_REMARK_1" AllowBlank="true" runat="server" FieldLabel="Remark 1" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                            <ext:DisplayField ID="txt_CUSTOM_REMARK_2" AllowBlank="true" runat="server" FieldLabel="Remark 2" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                            <ext:DisplayField ID="txt_CUSTOM_REMARK_3" AllowBlank="true" runat="server" FieldLabel="Remark 3" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                            <ext:DisplayField ID="txt_CUSTOM_REMARK_4" AllowBlank="true" runat="server" FieldLabel="Remark 4" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                            <ext:DisplayField ID="txt_CUSTOM_REMARK_5" AllowBlank="true" runat="server" FieldLabel="Remark 5" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>

                        </Content>
                    </ext:Panel>
                    <%-- End of Watch List Remarks --%>
                </Items>
            </ext:Panel>
        </Items>

        <Buttons>
            <ext:Button ID="btn_WATCHLIST_Approve" runat="server" Icon="Accept" Text="Approve">
                <DirectEvents>
                    <Click OnEvent="btn_WATCHLIST_Approve_Click">
                        <EventMask ShowMask="true" Msg="Approve Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_WATCHLIST_Reject" runat="server" Icon="Decline" Text="Reject">
                <DirectEvents>
                    <Click OnEvent="btn_WATCHLIST_Reject_Click">
                        <Confirmation Message="Are you sure to Reject this Data ?" ConfirmRequest="true" Title="Delete"></Confirmation>
                        <EventMask ShowMask="true" Msg="Reject Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_WATCHLIST_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_WATCHLIST_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

</asp:Content>

