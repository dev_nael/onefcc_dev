﻿Imports Ext
Imports NawaDevBLL
Imports NawaDevDAL
Imports Elmah
Imports System.Data

Partial Class AML_WATCHLIST_DELETE
    Inherits ParentPage

    Public Property IDModule() As String
        Get
            Return Session("AML_WATCHLIST_DELETE.IDModule")
        End Get
        Set(ByVal value As String)
            Session("AML_WATCHLIST_DELETE.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("AML_WATCHLIST_DELETE.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AML_WATCHLIST_DELETE.IDUnik") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_CLASS() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AML_WATCHLIST_DELETE.objAML_WATCHLIST_CLASS")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AML_WATCHLIST_DELETE.objAML_WATCHLIST_CLASS") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_ALIAS_Edit() As NawaDevDAL.AML_WATCHLIST_ALIAS
        Get
            Return Session("AML_WATCHLIST_DELETE.objAML_WATCHLIST_ALIAS_Edit")
        End Get
        Set(ByVal value As NawaDevDAL.AML_WATCHLIST_ALIAS)
            Session("AML_WATCHLIST_DELETE.objAML_WATCHLIST_ALIAS_Edit") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_ADDRESS_Edit() As NawaDevDAL.AML_WATCHLIST_ADDRESS
        Get
            Return Session("AML_WATCHLIST_DELETE.objAML_WATCHLIST_ADDRESS_Edit")
        End Get
        Set(ByVal value As NawaDevDAL.AML_WATCHLIST_ADDRESS)
            Session("AML_WATCHLIST_DELETE.objAML_WATCHLIST_ADDRESS_Edit") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_IDENTITY_Edit() As NawaDevDAL.AML_WATCHLIST_IDENTITY
        Get
            Return Session("AML_WATCHLIST_DELETE.objAML_WATCHLIST_IDENTITY_Edit")
        End Get
        Set(ByVal value As NawaDevDAL.AML_WATCHLIST_IDENTITY)
            Session("AML_WATCHLIST_DELETE.objAML_WATCHLIST_IDENTITY_Edit") = value
        End Set
    End Property

    Sub ClearSession()
        objAML_WATCHLIST_CLASS = New AML_WATCHLIST_CLASS

        objAML_WATCHLIST_ADDRESS_Edit = Nothing
        objAML_WATCHLIST_ALIAS_Edit = Nothing
        objAML_WATCHLIST_IDENTITY_Edit = Nothing
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As Integer = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            If Not Ext.Net.X.IsAjaxRequest Then
                'Tampilkan notifikasi jika ada di bucket Pending Approval
                If AML_WATCHLIST_BLL.IsExistsInApproval(ObjModule.ModuleName, IDUnik) Then
                    LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."

                    Panelconfirmation.Hidden = False
                    FormPanelInput.Hidden = True
                End If

                ClearSession()

                FormPanelInput.Title = ObjModule.ModuleLabel & " - Delete"

                SetCommandColumnLocation()

                LoadWatchList()

                SetControlReadOnly()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    '------- LOAD WATCHLIST
    Protected Sub LoadWatchList()
        Try
            'Load Watchlist
            objAML_WATCHLIST_CLASS = AML_WATCHLIST_BLL.GetWatchlistClassByID(IDUnik)

            'Bind Data Header
            With objAML_WATCHLIST_CLASS.objAML_WATCHLIST
                txt_AML_WATCHLIST_ID.Value = .PK_AML_WATCHLIST_ID

                If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                    Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                    If objWatchlistCategory IsNot Nothing Then
                        cmb_AML_WATCHLIST_CATEGORY.SetTextWithTextValue(objWatchlistCategory.PK_AML_WATCHLIST_CATEGORY_ID, objWatchlistCategory.CATEGORY_NAME)
                    End If
                End If

                If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                    Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                    If objWatchlistType IsNot Nothing Then
                        cmb_AML_WATCHLIST_TYPE.SetTextWithTextValue(objWatchlistType.PK_AML_WATCHLIST_TYPE_ID, objWatchlistType.TYPE_NAME)
                    End If
                End If

                txt_NAME.Value = .NAME
                txt_DATE_OF_BIRTH.Value = .DATE_OF_BIRTH
                txt_PLACE_OF_BIRTH.Value = .BIRTH_PLACE
                txt_YOB.Value = .YOB
                txt_NATIONALITY.Value = .NATIONALITY

                txt_CUSTOM_REMARK_1.Value = .CUSTOM_REMARK_1
                txt_CUSTOM_REMARK_2.Value = .CUSTOM_REMARK_2
                txt_CUSTOM_REMARK_3.Value = .CUSTOM_REMARK_3
                txt_CUSTOM_REMARK_4.Value = .CUSTOM_REMARK_4
                txt_CUSTOM_REMARK_5.Value = .CUSTOM_REMARK_5
            End With

            'Bind Data Detail to Grid Panel
            Bind_AML_WATCHLIST_ALIAS()
            Bind_AML_WATCHLIST_ADDRESS()
            Bind_AML_WATCHLIST_IDENTITY()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF LOAD WATCHLIST

    '------- WATCHLIST ALIAS
    Protected Sub btn_AML_WATCHLIST_ALIAS_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objAML_WATCHLIST_ALIAS_Edit = Nothing
            Clean_Window_AML_WATCHLIST_ALIAS()

            'Show window pop up
            Window_AML_WATCHLIST_ALIAS.Title = "Watch List Alias - Add"
            Window_AML_WATCHLIST_ALIAS.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_AML_WATCHLIST_ALIAS(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Find(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID = strID)
                If objToDelete IsNot Nothing Then
                    objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Remove(objToDelete)
                End If
                Bind_AML_WATCHLIST_ALIAS()
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objAML_WATCHLIST_ALIAS_Edit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Find(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID = strID)
                Load_Window_AML_WATCHLIST_ALIAS(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_ALIAS_Cancel_Click()
        Try
            'Hide window pop up
            Window_AML_WATCHLIST_ALIAS.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_ALIAS_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(txt_AML_WATCHLIST_ALIAS_NAME.Value) Then
                Throw New ApplicationException(txt_AML_WATCHLIST_ALIAS_NAME.FieldLabel & " harus diisi.")
            End If

            'Action save here
            Dim intPK As Long = -1

            If objAML_WATCHLIST_ALIAS_Edit Is Nothing Then  'Add
                Dim objAdd As New NawaDevDAL.AML_WATCHLIST_ALIAS
                If objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS IsNot Nothing AndAlso objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Count > 0 Then
                    intPK = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Min(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                With objAdd
                    .PK_AML_WATCHLIST_ALIAS_ID = intPK
                    .ALIAS_NAME = Trim(txt_AML_WATCHLIST_ALIAS_NAME.Value)
                End With

                objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Add(objAdd)
            Else    'Edit
                Dim objEdit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Find(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID = objAML_WATCHLIST_ALIAS_Edit.PK_AML_WATCHLIST_ALIAS_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        .ALIAS_NAME = Trim(txt_AML_WATCHLIST_ALIAS_NAME.Value)
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_AML_WATCHLIST_ALIAS()

            'Hide window popup
            Window_AML_WATCHLIST_ALIAS.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ALIAS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS)

        'Running the following script for additional columns
        'objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        'If objtable.Rows.Count > 0 Then
        '    For Each item As Data.DataRow In objtable.Rows
        '        item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
        '    Next
        'End If
        gp_AML_WATCHLIST_ALIAS.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_ALIAS.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_AML_WATCHLIST_ALIAS()
        'Clean fields
        txt_AML_WATCHLIST_ALIAS_NAME.Value = Nothing

        'Set fields' ReadOnly
        txt_AML_WATCHLIST_ALIAS_NAME.ReadOnly = False

        'Show Buttons
        btn_AML_WATCHLIST_ALIAS_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_AML_WATCHLIST_ALIAS(strAction As String)
        'Clean window pop up
        Clean_Window_AML_WATCHLIST_ALIAS()

        'Populate fields
        With objAML_WATCHLIST_ALIAS_Edit
            txt_AML_WATCHLIST_ALIAS_NAME.Value = .ALIAS_NAME
        End With

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            txt_AML_WATCHLIST_ALIAS_NAME.ReadOnly = False
        Else
            txt_AML_WATCHLIST_ALIAS_NAME.ReadOnly = True
            btn_AML_WATCHLIST_ALIAS_Save.Hidden = True
        End If

        'Show window pop up
        Window_AML_WATCHLIST_ALIAS.Title = "Watch List Alias - " & strAction
        Window_AML_WATCHLIST_ALIAS.Hidden = False
    End Sub
    '------- END OF WATCHLIST ALIAS

    '------- WATCHLIST ADDRESS
    Protected Sub btn_AML_WATCHLIST_ADDRESS_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objAML_WATCHLIST_ADDRESS_Edit = Nothing
            Clean_Window_AML_WATCHLIST_ADDRESS()

            'Show window pop up
            Window_AML_WATCHLIST_ADDRESS.Title = "Watch List Address - Add"
            Window_AML_WATCHLIST_ADDRESS.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_AML_WATCHLIST_ADDRESS(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS.Find(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID = strID)
                If objToDelete IsNot Nothing Then
                    objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS.Remove(objToDelete)
                End If
                Bind_AML_WATCHLIST_ADDRESS()
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objAML_WATCHLIST_ADDRESS_Edit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS.Find(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID = strID)
                Load_Window_AML_WATCHLIST_ADDRESS(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_ADDRESS_Cancel_Click()
        Try
            'Hide window pop up
            Window_AML_WATCHLIST_ADDRESS.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_ADDRESS_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(txt_AML_WATCHLIST_ADDRESS.Value) Then
                Throw New ApplicationException(txt_AML_WATCHLIST_ADDRESS.FieldLabel & " harus diisi.")
            End If

            'Action save here
            Dim intPK As Long = -1

            If objAML_WATCHLIST_ADDRESS_Edit Is Nothing Then  'Add
                Dim objAdd As New NawaDevDAL.AML_WATCHLIST_ADDRESS
                If objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS IsNot Nothing AndAlso objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS.Count > 0 Then
                    intPK = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS.Min(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                With objAdd
                    .PK_AML_WATCHLIST_ADDRESS_ID = intPK

                    If Not String.IsNullOrEmpty(cmb_AML_WATCHLIST_ADDRESS_TYPE_CODE.SelectedItemValue) Then
                        .FK_AML_ADDRESS_TYPE_CODE = cmb_AML_WATCHLIST_ADDRESS_TYPE_CODE.SelectedItemValue
                    Else
                        .FK_AML_ADDRESS_TYPE_CODE = Nothing
                    End If
                    If Not String.IsNullOrEmpty(cmb_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.SelectedItemValue) Then
                        .FK_AML_COUNTRY_CODE = cmb_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.SelectedItemValue
                    Else
                        .FK_AML_COUNTRY_CODE = Nothing
                    End If

                    .ADDRESS = Trim(txt_AML_WATCHLIST_ADDRESS.Value)
                    .POSTAL_CODE = Trim(txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.Value)
                End With

                objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS.Add(objAdd)
            Else    'Edit
                Dim objEdit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS.Find(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID = objAML_WATCHLIST_ADDRESS_Edit.PK_AML_WATCHLIST_ADDRESS_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        If Not String.IsNullOrEmpty(cmb_AML_WATCHLIST_ADDRESS_TYPE_CODE.SelectedItemValue) Then
                            .FK_AML_ADDRESS_TYPE_CODE = cmb_AML_WATCHLIST_ADDRESS_TYPE_CODE.SelectedItemValue
                        Else
                            .FK_AML_ADDRESS_TYPE_CODE = Nothing
                        End If
                        If Not String.IsNullOrEmpty(cmb_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.SelectedItemValue) Then
                            .FK_AML_COUNTRY_CODE = cmb_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.SelectedItemValue
                        Else
                            .FK_AML_COUNTRY_CODE = Nothing
                        End If

                        .ADDRESS = Trim(txt_AML_WATCHLIST_ADDRESS.Value)
                        .POSTAL_CODE = Trim(txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.Value)
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_AML_WATCHLIST_ADDRESS()

            'Hide window popup
            Window_AML_WATCHLIST_ADDRESS.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ADDRESS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS)

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
                If objCountry IsNot Nothing Then
                    item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
                Else
                    item("COUNTRY_NAME") = Nothing
                End If

                Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
                If objAddressType IsNot Nothing Then
                    item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
                Else
                    item("AML_ADDRESS_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gp_AML_WATCHLIST_ADDRESS.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_ADDRESS.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_AML_WATCHLIST_ADDRESS()
        'Clean fields
        cmb_AML_WATCHLIST_ADDRESS_TYPE_CODE.SetTextValue("")
        txt_AML_WATCHLIST_ADDRESS.Value = Nothing
        cmb_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.SetTextValue("")
        txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.Value = Nothing

        'Set fields' ReadOnly
        cmb_AML_WATCHLIST_ADDRESS_TYPE_CODE.IsReadOnly = False
        txt_AML_WATCHLIST_ADDRESS.ReadOnly = False
        cmb_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.IsReadOnly = False
        txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.ReadOnly = False
        'Show Buttons
        btn_AML_WATCHLIST_ADDRESS_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_AML_WATCHLIST_ADDRESS(strAction As String)
        'Clean window pop up
        Clean_Window_AML_WATCHLIST_ADDRESS()

        'Populate fields
        With objAML_WATCHLIST_ADDRESS_Edit
            txt_AML_WATCHLIST_ADDRESS.Value = .ADDRESS
            txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.Value = .POSTAL_CODE

            If Not IsNothing(.FK_AML_ADDRESS_TYPE_CODE) Then
                Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(.FK_AML_ADDRESS_TYPE_CODE)
                If objAddressType IsNot Nothing Then
                    cmb_AML_WATCHLIST_ADDRESS_TYPE_CODE.SetTextWithTextValue(objAddressType.FK_AML_ADDRES_TYPE_CODE, objAddressType.ADDRESS_TYPE_NAME)
                End If
            End If

            If Not IsNothing(.FK_AML_COUNTRY_CODE) Then
                Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(.FK_AML_COUNTRY_CODE)
                If objCountry IsNot Nothing Then
                    cmb_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.SetTextWithTextValue(objCountry.FK_AML_COUNTRY_Code, objCountry.AML_COUNTRY_Name)
                End If
            End If

        End With

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            cmb_AML_WATCHLIST_ADDRESS_TYPE_CODE.IsReadOnly = True
            txt_AML_WATCHLIST_ADDRESS.ReadOnly = True
            cmb_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.IsReadOnly = True
            txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.ReadOnly = True
            btn_AML_WATCHLIST_ADDRESS_Save.Hidden = True
        End If

        'Show window pop up
        Window_AML_WATCHLIST_ADDRESS.Title = "Watch List Address - " & strAction
        Window_AML_WATCHLIST_ADDRESS.Hidden = False
    End Sub
    '------- END OF WATCHLIST ADDRESS

    '------- WATCHLIST IDENTITY
    Protected Sub btn_AML_WATCHLIST_IDENTITY_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objAML_WATCHLIST_IDENTITY_Edit = Nothing
            Clean_Window_AML_WATCHLIST_IDENTITY()

            'Show window pop up
            Window_AML_WATCHLIST_IDENTITY.Title = "Watch List Identity - Add"
            Window_AML_WATCHLIST_IDENTITY.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_AML_WATCHLIST_IDENTITY(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY.Find(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID = strID)
                If objToDelete IsNot Nothing Then
                    objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY.Remove(objToDelete)
                End If
                Bind_AML_WATCHLIST_IDENTITY()
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objAML_WATCHLIST_IDENTITY_Edit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY.Find(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID = strID)
                Load_Window_AML_WATCHLIST_IDENTITY(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_IDENTITY_Cancel_Click()
        Try
            'Hide window pop up
            Window_AML_WATCHLIST_IDENTITY.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_IDENTITY_Save_Click()
        Try
            'Validate input
            'If String.IsNullOrEmpty(cmb_AML_WATCHLIST_IDENTITY_TYPE_CODE.SelectedItemValue) Then
            '    Throw New ApplicationException(cmb_AML_WATCHLIST_IDENTITY_TYPE_CODE.Label & " harus diisi.")
            'End If
            If String.IsNullOrEmpty(txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.Value) Then
                Throw New ApplicationException(txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.FieldLabel & " harus diisi.")
            End If

            If Not String.IsNullOrEmpty(txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.Value) Then
                Dim strIdentityType As String = cmb_AML_WATCHLIST_IDENTITY_TYPE_CODE.SelectedItemValue
                Dim strSSN As String = txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.Value
                If strIdentityType = "KTP" And (Len(strSSN) <> 16 Or Not strSSN.All(AddressOf Char.IsDigit)) Then
                    Throw New ApplicationException(txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.FieldLabel & " must 16 digit numeric only for KTP.")
                End If
            End If

            'Action save here
            Dim intPK As Long = -1

            If objAML_WATCHLIST_IDENTITY_Edit Is Nothing Then  'Add
                Dim objAdd As New NawaDevDAL.AML_WATCHLIST_IDENTITY
                If objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY IsNot Nothing AndAlso objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY.Count > 0 Then
                    intPK = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY.Min(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                With objAdd
                    .PK_AML_WATCHLIST_IDENTITY_ID = intPK

                    If Not String.IsNullOrEmpty(cmb_AML_WATCHLIST_IDENTITY_TYPE_CODE.SelectedItemValue) Then
                        .FK_AML_IDENTITY_TYPE_CODE = cmb_AML_WATCHLIST_IDENTITY_TYPE_CODE.SelectedItemValue
                    Else
                        .FK_AML_IDENTITY_TYPE_CODE = Nothing
                    End If

                    .IDENTITYNUMBER = Trim(txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.Value)
                End With

                objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY.Add(objAdd)
            Else    'Edit
                Dim objEdit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY.Find(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID = objAML_WATCHLIST_IDENTITY_Edit.PK_AML_WATCHLIST_IDENTITY_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        If Not String.IsNullOrEmpty(cmb_AML_WATCHLIST_IDENTITY_TYPE_CODE.SelectedItemValue) Then
                            .FK_AML_IDENTITY_TYPE_CODE = cmb_AML_WATCHLIST_IDENTITY_TYPE_CODE.SelectedItemValue
                        Else
                            .FK_AML_IDENTITY_TYPE_CODE = Nothing
                        End If

                        .IDENTITYNUMBER = Trim(txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.Value)
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_AML_WATCHLIST_IDENTITY()

            'Hide window popup
            Window_AML_WATCHLIST_IDENTITY.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_IDENTITY()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY)

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
                If objIDENTITYType IsNot Nothing Then
                    item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
                Else
                    item("AML_IDENTITY_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gp_AML_WATCHLIST_IDENTITY.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_IDENTITY.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_AML_WATCHLIST_IDENTITY()
        'Clean fields
        cmb_AML_WATCHLIST_IDENTITY_TYPE_CODE.SetTextValue("")
        txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.Value = Nothing

        'Set fields' ReadOnly
        cmb_AML_WATCHLIST_IDENTITY_TYPE_CODE.IsReadOnly = False
        txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.ReadOnly = False

        'Show Buttons
        btn_AML_WATCHLIST_IDENTITY_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_AML_WATCHLIST_IDENTITY(strAction As String)
        'Clean window pop up
        Clean_Window_AML_WATCHLIST_IDENTITY()

        'Populate fields
        With objAML_WATCHLIST_IDENTITY_Edit
            txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.Value = .IDENTITYNUMBER

            If Not IsNothing(.FK_AML_IDENTITY_TYPE_CODE) Then
                Dim objIdentityType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(.FK_AML_IDENTITY_TYPE_CODE)
                If objIdentityType IsNot Nothing Then
                    cmb_AML_WATCHLIST_IDENTITY_TYPE_CODE.SetTextWithTextValue(objIdentityType.FK_AML_IDENTITY_TYPE_CODE, objIdentityType.IDENTITY_TYPE_NAME)
                End If
            End If
        End With

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            cmb_AML_WATCHLIST_IDENTITY_TYPE_CODE.IsReadOnly = True
            txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.ReadOnly = True
            btn_AML_WATCHLIST_IDENTITY_Save.Hidden = True
        End If

        'Show window pop up
        Window_AML_WATCHLIST_IDENTITY.Title = "Watch List Identity - " & strAction
        Window_AML_WATCHLIST_IDENTITY.Hidden = False
    End Sub
    '------- END OF WATCHLIST IDENTITY

    '------- DOCUMENT SAVE
    Protected Sub btn_WATCHLIST_Submit_Click()
        Try
            'Delete Data With/Without Approval
            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
                AML_WATCHLIST_BLL.SaveDeleteWithoutApproval(ObjModule, objAML_WATCHLIST_CLASS)
                LblConfirmation.Text = "Data Deleted from Database"
            Else
                AML_WATCHLIST_BLL.SaveDeleteWithApproval(ObjModule, objAML_WATCHLIST_CLASS)
                LblConfirmation.Text = "Data Deleted Pending Approval"
            End If

            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_WATCHLIST_Back_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF DOCUMENT SAVE

    Protected Sub btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SetCommandColumnLocation()
        ColumnActionLocation(gp_AML_WATCHLIST_ALIAS, cc_AML_WATCHLIST_ALIAS)
        ColumnActionLocation(gp_AML_WATCHLIST_ADDRESS, cc_AML_WATCHLIST_ADDRESS)
        ColumnActionLocation(gp_AML_WATCHLIST_IDENTITY, cc_AML_WATCHLIST_IDENTITY)
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SetControlReadOnly()
        'Read Only property
        cmb_AML_WATCHLIST_CATEGORY.IsReadOnly = True
        cmb_AML_WATCHLIST_TYPE.IsReadOnly = True
        txt_NAME.ReadOnly = True
        txt_PLACE_OF_BIRTH.ReadOnly = True
        txt_DATE_OF_BIRTH.ReadOnly = True
        txt_YOB.ReadOnly = True
        txt_NATIONALITY.ReadOnly = True
        txt_CUSTOM_REMARK_1.ReadOnly = True
        txt_CUSTOM_REMARK_2.ReadOnly = True
        txt_CUSTOM_REMARK_3.ReadOnly = True
        txt_CUSTOM_REMARK_4.ReadOnly = True
        txt_CUSTOM_REMARK_5.ReadOnly = True

        'Background Color
        txt_NAME.FieldStyle = "background-color:#ddd;"
        txt_PLACE_OF_BIRTH.FieldStyle = "background-color:#ddd;"
        txt_DATE_OF_BIRTH.FieldStyle = "background-color:#ddd;"
        txt_YOB.FieldStyle = "background-color:#ddd;"
        txt_NATIONALITY.FieldStyle = "background-color:#ddd;"
        txt_CUSTOM_REMARK_1.FieldStyle = "background-color:#ddd;"
        txt_CUSTOM_REMARK_2.FieldStyle = "background-color:#ddd;"
        txt_CUSTOM_REMARK_3.FieldStyle = "background-color:#ddd;"
        txt_CUSTOM_REMARK_4.FieldStyle = "background-color:#ddd;"
        txt_CUSTOM_REMARK_5.FieldStyle = "background-color:#ddd;"

        'Hide Button
        'btn_WATCHLIST_Submit.Hidden = True

        gp_AML_WATCHLIST_ALIAS.TopBar.Item(0).Hidden = True
        cc_AML_WATCHLIST_ALIAS.Commands.RemoveAt(1)
        cc_AML_WATCHLIST_ALIAS.Commands.RemoveAt(1)

        gp_AML_WATCHLIST_ADDRESS.TopBar.Item(0).Hidden = True
        cc_AML_WATCHLIST_ADDRESS.Commands.RemoveAt(1)
        cc_AML_WATCHLIST_ADDRESS.Commands.RemoveAt(1)

        gp_AML_WATCHLIST_IDENTITY.TopBar.Item(0).Hidden = True
        cc_AML_WATCHLIST_IDENTITY.Commands.RemoveAt(1)
        cc_AML_WATCHLIST_IDENTITY.Commands.RemoveAt(1)

    End Sub

    Private Sub AML_WATCHLIST_DELETE_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Delete
    End Sub

End Class