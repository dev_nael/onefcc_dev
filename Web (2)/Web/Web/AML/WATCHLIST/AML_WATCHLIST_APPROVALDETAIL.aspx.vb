﻿Imports Ext
Imports NawaDevBLL
Imports NawaDevDAL
Imports Elmah
Imports System.Data

Partial Class AML_WATCHLIST_APPROVALDETAIL
    Inherits ParentPage

    Public Property IDModule() As String
        Get
            Return Session("AML_WATCHLIST_APPROVALDETAIL.IDModule")
        End Get
        Set(ByVal value As String)
            Session("AML_WATCHLIST_APPROVALDETAIL.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("AML_WATCHLIST_APPROVALDETAIL.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AML_WATCHLIST_APPROVALDETAIL.IDUnik") = value
        End Set
    End Property

    Public Property objApproval() As NawaDAL.ModuleApproval
        Get
            Return Session("AML_WATCHLIST_APPROVALDETAIL.objApproval")
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            Session("AML_WATCHLIST_APPROVALDETAIL.objApproval") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_CLASS() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AML_WATCHLIST_APPROVALDETAIL.objAML_WATCHLIST_CLASS")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AML_WATCHLIST_APPROVALDETAIL.objAML_WATCHLIST_CLASS") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_CLASS_BEFORE() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AML_WATCHLIST_APPROVALDETAIL.objAML_WATCHLIST_CLASS_BEFORE")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AML_WATCHLIST_APPROVALDETAIL.objAML_WATCHLIST_CLASS_BEFORE") = value
        End Set
    End Property

    Sub ClearSession()
        objApproval = Nothing
        objAML_WATCHLIST_CLASS = New AML_WATCHLIST_CLASS
        objAML_WATCHLIST_CLASS_BEFORE = New AML_WATCHLIST_CLASS
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As Integer = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()

                FormPanelInput.Title = ObjModule.ModuleLabel & " - Approval"

                LoadModuleApproval()

                If Not String.IsNullOrEmpty(objApproval.ModuleFieldBefore) Then
                    LoadDataBefore()
                End If

                If Not String.IsNullOrEmpty(objApproval.ModuleField) Then
                    LoadDataAfter()
                End If

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    '------- LOAD MODULE APPROVAL
    Protected Sub LoadModuleApproval()
        Try
            ObjApproval = NawaBLL.ModuleApprovalBLL.GetModuleApprovalByID(IDUnik)
            If Not objApproval Is Nothing Then
                txt_ModuleApproval_ID.Value = objApproval.PK_ModuleApproval_ID
                Dim objModuleToApprove = NawaBLL.ModuleBLL.GetModuleByModuleName(objApproval.ModuleName)
                If objModuleToApprove IsNot Nothing Then
                    txt_ModuleLabel.Value = objModuleToApprove.ModuleLabel
                End If
                txt_ModuleKey.Text = objApproval.ModuleKey
                txt_Action.Text = NawaBLL.ModuleBLL.GetModuleActionNamebyID(objApproval.PK_ModuleAction_ID)

                Dim objUser = NawaBLL.MUserBLL.GetMuserbyUSerId(objApproval.CreatedBy)
                If objUser IsNot Nothing Then
                    txt_CreatedBy.Text = objApproval.CreatedBy & " - " & objUser.UserName
                Else
                    txt_CreatedBy.Text = objApproval.CreatedBy
                End If
                txt_CreatedDate.Text = objApproval.CreatedDate.Value.ToString("dd-MMM-yyyy")

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF LOAD MODULE APPROVAL

    '------- LOAD DATA BEFORE
    Protected Sub LoadDataBefore()
        Try
            If Not String.IsNullOrEmpty(objApproval.ModuleFieldBefore) Then
                objAML_WATCHLIST_CLASS_BEFORE = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(NawaDevBLL.AML_WATCHLIST_CLASS))
            End If

            LoadWatchList_Before()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadWatchList_Before()
        Try
            'Bind Data Header
            With objAML_WATCHLIST_CLASS_BEFORE.objAML_WATCHLIST
                txt_AML_WATCHLIST_ID_Before.Value = IIf(.PK_AML_WATCHLIST_ID = 0, "New Data", .PK_AML_WATCHLIST_ID)

                txt_AML_WATCHLIST_CATEGORY_Before.Value = .FK_AML_WATCHLIST_CATEGORY_ID
                If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                    Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                    If objWatchlistCategory IsNot Nothing Then
                        txt_AML_WATCHLIST_CATEGORY_Before.Value &= " - " & objWatchlistCategory.CATEGORY_NAME
                    End If
                End If

                If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                    txt_AML_WATCHLIST_TYPE_Before.Value = .FK_AML_WATCHLIST_TYPE_ID
                    Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                    If objWatchlistType IsNot Nothing Then
                        txt_AML_WATCHLIST_TYPE_Before.Value &= " - " & objWatchlistType.TYPE_NAME
                    End If
                End If

                txt_NAME_Before.Value = .NAME
                If .DATE_OF_BIRTH.HasValue Then
                    txt_DATE_OF_BIRTH_Before.Value = .DATE_OF_BIRTH.GetValueOrDefault().ToString("dd-MMM-yyyy")
                End If
                txt_PLACE_OF_BIRTH_Before.Value = .BIRTH_PLACE
                txt_YOB_Before.Value = .YOB
                txt_NATIONALITY_Before.Value = .NATIONALITY

                txt_CUSTOM_REMARK_1_Before.Value = .CUSTOM_REMARK_1
                txt_CUSTOM_REMARK_2_Before.Value = .CUSTOM_REMARK_2
                txt_CUSTOM_REMARK_3_Before.Value = .CUSTOM_REMARK_3
                txt_CUSTOM_REMARK_4_Before.Value = .CUSTOM_REMARK_4
                txt_CUSTOM_REMARK_5_Before.Value = .CUSTOM_REMARK_5
            End With

            'Bind Data Detail to Grid Panel
            Bind_AML_WATCHLIST_ALIAS_Before()
            Bind_AML_WATCHLIST_ADDRESS_Before()
            Bind_AML_WATCHLIST_IDENTITY_Before()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ALIAS_Before()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS_BEFORE.objList_AML_WATCHLIST_ALIAS)

        'Running the following script for additional columns
        'objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        'If objtable.Rows.Count > 0 Then
        '    For Each item As Data.DataRow In objtable.Rows
        '        item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
        '    Next
        'End If
        gp_AML_WATCHLIST_ALIAS_Before.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_ALIAS_Before.GetStore().DataBind()
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ADDRESS_Before()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS_BEFORE.objList_AML_WATCHLIST_ADDRESS)

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
                If objCountry IsNot Nothing Then
                    item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
                Else
                    item("COUNTRY_NAME") = Nothing
                End If

                Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
                If objAddressType IsNot Nothing Then
                    item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
                Else
                    item("AML_ADDRESS_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gp_AML_WATCHLIST_ADDRESS_Before.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_ADDRESS_Before.GetStore().DataBind()
    End Sub

    Protected Sub Bind_AML_WATCHLIST_IDENTITY_Before()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS_BEFORE.objList_AML_WATCHLIST_IDENTITY)

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
                If objIDENTITYType IsNot Nothing Then
                    item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
                Else
                    item("AML_IDENTITY_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gp_AML_WATCHLIST_IDENTITY_Before.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_IDENTITY_Before.GetStore().DataBind()
    End Sub
    '------- END OF LOAD DATA BEFORE

    '------- LOAD DATA AFTER
    Protected Sub LoadDataAfter()
        Try
            objAML_WATCHLIST_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(NawaDevBLL.AML_WATCHLIST_CLASS))

            LoadWatchList()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadWatchList()
        Try
            'Bind Data Header
            With objAML_WATCHLIST_CLASS.objAML_WATCHLIST
                txt_AML_WATCHLIST_ID.Value = IIf(.PK_AML_WATCHLIST_ID = 0, "New Data", .PK_AML_WATCHLIST_ID)

                txt_AML_WATCHLIST_CATEGORY.Value = .FK_AML_WATCHLIST_CATEGORY_ID
                If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                    Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                    If objWatchlistCategory IsNot Nothing Then
                        txt_AML_WATCHLIST_CATEGORY.Value &= " - " & objWatchlistCategory.CATEGORY_NAME
                    End If
                End If

                If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                    txt_AML_WATCHLIST_TYPE.Value = .FK_AML_WATCHLIST_TYPE_ID
                    Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                    If objWatchlistType IsNot Nothing Then
                        txt_AML_WATCHLIST_TYPE.Value &= " - " & objWatchlistType.TYPE_NAME
                    End If
                End If

                txt_NAME.Value = .NAME
                If .DATE_OF_BIRTH.HasValue Then
                    txt_DATE_OF_BIRTH.Value = .DATE_OF_BIRTH.GetValueOrDefault().ToString("dd-MMM-yyyy")
                End If
                txt_PLACE_OF_BIRTH.Value = .BIRTH_PLACE
                txt_YOB.Value = .YOB
                txt_NATIONALITY.Value = .NATIONALITY

                txt_CUSTOM_REMARK_1.Value = .CUSTOM_REMARK_1
                txt_CUSTOM_REMARK_2.Value = .CUSTOM_REMARK_2
                txt_CUSTOM_REMARK_3.Value = .CUSTOM_REMARK_3
                txt_CUSTOM_REMARK_4.Value = .CUSTOM_REMARK_4
                txt_CUSTOM_REMARK_5.Value = .CUSTOM_REMARK_5
            End With

            'Bind Data Detail to Grid Panel
            Bind_AML_WATCHLIST_ALIAS()
            Bind_AML_WATCHLIST_ADDRESS()
            Bind_AML_WATCHLIST_IDENTITY()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ALIAS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS)

        'Running the following script for additional columns
        'objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        'If objtable.Rows.Count > 0 Then
        '    For Each item As Data.DataRow In objtable.Rows
        '        item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
        '    Next
        'End If
        gp_AML_WATCHLIST_ALIAS.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_ALIAS.GetStore().DataBind()
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ADDRESS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS)

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
                If objCountry IsNot Nothing Then
                    item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
                Else
                    item("COUNTRY_NAME") = Nothing
                End If

                Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
                If objAddressType IsNot Nothing Then
                    item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
                Else
                    item("AML_ADDRESS_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gp_AML_WATCHLIST_ADDRESS.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_ADDRESS.GetStore().DataBind()
    End Sub

    Protected Sub Bind_AML_WATCHLIST_IDENTITY()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY)

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
                If objIDENTITYType IsNot Nothing Then
                    item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
                Else
                    item("AML_IDENTITY_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gp_AML_WATCHLIST_IDENTITY.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_IDENTITY.GetStore().DataBind()
    End Sub
    '------- END OF LOAD DATA AFTER

    '------- DOCUMENT APPROVE/REJECT
    Protected Sub btn_WATCHLIST_Approve_Click()
        Try
            'Approve Data
            AML_WATCHLIST_BLL.Accept(objApproval.PK_ModuleApproval_ID)
            LblConfirmation.Text = "Data Approved"

            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_WATCHLIST_Reject_Click()
        Try
            'Reject Data
            AML_WATCHLIST_BLL.Reject(objApproval.PK_ModuleApproval_ID)
            LblConfirmation.Text = "Data Rejected"

            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_WATCHLIST_Back_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF DOCUMENT SAVE

    Protected Sub btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AML_WATCHLIST_APPROVALDETAIL_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Approval
    End Sub

End Class