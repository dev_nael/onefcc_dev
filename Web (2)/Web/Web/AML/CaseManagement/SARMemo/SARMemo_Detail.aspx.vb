﻿Imports NawaDevDAL
Imports NawaDevBLL
Imports CasemanagementDAL
Imports CasemanagementBLL
Imports System.Data
Imports System.Data.SqlClient
Imports NawaBLL
Imports OfficeOpenXml
Imports System.IO
Imports System.Globalization
Partial Class AML_CaseManagement_SARMemo_SARMemo_Detail
    Inherits ParentPage
    Public objFormModuleAdd As NawaBLL.FormModuleAdd
    Public objFormModuleView As FormModuleView

    Public Property IDUnik() As Long
        Get
            Return Session("SARMemo_Detail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("SARMemo_Detail.IDUnik") = value
        End Set
    End Property
    Public Property objSARMemoClass As SARMemo_BLL.SARMemoClass
        Get
            If Session("SARMemo_Detail.objSARMemoClass") Is Nothing Then
                Session("SARMemo_Detail.objSARMemoClass") = New SARMemo_BLL.SARMemoClass
            End If

            Return Session("SARMemo_Detail.objNewsClass")
        End Get
        Set(value As SARMemo_BLL.SARMemoClass)
            Session("SARMemo_Detail.objNewsClass") = value
        End Set
    End Property
    'Public Property ObjModule() As NawaDAL.Module
    '    Get
    '        Return Session("SARMemo_Detail.ObjModule")
    '    End Get
    '    Set(ByVal value As NawaDAL.Module)
    '        Session("SARMemo_Detail.ObjModule") = value
    '    End Set
    'End Property

    Sub SetCommandColumnLocation()
        'dim settingvaluestring as String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select SettingValue FROM systemParameter where PK_SystemParameterID = '32'", Nothing)
        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim buttonPosition As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            buttonPosition = objParamSettingbutton.SettingValue
        End If
        'buttonPosition = settingvaluestring

        'Report Level

        ColumnActionLocation(gp_indikator, cc_indikator, buttonPosition)
        ColumnActionLocation(gp_Pihak_Terkait, cc_pihakterkait, buttonPosition)
        ColumnActionLocation(gp_TIndakLanjut, cc_tindaklanjut, buttonPosition)
        ColumnActionLocation(gp_activity, cc_Activity, buttonPosition)
    End Sub

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        If buttonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub


    Private Sub SARMemo_Detail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()

                SetCommandColumnLocation()
                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If
                LoadData()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ClearSession()
        'objEmployee = Nothing
        'ObjModule = Nothing
        objSARMemoClass = New SARMemo_BLL.SARMemoClass
        obj_ListIndikator_Edit = Nothing
        obj_ListPihakTerkait_Edit = Nothing
        obj_ListTindakLanjut_Edit = Nothing
    End Sub



    Private Sub LoadData()
        Try
            displayPK.ReadOnly = True
            displayPK.Text = True
            txtCIF.ReadOnly = True
            txtNo.ReadOnly = True
            txtKepada.ReadOnly = True
            txtDari.ReadOnly = True
            txtTanggal.ReadOnly = True
            txtPerihal.ReadOnly = True
            txtSumberPelaporan.ReadOnly = True
            txtNamaPihakPelapor.ReadOnly = True
            txtKategoriPihakPelapor.ReadOnly = True
            txtPekerjaanBidangUsaha.ReadOnly = True
            txtPengahasilanTahun.ReadOnly = True
            txtProfilLainnya.ReadOnly = True
            txtHasilAnalisis.ReadOnly = True
            txtKesimpulan.ReadOnly = True
            cmb_KesimpulanDetail.IsReadOnly = True
            LblLatarBelakang.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblPihakCalonTerkait.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblPihakTerkait.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblHasilAnalisis.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblKesimpulan.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblTindakLanjut.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblTransaksiSAR.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblWorkflowHistory.StyleSpec = "font-weight: bold;font-size : 20px;"
            objSARMemoClass = CasemanagementBLL.SARMemo_BLL.getSARMemoClassByID(IDUnik)
            '' Edit 5 Oct 2022
            'Using objdb As New CasemanagementEntities
            '    Dim listHistory As List(Of OneFCC_CaseManagement_WorkflowHistory_SARMemo) = objdb.OneFCC_CaseManagement_WorkflowHistory_SARMemo.Where(Function(x) x.FK_OneFCC_CaseManagement_SARMemo_ID = IDUnik).ToList
            '    If listHistory.Count > 0 Then
            '        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listHistory)
            '        gp_workflowhistory.GetStore().DataSource = objtable
            '        gp_workflowhistory.GetStore().DataBind()
            '    End If
            'End Using

            Dim listHistory As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_WorkflowHistory_SARMemo where FK_OneFCC_CaseManagement_SARMemo_ID = " & objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID & "", Nothing)
            'NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select Attachment, AttachmentName FROM OneFCC_CaseManagement_WorkflowHistory where PK_CaseManagement_WorkflowHistoryID = " & ID & "", Nothing)
            If listHistory IsNot Nothing Then
                gp_workflowhistory.GetStore().DataSource = listHistory
                gp_workflowhistory.GetStore().DataBind()
            End If

            If objSARMemoClass.listCaseID.Count > 0 Then
                Dim objtable As New Data.DataTable

                '' Edit 5-Oct-2022 Felix 
                'Using objdb As New CasemanagementEntities
                '    Dim listCaseID As New List(Of OneFCC_CaseManagement)
                '    Dim listCaseIDSAR As List(Of OneFCC_CaseManagement_SARMemo_CaseID) = objdb.OneFCC_CaseManagement_SARMemo_CaseID.Where(Function(x) x.FK_OneFCC_CaseManagement_SARMemo_ID = objSARMemoClass.objSARMemo.PK_OneFCC_CaseManagement_SARMemoID).ToList
                '    Dim objtableTemp As New Data.DataTable
                '    For Each item In objSARMemoClass.listCaseID

                '        Dim CaseID As OneFCC_CaseManagement = objdb.OneFCC_CaseManagement.Where(Function(x) x.PK_CaseManagement_ID = item.CaseID).FirstOrDefault
                '        listCaseID.Add(CaseID)

                '    Next
                '    For Each item In listCaseID
                '        For Each item2 In listCaseIDSAR
                '            If item.PK_CaseManagement_ID = item2.CaseID Then
                '                item.FK_OneFCC_CaseManagement_SARMemo_ID = item2.FK_OneFCC_CaseManagement_SARMemo_ID
                '            End If
                '        Next
                '    Next
                '    objtable = NawaBLL.Common.CopyGenericToDataTable(listCaseID)
                '    objtable.Columns.Add(New DataColumn("Rule", GetType(String)))
                '    For Each item As Data.DataRow In objtable.Rows
                '        If Not IsDBNull(item("FK_Rule_Basic_ID")) Then
                '            'Dim RuleBasic As New OneFCC_MS_Rule_Basic
                '            'Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                '            'RuleBasic = objdb.OneFCC_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault
                '            'item("Rule") = RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                '        End If

                '        If IsDBNull(item("FK_OneFCC_CaseManagement_SARMemo_ID")) Then
                '            item("Memo_No") = "0 - New"
                '        Else
                '            Dim StrMemo As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo
                '            Dim FkSTRMemo As String = item("FK_OneFCC_CaseManagement_SARMemo_ID").ToString
                '            StrMemo = objdb.OneFCC_CaseManagement_SARMemo.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemoID = FkSTRMemo).FirstOrDefault
                '            If StrMemo.StatusID = "1" Then
                '                item("Memo_No") = "1 - On Progress"
                '            ElseIf StrMemo.StatusID = "2" Then
                '                item("Memo_No") = "2 - Approved"
                '            ElseIf StrMemo.StatusID = "3" Then
                '                item("Memo_No") = "3 - Rejected"
                '            ElseIf StrMemo.StatusID = "4" Then
                '                item("Memo_No") = "4 - Generate"
                '            ElseIf StrMemo.StatusID = "5" Then
                '                item("Memo_No") = "5 - On Progress Generate Report"
                '            End If
                '        End If
                '    Next
                '    gp_case_alert.GetStore().DataSource = objtable
                '    gp_case_alert.GetStore().DataBind()
                'End Using


                Dim listCaseID As List(Of OneFCC_CaseManagement) = New List(Of OneFCC_CaseManagement)
                Dim listCaseIDSAR As New DataTable '' List(Of OneFCC_CaseManagement_SARMemo_CaseID) = 
                listCaseIDSAR = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_SARMemo_CaseID where FK_OneFCC_CaseManagement_SARMemo_ID = " & objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID)

                Dim objtableTemp As New Data.DataTable
                For Each item In objSARMemoClass.listCaseID

                    Dim drCaseID As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select * FROM OneFCC_CaseManagement where PK_CaseManagement_ID = " & item.CaseID)
                    Dim CaseID As OneFCC_CaseManagement = New OneFCC_CaseManagement()
                    If drCaseID IsNot Nothing Then
                        If Not IsDBNull(drCaseID("PK_CaseManagement_ID")) Then
                            CaseID.PK_CaseManagement_ID = drCaseID("PK_CaseManagement_ID")
                        End If

                        If Not IsDBNull(drCaseID("Alert_Type")) Then
                            CaseID.Alert_Type = drCaseID("Alert_Type")
                        End If

                        If Not IsDBNull(drCaseID("FK_Rule_Basic_ID")) Then
                            CaseID.FK_Rule_Basic_ID = drCaseID("FK_Rule_Basic_ID")
                        End If

                        If Not IsDBNull(drCaseID("Case_Description")) Then
                            CaseID.Case_Description = drCaseID("Case_Description")
                        End If

                        If Not IsDBNull(drCaseID("Is_Customer")) Then
                            CaseID.Is_Customer = drCaseID("Is_Customer")
                        End If

                        If Not IsDBNull(drCaseID("CIF_No")) Then
                            CaseID.CIF_No = drCaseID("CIF_No")
                        End If

                        If Not IsDBNull(drCaseID("Account_No")) Then
                            CaseID.Account_No = drCaseID("Account_No")
                        End If

                        If Not IsDBNull(drCaseID("Customer_Name")) Then
                            CaseID.Customer_Name = drCaseID("Customer_Name")
                        End If

                        If Not IsDBNull(drCaseID("WIC_No")) Then
                            CaseID.WIC_No = drCaseID("WIC_No")
                        End If

                        If Not IsDBNull(drCaseID("WIC_Name")) Then
                            CaseID.WIC_Name = drCaseID("WIC_Name")
                        End If

                        If Not IsDBNull(drCaseID("FK_Proposed_Status_ID")) Then
                            CaseID.FK_Proposed_Status_ID = drCaseID("FK_Proposed_Status_ID")
                        End If

                        If Not IsDBNull(drCaseID("Proposed_By")) Then
                            CaseID.Proposed_By = drCaseID("Proposed_By")
                        End If

                        If Not IsDBNull(drCaseID("FK_CaseStatus_ID")) Then
                            CaseID.FK_CaseStatus_ID = drCaseID("FK_CaseStatus_ID")
                        End If

                        If Not IsDBNull(drCaseID("FK_CaseManagement_Workflow_ID")) Then
                            CaseID.FK_CaseManagement_Workflow_ID = drCaseID("FK_CaseManagement_Workflow_ID")
                        End If

                        If Not IsDBNull(drCaseID("Workflow_Step")) Then
                            CaseID.Workflow_Step = drCaseID("Workflow_Step")
                        End If

                        If Not IsDBNull(drCaseID("PIC")) Then
                            CaseID.PIC = drCaseID("PIC")
                        End If

                        If Not IsDBNull(drCaseID("Aging")) Then
                            CaseID.Aging = drCaseID("Aging")
                        End If

                        If Not IsDBNull(drCaseID("StatusRFI")) Then
                            CaseID.StatusRFI = drCaseID("StatusRFI")
                        End If

                        If Not IsDBNull(drCaseID("StatusSTRMemo")) Then
                            CaseID.StatusSTRMemo = drCaseID("StatusSTRMemo")
                        End If

                        If Not IsDBNull(drCaseID("ProcessDate")) Then
                            CaseID.ProcessDate = drCaseID("ProcessDate")
                        End If

                        If Not IsDBNull(drCaseID("FK_Report_ID")) Then
                            CaseID.FK_Report_ID = drCaseID("FK_Report_ID")
                        End If

                        If Not IsDBNull(drCaseID("Active")) Then
                            CaseID.Active = drCaseID("Active")
                        End If

                        If Not IsDBNull(drCaseID("CreatedBy")) Then
                            CaseID.CreatedBy = drCaseID("CreatedBy")
                        End If

                        If Not IsDBNull(drCaseID("LastUpdateBy")) Then
                            CaseID.LastUpdateBy = drCaseID("LastUpdateBy")
                        End If

                        If Not IsDBNull(drCaseID("ApprovedBy")) Then
                            CaseID.ApprovedBy = drCaseID("ApprovedBy")
                        End If

                        If Not IsDBNull(drCaseID("CreatedDate")) Then
                            CaseID.CreatedDate = drCaseID("CreatedDate")
                        End If

                        If Not IsDBNull(drCaseID("LastUpdateDate")) Then
                            CaseID.LastUpdateDate = drCaseID("LastUpdateDate")
                        End If

                        If Not IsDBNull(drCaseID("ApprovedDate")) Then
                            CaseID.ApprovedDate = drCaseID("ApprovedDate")
                        End If

                        If Not IsDBNull(drCaseID("Alternateby")) Then
                            CaseID.Alternateby = drCaseID("Alternateby")
                        End If

                        'If Not IsDBNull(drCaseID("Rule")) Then
                        '    CaseID.Account_No = drCaseID("Rule")
                        'End If
                        'If Not IsDBNull(drCaseID("FK_OneFCC_CaseManagement_SARMemo_ID")) Then
                        '    CaseID.FK_OneFCC_CaseManagement_SARMemo_ID = drCaseID("FK_OneFCC_CaseManagement_SARMemo_ID")
                        'End If
                        If Not IsDBNull(drCaseID("Memo_No")) Then
                            CaseID.Memo_No = drCaseID("Memo_No")
                        End If
                    End If

                    listCaseID.Add(CaseID)

                Next
                For Each item In listCaseID
                    For Each item2 As DataRow In listCaseIDSAR.Rows
                        ' Jika case ID yang ditemukan di casemanagement ternyata sudah ada di case STR Memo
                        If item.PK_CaseManagement_ID = item2("CaseID") Then
                            ' maka fk str memo dari case id itu isi dengan fk str memo
                            Dim FKSARMemo As String = ""
                            FKSARMemo = item2("FK_OneFCC_CaseManagement_SARMemo_ID").ToString
                            'item.FK_OneFCC_CaseManagement_SARMemo_ID = FKSARMemo
                        End If
                    Next
                Next
                objtable = NawaBLL.Common.CopyGenericToDataTable(listCaseID)
                objtable.Columns.Add(New DataColumn("Rule", GetType(String)))
                objtable.Columns.Add(New DataColumn("UniqueCMID", GetType(String))) '' Add 7-Mar-23, Ari. FAMA tambah 1 field baru untuk unique cm id
                For Each item As Data.DataRow In objtable.Rows
                    If Not IsDBNull(item("FK_Rule_Basic_ID")) Then
                        '' Edit 5-Oct-2022 Felix
                        'Dim RuleBasic As New OneFCC_MS_Rule_Basic
                        'Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                        'RuleBasic = objdb.OneFCC_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault
                        'item("Rule") = RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                        'Dim RuleBasic As New OneFcc_MS_Rule_Basic
                        Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                        'RuleBasic = objdb.OneFcc_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault '' Edit 5 Oct 2022
                        Dim RuleBasic As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select cast (PK_Rule_Basic_ID as varchar(100)) + ' - ' + Rule_Basic_Name from OneFcc_MS_Rule_Basic where PK_Rule_Basic_ID = " & FKRule & "", Nothing)
                        item("Rule") = RuleBasic ' RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                        '' End 5-Oct-2022
                    End If
                    '' Add 7-Mar-23, Ari. FAMA tambah 1 field baru untuk unique cm id
                    If item("PK_CaseManagement_ID") > 0 Then
                        Dim PKCM As Long = Convert.ToInt64(item("PK_CaseManagement_ID"))
                        Dim UniqueCMDID As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select Unique_CM_ID from OneFCC_CaseManagement where PK_CaseManagement_ID = " & PKCM, Nothing).ToString
                        item("UniqueCMID") = UniqueCMDID
                    End If
                    Dim PKCasManagement As String
                    Dim FKSARMemo_FromTable As String = ""
                    If Not IsDBNull(item("PK_CaseManagement_ID")) Then
                        PKCasManagement = item("PK_CaseManagement_ID").ToString
                        FKSARMemo_FromTable = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select isnull(FK_OneFCC_CaseManagement_SARMemo_ID,'') from OneFCC_CaseManagement_SARMemo_CaseID where caseID = " & PKCasManagement, Nothing)
                    End If



                    ' Jika case ID nya tidak ada di Case ID STR Memo maka case id nya baru
                    ' Jika case ID nya tidak ada di Case ID STR Memo maka case id nya baru
                    If FKSARMemo_FromTable Is Nothing Or FKSARMemo_FromTable = "" Then
                        item("Memo_No") = "0 - New"
                    Else
                        Dim SARMemo As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo
                        Dim FkSARMemo As String = FKSARMemo_FromTable
                        'StrMemo = objdb.OneFCC_CaseManagement_SARMemo.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemoID = FkSTRMemo).FirstOrDefault
                        Dim drStrMemo As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select StatusID from OneFCC_CaseManagement_SARMemo where PK_OneFCC_CaseManagement_SARMemo_ID = " & FkSARMemo, Nothing)

                        If drStrMemo IsNot Nothing Then
                            If drStrMemo("StatusID") = "1" Then
                                item("Memo_No") = "1 - On Progress"
                            ElseIf drStrMemo("StatusID") = "2" Then
                                item("Memo_No") = "2 - Approved"
                            ElseIf drStrMemo("StatusID") = "3" Then
                                item("Memo_No") = "3 - Rejected"
                            ElseIf drStrMemo("StatusID") = "4" Then
                                item("Memo_No") = "4 - Generate"
                            ElseIf drStrMemo("StatusID") = "5" Then
                                item("Memo_No") = "5 - On Progress Generate Report"
                            End If
                        End If


                    End If
                Next
                gp_case_alert.GetStore().DataSource = objtable
                gp_case_alert.GetStore().DataBind()
                '' End 5 Oct 2022

            End If
            If objSARMemoClass.listIndikator.Count > 0 Then
                obj_ListIndikator_Edit = objSARMemoClass.listIndikator
                Bind_Indikator()
            End If
            If objSARMemoClass.listPihakTerkait.Count > 0 Then
                obj_ListPihakTerkait_Edit = objSARMemoClass.listPihakTerkait
                Bind_Pihak_Terkait()

            End If
            If objSARMemoClass.listTindakLanjut.Count > 0 Then
                obj_ListTindakLanjut_Edit = objSARMemoClass.listTindakLanjut
                Bind_TindakLanjut()
            End If
            If objSARMemoClass.listAttachment.Count > 0 Then
                obj_ListAttachment_Edit = objSARMemoClass.listAttachment
                bindAttachment(StoreAttachment, obj_ListAttachment_Edit)
            End If
            If objSARMemoClass.listActivity.Count > 0 Then
                obj_ListActivity_Edit = objSARMemoClass.listActivity
                Bind_Activity()
            End If
            'If objSARMemoClass.listTransaction.Count > 0 Then
            '    Dim transactiontable As OneFCC_CaseManagement_SARMemo_Transaction = objSARMemoClass.listTransaction.Where(Function(x) x.FK_OneFCC_CaseManagement_SARMemo_ID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID).FirstOrDefault
            '    Dim datefrom As DateTime
            '    Dim dateto As DateTime
            '    Dim param_RadioOption As String = ""
            '    Dim noid As String = ""
            '    With transactiontable
            '        TanggalLaporan.Value = .Tanggal_Laporan
            '        sar_jenisLaporan.Value = .Jenis_Laporan
            '        alasan.Value = .Alasan
            '        If .Jenis_Laporan = "LTKMP" Then
            '            NoRefPPATK.Value = .NoRefPPATK
            '            NoRefPPATK.Hidden = False
            '        Else
            '            NoRefPPATK.Value = Nothing
            '            NoRefPPATK.Hidden = True
            '        End If
            '        If .Date_Form IsNot Nothing Then
            '            datefrom = .Date_Form
            '            dateto = .Date_To
            '        Else
            '            datefrom = DateTime.Now
            '            dateto = DateTime.Now
            '        End If

            '        If .FilterTransaction IsNot Nothing Then
            '            If .FilterTransaction = "TrnHitByCaseAlert" Then
            '                Dim Keterangan As String = "Transactions Hit By Case Alert"
            '                Dim StrQueryCode As String = "select kode from vw_OneFCC_TransactionFilter where kode = 1"
            '                Dim Kode As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
            '                dd_TrnFilterBy.SetTextWithTextValue(Kode, Keterangan)
            '            ElseIf .FilterTransaction = "Trn5Year" Then
            '                Dim Keterangan As String = "Transaction 5 years back, starts from H-1"
            '                Dim StrQueryCode As String = "select kode from vw_OneFCC_TransactionFilter where kode = 2"
            '                Dim Kode As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
            '                dd_TrnFilterBy.SetTextWithTextValue(Kode, Keterangan)
            '            ElseIf .FilterTransaction = "CustomDate" Then
            '                Dim Keterangan As String = "Custom Date"
            '                Dim StrQueryCode As String = "select kode from vw_OneFCC_TransactionFilter where kode = 3"
            '                Dim Kode As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
            '                dd_TrnFilterBy.SetTextWithTextValue(Kode, Keterangan)
            '            End If
            '        End If
            '        dd_TrnFilterBy.IsReadOnly = True
            '        If .FilterTransaction = "CustomDate" Then
            '            sar_DateFrom.Value = .Date_Form
            '            sar_DateTo.Value = .Date_To
            '            sar_DateFrom.Hidden = False
            '            sar_DateTo.Hidden = False
            '            sar_DateFrom.ReadOnly = True
            '            sar_DateTo.ReadOnly = True
            '        End If
            '        param_RadioOption = .FilterTransaction
            '        noid = .NO_IDTransaction
            '    End With
            '    Dim tbl_goAML_odm_Transaksi As New DataTable

            '    Dim StrQueryTransaction As String = "select * from OneFCC_CaseManagement_SARMemo_Transaction where FK_OneFCC_CaseManagement_SARMemo_ID = " & objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID
            '    tbl_goAML_odm_Transaksi = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryTransaction, Nothing)
            '    'For Each item In objSARMemoClass.listCaseID
            '    '    Dim recordID = item.CaseID
            '    '    Dim tbl_goAML_odm_Transaksi_temp As New DataTable
            '    '    Dim objGetTrn(3) As SqlParameter
            '    '    objGetTrn(0) = New SqlParameter
            '    '    objGetTrn(0).ParameterName = "@CaseID"
            '    '    objGetTrn(0).Value = recordID

            '    '    objGetTrn(1) = New SqlParameter
            '    '    objGetTrn(1).ParameterName = "@RadioOption"
            '    '    objGetTrn(1).Value = param_RadioOption

            '    '    objGetTrn(2) = New SqlParameter
            '    '    objGetTrn(2).ParameterName = "@DateFrom"
            '    '    objGetTrn(2).Value = datefrom.ToString("yyyy-MM-dd")

            '    '    objGetTrn(3) = New SqlParameter
            '    '    objGetTrn(3).ParameterName = "@DateTo"
            '    '    objGetTrn(3).Value = dateto.ToString("yyyy-MM-dd")

            '    '    tbl_goAML_odm_Transaksi_temp = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_OneFCC_GetTransactionOfCaseAlert", objGetTrn)
            '    '    tbl_goAML_odm_Transaksi.Merge(tbl_goAML_odm_Transaksi_temp)
            '    'Next

            '    For Each row As DataRow In tbl_goAML_odm_Transaksi.Rows

            '        Dim Transaction As NawaDevDAL.goAML_ODM_Transaksi = New NawaDevDAL.goAML_ODM_Transaksi
            '        Transaction.Account_NO = row.Item("Account_NO")
            '        Transaction.ACCOUNT_No_Lawan = row.Item("ACCOUNT_No_Lawan")
            '        Transaction.Active = row.Item("Active")
            '        If Not IsDBNull(row.Item("Alternateby")) Then
            '            Transaction.Alternateby = row.Item("Alternateby")
            '        End If
            '        If Not IsDBNull(row.Item("ApprovedBy")) Then
            '            Transaction.ApprovedBy = row.Item("ApprovedBy")
            '        End If
            '        If Not IsDBNull(row.Item("ApprovedDate")) Then
            '            Transaction.ApprovedDate = row.Item("ApprovedDate")
            '        End If
            '        If Not IsDBNull(row.Item("CreatedBy")) Then
            '            Transaction.CreatedBy = row.Item("CreatedBy")
            '        End If
            '        If Not IsDBNull(row.Item("CreatedDate")) Then
            '            Transaction.CreatedDate = row.Item("CreatedDate")
            '        End If
            '        If Not IsDBNull(row.Item("LastUpdateBy")) Then
            '            Transaction.LastUpdateBy = row.Item("LastUpdateBy")
            '        End If
            '        If Not IsDBNull(row.Item("LastUpdateDate")) Then
            '            Transaction.LastUpdateDate = row.Item("LastUpdateDate")
            '        End If

            '        Transaction.Authorized = row.Item("Authorized")
            '        Transaction.BiMultiParty = row.Item("BiMultiParty")
            '        Transaction.Business_date = row.Item("Business_date")
            '        Transaction.CIF_NO = row.Item("CIF_NO")
            '        Transaction.CIF_No_Lawan = row.Item("CIF_No_Lawan")
            '        Transaction.Comments = row.Item("Comments")
            '        Transaction.ConductorID = row.Item("Conductor_ID")
            '        Transaction.Country_Code = row.Item("Country_Code")
            '        Transaction.Country_Code_Lawan = row.Item("Country_Code_Lawan")
            '        Transaction.Currency = row.Item("Currency")
            '        Transaction.Currency_Lawan = row.Item("Currency_Lawan")
            '        Transaction.Date_Posting = row.Item("Date_Posting")
            '        Transaction.Date_Transaction = row.Item("Date_Transaction")
            '        Transaction.Debit_Credit = row.Item("Debit_Credit")
            '        Transaction.Exchange_Rate = row.Item("Exchange_Rate")
            '        Transaction.From_Funds_Code = row.Item("From_Funds_Code")
            '        Transaction.From_Type = row.Item("From_Type")
            '        Transaction.GCN = row.Item("GCN")
            '        Transaction.GCN_Lawan = row.Item("GCN_Lawan")
            '        Transaction.IDR_Amount = row.Item("IDR_Amount")
            '        'Transaction.MsgSwiftType = row.Item("MsgSwiftType")
            '        Transaction.NOID = row.Item("Pk_OneFCC_CaseManagement_SARMemo_Transaction_ID")
            '        Transaction.Original_Amount = row.Item("Original_Amount")
            '        Transaction.Ref_Num = row.Item("Ref_Num")
            '        Transaction.Source_Data = row.Item("Source_Data")
            '        Transaction.Swift_Code_Lawan = row.Item("Swift_Code_Lawan")
            '        Transaction.Teller = row.Item("Teller")
            '        Transaction.To_Funds_Code = row.Item("To_Funds_Code")
            '        Transaction.To_Type = row.Item("To_Type")
            '        Transaction.Transaction_Code = row.Item("Transaction_Code")
            '        Transaction.Transaction_Location = row.Item("Transaction_Location")
            '        Transaction.Transaction_Number = row.Item("Transaction_Number")
            '        Transaction.Transaction_Remark = row.Item("Transaction_Remark")
            '        Transaction.Transmode_Code = row.Item("Transmode_Code")
            '        Transaction.Transmode_Comment = row.Item("Transmode_Comment")
            '        Transaction.WIC_No = row.Item("WIC_No")
            '        Transaction.WIC_No_Lawan = row.Item("WIC_No_Lawan")


            '        ListTransaction.Add(Transaction)
            '    Next

            '    'Dim JumlahListTrans As Integer = objSARMemoClass.listTransaction.Count
            '    'Dim CountTrans As Integer = 0
            '    'If noid = "ALL" Then
            '    'Else
            '    '    For Each itemlist In ListTransaction.ToList
            '    '        If CountTrans < JumlahListTrans Then
            '    '            Dim CountListTrans2 As Integer = 0
            '    '            For Each itemstrmemolist In objSARMemoClass.listTransaction.ToList
            '    '                If itemlist.Transaction_Number <> itemstrmemolist.Transaction_Number Then

            '    '                Else
            '    '                    objSARMemoClass.listTransaction.Remove(itemstrmemolist)
            '    '                    CountListTrans2 = CountListTrans2 + 1
            '    '                    CountTrans = CountTrans + 1
            '    '                End If
            '    '                If CountListTrans2 > 0 Then
            '    '                    Exit For
            '    '                Else

            '    '                End If
            '    '            Next
            '    '        Else
            '    '            ListTransaction.Remove(itemlist)
            '    '        End If


            '    '    Next
            '    'End If
            '    'Using objdb As New CasemanagementEntities
            '    '    Dim listTransactionTemp2 As List(Of OneFCC_CaseManagement_SARMemo_Transaction) = objdb.OneFCC_CaseManagement_SARMemo_Transaction.Where(Function(x) x.FK_OneFCC_CaseManagement_SARMemo_ID = objSARMemoClass.objSARMemo.PK_OneFCC_CaseManagement_SARMemoID).ToList
            '    '    objSARMemoClass.listTransaction = listTransactionTemp2
            '    'End Using

            '    'bindTransactionDataTable(StoreTransaction, tbl_goAML_odm_Transaksi)
            '    bindTransactionDataTable(StoreTransaksiNoChecked, tbl_goAML_odm_Transaksi)
            'End If
            With objSARMemoClass.ObjSARMemo
                displayPK.Value = IDUnik
                displayPK.Text = IDUnik
                TanggalLaporan.Value = .Tanggal_Laporan
                NoRefPPATK.Value = .NoRefPPATK
                sar_jenisLaporan.Value = .Jenis_Laporan
                alasan.Value = .Alasan
                If .IsCustomer.Value Then
                    txtCIF.Hidden = False
                    txtWIC.Hidden = True
                    IsCustomer.Value = "Yes"
                    Dim objCust As New goAML_Ref_Customer
                    Dim KategoriCust As String = ""
                    Dim CustName As String = ""
                    Dim CIF As String = .CIF.ToString
                    Using objdb As New NawaDatadevEntities
                        objCust = objdb.goAML_Ref_Customer.Where(Function(x) x.CIF = CIF).FirstOrDefault
                        If objCust IsNot Nothing Then
                            If objCust.FK_Customer_Type_ID = 2 Then
                                CustName = objCust.Corp_Name
                            Else
                                CustName = objCust.INDV_Last_Name
                            End If
                            KategoriCust = "Nasabah"
                        Else
                            KategoriCust = "WIC"
                        End If

                    End Using
                    Dim CIFandName As String = CIF & " - " & CustName
                    txtCIF.Value = CIFandName
                    Session("SARMemo_CIFNO") = .CIF
                    txtUnitBisnis.Hidden = False
                    txtDirektor.Hidden = False
                    txtCabang.Hidden = False
                    FieldSet1.Hidden = False
                    txtLamaMenjadiNasabah.Hidden = False
                    txtUnitBisnis.Value = .Unit_Bisnis_Directorat
                    txtDirektor.Value = .Direktorat
                    txtCabang.Value = .Cabang
                    txtLamaMenjadiNasabah.Value = .Lama_menjadi_Nasabah
                Else
                    txtUnitBisnis.Hidden = True
                    txtDirektor.Hidden = True
                    txtCabang.Hidden = True
                    FieldSet1.Hidden = True
                    txtLamaMenjadiNasabah.Hidden = True
                    IsCustomer.Value = "No"
                    txtCIF.Hidden = True
                    txtWIC.Hidden = False
                    Dim objWIC As New goAML_Ref_WIC
                    Dim KategoriWIC As String = ""
                    Dim WICName As String = ""
                    Dim WIC As String = .WIC.ToString
                    Using objdb As New NawaDatadevEntities
                        objWIC = objdb.goAML_Ref_WIC.Where(Function(x) x.WIC_No = WIC).FirstOrDefault
                        If objWIC IsNot Nothing Then
                            If objWIC.FK_Customer_Type_ID = 2 Then
                                WICName = objWIC.Corp_Name
                            Else
                                WICName = objWIC.INDV_Last_Name
                            End If
                            KategoriWIC = "Nasabah"
                        Else
                            KategoriWIC = "WIC"
                        End If

                    End Using
                    Dim WICandName As String = WIC & " - " & WICName
                    txtWIC.Value = WICandName
                    Session("SARMemo_WICNO") = .WIC
                End If


                txtNo.Value = .No
                txtKepada.Value = .Kepada
                txtDari.Value = .Dari
                txtTanggal.Value = .Tanggal
                txtPerihal.Value = .Perihal
                txtSumberPelaporan.Value = .Sumber_Pelaporan
                txtNamaPihakPelapor.Value = .Nama_Pihak_Terlapor
                txtKategoriPihakPelapor.Value = .Kateri_Pihak_Terlapor

                If .Detail_Kesimpulan IsNot Nothing Then
                    If Not String.IsNullOrEmpty(.Detail_Kesimpulan) Then
                        ''Edit 5-Oct-2022 Felix
                        'Using objdb As New CasemanagementEntities
                        '    Dim SARMemo As New OneFCC_CaseManagement_Kesimpulan
                        '    SARMemo = objdb.OneFCC_CaseManagement_Kesimpulan.Where(Function(x) x.PK_OneFCC_CaseManagement_Kesimpulan_ID = .Detail_Kesimpulan).FirstOrDefault
                        '    cmb_KesimpulanDetail.SetTextWithTextValue(SARMemo.PK_OneFCC_CaseManagement_Kesimpulan_ID, SARMemo.KesimpulanDescription)
                        'End Using

                        Dim RowKesimpulan As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_OneFCC_CaseManagement_Kesimpulan_ID, KesimpulanDescription FROM OneFCC_CaseManagement_Kesimpulan where PK_OneFCC_CaseManagement_Kesimpulan_ID = " & .Detail_Kesimpulan & "", Nothing)
                        If RowKesimpulan IsNot Nothing Then
                            cmb_KesimpulanDetail.SetTextWithTextValue(RowKesimpulan("PK_OneFCC_CaseManagement_Kesimpulan_ID"), RowKesimpulan("KesimpulanDescription"))
                        End If
                        '' End 5 Oct 2022
                    End If
                End If


                txtPekerjaanBidangUsaha.Value = .Pekerjaan_Bidang_Usaha
                txtPengahasilanTahun.Value = .Penghasilan_Tahun
                txtProfilLainnya.Value = .Profil_Lainnya
                txtHasilAnalisis.Value = .Hasil_Analisis
                txtKesimpulan.Value = .Kesimpulan

            End With


        Catch ex As Exception

        End Try
    End Sub

    Private Sub isDataValid()
        Try
            If String.IsNullOrEmpty(txtNo.Value) Then
                Throw New ApplicationException(txtNo.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtKepada.Value) Then
                Throw New ApplicationException(txtKepada.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtDari.Value) Then
                Throw New ApplicationException(txtDari.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtTanggal.Value) Then
                Throw New ApplicationException(txtTanggal.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtPerihal.Value) Then
                Throw New ApplicationException(txtPerihal.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtSumberPelaporan.Value) Then
                Throw New ApplicationException(txtSumberPelaporan.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtNamaPihakPelapor.Value) Then
                Throw New ApplicationException(txtNamaPihakPelapor.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtKategoriPihakPelapor.Value) Then
                Throw New ApplicationException(txtKategoriPihakPelapor.FieldLabel + " is required.")
            End If

            If String.IsNullOrEmpty(txtPekerjaanBidangUsaha.Value) Then
                Throw New ApplicationException(txtPekerjaanBidangUsaha.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtPengahasilanTahun.Value) Then
                Throw New ApplicationException(txtPengahasilanTahun.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtProfilLainnya.Value) Then
                Throw New ApplicationException(txtProfilLainnya.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtHasilAnalisis.Value) Then
                Throw New ApplicationException(txtHasilAnalisis.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtKesimpulan.Value) Then
                Throw New ApplicationException(txtKesimpulan.FieldLabel + " is required.")
            End If

        Catch ex As Exception

        End Try
    End Sub


    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SARMemo_Detail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    'Private Sub SARMemo_Detail_Init(sender As Object, e As EventArgs) Handles Me.Init
    '    objFormModuleAdd = New NawaBLL.FormModuleAdd(fpMain, Panelconfirmation, LblConfirmation)
    'End Sub

#Region "Indikator"

    Public Property IDIndikator() As Long
        Get
            Return Session("SARMemo_Detail.IDIndikator")
        End Get
        Set(ByVal value As Long)
            Session("SARMemo_Detail.IDIndikator") = value
        End Set
    End Property

    Protected Sub Clean_Window_Indikator()
        'Clean fields
        cmb_indikator.SetTextValue("")
        'Show Buttons
        btn_indikator_Save.Hidden = False
    End Sub

    Public Property obj_Indikator_Edit() As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator
        Get
            Return Session("SARMemo_Detail.obj_Indikator_Edit")
        End Get
        Set(ByVal value As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator)
            Session("SARMemo_Detail.obj_Indikator_Edit") = value
        End Set
    End Property

    Public Property obj_ListIndikator_Edit() As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator)
        Get
            Return Session("SARMemo_Detail.obj_ListIndikator_Edit")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator))
            Session("SARMemo_Detail.obj_ListIndikator_Edit") = value
        End Set
    End Property


    Protected Sub gc_indikator(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListIndikator_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_IndikatorID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListIndikator_Edit.Remove(objToDelete)
                    End If
                    Bind_Indikator()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Indikator_Edit = obj_ListIndikator_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_IndikatorID = strID)
                    Load_Window_Indikator(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Indikator_Cancel_Click()
        Try
            'Hide window pop up
            Window_Indikator.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Indikator_Save_Click()
        Try
            'Validate input

            If String.IsNullOrEmpty(cmb_indikator.SelectedItemValue) Then
                Throw New ApplicationException(cmb_indikator.Label & " harus diisi.")
            End If


            'Action save here
            Dim intPK As Long = -1

            If obj_Indikator_Edit Is Nothing Then  'Add
                Dim objAdd As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator
                If obj_ListIndikator_Edit IsNot Nothing Then
                    If obj_ListIndikator_Edit.Count > 0 Then
                        intPK = obj_ListIndikator_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_IndikatorID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDIndikator = intPK

                With objAdd
                    .PK_OneFCC_CaseManagement_SARMemo_IndikatorID = intPK

                    If Not String.IsNullOrEmpty(cmb_indikator.SelectedItemValue) Then
                        .FK_Indicator = cmb_indikator.SelectedItemValue
                    Else
                        .FK_Indicator = Nothing
                    End If


                End With
                If obj_ListIndikator_Edit Is Nothing Then
                    obj_ListIndikator_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator)
                End If
                obj_ListIndikator_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListIndikator_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_IndikatorID = obj_Indikator_Edit.PK_OneFCC_CaseManagement_SARMemo_IndikatorID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDIndikator = .PK_OneFCC_CaseManagement_SARMemo_IndikatorID

                        If Not String.IsNullOrEmpty(cmb_indikator.SelectedItemValue) Then
                            .FK_Indicator = cmb_indikator.SelectedItemValue
                        Else
                            .FK_Indicator = Nothing
                        End If

                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_Indikator()

            'Hide window popup
            Window_Indikator.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_Indikator()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListIndikator_Edit)

        objtable.Columns.Add(New DataColumn("Kode", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        For Each row As DataRow In objtable.Rows
            row("Kode") = row("FK_Indicator")
            If Not IsDBNull(row("Kode")) Then
                Dim StrQueryCode As String = "select Keterangan from goAML_Ref_Indikator_Laporan where Kode = '" & row("Kode").ToString & "'"
                Dim Keterangan As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                row("Keterangan") = Keterangan
            Else
                row("Keterangan") = Nothing
            End If
        Next
        gp_indikator.GetStore().DataSource = objtable
        gp_indikator.GetStore().DataBind()


    End Sub

    Protected Sub Load_Window_Indikator(strAction As String)
        'Clean window pop up
        Clean_Window_Indikator()

        If obj_Indikator_Edit IsNot Nothing Then
            'Populate fields
            With obj_Indikator_Edit
                If .FK_Indicator IsNot Nothing Then
                    Using objdb As New NawaDatadevEntities
                        Dim objKode As goAML_Ref_Indikator_Laporan = objdb.goAML_Ref_Indikator_Laporan.Where(Function(X) X.Kode = .FK_Indicator).FirstOrDefault
                        If Not objKode Is Nothing Then
                            cmb_indikator.SetTextWithTextValue(obj_Indikator_Edit.FK_Indicator, objKode.Keterangan)
                        End If
                    End Using

                End If

            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            cmb_indikator.IsReadOnly = False
            btn_indikator_Save.Hidden = False '' Added on 12 Aug 2021
        Else
            cmb_indikator.IsReadOnly = True

            btn_indikator_Save.Hidden = True '' Added on 12 Aug 2021
        End If
        cmb_indikator.StringFieldStyle = "background-color:#FFE4C4"
        'Bind Indikator
        IDIndikator = obj_Indikator_Edit.PK_OneFCC_CaseManagement_SARMemo_IndikatorID

        'Show window pop up
        Window_Indikator.Title = "Indikator - " & strAction
        Window_Indikator.Hidden = False
    End Sub


#End Region

#Region "Pihak Terkait"

    Public Property IDPihakTerkait() As Long
        Get
            Return Session("SARMemo_Detail.IDPihakTerkait")
        End Get
        Set(ByVal value As Long)
            Session("SARMemo_Detail.IDPihakTerkait") = value
        End Set
    End Property

    Protected Sub Clean_Window_PihakTerkait()
        'Clean fields
        txtPT_CIF.Value = Nothing
        txtPT_HubunganPihakTerkait.Value = Nothing
        txtPT_RefGrips.Value = Nothing
        txtPT_TelahDilaporkan.Value = Nothing

        cmbPT_CIF.SetTextValue("")
        'Show Buttons
        btn_PihakTerkait_Save.Hidden = False

    End Sub


    Public Property obj_PihakTerkait_Edit() As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait
        Get
            Return Session("SARMemo_Detail.obj_PihakTerkait_Edit")
        End Get
        Set(ByVal value As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait)
            Session("SARMemo_Detail.obj_PihakTerkait_Edit") = value
        End Set
    End Property

    Public Property obj_ListPihakTerkait_Edit() As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait)
        Get
            Return Session("SARMemo_Detail.obj_ListPihakTerkait_Edit")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait))
            Session("SARMemo_Detail.obj_ListPihakTerkait_Edit") = value
        End Set
    End Property

    Protected Sub btn_Pihak_Terkait_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            obj_PihakTerkait_Edit = Nothing
            Clean_Window_PihakTerkait()

            'Show window pop up
            txtPT_CIF.ReadOnly = False
            cmbPT_CIF.IsReadOnly = False
            txtPT_HubunganPihakTerkait.ReadOnly = False
            txtPT_RefGrips.ReadOnly = False
            txtPT_TelahDilaporkan.ReadOnly = False
            cmbPT_CIF.StringFieldStyle = "background-color:#FFE4C4"
            btn_PihakTerkait_Save.Hidden = False '' Added on 12 Aug 2021
            Window_PihakTerkair.Title = "Pihak Terkait - Add"
            Window_PihakTerkair.Hidden = False
            txtPT_CIF.Value = Session("SARMemo_CIFNO")
            'Set IDIndikator to 0
            IDPihakTerkait = 0


        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_PihakTerkait(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListPihakTerkait_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListPihakTerkait_Edit.Remove(objToDelete)
                    End If
                    Bind_Pihak_Terkait()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_PihakTerkait_Edit = obj_ListPihakTerkait_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID = strID)
                    Load_Window_PihakTerkait(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_PihakTerkait_Cancel_Click()
        Try
            'Hide window pop up
            Window_PihakTerkair.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_PihakTerkait_Save_Click()
        Try
            'Validate input


            If String.IsNullOrWhiteSpace(cmbPT_CIF.SelectedItemValue) Then
                Throw New ApplicationException(cmbPT_CIF.Label & " is required.")
            End If
            If String.IsNullOrEmpty(txtPT_HubunganPihakTerkait.Value) Then
                Throw New ApplicationException(txtPT_HubunganPihakTerkait.FieldLabel & " harus diisi.")
            End If
            'If String.IsNullOrEmpty(txtPT_RefGrips.Value) Then
            '    Throw New ApplicationException(txtPT_RefGrips.FieldLabel & " harus diisi.")
            'End If
            'If String.IsNullOrEmpty(txtPT_TelahDilaporkan.Value) Then
            '    Throw New ApplicationException(txtPT_TelahDilaporkan.FieldLabel & " harus diisi.")
            'End If

            ''set account balance



            'Action save here
            Dim intPK As Long = -1

            If obj_PihakTerkait_Edit Is Nothing Then  'Add
                Dim objAdd As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait
                If obj_ListPihakTerkait_Edit IsNot Nothing Then
                    If obj_ListPihakTerkait_Edit.Count > 0 Then
                        intPK = obj_ListPihakTerkait_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDPihakTerkait = intPK

                With objAdd
                    .PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID = intPK

                    If Not String.IsNullOrEmpty(cmbPT_CIF.SelectedItemValue) Then
                        .CIF = cmbPT_CIF.SelectedItemValue
                        Dim Name As String = ""
                        Dim StrQueryCode As String = "select case when FK_Customer_Type_ID = 1 then indv_last_name else corp_name end as indv_last_name from goaml_Ref_customer where cif = '" & cmbPT_CIF.SelectedItemValue & "'"
                        Name = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                        .Nama_PihakTerkait = Name
                    Else
                        .CIF = Nothing
                    End If

                    If Not String.IsNullOrEmpty(txtPT_HubunganPihakTerkait.Value) Then
                        .Hubungan_PihakTerkait = txtPT_HubunganPihakTerkait.Value
                    Else
                        .Hubungan_PihakTerkait = Nothing
                    End If

                    If Not String.IsNullOrEmpty(txtPT_RefGrips.Value) Then
                        .RefGrips_PihakTerkait = txtPT_RefGrips.Value
                    Else
                        .RefGrips_PihakTerkait = Nothing
                    End If

                    If Not String.IsNullOrEmpty(txtPT_TelahDilaporkan.Value) Then
                        .Dilaporkan_PihakTerkait = txtPT_TelahDilaporkan.Value
                    Else
                        .Dilaporkan_PihakTerkait = Nothing
                    End If

                End With
                If obj_ListPihakTerkait_Edit Is Nothing Then
                    obj_ListPihakTerkait_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait)
                End If
                obj_ListPihakTerkait_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListPihakTerkait_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID = obj_PihakTerkait_Edit.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDPihakTerkait = .PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID

                        If Not String.IsNullOrEmpty(cmbPT_CIF.SelectedItemValue) Then
                            .CIF = cmbPT_CIF.SelectedItemValue
                            Dim Name As String = ""
                            Dim StrQueryCode As String = "select case when FK_Customer_Type_ID = 1 then indv_last_name else corp_name end as indv_last_name from goaml_Ref_customer where cif = '" & cmbPT_CIF.SelectedItemValue & "'"
                            Name = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                            .Nama_PihakTerkait = Name
                        Else
                            .CIF = Nothing
                        End If



                        If Not String.IsNullOrEmpty(txtPT_HubunganPihakTerkait.Value) Then
                            .Hubungan_PihakTerkait = txtPT_HubunganPihakTerkait.Value
                        Else
                            .Hubungan_PihakTerkait = Nothing
                        End If

                        If Not String.IsNullOrEmpty(txtPT_RefGrips.Value) Then
                            .RefGrips_PihakTerkait = txtPT_RefGrips.Value
                        Else
                            .RefGrips_PihakTerkait = Nothing
                        End If

                        If Not String.IsNullOrEmpty(txtPT_TelahDilaporkan.Value) Then
                            .Dilaporkan_PihakTerkait = txtPT_TelahDilaporkan.Value
                        Else
                            .Dilaporkan_PihakTerkait = Nothing
                        End If

                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_Pihak_Terkait()

            'Hide window popup
            Window_PihakTerkair.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_Pihak_Terkait()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListPihakTerkait_Edit)
        For Each row As DataRow In objtable.Rows
            If Not IsDBNull(row("CIF")) Then
                Dim CIF As String = ""
                Dim StrQueryCode As String = "select case when FK_Customer_Type_ID = 1 then indv_last_name else corp_name end as indv_last_name from goaml_Ref_customer where cif = '" & row("CIF").ToString & "'"
                CIF = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                row("Nama_PihakTerkait") = CIF
            Else
                row("Nama_PihakTerkait") = Nothing
            End If
        Next

        gp_Pihak_Terkait.GetStore().DataSource = objtable
        gp_Pihak_Terkait.GetStore().DataBind()


    End Sub

    Protected Sub Load_Window_PihakTerkait(strAction As String)
        'Clean window pop up
        Clean_Window_PihakTerkait()

        If obj_PihakTerkait_Edit IsNot Nothing Then
            'Populate fields
            With obj_PihakTerkait_Edit
                If Not String.IsNullOrEmpty(.CIF) Then
                    Dim strfieldtypestring As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT Keterangan FROM vw_goAML_Ref_Customer WHERE kode='" & .CIF & "'", Nothing)
                    cmbPT_CIF.SetTextWithTextValue(.CIF, strfieldtypestring)
                Else
                    cmbPT_CIF.SetTextValue("")
                End If

                If Not String.IsNullOrEmpty(.Hubungan_PihakTerkait) Then
                    txtPT_HubunganPihakTerkait.Value = .Hubungan_PihakTerkait
                Else
                    txtPT_HubunganPihakTerkait.Value = Nothing
                End If

                If Not String.IsNullOrEmpty(.RefGrips_PihakTerkait) Then
                    txtPT_RefGrips.Value = .RefGrips_PihakTerkait
                Else
                    txtPT_RefGrips.Value = Nothing
                End If

                If Not String.IsNullOrEmpty(.Dilaporkan_PihakTerkait) Then
                    txtPT_TelahDilaporkan.Value = .Dilaporkan_PihakTerkait
                Else
                    txtPT_TelahDilaporkan.Value = Nothing
                End If

            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            txtPT_CIF.ReadOnly = False
            cmbPT_CIF.IsReadOnly = False
            cmbPT_CIF.StringFieldStyle = "background-color:#FFE4C4"
            txtPT_HubunganPihakTerkait.ReadOnly = False
            txtPT_RefGrips.ReadOnly = False
            txtPT_TelahDilaporkan.ReadOnly = False
            btn_PihakTerkait_Save.Hidden = False '' Added on 12 Aug 2021
        Else
            cmbPT_CIF.IsReadOnly = True
            txtPT_CIF.ReadOnly = True
            txtPT_HubunganPihakTerkait.ReadOnly = True
            txtPT_RefGrips.ReadOnly = True
            txtPT_TelahDilaporkan.ReadOnly = True
            btn_PihakTerkait_Save.Hidden = True '' Added on 12 Aug 2021
        End If

        'Bind Indikator
        IDPihakTerkait = obj_PihakTerkait_Edit.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID

        'Show window pop up
        Window_PihakTerkair.Title = "Pihak Terkait - " & strAction
        Window_PihakTerkair.Hidden = False
    End Sub
#End Region

#Region "Tindak Lanjut"

    Public Property IDTindakLanjut() As Long
        Get
            Return Session("SARMemo_Detail.IDTindakLanjut")
        End Get
        Set(ByVal value As Long)
            Session("SARMemo_Detail.IDTindakLanjut") = value
        End Set
    End Property

    Protected Sub Clean_Window_TindakLanjut()
        'Clean fields
        cmb_tindaklanjut.SetTextValue("")
        'Show Buttons
        btn_TindakLanjut_Save.Hidden = False
    End Sub

    Public Property obj_TindakLanjut_Edit() As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut
        Get
            Return Session("SARMemo_Detail.obj_TindakLanjut_Edit")
        End Get
        Set(ByVal value As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut)
            Session("SARMemo_Detail.obj_TindakLanjut_Edit") = value
        End Set
    End Property

    Public Property obj_ListTindakLanjut_Edit() As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut)
        Get
            Return Session("SARMemo_Detail.obj_ListTindakLanjut_Edit")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut))
            Session("SARMemo_Detail.obj_ListTindakLanjut_Edit") = value
        End Set
    End Property



    Protected Sub gc_TindakLanjut(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListTindakLanjut_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListTindakLanjut_Edit.Remove(objToDelete)
                    End If
                    Bind_TindakLanjut()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_TindakLanjut_Edit = obj_ListTindakLanjut_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID = strID)
                    Load_Window_TindakLanjut(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_TindakLanjut_Cancel_Click()
        Try
            'Hide window pop up
            window_tindaklanjut.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_TindakLanjut_Save_Click()
        Try
            'Validate input

            If String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemValue) Then
                Throw New ApplicationException(cmb_tindaklanjut.Label & " harus diisi.")
            End If



            'Action save here
            Dim intPK As Long = -1

            If obj_TindakLanjut_Edit Is Nothing Then  'Add
                Dim objAdd As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut
                If obj_ListTindakLanjut_Edit IsNot Nothing Then
                    If obj_ListTindakLanjut_Edit.Count > 0 Then
                        intPK = obj_ListTindakLanjut_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDTindakLanjut = intPK

                With objAdd
                    .PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID = intPK

                    If Not String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemText) Then
                        .TindakLanjutDescription = cmb_tindaklanjut.SelectedItemText
                    Else
                        .TindakLanjutDescription = Nothing
                    End If

                End With
                If obj_ListTindakLanjut_Edit Is Nothing Then
                    obj_ListTindakLanjut_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut)
                End If
                obj_ListTindakLanjut_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListTindakLanjut_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID = obj_TindakLanjut_Edit.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDTindakLanjut = .PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID

                        If Not String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemValue) Then
                            .TindakLanjutDescription = cmb_tindaklanjut.SelectedItemValue
                        Else
                            .TindakLanjutDescription = Nothing
                        End If

                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_TindakLanjut()

            'Hide window popup
            window_tindaklanjut.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_TindakLanjut()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListTindakLanjut_Edit)


        gp_TIndakLanjut.GetStore().DataSource = objtable
        gp_TIndakLanjut.GetStore().DataBind()


    End Sub

    Protected Sub Load_Window_TindakLanjut(strAction As String)
        'Clean window pop up
        Clean_Window_TindakLanjut()

        If obj_TindakLanjut_Edit IsNot Nothing Then
            'Populate fields
            With obj_TindakLanjut_Edit
                If .FK_OneFCC_CaseManagement_TindakLanjut_ID IsNot Nothing Then

                    cmb_tindaklanjut.SetTextWithTextValue(.FK_OneFCC_CaseManagement_TindakLanjut_ID, .TindakLanjutDescription)


                End If

            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            cmb_tindaklanjut.IsReadOnly = False
            btn_TindakLanjut_Save.Hidden = False '' Added on 12 Aug 2021
        Else
            cmb_tindaklanjut.IsReadOnly = True

            btn_TindakLanjut_Save.Hidden = True '' Added on 12 Aug 2021
        End If
        cmb_tindaklanjut.StringFieldStyle = "background-color:#FFE4C4"
        'Bind Indikator
        IDTindakLanjut = obj_TindakLanjut_Edit.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID

        'Show window pop up
        window_tindaklanjut.Title = "Tindak Lanjut - " & strAction
        window_tindaklanjut.Hidden = False
    End Sub
#End Region

#Region "Activity"

    Public Property ListActivity As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
        Get
            If Session("SARMemo_Add.ListActivity") Is Nothing Then
                Session("SARMemo_Add.ListActivity") = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
            End If
            Return Session("SARMemo_Add.ListActivity")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity))
            Session("SARMemo_Add.ListActivity") = value
        End Set
    End Property

    Public Property obj_ListActivity_Edit() As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
        Get
            Return Session("SARMemo_Add.obj_ListActivity_Edit")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity))
            Session("SARMemo_Add.obj_ListActivity_Edit") = value
        End Set
    End Property


    Public Property obj_Activity_Edit() As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity
        Get
            Return Session("SARMemo_Add.obj_Activity_Edit")
        End Get
        Set(ByVal value As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
            Session("SARMemo_Add.obj_Activity_Edit") = value
        End Set
    End Property
    Public Property IDDokumen As String
        Get
            Return Session("SARMemo_Add.IDDokumen")
        End Get
        Set(value As String)
            Session("SARMemo_Add.IDDokumen") = value
        End Set
    End Property
    Public Property ListSelectedActivity As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
        Get
            If Session("SARMemo_Add.ListSelectedActivity") Is Nothing Then
                Session("SARMemo_Add.ListSelectedActivity") = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
            End If
            Return Session("SARMemo_Add.ListSelectedActivity")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity))
            Session("SARMemo_Add.ListSelectedActivity") = value
        End Set
    End Property

    Sub bindActivityDataTable(store As Ext.Net.Store, dt As DataTable)
        'store.DataSource = New DataTable
        'Dim objtable As New DataTable()
        'Dim fields() As FieldInfo = GetType(SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity).GetFields()
        'For Each field As FieldInfo In fields
        '    objtable.Columns.Add(field.Name, field.FieldType)
        'Next
        'For Each item As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity In ListActivity
        '    Dim row As DataRow = objtable.NewRow()
        '    For Each field As FieldInfo In fields
        '        row(field.Name) = field.GetValue(item)
        '    Next
        '    objtable.Rows.Add(row)
        'Next
        Dim objtable As New Data.DataTable
        objtable = NawaBLL.Common.CopyGenericToDataTable(ListActivity)
        'Dim objtable As Data.DataTable = dt

        objtable.Columns.Add(New DataColumn("PK_Sementara", GetType(String)))


        'store.DataSource = objtable
        'store.DataBind()
        gp_activity.GetStore.DataSource = objtable
        gp_activity.GetStore.DataBind()

    End Sub

    Protected Sub Bind_Activity()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListActivity_Edit)


        gp_activity.GetStore.DataSource = objtable
        gp_activity.GetStore.DataBind()


    End Sub

    Protected Sub Clean_Window_Activity()
        'Clean fields
        txtComment.Value = Nothing
        txtSignificance.Value = Nothing
        txtDescription.Value = Nothing
        'Show Buttons
        btn_TindakLanjut_Save.Hidden = False
    End Sub


    Protected Sub Load_Window_Activity(strAction As String)
        'Clean window pop up
        Clean_Window_Activity()

        If obj_Activity_Edit IsNot Nothing Then
            'Populate fields
            With obj_Activity_Edit
                If .Reason IsNot Nothing Then
                    txtDescription.Value = .Reason
                Else
                    txtDescription.Value = Nothing
                End If
                If .Significance IsNot Nothing Then
                    txtSignificance.Value = .Significance
                Else
                    txtSignificance.Value = 0
                End If
                If .Comments IsNot Nothing Then
                    txtComment.Value = .Comments
                Else
                    txtComment.Value = Nothing
                End If

            End With
        End If

        If strAction = "Edit" Then
            txtDescription.ReadOnly = False
            txtSignificance.ReadOnly = False
            txtComment.ReadOnly = False
        Else
            txtDescription.ReadOnly = True
            txtSignificance.ReadOnly = True
            txtComment.ReadOnly = True
        End If
        'Show window pop up
        Window_Activity.Title = "Activity SAR - " & strAction
        Window_Activity.Hidden = False
    End Sub

    Protected Sub gc_Activity(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListActivity_Edit.Find(Function(x) x.Pk_OneFCC_CaseManagement_SARMemo_ActivityID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListActivity_Edit.Remove(objToDelete)
                    End If
                    Bind_Activity()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Activity_Edit = obj_ListActivity_Edit.Find(Function(x) x.Pk_OneFCC_CaseManagement_SARMemo_ActivityID = strID)
                    Load_Window_Activity(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btn_Activity_Cancel_Click()
        Try
            'Hide window pop up
            Window_Activity.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Private Shared Sub DataSetToExcel(ByVal dataSet As DataSet, ByVal objfileinfo As FileInfo)
        Using pck As ExcelPackage = New ExcelPackage()

            For Each dataTable As DataTable In dataSet.Tables
                Dim workSheet As ExcelWorksheet = pck.Workbook.Worksheets.Add(dataTable.TableName)
                workSheet.Cells("A1").LoadFromDataTable(dataTable, True)

                Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                Dim intcolnumber As Integer = 1
                For Each item As System.Data.DataColumn In dataTable.Columns
                    If item.DataType = GetType(Date) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = dateformat
                    End If
                    If item.DataType = GetType(Integer) Or item.DataType = GetType(Double) Or item.DataType = GetType(Long) Or item.DataType = GetType(Decimal) Or item.DataType = GetType(Single) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = "#,##0.00"
                    End If

                    intcolnumber = intcolnumber + 1
                Next
                workSheet.Cells(workSheet.Dimension.Address).AutoFitColumns()
            Next

            pck.SaveAs(objfileinfo)
        End Using
    End Sub



    Protected Sub CreateExcel2007WithData(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try


            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            'Dim intTotalRow As Long
            Dim objSchemaModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(30185)
            Dim objSchemaModuleField As List(Of NawaDAL.ModuleField) = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(30185)
            Dim objSchemaModuleFieldDefault As New List(Of NawaDAL.ModuleFieldDefault)
            Dim objmdl As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(30185)

            Dim objList As New DataTable
            For Each item In objSARMemoClass.listCaseID
                Dim recordID = item.PK_OneFCC_CaseManagement_SARMemo_CaseIDID.ToString
                Dim tbl_activity_temp As New DataTable
                Dim StrQuery As String = "select * from OneFCC_CaseManagement_SARMemo_Activity where FK_OneFCC_CaseManagement_SARMemo_ID = " & objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID
                tbl_activity_temp = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQuery, Nothing)
                objList.Merge(tbl_activity_temp)

            Next




            objFormModuleView = New FormModuleView(gp_activity, BtnExport)
            objFormModuleView.ModuleID = objmdl.PK_Module_ID
            objFormModuleView.ModuleName = objmdl.ModuleName

            Dim delimitercheckboxparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(39)
            Dim strdelimiter As String = ";"
            If Not delimitercheckboxparam Is Nothing Then
                strdelimiter = delimitercheckboxparam.SettingValue
            End If

            Dim objtb As DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListActivity_Edit)
            Dim intmaxrow As Integer
            If objtb.Rows.Count + 1000 > 1048575 Then
                intmaxrow = 1048575
            Else
                intmaxrow = objtb.Rows.Count + 1000
            End If


            Using objtbl As Data.DataTable = objtb
                'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                objtbl.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                Dim intTotalPage As Integer = (intmaxrow + 1048575 - 1) / 1048575
                Dim dsTable As New Data.DataSet

                If intTotalPage = 1 Then
                    'objFormModuleView.changeHeader(objtbl)

                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In gp_activity.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    DataSetToExcel(dsTable, objfileinfo)

                    dsTable.Dispose()
                    dsTable = Nothing



                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If



                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")



                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()
                ElseIf intTotalPage > 1 Then
                    'objFormModuleView.changeHeader(objtbl)



                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In gp_activity.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    'ambil page 2 dst sampe page terakhir
                    For index = 1 To intTotalPage - 2
                        Using objtbltemp As Data.DataTable = objtb
                            objtbltemp.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                            dsTable.Tables.Add(objtbltemp.Copy)
                        End Using
                    Next
                    DataSetToExcel(dsTable, objfileinfo)
                    dsTable.Dispose()
                    dsTable = Nothing

                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If
                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()

                End If
            End Using

        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub

    Protected Sub sar_jenisLaporan_DirectSelect(sender As Object, e As DirectEventArgs)
        Try
            'If sar_jenisLaporan.SelectedItem.Value = "LTKMT" Or sar_jenisLaporan.SelectedItem.Value = "LAPT" Or sar_jenisLaporan.SelectedItem.Value = "LTKM" Then
            If sar_jenisLaporan.SelectedItem.Value IsNot Nothing Then
                'PanelReportIndicator.Hidden = False
            Else
                'PanelReportIndicator.Hidden = True
            End If
            If sar_jenisLaporan.SelectedItem.Value = "LTKMP" Then
                NoRefPPATK.Hidden = False
            Else
                NoRefPPATK.Hidden = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Property obj_Attachment_Edit() As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment
        Get
            Return Session("SARMemo_Add.obj_Attachment_Edit")
        End Get
        Set(ByVal value As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment)
            Session("SARMemo_Add.obj_Attachment_Edit") = value
        End Set
    End Property

    Public Property obj_ListAttachment_Edit() As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment)
        Get
            Return Session("SARMemo_Add.obj_ListAttachment_Edit")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment))
            Session("SARMemo_Add.obj_ListAttachment_Edit") = value
        End Set
    End Property

    Protected Sub btn_addAttachment_click(sender As Object, e As DirectEventArgs)
        Try
            obj_Attachment_Edit = Nothing
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()
            WindowAttachment.Hidden = False
            FileDoc.Hidden = False
            BtnsaveAttachment.Hidden = False
            txtKeterangan.Editable = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                obj_ListAttachment_Edit.Remove(obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = ID).FirstOrDefault)
                bindAttachment(StoreAttachment, obj_ListAttachment_Edit)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                editDokumen(ID, "Edit")
                IDDokumen = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editDokumen(ID, "Detail")
            ElseIf e.ExtraParams(1).Value = "Download" Then
                DownloadFile(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim status As Boolean = False
            Dim DokumenOld As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment
            Dim Dokumen As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment
            If IDDokumen IsNot Nothing Or IDDokumen <> "" Then
                status = True
                DokumenOld = obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = IDDokumen).FirstOrDefault
                obj_ListAttachment_Edit.Remove(obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = IDDokumen).FirstOrDefault)
            ElseIf Not FileDoc.HasFile Then
                Throw New Exception("Please Upload File")
            End If

            Dim docName As String = IO.Path.GetFileName(FileDoc.FileName)
            If obj_ListAttachment_Edit IsNot Nothing Then
                For Each item In obj_ListAttachment_Edit
                    If item.File_Name = docName Then
                        Throw New ApplicationException(docName + " Sudah Ada")
                    End If
                Next
            Else
                obj_ListAttachment_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment)
            End If


            If obj_ListAttachment_Edit.Count = 0 Then
                Dokumen.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = -1
            ElseIf obj_ListAttachment_Edit.Count >= 1 Then
                Dokumen.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = obj_ListAttachment_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID) - 1
            End If

            If status = True And Not FileDoc.HasFile Then
                Dokumen.File_Name = DokumenOld.File_Name
                Dokumen.File_Doc = DokumenOld.File_Doc
            Else
                Dokumen.File_Name = IO.Path.GetFileName(FileDoc.FileName)
                Dokumen.File_Doc = FileDoc.FileBytes
            End If
            Dokumen.Remarks = txtKeterangan.Text.Trim

            Dokumen.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            Dokumen.CreatedDate = DateTime.Now

            obj_ListAttachment_Edit.Add(Dokumen)

            bindAttachment(StoreAttachment, obj_ListAttachment_Edit)
            WindowAttachment.Hidden = True
            IDDokumen = Nothing

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAttachment.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub bindAttachment(store As Ext.Net.Store, listAttachment As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAttachment)
        objtable.Columns.Add(New DataColumn("FileName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("FileName") = item("File_Name")
                item("Keterangan") = item("Remarks")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub editDokumen(id As String, command As String)
        Try
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()

            WindowAttachment.Hidden = False
            Dim Dokumen As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment
            Dokumen = obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = id).FirstOrDefault

            txtFileName.Text = Dokumen.File_Name
            txtKeterangan.Text = Dokumen.Remarks

            If command = "Detail" Then
                FileDoc.Hidden = True
                BtnsaveAttachment.Hidden = True
                txtKeterangan.Editable = False
            Else
                FileDoc.Hidden = False
                BtnsaveAttachment.Hidden = False
                txtKeterangan.Editable = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DownloadFile(id As Long)
        Dim objdownload As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment = obj_ListAttachment_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = id)
        If Not objdownload Is Nothing Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & objdownload.File_Name)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            'Response.ContentType = "ContentType"
            Response.ContentType = MimeMapping.GetMimeMapping(objdownload.File_Name)
            Response.BinaryWrite(objdownload.File_Doc)
            Response.End()
        End If
    End Sub

    Protected Sub ReportIndicator_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goaml_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub JenisLaporan_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and TRNorACT = 'ACT'"
            Else
                strfilter += "TRNorACT = 'ACT'"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Jenis_Laporan_STR", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
#End Region
End Class


