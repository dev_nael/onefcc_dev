﻿Imports NawaDevDAL
Imports NawaDevBLL
Imports CasemanagementDAL
Imports CasemanagementBLL
Imports System.Data
Imports System.Data.SqlClient
Imports NawaBLL
Imports OfficeOpenXml
Imports System.IO
Imports System.Globalization
Imports System.Reflection

Partial Class AML_CaseManagement_SARMemo_SARMemo_Add
    Inherits ParentPage

    Public objFormModuleAdd As NawaBLL.FormModuleAdd
    Public objFormModuleView As FormModuleView
    Public Property ObjSARData As NawaDevBLL.GenerateSARData
        Get
            Return Session("SARMemo_Add.ObjSARData")
        End Get
        Set(ByVal value As NawaDevBLL.GenerateSARData)
            Session("SARMemo_Add.ObjSARData") = value
        End Set
    End Property
    Public Property objSARMemoClass As SARMemo_BLL.SARMemoClass
        Get
            If Session("SARMemo_Add.objSARMemoClass") Is Nothing Then
                Session("SARMemo_Add.objSARMemoClass") = New SARMemo_BLL.SARMemoClass
            End If

            Return Session("SARMemo_Add.objNewsClass")
        End Get
        Set(value As SARMemo_BLL.SARMemoClass)
            Session("SARMemo_Add.objNewsClass") = value
        End Set
    End Property
    Protected Sub IsCustomer_Changed(sender As Object, e As EventArgs) Handles IsCustomer.DirectCheck
        If IsCustomer.Value Then
            cmb_Cifno.IsHidden = False
            btnSearch.Hidden = False
            cmb_WICno.IsHidden = True
        Else
            cmb_Cifno.IsHidden = True
            cmb_WICno.IsHidden = False
            btnSearch.Hidden = False
        End If
    End Sub

    ' Set Posisi Action dari Grid
    Sub SetCommandColumnLocation()
        'dim settingvaluestring as String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select SettingValue FROM systemParameter where PK_SystemParameter_ID = '32'", Nothing)
        Dim ObjParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim ButtonPosition As Integer = 1
        If Not ObjParamSettingbutton Is Nothing Then
            ButtonPosition = ObjParamSettingbutton.SettingValue
        End If


        ColumnActionLocation(gp_indikator, cc_indikator, ButtonPosition)
        ColumnActionLocation(gp_Pihak_Terkait, cc_pihakterkait, ButtonPosition)
        ColumnActionLocation(gp_TIndakLanjut, cc_tindaklanjut, ButtonPosition)
        ColumnActionLocation(gp_activity, cc_Activity, ButtonPosition)
    End Sub

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, ButtonPosition As Integer)
        If ButtonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub

    Private Sub SARMemo_Add_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ' Clear Session
                ClearSession()
                ' Session sebagai parameter bahwa user pertama kali add data
                Session("FirstAddData") = 1
                ' Clear TextField dan Grid 
                ClearTextAndGrid()
                ClearTransaction()
                ' Set action grid
                SetCommandColumnLocation()
                ' Set untuk NDS Dropdown pilih cif dia filter berdasarkan PIC
                Dim UserCIF As String = NawaBLL.Common.SessionCurrentUser.UserID
                cmb_Cifno.StringFilter = " dbo.ufn_CheckIfStringExists('" & UserCIF & "',PIC)=1"
                cmb_WICno.StringFilter = " dbo.ufn_CheckIfStringExists('" & UserCIF & "',PIC)=1"
                ' Load Data 
                LoadData()

            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ClearSession()
        Session("FirstAddData") = Nothing
        objSARMemoClass = New SARMemo_BLL.SARMemoClass
        obj_ListAttachment_Edit = Nothing
        obj_ListActivity_Edit = Nothing
        obj_ListIndikator_Edit = Nothing
        obj_ListPihakTerkait_Edit = Nothing
        obj_ListTindakLanjut_Edit = Nothing
        Session("SARMemo_CIFNO") = Nothing ' Session untuk menampung CIFNo yang dipilih
        Session("SARMemo_WICNO") = Nothing
    End Sub

    Private Sub ClearTextAndGrid()
        txtNo.Value = Nothing
        txtKepada.Value = Nothing
        txtDari.Value = Nothing
        txtTanggal.Value = Nothing
        txtPerihal.Value = Nothing
        txtSumberPelaporan.Value = Nothing
        txtNamaPihakPelapor.Value = Nothing
        txtKategoriPihakPelapor.Value = Nothing
        txtUnitBisnis.Value = Nothing
        txtDirektor.Value = Nothing
        txtCabang.Value = Nothing
        txtLamaMenjadiNasabah.Value = Nothing
        txtPekerjaanBidangUsaha.Value = Nothing
        txtPengahasilanTahun.Value = Nothing
        txtProfilLainnya.Value = Nothing
        txtHasilAnalisis.Value = Nothing
        txtKesimpulan.Value = Nothing
        NoRefPPATK.Value = Nothing
        alasan.Value = Nothing
        'TxtActivity.Value = Nothing
        'txtCabang.Value = Nothing
        cmb_KesimpulanDetail.SetTextValue("")
        sar_jenisLaporan.Value = Nothing
        TanggalLaporan.Value = Nothing
        objSARMemoClass = New SARMemo_BLL.SARMemoClass
        obj_Indikator_Edit = New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator
        obj_PihakTerkait_Edit = New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait
        obj_TindakLanjut_Edit = New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut
        obj_Activity_Edit = New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity
        obj_ListAttachment_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment)
        obj_ListIndikator_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator)
        obj_ListActivity_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
        obj_ListPihakTerkait_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait)
        obj_ListTindakLanjut_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut)
        ListActivity = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)

        Dim Dt_NewDataTable As New DataTable
        'bindTransactionDataTable(StoreTransaction, Dt_NewDataTable)
        bindAttachment(StoreAttachment, obj_ListAttachment_Edit)

        gp_indikator.GetStore.DataSource = Dt_NewDataTable
        gp_indikator.GetStore.DataBind()
        gp_Pihak_Terkait.GetStore.DataSource = Dt_NewDataTable
        gp_Pihak_Terkait.GetStore.DataBind()
        gp_TIndakLanjut.GetStore.DataSource = Dt_NewDataTable
        gp_TIndakLanjut.GetStore.DataBind()

    End Sub

    ' Clear hasil dari pencarian transaction STR
    Private Sub ClearTransaction()
        NoRefPPATK.Value = Nothing
        alasan.Value = Nothing
        'dd_TrnFilterBy.SetTextValue("")
        'sar_DateFrom.Value = Nothing
        'sar_DateTo.Value = Nothing
        sar_jenisLaporan.Value = Nothing
        TanggalLaporan.Value = Nothing
        'obj_ListAttachment_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment)
        'ListTransaction = New List(Of goAML_ODM_Transaksi)
        'ListSelectedTransaction = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)

        'bindAttachment(StoreAttachment, obj_ListAttachment_Edit)

    End Sub

    ' Prosedur untuk search case alert berdasarkan CIF yang dipilih
    Protected Sub btnSearch_Click()
        Try
            ' Unhide panel SARMemo nya
            FpSARMemo.Hidden = True
            ' Clear Text and Grid nya
            ClearTextAndGrid()
            ' Jika user tidak memilih CIF/WIC
            If IsCustomer.Value Then
                If Session("SARMemo_CIFNO") Is Nothing Then
                    Throw New ApplicationException("Pilih Salah Satu CIF")
                End If
                txtUnitBisnis.Hidden = False
                txtDirektor.Hidden = False
                txtCabang.Hidden = False
                FieldSet1.Hidden = False
                txtLamaMenjadiNasabah.Hidden = False
                ' Mencari status dari CIF yang dipilih
                Dim StrStatusCIF As String = "Select status from VW_SARMemo_CIFNo where cifno = '" & Session("SARMemo_CIFNO").ToString & "'"
                Dim Status As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrStatusCIF, Nothing)
                ' Jika statusnya 0 atau Uncofigured
                If Status = "0" Then
                    Throw New ApplicationException("CIF yang dipilih belum ada konfigurasi workflownya")
                End If
                ' Tampung value CIF nya
                Dim CIFSARMemo As String = Session("SARMemo_CIFNO")

                '' Edit 5-Oct-2022 Felix 
                'Using objdb As New CasemanagementEntities
                '    Dim StatusMemo As String = ""
                '    ' Cari dulu case id berdasarkan CIF yang dipilih dan dimana statusnya 3 atau issue/closed
                '    Dim ListCaseID As List(Of OneFCC_CaseManagement) = objdb.OneFCC_CaseManagement.Where(Function(x) x.CIF_No = CIFSARMemo And x.FK_CaseStatus_ID = 3).ToList
                '    ' Cari juga case id yang pernah di simpan di STR Memo
                '    Dim ListCaseIDSAR As List(Of OneFCC_CaseManagement_SARMemo_CaseID) = objdb.OneFCC_CaseManagement_SARMemo_CaseID.ToList()
                '    ' Looping case ID
                '    For Each item In ListCaseID
                '        For Each item2 In ListCaseIDSAR
                '            ' Jika case ID yang ditemukan di casemanagement ternyata sudah ada di case STR Memo
                '            If item.PK_CaseManagement_ID = item2.CaseID Then
                '                ' maka fk str memo dari case id itu isi dengan fk str memo
                '                item.FK_OneFCC_CaseManagement_SARMemo_ID = item2.FK_OneFCC_CaseManagement_SARMemo_ID
                '            End If
                '        Next
                '    Next
                '    Dim ObjTable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(ListCaseID)
                '    ' Tambah Column Rule
                '    ObjTable.Columns.Add(New DataColumn("Rule", GetType(String)))
                '    For Each item As Data.DataRow In ObjTable.Rows
                '        ' Jika rule basic nya ada, maka cari description rule nya
                '        If Not IsDBNull(item("FK_Rule_Basic_ID")) Then
                '            'Dim RuleBasic As New OneFCC_MS_Rule_Basic
                '            'Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                '            'RuleBasic = objdb.OneFCC_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault
                '            'item("Rule") = RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                '            '' Edit 5-Oct-2022
                '            Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                '            'RuleBasic = objdb.OneFcc_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault '' Edit 5 Oct 2022
                '            Dim RuleBasic As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select PK_Rule_Basic_ID + ' - ' + Rule_Basic_Name from OneFcc_MS_Rule_Basic where PK_Rule_Basic_ID = " & FKRule & "", Nothing)
                '            item("Rule") = RuleBasic ' RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                '            '' End 5-Oct-2022
                '        End If
                '        ' Jika case ID nya tidak ada di Case ID STR Memo maka case id nya baru
                '        If IsDBNull(item("FK_OneFCC_CaseManagement_SARMemo_ID")) Then
                '            item("Memo_No") = "0 - New"
                '        Else
                '            Dim SARMemo As New OneFCC_CaseManagement_SARMemo
                '            Dim FkSARMemo As String = item("FK_OneFCC_CaseManagement_SARMemo_ID").ToString
                '            SARMemo = objdb.OneFCC_CaseManagement_SARMemo.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_ID = FkSARMemo).FirstOrDefault
                '            If SARMemo.StatusID = "1" Then
                '                item("Memo_No") = "1 - On Progress"
                '            ElseIf SARMemo.StatusID = "2" Then
                '                item("Memo_No") = "2 - Approved"
                '            ElseIf SARMemo.StatusID = "3" Then
                '                item("Memo_No") = "3 - Rejected"
                '            ElseIf SARMemo.StatusID = "4" Then
                '                item("Memo_No") = "4 - Generate"
                '            ElseIf SARMemo.StatusID = "5" Then
                '                item("Memo_No") = "5 - On Progress Generate Report"
                '            End If
                '        End If
                '    Next
                '    gp_case_alert.GetStore().DataSource = ObjTable
                '    gp_case_alert.GetStore().DataBind()
                'End Using

                Dim StatusMemo As String = ""
                ' Cari dulu case id berdasarkan CIF yang dipilih dan dimana statusnya 3 atau issue/closed
                Dim listCaseID As List(Of OneFCC_CaseManagement) = New List(Of OneFCC_CaseManagement)
                'listCaseID = objdb.OneFCC_CaseManagement.Where(Function(x) x.CIF_No = CIFSARMemo And x.FK_CaseStatus_ID = 3).ToList
                Dim dtCaseID As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement csm inner join OneFCC_CaseManagement_Typology tp on csm.PK_CaseManagement_ID = tp.FK_CaseManagement_ID where csm.CIF_No = '" & CIFSARMemo & "' and csm.FK_CaseStatus_ID = 3 and csm.FK_OneFCC_CaseManagement_STRMemo_ID is null and tp.Is_Transaction = 0", Nothing)

                If dtCaseID IsNot Nothing Then
                    For Each row As DataRow In dtCaseID.Rows
                        Dim CaseID As OneFCC_CaseManagement = New OneFCC_CaseManagement()

                        If Not IsDBNull(row("PK_CaseManagement_ID")) Then
                            CaseID.PK_CaseManagement_ID = row("PK_CaseManagement_ID")
                        End If

                        If Not IsDBNull(row("Alert_Type")) Then
                            CaseID.Alert_Type = row("Alert_Type")
                        End If

                        If Not IsDBNull(row("FK_Rule_Basic_ID")) Then
                            CaseID.FK_Rule_Basic_ID = row("FK_Rule_Basic_ID")
                        End If

                        If Not IsDBNull(row("Case_Description")) Then
                            CaseID.Case_Description = row("Case_Description")
                        End If

                        If Not IsDBNull(row("Is_Customer")) Then
                            CaseID.Is_Customer = row("Is_Customer")
                        End If

                        If Not IsDBNull(row("CIF_No")) Then
                            CaseID.CIF_No = row("CIF_No")
                        End If

                        If Not IsDBNull(row("Account_No")) Then
                            CaseID.Account_No = row("Account_No")
                        End If

                        If Not IsDBNull(row("Customer_Name")) Then
                            CaseID.Customer_Name = row("Customer_Name")
                        End If

                        If Not IsDBNull(row("WIC_No")) Then
                            CaseID.WIC_No = row("WIC_No")
                        End If

                        If Not IsDBNull(row("WIC_Name")) Then
                            CaseID.WIC_Name = row("WIC_Name")
                        End If

                        If Not IsDBNull(row("FK_Proposed_Status_ID")) Then
                            CaseID.FK_Proposed_Status_ID = row("FK_Proposed_Status_ID")
                        End If

                        If Not IsDBNull(row("Proposed_By")) Then
                            CaseID.Proposed_By = row("Proposed_By")
                        End If

                        If Not IsDBNull(row("FK_CaseStatus_ID")) Then
                            CaseID.FK_CaseStatus_ID = row("FK_CaseStatus_ID")
                        End If

                        If Not IsDBNull(row("FK_CaseManagement_Workflow_ID")) Then
                            CaseID.FK_CaseManagement_Workflow_ID = row("FK_CaseManagement_Workflow_ID")
                        End If

                        If Not IsDBNull(row("Workflow_Step")) Then
                            CaseID.Workflow_Step = row("Workflow_Step")
                        End If

                        If Not IsDBNull(row("PIC")) Then
                            CaseID.PIC = row("PIC")
                        End If

                        If Not IsDBNull(row("Aging")) Then
                            CaseID.Aging = row("Aging")
                        End If

                        If Not IsDBNull(row("StatusRFI")) Then
                            CaseID.StatusRFI = row("StatusRFI")
                        End If

                        If Not IsDBNull(row("FK_CaseStatus_ID")) Then
                            CaseID.FK_CaseStatus_ID = row("FK_CaseStatus_ID")
                        End If

                        If Not IsDBNull(row("ProcessDate")) Then
                            CaseID.ProcessDate = row("ProcessDate")
                        End If

                        If Not IsDBNull(row("FK_Report_ID")) Then
                            CaseID.FK_Report_ID = row("FK_Report_ID")
                        End If

                        If Not IsDBNull(row("Active")) Then
                            CaseID.Active = row("Active")
                        End If

                        If Not IsDBNull(row("CreatedBy")) Then
                            CaseID.CreatedBy = row("CreatedBy")
                        End If

                        If Not IsDBNull(row("LastUpdateBy")) Then
                            CaseID.LastUpdateBy = row("LastUpdateBy")
                        End If

                        If Not IsDBNull(row("ApprovedBy")) Then
                            CaseID.ApprovedBy = row("ApprovedBy")
                        End If

                        If Not IsDBNull(row("CreatedDate")) Then
                            CaseID.CreatedDate = row("CreatedDate")
                        End If

                        If Not IsDBNull(row("LastUpdateDate")) Then
                            CaseID.LastUpdateDate = row("LastUpdateDate")
                        End If

                        If Not IsDBNull(row("ApprovedDate")) Then
                            CaseID.ApprovedDate = row("ApprovedDate")
                        End If

                        If Not IsDBNull(row("Alternateby")) Then
                            CaseID.Alternateby = row("Alternateby")
                        End If

                        'If Not IsDBNull(drCaseID("Rule")) Then
                        '    CaseID.Account_No = drCaseID("Rule")
                        'End If
                        'If Not IsDBNull(row("FK_OneFCC_CaseManagement_SARMemo_ID")) Then
                        '    CaseID.FK_OneFCC_CaseManagement_STRMemo_ID = row("FK_OneFCC_CaseManagement_SARMemo_ID")
                        'End If
                        If Not IsDBNull(row("Memo_No")) Then
                            CaseID.Memo_No = row("Memo_No")
                        End If

                        listCaseID.Add(CaseID)
                    Next
                End If


                ' Cari juga case id yang pernah di simpan di STR Memo
                Dim listCaseIDSAR As New DataTable
                listCaseIDSAR = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_SARMemo_CaseID")
                ' Looping case ID

                For Each item In listCaseID
                    For Each item2 As DataRow In listCaseIDSAR.Rows
                        ' Jika case ID yang ditemukan di casemanagement ternyata sudah ada di case STR Memo
                        If item.PK_CaseManagement_ID = item2("CaseID") Then
                            ' maka fk str memo dari case id itu isi dengan fk str memo
                            Dim FKSARMemo As String = ""
                            'FKSARMemo = item2("FK_OneFCC_CaseManagement_SARMemo_ID").ToString
                            'item.FK_OneFCC_CaseManagement_STRMemo_ID = FKSARMemo
                        End If
                    Next
                Next
                Dim ObjTable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listCaseID)
                ' Tambah Column Rule
                ObjTable.Columns.Add(New DataColumn("Rule", GetType(String)))
                ObjTable.Columns.Add(New DataColumn("UniqueCMID", GetType(String))) '' Add 7-Mar-23, Ari. FAMA tambah 1 field baru untuk unique cm id
                For Each item As Data.DataRow In ObjTable.Rows
                    If Not IsDBNull(item("FK_Rule_Basic_ID")) Then
                        '' Edit 5-Oct-2022 Felix
                        'Dim RuleBasic As New OneFCC_MS_Rule_Basic
                        'Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                        'RuleBasic = objdb.OneFCC_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault
                        'item("Rule") = RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                        'Dim RuleBasic As New OneFcc_MS_Rule_Basic
                        Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                        'RuleBasic = objdb.OneFcc_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault '' Edit 5 Oct 2022
                        Dim RuleBasic As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select cast (PK_Rule_Basic_ID as varchar(100)) + ' - ' + Rule_Basic_Name from OneFcc_MS_Rule_Basic where PK_Rule_Basic_ID = " & FKRule & "", Nothing)
                        item("Rule") = RuleBasic ' RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                        '' End 5-Oct-2022
                    End If
                    '' Add 7-Mar-23, Ari. FAMA tambah 1 field baru untuk unique cm id
                    If item("PK_CaseManagement_ID") > 0 Then
                        Dim PKCM As Long = Convert.ToInt64(item("PK_CaseManagement_ID"))
                        Dim UniqueCMDID As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select Unique_CM_ID from OneFCC_CaseManagement where PK_CaseManagement_ID = " & PKCM, Nothing).ToString
                        item("UniqueCMID") = UniqueCMDID
                    End If
                    Dim PKCasManagement As String
                    Dim FKSARMemo_FromTable As String = ""
                    If Not IsDBNull(item("PK_CaseManagement_ID")) Then
                        PKCasManagement = item("PK_CaseManagement_ID").ToString
                        FKSARMemo_FromTable = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select isnull(FK_OneFCC_CaseManagement_SARMemo_ID,'') from OneFCC_CaseManagement_SARMemo_CaseID where caseID = " & PKCasManagement, Nothing)
                    End If



                    ' Jika case ID nya tidak ada di Case ID STR Memo maka case id nya baru
                    ' Jika case ID nya tidak ada di Case ID STR Memo maka case id nya baru
                    If FKSARMemo_FromTable Is Nothing Or FKSARMemo_FromTable = "" Then
                        item("Memo_No") = "0 - New"
                    Else
                        Dim SARMemo As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo
                        Dim FkSARMemo As String = FKSARMemo_FromTable
                        'SARMemo = objdb.OneFCC_CaseManagement_SARMemo.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_ID = FkSARMemo).FirstOrDefault
                        Dim drSARMemo As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select StatusID from OneFCC_CaseManagement_SARMemo where PK_OneFCC_CaseManagement_SARMemo_ID = " & FkSARMemo, Nothing)
                        If drSARMemo IsNot Nothing Then
                            If drSARMemo("StatusID") = "1" Then
                                item("Memo_No") = "1 - On Progress"
                            ElseIf drSARMemo("StatusID") = "2" Then
                                item("Memo_No") = "2 - Approved"
                            ElseIf drSARMemo("StatusID") = "3" Then
                                item("Memo_No") = "3 - Rejected"
                            ElseIf drSARMemo("StatusID") = "4" Then
                                item("Memo_No") = "4 - Generate"
                            ElseIf drSARMemo("StatusID") = "5" Then
                                item("Memo_No") = "5 - On Progress Generate Report"
                            End If
                        End If
                    End If
                Next
                gp_case_alert.GetStore().DataSource = ObjTable
                gp_case_alert.GetStore().DataBind()


                '' End 5-Oct-2022
                FpSARMemo.Hidden = False

                'Set Value Otomatis
                Dim objCust As New goAML_Ref_Customer
                Dim KategoriCust As String = ""
                Dim CustName As String = ""
                Dim Pekerjaan As String = ""
                Dim CIF As String = Session("SARMemo_CIFNO").ToString
                Using objdb As New NawaDatadevEntities
                    objCust = objdb.goAML_Ref_Customer.Where(Function(x) x.CIF = CIF).FirstOrDefault
                    If objCust IsNot Nothing Then
                        If objCust.FK_Customer_Type_ID = 2 Then
                            CustName = objCust.Corp_Name
                            Pekerjaan = objCust.Corp_Business
                        Else
                            CustName = objCust.INDV_Last_Name
                            Pekerjaan = objCust.INDV_Occupation
                        End If
                        KategoriCust = "Nasabah"
                    Else
                        KategoriCust = "WIC"
                    End If

                End Using

                txtTanggal.Value = DateTime.Now
                Dim Perihal As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select parametervalue from OneFCC_CaseManagement_Parameter where pk_globalreportparameter_id = 23", Nothing)
                txtPerihal.Value = Perihal.Replace("$Customer$", CustName).Replace("$KategoriCust$", KategoriCust).Replace("$CIF$", CIF)
                txtKepada.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select parametervalue from OneFCC_CaseManagement_Parameter where pk_globalreportparameter_id = 20", Nothing)
                txtDari.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select parametervalue from OneFCC_CaseManagement_Parameter where pk_globalreportparameter_id = 21", Nothing)
                txtNamaPihakPelapor.Value = CustName
                txtKategoriPihakPelapor.Value = KategoriCust
                txtCabang.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select branch.branch_name from goaml_ref_customer cust inner join aml_branch branch on cust.opening_branch_code = branch.fk_aml_branch_code where cif ='" + objCust.CIF + "'", Nothing)
                txtLamaMenjadiNasabah.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " SELECT isnull('Sejak '+convert(varchar, Opening_Date, 106)+ ' ('+ CAST(DATEDIFF(MONTH, Opening_Date, GETDATE()) AS VARCHAR(10))+' Bulan)', 'Sejak') FROM dbo.goaml_ref_customer WHERE CIF ='" + objCust.CIF + "'", Nothing)
                txtProfilLainnya.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select parametervalue from OneFCC_CaseManagement_Parameter where pk_globalreportparameter_id = 22", Nothing)
                txtPekerjaanBidangUsaha.Value = Pekerjaan
            Else
                If Session("SARMemo_WICNO") Is Nothing Then
                    Throw New ApplicationException("Pilih Salah Satu WIC")
                End If
                txtUnitBisnis.Hidden = True
                txtDirektor.Hidden = True
                txtCabang.Hidden = True
                FieldSet1.Hidden = True
                txtLamaMenjadiNasabah.Hidden = True
                ' Mencari status dari CIF yang dipilih
                Dim StrStatusCIF As String = "Select status from VW_SARMemo_WICNo where wicno = '" & Session("SARMemo_WICNO").ToString & "'"
                Dim Status As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrStatusCIF, Nothing)
                ' Jika statusnya 0 atau Uncofigured
                If Status = "0" Then
                    Throw New ApplicationException("WIC yang dipilih belum ada konfigurasi workflownya")
                End If
                ' Tampung value CIF nya
                Dim WICSARMemo As String = Session("SARMemo_WICNO")

                '' Edit 5-Oct-2022 Felix 
                'Using objdb As New CasemanagementEntities
                '    Dim StatusMemo As String = ""
                '    ' Cari dulu case id berdasarkan CIF yang dipilih dan dimana statusnya 3 atau issue/closed
                '    Dim ListCaseID As List(Of OneFCC_CaseManagement) = objdb.OneFCC_CaseManagement.Where(Function(x) x.CIF_No = CIFSARMemo And x.FK_CaseStatus_ID = 3).ToList
                '    ' Cari juga case id yang pernah di simpan di STR Memo
                '    Dim ListCaseIDSAR As List(Of OneFCC_CaseManagement_SARMemo_CaseID) = objdb.OneFCC_CaseManagement_SARMemo_CaseID.ToList()
                '    ' Looping case ID
                '    For Each item In ListCaseID
                '        For Each item2 In ListCaseIDSAR
                '            ' Jika case ID yang ditemukan di casemanagement ternyata sudah ada di case STR Memo
                '            If item.PK_CaseManagement_ID = item2.CaseID Then
                '                ' maka fk str memo dari case id itu isi dengan fk str memo
                '                item.FK_OneFCC_CaseManagement_SARMemo_ID = item2.FK_OneFCC_CaseManagement_SARMemo_ID
                '            End If
                '        Next
                '    Next
                '    Dim ObjTable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(ListCaseID)
                '    ' Tambah Column Rule
                '    ObjTable.Columns.Add(New DataColumn("Rule", GetType(String)))
                '    For Each item As Data.DataRow In ObjTable.Rows
                '        ' Jika rule basic nya ada, maka cari description rule nya
                '        If Not IsDBNull(item("FK_Rule_Basic_ID")) Then
                '            'Dim RuleBasic As New OneFCC_MS_Rule_Basic
                '            'Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                '            'RuleBasic = objdb.OneFCC_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault
                '            'item("Rule") = RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                '            '' Edit 5-Oct-2022
                '            Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                '            'RuleBasic = objdb.OneFcc_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault '' Edit 5 Oct 2022
                '            Dim RuleBasic As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select PK_Rule_Basic_ID + ' - ' + Rule_Basic_Name from OneFcc_MS_Rule_Basic where PK_Rule_Basic_ID = " & FKRule & "", Nothing)
                '            item("Rule") = RuleBasic ' RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                '            '' End 5-Oct-2022
                '        End If
                '        ' Jika case ID nya tidak ada di Case ID STR Memo maka case id nya baru
                '        If IsDBNull(item("FK_OneFCC_CaseManagement_SARMemo_ID")) Then
                '            item("Memo_No") = "0 - New"
                '        Else
                '            Dim SARMemo As New OneFCC_CaseManagement_SARMemo
                '            Dim FkSARMemo As String = item("FK_OneFCC_CaseManagement_SARMemo_ID").ToString
                '            SARMemo = objdb.OneFCC_CaseManagement_SARMemo.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_ID = FkSARMemo).FirstOrDefault
                '            If SARMemo.StatusID = "1" Then
                '                item("Memo_No") = "1 - On Progress"
                '            ElseIf SARMemo.StatusID = "2" Then
                '                item("Memo_No") = "2 - Approved"
                '            ElseIf SARMemo.StatusID = "3" Then
                '                item("Memo_No") = "3 - Rejected"
                '            ElseIf SARMemo.StatusID = "4" Then
                '                item("Memo_No") = "4 - Generate"
                '            ElseIf SARMemo.StatusID = "5" Then
                '                item("Memo_No") = "5 - On Progress Generate Report"
                '            End If
                '        End If
                '    Next
                '    gp_case_alert.GetStore().DataSource = ObjTable
                '    gp_case_alert.GetStore().DataBind()
                'End Using

                Dim StatusMemo As String = ""
                ' Cari dulu case id berdasarkan CIF yang dipilih dan dimana statusnya 3 atau issue/closed
                Dim listCaseID As List(Of OneFCC_CaseManagement) = New List(Of OneFCC_CaseManagement)
                'listCaseID = objdb.OneFCC_CaseManagement.Where(Function(x) x.CIF_No = CIFSARMemo And x.FK_CaseStatus_ID = 3).ToList
                'Dim dtCaseID As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement csm inner join OneFCC_MS_Rule_Basic rb on csm.FK_Rule_Basic_ID = rb.pk_rule_basic_id where csm.WIC_No = '" & WICSARMemo & "' and csm.FK_CaseStatus_ID = 3 and csm.FK_OneFCC_CaseManagement_SARMemo_ID is null and rb.FK_Rule_Template_ID = 3", Nothing)
                Dim dtCaseID As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement csm inner join OneFCC_CaseManagement_Typology tp on csm.PK_CaseManagement_ID = tp.FK_CaseManagement_ID where csm.WIC_No = '" & WICSARMemo & "' and csm.FK_CaseStatus_ID = 3 and csm.FK_OneFCC_CaseManagement_STRMemo_ID is null and tp.Is_Transaction = 0", Nothing)

                If dtCaseID IsNot Nothing Then
                    For Each row As DataRow In dtCaseID.Rows
                        Dim CaseID As OneFCC_CaseManagement = New OneFCC_CaseManagement()

                        If Not IsDBNull(row("PK_CaseManagement_ID")) Then
                            CaseID.PK_CaseManagement_ID = row("PK_CaseManagement_ID")
                        End If

                        If Not IsDBNull(row("Alert_Type")) Then
                            CaseID.Alert_Type = row("Alert_Type")
                        End If

                        If Not IsDBNull(row("FK_Rule_Basic_ID")) Then
                            CaseID.FK_Rule_Basic_ID = row("FK_Rule_Basic_ID")
                        End If

                        If Not IsDBNull(row("Case_Description")) Then
                            CaseID.Case_Description = row("Case_Description")
                        End If

                        If Not IsDBNull(row("Is_Customer")) Then
                            CaseID.Is_Customer = row("Is_Customer")
                        End If

                        If Not IsDBNull(row("CIF_No")) Then
                            CaseID.CIF_No = row("CIF_No")
                        End If

                        If Not IsDBNull(row("Account_No")) Then
                            CaseID.Account_No = row("Account_No")
                        End If

                        If Not IsDBNull(row("Customer_Name")) Then
                            CaseID.Customer_Name = row("Customer_Name")
                        End If

                        If Not IsDBNull(row("WIC_No")) Then
                            CaseID.WIC_No = row("WIC_No")
                        End If

                        If Not IsDBNull(row("WIC_Name")) Then
                            CaseID.WIC_Name = row("WIC_Name")
                        End If

                        If Not IsDBNull(row("FK_Proposed_Status_ID")) Then
                            CaseID.FK_Proposed_Status_ID = row("FK_Proposed_Status_ID")
                        End If

                        If Not IsDBNull(row("Proposed_By")) Then
                            CaseID.Proposed_By = row("Proposed_By")
                        End If

                        If Not IsDBNull(row("FK_CaseStatus_ID")) Then
                            CaseID.FK_CaseStatus_ID = row("FK_CaseStatus_ID")
                        End If

                        If Not IsDBNull(row("FK_CaseManagement_Workflow_ID")) Then
                            CaseID.FK_CaseManagement_Workflow_ID = row("FK_CaseManagement_Workflow_ID")
                        End If

                        If Not IsDBNull(row("Workflow_Step")) Then
                            CaseID.Workflow_Step = row("Workflow_Step")
                        End If

                        If Not IsDBNull(row("PIC")) Then
                            CaseID.PIC = row("PIC")
                        End If

                        If Not IsDBNull(row("Aging")) Then
                            CaseID.Aging = row("Aging")
                        End If

                        If Not IsDBNull(row("StatusRFI")) Then
                            CaseID.StatusRFI = row("StatusRFI")
                        End If


                        If Not IsDBNull(row("FK_CaseStatus_ID")) Then
                            CaseID.FK_CaseStatus_ID = row("FK_CaseStatus_ID")
                        End If

                        If Not IsDBNull(row("ProcessDate")) Then
                            CaseID.ProcessDate = row("ProcessDate")
                        End If

                        If Not IsDBNull(row("FK_Report_ID")) Then
                            CaseID.FK_Report_ID = row("FK_Report_ID")
                        End If

                        If Not IsDBNull(row("Active")) Then
                            CaseID.Active = row("Active")
                        End If

                        If Not IsDBNull(row("CreatedBy")) Then
                            CaseID.CreatedBy = row("CreatedBy")
                        End If

                        If Not IsDBNull(row("LastUpdateBy")) Then
                            CaseID.LastUpdateBy = row("LastUpdateBy")
                        End If

                        If Not IsDBNull(row("ApprovedBy")) Then
                            CaseID.ApprovedBy = row("ApprovedBy")
                        End If

                        If Not IsDBNull(row("CreatedDate")) Then
                            CaseID.CreatedDate = row("CreatedDate")
                        End If

                        If Not IsDBNull(row("LastUpdateDate")) Then
                            CaseID.LastUpdateDate = row("LastUpdateDate")
                        End If

                        If Not IsDBNull(row("ApprovedDate")) Then
                            CaseID.ApprovedDate = row("ApprovedDate")
                        End If

                        If Not IsDBNull(row("Alternateby")) Then
                            CaseID.Alternateby = row("Alternateby")
                        End If

                        'If Not IsDBNull(drCaseID("Rule")) Then
                        '    CaseID.Account_No = drCaseID("Rule")
                        'End If
                        'If Not IsDBNull(row("FK_OneFCC_CaseManagement_SARMemo_ID")) Then
                        '    CaseID.FK_OneFCC_CaseManagement_STRMemo_ID = row("FK_OneFCC_CaseManagement_SARMemo_ID")
                        'End If
                        If Not IsDBNull(row("Memo_No")) Then
                            CaseID.Memo_No = row("Memo_No")
                        End If

                        listCaseID.Add(CaseID)
                    Next
                End If


                ' Cari juga case id yang pernah di simpan di STR Memo
                Dim listCaseIDSAR As New DataTable
                listCaseIDSAR = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_SARMemo_CaseID")
                ' Looping case ID

                For Each item In listCaseID
                    For Each item2 As DataRow In listCaseIDSAR.Rows
                        ' Jika case ID yang ditemukan di casemanagement ternyata sudah ada di case STR Memo
                        If item.PK_CaseManagement_ID = item2("CaseID") Then
                            ' maka fk str memo dari case id itu isi dengan fk str memo
                            Dim FKSARMemo As String = ""
                            'FKSARMemo = item2("FK_OneFCC_CaseManagement_SARMemo_ID").ToString
                            'item.FK_OneFCC_CaseManagement_STRMemo_ID = FKSARMemo
                        End If
                    Next
                Next
                Dim ObjTable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listCaseID)
                ' Tambah Column Rule
                ObjTable.Columns.Add(New DataColumn("Rule", GetType(String)))
                ObjTable.Columns.Add(New DataColumn("UniqueCMID", GetType(String))) '' Add 7-Mar-23, Ari. FAMA tambah 1 field baru untuk unique cm id
                For Each item As Data.DataRow In ObjTable.Rows
                    If Not IsDBNull(item("FK_Rule_Basic_ID")) Then
                        '' Edit 5-Oct-2022 Felix
                        'Dim RuleBasic As New OneFCC_MS_Rule_Basic
                        'Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                        'RuleBasic = objdb.OneFCC_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault
                        'item("Rule") = RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                        'Dim RuleBasic As New OneFcc_MS_Rule_Basic
                        Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                        'RuleBasic = objdb.OneFcc_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault '' Edit 5 Oct 2022
                        Dim RuleBasic As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select cast (PK_Rule_Basic_ID as varchar(100)) + ' - ' + Rule_Basic_Name from OneFcc_MS_Rule_Basic where PK_Rule_Basic_ID = " & FKRule & "", Nothing)
                        item("Rule") = RuleBasic ' RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                        '' End 5-Oct-2022
                    End If
                    '' Add 7-Mar-23, Ari. FAMA tambah 1 field baru untuk unique cm id
                    If item("PK_CaseManagement_ID") > 0 Then
                        Dim PKCM As Long = Convert.ToInt64(item("PK_CaseManagement_ID"))
                        Dim UniqueCMDID As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select Unique_CM_ID from OneFCC_CaseManagement where PK_CaseManagement_ID = " & PKCM, Nothing).ToString
                        item("UniqueCMID") = UniqueCMDID
                    End If

                    Dim PKCasManagement As String
                    Dim FKSARMemo_FromTable As String = ""
                    If Not IsDBNull(item("PK_CaseManagement_ID")) Then
                        PKCasManagement = item("PK_CaseManagement_ID").ToString
                        FKSARMemo_FromTable = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select isnull(FK_OneFCC_CaseManagement_SARMemo_ID,'') from OneFCC_CaseManagement_SARMemo_CaseID where caseID = " & PKCasManagement, Nothing)
                    End If



                    ' Jika case ID nya tidak ada di Case ID STR Memo maka case id nya baru
                    ' Jika case ID nya tidak ada di Case ID STR Memo maka case id nya baru
                    If FKSARMemo_FromTable Is Nothing Or FKSARMemo_FromTable = "" Then
                        item("Memo_No") = "0 - New"
                    Else
                        Dim SARMemo As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo
                        Dim FkSARMemo As String = FKSARMemo_FromTable
                        'SARMemo = objdb.OneFCC_CaseManagement_SARMemo.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_ID = FkSARMemo).FirstOrDefault
                        Dim drSARMemo As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select StatusID from OneFCC_CaseManagement_SARMemo where PK_OneFCC_CaseManagement_SARMemo_ID = " & FkSARMemo, Nothing)
                        If drSARMemo IsNot Nothing Then
                            If drSARMemo("StatusID") = "1" Then
                                item("Memo_No") = "1 - On Progress"
                            ElseIf drSARMemo("StatusID") = "2" Then
                                item("Memo_No") = "2 - Approved"
                            ElseIf drSARMemo("StatusID") = "3" Then
                                item("Memo_No") = "3 - Rejected"
                            ElseIf drSARMemo("StatusID") = "4" Then
                                item("Memo_No") = "4 - Generate"
                            ElseIf drSARMemo("StatusID") = "5" Then
                                item("Memo_No") = "5 - On Progress Generate Report"
                            End If
                        End If
                    End If
                Next
                gp_case_alert.GetStore().DataSource = ObjTable
                gp_case_alert.GetStore().DataBind()


                '' End 5-Oct-2022
                FpSARMemo.Hidden = False

                'Set Value Otomatis
                Dim objWIC As New goAML_Ref_WIC
                Dim KategoriCust As String = ""
                Dim CustName As String = ""
                Dim Pekerjaan As String = ""
                Dim WIC As String = Session("SARMemo_WICNO").ToString
                Using objdb As New NawaDatadevEntities
                    objWIC = objdb.goAML_Ref_WIC.Where(Function(x) x.WIC_No = WIC).FirstOrDefault
                    If objWIC IsNot Nothing Then
                        If objWIC.FK_Customer_Type_ID = 2 Then
                            CustName = objWIC.Corp_Name
                            Pekerjaan = objWIC.Corp_Business
                        Else
                            CustName = objWIC.INDV_Last_Name
                            Pekerjaan = objWIC.INDV_Occupation
                        End If
                        KategoriCust = "Nasabah"
                    Else
                        KategoriCust = "WIC"
                    End If

                End Using

                txtTanggal.Value = DateTime.Now
                Dim Perihal As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select parametervalue from OneFCC_CaseManagement_Parameter where pk_globalreportparameter_id = 23", Nothing)
                txtPerihal.Value = Perihal.Replace("$Customer$", CustName).Replace("$KategoriCust$", KategoriCust).Replace("$CIF$", WIC)
                txtKepada.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select parametervalue from OneFCC_CaseManagement_Parameter where pk_globalreportparameter_id = 20", Nothing)
                txtDari.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select parametervalue from OneFCC_CaseManagement_Parameter where pk_globalreportparameter_id = 21", Nothing)
                txtNamaPihakPelapor.Value = CustName
                txtKategoriPihakPelapor.Value = KategoriCust
                'txtCabang.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select branch.branch_name from goaml_ref_customer cust inner join aml_branch branch on cust.opening_branch_code = branch.fk_aml_branch_code where cif ='" + objWIC.WIC_No + "'", Nothing)
                '    txtLamaMenjadiNasabah.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " SELECT isnull('Sejak '+convert(varchar, Opening_Date, 106)+ ' ('+ CAST(DATEDIFF(MONTH, Opening_Date, GETDATE()) AS VARCHAR(10))+' Bulan)', 'Sejak') FROM dbo.goaml_ref_customer WHERE CIF ='" + objWIC.WIC_No + "'", Nothing)
                txtProfilLainnya.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select parametervalue from OneFCC_CaseManagement_Parameter where pk_globalreportparameter_id = 22", Nothing)
                txtPekerjaanBidangUsaha.Value = Pekerjaan
            End If
            'TxtActivity.Value = "Transactions Hit By Case Alert"
            'TxtActivity.ReadOnly = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try



    End Sub

    Private Sub LoadData()
        Try
            ' Set Label
            LblLatarBelakang.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblPihakCalonTerkait.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblPihakTerkait.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblHasilAnalisis.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblKesimpulan.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblTindakLanjut.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblTransaksiSTR.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblWorkflowHistory.StyleSpec = "font-weight: bold;font-size : 20px;"
            'TxtActivity.Value = "Transactions Hit By Case Alert"
            'TxtActivity.ReadOnly = True
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub isDataValid()
        Try
            If String.IsNullOrEmpty(IsCustomer.Value) Then
                Throw New ApplicationException(IsCustomer.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtNo.Value) Then
                Throw New ApplicationException(txtNo.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtKepada.Value) Then
                Throw New ApplicationException(txtKepada.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtDari.Value) Then
                Throw New ApplicationException(txtDari.FieldLabel + " is required.")
            End If
            If txtTanggal.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException(txtTanggal.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtPerihal.Value) Then
                Throw New ApplicationException(txtPerihal.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtSumberPelaporan.Value) Then
                Throw New ApplicationException(txtSumberPelaporan.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtNamaPihakPelapor.Value) Then
                Throw New ApplicationException(txtNamaPihakPelapor.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtKategoriPihakPelapor.Value) Then
                Throw New ApplicationException(txtKategoriPihakPelapor.FieldLabel + " is required.")
            End If
            If IsCustomer.Value Then
                ' 3 Jan 2022 Ari : Field ini jadi tidak mandatory
                'If String.IsNullOrEmpty(txtUnitBisnis.Value) Then
                '    Throw New ApplicationException(txtUnitBisnis.FieldLabel + " is required.")
                'End If
                'If String.IsNullOrEmpty(txtDirektor.Value) Then
                '    Throw New ApplicationException(txtUnitBisnis.FieldLabel + " is required.")
                'End If
                If String.IsNullOrEmpty(txtCabang.Value) Then
                    Throw New ApplicationException(txtUnitBisnis.FieldLabel + " is required.")
                End If
                If String.IsNullOrEmpty(txtLamaMenjadiNasabah.Value) Then
                    Throw New ApplicationException(txtLamaMenjadiNasabah.FieldLabel + " is required.")
                End If
            End If


            If String.IsNullOrEmpty(txtPekerjaanBidangUsaha.Value) Then
                Throw New ApplicationException(txtPekerjaanBidangUsaha.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtPengahasilanTahun.Value) Then
                Throw New ApplicationException(txtPengahasilanTahun.FieldLabel + " is required Or It Has Character.")
                'Else
                '    Dim JumlahKarakterPenghasilan As Integer = 0
                '    Dim PenghasilanThn As String = txtPengahasilanTahun.Value
                '    For i = 0 To PenghasilanThn.Length - 1
                '        If Char.IsLetter(PenghasilanThn.Chars(i)) Then
                '            JumlahKarakterPenghasilan = JumlahKarakterPenghasilan + 1
                '        End If
                '    Next
                '    If JumlahKarakterPenghasilan > 0 Then
                '        Throw New ApplicationException("Inputan Dari Penghasilan Tahunan Terdapat Karakter!")
                '    End If
            End If
            If String.IsNullOrEmpty(txtProfilLainnya.Value) Then
                Throw New ApplicationException(txtProfilLainnya.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtHasilAnalisis.Value) Then
                Throw New ApplicationException("Hasil Analisis is required.")
            End If
            If String.IsNullOrEmpty(txtKesimpulan.Value) Then
                Throw New ApplicationException("Kesimpulan is required.")
            End If
            If obj_ListIndikator_Edit.Count = 0 Then
                Throw New ApplicationException("Indikator is required.")
            End If
            'If obj_ListPihakTerkait_Edit.Count = 0 Then
            '    Throw New ApplicationException("Pihak Terkait is required.")
            'End If
            If obj_ListTindakLanjut_Edit.Count = 0 Then
                Throw New ApplicationException("Tindak Lanjut is required.")
            End If
            If obj_ListActivity_Edit.Count = 0 Then
                Throw New ApplicationException("Activity is required.")
            End If
            'If dd_TrnFilterBy.SelectedItemValue = "3" Then
            '    If sar_DateFrom.SelectedDate = DateTime.MinValue Then
            '        Throw New ApplicationException(sar_DateFrom.FieldLabel + " is required.")
            '    End If
            '    If sar_DateTo.SelectedDate = DateTime.MinValue Then
            '        Throw New ApplicationException(sar_DateTo.FieldLabel + " is required.")
            '    End If

            'End If
            If TanggalLaporan.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException(TanggalLaporan.FieldLabel + " is required.")
            End If

            If String.IsNullOrEmpty(sar_jenisLaporan.SelectedItem.Value) Then
                Throw New ApplicationException(sar_jenisLaporan.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(alasan.Value) Then
                Throw New ApplicationException(alasan.FieldLabel + " is required.")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            isDataValid()

            Dim SmCaseID As RowSelectionModel = TryCast(gp_case_alert.GetSelectionModel(), RowSelectionModel)
            Dim SelectedCaseID As RowSelectionModel = gp_case_alert.SelectionModel.Primary
            ' Jika user tidak memilih case iD
            If SelectedCaseID.SelectedRows.Count = 0 Then

                Throw New Exception("Minimal 1 Case ID harus dipilih")
            End If
            Dim TotalCaseID As Integer = 0
            For Each item As SelectedRow In SelectedCaseID.SelectedRows
                Dim JumlahCase As Integer = 0
                Dim recordID = item.RecordID.ToString
                Dim StrStatusCIF As String = "select count(*) from OneFCC_CaseManagement_SARMemo_CaseID cases inner join OneFCC_CaseManagement ofcm on cases.caseid = ofcm.pk_CaseManagement_id where ofcm.pk_CaseManagement_id = " & recordID.ToString
                JumlahCase = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrStatusCIF, Nothing)
                TotalCaseID = TotalCaseID + JumlahCase
            Next
            ' Jika case ID yang dipilih sudah ada di CaseID STR Memo
            If TotalCaseID > 0 Then
                Throw New ApplicationException("Salah Satu Case yang dipilih sudah di pilih oleh SARMemo yang lain")
            End If

            For Each item As SelectedRow In SmCaseID.SelectedRows
                Dim RecordID = item.RecordID.ToString
                Dim ObjNew = New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_CaseID
                With ObjNew
                    .CaseID = RecordID
                End With

                objSARMemoClass.listCaseID.Add(ObjNew)
            Next
            If IsCustomer.Value Then
                Dim CIF As String = Session("SARMemo_CIFNO")
                Dim StrQueryCode As String = "select isnull(indv_last_name,'') as indv_last_name from goaml_Ref_customer where cif = '" & CIF.ToString & "'"
                Dim CIFLastName As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)

                objSARMemoClass.ObjSARMemo.CIF = CIF
                objSARMemoClass.ObjSARMemo.IsCustomer = True
                objSARMemoClass.ObjSARMemo.No = txtNo.Value
                objSARMemoClass.ObjSARMemo.Kepada = txtKepada.Value
                objSARMemoClass.ObjSARMemo.Dari = txtDari.Value
                objSARMemoClass.ObjSARMemo.Tanggal = txtTanggal.Value
                objSARMemoClass.ObjSARMemo.Perihal = txtPerihal.Value
                objSARMemoClass.ObjSARMemo.Sumber_Pelaporan = txtSumberPelaporan.Value
                objSARMemoClass.ObjSARMemo.Nama_Pihak_Terlapor = txtNamaPihakPelapor.Value
                objSARMemoClass.ObjSARMemo.Kateri_Pihak_Terlapor = txtKategoriPihakPelapor.Value
                objSARMemoClass.ObjSARMemo.Unit_Bisnis_Directorat = txtUnitBisnis.Value
                objSARMemoClass.ObjSARMemo.Direktorat = txtDirektor.Value
                objSARMemoClass.ObjSARMemo.Cabang = txtCabang.Value
                If Not String.IsNullOrEmpty(cmb_KesimpulanDetail.SelectedItemValue) Then
                    objSARMemoClass.ObjSARMemo.Detail_Kesimpulan = cmb_KesimpulanDetail.SelectedItemValue
                End If

                objSARMemoClass.ObjSARMemo.Lama_menjadi_Nasabah = txtLamaMenjadiNasabah.Value
                objSARMemoClass.ObjSARMemo.Pekerjaan_Bidang_Usaha = txtPekerjaanBidangUsaha.Value
                'Dim Penghasilan As Decimal = Convert.ToDecimal(txtPengahasilanTahun.Value, New CultureInfo("en-US"))
                'Dim multiplier As Integer = Convert.ToInt32(Math.Pow(10, 2))
                'Dim newD As Decimal = Math.Truncate(Penghasilan * multiplier) / multiplier
                objSARMemoClass.ObjSARMemo.Penghasilan_Tahun = txtPengahasilanTahun.Value
                objSARMemoClass.ObjSARMemo.Profil_Lainnya = txtProfilLainnya.Value
                objSARMemoClass.ObjSARMemo.Hasil_Analisis = txtHasilAnalisis.Value.ToString
                objSARMemoClass.ObjSARMemo.Kesimpulan = txtKesimpulan.Value
                objSARMemoClass.ObjSARMemo.Jenis_Laporan = sar_jenisLaporan.SelectedItem.Value
                objSARMemoClass.ObjSARMemo.Tanggal_Laporan = TanggalLaporan.Value
                objSARMemoClass.ObjSARMemo.NoRefPPATK = NoRefPPATK.Value
                objSARMemoClass.ObjSARMemo.Alasan = alasan.Value
                If obj_ListPihakTerkait_Edit IsNot Nothing Then
                    objSARMemoClass.listPihakTerkait = obj_ListPihakTerkait_Edit
                Else
                    obj_ListPihakTerkait_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait)
                    objSARMemoClass.listPihakTerkait = obj_ListPihakTerkait_Edit
                End If

                objSARMemoClass.listTindakLanjut = obj_ListTindakLanjut_Edit
                objSARMemoClass.listIndikator = obj_ListIndikator_Edit
                objSARMemoClass.listAttachment = obj_ListAttachment_Edit
                objSARMemoClass.listActivity = obj_ListActivity_Edit


                'End If
                objSARMemoClass.ObjSARMemo.StatusID = 1
                objSARMemoClass.ObjSARMemo.Workflow_Step = 2
            Else
                Dim WIC As String = Session("SARMemo_WICNO")
                Dim StrQueryCode As String = "select isnull(indv_last_name,'') as indv_last_name from goaml_Ref_wic where wic_no = '" & WIC.ToString & "'"
                Dim CIFLastName As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)

                objSARMemoClass.ObjSARMemo.WIC = WIC
                objSARMemoClass.ObjSARMemo.IsCustomer = False
                objSARMemoClass.ObjSARMemo.No = txtNo.Value
                objSARMemoClass.ObjSARMemo.Kepada = txtKepada.Value
                objSARMemoClass.ObjSARMemo.Dari = txtDari.Value
                objSARMemoClass.ObjSARMemo.Tanggal = txtTanggal.Value
                objSARMemoClass.ObjSARMemo.Perihal = txtPerihal.Value
                objSARMemoClass.ObjSARMemo.Sumber_Pelaporan = txtSumberPelaporan.Value
                objSARMemoClass.ObjSARMemo.Nama_Pihak_Terlapor = txtNamaPihakPelapor.Value
                objSARMemoClass.ObjSARMemo.Kateri_Pihak_Terlapor = txtKategoriPihakPelapor.Value
                'objSARMemoClass.ObjSARMemo.Unit_Bisnis_Directorat = txtUnitBisnis.Value
                'objSARMemoClass.ObjSARMemo.Direktorat = txtDirektor.Value
                'objSARMemoClass.ObjSARMemo.Cabang = txtCabang.Value
                If Not String.IsNullOrEmpty(cmb_KesimpulanDetail.SelectedItemValue) Then
                    objSARMemoClass.ObjSARMemo.Detail_Kesimpulan = cmb_KesimpulanDetail.SelectedItemValue
                End If

                'objSARMemoClass.ObjSARMemo.Lama_menjadi_Nasabah = txtLamaMenjadiNasabah.Value
                objSARMemoClass.ObjSARMemo.Pekerjaan_Bidang_Usaha = txtPekerjaanBidangUsaha.Value
                'Dim Penghasilan As Decimal = Convert.ToDecimal(txtPengahasilanTahun.Value, New CultureInfo("en-US"))
                'Dim multiplier As Integer = Convert.ToInt32(Math.Pow(10, 2))
                'Dim newD As Decimal = Math.Truncate(Penghasilan * multiplier) / multiplier
                objSARMemoClass.ObjSARMemo.Penghasilan_Tahun = txtPengahasilanTahun.Value
                objSARMemoClass.ObjSARMemo.Profil_Lainnya = txtProfilLainnya.Value
                objSARMemoClass.ObjSARMemo.Hasil_Analisis = txtHasilAnalisis.Value.ToString
                objSARMemoClass.ObjSARMemo.Kesimpulan = txtKesimpulan.Value
                objSARMemoClass.ObjSARMemo.Jenis_Laporan = sar_jenisLaporan.SelectedItem.Value
                objSARMemoClass.ObjSARMemo.Tanggal_Laporan = TanggalLaporan.Value
                objSARMemoClass.ObjSARMemo.NoRefPPATK = NoRefPPATK.Value
                objSARMemoClass.ObjSARMemo.Alasan = alasan.Value
                If obj_ListPihakTerkait_Edit IsNot Nothing Then
                    objSARMemoClass.listPihakTerkait = obj_ListPihakTerkait_Edit
                Else
                    obj_ListPihakTerkait_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait)
                    objSARMemoClass.listPihakTerkait = obj_ListPihakTerkait_Edit
                End If

                objSARMemoClass.listTindakLanjut = obj_ListTindakLanjut_Edit
                objSARMemoClass.listIndikator = obj_ListIndikator_Edit
                objSARMemoClass.listAttachment = obj_ListAttachment_Edit
                objSARMemoClass.listActivity = obj_ListActivity_Edit


                'End If
                objSARMemoClass.ObjSARMemo.StatusID = 1
                objSARMemoClass.ObjSARMemo.Workflow_Step = 2
            End If


            '' Isi Value Transcation STR ke STR Memo Transcation
            'Dim ListSar As New List(Of goAML_ODM_Generate_STR_SAR)
            'Dim ListODMTransactionIncomplete As New List(Of goAML_ODM_Transaksi)
            'Dim ListTransactionIncomplete As New List(Of OneFCC_CaseManagement_SARMemo_Transaction)
            'Dim getIsGenerateSAR As String = NawaDevBLL.GenerateSarBLL.getIsGenerateSAR("IsGenerateSAR")
            'Dim ListTransactionNoTrnMode As New List(Of OneFCC_CaseManagement_SARMemo_Transaction)
            'Dim ListODMTransactionNoTrnMode As New List(Of goAML_ODM_Transaksi)
            'Dim dateReport As Date = TanggalLaporan.SelectedDate
            'Dim maxDate As DateTime = DateTime.Now

            'If sar_jenisLaporan.Value = "LTKMP" And NoRefPPATK.Text = "" Then
            '    Throw New ApplicationException("Jika Jenis Laporannya LTKMP maka No Ref PPATK Tidak Boleh Kosong")
            'ElseIf dateReport <> DateTime.MinValue AndAlso dateReport > maxDate Then
            '    Throw New Exception("Tanngal Laporan : Can not select future date")
            'End If




            'Dim selected As Boolean
            'If ListTransaction.Count = ListSelectedTransaction.Count Or sar_IsSelectedAll.Value Then
            '    selected = True
            'Else
            '    selected = False
            'End If


            'If selected = False Then
            '    If ListSelectedTransaction.Count = 0 Then
            '        Throw New ApplicationException("Tidak Ada Transaksi yang dipilih")
            '    End If

            '    Dim objODMTransaksi As New goAML_ODM_Transaksi
            '    Dim objSARTransaksi As New OneFCC_CaseManagement_SARMemo_Transaction

            '    objSARMemoClass.listTransaction = New List(Of OneFCC_CaseManagement_SARMemo_Transaction)
            '    Dim param_RadioOption As String = ""

            '    If dd_TrnFilterBy.SelectedItemValue = "1" Then
            '        param_RadioOption = "TrnHitByCaseAlert"
            '    ElseIf dd_TrnFilterBy.SelectedItemValue = "2" Then
            '        param_RadioOption = "Trn5Year"
            '    ElseIf dd_TrnFilterBy.SelectedItemValue = "3" Then
            '        param_RadioOption = "CustomDate"
            '    End If

            '    For Each item In ListSelectedTransaction
            '        objODMTransaksi = ListTransaction.Where(Function(x) x.NO_ID = item.NO_ID).FirstOrDefault
            '        If objODMTransaksi IsNot Nothing Then
            '            With objSARTransaksi
            '                .Jenis_Laporan = sar_jenisLaporan.SelectedItem.Value
            '                .Tanggal_Laporan = TanggalLaporan.Value
            '                .NoRefPPATK = NoRefPPATK.Text
            '                .Alasan = alasan.Text
            '                If dd_TrnFilterBy.SelectedItemValue = "3" Then
            '                    .Date_Form = sar_DateFrom.Value
            '                    .Date_To = sar_DateTo.Value
            '                Else
            '                    .Date_Form = Nothing
            '                    .Date_To = Nothing
            '                End If

            '                .FilterTransaction = param_RadioOption

            '                .NO_IDTransaction = objODMTransaksi.NO_ID
            '                .Date_Transaction = objODMTransaksi.Date_Transaction
            '                .CIF_NO = objODMTransaksi.CIF_NO
            '                .Account_NO = objODMTransaksi.Account_NO
            '                .WIC_NO = objODMTransaksi.WIC_No
            '                .Ref_Num = objODMTransaksi.Ref_Num
            '                .Debit_Credit = objODMTransaksi.Debit_Credit
            '                .Original_Amount = objODMTransaksi.Original_Amount
            '                .Currency = objODMTransaksi.Currency
            '                .Exchange_Rate = objODMTransaksi.Exchange_Rate
            '                .IDR_Amount = objODMTransaksi.IDR_Amount
            '                .Transaction_Code = objODMTransaksi.Transaction_Code
            '                .Source_Data = objODMTransaksi.Source_Data
            '                .Transaction_Remark = objODMTransaksi.Transaction_Remark
            '                .Transaction_Number = objODMTransaksi.Transaction_Number
            '                .Transaction_Location = objODMTransaksi.Transaction_Location
            '                .Teller = objODMTransaksi.Teller
            '                .Authorized = objODMTransaksi.Authorized
            '                .Date_Posting = objODMTransaksi.Date_Posting
            '                .Transmode_Code = objODMTransaksi.Transmode_Code
            '                .Transmode_Comment = objODMTransaksi.Transmode_Comment
            '                .Comments = objODMTransaksi.Comments
            '                .CIF_No_Lawan = objODMTransaksi.CIF_No_Lawan
            '                .ACCOUNT_No_Lawan = objODMTransaksi.ACCOUNT_No_Lawan
            '                .WIC_No_Lawan = objODMTransaksi.WIC_No_Lawan
            '                .Conductor_ID = objODMTransaksi.Conductor_ID
            '                .Swift_Code_Lawan = objODMTransaksi.Swift_Code_Lawan
            '                .country_code_lawan = objODMTransaksi.Country_Code_Lawan
            '                .MsgTypeSwift = objODMTransaksi.MsgSwiftType
            '                .From_Funds_Code = objODMTransaksi.From_Funds_Code
            '                .To_Funds_Code = objODMTransaksi.To_Funds_Code
            '                .country_code = objODMTransaksi.Country_Code
            '                .Currency_Lawan = objODMTransaksi.Currency_Lawan
            '                If objODMTransaksi.BiMultiParty.HasValue Then
            '                    .BiMultiParty = objODMTransaksi.BiMultiParty
            '                End If
            '                .GCN = objODMTransaksi.GCN
            '                .GCN_Lawan = objODMTransaksi.GCN_Lawan
            '                .Active = 1
            '                .Business_date = objODMTransaksi.Business_date
            '                If objODMTransaksi.From_Type.HasValue Then
            '                    .From_Type = objODMTransaksi.From_Type
            '                End If
            '                If objODMTransaksi.To_Type.HasValue Then
            '                    .To_Type = objODMTransaksi.To_Type
            '                End If
            '            End With

            '            objSARMemoClass.listTransaction.Add(objSARTransaksi)

            '        End If


            '        objODMTransaksi = New goAML_ODM_Transaksi
            '        objSARTransaksi = New OneFCC_CaseManagement_SARMemo_Transaction
            '    Next

            '    If getIsGenerateSAR = "1" Then
            '        ListTransactionIncomplete = objSARMemoClass.listTransaction.Where(Function(x) x.BiMultiParty = 1 And x.CIF_No_Lawan Is Nothing And x.WIC_No_Lawan Is Nothing And x.ACCOUNT_No_Lawan Is Nothing).ToList
            '        If ListTransactionIncomplete.Count > 0 Then
            '            Throw New ApplicationException("Any Counter Party of Transaction is not completed")
            '        End If
            '    End If

            '    ListTransactionNoTrnMode = objSARMemoClass.listTransaction.Where(Function(x) x.Transmode_Code = "" Or x.Transmode_Code Is Nothing).ToList
            '    If ListTransactionNoTrnMode.Count > 0 Then
            '        Throw New ApplicationException("Cara Tranaksi Dilakukan Kosong Di " + ListTransactionNoTrnMode.Count.ToString + " Transaksi")
            '    End If

            'ElseIf selected = True Then
            '    Dim param_RadioOption As String = ""

            '    If dd_TrnFilterBy.SelectedItemValue = "1" Then
            '        param_RadioOption = "TrnHitByCaseAlert"
            '    ElseIf dd_TrnFilterBy.SelectedItemValue = "2" Then
            '        param_RadioOption = "Trn5Year"
            '    ElseIf dd_TrnFilterBy.SelectedItemValue = "3" Then
            '        param_RadioOption = "CustomDate"
            '    End If
            '    '' add 23-Feb-2022
            '    'Dim objODMTransaksi As New goAML_ODM_Transaksi
            '    Dim objSARTransaksi As New OneFCC_CaseManagement_SARMemo_Transaction
            '    For Each item In ListTransaction
            '        With objSARTransaksi
            '            .Jenis_Laporan = sar_jenisLaporan.SelectedItem.Value
            '            .Tanggal_Laporan = TanggalLaporan.Value
            '            .NoRefPPATK = NoRefPPATK.Text
            '            .Alasan = alasan.Text
            '            .Date_Transaction = item.Date_Transaction
            '            If dd_TrnFilterBy.SelectedItemValue = "3" Then
            '                .Date_Form = sar_DateFrom.Value
            '                .Date_To = sar_DateTo.Value
            '            Else
            '                .Date_Form = Nothing
            '                .Date_To = Nothing
            '            End If

            '            .FilterTransaction = param_RadioOption
            '            .NO_IDTransaction = "ALL"
            '            .CIF_NO = item.CIF_NO
            '            .Account_NO = item.Account_NO
            '            .WIC_NO = item.WIC_No
            '            .Ref_Num = item.Ref_Num
            '            .Debit_Credit = item.Debit_Credit
            '            .Original_Amount = item.Original_Amount
            '            .Currency = item.Currency
            '            .Exchange_Rate = item.Exchange_Rate
            '            .IDR_Amount = item.IDR_Amount
            '            .Transaction_Code = item.Transaction_Code
            '            .Source_Data = item.Source_Data
            '            .Transaction_Remark = item.Transaction_Remark
            '            .Transaction_Number = item.Transaction_Number
            '            .Transaction_Location = item.Transaction_Location
            '            .Teller = item.Teller
            '            .Authorized = item.Authorized
            '            .Date_Posting = item.Date_Posting
            '            .Transmode_Code = item.Transmode_Code
            '            .Transmode_Comment = item.Transmode_Comment
            '            .Comments = item.Comments
            '            .CIF_No_Lawan = item.CIF_No_Lawan
            '            .ACCOUNT_No_Lawan = item.ACCOUNT_No_Lawan
            '            .WIC_No_Lawan = item.WIC_No_Lawan
            '            .Conductor_ID = item.Conductor_ID
            '            .Swift_Code_Lawan = item.Swift_Code_Lawan
            '            .country_code_lawan = item.Country_Code_Lawan
            '            .MsgTypeSwift = item.MsgSwiftType
            '            .From_Funds_Code = item.From_Funds_Code
            '            .To_Funds_Code = item.To_Funds_Code
            '            .country_code = item.Country_Code
            '            .Currency_Lawan = item.Currency_Lawan
            '            If item.BiMultiParty.HasValue Then
            '                .BiMultiParty = item.BiMultiParty
            '            End If
            '            .GCN = item.GCN
            '            .GCN_Lawan = item.GCN_Lawan
            '            .Active = 1
            '            .Business_date = item.Business_date
            '            If item.From_Type.HasValue Then
            '                .From_Type = item.From_Type
            '            End If
            '            If item.To_Type.HasValue Then
            '                .To_Type = item.To_Type
            '            End If
            '        End With

            '        objSARMemoClass.listTransaction.Add(objSARTransaksi)

            '        'objODMTransaksi = New goAML_ODM_Transaksi
            '        objSARTransaksi = New OneFCC_CaseManagement_SARMemo_Transaction
            '    Next
            '    '' End 23-Feb-2022

            '    If ListTransaction.Count = 0 Then
            '        Throw New ApplicationException("Tidak Ada Transaksi")
            '    End If
            '    If getIsGenerateSAR = "1" Then
            '        ListODMTransactionIncomplete = ListTransaction.Where(Function(x) x.BiMultiParty = 1 And x.CIF_No_Lawan Is Nothing And x.WIC_No_Lawan Is Nothing And x.ACCOUNT_No_Lawan Is Nothing).ToList
            '        If ListODMTransactionIncomplete.Count > 0 Then
            '            Throw New ApplicationException("Any Counter Party of Transaction is not completed")
            '        End If
            '    End If

            '    ListODMTransactionNoTrnMode = ListTransaction.Where(Function(x) x.Transmode_Code = "" Or x.Transmode_Code Is Nothing).ToList
            '    If ListODMTransactionNoTrnMode.Count > 0 Then
            '        Throw New ApplicationException("Cara Tranaksi Dilakukan Kosong Di " + ListODMTransactionNoTrnMode.Count.ToString + " Transaksi")
            '    End If
            'End If

            '' Edit 16-Feb-2022, Untuk Case Alert, Tanpa Approval

            'Dim DateFrom As DateTime = sar_DateFrom.Value
            'Dim DateTo As DateTime = sar_DateTo.Value
            ''SaveGenerateSTRSarTanpaApproval(ObjSARData, ObjModule, 1, DateFrom, DateTo)

            Dim paramm(0) As SqlParameter

            paramm(0) = New SqlParameter
            paramm(0).ParameterName = "@CIF"
            paramm(0).Value = objSARMemoClass.ObjSARMemo.CIF
            paramm(0).SqlDbType = SqlDbType.VarChar

            Dim FkWorkflow As String = ""
            If IsCustomer.Value Then
                FkWorkflow = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CaseManagement_SARMemo_GetWorkflow", paramm)
            Else
                FkWorkflow = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select parametervalue from onefcc_casemanagement_parameter where pk_globalreportparameter_id = 25", Nothing)
            End If

            objSARMemoClass.ObjSARMemo.FK_CaseManagement_Workflow_SARMemo_ID = FkWorkflow
            SARMemo_BLL.SaveAddTanpaApproval(objSARMemoClass, ObjModule)
            Dim StrQueryUpdateHasilAnalisis As String = "Update OneFCC_CaseManagement_SARMemo "
            StrQueryUpdateHasilAnalisis = StrQueryUpdateHasilAnalisis + " set Hasil_Analisis = case when right(Hasil_Analisis,2) = '??' then Substring(Hasil_Analisis,0,len(Hasil_Analisis))"
            StrQueryUpdateHasilAnalisis = StrQueryUpdateHasilAnalisis + " when right(Hasil_Analisis,2) like '%?' then Substring(Hasil_Analisis,0,len(Hasil_Analisis)) else Hasil_Analisis END"
            StrQueryUpdateHasilAnalisis = StrQueryUpdateHasilAnalisis + " From OneFCC_CaseManagement_SARMemo"
            StrQueryUpdateHasilAnalisis = StrQueryUpdateHasilAnalisis + " where PK_OneFCC_CaseManagement_SARMemo_ID =" & objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryUpdateHasilAnalisis, Nothing)
            'SARMemo_BLL.SaveAddTanpaApproval(objSARMemoClass, ObjModule)
            Panelconfirmation.Hidden = False
            fpMain.Hidden = True
            LblConfirmation.Text = "Data Saved into Pending Approval"
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SARMemo_Add_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Insert
    End Sub

    Private Sub SARMemo_Add_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleAdd = New NawaBLL.FormModuleAdd(fpMain, Panelconfirmation, LblConfirmation)
    End Sub

#Region "Indikator"

    Public Property IDIndikator() As Long
        Get
            Return Session("SARMemo_Add.IDIndikator")
        End Get
        Set(ByVal value As Long)
            Session("SARMemo_Add.IDIndikator") = value
        End Set
    End Property

    Protected Sub Clean_Window_Indikator()
        'Clean fields
        cmb_indikator.SetTextValue("")
        'Show Buttons
        btn_indikator_Save.Hidden = False
    End Sub

    Public Property obj_Indikator_Edit() As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator
        Get
            Return Session("SARMemo_Add.obj_Indikator_Edit")
        End Get
        Set(ByVal value As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator)
            Session("SARMemo_Add.obj_Indikator_Edit") = value
        End Set
    End Property


    Public Property obj_ListIndikator_Edit() As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator)
        Get
            Return Session("SARMemo_Add.obj_ListIndikator_Edit")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator))
            Session("SARMemo_Add.obj_ListIndikator_Edit") = value
        End Set
    End Property

    Protected Sub btn_Indikator_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            obj_Indikator_Edit = Nothing
            Clean_Window_Indikator()

            'Show window pop up
            cmb_indikator.IsReadOnly = False
            btn_indikator_Save.Hidden = False '' Added on 12 Aug 2021
            cmb_indikator.StringFieldStyle = "background-color:#FFE4C4"
            Window_Indikator.Title = "Indikator - Add"
            Window_Indikator.Hidden = False
            Window_Indikator.StyleSpec = "Width : 400px;Height : 400px;"

            'Set IDIndikator to 0
            IDIndikator = 0

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_indikator(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListIndikator_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_IndikatorID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListIndikator_Edit.Remove(objToDelete)
                    End If
                    Bind_Indikator()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Indikator_Edit = obj_ListIndikator_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_IndikatorID = strID)
                    Load_Window_Indikator(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Indikator_Cancel_Click()
        Try
            'Hide window pop up
            Window_Indikator.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Indikator_Save_Click()
        Try
            'Validate input

            If String.IsNullOrEmpty(cmb_indikator.SelectedItemValue) Then
                Throw New ApplicationException(cmb_indikator.Label & " harus diisi.")
            End If

            'Action save here
            Dim intPK As Long = -1

            If obj_Indikator_Edit Is Nothing Then  'Add
                Dim objAdd As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator
                If obj_ListIndikator_Edit IsNot Nothing Then
                    If obj_ListIndikator_Edit.Count > 0 Then
                        intPK = obj_ListIndikator_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_IndikatorID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDIndikator = intPK

                With objAdd
                    .PK_OneFCC_CaseManagement_SARMemo_IndikatorID = intPK

                    If Not String.IsNullOrEmpty(cmb_indikator.SelectedItemValue) Then
                        .FK_Indicator = cmb_indikator.SelectedItemValue
                    Else
                        .FK_Indicator = Nothing
                    End If


                End With
                If obj_ListIndikator_Edit Is Nothing Then
                    obj_ListIndikator_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Indikator)
                End If
                obj_ListIndikator_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListIndikator_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_IndikatorID = obj_Indikator_Edit.PK_OneFCC_CaseManagement_SARMemo_IndikatorID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDIndikator = .PK_OneFCC_CaseManagement_SARMemo_IndikatorID

                        If Not String.IsNullOrEmpty(cmb_indikator.SelectedItemValue) Then
                            .FK_Indicator = cmb_indikator.SelectedItemValue
                        Else
                            .FK_Indicator = Nothing
                        End If

                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_Indikator()

            'Hide window popup
            Window_Indikator.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_Indikator()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListIndikator_Edit)

        objtable.Columns.Add(New DataColumn("Kode", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        For Each row As DataRow In objtable.Rows
            row("Kode") = row("FK_Indicator")
            If Not IsDBNull(row("Kode")) Then
                Dim StrQueryCode As String = "select Keterangan from goAML_Ref_Indikator_Laporan where Kode = '" & row("Kode").ToString & "'"
                Dim Keterangan As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                row("Keterangan") = Keterangan
            Else
                row("Keterangan") = Nothing
            End If
        Next
        gp_indikator.GetStore().DataSource = objtable
        gp_indikator.GetStore().DataBind()


    End Sub

    Protected Sub Load_Window_Indikator(strAction As String)
        'Clean window pop up
        Clean_Window_Indikator()

        If obj_Indikator_Edit IsNot Nothing Then
            'Populate fields
            With obj_Indikator_Edit
                If .FK_Indicator IsNot Nothing Then
                    Using objdb As New NawaDatadevEntities
                        Dim objKode As goAML_Ref_Indikator_Laporan = objdb.goAML_Ref_Indikator_Laporan.Where(Function(X) X.Kode = .FK_Indicator).FirstOrDefault
                        If Not objKode Is Nothing Then
                            cmb_indikator.SetTextWithTextValue(obj_Indikator_Edit.FK_Indicator, objKode.Keterangan)
                        End If
                    End Using

                End If

            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            cmb_indikator.IsReadOnly = False
            btn_indikator_Save.Hidden = False '' Added on 12 Aug 2021
        Else
            cmb_indikator.IsReadOnly = True

            btn_indikator_Save.Hidden = True '' Added on 12 Aug 2021
        End If
        cmb_indikator.StringFieldStyle = "background-color:#FFE4C4"
        'Bind Indikator
        IDIndikator = obj_Indikator_Edit.PK_OneFCC_CaseManagement_SARMemo_IndikatorID

        'Show window pop up
        Window_Indikator.Title = "Indikator - " & strAction
        Window_Indikator.Hidden = False
    End Sub


#End Region

#Region "Pihak Terkait"

    Public Property IDPihakTerkait() As Long
        Get
            Return Session("SARMemo_Add.IDPihakTerkait")
        End Get
        Set(ByVal value As Long)
            Session("SARMemo_Add.IDPihakTerkait") = value
        End Set
    End Property

    Protected Sub Clean_Window_PihakTerkait()
        'Clean fields
        txtPT_CIF.Value = Nothing
        txtPT_HubunganPihakTerkait.Value = Nothing
        txtPT_RefGrips.Value = Nothing
        txtPT_TelahDilaporkan.Value = Nothing

        cmbPT_CIF.SetTextValue("")
        'Show Buttons
        btn_PihakTerkait_Save.Hidden = False

    End Sub


    Public Property obj_PihakTerkait_Edit() As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait
        Get
            Return Session("SARMemo_Add.obj_PihakTerkait_Edit")
        End Get
        Set(ByVal value As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait)
            Session("SARMemo_Add.obj_PihakTerkait_Edit") = value
        End Set
    End Property

    Public Property obj_ListPihakTerkait_Edit() As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait)
        Get
            Return Session("SARMemo_Add.obj_ListPihakTerkait_Edit")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait))
            Session("SARMemo_Add.obj_ListPihakTerkait_Edit") = value
        End Set
    End Property

    Protected Sub btn_Pihak_Terkait_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            obj_PihakTerkait_Edit = Nothing
            Clean_Window_PihakTerkait()

            'Show window pop up
            txtPT_CIF.ReadOnly = False
            cmbPT_CIF.IsReadOnly = False
            txtPT_HubunganPihakTerkait.ReadOnly = False
            txtPT_RefGrips.ReadOnly = False
            txtPT_TelahDilaporkan.ReadOnly = False
            cmbPT_CIF.StringFieldStyle = "background-color:#FFE4C4"
            btn_PihakTerkait_Save.Hidden = False '' Added on 12 Aug 2021
            Window_PihakTerkair.Title = "Pihak Terkait - Add"
            Window_PihakTerkair.Hidden = False
            txtPT_CIF.Value = Session("SARMemo_CIFNO")
            'Set IDIndikator to 0
            IDPihakTerkait = 0


        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_PihakTerkait(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListPihakTerkait_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListPihakTerkait_Edit.Remove(objToDelete)
                    End If
                    Bind_Pihak_Terkait()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_PihakTerkait_Edit = obj_ListPihakTerkait_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID = strID)
                    Load_Window_PihakTerkait(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_PihakTerkait_Cancel_Click()
        Try
            'Hide window pop up
            Window_PihakTerkair.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_PihakTerkait_Save_Click()
        Try
            'Validate input


            If String.IsNullOrWhiteSpace(cmbPT_CIF.SelectedItemValue) Then
                Throw New ApplicationException(cmbPT_CIF.Label & " is required.")
            End If
            If String.IsNullOrEmpty(txtPT_HubunganPihakTerkait.Value) Then
                Throw New ApplicationException(txtPT_HubunganPihakTerkait.FieldLabel & " harus diisi.")
            End If
            'If String.IsNullOrEmpty(txtPT_RefGrips.Value) Then
            '    Throw New ApplicationException(txtPT_RefGrips.FieldLabel & " harus diisi.")
            'End If
            'If String.IsNullOrEmpty(txtPT_TelahDilaporkan.Value) Then
            '    Throw New ApplicationException(txtPT_TelahDilaporkan.FieldLabel & " harus diisi.")
            'End If



            'Action save here
            Dim intPK As Long = -1

            If obj_PihakTerkait_Edit Is Nothing Then  'Add
                Dim objAdd As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait
                If obj_ListPihakTerkait_Edit IsNot Nothing Then
                    If obj_ListPihakTerkait_Edit.Count > 0 Then
                        intPK = obj_ListPihakTerkait_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDPihakTerkait = intPK

                With objAdd
                    .PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID = intPK

                    If Not String.IsNullOrEmpty(cmbPT_CIF.SelectedItemValue) Then
                        .CIF = cmbPT_CIF.SelectedItemValue
                        Dim Name As String = ""
                        Dim StrQueryCode As String = "select case when fk_customer_type_id = 1 then indv_last_name else corp_name end as indv_last_name from goaml_Ref_customer where cif = '" & cmbPT_CIF.SelectedItemValue & "'"
                        Name = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                        .Nama_PihakTerkait = Name
                    Else
                        .CIF = Nothing
                    End If

                    If Not String.IsNullOrEmpty(txtPT_HubunganPihakTerkait.Value) Then
                        .Hubungan_PihakTerkait = txtPT_HubunganPihakTerkait.Value
                    Else
                        .Hubungan_PihakTerkait = Nothing
                    End If

                    If Not String.IsNullOrEmpty(txtPT_RefGrips.Value) Then
                        .RefGrips_PihakTerkait = txtPT_RefGrips.Value
                    Else
                        .RefGrips_PihakTerkait = Nothing
                    End If

                    If Not String.IsNullOrEmpty(txtPT_TelahDilaporkan.Value) Then
                        .Dilaporkan_PihakTerkait = txtPT_TelahDilaporkan.Value
                    Else
                        .Dilaporkan_PihakTerkait = Nothing
                    End If

                End With
                If obj_ListPihakTerkait_Edit Is Nothing Then
                    obj_ListPihakTerkait_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Pihak_Terkait)
                End If
                obj_ListPihakTerkait_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListPihakTerkait_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID = obj_PihakTerkait_Edit.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDPihakTerkait = .PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID

                        If Not String.IsNullOrEmpty(cmbPT_CIF.SelectedItemValue) Then
                            .CIF = cmbPT_CIF.SelectedItemValue
                            Dim Name As String = ""
                            Dim StrQueryCode As String = "select case when fk_customer_type_id = 1 then indv_last_name else corp_name end as indv_last_name from goaml_Ref_customer where cif = '" & cmbPT_CIF.SelectedItemValue & "'"
                            Name = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                            .Nama_PihakTerkait = Name
                        Else
                            .CIF = Nothing
                        End If



                        If Not String.IsNullOrEmpty(txtPT_HubunganPihakTerkait.Value) Then
                            .Hubungan_PihakTerkait = txtPT_HubunganPihakTerkait.Value
                        Else
                            .Hubungan_PihakTerkait = Nothing
                        End If

                        If Not String.IsNullOrEmpty(txtPT_RefGrips.Value) Then
                            .RefGrips_PihakTerkait = txtPT_RefGrips.Value
                        Else
                            .RefGrips_PihakTerkait = Nothing
                        End If

                        If Not String.IsNullOrEmpty(txtPT_TelahDilaporkan.Value) Then
                            .Dilaporkan_PihakTerkait = txtPT_TelahDilaporkan.Value
                        Else
                            .Dilaporkan_PihakTerkait = Nothing
                        End If

                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_Pihak_Terkait()

            'Hide window popup
            Window_PihakTerkair.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_Pihak_Terkait()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListPihakTerkait_Edit)
        For Each row As DataRow In objtable.Rows
            If Not IsDBNull(row("CIF")) Then
                Dim CIF As String = ""
                Dim StrQueryCode As String = "select case when fk_customer_type_id = 1 then indv_last_name else corp_name end as indv_last_name from goaml_Ref_customer where cif = '" & row("CIF").ToString & "'"
                CIF = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                row("Nama_PihakTerkait") = CIF
            Else
                row("Nama_PihakTerkait") = Nothing
            End If
        Next

        gp_Pihak_Terkait.GetStore().DataSource = objtable
        gp_Pihak_Terkait.GetStore().DataBind()


    End Sub

    Protected Sub Load_Window_PihakTerkait(strAction As String)
        'Clean window pop up
        Clean_Window_PihakTerkait()

        If obj_PihakTerkait_Edit IsNot Nothing Then
            'Populate fields
            With obj_PihakTerkait_Edit
                If Not String.IsNullOrEmpty(.CIF) Then
                    Dim strfieldtypestring As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT Keterangan FROM vw_goAML_Ref_Customer WHERE kode='" & .CIF & "'", Nothing)
                    cmbPT_CIF.SetTextWithTextValue(.CIF, strfieldtypestring)
                Else
                    cmbPT_CIF.SetTextValue("")
                End If

                If Not String.IsNullOrEmpty(.Hubungan_PihakTerkait) Then
                    txtPT_HubunganPihakTerkait.Value = .Hubungan_PihakTerkait
                Else
                    txtPT_HubunganPihakTerkait.Value = Nothing
                End If

                If Not String.IsNullOrEmpty(.RefGrips_PihakTerkait) Then
                    txtPT_RefGrips.Value = .RefGrips_PihakTerkait
                Else
                    txtPT_RefGrips.Value = Nothing
                End If

                If Not String.IsNullOrEmpty(.Dilaporkan_PihakTerkait) Then
                    txtPT_TelahDilaporkan.Value = .Dilaporkan_PihakTerkait
                Else
                    txtPT_TelahDilaporkan.Value = Nothing
                End If

            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            txtPT_CIF.ReadOnly = False
            cmbPT_CIF.IsReadOnly = False
            txtPT_HubunganPihakTerkait.ReadOnly = False
            txtPT_RefGrips.ReadOnly = False
            txtPT_TelahDilaporkan.ReadOnly = False
            btn_PihakTerkait_Save.Hidden = False '' Added on 12 Aug 2021
        Else
            cmbPT_CIF.IsReadOnly = True
            txtPT_CIF.ReadOnly = True
            txtPT_HubunganPihakTerkait.ReadOnly = True
            txtPT_RefGrips.ReadOnly = True
            txtPT_TelahDilaporkan.ReadOnly = True
            btn_PihakTerkait_Save.Hidden = True '' Added on 12 Aug 2021
        End If

        'Bind Indikator
        IDPihakTerkait = obj_PihakTerkait_Edit.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID

        'Show window pop up
        Window_PihakTerkair.Title = "Pihak Terkait - " & strAction
        Window_PihakTerkair.Hidden = False
    End Sub
#End Region

#Region "Tindak Lanjut"

    Public Property IDTindakLanjut() As Long
        Get
            Return Session("SARMemo_Add.IDTindakLanjut")
        End Get
        Set(ByVal value As Long)
            Session("SARMemo_Add.IDTindakLanjut") = value
        End Set
    End Property

    Protected Sub Clean_Window_TindakLanjut()
        'Clean fields
        cmb_tindaklanjut.SetTextValue("")
        'Show Buttons
        btn_TindakLanjut_Save.Hidden = False
    End Sub

    Public Property obj_TindakLanjut_Edit() As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut
        Get
            Return Session("SARMemo_Add.obj_TindakLanjut_Edit")
        End Get
        Set(ByVal value As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut)
            Session("SARMemo_Add.obj_TindakLanjut_Edit") = value
        End Set
    End Property


    Public Property obj_ListTindakLanjut_Edit() As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut)
        Get
            Return Session("SARMemo_Add.obj_ListTindakLanjut_Edit")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut))
            Session("SARMemo_Add.obj_ListTindakLanjut_Edit") = value
        End Set
    End Property

    Protected Sub btn_Tindak_Lanjut_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            obj_TindakLanjut_Edit = Nothing
            Clean_Window_TindakLanjut()

            'Show window pop up
            cmb_tindaklanjut.IsReadOnly = False
            btn_TindakLanjut_Save.Hidden = False '' Added on 12 Aug 2021
            cmb_tindaklanjut.StringFieldStyle = "background-color:#FFE4C4"
            window_tindaklanjut.Title = "Tindak Lanjut - Add"
            window_tindaklanjut.Hidden = False

            'Set IDIndikator to 0
            IDTindakLanjut = 0

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_TindakLanjut(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListTindakLanjut_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListTindakLanjut_Edit.Remove(objToDelete)
                    End If
                    Bind_TindakLanjut()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_TindakLanjut_Edit = obj_ListTindakLanjut_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID = strID)
                    Load_Window_TindakLanjut(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_TindakLanjut_Cancel_Click()
        Try
            'Hide window pop up
            window_tindaklanjut.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_TindakLanjut_Save_Click()
        Try
            'Validate input

            If String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemValue) Then
                Throw New ApplicationException(cmb_tindaklanjut.Label & " harus diisi.")
            End If


            'Action save here
            Dim intPK As Long = -1

            If obj_TindakLanjut_Edit Is Nothing Then  'Add
                Dim objAdd As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut
                If obj_ListTindakLanjut_Edit IsNot Nothing Then
                    If obj_ListTindakLanjut_Edit.Count > 0 Then
                        intPK = obj_ListTindakLanjut_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDTindakLanjut = intPK

                With objAdd
                    .PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID = intPK
                    If Not String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemValue) Then
                        .FK_OneFCC_CaseManagement_TindakLanjut_ID = cmb_tindaklanjut.SelectedItemValue
                    Else
                        .FK_OneFCC_CaseManagement_TindakLanjut_ID = Nothing
                    End If
                    If Not String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemText) Then
                        .TindakLanjutDescription = cmb_tindaklanjut.SelectedItemText
                    Else
                        .TindakLanjutDescription = Nothing
                    End If

                End With
                If obj_ListTindakLanjut_Edit Is Nothing Then
                    obj_ListTindakLanjut_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_TindakLanjut)
                End If
                obj_ListTindakLanjut_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListTindakLanjut_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID = obj_TindakLanjut_Edit.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDTindakLanjut = .PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID

                        If Not String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemValue) Then
                            .FK_OneFCC_CaseManagement_TindakLanjut_ID = cmb_tindaklanjut.SelectedItemValue
                        Else
                            .FK_OneFCC_CaseManagement_TindakLanjut_ID = Nothing
                        End If
                        If Not String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemText) Then
                            .TindakLanjutDescription = cmb_tindaklanjut.SelectedItemText
                        Else
                            .TindakLanjutDescription = Nothing
                        End If

                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_TindakLanjut()

            'Hide window popup
            window_tindaklanjut.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_TindakLanjut()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListTindakLanjut_Edit)


        gp_TIndakLanjut.GetStore().DataSource = objtable
        gp_TIndakLanjut.GetStore().DataBind()


    End Sub

    Protected Sub Load_Window_TindakLanjut(strAction As String)
        'Clean window pop up
        Clean_Window_TindakLanjut()

        If obj_TindakLanjut_Edit IsNot Nothing Then
            'Populate fields
            With obj_TindakLanjut_Edit
                If .FK_OneFCC_CaseManagement_TindakLanjut_ID IsNot Nothing Then

                    cmb_tindaklanjut.SetTextWithTextValue(.FK_OneFCC_CaseManagement_TindakLanjut_ID, .TindakLanjutDescription)


                End If

            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            cmb_tindaklanjut.IsReadOnly = False
            btn_TindakLanjut_Save.Hidden = False '' Added on 12 Aug 2021
        Else
            cmb_tindaklanjut.IsReadOnly = True

            btn_TindakLanjut_Save.Hidden = True '' Added on 12 Aug 2021
        End If
        cmb_tindaklanjut.StringFieldStyle = "background-color:#FFE4C4"
        'Bind Indikator
        IDTindakLanjut = obj_TindakLanjut_Edit.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID

        'Show window pop up
        window_tindaklanjut.Title = "Tindak Lanjut - " & strAction
        window_tindaklanjut.Hidden = False
    End Sub
#End Region

#Region "Activity"

    Public Property ListActivity As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
        Get
            If Session("SARMemo_Add.ListActivity") Is Nothing Then
                Session("SARMemo_Add.ListActivity") = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
            End If
            Return Session("SARMemo_Add.ListActivity")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity))
            Session("SARMemo_Add.ListActivity") = value
        End Set
    End Property

    Public Property obj_ListActivity_Edit() As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
        Get
            Return Session("SARMemo_Add.obj_ListActivity_Edit")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity))
            Session("SARMemo_Add.obj_ListActivity_Edit") = value
        End Set
    End Property


    Public Property obj_Activity_Edit() As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity
        Get
            Return Session("SARMemo_Add.obj_Activity_Edit")
        End Get
        Set(ByVal value As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
            Session("SARMemo_Add.obj_Activity_Edit") = value
        End Set
    End Property
    Public Property IDDokumen As String
        Get
            Return Session("SARMemo_Add.IDDokumen")
        End Get
        Set(value As String)
            Session("SARMemo_Add.IDDokumen") = value
        End Set
    End Property
    Public Property ListSelectedActivity As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
        Get
            If Session("SARMemo_Add.ListSelectedActivity") Is Nothing Then
                Session("SARMemo_Add.ListSelectedActivity") = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
            End If
            Return Session("SARMemo_Add.ListSelectedActivity")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity))
            Session("SARMemo_Add.ListSelectedActivity") = value
        End Set
    End Property

    Sub bindActivityDataTable(store As Ext.Net.Store, dt As DataTable)
        'store.DataSource = New DataTable
        'Dim objtable As New DataTable()
        'Dim fields() As FieldInfo = GetType(SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity).GetFields()
        'For Each field As FieldInfo In fields
        '    objtable.Columns.Add(field.Name, field.FieldType)
        'Next
        'For Each item As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity In ListActivity
        '    Dim row As DataRow = objtable.NewRow()
        '    For Each field As FieldInfo In fields
        '        row(field.Name) = field.GetValue(item)
        '    Next
        '    objtable.Rows.Add(row)
        'Next
        Dim objtable As New Data.DataTable
        objtable = NawaBLL.Common.CopyGenericToDataTable(ListActivity)
        'Dim objtable As Data.DataTable = dt

        objtable.Columns.Add(New DataColumn("PK_Sementara", GetType(String)))


        'store.DataSource = objtable
        'store.DataBind()
        gp_activity.GetStore.DataSource = objtable
        gp_activity.GetStore.DataBind()

    End Sub

    Protected Sub Bind_Activity()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListActivity_Edit)


        gp_activity.GetStore.DataSource = objtable
        gp_activity.GetStore.DataBind()


    End Sub

    Protected Sub Clean_Window_Activity()
        'Clean fields
        txtComment.Value = Nothing
        txtSignificance.Value = Nothing
        txtDescription.Value = Nothing
        'Show Buttons
        btn_TindakLanjut_Save.Hidden = False
    End Sub


    Protected Sub Load_Window_Activity(strAction As String)
        'Clean window pop up
        Clean_Window_Activity()

        If obj_Activity_Edit IsNot Nothing Then
            'Populate fields
            With obj_Activity_Edit
                If .Reason IsNot Nothing Then
                    txtDescription.Value = .Reason
                Else
                    txtDescription.Value = Nothing
                End If
                If .Significance IsNot Nothing Then
                    txtSignificance.Value = .Significance
                Else
                    txtSignificance.Value = 0
                End If
                If .Comments IsNot Nothing Then
                    txtComment.Value = .Comments
                Else
                    txtComment.Value = Nothing
                End If

            End With
        End If
        If strAction = "Edit" Then
            txtSignificance.ReadOnly = False
            txtComment.ReadOnly = False
            txtDescription.ReadOnly = False
            btn_Activity_Save.Hidden = False
        Else
            txtSignificance.ReadOnly = True
            txtComment.ReadOnly = True
            txtDescription.ReadOnly = True
            btn_Activity_Save.Hidden = True
        End If


        'Show window pop up
        Window_Activity.Title = "Activity SAR - " & strAction
        Window_Activity.Hidden = False
    End Sub

    Protected Sub gc_Activity(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListActivity_Edit.Find(Function(x) x.Pk_OneFCC_CaseManagement_SARMemo_ActivityID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListActivity_Edit.Remove(objToDelete)
                    End If
                    Bind_Activity()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Activity_Edit = obj_ListActivity_Edit.Find(Function(x) x.Pk_OneFCC_CaseManagement_SARMemo_ActivityID = strID)
                    Load_Window_Activity(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Activity_Save_Click()
        Try
            'Validate input

            'If String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemValue) Then
            '    Throw New ApplicationException(cmb_tindaklanjut.Label & " harus diisi.")
            'End If


            'Action save here
            Dim intPK As Long = -1

            If obj_ListActivity_Edit Is Nothing Then  'Add
                Dim objAdd As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity
                If obj_ListActivity_Edit IsNot Nothing Or obj_ListActivity_Edit.Count = 0 Then
                    If obj_ListActivity_Edit.Count > 0 Then
                        intPK = obj_ListActivity_Edit.Min(Function(x) x.Pk_OneFCC_CaseManagement_SARMemo_ActivityID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDTindakLanjut = intPK

                With objAdd
                    .Pk_OneFCC_CaseManagement_SARMemo_ActivityID = intPK

                    If Not String.IsNullOrEmpty(txtSignificance.Value) Then
                        .Significance = Convert.ToInt32(txtSignificance.Value)
                    Else
                        .Significance = 0
                    End If

                    If Not String.IsNullOrEmpty(txtComment.Value) Then
                        .Comments = txtComment.Value
                    Else
                        .Comments = Nothing
                    End If
                    If Not String.IsNullOrEmpty(txtDescription.Value) Then
                        .Reason = txtDescription.Value
                    Else
                        .Reason = Nothing
                    End If

                End With
                If obj_ListActivity_Edit Is Nothing Then
                    obj_ListActivity_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
                End If
                obj_ListActivity_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListActivity_Edit.Find(Function(x) x.Pk_OneFCC_CaseManagement_SARMemo_ActivityID = obj_Activity_Edit.Pk_OneFCC_CaseManagement_SARMemo_ActivityID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDIndikator = .Pk_OneFCC_CaseManagement_SARMemo_ActivityID
                        If .Account_NO IsNot Nothing Then
                            .Account_NO = .Account_NO
                        End If
                        If Not String.IsNullOrEmpty(txtSignificance.Value) Then
                            .Significance = Convert.ToInt32(txtSignificance.Value)
                        Else
                            .Significance = 0
                        End If

                        If Not String.IsNullOrEmpty(txtComment.Value) Then
                            .Comments = txtComment.Value
                        Else
                            .Comments = Nothing
                        End If
                        If Not String.IsNullOrEmpty(txtDescription.Value) Then
                            .Reason = txtDescription.Value
                        Else
                            .Reason = Nothing
                        End If

                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_Activity()

            'Hide window popup
            Window_Activity.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Activity_Cancel_Click()
        Try
            'Hide window pop up
            Window_Activity.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnSearch_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If Session("FirstAddData") = 1 Then
                Session("FirstAddData") = 0
            Else
                'ClearActivity()
            End If
            obj_Activity_Edit = New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity
            obj_ListActivity_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
            ListActivity = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
            Dim SmCaseID As RowSelectionModel = TryCast(gp_case_alert.GetSelectionModel(), RowSelectionModel)
            Dim SelectedCaseID As RowSelectionModel = gp_case_alert.SelectionModel.Primary
            If SelectedCaseID.SelectedRows.Count = 0 Then

                Throw New Exception("Minimal 1 Case ID harus dipilih")
            Else
                Dim tbl_Activity As New DataTable
                If SelectedCaseID.SelectedRows.Count > 1 Then
                    Dim CIF As String = Session("SARMemo_CIFNO")
                    Dim WIC As String = Session("SARMemo_WICNO")
                    For Each item As SelectedRow In SmCaseID.SelectedRows
                        Dim recordID = item.RecordID.ToString
                        Dim tbl_activity_temp As New DataTable
                        If IsCustomer.Value Then
                            Dim StrQuery As String = "select topotrans.Account_NO, topotrans.Date_Activity, topotrans.Activity_Description, topotrans.Date_Activity from OneFCC_CaseManagement_Typology as topo " &
                      " join OneFCC_CaseManagement_Typology_NonTransaction As topotrans " &
                      " On topo.pk_OneFCC_CaseManagement_Typology_id = topotrans.fk_OneFCC_CaseManagement_Typology_id " &
                      " where topo.fk_casemanagement_id = " & recordID & " and topo.cif_no = '" & CIF & "'"
                            tbl_activity_temp = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQuery, Nothing)
                            tbl_Activity.Merge(tbl_activity_temp)
                        Else
                            Dim StrQuery As String = "select topotrans.Account_NO, topotrans.Date_Activity, topotrans.Activity_Description, topotrans.Date_Activity from OneFCC_CaseManagement_Typology as topo " &
                      " join OneFCC_CaseManagement_Typology_NonTransaction As topotrans " &
                      " On topo.pk_OneFCC_CaseManagement_Typology_id = topotrans.fk_OneFCC_CaseManagement_Typology_id " &
                      " where topo.fk_casemanagement_id = " & recordID & " and topo.wic_no = '" & WIC & "'"
                            tbl_activity_temp = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQuery, Nothing)
                            tbl_Activity.Merge(tbl_activity_temp)
                        End If

                    Next
                Else
                    For Each item As SelectedRow In SmCaseID.SelectedRows
                        Dim tbl_activity_temp As New DataTable
                        Dim recordID = item.RecordID.ToString
                        Dim StrQuery As String = "select topotrans.Account_NO, topotrans.Date_Activity, topotrans.Activity_Description, topotrans.Date_Activity from OneFCC_CaseManagement_Typology as topo " &
                        " join OneFCC_CaseManagement_Typology_NonTransaction As topotrans " &
                        " On topo.pk_OneFCC_CaseManagement_Typology_id = topotrans.fk_OneFCC_CaseManagement_Typology_id " &
                        " where topo.fk_casemanagement_id = " & recordID
                        tbl_activity_temp = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQuery, Nothing)
                        tbl_Activity.Merge(tbl_activity_temp)
                    Next

                End If
                'Action save here
                Dim intPK As Long = -1

                For Each row As DataRow In tbl_Activity.Rows
                    Dim Activity As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity = New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity

                    'Add
                    Dim objAdd As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity
                    If obj_ListActivity_Edit IsNot Nothing Or obj_ListActivity_Edit.Count = 0 Then
                        If obj_ListActivity_Edit.Count > 0 Then
                            intPK = obj_ListActivity_Edit.Min(Function(x) x.Pk_OneFCC_CaseManagement_SARMemo_ActivityID)
                            If intPK > 0 Then
                                intPK = -1
                            Else
                                intPK = intPK - 1
                            End If
                        End If

                    End If

                    IDIndikator = intPK

                    With objAdd
                        .Pk_OneFCC_CaseManagement_SARMemo_ActivityID = intPK

                        .Significance = 0
                        .Comments = ""
                        If Not IsDBNull(row.Item("Activity_Description")) Then
                            .Reason = row.Item("Activity_Description")
                        Else
                            .Reason = Nothing
                        End If
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedDate = DateTime.Now
                        .Date_Activity = row.Item("Date_Activity")
                    End With
                    If obj_ListActivity_Edit Is Nothing Then
                        obj_ListActivity_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity)
                    End If
                    obj_ListActivity_Edit.Add(objAdd)

                    If IsCustomer.Value Then
                        If Not IsDBNull(row.Item("Account_NO")) Then
                            Activity.Account_NO = row.Item("Account_NO")
                        End If
                    Else
                        Activity.Account_NO = Nothing
                    End If
                    Activity.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    Activity.CreatedDate = DateTime.Now
                    Activity.Date_Activity = row.Item("Date_Activity")
                    Activity.Reason = row.Item("Activity_Description")
                    Activity.Significance = 0
                    Activity.Comments = ""
                    Activity.Pk_OneFCC_CaseManagement_SARMemo_ActivityID = intPK
                    'Dim CekActivity As Integer = 0
                    'CekActivity = ListActivity.FindAll(Function(item As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Activity) item.Account_NO = Activity.Activity_Number).Count
                    'If CekActivity = 0 Then
                    '    ListActivity.Add(Activity)
                    'End If
                    ListActivity.Add(Activity)
                Next
                obj_ListActivity_Edit = ListActivity
                If tbl_Activity.Rows.Count > 0 Then

                    bindActivityDataTable(StoreActivity, tbl_Activity)

                End If
                If tbl_Activity.Rows.Count = 0 Then
                    Ext.Net.X.Msg.Alert("Message", "Tidak Ada Activity").Show()
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub


    Private Shared Sub DataSetToExcel(ByVal dataSet As DataSet, ByVal objfileinfo As FileInfo)
        Using pck As ExcelPackage = New ExcelPackage()

            For Each dataTable As DataTable In dataSet.Tables
                Dim workSheet As ExcelWorksheet = pck.Workbook.Worksheets.Add(dataTable.TableName)
                workSheet.Cells("A1").LoadFromDataTable(dataTable, True)

                Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                Dim intcolnumber As Integer = 1
                For Each item As System.Data.DataColumn In dataTable.Columns
                    If item.DataType = GetType(Date) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = dateformat
                    End If
                    If item.DataType = GetType(Integer) Or item.DataType = GetType(Double) Or item.DataType = GetType(Long) Or item.DataType = GetType(Decimal) Or item.DataType = GetType(Single) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = "#,##0.00"
                    End If

                    intcolnumber = intcolnumber + 1
                Next
                workSheet.Cells(workSheet.Dimension.Address).AutoFitColumns()
            Next

            pck.SaveAs(objfileinfo)
        End Using
    End Sub



    Protected Sub CreateExcel2007WithData(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try


            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            'Dim intTotalRow As Long
            Dim objSchemaModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(12226)
            Dim objSchemaModuleField As List(Of NawaDAL.ModuleField) = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(12226)
            Dim objSchemaModuleFieldDefault As New List(Of NawaDAL.ModuleFieldDefault)
            Dim objmdl As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(12226)
            Dim CIF As String = Session("SARMemo_CIFNO")
            Dim WIC As String = Session("SARMemo_WICNO")
            Dim objList As New DataTable
            Dim SmCaseID As RowSelectionModel = TryCast(gp_case_alert.GetSelectionModel(), RowSelectionModel)
            For Each item As SelectedRow In SmCaseID.SelectedRows
                Dim recordID = item.RecordID.ToString
                Dim tbl_activity_temp As New DataTable
                If IsCustomer.Value Then
                    Dim StrQuery As String = "select * from OneFCC_CaseManagement_Typology as topo " &
                      " join OneFCC_CaseManagement_Typology_NonTransaction As topotrans " &
                      " On topo.pk_OneFCC_CaseManagement_Typology_id = topotrans.fk_OneFCC_CaseManagement_Typology_id " &
                      " where topo.fk_casemanagement_id = " & recordID & " and topo.cif_no = '" & CIF & "'"
                    tbl_activity_temp = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQuery, Nothing)
                    objList.Merge(tbl_activity_temp)
                Else
                    Dim StrQuery As String = "select * from OneFCC_CaseManagement_Typology as topo " &
                      " join OneFCC_CaseManagement_Typology_NonTransaction As topotrans " &
                      " On topo.pk_OneFCC_CaseManagement_Typology_id = topotrans.fk_OneFCC_CaseManagement_Typology_id " &
                      " where topo.fk_casemanagement_id = " & recordID & " and topo.wic_no = '" & WIC & "'"
                    tbl_activity_temp = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQuery, Nothing)
                    objList.Merge(tbl_activity_temp)
                End If

            Next




            objFormModuleView = New FormModuleView(gp_activity, BtnExport)
            objFormModuleView.ModuleID = objmdl.PK_Module_ID
            objFormModuleView.ModuleName = objmdl.ModuleName

            Dim delimitercheckboxparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(39)
            Dim strdelimiter As String = ";"
            If Not delimitercheckboxparam Is Nothing Then
                strdelimiter = delimitercheckboxparam.SettingValue
            End If

            Dim objtb As DataTable = NawaBLL.Common.CopyGenericToDataTable(ListActivity)
            Dim intmaxrow As Integer
            If objtb.Rows.Count + 1000 > 1048575 Then
                intmaxrow = 1048575
            Else
                intmaxrow = objtb.Rows.Count + 1000
            End If


            Using objtbl As Data.DataTable = objtb
                'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                objtbl.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                Dim intTotalPage As Integer = (intmaxrow + 1048575 - 1) / 1048575
                Dim dsTable As New Data.DataSet

                If intTotalPage = 1 Then
                    'objFormModuleView.changeHeader(objtbl)

                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In gp_activity.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    DataSetToExcel(dsTable, objfileinfo)

                    dsTable.Dispose()
                    dsTable = Nothing



                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If



                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")



                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()
                ElseIf intTotalPage > 1 Then
                    'objFormModuleView.changeHeader(objtbl)



                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In gp_activity.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    'ambil page 2 dst sampe page terakhir
                    For index = 1 To intTotalPage - 2
                        Using objtbltemp As Data.DataTable = objtb
                            objtbltemp.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                            dsTable.Tables.Add(objtbltemp.Copy)
                        End Using
                    Next
                    DataSetToExcel(dsTable, objfileinfo)
                    dsTable.Dispose()
                    dsTable = Nothing

                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If
                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()

                End If
            End Using

        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub

    Protected Sub sar_jenisLaporan_DirectSelect(sender As Object, e As DirectEventArgs)
        Try
            'If sar_jenisLaporan.SelectedItem.Value = "LTKMT" Or sar_jenisLaporan.SelectedItem.Value = "LAPT" Or sar_jenisLaporan.SelectedItem.Value = "LTKM" Then
            If sar_jenisLaporan.SelectedItem.Value IsNot Nothing Then
                'PanelReportIndicator.Hidden = False
            Else
                'PanelReportIndicator.Hidden = True
            End If
            If sar_jenisLaporan.SelectedItem.Value = "LTKMP" Then
                NoRefPPATK.Hidden = False
            Else
                NoRefPPATK.Hidden = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Property obj_Attachment_Edit() As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment
        Get
            Return Session("SARMemo_Add.obj_Attachment_Edit")
        End Get
        Set(ByVal value As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment)
            Session("SARMemo_Add.obj_Attachment_Edit") = value
        End Set
    End Property

    Public Property obj_ListAttachment_Edit() As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment)
        Get
            Return Session("SARMemo_Add.obj_ListAttachment_Edit")
        End Get
        Set(ByVal value As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment))
            Session("SARMemo_Add.obj_ListAttachment_Edit") = value
        End Set
    End Property

    Protected Sub btn_addAttachment_click(sender As Object, e As DirectEventArgs)
        Try
            obj_Attachment_Edit = Nothing
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()
            WindowAttachment.Hidden = False
            FileDoc.Hidden = False
            BtnsaveAttachment.Hidden = False
            txtKeterangan.Editable = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                obj_ListAttachment_Edit.Remove(obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = ID).FirstOrDefault)
                bindAttachment(StoreAttachment, obj_ListAttachment_Edit)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                editDokumen(ID, "Edit")
                IDDokumen = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editDokumen(ID, "Detail")
            ElseIf e.ExtraParams(1).Value = "Download" Then
                DownloadFile(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim status As Boolean = False
            Dim DokumenOld As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment
            Dim Dokumen As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment
            If IDDokumen IsNot Nothing Or IDDokumen <> "" Then
                status = True
                DokumenOld = obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = IDDokumen).FirstOrDefault
                obj_ListAttachment_Edit.Remove(obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = IDDokumen).FirstOrDefault)
            ElseIf Not FileDoc.HasFile Then
                Throw New Exception("Please Upload File")
            End If

            Dim docName As String = IO.Path.GetFileName(FileDoc.FileName)
            If obj_ListAttachment_Edit IsNot Nothing Then
                For Each item In obj_ListAttachment_Edit
                    If item.File_Name = docName Then
                        Throw New ApplicationException(docName + " Sudah Ada")
                    End If
                Next
            Else
                obj_ListAttachment_Edit = New List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment)
            End If


            If obj_ListAttachment_Edit.Count = 0 Then
                Dokumen.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = -1
            ElseIf obj_ListAttachment_Edit.Count >= 1 Then
                Dokumen.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = obj_ListAttachment_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID) - 1
            End If

            If status = True And Not FileDoc.HasFile Then
                Dokumen.File_Name = DokumenOld.File_Name
                Dokumen.File_Doc = DokumenOld.File_Doc
            Else
                Dokumen.File_Name = IO.Path.GetFileName(FileDoc.FileName)
                Dokumen.File_Doc = FileDoc.FileBytes
            End If
            Dokumen.Remarks = txtKeterangan.Text.Trim

            Dokumen.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            Dokumen.CreatedDate = DateTime.Now

            obj_ListAttachment_Edit.Add(Dokumen)

            bindAttachment(StoreAttachment, obj_ListAttachment_Edit)
            WindowAttachment.Hidden = True
            IDDokumen = Nothing

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAttachment.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub bindAttachment(store As Ext.Net.Store, listAttachment As List(Of SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAttachment)
        objtable.Columns.Add(New DataColumn("FileName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("FileName") = item("File_Name")
                item("Keterangan") = item("Remarks")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub editDokumen(id As String, command As String)
        Try
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()

            WindowAttachment.Hidden = False
            Dim Dokumen As New SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment
            Dokumen = obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = id).FirstOrDefault

            txtFileName.Text = Dokumen.File_Name
            txtKeterangan.Text = Dokumen.Remarks

            If command = "Detail" Then
                FileDoc.Hidden = True
                BtnsaveAttachment.Hidden = True
                txtKeterangan.Editable = False
            Else
                FileDoc.Hidden = False
                BtnsaveAttachment.Hidden = False
                txtKeterangan.Editable = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DownloadFile(id As Long)
        Dim objdownload As SARMemo_BLL.OneFCC_CaseManagement_SARMemo_Attachment = obj_ListAttachment_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = id)
        If Not objdownload Is Nothing Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & objdownload.File_Name)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            'Response.ContentType = "ContentType"
            Response.ContentType = MimeMapping.GetMimeMapping(objdownload.File_Name)
            Response.BinaryWrite(objdownload.File_Doc)
            Response.End()
        End If
    End Sub

    Protected Sub ReportIndicator_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goaml_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub JenisLaporan_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and TRNorACT = 'ACT'"
            Else
                strfilter += "TRNorACT = 'ACT'"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Jenis_Laporan_STR", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
#End Region
    Protected Sub cmb_Cifno_OnValueChanged(sender As Object, e As EventArgs)
        Try
            Session("SARMemo_CIFNO") = cmb_Cifno.SelectedItemValue

        Catch ex As Exception

        End Try
    End Sub




    Protected Sub cmb_WICno_OnValueChanged(sender As Object, e As EventArgs)
        Try
            Session("SARMemo_WICNO") = cmb_WICno.SelectedItemValue
        Catch ex As Exception

        End Try
    End Sub
End Class
