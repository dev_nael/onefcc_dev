﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="SARMemo_Add.aspx.vb" Inherits="AML_CaseManagement_SARMemo_SARMemo_Add" %>

<%--Begin Update Penambahan Advance Filter--%>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>
<%--End Update Penambahan Advance Filter--%>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        var onKeyUp = function (combo, e) {
            var v = combo.getRawValue();
            combo.store.filter(combo.displayField, new RegExp(v, "i"));
            combo.onLoad();
        };

        var onInitialize = function (editor) {

            var styles = {
                "background-image": "none",
                "background-color": "#FFE4C4"
            };
            Ext.DomHelper.applyStyles(editor.getEditorBody(), styles);
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
       <style type="text/css">
        #ContentPlaceHolder1_txtLamaMenjadiNasabah { width : 500px !important; }
       #ContentPlaceHolder1_txtUnitBisnis { width : 350px !important; }
       #ContentPlaceHolder1_txtDirektor { width : 200px !important; }
       #ContentPlaceHolder1_txtCabang { width : 200px !important; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel runat="server" ID="fpMain" BodyPadding="10" ButtonAlign="Center" Scrollable="Both" Title="SAR Memo Add">
        <Items>
             <ext:Checkbox ID="IsCustomer" runat="server" FieldLabel="Is Customer? " />
             <ext:Panel runat="server"  Layout="AnchorLayout" >
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_Cifno" ValueField="CIFNo" DisplayField="Name" StringField="CIFNo, Name" StringTable="VW_SARMemo_CIFNo" Label="CIF" AnchorHorizontal="80%" AllowBlank="false" OnOnValueChanged="cmb_Cifno_OnValueChanged" IsHidden="true"/>
                </Content>
             </ext:Panel>
              <ext:Panel runat="server"  Layout="AnchorLayout" >
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_WICno" ValueField="WICNo" DisplayField="Name" StringField="WICNo, Name" StringTable="VW_SARMemo_WICNo" Label="WIC" AnchorHorizontal="80%" AllowBlank="false" OnOnValueChanged="cmb_WICno_OnValueChanged" IsHidden="false"/>
                </Content>
             </ext:Panel>
            <ext:Button runat="server" ID="btnSearch" Text="Search" Hidden="false"  Icon="PlayGreen" >
                <DirectEvents>
                    <Click OnEvent="btnSearch_Click">
                        <EventMask Msg="Processing..." MinDelay="500" ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:FormPanel runat="server" ID="FpSARMemo" Hidden="true">
                <items>
                 
             <ext:GridPanel ID="gp_case_alert" runat="server" Border="true" MarginSpec="10 0"  ClientIDMode="Static" Title="Case Alert" Collapsible="true">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store1" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="model3" IDProperty="PK_CaseManagement_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CaseManagement_ID" Type="String" ></ext:ModelField>
                                            <ext:ModelField Name="Alert_Type" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="FK_Rule_Basic_ID" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="Rule" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ProcessDate" Type="String"></ext:ModelField>
                                          <ext:ModelField Name="Memo_No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="UniqueCMID" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column35" runat="server" DataIndex="PK_CaseManagement_ID" Text="Case ID" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="UniqueCMID" Text="Unique Case ID" MinWidth="200"></ext:Column>
                               <ext:Column ID="Column36" runat="server" DataIndex="Rule" Text="Rule ID" MinWidth="200"></ext:Column>
                                 <ext:Column ID="Column8" runat="server" DataIndex="Alert_Type" Text="Alert Type" MinWidth="200"></ext:Column>
                                        <ext:Column ID="Column1" runat="server" DataIndex="ProcessDate" Text="Process Date" MinWidth="200"></ext:Column>
                                        <ext:Column ID="Column6" runat="server" DataIndex="Memo_No" Text="Status Memo" MinWidth="530"></ext:Column>
                                                       <%--                 
                                <ext:CommandColumn ID="gp_country_CommandColumn" runat="server" Text="Action" CellWrap="true">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail Data"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gp_country_Gridcommand">
                                        <EventMask ShowMask="true"></EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>--%>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:CheckboxSelectionModel runat="server" Mode="Multi" CheckOnly="true">
                               <%-- <DirectEvents>
                                    <SelectionChange OnEvent="sm_DownloadFromScreeningResult_change">
                                    </SelectionChange>
                                </DirectEvents>--%>
                            </ext:CheckboxSelectionModel>
                        </SelectionModel>
                        <Plugins>
                        <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="false" >
                                <Items>  
                                    <%-- <ext:Button runat="server" Text="Close Picker" ID="Button5">
                                    <Listeners>
                                    <Click Handler="#{window_SEARCH_CUSTOMER}.hide()"> </Click>
                                    </Listeners>
                                    </ext:Button>--%>
                                    <ext:Label runat="server" ID="LabelScreeningResultCount">

                                    </ext:Label>
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
                    </ext:GridPanel>
             <ext:TextField ID="txtNo" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="NO" MaxLength="500" EnforceMaxLength="true" />
             <ext:TextField ID="txtKepada" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Kepada" MaxLength="500" EnforceMaxLength="true"  />
             <ext:TextField ID="txtDari" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Dari" MaxLength="500" EnforceMaxLength="true"  />
             <ext:DateField ID="txtTanggal" runat="server"  AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Tanggal"    Format="dd-MMM-yyyy"  />
             <ext:TextField ID="txtPerihal" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Perihal" MaxLength="500" EnforceMaxLength="true"  />
             <ext:DisplayField ID="displayspace1" runat="server" AnchorHorizontal="80%" AllowBlank="false"  MaxLength="500" EnforceMaxLength="true" />
             <ext:Label runat="server" ID="LblLatarBelakang" Text="A. Latar Belakang"></ext:Label>
             <ext:TextField ID="txtSumberPelaporan" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Sumber Pelaporan" MaxLength="500" EnforceMaxLength="true" StyleSpec="margin-top : 10px;"  />
                  <ext:Panel runat="server" ID="pnl_indikator" Border="true" Layout="AnchorLayout" title="Daftar Indikator Terkait"   MarginSpec="10 0 0 0" Hidden="false" Collapsible="true">
                <Items>
                    <ext:GridPanel ID="gp_indikator" runat="server">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <TopBar>
                    <ext:Toolbar runat="server">
                        <Items>
                            <ext:Button runat="server" ID="btn_Indikator_Add" Text="Add New Indikator Laporan" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btn_Indikator_Add_Click">
                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="storeindikator" runat="server" IsPagingStore="true" PageSize="10" >
                        <Model>
                            <ext:Model runat="server" ID="Model1" IDProperty="PK_OneFCC_CaseManagement_SARMemo_IndikatorID">
                                <Fields>
                                    <ext:ModelField Name="PK_OneFCC_CaseManagement_SARMemo_IndikatorID" Type="Auto"></ext:ModelField>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column4" runat="server" DataIndex="Kode" Text="Kode" MinWidth="220"></ext:Column>
                        <ext:Column ID="Column5" runat="server" DataIndex="Keterangan" Text="Keterangan" MinWidth="520"></ext:Column>
                        <ext:CommandColumn ID="cc_indikator" runat="server" Text="Action" MinWidth="220">
                            <Commands>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                    <ToolTip Text="Edit"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                    <ToolTip Text="Delete"></ToolTip>
                                </ext:GridCommand>
                            </Commands>

                            <DirectEvents>
                                <Command OnEvent="gc_indikator">
                                    <EventMask ShowMask="true"></EventMask>
                                    <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_OneFCC_CaseManagement_SARMemo_IndikatorID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                                 
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader runat="server"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
                  
                </Items>
                </ext:Panel>
          
             <ext:DisplayField ID="DisplayField2" runat="server" AnchorHorizontal="80%" AllowBlank="false"  MaxLength="500" EnforceMaxLength="true" />
             <ext:Label runat="server" ID="LblPihakCalonTerkait" Text="B. Profil Nasabah"></ext:Label>
             <ext:TextField ID="txtNamaPihakPelapor" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Nama Pihak Pelapor" MaxLength="500" EnforceMaxLength="true" StyleSpec="margin-top : 10px"  />
             <ext:TextField ID="txtKategoriPihakPelapor" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Kategori Pihak Pelapor" MaxLength="500" EnforceMaxLength="true"  />
             <ext:FieldSet runat="server" ColumnWidth="0.8" ID="FieldSet3" Border="false" Layout="ColumnLayout" StyleSpec="margin-left:-10px;" AnchorHorizontal="80%">    
                   <Content>
                    <ext:TextField ID="txtUnitBisnis" runat="server" AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Unit Bisnis/Direktorat/Cabang" MaxLength="500" EnforceMaxLength="true"  />
                    <ext:TextField ID="txtDirektor" runat="server" AnchorHorizontal="100%" AllowBlank="true"  MaxLength="500" EnforceMaxLength="true" StyleSpec="margin-left : 10px;"  />
                    <ext:TextField ID="txtCabang" runat="server" AnchorHorizontal="100%" AllowBlank="false"  MaxLength="500" EnforceMaxLength="true" StyleSpec="margin-left : 10px;" />
                   </Content>
             </ext:FieldSet>
             <ext:FieldSet runat="server" ColumnWidth="0.8" ID="FieldSet1" Border="false" Layout="ColumnLayout" StyleSpec="margin-left:-10px;" AnchorHorizontal="80%">    
                   <Content>
                             <ext:TextField ID="txtLamaMenjadiNasabah" runat="server" AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Lama Menjadi Nasabah" MaxLength="500" EnforceMaxLength="true" StyleSpec="margin-right : 10px;" />
                              <ext:Label runat="server" ID="Label2" Text=" Contoh : Sejak 20 Agustus 2020 (6 Bulan)"></ext:Label>
                        </Content>
             </ext:FieldSet> 
            
             <ext:TextField ID="txtPekerjaanBidangUsaha" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Pekerjaan/Bidang Usaha" MaxLength="500" EnforceMaxLength="true"  />
             <ext:TextField ID="txtPengahasilanTahun" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Penghasilan/Tahun(IDR)"  InputType="Number" EnforceMaxLength="true"  MaxLength="250"  />
             <ext:TextField ID="txtProfilLainnya" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Profil Lainnya" MaxLength="500" EnforceMaxLength="true"  />
             <ext:DisplayField ID="DisplayField3" runat="server" AnchorHorizontal="80%" AllowBlank="false"  MaxLength="500" EnforceMaxLength="true" />
             <ext:Label runat="server" ID="LblPihakTerkait" Text="C. Pihak Terkait"></ext:Label>
             <ext:Panel runat="server" ID="pnl_pihak_terkait" Border="true" Layout="AnchorLayout" title="Pihak Terkait"   MarginSpec="10 0 10 0" Hidden="false" Collapsible="true">
                        <Items>
                            <ext:GridPanel ID="gp_Pihak_Terkait" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Pihak_Terkait_Add" Text="Add New Pihak Terkait" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Pihak_Terkait_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store2" runat="server" IsPagingStore="true" PageSize="10" >
                                <Model>
                                    <ext:Model runat="server" ID="Model2" IDProperty="PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID">
                                        <Fields>
                                            <ext:ModelField Name="PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nama_PihakTerkait" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Hubungan_PihakTerkait" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RefGrips_PihakTerkait" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" ></ext:RowNumbererColumn>
                                <ext:Column ID="Column2" runat="server" DataIndex="CIF" Text="CIF" MinWidth="220"></ext:Column>
                                <ext:Column ID="Column3" runat="server" DataIndex="Nama_PihakTerkait" Text="Nama" MinWidth="440"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="Hubungan_PihakTerkait" Text="Hubungan Dengan Pihak Terkait" MinWidth="220"></ext:Column>
                               <%-- <ext:Column ID="Column9" runat="server" DataIndex="RefGrips_PihakTerkait" Text="Ref.Grips" MinWidth="220"></ext:Column>--%>
                                <ext:CommandColumn ID="cc_pihakterkait" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="gc_PihakTerkait">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                 
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                  
                        </Items>
                     </ext:Panel>
           
             <ext:DisplayField ID="DisplayField4" runat="server" AnchorHorizontal="80%" AllowBlank="false"  MaxLength="500" EnforceMaxLength="true" />
             <ext:Label runat="server" ID="LblHasilAnalisis" Text="D. Hasil Analisis"></ext:Label>
              <ext:HtmlEditor ID="txtHasilAnalisis" runat="server"  AllowBlank="false" Height="200" AutoScroll="true" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true" FieldStyle="background-color: red" StyleSpec="margin-top : 10px;">
                <Listeners>
                <Initialize Fn="onInitialize" />
             </Listeners>    
             </ext:HtmlEditor>
             <ext:DisplayField ID="DisplayField7" runat="server" AnchorHorizontal="80%" AllowBlank="false"  MaxLength="500" EnforceMaxLength="true" />
             <ext:Label runat="server" ID="LblKesimpulan" Text="E. Kesimpulan"></ext:Label>
             <ext:TextArea ID="txtKesimpulan" runat="server" AnchorHorizontal="80%" AllowBlank="false"  MaxLength="4000" EnforceMaxLength="true" StyleSpec="margin-top : 10px;" />
              <ext:Panel runat="server"  Layout="AnchorLayout" >
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_KesimpulanDetail" ValueField="PK_OneFCC_CaseManagement_Kesimpulan_ID" DisplayField="KesimpulanDescription" StringField="PK_OneFCC_CaseManagement_Kesimpulan_ID, KesimpulanDescription" StringTable="OneFCC_CaseManagement_Kesimpulan" Label="Detail" AnchorHorizontal="80%" AllowBlank="true"/>
                </Content>
             </ext:Panel>
             <ext:DisplayField ID="DisplayField5" runat="server" AnchorHorizontal="80%" AllowBlank="false"  MaxLength="500" EnforceMaxLength="true" />
             <ext:Label runat="server" ID="LblTindakLanjut" Text="F. Tindak Lanjut"></ext:Label>
              <ext:Panel runat="server" ID="pnl_tindak_lanjut" Border="true" Layout="AnchorLayout" title="Daftar Tindak Lanjut"  MarginSpec="10 0 0 0" Hidden="false" Collapsible="true">
                        <Items>
                            <ext:GridPanel ID="gp_TIndakLanjut" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Tindak_Lanjut_Add" Text="Add New Tindak Lanjut" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Tindak_Lanjut_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store3" runat="server" IsPagingStore="true" PageSize="10" >
                                <Model>
                                    <ext:Model runat="server" ID="Model4" IDProperty="PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID">
                                        <Fields>
                                            <ext:ModelField Name="PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="FK_OneFCC_CaseManagement_TindakLanjut_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TindakLanjutDescription" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No"></ext:RowNumbererColumn>
                            <%--    <ext:Column ID="Column10" runat="server" DataIndex="CIF" Text="CIF" MinWidth="220"></ext:Column>--%>
                                <ext:Column ID="Column11" runat="server" DataIndex="FK_OneFCC_CaseManagement_TindakLanjut_ID" Text="ID" MinWidth="230"></ext:Column>
                                   <ext:Column ID="Column37" runat="server" DataIndex="TindakLanjutDescription" Text="Tindak Lanjut" MinWidth="500"></ext:Column>
                                <ext:CommandColumn ID="cc_tindaklanjut" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="gc_TindakLanjut">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                 
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                  
                        </Items>
                     </ext:Panel>
           
             <ext:DisplayField ID="DisplayField6" runat="server" AnchorHorizontal="80%" AllowBlank="false"  MaxLength="500" EnforceMaxLength="true" />
             <ext:Label runat="server" ID="LblTransaksiSTR" Text="G. Activity SAR"></ext:Label>
           
           <ext:DisplayField ID="DisplayField1" runat="server" AnchorHorizontal="80%" AllowBlank="false"  MaxLength="500" EnforceMaxLength="true" />
                                  <ext:Button ID="BtnSaveDetail" runat="server" Text="Load Activity">
                <DirectEvents>
                    <Click OnEvent="BtnSearch_DirectEvent">
                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
             </ext:Button>
             <ext:DisplayField ID="TextField5" runat="server" AnchorHorizontal="80%" />
             <ext:Panel runat="server" ID="PanelChecked" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Activities to be included in SAR"  StyleSpec="margin-top : 10px;margin-bottom : 10px;" Collapsible="true">
                <Items>
                    <ext:GridPanel ID="gp_activity" runat="server" AutoScroll="true" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreActivity" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model ID="ModelDetail" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Pk_OneFCC_CaseManagement_SARMemo_ActivityID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Date_Activity" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Reason" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Significance" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn28" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="ColAccount_NO" runat="server" DataIndex="Account_NO" Text="Account_NO" Width="150"></ext:Column>
                                <ext:Column ID="colSignificance" runat="server" DataIndex="Significance" Text="Significance" Width="150"></ext:Column>
                                <ext:Column ID="colReason" runat="server" DataIndex="Reason" Text="Reason" Width="150"></ext:Column>
                                <ext:DateColumn ID="colDate_Activity" runat="server" DataIndex="Date_Activity" Text="Date Activity" Width="150"></ext:DateColumn>
                                <ext:Column ID="colComments" runat="server" DataIndex="Comments" Text="Comments" Width="150"></ext:Column>
                              

                                               <ext:CommandColumn ID="cc_Activity" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="gc_Activity">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.Pk_OneFCC_CaseManagement_SARMemo_ActivityID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                                <%--                                <Items>
                                       <ext:Button runat="server" ID="BtnExport" Text="Export" AutoPostBack="true" OnClick="CreateExcel2007WithData"  />
                                </Items>--%>
                                <Items>
                                    <ext:Button runat="server" ID="BtnExport" Text="Export" Icon="Disk">
                                        <DirectEvents>
                                            <Click OnEvent="CreateExcel2007WithData">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <SelectionModel>
                           
                        </SelectionModel>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>

                          <ext:DateField ID="TanggalLaporan" runat="server" Format="dd-MMM-yyyy" AllowBlank="false" FieldLabel="Tanggal Laporan" AnchorHorizontal="40%" />
             <ext:ComboBox ID="sar_jenisLaporan" runat="server" FieldLabel="Jenis Laporan" DisplayField="Keterangan" ValueField="Kode" EmptyText="Pilih Salah Satu" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreJenisLaporan" OnReadData="JenisLaporan_ReadData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server" ID="Model8">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <DirectEvents>
                    <Change OnEvent="sar_jenisLaporan_DirectSelect"></Change>
                </DirectEvents>
            </ext:ComboBox>
             <ext:TextField ID="NoRefPPATK" runat="server" FieldLabel="No Ref PPATK" AnchorHorizontal="80%" Hidden="true" />
             <ext:TextArea ID="alasan" runat="server" FieldLabel="Alasan" AnchorHorizontal="80%" AllowBlank="false" />
         

            <ext:Panel runat="server" ID="PanelAttachment" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Attachment Document"   Collapsible="true" StyleSpec="margin-top : 10px;">
                <Items>
                    <ext:GridPanel ID="GridPanelAttachment" runat="server" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="ButtonAttachment" Text="Tambah Dokumen" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_addAttachment_click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="500"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreAttachment" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model7" IDProperty="PK_OneFCC_CaseManagement_SARMemo_AttachmentID">
                                        <Fields>
                                            <ext:ModelField Name="PK_OneFCC_CaseManagement_SARMemo_AttachmentID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FileName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column33" runat="server" DataIndex="FileName" Text="File Name" Flex="1">
                                    <Commands>
                                        <ext:ImageCommand Icon="DiskDownload" CommandName="Download" ToolTip-Text="Download Dokumen"></ext:ImageCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridcommandAttachment">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_OneFCC_CaseManagement_SARMemo_AttachmentID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:Column>
                                <ext:Column ID="Column34" runat="server" DataIndex="Keterangan" Text="Description" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAttachment" runat="server" Text="Action" Flex="1">

                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>

                                        <Command OnEvent="GridcommandAttachment">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>

                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_OneFCC_CaseManagement_SARMemo_AttachmentID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
             <ext:DisplayField ID="DisplayField8" runat="server" AnchorHorizontal="80%" AllowBlank="false"  MaxLength="500" EnforceMaxLength="true" />
             <ext:Label runat="server" ID="LblWorkflowHistory" Text="H. Workflow History"></ext:Label>
             <ext:GridPanel ID="gp_workflowhistory" runat="server" Border="true" MarginSpec="10 0"   ClientIDMode="Static" Title="Workflow History" Collapsible="true" >
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreWH" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="model5" IDProperty="PK_CaseManagement_WorkflowHistory_SARMemo_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CaseManagement_WorkflowHistory_SARMemo_ID" Type="String" ></ext:ModelField>
                                            <ext:ModelField Name="Workflow_Step" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="UserID" Type="String"></ext:ModelField>
                                          <ext:ModelField Name="Approval_Status" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Analysis_Result" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No"></ext:RowNumbererColumn>
                               
                                 <ext:Column ID="Column12" runat="server" DataIndex="Workflow_Step" Text="Workflow Step" MinWidth="100"></ext:Column>
                                        <ext:Column ID="Column13" runat="server" DataIndex="UserID" Text="User ID" MinWidth="200"></ext:Column>
                                        <ext:Column ID="Column14" runat="server" DataIndex="Approval_Status" Text="Status" MinWidth="400"></ext:Column>
                                                        <ext:Column ID="Column15" runat="server" DataIndex="Analysis_Result" Text="Approval Notes" MinWidth="400"></ext:Column>
                                        <ext:Column ID="Column16" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="400"></ext:Column>
                                                       <%--                 
                                <ext:CommandColumn ID="gp_country_CommandColumn" runat="server" Text="Action" CellWrap="true">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail Data"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gp_country_Gridcommand">
                                        <EventMask ShowMask="true"></EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>--%>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                           
                        </SelectionModel>
                        <Plugins>
                        <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="false" >
                                <Items>  
                                    <%-- <ext:Button runat="server" Text="Close Picker" ID="Button5">
                                    <Listeners>
                                    <Click Handler="#{window_SEARCH_CUSTOMER}.hide()"> </Click>
                                    </Listeners>
                                    </ext:Button>--%>
                                    <ext:Label runat="server" ID="Label1">

                                    </ext:Label>
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
                    </ext:GridPanel>
                </items>
            </ext:FormPanel>
           
        </Items>            
        <Buttons>
            <ext:Button runat="server" ID="btnSave" Text="Save">
          
                <DirectEvents>
                    <Click OnEvent="btnSave_Click">
                        <EventMask Msg="Processing..." MinDelay="500" ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancel" runat="server" Text="Cancel" Icon="Cancel">
                <Listeners>
                    <Click Handler="#{BtnCancel}.disable()" />
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="btnCancel_DirectEvent">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="500">
                        </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>

            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        
    </ext:FormPanel>
       <%-- Pop Up Window Indikator --%>
             <ext:Window ID="Window_Indikator" runat="server"  Maximized="true" Maximizable="true"  Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="400" Height="400" Layout="FitLayout">
                <Items>
                    <ext:FormPanel ID="pn_indikator" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_indikator" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode, Keterangan" StringTable="goAML_Ref_Indikator_Laporan" Label="Indikator" AnchorHorizontal="100%" AllowBlank="false"/>
                        </Content>
                    </ext:FormPanel>
                </Items>
                <Buttons>
                    <ext:Button ID="btn_indikator_Save" runat="server" Icon="Disk" Text="Save">
                        <DirectEvents>
                            <Click OnEvent="btn_indikator_Save_Click">
                                <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="btn_indikator_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="btn_indikator_Cancel_Click">
                                <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
                <Listeners>
                    <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.4, height: size.height * 0.4});" />

                    <Resize Handler="#{Window_Indikator}.center()" />
                </Listeners>
            </ext:Window>
      <ext:Window ID="Window_PihakTerkair" runat="server"  Maximized="true" Maximizable="true"  Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="400" Height="400" Layout="FitLayout">
                <Items>
                    <ext:FormPanel ID="FormPanel1" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                        <Content>
                             <ext:Panel runat="server"  Layout="AnchorLayout" >
                                <Content>
                                    <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmbPT_CIF" ValueField="Kode" DisplayField="Keterangan" StringField="Kode, Keterangan" StringTable="vw_goAML_Ref_Customer" Label="CIF" AnchorHorizontal="80%" AllowBlank="true"/>
                                </Content>
                             </ext:Panel>
                            <ext:TextField ID="txtPT_CIF" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="CIF" MaxLength="500" EnforceMaxLength="true" ReadOnly ="true" Hidden="true" />
                            <ext:TextField ID="txtPT_HubunganPihakTerkait" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Hubungan Pihak Terkait" MaxLength="500" EnforceMaxLength="true"  />
                            <ext:TextField ID="txtPT_TelahDilaporkan" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Telah Pelaporan" MaxLength="500" EnforceMaxLength="true" hidden="true" />
                            <ext:TextField ID="txtPT_RefGrips" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Ref Grips" MaxLength="500" EnforceMaxLength="true" hidden="true" />

                        </Content>
                    </ext:FormPanel>
                </Items>
                <Buttons>
                    <ext:Button ID="btn_PihakTerkait_Save" runat="server" Icon="Disk" Text="Save">
                        <DirectEvents>
                            <Click OnEvent="btn_PihakTerkait_Save_Click">
                                <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="btn_PihakTerkait_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="btn_PihakTerkait_Cancel_Click">
                                <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
                <Listeners>
                    <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.4, height: size.height * 0.4});" />

                    <Resize Handler="#{Window_PihakTerkair}.center()" />
                </Listeners>
            </ext:Window>
      <ext:Window ID="window_tindaklanjut" runat="server"  Maximized="true" Maximizable="true"  Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="400" Height="400" Layout="FitLayout">
                <Items>
                    <ext:FormPanel ID="FormPanel2" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_tindaklanjut" ValueField="PK_OneFCC_CaseManagement_TindakLanjut_ID" DisplayField="TindakLanjutDescription" runat="server" StringField="PK_OneFCC_CaseManagement_TindakLanjut_ID, TindakLanjutDescription" StringTable="OneFCC_CaseManagement_TindakLanjut" Label="Tindak Lanjut" AnchorHorizontal="100%" AllowBlank="false"/>
                        </Content>
                    </ext:FormPanel>
                </Items>
                <Buttons>
                    <ext:Button ID="btn_TindakLanjut_Save" runat="server" Icon="Disk" Text="Save">
                        <DirectEvents>
                            <Click OnEvent="btn_TindakLanjut_Save_Click">
                                <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="btn_TindakLanjut_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="btn_TindakLanjut_Cancel_Click">
                                <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
                <Listeners>
                    <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.4, height: size.height * 0.4});" />

                    <Resize Handler="#{window_tindaklanjut}.center()" />
                </Listeners>
            </ext:Window>
      
            <ext:Window ID="WindowAttachment" Layout="AnchorLayout" Title="Dokumen" runat="server" Modal="true" Hidden="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
            <Items>
            <ext:FileUploadField ID="FileDoc" runat="server" FieldLabel="File Document" AnchorHorizontal="100%">
            </ext:FileUploadField>
            <ext:DisplayField ID="txtFileName" runat="server" FieldLabel="File Name" AnchorHorizontal="100%">
            </ext:DisplayField>
            <ext:TextArea ID="txtKeterangan" runat="server" FieldLabel="Description" AnchorHorizontal="100%">
            </ext:TextArea>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.4, height: size.height * 0.4});" />

            <Resize Handler="#{WindowAttachment}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="BtnsaveAttachment" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">

                <DirectEvents>
                    <Click OnEvent="btnSaveAttachment_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancelAttachment" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancelAttachment_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

     <ext:Window ID="Window_Activity" runat="server"  Maximized="true" Maximizable="true"  Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="400" Height="400" Layout="FitLayout">
                <Items>
                    <ext:FormPanel ID="FormPanel3" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                        <Content>
                            <ext:NumberField ID="txtSignificance" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Significance" MaxLength="1" EnforceMaxLength="true" />
                            <ext:TextField ID="txtDescription" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Description" MaxLength="500" EnforceMaxLength="true" />
                            <ext:TextArea ID="txtComment" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Comment" MaxLength="500" EnforceMaxLength="true"  />

                        </Content>
                    </ext:FormPanel>
                </Items>
                <Buttons>
                    <ext:Button ID="btn_Activity_Save" runat="server" Icon="Disk" Text="Save">
                        <DirectEvents>
                            <Click OnEvent="btn_Activity_Save_Click">
                                <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="btn_Activity_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="btn_Activity_Cancel_Click">
                                <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
                <Listeners>
                    <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.4, height: size.height * 0.4});" />

                    <Resize Handler="#{window_tindaklanjut}.center()" />
                </Listeners>
            </ext:Window>
      
</asp:Content>



