﻿Imports Ext.Net
Imports System.Data
Imports CasemanagementDAL
Imports CasemanagementBLL
Partial Class OneFcc_Rule_RuleTemplateDetail
    Inherits Parent

    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("RuleTemplateDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("RuleTemplateDetail.ObjModule") = value
        End Set
    End Property

    Public Property objRuleTemplate() As OneFcc_MS_Rule_Template
        Get
            If Session("RuleTemplateDetail.objRuleTemplate") Is Nothing Then
                Dim strid As String = Request.Params("ID")
                Dim id As String = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim oNewObj As OneFcc_MS_Rule_Template = RuleTemplateBLL.GetRuleTemplate(id)
                Session("RuleTemplateDetail.objRuleTemplate") = oNewObj
            End If
            Return Session("RuleTemplateDetail.objRuleTemplate")
        End Get
        Set(ByVal value As OneFcc_MS_Rule_Template)
            Session("RuleTemplateDetail.objRuleTemplate") = value
        End Set
    End Property

    Public Property objRuleTemplateDetail() As List(Of OneFcc_MS_Rule_Template_Detail)
        Get
            If Session("RuleTemplateDetail.objRuleTemplateDetail") Is Nothing Then
                Dim oNewObj As New List(Of OneFcc_MS_Rule_Template_Detail)
                Session("RuleTemplateDetail.objRuleTemplateDetail") = oNewObj
            End If
            Return Session("RuleTemplateDetail.objRuleTemplateDetail")
        End Get
        Set(ByVal value As List(Of OneFcc_MS_Rule_Template_Detail))
            Session("RuleTemplateDetail.objRuleTemplateDetail") = value
        End Set
    End Property

    Public Property objRuleTemplateDetailEdit() As OneFcc_MS_Rule_Template_Detail
        Get
            Return Session("RulesBasicTemplateAdd.objRuleTemplateDetailEdit")
        End Get
        Set(ByVal value As OneFcc_MS_Rule_Template_Detail)
            Session("RulesBasicTemplateAdd.objRuleTemplateDetailEdit") = value
        End Set
    End Property
    Public Property objRuleTemplateOld() As OneFcc_MS_Rule_Template
        Get
            If Session("RuleTemplateDetail.objRuleTemplateOld") Is Nothing Then
                Dim strid As String = Request.Params("ID")
                Dim id As String = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim oNewObj As OneFcc_MS_Rule_Template = RuleTemplateBLL.GetRuleTemplate(id)
                Session("RuleTemplateDetail.objRuleTemplateOld") = oNewObj
            End If
            Return Session("RuleTemplateDetail.objRuleTemplateOld")
        End Get
        Set(ByVal value As OneFcc_MS_Rule_Template)
            Session("RuleTemplateDetail.objRuleTemplateOld") = value
        End Set
    End Property


    Private Sub MUserAdd_Init(sender As Object, e As EventArgs) Handles Me.Init
        'objRuleBasicTempateBll = New NawaDevBLL.RulesBasicTemplateBLL(FormPanelInput)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                Dim strmodule As String = Request.Params("ModuleID")
                ClearSession()
                Try
                    Dim intmodule As Integer = NawaBLL.Common.DecryptQueryString(strmodule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                    Me.ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intmodule)

                    If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Update) Then
                        Dim strIDCode As String = 1
                        strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                        Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    FormPanelInput.Title = ObjModule.ModuleLabel & " Add"
                    Panelconfirmation.Title = ObjModule.ModuleLabel & " Add"
                    LoadDataSetting()



                    Dim objnavimagecommand As Ext.Net.CommandColumn = GridPanelDetail.ColumnModel.Columns.Find(Function(x) x.ID = "commandfield")

                    Dim objparamsettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
                    Dim bsettingright As Integer = 1
                    If Not objparamsettingbutton Is Nothing Then
                        bsettingright = objparamsettingbutton.SettingValue
                    End If

                    If bsettingright = 1 Then

                        GridPanelDetail.ColumnModel.Columns.Remove(objnavimagecommand)
                        GridPanelDetail.ColumnModel.Columns.Add(objnavimagecommand)
                    ElseIf bsettingright = 2 Then
                        GridPanelDetail.ColumnModel.Columns.Remove(objnavimagecommand)
                        GridPanelDetail.ColumnModel.Columns.Insert(1, objnavimagecommand)

                    End If
                Catch ex As Exception
                    'Throw New Exception("Invalid Module ID")
                    Throw New Exception(ex.Message & " " & ex.InnerException.Message)
                End Try
            End If


            'objRuleBasicTempateBll.Bentukform()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub StoreTableReference_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = "  TABLE_NAME like '%" & query & "%'"
            End If

            StoreTableReference.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("INFORMATION_SCHEMA.TABLES", "TABLE_NAME", strfilter, "TABLE_NAME", e.Start, e.Limit, e.Total)
            StoreTableReference.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub StoreTableReferenceKey_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = " COLUMN_NAME like '" & query & "%' and table_Name='" & cboTableReference.SelectedItem.Value & "'"
            Else
                strfilter = " table_Name='" & cboTableReference.SelectedItem.Value & "'"
            End If

            StoreTableReferenceKey.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("INFORMATION_SCHEMA.COLUMNS", "COLUMN_NAME", strfilter, "TABLE_NAME", e.Start, e.Limit, e.Total)
            StoreTableReferenceKey.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub StoreTableReferenceDisplayName_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = " COLUMN_NAME like '" & query & "%' and table_Name='" & cboTableReference.SelectedItem.Value & "'"
            Else
                strfilter = " table_Name='" & cboTableReference.SelectedItem.Value & "'"
            End If

            StoreTableReferenceDisplayName.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("INFORMATION_SCHEMA.COLUMNS", "COLUMN_NAME", strfilter, "TABLE_NAME", e.Start, e.Limit, e.Total)
            StoreTableReferenceDisplayName.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
    Sub LoadDataEdit()
        If Not objRuleTemplate Is Nothing Then
            With objRuleTemplate
                txtruleTemplateName.Text = .Rule_Template_Name
                txtquery.Text = .Rule_Template_Query
                CboCaseGroupingBy.SelectedItem.Value = .Case_Grouping_By

                '19-Aug-2022 Adi : Penambahan Rule Template Category
                cboTemplateCategory.SelectedItem.Value = .FK_Template_Category_Parameter

                txtruleTemplateName.ReadOnly = True
                txtquery.ReadOnly = True
                CboCaseGroupingBy.ReadOnly = True
                cboTemplateCategory.ReadOnly = True
            End With

            Dim ListDetail = RuleTemplateBLL.GetRuleTemplateDetail(objRuleTemplate.PK_Rule_Template_ID)
            For Each Detail In ListDetail
                Dim newDetail As New OneFcc_MS_Rule_Template_Detail
                With newDetail
                    .PK_Rule_Template_Detail_ID = Detail.PK_Rule_Template_Detail_ID
                    .FK_Rule_Template_ID = Detail.FK_Rule_Template_ID
                    .Variable_Name = Detail.Variable_Name
                    .FK_FieldType_ID = Detail.FK_FieldType_ID
                    .FK_ExtType_ID = Detail.FK_ExtType_ID
                    .Tabel_Reference_Name = Detail.Tabel_Reference_Name
                    .Tabel_Reference_Name_Alias = Detail.Tabel_Reference_Name_Alias
                    .Table_Reference_Field_Key = Detail.Table_Reference_Field_Key
                    .Table_Reference_Field_Display_Name = Detail.Table_Reference_Field_Display_Name
                    .Table_Reference_Filter = Detail.Table_Reference_Filter
                    .Table_Reference_Additonal_Join = Detail.Table_Reference_Additonal_Join
                    .Variable_Description = Detail.Variable_Description
                    .MFieldTypeName = Detail.MFieldTypeName
                    .MExtTypeName = Detail.MExtTypeName
                    objRuleTemplateDetail.Add(newDetail)
                End With
            Next

            StoreDetail.DataSource = objRuleTemplateDetail
            StoreDetail.DataBind()

            BindDetail()
        End If

    End Sub
    Sub LoadDataSetting()
        cboFieldType.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        cboExtType.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreextType.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreFieldType.PageSize = NawaBLL.SystemParameterBLL.GetPageSize

        StoreCaseGroupingBy.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT Case_Grouping FROM OneFcc_MS_Rule_Case_Grouping ", Nothing)
        StoreCaseGroupingBy.DataBind()
        CboCaseGroupingBy.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreCaseGroupingBy.PageSize = NawaBLL.SystemParameterBLL.GetPageSize

        '19-Aug-2022 Adi : Penambahan Rule Template Category
        storeTemplateCategory.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT PK_Template_Category_Parameter, Template_Category_Parameter_Name FROM OneFCC_MS_Template_Category_Parameter", Nothing)
        storeTemplateCategory.DataBind()
        cboTemplateCategory.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        storeTemplateCategory.PageSize = NawaBLL.SystemParameterBLL.GetPageSize

        LoadDataEdit()

    End Sub

    Sub ClearSession()
        objRuleTemplate = Nothing
        objRuleTemplateDetail = Nothing
        objRuleTemplateDetailEdit = Nothing
        objRuleTemplateOld = Nothing
    End Sub

    Protected Sub StoreextType_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = " ExtTypeName like '" & query & "%'"
            End If

            StoreextType.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("MExtType", "PK_ExtType_ID,ExtTypeName", strfilter, "PK_ExtType_ID", e.Start, e.Limit, e.Total)
            StoreextType.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub StoreFieldType_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = " FieldTypeCaption like '" & query & "%'"
            End If
            StoreFieldType.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("MFieldType", "PK_FieldType_ID,FieldTypeCaption", strfilter, "PK_FieldType_ID", e.Start, e.Limit, e.Total)
            StoreFieldType.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnCancelField_Directclick(sender As Object, e As DirectEventArgs)
        Try

            btnsaveField.Text = "Add Replacer"
            ClearDetail()
            objRuleTemplateDetailEdit = Nothing
            WindowDetail.Close()
            BindDetail()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Function IsdatavalidField() As Boolean

        'done:IsdatavalidField
        If cboFieldType.SelectedItem.Value = "11" And cboTableReference.SelectedItem.Value Is Nothing Then
            'validasi table
            Throw New Exception("Table Reference Field Display Name is required.")
        End If
        Return True
    End Function

    Sub ClearDetail()
        'done: clearDetail
        txtvariableName.Clear()
        txtvariableDesc.Clear()
        cboExtType.Clear()
        cboFieldType.Clear()
        cboTableReference.Clear()
        txtTableAlias.Clear()
        CboTableReferencKey.Clear()
        CboTableReferenceDisplayName.Clear()
        txtTableReferenceAdditionalJoin.Clear()
        txtTableReferenceFilter.Clear()

    End Sub

    Sub BindDetail()
        'done:BindDetail grid field

        Dim objdt As System.Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objRuleTemplateDetail)
        'objdt = NawaFramework.GetDataTable(objdt, New Data.DataColumn("MFieldTypeName", GetType(String)))

        'objdt = NawaFramework.GetDataTable(objdt, New Data.DataColumn("MExtTypeName", GetType(String)))

        Dim objtblMFieldType As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT mt.PK_FieldType_ID, mt.FieldTypeCaption FROM MFieldType AS mt", Nothing)
        Dim objtblExtType As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT mt.PK_ExtType_ID, mt.ExtTypeName FROM MExtType AS mt", Nothing)

        For Each item As Data.DataRow In objdt.Rows
            item("MFieldTypeName") = objtblMFieldType.Select("PK_FieldType_ID=" & item("FK_FieldType_ID"))(0)("FieldTypeCaption").ToString
            item("MExtTypeName") = objtblExtType.Select("PK_ExtType_ID=" & item("FK_ExtType_ID"))(0)("ExtTypeName").ToString
        Next

        StoreDetail.DataSource = objdt
        StoreDetail.DataBind()

    End Sub

    Sub LoadDataEditModuleField(id As Long)
        'done:LoadDataEditModuleField
        WindowDetail.Hidden = False
        btnsaveField.Text = "Save Replacer"
        objRuleTemplateDetailEdit = objRuleTemplateDetail.Find(Function(x) x.PK_Rule_Template_Detail_ID = id)
        With objRuleTemplateDetailEdit
            txtvariableName.Text = .Variable_Name
            txtvariableDesc.Text = .Variable_Description
            Dim strfieldtypestring As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT mt.FieldTypeCaption FROM MFieldType AS mt WHERE mt.PK_FieldType_ID='" & .FK_FieldType_ID & "'", Nothing)
            Dim strfieldextstring As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT mt.ExtTypeName FROM MExtType AS mt WHERE mt.PK_ExtType_ID='" & .FK_ExtType_ID & "'", Nothing)

            cboExtType.DoQuery(strfieldextstring, True)
            cboExtType.SetValueAndFireSelect(.FK_ExtType_ID)

            cboFieldType.DoQuery(strfieldtypestring, True)
            cboFieldType.SetValueAndFireSelect(.FK_FieldType_ID)

            If cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.ReferenceTable) Then

                txtTableAlias.Text = .Tabel_Reference_Name_Alias


                cboTableReference.DoQuery(.Tabel_Reference_Name, True)
                cboTableReference.SetValueAndFireSelect(.Tabel_Reference_Name)
                cboTableReference.SelectedItem.Value = .Tabel_Reference_Name

                CboTableReferenceDisplayName.DoQuery(.Table_Reference_Field_Key, True)
                CboTableReferencKey.SetValueAndFireSelect(.Table_Reference_Field_Key)
                CboTableReferencKey.SelectedItem.Value = .Table_Reference_Field_Key

                CboTableReferenceDisplayName.DoQuery(.Table_Reference_Field_Display_Name, True)
                CboTableReferenceDisplayName.SetValueAndFireSelect(.Table_Reference_Field_Display_Name)
                CboTableReferenceDisplayName.SelectedItem.Value = .Table_Reference_Field_Display_Name

                txtTableReferenceAdditionalJoin.Text = .Table_Reference_Additonal_Join
                txtTableReferenceFilter.Text = .Table_Reference_Filter


            End If


        End With
    End Sub

    Sub DeleteModuleField(id As Long)
        'done:DeleteModuleField
        Dim objdelete As OneFcc_MS_Rule_Template_Detail = objRuleTemplateDetail.Find(Function(x) x.PK_Rule_Template_Detail_ID = id)
        If Not objdelete Is Nothing Then
            objRuleTemplateDetail.Remove(objdelete)
        End If
        BindDetail()
    End Sub

    Protected Sub CallBackModuleField(sender As Object, e As DirectEventArgs)
        'done:CallBackModuleField
        Dim id As Long = e.ExtraParams(0).Value

        Select Case e.ExtraParams("command")
            Case "Edit"
                LoadDataEditModuleField(id)
            Case "Delete"
                DeleteModuleField(id)

        End Select
    End Sub

    Protected Sub BtnSaveField_Directclick(sender As Object, e As DirectEventArgs)
        Try
            If IsdatavalidField() Then

                If Me.objRuleTemplateDetailEdit Is Nothing Then
                    Dim objNewTR As New OneFcc_MS_Rule_Template_Detail
                    With objNewTR
                        Dim objrand As New Random
                        Dim intpk As Long = objrand.Next(Integer.MinValue, -1)
                        While Not objRuleTemplateDetail.Find(Function(x) x.PK_Rule_Template_Detail_ID = intpk) Is Nothing
                            intpk = objrand.Next(Integer.MinValue, -1)
                        End While

                        .PK_Rule_Template_Detail_ID = intpk
                        .Variable_Name = txtvariableName.Text.Trim
                        .Variable_Description = txtvariableDesc.Text.Trim
                        .FK_FieldType_ID = cboFieldType.SelectedItem.Value
                        .FK_ExtType_ID = cboExtType.SelectedItem.Value
                        .Tabel_Reference_Name = cboTableReference.SelectedItem.Value
                        .Tabel_Reference_Name_Alias = txtTableAlias.Text.Trim
                        .Table_Reference_Field_Key = CboTableReferencKey.SelectedItem.Value
                        .Table_Reference_Field_Display_Name = CboTableReferenceDisplayName.SelectedItem.Value
                        .Table_Reference_Filter = txtTableReferenceFilter.Text.Trim
                        .Table_Reference_Additonal_Join = txtTableReferenceAdditionalJoin.Text.Trim
                    End With
                    objRuleTemplateDetail.Add(objNewTR)
                Else

                    With objRuleTemplateDetailEdit

                        .Variable_Name = txtvariableName.Text.Trim
                        .Variable_Description = txtvariableDesc.Text.Trim
                        .FK_FieldType_ID = cboFieldType.SelectedItem.Value
                        .FK_ExtType_ID = cboExtType.SelectedItem.Value
                        .Tabel_Reference_Name = cboTableReference.SelectedItem.Value
                        .Tabel_Reference_Name_Alias = txtTableAlias.Text.Trim
                        .Table_Reference_Field_Key = CboTableReferencKey.SelectedItem.Value
                        .Table_Reference_Field_Display_Name = CboTableReferenceDisplayName.SelectedItem.Value
                        .Table_Reference_Filter = txtTableReferenceFilter.Text.Trim
                        .Table_Reference_Additonal_Join = txtTableReferenceAdditionalJoin.Text.Trim
                    End With

                    Me.objRuleTemplateDetailEdit = Nothing
                End If

                ClearDetail()
                WindowDetail.Close()
                BindDetail()
                btnsaveField.Text = "Add Replacer"

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub cboFieldType_DirectSelect(sender As Object, e As DirectEventArgs)
        Try

            If cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.ReferenceTable) Then
                fieldsetref.Hidden = False
            Else
                fieldsetref.Hidden = True
            End If

            'If cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.VARCHARValue) Or cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.FLOATValue) Or cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.MONEYValue) Or (cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.REALValue)) Or (cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.VarBinaryValue)) Then

            '    txtFieldSize.Hidden = False
            'Else
            '    txtFieldSize.Hidden = True
            'End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub


    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Function IsDataValid() As Boolean
        'todohendra:IsDataValid
        Return True
    End Function

    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

End Class
