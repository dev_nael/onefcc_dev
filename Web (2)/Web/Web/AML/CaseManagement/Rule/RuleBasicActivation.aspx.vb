﻿Imports Ext.Net
Imports CasemanagementDAL
Imports CasemanagementBLL
Imports System.Data.SqlClient
Imports System.Data
Partial Class OneFcc_Rule_RuleBasicActivation
    Inherits ParentPage
    Public objFormModuleAdd As NawaBLL.FormModuleAdd
    Public Property objRuleBasic() As OneFcc_MS_Rule_Basic
        Get
            If Session("RuleBasicEdit.objRuleBasic") Is Nothing Then
                Dim strid As String = Request.Params("ID")
                Dim id As String = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim oNewObj As OneFcc_MS_Rule_Basic = RuleBasicBLL.GetRuleBasic(id)
                Session("RuleBasicEdit.objRuleBasic") = oNewObj
            End If
            Return Session("RuleBasicEdit.objRuleBasic")
        End Get
        Set(ByVal value As OneFcc_MS_Rule_Basic)
            Session("RuleBasicEdit.objRuleBasic") = value
        End Set
    End Property

    Public Property objRuleBasicDetail() As List(Of OneFcc_MS_Rule_Basic_Detail)
        Get
            If Session("RuleBasicEdit.objRuleBasicDetail") Is Nothing Then
                Dim oNewObj As New List(Of OneFcc_MS_Rule_Basic_Detail)
                Session("RuleBasicEdit.objRuleBasicDetail") = oNewObj
            End If
            Return Session("RuleBasicEdit.objRuleBasicDetail")
        End Get
        Set(ByVal value As List(Of OneFcc_MS_Rule_Basic_Detail))
            Session("RuleBasicEdit.objRuleBasicDetail") = value
        End Set
    End Property

    Public Property objRuleBasicDetailEdit() As OneFcc_MS_Rule_Basic_Detail
        Get
            Return Session("RuleBasicEdit.objRuleBasicDetailEdit")
        End Get
        Set(ByVal value As OneFcc_MS_Rule_Basic_Detail)
            Session("RuleBasicEdit.objRuleBasicDetailEdit") = value
        End Set
    End Property
    Public Property RulesName() As String
        Get
            Return Session("RulesBasicEdit.RulesName")
        End Get
        Set(ByVal value As String)
            Session("RulesBasicEdit.RulesName") = value
        End Set
    End Property

    Public Property RulesTemplateId() As Integer
        Get
            Return Session("RulesBasicEdit.RulesTemplateId")
        End Get
        Set(ByVal value As Integer)
            Session("RulesBasicEdit.RulesTemplateId") = value
        End Set
    End Property

    Private Sub AML_RulesAdvanced_Add_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleAdd = New NawaBLL.FormModuleAdd(FormPanelInput, Panelconfirmation, LblConfirmation)
    End Sub

    Public Property Pk_RulesBasic_Id() As Long
        Get
            Return Session("RulesBasicEdit.Pk_RulesBasic_Id")
        End Get
        Set(ByVal value As Long)
            Session("RulesBasicEdit.Pk_RulesBasic_Id") = value
        End Set
    End Property
    Public Property SegmentCode() As String
        Get
            Return Session("RulesBasicEdit.SegmentCode")
        End Get
        Set(ByVal value As String)
            Session("RulesBasicEdit.SegmentCode") = value
        End Set
    End Property
    Public Property CategoryParameterID() As Integer
        Get
            Return Session("RulesBasicEdit.CategoryParameterID")
        End Get
        Set(ByVal value As Integer)
            Session("RulesBasicEdit.CategoryParameterID") = value
        End Set
    End Property
    Public Property TemplateCategoryID() As Integer
        Get
            Return Session("RulesBasicEdit.TemplateCategoryID")
        End Get
        Set(ByVal value As Integer)
            Session("RulesBasicEdit.TemplateCategoryID") = value
        End Set
    End Property


    Public Function ExtMultiCombo(pn As FormPanel, strLabel As String, strFieldName As String, bRequired As Boolean, intgridpos As Integer, strTableRef As String, strFieldKey As String, strFieldDisplay As String, strFilterField As String, strTableRefAlias As String, strvalue As String) As NDS.NDSDropDownFieldCombination

        Dim objPanel As New Ext.Net.Panel
        objPanel.ID = "Pnl" & strFieldName
        objPanel.AnchorHorizontal = "80%"
        objPanel.Layout = "AnchorLayout"
        Dim objmulticombo As New NDS.NDSDropDownFieldCombination
        objmulticombo.ID = strFieldName
        objmulticombo.AnchorHorizontal = "80%"
        objmulticombo.Label = strLabel
        objmulticombo.StringField = strFieldKey & "," & strFieldDisplay
        objmulticombo.StringTable = strTableRef
        objmulticombo.IDPropertyUnik = strFieldKey
        objmulticombo.Tipe = "checkbox"
        objmulticombo.AllowBlank = Not bRequired

        objPanel.ContentControls.Add(objmulticombo)


        pn.Add(objPanel)
        If Not Ext.Net.X.IsAjaxRequest Then
            Dim strquery As String = "SELECT ISNULL(STUFF((SELECT ',' + " & strFieldDisplay & " +'' FROM  " & strTableRef & "  WHERE " & strFieldKey & " IN (" & strvalue & ") FOR XML PATH('')), 1, 1, ''),'') Datadisplay"
            Dim strdisplay As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquery, Nothing)

            objmulticombo.SetTextValue(strvalue, strdisplay)

        End If


    End Function

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()
                FormPanelInput.Title = ObjModule.ModuleLabel & " Activation"
                Panelconfirmation.Title = ObjModule.ModuleLabel & " Activation"

                Dim strID As String = Request.Params("ID")
                If strID Is Nothing Then
                    Pk_RulesBasic_Id = 0
                Else
                    Pk_RulesBasic_Id = NawaBLL.Common.DecryptQueryString(strID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If

                'Using ObjDb As New NawaDatadevEntities
                '    Dim objRuleBasic As OneFcc_MS_Rule_Basic = ObjDb.OneFcc_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = Pk_RulesBasic_Id).FirstOrDefault
                '    If Not objRuleBasic Is Nothing Then
                '        RulesName = objRuleBasic.Rule_Basic_Name
                '        RulesTemplateId = objRuleBasic.FK_Rule_Template_ID

                '        If RulesTemplateId <> 0 Then
                '            TxtRulesBasicName.Text = RulesName

                '            Dim objRuleTemplate As OneFcc_MS_Rule_Template = ObjDb.OneFcc_MS_Rule_Template.Where(Function(x) x.PK_Rule_Template_ID = objRuleBasic.FK_Rule_Template_ID).FirstOrDefault
                '            If Not objRuleTemplate Is Nothing Then
                '                TxtRuleTemplate.Text = objRuleTemplate.Rule_Template_Name
                '            End If
                '        End If
                '    End If
                'End Using
                LoadData()

                '23-Aug-2022 Adi : Tampilkan lagi Rule Category Parameter (Transaction/Activity)
                'TxtCategoryParameter.Hidden = True
                TxtTemplateCategory.Hidden = True

            End If
            LoadFieldParameter()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Informasi", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadData()
        Using objdb As New CasemanagementDAL.CasemanagementEntities
            With objRuleBasic
                RulesName = .Rule_Basic_Name
                RulesTemplateId = .FK_Rule_Template_ID
                'SegmentCode = .Segment_Code
                CategoryParameterID = .FK_Template_Category_Parameter
                TemplateCategoryID = .FK_Template_Category
                txt_Transaction_Period.Value = .Transaction_Period


                If RulesTemplateId <> 0 Then
                    TxtRulesBasicName.Text = RulesName
                    Dim objRuleTemplate As OneFcc_MS_Rule_Template = objdb.OneFcc_MS_Rule_Template.Where(Function(x) x.PK_Rule_Template_ID = objRuleBasic.FK_Rule_Template_ID).FirstOrDefault
                    'Dim objSegment As OneFcc_MS_Segment = objdb.OneFcc_MS_Segment.Where(Function(x) x.Segment_Code = objRuleBasic.Segment_Code).FirstOrDefault
                    'Dim objCategoryParameter As OneFcc_MS_Template_Category_Parameter = objdb.OneFcc_MS_Template_Category_Parameter.Where(Function(x) x.PK_Template_Category_Parameter = objRuleBasic.FK_Template_Category_Parameter).FirstOrDefault
                    'Dim objTemplateCategory As OneFcc_MS_Rule_Template_Category = objdb.OneFcc_MS_Rule_Template_Category.Where(Function(x) x.PK_Rule_Template_Category_ID = objRuleBasic.FK_Template_Category).FirstOrDefault
                    TxtRulesTemplate.Text = objRuleTemplate.Rule_Template_Name
                    'If objSegment IsNot Nothing Then
                    '    TxtSegment.Text = objSegment.Segment_Name
                    'End If

                    'If objCategoryParameter IsNot Nothing Then
                    '    TxtCategoryParameter.Text = objCategoryParameter.Template_Category_Parameter_Name
                    'End If
                    'If objTemplateCategory IsNot Nothing Then
                    '    TxtTemplateCategory.Text = objTemplateCategory.Rule_Template_Category_Name
                    'End If

                    If Not IsNothing(objRuleTemplate.FK_Template_Category_Parameter) Then
                        Dim drTemplateCategory As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, System.Data.CommandType.Text, "SELECT TOP 1 * FROM OneFCC_MS_Template_Category_Parameter WHERE PK_Template_Category_Parameter=" & objRuleTemplate.FK_Template_Category_Parameter, Nothing)
                        If drTemplateCategory IsNot Nothing Then
                            TxtCategoryParameter.Value = drTemplateCategory("Template_Category_Parameter_Name")
                        End If
                    End If


                    If .Active = True Then
                        TxtActive.Text = "True > False"
                    Else
                        TxtActive.Text = "False > True"
                    End If
                End If

            End With
        End Using
    End Sub
    Protected Sub LoadFieldParameter()
        Dim PkRuleTemplateId As Long = RulesTemplateId
        Dim IntSequence As Integer = 1

        Using objdb As New CasemanagementDAL.CasemanagementEntities
            For Each item As OneFcc_MS_Rule_Template_Detail In objdb.OneFcc_MS_Rule_Template_Detail.Where(Function(x) x.FK_Rule_Template_ID = PkRuleTemplateId).ToList
                Dim objRuleBasicDetail As OneFcc_MS_Rule_Basic_Detail = objdb.OneFcc_MS_Rule_Basic_Detail.Where(Function(x) x.FK_Rule_Basic_ID = Pk_RulesBasic_Id And x.Variable_Name.ToUpper = item.Variable_Name.Replace("@", "").ToUpper).FirstOrDefault
                If Not objRuleBasicDetail Is Nothing Then
                    Select Case CType(item.FK_ExtType_ID, EnumExtType)
                        Case EnumExtType.TextField
                            ExtText(FormParameter, item.Variable_Description, item.Variable_Name.Replace("@", ""), True, 8000, IntSequence, objRuleBasicDetail.Value_Data)
                        Case EnumExtType.NumberField
                            ExtNumber(FormParameter, item.Variable_Description, item.Variable_Name.Replace("@", ""), True, 0, IntSequence, Long.MinValue, Long.MaxValue, objRuleBasicDetail.Value_Data)
                        Case EnumExtType.DateField
                            ExtDate(FormParameter, item.Variable_Description, item.Variable_Name.Replace("@", ""), True, 150, IntSequence, objRuleBasicDetail.Value_Data)
                        Case EnumExtType.DropDownField
                            ExtCombo(FormParameter, item.Variable_Description, item.Variable_Name.Replace("@", ""), True, IntSequence, item.Tabel_Reference_Name, item.Table_Reference_Field_Key, item.Table_Reference_Field_Display_Name, item.Table_Reference_Filter, item.Tabel_Reference_Name_Alias, objRuleBasicDetail.Value_Data)

                        Case EnumExtType.MultiCombo
                            ExtMultiCombo(FormParameter, item.Variable_Description, item.Variable_Name.Replace("@", ""), True, IntSequence, item.Tabel_Reference_Name, item.Table_Reference_Field_Key, item.Table_Reference_Field_Display_Name, item.Table_Reference_Filter, item.Tabel_Reference_Name_Alias, objRuleBasicDetail.Value_Data)


                    End Select
                    IntSequence += 1

                End If
            Next
        End Using
    End Sub

    Enum EnumExtType
        DateField = 1
        DropDownField = 2
        NumberField = 4
        TextField = 5
        Radio = 6
        DisplayField = 7
        FileUpload = 8
        Password = 9
        HTMLEditor = 10
        FormulaField = 11
        MultiCombo = 12
    End Enum

    Public Function ExtCombo(pn As FormPanel, strLabel As String, strFieldName As String, bRequired As Boolean, intgridpos As Integer, strTableRef As String, strFieldKey As String, strFieldDisplay As String, strFilterField As String, strTableRefAlias As String, strValue As String) As Ext.Net.ComboBox
        Using objcombo As New Ext.Net.ComboBox
            objcombo.ID = strFieldName
            objcombo.ClientIDMode = Web.UI.ClientIDMode.Static
            If Not Ext.Net.X.IsAjaxRequest Then
                objcombo.FieldLabel = strLabel

                objcombo.LabelWidth = 150 'intLabelWidth
                objcombo.AnchorHorizontal = "80%"
                objcombo.Name = strFieldName
                objcombo.AllowBlank = Not bRequired
                objcombo.BlankText = strLabel & " is required."
                objcombo.Width = objcombo.LabelWidth + 150
                objcombo.MatchFieldWidth = True
                objcombo.MinChars = "0"
                objcombo.ForceSelection = True

                'objcombo.TypeAhead = True
                'objcombo.EnableRegEx = True
                objcombo.AnyMatch = True

                objcombo.QueryMode = DataLoadMode.Local
                objcombo.ValueField = strFieldKey
                objcombo.DisplayField = strFieldDisplay
                objcombo.TriggerAction = Ext.Net.TriggerAction.All

                Dim objFieldtrigger As New Ext.Net.FieldTrigger
                objFieldtrigger.Icon = Ext.Net.TriggerIcon.Clear
                objFieldtrigger.Hidden = True
                objFieldtrigger.Weight = "-1"
                objcombo.Triggers.Add(objFieldtrigger)


                objcombo.Listeners.Select.Handler = "this.getTrigger(0).show();"

                objcombo.Listeners.TriggerClick.Handler = "if (index == 0) {  this.clearValue(); this.getTrigger(0).hide();}"



                'buat store dan modelnya

                Using objStore As New Ext.Net.Store
                    objStore.ID = "_Store_" + objcombo.ID
                    objStore.ClientIDMode = Web.UI.ClientIDMode.Static


                    Using objModel As New Ext.Net.Model
                        objModel.Fields.Add(strFieldKey, Ext.Net.ModelFieldType.String)
                        objModel.Fields.Add(strFieldDisplay, Ext.Net.ModelFieldType.String)
                        objStore.Model.Add(objModel)
                    End Using


                    objcombo.Store.Add(objStore)
                    objStore.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, System.Data.CommandType.Text, GetQueryRef(strTableRef & " " & strTableRefAlias, strFieldKey, strFieldDisplay, strFilterField), Nothing)
                    objStore.DataBind()
                End Using

                objcombo.SelectedItem.Value = strValue

                objcombo.ReadOnly = True
            End If

            pn.Add(objcombo)
            Return objcombo
        End Using
    End Function

    Function GetQueryRef(strTable As String, strfieldkey As String, strfielddisplay As String, strfilter As String) As String
        Dim strquery As String
        strquery = "select " & strfieldkey & ", convert(Varchar(1000),[" & strfieldkey & "])+ ' - '+ convert(varchar(1000), [" & strfielddisplay & "]) as [" & strfielddisplay & "] from " & strTable

        strfilter = strfilter.Replace("@userid", NawaBLL.Common.SessionCurrentUser.UserID)
        strfilter = strfilter.Replace("@PK_MUser_ID", NawaBLL.Common.SessionCurrentUser.PK_MUser_ID)

        If strfilter.Trim.Length > 0 Then
            strquery = strquery & " where " & strfilter
        End If
        Return strquery
    End Function

    Public Function ExtDate(pn As FormPanel, strLabel As String, strFieldName As String, bRequired As Boolean, intMaxSize As Integer, intgridpos As Integer, strValue As String) As Ext.Net.DateField
        Dim objDateField As New Ext.Net.DateField
        objDateField.ID = strFieldName
        objDateField.ClientIDMode = Web.UI.ClientIDMode.Static
        If Not Ext.Net.X.IsAjaxRequest Then
            objDateField.FieldLabel = strLabel
            objDateField.LabelStyle = "word-wrap: break-word"
            objDateField.LabelWidth = 150 'intLabelWidth
            objDateField.Name = strFieldName
            objDateField.AllowBlank = Not bRequired
            objDateField.BlankText = strLabel & " is required."
            'objDateField.MaxLength = intMaxSize
            objDateField.Width = objDateField.LabelWidth + 150
            objDateField.Format = NawaBLL.SystemParameterBLL.GetDateFormat
            objDateField.AnchorHorizontal = "40%"

            objDateField.Value = strValue

            objDateField.ReadOnly = True
        End If
        pn.Add(objDateField)
        Return objDateField
    End Function

    Public Function ExtNumber(pn As FormPanel, strLabel As String, strFieldName As String, bRequired As Boolean, intDecimalPrecition As Integer, intgridpos As Integer, dminvalue As Double, dmaxvalue As Double, strValue As String) As Ext.Net.NumberField
        Dim objNumberField As New Ext.Net.NumberField
        objNumberField.ID = strFieldName
        objNumberField.ClientIDMode = Web.UI.ClientIDMode.Static
        If Not Ext.Net.X.IsAjaxRequest Then
            objNumberField.FieldLabel = strLabel
            objNumberField.LabelStyle = "word-wrap: break-word"
            objNumberField.LabelWidth = 150 'intLabelWidth
            objNumberField.Name = strFieldName
            objNumberField.AllowBlank = Not bRequired
            objNumberField.BlankText = strLabel & " is required."
            objNumberField.DecimalPrecision = intDecimalPrecition
            objNumberField.MinValue = dminvalue
            objNumberField.MaxValue = dmaxvalue
            objNumberField.Width = objNumberField.LabelWidth + 150
            objNumberField.AnchorHorizontal = "40%"

            objNumberField.Text = strValue

            objNumberField.ReadOnly = True

        End If
        pn.Add(objNumberField)
        Return objNumberField
    End Function

    Public Function ExtText(pn As FormPanel, strLabel As String, strFieldName As String, bRequired As Boolean, intMaxSize As Integer, intgridpos As Integer, strValue As String) As Ext.Net.TextField
        Dim objDateField As New Ext.Net.TextField
        objDateField.ID = strFieldName
        objDateField.ClientIDMode = Web.UI.ClientIDMode.Static
        If Not Ext.Net.X.IsAjaxRequest Then
            objDateField.FieldLabel = strLabel

            objDateField.Name = strFieldName
            objDateField.AllowBlank = Not bRequired
            objDateField.BlankText = strLabel & " is required."
            objDateField.MaxLength = intMaxSize

            objDateField.AnchorHorizontal = "80%"

            objDateField.Text = strValue

            objDateField.ReadOnly = True
        End If
        pn.Add(objDateField)
        Return objDateField
    End Function


    Private Sub ClearSession()
        'RulesName = Nothing
        'RulesTemplateId = 0
        objRuleBasic = Nothing
        objRuleBasicDetail = Nothing
        objRuleBasicDetailEdit = Nothing
    End Sub

    Protected Sub Callback(sender As Object, e As DirectEventArgs)

        Dim LstVariableName As New List(Of String)
        Dim LstValueData As New List(Of String)
        Dim LstVariableDescription As New List(Of String)

        Try
            Using ObjDb As New CasemanagementDAL.CasemanagementEntities
                'If Pk_RulesBasic_Id <> 0 Then
                '    Dim ObjModuleApproval As ModuleApproval = ObjDb.ModuleApprovals.Where(Function(x) x.ModuleName = "AML_Ms_RuleBasic" And x.ModuleKey = Pk_RulesBasic_Id).FirstOrDefault
                '    If Not ObjModuleApproval Is Nothing Then
                '        Throw New Exception("Data tidak dapat diedit karena sedang menunggu approval")
                '    End If
                'End If

                'If TxtRulesBasicName.Text.Trim <> "" Then
                '    Dim ObjAMLRules As List(Of AML_Ms_RuleBasic) = ObjDb.AML_Ms_RuleBasic.Where(Function(x) x.AML_Ms_RuleBasicName.ToUpper = TxtRulesBasicName.Text.Trim.ToUpper).ToList
                '    If ObjAMLRules.Count > 0 Then
                '        Throw New ApplicationException("Rules Name sudah digunakan")
                '    End If
                'End If

                Dim PkRuleTemplateId As Long = RulesTemplateId
                Dim IntSequence As Integer = 1

                For Each item As OneFcc_MS_Rule_Template_Detail In ObjDb.OneFcc_MS_Rule_Template_Detail.Where(Function(x) x.FK_Rule_Template_ID = PkRuleTemplateId).ToList
                    Select Case CType(item.FK_ExtType_ID, EnumExtType)
                        Case EnumExtType.TextField
                            Dim objfield As TextField = FormParameter.FindControl(item.Variable_Name.Replace("@", ""))
                            If Not objfield Is Nothing Then

                                LstVariableName.Add(item.Variable_Name.Replace("@", ""))
                                LstValueData.Add(System.Web.HttpContext.Current.Server.HtmlEncode(IIf(objfield.Text Is Nothing, "", objfield.Text.Trim)))
                                LstVariableDescription.Add(item.Variable_Description)
                            End If
                        Case EnumExtType.NumberField
                            Dim objfield As NumberField = FormParameter.FindControl(item.Variable_Name.Replace("@", ""))
                            If Not objfield Is Nothing Then

                                LstVariableName.Add(item.Variable_Name.Replace("@", ""))
                                LstValueData.Add(objfield.Value)
                                LstVariableDescription.Add(item.Variable_Description)
                            End If
                        Case EnumExtType.DateField
                            Dim objfield As DateField = FormParameter.FindControl(item.Variable_Name.Replace("@", ""))
                            If Not objfield Is Nothing Then

                                LstVariableName.Add(item.Variable_Name.Replace("@", ""))
                                LstValueData.Add(NawaBLL.Common.ConvertToDate(NawaBLL.SystemParameterBLL.GetDateFormat, objfield.RawText).ToString("yyyy-MM-dd"))
                                LstVariableDescription.Add(item.Variable_Description)
                            End If
                        Case EnumExtType.DropDownField
                            Dim objfield As ComboBox = FormParameter.FindControl(item.Variable_Name.Replace("@", ""))
                            If Not objfield Is Nothing Then

                                LstVariableName.Add(item.Variable_Name.Replace("@", ""))
                                LstValueData.Add(objfield.SelectedItem.Value)
                                LstVariableDescription.Add(item.Variable_Description)
                            End If
                        Case EnumExtType.MultiCombo
                            Dim objfield As NDS.NDSDropDownFieldCombination = FormParameter.FindControl(item.Variable_Name.Replace("@", ""))
                            If Not objfield Is Nothing Then
                                LstVariableName.Add(item.Variable_Name.Replace("@", ""))
                                LstValueData.Add(objfield.TextValue)
                                LstVariableDescription.Add(item.Variable_Description)
                            End If
                    End Select
                    IntSequence += 1
                Next

                Dim NewBll As New RuleBasicBLL

                Dim IsProcess As Boolean = True
                If objRuleBasic.Active = True Then
                    IsProcess = False
                Else
                    IsProcess = True
                End If
                NewBll.ActivationDocument(objRuleBasic, IsProcess, ObjModule)

                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Activation into Database"

            End Using


        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Informasi", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_Click()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Function IsDataValid() As Boolean
        Return True
    End Function

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Informasi", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AML_RulesAdvanced_Add_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Update
    End Sub
End Class
