﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="RuleBasicActivation.aspx.vb" Inherits="OneFcc_Rule_RuleBasicActivation" %>

<%--<%@ Register Src="~/NDSDropDownFieldCombination.ascx" TagPrefix="uc1" TagName="NDSDropDownFieldCombination" %>--%>
<%@ Register TagPrefix="NDS" Namespace="NDS" Assembly="App_Web_ndsdropdownfieldcombination.ascx.43883e39" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:FormPanel ID="FormPanelInput" runat="server" ButtonAlign="Center" Title="" BodyStyle="padding:20px" AutoScroll="true">
        <Items>
          <ext:DisplayField ID="TxtRulesBasicName" runat="server" FieldLabel="Rules Name" AnchorHorizontal="85%" AllowBlank="false" />
            <ext:DisplayField ID="TxtRulesTemplate" runat="server" FieldLabel="Rules Template" AnchorHorizontal="85%" AllowBlank="false" />
            <ext:DisplayField ID="TxtSegment" runat="server" FieldLabel="Segment" AnchorHorizontal="85%" AllowBlank="false" hidden =" true"/>
            <ext:DisplayField ID="TxtCategoryParameter" runat="server" FieldLabel="Template Category" AnchorHorizontal="85%" AllowBlank="false" />
            <ext:DisplayField ID="TxtTemplateCategory" runat="server" FieldLabel="Template Category" AnchorHorizontal="85%" AllowBlank="false" />
            <ext:DisplayField ID="txt_Transaction_Period" runat="server" FieldLabel="Days to Alert same Case" AnchorHorizontal="85%" AllowBlank="false" />

            <ext:DisplayField ID="TxtActive" runat="server" FieldLabel="Active" AnchorHorizontal="85%" AllowBlank="false" />
           
            <ext:DisplayField ID="LblBlank" runat="server" FieldLabel="" Text="" EmptyText="" AutoScroll="true" AnchorHorizontal="90%" ClientIDMode="Static">
            </ext:DisplayField>
            <ext:FormPanel ID="FormParameter" Title="Parameter" BodyPadding="20" runat="server" ClientIDMode="Static" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true">
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="Button1" ClientIDMode="Static" runat="server" Text="Activation" Enabled="false" Icon="DiskBlack">
                <Listeners>
                    <Click Handler="if (!#{FormPanelInput}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="Callback">
                        <ExtraParams>
                            <ext:Parameter Name="command" Value="New" Mode="Value">
                            </ext:Parameter>

                        </ExtraParams>
                        <EventMask ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>

        </Items>

        <Buttons>

            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>

