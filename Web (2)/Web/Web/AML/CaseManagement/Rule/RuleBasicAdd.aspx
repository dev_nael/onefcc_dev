﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="RuleBasicAdd.aspx.vb" Inherits="OneFcc_Rule_RuleBasicAdd" %>
<%--<%@ Register Src="~/NDSDropDownFieldCombination.ascx" TagPrefix="uc1" TagName="NDSDropDownFieldCombination" %>--%>
<%--<%@ Register TagPrefix="NDS" Namespace="NDS" Assembly="App_Web_ndsdropdownfieldcombination.ascx.43883e39" %>--%> 
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:FormPanel ID="FormPanelInput" runat="server" Title="Rule Basic" BodyStyle="padding:20px" AutoScroll="true" Hidden="false" ButtonAlign="Center">
        <Content>
            <ext:TextField ID="TxtRulesBasicName" runat="server" FieldLabel="Rules Name" AnchorHorizontal="80%" AllowBlank="false" />
            <%--<ext:ComboBox ID="CboRuleTemplate" runat="server" FieldLabel="Rules Template" DisplayField="Rule_Template_Name"
                ValueField="PK_Rule_Template_ID" TriggerAction="All" ForceSelection="True" AllowBlank="false" AnchorHorizontal="85%">
                <Store>
                    <ext:Store runat="server" ID="StoreRuleTemplate">
                        <Model>
                            <ext:Model ID="Model1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="PK_Rule_Template_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Rule_Template_Name" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Select Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                    this.clearValue();
                                    this.getTrigger(0).hide();
                                }" />
                </Listeners>
                <DirectEvents>
                    <Change OnEvent="CboRuleTemplate_Changed">
                        <EventMask Msg="Loading..." MinDelay="100" ShowMask="true"></EventMask>
                    </Change>
                </DirectEvents>
            </ext:ComboBox>--%>

            <NDS:NDSDropDownField ID="CboRuleTemplate" LabelWidth="150" ValueField="PK_Rule_Template_ID" DisplayField="Rule_Template_Name" runat="server" StringField="PK_Rule_Template_ID, Rule_Template_Name" StringTable="OneFcc_MS_Rule_Template" Label="Rule Template" AnchorHorizontal="80%" AllowBlank="false" OnOnValueChanged="CboRuleTemplate_Changed" />

            
            <ext:ComboBox ID="cboCategoryCaseManagement" runat="server" FieldLabel=" Category Case Management" DisplayField="Category_Case_Management_Name" ValueField="PK_Category_Case_Management_ID" AnchorHorizontal="85%" MinChars="0" ForceSelection="true" TriggerAction="Query" AllowBlank="false" BlankText="Category Case Management By is required">
                        <Store>
                            <ext:Store ID="StoreCategoryCaseManagement" runat="server" IsPagingStore="true" >
                                <Model>
                                    <ext:Model ID="ModelCategoryCaseManagement" runat="server">
                                        <Fields>                                            
                                            <ext:ModelField Name="PK_Category_Case_Management_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Category_Case_Management_Name" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                         <DirectEvents>
                            <Change OnEvent="cboCategoryCaseManagement_Changed">
                                <EventMask Msg="Loading..." MinDelay="100" ShowMask="true"></EventMask>
                            </Change>
                        </DirectEvents>
                </ext:ComboBox>

                 <ext:ComboBox ID="cboJenisLaporan" runat="server" FieldLabel="Jenis Laporan" DisplayField="Keterangan" ValueField="Kode" AnchorHorizontal="85%" MinChars="0" ForceSelection="true" TriggerAction="Query" AllowBlank="true" Hidden="true">
                            <Store>
                                <ext:Store ID="StoreJenisLaporan" runat="server" IsPagingStore="true" >
                                    <Model>
                                        <ext:Model ID="ModelJenisLaporan" runat="server">
                                            <Fields>                                            
                                                <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <TriggerClick Handler="if (index == 0) {
                                               this.clearValue();
                                               this.getTrigger(0).hide();
                                           }" />
                            </Listeners>
                    </ext:ComboBox>


                 <%--<ext:ComboBox ID="cboCatagoryParameter" runat="server" FieldLabel="Category Parameter" DisplayField="Template_Category_Parameter_Name" ValueField="PK_Template_Category_Parameter" AnchorHorizontal="85%" MinChars="0" ForceSelection="true" TriggerAction="Query" AllowBlank="false" BlankText="Category Parameter By is required">--%>
                 <ext:ComboBox ID="cboCatagoryParameter" runat="server" FieldLabel="Category Parameter" DisplayField="Template_Category_Parameter_Name" ValueField="PK_Template_Category_Parameter" AnchorHorizontal="80%" MinChars="0" ForceSelection="true" TriggerAction="Query" AllowBlank="false" ReadOnly="true">
                            <Store>
                                <ext:Store ID="StoreCatagoryParameter" runat="server" IsPagingStore="true" >
                                    <Model>
                                        <ext:Model ID="ModelCatagoryParameter" runat="server">
                                            <Fields>                                            
                                                <ext:ModelField Name="PK_Template_Category_Parameter" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="Template_Category_Parameter_Name" Type="String"></ext:ModelField>
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Change OnEvent="cboCatagoryParameter_Changed" />
                            </DirectEvents>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                            </Triggers>
                    </ext:ComboBox>


             <ext:ComboBox ID="cboCategoryTemplate" runat="server" FieldLabel="Template Category" DisplayField="Rule_Template_Category_Name" ValueField="PK_Rule_Template_Category_ID" AnchorHorizontal="85%" MinChars="0" ForceSelection="true" TriggerAction="Query" AllowBlank="false" BlankText="Template Category By is required">
                    <Store>
                        <ext:Store ID="StoreCategoryTemplate" runat="server" IsPagingStore="true" >
                            <Model>
                                <ext:Model ID="ModelCategoryTemplate" runat="server">
                                    <Fields>                                            
                                        <ext:ModelField Name="PK_Rule_Template_Category_ID" Type="String"></ext:ModelField>
                                        <ext:ModelField Name="Rule_Template_Category_Name" Type="String"></ext:ModelField>
                                    </Fields>
                                </ext:Model>
                            </Model>
                                
                        </ext:Store>
                    </Store>
                    <Triggers>
                        <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                    </Triggers>
                    <Listeners>
                        <Select Handler="this.getTrigger(0).show();" />
                        <TriggerClick Handler="if (index == 0) {
                                        this.clearValue();
                                        this.getTrigger(0).hide();
                                    }" />
                    </Listeners>
                </ext:ComboBox>
                <ext:NumberField ID="txt_Transaction_Period" runat="server" FieldLabel="Days to Alert same Case" AnchorHorizontal="30%" AllowBlank="false" MaxLength="4" EnforceMaxLength="true" EmptyText="1-1000"></ext:NumberField>



            <ext:DisplayField ID="LblBlank" runat="server" FieldLabel="" Text="" EmptyText="" AutoScroll="true" AnchorHorizontal="90%" ClientIDMode="Static">
            </ext:DisplayField>
            <ext:FormPanel ID="FormParameter" Title="Parameter" BodyPadding="20" runat="server" ClientIDMode="Static" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" >

            </ext:FormPanel>
        </Content>
        <Buttons>
            <ext:Button ID="Button1" runat="server" Text="Save" Icon="DiskBlack">
                <Listeners>
                    <Click Handler="if (!#{FormPanelInput}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="Callback">
                        <%--<ExtraParams>
                            <ext:Parameter Name="command" Value="New" Mode="Value">
                            </ext:Parameter>

                        </ExtraParams>
                        <EventMask ShowMask="true"></EventMask>--%>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>

        </Items>

        <Buttons>

            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>



