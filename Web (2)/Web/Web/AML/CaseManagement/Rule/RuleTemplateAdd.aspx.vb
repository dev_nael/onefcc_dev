﻿Imports Ext.Net
Imports System.Data
Imports CasemanagementDAL
Imports CasemanagementBLL
Imports System.Data.SqlClient

Partial Class OneFcc_Rule_RuleTemplateAdd
    Inherits Parent

    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("RuleTemplateAdd.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("RuleTemplateAdd.ObjModule") = value
        End Set
    End Property

    Public Property objRuleTemplate() As OneFcc_MS_Rule_Template
        Get
            If Session("RuleTemplateAdd.objRuleTemplate") Is Nothing Then
                Dim oNewObj As New OneFcc_MS_Rule_Template
                Session("RuleTemplateAdd.objRuleTemplate") = oNewObj
            End If
            Return Session("RuleTemplateAdd.objRuleTemplate")
        End Get
        Set(ByVal value As OneFcc_MS_Rule_Template)
            Session("RuleTemplateAdd.objRuleTemplate") = value
        End Set
    End Property

    Public Property objRuleTemplateDetail() As List(Of OneFcc_MS_Rule_Template_Detail)
        Get
            If Session("RuleTemplateAdd.objRuleTemplateDetail") Is Nothing Then
                Dim oNewObj As New List(Of OneFcc_MS_Rule_Template_Detail)
                Session("RuleTemplateAdd.objRuleTemplateDetail") = oNewObj
            End If
            Return Session("RuleTemplateAdd.objRuleTemplateDetail")
        End Get
        Set(ByVal value As List(Of OneFcc_MS_Rule_Template_Detail))
            Session("RuleTemplateAdd.objRuleTemplateDetail") = value
        End Set
    End Property

    Public Property objRuleTemplateDetailEdit() As OneFcc_MS_Rule_Template_Detail
        Get
            Return Session("RulesBasicTemplateAdd.objRuleTemplateDetailEdit")
        End Get
        Set(ByVal value As OneFcc_MS_Rule_Template_Detail)
            Session("RulesBasicTemplateAdd.objRuleTemplateDetailEdit") = value
        End Set
    End Property

    '12-Apr-2022 Adi : Save DataTable Replacer in session for validating query
    Public Property dtReplacer() As DataTable
        Get
            Return Session("RuleTemplateEdit.dtReplacer")
        End Get
        Set(ByVal value As DataTable)
            Session("RuleTemplateEdit.dtReplacer") = value
        End Set
    End Property


    Private Sub MUserAdd_Init(sender As Object, e As EventArgs) Handles Me.Init
        'objRuleBasicTempateBll = New NawaDevBLL.RulesBasicTemplateBLL(FormPanelInput)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                Dim strmodule As String = Request.Params("ModuleID")
                ClearSession()
                Try
                    Dim intmodule As Integer = NawaBLL.Common.DecryptQueryString(strmodule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                    Me.ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intmodule)

                    If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Insert) Then
                        Dim strIDCode As String = 1
                        strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                        Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    FormPanelInput.Title = ObjModule.ModuleLabel & " Add"
                    Panelconfirmation.Title = ObjModule.ModuleLabel & " Add"
                    LoadDataSetting()



                    Dim objnavimagecommand As Ext.Net.CommandColumn = GridPanelDetail.ColumnModel.Columns.Find(Function(x) x.ID = "commandfield")

                    Dim objparamsettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
                    Dim bsettingright As Integer = 1
                    If Not objparamsettingbutton Is Nothing Then
                        bsettingright = objparamsettingbutton.SettingValue
                    End If

                    If bsettingright = 1 Then

                        GridPanelDetail.ColumnModel.Columns.Remove(objnavimagecommand)
                        GridPanelDetail.ColumnModel.Columns.Add(objnavimagecommand)
                    ElseIf bsettingright = 2 Then
                        GridPanelDetail.ColumnModel.Columns.Remove(objnavimagecommand)
                        GridPanelDetail.ColumnModel.Columns.Insert(1, objnavimagecommand)

                    End If
                Catch ex As Exception
                    Throw New Exception("Invalid Module ID")
                End Try
            End If


            'objRuleBasicTempateBll.Bentukform()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub StoreTableReference_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = "  TABLE_NAME like '%" & query & "%'"
            End If

            StoreTableReference.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("INFORMATION_SCHEMA.TABLES", "TABLE_NAME", strfilter, "TABLE_NAME", e.Start, e.Limit, e.Total)
            StoreTableReference.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub StoreTableReferenceKey_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = " COLUMN_NAME like '" & query & "%' and table_Name='" & cboTableReference.SelectedItem.Value & "'"
            Else
                strfilter = " table_Name='" & cboTableReference.SelectedItem.Value & "'"
            End If

            StoreTableReferenceKey.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("INFORMATION_SCHEMA.COLUMNS", "COLUMN_NAME", strfilter, "TABLE_NAME", e.Start, e.Limit, e.Total)
            StoreTableReferenceKey.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub StoreTableReferenceDisplayName_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = " COLUMN_NAME like '" & query & "%' and table_Name='" & cboTableReference.SelectedItem.Value & "'"
            Else
                strfilter = " table_Name='" & cboTableReference.SelectedItem.Value & "'"
            End If

            StoreTableReferenceDisplayName.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("INFORMATION_SCHEMA.COLUMNS", "COLUMN_NAME", strfilter, "TABLE_NAME", e.Start, e.Limit, e.Total)
            StoreTableReferenceDisplayName.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub LoadDataSetting()
        cboFieldType.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        cboExtType.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreextType.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreFieldType.PageSize = NawaBLL.SystemParameterBLL.GetPageSize

        StoreCaseGroupingBy.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT Case_Grouping FROM OneFcc_MS_Rule_Case_Grouping ", Nothing)
        StoreCaseGroupingBy.DataBind()
        CboCaseGroupingBy.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreCaseGroupingBy.PageSize = NawaBLL.SystemParameterBLL.GetPageSize

        '19-Aug-2022 Adi : Penambahan Rule Template Category
        storeTemplateCategory.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT PK_Template_Category_Parameter, Template_Category_Parameter_Name FROM OneFCC_MS_Template_Category_Parameter", Nothing)
        storeTemplateCategory.DataBind()
        cboTemplateCategory.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        storeTemplateCategory.PageSize = NawaBLL.SystemParameterBLL.GetPageSize

    End Sub

    Sub ClearSession()
        objRuleTemplate = Nothing
        objRuleTemplateDetail = Nothing
        objRuleTemplateDetailEdit = Nothing
    End Sub

    Protected Sub StoreextType_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = " ExtTypeName like '" & query & "%'"
            End If

            StoreextType.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("MExtType", "PK_ExtType_ID,ExtTypeName", strfilter, "PK_ExtType_ID", e.Start, e.Limit, e.Total)
            StoreextType.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub StoreFieldType_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = " FieldTypeCaption like '" & query & "%'"
            End If
            StoreFieldType.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("MFieldType", "PK_FieldType_ID,FieldTypeCaption", strfilter, "PK_FieldType_ID", e.Start, e.Limit, e.Total)
            StoreFieldType.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnCancelField_Directclick(sender As Object, e As DirectEventArgs)
        Try

            btnsaveField.Text = "Add Replacer"
            ClearDetail()
            objRuleTemplateDetailEdit = Nothing
            WindowDetail.Close()
            BindDetail()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Function IsdatavalidField() As Boolean

        'done:IsdatavalidField
        If cboFieldType.SelectedItem.Value = "11" And cboTableReference.SelectedItem.Value Is Nothing Then
            'validasi table
            Throw New Exception("Table Reference Field Display Name is required.")
        End If
        Return True
    End Function

    Sub ClearDetail()
        'done: clearDetail
        txtvariableName.Clear()
        txtvariableDesc.Clear()
        cboExtType.Clear()
        cboFieldType.Clear()
        cboTableReference.Clear()
        txtTableAlias.Clear()
        CboTableReferencKey.Clear()
        CboTableReferenceDisplayName.Clear()
        txtTableReferenceAdditionalJoin.Clear()
        txtTableReferenceFilter.Clear()

    End Sub

    Sub BindDetail()
        'done:BindDetail grid field

        '12-Apr-2022 Adi
        'Dim objdt As System.Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objRuleTemplateDetail)
        Me.dtReplacer = NawaBLL.Common.CopyGenericToDataTable(objRuleTemplateDetail)

        'objdt = NawaFramework.GetDataTable(objdt, New Data.DataColumn("MFieldTypeName", GetType(String)))
        'objdt = NawaFramework.GetDataTable(objdt, New Data.DataColumn("MExtTypeName", GetType(String)))

        Dim objtblMFieldType As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT mt.PK_FieldType_ID, mt.FieldTypeCaption FROM MFieldType AS mt", Nothing)
        Dim objtblExtType As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT mt.PK_ExtType_ID, mt.ExtTypeName FROM MExtType AS mt", Nothing)

        '12-Apr-2022 Adi : Add column Variable Sample Value for validating
        Me.dtReplacer.Columns.Add(New DataColumn("Variable_Sample_Value", GetType(String)))

        'For Each item As Data.DataRow In objdt.Rows
        For Each item As Data.DataRow In dtReplacer.Rows
            item("MFieldTypeName") = objtblMFieldType.Select("PK_FieldType_ID=" & item("FK_FieldType_ID"))(0)("FieldTypeCaption").ToString
            item("MExtTypeName") = objtblExtType.Select("PK_ExtType_ID=" & item("FK_ExtType_ID"))(0)("ExtTypeName").ToString

            '12-Apr-2022 Adi : Add column Variable Sample Value for validating
            Select Case item("FK_FieldType_ID")
                Case 1, 2, 3, 4, 5, 6, 7, 8  'BIGINT,INT,SMALLINT,TINYINT,NUMERIC/DECIMAL,FLOAT,REAL,MONEY
                    item("Variable_Sample_Value") = 0
                Case 9      'VARCHAR
                    item("Variable_Sample_Value") = "'Sample Value'"
                Case 10     'DATETIME
                    item("Variable_Sample_Value") = "'" & DateTime.Now.ToString("yyyy-MM-dd") & "'"
                Case 11     'Reference Table
                    item("Variable_Sample_Value") = 0
                Case 12, 15     'IDENTITY, BIGIDENTITY
                    item("Variable_Sample_Value") = 0
                Case 13     'BOOLEAN
                    item("Variable_Sample_Value") = 0
                Case 14     'VARBINARY - no sample data
                    item("Variable_Sample_Value") = ""
            End Select
        Next

        StoreDetail.DataSource = dtReplacer
        StoreDetail.DataBind()

    End Sub

    Sub LoadDataEditModuleField(id As Long)
        'done:LoadDataEditModuleField
        WindowDetail.Hidden = False
        btnsaveField.Text = "Save Replacer"
        objRuleTemplateDetailEdit = objRuleTemplateDetail.Find(Function(x) x.PK_Rule_Template_Detail_ID = id)
        With objRuleTemplateDetailEdit
            txtvariableName.Text = .Variable_Name
            txtvariableDesc.Text = .Variable_Description
            Dim strfieldtypestring As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT mt.FieldTypeCaption FROM MFieldType AS mt WHERE mt.PK_FieldType_ID='" & .FK_FieldType_ID & "'", Nothing)
            Dim strfieldextstring As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT mt.ExtTypeName FROM MExtType AS mt WHERE mt.PK_ExtType_ID='" & .FK_ExtType_ID & "'", Nothing)

            cboExtType.DoQuery(strfieldextstring, True)
            cboExtType.SetValueAndFireSelect(.FK_ExtType_ID)

            cboFieldType.DoQuery(strfieldtypestring, True)
            cboFieldType.SetValueAndFireSelect(.FK_FieldType_ID)
            btnAddDetail.Focus()

            If cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.ReferenceTable) Then

                txtTableAlias.Text = .Tabel_Reference_Name_Alias


                cboTableReference.DoQuery(.Tabel_Reference_Name, True)
                cboTableReference.SetValueAndFireSelect(.Tabel_Reference_Name)
                cboTableReference.SelectedItem.Value = .Tabel_Reference_Name
                btnAddDetail.Focus()

                CboTableReferenceDisplayName.DoQuery(.Table_Reference_Field_Key, True)
                CboTableReferencKey.SetValueAndFireSelect(.Table_Reference_Field_Key)
                CboTableReferencKey.SelectedItem.Value = .Table_Reference_Field_Key
                btnAddDetail.Focus()
                CboTableReferenceDisplayName.DoQuery(.Table_Reference_Field_Display_Name, True)
                CboTableReferenceDisplayName.SetValueAndFireSelect(.Table_Reference_Field_Display_Name)
                CboTableReferenceDisplayName.SelectedItem.Value = .Table_Reference_Field_Display_Name
                btnAddDetail.Focus()
                txtTableReferenceAdditionalJoin.Text = .Table_Reference_Additonal_Join
                txtTableReferenceFilter.Text = .Table_Reference_Filter


            End If


        End With
    End Sub

    Sub DeleteModuleField(id As Long)
        'done:DeleteModuleField
        Dim objdelete As OneFcc_MS_Rule_Template_Detail = objRuleTemplateDetail.Find(Function(x) x.PK_Rule_Template_Detail_ID = id)
        If Not objdelete Is Nothing Then
            objRuleTemplateDetail.Remove(objdelete)
        End If
        BindDetail()
    End Sub

    Protected Sub CallBackModuleField(sender As Object, e As DirectEventArgs)
        'done:CallBackModuleField
        Dim id As Long = e.ExtraParams(0).Value

        Select Case e.ExtraParams("command")
            Case "Edit"
                LoadDataEditModuleField(id)
            Case "Delete"
                DeleteModuleField(id)

        End Select
    End Sub

    Protected Sub BtnSaveField_Directclick(sender As Object, e As DirectEventArgs)
        Try
            If IsdatavalidField() Then

                If Me.objRuleTemplateDetailEdit Is Nothing Then
                    Dim objNewTR As New OneFcc_MS_Rule_Template_Detail
                    With objNewTR
                        Dim objrand As New Random
                        Dim intpk As Long = objrand.Next(Integer.MinValue, -1)
                        While Not objRuleTemplateDetail.Find(Function(x) x.PK_Rule_Template_Detail_ID = intpk) Is Nothing
                            intpk = objrand.Next(Integer.MinValue, -1)
                        End While

                        .PK_Rule_Template_Detail_ID = intpk
                        .Variable_Name = txtvariableName.Text.Trim
                        .Variable_Description = txtvariableDesc.Text.Trim
                        .FK_FieldType_ID = cboFieldType.SelectedItem.Value
                        .FK_ExtType_ID = cboExtType.SelectedItem.Value
                        .Tabel_Reference_Name = cboTableReference.SelectedItem.Value
                        .Tabel_Reference_Name_Alias = txtTableAlias.Text.Trim
                        .Table_Reference_Field_Key = CboTableReferencKey.SelectedItem.Value
                        .Table_Reference_Field_Display_Name = CboTableReferenceDisplayName.SelectedItem.Value
                        .Table_Reference_Filter = txtTableReferenceFilter.Text.Trim
                        .Table_Reference_Additonal_Join = txtTableReferenceAdditionalJoin.Text.Trim
                    End With
                    objRuleTemplateDetail.Add(objNewTR)
                Else

                    With objRuleTemplateDetailEdit

                        .Variable_Name = txtvariableName.Text.Trim
                        .Variable_Description = txtvariableDesc.Text.Trim
                        .FK_FieldType_ID = cboFieldType.SelectedItem.Value
                        .FK_ExtType_ID = cboExtType.SelectedItem.Value
                        .Tabel_Reference_Name = cboTableReference.SelectedItem.Value
                        .Tabel_Reference_Name_Alias = txtTableAlias.Text.Trim
                        .Table_Reference_Field_Key = CboTableReferencKey.SelectedItem.Value
                        .Table_Reference_Field_Display_Name = CboTableReferenceDisplayName.SelectedItem.Value
                        .Table_Reference_Filter = txtTableReferenceFilter.Text.Trim
                        .Table_Reference_Additonal_Join = txtTableReferenceAdditionalJoin.Text.Trim
                    End With

                    Me.objRuleTemplateDetailEdit = Nothing
                End If

                ClearDetail()
                WindowDetail.Close()
                BindDetail()
                btnsaveField.Text = "Add Replacer"

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub cboFieldType_DirectSelect(sender As Object, e As DirectEventArgs)
        Try

            If cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.ReferenceTable) Then
                fieldsetref.Hidden = False
            Else
                fieldsetref.Hidden = True
            End If

            'If cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.VARCHARValue) Or cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.FLOATValue) Or cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.MONEYValue) Or (cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.REALValue)) Or (cboFieldType.SelectedItem.Value = CStr(NawaBLL.Common.MFieldType.VarBinaryValue)) Then

            '    txtFieldSize.Hidden = False
            'Else
            '    txtFieldSize.Hidden = True
            'End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub btnAddDetail_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail.Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Function IsDataValid() As Boolean
        'todohendra:IsDataValid
        Return True
    End Function

    'Sub LoadDataFromInterfacetoObject()
    '    'done:LoadDataFromInterfacetoObject

    '    With objRuleTemplate

    '        .Rule_Template_Name = txtruleTemplateName.Text.Trim
    '        .Rule_Template_Query = txtquery.Text.Trim
    '        .Case_Grouping_By = CboCaseGroupingBy.SelectedItem.Value
    '        .[Active] = True
    '        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
    '        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '        .CreatedDate = DateTime.Now
    '        .LastUpdateDate = DateTime.Now
    '        .ApprovedDate = DateTime.Now
    '        If NawaBLL.Common.SessionAlternateUser Is Nothing Then
    '            .Alternateby = NawaBLL.Common.SessionCurrentUser.UserID
    '        Else
    '            .Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
    '        End If


    '    End With

    'End Sub


    'Sub LoadDataFromInterfacetoObjectwithApproval()


    '    With objRuleTemplate

    '        .Rule_Template_Name = txtruleTemplateName.Text.Trim
    '        .Rule_Template_Query = txtquery.Text.Trim
    '        .Case_Grouping_By = CboCaseGroupingBy.SelectedItem.Value
    '        .[Active] = True
    '        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
    '        .ApprovedBy = ""
    '        .CreatedDate = DateTime.Now
    '        .LastUpdateDate = DateTime.Now
    '        .ApprovedDate = Nothing
    '        If NawaBLL.Common.SessionAlternateUser Is Nothing Then
    '            .Alternateby = NawaBLL.Common.SessionCurrentUser.UserID
    '        Else
    '            .Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
    '        End If


    '    End With

    'End Sub

    Protected Sub BtnSave_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            '12-Apr-2022 Adi : Validate First and compare the required fields with the fields from query result
            ValidateQuery()

            'Dim NewBll As New RuleTemplateBLL

            'If IsDataValid() Then

            '    With objRuleTemplate

            '        .Rule_Template_Name = txtruleTemplateName.Text.Trim
            '        .Rule_Template_Query = txtquery.Text.Trim
            '        .Case_Grouping_By = CboCaseGroupingBy.SelectedItem.Value
            '    End With

            '    NewBll.SubmitDocument(objRuleTemplate, objRuleTemplateDetail, ObjModule)

            '    LblConfirmation.Text = "Data Saved into Database"
            '    Panelconfirmation.Hidden = False
            '    FormPanelInput.Hidden = True

            'End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_ContinueSave_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim NewBll As New RuleTemplateBLL

            If IsDataValid() Then

                With objRuleTemplate

                    .Rule_Template_Name = txtruleTemplateName.Text.Trim
                    .Rule_Template_Query = txtquery.Text.Trim
                    .Case_Grouping_By = CboCaseGroupingBy.SelectedItem.Value

                    '19-Aug-2022 Adi : Penambahan Rule Template Category
                    .FK_Template_Category_Parameter = cboTemplateCategory.SelectedItem.Value
                End With

                NewBll.SubmitDocument(objRuleTemplate, objRuleTemplateDetail, ObjModule)

                window_ValidationResult.Hidden = True

                LblConfirmation.Text = "Data Saved into Database"
                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_CancelSave_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            window_ValidationResult.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub


#Region "12-Apr-2022 Adi : Query Validation"
    Protected Sub ValidateQuery()
        '12-Apr-2022 Adi : Validating Query by using Variable_Sample_Value
        Dim strQuery As String = txtquery.Value

        Try

            'Query harus mengandung kata "DROP TABLE IF EXISTS TEMP_CASEMANAGEMENT_RULE" dan "SELECT * INTO TEMP_CASEMANAGEMENT_RULE"
            Dim strMust1 As String = "DROP TABLE IF EXISTS TEMP_CASE_MANAGEMENT"
            Dim strMust2 As String = "SELECT * INTO TEMP_CASE_MANAGEMENT"

            If Not (strQuery.Contains(strMust1) And strQuery.Contains(strMust2)) Then
                Throw New Exception("The query must contain below statements:<br><br>1. " & strMust1 & "<br>2. " & strMust2)
            End If

            'Replace variables with sample value
            If dtReplacer IsNot Nothing AndAlso dtReplacer.Rows.Count > 0 Then
                For Each item In dtReplacer.Rows
                    strQuery = strQuery.Replace(item("Variable_Name"), item("Variable_Sample_Value"))
                Next
            End If

            'Show The Query
            display_Query.Value = strQuery

            'Run SP to Validate
            'NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@Query"
            param(0).Value = strQuery
            param(0).DbType = SqlDbType.VarChar

            param(1) = New SqlParameter
            param(1).ParameterName = "@UserID"
            param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(1).DbType = SqlDbType.VarChar

            '19-Aug-2022 Adi : Penambahan Rule Template Category
            param(2) = New SqlParameter
            param(2).ParameterName = "@TemplateCategory"
            param(2).Value = cboTemplateCategory.SelectedItem.Value
            param(2).DbType = SqlDbType.Int

            Dim strResult As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CaseManagement_ValidateRuleTemplate", param)
            display_ValidationResult.Value = Mid(strResult, 2)

            If Left(strResult, 1) = "1" Then
                btn_ContinueSave.Disabled = False
                display_ValidationResult.FieldStyle = "color:#006400;"
            Else
                btn_ContinueSave.Disabled = True
                display_ValidationResult.FieldStyle = "color:#f00;"
            End If

            '19-Aug-2022 Adi : Penambahan Transaction/Activity
            Dim intTemplateCategory As Integer = cboTemplateCategory.SelectedItem.Value
            If intTemplateCategory = 1 Then  'Transaction
                If Left(strResult, 1) = "1" Or Left(strResult, 1) = "2" Then
                    BindFieldsCompare()
                    gp_FieldsCompare.Hidden = False
                Else
                    gp_FieldsCompare.Hidden = True
                End If
            Else
                If Left(strResult, 1) = "1" Or Left(strResult, 1) = "2" Then
                    BindFieldsCompare_Activity()
                    gp_FieldsCompare.Hidden = False
                Else
                    gp_FieldsCompare.Hidden = True
                End If
            End If

            'Show the window
            window_ValidationResult.Hidden = False

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    <DirectMethod([Namespace]:="ReplacerSampleValue")>
    Public Sub Edit(ByVal id As Integer, ByVal field As String, ByVal oldValue As String, ByVal newValue As String, ByVal customer As Object)
        'Ext.Net.X.Msg.Alert("Information", "gua berubah!").Show()
        Try
            GridPanelDetail.GetStore().GetById(id).Commit()

            Dim drCek As DataRow = dtReplacer.Select("PK_Rule_Template_Detail_ID=" & id).FirstOrDefault
            If drCek IsNot Nothing Then
                drCek("Variable_Sample_Value") = newValue
            End If
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BindFieldsCompare()
        Try

            Dim dtFieldsCompare As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CaseManagement_ValidateRuleTemplate_Compare", Nothing)
            gp_FieldsCompare.GetStore.DataSource = dtFieldsCompare
            gp_FieldsCompare.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '23-Aug-2022 Adi : Fields compare for Activity
    Protected Sub BindFieldsCompare_Activity()
        Try

            Dim dtFieldsCompare As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CaseManagement_ValidateRuleTemplate_Compare_Activity", Nothing)
            gp_FieldsCompare.GetStore.DataSource = dtFieldsCompare
            gp_FieldsCompare.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_GetQuerySample_Click()
        Try
            Dim strQuerySample As String = "DROP TABLE IF EXISTS TEMP_CASE_MANAGEMENT;"
            strQuerySample &= " SELECT * INTO TEMP_CASE_MANAGEMENT FROM ("
            strQuerySample &= " SELECT a.* FROM goAML_ODM_Transaksi a "
            strQuerySample &= " JOIN AML_CUSTOMER b"
            strQuerySample &= " ON a.CIF_NO = b.CIFNo"
            strQuerySample &= " AND b.FK_AML_RISK_CODE = 'H'"
            strQuerySample &= " WHERE a.Transmode_Code IN (@Transmode_Code)"
            strQuerySample &= " AND a.IDR_Amount >= @IDR_Amount) xx"

            txtquery.Value = strQuerySample
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class
