﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="RuleTemplateDetail.aspx.vb" Inherits="OneFcc_Rule_RuleTemplateDetail" %>

<%--<%@ Register Src="~/NDSDropDownFieldCombination.ascx" TagPrefix="uc1" TagName="NDSDropDownFieldCombination" %>--%>
<%@ Register TagPrefix="NDS" Namespace="NDS" Assembly="App_Web_ndsdropdownfieldcombination.ascx.43883e39" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

          function OpenQuery1() {

            var x = window.open("../../Parameter/buttonQuery.aspx?object=1", "popupquery", "width=1000,height=600,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no");
            x.focus();


        }

        function OpenQuery() {

            var x = window.open("../../Parameter/buttonQuery.aspx", "popupquery", "width=1000,height=600,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no");
            x.focus();

        }
         var columnAutoResize = function (grid) {

             grid.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

                
                if (col.xtype == 'rownumberer') {
                    
                    col.width=60;
                }
                

            });
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <ext:Window ID="WindowDetail" runat="server" Height="185" Title="Replacer" Width="350" Hidden="true" BodyPadding="5" Layout="AnchorLayout" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:FormPanel ID="FormPanelDetail" runat="server" Padding="5" Layout="AnchorLayout">
                <Items>
                    <ext:TextField ID="txtvariableName" AllowBlank="false" runat="server" FieldLabel="Variable Name" AnchorHorizontal="85%">
                    </ext:TextField>
                    <ext:TextField ID="txtvariableDesc" AllowBlank="false" runat="server" FieldLabel="Variable Description" AnchorHorizontal="85%">
                    </ext:TextField>

                    
                    

                    <ext:ComboBox ID="cboExtType" runat="server" FieldLabel="Ext Type" DisplayField="ExtTypeName" ValueField="PK_ExtType_ID" AnchorHorizontal="85%" MinChars="0" ForceSelection="true" TriggerAction="Query" AllowBlank="false" BlankText="Ext Type is required">
                        <Store>
                            <ext:Store ID="StoreextType" runat="server" OnReadData="StoreextType_ReadData" IsPagingStore="true">
                                <Model>
                                    <ext:Model ID="ModelExtType" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_ExtType_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="ExtTypeName" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                    <ext:ComboBox ID="cboFieldType" runat="server" FieldLabel="Field Type" AnchorHorizontal="85%" DisplayField="FieldTypeCaption" ValueField="PK_FieldType_ID" MinChars="0" ForceSelection="true" TriggerAction="Query" AllowBlank="false" BlankText="Field Type is required">
                        <Store>
                            <ext:Store ID="StoreFieldType" runat="server" OnReadData="StoreFieldType_ReadData" IsPagingStore="true">
                                <Model>
                                    <ext:Model ID="ModelFieldType" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_FieldType_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FieldTypeCaption" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                            <Change OnEvent="cboFieldType_DirectSelect"></Change>
                        </DirectEvents>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>

                    <ext:FieldSet ID="fieldsetref" runat="server" Hidden="true">
                        <Items>

                            <ext:ComboBox ID="cboTableReference" ClientIDMode="Static" runat="server" FieldLabel="Table Reference" AnchorHorizontal="80%" DisplayField="TABLE_NAME" ValueField="TABLE_NAME" MinChars="0" EmptyText="[Select Data]" ForceSelection="true" TriggerAction="Query">
                                <Store>
                                    <ext:Store ID="StoreTableReference" runat="server" OnReadData="StoreTableReference_ReadData" IsPagingStore="true">
                                        <Model>
                                            <ext:Model ID="ModelTableReference" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="TABLE_NAME" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>

                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();#{CboTableReferencKey}.clearValue();#{CboTableReferenceDisplayName}.clearValue();#{StoreTableReferenceKey}.reload();#{StoreTableReferenceDisplayName}.reload();" />
                                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                            <ext:TextField ID="txtTableAlias" runat="server" FieldLabel="Table Reference Alias" AnchorHorizontal="80%" ClientIDMode="Static">
                            </ext:TextField>

                            <ext:ComboBox ID="CboTableReferencKey" runat="server" FieldLabel="Table Reference Key" AnchorHorizontal="80%" DisplayField="COLUMN_NAME" ValueField="COLUMN_NAME" MinChars="0" EmptyText="[Select Data]" ForceSelection="true" TriggerAction="Query">
                                <Store>
                                    <ext:Store ID="StoreTableReferenceKey" runat="server" OnReadData="StoreTableReferenceKey_ReadData" IsPagingStore="true">
                                        <Model>
                                            <ext:Model ID="ModelTableReferenceKey" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="COLUMN_NAME" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>

                            <ext:ComboBox ID="CboTableReferenceDisplayName" runat="server" FieldLabel="Table Reference Display Name" AnchorHorizontal="80%" DisplayField="COLUMN_NAME" ValueField="COLUMN_NAME" MinChars="0" EmptyText="[Select Data]" ForceSelection="true" TriggerAction="Query">
                                <Store>
                                    <ext:Store ID="StoreTableReferenceDisplayName" runat="server" OnReadData="StoreTableReferenceDisplayName_ReadData" IsPagingStore="true">
                                        <Model>
                                            <ext:Model ID="ModelTableReferenceDisplayName" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="COLUMN_NAME" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>

                            <ext:TextField ID="txtTableReferenceAdditionalJoin" runat="server" FieldLabel="Table Reference Additional Join" AnchorHorizontal="85%">
                            </ext:TextField>
                            <ext:TextField ID="txtTableReferenceFilter" runat="server" FieldLabel="Table Reference Filter" AnchorHorizontal="85%">
                            </ext:TextField>
                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.8, height: size.height * 0.8});" />

            <Resize Handler="#{WindowDetail}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btnsaveField" runat="server" Icon="Disk" Text="Add Replacer">
                <DirectEvents>
                    <Click OnEvent="BtnSaveField_Directclick">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="if (!#{FormPanelDetail}.getForm().isValid()) return false;"></Click>
                </Listeners>
            </ext:Button>
            <ext:Button ID="btnCancelField" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btnCancelField_Directclick">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <ext:FormPanel ID="FormPanelInput" runat="server" Layout="AnchorLayout" ButtonAlign="Center" Title="" BodyStyle="padding:20px" AutoScroll="true">
        <Items>
            <ext:TextField ID="txtruleTemplateName" FieldLabel="Rule Template Name" runat="server" AnchorHorizontal="80%" AllowBlank="false">
            </ext:TextField>

            <ext:ComboBox ID="CboCaseGroupingBy" runat="server" FieldLabel="Grouping by" DisplayField="Case_Grouping" ValueField="Case_Grouping" AnchorHorizontal="80%" MinChars="0" ForceSelection="true" TriggerAction="Query" AllowBlank="false" BlankText="Case Grouping By is required">
                <Store>
                    <ext:Store ID="StoreCaseGroupingBy" runat="server" IsPagingStore="true" >
                        <Model>
                            <ext:Model ID="Model1" runat="server">
                                <Fields>                                            
                                    <ext:ModelField Name="Case_Grouping" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                                
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Select Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                    this.clearValue();
                                    this.getTrigger(0).hide();
                                }" />
                </Listeners>
            </ext:ComboBox>

            <%-- 19-Aug-2022 Adi penambahan Template Category (Transaction or Activity) --%>
            <ext:ComboBox ID="cboTemplateCategory" runat="server" FieldLabel="Template Category" DisplayField="Template_Category_Parameter_Name" ValueField="PK_Template_Category_Parameter" AnchorHorizontal="80%" MinChars="0" ForceSelection="true" AllowBlank="false" Editable="false" >
                <Store>
                    <ext:Store ID="storeTemplateCategory" runat="server" IsPagingStore="true">
                        <Model>
                            <ext:Model ID="modelTemplateCategory" runat="server">
                                <Fields>                                            
                                    <ext:ModelField Name="PK_Template_Category_Parameter" Type="Int"></ext:ModelField>
                                    <ext:ModelField Name="Template_Category_Parameter_Name" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                                
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Select Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                    this.clearValue();
                                    this.getTrigger(0).hide();
                                }" />
                </Listeners>
            </ext:ComboBox>
            <%-- End of 19-Aug-2022 Adi penambahan Template Category (Transaction or Activity) --%>

            <ext:TextArea ID="txtquery" runat="server" FieldLabel="Rule Template Query" AllowBlank="false" EmptyText="Please Fill QueryData. Use @FieldName for Replacer Field." Height="150" AutoScroll="true" AnchorHorizontal="80%" ClientIDMode="Static">
                <%--<RightButtons>
                    <ext:Button ID="btnDesigner" runat="server" Text="Use Visual Quary Designer" ValidationGroup="MainForm">
                        <Listeners>
                            <Click Handler="OpenQuery()"></Click>
                        </Listeners>
                    </ext:Button>
                </RightButtons>--%>
            </ext:TextArea>
            <ext:Hidden ID="hQueryObjectDesigner" runat="server" ClientIDMode="Static">
            </ext:Hidden>

            <ext:GridPanel ID="GridPanelDetail" Border="true" runat="server" ClientIDMode="Static" Height="300" Title="Replacer" AutoScroll="true">
                <Store>
                    <ext:Store ID="StoreDetail" runat="server">
                        <Model>
                            <ext:Model ID="ModelDetail" runat="server" IDProperty="ISEA_MS_Rule_Template_Detail">
                                <Fields>
                                    <ext:ModelField Name="PK_Rule_Template_Detail_ID" Type="Auto" />
                                    <ext:ModelField Name="FK_Rule_Template_ID" Type="int" />
                                    <ext:ModelField Name="Variable_Name" Type="String" />
                                    <ext:ModelField Name="Variable_Description" Type="String" />
                                    <ext:ModelField Name="FK_FieldType_ID" Type="Int" />
                                    <ext:ModelField Name="FK_ExtType_ID" Type="Int" />
                                    <ext:ModelField Name="MFieldTypeName" Type="String" />
                                    <ext:ModelField Name="MExtTypeName" Type="String" />
                                    <ext:ModelField Name="Tabel_Reference_Name" Type="String" />
                                    <ext:ModelField Name="Tabel_Reference_Name_Alias" Type="String" />
                                    <ext:ModelField Name="Table_Reference_Field_Key" Type="String" />
                                    <ext:ModelField Name="Table_Reference_Field_Display_Name" Type="String" />
                                    <ext:ModelField Name="Table_Reference_Additonal_Join" Type="String" />
                                    <ext:ModelField Name="Table_Reference_Filter" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>

                        <ext:RowNumbererColumn runat="server" Text="No." Width="60"></ext:RowNumbererColumn>
                        <ext:Column runat="server" Text="Variable Name" DataIndex="Variable_Name" ></ext:Column>
                        <ext:Column runat="server" Text="Variable Description" DataIndex="Variable_Description" ></ext:Column>
                        <ext:Column runat="server" Text="Ext Type" DataIndex="MExtTypeName" ></ext:Column>
                        <ext:Column runat="server" Text="Field Type" DataIndex="MFieldTypeName" ></ext:Column>
                        <ext:Column runat="server" Text="Table Reference" DataIndex="Tabel_Reference_Name" ></ext:Column>
                        <ext:Column runat="server" Text="Table Reference Alias" DataIndex="Tabel_Reference_Name_Alias" ></ext:Column>
                        <ext:Column runat="server" Text="Table Reference Display Name" DataIndex="Table_Reference_Field_Display_Name" ></ext:Column>
                        <ext:Column runat="server" Text="Table Reference Additional Join" DataIndex="Table_Reference_Additonal_Join" ></ext:Column>
                        <ext:Column runat="server" Text="Table Reference Filter" DataIndex="Table_Reference_Filter" ></ext:Column>
                        

                         <ext:CommandColumn ID="commandfield" ClientIDMode="Static" runat="server"  Width="120">

                                    <DirectEvents>

                                        <Command OnEvent="CallBackModuleField">
                                            <Confirmation BeforeConfirm="if (command=='Edit') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Rule_Template_Detail_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                    <Commands>

                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="NoteEdit" MinWidth="50" >
                                            <ToolTip Text="Edit"></ToolTip>

                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="Delete" MinWidth="70" >
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <Listeners>
                <ViewReady Handler="columnAutoResize(this);this.getStore().on('load', Ext.bind(columnAutoResize, null, [this]));" Delay="10" />
            </Listeners>
            </ext:GridPanel>
        </Items>

        <Buttons>
            <ext:Button ID="btnCancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>

        <Buttons>

            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

</asp:Content>



