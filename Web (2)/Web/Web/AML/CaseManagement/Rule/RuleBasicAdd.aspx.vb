﻿Imports Ext.Net
Imports System.Data
Imports CasemanagementDAL
Imports CasemanagementBLL
Partial Class OneFcc_Rule_RuleBasicAdd
    Inherits ParentPage
    Public objFormModuleAdd As NawaBLL.FormModuleAdd


    Public Property objRuleBasic() As OneFcc_MS_Rule_Basic
        Get
            If Session("RuleBasicAdd.objRuleBasic") Is Nothing Then
                Dim oNewObj As New OneFcc_MS_Rule_Basic
                Session("RuleBasicAdd.objRuleBasic") = oNewObj
            End If
            Return Session("RuleBasicAdd.objRuleBasic")
        End Get
        Set(ByVal value As OneFcc_MS_Rule_Basic)
            Session("RuleBasicAdd.objRuleBasic") = value
        End Set
    End Property

    Public Property objRuleBasicDetail() As List(Of OneFcc_MS_Rule_Basic_Detail)
        Get
            If Session("RuleBasicAdd.objRuleBasicDetail") Is Nothing Then
                Dim oNewObj As New List(Of OneFcc_MS_Rule_Basic_Detail)
                Session("RuleBasicAdd.objRuleBasicDetail") = oNewObj
            End If
            Return Session("RuleBasicAdd.objRuleBasicDetail")
        End Get
        Set(ByVal value As List(Of OneFcc_MS_Rule_Basic_Detail))
            Session("RuleBasicAdd.objRuleBasicDetail") = value
        End Set
    End Property

    Public Property objRuleBasicDetailEdit() As OneFcc_MS_Rule_Basic_Detail
        Get
            Return Session("RuleBasicAdd.objRuleBasicDetailEdit")
        End Get
        Set(ByVal value As OneFcc_MS_Rule_Basic_Detail)
            Session("RuleBasicAdd.objRuleBasicDetailEdit") = value
        End Set
    End Property

    Public Property RulesName() As String
        Get
            Return Session("RuleBasicAdd.RulesName")
        End Get
        Set(ByVal value As String)
            Session("RuleBasicAdd.RulesName") = value
        End Set
    End Property

    Public Property RulesTemplateId() As Integer
        Get
            Return Session("RuleBasicAdd.RulesTemplateId")
        End Get
        Set(ByVal value As Integer)
            Session("RuleBasicAdd.RulesTemplateId") = value
        End Set
    End Property

    Public Property CategoryCaseManagement() As Integer
        Get
            Return Session("RuleBasicAdd.CategoryCaseManagement")
        End Get
        Set(ByVal value As Integer)
            Session("RuleBasicAdd.CategoryCaseManagement") = value
        End Set
    End Property

    Public Property JenisLaporan() As String
        Get
            Return Session("RuleBasicAdd.JenisLaporan")
        End Get
        Set(ByVal value As String)
            Session("RuleBasicAdd.JenisLaporan") = value
        End Set
    End Property
    Public Property CategoryParameterID() As Integer
        Get
            Return Session("RuleBasicAdd.CategoryParameterID")
        End Get
        Set(ByVal value As Integer)
            Session("RuleBasicAdd.CategoryParameterID") = value
        End Set
    End Property
    Public Property TemplateCategoryID() As Integer
        Get
            Return Session("RuleBasicAdd.TemplateCategoryID")
        End Get
        Set(ByVal value As Integer)
            Session("RuleBasicAdd.TemplateCategoryID") = value
        End Set
    End Property

    Public Property TransactionPeriod() As Integer
        Get
            Return Session("RuleBasicAdd.TransactionPeriod")
        End Get
        Set(ByVal value As Integer)
            Session("RuleBasicAdd.TransactionPeriod") = value
        End Set
    End Property

    Public Property IsLoadingTemplate() As Boolean
        Get
            Return Session("RuleBasicAdd.IsLoadingTemplate")
        End Get
        Set(ByVal value As Boolean)
            Session("RuleBasicAdd.IsLoadingTemplate") = value
        End Set
    End Property

    Private Sub RulesAdvanced_Add_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleAdd = New NawaBLL.FormModuleAdd(FormPanelInput, Panelconfirmation, LblConfirmation)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()
                'FormPanelInput.Title = ObjModule.ModuleLabel & " Add"
                'Panelconfirmation.Title = ObjModule.ModuleLabel & " Add"
                FormPanelInput.Title = "Rule Basic - Add"

                LoadRuleTemplate()

                If RulesTemplateId <> 0 Then
                    TxtRulesBasicName.Text = RulesName
                    'CboRuleTemplate.SelectedItem.Value = RulesTemplateId
                    cboCategoryCaseManagement.SelectedItem.Value = CategoryCaseManagement
                    cboJenisLaporan.SelectedItem.Value = JenisLaporan
                    cboCatagoryParameter.SelectedItem.Value = CategoryParameterID
                    cboCategoryTemplate.SelectedItem.Value = TemplateCategoryID

                    txt_Transaction_Period.Value = TransactionPeriod
                End If

                'Hide unnecessary fields
                cboCategoryCaseManagement.Hidden = True
                'cboCatagoryParameter.Hidden = True
                '19-Aug-2022 Adi : Template Category Parameter dimunculkan lagi (Transaction/Activity)
                cboCatagoryParameter.Hidden = False
                cboJenisLaporan.Hidden = True
                cboCategoryTemplate.Hidden = True

                'Make unnecessary fields to not mandatory.
                cboCategoryCaseManagement.AllowBlank = True
                cboCatagoryParameter.AllowBlank = True
                cboJenisLaporan.AllowBlank = True
                cboCategoryTemplate.AllowBlank = True

                '21-Feb-2022 Adi : Bugs input field tidak clear
                Dim strIsLoadingTemplate As String = Request.Params("IsLoadingTemplate")
                If String.IsNullOrEmpty(strIsLoadingTemplate) Then
                    Me.IsLoadingTemplate = False
                    TxtRulesBasicName.Value = Nothing
                    CboRuleTemplate.SetTextValue("")
                    txt_Transaction_Period.Value = Nothing
                    FormParameter.ClearContent()
                    FormParameter.Hidden = True

                    '19-Aug-2022 Adi : Template Category Parameter dimunculkan lagi (Transaction/Activity)
                    cboCatagoryParameter.Clear()
                Else
                    Me.IsLoadingTemplate = True
                End If
            End If

            LoadFieldParameter()

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Informasi", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadRuleTemplate()

        'StoreRuleTemplate.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT PK_Rule_Template_ID, Rule_Template_Name FROM OneFcc_MS_Rule_Template WHERE Active = 1 ", Nothing)
        'StoreRuleTemplate.DataBind()
        'CboRuleTemplate.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        'StoreRuleTemplate.PageSize = NawaBLL.SystemParameterBLL.GetPageSize

        StoreCategoryCaseManagement.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT PK_Category_Case_Management_ID, Category_Case_Management_Name FROM dbo.OneFcc_MS_Category_Case_Management WHERE Active = 1 ", Nothing)
        StoreCategoryCaseManagement.DataBind()
        cboCategoryCaseManagement.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreCategoryCaseManagement.PageSize = NawaBLL.SystemParameterBLL.GetPageSize

        StoreJenisLaporan.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT Kode, Keterangan FROM goAML_Ref_Jenis_Laporan WHERE Active = 1 ", Nothing)
        StoreJenisLaporan.DataBind()
        cboJenisLaporan.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreJenisLaporan.PageSize = NawaBLL.SystemParameterBLL.GetPageSize


        StoreCatagoryParameter.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT PK_Template_Category_Parameter, Template_Category_Parameter_Name FROM OneFcc_MS_Template_Category_Parameter WHERE Active = 1  ", Nothing)
        StoreCatagoryParameter.DataBind()
        cboCatagoryParameter.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreCatagoryParameter.PageSize = NawaBLL.SystemParameterBLL.GetPageSize


    End Sub

    Protected Sub LoadFieldParameter()
        If Not IsLoadingTemplate Then
            Exit Sub
        End If

        Dim PkRuleTemplateId As Long = RulesTemplateId
        Dim IntSequence As Integer = 1

        Dim strSQL As String = "SELECT TOP 1 * FROM OneFcc_MS_Rule_Template WHERE PK_Rule_Template_ID=" & RulesTemplateId
        Dim drRuleTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
        If drRuleTemp IsNot Nothing Then
            CboRuleTemplate.SetTextWithTextValue(RulesTemplateId, drRuleTemp("Rule_Template_Name"))
        End If

        Using objdb As New CasemanagementDAL.CasemanagementEntities
            For Each item As OneFcc_MS_Rule_Template_Detail In objdb.OneFcc_MS_Rule_Template_Detail.Where(Function(x) x.FK_Rule_Template_ID = PkRuleTemplateId).ToList
                Select Case CType(item.FK_ExtType_ID, EnumExtType)
                    Case EnumExtType.TextField
                        ExtText(FormParameter, item.Variable_Description, item.Variable_Name.Replace("@", ""), True, 8000, IntSequence)
                    Case EnumExtType.NumberField
                        ExtNumber(FormParameter, item.Variable_Description, item.Variable_Name.Replace("@", ""), True, 0, IntSequence, Long.MinValue, Long.MaxValue)
                    Case EnumExtType.DateField
                        ExtDate(FormParameter, item.Variable_Description, item.Variable_Name.Replace("@", ""), True, 150, IntSequence)
                    Case EnumExtType.DropDownField
                        ExtCombo(FormParameter, item.Variable_Description, item.Variable_Name.Replace("@", ""), True, IntSequence, item.Tabel_Reference_Name, item.Table_Reference_Field_Key, item.Table_Reference_Field_Display_Name, item.Table_Reference_Filter, item.Tabel_Reference_Name_Alias)
                    Case EnumExtType.MultiCombo
                        'ExtMultiCombo(FormParameter, item.Variable_Description, item.Variable_Name.Replace("@", ""), True, IntSequence, item.Tabel_Reference_Name, item.Table_Reference_Field_Key, item.Table_Reference_Field_Display_Name, item.Table_Reference_Filter, item.Tabel_Reference_Name_Alias)

                End Select
                IntSequence += 1
            Next
        End Using

    End Sub

    Protected Sub CboRuleTemplate_Changed()
        Try
            'If Not CboRuleTemplate.Value Is Nothing Then
            '    If RulesTemplateId <> CboRuleTemplate.Value.ToString Then
            '        RulesName = TxtRulesBasicName.Text
            '        RulesTemplateId = CboRuleTemplate.Value.ToString

            '        If cboCategoryCaseManagement.SelectedItem.Value IsNot Nothing Then
            '            CategoryCaseManagement = cboCategoryCaseManagement.Value.ToString
            '        End If

            '        If cboJenisLaporan.SelectedItem.Value IsNot Nothing Then
            '            JenisLaporan = cboJenisLaporan.Value.ToString
            '        End If

            '        If cboCatagoryParameter.SelectedItem.Value IsNot Nothing Then
            '            CategoryParameterID = cboCatagoryParameter.Value.ToString
            '        End If

            '        If cboCategoryTemplate.SelectedItem.Value IsNot Nothing Then
            '            TemplateCategoryID = cboCategoryTemplate.Value.ToString
            '        End If


            '        Dim Moduleid As String = Request.Params("ModuleID")


            '        Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlAdd & "?ModuleID=" & Moduleid)
            '        'Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(Request.Params("ModuleID"), NawaBLL.SystemParameterBLL.GetEncriptionKey)
            '        'Dim Moduleid As String = NawaBLL.Common.DecryptQueryString(Request.Params("ModuleID"), NawaBLL.SystemParameterBLL.GetEncriptionKey)

            '        'Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading...")
            '    End If
            'End If

            If Not String.IsNullOrEmpty(CboRuleTemplate.SelectedItemValue) Then
                RulesName = TxtRulesBasicName.Text
                RulesTemplateId = CboRuleTemplate.SelectedItemValue
                TransactionPeriod = txt_Transaction_Period.Value

                Dim strQuery As String = "SELECT TOP 1 FK_Template_Category_Parameter FROM OneFcc_MS_Rule_Template WHERE PK_Rule_Template_ID = " & CboRuleTemplate.SelectedItemValue
                Dim intTemplateCategory As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, System.Data.CommandType.Text, strQuery, Nothing)
                If Not IsNothing(intTemplateCategory) Then
                    CategoryParameterID = intTemplateCategory
                End If
            End If

            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlAdd & "?ModuleID=" & Moduleid & "&IsLoadingTemplate=1")

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub cboCategoryCaseManagement_Changed()
        Try
            Using objdb As New CasemanagementDAL.CasemanagementEntities
                If cboCategoryCaseManagement.SelectedItem.Value IsNot Nothing Then
                    Dim FlaggoAMLReporting = (From x In objdb.OneFcc_MS_Category_Case_Management Where x.PK_Category_Case_Management_ID = cboCategoryCaseManagement.SelectedItem.Value Select x).FirstOrDefault
                    If FlaggoAMLReporting IsNot Nothing Then
                        If FlaggoAMLReporting.Flag_goAML_Reporting = True Then
                            cboJenisLaporan.Hidden = False
                            cboJenisLaporan.Clear()
                        Else
                            cboJenisLaporan.Hidden = True
                            cboJenisLaporan.Clear()
                        End If
                    End If
                End If
            End Using
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Enum EnumExtType
        DateField = 1
        DropDownField = 2
        NumberField = 4
        TextField = 5
        Radio = 6
        DisplayField = 7
        FileUpload = 8
        Password = 9
        HTMLEditor = 10
        FormulaField = 11
        MultiCombo = 12
    End Enum

    'Public Function ExtMultiCombo(pn As FormPanel, strLabel As String, strFieldName As String, bRequired As Boolean, intgridpos As Integer, strTableRef As String, strFieldKey As String, strFieldDisplay As String, strFilterField As String, strTableRefAlias As String) As NDS.NDSDropDownFieldCombination

    'Dim objPanel As New Ext.Net.Panel
    'objPanel.ID = "Pnl" & strFieldName
    'objPanel.AnchorHorizontal = "80%"
    'objPanel.Layout = "AnchorLayout"
    'Dim objmulticombo As New NDS.NDSDropDownFieldCombination
    'objmulticombo.ID = strFieldName
    'objmulticombo.AnchorHorizontal = "80%"
    'objmulticombo.Label = strLabel
    'objmulticombo.StringField = strFieldKey & "," & strFieldDisplay
    'objmulticombo.StringTable = strTableRef
    'objmulticombo.IDPropertyUnik = strFieldKey
    'objmulticombo.Tipe = "checkbox"
    'objmulticombo.AllowBlank = Not bRequired

    'objPanel.ContentControls.Add(objmulticombo)

    'pn.Add(objPanel)

    'End Function

    Public Function ExtCombo(pn As FormPanel, strLabel As String, strFieldName As String, bRequired As Boolean, intgridpos As Integer, strTableRef As String, strFieldKey As String, strFieldDisplay As String, strFilterField As String, strTableRefAlias As String) As Ext.Net.ComboBox
        Using objcombo As New Ext.Net.ComboBox
            objcombo.ID = strFieldName
            objcombo.ClientIDMode = Web.UI.ClientIDMode.Static
            If Not Ext.Net.X.IsAjaxRequest Then
                objcombo.FieldLabel = strLabel

                objcombo.LabelWidth = 150 'intLabelWidth
                objcombo.AnchorHorizontal = "80%"
                objcombo.Name = strFieldName
                objcombo.AllowBlank = Not bRequired
                objcombo.BlankText = strLabel & " is required."
                objcombo.Width = objcombo.LabelWidth + 150
                objcombo.MatchFieldWidth = True
                objcombo.MinChars = "0"
                objcombo.ForceSelection = True

                'objcombo.TypeAhead = True
                'objcombo.EnableRegEx = True
                objcombo.AnyMatch = True

                objcombo.QueryMode = DataLoadMode.Local
                objcombo.ValueField = strFieldKey
                objcombo.DisplayField = strFieldDisplay
                objcombo.TriggerAction = Ext.Net.TriggerAction.All

                Dim objFieldtrigger As New Ext.Net.FieldTrigger
                objFieldtrigger.Icon = Ext.Net.TriggerIcon.Clear
                objFieldtrigger.Hidden = True
                objFieldtrigger.Weight = "-1"
                objcombo.Triggers.Add(objFieldtrigger)

                objcombo.Listeners.Select.Handler = "this.getTrigger(0).show();"

                objcombo.Listeners.TriggerClick.Handler = "if (index == 0) {  this.clearValue(); this.getTrigger(0).hide();}"

                'buat store dan modelnya

                Using objStore As New Ext.Net.Store
                    objStore.ID = "_Store_" + objcombo.ID
                    objStore.ClientIDMode = Web.UI.ClientIDMode.Static

                    Using objModel As New Ext.Net.Model
                        objModel.Fields.Add(strFieldKey, Ext.Net.ModelFieldType.String)
                        objModel.Fields.Add(strFieldDisplay, Ext.Net.ModelFieldType.String)
                        objStore.Model.Add(objModel)
                    End Using

                    objcombo.Store.Add(objStore)
                    objStore.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, System.Data.CommandType.Text, GetQueryRef(strTableRef & " " & strTableRefAlias, strFieldKey, strFieldDisplay, strFilterField), Nothing)
                    objStore.DataBind()
                End Using
            End If

            pn.Add(objcombo)
            Return objcombo
        End Using
    End Function

    Function GetQueryRef(strTable As String, strfieldkey As String, strfielddisplay As String, strfilter As String) As String
        Dim strquery As String
        strquery = "select " & strfieldkey & ", convert(Varchar(1000),[" & strfieldkey & "])+ ' - '+ convert(varchar(1000), [" & strfielddisplay & "]) as [" & strfielddisplay & "] from " & strTable

        strfilter = strfilter.Replace("@userid", NawaBLL.Common.SessionCurrentUser.UserID)
        strfilter = strfilter.Replace("@PK_MUser_ID", NawaBLL.Common.SessionCurrentUser.PK_MUser_ID)

        If strfilter.Trim.Length > 0 Then
            strquery = strquery & " where " & strfilter
        End If
        Return strquery
    End Function

    Public Function ExtDate(pn As FormPanel, strLabel As String, strFieldName As String, bRequired As Boolean, intMaxSize As Integer, intgridpos As Integer) As Ext.Net.DateField
        Dim objDateField As New Ext.Net.DateField
        objDateField.ID = strFieldName
        objDateField.ClientIDMode = Web.UI.ClientIDMode.Static
        If Not Ext.Net.X.IsAjaxRequest Then
            objDateField.FieldLabel = strLabel
            objDateField.LabelStyle = "word-wrap: break-word"
            objDateField.LabelWidth = 150 'intLabelWidth
            objDateField.Name = strFieldName
            objDateField.AllowBlank = Not bRequired
            objDateField.BlankText = strLabel & " is required."
            'objDateField.MaxLength = intMaxSize
            objDateField.Width = objDateField.LabelWidth + 150
            objDateField.Format = NawaBLL.SystemParameterBLL.GetDateFormat
            objDateField.AnchorHorizontal = "40%"
        End If
        pn.Add(objDateField)
        Return objDateField
    End Function

    Public Function ExtNumber(pn As FormPanel, strLabel As String, strFieldName As String, bRequired As Boolean, intDecimalPrecition As Integer, intgridpos As Integer, dminvalue As Double, dmaxvalue As Double) As Ext.Net.NumberField
        Dim objNumberField As New Ext.Net.NumberField
        objNumberField.ID = strFieldName
        objNumberField.ClientIDMode = Web.UI.ClientIDMode.Static
        If Not Ext.Net.X.IsAjaxRequest Then
            objNumberField.FieldLabel = strLabel
            objNumberField.LabelStyle = "word-wrap: break-word"
            objNumberField.LabelWidth = 150 'intLabelWidth
            objNumberField.Name = strFieldName
            objNumberField.AllowBlank = Not bRequired
            objNumberField.BlankText = strLabel & " is required."
            objNumberField.DecimalPrecision = intDecimalPrecition
            objNumberField.MinValue = dminvalue
            objNumberField.MaxValue = dmaxvalue
            objNumberField.Width = objNumberField.LabelWidth + 150
            objNumberField.AnchorHorizontal = "40%"
        End If
        pn.Add(objNumberField)
        Return objNumberField
    End Function

    Public Function ExtText(pn As FormPanel, strLabel As String, strFieldName As String, bRequired As Boolean, intMaxSize As Integer, intgridpos As Integer) As Ext.Net.TextField
        Dim objDateField As New Ext.Net.TextField
        objDateField.ID = strFieldName
        objDateField.ClientIDMode = Web.UI.ClientIDMode.Static
        If Not Ext.Net.X.IsAjaxRequest Then
            objDateField.FieldLabel = strLabel

            objDateField.Name = strFieldName
            objDateField.AllowBlank = Not bRequired
            objDateField.BlankText = strLabel & " is required."
            objDateField.MaxLength = intMaxSize
            objDateField.EmptyText = "Must use apostrophe. If field label using statement IN, you can input with comma separated. Eg. 'TKLN','TMLN'"

            objDateField.AnchorHorizontal = "90%"
        End If
        pn.Add(objDateField)
        Return objDateField
    End Function

    Private Sub ClearSession()
        'RulesName = Nothing
        'RulesTemplateId = 0
        objRuleBasic = Nothing
        objRuleBasicDetail = Nothing
        objRuleBasicDetailEdit = Nothing
    End Sub

    Protected Sub Callback(sender As Object, e As DirectEventArgs)
        Dim LstVariableName As New List(Of String)
        Dim LstValueData As New List(Of String)
        Dim LstVariableDescription As New List(Of String)

        Try

            If String.IsNullOrWhiteSpace(TxtRulesBasicName.Value) Then
                Throw New ApplicationException(TxtRulesBasicName.FieldLabel & " is required")
            End If
            If String.IsNullOrEmpty(CboRuleTemplate.SelectedItemValue) Then
                Throw New ApplicationException(CboRuleTemplate.Label & " is required")
            End If
            If String.IsNullOrEmpty(txt_Transaction_Period.Text) Or txt_Transaction_Period.Text = "N/A" Then
                Throw New ApplicationException(txt_Transaction_Period.FieldLabel & " is required")
            End If

            Using ObjDb As New CasemanagementDAL.CasemanagementEntities
                If TxtRulesBasicName.Text.Trim <> "" Then
                    Dim ObjAMLRules As List(Of OneFcc_MS_Rule_Basic) = ObjDb.OneFcc_MS_Rule_Basic.Where(Function(x) x.Rule_Basic_Name.ToUpper = TxtRulesBasicName.Text.Trim.ToUpper).ToList
                    If ObjAMLRules.Count > 0 Then
                        Throw New ApplicationException("Rules Name already in used")
                    End If
                End If

                If txt_Transaction_Period.Value <= 0 Or txt_Transaction_Period.Value > 1000 Then
                    Throw New ApplicationException(txt_Transaction_Period.FieldLabel & " must between 1-1000")
                End If

                'Dim PkRuleTemplateId As Long = CboRuleTemplate.Value.ToString
                Dim PkRuleTemplateId As Long = CboRuleTemplate.SelectedItemValue
                Dim IntSequence As Integer = 1

                For Each item As OneFcc_MS_Rule_Template_Detail In ObjDb.OneFcc_MS_Rule_Template_Detail.Where(Function(x) x.FK_Rule_Template_ID = PkRuleTemplateId).ToList
                    Select Case CType(item.FK_ExtType_ID, EnumExtType)
                        Case EnumExtType.TextField
                            Dim objfield As TextField = FormParameter.FindControl(item.Variable_Name.Replace("@", ""))
                            If Not objfield Is Nothing Then
                                LstVariableName.Add(item.Variable_Name.Replace("@", ""))
                                'LstValueData.Add(System.Web.HttpContext.Current.Server.HtmlEncode(IIf(objfield.Text Is Nothing, "", objfield.Text.Trim)))
                                LstValueData.Add(IIf(objfield.Text Is Nothing, "", objfield.Text.Trim))
                                LstVariableDescription.Add(item.Variable_Description)
                            End If
                        Case EnumExtType.NumberField
                            Dim objfield As NumberField = FormParameter.FindControl(item.Variable_Name.Replace("@", ""))
                            If Not objfield Is Nothing Then
                                LstVariableName.Add(item.Variable_Name.Replace("@", ""))
                                LstValueData.Add(objfield.Value)
                                LstVariableDescription.Add(item.Variable_Description)
                            End If
                        Case EnumExtType.DateField
                            Dim objfield As DateField = FormParameter.FindControl(item.Variable_Name.Replace("@", ""))
                            If Not objfield Is Nothing Then
                                LstVariableName.Add(item.Variable_Name.Replace("@", ""))
                                LstValueData.Add(NawaBLL.Common.ConvertToDate(NawaBLL.SystemParameterBLL.GetDateFormat, objfield.RawText).ToString("yyyy-MM-dd"))
                                LstVariableDescription.Add(item.Variable_Description)
                            End If
                        Case EnumExtType.DropDownField
                            Dim objfield As ComboBox = FormParameter.FindControl(item.Variable_Name.Replace("@", ""))
                            If Not objfield Is Nothing Then
                                LstVariableName.Add(item.Variable_Name.Replace("@", ""))
                                LstValueData.Add(objfield.SelectedItem.Value)
                                LstVariableDescription.Add(item.Variable_Description)
                            End If
                        Case EnumExtType.MultiCombo

                            'Dim objfield As NDS.NDSDropDownFieldCombination = FormParameter.FindControl(item.Variable_Name.Replace("@", ""))
                            'If Not objfield Is Nothing Then
                            '    LstVariableName.Add(item.Variable_Name.Replace("@", ""))
                            '    LstValueData.Add(objfield.TextValue)
                            '    LstVariableDescription.Add(item.Variable_Description)
                            'End If
                    End Select
                    IntSequence += 1
                Next


                Dim NewBll As New RuleBasicBLL

                With objRuleBasic
                    .Rule_Basic_Name = TxtRulesBasicName.Text

                    '.FK_Rule_Template_ID = CboRuleTemplate.SelectedItem.Value
                    .FK_Rule_Template_ID = CboRuleTemplate.SelectedItemValue

                    '.Segment_Code = cboSegment.SelectedItem.Value
                    '.FK_Category_Case_Management_ID = cboCategoryCaseManagement.SelectedItem.Value
                    .FK_goAML_Ref_Jenis_Laporan = cboJenisLaporan.SelectedItem.Value

                    '.FK_Template_Category_Parameter = 1     '21-Feb-2022 Adi : Hard coded to 1=Transaction
                    '19-Aug-2022 Adi : Template Category Parameter dimunculkan lagi (Transaction/Activity)
                    .FK_Template_Category_Parameter = cboCatagoryParameter.SelectedItem.Value

                    .FK_Template_Category = cboCategoryTemplate.SelectedItem.Value
                    .Transaction_Period = CInt(txt_Transaction_Period.Value)
                End With

                'For x As Integer = 0 To LstValueData.Count - 1
                '    If String.IsNullOrEmpty(LstValueData(x)) Then
                '        Throw New ApplicationException("Please fill in all the parameters")
                '    End If
                'Next

                For x As Integer = 0 To LstValueData.Count - 1
                    Dim objdetail As New OneFcc_MS_Rule_Basic_Detail
                    With objdetail
                        .Variable_Name = LstVariableName(x)
                        .Variable_Description = LstVariableDescription(x)
                        .Value_Data = LstValueData(x)

                    End With
                    objRuleBasicDetail.Add(objdetail)

                Next

                NewBll.SubmitDocument(objRuleBasic, objRuleBasicDetail, ObjModule)

                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Saved into Database"


            End Using
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Informasi", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_Click()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Function IsDataValid() As Boolean
        Return True
    End Function

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Informasi", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub cboCatagoryParameter_Changed(sender As Object, e As DirectEventArgs)
        Try
            '19-Aug-2022 Adi : commented. tidak terpakai.
            'cboCategoryTemplate.Clear()


            'StoreCategoryTemplate.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "Select PK_Rule_Template_Category_ID, Rule_Template_Category_Name FROM OneFcc_MS_Rule_Template_Category WHERE Active = 1 AND FK_Template_Category_Parameter = " & cboCatagoryParameter.SelectedItem.Value & " ", Nothing)
            'StoreCategoryTemplate.DataBind()
            'cboCategoryTemplate.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
            'StoreCategoryTemplate.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Informasi", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub RulesAdvanced_Add_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Insert
    End Sub

End Class
