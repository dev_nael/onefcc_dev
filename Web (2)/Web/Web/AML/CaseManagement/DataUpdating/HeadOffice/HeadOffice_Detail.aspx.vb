﻿Imports Newtonsoft.Json
Imports System.Web.Services
Imports System.Web.Script.ServicesImports
Imports NawaDevDAL
Imports NawaDevBLL
Imports System.Data
Imports System.Data.Sql
Imports System.IO
Imports System.Web
Imports OfficeOpenXml
Imports NPOI.HSSF.UserModel
Imports NPOI.SS.UserModel
Imports System.Net
Partial Class AML_DataUpdating_HeadOffice_HeadOffice_Detail
    Inherits ParentPage
    Public objFormModuleDetail As NawaBLL.FormModuleDetail

    Protected Sub LoadData()


        Dim strid As String = Request.Params("ID")
        Dim id As Long = 0

        If IsNumeric(strid) Then
            id = strid
        Else
            id = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
        End If
        Session("IdUnik") = id


        Dim objresult As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from AML_DU_ALERT where PK_AML_DU_ALERT_ID = " & id)
        For Each item As DataRow In objresult.Rows
            If Not IsDBNull(item("PK_AML_DU_ALERT_ID")) Then
                txtID.Value = item("PK_AML_DU_ALERT_ID")
            End If
            If Not IsDBNull(item("PROCESS_DATE")) Then
                Dim processdate As DateTime = item("PROCESS_DATE")
                txtProcessDate.Value = processdate.ToString("dd-MMM-yyyy")
            End If
            If Not IsDBNull(item("FK_AML_BRANCH_CODE")) Then
                Dim query As String = "select BRANCH_NAME from AML_BRANCH where FK_AML_BRANCH_CODE = '" & item("FK_AML_BRANCH_CODE").ToString & "'"
                Dim branchname As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
                txtBranch.Value = item("FK_AML_BRANCH_CODE").ToString & " - " & branchname
            End If
            If Not IsDBNull(item("CIF_NO")) Then
                txtCIFNO.Value = item("CIF_NO")
                Dim querydateupdate As String = "select LastUpdateDate from AML_DU_LAST_MODIFIED_CIF where CIF = '" & item("CIF_NO").ToString & "'"
                Dim dataupdating As DateTime = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, querydateupdate, Nothing)

                If dataupdating = DateTime.MinValue Then
                    txtDataUpdated.Value = DateTime.Now.ToString("dd-MMM-yyyy")
                Else
                    txtDataUpdated.Value = dataupdating.ToString("dd-MMM-yyyy")
                End If
            End If
            If Not IsDBNull(item("CIF_NAME")) Then
                txtName.Value = item("CIF_NAME")
            End If
            If Not IsDBNull(item("CUSTOMER_TYPE")) Then
                Dim query As String = "select RiskName from VW_AML_DataUpdating_CustomerType where RiskCode = '" & item("CUSTOMER_TYPE").ToString & "'"
                Dim customertype As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
                Dim customertypename As String = item("CUSTOMER_TYPE").ToString & " - " & customertype
                txtCustomerType.Value = customertypename
            End If
            If Not IsDBNull(item("IS_VALID")) Then
                txtValid.Value = item("IS_VALID")
            End If
            If Not IsDBNull(item("FILE_ATTACHMENTName")) Then
                txtAttachment.Value = item("FILE_ATTACHMENTName")
                Session("AttachmentName") = item("FILE_ATTACHMENTName")
                If Not IsDBNull(item("FILE_ATTACHMENT")) Then
                    Session("AttachmentDU") = item("FILE_ATTACHMENT")
                End If
                btnDownloadAttachment.Hidden = False
            Else
                btnDownloadAttachment.Hidden = True
            End If
            If Not IsDBNull(item("Note")) Then
                txtNotes.Value = item("Note")
            End If
            If Not IsDBNull(item("ACTION")) Then
                Dim query As String = "select DESCRIPTION from AML_DU_ACTION where ACTION_CODE = '" & item("ACTION").ToString & "'"
                Dim actionname As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
                cmb_Action.Value = item("ACTION").ToString & " - " & actionname
            End If
            If Not IsDBNull(item("INVALID_MESSAGE")) Then
                txtInvalidMessage.Value = item("INVALID_MESSAGE")
            End If
            If Not IsDBNull(item("MANDATORY_MESSAGE")) Then
                txtMandatory.Value = item("MANDATORY_MESSAGE")
            End If






        Next


    End Sub


    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AML_DataUpdating_HeadOffice_HeadOffice_Edit_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleDetail = New NawaBLL.FormModuleDetail(fpMain, Panelconfirmation, LblConfirmation, Nothing)
    End Sub

    Private Sub AML_DataUpdating_HeadOffice_HeadOffice_Edit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                Session("IdUnik") = Nothing
                Session("AttachmentName") = Nothing
                LoadData()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Confirmation_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim strEncryptedModuleID = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & strEncryptedModuleID)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & strEncryptedModuleID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub AML_DataUpdating_HeadOffice_HeadOffice_Edit_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    Protected Sub btnDownloadAttachment_Click()
        Try
            Dim query As String = "select * from AML_DU_ALERT where PK_AML_DU_ALERT_ID = '" & Session("IdUnik").ToString & "'"
            Dim objtable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
            For Each item As DataRow In objtable.Rows
                Response.Clear()
                Response.ClearHeaders()
                'Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("content-disposition", "attachment;filename=" & item("FILE_ATTACHMENTName").ToString)
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(item("FILE_ATTACHMENT"))
                Response.End()
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub store_gpStore_history_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim dataStr As String = Request.Params("ID")
            Dim dataID As String = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim intStart As Integer = e.Start
            Dim intLimit As Integer = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                If strsort = "" Then
                    strsort += item.Property & " " & item.Direction.ToString
                Else
                    strsort += " , " & item.Property & " " & item.Direction.ToString
                End If
            Next

            If strsort = "" Then
                strsort = "CreatedDate Desc"
            End If

            Dim strFields As String = "*"

            If String.IsNullOrEmpty(strfilter) Then
                strfilter = "FK_AML_DU_ALERT_ID = " & dataID
            Else
                strfilter += " AND FK_AML_DU_ALERT_ID = " & dataID
            End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("AML_DU_ALERT_NOTE_HISTORY", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start <0 OrElse limit <0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            For Each item As DataRow In DataPaging.Rows
                Dim query As String = "select DESCRIPTION from AML_DU_ACTION where ACTION_CODE = '" & item("ACTION").ToString & "'"
                Dim actionname As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
                If item("ACTION").ToString = "Re-Open" Then
                Else
                    item("ACTION") = item("ACTION").ToString & " - " & actionname
                End If

            Next
            gpStore_history.DataSource = DataPaging
            Session("filterstring") = strfilter



            gpStore_history.DataBind()
            'Ari 15-09-2021 Inisiasi total watchlist dari store read data


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class
