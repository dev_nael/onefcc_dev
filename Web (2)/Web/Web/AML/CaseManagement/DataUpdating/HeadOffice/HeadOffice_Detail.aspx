﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="HeadOffice_Detail.aspx.vb" Inherits="AML_DataUpdating_HeadOffice_HeadOffice_Detail" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        var onKeyUp = function (combo, e) {
            var v = combo.getRawValue();
            combo.store.filter(combo.displayField, new RegExp(v, "i"));
            combo.onLoad();
        };

   
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel runat="server" ID="fpMain" BodyPadding="10" ButtonAlign="Center" Scrollable="Both" Title="Data Updating Head Office - Detail">
        <Items>
            <ext:DisplayField ID="txtID" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="ID" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
            <ext:DisplayField ID="txtProcessDate" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Process Date" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
            <ext:DisplayField ID="txtBranch" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Branch" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
            <ext:DisplayField ID="txtCIFNO" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="CIF No" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
            <ext:DisplayField ID="txtName" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Name" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
            <ext:DisplayField ID="txtCustomerType" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Customer Type" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
			<ext:DisplayField ID="txtDataUpdated" runat="server" FieldLabel="Data Updated" AnchorHorizontal="80%" AllowBlank="false" MaxLength="500" EnforceMaxLength="true" />
            <ext:Panel runat="server"  ButtonAlign="Center" Layout="AnchorLayout">
                  <Content>
                      <ext:DisplayField runat="server"  ID="cmb_Action"  FieldLabel="Action" AnchorHorizontal="80%" AllowBlank="false" />
                    </Content>
            </ext:Panel>
			            <ext:DisplayField ID="txtNotes" runat="server" FieldLabel="Notes" AnchorHorizontal="80%" AllowBlank="true"  ReadOnly="true"/>
             <ext:DisplayField ID="txtAttachment" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Attachment" />  
            <ext:Button ID="btnDownloadAttachment" runat="server" Text="Download" Icon="DiskDownload" Style="margin-left:155px;margin-bottom:10px;margin-top:-10px;">
                <DirectEvents>
                    <Click OnEvent="btnDownloadAttachment_Click" IsUpload="true" />
                </DirectEvents>
            </ext:Button>
			<ext:DisplayField ID="txtValid" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Valid" MaxLength="500" EnforceMaxLength="true" />
             <ext:Panel runat="server" ID="panel1" ClientIDMode="Static" Layout="AnchorLayout"  ComponentCls="xPanelContainer" Title="Mandatory" Border="true" BodyPadding="10" Collapsible="true">
                 <Items>
                      <ext:DisplayField ID="txtMandatory" runat="server" AnchorHorizontal="80%" AllowBlank="false" MaxLength="500" EnforceMaxLength="true" Border="true" BodyPadding="10"/>
                 </Items>
                 </ext:Panel>
            <ext:Panel runat="server" ID="panel2" ClientIDMode="Static" Layout="AnchorLayout"  ComponentCls="xPanelContainer" Title="Invalid Message" Border="true" BodyPadding="10"  StyleSpec="margin-top:20px" Collapsible="true">
                 <Items>
                       <ext:DisplayField ID="txtInvalidMessage" runat="server" AnchorHorizontal="80%" AllowBlank="false" MaxLength="500" EnforceMaxLength="true" />
                     </items>
                </ext:Panel>
                 <ext:Panel runat="server" Collapsible="true" Title="History" MarginSpec="10 0 0 0" Border="true" >
                        <Items>
                            <ext:GridPanel ID="gp_Note_History" runat="server">
                                <View>
                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                </View>
                                <Store>
                                    <ext:Store ID="gpStore_history" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" PageSize="10"
                                        OnReadData="store_gpStore_history_ReadData" RemotePaging="true" ClientIDMode="Static">
                                        <Model>
                                            <ext:Model runat="server" ID="Model357" >
                                                <Fields>
                                                    <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="ACTION" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="NOTE" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy />
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn89" runat="server" Text="No"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column314" runat="server" DataIndex="CreatedBy" Text="Created By" Flex="1"></ext:Column>
                                        <ext:Column ID="Column315" runat="server" DataIndex="ACTION" Text="Action" Flex="1"></ext:Column>
                                        <ext:Column ID="Column316" runat="server" DataIndex="NOTE" Text="Note" Flex="1"></ext:Column>
                                        <ext:DateColumn ID="ColumnDateNote" runat="server" DataIndex="CreatedDate" Text="Created Date" Flex="1" ClientIDMode="Static" Format="dd-MMM-yyyy">
                                            <Items>
                                                <ext:DateField ID="DatePickerColumnDateNote" runat="server" Format="dd-MMM-yyyy" FormatText="Expected Date Format dd-MMM-yyyy">
                                                    <Plugins>
                                                        <ext:ClearButton ID="Column16_ClearButton1">
                                                        </ext:ClearButton>
                                                    </Plugins>
                                                </ext:DateField>
                                            </Items>
                                            <Filter>
                                                <ext:DateFilter>
                                                </ext:DateFilter>
                                            </Filter>
                                        </ext:DateColumn>
                                    </Columns>
                                </ColumnModel>
                                <Plugins>
                                    <ext:FilterHeader runat="server" ID="FilterHeader_Valid" Remote="true" ClientIDMode="Static"></ext:FilterHeader>
                                </Plugins>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                        </Items>
                    </ext:Panel>
        </Items>                 
        <Buttons>
           
            <ext:Button ID="BtnCancel" runat="server" Text="Back" Icon="Cancel">
             
                <DirectEvents>
                    <Click OnEvent="btnCancel_DirectEvent">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="500">
                        </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
      <%-- ================== CONFIRMATION PANEL ========================== --%>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Confirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <%-- ================== END OF CONFIRMATION PANEL ========================== --%>
</asp:Content>

