﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CaseManagementAlertBatchCloseAdd.aspx.vb" Inherits="CaseManagementAlertBatchCloseAdd" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="SiPendar Profile Data">
        <Items>

            <ext:Button ID="btn_BatchCloseSelection" runat="server" Icon="ShapeSquareSelect" Text="Batch Closing by Selection" MarginSpec="0 0 10 0" Disabled="true">
                <DirectEvents>
                    <Click OnEvent="btn_BatchCloseSelection_Click">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_BatchCloseUpload" runat="server" Icon="DiskUpload" Text="Batch Closing by Upload" MarginSpec="0 0 10 10">
                <DirectEvents>
                    <Click OnEvent="btn_BatchCloseUpload_Click">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

            <ext:GridPanel ID="gp_CaseAlert" runat="server" Title="Select Case Alert(s) to be Closed" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store4" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                        <Model>
                            <ext:Model runat="server" ID="Model6" IDProperty="PK_CaseManagement_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_CaseManagement_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Alert_Type" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Case_Description" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CIF_No" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Customer_Name" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CaseStatus" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ProcessDate" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Aging_Workdays" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
<%--                        <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column56" runat="server" DataIndex="PK_CaseManagement_ID" Text="Case ID" Width="70"></ext:Column>
                        <ext:Column ID="Column33" runat="server" DataIndex="Alert_Type" Text="Alert Type" Width="120"></ext:Column>
                        <ext:Column ID="Column34" runat="server" DataIndex="Case_Description" Text="Case Description" MinWidth="400"></ext:Column>
                        <ext:Column ID="Column1" runat="server" DataIndex="CIF_No" Text="CIF" Width="150"></ext:Column>
                        <ext:Column ID="Column2" runat="server" DataIndex="Customer_Name" Text="Customer Name" Width="200"></ext:Column>
                        <ext:Column ID="Column35" runat="server" DataIndex="CaseStatus" Text="Status" Width="150"></ext:Column>
                        <ext:DateColumn ID="DateColumn5" runat="server" DataIndex="ProcessDate" Text="Alert Date" Width="110" Format="dd-MMM-yyyy"></ext:DateColumn>--%>

                        <%-- 21-Apr-2022 Adi : Case diubah jadi Group by ProcessDate dan CIF --%>
                        <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column56" runat="server" DataIndex="PK_CaseManagement_ID" Text="Case ID" Flex="1"></ext:Column>
                        <ext:DateColumn ID="DateColumn5" runat="server" DataIndex="ProcessDate" Text="Alert Date" Format="dd-MMM-yyyy" Flex="1"></ext:DateColumn>
                        <ext:Column ID="Column1" runat="server" DataIndex="CIF_No" Text="CIF" Flex="1"></ext:Column>
                        <ext:Column ID="Column2" runat="server" DataIndex="Customer_Name" Text="Customer Name" Flex="1"></ext:Column>
                        <ext:Column ID="Column35" runat="server" DataIndex="CaseStatus" Text="Status" Flex="1"></ext:Column>
                        <ext:Column ID="Column17" runat="server" DataIndex="Aging_Workdays" Text="Aging" Flex="1"></ext:Column>

                        <ext:CommandColumn ID="cc_CaseAlert" runat="server" Text="Action" Width="70">
                            <Commands>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                            </Commands>

                            <DirectEvents>
                                <Command OnEvent="gc_CaseAlert">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_CaseManagement_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CheckboxSelectionModel runat="server" Mode="Multi" CheckOnly="true">
                    </ext:CheckboxSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader3" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="true" />
                </BottomBar>
            </ext:GridPanel>

            <ext:Panel runat="server" ID="pnl_BatchCloseUpload" Title="Batch Closing by Upload" MarginSpec="0 0 10 0" Collapsible="true" Hidden="true" Border="true" Layout="AnchorLayout" BodyPadding="10">
                <Content>
                    <ext:HyperlinkButton ID="btn_DownloadTemplate" MarginSpec="10 0" runat="server"  Text="Export Template">
                        <DirectEvents>
                            <Click OnEvent="btn_DownloadTemplate_Click"></Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                    <ext:FileUploadField runat="server" ID="fileBatchUpload"  FieldLabel="Upload File" Accept=".xlsx" AnchorHorizontal="100%" AllowBlank="false"/>
                    <ext:Button ID="btn_BatchCloseUpload_Save" runat="server" Icon="DiskUpload" Text="Import" MarginSpec="10 0 0 0">
                        <DirectEvents>
                            <Click OnEvent="btn_BatchCloseUpload_Save_Click">
                                <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                    <ext:GridPanel ID="gp_BatchCloseUpload_Valid" runat="server" Title="Valid Data" MarginSpec="10 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="store1" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model1" IDProperty="PK_CaseManagement_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CaseManagement_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Alert_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Case_Description" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIF_No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Customer_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CaseStatus" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ProcessDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Aging_Workdays" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
<%--                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column10" runat="server" DataIndex="PK_CaseManagement_ID" Text="Case ID" Width="70"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="Alert_Type" Text="Alert Type" Width="120"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="Case_Description" Text="Case Description" MinWidth="400"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="CIF_No" Text="CIF" Width="150"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="Customer_Name" Text="Customer Name" Width="200"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="CaseStatus" Text="Status" Width="150"></ext:Column>
                                <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="ProcessDate" Text="Alert Date" Width="110" Format="dd-MMM-yyyy"></ext:DateColumn>--%>

                                <%-- 21-Apr-2022 Adi : Case diubah jadi Group by ProcessDate dan CIF --%>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column10" runat="server" DataIndex="PK_CaseManagement_ID" Text="Case ID" Flex="1"></ext:Column>
                                <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="ProcessDate" Text="Alert Date" Format="dd-MMM-yyyy" Flex="1"></ext:DateColumn>
                                <ext:Column ID="Column11" runat="server" DataIndex="CIF_No" Text="CIF" Flex="1"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="Customer_Name" Text="Customer Name" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="CaseStatus" Text="Status" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="Aging_Workdays" Text="Aging" Flex="1"></ext:Column>

                                <ext:CommandColumn ID="cc_BatchCloseUpload_Valid" runat="server" Text="Action" Width="70">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="gc_CaseAlert">
                                            <EventMask ShowMask="true"></EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CaseManagement_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="true" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="gp_BatchCloseUpload_InValid" runat="server" Title="Invalid Data" MarginSpec="10 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="store2" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model2">
                                        <Fields>
                                            <ext:ModelField Name="PK_CaseManagement_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIF_No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Invalid_Msg" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column16" runat="server" DataIndex="PK_CaseManagement_ID" Text="Case ID" Width="120"></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="CIF_No" Text="CIF" Width="150"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="Invalid_Msg" Text="Invalid Message" MinWidth="200" CellWrap="true" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="true" />
                        </BottomBar>
                    </ext:GridPanel>
                </Content>
            </ext:Panel>

            <ext:Panel runat="server" ID="pnl_Closing_Reason" Title="Closing Reason" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="10">
                <Content>
                    <ext:TextArea ID="txt_Closing_Reason" AllowBlank="false" LabelWidth="150" runat="server" FieldLabel="Closing Reason" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true" Height="150"></ext:TextArea>
                </Content>
            </ext:Panel>
        </Items>

        <Buttons>
            <ext:Button ID="btn_Save" runat="server" Icon="Disk" Text="Submit">
                <DirectEvents>
                    <Click OnEvent="btn_Save_Click">
                        <EventMask ShowMask="true" Msg="Submitting Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>


    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="Accept">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>


    <%-- ================== WINDOW CASE ALERT ========================== --%>
    <ext:Window ID="window_CaseAlert" Layout="AnchorLayout" Title="Case Alert Detail" runat="server" Modal="true" Hidden="true" Maximizable="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:Panel runat="server" ID="Panel2" Title="Case Alert Information" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="10">
                <Items>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_PK_CaseManagement_ID" AnchorHorizontal="100%" FieldLabel="Case Management ID" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_Alert_Type" AnchorHorizontal="100%" FieldLabel="Alert Type" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_Case_Description" AnchorHorizontal="100%" FieldLabel="Case Description" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_FK_Case_Status_ID" AnchorHorizontal="100%" FieldLabel="Case Status" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_CreatedDate" AnchorHorizontal="100%" FieldLabel="Created Date" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_Workflow_Step" AnchorHorizontal="100%" FieldLabel="Workflow Step" LabelWidth="150" Hidden="true"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_PIC" AnchorHorizontal="100%" FieldLabel="PIC" LabelWidth="150" Hidden="true"></ext:DisplayField>
                </Items>
            </ext:Panel>

            <ext:Panel runat="server" ID="pnlCustomerInformation" Title="Customer Information" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="ColumnLayout" BodyPadding="10">
                <Items>
                    <ext:Container runat="server" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField runat="server" ID="txt_CIF_No" AnchorHorizontal="100%" FieldLabel="CIF" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Account_No" AnchorHorizontal="100%" FieldLabel="Account No." LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Customer_Name" AnchorHorizontal="100%" FieldLabel="Customer Name" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_DOB" AnchorHorizontal="100%" FieldLabel="Date of Birth" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_POB" AnchorHorizontal="100%" FieldLabel="Place of Birth" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Gender" AnchorHorizontal="100%" FieldLabel="Gender" LabelWidth="150"></ext:DisplayField>
                        </Items>
                    </ext:Container>
                    <ext:Container runat="server" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField runat="server" ID="txt_CIF_Opening_Branch" AnchorHorizontal="100%" FieldLabel="Opening Branch" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_CIF_Opening_Date" AnchorHorizontal="100%" FieldLabel="Opening Date" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Is_PEP" AnchorHorizontal="100%" FieldLabel="PEP" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Risk_Code" AnchorHorizontal="100%" FieldLabel="Customer Risk" LabelWidth="150"></ext:DisplayField>

                           <%-- <ext:Button ID="btn_CustomerDetail" runat="server" Icon="ApplicationViewDetail" Text="More Details">
                                <DirectEvents>
                                    <Click OnEvent="btn_CustomerDetail_Click">
                                        <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>--%>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Panel>

            <ext:GridPanel ID="gp_Account" runat="server" Title="Account Information" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store_Account" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="Model43" IDProperty="ACCOUNT_NO">
                                <Fields>
                                    <ext:ModelField Name="ACCOUNT_NO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="DATEOPENING" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="BRANCH_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_TYPE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="PRODUCT_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CURRENT_BALANCE" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_CURRENCY_CODE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_ACCOUNT_STATUS_CODE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="LIMITCREDITCARD" Type="Float"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column5" runat="server" DataIndex="ACCOUNT_TYPE" Text="Acc Type" Width="100" Locked="true"></ext:Column>
                        <ext:Column ID="Column412" runat="server" DataIndex="ACCOUNT_NO" Text="Account No." Width="150" Locked="true"></ext:Column>
                        <ext:Column ID="Column72" runat="server" DataIndex="ACCOUNT_NAME" Text="Name" Width="150" Locked="true"></ext:Column>
                        <ext:Column ID="Column7" runat="server" DataIndex="FK_AML_ACCOUNT_STATUS_CODE" Text="Status" Width="100"></ext:Column>
                        <ext:Column ID="Column3" runat="server" DataIndex="BRANCH_NAME" Text="Branch" Width="150"></ext:Column>
                        <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="DATEOPENING" Text="Opening Date" Width="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:NumberColumn ID="NumberColumn1" runat="server" DataIndex="CURRENT_BALANCE" Text="Current Balance" Width="150" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:NumberColumn ID="NumberColumn11" runat="server" DataIndex="LIMITCREDITCARD" Text="CC Limit" Width="150" Format="#,###.00" Align="Right"></ext:NumberColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

            <ext:GridPanel ID="gp_Loan" runat="server" Title="Loan Information" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="150">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store8" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="Model14" IDProperty="ACCOUNT_NO">
                                <Fields>
                                    <ext:ModelField Name="LOAN_NUMBER" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="LOAN_TYPE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="SEGMENT_LOAN" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="TENOR" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="LOAN_AMOUNT" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="COLLATERAL_TYPE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="BRANCH_NAME" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn15" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column122" runat="server" DataIndex="LOAN_NUMBER" Text="Loan Number" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column120" runat="server" DataIndex="LOAN_TYPE" Text="Loan Type" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column121" runat="server" DataIndex="SEGMENT_LOAN" Text="Sector" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column119" runat="server" DataIndex="TENOR" Text="Tenor" MinWidth="100"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn12" runat="server" DataIndex="LOAN_AMOUNT" Text="Loan Amount" Width="150" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:Column ID="Column123" runat="server" DataIndex="COLLATERAL_TYPE" Text="Collateral Type" MinWidth="100"></ext:Column>
                        <ext:Column ID="Column4" runat="server" DataIndex="BRANCH_NAME" Text="Branch Name" MinWidth="150"></ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar15" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

            <ext:GridPanel ID="gp_CaseAlert_Transaction" runat="server" Title="Case Alerts" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="300" ColumnWidth="1">
                <Features>
                    <ext:GroupingSummary
                        ID="gs_Alert"
                        runat="server"
                        GroupHeaderTplString='Account No. {name} ({rows.length} Transaction{[values.rows.length > 1 ? "s" : ""]})'
                        HideGroupedHeader="false"
                        StartCollapsed="true" ShowSummaryRow="false"
                        >
                    </ext:GroupingSummary>
                </Features>
                <Store>
                    <ext:Store ID="store9" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Transaction"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static" GroupField="Alert_Group">
                        <Model>
                            <ext:Model runat="server" ID="Model16">
                                <Fields>
                                    <ext:ModelField Name="Alert_Group" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn16" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:DateColumn ID="DateColumn12" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column18" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column21" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                        <ext:Column ID="Column124" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column125" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                        <ext:Column ID="Column126" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn13" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:NumberColumn ID="NumberColumn14" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:Column ID="Column127" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column128" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                        <ext:Column ID="Column129" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                        <ext:Column ID="Column130" runat="server" DataIndex="Account_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column131" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column132" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column133" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column134" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader6" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar16" runat="server" HideRefresh="true" />
                </BottomBar>
            </ext:GridPanel>

            <ext:GridPanel ID="gp_CaseAlert_Typology_Transaction_CaseAlert" runat="server" Title="Case Alert Typology Transaction" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store5" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Typology_Transaction_CaseAlert"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                        <Model>
                            <ext:Model runat="server" ID="Model8" IDProperty="PK_OneFCC_CaseManagement_Typology_Transaction_ID">
                                <Fields>
                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:DateColumn ID="DateColumn7" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column42" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column43" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                        <ext:Column ID="Column44" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column45" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                        <ext:Column ID="Column46" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn4" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:NumberColumn ID="NumberColumn9" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:Column ID="Column97" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column98" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                        <ext:Column ID="Column99" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                        <ext:Column ID="Column100" runat="server" DataIndex="Account_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column101" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column102" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column103" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column6" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader4" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="true" />
                </BottomBar>
            </ext:GridPanel>

            <ext:Panel runat="server" ID="pnl_CaseAlert_Outlier_Transaction_CaseAlert" Title="Case Alert Outlier Transaction" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="ColumnLayout" BodyPadding="10">
                <Items>
                    <ext:FieldSet runat="server" ID="fs_CaseAlert_Outlier_Transaction_CaseAlert" ColumnWidth="1" Title="Financial Statistics" Padding="5" Layout="ColumnLayout">
                        <Items>
                            <ext:Container runat="server" ColumnWidth="0.5" PaddingSpec="0 10" Layout="AnchorLayout"> 
                                <Items>
                                    <ext:DisplayField runat="server" ID="txt_MeanDebit_CaseAlert" AnchorHorizontal="100%" FieldLabel="Mean Debit (IDR)" LabelWidth="130"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_MeanCredit_CaseAlert" AnchorHorizontal="100%" FieldLabel="Mean Credit (IDR)" LabelWidth="130"></ext:DisplayField>
                                </Items>
                            </ext:Container>
                            <ext:Container runat="server" ColumnWidth="0.5" PaddingSpec="0 10" Layout="AnchorLayout">
                                <Items>
                                    <ext:DisplayField runat="server" ID="txt_ModusDebit_CaseAlert" AnchorHorizontal="100%" FieldLabel="Modus Debit (IDR)" LabelWidth="130"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_ModusCredit_CaseAlert" AnchorHorizontal="100%" FieldLabel="Modus Credit (IDR)" LabelWidth="130"></ext:DisplayField>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:FieldSet>

                    <ext:Container runat="server" ColumnWidth="1">
                        <Items>
                            <ext:GridPanel ID="gp_CaseAlert_Outlier_Transaction_CaseAlert" runat="server" Title="" MarginSpec="0 0 10 0" Border="true" MinHeight="200">
                                <View>
                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                </View>
                                <Store>
                                    <ext:Store ID="store6" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Outlier_Transaction_CaseAlert"
                                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                                        <Model>
                                            <ext:Model runat="server" ID="Model9" IDProperty="PK_OneFCC_CaseManagement_Outlier_Transaction_ID">
                                                <Fields>
                                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                                    <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                                    <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Sorters></Sorters>
                                        <Proxy>
                                            <ext:PageProxy />
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server" Text="No"></ext:RowNumbererColumn>
                                        <ext:DateColumn ID="DateColumn8" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                                        <ext:Column ID="Column47" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column48" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                                        <ext:Column ID="Column49" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                                        <ext:Column ID="Column50" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                                        <ext:Column ID="Column51" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                                        <ext:NumberColumn ID="NumberColumn5" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                        <ext:NumberColumn ID="NumberColumn10" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                        <ext:Column ID="Column104" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                                        <ext:Column ID="Column105" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column106" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                                        <ext:Column ID="Column107" runat="server" DataIndex="Account_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column108" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column109" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column110" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>                                    
                                        <ext:Column ID="Column8" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <Plugins>
                                    <ext:FilterHeader ID="FilterHeader5" runat="server" Remote="true"></ext:FilterHeader>
                                </Plugins>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Panel>

            <ext:GridPanel ID="gp_RFI_CaseAlert" runat="server" Title="Request For Information (RFI)" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store_RFI_CaseAlert" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="Model7" IDProperty="PK_OneFCC_CaseManagement_RFI_ID">
                                <Fields>
                                    <ext:ModelField Name="EmailTo" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="EmailCc" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Subject" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="EmailStatusName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedDate" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="AttachmentName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Notes" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column36" runat="server" DataIndex="EmailTo" Text="Email To" MinWidth="150" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column37" runat="server" DataIndex="EmailCc" Text="CC" MinWidth="150" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column38" runat="server" DataIndex="Subject" Text="Subject" Width="200" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column39" runat="server" DataIndex="EmailStatusName" Text="Status" Width="100"></ext:Column>
                        <ext:Column ID="Column40" runat="server" DataIndex="CreatedBy" Text="Created By" Width="100"></ext:Column>
                        <ext:Column ID="Column41" runat="server" DataIndex="AttachmentName" Text="Attachment" MinWidth="200">
                            <Commands>
                                <ext:ImageCommand Icon="DiskDownload" CommandName="Download" ToolTip-Text="Download Report File"></ext:ImageCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_RFI_CaseAlert">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_OneFCC_CaseManagement_RFI_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:Column>                        
                        <ext:DateColumn ID="DateColumn6" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column9" runat="server" DataIndex="Notes" Text="Notes" MinWidth="150" CellWrap="true"></ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

            <ext:GridPanel ID="gp_WorkflowHistory_CaseAlert" runat="server" Title="Workflow History" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store7" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_WorkflowHistory_CaseAlert"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                        <Model>
                            <ext:Model runat="server" ID="Model10" IDProperty="PK_CaseManagement_WorkflowHistory_ID">
                                <Fields>
                                    <ext:ModelField Name="Workflow_Step" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Proposed_Action" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Analysis_Result" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedDate" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column52" runat="server" DataIndex="Workflow_Step" Text="Step" Width="50" Align="Center"></ext:Column>
                        <ext:Column ID="Column53" runat="server" DataIndex="CreatedBy" Text="Created By" Width="150"></ext:Column>
                        <ext:Column ID="Column54" runat="server" DataIndex="Proposed_Action" Text="Proposed Status" Width="150"></ext:Column>
                        <ext:Column ID="Column55" runat="server" DataIndex="Analysis_Result" Text="Analysis Result" Width="300" CellWrap="true"></ext:Column>
                        <ext:DateColumn ID="DateColumn9" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.9, height: size.height * 0.9});" />
            <Resize Handler="#{window_CaseAlert}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_CaseAlert_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_CaseAlert_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%-- ================== END OF WINDOW CASE ALERT ========================== --%>

</asp:Content>

