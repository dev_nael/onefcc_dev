﻿Imports Ext
Imports Elmah
Imports System.Data
Imports CasemanagementBLL
Imports CasemanagementDAL
Imports System.Data.SqlClient
Imports OfficeOpenXml
Imports System.IO
Imports System.Drawing
Imports OfficeOpenXml.Style

Partial Class CaseManagementAlertBatchCloseAdd
    Inherits ParentPage

    Const PATH_TEMP_DIR As String = "~\temp\"

    Public Property IDModule() As String
        Get
            Return Session("CaseManagementAlertBatchCloseAdd.IDModule")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertBatchCloseAdd.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("CaseManagementAlertBatchCloseAdd.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertBatchCloseAdd.IDUnik") = value
        End Set
    End Property

    Public Property CIFNo() As String
        Get
            Return Session("CaseManagementAlertBatchCloseAdd.CIFNo")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertBatchCloseAdd.CIFNo") = value
        End Set
    End Property

    Public Property Workflow_Step() As Integer
        Get
            Return Session("CaseManagementAlertBatchCloseAdd.Workflow_Step")
        End Get
        Set(ByVal value As Integer)
            Session("CaseManagementAlertBatchCloseAdd.Workflow_Step") = value
        End Set
    End Property

    Public Property Workflow_Step_Total() As Integer
        Get
            Return Session("CaseManagementAlertBatchCloseAdd.Workflow_Step_Total")
        End Get
        Set(ByVal value As Integer)
            Session("CaseManagementAlertBatchCloseAdd.Workflow_Step_Total") = value
        End Set
    End Property

    Public Property Workflow_ID() As Integer
        Get
            Return Session("CaseManagementAlertBatchCloseAdd.Workflow_ID")
        End Get
        Set(ByVal value As Integer)
            Session("CaseManagementAlertBatchCloseAdd.Workflow_ID") = value
        End Set
    End Property

    Public Property CaseID_CaseAlert() As Long
        Get
            If Session("CaseManagementAlertBatchCloseAdd.CaseID_CaseAlert") Is Nothing Then
                Session("CaseManagementAlertBatchCloseAdd.CaseID_CaseAlert") = 0
            End If
            Return Session("CaseManagementAlertBatchCloseAdd.CaseID_CaseAlert")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertBatchCloseAdd.CaseID_CaseAlert") = value
        End Set
    End Property

    Public Property RFI_ID() As Long
        Get
            Return Session("CaseManagementAlertBatchCloseAdd.RFI_ID")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertBatchCloseAdd.RFI_ID") = value
        End Set
    End Property

    Public Property dtValidUpload() As DataTable
        Get
            Return Session("CaseManagementAlertBatchCloseAdd.dtValidUpload")
        End Get
        Set(ByVal value As DataTable)
            Session("CaseManagementAlertBatchCloseAdd.dtValidUpload") = value
        End Set
    End Property

    Public Property dtInvalidUpload() As DataTable
        Get
            Return Session("CaseManagementAlertBatchCloseAdd.dtInvalidUpload")
        End Get
        Set(ByVal value As DataTable)
            Session("CaseManagementAlertBatchCloseAdd.dtInvalidUpload") = value
        End Set
    End Property

    'Enum for Batch Closing methods
    Public Enum enumBatchCloseMethod
        Selection = 1
        Upload = 2
    End Enum

    Public Property intBatchCloseMethod() As Integer
        Get
            Return Session("CaseManagementAlertBatchCloseAdd.intBatchCloseMethod")
        End Get
        Set(ByVal value As Integer)
            Session("CaseManagementAlertBatchCloseAdd.intBatchCloseMethod") = value
        End Set
    End Property

    Private Sub CaseManagementAlertBatchCloseAdd_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Insert
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As New Integer
            If IDModule IsNot Nothing Then
                intModuleID = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = ObjModule.ModuleLabel & " - Batch Closing"

                SetCommandColumnLocation()

                '11-Apr-2022 Adi : Set initial Batch Close Method to Selection
                intBatchCloseMethod = enumBatchCloseMethod.Selection

                '21-Apr-2022 Adi 
                txt_WindowOther_Alert_Type.Hidden = True
                txt_WindowOther_Case_Description.Hidden = True
                txt_Account_No.Hidden = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub btn_Back_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Save_Click()
        Try
            If String.IsNullOrWhiteSpace(txt_Closing_Reason.Value) Then
                Throw New ApplicationException(txt_Closing_Reason.FieldLabel & " is required.")
            End If

            If intBatchCloseMethod = enumBatchCloseMethod.Selection Then
                BatchCloseSelection()
            ElseIf intBatchCloseMethod = enumBatchCloseMethod.Upload Then
                BatchCloseUpload()
            End If

            'Show Confirmation
            LblConfirmation.Text = "Case Management Batch Closing has been submitted for approval."
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BatchCloseSelection()
        Try
            'Cek minimal 1 row dipilih
            Dim smCaseAlert As RowSelectionModel = TryCast(gp_CaseAlert.GetSelectionModel(), RowSelectionModel)
            If smCaseAlert.SelectedRows.Count = 0 Then
                Throw New ApplicationException("Minimum 1 alert must be selected!")
            End If

            Dim strQuery As String = ""

            'Insert Header
            strQuery = "INSERT INTO OneFCC_CaseManagement_BatchClosed ("
            strQuery &= "Closing_Reason,"
            strQuery &= "Status,"
            strQuery &= "Active,"
            strQuery &= "CreatedBy,"
            strQuery &= "CreatedDate) "
            strQuery &= "VALUES ("
            strQuery &= "'" & txt_Closing_Reason.Value & "',"
            strQuery &= "'Waiting for Approval',"
            strQuery &= "1,"
            strQuery &= "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
            strQuery &= "GETDATE());"
            strQuery &= " SELECT SCOPE_IDENTITY()"
            Dim lngBatchClosedID As Long = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Looping through Selected
            For Each item As SelectedRow In smCaseAlert.SelectedRows
                Dim pkCaseID = item.RecordID.ToString

                'Insert into Workflow History
                strQuery = "INSERT INTO OneFCC_CaseManagement_BatchClosed_Detail ("
                strQuery &= "FK_CaseManagement_BatchClosed_ID,"
                strQuery &= "FK_CaseManagement_ID,"
                strQuery &= "Active,"
                strQuery &= "CreatedBy,"
                strQuery &= "CreatedDate) "
                strQuery &= "VALUES ("
                strQuery &= lngBatchClosedID & ","
                strQuery &= pkCaseID & ","
                strQuery &= "1,"
                strQuery &= "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
                strQuery &= "GETDATE())"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                'Update Case Management Status and Proposed Status
                strQuery = "UPDATE OneFCC_CaseManagement SET FK_Proposed_Status_ID=6"       '6=Submitted as Closed by System
                strQuery &= ", FK_CaseStatus_ID=6"
                strQuery &= ", LastUpdateBy='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                strQuery &= ", LastUpdateDate=GETDATE()"
                strQuery &= " WHERE PK_CaseManagement_ID=" & pkCaseID
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                'Insert into Workflow History
                strQuery = "INSERT INTO OneFCC_CaseManagement_WorkflowHistory ("
                strQuery &= "FK_CaseManagement_ID,"
                strQuery &= "Workflow_Step,"
                strQuery &= "FK_Proposed_Status_ID,"
                strQuery &= "Analysis_Result,"
                strQuery &= "CreatedBy,"
                strQuery &= "CreatedDate) "
                strQuery &= "VALUES ("
                strQuery &= pkCaseID & ","
                strQuery &= "(SELECT Workflow_Step FROM OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & pkCaseID & "),"
                strQuery &= "6,"
                strQuery &= "'" & txt_Closing_Reason.Value & "',"
                strQuery &= "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
                strQuery &= "GETDATE())"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Function CheckIfStringExists(strPart As String, strFull As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim dt As DataTable = New DataTable

            Dim strsplit As String() = strFull.Split(","c)
            For Each item As String In strsplit
                If strPart = item Then
                    Return True
                End If
            Next

            Return bolResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        If buttonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub

    Sub SetCommandColumnLocation()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim buttonPosition As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            buttonPosition = objParamSettingbutton.SettingValue
        End If

        ColumnActionLocation(gp_CaseAlert, cc_CaseAlert, buttonPosition)
        ColumnActionLocation(gp_BatchCloseUpload_Valid, cc_BatchCloseUpload_Valid, buttonPosition)

    End Sub

    Protected Sub store_ReadData_CaseAlert(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = ""

            'Get allowed Case Status untuk batch closing
            strQuery = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=15"
            Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                strQuery = "SELECT * FROM vw_OneFCC_CaseManagement"
                strQuery += " WHERE FK_CaseStatus_ID IN (" & drParam("ParameterValue") & ")"
            Else    'Hard Code (0=New, 2=Submitted as non-issue)
                strQuery = "SELECT * FROM vw_OneFCC_CaseManagement"
                strQuery += " WHERE (FK_CaseStatus_ID = 0 OR FK_CaseStatus_ID = 2)"
            End If

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " AND " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert.GetStore.DataSource = DataPaging
            gp_CaseAlert.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_NewCase(sender As Object, e As DirectEventArgs)
        Try

            'Dim ID As String = e.ExtraParams(0).Value
            'Dim strCommandName As String = e.ExtraParams(1).Value
            'If strCommandName = "Detail" Then
            '    LoadDataCaseAlert(ID)
            'End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#Region "List of New Case Alerts"
    Protected Sub gc_CaseAlert(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            If strCommandName = "Detail" Then
                LoadDataCaseAlert(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDataCaseAlert(strCaseID As String)
        Try
            'To Do Load Data Other Case
            Dim strSQL As String = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & strCaseID
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            If drCM IsNot Nothing Then

                Me.CaseID_CaseAlert = drCM("PK_CaseManagement_ID")

                'General Information
                txt_WindowOther_PK_CaseManagement_ID.Value = drCM("PK_CaseManagement_ID")
                txt_WindowOther_Case_Description.Value = drCM("Case_Description")
                txt_WindowOther_Alert_Type.Value = drCM("Alert_Type")

                If Not IsDBNull(drCM("FK_CaseStatus_ID")) Then
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_ProposedAction WHERE PK_OneFCC_CaseManagement_ProposedAction_ID=" & drCM("FK_CaseStatus_ID")
                    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drTemp IsNot Nothing Then
                        txt_WindowOther_FK_Case_Status_ID.Value = drTemp("PK_OneFCC_CaseManagement_ProposedAction_ID") & " - " & drTemp("Proposed_Action")
                    Else
                        txt_WindowOther_FK_Case_Status_ID.Value = "New Case"
                    End If
                Else
                    txt_WindowOther_FK_Case_Status_ID.Value = "New Case"
                End If

                If Not IsDBNull(drCM("CreatedDate")) Then
                    txt_WindowOther_CreatedDate.Value = CDate(drCM("CreatedDate")).ToString("dd-MMM-yyyy HH:mm:ss")
                End If

                txt_WindowOther_Workflow_Step.Value = drCM("Workflow_Step_Of")
                txt_WindowOther_PIC.Value = drCM("PIC")

                'Customer Information
                txt_CIF_No.Value = drCM("CIF_No")
                txt_Account_No.Value = drCM("Account_No")
                txt_Customer_Name.Value = drCM("Customer_Name")
                LoadCustomerInformation(drCM("CIF_No"))

                'List of Accounts
                LoadDataAccounts(drCM("CIF_No"))

                '29-Mar-2022 Adi : Load data Loan
                LoadDataLoans(drCM("CIF_No"))

                'Bind RFI sent
                BindDataRFI_CaseAlert(drCM("PK_CaseManagement_ID"))

                'Reload GP Transaction
                gp_CaseAlert_Typology_Transaction_CaseAlert.Hidden = True
                pnl_CaseAlert_Outlier_Transaction_CaseAlert.Hidden = True

                'Load Transaction
                If Not IsDBNull(drCM("Alert_Type")) Then
                    If drCM("Alert_Type") = "Typology Risk" Then
                        gp_CaseAlert_Typology_Transaction_CaseAlert.GetStore.Reload()
                        gp_CaseAlert_Typology_Transaction_CaseAlert.Hidden = False
                    ElseIf drCM("Alert_Type") = "Financial Risk" Then
                        gp_CaseAlert_Outlier_Transaction_CaseAlert.GetStore.Reload()
                        pnl_CaseAlert_Outlier_Transaction_CaseAlert.Hidden = False

                        'Display Mean dan Modus
                        strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=2"
                        Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                        If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                            fs_CaseAlert_Outlier_Transaction_CaseAlert.Title = "<b>Financial Statistics in past " & drParam("ParameterValue") & " month(s)</b>"
                        End If

                        strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Outlier WHERE FK_CaseManagement_ID=" & drCM("PK_CaseManagement_ID")
                        Dim drCMO As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                        If drCMO IsNot Nothing Then
                            txt_MeanDebit_CaseAlert.Value = CDbl(drCMO("Mean_Debit")).ToString("#,##0.00")
                            txt_MeanCredit_CaseAlert.Value = CDbl(drCMO("Mean_Credit")).ToString("#,##0.00")

                            txt_ModusDebit_CaseAlert.Value = CDbl(drCMO("Modus_Debit")).ToString("#,##0.00")
                            txt_ModusCredit_CaseAlert.Value = CDbl(drCMO("Modus_Credit")).ToString("#,##0.00")
                        End If
                    End If
                End If

                'Load Workflow History
                gp_WorkflowHistory_CaseAlert.GetStore.Reload()

                '21-Apr-2022 Adi Load Case Alert Transaction
                gp_CaseAlert_Transaction.GetStore.Reload()

            End If

            'Show Window
            window_CaseAlert.Hidden = False

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadCustomerInformation(strCIF As String)
        Try
            If String.IsNullOrEmpty(strCIF) Then
                strCIF = ""
            End If

            Dim strSQL As String = ""
            strSQL = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & strCIF & "'"
            Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            If drCustomer IsNot Nothing Then
                If Not IsDBNull(drCustomer("DATEOFBIRTH")) Then
                    txt_DOB.Value = CDate(drCustomer("DATEOFBIRTH")).ToString("dd-MMM-yyyy")
                End If

                txt_POB.Value = drCustomer("PLACEOFBIRTH")

                If Not IsDBNull(drCustomer("FK_AML_JenisKelamin_Code")) Then
                    strSQL = "SELECT TOP 1 * FROM goAML_Ref_Jenis_Kelamin WHERE Kode='" & drCustomer("FK_AML_JenisKelamin_Code") & "'"
                    Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drGender IsNot Nothing Then
                        txt_Gender.Value = drGender("Kode") & " - " & drGender("Keterangan")
                    Else
                        txt_Gender.Value = drCustomer("FK_AML_JenisKelamin_Code")
                    End If
                End If

                If Not IsDBNull(drCustomer("FK_AML_Creation_Branch_Code")) Then
                    strSQL = "SELECT TOP 1 * FROM AML_BRANCH WHERE FK_AML_BRANCH_CODE='" & drCustomer("FK_AML_Creation_Branch_Code") & "'"
                    Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drGender IsNot Nothing Then
                        txt_CIF_Opening_Branch.Value = drGender("FK_AML_BRANCH_CODE") & " - " & drGender("BRANCH_NAME")
                    Else
                        txt_CIF_Opening_Branch.Value = drCustomer("FK_AML_Creation_Branch_Code")
                    End If
                End If

                If Not IsDBNull(drCustomer("OpeningDate")) Then
                    txt_CIF_Opening_Date.Value = CDate(drCustomer("OpeningDate")).ToString("dd-MMM-yyyy")
                End If

                If Not IsDBNull(drCustomer("IS_PEP")) Then
                    If drCustomer("IS_PEP") = 0 Then
                        txt_Is_PEP.Value = "No"
                    Else
                        txt_Is_PEP.Value = "Yes"
                    End If
                Else
                    txt_Is_PEP.Value = "N/A"
                End If

                If Not IsDBNull(drCustomer("FK_AML_RISK_CODE")) Then
                    strSQL = "SELECT TOP 1 * FROM AML_RISK_RATING WHERE RISK_RATING_CODE ='" & drCustomer("FK_AML_RISK_CODE") & "'"
                    Dim drRisk As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drRisk IsNot Nothing Then
                        txt_Risk_Code.Value = drRisk("RISK_RATING_CODE") & " - " & drRisk("RISK_RATING_NAME")
                    Else
                        txt_Risk_Code.Value = drCustomer("FK_AML_RISK_CODE")
                    End If

                    If drCustomer("FK_AML_RISK_CODE") = "H" Then
                        txt_Risk_Code.FieldStyle = "Color:Red;font-weight:Bold;"
                    Else
                        txt_Risk_Code.FieldStyle = "Color:Black;font-weight:Normal;"
                    End If
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadDataAccounts(strCIFNo As String)
        Try
            Dim strQuery As String = "SELECT acc.*, card.FK_AML_CARD_NO, card.LIMITCREDITCARD"
            strQuery &= " FROM AML_ACCOUNT acc"
            strQuery &= " LEFT JOIN AML_CARD card ON acc.ACCOUNT_NO = card.ACCOUNT_NO"
            strQuery &= " WHERE acc.CIFNO='" & strCIFNo & "'"

            Dim dtAccount As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If dtAccount Is Nothing Then
                dtAccount = New DataTable
            End If

            If dtAccount IsNot Nothing Then
                'Running the following script for additional columns
                dtAccount.Columns.Add(New DataColumn("BRANCH_NAME", GetType(String)))
                dtAccount.Columns.Add(New DataColumn("ACCOUNT_TYPE", GetType(String)))
                dtAccount.Columns.Add(New DataColumn("PRODUCT_NAME", GetType(String)))

                'Get reference Table
                strQuery = "SELECT * FROM AML_BRANCH"
                Dim dtBranch As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                strQuery = "SELECT * FROM AML_ACCOUNT_TYPE"
                Dim dtAccountType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                strQuery = "SELECT * FROM AML_PRODUCT"
                Dim dtProduct As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                For Each row As DataRow In dtAccount.Rows
                    If dtBranch IsNot Nothing AndAlso Not IsDBNull(row("FK_AML_BRANCH_CODE")) Then
                        Dim drCek = dtBranch.Select("FK_AML_BRANCH_CODE='" & row("FK_AML_BRANCH_CODE") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("BRANCH_NAME") = drCek("BRANCH_NAME")
                        End If
                    End If
                    If dtAccountType IsNot Nothing AndAlso Not IsDBNull(row("FK_AML_ACCOUNT_TYPE_CODE")) Then
                        Dim drCek = dtAccountType.Select("FK_AML_ACCOUNT_TYPE_CODE='" & row("FK_AML_ACCOUNT_TYPE_CODE") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("ACCOUNT_TYPE") = drCek("ACCOUNT_TYPE_NAME")
                        End If
                    End If
                    If dtProduct IsNot Nothing AndAlso Not IsDBNull(row("FK_AML_PRODUCT_CODE")) Then
                        Dim drCek = dtProduct.Select("FK_AML_PRODUCT_CODE='" & row("FK_AML_PRODUCT_CODE") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("PRODUCT_NAME") = drCek("PRODUCT_NAME")
                        End If
                    End If

                Next

                'Bind to gridpanel
                gp_Account.GetStore.DataSource = dtAccount
                gp_Account.GetStore.DataBind()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDataLoans(strCIFNo As String)
        Try
            Dim strQuery As String = "SELECT al.*, ac.COLLATERAL_TYPE"
            strQuery &= " FROM AML_LOAN al"
            strQuery &= " LEFT JOIN AML_COLLATERAL_LOAN acl ON acl.LOAN_NUMBER = al.LOAN_NUMBER"
            strQuery &= " LEFT JOIN AML_COLLATERAL ac ON ac.COLLATERAL_NUMBER = acl.COLLATERAL_NUMBER"
            strQuery &= " WHERE al.CIF='" & strCIFNo & "'"

            Dim dtLoan As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If dtLoan Is Nothing Then
                dtLoan = New DataTable
            End If

            If dtLoan IsNot Nothing Then
                'Running the following script for additional columns
                dtLoan.Columns.Add(New DataColumn("BRANCH_NAME", GetType(String)))

                'Get reference Table
                strQuery = "SELECT * FROM AML_BRANCH"
                Dim dtBranch As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                For Each row As DataRow In dtLoan.Rows
                    If dtBranch IsNot Nothing AndAlso Not IsDBNull(row("BRANCH_CODE")) Then
                        Dim drCek = dtBranch.Select("FK_AML_BRANCH_CODE='" & row("BRANCH_CODE") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("BRANCH_NAME") = drCek("BRANCH_NAME")
                        End If
                    End If
                Next

                'Bind to gridpanel
                gp_Loan.GetStore.DataSource = dtLoan
                gp_Loan.GetStore.DataBind()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_CaseAlert_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            window_CaseAlert.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BindDataRFI_CaseAlert(strCaseID As String)
        Try
            Dim strQuery As String = "SELECT * FROM OneFCC_CaseManagement_RFI WHERE FK_CaseManagement_ID=" & strCaseID
            Dim dtRFI As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If dtRFI Is Nothing Then
                dtRFI = New DataTable
            End If

            If dtRFI IsNot Nothing Then
                'Running the following script for additional columns
                dtRFI.Columns.Add(New DataColumn("EmailStatusName", GetType(String)))

                'Get reference Table
                strQuery = "SELECT * FROM EmailStatus"
                Dim dtEmailStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                For Each row As DataRow In dtRFI.Rows
                    If dtEmailStatus IsNot Nothing AndAlso Not IsDBNull(row("FK_EmailStatus_ID")) Then
                        Dim drCek = dtEmailStatus.Select("PK_EmailStatus_ID='" & row("FK_EmailStatus_ID") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("EmailStatusName") = drCek("EmailStatusName")
                        End If
                    End If
                Next

                'Bind to gridpanel
                gp_RFI_CaseAlert.GetStore.DataSource = dtRFI
                gp_RFI_CaseAlert.GetStore.DataBind()

            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub gc_RFI_CaseAlert(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            If strCommandName = "Download" Then
                DownloadRFI(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Outlier_Transaction_CaseAlert(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Outlier AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_CaseAlert

            'Dim strQuery As String = "SELECT ofcmtt.*"
            'strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            'strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            'strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmt"
            'strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            'strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            'strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            'strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            'strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            'strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_CaseAlert

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_CaseAlert

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " AND " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Outlier_Transaction_CaseAlert.GetStore.DataSource = DataPaging
            gp_CaseAlert_Outlier_Transaction_CaseAlert.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Typology_Transaction_CaseAlert(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_CaseAlert

            'Dim strQuery As String = "SELECT ofcmtt.*"
            'strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            'strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            'strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            'strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            'strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            'strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            'strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_CaseAlert

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_CaseAlert

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " AND " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Typology_Transaction_CaseAlert.GetStore.DataSource = DataPaging
            gp_CaseAlert_Typology_Transaction_CaseAlert.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_WorkflowHistory_CaseAlert(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT history.*, proposed.Proposed_Action FROM onefcc_casemanagement_workflowhistory history"
            strQuery += " LEFT JOIN OneFCC_CaseManagement_ProposedAction proposed ON history.FK_Proposed_Status_ID = proposed.PK_OneFCC_CaseManagement_ProposedAction_ID"
            strQuery += " WHERE FK_CaseManagement_ID = " & Me.CaseID_CaseAlert

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)

            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_WorkflowHistory_CaseAlert.GetStore.DataSource = DataPaging
            gp_WorkflowHistory_CaseAlert.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#End Region

    Private Sub DownloadRFI(id As Long)
        Using objdb As New CasemanagementEntities
            Dim objRFI = objdb.OneFCC_CaseManagement_RFI.Where(Function(x) x.PK_OneFCC_CaseManagement_RFI_ID = id).FirstOrDefault
            If Not objRFI Is Nothing Then
                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & objRFI.AttachmentName)
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(objRFI.Attachment)
                Response.End()
            End If
        End Using
    End Sub

#Region "Update 8-Apr-2022 Adi : Batch Closing by Upload"
    Protected Sub btn_BatchCloseSelection_Click(sender As Object, e As DirectEventArgs)
        Try
            intBatchCloseMethod = enumBatchCloseMethod.Selection

            gp_CaseAlert.Hidden = False
            pnl_BatchCloseUpload.Hidden = True
            btn_BatchCloseSelection.Disabled = True
            btn_BatchCloseUpload.Disabled = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_BatchCloseUpload_Click(sender As Object, e As DirectEventArgs)
        Try
            intBatchCloseMethod = enumBatchCloseMethod.Upload

            gp_CaseAlert.Hidden = True
            pnl_BatchCloseUpload.Hidden = False
            btn_BatchCloseSelection.Disabled = False
            btn_BatchCloseUpload.Disabled = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_DownloadTemplate_Click(sender As Object, e As DirectEventArgs)
        Try

            'Create Directory if not exists
            If Not Directory.Exists(Server.MapPath(PATH_TEMP_DIR)) Then
                Directory.CreateDirectory(Server.MapPath(PATH_TEMP_DIR))
            End If

            'Filename and location for output template
            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            Dim objfileinfo = New FileInfo(Server.MapPath(PATH_TEMP_DIR & tempfilexls))

            'Object to generate excel template
            Dim dtUploadTemplate As Data.DataTable = New Data.DataTable()
            dtUploadTemplate.Columns.Add(New Data.DataColumn("Case ID *", GetType(Long)))
            dtUploadTemplate.Columns.Add(New Data.DataColumn("CIF *", GetType(String)))

            'Generate excel template
            Using resource As New ExcelPackage(objfileinfo)
                'Membuath work sheet
                Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("BatchClosingCaseAlert")

                'Mengisi row 1 dengan header template
                ws.Cells("A1").LoadFromDataTable(dtUploadTemplate, True)

                Dim rng As ExcelRange

                'Kolom B1 set cell format to Text
                rng = ws.Cells("B1:B" & (Int16.MaxValue) - 1)
                rng.Style.Numberformat.Format = "@"

                'Background and Font Color
                rng = ws.Cells("A1:B1")
                rng.Style.Font.Color.SetColor(Color.White)
                rng.Style.Font.Bold = True
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid
                rng.Style.Fill.BackgroundColor.SetColor(Color.Blue)

                ws.Cells(ws.Dimension.Address).AutoFitColumns()
                resource.Save()
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("content-disposition", "attachment;filename=" & "CaseManagementBatchClosingUpload.xlsx")
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                Response.End()
            End Using

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_BatchCloseUpload_Save_Click(sender As Object, e As DirectEventArgs)
        Try
            'Validate
            If Not fileBatchUpload.HasFile Then
                Throw New ApplicationException("Please browse a file to import.")
            End If
            If Not fileBatchUpload.FileName.EndsWith(".xlsx") Then
                Throw New ApplicationException("Only allowed excel (.xlsx) file format.")
            End If

            'Read Excel content
            If fileBatchUpload.HasFile Then
                'Directory file fisik yang di-write di server
                Dim strFilePath As String = Server.MapPath(PATH_TEMP_DIR)
                Dim strFileName As String = Guid.NewGuid.ToString & ".xlsx"

                'Write file uploaded to server
                System.IO.File.WriteAllBytes(strFilePath & strFileName, fileBatchUpload.FileBytes)

                Dim br As BinaryReader
                Dim bData As Byte()
                br = New BinaryReader(System.IO.File.OpenRead(strFilePath & strFileName))
                bData = br.ReadBytes(br.BaseStream.Length)
                Dim ms As MemoryStream = New MemoryStream(bData, 0, bData.Length)
                ms.Write(bData, 0, bData.Length)

                Using excelFile As New ExcelPackage(ms)
                    If excelFile.Workbook.Worksheets("BatchClosingCaseAlert") IsNot Nothing Then
                        ReadFileUpload(excelFile.Workbook.Worksheets("BatchClosingCaseAlert"))
                    End If
                End Using

                ms.Flush()
                ms.Close()
            End If

            'Bind to Grid 
            BindUploadResult()

            'Clear File Upload
            fileBatchUpload.Reset()

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ReadFileUpload(ByVal sheet As ExcelWorksheet)
        Try

            Dim strQuery As String = ""

            'Create Temp DataTable for Valid Upload Data
            dtValidUpload = New DataTable
            dtValidUpload.Columns.Add(New DataColumn("PK_CaseManagement_ID", GetType(Long)))
            dtValidUpload.Columns.Add(New DataColumn("Alert_Type", GetType(String)))
            dtValidUpload.Columns.Add(New DataColumn("Case_Description", GetType(String)))
            dtValidUpload.Columns.Add(New DataColumn("CIF_No", GetType(String)))
            dtValidUpload.Columns.Add(New DataColumn("Customer_Name", GetType(String)))
            dtValidUpload.Columns.Add(New DataColumn("CaseStatus", GetType(String)))
            dtValidUpload.Columns.Add(New DataColumn("ProcessDate", GetType(String)))
            dtValidUpload.Columns.Add(New DataColumn("Aging_Workdays", GetType(String)))

            'Create Temp DataTable for Invalid Upload Data
            dtInvalidUpload = New DataTable
            dtInvalidUpload.Columns.Add(New DataColumn("PK_CaseManagement_ID", GetType(Long)))
            dtInvalidUpload.Columns.Add(New DataColumn("CIF_No", GetType(String)))
            dtInvalidUpload.Columns.Add(New DataColumn("Invalid_Msg", GetType(String)))

            'Loop through data
            Dim IRow As Integer = 2
            While IRow <> 0
                'Cek apakah CaseID adalah BIGINT atau bukan
                Dim strCaseID As String = sheet.Cells(IRow, 1).Text.Trim
                Dim strCIF As String = sheet.Cells(IRow, 2).Text.Trim

                'Gunakan CaseID Sebagai Key... 
                'Cari CaseID, jika kosong diasumsikan bahwa itu row terakhir
                If Not String.IsNullOrEmpty(strCaseID) Then
                    If Not IsNumeric(strCaseID) Then
                        Throw New ApplicationException("Case ID must in numeric values.")
                    End If

                    strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & strCaseID
                    Dim drCek As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                    If drCek IsNot Nothing Then
                        Dim strInvalidMsg As String = ""

                        'Validate CIF Number
                        strQuery = "SELECT TOP 1 * FROM goAML_Ref_Customer WHERE CIF='" & strCIF & "'"
                        Dim drCust As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                        If drCust Is Nothing Then
                            strInvalidMsg &= "<br>CIF not exists in Database."
                        End If

                        'Validate Case ID is match with CIF
                        If Not IsDBNull(drCek("CIF_No")) AndAlso drCek("CIF_No") <> strCIF Then
                            strInvalidMsg &= "<br>Case ID not matched with CIF. This Case ID should be owned by CIF " & drCek("CIF_No") & "."
                        End If

                        'Get allowed Case Status untuk batch closing
                        strQuery = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=15"
                        Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                        If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                            If Not CheckIfStringExists(drCek("FK_CaseStatus_ID"), drParam("ParameterValue")) Then
                                strInvalidMsg &= "<br>Case Status " & drCek("CaseStatus") & " is not allowed to be closed."
                            Else    'Hard Code (0=New, 2=Submitted as non-issue)
                                If Not IsDBNull(drCek("FK_CaseStatus_ID")) AndAlso Not (drCek("FK_CaseStatus_ID") = 0 Or drCek("FK_CaseStatus_ID") = 2) Then
                                    strInvalidMsg &= "<br>Case Status " & drCek("CaseStatus") & " is not allowed to be closed."
                                End If
                            End If
                        End If

                        'Insert ke dtInvalid jika strInvalidMsg tidak kosong
                        If Not String.IsNullOrEmpty(strInvalidMsg) Then
                            'Buang <br> pertama
                            strInvalidMsg = Mid(strInvalidMsg, 5)
                            dtInvalidUpload.Rows.Add(strCaseID, strCIF, strInvalidMsg)
                        Else
                            dtValidUpload.Rows.Add(strCaseID, drCek("Alert_Type"), drCek("Case_Description"), strCIF, drCek("Customer_Name"), drCek("CaseStatus"), drCek("ProcessDate"), drCek("Aging_Workdays"))
                        End If


                    Else
                        dtInvalidUpload.Rows.Add(strCaseID, strCIF, "Case ID is not exits in Database")
                    End If

                    IRow = IRow + 1
                Else
                    IRow = 0
                End If
            End While

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindUploadResult()
        Try
            gp_BatchCloseUpload_Valid.GetStore.DataSource = dtValidUpload
            gp_BatchCloseUpload_Valid.GetStore.DataBind()

            gp_BatchCloseUpload_InValid.GetStore.DataSource = dtInvalidUpload
            gp_BatchCloseUpload_InValid.GetStore.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BatchCloseUpload()
        Try
            'Cek apakah masih ada yang InvalidUpload
            If dtInvalidUpload.Rows.Count > 0 Then
                Throw New ApplicationException("There is still invalid data.")
            End If

            'Cek minimal 1 row di ValidUpload
            If dtValidUpload.Rows.Count = 0 Then
                Throw New ApplicationException("There is no valid Case Alerts to be closed.")
            End If

            Dim strQuery As String = ""

            'Insert Header
            strQuery = "INSERT INTO OneFCC_CaseManagement_BatchClosed ("
            strQuery &= "Closing_Reason,"
            strQuery &= "Status,"
            strQuery &= "Active,"
            strQuery &= "CreatedBy,"
            strQuery &= "CreatedDate) "
            strQuery &= "VALUES ("
            strQuery &= "'" & txt_Closing_Reason.Value & "',"
            strQuery &= "'Waiting for Approval',"
            strQuery &= "1,"
            strQuery &= "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
            strQuery &= "GETDATE());"
            strQuery &= " SELECT SCOPE_IDENTITY()"
            Dim lngBatchClosedID As Long = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Looping through Selected
            For Each item As DataRow In dtValidUpload.Rows
                Dim pkCaseID = item("PK_CaseManagement_ID")

                'Insert into Workflow History
                strQuery = "INSERT INTO OneFCC_CaseManagement_BatchClosed_Detail ("
                strQuery &= "FK_CaseManagement_BatchClosed_ID,"
                strQuery &= "FK_CaseManagement_ID,"
                strQuery &= "Active,"
                strQuery &= "CreatedBy,"
                strQuery &= "CreatedDate) "
                strQuery &= "VALUES ("
                strQuery &= lngBatchClosedID & ","
                strQuery &= pkCaseID & ","
                strQuery &= "1,"
                strQuery &= "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
                strQuery &= "GETDATE())"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                'Update Case Management Status and Proposed Status
                strQuery = "UPDATE OneFCC_CaseManagement SET FK_Proposed_Status_ID=6"       '6=Submitted as Closed by System
                strQuery &= ", FK_CaseStatus_ID=6"
                strQuery &= ", LastUpdateBy='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                strQuery &= ", LastUpdateDate=GETDATE()"
                strQuery &= " WHERE PK_CaseManagement_ID=" & pkCaseID
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                'Insert into Workflow History
                strQuery = "INSERT INTO OneFCC_CaseManagement_WorkflowHistory ("
                strQuery &= "FK_CaseManagement_ID,"
                strQuery &= "Workflow_Step,"
                strQuery &= "FK_Proposed_Status_ID,"
                strQuery &= "Analysis_Result,"
                strQuery &= "CreatedBy,"
                strQuery &= "CreatedDate) "
                strQuery &= "VALUES ("
                strQuery &= pkCaseID & ","
                strQuery &= "(SELECT Workflow_Step FROM OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & pkCaseID & "),"
                strQuery &= "6,"
                strQuery &= "'" & txt_Closing_Reason.Value & "',"
                strQuery &= "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
                strQuery &= "GETDATE())"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "20-Apr-2022 Adi : Alerts grouped by ProcessDate and CIF_No"
    Protected Sub store_ReadData_CaseAlert_Transaction(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT * FROM vw_OneFCC_CaseManagement_Alert"
            strQuery &= " WHERE FK_CaseManagement_ID = " & Me.CaseID_CaseAlert

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Transaction.GetStore.DataSource = DataPaging
            gp_CaseAlert_Transaction.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

End Class