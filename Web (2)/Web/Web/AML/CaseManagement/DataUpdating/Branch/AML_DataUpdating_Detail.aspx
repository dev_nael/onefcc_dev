﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AML_DataUpdating_Detail.aspx.vb" Inherits="AML_DataUpdating_Detail" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .x-form-item-label.x-form-item-label-default { width : 230px !important; }
        
        .text-wrapper .x-form-display-field {
    	    word-break: break-word;
    	    word-wrap: break-word;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel ID="FormPanelInput" runat="server" Layout="AnchorLayout" ButtonAlign="Center" Title="Data Updating - Detail" BodyStyle="padding:10px" AutoScroll="true" Hidden="false">
        <Items>
            <ext:Panel runat="server" ID="ReportGeneralInformationPanel" Layout="AnchorLayout" ClientIDMode="Static" BodyStyle="padding:10px" Margin="5" Border="True">
                <Items>
                    <ext:DisplayField ID="txt_id" runat="server" FieldLabel="ID" />
                    <ext:DisplayField ID="txt_ProcessDate" runat="server" FieldLabel="Process Date" />
                    <ext:DisplayField ID="txt_Branch" runat="server" FieldLabel="Branch" />
                    <ext:DisplayField ID="txt_CIFNo" runat="server" FieldLabel="CIF No" />
                    <ext:DisplayField ID="txt_CIFName" runat="server" FieldLabel="Name" />
                    <ext:DisplayField ID="txt_CustomerType" runat="server" FieldLabel="Customer Type" />
                    <ext:DateField ID="df_DateUpdated" runat="server" FieldLabel="Date Updated" AnchorHorizontal="40%" AllowBlank="false" Selectable="false" FieldStyle="background-color: #ddd;"/>
                    <ext:Panel ID="Pnl_Action" runat="server" Layout="AnchorLayout" AnchorHorizontal="70%">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_Action" ValueField="ACTION_CODE" DisplayField="DESCRIPTION" runat="server" StringField="ACTION_CODE, DESCRIPTION" StringTable="AML_DU_ACTION" Label="Action" AnchorHorizontal="100%" AllowBlank="false" IsReadOnly="true" />
                        </Content>
                    </ext:Panel>
                    <ext:TextArea ID="txt_Note" runat="server" FieldLabel="Note" AnchorHorizontal="70%" ReadOnly="true" FieldStyle="background-color: #ddd;"/>
                    <ext:DisplayField ID="File_Attachment" runat="server" FieldLabel="File Attachment" />
                    <ext:Button runat="server" ID="BtnDownloadFile" Icon="DiskDownload" Hidden="true" Text="Download File" MarginSpec="0 0 0 230">
                        <DirectEvents>
                            <Click OnEvent="DownloadFileUploaded" IsUpload="true" Success="NawadataDirect.Download({isUpload : true});"></Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Panel runat="server" Collapsible="true" Title="Mandatory" MarginSpec="10 0 0 0" Border="true" BodyStyle="padding:10px">
                        <Items>
                            <ext:DisplayField ID="txt_Mandatory" runat="server" LabelSeparator="" Cls="text-wrapper"/>
                        </Items>
                    </ext:Panel>
                    <ext:Panel runat="server" Collapsible="true" Title="Invalid Message" MarginSpec="10 0 0 0" Border="true" BodyStyle="padding:10px">
                        <Items>
                            <ext:DisplayField ID="txt_InvalidMessage" runat="server" LabelSeparator="" Cls="text-wrapper"/>
                        </Items>
                    </ext:Panel>
                    <ext:Panel runat="server" Collapsible="true" Title="History" MarginSpec="10 0 0 0" >
                        <Items>
                            <ext:GridPanel ID="gp_Note_History" runat="server">
                                <View>
                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                </View>
                                <Store>
                                    <ext:Store ID="store_Note_History" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" PageSize="10"
                                        OnReadData="Store_Note_History_ReadData" RemotePaging="true" ClientIDMode="Static">
                                        <Model>
                                            <ext:Model runat="server" ID="Model357" >
                                                <Fields>
                                                    <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="ACTION" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="NOTE" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy />
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn89" runat="server" Text="No"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column314" runat="server" DataIndex="CreatedBy" Text="Created By" Flex="1"></ext:Column>
                                        <ext:Column ID="Column315" runat="server" DataIndex="ACTION" Text="Action" Flex="1"></ext:Column>
                                        <ext:Column ID="Column316" runat="server" DataIndex="NOTE" Text="Note" Flex="1"></ext:Column>
                                        <ext:DateColumn ID="ColumnDateNote" runat="server" DataIndex="CreatedDate" Text="Created Date" Flex="1" ClientIDMode="Static">
                                            <Items>
                                                <ext:DateField ID="DatePickerColumnDateNote" runat="server" >
                                                    <Plugins>
                                                        <ext:ClearButton ID="Column16_ClearButton1">
                                                        </ext:ClearButton>
                                                    </Plugins>
                                                </ext:DateField>
                                            </Items>
                                            <Filter>
                                                <ext:DateFilter>
                                                </ext:DateFilter>
                                            </Filter>
                                        </ext:DateColumn>
                                    </Columns>
                                </ColumnModel>
                                <Plugins>
                                    <ext:FilterHeader runat="server" ID="FilterHeader_Valid" Remote="true" ClientIDMode="Static"></ext:FilterHeader>
                                </Plugins>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btnCancelReport" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancelReport_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>

