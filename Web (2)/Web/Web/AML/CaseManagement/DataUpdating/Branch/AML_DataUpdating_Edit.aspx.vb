﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class AML_DataUpdating_Edit
    'Inherits System.Web.UI.Page
    Inherits ParentPage

    Public Property StringDateFormat() As String
        Get
            Return Session("AML_DataUpdating_Edit.StringDateFormat")
        End Get
        Set(ByVal value As String)
            Session("AML_DataUpdating_Edit.StringDateFormat") = value
        End Set
    End Property

    Protected Sub DownloadFileUploaded()
        Try
            Dim dataStr As String = Request.Params("ID")
            Dim dataID As String = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim PKID As Long = Convert.ToInt64(dataID)
            Dim tempDRDataUpdating As DataRow = getDataRowByID("AML_DU_ALERT", "PK_AML_DU_ALERT_ID", PKID)
            If tempDRDataUpdating IsNot Nothing AndAlso Not IsDBNull(tempDRDataUpdating("FILE_ATTACHMENTName")) AndAlso Not IsDBNull(tempDRDataUpdating("FILE_ATTACHMENT")) Then
                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & tempDRDataUpdating("FILE_ATTACHMENTName"))
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(tempDRDataUpdating("FILE_ATTACHMENT"))
                Response.End()
            Else
                Throw New ApplicationException("No Files to Download")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnSaveReport_Click(sender As Object, e As DirectEventArgs)
        Try
            'If CheckChange() Then
            'Else
            '    Throw New Exception("Please Do Some Data Change Before Save")
            'End If
            Dim dataStr As String = Request.Params("ID")
            Dim dataID As String = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim PKID As Long = Convert.ToInt64(dataID)
            Dim DateUpdated As Date
            If Not df_DateUpdated.SelectedDate = DateTime.MinValue Then
                DateUpdated = df_DateUpdated.SelectedDate
            Else
                DateUpdated = Nothing
            End If
            If DateUpdated = Nothing Then
                Throw New ApplicationException(df_DateUpdated.FieldLabel & " Is Mandatory")
            End If

            Dim ActionCode As String = cmb_Action.SelectedItemValue
            If String.IsNullOrEmpty(ActionCode) Then
                Throw New ApplicationException(cmb_Action.Label & " Is Mandatory")
            End If
            Dim Note As String = txt_Note.Value
            Dim FileAttachment As Byte()
            Dim FileAttachmentName As String

            If File_Attachment.HasFile Then
                FileAttachment = File_Attachment.FileBytes
                FileAttachmentName = File_Attachment.FileName
            Else
                If String.IsNullOrEmpty(File_Attachment.Value) Then
                    Dim tempDRDataUpdating As DataRow = getDataRowByID("AML_DU_ALERT", "PK_AML_DU_ALERT_ID", PKID)
                    If tempDRDataUpdating IsNot Nothing Then
                        If Not IsDBNull(tempDRDataUpdating("FILE_ATTACHMENT")) Then
                            FileAttachment = tempDRDataUpdating("FILE_ATTACHMENT")
                        End If
                        If Not IsDBNull(tempDRDataUpdating("FILE_ATTACHMENTName")) Then
                            FileAttachmentName = tempDRDataUpdating("FILE_ATTACHMENTName")
                        End If
                    End If
                End If
            End If

            SaveChangeData(PKID, DateUpdated, ActionCode, Note, FileAttachment, FileAttachmentName)
            LblConfirmation.Text = "Data Saved into DataBase"
            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Private Function CheckChange() As Boolean
    '    Try
    '        Dim checker As Boolean = False
    '        Return checker
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Protected Sub GetDateFormat()
        Try
            Dim tempDRSystermParameter As DataRow = getDataRowByID("SystemParameter", "SettingName", "FormatDate")
            If tempDRSystermParameter IsNot Nothing Then
                If Not IsDBNull(tempDRSystermParameter("SettingValue")) Then
                    StringDateFormat = tempDRSystermParameter("SettingValue")
                Else
                    StringDateFormat = "dd-MMM-yyyy"
                End If
            Else
                StringDateFormat = "dd-MMM-yyyy"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SetDateFormat()
        Try
            df_DateUpdated.Format = StringDateFormat
            ColumnDateNote.Format = StringDateFormat
            DatePickerColumnDateNote.Format = StringDateFormat
            DatePickerColumnDateNote.FormatText = "Expected Date Format " & StringDateFormat
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadData()
        Try
            Dim dataStr As String = Request.Params("ID")
            Dim dataID As String = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim ID As Integer = Convert.ToInt64(dataID)
            Dim tempDRDataUpdating As DataRow = getDataRowByID("VW_AML_DU_Branch", "PK_AML_DU_ALERT_ID", dataID)
            If tempDRDataUpdating IsNot Nothing Then
                If Not IsDBNull(tempDRDataUpdating("PK_AML_DU_ALERT_ID")) Then
                    txt_id.Text = tempDRDataUpdating("PK_AML_DU_ALERT_ID")
                End If
                If Not IsDBNull(tempDRDataUpdating("PROCESS_DATE")) Then
                    'txt_ProcessDate.Text = tempDRDataUpdating("PROCESS_DATE").ToString("dd-MM-yyyy")
                    'txt_ProcessDate.Text = String.Format("dd-MM-yyyy", tempDRDataUpdating("PROCESS_DATE"))
                    Dim tempDate As Date = tempDRDataUpdating("PROCESS_DATE")
                    txt_ProcessDate.Text = tempDate.ToString(StringDateFormat)
                End If
                If Not IsDBNull(tempDRDataUpdating("FK_AML_BRANCH_CODENAME")) Then
                    txt_Branch.Text = tempDRDataUpdating("FK_AML_BRANCH_CODENAME")
                End If
                If Not IsDBNull(tempDRDataUpdating("CIF_NO")) Then
                    txt_CIFNo.Text = tempDRDataUpdating("CIF_NO")
                End If
                If Not IsDBNull(tempDRDataUpdating("CIF_NAME")) Then
                    txt_CIFName.Text = tempDRDataUpdating("CIF_NAME")
                End If
                If Not IsDBNull(tempDRDataUpdating("CUSTOMER_TYPE")) Then
                    txt_CustomerType.Text = tempDRDataUpdating("CUSTOMER_TYPE")
                End If
                If Not IsDBNull(tempDRDataUpdating("LastModified")) Then
                    df_DateUpdated.Value = tempDRDataUpdating("LastModified")
                End If
                If Not IsDBNull(tempDRDataUpdating("ACTION_CODE")) Then
                    Dim tempDRAction As DataRow = getDataRowByID("AML_DU_ACTION", "ACTION_CODE", tempDRDataUpdating("ACTION_CODE"))
                    If tempDRAction IsNot Nothing Then
                        cmb_Action.SetTextWithTextValue(tempDRAction("ACTION_CODE"), tempDRAction("DESCRIPTION"))
                    End If
                End If
                If Not IsDBNull(tempDRDataUpdating("Note")) Then
                    txt_Note.Value = tempDRDataUpdating("Note")
                End If
                'If Not IsDBNull(tempDRDataUpdating("FILE_ATTACHMENT")) Then
                '    File_Attachment.Value = tempDRDataUpdating("FILE_ATTACHMENT")
                'End If
                If Not IsDBNull(tempDRDataUpdating("FILE_ATTACHMENTName")) Then
                    File_Attachment.Value = tempDRDataUpdating("FILE_ATTACHMENTName")
                    BtnDownloadFile.Hidden = False
                    'File_Attachment.Text = tempDRDataUpdating("FILE_ATTACHMENTName")
                    'File_Attachment.Name = tempDRDataUpdating("FILE_ATTACHMENTName")
                End If
                If Not IsDBNull(tempDRDataUpdating("MANDATORY_MESSAGE")) Then
                    txt_Mandatory.Value = tempDRDataUpdating("MANDATORY_MESSAGE")
                End If
                If Not IsDBNull(tempDRDataUpdating("INVALID_MESSAGE")) Then
                    txt_InvalidMessage.Text = tempDRDataUpdating("INVALID_MESSAGE")
                End If
            End If
            'Dim strSQL As String = "select * from VW_AML_DU_ALERT_NOTE_HISTORY where FK_AML_DU_ALERT_ID = " & dataID
            'Dim tempDTNoteHistory As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            'store_Note_History.DataSource = tempDTNoteHistory
            'store_Note_History.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Store_Note_History_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim dataStr As String = Request.Params("ID")
            Dim dataID As String = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim intStart As Integer = e.Start
            Dim intLimit As Integer = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                If strsort = "" Then
                    strsort += item.Property & " " & item.Direction.ToString
                Else
                    strsort += " , " & item.Property & " " & item.Direction.ToString
                End If
            Next

            If strsort = "" Then
                strsort = "CreatedDate Desc"
            End If

            Dim strFields As String = "*"

            If String.IsNullOrEmpty(strfilter) Then
                strfilter = "FK_AML_DU_ALERT_ID = " & dataID
            Else
                strfilter += " AND FK_AML_DU_ALERT_ID = " & dataID
            End If

            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_WATCHLIST_Upload a", strFields, strfilter, strsort, intStart, NawaBLL.SystemParameterBLL.GetPageSize, inttotalRecord)
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("VW_AML_DU_ALERT_NOTE_HISTORY ", strFields, strfilter, strsort, intStart, intLimit, inttotalRecord)

            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SIPENDAR_WATCHLIST", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start <0 OrElse limit <0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord

            store_Note_History.DataSource = DataPaging
            store_Note_History.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelReport_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            'Moduleid = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub AML_DataUpdating_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                GetDateFormat()
                SetDateFormat()
                LoadData()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AML_DataUpdating_Edit_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Update
    End Sub

    Shared Function getDataRowByID(strTable As String, strTableKeyField As String, strKey As String) As DataRow
        Try
            Dim strSQL As String = "SELECT TOP 1 * FROM " & strTable & " WHERE " & strTableKeyField & "='" & strKey & "'"
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

            Return drResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Protected Sub SaveChangeData(PKID As Long, DateUpdated As Date, ActionCode As String, Note As String, FileAttachment As Byte(), FileAttachmentName As String)
        Try
            Dim param(6) As SqlParameter

            param(0) = New SqlParameter With {
                .ParameterName = "@PKID",
                .Value = PKID,
                .SqlDbType = SqlDbType.BigInt
            }

            'param(1) = New SqlParameter
            'param(1).ParameterName = "@DateUpdated"
            'If DateUpdated = Nothing Then
            '    param(1).Value = DBNull
            'Else
            '    param(1).Value = DateUpdated
            'End If
            'param(1).SqlDbType = SqlDbType.Date
            param(1) = New SqlParameter With {
                .ParameterName = "@DateUpdated",
                .Value = DateUpdated,
                .SqlDbType = SqlDbType.Date
            }

            param(2) = New SqlParameter With {
                .ParameterName = "@ActionCode",
                .Value = ActionCode,
                .SqlDbType = SqlDbType.VarChar
            }

            param(3) = New SqlParameter With {
                .ParameterName = "@Note",
                .Value = Note,
                .SqlDbType = SqlDbType.VarChar
            }

            param(4) = New SqlParameter With {
                .ParameterName = "@FileAttachment",
                .Value = FileAttachment,
                .SqlDbType = SqlDbType.VarBinary
            }

            param(5) = New SqlParameter With {
                .ParameterName = "@FileAttachmentName",
                .Value = FileAttachmentName,
                .SqlDbType = SqlDbType.VarChar
            }

            param(6) = New SqlParameter With {
                .ParameterName = "@UserID",
                .Value = NawaBLL.Common.SessionCurrentUser.UserID,
                .SqlDbType = SqlDbType.VarChar
            }

            Dim dtPK As DataTable = Nothing
            dtPK = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_DataUpdating_Alert_SaveUpdateData", param)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
