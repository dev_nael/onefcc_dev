﻿Imports NawaDevDAL
Imports NawaDevBLL
Imports CasemanagementDAL
Imports CasemanagementBLL
Imports System.Data
Imports System.Data.SqlClient
Imports NawaBLL
Imports OfficeOpenXml
Imports System.IO
Imports System.Globalization

Partial Class AML_CaseManagement_STRMemo_STRMemo_Add
    Inherits ParentPage

    Public objFormModuleAdd As NawaBLL.FormModuleAdd
    Public objFormModuleView As FormModuleView
    Public Property ObjSARData As NawaDevBLL.GenerateSARData
        Get
            Return Session("STRMemo_Add.ObjSARData")
        End Get
        Set(ByVal value As NawaDevBLL.GenerateSARData)
            Session("STRMemo_Add.ObjSARData") = value
        End Set
    End Property
    Public Property objSTRMemoClass As STRMemo_BLL.STRMemoClass
        Get
            If Session("STRMemo_Add.objSTRMemoClass") Is Nothing Then
                Session("STRMemo_Add.objSTRMemoClass") = New STRMemo_BLL.STRMemoClass
            End If

            Return Session("STRMemo_Add.objNewsClass")
        End Get
        Set(value As STRMemo_BLL.STRMemoClass)
            Session("STRMemo_Add.objNewsClass") = value
        End Set
    End Property

    ' Set Posisi Action dari Grid
    Sub SetCommandColumnLocation()
        'dim settingvaluestring as String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select SettingValue FROM systemParameter where PK_SystemParameter_ID = '32'", Nothing)
        Dim ObjParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim ButtonPosition As Integer = 1
        If Not ObjParamSettingbutton Is Nothing Then
            ButtonPosition = ObjParamSettingbutton.SettingValue
        End If


        ColumnActionLocation(gp_indikator, cc_indikator, ButtonPosition)
        ColumnActionLocation(gp_Pihak_Terkait, cc_pihakterkait, ButtonPosition)
        ColumnActionLocation(gp_TIndakLanjut, cc_tindaklanjut, ButtonPosition)

    End Sub

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, ButtonPosition As Integer)
        If ButtonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub

    Private Sub STRMemo_Add_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ' Clear Session
                ClearSession()
                ' Session sebagai parameter bahwa user pertama kali add data
                Session("FirstAddData") = 1
                ' Clear TextField dan Grid 
                ClearTextAndGrid()
                ClearTransaction()
                ' Set action grid
                SetCommandColumnLocation()
                ' Set untuk NDS Dropdown pilih cif dia filter berdasarkan PIC
                Dim UserCIF As String = NawaBLL.Common.SessionCurrentUser.UserID
                cmb_Cifno.StringFilter = " dbo.ufn_CheckIfStringExists('" & UserCIF & "',PIC)=1"
                ' Load Data 
                LoadData()

            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ClearSession()
        Session("FirstAddData") = Nothing
        objSTRMemoClass = New STRMemo_BLL.STRMemoClass
        obj_ListAttachment_Edit = Nothing
        obj_ListIndikator_Edit = Nothing
        obj_ListPihakTerkait_Edit = Nothing
        obj_ListTindakLanjut_Edit = Nothing
        Session("STRMemo_CIFNO") = Nothing ' Session untuk menampung CIFNo yang dipilih
    End Sub

    Private Sub ClearTextAndGrid()
        txtNo.Value = Nothing
        txtKepada.Value = Nothing
        txtDari.Value = Nothing
        txtTanggal.Value = Nothing
        txtPerihal.Value = Nothing
        txtSumberPelaporan.Value = Nothing
        txtNamaPihakPelapor.Value = Nothing
        txtKategoriPihakPelapor.Value = Nothing
        txtUnitBisnis.Value = Nothing
        txtDirektor.Value = Nothing
        txtCabang.Value = Nothing
        txtLamaMenjadiNasabah.Value = Nothing
        txtPekerjaanBidangUsaha.Value = Nothing
        txtPengahasilanTahun.Value = Nothing
        txtProfilLainnya.Value = Nothing
        txtHasilAnalisis.Value = Nothing
        txtKesimpulan.Value = Nothing
        NoRefPPATK.Value = Nothing
        alasan.Value = Nothing
        dd_TrnFilterBy.SetTextValue("")
        txtCabang.Value = Nothing
        cmb_KesimpulanDetail.SetTextValue("")
        sar_DateFrom.Value = Nothing
        sar_DateTo.Value = Nothing
        sar_jenisLaporan.Value = Nothing
        TanggalLaporan.Value = Nothing
        objSTRMemoClass = New STRMemo_BLL.STRMemoClass
        obj_Indikator_Edit = New OneFCC_CaseManagement_STRMemo_Indikator
        obj_PihakTerkait_Edit = New OneFCC_CaseManagement_STRMemo_Pihak_Terkait
        obj_TindakLanjut_Edit = New OneFCC_CaseManagement_STRMemo_TindakLanjut
        obj_ListAttachment_Edit = New List(Of OneFCC_CaseManagement_STRMemo_Attachment)
        obj_ListIndikator_Edit = New List(Of OneFCC_CaseManagement_STRMemo_Indikator)
        obj_ListPihakTerkait_Edit = New List(Of OneFCC_CaseManagement_STRMemo_Pihak_Terkait)
        obj_ListTindakLanjut_Edit = New List(Of OneFCC_CaseManagement_STRMemo_TindakLanjut)
        ListTransaction = New List(Of goAML_ODM_Transaksi)

        Dim Dt_NewDataTable As New DataTable
        bindTransactionDataTable(StoreTransaction, Dt_NewDataTable)
        bindAttachment(StoreAttachment, obj_ListAttachment_Edit)

        gp_indikator.GetStore.DataSource = Dt_NewDataTable
        gp_indikator.GetStore.DataBind()
        gp_Pihak_Terkait.GetStore.DataSource = Dt_NewDataTable
        gp_Pihak_Terkait.GetStore.DataBind()
        gp_TIndakLanjut.GetStore.DataSource = Dt_NewDataTable
        gp_TIndakLanjut.GetStore.DataBind()

    End Sub

    ' Clear hasil dari pencarian transaction STR
    Private Sub ClearTransaction()
        NoRefPPATK.Value = Nothing
        alasan.Value = Nothing
        'dd_TrnFilterBy.SetTextValue("")
        'sar_DateFrom.Value = Nothing
        'sar_DateTo.Value = Nothing
        sar_jenisLaporan.Value = Nothing
        TanggalLaporan.Value = Nothing
        obj_ListAttachment_Edit = New List(Of OneFCC_CaseManagement_STRMemo_Attachment)
        ListTransaction = New List(Of goAML_ODM_Transaksi)
        ListSelectedTransaction = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
      
        bindAttachment(StoreAttachment, obj_ListAttachment_Edit)

    End Sub

    ' Prosedur untuk search case alert berdasarkan CIF yang dipilih
    Protected Sub btnSearch_Click()
        Try
            ' Unhide panel STRMemo nya
            FpSTRMemo.Hidden = True
            ' Clear Text and Grid nya
            ClearTextAndGrid()
            ' Jika user tidak memilih CIF
            If Session("STRMemo_CIFNO") Is Nothing Then
                Throw New ApplicationException("Pilih Salah Satu CIF")
            End If
            ' Mencari status dari CIF yang dipilih
            Dim StrStatusCIF As String = "Select status from VW_STRMemo_CIFNo where cifno = '" & Session("STRMemo_CIFNO").ToString & "'"
            Dim Status As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrStatusCIF, Nothing)
            ' Jika statusnya 0 atau Uncofigured
            If Status = "0" Then
                Throw New ApplicationException("CIF yang dipilih belum ada konfigurasi workflownya")
            End If
            ' Tampung value CIF nya
            Dim CIFSTRMemo As String = Session("STRMemo_CIFNO")

            '' Edit 5-Oct-2022 Felix 
            'Using objdb As New CasemanagementEntities
            '    Dim StatusMemo As String = ""
            '    ' Cari dulu case id berdasarkan CIF yang dipilih dan dimana statusnya 3 atau issue/closed
            '    Dim ListCaseID As List(Of OneFCC_CaseManagement) = objdb.OneFCC_CaseManagement.Where(Function(x) x.CIF_No = CIFSTRMemo And x.FK_CaseStatus_ID = 3).ToList
            '    ' Cari juga case id yang pernah di simpan di STR Memo
            '    Dim ListCaseIDSTR As List(Of OneFCC_CaseManagement_STRMemo_CaseID) = objdb.OneFCC_CaseManagement_STRMemo_CaseID.ToList()
            '    ' Looping case ID
            '    For Each item In ListCaseID
            '        For Each item2 In ListCaseIDSTR
            '            ' Jika case ID yang ditemukan di casemanagement ternyata sudah ada di case STR Memo
            '            If item.PK_CaseManagement_ID = item2.CaseID Then
            '                ' maka fk str memo dari case id itu isi dengan fk str memo
            '                item.FK_OneFCC_CaseManagement_STRMemo_ID = item2.FK_OneFCC_CaseManagement_STRMemo_ID
            '            End If
            '        Next
            '    Next
            '    Dim ObjTable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(ListCaseID)
            '    ' Tambah Column Rule
            '    ObjTable.Columns.Add(New DataColumn("Rule", GetType(String)))
            '    For Each item As Data.DataRow In ObjTable.Rows
            '        ' Jika rule basic nya ada, maka cari description rule nya
            '        If Not IsDBNull(item("FK_Rule_Basic_ID")) Then
            '            'Dim RuleBasic As New OneFCC_MS_Rule_Basic
            '            'Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
            '            'RuleBasic = objdb.OneFCC_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault
            '            'item("Rule") = RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
            '            '' Edit 5-Oct-2022
            '            Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
            '            'RuleBasic = objdb.OneFcc_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault '' Edit 5 Oct 2022
            '            Dim RuleBasic As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select PK_Rule_Basic_ID + ' - ' + Rule_Basic_Name from OneFcc_MS_Rule_Basic where PK_Rule_Basic_ID = " & FKRule & "", Nothing)
            '            item("Rule") = RuleBasic ' RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
            '            '' End 5-Oct-2022
            '        End If
            '        ' Jika case ID nya tidak ada di Case ID STR Memo maka case id nya baru
            '        If IsDBNull(item("FK_OneFCC_CaseManagement_STRMemo_ID")) Then
            '            item("Memo_No") = "0 - New"
            '        Else
            '            Dim StrMemo As New OneFCC_CaseManagement_STRMemo
            '            Dim FkSTRMemo As String = item("FK_OneFCC_CaseManagement_STRMemo_ID").ToString
            '            StrMemo = objdb.OneFCC_CaseManagement_STRMemo.Where(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_ID = FkSTRMemo).FirstOrDefault
            '            If StrMemo.StatusID = "1" Then
            '                item("Memo_No") = "1 - On Progress"
            '            ElseIf StrMemo.StatusID = "2" Then
            '                item("Memo_No") = "2 - Approved"
            '            ElseIf StrMemo.StatusID = "3" Then
            '                item("Memo_No") = "3 - Rejected"
            '            ElseIf StrMemo.StatusID = "4" Then
            '                item("Memo_No") = "4 - Generate"
            '            ElseIf StrMemo.StatusID = "5" Then
            '                item("Memo_No") = "5 - On Progress Generate Report"
            '            End If
            '        End If
            '    Next
            '    gp_case_alert.GetStore().DataSource = ObjTable
            '    gp_case_alert.GetStore().DataBind()
            'End Using

            Dim StatusMemo As String = ""
            ' Cari dulu case id berdasarkan CIF yang dipilih dan dimana statusnya 3 atau issue/closed
            Dim listCaseID As List(Of OneFCC_CaseManagement) = New List(Of OneFCC_CaseManagement)
            'listCaseID = objdb.OneFCC_CaseManagement.Where(Function(x) x.CIF_No = CIFSTRMemo And x.FK_CaseStatus_ID = 3).ToList
            Dim strQueryCaseID As String = ""
            strQueryCaseID = "select * "
            strQueryCaseID += "From OneFCC_CaseManagement csm "
            strQueryCaseID += "left join OneFCC_CaseManagement_Typology tp "
            strQueryCaseID += "on csm.PK_CaseManagement_ID = tp.FK_CaseManagement_ID "
            strQueryCaseID += "left join OneFCC_CaseManagement_Outlier ot "
            strQueryCaseID += "on csm.PK_CaseManagement_ID = ot.FK_CaseManagement_ID "
            strQueryCaseID += "where csm.CIF_No = '" & CIFSTRMemo & "'"
            strQueryCaseID += "and csm.FK_CaseStatus_ID = 3 "
            strQueryCaseID += "and csm.FK_OneFCC_CaseManagement_STRMemo_ID is null "
            strQueryCaseID += "and ((tp.PK_OneFCC_CaseManagement_Typology_ID is not null AND tp.Is_Transaction = 1) or ot.PK_OneFCC_CaseManagement_Outlier_ID is not null)"
            Dim dtCaseID As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryCaseID, Nothing)

            If dtCaseID IsNot Nothing Then
                For Each row As DataRow In dtCaseID.Rows
                    Dim CaseID As OneFCC_CaseManagement = New OneFCC_CaseManagement()

                    If Not IsDBNull(row("PK_CaseManagement_ID")) Then
                        CaseID.PK_CaseManagement_ID = row("PK_CaseManagement_ID")
                    End If

                    If Not IsDBNull(row("Alert_Type")) Then
                        CaseID.Alert_Type = row("Alert_Type")
                    End If

                    If Not IsDBNull(row("FK_Rule_Basic_ID")) Then
                        CaseID.FK_Rule_Basic_ID = row("FK_Rule_Basic_ID")
                    End If

                    If Not IsDBNull(row("Case_Description")) Then
                        CaseID.Case_Description = row("Case_Description")
                    End If

                    If Not IsDBNull(row("Is_Customer")) Then
                        CaseID.Is_Customer = row("Is_Customer")
                    End If

                    If Not IsDBNull(row("CIF_No")) Then
                        CaseID.CIF_No = row("CIF_No")
                    End If

                    If Not IsDBNull(row("Account_No")) Then
                        CaseID.Account_No = row("Account_No")
                    End If

                    If Not IsDBNull(row("Customer_Name")) Then
                        CaseID.Customer_Name = row("Customer_Name")
                    End If

                    If Not IsDBNull(row("WIC_No")) Then
                        CaseID.WIC_No = row("WIC_No")
                    End If

                    If Not IsDBNull(row("WIC_Name")) Then
                        CaseID.WIC_Name = row("WIC_Name")
                    End If

                    If Not IsDBNull(row("FK_Proposed_Status_ID")) Then
                        CaseID.FK_Proposed_Status_ID = row("FK_Proposed_Status_ID")
                    End If

                    If Not IsDBNull(row("Proposed_By")) Then
                        CaseID.Proposed_By = row("Proposed_By")
                    End If

                    If Not IsDBNull(row("FK_CaseStatus_ID")) Then
                        CaseID.FK_CaseStatus_ID = row("FK_CaseStatus_ID")
                    End If

                    If Not IsDBNull(row("FK_CaseManagement_Workflow_ID")) Then
                        CaseID.FK_CaseManagement_Workflow_ID = row("FK_CaseManagement_Workflow_ID")
                    End If

                    If Not IsDBNull(row("Workflow_Step")) Then
                        CaseID.Workflow_Step = row("Workflow_Step")
                    End If

                    If Not IsDBNull(row("PIC")) Then
                        CaseID.PIC = row("PIC")
                    End If

                    If Not IsDBNull(row("Aging")) Then
                        CaseID.Aging = row("Aging")
                    End If

                    If Not IsDBNull(row("StatusRFI")) Then
                        CaseID.StatusRFI = row("StatusRFI")
                    End If

                    If Not IsDBNull(row("StatusSTRMemo")) Then
                        CaseID.StatusSTRMemo = row("StatusSTRMemo")
                    End If

                    If Not IsDBNull(row("ProcessDate")) Then
                        CaseID.ProcessDate = row("ProcessDate")
                    End If

                    If Not IsDBNull(row("FK_Report_ID")) Then
                        CaseID.FK_Report_ID = row("FK_Report_ID")
                    End If

                    If Not IsDBNull(row("Active")) Then
                        CaseID.Active = row("Active")
                    End If

                    If Not IsDBNull(row("CreatedBy")) Then
                        CaseID.CreatedBy = row("CreatedBy")
                    End If

                    If Not IsDBNull(row("LastUpdateBy")) Then
                        CaseID.LastUpdateBy = row("LastUpdateBy")
                    End If

                    If Not IsDBNull(row("ApprovedBy")) Then
                        CaseID.ApprovedBy = row("ApprovedBy")
                    End If

                    If Not IsDBNull(row("CreatedDate")) Then
                        CaseID.CreatedDate = row("CreatedDate")
                    End If

                    If Not IsDBNull(row("LastUpdateDate")) Then
                        CaseID.LastUpdateDate = row("LastUpdateDate")
                    End If

                    If Not IsDBNull(row("ApprovedDate")) Then
                        CaseID.ApprovedDate = row("ApprovedDate")
                    End If

                    If Not IsDBNull(row("Alternateby")) Then
                        CaseID.Alternateby = row("Alternateby")
                    End If

                    'If Not IsDBNull(drCaseID("Rule")) Then
                    '    CaseID.Account_No = drCaseID("Rule")
                    'End If
                    If Not IsDBNull(row("FK_OneFCC_CaseManagement_STRMemo_ID")) Then
                        CaseID.FK_OneFCC_CaseManagement_STRMemo_ID = row("FK_OneFCC_CaseManagement_STRMemo_ID")
                    End If
                    If Not IsDBNull(row("Memo_No")) Then
                        CaseID.Memo_No = row("Memo_No")
                    End If

                    listCaseID.Add(CaseID)
                Next
            End If


            ' Cari juga case id yang pernah di simpan di STR Memo
            Dim listCaseIDSTR As New DataTable
            listCaseIDSTR = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_STRMemo_CaseID")
            ' Looping case ID

            For Each item In listCaseID
                For Each item2 As DataRow In listCaseIDSTR.Rows
                    ' Jika case ID yang ditemukan di casemanagement ternyata sudah ada di case STR Memo
                    If item.PK_CaseManagement_ID = item2("CaseID") Then
                        ' maka fk str memo dari case id itu isi dengan fk str memo
                        Dim FKStrMemo As String = ""
                        FKStrMemo = item2("FK_OneFCC_CaseManagement_STRMemo_ID").ToString
                        item.FK_OneFCC_CaseManagement_STRMemo_ID = FKStrMemo
                    End If
                Next
            Next
            Dim ObjTable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listCaseID)
            ' Tambah Column Rule
            ObjTable.Columns.Add(New DataColumn("Rule", GetType(String)))
            ObjTable.Columns.Add(New DataColumn("UniqueCMID", GetType(String))) '' Add 7-Mar-23, Ari. FAMA tambah 1 field baru untuk unique cm id
            For Each item As Data.DataRow In ObjTable.Rows
                If Not IsDBNull(item("FK_Rule_Basic_ID")) Then
                    '' Edit 5-Oct-2022 Felix
                    'Dim RuleBasic As New OneFCC_MS_Rule_Basic
                    'Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                    'RuleBasic = objdb.OneFCC_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault
                    'item("Rule") = RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                    'Dim RuleBasic As New OneFcc_MS_Rule_Basic
                    Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                    'RuleBasic = objdb.OneFcc_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault '' Edit 5 Oct 2022
                    Dim RuleBasic As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select cast (PK_Rule_Basic_ID as varchar(100)) + ' - ' + Rule_Basic_Name from OneFcc_MS_Rule_Basic where PK_Rule_Basic_ID = " & FKRule & "", Nothing)
                    item("Rule") = RuleBasic ' RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                    '' End 5-Oct-2022
                End If
                '' Add 7-Mar-23, Ari. FAMA tambah 1 field baru untuk unique cm id
                If item("PK_CaseManagement_ID") > 0 Then
                    Dim PKCM As Long = Convert.ToInt64(item("PK_CaseManagement_ID"))
                    Dim UniqueCMDID As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select Unique_CM_ID from OneFCC_CaseManagement where PK_CaseManagement_ID = " & PKCM, Nothing).ToString
                    item("UniqueCMID") = UniqueCMDID
                End If
                ' Jika case ID nya tidak ada di Case ID STR Memo maka case id nya baru
                If IsDBNull(item("FK_OneFCC_CaseManagement_STRMemo_ID")) Then
                    item("Memo_No") = "0 - New"
                Else
                    Dim StrMemo As New OneFCC_CaseManagement_STRMemo
                    Dim FkSTRMemo As String = item("FK_OneFCC_CaseManagement_STRMemo_ID").ToString
                    'StrMemo = objdb.OneFCC_CaseManagement_STRMemo.Where(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_ID = FkSTRMemo).FirstOrDefault
                    Dim drStrMemo As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select StatusID from OneFCC_CaseManagement_STRMemo where PK_OneFCC_CaseManagement_STRMemo_ID = " & FkSTRMemo, Nothing)
                    If drStrMemo IsNot Nothing Then
                        If drStrMemo("StatusID") = "1" Then
                            item("Memo_No") = "1 - On Progress"
                        ElseIf drStrMemo("StatusID") = "2" Then
                            item("Memo_No") = "2 - Approved"
                        ElseIf drStrMemo("StatusID") = "3" Then
                            item("Memo_No") = "3 - Rejected"
                        ElseIf drStrMemo("StatusID") = "4" Then
                            item("Memo_No") = "4 - Generate"
                        ElseIf drStrMemo("StatusID") = "5" Then
                            item("Memo_No") = "5 - On Progress Generate Report"
                        End If
                    End If
                End If
            Next
            gp_case_alert.GetStore().DataSource = ObjTable
            gp_case_alert.GetStore().DataBind()


            '' End 5-Oct-2022
            FpSTRMemo.Hidden = False

            'Set Value Otomatis
            Dim objCust As New goAML_Ref_Customer
            Dim KategoriCust As String = ""
            Dim CustName As String = ""
            Dim Pekerjaan As String = ""
            Dim CIF As String = Session("STRMemo_CIFNO").ToString
            Using objdb As New NawaDatadevEntities
                objCust = objdb.goAML_Ref_Customer.Where(Function(x) x.CIF = CIF).FirstOrDefault
                If objCust IsNot Nothing Then
                    If objCust.FK_Customer_Type_ID = 2 Then
                        CustName = objCust.Corp_Name
                        Pekerjaan = objCust.Corp_Business
                    Else
                        CustName = objCust.INDV_Last_Name
                        Pekerjaan = objCust.INDV_Occupation
                    End If
                    KategoriCust = "Nasabah"
                Else
                    KategoriCust = "WIC"
                End If

            End Using

            txtTanggal.Value = DateTime.Now
            Dim Perihal As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select parametervalue from OneFCC_CaseManagement_Parameter where pk_globalreportparameter_id = 23", Nothing)
            txtPerihal.Value = Perihal.Replace("$Customer$", CustName).Replace("$KategoriCust$", KategoriCust).Replace("$CIF$", CIF)
            txtKepada.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select parametervalue from OneFCC_CaseManagement_Parameter where pk_globalreportparameter_id = 20", Nothing)
            txtDari.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select parametervalue from OneFCC_CaseManagement_Parameter where pk_globalreportparameter_id = 21", Nothing)
            txtNamaPihakPelapor.Value = CustName
            txtKategoriPihakPelapor.Value = KategoriCust
            txtCabang.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select branch.branch_name from goaml_ref_customer cust inner join aml_branch branch on cust.opening_branch_code = branch.fk_aml_branch_code where cif ='" + objCust.CIF + "'", Nothing)
            txtLamaMenjadiNasabah.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " SELECT isnull('Sejak '+convert(varchar, Opening_Date, 106)+ ' ('+ CAST(DATEDIFF(MONTH, Opening_Date, GETDATE()) AS VARCHAR(10))+' Bulan)', 'Sejak') FROM dbo.goaml_ref_customer WHERE CIF ='" + objCust.CIF + "'", Nothing)
            txtProfilLainnya.Value = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select parametervalue from OneFCC_CaseManagement_Parameter where pk_globalreportparameter_id = 22", Nothing)
            txtPekerjaanBidangUsaha.Value = Pekerjaan
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try



    End Sub

    Private Sub LoadData()
        Try
            ' Set Label
            LblLatarBelakang.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblPihakCalonTerkait.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblPihakTerkait.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblHasilAnalisis.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblKesimpulan.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblTindakLanjut.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblTransaksiSTR.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblWorkflowHistory.StyleSpec = "font-weight: bold;font-size : 20px;"
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub isDataValid()
        Try
            If String.IsNullOrEmpty(txtNo.Value) Then
                Throw New ApplicationException(txtNo.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtKepada.Value) Then
                Throw New ApplicationException(txtKepada.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtDari.Value) Then
                Throw New ApplicationException(txtDari.FieldLabel + " is required.")
            End If
            If txtTanggal.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException(txtTanggal.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtPerihal.Value) Then
                Throw New ApplicationException(txtPerihal.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtSumberPelaporan.Value) Then
                Throw New ApplicationException(txtSumberPelaporan.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtNamaPihakPelapor.Value) Then
                Throw New ApplicationException(txtNamaPihakPelapor.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtKategoriPihakPelapor.Value) Then
                Throw New ApplicationException(txtKategoriPihakPelapor.FieldLabel + " is required.")
            End If
            ' 3 Dec 2022 Ari : Field jadi tidak mandatory
            'If String.IsNullOrEmpty(txtUnitBisnis.Value) Then
            '    Throw New ApplicationException(txtUnitBisnis.FieldLabel + " is required.")
            'End If
            'If String.IsNullOrEmpty(txtDirektor.Value) Then
            '    Throw New ApplicationException(txtUnitBisnis.FieldLabel + " is required.")
            'End If
            If String.IsNullOrEmpty(txtCabang.Value) Then
                Throw New ApplicationException(txtCabang.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtLamaMenjadiNasabah.Value) Then
                Throw New ApplicationException(txtLamaMenjadiNasabah.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtPekerjaanBidangUsaha.Value) Then
                Throw New ApplicationException(txtPekerjaanBidangUsaha.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtPengahasilanTahun.Value) Then
                Throw New ApplicationException(txtPengahasilanTahun.FieldLabel + " is required Or It Has Character.")
                'Else
                '    Dim JumlahKarakterPenghasilan As Integer = 0
                '    Dim PenghasilanThn As String = txtPengahasilanTahun.Value
                '    For i = 0 To PenghasilanThn.Length - 1
                '        If Char.IsLetter(PenghasilanThn.Chars(i)) Then
                '            JumlahKarakterPenghasilan = JumlahKarakterPenghasilan + 1
                '        End If
                '    Next
                '    If JumlahKarakterPenghasilan > 0 Then
                '        Throw New ApplicationException("Inputan Dari Penghasilan Tahunan Terdapat Karakter!")
                '    End If
            End If
            If String.IsNullOrEmpty(txtProfilLainnya.Value) Then
                Throw New ApplicationException(txtProfilLainnya.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtHasilAnalisis.Value) Then
                Throw New ApplicationException("Hasil Analisis is required.")
            End If
            If String.IsNullOrEmpty(txtKesimpulan.Value) Then
                Throw New ApplicationException("Kesimpulan is required.")
            End If
            If obj_ListIndikator_Edit.Count = 0 Then
                Throw New ApplicationException("Indikator is required.")
            End If
            'If obj_ListPihakTerkait_Edit.Count = 0 Then
            '    Throw New ApplicationException("Pihak Terkait is required.")
            'End If
            If obj_ListTindakLanjut_Edit.Count = 0 Then
                Throw New ApplicationException("Tindak Lanjut is required.")
            End If
            If dd_TrnFilterBy.SelectedItemValue = "3" Then
                If sar_DateFrom.SelectedDate = DateTime.MinValue Then
                    Throw New ApplicationException(sar_DateFrom.FieldLabel + " is required.")
                End If
                If sar_DateTo.SelectedDate = DateTime.MinValue Then
                    Throw New ApplicationException(sar_DateTo.FieldLabel + " is required.")
                End If

            End If
            If TanggalLaporan.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException(TanggalLaporan.FieldLabel + " is required.")
            End If

            If String.IsNullOrEmpty(sar_jenisLaporan.SelectedItem.Value) Then
                Throw New ApplicationException(sar_jenisLaporan.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(alasan.Value) Then
                Throw New ApplicationException(alasan.FieldLabel + " is required.")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            isDataValid()

            Dim SmCaseID As RowSelectionModel = TryCast(gp_case_alert.GetSelectionModel(), RowSelectionModel)
            Dim SelectedCaseID As RowSelectionModel = gp_case_alert.SelectionModel.Primary
            ' Jika user tidak memilih case iD
            If SelectedCaseID.SelectedRows.Count = 0 Then

                Throw New Exception("Minimal 1 Case ID harus dipilih")
            End If
            Dim TotalCaseID As Integer = 0
            For Each item As SelectedRow In SelectedCaseID.SelectedRows
                Dim JumlahCase As Integer = 0
                Dim recordID = item.RecordID.ToString
                Dim StrStatusCIF As String = "select count(*) from OneFCC_CaseManagement_STRMemo_CaseID cases inner join OneFCC_CaseManagement ofcm on cases.caseid = ofcm.pk_CaseManagement_id where ofcm.pk_CaseManagement_id = " & recordID.ToString
                JumlahCase = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrStatusCIF, Nothing)
                TotalCaseID = TotalCaseID + JumlahCase
            Next
            ' Jika case ID yang dipilih sudah ada di CaseID STR Memo
            If TotalCaseID > 0 Then
                Throw New ApplicationException("Salah Satu Case yang dipilih sudah di pilih oleh STRMemo yang lain")
            End If

            For Each item As SelectedRow In SmCaseID.SelectedRows
                Dim RecordID = item.RecordID.ToString
                Dim ObjNew = New CasemanagementDAL.OneFCC_CaseManagement_STRMemo_CaseID
                With ObjNew
                    .CaseID = RecordID
                End With

                objSTRMemoClass.listCaseID.Add(ObjNew)
            Next
            Dim CIF As String = Session("STRMemo_CIFNO")
            Dim StrQueryCode As String = "select isnull(indv_last_name,'') as indv_last_name from goaml_Ref_customer where cif = '" & CIF.ToString & "'"
            Dim CIFLastName As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)

            objSTRMemoClass.objSTRMemo.CIF = CIF
            objSTRMemoClass.objSTRMemo.No = txtNo.Value
            objSTRMemoClass.objSTRMemo.Kepada = txtKepada.Value
            objSTRMemoClass.objSTRMemo.Dari = txtDari.Value
            objSTRMemoClass.objSTRMemo.Tanggal = txtTanggal.Value
            objSTRMemoClass.objSTRMemo.Perihal = txtPerihal.Value
            objSTRMemoClass.objSTRMemo.Sumber_Pelaporan = txtSumberPelaporan.Value
            objSTRMemoClass.objSTRMemo.Nama_Pihak_Terlapor = txtNamaPihakPelapor.Value
            objSTRMemoClass.objSTRMemo.Kategori_Pihak_Terlapor = txtKategoriPihakPelapor.Value
            objSTRMemoClass.objSTRMemo.Unit_Bisnis_Directorat = txtUnitBisnis.Value
            objSTRMemoClass.objSTRMemo.Direktorat = txtDirektor.Value
            objSTRMemoClass.objSTRMemo.Cabang = txtCabang.Value
            If Not String.IsNullOrEmpty(cmb_KesimpulanDetail.SelectedItemValue) Then
                objSTRMemoClass.objSTRMemo.Detail_Kesimpulan = cmb_KesimpulanDetail.SelectedItemValue
            End If
    
            objSTRMemoClass.objSTRMemo.Lama_menjadi_Nasabah = txtLamaMenjadiNasabah.Value
            objSTRMemoClass.objSTRMemo.Pekerjaan_Bidang_Usaha = txtPekerjaanBidangUsaha.Value
            'Dim Penghasilan As Decimal = Convert.ToDecimal(txtPengahasilanTahun.Value, New CultureInfo("en-US"))
            'Dim multiplier As Integer = Convert.ToInt32(Math.Pow(10, 2))
            'Dim newD As Decimal = Math.Truncate(Penghasilan * multiplier) / multiplier
            objSTRMemoClass.objSTRMemo.Penghasilan_Tahun = txtPengahasilanTahun.Value
            objSTRMemoClass.objSTRMemo.Profil_Lainnya = txtProfilLainnya.Value
            objSTRMemoClass.objSTRMemo.Hasil_Analisis = txtHasilAnalisis.Value.ToString
            objSTRMemoClass.objSTRMemo.Kesimpulan = txtKesimpulan.Value
            If obj_ListPihakTerkait_Edit IsNot Nothing Then
                objSTRMemoClass.listPihakTerkait = obj_ListPihakTerkait_Edit
            Else
                obj_ListPihakTerkait_Edit = New List(Of OneFCC_CaseManagement_STRMemo_Pihak_Terkait)
                objSTRMemoClass.listPihakTerkait = obj_ListPihakTerkait_Edit
            End If

            objSTRMemoClass.listTindakLanjut = obj_ListTindakLanjut_Edit
            objSTRMemoClass.listIndikator = obj_ListIndikator_Edit
            objSTRMemoClass.listAttachment = obj_ListAttachment_Edit



            'End If
            objSTRMemoClass.objSTRMemo.StatusID = 1
            objSTRMemoClass.objSTRMemo.Workflow_Step = 2

            ' Isi Value Transcation STR ke STR Memo Transcation
            Dim ListSar As New List(Of goAML_ODM_Generate_STR_SAR)
            Dim ListODMTransactionIncomplete As New List(Of goAML_ODM_Transaksi)
            Dim ListTransactionIncomplete As New List(Of OneFCC_CaseManagement_STRMemo_Transaction)
            Dim getIsGenerateSAR As String = NawaDevBLL.GenerateSarBLL.getIsGenerateSAR("IsGenerateSAR")
            Dim ListTransactionNoTrnMode As New List(Of OneFCC_CaseManagement_STRMemo_Transaction)
            Dim ListODMTransactionNoTrnMode As New List(Of goAML_ODM_Transaksi)
            Dim dateReport As Date = TanggalLaporan.SelectedDate
            Dim maxDate As DateTime = DateTime.Now

            If sar_jenisLaporan.Value = "LTKMP" And NoRefPPATK.Text = "" Then
                Throw New ApplicationException("Jika Jenis Laporannya LTKMP maka No Ref PPATK Tidak Boleh Kosong")
            ElseIf dateReport <> DateTime.MinValue AndAlso dateReport > maxDate Then
                Throw New Exception("Tanngal Laporan : Can not select future date")
            End If




            Dim selected As Boolean
            If ListTransaction.Count = ListSelectedTransaction.Count Or sar_IsSelectedAll.Value Then
                selected = True
            Else
                selected = False
            End If


            If selected = False Then
                If ListSelectedTransaction.Count = 0 Then
                    Throw New ApplicationException("Tidak Ada Transaksi yang dipilih")
                End If

                Dim objODMTransaksi As New goAML_ODM_Transaksi
                Dim objSARTransaksi As New OneFCC_CaseManagement_STRMemo_Transaction

                objSTRMemoClass.listTransaction = New List(Of OneFCC_CaseManagement_STRMemo_Transaction)
                Dim param_RadioOption As String = ""

                If dd_TrnFilterBy.SelectedItemValue = "1" Then
                    param_RadioOption = "TrnHitByCaseAlert"
                ElseIf dd_TrnFilterBy.SelectedItemValue = "2" Then
                    param_RadioOption = "Trn5Year"
                ElseIf dd_TrnFilterBy.SelectedItemValue = "3" Then
                    param_RadioOption = "CustomDate"
                End If

                For Each item In ListSelectedTransaction
                    objODMTransaksi = ListTransaction.Where(Function(x) x.NO_ID = item.NO_ID).FirstOrDefault
                    If objODMTransaksi IsNot Nothing Then
                        With objSARTransaksi
                            .Jenis_Laporan = sar_jenisLaporan.SelectedItem.Value
                            .Tanggal_Laporan = TanggalLaporan.Value
                            .NoRefPPATK = NoRefPPATK.Text
                            .Alasan = alasan.Text
                            If dd_TrnFilterBy.SelectedItemValue = "3" Then
                                .Date_Form = sar_DateFrom.Value
                                .Date_To = sar_DateTo.Value
                            Else
                                .Date_Form = Nothing
                                .Date_To = Nothing
                            End If

                            .FilterTransaction = param_RadioOption

                            .NO_IDTransaction = objODMTransaksi.NO_ID
                            .Date_Transaction = objODMTransaksi.Date_Transaction
                            .CIF_NO = objODMTransaksi.CIF_NO
                            .Account_NO = objODMTransaksi.Account_NO
                            .WIC_NO = objODMTransaksi.WIC_No
                            .Ref_Num = objODMTransaksi.Ref_Num
                            .Debit_Credit = objODMTransaksi.Debit_Credit
                            .Original_Amount = objODMTransaksi.Original_Amount
                            .Currency = objODMTransaksi.Currency
                            .Exchange_Rate = objODMTransaksi.Exchange_Rate
                            .IDR_Amount = objODMTransaksi.IDR_Amount
                            .Transaction_Code = objODMTransaksi.Transaction_Code
                            .Source_Data = objODMTransaksi.Source_Data
                            .Transaction_Remark = objODMTransaksi.Transaction_Remark
                            .Transaction_Number = objODMTransaksi.Transaction_Number
                            .Transaction_Location = objODMTransaksi.Transaction_Location
                            .Teller = objODMTransaksi.Teller
                            .Authorized = objODMTransaksi.Authorized
                            .Date_Posting = objODMTransaksi.Date_Posting
                            .Transmode_Code = objODMTransaksi.Transmode_Code
                            .Transmode_Comment = objODMTransaksi.Transmode_Comment
                            .Comments = objODMTransaksi.Comments
                            .CIF_No_Lawan = objODMTransaksi.CIF_No_Lawan
                            .ACCOUNT_No_Lawan = objODMTransaksi.ACCOUNT_No_Lawan
                            .WIC_No_Lawan = objODMTransaksi.WIC_No_Lawan
                            .Conductor_ID = objODMTransaksi.Conductor_ID
                            .Swift_Code_Lawan = objODMTransaksi.Swift_Code_Lawan
                            .country_code_lawan = objODMTransaksi.Country_Code_Lawan
                            .MsgTypeSwift = objODMTransaksi.MsgSwiftType
                            .From_Funds_Code = objODMTransaksi.From_Funds_Code
                            .To_Funds_Code = objODMTransaksi.To_Funds_Code
                            .country_code = objODMTransaksi.Country_Code
                            .Currency_Lawan = objODMTransaksi.Currency_Lawan
                            If objODMTransaksi.BiMultiParty.HasValue Then
                                .BiMultiParty = objODMTransaksi.BiMultiParty
                            End If
                            .GCN = objODMTransaksi.GCN
                            .GCN_Lawan = objODMTransaksi.GCN_Lawan
                            .Active = 1
                            .Business_date = objODMTransaksi.Business_date
                            If objODMTransaksi.From_Type.HasValue Then
                                .From_Type = objODMTransaksi.From_Type
                            End If
                            If objODMTransaksi.To_Type.HasValue Then
                                .To_Type = objODMTransaksi.To_Type
                            End If
                        End With

                        objSTRMemoClass.listTransaction.Add(objSARTransaksi)

                    End If


                    objODMTransaksi = New goAML_ODM_Transaksi
                    objSARTransaksi = New OneFCC_CaseManagement_STRMemo_Transaction
                Next

                If getIsGenerateSAR = "1" Then
                    ListTransactionIncomplete = objSTRMemoClass.listTransaction.Where(Function(x) x.BiMultiParty = 1 And x.CIF_No_Lawan Is Nothing And x.WIC_No_Lawan Is Nothing And x.ACCOUNT_No_Lawan Is Nothing).ToList
                    If ListTransactionIncomplete.Count > 0 Then
                        Throw New ApplicationException("Any Counter Party of Transaction is not completed")
                    End If
                End If

                ListTransactionNoTrnMode = objSTRMemoClass.listTransaction.Where(Function(x) x.Transmode_Code = "" Or x.Transmode_Code Is Nothing).ToList
                If ListTransactionNoTrnMode.Count > 0 Then
                    Throw New ApplicationException("Cara Tranaksi Dilakukan Kosong Di " + ListTransactionNoTrnMode.Count.ToString + " Transaksi")
                End If

            ElseIf selected = True Then
                Dim param_RadioOption As String = ""

                If dd_TrnFilterBy.SelectedItemValue = "1" Then
                    param_RadioOption = "TrnHitByCaseAlert"
                ElseIf dd_TrnFilterBy.SelectedItemValue = "2" Then
                    param_RadioOption = "Trn5Year"
                ElseIf dd_TrnFilterBy.SelectedItemValue = "3" Then
                    param_RadioOption = "CustomDate"
                End If
                '' add 23-Feb-2022
                'Dim objODMTransaksi As New goAML_ODM_Transaksi
                Dim objSARTransaksi As New OneFCC_CaseManagement_STRMemo_Transaction
                For Each item In ListTransaction
                    With objSARTransaksi
                        .Jenis_Laporan = sar_jenisLaporan.SelectedItem.Value
                        .Tanggal_Laporan = TanggalLaporan.Value
                        .NoRefPPATK = NoRefPPATK.Text
                        .Alasan = alasan.Text
                        .Date_Transaction = item.Date_Transaction
                        If dd_TrnFilterBy.SelectedItemValue = "3" Then
                            .Date_Form = sar_DateFrom.Value
                            .Date_To = sar_DateTo.Value
                        Else
                            .Date_Form = Nothing
                            .Date_To = Nothing
                        End If

                        .FilterTransaction = param_RadioOption
                        .NO_IDTransaction = "ALL"
                        .CIF_NO = item.CIF_NO
                        .Account_NO = item.Account_NO
                        .WIC_NO = item.WIC_No
                        .Ref_Num = item.Ref_Num
                        .Debit_Credit = item.Debit_Credit
                        .Original_Amount = item.Original_Amount
                        .Currency = item.Currency
                        .Exchange_Rate = item.Exchange_Rate
                        .IDR_Amount = item.IDR_Amount
                        .Transaction_Code = item.Transaction_Code
                        .Source_Data = item.Source_Data
                        .Transaction_Remark = item.Transaction_Remark
                        .Transaction_Number = item.Transaction_Number
                        .Transaction_Location = item.Transaction_Location
                        .Teller = item.Teller
                        .Authorized = item.Authorized
                        .Date_Posting = item.Date_Posting
                        .Transmode_Code = item.Transmode_Code
                        .Transmode_Comment = item.Transmode_Comment
                        .Comments = item.Comments
                        .CIF_No_Lawan = item.CIF_No_Lawan
                        .ACCOUNT_No_Lawan = item.ACCOUNT_No_Lawan
                        .WIC_No_Lawan = item.WIC_No_Lawan
                        .Conductor_ID = item.Conductor_ID
                        .Swift_Code_Lawan = item.Swift_Code_Lawan
                        .country_code_lawan = item.Country_Code_Lawan
                        .MsgTypeSwift = item.MsgSwiftType
                        .From_Funds_Code = item.From_Funds_Code
                        .To_Funds_Code = item.To_Funds_Code
                        .country_code = item.Country_Code
                        .Currency_Lawan = item.Currency_Lawan
                        If item.BiMultiParty.HasValue Then
                            .BiMultiParty = item.BiMultiParty
                        End If
                        .GCN = item.GCN
                        .GCN_Lawan = item.GCN_Lawan
                        .Active = 1
                        .Business_date = item.Business_date
                        If item.From_Type.HasValue Then
                            .From_Type = item.From_Type
                        End If
                        If item.To_Type.HasValue Then
                            .To_Type = item.To_Type
                        End If
                    End With

                    objSTRMemoClass.listTransaction.Add(objSARTransaksi)

                    'objODMTransaksi = New goAML_ODM_Transaksi
                    objSARTransaksi = New OneFCC_CaseManagement_STRMemo_Transaction
                Next
                '' End 23-Feb-2022

                If ListTransaction.Count = 0 Then
                    Throw New ApplicationException("Tidak Ada Transaksi")
                End If
                If getIsGenerateSAR = "1" Then
                    ListODMTransactionIncomplete = ListTransaction.Where(Function(x) x.BiMultiParty = 1 And x.CIF_No_Lawan Is Nothing And x.WIC_No_Lawan Is Nothing And x.ACCOUNT_No_Lawan Is Nothing).ToList
                    If ListODMTransactionIncomplete.Count > 0 Then
                        Throw New ApplicationException("Any Counter Party of Transaction is not completed")
                    End If
                End If

                ListODMTransactionNoTrnMode = ListTransaction.Where(Function(x) x.Transmode_Code = "" Or x.Transmode_Code Is Nothing).ToList
                If ListODMTransactionNoTrnMode.Count > 0 Then
                    Throw New ApplicationException("Cara Tranaksi Dilakukan Kosong Di " + ListODMTransactionNoTrnMode.Count.ToString + " Transaksi")
                End If
            End If

            '' Edit 16-Feb-2022, Untuk Case Alert, Tanpa Approval

            Dim DateFrom As DateTime = sar_DateFrom.Value
            Dim DateTo As DateTime = sar_DateTo.Value
            'SaveGenerateSTRSarTanpaApproval(ObjSARData, ObjModule, 1, DateFrom, DateTo)

            Dim paramm(0) As SqlParameter

            paramm(0) = New SqlParameter
            paramm(0).ParameterName = "@CIF"
            paramm(0).Value = objSTRMemoClass.objSTRMemo.CIF
            paramm(0).SqlDbType = SqlDbType.VarChar

            Dim FkWorkflow As String = ""
            FkWorkflow = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CaseManagement_STRMemo_GetWorkflow", paramm)
            objSTRMemoClass.objSTRMemo.FK_CaseManagement_Workflow_STRMemo_ID = FkWorkflow
            STRMemo_BLL.SaveAddTanpaApproval(objSTRMemoClass, ObjModule)
            Dim StrQueryUpdateHasilAnalisis As String = "Update OneFCC_CaseManagement_STRMemo "
            StrQueryUpdateHasilAnalisis = StrQueryUpdateHasilAnalisis + " set Hasil_Analisis = case when right(Hasil_Analisis,2) = '??' then Substring(Hasil_Analisis,0,len(Hasil_Analisis))"
            StrQueryUpdateHasilAnalisis = StrQueryUpdateHasilAnalisis + " when right(Hasil_Analisis,2) like '%?' then Substring(Hasil_Analisis,0,len(Hasil_Analisis)) else Hasil_Analisis END"
            StrQueryUpdateHasilAnalisis = StrQueryUpdateHasilAnalisis + " From OneFCC_CaseManagement_STRMemo"
            StrQueryUpdateHasilAnalisis = StrQueryUpdateHasilAnalisis + " where PK_OneFCC_CaseManagement_STRMemo_ID =" & objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryUpdateHasilAnalisis, Nothing)
            Panelconfirmation.Hidden = False
            fpMain.Hidden = True
            LblConfirmation.Text = "Data Saved into Pending Approval"
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub STRMemo_Add_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Insert
    End Sub

    Private Sub STRMemo_Add_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleAdd = New NawaBLL.FormModuleAdd(fpMain, Panelconfirmation, LblConfirmation)
    End Sub

#Region "Indikator"

    Public Property IDIndikator() As Long
        Get
            Return Session("STRMemo_Add.IDIndikator")
        End Get
        Set(ByVal value As Long)
            Session("STRMemo_Add.IDIndikator") = value
        End Set
    End Property

    Protected Sub Clean_Window_Indikator()
        'Clean fields
        cmb_indikator.SetTextValue("")
        'Show Buttons
        btn_indikator_Save.Hidden = False
    End Sub

    Public Property obj_Indikator_Edit() As OneFCC_CaseManagement_STRMemo_Indikator
        Get
            Return Session("STRMemo_Add.obj_Indikator_Edit")
        End Get
        Set(ByVal value As OneFCC_CaseManagement_STRMemo_Indikator)
            Session("STRMemo_Add.obj_Indikator_Edit") = value
        End Set
    End Property

    Public Property obj_ListIndikator_Edit() As List(Of OneFCC_CaseManagement_STRMemo_Indikator)
        Get
            Return Session("STRMemo_Add.obj_ListIndikator_Edit")
        End Get
        Set(ByVal value As List(Of OneFCC_CaseManagement_STRMemo_Indikator))
            Session("STRMemo_Add.obj_ListIndikator_Edit") = value
        End Set
    End Property

    Protected Sub btn_Indikator_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            obj_Indikator_Edit = Nothing
            Clean_Window_Indikator()
            
            'Show window pop up
            cmb_indikator.IsReadOnly = False
            btn_indikator_Save.Hidden = False '' Added on 12 Aug 2021
            cmb_indikator.StringFieldStyle = "background-color:#FFE4C4"
            Window_Indikator.Title = "Indikator - Add"
            Window_Indikator.Hidden = False
            Window_Indikator.StyleSpec = "Width : 400px;Height : 400px;"

            'Set IDIndikator to 0
            IDIndikator = 0

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_indikator(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListIndikator_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListIndikator_Edit.Remove(objToDelete)
                    End If
                    Bind_Indikator()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Indikator_Edit = obj_ListIndikator_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID = strID)
                    Load_Window_Indikator(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Indikator_Cancel_Click()
        Try
            'Hide window pop up
            Window_Indikator.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Indikator_Save_Click()
        Try
            'Validate input

            If String.IsNullOrEmpty(cmb_indikator.SelectedItemValue) Then
                Throw New ApplicationException(cmb_indikator.Label & " harus diisi.")
            End If

            'Action save here
            Dim intPK As Long = -1

            If obj_Indikator_Edit Is Nothing Then  'Add
                Dim objAdd As New OneFCC_CaseManagement_STRMemo_Indikator
                If obj_ListIndikator_Edit IsNot Nothing Then
                    If obj_ListIndikator_Edit.Count > 0 Then
                        intPK = obj_ListIndikator_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDIndikator = intPK

                With objAdd
                    .PK_OneFCC_CaseManagement_STRMemo_Indikator_ID = intPK

                    If Not String.IsNullOrEmpty(cmb_indikator.SelectedItemValue) Then
                        .FK_Indicator = cmb_indikator.SelectedItemValue
                    Else
                        .FK_Indicator = Nothing
                    End If


                End With
                If obj_ListIndikator_Edit Is Nothing Then
                    obj_ListIndikator_Edit = New List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Indikator)
                End If
                obj_ListIndikator_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListIndikator_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID = obj_Indikator_Edit.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDIndikator = .PK_OneFCC_CaseManagement_STRMemo_Indikator_ID

                        If Not String.IsNullOrEmpty(cmb_indikator.SelectedItemValue) Then
                            .FK_Indicator = cmb_indikator.SelectedItemValue
                        Else
                            .FK_Indicator = Nothing
                        End If

                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_Indikator()

            'Hide window popup
            Window_Indikator.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_Indikator()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListIndikator_Edit)

        objtable.Columns.Add(New DataColumn("Kode", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        For Each row As DataRow In objtable.Rows
            row("Kode") = row("FK_Indicator")
            If Not IsDBNull(row("Kode")) Then
                Dim StrQueryCode As String = "select Keterangan from goAML_Ref_Indikator_Laporan where Kode = '" & row("Kode").ToString & "'"
                Dim Keterangan As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                row("Keterangan") = Keterangan
            Else
                row("Keterangan") = Nothing
            End If
        Next
        gp_indikator.GetStore().DataSource = objtable
        gp_indikator.GetStore().DataBind()


    End Sub

    Protected Sub Load_Window_Indikator(strAction As String)
        'Clean window pop up
        Clean_Window_Indikator()

        If obj_Indikator_Edit IsNot Nothing Then
            'Populate fields
            With obj_Indikator_Edit
                If .FK_Indicator IsNot Nothing Then
                    Using objdb As New NawaDatadevEntities
                        Dim objKode As goAML_Ref_Indikator_Laporan = objdb.goAML_Ref_Indikator_Laporan.Where(Function(X) X.Kode = .FK_Indicator).FirstOrDefault
                        If Not objKode Is Nothing Then
                            cmb_indikator.SetTextWithTextValue(obj_Indikator_Edit.FK_Indicator, objKode.Keterangan)
                        End If
                    End Using

                End If

            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            cmb_indikator.IsReadOnly = False
            btn_indikator_Save.Hidden = False '' Added on 12 Aug 2021
        Else
            cmb_indikator.IsReadOnly = True

            btn_indikator_Save.Hidden = True '' Added on 12 Aug 2021
        End If
        cmb_indikator.StringFieldStyle = "background-color:#FFE4C4"
        'Bind Indikator
        IDIndikator = obj_Indikator_Edit.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID

        'Show window pop up
        Window_Indikator.Title = "Indikator - " & strAction
        Window_Indikator.Hidden = False
    End Sub


#End Region

#Region "Pihak Terkait"

    Public Property IDPihakTerkait() As Long
        Get
            Return Session("STRMemo_Add.IDPihakTerkait")
        End Get
        Set(ByVal value As Long)
            Session("STRMemo_Add.IDPihakTerkait") = value
        End Set
    End Property

    Protected Sub Clean_Window_PihakTerkait()
        'Clean fields
        txtPT_CIF.Value = Nothing
        txtPT_HubunganPihakTerkait.Value = Nothing
        txtPT_RefGrips.Value = Nothing
        txtPT_TelahDilaporkan.Value = Nothing

        cmbPT_CIF.SetTextValue("")
        'Show Buttons
        btn_PihakTerkait_Save.Hidden = False

    End Sub


    Public Property obj_PihakTerkait_Edit() As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait
        Get
            Return Session("STRMemo_Add.obj_PihakTerkait_Edit")
        End Get
        Set(ByVal value As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait)
            Session("STRMemo_Add.obj_PihakTerkait_Edit") = value
        End Set
    End Property

    Public Property obj_ListPihakTerkait_Edit() As List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait)
        Get
            Return Session("STRMemo_Add.obj_ListPihakTerkait_Edit")
        End Get
        Set(ByVal value As List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait))
            Session("STRMemo_Add.obj_ListPihakTerkait_Edit") = value
        End Set
    End Property

    Protected Sub btn_Pihak_Terkait_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            obj_PihakTerkait_Edit = Nothing
            Clean_Window_PihakTerkait()
            
            'Show window pop up
            txtPT_CIF.ReadOnly = False
            cmbPT_CIF.IsReadOnly = False
            txtPT_HubunganPihakTerkait.ReadOnly = False
            txtPT_RefGrips.ReadOnly = False
            txtPT_TelahDilaporkan.ReadOnly = False
            cmbPT_CIF.StringFieldStyle = "background-color:#FFE4C4"
            btn_PihakTerkait_Save.Hidden = False '' Added on 12 Aug 2021
            Window_PihakTerkair.Title = "Pihak Terkait - Add"
            Window_PihakTerkair.Hidden = False
            txtPT_CIF.Value = Session("STRMemo_CIFNO")
            'Set IDIndikator to 0
            IDPihakTerkait = 0


        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_PihakTerkait(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListPihakTerkait_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListPihakTerkait_Edit.Remove(objToDelete)
                    End If
                    Bind_Pihak_Terkait()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_PihakTerkait_Edit = obj_ListPihakTerkait_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID = strID)
                    Load_Window_PihakTerkait(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_PihakTerkait_Cancel_Click()
        Try
            'Hide window pop up
            Window_PihakTerkair.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_PihakTerkait_Save_Click()
        Try
            'Validate input


            If String.IsNullOrWhiteSpace(cmbPT_CIF.SelectedItemValue) Then
                Throw New ApplicationException(cmbPT_CIF.Label & " is required.")
            End If
            If String.IsNullOrEmpty(txtPT_HubunganPihakTerkait.Value) Then
                Throw New ApplicationException(txtPT_HubunganPihakTerkait.FieldLabel & " harus diisi.")
            End If
            'If String.IsNullOrEmpty(txtPT_RefGrips.Value) Then
            '    Throw New ApplicationException(txtPT_RefGrips.FieldLabel & " harus diisi.")
            'End If
            'If String.IsNullOrEmpty(txtPT_TelahDilaporkan.Value) Then
            '    Throw New ApplicationException(txtPT_TelahDilaporkan.FieldLabel & " harus diisi.")
            'End If



            'Action save here
            Dim intPK As Long = -1

            If obj_PihakTerkait_Edit Is Nothing Then  'Add
                Dim objAdd As New CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait
                If obj_ListPihakTerkait_Edit IsNot Nothing Then
                    If obj_ListPihakTerkait_Edit.Count > 0 Then
                        intPK = obj_ListPihakTerkait_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDPihakTerkait = intPK

                With objAdd
                    .PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID = intPK

                    If Not String.IsNullOrEmpty(cmbPT_CIF.SelectedItemValue) Then
                        .CIF = cmbPT_CIF.SelectedItemValue
                        Dim Name As String = ""
                        Dim StrQueryCode As String = "select case when fk_customer_type_id = 1 then indv_last_name else corp_name end as indv_last_name from goaml_Ref_customer where cif = '" & cmbPT_CIF.SelectedItemValue & "'"
                        Name = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                        .Nama_PihakTerkait = Name
                    Else
                        .CIF = Nothing
                    End If

                    If Not String.IsNullOrEmpty(txtPT_HubunganPihakTerkait.Value) Then
                        .Hubungan_PihakTerkait = txtPT_HubunganPihakTerkait.Value
                    Else
                        .Hubungan_PihakTerkait = Nothing
                    End If

                    If Not String.IsNullOrEmpty(txtPT_RefGrips.Value) Then
                        .RefGrips_PihakTerkait = txtPT_RefGrips.Value
                    Else
                        .RefGrips_PihakTerkait = Nothing
                    End If

                    If Not String.IsNullOrEmpty(txtPT_TelahDilaporkan.Value) Then
                        .Dilaporkan_PihakTerkait = txtPT_TelahDilaporkan.Value
                    Else
                        .Dilaporkan_PihakTerkait = Nothing
                    End If

                End With
                If obj_ListPihakTerkait_Edit Is Nothing Then
                    obj_ListPihakTerkait_Edit = New List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait)
                End If
                obj_ListPihakTerkait_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListPihakTerkait_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID = obj_PihakTerkait_Edit.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDPihakTerkait = .PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID

                        If Not String.IsNullOrEmpty(cmbPT_CIF.SelectedItemValue) Then
                            .CIF = cmbPT_CIF.SelectedItemValue
                            Dim Name As String = ""
                            Dim StrQueryCode As String = "select case when fk_customer_type_id = 1 then indv_last_name else corp_name end as indv_last_name from goaml_Ref_customer where cif = '" & cmbPT_CIF.SelectedItemValue & "'"
                            Name = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                            .Nama_PihakTerkait = Name
                        Else
                            .CIF = Nothing
                        End If



                        If Not String.IsNullOrEmpty(txtPT_HubunganPihakTerkait.Value) Then
                            .Hubungan_PihakTerkait = txtPT_HubunganPihakTerkait.Value
                        Else
                            .Hubungan_PihakTerkait = Nothing
                        End If

                        If Not String.IsNullOrEmpty(txtPT_RefGrips.Value) Then
                            .RefGrips_PihakTerkait = txtPT_RefGrips.Value
                        Else
                            .RefGrips_PihakTerkait = Nothing
                        End If

                        If Not String.IsNullOrEmpty(txtPT_TelahDilaporkan.Value) Then
                            .Dilaporkan_PihakTerkait = txtPT_TelahDilaporkan.Value
                        Else
                            .Dilaporkan_PihakTerkait = Nothing
                        End If

                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_Pihak_Terkait()

            'Hide window popup
            Window_PihakTerkair.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_Pihak_Terkait()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListPihakTerkait_Edit)
        For Each row As DataRow In objtable.Rows
            If Not IsDBNull(row("CIF")) Then
                Dim CIF As String = ""
                Dim StrQueryCode As String = "select case when fk_customer_type_id = 1 then indv_last_name else corp_name end as indv_last_name from goaml_Ref_customer where cif = '" & row("CIF").ToString & "'"
                CIF = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                row("Nama_PihakTerkait") = CIF
            Else
                row("Nama_PihakTerkait") = Nothing
            End If
        Next

        gp_Pihak_Terkait.GetStore().DataSource = objtable
        gp_Pihak_Terkait.GetStore().DataBind()


    End Sub

    Protected Sub Load_Window_PihakTerkait(strAction As String)
        'Clean window pop up
        Clean_Window_PihakTerkait()

        If obj_PihakTerkait_Edit IsNot Nothing Then
            'Populate fields
            With obj_PihakTerkait_Edit
                If Not String.IsNullOrEmpty(.CIF) Then
                    Dim strfieldtypestring As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT Keterangan FROM vw_goAML_Ref_Customer WHERE kode='" & .CIF & "'", Nothing)
                    cmbPT_CIF.SetTextWithTextValue(.CIF, strfieldtypestring)
                Else
                    cmbPT_CIF.SetTextValue("")
                End If

                If Not String.IsNullOrEmpty(.Hubungan_PihakTerkait) Then
                    txtPT_HubunganPihakTerkait.Value = .Hubungan_PihakTerkait
                Else
                    txtPT_HubunganPihakTerkait.Value = Nothing
                End If

                If Not String.IsNullOrEmpty(.RefGrips_PihakTerkait) Then
                    txtPT_RefGrips.Value = .RefGrips_PihakTerkait
                Else
                    txtPT_RefGrips.Value = Nothing
                End If

                If Not String.IsNullOrEmpty(.Dilaporkan_PihakTerkait) Then
                    txtPT_TelahDilaporkan.Value = .Dilaporkan_PihakTerkait
                Else
                    txtPT_TelahDilaporkan.Value = Nothing
                End If

            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            txtPT_CIF.ReadOnly = False
            cmbPT_CIF.IsReadOnly = False
            txtPT_HubunganPihakTerkait.ReadOnly = False
            txtPT_RefGrips.ReadOnly = False
            txtPT_TelahDilaporkan.ReadOnly = False
            btn_PihakTerkait_Save.Hidden = False '' Added on 12 Aug 2021
        Else
            cmbPT_CIF.IsReadOnly = True
            txtPT_CIF.ReadOnly = True
            txtPT_HubunganPihakTerkait.ReadOnly = True
            txtPT_RefGrips.ReadOnly = True
            txtPT_TelahDilaporkan.ReadOnly = True
            btn_PihakTerkait_Save.Hidden = True '' Added on 12 Aug 2021
        End If

        'Bind Indikator
        IDPihakTerkait = obj_PihakTerkait_Edit.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID

        'Show window pop up
        Window_PihakTerkair.Title = "Pihak Terkait - " & strAction
        Window_PihakTerkair.Hidden = False
    End Sub
#End Region

#Region "Tindak Lanjut"

    Public Property IDTindakLanjut() As Long
        Get
            Return Session("STRMemo_Add.IDTindakLanjut")
        End Get
        Set(ByVal value As Long)
            Session("STRMemo_Add.IDTindakLanjut") = value
        End Set
    End Property

    Protected Sub Clean_Window_TindakLanjut()
        'Clean fields
        cmb_tindaklanjut.SetTextValue("")
        'Show Buttons
        btn_TindakLanjut_Save.Hidden = False
    End Sub

    Public Property obj_TindakLanjut_Edit() As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut
        Get
            Return Session("STRMemo_Add.obj_TindakLanjut_Edit")
        End Get
        Set(ByVal value As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut)
            Session("STRMemo_Add.obj_TindakLanjut_Edit") = value
        End Set
    End Property

    Public Property obj_ListTindakLanjut_Edit() As List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut)
        Get
            Return Session("STRMemo_Add.obj_ListTindakLanjut_Edit")
        End Get
        Set(ByVal value As List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut))
            Session("STRMemo_Add.obj_ListTindakLanjut_Edit") = value
        End Set
    End Property

    Protected Sub btn_Tindak_Lanjut_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            obj_TindakLanjut_Edit = Nothing
            Clean_Window_TindakLanjut()
            
            'Show window pop up
            cmb_tindaklanjut.IsReadOnly = False
            btn_TindakLanjut_Save.Hidden = False '' Added on 12 Aug 2021
            cmb_tindaklanjut.StringFieldStyle = "background-color:#FFE4C4"
            window_tindaklanjut.Title = "Tindak Lanjut - Add"
            window_tindaklanjut.Hidden = False

            'Set IDIndikator to 0
            IDTindakLanjut = 0

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_TindakLanjut(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListTindakLanjut_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListTindakLanjut_Edit.Remove(objToDelete)
                    End If
                    Bind_TindakLanjut()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_TindakLanjut_Edit = obj_ListTindakLanjut_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID = strID)
                    Load_Window_TindakLanjut(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_TindakLanjut_Cancel_Click()
        Try
            'Hide window pop up
            window_tindaklanjut.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_TindakLanjut_Save_Click()
        Try
            'Validate input

            If String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemValue) Then
                Throw New ApplicationException(cmb_tindaklanjut.Label & " harus diisi.")
            End If


            'Action save here
            Dim intPK As Long = -1

            If obj_TindakLanjut_Edit Is Nothing Then  'Add
                Dim objAdd As New CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut
                If obj_ListTindakLanjut_Edit IsNot Nothing Then
                    If obj_ListTindakLanjut_Edit.Count > 0 Then
                        intPK = obj_ListTindakLanjut_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDTindakLanjut = intPK

                With objAdd
                    .PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID = intPK
                    If Not String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemValue) Then
                        .FK_OneFCC_CaseManagement_TindakLanjut_ID = cmb_tindaklanjut.SelectedItemValue
                    Else
                        .FK_OneFCC_CaseManagement_TindakLanjut_ID = Nothing
                    End If
                    If Not String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemText) Then
                        .TindakLanjutDescription = cmb_tindaklanjut.SelectedItemText
                    Else
                        .TindakLanjutDescription = Nothing
                    End If

                End With
                If obj_ListTindakLanjut_Edit Is Nothing Then
                    obj_ListTindakLanjut_Edit = New List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut)
                End If
                obj_ListTindakLanjut_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListTindakLanjut_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID = obj_TindakLanjut_Edit.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDTindakLanjut = .PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID

                        If Not String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemValue) Then
                            .FK_OneFCC_CaseManagement_TindakLanjut_ID = cmb_tindaklanjut.SelectedItemValue
                        Else
                            .FK_OneFCC_CaseManagement_TindakLanjut_ID = Nothing
                        End If
                        If Not String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemText) Then
                            .TindakLanjutDescription = cmb_tindaklanjut.SelectedItemText
                        Else
                            .TindakLanjutDescription = Nothing
                        End If

                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_TindakLanjut()

            'Hide window popup
            window_tindaklanjut.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_TindakLanjut()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListTindakLanjut_Edit)


        gp_TIndakLanjut.GetStore().DataSource = objtable
        gp_TIndakLanjut.GetStore().DataBind()


    End Sub

    Protected Sub Load_Window_TindakLanjut(strAction As String)
        'Clean window pop up
        Clean_Window_TindakLanjut()

        If obj_TindakLanjut_Edit IsNot Nothing Then
            'Populate fields
            With obj_TindakLanjut_Edit
                If .FK_OneFCC_CaseManagement_TindakLanjut_ID IsNot Nothing Then

                    cmb_tindaklanjut.SetTextWithTextValue(.FK_OneFCC_CaseManagement_TindakLanjut_ID, .TindakLanjutDescription)


                End If

            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            cmb_tindaklanjut.IsReadOnly = False
            btn_TindakLanjut_Save.Hidden = False '' Added on 12 Aug 2021
        Else
            cmb_tindaklanjut.IsReadOnly = True

            btn_TindakLanjut_Save.Hidden = True '' Added on 12 Aug 2021
        End If
        cmb_tindaklanjut.StringFieldStyle = "background-color:#FFE4C4"
        'Bind Indikator
        IDTindakLanjut = obj_TindakLanjut_Edit.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID

        'Show window pop up
        window_tindaklanjut.Title = "Tindak Lanjut - " & strAction
        window_tindaklanjut.Hidden = False
    End Sub
#End Region

#Region "Transaction"
    Public Property IDDokumen As String
        Get
            Return Session("STRMemo_Add.IDDokumen")
        End Get
        Set(value As String)
            Session("STRMemo_Add.IDDokumen") = value
        End Set
    End Property
    Public Property ListSelectedTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi)
        Get
            If Session("STRMemo_Add.ListSelectedTransaction") Is Nothing Then
                Session("STRMemo_Add.ListSelectedTransaction") = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
            End If
            Return Session("STRMemo_Add.ListSelectedTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Transaksi))
            Session("STRMemo_Add.ListSelectedTransaction") = value
        End Set
    End Property
    Public Property ListTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi)
        Get
            If Session("STRMemo_Add.ListTransaction") Is Nothing Then
                Session("STRMemo_Add.ListTransaction") = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
            End If
            Return Session("STRMemo_Add.ListTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Transaksi))
            Session("STRMemo_Add.ListTransaction") = value
        End Set
    End Property
    Protected Sub dd_TrnFilterBy_ValueChanged(sender As Object, e As DirectEventArgs)
        Try

            If dd_TrnFilterBy.SelectedItemValue = "1" Then
                sar_DateFrom.Hidden = True
                sar_DateTo.Hidden = True
            ElseIf dd_TrnFilterBy.SelectedItemValue = "2" Then
                sar_DateFrom.Hidden = True
                sar_DateTo.Hidden = True
            ElseIf dd_TrnFilterBy.SelectedItemValue = "3" Then
                sar_DateFrom.Hidden = False
                sar_DateTo.Hidden = False
            Else
                Throw New ApplicationException("Please select one of the filter options")
            End If

            'rb_CustomDate.Checked = True
            'sar_DateFrom.Hidden = False
            'sar_DateTo.Hidden = False

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub bindTransactionDataTable(store As Ext.Net.Store, dt As DataTable)
        'store.DataSource = New DataTable

        Dim objtable As New Data.DataTable
        objtable = NawaBLL.Common.CopyGenericToDataTable(ListTransaction)
        'Dim objtable As Data.DataTable = dt

        objtable.Columns.Add(New DataColumn("Valid", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoRefTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("TipeTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("LokasiTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("KeteranganTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("DateTransaction", GetType(Date)))
        objtable.Columns.Add(New DataColumn("AccountNo", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaTeller", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaPejabat", GetType(String)))
        objtable.Columns.Add(New DataColumn("TglPembukuan", GetType(Date)))
        objtable.Columns.Add(New DataColumn("CaraTranDilakukan", GetType(String)))
        objtable.Columns.Add(New DataColumn("CaraTranLain", GetType(String)))
        objtable.Columns.Add(New DataColumn("DebitCredit", GetType(String)))
        objtable.Columns.Add(New DataColumn("OriginalAmount", GetType(String)))
        objtable.Columns.Add(New DataColumn("IDR", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim CifLawan As String = item("CIF_No_Lawan").ToString
                Dim WicLawan As String = item("WIC_No_Lawan").ToString
                Dim AccountLawan As String = item("ACCOUNT_No_Lawan").ToString
                Dim BiMulti As String = item("BiMultiParty").ToString
                If CifLawan = "" And WicLawan = "" And AccountLawan = "" And BiMulti = "1" Then
                    item("Valid") = "Tidak"
                Else
                    item("Valid") = "Ya"
                End If
                item("NoTran") = item("Transaction_Number")
                item("NoRefTran") = item("Ref_Num")
                Dim SourceData As String = item("Source_Data").ToString
                Dim SourceDataTemp As Integer
                If Integer.TryParse(SourceData, SourceDataTemp) Then
                    If SourceData <> "" Then
                        item("TipeTran") = NawaDevBLL.GenerateSarBLL.getSourceData(SourceData)
                    End If
                Else
                    item("TipeTran") = SourceData
                End If
                item("LokasiTran") = item("Transaction_Location")
                item("KeteranganTran") = item("Transaction_Remark")
                item("DateTransaction") = item("Date_Transaction")
                item("AccountNo") = item("Account_NO")
                item("NamaTeller") = item("Teller")
                item("NamaPejabat") = item("Authorized")
                item("TglPembukuan") = item("Date_Posting")
                item("CaraTranDilakukan") = item("Transmode_Code")
                item("CaraTranLain") = item("Transmode_Comment")
                item("DebitCredit") = item("Debit_Credit")
                If item("Original_Amount").ToString <> "" Then
                    Dim OriginalAmount As Decimal = item("Original_Amount").ToString
                    item("OriginalAmount") = OriginalAmount.ToString("#,###.00")
                End If
                If item("IDR_Amount").ToString <> "" Then
                    Dim IDR As Decimal = item("IDR_Amount").ToString
                    item("IDR") = IDR.ToString("#,###.00")
                End If
            Next
        End If
        'store.DataSource = objtable
        'store.DataBind()
        GridPaneldetail.GetStore.DataSource = objtable
        GridPaneldetail.GetStore.DataBind()

    End Sub

    Sub bindTransactionNoCheckedDataTable(store As Ext.Net.Store, dt As DataTable)
        'store.DataSource = New DataTable

        Dim objtable As New Data.DataTable
        objtable = NawaBLL.Common.CopyGenericToDataTable(ListTransaction)
        'Dim objtable As Data.DataTable = dt

        objtable.Columns.Add(New DataColumn("Valid", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoRefTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("TipeTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("LokasiTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("KeteranganTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("DateTransaction", GetType(Date)))
        objtable.Columns.Add(New DataColumn("AccountNo", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaTeller", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaPejabat", GetType(String)))
        objtable.Columns.Add(New DataColumn("TglPembukuan", GetType(Date)))
        objtable.Columns.Add(New DataColumn("CaraTranDilakukan", GetType(String)))
        objtable.Columns.Add(New DataColumn("CaraTranLain", GetType(String)))
        objtable.Columns.Add(New DataColumn("DebitCredit", GetType(String)))
        objtable.Columns.Add(New DataColumn("OriginalAmount", GetType(String)))
        objtable.Columns.Add(New DataColumn("IDR", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim CifLawan As String = item("CIF_No_Lawan").ToString
                Dim WicLawan As String = item("WIC_No_Lawan").ToString
                Dim AccountLawan As String = item("ACCOUNT_No_Lawan").ToString
                Dim BiMulti As String = item("BiMultiParty").ToString
                If CifLawan = "" And WicLawan = "" And AccountLawan = "" And BiMulti = "1" Then
                    item("Valid") = "Tidak"
                Else
                    item("Valid") = "Ya"
                End If
                item("NoTran") = item("Transaction_Number")
                item("NoRefTran") = item("Ref_Num")
                Dim SourceData As String = item("Source_Data").ToString
                Dim SourceDataTemp As Integer
                If Integer.TryParse(SourceData, SourceDataTemp) Then
                    If SourceData <> "" Then
                        item("TipeTran") = NawaDevBLL.GenerateSarBLL.getSourceData(SourceData)
                    End If
                Else
                    item("TipeTran") = SourceData
                End If
                item("LokasiTran") = item("Transaction_Location")
                item("KeteranganTran") = item("Transaction_Remark")
                item("DateTransaction") = item("Date_Transaction")
                item("AccountNo") = item("Account_NO")
                item("NamaTeller") = item("Teller")
                item("NamaPejabat") = item("Authorized")
                item("TglPembukuan") = item("Date_Posting")
                item("CaraTranDilakukan") = item("Transmode_Code")
                item("CaraTranLain") = item("Transmode_Comment")
                item("DebitCredit") = item("Debit_Credit")
                If item("Original_Amount").ToString <> "" Then
                    Dim OriginalAmount As Decimal = item("Original_Amount").ToString
                    item("OriginalAmount") = OriginalAmount.ToString("#,###.00")
                End If
                If item("IDR_Amount").ToString <> "" Then
                    Dim IDR As Decimal = item("IDR_Amount").ToString
                    item("IDR") = IDR.ToString("#,###.00")
                End If
            Next
        End If
        'store.DataSource = objtable
        'store.DataBind()
        GridPanel1.GetStore.DataSource = objtable
        GridPanel1.GetStore.DataBind()

    End Sub

    Protected Sub BtnSearch_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If Session("FirstAddData") = 1 Then
                Session("FirstAddData") = 0
            Else
                ClearTransaction()
            End If

            Dim cif As String = Session("STRMemo_CIFNO")
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate
            Dim maxDate As DateTime = DateTime.Now
            'Update 20210426 Adi Y : Pindah buttons ke Form Panel
            If cif Is Nothing Then
                Throw New ApplicationException("CIF No Tidak boleh kosong")
            ElseIf datefrom <> DateTime.MinValue AndAlso datefrom > maxDate AndAlso dd_TrnFilterBy.SelectedItemValue = "3" Then
                Throw New Exception("Date From : Can not select future date")
            ElseIf dateto <> DateTime.MinValue AndAlso dateto > maxDate AndAlso dd_TrnFilterBy.SelectedItemValue = "3" Then
                Throw New Exception("Date To : Can not select future date")
            ElseIf sar_DateFrom.Text = "1/1/0001 12:00:00 AM" AndAlso dd_TrnFilterBy.SelectedItemValue = "3" Then
                Throw New ApplicationException("Date From : Tidak Boleh Kosong")
            ElseIf sar_DateTo.Text = "1/1/0001 12:00:00 AM" AndAlso dd_TrnFilterBy.SelectedItemValue = "3" Then
                Throw New ApplicationException("Date To : Tidak Boleh Kosong")
            ElseIf datefrom > dateto AndAlso dd_TrnFilterBy.SelectedItemValue = "3" Then
                Throw New Exception("Date To : Harus lebih besar dari Date From")
            End If

            If dd_TrnFilterBy.SelectedItemValue <> "3" Then '' kalau selain Custom Date, dateFrom dan DateTo default now, agar parameter tidak error
                datefrom = DateTime.Now
                dateto = DateTime.Now
            End If

            'Update 20210426 Adi Y : Pindah buttons ke Form Panel
            'ListTransaction = NawaDevBLL.GenerateSarBLL.getListTransactionBySearch(cif, datefrom, dateto)

            Dim param_RadioOption As String = ""

            If dd_TrnFilterBy.SelectedItemValue = "1" Then
                param_RadioOption = "TrnHitByCaseAlert"
            ElseIf dd_TrnFilterBy.SelectedItemValue = "2" Then
                param_RadioOption = "Trn5Year"
            ElseIf dd_TrnFilterBy.SelectedItemValue = "3" Then
                param_RadioOption = "CustomDate"
            Else
                Throw New ApplicationException("Please select one of the filter options")
            End If

            Dim tbl_goAML_odm_Transaksi As New DataTable
            Dim SmCaseID As RowSelectionModel = TryCast(gp_case_alert.GetSelectionModel(), RowSelectionModel)
            Dim SelectedCaseID As RowSelectionModel = gp_case_alert.SelectionModel.Primary
            If SelectedCaseID.SelectedRows.Count = 0 Then

                Throw New Exception("Minimal 1 Case ID harus dipilih")
            End If
            For Each item As SelectedRow In SmCaseID.SelectedRows
                Dim recordID = item.RecordID.ToString
                Dim tbl_goAML_odm_Transaksi_temp As New DataTable
                Dim objGetTrn(3) As SqlParameter
                objGetTrn(0) = New SqlParameter
                objGetTrn(0).ParameterName = "@CaseID"
                objGetTrn(0).Value = recordID

                objGetTrn(1) = New SqlParameter
                objGetTrn(1).ParameterName = "@RadioOption"
                objGetTrn(1).Value = param_RadioOption

                objGetTrn(2) = New SqlParameter
                objGetTrn(2).ParameterName = "@DateFrom"
                objGetTrn(2).Value = datefrom.ToString("yyyy-MM-dd")

                objGetTrn(3) = New SqlParameter
                objGetTrn(3).ParameterName = "@DateTo"
                objGetTrn(3).Value = dateto.ToString("yyyy-MM-dd")

                tbl_goAML_odm_Transaksi_temp = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_OneFCC_GetTransactionOfCaseAlert_STRMemo", objGetTrn)
                tbl_goAML_odm_Transaksi.Merge(tbl_goAML_odm_Transaksi_temp)
            Next



            For Each row As DataRow In tbl_goAML_odm_Transaksi.Rows

                Dim Transaction As NawaDevDAL.goAML_ODM_Transaksi = New NawaDevDAL.goAML_ODM_Transaksi
                'Transaction.Account_NO = row.Item("Account_NO")
                'Transaction.ACCOUNT_No_Lawan = row.Item("ACCOUNT_No_Lawan")
                'Transaction.Active = row.Item("Active")
                'Transaction.Alternateby = row.Item("Alternateby")
                'Transaction.ApprovedBy = row.Item("ApprovedBy")
                'Transaction.ApprovedDate = row.Item("ApprovedDate")
                'Transaction.Authorized = row.Item("Authorized")
                'Transaction.BiMultiParty = row.Item("BiMultiParty")
                'Transaction.Business_date = row.Item("Business_date")
                'Transaction.CIF_NO = row.Item("CIF_NO")
                'Transaction.CIF_No_Lawan = row.Item("CIF_No_Lawan")
                'Transaction.Comments = row.Item("Comments")
                'Transaction.Conductor_ID = row.Item("Conductor_ID")
                'Transaction.Country_Code = row.Item("Country_Code")
                'Transaction.Country_Code_Lawan = row.Item("Country_Code_Lawan")
                'Transaction.CreatedBy = row.Item("CreatedBy")
                'Transaction.CreatedDate = row.Item("CreatedDate")
                'Transaction.Currency = row.Item("Currency")
                'Transaction.Currency_Lawan = row.Item("Currency_Lawan")
                'Transaction.Date_Posting = row.Item("Date_Posting")
                'Transaction.Date_Transaction = row.Item("Date_Transaction")
                'Transaction.Debit_Credit = row.Item("Debit_Credit")
                'Transaction.Exchange_Rate = row.Item("Exchange_Rate")
                'Transaction.From_Funds_Code = row.Item("From_Funds_Code")
                'Transaction.From_Type = row.Item("From_Type")
                'Transaction.GCN = row.Item("GCN")
                'Transaction.GCN_Lawan = row.Item("GCN_Lawan")
                'Transaction.IDR_Amount = row.Item("IDR_Amount")
                'Transaction.LastUpdateBy = row.Item("LastUpdateBy")
                'Transaction.LastUpdateDate = row.Item("LastUpdateDate")
                'Transaction.MsgSwiftType = row.Item("MsgSwiftType")
                'Transaction.NO_ID = row.Item("NO_ID")
                'Transaction.Original_Amount = row.Item("Original_Amount")
                'Transaction.Ref_Num = row.Item("Ref_Num")
                'Transaction.Source_Data = row.Item("Source_Data")
                'Transaction.Swift_Code_Lawan = row.Item("Swift_Code_Lawan")
                'Transaction.Teller = row.Item("Teller")
                'Transaction.To_Funds_Code = row.Item("To_Funds_Code")
                'Transaction.To_Type = row.Item("To_Type")
                'Transaction.Transaction_Code = row.Item("Transaction_Code")
                'Transaction.Transaction_Location = row.Item("Transaction_Location")
                'Transaction.Transaction_Number = row.Item("Transaction_Number")
                'Transaction.Transaction_Remark = row.Item("Transaction_Remark")
                'Transaction.Transmode_Code = row.Item("Transmode_Code")
                'Transaction.Transmode_Comment = row.Item("Transmode_Comment")
                'Transaction.WIC_No = row.Item("WIC_No")
                'Transaction.WIC_No_Lawan = row.Item("WIC_No_Lawan")
                If Not IsDBNull(row.Item("Account_NO")) Then
                    Transaction.Account_NO = row.Item("Account_NO")
                End If
                If Not IsDBNull(row.Item("ACCOUNT_No_Lawan")) Then
                    Transaction.ACCOUNT_No_Lawan = row.Item("ACCOUNT_No_Lawan")
                End If
                If Not IsDBNull(row.Item("Active")) Then
                    Transaction.Active = row.Item("Active")
                End If
                If Not IsDBNull(row.Item("Alternateby")) Then
                    Transaction.Alternateby = row.Item("Alternateby")
                End If
                If Not IsDBNull(row.Item("ApprovedBy")) Then
                    Transaction.ApprovedBy = row.Item("ApprovedBy")
                End If
                If Not IsDBNull(row.Item("ApprovedDate")) Then
                    If row.Item("ApprovedDate") = "1/1/1900 12:00:00 AM" Then '' Edit 06-Jan-2023, Felix. kalau date default, ganti jd nothing
                        Transaction.ApprovedDate = Nothing
                    Else
                        Transaction.ApprovedDate = row.Item("ApprovedDate")
                    End If
                End If
                If Not IsDBNull(row.Item("CreatedBy")) Then
                    Transaction.CreatedBy = row.Item("CreatedBy")
                End If
                If Not IsDBNull(row.Item("CreatedDate")) Then
                    If row.Item("CreatedDate") = "1/1/1900 12:00:00 AM" Then '' Edit 06-Jan-2023, Felix. kalau date default, ganti jd nothing
                        Transaction.CreatedDate = Nothing
                    Else
                        Transaction.CreatedDate = row.Item("CreatedDate")
                    End If
                End If
                If Not IsDBNull(row.Item("LastUpdateBy")) Then
                    Transaction.LastUpdateBy = row.Item("LastUpdateBy")
                End If
                If Not IsDBNull(row.Item("LastUpdateDate")) Then
                    If row.Item("LastUpdateDate") = "1/1/1900 12:00:00 AM" Then '' Edit 06-Jan-2023, Felix. kalau date default, ganti jd nothing
                        Transaction.LastUpdateDate = Nothing
                    Else
                        Transaction.LastUpdateDate = row.Item("LastUpdateDate")
                    End If
                End If
                If Not IsDBNull(row.Item("Authorized")) Then
                    Transaction.Authorized = row.Item("Authorized")
                End If
                If Not IsDBNull(row.Item("BiMultiParty")) Then
                    Transaction.BiMultiParty = row.Item("BiMultiParty")
                End If
                If Not IsDBNull(row.Item("Business_date")) Then
                    If row.Item("Business_date") = "1/1/1900 12:00:00 AM" Then '' Edit 06-Jan-2023, Felix. kalau date default, ganti jd nothing
                        Transaction.Business_date = Nothing
                    Else
                        Transaction.Business_date = row.Item("Business_date")
                    End If
                End If
                If Not IsDBNull(row.Item("CIF_NO")) Then
                    Transaction.CIF_NO = row.Item("CIF_NO")
                End If
                If Not IsDBNull(row.Item("CIF_No_Lawan")) Then
                    Transaction.CIF_No_Lawan = row.Item("CIF_No_Lawan")
                End If
                If Not IsDBNull(row.Item("Comments")) Then
                    Transaction.Conductor_ID = row.Item("Comments")
                End If
                If Not IsDBNull(row.Item("Conductor_ID")) Then
                    Transaction.Conductor_ID = row.Item("Conductor_ID")
                End If
                If Not IsDBNull(row.Item("Country_Code")) Then
                    Transaction.Country_Code = row.Item("Country_Code")
                End If
                If Not IsDBNull(row.Item("Country_Code_Lawan")) Then
                    Transaction.Country_Code_Lawan = row.Item("Country_Code_Lawan")
                End If
                If Not IsDBNull(row.Item("Currency")) Then
                    Transaction.Currency = row.Item("Currency")
                End If
                If Not IsDBNull(row.Item("Currency_Lawan")) Then
                    Transaction.Currency_Lawan = row.Item("Currency_Lawan")
                End If
                If Not IsDBNull(row.Item("Date_Posting")) Then
                    If row.Item("Date_Posting") = "1/1/1900 12:00:00 AM" Then '' Edit 06-Jan-2023, Felix. kalau date default, ganti jd nothing
                        Transaction.Date_Posting = Nothing
                    Else
                        Transaction.Date_Posting = row.Item("Date_Posting")
                    End If
                End If
                If Not IsDBNull(row.Item("Debit_Credit")) Then
                    Transaction.Debit_Credit = row.Item("Debit_Credit")
                End If
                If Not IsDBNull(row.Item("Date_Transaction")) Then
                    Transaction.Date_Transaction = row.Item("Date_Transaction")
                End If
                If Not IsDBNull(row.Item("Exchange_Rate")) Then
                    Transaction.Exchange_Rate = row.Item("Exchange_Rate")
                End If
                If Not IsDBNull(row.Item("From_Funds_Code")) Then
                    Transaction.From_Funds_Code = row.Item("From_Funds_Code")
                End If
                If Not IsDBNull(row.Item("From_Type")) Then
                    Transaction.From_Type = row.Item("From_Type")
                End If
                If Not IsDBNull(row.Item("GCN")) Then
                    Transaction.GCN = row.Item("GCN")
                End If
                If Not IsDBNull(row.Item("GCN_Lawan")) Then
                    Transaction.GCN_Lawan = row.Item("GCN_Lawan")
                End If
                If Not IsDBNull(row.Item("IDR_Amount")) Then
                    Transaction.IDR_Amount = row.Item("IDR_Amount")
                End If
                'If Not IsDBNull(row.Item("MsgSwiftType")) Then
                '    Transaction.MsgSwiftType = row.Item("MsgSwiftType")
                'End If
                If Not IsDBNull(row.Item("NO_ID")) Then
                    Transaction.NO_ID = row.Item("NO_ID")
                End If
                If Not IsDBNull(row.Item("Original_Amount")) Then
                    Transaction.Original_Amount = row.Item("Original_Amount")
                End If
                If Not IsDBNull(row.Item("Ref_Num")) Then
                    Transaction.Ref_Num = row.Item("Ref_Num")
                End If
                If Not IsDBNull(row.Item("Source_Data")) Then
                    Transaction.Source_Data = row.Item("Source_Data")
                End If
                If Not IsDBNull(row.Item("Swift_Code_Lawan")) Then
                    Transaction.Swift_Code_Lawan = row.Item("Swift_Code_Lawan")
                End If
                If Not IsDBNull(row.Item("Teller")) Then
                    Transaction.Teller = row.Item("Teller")
                End If
                If Not IsDBNull(row.Item("To_Funds_Code")) Then
                    Transaction.To_Funds_Code = row.Item("To_Funds_Code")
                End If
                If Not IsDBNull(row.Item("To_Type")) Then
                    Transaction.To_Type = row.Item("To_Type")
                End If
                If Not IsDBNull(row.Item("Transaction_Code")) Then
                    Transaction.Transaction_Code = row.Item("Transaction_Code")
                End If
                If Not IsDBNull(row.Item("Transaction_Location")) Then
                    Transaction.Transaction_Location = row.Item("Transaction_Location")
                End If
                If Not IsDBNull(row.Item("Transaction_Number")) Then
                    Transaction.Transaction_Number = row.Item("Transaction_Number")
                End If
                If Not IsDBNull(row.Item("Transaction_Remark")) Then
                    Transaction.Transaction_Remark = row.Item("Transaction_Remark")
                End If
                If Not IsDBNull(row.Item("Transmode_Code")) Then
                    Transaction.Transmode_Code = row.Item("Transmode_Code")
                End If
                If Not IsDBNull(row.Item("Transmode_Comment")) Then
                    Transaction.Transmode_Comment = row.Item("Transmode_Comment")
                End If
                If Not IsDBNull(row.Item("WIC_No")) Then
                    Transaction.WIC_No = row.Item("WIC_No")
                End If
                If Not IsDBNull(row.Item("WIC_No_Lawan")) Then
                    Transaction.WIC_No_Lawan = row.Item("WIC_No_Lawan")
                End If

                Dim CekTransaction As Integer = 0
                CekTransaction = ListTransaction.FindAll(Function(item As goAML_ODM_Transaksi) item.Transaction_Number = Transaction.Transaction_Number).Count
                If CekTransaction = 0 Then
                    ListTransaction.Add(Transaction)
                End If

            Next

            If tbl_goAML_odm_Transaksi.Rows.Count > 0 Then

                bindTransactionDataTable(StoreTransaction, tbl_goAML_odm_Transaksi)
                bindTransactionNoCheckedDataTable(StoreTransaksiNoChecked, tbl_goAML_odm_Transaksi)
            End If
            If tbl_goAML_odm_Transaksi.Rows.Count = 0 Then
                Ext.Net.X.Msg.Alert("Message", "Tidak Ada Transaksi").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Private Shared Sub DataSetToExcel(ByVal dataSet As DataSet, ByVal objfileinfo As FileInfo)
        Using pck As ExcelPackage = New ExcelPackage()

            For Each dataTable As DataTable In dataSet.Tables
                Dim workSheet As ExcelWorksheet = pck.Workbook.Worksheets.Add(dataTable.TableName)
                workSheet.Cells("A1").LoadFromDataTable(dataTable, True)

                Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                Dim intcolnumber As Integer = 1
                For Each item As System.Data.DataColumn In dataTable.Columns
                    If item.DataType = GetType(Date) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = dateformat
                    End If
                    If item.DataType = GetType(Integer) Or item.DataType = GetType(Double) Or item.DataType = GetType(Long) Or item.DataType = GetType(Decimal) Or item.DataType = GetType(Single) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = "#,##0.00"
                    End If

                    intcolnumber = intcolnumber + 1
                Next
                workSheet.Cells(workSheet.Dimension.Address).AutoFitColumns()
            Next

            pck.SaveAs(objfileinfo)
        End Using
    End Sub

    Protected Sub CreateExcel2007WithData(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try


            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            'Dim intTotalRow As Long
            Dim objSchemaModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(12226)
            Dim objSchemaModuleField As List(Of NawaDAL.ModuleField) = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(12226)
            Dim objSchemaModuleFieldDefault As New List(Of NawaDAL.ModuleFieldDefault)
            Dim objmdl As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(12226)

            Dim cif As String = Session("STRMemo_CIFNO")
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate

            Dim objList = NawaDevBLL.GenerateSarBLL.getListTransactionBySearch(cif, datefrom, dateto)



            objFormModuleView = New FormModuleView(GridPaneldetail, BtnExport)
            objFormModuleView.ModuleID = objmdl.PK_Module_ID
            objFormModuleView.ModuleName = objmdl.ModuleName

            Dim delimitercheckboxparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(39)
            Dim strdelimiter As String = ";"
            If Not delimitercheckboxparam Is Nothing Then
                strdelimiter = delimitercheckboxparam.SettingValue
            End If

            Dim objtb As DataTable = NawaBLL.Common.CopyGenericToDataTable(ListTransaction)
            Dim intmaxrow As Integer
            If objtb.Rows.Count + 1000 > 1048575 Then
                intmaxrow = 1048575
            Else
                intmaxrow = objtb.Rows.Count + 1000
            End If


            Using objtbl As Data.DataTable = objtb
                'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                objtbl.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                Dim intTotalPage As Integer = (intmaxrow + 1048575 - 1) / 1048575
                Dim dsTable As New Data.DataSet

                If intTotalPage = 1 Then
                    'objFormModuleView.changeHeader(objtbl)

                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    DataSetToExcel(dsTable, objfileinfo)

                    dsTable.Dispose()
                    dsTable = Nothing



                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If



                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")



                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()
                ElseIf intTotalPage > 1 Then
                    'objFormModuleView.changeHeader(objtbl)



                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    'ambil page 2 dst sampe page terakhir
                    For index = 1 To intTotalPage - 2
                        Using objtbltemp As Data.DataTable = objtb
                            objtbltemp.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                            dsTable.Tables.Add(objtbltemp.Copy)
                        End Using
                    Next
                    DataSetToExcel(dsTable, objfileinfo)
                    dsTable.Dispose()
                    dsTable = Nothing

                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If
                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()

                End If
            End Using

        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub
    Protected Sub CreateExcel2007WithDataNoChecked(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try


            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            'Dim intTotalRow As Long
            Dim objSchemaModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(12226)
            Dim objSchemaModuleField As List(Of NawaDAL.ModuleField) = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(12226)
            Dim objSchemaModuleFieldDefault As New List(Of NawaDAL.ModuleFieldDefault)
            Dim objmdl As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(12226)

            Dim cif As String = Session("STRMemo_CIFNO")
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate

            Dim objList = NawaDevBLL.GenerateSarBLL.getListTransactionBySearch(cif, datefrom, dateto)



            objFormModuleView = New FormModuleView(GridPanel1, BtnExportNoChecked)
            objFormModuleView.ModuleID = objmdl.PK_Module_ID
            objFormModuleView.ModuleName = objmdl.ModuleName

            Dim delimitercheckboxparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(39)
            Dim strdelimiter As String = ";"
            If Not delimitercheckboxparam Is Nothing Then
                strdelimiter = delimitercheckboxparam.SettingValue
            End If

            Dim objtb As DataTable = NawaBLL.Common.CopyGenericToDataTable(ListTransaction)
            Dim intmaxrow As Integer
            If objtb.Rows.Count + 1000 > 1048575 Then
                intmaxrow = 1048575
            Else
                intmaxrow = objtb.Rows.Count + 1000
            End If


            Using objtbl As Data.DataTable = objtb
                'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                objtbl.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                Dim intTotalPage As Integer = (intmaxrow + 1048575 - 1) / 1048575
                Dim dsTable As New Data.DataSet

                If intTotalPage = 1 Then
                    'objFormModuleView.changeHeader(objtbl)

                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    DataSetToExcel(dsTable, objfileinfo)

                    dsTable.Dispose()
                    dsTable = Nothing



                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If



                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")



                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()
                ElseIf intTotalPage > 1 Then
                    'objFormModuleView.changeHeader(objtbl)



                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    'ambil page 2 dst sampe page terakhir
                    For index = 1 To intTotalPage - 2
                        Using objtbltemp As Data.DataTable = objtb
                            objtbltemp.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                            dsTable.Tables.Add(objtbltemp.Copy)
                        End Using
                    Next
                    DataSetToExcel(dsTable, objfileinfo)
                    dsTable.Dispose()
                    dsTable = Nothing

                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If
                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()

                End If
            End Using

        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub

    Protected Sub OnSelect_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim data As New goAML_ODM_Transaksi
            Dim idx As String
            idx = e.ExtraParams(0).Value

            data.NO_ID = e.ExtraParams(0).Value

            ListSelectedTransaction.Add(data)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub OnDeSelect_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim data As New goAML_ODM_Transaksi

            For Each item In ListSelectedTransaction
                If item.NO_ID = CInt(e.ExtraParams(0).Value) Then
                    data = item
                    Exit For
                End If
            Next

            ListSelectedTransaction.Remove(data)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub sar_jenisLaporan_DirectSelect(sender As Object, e As DirectEventArgs)
        Try
            'If sar_jenisLaporan.SelectedItem.Value = "LTKMT" Or sar_jenisLaporan.SelectedItem.Value = "LAPT" Or sar_jenisLaporan.SelectedItem.Value = "LTKM" Then
            If sar_jenisLaporan.SelectedItem.Value IsNot Nothing Then
                'PanelReportIndicator.Hidden = False
            Else
                'PanelReportIndicator.Hidden = True
            End If
            If sar_jenisLaporan.SelectedItem.Value = "LTKMP" Then
                NoRefPPATK.Hidden = False
            Else
                NoRefPPATK.Hidden = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Property obj_Attachment_Edit() As OneFCC_CaseManagement_STRMemo_Attachment
        Get
            Return Session("STRMemo_Add.obj_Attachment_Edit")
        End Get
        Set(ByVal value As OneFCC_CaseManagement_STRMemo_Attachment)
            Session("STRMemo_Add.obj_Attachment_Edit") = value
        End Set
    End Property

    Public Property obj_ListAttachment_Edit() As List(Of OneFCC_CaseManagement_STRMemo_Attachment)
        Get
            Return Session("STRMemo_Add.obj_ListAttachment_Edit")
        End Get
        Set(ByVal value As List(Of OneFCC_CaseManagement_STRMemo_Attachment))
            Session("STRMemo_Add.obj_ListAttachment_Edit") = value
        End Set
    End Property

    Protected Sub btn_addAttachment_click(sender As Object, e As DirectEventArgs)
        Try
            obj_Attachment_Edit = Nothing
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()
            WindowAttachment.Hidden = False
            FileDoc.Hidden = False
            BtnsaveAttachment.Hidden = False
            txtKeterangan.Editable = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                obj_ListAttachment_Edit.Remove(obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = ID).FirstOrDefault)
                bindAttachment(StoreAttachment, obj_ListAttachment_Edit)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                editDokumen(ID, "Edit")
                IDDokumen = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editDokumen(ID, "Detail")
            ElseIf e.ExtraParams(1).Value = "Download" Then
                DownloadFile(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim status As Boolean = False
            Dim DokumenOld As New CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment
            Dim Dokumen As New CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment
            If IDDokumen IsNot Nothing Or IDDokumen <> "" Then
                status = True
                DokumenOld = obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = IDDokumen).FirstOrDefault
                obj_ListAttachment_Edit.Remove(obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = IDDokumen).FirstOrDefault)
            ElseIf Not FileDoc.HasFile Then
                Throw New Exception("Please Upload File")
            End If

            Dim docName As String = IO.Path.GetFileName(FileDoc.FileName)
            If obj_ListAttachment_Edit IsNot Nothing Then
                For Each item In obj_ListAttachment_Edit
                    If item.File_Name = docName Then
                        Throw New ApplicationException(docName + " Sudah Ada")
                    End If
                Next
            Else
                obj_ListAttachment_Edit = New List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment)
            End If


            If obj_ListAttachment_Edit.Count = 0 Then
                Dokumen.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = -1
            ElseIf obj_ListAttachment_Edit.Count >= 1 Then
                Dokumen.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = obj_ListAttachment_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID) - 1
            End If

            If status = True And Not FileDoc.HasFile Then
                Dokumen.File_Name = DokumenOld.File_Name
                Dokumen.File_Doc = DokumenOld.File_Doc
            Else
                Dokumen.File_Name = IO.Path.GetFileName(FileDoc.FileName)
                Dokumen.File_Doc = FileDoc.FileBytes
            End If
            Dokumen.Remarks = txtKeterangan.Text.Trim

            Dokumen.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            Dokumen.CreatedDate = DateTime.Now

            obj_ListAttachment_Edit.Add(Dokumen)

            bindAttachment(StoreAttachment, obj_ListAttachment_Edit)
            WindowAttachment.Hidden = True
            IDDokumen = Nothing

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAttachment.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub bindAttachment(store As Ext.Net.Store, listAttachment As List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAttachment)
        objtable.Columns.Add(New DataColumn("FileName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("FileName") = item("File_Name")
                item("Keterangan") = item("Remarks")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub editDokumen(id As String, command As String)
        Try
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()

            WindowAttachment.Hidden = False
            Dim Dokumen As New CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment
            Dokumen = obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = id).FirstOrDefault

            txtFileName.Text = Dokumen.File_Name
            txtKeterangan.Text = Dokumen.Remarks

            If command = "Detail" Then
                FileDoc.Hidden = True
                BtnsaveAttachment.Hidden = True
                txtKeterangan.Editable = False
            Else
                FileDoc.Hidden = False
                BtnsaveAttachment.Hidden = False
                txtKeterangan.Editable = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DownloadFile(id As Long)
        Dim objdownload As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment = obj_ListAttachment_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = id)
        If Not objdownload Is Nothing Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & objdownload.File_Name)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            'Response.ContentType = "ContentType"
            Response.ContentType = MimeMapping.GetMimeMapping(objdownload.File_Name)
            Response.BinaryWrite(objdownload.File_Doc)
            Response.End()
        End If
    End Sub

    Protected Sub ReportIndicator_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goaml_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub JenisLaporan_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Jenis_Laporan_STR", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub sar_IsSelectedAll_CheckedChanged(sender As Object, e As EventArgs) Handles sar_IsSelectedAll.DirectCheck
        If sar_IsSelectedAll.Value Then
            PanelChecked.Hide()
            PanelnoCheked.Show()
        Else
            PanelChecked.Show()
            PanelnoCheked.Hide()
        End If
    End Sub
#End Region
    Protected Sub cmb_Cifno_OnValueChanged(sender As Object, e As EventArgs)
        Try
            Session("STRMemo_CIFNO") = cmb_Cifno.SelectedItemValue

        Catch ex As Exception

        End Try
    End Sub




End Class
