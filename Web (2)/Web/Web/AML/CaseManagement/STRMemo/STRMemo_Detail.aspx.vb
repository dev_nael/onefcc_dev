﻿Imports NawaDevDAL
Imports NawaDevBLL
Imports CasemanagementDAL
Imports CasemanagementBLL
Imports System.Data
Imports System.Data.SqlClient
Imports NawaBLL
Imports OfficeOpenXml
Imports System.IO
Imports System.Globalization

Partial Class AML_CaseManagement_STRMemo_STRMemo_Detail
    Inherits ParentPage
    Public objFormModuleAdd As NawaBLL.FormModuleAdd
    Public objFormModuleView As FormModuleView

    Public Property IDUnik() As Long
        Get
            Return Session("STRMemo_Detail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("STRMemo_Detail.IDUnik") = value
        End Set
    End Property
    Public Property objSTRMemoClass As STRMemo_BLL.STRMemoClass
        Get
            If Session("STRMemo_Detail.objSTRMemoClass") Is Nothing Then
                Session("STRMemo_Detail.objSTRMemoClass") = New STRMemo_BLL.STRMemoClass
            End If

            Return Session("STRMemo_Detail.objNewsClass")
        End Get
        Set(value As STRMemo_BLL.STRMemoClass)
            Session("STRMemo_Detail.objNewsClass") = value
        End Set
    End Property
    'Public Property ObjModule() As NawaDAL.Module
    '    Get
    '        Return Session("STRMemo_Detail.ObjModule")
    '    End Get
    '    Set(ByVal value As NawaDAL.Module)
    '        Session("STRMemo_Detail.ObjModule") = value
    '    End Set
    'End Property

    Sub SetCommandColumnLocation()
        'dim settingvaluestring as String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select SettingValue FROM systemParameter where PK_SystemParameter_ID = '32'", Nothing)
        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim buttonPosition As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            buttonPosition = objParamSettingbutton.SettingValue
        End If
        'buttonPosition = settingvaluestring

        'Report Level

        ColumnActionLocation(gp_indikator, cc_indikator, buttonPosition)
        ColumnActionLocation(gp_Pihak_Terkait, cc_pihakterkait, buttonPosition)
        ColumnActionLocation(gp_TIndakLanjut, cc_tindaklanjut, buttonPosition)

    End Sub

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        If buttonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub


    Private Sub STRMemo_Detail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()
                PanelChecked.Hide()
                PanelnoCheked.Show()
                SetCommandColumnLocation()
                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If
                LoadData()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ClearSession()
        'objEmployee = Nothing
        'ObjModule = Nothing
        objSTRMemoClass = New STRMemo_BLL.STRMemoClass
        obj_ListIndikator_Edit = Nothing
        obj_ListPihakTerkait_Edit = Nothing
        obj_ListTindakLanjut_Edit = Nothing
    End Sub



    Private Sub LoadData()
        Try
            displayPK.ReadOnly = True
            displayPK.Text = True
            txtCIF.ReadOnly = True
            txtNo.ReadOnly = True
            txtKepada.ReadOnly = True
            txtDari.ReadOnly = True
            txtTanggal.ReadOnly = True
            txtPerihal.ReadOnly = True
            txtSumberPelaporan.ReadOnly = True
            txtNamaPihakPelapor.ReadOnly = True
            txtKategoriPihakPelapor.ReadOnly = True
            txtUnitBisnis.ReadOnly = True
            txtDirektor.ReadOnly = True
            txtCabang.ReadOnly = True
            txtLamaMenjadiNasabah.ReadOnly = True
            txtPekerjaanBidangUsaha.ReadOnly = True
            txtPengahasilanTahun.ReadOnly = True
            txtProfilLainnya.ReadOnly = True
            txtHasilAnalisis.ReadOnly = True
            txtKesimpulan.ReadOnly = True
            cmb_KesimpulanDetail.IsReadOnly = True
            LblLatarBelakang.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblPihakCalonTerkait.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblPihakTerkait.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblHasilAnalisis.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblKesimpulan.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblTindakLanjut.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblTransaksiSTR.StyleSpec = "font-weight: bold;font-size : 20px;"
            LblWorkflowHistory.StyleSpec = "font-weight: bold;font-size : 20px;"
            objSTRMemoClass = CasemanagementBLL.STRMemo_BLL.getSTRMemoClassByID(IDUnik)
            '' Edit 5 Oct 2022
            'Using objdb As New CasemanagementEntities
            '    Dim listHistory As List(Of OneFCC_CaseManagement_WorkflowHistory_STRMemo) = objdb.OneFCC_CaseManagement_WorkflowHistory_STRMemo.Where(Function(x) x.FK_OneFCC_CaseManagement_STRMemo_ID = IDUnik).ToList
            '    If listHistory.Count > 0 Then
            '        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listHistory)
            '        gp_workflowhistory.GetStore().DataSource = objtable
            '        gp_workflowhistory.GetStore().DataBind()
            '    End If
            'End Using

            Dim listHistory As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_WorkflowHistory_STRMemo where FK_OneFCC_CaseManagement_STRMemo_ID = " & IDUnik & "", Nothing)
            'NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select Attachment, AttachmentName FROM OneFCC_CaseManagement_WorkflowHistory where PK_CaseManagement_WorkflowHistory_ID = " & ID & "", Nothing)
            If listHistory IsNot Nothing Then
                gp_workflowhistory.GetStore().DataSource = listHistory
                gp_workflowhistory.GetStore().DataBind()
            End If

            If objSTRMemoClass.listCaseID.Count > 0 Then
                Dim objtable As New Data.DataTable

                '' Edit 5-Oct-2022 Felix 
                'Using objdb As New CasemanagementEntities
                '    Dim listCaseID As New List(Of OneFCC_CaseManagement)
                '    Dim listCaseIDSTR As List(Of OneFCC_CaseManagement_STRMemo_CaseID) = objdb.OneFCC_CaseManagement_STRMemo_CaseID.Where(Function(x) x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID).ToList
                '    Dim objtableTemp As New Data.DataTable
                '    For Each item In objSTRMemoClass.listCaseID

                '        Dim CaseID As OneFCC_CaseManagement = objdb.OneFCC_CaseManagement.Where(Function(x) x.PK_CaseManagement_ID = item.CaseID).FirstOrDefault
                '        listCaseID.Add(CaseID)

                '    Next
                '    For Each item In listCaseID
                '        For Each item2 In listCaseIDSTR
                '            If item.PK_CaseManagement_ID = item2.CaseID Then
                '                item.FK_OneFCC_CaseManagement_STRMemo_ID = item2.FK_OneFCC_CaseManagement_STRMemo_ID
                '            End If
                '        Next
                '    Next
                '    objtable = NawaBLL.Common.CopyGenericToDataTable(listCaseID)
                '    objtable.Columns.Add(New DataColumn("Rule", GetType(String)))
                '    For Each item As Data.DataRow In objtable.Rows
                '        If Not IsDBNull(item("FK_Rule_Basic_ID")) Then
                '            'Dim RuleBasic As New OneFCC_MS_Rule_Basic
                '            'Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                '            'RuleBasic = objdb.OneFCC_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault
                '            'item("Rule") = RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                '        End If

                '        If IsDBNull(item("FK_OneFCC_CaseManagement_STRMemo_ID")) Then
                '            item("Memo_No") = "0 - New"
                '        Else
                '            Dim StrMemo As New OneFCC_CaseManagement_STRMemo
                '            Dim FkSTRMemo As String = item("FK_OneFCC_CaseManagement_STRMemo_ID").ToString
                '            StrMemo = objdb.OneFCC_CaseManagement_STRMemo.Where(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_ID = FkSTRMemo).FirstOrDefault
                '            If StrMemo.StatusID = "1" Then
                '                item("Memo_No") = "1 - On Progress"
                '            ElseIf StrMemo.StatusID = "2" Then
                '                item("Memo_No") = "2 - Approved"
                '            ElseIf StrMemo.StatusID = "3" Then
                '                item("Memo_No") = "3 - Rejected"
                '            ElseIf StrMemo.StatusID = "4" Then
                '                item("Memo_No") = "4 - Generate"
                '            ElseIf StrMemo.StatusID = "5" Then
                '                item("Memo_No") = "5 - On Progress Generate Report"
                '            End If
                '        End If
                '    Next
                '    gp_case_alert.GetStore().DataSource = objtable
                '    gp_case_alert.GetStore().DataBind()
                'End Using


                Dim listCaseID As List(Of OneFCC_CaseManagement) = New List(Of OneFCC_CaseManagement)
                Dim listCaseIDSTR As New DataTable '' List(Of OneFCC_CaseManagement_STRMemo_CaseID) = 
                listCaseIDSTR = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_STRMemo_CaseID where FK_OneFCC_CaseManagement_STRMemo_ID = " & objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID)

                Dim objtableTemp As New Data.DataTable
                For Each item In objSTRMemoClass.listCaseID

                    Dim drCaseID As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select * FROM OneFCC_CaseManagement where PK_CaseManagement_ID = " & item.CaseID)
                    Dim CaseID As OneFCC_CaseManagement = New OneFCC_CaseManagement()
                    If drCaseID IsNot Nothing Then
                        If Not IsDBNull(drCaseID("PK_CaseManagement_ID")) Then
                            CaseID.PK_CaseManagement_ID = drCaseID("PK_CaseManagement_ID")
                        End If

                        If Not IsDBNull(drCaseID("Alert_Type")) Then
                            CaseID.Alert_Type = drCaseID("Alert_Type")
                        End If

                        If Not IsDBNull(drCaseID("FK_Rule_Basic_ID")) Then
                            CaseID.FK_Rule_Basic_ID = drCaseID("FK_Rule_Basic_ID")
                        End If

                        If Not IsDBNull(drCaseID("Case_Description")) Then
                            CaseID.Case_Description = drCaseID("Case_Description")
                        End If

                        If Not IsDBNull(drCaseID("Is_Customer")) Then
                            CaseID.Is_Customer = drCaseID("Is_Customer")
                        End If

                        If Not IsDBNull(drCaseID("CIF_No")) Then
                            CaseID.CIF_No = drCaseID("CIF_No")
                        End If

                        If Not IsDBNull(drCaseID("Account_No")) Then
                            CaseID.Account_No = drCaseID("Account_No")
                        End If

                        If Not IsDBNull(drCaseID("Customer_Name")) Then
                            CaseID.Customer_Name = drCaseID("Customer_Name")
                        End If

                        If Not IsDBNull(drCaseID("WIC_No")) Then
                            CaseID.WIC_No = drCaseID("WIC_No")
                        End If

                        If Not IsDBNull(drCaseID("WIC_Name")) Then
                            CaseID.WIC_Name = drCaseID("WIC_Name")
                        End If

                        If Not IsDBNull(drCaseID("FK_Proposed_Status_ID")) Then
                            CaseID.FK_Proposed_Status_ID = drCaseID("FK_Proposed_Status_ID")
                        End If

                        If Not IsDBNull(drCaseID("Proposed_By")) Then
                            CaseID.Proposed_By = drCaseID("Proposed_By")
                        End If

                        If Not IsDBNull(drCaseID("FK_CaseStatus_ID")) Then
                            CaseID.FK_CaseStatus_ID = drCaseID("FK_CaseStatus_ID")
                        End If

                        If Not IsDBNull(drCaseID("FK_CaseManagement_Workflow_ID")) Then
                            CaseID.FK_CaseManagement_Workflow_ID = drCaseID("FK_CaseManagement_Workflow_ID")
                        End If

                        If Not IsDBNull(drCaseID("Workflow_Step")) Then
                            CaseID.Workflow_Step = drCaseID("Workflow_Step")
                        End If

                        If Not IsDBNull(drCaseID("PIC")) Then
                            CaseID.PIC = drCaseID("PIC")
                        End If

                        If Not IsDBNull(drCaseID("Aging")) Then
                            CaseID.Aging = drCaseID("Aging")
                        End If

                        If Not IsDBNull(drCaseID("StatusRFI")) Then
                            CaseID.StatusRFI = drCaseID("StatusRFI")
                        End If

                        If Not IsDBNull(drCaseID("StatusSTRMemo")) Then
                            CaseID.StatusSTRMemo = drCaseID("StatusSTRMemo")
                        End If

                        If Not IsDBNull(drCaseID("ProcessDate")) Then
                            CaseID.ProcessDate = drCaseID("ProcessDate")
                        End If

                        If Not IsDBNull(drCaseID("FK_Report_ID")) Then
                            CaseID.FK_Report_ID = drCaseID("FK_Report_ID")
                        End If

                        If Not IsDBNull(drCaseID("Active")) Then
                            CaseID.Active = drCaseID("Active")
                        End If

                        If Not IsDBNull(drCaseID("CreatedBy")) Then
                            CaseID.CreatedBy = drCaseID("CreatedBy")
                        End If

                        If Not IsDBNull(drCaseID("LastUpdateBy")) Then
                            CaseID.LastUpdateBy = drCaseID("LastUpdateBy")
                        End If

                        If Not IsDBNull(drCaseID("ApprovedBy")) Then
                            CaseID.ApprovedBy = drCaseID("ApprovedBy")
                        End If

                        If Not IsDBNull(drCaseID("CreatedDate")) Then
                            CaseID.CreatedDate = drCaseID("CreatedDate")
                        End If

                        If Not IsDBNull(drCaseID("LastUpdateDate")) Then
                            CaseID.LastUpdateDate = drCaseID("LastUpdateDate")
                        End If

                        If Not IsDBNull(drCaseID("ApprovedDate")) Then
                            CaseID.ApprovedDate = drCaseID("ApprovedDate")
                        End If

                        If Not IsDBNull(drCaseID("Alternateby")) Then
                            CaseID.Alternateby = drCaseID("Alternateby")
                        End If

                        'If Not IsDBNull(drCaseID("Rule")) Then
                        '    CaseID.Account_No = drCaseID("Rule")
                        'End If
                        If Not IsDBNull(drCaseID("FK_OneFCC_CaseManagement_STRMemo_ID")) Then
                            CaseID.FK_OneFCC_CaseManagement_STRMemo_ID = drCaseID("FK_OneFCC_CaseManagement_STRMemo_ID")
                        End If
                        If Not IsDBNull(drCaseID("Memo_No")) Then
                            CaseID.Memo_No = drCaseID("Memo_No")
                        End If
                    End If

                    listCaseID.Add(CaseID)

                Next
                For Each item In listCaseID
                    For Each item2 As DataRow In listCaseIDSTR.Rows
                        If item.PK_CaseManagement_ID = item2("CaseID") Then
                            item.FK_OneFCC_CaseManagement_STRMemo_ID = item2("FK_OneFCC_CaseManagement_STRMemo_ID").ToString
                        End If
                    Next
                Next
                objtable = NawaBLL.Common.CopyGenericToDataTable(listCaseID)
                objtable.Columns.Add(New DataColumn("Rule", GetType(String)))
                objtable.Columns.Add(New DataColumn("UniqueCMID", GetType(String))) '' Add 7-Mar-23, Ari. FAMA tambah 1 field baru untuk unique cm id
                For Each item As Data.DataRow In objtable.Rows
                    If Not IsDBNull(item("FK_Rule_Basic_ID")) Then
                        '' Edit 5-Oct-2022 Felix
                        'Dim RuleBasic As New OneFCC_MS_Rule_Basic
                        'Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                        'RuleBasic = objdb.OneFCC_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault
                        'item("Rule") = RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                        'Dim RuleBasic As New OneFcc_MS_Rule_Basic
                        Dim FKRule As String = item("FK_Rule_Basic_ID").ToString
                        'RuleBasic = objdb.OneFcc_MS_Rule_Basic.Where(Function(x) x.PK_Rule_Basic_ID = FKRule).FirstOrDefault '' Edit 5 Oct 2022
                        Dim RuleBasic As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select cast (PK_Rule_Basic_ID as varchar(100)) + ' - ' + Rule_Basic_Name from OneFcc_MS_Rule_Basic where PK_Rule_Basic_ID = " & FKRule & "", Nothing)
                        item("Rule") = RuleBasic ' RuleBasic.PK_Rule_Basic_ID & " - " & RuleBasic.Rule_Basic_Name
                        '' End 5-Oct-2022
                    End If
                    '' Add 7-Mar-23, Ari. FAMA tambah 1 field baru untuk unique cm id
                    If item("PK_CaseManagement_ID") > 0 Then
                        Dim PKCM As Long = Convert.ToInt64(item("PK_CaseManagement_ID"))
                        Dim UniqueCMDID As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select Unique_CM_ID from OneFCC_CaseManagement where PK_CaseManagement_ID = " & PKCM, Nothing).ToString
                        item("UniqueCMID") = UniqueCMDID
                    End If

                    If IsDBNull(item("FK_OneFCC_CaseManagement_STRMemo_ID")) Then
                        item("Memo_No") = "0 - New"
                    Else
                        Dim StrMemo As New OneFCC_CaseManagement_STRMemo
                        Dim FkSTRMemo As String = item("FK_OneFCC_CaseManagement_STRMemo_ID").ToString
                        'StrMemo = objdb.OneFCC_CaseManagement_STRMemo.Where(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_ID = FkSTRMemo).FirstOrDefault
                        Dim drStrMemo As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select StatusID from OneFCC_CaseManagement_STRMemo where PK_OneFCC_CaseManagement_STRMemo_ID = " & FkSTRMemo, Nothing)

                        If drStrMemo IsNot Nothing Then
                            If drStrMemo("StatusID") = "1" Then
                                item("Memo_No") = "1 - On Progress"
                            ElseIf drStrMemo("StatusID") = "2" Then
                                item("Memo_No") = "2 - Approved"
                            ElseIf drStrMemo("StatusID") = "3" Then
                                item("Memo_No") = "3 - Rejected"
                            ElseIf drStrMemo("StatusID") = "4" Then
                                item("Memo_No") = "4 - Generate"
                            ElseIf drStrMemo("StatusID") = "5" Then
                                item("Memo_No") = "5 - On Progress Generate Report"
                            End If
                        End If


                    End If
                Next
                gp_case_alert.GetStore().DataSource = objtable
                gp_case_alert.GetStore().DataBind()
                '' End 5 Oct 2022

            End If
            If objSTRMemoClass.listIndikator.Count > 0 Then
                obj_ListIndikator_Edit = objSTRMemoClass.listIndikator
                Bind_Indikator()
            End If
            If objSTRMemoClass.listPihakTerkait.Count > 0 Then
                obj_ListPihakTerkait_Edit = objSTRMemoClass.listPihakTerkait
                Bind_Pihak_Terkait()

            End If
            If objSTRMemoClass.listTindakLanjut.Count > 0 Then
                obj_ListTindakLanjut_Edit = objSTRMemoClass.listTindakLanjut
                Bind_TindakLanjut()
            End If
            If objSTRMemoClass.listAttachment.Count > 0 Then
                obj_ListAttachment_Edit = objSTRMemoClass.listAttachment
                bindAttachment(StoreAttachment, obj_ListAttachment_Edit)
            End If
            If objSTRMemoClass.listTransaction.Count > 0 Then
                Dim transactiontable As OneFCC_CaseManagement_STRMemo_Transaction = objSTRMemoClass.listTransaction.Where(Function(x) x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID).FirstOrDefault
                Dim datefrom As DateTime
                Dim dateto As DateTime
                Dim param_RadioOption As String = ""
                Dim noid As String = ""
                With transactiontable
                    TanggalLaporan.Value = .Tanggal_Laporan
                    sar_jenisLaporan.Value = .Jenis_Laporan
                    alasan.Value = .Alasan
                    If .Jenis_Laporan = "LTKMP" Then
                        NoRefPPATK.Value = .NoRefPPATK
                        NoRefPPATK.Hidden = False
                    Else
                        NoRefPPATK.Value = Nothing
                        NoRefPPATK.Hidden = True
                    End If
                    If .Date_Form IsNot Nothing Then
                        datefrom = .Date_Form
                        dateto = .Date_To
                    Else
                        datefrom = DateTime.Now
                        dateto = DateTime.Now
                    End If

                    If .FilterTransaction IsNot Nothing Then
                        If .FilterTransaction = "TrnHitByCaseAlert" Then
                            Dim Keterangan As String = "Transactions Hit By Case Alert"
                            Dim StrQueryCode As String = "select kode from vw_OneFCC_TransactionFilter where kode = 1"
                            Dim Kode As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                            dd_TrnFilterBy.SetTextWithTextValue(Kode, Keterangan)
                        ElseIf .FilterTransaction = "Trn5Year" Then
                            Dim Keterangan As String = "Transaction 5 years back, starts from H-1"
                            Dim StrQueryCode As String = "select kode from vw_OneFCC_TransactionFilter where kode = 2"
                            Dim Kode As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                            dd_TrnFilterBy.SetTextWithTextValue(Kode, Keterangan)
                        ElseIf .FilterTransaction = "CustomDate" Then
                            Dim Keterangan As String = "Custom Date"
                            Dim StrQueryCode As String = "select kode from vw_OneFCC_TransactionFilter where kode = 3"
                            Dim Kode As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                            dd_TrnFilterBy.SetTextWithTextValue(Kode, Keterangan)
                        End If
                    End If
                    dd_TrnFilterBy.IsReadOnly = True
                    If .FilterTransaction = "CustomDate" Then
                        sar_DateFrom.Value = .Date_Form
                        sar_DateTo.Value = .Date_To
                        sar_DateFrom.Hidden = False
                        sar_DateTo.Hidden = False
                        sar_DateFrom.ReadOnly = True
                        sar_DateTo.ReadOnly = True
                    End If
                    param_RadioOption = .FilterTransaction
                    noid = .NO_IDTransaction
                End With
                Dim tbl_goAML_odm_Transaksi As New DataTable

                Dim StrQueryTransaction As String = "select * from OneFCC_CaseManagement_STRMemo_Transaction where FK_OneFCC_CaseManagement_STRMemo_ID = " & objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                tbl_goAML_odm_Transaksi = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryTransaction, Nothing)
                'For Each item In objSTRMemoClass.listCaseID
                '    Dim recordID = item.CaseID
                '    Dim tbl_goAML_odm_Transaksi_temp As New DataTable
                '    Dim objGetTrn(3) As SqlParameter
                '    objGetTrn(0) = New SqlParameter
                '    objGetTrn(0).ParameterName = "@CaseID"
                '    objGetTrn(0).Value = recordID

                '    objGetTrn(1) = New SqlParameter
                '    objGetTrn(1).ParameterName = "@RadioOption"
                '    objGetTrn(1).Value = param_RadioOption

                '    objGetTrn(2) = New SqlParameter
                '    objGetTrn(2).ParameterName = "@DateFrom"
                '    objGetTrn(2).Value = datefrom.ToString("yyyy-MM-dd")

                '    objGetTrn(3) = New SqlParameter
                '    objGetTrn(3).ParameterName = "@DateTo"
                '    objGetTrn(3).Value = dateto.ToString("yyyy-MM-dd")

                '    tbl_goAML_odm_Transaksi_temp = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_OneFCC_GetTransactionOfCaseAlert", objGetTrn)
                '    tbl_goAML_odm_Transaksi.Merge(tbl_goAML_odm_Transaksi_temp)
                'Next

                For Each row As DataRow In tbl_goAML_odm_Transaksi.Rows

                    Dim Transaction As NawaDevDAL.goAML_ODM_Transaksi = New NawaDevDAL.goAML_ODM_Transaksi
                    If Not IsDBNull(row.Item("Account_NO")) Then
                        Transaction.Account_NO = row.Item("Account_NO")
                    End If
                    If Not IsDBNull(row.Item("ACCOUNT_No_Lawan")) Then
                        Transaction.ACCOUNT_No_Lawan = row.Item("ACCOUNT_No_Lawan")
                    End If
                    If Not IsDBNull(row.Item("Active")) Then
                        Transaction.Active = row.Item("Active")
                    End If
                    If Not IsDBNull(row.Item("Alternateby")) Then
                        Transaction.Alternateby = row.Item("Alternateby")
                    End If
                    If Not IsDBNull(row.Item("ApprovedBy")) Then
                        Transaction.ApprovedBy = row.Item("ApprovedBy")
                    End If
                    If Not IsDBNull(row.Item("ApprovedDate")) Then
                        Transaction.ApprovedDate = row.Item("ApprovedDate")
                    End If
                    If Not IsDBNull(row.Item("CreatedBy")) Then
                        Transaction.CreatedBy = row.Item("CreatedBy")
                    End If
                    If Not IsDBNull(row.Item("CreatedDate")) Then
                        Transaction.CreatedDate = row.Item("CreatedDate")
                    End If
                    If Not IsDBNull(row.Item("LastUpdateBy")) Then
                        Transaction.LastUpdateBy = row.Item("LastUpdateBy")
                    End If
                    If Not IsDBNull(row.Item("LastUpdateDate")) Then
                        Transaction.LastUpdateDate = row.Item("LastUpdateDate")
                    End If
                    If Not IsDBNull(row.Item("Authorized")) Then
                        Transaction.Authorized = row.Item("Authorized")
                    End If
                    If Not IsDBNull(row.Item("BiMultiParty")) Then
                        Transaction.BiMultiParty = row.Item("BiMultiParty")
                    End If
                    If Not IsDBNull(row.Item("Business_date")) Then
                        Transaction.Business_date = row.Item("Business_date")
                    End If
                    If Not IsDBNull(row.Item("CIF_NO")) Then
                        Transaction.CIF_NO = row.Item("CIF_NO")
                    End If
                    If Not IsDBNull(row.Item("CIF_No_Lawan")) Then
                        Transaction.CIF_No_Lawan = row.Item("CIF_No_Lawan")
                    End If
                    If Not IsDBNull(row.Item("Comments")) Then
                        Transaction.Conductor_ID = row.Item("Comments")
                    End If
                    If Not IsDBNull(row.Item("Conductor_ID")) Then
                        Transaction.Conductor_ID = row.Item("Conductor_ID")
                    End If
                    If Not IsDBNull(row.Item("Country_Code")) Then
                        Transaction.Country_Code = row.Item("Country_Code")
                    End If
                    If Not IsDBNull(row.Item("Country_Code_Lawan")) Then
                        Transaction.Country_Code_Lawan = row.Item("Country_Code_Lawan")
                    End If
                    If Not IsDBNull(row.Item("Currency")) Then
                        Transaction.Currency = row.Item("Currency")
                    End If
                    If Not IsDBNull(row.Item("Currency_Lawan")) Then
                        Transaction.Currency_Lawan = row.Item("Currency_Lawan")
                    End If
                    If Not IsDBNull(row.Item("Date_Posting")) Then
                        Transaction.Date_Posting = row.Item("Date_Posting")
                    End If
                    If Not IsDBNull(row.Item("Debit_Credit")) Then
                        Transaction.Debit_Credit = row.Item("Debit_Credit")
                    End If
                    If Not IsDBNull(row.Item("Date_Transaction")) Then
                        Transaction.Date_Transaction = row.Item("Date_Transaction")
                    End If
                    If Not IsDBNull(row.Item("Exchange_Rate")) Then
                        Transaction.Exchange_Rate = row.Item("Exchange_Rate")
                    End If
                    If Not IsDBNull(row.Item("From_Funds_Code")) Then
                        Transaction.From_Funds_Code = row.Item("From_Funds_Code")
                    End If
                    If Not IsDBNull(row.Item("From_Type")) Then
                        Transaction.From_Type = row.Item("From_Type")
                    End If
                    If Not IsDBNull(row.Item("GCN")) Then
                        Transaction.GCN = row.Item("GCN")
                    End If
                    If Not IsDBNull(row.Item("GCN_Lawan")) Then
                        Transaction.GCN_Lawan = row.Item("GCN_Lawan")
                    End If
                    If Not IsDBNull(row.Item("IDR_Amount")) Then
                        Transaction.IDR_Amount = row.Item("IDR_Amount")
                    End If
                    'If Not IsDBNull(row.Item("MsgSwiftType")) Then
                    '    Transaction.MsgSwiftType = row.Item("MsgSwiftType")
                    'End If
                    If Not IsDBNull(row.Item("Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID")) Then
                        Transaction.NO_ID = row.Item("Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID")
                    End If
                    If Not IsDBNull(row.Item("Original_Amount")) Then
                        Transaction.Original_Amount = row.Item("Original_Amount")
                    End If
                    If Not IsDBNull(row.Item("Ref_Num")) Then
                        Transaction.Ref_Num = row.Item("Ref_Num")
                    End If
                    If Not IsDBNull(row.Item("Source_Data")) Then
                        Transaction.Source_Data = row.Item("Source_Data")
                    End If
                    If Not IsDBNull(row.Item("Swift_Code_Lawan")) Then
                        Transaction.Swift_Code_Lawan = row.Item("Swift_Code_Lawan")
                    End If
                    If Not IsDBNull(row.Item("Teller")) Then
                        Transaction.Teller = row.Item("Teller")
                    End If
                    If Not IsDBNull(row.Item("To_Funds_Code")) Then
                        Transaction.To_Funds_Code = row.Item("To_Funds_Code")
                    End If
                    If Not IsDBNull(row.Item("To_Type")) Then
                        Transaction.To_Type = row.Item("To_Type")
                    End If
                    If Not IsDBNull(row.Item("Transaction_Code")) Then
                        Transaction.Transaction_Code = row.Item("Transaction_Code")
                    End If
                    If Not IsDBNull(row.Item("Transaction_Location")) Then
                        Transaction.Transaction_Location = row.Item("Transaction_Location")
                    End If
                    If Not IsDBNull(row.Item("Transaction_Number")) Then
                        Transaction.Transaction_Number = row.Item("Transaction_Number")
                    End If
                    If Not IsDBNull(row.Item("Transaction_Remark")) Then
                        Transaction.Transaction_Remark = row.Item("Transaction_Remark")
                    End If
                    If Not IsDBNull(row.Item("Transmode_Code")) Then
                        Transaction.Transmode_Code = row.Item("Transmode_Code")
                    End If
                    If Not IsDBNull(row.Item("Transmode_Comment")) Then
                        Transaction.Transmode_Comment = row.Item("Transmode_Comment")
                    End If
                    If Not IsDBNull(row.Item("WIC_No")) Then
                        Transaction.WIC_No = row.Item("WIC_No")
                    End If
                    If Not IsDBNull(row.Item("WIC_No_Lawan")) Then
                        Transaction.WIC_No_Lawan = row.Item("WIC_No_Lawan")
                    End If





                    ListTransaction.Add(Transaction)
                Next

                'Dim JumlahListTrans As Integer = objSTRMemoClass.listTransaction.Count
                'Dim CountTrans As Integer = 0
                'If noid = "ALL" Then
                'Else
                '    For Each itemlist In ListTransaction.ToList
                '        If CountTrans < JumlahListTrans Then
                '            Dim CountListTrans2 As Integer = 0
                '            For Each itemstrmemolist In objSTRMemoClass.listTransaction.ToList
                '                If itemlist.Transaction_Number <> itemstrmemolist.Transaction_Number Then

                '                Else
                '                    objSTRMemoClass.listTransaction.Remove(itemstrmemolist)
                '                    CountListTrans2 = CountListTrans2 + 1
                '                    CountTrans = CountTrans + 1
                '                End If
                '                If CountListTrans2 > 0 Then
                '                    Exit For
                '                Else

                '                End If
                '            Next
                '        Else
                '            ListTransaction.Remove(itemlist)
                '        End If


                '    Next
                'End If
                'Using objdb As New CasemanagementEntities
                '    Dim listTransactionTemp2 As List(Of OneFCC_CaseManagement_STRMemo_Transaction) = objdb.OneFCC_CaseManagement_STRMemo_Transaction.Where(Function(x) x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID).ToList
                '    objSTRMemoClass.listTransaction = listTransactionTemp2
                'End Using

                'bindTransactionDataTable(StoreTransaction, tbl_goAML_odm_Transaksi)
                bindTransactionDataTable(StoreTransaksiNoChecked, tbl_goAML_odm_Transaksi)
            End If
            With objSTRMemoClass.objSTRMemo
                displayPK.Value = IDUnik
                displayPK.Text = IDUnik
                Dim objCust As New goAML_Ref_Customer
                Dim KategoriCust As String = ""
                Dim CustName As String = ""
                Dim CIF As String = .CIF.ToString
                Using objdb As New NawaDatadevEntities
                    objCust = objdb.goAML_Ref_Customer.Where(Function(x) x.CIF = CIF).FirstOrDefault
                    If objCust IsNot Nothing Then
                        If objCust.FK_Customer_Type_ID = 2 Then
                            CustName = objCust.Corp_Name
                        Else
                            CustName = objCust.INDV_Last_Name
                        End If
                        KategoriCust = "Nasabah"
                    Else
                        KategoriCust = "WIC"
                    End If

                End Using
                Dim CIFandName As String = CIF & " - " & CustName
                txtCIF.Value = CIFandName
                txtNo.Value = .No
                txtKepada.Value = .Kepada
                txtDari.Value = .Dari
                txtTanggal.Value = .Tanggal
                txtPerihal.Value = .Perihal
                txtSumberPelaporan.Value = .Sumber_Pelaporan
                txtNamaPihakPelapor.Value = .Nama_Pihak_Terlapor
                txtKategoriPihakPelapor.Value = .Kategori_Pihak_Terlapor
                txtUnitBisnis.Value = .Unit_Bisnis_Directorat
                txtDirektor.Value = .Direktorat
                txtCabang.Value = .Cabang
                If .Detail_Kesimpulan IsNot Nothing Then
                    If Not String.IsNullOrEmpty(.Detail_Kesimpulan) Then
                        ''Edit 5-Oct-2022 Felix
                        'Using objdb As New CasemanagementEntities
                        '    Dim StrMemo As New OneFCC_CaseManagement_Kesimpulan
                        '    StrMemo = objdb.OneFCC_CaseManagement_Kesimpulan.Where(Function(x) x.PK_OneFCC_CaseManagement_Kesimpulan_ID = .Detail_Kesimpulan).FirstOrDefault
                        '    cmb_KesimpulanDetail.SetTextWithTextValue(StrMemo.PK_OneFCC_CaseManagement_Kesimpulan_ID, StrMemo.KesimpulanDescription)
                        'End Using

                        Dim RowKesimpulan As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_OneFCC_CaseManagement_Kesimpulan_ID, KesimpulanDescription FROM OneFCC_CaseManagement_Kesimpulan where PK_OneFCC_CaseManagement_Kesimpulan_ID = " & .Detail_Kesimpulan & "", Nothing)
                        If RowKesimpulan IsNot Nothing Then
                            cmb_KesimpulanDetail.SetTextWithTextValue(RowKesimpulan("PK_OneFCC_CaseManagement_Kesimpulan_ID"), RowKesimpulan("KesimpulanDescription"))
                        End If
                        '' End 5 Oct 2022

                    End If
                End If


                txtLamaMenjadiNasabah.Value = .Lama_menjadi_Nasabah
                txtPekerjaanBidangUsaha.Value = .Pekerjaan_Bidang_Usaha

                txtPengahasilanTahun.Value = .Penghasilan_Tahun


                txtProfilLainnya.Value = .Profil_Lainnya
                txtHasilAnalisis.Value = .Hasil_Analisis
                txtKesimpulan.Value = .Kesimpulan

            End With



        Catch ex As Exception

        End Try
    End Sub

    Private Sub isDataValid()
        Try
            If String.IsNullOrEmpty(txtNo.Value) Then
                Throw New ApplicationException(txtNo.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtKepada.Value) Then
                Throw New ApplicationException(txtKepada.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtDari.Value) Then
                Throw New ApplicationException(txtDari.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtTanggal.Value) Then
                Throw New ApplicationException(txtTanggal.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtPerihal.Value) Then
                Throw New ApplicationException(txtPerihal.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtSumberPelaporan.Value) Then
                Throw New ApplicationException(txtSumberPelaporan.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtNamaPihakPelapor.Value) Then
                Throw New ApplicationException(txtNamaPihakPelapor.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtKategoriPihakPelapor.Value) Then
                Throw New ApplicationException(txtKategoriPihakPelapor.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtUnitBisnis.Value) Then
                Throw New ApplicationException(txtUnitBisnis.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtDirektor.Value) Then
                Throw New ApplicationException(txtUnitBisnis.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtCabang.Value) Then
                Throw New ApplicationException(txtUnitBisnis.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtLamaMenjadiNasabah.Value) Then
                Throw New ApplicationException(txtLamaMenjadiNasabah.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtPekerjaanBidangUsaha.Value) Then
                Throw New ApplicationException(txtPekerjaanBidangUsaha.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtPengahasilanTahun.Value) Then
                Throw New ApplicationException(txtPengahasilanTahun.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtProfilLainnya.Value) Then
                Throw New ApplicationException(txtProfilLainnya.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtHasilAnalisis.Value) Then
                Throw New ApplicationException(txtHasilAnalisis.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtKesimpulan.Value) Then
                Throw New ApplicationException(txtKesimpulan.FieldLabel + " is required.")
            End If

        Catch ex As Exception

        End Try
    End Sub


    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub STRMemo_Detail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    'Private Sub STRMemo_Detail_Init(sender As Object, e As EventArgs) Handles Me.Init
    '    objFormModuleAdd = New NawaBLL.FormModuleAdd(fpMain, Panelconfirmation, LblConfirmation)
    'End Sub

#Region "Indikator"

    Public Property IDIndikator() As Long
        Get
            Return Session("STRMemo_Detail.IDIndikator")
        End Get
        Set(ByVal value As Long)
            Session("STRMemo_Detail.IDIndikator") = value
        End Set
    End Property

    Protected Sub Clean_Window_Indikator()
        'Clean fields
        cmb_indikator.SetTextValue("")
        'Show Buttons
        btn_indikator_Save.Hidden = False
    End Sub

    Public Property obj_Indikator_Edit() As OneFCC_CaseManagement_STRMemo_Indikator
        Get
            Return Session("STRMemo_Detail.obj_Indikator_Edit")
        End Get
        Set(ByVal value As OneFCC_CaseManagement_STRMemo_Indikator)
            Session("STRMemo_Detail.obj_Indikator_Edit") = value
        End Set
    End Property

    Public Property obj_ListIndikator_Edit() As List(Of OneFCC_CaseManagement_STRMemo_Indikator)
        Get
            Return Session("STRMemo_Detail.obj_ListIndikator_Edit")
        End Get
        Set(ByVal value As List(Of OneFCC_CaseManagement_STRMemo_Indikator))
            Session("STRMemo_Detail.obj_ListIndikator_Edit") = value
        End Set
    End Property


    Protected Sub gc_indikator(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListIndikator_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListIndikator_Edit.Remove(objToDelete)
                    End If
                    Bind_Indikator()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Indikator_Edit = obj_ListIndikator_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID = strID)
                    Load_Window_Indikator(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Indikator_Cancel_Click()
        Try
            'Hide window pop up
            Window_Indikator.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Indikator_Save_Click()
        Try
            'Validate input

            If String.IsNullOrEmpty(cmb_indikator.SelectedItemValue) Then
                Throw New ApplicationException(cmb_indikator.Label & " harus diisi.")
            End If


            'Action save here
            Dim intPK As Long = -1

            If obj_Indikator_Edit Is Nothing Then  'Add
                Dim objAdd As New OneFCC_CaseManagement_STRMemo_Indikator
                If obj_ListIndikator_Edit IsNot Nothing Then
                    If obj_ListIndikator_Edit.Count > 0 Then
                        intPK = obj_ListIndikator_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDIndikator = intPK

                With objAdd
                    .PK_OneFCC_CaseManagement_STRMemo_Indikator_ID = intPK

                    If Not String.IsNullOrEmpty(cmb_indikator.SelectedItemValue) Then
                        .FK_Indicator = cmb_indikator.SelectedItemValue
                    Else
                        .FK_Indicator = Nothing
                    End If


                End With
                If obj_ListIndikator_Edit Is Nothing Then
                    obj_ListIndikator_Edit = New List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Indikator)
                End If
                obj_ListIndikator_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListIndikator_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID = obj_Indikator_Edit.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDIndikator = .PK_OneFCC_CaseManagement_STRMemo_Indikator_ID

                        If Not String.IsNullOrEmpty(cmb_indikator.SelectedItemValue) Then
                            .FK_Indicator = cmb_indikator.SelectedItemValue
                        Else
                            .FK_Indicator = Nothing
                        End If

                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_Indikator()

            'Hide window popup
            Window_Indikator.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_Indikator()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListIndikator_Edit)

        objtable.Columns.Add(New DataColumn("Kode", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        For Each row As DataRow In objtable.Rows
            row("Kode") = row("FK_Indicator")
            If Not IsDBNull(row("Kode")) Then
                Dim StrQueryCode As String = "select Keterangan from goAML_Ref_Indikator_Laporan where Kode = '" & row("Kode").ToString & "'"
                Dim Keterangan As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                row("Keterangan") = Keterangan
            Else
                row("Keterangan") = Nothing
            End If
        Next
        gp_indikator.GetStore().DataSource = objtable
        gp_indikator.GetStore().DataBind()


    End Sub

    Protected Sub Load_Window_Indikator(strAction As String)
        'Clean window pop up
        Clean_Window_Indikator()

        If obj_Indikator_Edit IsNot Nothing Then
            'Populate fields
            With obj_Indikator_Edit
                If .FK_Indicator IsNot Nothing Then
                    Using objdb As New NawaDatadevEntities
                        Dim objKode As goAML_Ref_Indikator_Laporan = objdb.goAML_Ref_Indikator_Laporan.Where(Function(X) X.Kode = .FK_Indicator).FirstOrDefault
                        If Not objKode Is Nothing Then
                            cmb_indikator.SetTextWithTextValue(obj_Indikator_Edit.FK_Indicator, objKode.Keterangan)
                        End If
                    End Using

                End If

            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            cmb_indikator.IsReadOnly = False
            btn_indikator_Save.Hidden = False '' Added on 12 Aug 2021
        Else
            cmb_indikator.IsReadOnly = True

            btn_indikator_Save.Hidden = True '' Added on 12 Aug 2021
        End If
        cmb_indikator.StringFieldStyle = "background-color:#FFE4C4"
        'Bind Indikator
        IDIndikator = obj_Indikator_Edit.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID

        'Show window pop up
        Window_Indikator.Title = "Indikator - " & strAction
        Window_Indikator.Hidden = False
    End Sub


#End Region

#Region "Pihak Terkait"

    Public Property IDPihakTerkait() As Long
        Get
            Return Session("STRMemo_Detail.IDPihakTerkait")
        End Get
        Set(ByVal value As Long)
            Session("STRMemo_Detail.IDPihakTerkait") = value
        End Set
    End Property

    Protected Sub Clean_Window_PihakTerkait()
        'Clean fields
        txtPT_CIF.Value = Nothing
        txtPT_HubunganPihakTerkait.Value = Nothing
        txtPT_RefGrips.Value = Nothing
        txtPT_TelahDilaporkan.Value = Nothing

        cmbPT_CIF.SetTextValue("")
        'Show Buttons
        btn_PihakTerkait_Save.Hidden = False

    End Sub


    Public Property obj_PihakTerkait_Edit() As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait
        Get
            Return Session("STRMemo_Detail.obj_PihakTerkait_Edit")
        End Get
        Set(ByVal value As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait)
            Session("STRMemo_Detail.obj_PihakTerkait_Edit") = value
        End Set
    End Property

    Public Property obj_ListPihakTerkait_Edit() As List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait)
        Get
            Return Session("STRMemo_Detail.obj_ListPihakTerkait_Edit")
        End Get
        Set(ByVal value As List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait))
            Session("STRMemo_Detail.obj_ListPihakTerkait_Edit") = value
        End Set
    End Property

    Protected Sub btn_Pihak_Terkait_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            obj_PihakTerkait_Edit = Nothing
            Clean_Window_PihakTerkait()

            'Show window pop up
            txtPT_CIF.ReadOnly = False
            cmbPT_CIF.IsReadOnly = False
            txtPT_HubunganPihakTerkait.ReadOnly = False
            txtPT_RefGrips.ReadOnly = False
            txtPT_TelahDilaporkan.ReadOnly = False
            cmbPT_CIF.StringFieldStyle = "background-color:#FFE4C4"
            btn_PihakTerkait_Save.Hidden = False '' Added on 12 Aug 2021
            Window_PihakTerkair.Title = "Pihak Terkait - Add"
            Window_PihakTerkair.Hidden = False
            txtPT_CIF.Value = Session("STRMemo_CIFNO")
            'Set IDIndikator to 0
            IDPihakTerkait = 0


        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_PihakTerkait(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListPihakTerkait_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListPihakTerkait_Edit.Remove(objToDelete)
                    End If
                    Bind_Pihak_Terkait()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_PihakTerkait_Edit = obj_ListPihakTerkait_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID = strID)
                    Load_Window_PihakTerkait(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_PihakTerkait_Cancel_Click()
        Try
            'Hide window pop up
            Window_PihakTerkair.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_PihakTerkait_Save_Click()
        Try
            'Validate input


            If String.IsNullOrWhiteSpace(cmbPT_CIF.SelectedItemValue) Then
                Throw New ApplicationException(cmbPT_CIF.Label & " is required.")
            End If
            If String.IsNullOrEmpty(txtPT_HubunganPihakTerkait.Value) Then
                Throw New ApplicationException(txtPT_HubunganPihakTerkait.FieldLabel & " harus diisi.")
            End If
            'If String.IsNullOrEmpty(txtPT_RefGrips.Value) Then
            '    Throw New ApplicationException(txtPT_RefGrips.FieldLabel & " harus diisi.")
            'End If
            'If String.IsNullOrEmpty(txtPT_TelahDilaporkan.Value) Then
            '    Throw New ApplicationException(txtPT_TelahDilaporkan.FieldLabel & " harus diisi.")
            'End If

            ''set account balance



            'Action save here
            Dim intPK As Long = -1

            If obj_PihakTerkait_Edit Is Nothing Then  'Add
                Dim objAdd As New CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait
                If obj_ListPihakTerkait_Edit IsNot Nothing Then
                    If obj_ListPihakTerkait_Edit.Count > 0 Then
                        intPK = obj_ListPihakTerkait_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDPihakTerkait = intPK

                With objAdd
                    .PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID = intPK

                    If Not String.IsNullOrEmpty(cmbPT_CIF.SelectedItemValue) Then
                        .CIF = cmbPT_CIF.SelectedItemValue
                        Dim Name As String = ""
                        Dim StrQueryCode As String = "select case when fk_customer_type_id = 1 then indv_last_name else corp_name end as indv_last_name from goaml_Ref_customer where cif = '" & cmbPT_CIF.SelectedItemValue & "'"
                        Name = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                        .Nama_PihakTerkait = Name
                    Else
                        .CIF = Nothing
                    End If

                    If Not String.IsNullOrEmpty(txtPT_HubunganPihakTerkait.Value) Then
                        .Hubungan_PihakTerkait = txtPT_HubunganPihakTerkait.Value
                    Else
                        .Hubungan_PihakTerkait = Nothing
                    End If

                    If Not String.IsNullOrEmpty(txtPT_RefGrips.Value) Then
                        .RefGrips_PihakTerkait = txtPT_RefGrips.Value
                    Else
                        .RefGrips_PihakTerkait = Nothing
                    End If

                    If Not String.IsNullOrEmpty(txtPT_TelahDilaporkan.Value) Then
                        .Dilaporkan_PihakTerkait = txtPT_TelahDilaporkan.Value
                    Else
                        .Dilaporkan_PihakTerkait = Nothing
                    End If

                End With
                If obj_ListPihakTerkait_Edit Is Nothing Then
                    obj_ListPihakTerkait_Edit = New List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait)
                End If
                obj_ListPihakTerkait_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListPihakTerkait_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID = obj_PihakTerkait_Edit.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDPihakTerkait = .PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID

                        If Not String.IsNullOrEmpty(cmbPT_CIF.SelectedItemValue) Then
                            .CIF = cmbPT_CIF.SelectedItemValue
                            Dim Name As String = ""
                            Dim StrQueryCode As String = "select case when fk_customer_type_id = 1 then indv_last_name else corp_name end as indv_last_name from goaml_Ref_customer where cif = '" & cmbPT_CIF.SelectedItemValue & "'"
                            Name = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                            .Nama_PihakTerkait = Name
                        Else
                            .CIF = Nothing
                        End If



                        If Not String.IsNullOrEmpty(txtPT_HubunganPihakTerkait.Value) Then
                            .Hubungan_PihakTerkait = txtPT_HubunganPihakTerkait.Value
                        Else
                            .Hubungan_PihakTerkait = Nothing
                        End If

                        If Not String.IsNullOrEmpty(txtPT_RefGrips.Value) Then
                            .RefGrips_PihakTerkait = txtPT_RefGrips.Value
                        Else
                            .RefGrips_PihakTerkait = Nothing
                        End If

                        If Not String.IsNullOrEmpty(txtPT_TelahDilaporkan.Value) Then
                            .Dilaporkan_PihakTerkait = txtPT_TelahDilaporkan.Value
                        Else
                            .Dilaporkan_PihakTerkait = Nothing
                        End If

                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_Pihak_Terkait()

            'Hide window popup
            Window_PihakTerkair.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_Pihak_Terkait()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListPihakTerkait_Edit)
        For Each row As DataRow In objtable.Rows
            If Not IsDBNull(row("CIF")) Then
                Dim CIF As String = ""
                Dim StrQueryCode As String = "select case when fk_customer_type_id = 1 then indv_last_name else corp_name end as indv_last_name from goaml_Ref_customer where cif = '" & row("CIF").ToString & "'"
                CIF = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCode, Nothing)
                row("Nama_PihakTerkait") = CIF
            Else
                row("Nama_PihakTerkait") = Nothing
            End If
        Next

        gp_Pihak_Terkait.GetStore().DataSource = objtable
        gp_Pihak_Terkait.GetStore().DataBind()


    End Sub

    Protected Sub Load_Window_PihakTerkait(strAction As String)
        'Clean window pop up
        Clean_Window_PihakTerkait()

        If obj_PihakTerkait_Edit IsNot Nothing Then
            'Populate fields
            With obj_PihakTerkait_Edit
                If Not String.IsNullOrEmpty(.CIF) Then
                    Dim strfieldtypestring As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "SELECT Keterangan FROM vw_goAML_Ref_Customer WHERE kode='" & .CIF & "'", Nothing)
                    cmbPT_CIF.SetTextWithTextValue(.CIF, strfieldtypestring)
                Else
                    cmbPT_CIF.SetTextValue("")
                End If

                If Not String.IsNullOrEmpty(.Hubungan_PihakTerkait) Then
                    txtPT_HubunganPihakTerkait.Value = .Hubungan_PihakTerkait
                Else
                    txtPT_HubunganPihakTerkait.Value = Nothing
                End If

                If Not String.IsNullOrEmpty(.RefGrips_PihakTerkait) Then
                    txtPT_RefGrips.Value = .RefGrips_PihakTerkait
                Else
                    txtPT_RefGrips.Value = Nothing
                End If

                If Not String.IsNullOrEmpty(.Dilaporkan_PihakTerkait) Then
                    txtPT_TelahDilaporkan.Value = .Dilaporkan_PihakTerkait
                Else
                    txtPT_TelahDilaporkan.Value = Nothing
                End If

            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            txtPT_CIF.ReadOnly = False
            cmbPT_CIF.IsReadOnly = False
            cmbPT_CIF.StringFieldStyle = "background-color:#FFE4C4"
            txtPT_HubunganPihakTerkait.ReadOnly = False
            txtPT_RefGrips.ReadOnly = False
            txtPT_TelahDilaporkan.ReadOnly = False
            btn_PihakTerkait_Save.Hidden = False '' Added on 12 Aug 2021
        Else
            cmbPT_CIF.IsReadOnly = True
            txtPT_CIF.ReadOnly = True
            txtPT_HubunganPihakTerkait.ReadOnly = True
            txtPT_RefGrips.ReadOnly = True
            txtPT_TelahDilaporkan.ReadOnly = True
            btn_PihakTerkait_Save.Hidden = True '' Added on 12 Aug 2021
        End If

        'Bind Indikator
        IDPihakTerkait = obj_PihakTerkait_Edit.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID

        'Show window pop up
        Window_PihakTerkair.Title = "Pihak Terkait - " & strAction
        Window_PihakTerkair.Hidden = False
    End Sub
#End Region

#Region "Tindak Lanjut"

    Public Property IDTindakLanjut() As Long
        Get
            Return Session("STRMemo_Detail.IDTindakLanjut")
        End Get
        Set(ByVal value As Long)
            Session("STRMemo_Detail.IDTindakLanjut") = value
        End Set
    End Property

    Protected Sub Clean_Window_TindakLanjut()
        'Clean fields
        cmb_tindaklanjut.SetTextValue("")
        'Show Buttons
        btn_TindakLanjut_Save.Hidden = False
    End Sub

    Public Property obj_TindakLanjut_Edit() As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut
        Get
            Return Session("STRMemo_Detail.obj_TindakLanjut_Edit")
        End Get
        Set(ByVal value As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut)
            Session("STRMemo_Detail.obj_TindakLanjut_Edit") = value
        End Set
    End Property

    Public Property obj_ListTindakLanjut_Edit() As List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut)
        Get
            Return Session("STRMemo_Detail.obj_ListTindakLanjut_Edit")
        End Get
        Set(ByVal value As List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut))
            Session("STRMemo_Detail.obj_ListTindakLanjut_Edit") = value
        End Set
    End Property



    Protected Sub gc_TindakLanjut(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListTindakLanjut_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListTindakLanjut_Edit.Remove(objToDelete)
                    End If
                    Bind_TindakLanjut()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_TindakLanjut_Edit = obj_ListTindakLanjut_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID = strID)
                    Load_Window_TindakLanjut(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_TindakLanjut_Cancel_Click()
        Try
            'Hide window pop up
            window_tindaklanjut.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_TindakLanjut_Save_Click()
        Try
            'Validate input

            If String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemValue) Then
                Throw New ApplicationException(cmb_tindaklanjut.Label & " harus diisi.")
            End If



            'Action save here
            Dim intPK As Long = -1

            If obj_TindakLanjut_Edit Is Nothing Then  'Add
                Dim objAdd As New CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut
                If obj_ListTindakLanjut_Edit IsNot Nothing Then
                    If obj_ListTindakLanjut_Edit.Count > 0 Then
                        intPK = obj_ListTindakLanjut_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDTindakLanjut = intPK

                With objAdd
                    .PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID = intPK

                    If Not String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemText) Then
                        .TindakLanjutDescription = cmb_tindaklanjut.SelectedItemText
                    Else
                        .TindakLanjutDescription = Nothing
                    End If

                End With
                If obj_ListTindakLanjut_Edit Is Nothing Then
                    obj_ListTindakLanjut_Edit = New List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut)
                End If
                obj_ListTindakLanjut_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListTindakLanjut_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID = obj_TindakLanjut_Edit.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDTindakLanjut = .PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID

                        If Not String.IsNullOrEmpty(cmb_tindaklanjut.SelectedItemValue) Then
                            .TindakLanjutDescription = cmb_tindaklanjut.SelectedItemValue
                        Else
                            .TindakLanjutDescription = Nothing
                        End If

                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_TindakLanjut()

            'Hide window popup
            window_tindaklanjut.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_TindakLanjut()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(obj_ListTindakLanjut_Edit)


        gp_TIndakLanjut.GetStore().DataSource = objtable
        gp_TIndakLanjut.GetStore().DataBind()


    End Sub

    Protected Sub Load_Window_TindakLanjut(strAction As String)
        'Clean window pop up
        Clean_Window_TindakLanjut()

        If obj_TindakLanjut_Edit IsNot Nothing Then
            'Populate fields
            With obj_TindakLanjut_Edit
                If .FK_OneFCC_CaseManagement_TindakLanjut_ID IsNot Nothing Then

                    cmb_tindaklanjut.SetTextWithTextValue(.FK_OneFCC_CaseManagement_TindakLanjut_ID, .TindakLanjutDescription)


                End If

            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            cmb_tindaklanjut.IsReadOnly = False
            btn_TindakLanjut_Save.Hidden = False '' Added on 12 Aug 2021
        Else
            cmb_tindaklanjut.IsReadOnly = True

            btn_TindakLanjut_Save.Hidden = True '' Added on 12 Aug 2021
        End If
        cmb_tindaklanjut.StringFieldStyle = "background-color:#FFE4C4"
        'Bind Indikator
        IDTindakLanjut = obj_TindakLanjut_Edit.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID

        'Show window pop up
        window_tindaklanjut.Title = "Tindak Lanjut - " & strAction
        window_tindaklanjut.Hidden = False
    End Sub
#End Region

#Region "Transaction"
    Public Property IDDokumen As String
        Get
            Return Session("STRMemo_Detail.IDDokumen")
        End Get
        Set(value As String)
            Session("STRMemo_Detail.IDDokumen") = value
        End Set
    End Property
    Public Property ListSelectedTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi)
        Get
            If Session("STRMemo_Detail.ListSelectedTransaction") Is Nothing Then
                Session("STRMemo_Detail.ListSelectedTransaction") = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
            End If
            Return Session("STRMemo_Detail.ListSelectedTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Transaksi))
            Session("STRMemo_Detail.ListSelectedTransaction") = value
        End Set
    End Property
    Public Property ListTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi)
        Get
            If Session("STRMemo_Detail.ListTransaction") Is Nothing Then
                Session("STRMemo_Detail.ListTransaction") = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
            End If
            Return Session("STRMemo_Detail.ListTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Transaksi))
            Session("STRMemo_Detail.ListTransaction") = value
        End Set
    End Property
    Protected Sub dd_TrnFilterBy_ValueChanged(sender As Object, e As DirectEventArgs)
        Try

            If dd_TrnFilterBy.SelectedItemValue = "1" Then
                sar_DateFrom.Hidden = True
                sar_DateTo.Hidden = True
            ElseIf dd_TrnFilterBy.SelectedItemValue = "2" Then
                sar_DateFrom.Hidden = True
                sar_DateTo.Hidden = True
            ElseIf dd_TrnFilterBy.SelectedItemValue = "3" Then
                sar_DateFrom.Hidden = False
                sar_DateTo.Hidden = False
            Else
                Throw New ApplicationException("Please select one of the filter options")
            End If

            'rb_CustomDate.Checked = True
            'sar_DateFrom.Hidden = False
            'sar_DateTo.Hidden = False

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub bindTransactionDataTable(store As Ext.Net.Store, dt As DataTable)
        Dim objtable As New Data.DataTable
        objtable = dt
        'Dim objtable As Data.DataTable = dt

        objtable.Columns.Add(New DataColumn("Valid", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoRefTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("TipeTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("LokasiTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("KeteranganTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("DateTransaction", GetType(Date)))
        objtable.Columns.Add(New DataColumn("AccountNo", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaTeller", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaPejabat", GetType(String)))
        objtable.Columns.Add(New DataColumn("TglPembukuan", GetType(Date)))
        objtable.Columns.Add(New DataColumn("CaraTranDilakukan", GetType(String)))
        objtable.Columns.Add(New DataColumn("CaraTranLain", GetType(String)))
        objtable.Columns.Add(New DataColumn("DebitCredit", GetType(String)))
        objtable.Columns.Add(New DataColumn("OriginalAmount", GetType(String)))
        objtable.Columns.Add(New DataColumn("IDR", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim CifLawan As String = item("CIF_No_Lawan").ToString
                Dim WicLawan As String = item("WIC_No_Lawan").ToString
                Dim AccountLawan As String = item("ACCOUNT_No_Lawan").ToString
                Dim BiMulti As String = item("BiMultiParty").ToString
                If CifLawan = "" And WicLawan = "" And AccountLawan = "" And BiMulti = "1" Then
                    item("Valid") = "Tidak"
                Else
                    item("Valid") = "Ya"
                End If
                item("NoTran") = item("Transaction_Number")
                item("NoRefTran") = item("Ref_Num")
                Dim SourceData As String = item("Source_Data").ToString
                Dim SourceDataTemp As Integer
                If Integer.TryParse(SourceData, SourceDataTemp) Then
                    If SourceData <> "" Then
                        item("TipeTran") = NawaDevBLL.GenerateSarBLL.getSourceData(SourceData)
                    End If
                Else
                    item("TipeTran") = SourceData
                End If
                item("LokasiTran") = item("Transaction_Location")
                item("KeteranganTran") = item("Transaction_Remark")
                item("DateTransaction") = item("Date_Transaction")
                item("AccountNo") = item("Account_NO")
                item("NamaTeller") = item("Teller")
                item("NamaPejabat") = item("Authorized")
                item("TglPembukuan") = item("Date_Posting")
                item("CaraTranDilakukan") = item("Transmode_Code")
                item("CaraTranLain") = item("Transmode_Comment")
                item("DebitCredit") = item("Debit_Credit")
                If item("Original_Amount").ToString <> "" Then
                    Dim OriginalAmount As Decimal = item("Original_Amount").ToString
                    item("OriginalAmount") = OriginalAmount.ToString("#,###.00")
                End If
                If item("IDR_Amount").ToString <> "" Then
                    Dim IDR As Decimal = item("IDR_Amount").ToString
                    item("IDR") = IDR.ToString("#,###.00")
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Private Shared Sub DataSetToExcel(ByVal dataSet As DataSet, ByVal objfileinfo As FileInfo)
        Using pck As ExcelPackage = New ExcelPackage()

            For Each dataTable As DataTable In dataSet.Tables
                Dim workSheet As ExcelWorksheet = pck.Workbook.Worksheets.Add(dataTable.TableName)
                workSheet.Cells("A1").LoadFromDataTable(dataTable, True)

                Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                Dim intcolnumber As Integer = 1
                For Each item As System.Data.DataColumn In dataTable.Columns
                    If item.DataType = GetType(Date) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = dateformat
                    End If
                    If item.DataType = GetType(Integer) Or item.DataType = GetType(Double) Or item.DataType = GetType(Long) Or item.DataType = GetType(Decimal) Or item.DataType = GetType(Single) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = "#,##0.00"
                    End If

                    intcolnumber = intcolnumber + 1
                Next
                workSheet.Cells(workSheet.Dimension.Address).AutoFitColumns()
            Next

            pck.SaveAs(objfileinfo)
        End Using
    End Sub

    Protected Sub CreateExcel2007WithData(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try


            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            'Dim intTotalRow As Long
            Dim objSchemaModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(12226)
            Dim objSchemaModuleField As List(Of NawaDAL.ModuleField) = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(12226)
            Dim objSchemaModuleFieldDefault As New List(Of NawaDAL.ModuleFieldDefault)
            Dim objmdl As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(12226)

            Dim cif As String = Session("STRMemo_CIFNO")
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate

            Dim objList = NawaDevBLL.GenerateSarBLL.getListTransactionBySearch(cif, datefrom, dateto)



            objFormModuleView = New FormModuleView(GridPaneldetail, BtnExport)
            objFormModuleView.ModuleID = objmdl.PK_Module_ID
            objFormModuleView.ModuleName = objmdl.ModuleName

            Dim delimitercheckboxparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(39)
            Dim strdelimiter As String = ";"
            If Not delimitercheckboxparam Is Nothing Then
                strdelimiter = delimitercheckboxparam.SettingValue
            End If

            Dim objtb As DataTable = NawaBLL.Common.CopyGenericToDataTable(ListTransaction)
            Dim intmaxrow As Integer
            If objtb.Rows.Count + 1000 > 1048575 Then
                intmaxrow = 1048575
            Else
                intmaxrow = objtb.Rows.Count + 1000
            End If


            Using objtbl As Data.DataTable = objtb
                'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                objtbl.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                Dim intTotalPage As Integer = (intmaxrow + 1048575 - 1) / 1048575
                Dim dsTable As New Data.DataSet

                If intTotalPage = 1 Then
                    'objFormModuleView.changeHeader(objtbl)

                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    DataSetToExcel(dsTable, objfileinfo)

                    dsTable.Dispose()
                    dsTable = Nothing



                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If



                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")



                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()
                ElseIf intTotalPage > 1 Then
                    'objFormModuleView.changeHeader(objtbl)



                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    'ambil page 2 dst sampe page terakhir
                    For index = 1 To intTotalPage - 2
                        Using objtbltemp As Data.DataTable = objtb
                            objtbltemp.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                            dsTable.Tables.Add(objtbltemp.Copy)
                        End Using
                    Next
                    DataSetToExcel(dsTable, objfileinfo)
                    dsTable.Dispose()
                    dsTable = Nothing

                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If
                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()

                End If
            End Using

        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub
    Protected Sub CreateExcel2007WithDataNoChecked(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try


            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            'Dim intTotalRow As Long
            Dim objSchemaModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(12226)
            Dim objSchemaModuleField As List(Of NawaDAL.ModuleField) = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(12226)
            Dim objSchemaModuleFieldDefault As New List(Of NawaDAL.ModuleFieldDefault)
            Dim objmdl As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(12226)

            Dim cif As String = Session("STRMemo_CIFNO")
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate

            Dim objList = NawaDevBLL.GenerateSarBLL.getListTransactionBySearch(cif, datefrom, dateto)



            objFormModuleView = New FormModuleView(GridPanel1, BtnExportNoChecked)
            objFormModuleView.ModuleID = objmdl.PK_Module_ID
            objFormModuleView.ModuleName = objmdl.ModuleName

            Dim delimitercheckboxparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(39)
            Dim strdelimiter As String = ";"
            If Not delimitercheckboxparam Is Nothing Then
                strdelimiter = delimitercheckboxparam.SettingValue
            End If

            Dim objtb As DataTable = NawaBLL.Common.CopyGenericToDataTable(ListTransaction)
            Dim intmaxrow As Integer
            If objtb.Rows.Count + 1000 > 1048575 Then
                intmaxrow = 1048575
            Else
                intmaxrow = objtb.Rows.Count + 1000
            End If


            Using objtbl As Data.DataTable = objtb
                'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                objtbl.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                Dim intTotalPage As Integer = (intmaxrow + 1048575 - 1) / 1048575
                Dim dsTable As New Data.DataSet

                If intTotalPage = 1 Then
                    'objFormModuleView.changeHeader(objtbl)

                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    DataSetToExcel(dsTable, objfileinfo)

                    dsTable.Dispose()
                    dsTable = Nothing



                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If



                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")



                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()
                ElseIf intTotalPage > 1 Then
                    'objFormModuleView.changeHeader(objtbl)



                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    'ambil page 2 dst sampe page terakhir
                    For index = 1 To intTotalPage - 2
                        Using objtbltemp As Data.DataTable = objtb
                            objtbltemp.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                            dsTable.Tables.Add(objtbltemp.Copy)
                        End Using
                    Next
                    DataSetToExcel(dsTable, objfileinfo)
                    dsTable.Dispose()
                    dsTable = Nothing

                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If
                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()

                End If
            End Using

        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub

    Protected Sub OnSelect_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim data As New goAML_ODM_Transaksi
            Dim idx As String
            idx = e.ExtraParams(0).Value

            data.NO_ID = e.ExtraParams(0).Value

            ListSelectedTransaction.Add(data)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub OnDeSelect_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim data As New goAML_ODM_Transaksi

            For Each item In ListSelectedTransaction
                If item.NO_ID = CInt(e.ExtraParams(0).Value) Then
                    data = item
                    Exit For
                End If
            Next

            ListSelectedTransaction.Remove(data)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Public Property obj_Attachment_Edit() As OneFCC_CaseManagement_STRMemo_Attachment
        Get
            Return Session("STRMemo_Detail.obj_Attachment_Edit")
        End Get
        Set(ByVal value As OneFCC_CaseManagement_STRMemo_Attachment)
            Session("STRMemo_Detail.obj_Attachment_Edit") = value
        End Set
    End Property

    Public Property obj_ListAttachment_Edit() As List(Of OneFCC_CaseManagement_STRMemo_Attachment)
        Get
            Return Session("STRMemo_Detail.obj_ListAttachment_Edit")
        End Get
        Set(ByVal value As List(Of OneFCC_CaseManagement_STRMemo_Attachment))
            Session("STRMemo_Detail.obj_ListAttachment_Edit") = value
        End Set
    End Property


    Protected Sub GridcommandAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                obj_ListAttachment_Edit.Remove(obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = ID).FirstOrDefault)
                bindAttachment(StoreAttachment, obj_ListAttachment_Edit)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                editDokumen(ID, "Edit")
                IDDokumen = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editDokumen(ID, "Detail")
            ElseIf e.ExtraParams(1).Value = "Download" Then
                DownloadFile(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim status As Boolean = False
            Dim DokumenOld As New CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment
            Dim Dokumen As New CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment
            If IDDokumen IsNot Nothing Or IDDokumen <> "" Then
                status = True
                DokumenOld = obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = IDDokumen).FirstOrDefault
                obj_ListAttachment_Edit.Remove(obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = IDDokumen).FirstOrDefault)
            ElseIf Not FileDoc.HasFile Then
                Throw New Exception("Please Upload File")
            End If

            Dim docName As String = IO.Path.GetFileName(FileDoc.FileName)
            If obj_ListAttachment_Edit IsNot Nothing Then
                For Each item In obj_ListAttachment_Edit
                    If item.File_Name = docName Then
                        Throw New ApplicationException(docName + " Sudah Ada")
                    End If
                Next
            Else
                obj_ListAttachment_Edit = New List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment)
            End If


            If obj_ListAttachment_Edit.Count = 0 Then
                Dokumen.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = -1
            ElseIf obj_ListAttachment_Edit.Count >= 1 Then
                Dokumen.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = obj_ListAttachment_Edit.Min(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID) - 1
            End If

            If status = True And Not FileDoc.HasFile Then
                Dokumen.File_Name = DokumenOld.File_Name
                Dokumen.File_Doc = DokumenOld.File_Doc
            Else
                Dokumen.File_Name = IO.Path.GetFileName(FileDoc.FileName)
                Dokumen.File_Doc = FileDoc.FileBytes
            End If
            Dokumen.Remarks = txtKeterangan.Text.Trim

            Dokumen.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            Dokumen.CreatedDate = DateTime.Now

            obj_ListAttachment_Edit.Add(Dokumen)

            bindAttachment(StoreAttachment, obj_ListAttachment_Edit)
            WindowAttachment.Hidden = True
            IDDokumen = Nothing

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAttachment.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub bindAttachment(store As Ext.Net.Store, listAttachment As List(Of CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAttachment)
        objtable.Columns.Add(New DataColumn("FileName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("FileName") = item("File_Name")
                item("Keterangan") = item("Remarks")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub editDokumen(id As String, command As String)
        Try
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()

            WindowAttachment.Hidden = False
            Dim Dokumen As New CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment
            Dokumen = obj_ListAttachment_Edit.Where(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = id).FirstOrDefault

            txtFileName.Text = Dokumen.File_Name
            txtKeterangan.Text = Dokumen.Remarks

            If command = "Detail" Then
                FileDoc.Hidden = True
                BtnsaveAttachment.Hidden = True
                txtKeterangan.Editable = False
            Else
                FileDoc.Hidden = False
                BtnsaveAttachment.Hidden = False
                txtKeterangan.Editable = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DownloadFile(id As Long)
        Dim objdownload As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment = obj_ListAttachment_Edit.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = id)
        If Not objdownload Is Nothing Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & objdownload.File_Name)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            'Response.ContentType = "ContentType"
            Response.ContentType = MimeMapping.GetMimeMapping(objdownload.File_Name)
            Response.BinaryWrite(objdownload.File_Doc)
            Response.End()
        End If
    End Sub

    Protected Sub ReportIndicator_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goaml_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub JenisLaporan_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Jenis_Laporan_STR", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
#End Region
    Protected Sub cmb_Cifno_OnValueChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception

        End Try
    End Sub
End Class
