﻿Imports Ext.Net
Imports OfficeOpenXml
Imports NawaBLL
Imports System.Data
Imports NawaDAL
Imports System.Xml
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Services
Partial Class AML_CaseManagement_STRMemo_STRMemo_Approval
    Inherits Parent
    Public objFormModuleView As NawaBLL.FormModuleView
    Public Property strWhereClause() As String
        Get
            Return Session("AML_CaseManagement_STRMemo_Approval.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("AML_CaseManagement_STRMemo_Approval.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("AML_CaseManagement_STRMemo_Approval.strSort")
        End Get
        Set(ByVal value As String)
            Session("AML_CaseManagement_STRMemo_Approval.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("AML_CaseManagement_STRMemo_Approval.indexStart")
        End Get
        Set(ByVal value As String)
            Session("AML_CaseManagement_STRMemo_Approval.indexStart") = value
        End Set
    End Property
    'Private Sub Parameterview_Init(sender As Object, e As EventArgs) Handles Me.Init
    '    objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer
            Try
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)


                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                    Dim strIDCode As String = 1
                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If



                objFormModuleView.ModuleID = objmodule.PK_Module_ID
                objFormModuleView.ModuleName = objmodule.ModuleName
                objFormModuleView.AddField("PK_OneFCC_CaseManagement_STRMemo_ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("CIF", "CIF", 2, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Customer_Name", "Name", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("StatusID", "Status", 4, False, False, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("status", "Status", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                'objFormModuleView.AddField("Tindak_lanjut", "Tindak Lanjut", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Workflow_Step", "Workflow Step", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Workflow_Step_Of", "Workflow Step Of", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("PIC", "PIC", 7, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("PICFilter", "PIC", 8, False, False, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("CreatedDate", "CreatedDate", 9, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                objFormModuleView.SettingFormView()

                'Panel Title
                GridpanelView.Title = objmodule.ModuleLabel & " -  Approval"

                'Remove default form View buttons
                Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
                GridpanelView.ColumnModel.Columns.Remove(objcommandcol)


                'Dim stringcommand As String = "prepareCommandCollection"
                'objcommandcol.PrepareToolbar.Fn = stringcommand

                'Add button Approval Detail
                Using objCustomCommandColumn As New CommandColumn
                    objCustomCommandColumn.ID = "columncrudapproval"
                    objCustomCommandColumn.ClientIDMode = Web.UI.ClientIDMode.Static
                    objCustomCommandColumn.Width = 100


                    Dim extparam As New Ext.Net.Parameter
                    extparam.Name = "unikkey"
                    extparam.Value = "record.data.PK_OneFCC_CaseManagement_STRMemo_ID"
                    extparam.Mode = ParameterMode.Raw

                    Dim extparamcommand As New Ext.Net.Parameter
                    extparamcommand.Name = "command"
                    extparamcommand.Value = "command"
                    extparamcommand.Mode = ParameterMode.Raw

                    objCustomCommandColumn.DirectEvents.Command.ExtraParams.Add(extparam)
                    objCustomCommandColumn.DirectEvents.Command.ExtraParams.Add(extparamcommand)

                    Dim gcApprovalDetail As New GridCommand
                    gcApprovalDetail.CommandName = "ApprovalDetail"
                    gcApprovalDetail.Icon = Icon.ApplicationViewDetail
                    gcApprovalDetail.Text = "Detail"
                    gcApprovalDetail.ToolTip.Text = "Detail"
                    objCustomCommandColumn.Commands.Add(gcApprovalDetail)

                    AddHandler objCustomCommandColumn.DirectEvents.Command.Event, AddressOf GridCommandCustom

                    GridpanelView.ColumnModel.Columns.Insert(1, objCustomCommandColumn)
                End Using


            Catch ex As Exception

            End Try


            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"
            End If




        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            'Begin Penambahan Advanced Filter
            If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                Toolbar2.Hidden = True
            Else
                Toolbar2.Hidden = False
            End If
            LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")
            'END Penambahan Advanced Filter

            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)
            Dim strsort As String = " PK_OneFCC_CaseManagement_STRMemo_ID DESC "
            For Each item As DataSorter In e.Sort
                'strsort += ", " & item.Property & " " & item.Direction.ToString
                strsort = item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter

            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            End If

            If String.IsNullOrEmpty(strWhereClause) Then
                strWhereClause = "StatusID IN (1)"
            Else
                strWhereClause &= " AND StatusID IN (1) "
            End If



            'To DO : Filter sesuai PIC
            If String.IsNullOrEmpty(strWhereClause) Then
                strWhereClause = "dbo.ufn_CheckIfStringExists('" & NawaBLL.Common.SessionCurrentUser.UserID & "',PICFilter)=1"
            Else
                strWhereClause &= " AND dbo.ufn_CheckIfStringExists('" & NawaBLL.Common.SessionCurrentUser.UserID & "',PICFilter)=1"
            End If


            Me.strOrder = strsort
            Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vStockOpname", "Doc_Number, Posting_Date, Product_Location_Type, Regional, Location, Doc_Date, Warehouse_By, SPV_Technician_By, Finance_By, ASMAN_Logistic_By, SOM_By, RSM_By, RH_By, Deviation", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("STR_Memo")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=STR_Memo.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=STR_Memo.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("STR_Memo")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=STR_Memo.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.Text) Then
                                    objtbl.Columns.Remove(item.Text)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=STR_Memo.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    'Public Sub BtnAdd_Click()
    <DirectMethod>
    Public Sub BtnAdd_Click()

        Try

            Dim Moduleid As String = Request.Params("ModuleID")


            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Parameterview_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub


    'Begin Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridCommandCustom(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            Dim ModuleID As String = Request.Params("ModuleID")
            Dim intModuleID As Integer

            intModuleID = NawaBLL.Common.DecryptQueryString(ModuleID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleID)

            Dim ID As String = e.ExtraParams(0).Value
            ID = NawaBLL.Common.EncryptQueryString(ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            If e.ExtraParams(1).Value = "ApprovalDetail" Then
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApprovalDetail & "?ID={0}&ModuleID={1}", ID, ModuleID), "Loading...")
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


End Class
