﻿Imports System.Data
Imports OfficeOpenXml
Imports NawaBLL


'******************************************************************************
'Major changes 20-Apr-2022 Adi :
'Alert grouped by CIF_No and ProcessDate
'Before : Alert grouped by CIF_No, ProcessDate, Alert_Type and FK_Rule_Basic_ID
'******************************************************************************

Partial Class CaseManagementAlertView

    Inherits Parent
    Public objFormModuleView As NawaBLL.FormModuleView

    Public Property strWhereClause() As String
        Get
            Return Session("CaseManagementAlertView.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertView.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("CaseManagementAlertView.strSort")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertView.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("CaseManagementAlertView.indexStart")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertView.indexStart") = value
        End Set
    End Property
    Public Property QueryTable() As String
        Get
            Return Session("CaseManagementAlertView.Table")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertView.Table") = value
        End Set
    End Property
    Public Property QueryField() As String
        Get
            Return Session("CaseManagementAlertView.Field")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertView.Field") = value
        End Set
    End Property

    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub


    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            'Begin Penambahan Advanced Filter
            If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                Toolbar2.Hidden = True
            Else
                Toolbar2.Hidden = False
            End If
            LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")
            'END Penambahan Advanced Filter


            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

            strfilter = strfilter.Replace("Active", objFormModuleView.ModuleName & ".Active")


            'Dim strsort As String = "PK_CaseManagement_ID ASC"
            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += ", " & item.Property & " " & item.Direction.ToString
            Next
            strsort = Mid(strsort, 3)

            Me.indexStart = intStart
            Me.strWhereClause = strfilter
            'Begin Penambahan Advanced Filter
            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            End If
            'END Penambahan Advanced Filter

            Me.strOrder = strsort

            'QueryTable = "vw_SIPENDAR_PROFILE"
            'QueryField = "*"
            'Dim DataPaging As DataTable = SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)

            Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)

            'Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = DLL.SQLHelper.ExecuteTabelPaging("CustomerInformation_WebTempTable", "CIFNo, Name, DateOfBirth, AccountOwnerId, OpeningDate, WorkingUnitName, IsCustomerInList", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Begin Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdd_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim sm As RowSelectionModel = TryCast(Me.GridpanelView.GetSelectionModel(), RowSelectionModel)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub CaseManagementAlertView_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub

    Private Sub CaseManagementAlertView_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer
            Try
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)


                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                    Dim strIDCode As String = 1
                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If

                objFormModuleView.ModuleID = objmodule.PK_Module_ID
                objFormModuleView.ModuleName = objmodule.ModuleName

                objFormModuleView.AddField("PK_CaseManagement_ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.BIGIDENTITY)
                objFormModuleView.AddField("ProcessDate", "Process Date", 2, False, True, NawaBLL.Common.MFieldType.DATETIMEValue, "dd-MMM-yyyy")
                objFormModuleView.AddField("CIF_NO", "CIF", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                'objFormModuleView.AddField("ACCOUNT_NO", "Account No.", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Customer_Name", "Customer Name", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                'objFormModuleView.AddField("Alert_Type", "Alert Type", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                'objFormModuleView.AddField("Case_Description", "Case Description", 7, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("CaseStatus", "Status", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                '20-Apr-2022 Adi : Add field Aging_Workdays
                objFormModuleView.AddField("Aging_Workdays", "Aging", 9, False, True, NawaBLL.Common.MFieldType.INTValue)
                'objFormModuleView.AddField("LastProposedAction", "Last Proposed Action", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Workflow_Step_Of", "Workflow Step", 11, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("PIC", "PIC", 12, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("FK_CaseStatus_ID", "Case Status ID", 13, False, False, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("STRGenerated", "STR Generated", 14, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                '31-Mar-2022 Adi : Add field RFISent
                objFormModuleView.AddField("RFISent", "RFI Sent", 15, False, True, NawaBLL.Common.MFieldType.VARCHARValue)

                objFormModuleView.SettingFormView()

                'Custom GridCommand
                Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
                Dim gridCommandResponse As New GridCommand

                objcommandcol.Width = 360
                'Dim objSendRFI As New GridCommand
                'objSendRFI.CommandName = "cmdSendEmailRFI"
                'objSendRFI.Icon = Icon.Email
                'objSendRFI.Text = "Send RFI"
                'objSendRFI.ToolTip.Text = ""
                'objcommandcol.Commands.Add(objSendRFI)

                Dim objPrintMemo As New GridCommand
                objPrintMemo.CommandName = "cmdPrintSTRMemo"
                objPrintMemo.Icon = Icon.Printer
                objPrintMemo.Text = "Print Memo"
                objPrintMemo.ToolTip.Text = ""
                objcommandcol.Commands.Add(objPrintMemo)

                Dim objGenerate As New GridCommand
                objGenerate.CommandName = "cmdGenerateSTR"
                objGenerate.Icon = Icon.ArrowRight
                objGenerate.Text = "Generate STR"
                objGenerate.ToolTip.Text = ""
                objcommandcol.Commands.Add(objGenerate)

                Dim extparam As New Ext.Net.Parameter
                extparam.Name = "unikkey"
                extparam.Value = "record.data.PK_SIPENDAR_PROFILE_ID"
                extparam.Mode = ParameterMode.Raw
                objcommandcol.DirectEvents.Command.ExtraParams.Add(extparam)

                objcommandcol.DirectEvents.Command.IsUpload = True
                objcommandcol.DirectEvents.Command.EventMask.ShowMask = True
                objcommandcol.DirectEvents.Command.EventMask.Msg = "Loading..."
                objcommandcol.DirectEvents.Command.Success = "NawadataDirect.Download_XML({isUpload : true});"

                Dim extParamCommandName As New Ext.Net.Parameter
                extparam.Name = "command"
                extparam.Value = "command"
                extparam.Mode = ParameterMode.Raw
                objcommandcol.DirectEvents.Command.ExtraParams.Add(extparam)

                'AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf SendEmailRFI
                AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf PrintSTRMemo
                AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf GenerateSTR

                'objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"
                '22-Feb-2022 Adi : Cek hak akses. Jika tidak ada hak akses Edit maka buttonnya hanya akan terlihat 3. Jadi index nya akan berubah saat disabled
                If objmodule IsNot Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, Common.ModuleActionEnum.Update) Then
                        objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection_Checker"
                    Else
                        objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"
                    End If
                End If

                'Get list of Alert Batch Closing authorized users
                Dim strQuery As String = "SELECT TOP 1 * FROM OneFcc_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=14"
                Dim drBatchClosingAuthorized As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Dim strCurrentUser As String = NawaBLL.Common.SessionCurrentUser.UserID

            Catch ex As Exception

            End Try

            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


#Region "Custom Grid Commands"
    Protected Sub SendEmailRFI(sender As Object, e As DirectEventArgs)
        Try
            If e.ExtraParams("command") = "cmdSendEmailRFI" Then
                'To Do

            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub PrintSTRMemo(sender As Object, e As DirectEventArgs)
        Try
            If e.ExtraParams("command") = "cmdPrintSTRMemo" Then
                'Cek apakah Case yang terjadi individu or korporasi
                Dim lngCaseID As Long = e.ExtraParams("unikkey")

                Dim strSQL As String = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & lngCaseID
                Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                Dim intCustomerType As Integer = 1  'Default Individual

                If drCM IsNot Nothing AndAlso Not IsDBNull(drCM("CIF_No")) Then
                    strSQL = "SELECT TOP 1 * FROM goAML_Ref_Customer WHERE CIF='" & drCM("CIF_No") & "'"
                    Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

                    If drCustomer IsNot Nothing AndAlso Not IsDBNull(drCustomer("FK_Customer_Type_ID")) Then
                        intCustomerType = drCustomer("FK_Customer_Type_ID")
                    End If
                End If

                'Print Preview
                Dim strModule As String = ""
                If intCustomerType = 1 Then
                    strModule = "CaseManagement - KertasKerjaIndividu"
                Else
                    strModule = "CaseManagement - KertasKerjaKorporasi"
                End If

                Dim cekModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName(strModule)
                If cekModule Is Nothing Then
                    Throw New Exception("Module " & strModule & " not exists!")
                End If

                Dim strModuleEncrypted As String = NawaBLL.Common.EncryptQueryString(cekModule.PK_Module_ID.ToString, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim strCaseIDEncrypted As String = NawaBLL.Common.EncryptQueryString(lngCaseID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                'Dim Script As String = "window.open('" & NawaBLL.Common.GetApplicationPath & "/AML/CaseManagement/Alert/CaseManagementAlertPrintMemo.aspx?ModuleID=" & strModuleEncrypted & "&CaseID=" & strCaseIDEncrypted & "');"
                Dim Script As String = "window.open('" & NawaBLL.Common.GetApplicationPath & "/AML/CaseManagement/Alert_BSIM/CaseManagementAlertPrintMemo.aspx?ModuleID=" & strModuleEncrypted & "&CaseID=" & strCaseIDEncrypted & "');"
                GridpanelView.AddScript(Script)
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GenerateSTR(sender As Object, e As DirectEventArgs)
        Try
            If e.ExtraParams("command") = "cmdGenerateSTR" Then
                'To Do
                'Add Felix 16-Feb-2022
                Dim objModuleGenerateSTR As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("OneFCC_CaseManagement_GenerateSTR")
                If objModuleGenerateSTR IsNot Nothing Then
                    Dim strEncModuleID As String = NawaBLL.Common.EncryptQueryString(objModuleGenerateSTR.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                    'session untuk parameter pk di view transaction
                    Session("GenerateReportSTRAdd_PK_GenerateSTRMenu") = Nothing
                    'Session("ViewTransactionSIPENDAR_Pengayaan_PK_DetailMenu") = Nothing
                    'Session("ViewTransactionSIPENDAR_Pengayaan_PK_EditMenu") = Nothing
                    'Get Parameter Profile ID
                    Dim pkCaseID As String = e.ExtraParams("unikkey")
                    Session("GenerateReportSTRAdd_PK") = pkCaseID

                    If InStr(objModuleGenerateSTR.UrlView, "?") > 0 Then
                        Session("GenerateReportSTR") = True
                        Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objModuleGenerateSTR.UrlView & "&ModuleID=" & strEncModuleID & "&CaseID=" & pkCaseID)
                    Else
                        Session("GenerateReportSTR") = True
                        Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objModuleGenerateSTR.UrlView & "?ModuleID=" & strEncModuleID & "&CaseID=" & pkCaseID)
                    End If
                Else
                    Throw New ApplicationException("Module OneFCC_CaseManagement_GenerateSTR is not exists!")
                End If
                ' End 16-Feb-2022
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

End Class
