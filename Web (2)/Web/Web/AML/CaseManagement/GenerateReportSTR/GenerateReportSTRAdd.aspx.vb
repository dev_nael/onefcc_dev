﻿Imports System.Data
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevDAL
Imports NawaBLL
Imports Ext
Imports NawaDevBLL
Imports System.Data.SqlClient
Imports NawaDAL
Imports Elmah
Imports System.IO
Imports OfficeOpenXml

Partial Class GenerateReportSTRAdd
    Inherits Parent

#Region "Session"
    Public objFormModuleView As FormModuleView
    Public Property strWhereClause() As String
        Get
            Return Session("GenerateReportSTRAdd.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("GenerateReportSTRAdd.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("GenerateReportSTRAdd.strSort")
        End Get
        Set(ByVal value As String)
            Session("GenerateReportSTRAdd.strSort") = value
        End Set
    End Property

    Public Property indexStart() As String
        Get
            Return Session("GenerateReportSTRAdd.indexStart")
        End Get
        Set(ByVal value As String)
            Session("GenerateReportSTRAdd.indexStart") = value
        End Set
    End Property
    'Private ObjEODTask As NawaDevBLL.EODTaskBLL
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("GenerateReportSTRAdd.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("GenerateReportSTRAdd.ObjModule") = value
        End Set
    End Property
    Public Property ListTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi)
        Get
            If Session("GenerateReportSTRAdd.ListTransaction") Is Nothing Then
                Session("GenerateReportSTRAdd.ListTransaction") = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
            End If
            Return Session("GenerateReportSTRAdd.ListTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Transaksi))
            Session("GenerateReportSTRAdd.ListTransaction") = value
        End Set
    End Property
    Public Property ListSelectedTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi)
        Get
            If Session("GenerateReportSTRAdd.ListSelectedTransaction") Is Nothing Then
                Session("GenerateReportSTRAdd.ListSelectedTransaction") = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
            End If
            Return Session("GenerateReportSTRAdd.ListSelectedTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Transaksi))
            Session("GenerateReportSTRAdd.ListSelectedTransaction") = value
        End Set
    End Property
    Public Property ListSendTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi)
        Get
            If Session("GenerateReportSTRAdd.ListSendTransaction") Is Nothing Then
                Session("GenerateReportSTRAdd.ListSendTransaction") = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
            End If
            Return Session("GenerateReportSTRAdd.ListSendTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Transaksi))
            Session("GenerateReportSTRAdd.ListSendTransaction") = value
        End Set
    End Property
    Public Property ListGenerateSarTransaction As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction)
        Get
            If Session("GenerateReportSTRAdd.ListGenerateSarTransaction") Is Nothing Then
                Session("GenerateReportSTRAdd.ListGenerateSarTransaction") = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction)
            End If
            Return Session("GenerateReportSTRAdd.ListGenerateSarTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction))
            Session("GenerateReportSTRAdd.ListGenerateSarTransaction") = value
        End Set
    End Property
    Public Property objGenerateSAR As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR)
        Get
            If Session("GenerateReportSTRAdd.objGenerateSAR") Is Nothing Then
                Session("GenerateReportSTRAdd.objGenerateSAR") = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR)
            End If
            Return Session("GenerateReportSTRAdd.objGenerateSAR")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR))
            Session("GenerateReportSTRAdd.objGenerateSAR") = value
        End Set
    End Property
    Public Property ObjSARData As NawaDevBLL.GenerateSARData
        Get
            Return Session("GenerateReportSTRAdd.ObjSARData")
        End Get
        Set(ByVal value As NawaDevBLL.GenerateSARData)
            Session("GenerateReportSTRAdd.ObjSARData") = value
        End Set
    End Property
    Public Property ListIndicator As List(Of NawaDevDAL.goAML_Report_Indicator)
        Get
            Return Session("GenerateReportSTRAdd.ListIndicator")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Report_Indicator))
            Session("GenerateReportSTRAdd.ListIndicator") = value
        End Set
    End Property
    Public Property ListDokumen As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment)
        Get
            Return Session("GenerateReportSTRAdd.ListDokumen")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment))
            Session("GenerateReportSTRAdd.ListDokumen") = value
        End Set
    End Property

    Public Property IDIndicator As String
        Get
            Return Session("GenerateReportSTRAdd.IDIndicator")
        End Get
        Set(value As String)
            Session("GenerateReportSTRAdd.IDIndicator") = value
        End Set
    End Property

    Public Property IDDokumen As String
        Get
            Return Session("GenerateReportSTRAdd.IDDokumen")
        End Get
        Set(value As String)
            Session("GenerateReportSTRAdd.IDDokumen") = value
        End Set
    End Property

    '10-Feb-2022 
    Public Property CaseID() As String
        Get
            Return Session("GenerateReportSTRAdd.CaseID")
        End Get
        Set(ByVal value As String)
            Session("GenerateReportSTRAdd.CaseID") = value
        End Set
    End Property

    '24-Aug-2022 Adi : Tambah untuk Generate SAR
    Public Property CIF_NO() As String
        Get
            Return Session("GenerateReportSTRAdd.CIF_NO")
        End Get
        Set(ByVal value As String)
            Session("GenerateReportSTRAdd.CIF_NO") = value
        End Set
    End Property

    Public Property ACT_ACCOUNT_NO As String
        Get
            Return Session("GenerateReportSTRAdd.ACT_ACCOUNT_NO")
        End Get
        Set(value As String)
            Session("GenerateReportSTRAdd.ACT_ACCOUNT_NO") = value
        End Set
    End Property

    Public Property dtActivity As DataTable
        Get
            Return Session("GenerateReportSTRAdd.dtActivity")
        End Get
        Set(value As DataTable)
            Session("GenerateReportSTRAdd.dtActivity") = value
        End Set
    End Property
    'End of 24-Aug-2022 Adi : Tambah untuk Generate SAR

#End Region

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                Dim strmodule As String = Request.Params("ModuleID")
                ClearSession()
                Dim intmodule As Integer = NawaBLL.Common.DecryptQueryString(strmodule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intmodule)

                If Not ObjModule Is Nothing Then
                    If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                        Dim strIDCode As String = 1
                        strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                        Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    'FormPanelInput.Title = ObjModule.ModuleLabel & " Add"
                    FormPanelInput.Title = "Generate STR/SAR From Case Management"

                    'Panelconfirmation.Title = ObjModule.ModuleLabel & " Add"
                    'StoreDetailType.Reload()laporan dengan

                    'Dim objrand As New Random
                    'ObjTask.PK_EODTask_ID = objrand.Next

                    'LoadStoreComboBox()
                    sar_DateFrom.Value = Date.Now
                    sar_DateTo.Value = Date.Now
                    FileDoc.FieldStyle = "background-color: #FFE4C4"
                    NoRefPPATK.FieldStyle = "background-color: #FFE4C4"
                    sar_reportIndicator.FieldStyle = "background-color: #FFE4C4"
                    ColumnActionLocation()
                    PopupMaximizale()

                    LoadCaseInfo()

                    '24-Aug-2022 Adi : Hide dulu panel2 STR Reportnya
                    fs_STRSAR_General.Hidden = True
                    pnlTransaction.Hidden = True
                    pnlActivity.Hidden = True
                    PanelReportIndicator.Hidden = True
                    PanelAttachment.Hidden = True

                Else
                    Throw New Exception("Invalid Module ID")
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
    '    ObjEODTask = New NawaDevBLL.EODTaskBLL(FormPanelInput)
    'End Sub
#End Region

#Region "Method"
    Sub ColumnActionLocation()
        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 1 Then
            'GridPanelReportIndicator.ColumnModel.Columns.Add(CommandColumn87)
        ElseIf bsettingRight = 2 Then
            GridPanelReportIndicator.ColumnModel.Columns.RemoveAt(GridPanelReportIndicator.ColumnModel.Columns.Count - 1)
            GridPanelReportIndicator.ColumnModel.Columns.Insert(1, CommandColumn87)

            GridPanelAttachment.ColumnModel.Columns.RemoveAt(GridPanelAttachment.ColumnModel.Columns.Count - 1)
            GridPanelAttachment.ColumnModel.Columns.Insert(1, CommandColumnAttachment)
        End If

    End Sub

    Sub PopupMaximizale()
        WindowReportIndicator.Maximizable = True
        WindowAttachment.Maximizable = True
    End Sub
    Private Sub ClearSession()
        ListTransaction = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
        ListSelectedTransaction = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
        ListSendTransaction = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
        objGenerateSAR = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR)
        ListIndicator = New List(Of NawaDevDAL.goAML_Report_Indicator)
        ObjSARData = New NawaDevBLL.GenerateSARData
        ListGenerateSarTransaction = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction)
        ListDokumen = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment)

        dd_TrnFilterBy.SetTextValue("")
    End Sub
    'Sub LoadStoreComboBox()
    '    sar_jenisLaporan.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
    '    StoreJenisLaporan.Reload()

    '    sar_reportIndicator.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
    '    StoreReportIndicator.Reload()

    '    sar_CIF.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
    '    StoreCIF.Reload()
    'End Sub
    Protected Sub CIF_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Customer", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub ReportIndicator_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goaml_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub JenisLaporan_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If

            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            '25-Aug-2022 Adi : Tambahkan logic untuk filter transaksi/aktivitas
            Dim strFilterTRNorACT As String = ""
            If cboReportAs.SelectedItem.Value = "STR" Then
                strFilterTRNorACT = "TRNorACT='TRN'"
            Else
                strFilterTRNorACT = "TRNorACT='ACT'"
            End If

            If strfilter.Length > 0 Then
                strfilter &= " and " & strFilterTRNorACT
            Else
                strfilter &= strFilterTRNorACT
            End If
            'End of 25-Aug-2022 Adi : Tambahkan logic untuk filter transaksi/aktivitas


            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Jenis_Laporan_STR", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Sub bindTransaction(store As Ext.Net.Store, listTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listTransaction)
        objtable.Columns.Add(New DataColumn("Valid", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoRefTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("TipeTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("LokasiTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("KeteranganTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("DateTransaction", GetType(Date)))
        objtable.Columns.Add(New DataColumn("AccountNo", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaTeller", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaPejabat", GetType(String)))
        objtable.Columns.Add(New DataColumn("TglPembukuan", GetType(Date)))
        objtable.Columns.Add(New DataColumn("CaraTranDilakukan", GetType(String)))
        objtable.Columns.Add(New DataColumn("CaraTranLain", GetType(String)))
        objtable.Columns.Add(New DataColumn("DebitCredit", GetType(String)))
        objtable.Columns.Add(New DataColumn("OriginalAmount", GetType(String)))
        objtable.Columns.Add(New DataColumn("IDR", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim CifLawan As String = item("CIF_No_Lawan").ToString
                Dim WicLawan As String = item("WIC_No_Lawan").ToString
                Dim AccountLawan As String = item("ACCOUNT_No_Lawan").ToString
                Dim BiMulti As String = item("BiMultiParty").ToString
                If CifLawan = "" And WicLawan = "" And AccountLawan = "" And BiMulti = "1" Then
                    item("Valid") = "Tidak"
                Else
                    item("Valid") = "Ya"
                End If
                item("NoTran") = item("Transaction_Number")
                item("NoRefTran") = item("Ref_Num")
                Dim SourceData As String = item("Source_Data").ToString
                Dim SourceDataTemp As Integer
                If Integer.TryParse(SourceData, SourceDataTemp) Then
                    If SourceData <> "" Then
                        item("TipeTran") = NawaDevBLL.GenerateSarBLL.getSourceData(SourceData)
                    End If
                Else
                    item("TipeTran") = SourceData
                End If
                item("LokasiTran") = item("Transaction_Location")
                item("KeteranganTran") = item("Transaction_Remark")
                item("DateTransaction") = item("Date_Transaction")
                item("AccountNo") = item("Account_NO")
                item("NamaTeller") = item("Teller")
                item("NamaPejabat") = item("Authorized")
                item("TglPembukuan") = item("Date_Posting")
                item("CaraTranDilakukan") = item("Transmode_Code")
                item("CaraTranLain") = item("Transmode_Comment")
                item("DebitCredit") = item("Debit_Credit")
                If item("Original_Amount").ToString <> "" Then
                    Dim OriginalAmount As Decimal = item("Original_Amount").ToString
                    item("OriginalAmount") = OriginalAmount.ToString("#,###.00")
                End If
                If item("IDR_Amount").ToString <> "" Then
                    Dim IDR As Decimal = item("IDR_Amount").ToString
                    item("IDR") = IDR.ToString("#,###.00")
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAttachment(store As Ext.Net.Store, listAttachment As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAttachment)
        objtable.Columns.Add(New DataColumn("FileName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("FileName") = item("File_Name")
                item("Keterangan") = item("Remarks")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindReportIndicator(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Report_Indicator))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        '  objtable.Columns.Add(New DataColumn("Kode_Indicator", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                '     item("Kode_Indicator") = item("FK_Indicator")
                item("Keterangan") = getReportIndicatorByKode(item("FK_Indicator").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Shared Function getReportIndicatorByKode(Kode As String) As String
        Dim strKategori As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim kategori As NawaDevDAL.goAML_Ref_Indikator_Laporan = objdb.goAML_Ref_Indikator_Laporan.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If kategori IsNot Nothing Then
                strKategori = kategori.Keterangan
            End If
        End Using
        Return strKategori
    End Function


    Sub editIndicator(id As String, command As String)
        Try
            sar_reportIndicator.Clear()

            WindowReportIndicator.Hidden = False
            Dim reportIndicator As NawaDevDAL.goAML_Report_Indicator
            reportIndicator = ListIndicator.Where(Function(x) x.PK_Report_Indicator = id).FirstOrDefault


            sar_reportIndicator.SetValue(reportIndicator.FK_Indicator)
            If command = "Detail" Then
                sar_reportIndicator.Selectable = False
                BtnsaveReportIndicator.Hidden = True
            Else
                sar_reportIndicator.Selectable = True
                BtnsaveReportIndicator.Hidden = False
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub editDokumen(id As String, command As String)
        Try
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()

            WindowAttachment.Hidden = False
            Dim Dokumen As New NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment
            Dokumen = ListDokumen.Where(Function(x) x.PK_ID = id).FirstOrDefault

            txtFileName.Text = Dokumen.File_Name
            txtKeterangan.Text = Dokumen.Remarks

            If command = "Detail" Then
                FileDoc.Hidden = True
                BtnsaveAttachment.Hidden = True
                txtKeterangan.Editable = False
            Else
                FileDoc.Hidden = False
                BtnsaveAttachment.Hidden = False
                txtKeterangan.Editable = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DownloadFile(id As Long)
        Dim objdownload As NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment = ListDokumen.Find(Function(x) x.PK_ID = id)
        If Not objdownload Is Nothing Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & objdownload.File_Name)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            'Response.ContentType = "ContentType"
            Response.ContentType = MimeMapping.GetMimeMapping(objdownload.File_Name)
            Response.BinaryWrite(objdownload.File_Doc)
            Response.End()
        End If
    End Sub

    Protected Sub CreateExcel2007WithData(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try


            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            'Dim intTotalRow As Long
            Dim objSchemaModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(12226)
            Dim objSchemaModuleField As List(Of NawaDAL.ModuleField) = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(12226)
            Dim objSchemaModuleFieldDefault As New List(Of NawaDAL.ModuleFieldDefault)
            Dim objmdl As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(12226)

            Dim cif As String = txt_CIFNo.Value
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate

            Dim objList = NawaDevBLL.GenerateSarBLL.getListTransactionBySearch(cif, datefrom, dateto)



            objFormModuleView = New FormModuleView(GridPaneldetail, BtnExport)
            objFormModuleView.ModuleID = objmdl.PK_Module_ID
            objFormModuleView.ModuleName = objmdl.ModuleName

            Dim delimitercheckboxparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(39)
            Dim strdelimiter As String = ";"
            If Not delimitercheckboxparam Is Nothing Then
                strdelimiter = delimitercheckboxparam.SettingValue
            End If

            Dim objtb As DataTable = NawaBLL.Common.CopyGenericToDataTable(ListTransaction)
            Dim intmaxrow As Integer
            If objtb.Rows.Count + 1000 > 1048575 Then
                intmaxrow = 1048575
            Else
                intmaxrow = objtb.Rows.Count + 1000
            End If


            Using objtbl As Data.DataTable = objtb
                'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                objtbl.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                Dim intTotalPage As Integer = (intmaxrow + 1048575 - 1) / 1048575
                Dim dsTable As New Data.DataSet

                If intTotalPage = 1 Then
                    'objFormModuleView.changeHeader(objtbl)

                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    DataSetToExcel(dsTable, objfileinfo)

                    dsTable.Dispose()
                    dsTable = Nothing



                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If



                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")



                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()
                ElseIf intTotalPage > 1 Then
                    'objFormModuleView.changeHeader(objtbl)



                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    'ambil page 2 dst sampe page terakhir
                    For index = 1 To intTotalPage - 2
                        Using objtbltemp As Data.DataTable = objtb
                            objtbltemp.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                            dsTable.Tables.Add(objtbltemp.Copy)
                        End Using
                    Next
                    DataSetToExcel(dsTable, objfileinfo)
                    dsTable.Dispose()
                    dsTable = Nothing

                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If
                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()

                End If
            End Using

        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub
    Protected Sub CreateExcel2007WithDataNoChecked(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try


            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            'Dim intTotalRow As Long
            Dim objSchemaModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(12226)
            Dim objSchemaModuleField As List(Of NawaDAL.ModuleField) = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(12226)
            Dim objSchemaModuleFieldDefault As New List(Of NawaDAL.ModuleFieldDefault)
            Dim objmdl As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(12226)

            Dim cif As String = txt_CIFNo.Value
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate

            Dim objList = NawaDevBLL.GenerateSarBLL.getListTransactionBySearch(cif, datefrom, dateto)



            objFormModuleView = New FormModuleView(GridPanel1, BtnExportNoChecked)
            objFormModuleView.ModuleID = objmdl.PK_Module_ID
            objFormModuleView.ModuleName = objmdl.ModuleName

            Dim delimitercheckboxparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(39)
            Dim strdelimiter As String = ";"
            If Not delimitercheckboxparam Is Nothing Then
                strdelimiter = delimitercheckboxparam.SettingValue
            End If

            Dim objtb As DataTable = NawaBLL.Common.CopyGenericToDataTable(ListTransaction)
            Dim intmaxrow As Integer
            If objtb.Rows.Count + 1000 > 1048575 Then
                intmaxrow = 1048575
            Else
                intmaxrow = objtb.Rows.Count + 1000
            End If


            Using objtbl As Data.DataTable = objtb
                'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                objtbl.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                Dim intTotalPage As Integer = (intmaxrow + 1048575 - 1) / 1048575
                Dim dsTable As New Data.DataSet

                If intTotalPage = 1 Then
                    'objFormModuleView.changeHeader(objtbl)

                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    DataSetToExcel(dsTable, objfileinfo)

                    dsTable.Dispose()
                    dsTable = Nothing



                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If



                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")



                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()
                ElseIf intTotalPage > 1 Then
                    'objFormModuleView.changeHeader(objtbl)



                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    'ambil page 2 dst sampe page terakhir
                    For index = 1 To intTotalPage - 2
                        Using objtbltemp As Data.DataTable = objtb
                            objtbltemp.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                            dsTable.Tables.Add(objtbltemp.Copy)
                        End Using
                    Next
                    DataSetToExcel(dsTable, objfileinfo)
                    dsTable.Dispose()
                    dsTable = Nothing

                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If
                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()

                End If
            End Using

        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub
    Private Shared Sub DataSetToExcel(ByVal dataSet As DataSet, ByVal objfileinfo As FileInfo)
        Using pck As ExcelPackage = New ExcelPackage()

            For Each dataTable As DataTable In dataSet.Tables
                Dim workSheet As ExcelWorksheet = pck.Workbook.Worksheets.Add(dataTable.TableName)
                workSheet.Cells("A1").LoadFromDataTable(dataTable, True)

                Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                Dim intcolnumber As Integer = 1
                For Each item As System.Data.DataColumn In dataTable.Columns
                    If item.DataType = GetType(Date) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = dateformat
                    End If
                    If item.DataType = GetType(Integer) Or item.DataType = GetType(Double) Or item.DataType = GetType(Long) Or item.DataType = GetType(Decimal) Or item.DataType = GetType(Single) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = "#,##0.00"
                    End If

                    intcolnumber = intcolnumber + 1
                Next
                workSheet.Cells(workSheet.Dimension.Address).AutoFitColumns()
            Next

            pck.SaveAs(objfileinfo)
        End Using
    End Sub

    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

            'strfilter = strfilter.Replace("Active", objFormModuleView.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter

            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            End If

            Me.strOrder = strsort
            If strsort = "" Then
                strsort = "NO_ID asc"
            End If

            Dim DataPaging As DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)

            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If

            e.Total = inttotalRecord
            GridPaneldetail.GetStore.DataSource = DataPaging
            GridPaneldetail.GetStore.DataBind()


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Direct Event"
    Protected Sub OnSelect_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim data As New goAML_ODM_Transaksi
            Dim idx As String
            idx = e.ExtraParams(0).Value

            data.NO_ID = e.ExtraParams(0).Value

            ListSelectedTransaction.Add(data)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub OnDeSelect_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim data As New goAML_ODM_Transaksi

            For Each item In ListSelectedTransaction
                If item.NO_ID = CInt(e.ExtraParams(0).Value) Then
                    data = item
                    Exit For
                End If
            Next

            ListSelectedTransaction.Remove(data)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString("30122", NawaBLL.SystemParameterBLL.GetEncriptionKey) ''Request.Params("ModuleID") '' 24-Feb-2022 kembali ke module vw_OneFCC_CaseManagement
            Dim ObjModuleHeader = NawaBLL.ModuleBLL.GetModuleByModuleID(30122)

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModuleHeader.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnGenerate_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim ListSar As New List(Of goAML_ODM_Generate_STR_SAR)
            Dim ListODMTransactionIncomplete As New List(Of goAML_ODM_Transaksi)
            Dim ListTransactionIncomplete As New List(Of goAML_ODM_Generate_STR_SAR_Transaction)
            Dim getIsGenerateSAR As String = NawaDevBLL.GenerateSarBLL.getIsGenerateSAR("IsGenerateSAR")
            Dim ListTransactionNoTrnMode As New List(Of goAML_ODM_Generate_STR_SAR_Transaction)
            Dim ListODMTransactionNoTrnMode As New List(Of goAML_ODM_Transaksi)
            Dim dateReport As Date = TanggalLaporan.SelectedDate
            Dim maxDate As DateTime = DateTime.Now

            'Validate
            If sar_jenisLaporan.Value = "LTKMP" And NoRefPPATK.Text = "" Then
                Throw New ApplicationException("Jika Jenis Laporannya LTKMP maka No Ref PPATK Tidak Boleh Kosong")
            ElseIf dateReport <> DateTime.MinValue AndAlso dateReport > maxDate Then
                Throw New Exception("Tanngal Laporan : Can not select future date")
            End If
            If ListIndicator.Count = 0 Then
                Throw New ApplicationException("Indikator Tidak Boleh Kosong")
            End If

            Dim GenerateSAR As New goAML_ODM_Generate_STR_SAR
            With GenerateSAR
                .CIF_NO = txt_CIFNo.Value
                .Transaction_Code = sar_jenisLaporan.SelectedItem.Value
                .Date_Report = TanggalLaporan.Value
                .Fiu_Ref_Number = NoRefPPATK.Text
                .Reason = alasan.Text
                .total_transaction = ListSelectedTransaction.Count
                .Fk_Report_ID = 0
                .status = 1
                If ListTransaction.Count = ListSelectedTransaction.Count Or sar_IsSelectedAll.Value Then
                    .IsSelectedAll = True
                Else
                    .IsSelectedAll = False
                End If

                If ListIndicator.Count > 0 Then
                    Dim i As Integer
                    Dim temp As String
                    i = 0
                    temp = ""
                    For Each item In ListIndicator
                        i += 1
                        If ListIndicator.Count = 1 Then
                            'temp = getReportIndicatorByKode(item.FK_Indicator)
                            temp = item.FK_Indicator
                        Else
                            If i = 1 Then
                                'temp = getReportIndicatorByKode(item.FK_Indicator)
                                temp = item.FK_Indicator
                            Else
                                'temp += "," + getReportIndicatorByKode(item.FK_Indicator)
                                temp += "," + item.FK_Indicator
                            End If
                        End If
                    Next
                    .indicator = temp
                End If
            End With

            '24-Aug-2022 Adi : Tambah Opsi untuk Generate SAR (Activity) - Set Transaction Total to 0
            If cboReportAs.SelectedItem.Value = "SAR" Then     'Generate as SAR
                GenerateSAR.total_transaction = 0
            End If
            'End of 24-Aug-2022 Adi : Tambah Opsi untuk Generate SAR (Activity) - Set Transaction Total to 0

            ObjSARData.objGenerateSAR = GenerateSAR

            '' Remark 07-Mar-2022. Insert terus aja , karena di Processnya dia by SAR_ID
            '' Edit Felix 23-Feb-2022, biar user bisa generate ulang
            'ListSar = NawaDevBLL.GenerateSarBLL.getListSar(GenerateSAR.CIF_NO, GenerateSAR.Transaction_Code, GenerateSAR.Date_Report)
            'If ListSar.Count > 0 Then
            '    Throw New ApplicationException("Laporan Dengan CIF: " & GenerateSAR.CIF_NO & ", Jenis Laporan: " & GenerateSAR.Transaction_Code & " dan Tanggal Laporan: " & GenerateSAR.Date_Report.Value.ToString("dd-MMM-yyyy") & " Sudah Ada.")
            'End If

            'Dim paramDeleteOdmSAR(2) As SqlParameter

            'paramDeleteOdmSAR(0) = New SqlParameter
            'paramDeleteOdmSAR(0).ParameterName = "@TransactionCode"
            'paramDeleteOdmSAR(0).Value = GenerateSAR.Transaction_Code
            'paramDeleteOdmSAR(0).DbType = SqlDbType.VarChar

            'paramDeleteOdmSAR(1) = New SqlParameter
            'paramDeleteOdmSAR(1).ParameterName = "@cif"
            'paramDeleteOdmSAR(1).Value = GenerateSAR.CIF_NO
            'paramDeleteOdmSAR(1).DbType = SqlDbType.VarChar

            'paramDeleteOdmSAR(2) = New SqlParameter
            'paramDeleteOdmSAR(2).ParameterName = "@ReportDate"
            'paramDeleteOdmSAR(2).Value = GenerateSAR.Date_Report.Value.ToString("yyyy-MM-dd")
            'paramDeleteOdmSAR(2).DbType = SqlDbType.VarChar

            'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_OneFCC_DeleteSARForCaseAlert", paramDeleteOdmSAR)
            '' End 23-Feb-2022
            '' End 07-Mar-2022. Insert terus aja , karena di Processnya dia by SAR_ID

            For Each item In ListDokumen
                ObjSARData.listObjDokumen.Add(item)
            Next


            '24-Aug-2022 Adi : Tambah Opsi untuk Generate SAR (Activity)
            If cboReportAs.SelectedItem.Value = "STR" Then     'Generate as STR
                If GenerateSAR.IsSelectedAll = False Then
                    If ListSelectedTransaction.Count = 0 Then
                        Throw New ApplicationException("Tidak Ada Transaksi yang dipilih")
                    End If

                    Dim objODMTransaksi As New goAML_ODM_Transaksi
                    Dim objSARTransaksi As New goAML_ODM_Generate_STR_SAR_Transaction

                    ObjSARData.listObjGenerateSARTransaction = New List(Of goAML_ODM_Generate_STR_SAR_Transaction)

                    For Each item In ListSelectedTransaction
                        objODMTransaksi = ListTransaction.Where(Function(x) x.NO_ID = item.NO_ID).FirstOrDefault

                        With objSARTransaksi
                            .Date_Transaction = objODMTransaksi.Date_Transaction
                            .CIF_NO = objODMTransaksi.CIF_NO
                            .Account_NO = objODMTransaksi.Account_NO
                            .WIC_NO = objODMTransaksi.WIC_No
                            .Ref_Num = objODMTransaksi.Ref_Num
                            .Debit_Credit = objODMTransaksi.Debit_Credit
                            .Original_Amount = objODMTransaksi.Original_Amount
                            .Currency = objODMTransaksi.Currency
                            .Exchange_Rate = objODMTransaksi.Exchange_Rate
                            .IDR_Amount = objODMTransaksi.IDR_Amount
                            .Transaction_Code = objODMTransaksi.Transaction_Code
                            .Source_Data = objODMTransaksi.Source_Data
                            .Transaction_Remark = objODMTransaksi.Transaction_Remark
                            .Transaction_Number = objODMTransaksi.Transaction_Number
                            .Transaction_Location = objODMTransaksi.Transaction_Location
                            .Teller = objODMTransaksi.Teller
                            .Authorized = objODMTransaksi.Authorized
                            .Date_Posting = objODMTransaksi.Date_Posting
                            .Transmode_Code = objODMTransaksi.Transmode_Code
                            .Transmode_Comment = objODMTransaksi.Transmode_Comment
                            .Comments = objODMTransaksi.Comments
                            .CIF_No_Lawan = objODMTransaksi.CIF_No_Lawan
                            .ACCOUNT_No_Lawan = objODMTransaksi.ACCOUNT_No_Lawan
                            .WIC_No_Lawan = objODMTransaksi.WIC_No_Lawan
                            .Conductor_ID = objODMTransaksi.Conductor_ID
                            .Swift_Code_Lawan = objODMTransaksi.Swift_Code_Lawan
                            .country_code_lawan = objODMTransaksi.Country_Code_Lawan
                            .MsgTypeSwift = objODMTransaksi.MsgSwiftType
                            .From_Funds_Code = objODMTransaksi.From_Funds_Code
                            .To_Funds_Code = objODMTransaksi.To_Funds_Code
                            .country_code = objODMTransaksi.Country_Code
                            .Currency_Lawan = objODMTransaksi.Currency_Lawan
                            If objODMTransaksi.BiMultiParty.HasValue Then
                                .BiMultiParty = objODMTransaksi.BiMultiParty
                            End If
                            .GCN = objODMTransaksi.GCN
                            .GCN_Lawan = objODMTransaksi.GCN_Lawan
                            .Active = 1
                            .Business_date = objODMTransaksi.Business_date
                            If objODMTransaksi.From_Type.HasValue Then
                                .From_Type = objODMTransaksi.From_Type
                            End If
                            If objODMTransaksi.To_Type.HasValue Then
                                .To_Type = objODMTransaksi.To_Type
                            End If
                        End With

                        ObjSARData.listObjGenerateSARTransaction.Add(objSARTransaksi)

                        objODMTransaksi = New goAML_ODM_Transaksi
                        objSARTransaksi = New goAML_ODM_Generate_STR_SAR_Transaction
                    Next

                    If getIsGenerateSAR = "1" Then
                        ListTransactionIncomplete = ObjSARData.listObjGenerateSARTransaction.Where(Function(x) x.BiMultiParty = 1 And x.CIF_No_Lawan Is Nothing And x.WIC_No_Lawan Is Nothing And x.ACCOUNT_No_Lawan Is Nothing).ToList
                        If ListTransactionIncomplete.Count > 0 Then
                            Throw New ApplicationException("Any Counter Party of Transaction is not completed")
                        End If
                    End If

                    ListTransactionNoTrnMode = ObjSARData.listObjGenerateSARTransaction.Where(Function(x) x.Transmode_Code = "" Or x.Transmode_Code Is Nothing).ToList
                    If ListTransactionNoTrnMode.Count > 0 Then
                        Throw New ApplicationException("Cara Tranaksi Dilakukan Kosong Di " + ListTransactionNoTrnMode.Count.ToString + " Transaksi")
                    End If

                ElseIf GenerateSAR.IsSelectedAll = True Then
                    '' add 23-Feb-2022
                    'Dim objODMTransaksi As New goAML_ODM_Transaksi
                    Dim objSARTransaksi As New goAML_ODM_Generate_STR_SAR_Transaction
                    For Each item In ListTransaction
                        With objSARTransaksi
                            .Date_Transaction = item.Date_Transaction
                            .CIF_NO = item.CIF_NO
                            .Account_NO = item.Account_NO
                            .WIC_NO = item.WIC_No
                            .Ref_Num = item.Ref_Num
                            .Debit_Credit = item.Debit_Credit
                            .Original_Amount = item.Original_Amount
                            .Currency = item.Currency
                            .Exchange_Rate = item.Exchange_Rate
                            .IDR_Amount = item.IDR_Amount
                            .Transaction_Code = item.Transaction_Code
                            .Source_Data = item.Source_Data
                            .Transaction_Remark = item.Transaction_Remark
                            .Transaction_Number = item.Transaction_Number
                            .Transaction_Location = item.Transaction_Location
                            .Teller = item.Teller
                            .Authorized = item.Authorized
                            .Date_Posting = item.Date_Posting
                            .Transmode_Code = item.Transmode_Code
                            .Transmode_Comment = item.Transmode_Comment
                            .Comments = item.Comments
                            .CIF_No_Lawan = item.CIF_No_Lawan
                            .ACCOUNT_No_Lawan = item.ACCOUNT_No_Lawan
                            .WIC_No_Lawan = item.WIC_No_Lawan
                            .Conductor_ID = item.Conductor_ID
                            .Swift_Code_Lawan = item.Swift_Code_Lawan
                            .country_code_lawan = item.Country_Code_Lawan
                            .MsgTypeSwift = item.MsgSwiftType
                            .From_Funds_Code = item.From_Funds_Code
                            .To_Funds_Code = item.To_Funds_Code
                            .country_code = item.Country_Code
                            .Currency_Lawan = item.Currency_Lawan
                            If item.BiMultiParty.HasValue Then
                                .BiMultiParty = item.BiMultiParty
                            End If
                            .GCN = item.GCN
                            .GCN_Lawan = item.GCN_Lawan
                            .Active = 1
                            .Business_date = item.Business_date
                            If item.From_Type.HasValue Then
                                .From_Type = item.From_Type
                            End If
                            If item.To_Type.HasValue Then
                                .To_Type = item.To_Type
                            End If
                        End With

                        ObjSARData.listObjGenerateSARTransaction.Add(objSARTransaksi)

                        'objODMTransaksi = New goAML_ODM_Transaksi
                        objSARTransaksi = New goAML_ODM_Generate_STR_SAR_Transaction
                    Next
                    '' End 23-Feb-2022

                    If ListTransaction.Count = 0 Then
                        Throw New ApplicationException("Tidak Ada Transaksi")
                    End If
                    If getIsGenerateSAR = "1" Then
                        ListODMTransactionIncomplete = ListTransaction.Where(Function(x) x.BiMultiParty = 1 And x.CIF_No_Lawan Is Nothing And x.WIC_No_Lawan Is Nothing And x.ACCOUNT_No_Lawan Is Nothing).ToList
                        If ListODMTransactionIncomplete.Count > 0 Then
                            Throw New ApplicationException("Any Counter Party of Transaction is not completed")
                        End If
                    End If

                    ListODMTransactionNoTrnMode = ListTransaction.Where(Function(x) x.Transmode_Code = "" Or x.Transmode_Code Is Nothing).ToList
                    If ListODMTransactionNoTrnMode.Count > 0 Then
                        Throw New ApplicationException("Cara Tranaksi Dilakukan Kosong Di " + ListODMTransactionNoTrnMode.Count.ToString + " Transaksi")
                    End If
                End If

                '' Edit 16-Feb-2022, Untuk Case Alert, Tanpa Approval

                Dim DateFrom As DateTime = sar_DateFrom.Value
                Dim DateTo As DateTime = sar_DateTo.Value
                SaveGenerateSTRTanpaApproval(ObjSARData, ObjModule, 1, DateFrom, DateTo)
            Else    'Generate as SAR

                'Validate
                If dtActivity Is Nothing OrElse dtActivity.Rows.Count = 0 Then
                    Throw New ApplicationException("Minimum 1 activity needed to generate SAR Report.")
                End If

                'Save to DB and Create goAML Report
                SaveGenerateSARTanpaApproval(ObjSARData, ObjModule, 1)
            End If

            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True
            LblConfirmation.Text = "Data Saved into Database."

            '' Add Felix 23-Feb-2022, insert ke History Generate
            Dim paramInsertHistoryGenerate(2) As SqlParameter

            paramInsertHistoryGenerate(0) = New SqlParameter
            paramInsertHistoryGenerate(0).ParameterName = "@CaseID"
            paramInsertHistoryGenerate(0).Value = CaseID
            paramInsertHistoryGenerate(0).DbType = SqlDbType.VarChar

            paramInsertHistoryGenerate(1) = New SqlParameter
            paramInsertHistoryGenerate(1).ParameterName = "@userID"
            paramInsertHistoryGenerate(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
            paramInsertHistoryGenerate(1).DbType = SqlDbType.VarChar

            '' Add 07-Mar-2022
            paramInsertHistoryGenerate(2) = New SqlParameter
            paramInsertHistoryGenerate(2).ParameterName = "@SARID"
            paramInsertHistoryGenerate(2).Value = ObjSARData.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR
            paramInsertHistoryGenerate(2).DbType = SqlDbType.VarChar
            '' End 07-Mar-2022

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_OneFCC_InsertHistoryGenerate", paramInsertHistoryGenerate)
            '' End 23-Feb-2022

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnSearch_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim cif As String = txt_CIFNo.Value
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate
            Dim maxDate As DateTime = DateTime.Now
            'Update 20210426 Adi Y : Pindah buttons ke Form Panel
            If cif Is Nothing Then
                Throw New ApplicationException("CIF No Tidak boleh kosong")
            ElseIf datefrom <> DateTime.MinValue AndAlso datefrom > maxDate AndAlso dd_TrnFilterBy.SelectedItemValue = "3" Then
                Throw New Exception("Date From : Can not select future date")
            ElseIf dateto <> DateTime.MinValue AndAlso dateto > maxDate AndAlso dd_TrnFilterBy.SelectedItemValue = "3" Then
                Throw New Exception("Date To : Can not select future date")
            ElseIf sar_DateFrom.Text = "1/1/0001 12:00:00 AM" AndAlso dd_TrnFilterBy.SelectedItemValue = "3" Then
                Throw New ApplicationException("Date From : Tidak Boleh Kosong")
            ElseIf sar_DateTo.Text = "1/1/0001 12:00:00 AM" AndAlso dd_TrnFilterBy.SelectedItemValue = "3" Then
                Throw New ApplicationException("Date To : Tidak Boleh Kosong")
            ElseIf datefrom > dateto AndAlso dd_TrnFilterBy.SelectedItemValue = "3" Then
                Throw New Exception("Date To : Harus lebih besar dari Date From")
            End If

            If dd_TrnFilterBy.SelectedItemValue <> "3" Then '' kalau selain Custom Date, dateFrom dan DateTo default now, agar parameter tidak error
                datefrom = DateTime.Now
                dateto = DateTime.Now
            End If

            'Update 20210426 Adi Y : Pindah buttons ke Form Panel
            'ListTransaction = NawaDevBLL.GenerateSarBLL.getListTransactionBySearch(cif, datefrom, dateto)

            Dim param_RadioOption As String = ""

            If dd_TrnFilterBy.SelectedItemValue = "1" Then
                param_RadioOption = "TrnHitByCaseAlert"
            ElseIf dd_TrnFilterBy.SelectedItemValue = "2" Then
                param_RadioOption = "Trn5Year"
            ElseIf dd_TrnFilterBy.SelectedItemValue = "3" Then
                param_RadioOption = "CustomDate"
            Else
                Throw New ApplicationException("Please select one of the filter options")
            End If

            Dim objGetTrn(3) As SqlParameter
            objGetTrn(0) = New SqlParameter
            objGetTrn(0).ParameterName = "@CaseID"
            objGetTrn(0).Value = CaseID

            objGetTrn(1) = New SqlParameter
            objGetTrn(1).ParameterName = "@RadioOption"
            objGetTrn(1).Value = param_RadioOption

            objGetTrn(2) = New SqlParameter
            objGetTrn(2).ParameterName = "@DateFrom"
            objGetTrn(2).Value = datefrom.ToString("yyyy-MM-dd")

            objGetTrn(3) = New SqlParameter
            objGetTrn(3).ParameterName = "@DateTo"
            objGetTrn(3).Value = dateto.ToString("yyyy-MM-dd")

            Dim tbl_goAML_odm_Transaksi As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_OneFCC_GetTransactionOfCaseAlert", objGetTrn)


            For Each row As DataRow In tbl_goAML_odm_Transaksi.Rows

                Dim Transaction As NawaDevDAL.goAML_ODM_Transaksi = New NawaDevDAL.goAML_ODM_Transaksi
                Transaction.Account_NO = row.Item("Account_NO")
                Transaction.ACCOUNT_No_Lawan = row.Item("ACCOUNT_No_Lawan")
                Transaction.Active = row.Item("Active")
                Transaction.Alternateby = row.Item("Alternateby")
                Transaction.ApprovedBy = row.Item("ApprovedBy")
                Transaction.ApprovedDate = row.Item("ApprovedDate")
                Transaction.Authorized = row.Item("Authorized")
                Transaction.BiMultiParty = row.Item("BiMultiParty")
                Transaction.Business_date = row.Item("Business_date")
                Transaction.CIF_NO = row.Item("CIF_NO")
                Transaction.CIF_No_Lawan = row.Item("CIF_No_Lawan")
                Transaction.Comments = row.Item("Comments")
                Transaction.Conductor_ID = row.Item("Conductor_ID")
                Transaction.Country_Code = row.Item("Country_Code")
                Transaction.Country_Code_Lawan = row.Item("Country_Code_Lawan")
                Transaction.CreatedBy = row.Item("CreatedBy")
                Transaction.CreatedDate = row.Item("CreatedDate")
                Transaction.Currency = row.Item("Currency")
                Transaction.Currency_Lawan = row.Item("Currency_Lawan")
                Transaction.Date_Posting = row.Item("Date_Posting")
                Transaction.Date_Transaction = row.Item("Date_Transaction")
                Transaction.Debit_Credit = row.Item("Debit_Credit")
                Transaction.Exchange_Rate = row.Item("Exchange_Rate")
                Transaction.From_Funds_Code = row.Item("From_Funds_Code")
                Transaction.From_Type = row.Item("From_Type")
                Transaction.GCN = row.Item("GCN")
                Transaction.GCN_Lawan = row.Item("GCN_Lawan")
                Transaction.IDR_Amount = row.Item("IDR_Amount")
                Transaction.LastUpdateBy = row.Item("LastUpdateBy")
                Transaction.LastUpdateDate = row.Item("LastUpdateDate")
                Transaction.MsgSwiftType = row.Item("MsgSwiftType")
                Transaction.NO_ID = row.Item("NO_ID")
                Transaction.Original_Amount = row.Item("Original_Amount")
                Transaction.Ref_Num = row.Item("Ref_Num")
                Transaction.Source_Data = row.Item("Source_Data")
                Transaction.Swift_Code_Lawan = row.Item("Swift_Code_Lawan")
                Transaction.Teller = row.Item("Teller")
                Transaction.To_Funds_Code = row.Item("To_Funds_Code")
                Transaction.To_Type = row.Item("To_Type")
                Transaction.Transaction_Code = row.Item("Transaction_Code")
                Transaction.Transaction_Location = row.Item("Transaction_Location")
                Transaction.Transaction_Number = row.Item("Transaction_Number")
                Transaction.Transaction_Remark = row.Item("Transaction_Remark")
                Transaction.Transmode_Code = row.Item("Transmode_Code")
                Transaction.Transmode_Comment = row.Item("Transmode_Comment")
                Transaction.WIC_No = row.Item("WIC_No")
                Transaction.WIC_No_Lawan = row.Item("WIC_No_Lawan")


                ListTransaction.Add(Transaction)
            Next

            If tbl_goAML_odm_Transaksi.Rows.Count > 0 Then

                bindTransactionDataTable(StoreTransaction, tbl_goAML_odm_Transaksi)
                bindTransactionDataTable(StoreTransaksiNoChecked, tbl_goAML_odm_Transaksi)
            End If
            If tbl_goAML_odm_Transaksi.Rows.Count = 0 Then
                Ext.Net.X.Msg.Alert("Message", "Tidak Ada Transaksi").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString("30122", NawaBLL.SystemParameterBLL.GetEncriptionKey) ''Request.Params("ModuleID") '' 24-Feb-2022 kembali ke module vw_OneFCC_CaseManagement
            Dim ObjModuleHeader = NawaBLL.ModuleBLL.GetModuleByModuleID(30122)

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModuleHeader.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub btn_addIndicator_click(sender As Object, e As DirectEventArgs)
        Try
            sar_reportIndicator.Clear()
            WindowReportIndicator.Hidden = False
            sar_reportIndicator.Selectable = True
            BtnsaveReportIndicator.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_addAttachment_click(sender As Object, e As DirectEventArgs)
        Try
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()
            WindowAttachment.Hidden = False
            FileDoc.Hidden = False
            BtnsaveAttachment.Hidden = False
            txtKeterangan.Editable = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandReportIndicator(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = ID).FirstOrDefault)
                bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                editIndicator(ID, "Edit")
                IDIndicator = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editIndicator(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ListDokumen.Remove(ListDokumen.Where(Function(x) x.PK_ID = ID).FirstOrDefault)
                bindAttachment(StoreAttachment, ListDokumen)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                editDokumen(ID, "Edit")
                IDDokumen = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editDokumen(ID, "Detail")
            ElseIf e.ExtraParams(1).Value = "Download" Then
                DownloadFile(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveReportIndicator_Click(sender As Object, e As DirectEventArgs)
        Try
            If String.IsNullOrEmpty(sar_reportIndicator.SelectedItem.Value) Then
                Throw New ApplicationException(sar_reportIndicator.FieldLabel + " Tidak boleh kosong")
            End If

            For Each item In ListIndicator
                If item.FK_Indicator = sar_reportIndicator.SelectedItem.Value Then
                    Throw New ApplicationException(sar_reportIndicator.SelectedItem.Value + " Sudah Ada")
                End If
            Next

            Dim reportIndicator As New NawaDevDAL.goAML_Report_Indicator
            If IDIndicator IsNot Nothing Or IDIndicator <> "" Then
                ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = IDIndicator).FirstOrDefault)
            End If
            If ListIndicator.Count = 0 Then
                reportIndicator.PK_Report_Indicator = -1
            ElseIf ListIndicator.Count >= 1 Then
                reportIndicator.PK_Report_Indicator = ListIndicator.Min(Function(x) x.PK_Report_Indicator) - 1
            End If

            reportIndicator.FK_Indicator = sar_reportIndicator.SelectedItem.Value
            ListIndicator.Add(reportIndicator)
            bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            WindowReportIndicator.Hidden = True
            IDIndicator = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelreportIndicator_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowReportIndicator.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim status As Boolean = False
            Dim DokumenOld As New NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment
            Dim Dokumen As New NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment
            If IDDokumen IsNot Nothing Or IDDokumen <> "" Then
                status = True
                DokumenOld = ListDokumen.Where(Function(x) x.PK_ID = IDDokumen).FirstOrDefault
                ListDokumen.Remove(ListDokumen.Where(Function(x) x.PK_ID = IDDokumen).FirstOrDefault)
            ElseIf Not FileDoc.HasFile Then
                Throw New Exception("Please Upload File")
            End If

            Dim docName As String = IO.Path.GetFileName(FileDoc.FileName)
            For Each item In ListDokumen
                If item.File_Name = docName Then
                    Throw New ApplicationException(docName + " Sudah Ada")
                End If
            Next

            If ListDokumen.Count = 0 Then
                Dokumen.PK_ID = -1
            ElseIf ListDokumen.Count >= 1 Then
                Dokumen.PK_ID = ListDokumen.Min(Function(x) x.PK_ID) - 1
            End If

            If status = True And Not FileDoc.HasFile Then
                Dokumen.File_Name = DokumenOld.File_Name
                Dokumen.File_Doc = DokumenOld.File_Doc
            Else
                Dokumen.File_Name = IO.Path.GetFileName(FileDoc.FileName)
                Dokumen.File_Doc = FileDoc.FileBytes
            End If
            Dokumen.Remarks = txtKeterangan.Text.Trim

            Dokumen.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            Dokumen.CreatedDate = DateTime.Now

            ListDokumen.Add(Dokumen)

            bindAttachment(StoreAttachment, ListDokumen)
            WindowAttachment.Hidden = True
            IDDokumen = Nothing

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAttachment.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub sar_jenisLaporan_DirectSelect(sender As Object, e As DirectEventArgs)
        Try
            'If sar_jenisLaporan.SelectedItem.Value = "LTKMT" Or sar_jenisLaporan.SelectedItem.Value = "LAPT" Or sar_jenisLaporan.SelectedItem.Value = "LTKM" Then
            If sar_jenisLaporan.SelectedItem.Value IsNot Nothing Then
                PanelReportIndicator.Hidden = False
            Else
                PanelReportIndicator.Hidden = True
            End If
            If sar_jenisLaporan.SelectedItem.Value = "LTKMP" Then
                NoRefPPATK.Hidden = False
            Else
                NoRefPPATK.Hidden = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub sar_IsSelectedAll_CheckedChanged(sender As Object, e As EventArgs) Handles sar_IsSelectedAll.DirectCheck
        If sar_IsSelectedAll.Value Then
            PanelChecked.Hide()
            PanelnoCheked.Show()
        Else
            PanelChecked.Show()
            PanelnoCheked.Hide()
        End If
    End Sub



#End Region
#Region "Felix 10-Feb-2022"
    Sub LoadCaseInfo()

        CaseID = Request.Params("CaseID")
        'CaseID = 18

        If Not String.IsNullOrEmpty(CaseID) Then
            Dim tblOneFCC_CaseManagement As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement where PK_CaseManagement_ID = '" & CaseID & "'", )

            For Each row As DataRow In tblOneFCC_CaseManagement.Rows
                If Not IsDBNull(row.Item("PK_CaseManagement_ID")) Then
                    text_CaseID.Text = row.Item("PK_CaseManagement_ID")
                End If
                If Not IsDBNull(row.Item("Case_Description")) Then '' 22-Apr-2022 Felix add if Not IsDBNull
                    txt_CaseDescription.Text = row.Item("Case_Description")
                End If
                If Not IsDBNull(row.Item("CIF_No")) Then
                    txt_CIFNo.Text = row.Item("CIF_No")
                End If
                If Not IsDBNull(row.Item("Customer_Name")) Then
                    txt_CustomerName.Text = row.Item("Customer_Name")
                End If

            Next
            'rb_TrnHitByCaseAlert.Checked = True

            '' Add Felix 23-Feb-2022
            'Dim tbl_HistoryGenerateSTR As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text,
            '        " select userID,GenerateDate " &
            '        " From OneFCC_CaseManagement_HistoryGenerateSTR " &
            '        " where FK_CaseManagement_ID = '" & CaseID & "' ", )

            'Update 25-Aug-2022 Adi
            Dim strQuery As String = " select a.userID,a.GenerateDate, IIF((SELECT COUNT(1) FROM goAML_ODM_Generate_STR_SAR_Transaction WHERE Fk_goAML_Generate_STR_SAR=a.SARID)=0,'SAR','STR') AS ReportedAs, b.FK_Report_ID" &
                    " From OneFCC_CaseManagement_HistoryGenerateSTR a " &
                    " LEFT JOIN goAML_ODM_Generate_STR_SAR b ON b.PK_goAML_ODM_Generate_STR_SAR = a.SARID " &
                    " where a.FK_CaseManagement_ID = '" & CaseID & "'"
            Dim tbl_HistoryGenerateSTR As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)


            If tbl_HistoryGenerateSTR.Rows.Count > 0 Then
                StoreGenerateHistory.DataSource = tbl_HistoryGenerateSTR
                StoreGenerateHistory.DataBind()
            End If

            '' End


            '24-Aug-2022 Adi : Hide/Show Gridpanel Transaction and Activity
            Dim strSQL As String = "SELECT COUNT(1) FROM vw_OneFCC_CaseManagement_Alert_Transaction WHERE FK_CaseManagement_ID=" & CaseID
            Dim intTotalTransaction As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL, Nothing)
            If intTotalTransaction = 0 Then
                gp_CaseAlert_Transaction.Hidden = True
            Else
                gp_CaseAlert_Transaction.Hidden = False
            End If

            strSQL = "SELECT COUNT(1) FROM vw_OneFCC_CaseManagement_Alert_Activity WHERE FK_CaseManagement_ID=" & CaseID
            Dim intTotalActivity As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL, Nothing)
            If intTotalActivity = 0 Then
                gp_CaseAlert_Activity.Hidden = True
            Else
                gp_CaseAlert_Activity.Hidden = False
            End If

            'BSIM per CIF per Process Date jadi hide saja.
            txt_CaseDescription.Hidden = True
            'End of 24-Aug-2022 Adi : Hide/Show Gridpanel Transaction and Activity

            '24-Aug-2022 Adi : Load Activity Account No (if any)
            LoadActivity()

        End If

    End Sub

    'Sub rg_TrnFilterBy_click(sender As Object, e As DirectEventArgs)
    'If rb_CustomDate.Checked Then
    '    sar_DateFrom.Hidden = False
    '    sar_DateTo.Hidden = False
    'Else
    '    sar_DateFrom.Hidden = True
    '    sar_DateTo.Hidden = True
    'End If
    'End Sub

    Protected Sub rb_CustomDate_click(sender As Object, e As DirectEventArgs)
        Try

            'Dim radioChecked1 As String = rb_TrnHitByCaseAlert.Checked.ToString()
            'Dim radioChecked2 As String = rb_Trn5Year.Checked.ToString()
            'Dim radioChecked3 As String = rb_CustomDate.Checked.ToString()

            Dim objradio As Ext.Net.Radio = CType(sender, Ext.Net.Radio)
            If objradio.InputValue = "3" And objradio.Checked Then
                sar_DateFrom.Hidden = False
                sar_DateTo.Hidden = False
            Else
                sar_DateFrom.Hidden = True
                sar_DateTo.Hidden = True

            End If

            'rb_CustomDate.Checked = True
            'sar_DateFrom.Hidden = False
            'sar_DateTo.Hidden = False

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub rg_TrnFilterBy_click(sender As Object, e As DirectEventArgs)
        'If rb_CustomDate.Checked Then
        '    sar_DateFrom.Hidden = False
        '    sar_DateTo.Hidden = False
        'Else
        '    sar_DateFrom.Hidden = True
        '    sar_DateTo.Hidden = True
        'End If
    End Sub

    Protected Sub dd_TrnFilterBy_ValueChanged(sender As Object, e As DirectEventArgs)
        Try

            If dd_TrnFilterBy.SelectedItemValue = "1" Then
                sar_DateFrom.Hidden = True
                sar_DateTo.Hidden = True
            ElseIf dd_TrnFilterBy.SelectedItemValue = "2" Then
                sar_DateFrom.Hidden = True
                sar_DateTo.Hidden = True
            ElseIf dd_TrnFilterBy.SelectedItemValue = "3" Then
                sar_DateFrom.Hidden = False
                sar_DateTo.Hidden = False
            Else
                Throw New ApplicationException("Please select one of the filter options")
            End If

            'rb_CustomDate.Checked = True
            'sar_DateFrom.Hidden = False
            'sar_DateTo.Hidden = False

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub bindTransactionDataTable(store As Ext.Net.Store, dt As DataTable)
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(ListTransaction)
        'Dim objtable As Data.DataTable = dt

        objtable.Columns.Add(New DataColumn("Valid", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoRefTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("TipeTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("LokasiTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("KeteranganTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("DateTransaction", GetType(Date)))
        objtable.Columns.Add(New DataColumn("AccountNo", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaTeller", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaPejabat", GetType(String)))
        objtable.Columns.Add(New DataColumn("TglPembukuan", GetType(Date)))
        objtable.Columns.Add(New DataColumn("CaraTranDilakukan", GetType(String)))
        objtable.Columns.Add(New DataColumn("CaraTranLain", GetType(String)))
        objtable.Columns.Add(New DataColumn("DebitCredit", GetType(String)))
        objtable.Columns.Add(New DataColumn("OriginalAmount", GetType(String)))
        objtable.Columns.Add(New DataColumn("IDR", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim CifLawan As String = item("CIF_No_Lawan").ToString
                Dim WicLawan As String = item("WIC_No_Lawan").ToString
                Dim AccountLawan As String = item("ACCOUNT_No_Lawan").ToString
                Dim BiMulti As String = item("BiMultiParty").ToString
                If CifLawan = "" And WicLawan = "" And AccountLawan = "" And BiMulti = "1" Then
                    item("Valid") = "Tidak"
                Else
                    item("Valid") = "Ya"
                End If
                item("NoTran") = item("Transaction_Number")
                item("NoRefTran") = item("Ref_Num")
                Dim SourceData As String = item("Source_Data").ToString
                Dim SourceDataTemp As Integer
                If Integer.TryParse(SourceData, SourceDataTemp) Then
                    If SourceData <> "" Then
                        item("TipeTran") = NawaDevBLL.GenerateSarBLL.getSourceData(SourceData)
                    End If
                Else
                    item("TipeTran") = SourceData
                End If
                item("LokasiTran") = item("Transaction_Location")
                item("KeteranganTran") = item("Transaction_Remark")
                item("DateTransaction") = item("Date_Transaction")
                item("AccountNo") = item("Account_NO")
                item("NamaTeller") = item("Teller")
                item("NamaPejabat") = item("Authorized")
                item("TglPembukuan") = item("Date_Posting")
                item("CaraTranDilakukan") = item("Transmode_Code")
                item("CaraTranLain") = item("Transmode_Comment")
                item("DebitCredit") = item("Debit_Credit")
                If item("Original_Amount").ToString <> "" Then
                    Dim OriginalAmount As Decimal = item("Original_Amount").ToString
                    item("OriginalAmount") = OriginalAmount.ToString("#,###.00")
                End If
                If item("IDR_Amount").ToString <> "" Then
                    Dim IDR As Decimal = item("IDR_Amount").ToString
                    item("IDR") = IDR.ToString("#,###.00")
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    '25-Aug-2022 Adi : Renamed from SaveGenerateSTRSarTanpaApproval to SaveGenerateSTRTanpaApproval (Transaction)
    Sub SaveGenerateSTRTanpaApproval(GenerateSAR As GenerateSARData, objmodule As NawaDAL.Module, action As Integer, DateFrom As DateTime, DateTo As DateTime)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim actsInsert As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim actsUpdate As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim actsDelete As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim modulename As String = objmodule.ModuleName

                    If action = 1 Then ' insert
                        'Add Generate SAR
                        With GenerateSAR.objGenerateSAR
                            .Active = True
                            .status = 1
                            .CreatedBy = Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                            .ApprovedBy = Common.SessionCurrentUser.UserID
                            .ApprovedDate = DateTime.Now
                        End With
                        objdb.Entry(GenerateSAR.objGenerateSAR).State = Entity.EntityState.Added
                        objdb.SaveChanges()

                        Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaDevBLL.NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)

                        NawaDevBLL.NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, GenerateSAR.objGenerateSAR)

                        'If GenerateSAR.objGenerateSAR.IsSelectedAll = False Then

                        For Each item In GenerateSAR.listObjGenerateSARTransaction
                            item.Fk_goAML_Generate_STR_SAR = GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR
                            item.Active = True
                            item.CreatedBy = Common.SessionCurrentUser.UserID
                            item.ApprovedBy = Common.SessionCurrentUser.UserID
                            item.CreatedDate = DateTime.Now
                            item.ApprovedDate = DateTime.Now
                            objdb.Entry(item).State = Entity.EntityState.Added

                            NawaDevBLL.NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, item)
                        Next
                        'End If

                        For Each item In GenerateSAR.listObjDokumen
                            item.Fk_goAML_ODM_Generate_STR_SAR = GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR
                            item.Active = True
                            item.CreatedBy = Common.SessionCurrentUser.UserID
                            item.ApprovedBy = Common.SessionCurrentUser.UserID
                            item.CreatedDate = DateTime.Now
                            item.ApprovedDate = DateTime.Now
                            objdb.Entry(item).State = Entity.EntityState.Added

                            NawaDevBLL.NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, item)
                        Next
                        objdb.SaveChanges()

                    End If
                    objdb.SaveChanges()
                    objtrans.Commit()

                    If action = 1 Then ' insert
                        'If GenerateSAR.objGenerateSAR.IsSelectedAll Then
                        '    NawaDevBLL.GenerateSarBLL.SarToReportedInsert(GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR, DateFrom, DateTo, GenerateSAR.objGenerateSAR.CIF_NO, user)
                        'Else
                        NawaDevBLL.GenerateSarBLL.SarToReportedInsertChecked(GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR, user)
                        'End If
                    End If

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
#End Region

#Region "Update 24-Aug-2022 Adi : Penambahan Fitur Generate as SAR (Activity)"
    Protected Sub store_ReadData_CaseAlert_Transaction(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT * FROM vw_OneFCC_CaseManagement_Alert_Transaction"
            strQuery &= " WHERE FK_CaseManagement_ID = " & CaseID

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Transaction.GetStore.DataSource = DataPaging
            gp_CaseAlert_Transaction.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Activity(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT * FROM vw_OneFCC_CaseManagement_Alert_Activity"
            strQuery &= " WHERE FK_CaseManagement_ID = " & CaseID

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Activity.GetStore.DataSource = DataPaging
            gp_CaseAlert_Activity.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub cboReportAs_Change(sender As Object, e As DirectEventArgs)
        Try
            'Ext.Net.X.Msg.Alert("Selected Item", cboReportAs.SelectedItem.Value()).Show()
            fs_STRSAR_General.Hidden = False
            PanelAttachment.Hidden = False
            PanelReportIndicator.Hidden = False

            Dim strReportAs As String = cboReportAs.SelectedItem.Value()
            If strReportAs = "STR" Then
                pnlTransaction.Hidden = False
                pnlActivity.Hidden = True
            Else
                pnlTransaction.Hidden = True
                pnlActivity.Hidden = False
            End If

            'Reload combobox jenis laporan
            sar_jenisLaporan.GetStore.Reload()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadActivity()
        Try
            Dim dtNewActivity As New DataTable
            dtNewActivity.Columns.Add(New DataColumn("ACCOUNT_NO", GetType(String)))
            dtNewActivity.Columns.Add(New DataColumn("Significance", GetType(Integer)))
            dtNewActivity.Columns.Add(New DataColumn("Reason", GetType(String)))
            dtNewActivity.Columns.Add(New DataColumn("Comments", GetType(String)))
            dtActivity = dtNewActivity

            'Load Distinct Account No for initial
            Dim strQuery As String = "SELECT DISTINCT ACCOUNT_NO FROM vw_OneFCC_CaseManagement_Alert_Activity"
            strQuery &= " WHERE FK_CaseManagement_ID = " & CaseID & " ORDER BY ACCOUNT_NO"
            Dim dtCaseActivity As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            If dtCaseActivity IsNot Nothing Then
                For Each row In dtCaseActivity.Rows
                    Dim drNew As DataRow = dtActivity.NewRow()
                    drNew("ACCOUNT_NO") = row("ACCOUNT_NO")

                    dtActivity.Rows.Add(drNew)
                Next
            End If

            'Bind to GridPanel
            gp_Activity.GetStore.DataSource = dtActivity
            gp_Activity.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_Activity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                editActivity(ID, "Edit")
                ACT_ACCOUNT_NO = ID
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub editActivity(strAccNo As String, strAction As String)
        Try
            'Clear window
            txt_ACCOUNT_NO.Value = Nothing
            txt_Activity_Significance.Value = Nothing
            txt_Activity_Reason.Value = Nothing
            txt_Activity_Comment.Value = Nothing

            Dim drSearch As DataRow = dtActivity.Select("ACCOUNT_NO = '" & strAccNo & "'").FirstOrDefault
            If drSearch IsNot Nothing Then
                txt_ACCOUNT_NO.Value = drSearch("ACCOUNT_NO")
                txt_Activity_Significance.Value = drSearch("Significance")
                txt_Activity_Reason.Value = drSearch("Reason")
                txt_Activity_Comment.Value = drSearch("Comments")
            End If

            window_Activity.Hidden = False

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Activity_Save_Click()
        Try
            'Validate
            If String.IsNullOrEmpty(txt_Activity_Significance.Text) Or txt_Activity_Significance.Text = "NA" Then
                Throw New ApplicationException(txt_Activity_Significance.FieldLabel & " hanya boleh 0 - 10.")
            Else
                If txt_Activity_Significance.Value < 0 Or txt_Activity_Significance.Value > 10 Then
                    Throw New ApplicationException(txt_Activity_Significance.FieldLabel & " hanya boleh 0 - 10.")
                End If
            End If

            'Save to DataTable
            Dim strAccNo As String = ACT_ACCOUNT_NO
            For Each row In dtActivity.Rows
                If row("ACCOUNT_NO") = strAccNo Then
                    row("Significance") = txt_Activity_Significance.Value
                    row("Reason") = txt_Activity_Reason.Value
                    row("Comments") = txt_Activity_Comment.Value

                    Exit For
                End If
            Next

            'Refresh GridPanel
            gp_Activity.GetStore.DataSource = dtActivity
            gp_Activity.GetStore.DataBind()

            'Hide Window Activity
            window_Activity.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Activity_Back_Click()
        Try
            window_Activity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Sub SaveGenerateSARTanpaApproval(GenerateSAR As GenerateSARData, objmodule As NawaDAL.Module, action As Integer)
        Dim lngSARID As Long = 0
        Dim lngReportID As Long = 0

        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim actsInsert As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim actsUpdate As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim actsDelete As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim modulename As String = objmodule.ModuleName

                    If action = 1 Then ' insert
                        'Add Generate SAR
                        With GenerateSAR.objGenerateSAR
                            .Active = True
                            .status = 1
                            .CreatedBy = Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                            .ApprovedBy = Common.SessionCurrentUser.UserID
                            .ApprovedDate = DateTime.Now
                        End With
                        objdb.Entry(GenerateSAR.objGenerateSAR).State = Entity.EntityState.Added
                        objdb.SaveChanges()

                        'Get the saved PK SAR ID
                        lngSARID = GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR

                        Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, GenerateSAR.objGenerateSAR)

                        'Save Attachment
                        For Each item In GenerateSAR.listObjDokumen
                            item.Fk_goAML_ODM_Generate_STR_SAR = GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR
                            item.Active = True
                            item.CreatedBy = Common.SessionCurrentUser.UserID
                            item.ApprovedBy = Common.SessionCurrentUser.UserID
                            item.CreatedDate = DateTime.Now
                            item.ApprovedDate = DateTime.Now
                            objdb.Entry(item).State = Entity.EntityState.Added

                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, item)
                        Next
                        objdb.SaveChanges()


                        'Generate SAR GoAML Report
                        Dim obj_goAML_Report = New goAML_Report
                        With obj_goAML_Report
                            .Rentity_ID = getGlobalParameterValueByID(6)
                            .Rentity_Branch = getGlobalParameterValueByID(7)
                            .Submission_Code = getGlobalParameterValueByID(5)
                            .Report_Code = sar_jenisLaporan.SelectedItem.Value
                            If Not TanggalLaporan.SelectedDate = DateTime.MinValue Then
                                .Submission_Date = TanggalLaporan.SelectedDate
                                .Transaction_Date = TanggalLaporan.SelectedDate
                            Else
                                .Submission_Date = DateTime.Now
                                .Transaction_Date = DateTime.Now
                            End If

                            .Currency_Code_Local = getGlobalParameterValueByID(8)
                            .Reason = alasan.Value
                            .Action = ""
                            .FK_Address_ID = 35
                            .FK_Report_Type_ID = 4
                            .Fiu_Ref_Number = ""
                            .UnikReference = CaseID
                            .isValid = True     'Default true. Nanti akan diupdate saat panggil SP Report Validation
                            .MarkedAsDelete = False

                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .LastUpdateDate = DateTime.Now
                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .ApprovedDate = DateTime.Now
                        End With
                        objdb.Entry(obj_goAML_Report).State = Entity.EntityState.Added
                        objdb.SaveChanges()

                        objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, obj_goAML_Report)

                        lngReportID = obj_goAML_Report.PK_Report_ID

                        'Generate GoAML Report Activities
                        For Each row In dtActivity.Rows
                            Me.ACT_ACCOUNT_NO = row("ACCOUNT_NO")

                            Dim objNewActivity = New goAML_Act_ReportPartyType
                            With objNewActivity
                                .FK_Report_ID = obj_goAML_Report.PK_Report_ID
                                .SubNodeType = 1    'Account
                                .significance = IIf(Not IsDBNull(row("Significance")), row("Significance"), Nothing)
                                .reason = IIf(Not IsDBNull(row("Reason")), row("Reason"), Nothing)
                                .comments = IIf(Not IsDBNull(row("Comments")), row("Comments"), Nothing)

                                .Active = True
                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .CreatedDate = DateTime.Now
                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now

                            End With
                            objdb.Entry(objNewActivity).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objNewActivity)

                            'Save Activity Account
                            Dim objAccount = New goAML_Act_Account
                            Dim objSelAccount As goAML_Ref_Account = objdb.goAML_Ref_Account.Where(Function(x) x.Account_No = Me.ACT_ACCOUNT_NO).FirstOrDefault
                            With objAccount
                                .FK_Report_ID = objNewActivity.FK_Report_ID
                                .FK_Act_ReportParty_ID = objNewActivity.PK_goAML_Act_ReportPartyType_ID

                                Dim objGP As New NawaDevDAL.goAML_Ref_ReportGlobalParameter
                                objGP = objdb.goAML_Ref_ReportGlobalParameter.Where(Function(x) x.PK_GlobalReportParameter_ID = 1).FirstOrDefault
                                If objGP IsNot Nothing Then
                                    .Institution_Name = objGP.ParameterValue
                                End If

                                .Intitution_Code = Nothing

                                objGP = objdb.goAML_Ref_ReportGlobalParameter.Where(Function(x) x.PK_GlobalReportParameter_ID = 2).FirstOrDefault
                                If objGP IsNot Nothing Then
                                    .Swift_Code = objGP.ParameterValue
                                End If

                                objGP = objdb.goAML_Ref_ReportGlobalParameter.Where(Function(x) x.PK_GlobalReportParameter_ID = 3).FirstOrDefault
                                If objGP IsNot Nothing Then
                                    If CInt(objGP.ParameterValue) = 1 Then
                                        .Non_Banking_Institution = True
                                    Else
                                        .Non_Banking_Institution = False
                                    End If
                                End If

                                If objSelAccount IsNot Nothing Then
                                    .Branch = objSelAccount.Branch
                                    .Account = objSelAccount.Account_No
                                    .Currency_Code = objSelAccount.Currency_Code
                                    .Account_Name = objSelAccount.Account_Name
                                    .iban = objSelAccount.IBAN
                                    .Client_Number = objSelAccount.client_number
                                    .Personal_Account_Type = objSelAccount.personal_account_type
                                    .Opened = objSelAccount.opened
                                    .Closed = objSelAccount.closed
                                    .Date_Balance = objSelAccount.date_balance
                                    .Balance = objSelAccount.balance
                                    .Status_Code = objSelAccount.status_code
                                    .Beneficiary = objSelAccount.beneficiary
                                    .Beneficiary_Comment = objSelAccount.beneficiary_comment
                                    .Comments = objSelAccount.comments

                                    If Not String.IsNullOrEmpty(objSelAccount.FK_CIF_Entity_ID) Then
                                        .IsRekeningKorporasi = True
                                    Else
                                        .IsRekeningKorporasi = False
                                    End If

                                    .Active = True
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                End If
                            End With
                            objdb.Entry(objAccount).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objAccount)

                            'Signatory 
                            'Masukkand data2 Signatory dari goAML Ref
                            Dim objListgoAML_Ref_Account_Signatory As List(Of goAML_Ref_Account_Signatory) = objdb.goAML_Ref_Account_Signatory.Where(Function(x) x.FK_Account_No = objAccount.Account).ToList
                            If objListgoAML_Ref_Account_Signatory IsNot Nothing Then
                                For Each objSignatory In objListgoAML_Ref_Account_Signatory

                                    If objSignatory.isCustomer Then     'Jika Sigantory adalah Nasabah
                                        Dim objSignatory_New As New goAML_Act_acc_Signatory

                                        'Get Signatory Nasabah
                                        Dim objCIFPerson = objdb.goAML_Ref_Customer.Where(Function(x) x.CIF = objSignatory.FK_CIF_Person_ID).FirstOrDefault
                                        If objCIFPerson IsNot Nothing Then
                                            With objSignatory_New
                                                .FK_Report_ID = objAccount.FK_Report_ID
                                                .FK_Act_ReportParty_ID = objAccount.FK_Act_ReportParty_ID
                                                .FK_Activity_Account_ID = objAccount.PK_goAML_Act_Account_ID

                                                'Load Person CIF Detail
                                                If objCIFPerson IsNot Nothing Then
                                                    .Gender = objCIFPerson.INDV_Gender
                                                    .Title = objCIFPerson.INDV_Title
                                                    .First_Name = objCIFPerson.INDV_First_name
                                                    .Middle_Name = objCIFPerson.INDV_Middle_name
                                                    .Prefix = objCIFPerson.INDV_Prefix
                                                    .Last_Name = objCIFPerson.INDV_Last_Name
                                                    .BirthDate = objCIFPerson.INDV_BirthDate
                                                    .Birth_Place = objCIFPerson.INDV_Birth_Place
                                                    .Mothers_Name = objCIFPerson.INDV_Mothers_Name
                                                    .Alias = objCIFPerson.INDV_Alias
                                                    .SSN = objCIFPerson.INDV_SSN
                                                    .Passport_Number = objCIFPerson.INDV_Passport_Number
                                                    .Passport_Country = objCIFPerson.INDV_Passport_Country
                                                    .Id_Number = objCIFPerson.INDV_ID_Number
                                                    .Nationality1 = objCIFPerson.INDV_Nationality1
                                                    .Nationality2 = objCIFPerson.INDV_Nationality2
                                                    .Nationality3 = objCIFPerson.INDV_Nationality3
                                                    .Residence = objCIFPerson.INDV_Residence
                                                    .Email = objCIFPerson.INDV_Email
                                                    .email2 = objCIFPerson.INDV_Email2
                                                    .email3 = objCIFPerson.INDV_Email3
                                                    .email4 = objCIFPerson.INDV_Email4
                                                    .email5 = objCIFPerson.INDV_Email5
                                                    .Occupation = objCIFPerson.INDV_Occupation
                                                    .Employer_Name = objCIFPerson.INDV_Employer_Name
                                                    .Deceased = objCIFPerson.INDV_Deceased
                                                    .Deceased_Date = objCIFPerson.INDV_Deceased_Date
                                                    .Tax_Number = objCIFPerson.INDV_Tax_Number
                                                    .Tax_Reg_Number = objCIFPerson.INDV_Tax_Reg_Number
                                                    .Source_Of_Wealth = objCIFPerson.INDV_Source_of_Wealth
                                                    .Comment = objCIFPerson.INDV_Comments

                                                    .isPrimary = objSignatory.isPrimary
                                                    .role = objSignatory.Role
                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                End If
                                                objdb.Entry(objSignatory_New).State = Entity.EntityState.Added
                                                objdb.SaveChanges()

                                                objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objSignatory_New)

                                                'Signatory Address(es)  (1=Address, 5=Employer Address)
                                                Dim objListAddress = objdb.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_To_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                                If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                                    For Each objAddress In objListAddress
                                                        Dim objNewSignatoryAddress As New goAML_Act_Acc_sign_Address
                                                        With objNewSignatoryAddress
                                                            .FK_Report_ID = objAccount.FK_Report_ID
                                                            .FK_Act_Acc_Entity = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                            .Address_Type = objAddress.Address_Type
                                                            .Address = objAddress.Address
                                                            .Town = objAddress.Town
                                                            .City = objAddress.City
                                                            .Zip = objAddress.Zip
                                                            .Country_Code = objAddress.Country_Code
                                                            .State = objAddress.State
                                                            .Comments = objAddress.Comments

                                                            If objAddress.FK_Ref_Detail_Of = 1 Then
                                                                .isEmployer = False
                                                            Else
                                                                .isEmployer = True
                                                            End If

                                                            .Active = 1
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                        End With

                                                        objdb.Entry(objNewSignatoryAddress).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()

                                                        objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objNewSignatoryAddress)
                                                    Next
                                                End If

                                                'Signatory Phone(s) (1=Phone, 5=Employer Phone)
                                                Dim objListPhone = objdb.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_for_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                                If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                                    For Each objPhone In objListPhone
                                                        Dim objNewSignatoryPhone As New goAML_Act_Acc_sign_Phone
                                                        With objNewSignatoryPhone
                                                            .FK_Report_ID = objAccount.FK_Report_ID
                                                            .FK_Act_Acc_Entity_ID = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                            .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                            .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                            .tph_country_prefix = objPhone.tph_country_prefix
                                                            .tph_number = objPhone.tph_number
                                                            .tph_extension = objPhone.tph_extension
                                                            .comments = objPhone.comments

                                                            If objPhone.FK_Ref_Detail_Of = 1 Then
                                                                .isEmployer = False
                                                            Else
                                                                .isEmployer = True
                                                            End If

                                                            .Active = 1
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                        End With

                                                        objdb.Entry(objNewSignatoryPhone).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()

                                                        objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objNewSignatoryPhone)
                                                    Next
                                                End If

                                                'Signatory Identification(s)
                                                Dim objListIdentification = objdb.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = objCIFPerson.PK_Customer_ID).ToList
                                                If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                                    For Each objIdentification In objListIdentification
                                                        Dim objNewSignatoryIdentification As New goAML_Activity_Person_Identification
                                                        With objNewSignatoryIdentification
                                                            .FK_Report_ID = objAccount.FK_Report_ID
                                                            .FK_Person_Type = 2
                                                            .FK_Act_Person_ID = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                            .Type = objIdentification.Type
                                                            .Number = objIdentification.Number
                                                            .Issue_Date = objIdentification.Issue_Date
                                                            .Expiry_Date = objIdentification.Expiry_Date
                                                            .Issued_By = objIdentification.Issued_By
                                                            .Issued_Country = objIdentification.Issued_Country
                                                            .Identification_Comment = objIdentification.Identification_Comment

                                                            .Active = 1
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                        End With

                                                        objdb.Entry(objNewSignatoryIdentification).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()

                                                        objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objNewSignatoryIdentification)
                                                    Next
                                                End If
                                            End With
                                        End If
                                    Else        '====== SIGNATORY NON NASABAH ======
                                        Dim objSignatory_New As New goAML_Act_acc_Signatory

                                        'Get Signatory Non Nasabah
                                        Dim objWICPerson = objdb.goAML_Ref_WIC.Where(Function(x) x.WIC_No = objSignatory.WIC_No).FirstOrDefault
                                        If objWICPerson IsNot Nothing Then
                                            With objSignatory_New
                                                .FK_Report_ID = objAccount.FK_Report_ID
                                                .FK_Act_ReportParty_ID = objAccount.FK_Act_ReportParty_ID
                                                .FK_Activity_Account_ID = objAccount.PK_goAML_Act_Account_ID

                                                'Load Person CIF Detail
                                                If objWICPerson IsNot Nothing Then
                                                    .Gender = objWICPerson.INDV_Gender
                                                    .Title = objWICPerson.INDV_Title
                                                    .First_Name = objWICPerson.INDV_First_Name
                                                    .Middle_Name = objWICPerson.INDV_Middle_Name
                                                    .Prefix = objWICPerson.INDV_Prefix
                                                    .Last_Name = objWICPerson.INDV_Last_Name
                                                    .BirthDate = objWICPerson.INDV_BirthDate
                                                    .Birth_Place = objWICPerson.INDV_Birth_Place
                                                    .Mothers_Name = objWICPerson.INDV_Mothers_Name
                                                    .Alias = objWICPerson.INDV_Alias
                                                    .SSN = objWICPerson.INDV_SSN
                                                    .Passport_Number = objWICPerson.INDV_Passport_Number
                                                    .Passport_Country = objWICPerson.INDV_Passport_Country
                                                    .Id_Number = objWICPerson.INDV_ID_Number
                                                    .Nationality1 = objWICPerson.INDV_Nationality1
                                                    .Nationality2 = objWICPerson.INDV_Nationality2
                                                    .Nationality3 = objWICPerson.INDV_Nationality3
                                                    .Residence = objWICPerson.INDV_Residence
                                                    .Email = objWICPerson.INDV_Email
                                                    .email2 = objWICPerson.INDV_Email2
                                                    .email3 = objWICPerson.INDV_Email3
                                                    .email4 = objWICPerson.INDV_Email4
                                                    .email5 = objWICPerson.INDV_Email5
                                                    .Occupation = objWICPerson.INDV_Occupation
                                                    .Employer_Name = objWICPerson.INDV_Employer_Name
                                                    .Deceased = objWICPerson.INDV_Deceased
                                                    .Deceased_Date = objWICPerson.INDV_Deceased_Date
                                                    .Tax_Number = objWICPerson.INDV_Tax_Number
                                                    .Tax_Reg_Number = objWICPerson.INDV_Tax_Reg_Number
                                                    .Source_Of_Wealth = objWICPerson.INDV_SumberDana
                                                    .Comment = objWICPerson.INDV_Comment

                                                    .isPrimary = objSignatory.isPrimary
                                                    .role = objSignatory.Role
                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                End If
                                            End With

                                            objdb.Entry(objSignatory_New).State = Entity.EntityState.Added
                                            objdb.SaveChanges()

                                            objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objSignatory_New)

                                            'Signatory Address(es)  (3=Address, 10=Employer Address)
                                            Dim objListAddress = objdb.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_To_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                            If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                                For Each objAddress In objListAddress
                                                    Dim objNewSignatoryAddress As New goAML_Act_Acc_sign_Address
                                                    With objNewSignatoryAddress
                                                        .FK_Report_ID = objAccount.FK_Report_ID
                                                        .FK_Act_Acc_Entity = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                        .Address_Type = objAddress.Address_Type
                                                        .Address = objAddress.Address
                                                        .Town = objAddress.Town
                                                        .City = objAddress.City
                                                        .Zip = objAddress.Zip
                                                        .Country_Code = objAddress.Country_Code
                                                        .State = objAddress.State
                                                        .Comments = objAddress.Comments

                                                        If objAddress.FK_Ref_Detail_Of = 3 Then
                                                            .isEmployer = False
                                                        Else
                                                            .isEmployer = True
                                                        End If

                                                        .Active = 1
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                    End With
                                                    objdb.Entry(objNewSignatoryAddress).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()

                                                    objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objNewSignatoryAddress)
                                                Next
                                            End If

                                            'Signatory Phone(s) (3=Phone, 10=Employer Phone)
                                            Dim objListPhone = objdb.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_for_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                            If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                                For Each objPhone In objListPhone
                                                    Dim objNewSignatoryPhone As New goAML_Act_Acc_sign_Phone
                                                    With objNewSignatoryPhone
                                                        .FK_Report_ID = objAccount.FK_Report_ID
                                                        .FK_Act_Acc_Entity_ID = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                        .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                        .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                        .tph_country_prefix = objPhone.tph_country_prefix
                                                        .tph_number = objPhone.tph_number
                                                        .tph_extension = objPhone.tph_extension
                                                        .comments = objPhone.comments

                                                        If objPhone.FK_Ref_Detail_Of = 3 Then
                                                            .isEmployer = False
                                                        Else
                                                            .isEmployer = True
                                                        End If

                                                        .Active = 1
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                    End With
                                                    objdb.Entry(objNewSignatoryPhone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()

                                                    objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objNewSignatoryPhone)

                                                Next
                                            End If

                                            'Signatory Identification(s)
                                            Dim objListIdentification = objdb.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 8 And x.FK_Person_ID = objWICPerson.PK_Customer_ID).ToList
                                            If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                                For Each objIdentification In objListIdentification
                                                    Dim objNewSignatoryIdentification As New goAML_Activity_Person_Identification
                                                    With objNewSignatoryIdentification
                                                        .FK_Report_ID = objAccount.FK_Report_ID
                                                        .FK_Person_Type = 2
                                                        .FK_Act_Person_ID = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                        .Type = objIdentification.Type
                                                        .Number = objIdentification.Number
                                                        .Issue_Date = objIdentification.Issue_Date
                                                        .Expiry_Date = objIdentification.Expiry_Date
                                                        .Issued_By = objIdentification.Issued_By
                                                        .Issued_Country = objIdentification.Issued_Country
                                                        .Identification_Comment = objIdentification.Identification_Comment

                                                        .Active = 1
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                    End With
                                                    objdb.Entry(objNewSignatoryIdentification).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()

                                                    objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objNewSignatoryIdentification)
                                                Next
                                            End If
                                        End If
                                    End If
                                Next
                            End If

                            'Account Entity
                            If objAccount.IsRekeningKorporasi Then
                                Dim objAccountEntity = New NawaDevDAL.goAML_Act_Entity_Account
                                objAccountEntity.PK_goAML_Act_Entity_account = objAccount.PK_goAML_Act_Account_ID

                                With objAccountEntity
                                    .FK_Report_ID = objAccount.FK_Report_ID
                                    .FK_Activity_ReportParty_ID = objAccount.FK_Act_ReportParty_ID
                                    .FK_Act_Account_ID = objAccount.PK_goAML_Act_Account_ID

                                    'Load Informasi Korporasi
                                    Dim objCIFKorporasi = NawaDevBLL.goAML_CustomerBLL.GetCustomerbyCIF(objSelAccount.FK_CIF_Entity_ID)
                                    If objCIFKorporasi IsNot Nothing Then
                                        .Name = objCIFKorporasi.Corp_Name
                                        .Commercial_Name = objCIFKorporasi.Corp_Commercial_Name
                                        .Incorporation_Legal_Form = objCIFKorporasi.Corp_Incorporation_Legal_Form
                                        .Incorporation_Number = objCIFKorporasi.Corp_Incorporation_Number
                                        .Business = objCIFKorporasi.Corp_Business
                                        .Email = objCIFKorporasi.Corp_Email
                                        .Url = objCIFKorporasi.Corp_Url
                                        .Incorporation_State = objCIFKorporasi.Corp_Incorporation_State
                                        .Incorporation_Country_Code = objCIFKorporasi.Corp_Incorporation_Country_Code
                                        .Incorporation_Date = objCIFKorporasi.Corp_Incorporation_Date
                                        If Not IsNothing(.Business_Closed) Then
                                            .Business_Closed = objCIFKorporasi.Corp_Business_Closed
                                        Else
                                            .Business_Closed = False
                                        End If
                                        .Date_Business_Closed = objCIFKorporasi.Corp_Date_Business_Closed
                                        .Tax_Number = objCIFKorporasi.Corp_Tax_Number
                                        .Comments = objCIFKorporasi.Corp_Comments
                                    End If

                                    .Active = True
                                    .CreatedDate = DateTime.Now
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                End With
                                objdb.Entry(objAccountEntity).State = Entity.EntityState.Added
                                objdb.SaveChanges()

                                objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objAccountEntity)

                                'Masukkan data2 Entity Address & Phone dari goAML Ref
                                'Account Entity Address(es)
                                Dim objCustomer = objdb.goAML_Ref_Customer.Where(Function(x) x.CIF = objAccount.Client_Number).FirstOrDefault
                                If objCustomer IsNot Nothing Then
                                    Dim objListEntityAddress = objdb.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = objCustomer.PK_Customer_ID).ToList
                                    If objListEntityAddress IsNot Nothing AndAlso objListEntityAddress.Count > 0 Then
                                        For Each objAddress In objListEntityAddress
                                            Dim objNewEntityAddress As New goAML_Act_Acc_Entity_Address
                                            With objNewEntityAddress
                                                .FK_Report_ID = objAccount.FK_Report_ID
                                                .FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                                .Address_Type = objAddress.Address_Type
                                                .Address = objAddress.Address
                                                .Town = objAddress.Town
                                                .City = objAddress.City
                                                .Zip = objAddress.Zip
                                                .Country_Code = objAddress.Country_Code
                                                .State = objAddress.State
                                                .Comments = objAddress.Comments

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                            End With
                                            objdb.Entry(objNewEntityAddress).State = Entity.EntityState.Added
                                            objdb.SaveChanges()

                                            objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objNewEntityAddress)

                                        Next
                                    End If

                                    'Entity Phone(s)
                                    Dim objListEntityPhone = objdb.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = objCustomer.PK_Customer_ID).ToList
                                    If objListEntityPhone IsNot Nothing AndAlso objListEntityPhone.Count > 0 Then
                                        For Each objPhone In objListEntityPhone
                                            Dim objNewEntityPhone As New goAML_Act_Acc_Entity_Phone
                                            With objNewEntityPhone
                                                .FK_Report_ID = objAccount.FK_Report_ID
                                                .FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                .tph_country_prefix = objPhone.tph_country_prefix
                                                .tph_number = objPhone.tph_number
                                                .tph_extension = objPhone.tph_extension
                                                .comments = objPhone.comments

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                            End With
                                            objdb.Entry(objNewEntityPhone).State = Entity.EntityState.Added
                                            objdb.SaveChanges()

                                            objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objNewEntityPhone)
                                        Next
                                    End If
                                End If

                                'Masukkand data2 Director dari goAML Ref
                                Dim objListgoAML_Ref_Account_Director As List(Of goAML_Ref_Customer_Entity_Director) = objdb.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.FK_Entity_ID = objAccount.Client_Number).ToList
                                If objListgoAML_Ref_Account_Director IsNot Nothing Then
                                    For Each objDirector In objListgoAML_Ref_Account_Director
                                        Dim objDirector_New As New goAML_Act_Acc_Ent_Director
                                        With objDirector_New
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                            .Gender = objDirector.Gender
                                            .Title = objDirector.Title
                                            .First_Name = objDirector.First_name
                                            .Middle_Name = objDirector.Middle_Name
                                            .Prefix = objDirector.Prefix
                                            .Last_Name = objDirector.Last_Name
                                            .BirthDate = objDirector.BirthDate
                                            .Birth_Place = objDirector.Birth_Place
                                            .Mothers_Name = objDirector.Mothers_Name
                                            .Alias = objDirector.Alias
                                            .SSN = objDirector.SSN
                                            .Passport_Number = objDirector.Passport_Number
                                            .Passport_Country = objDirector.Passport_Country
                                            .Id_Number = objDirector.ID_Number
                                            .Nationality1 = objDirector.Nationality1
                                            .Nationality2 = objDirector.Nationality2
                                            .Nationality3 = objDirector.Nationality3
                                            .Residence = objDirector.Residence
                                            .Email = objDirector.Email
                                            .email2 = objDirector.Email2
                                            .email3 = objDirector.Email3
                                            .email4 = objDirector.Email4
                                            .email5 = objDirector.Email5
                                            .Occupation = objDirector.Occupation
                                            .Employer_Name = objDirector.Employer_Name
                                            .Deceased = objDirector.Deceased
                                            .Deceased_Date = objDirector.Deceased_Date
                                            .Tax_Number = objDirector.Tax_Number
                                            .Tax_Reg_Number = objDirector.Tax_Reg_Number
                                            .Source_Of_Wealth = objDirector.Source_of_Wealth
                                            .Comment = objDirector.Comments
                                            .role = objDirector.Role


                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                        End With
                                        objdb.Entry(objDirector_New).State = Entity.EntityState.Added
                                        objdb.SaveChanges()

                                        objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objDirector_New)


                                        'Director Address(es)  (6=Address, 7=Employer Address)
                                        Dim objListAddress = objdb.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_To_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                            For Each objAddress In objListAddress
                                                Dim objNewDirectorAddress As New goAML_Act_Acc_Entity_Director_address
                                                With objNewDirectorAddress
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Act_Entity_Director_ID = objDirector_New.PK_Act_Acc_Ent_Director_ID

                                                    .Address_Type = objAddress.Address_Type
                                                    .Address = objAddress.Address
                                                    .Town = objAddress.Town
                                                    .City = objAddress.City
                                                    .Zip = objAddress.Zip
                                                    .Country_Code = objAddress.Country_Code
                                                    .State = objAddress.State
                                                    .Comments = objAddress.Comments

                                                    If objAddress.FK_Ref_Detail_Of = 6 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                End With
                                                objdb.Entry(objNewDirectorAddress).State = Entity.EntityState.Added
                                                objdb.SaveChanges()

                                                objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objNewDirectorAddress)
                                            Next
                                        End If

                                        'Director Phone(s) (6=Phone, 7=Employer Phone)
                                        Dim objListPhone = objdb.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_for_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                            For Each objPhone In objListPhone
                                                Dim objNewDirectorPhone As New goAML_Act_Acc_Entity_Director_Phone
                                                With objNewDirectorPhone
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Act_Entity_Director_ID = objDirector_New.PK_Act_Acc_Ent_Director_ID

                                                    .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                    .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                    .tph_country_prefix = objPhone.tph_country_prefix
                                                    .tph_number = objPhone.tph_number
                                                    .tph_extension = objPhone.tph_extension
                                                    .comments = objPhone.comments

                                                    If objPhone.FK_Ref_Detail_Of = 6 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                End With
                                                objdb.Entry(objNewDirectorPhone).State = Entity.EntityState.Added
                                                objdb.SaveChanges()

                                                objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objNewDirectorPhone)
                                            Next
                                        End If

                                        'Director Identification(s)
                                        Dim intDirectrorAccountPersonType As Integer = 5
                                        Dim objListIdentification = objdb.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = intDirectrorAccountPersonType And x.FK_Person_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                            For Each objIdentification In objListIdentification
                                                Dim objNewDirectorIdentification As New goAML_Activity_Person_Identification
                                                With objNewDirectorIdentification
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Person_Type = intDirectrorAccountPersonType
                                                    .FK_Act_Person_ID = objDirector_New.PK_Act_Acc_Ent_Director_ID

                                                    .Type = objIdentification.Type
                                                    .Number = objIdentification.Number
                                                    .Issue_Date = objIdentification.Issue_Date
                                                    .Expiry_Date = objIdentification.Expiry_Date
                                                    .Issued_By = objIdentification.Issued_By
                                                    .Issued_Country = objIdentification.Issued_Country
                                                    .Identification_Comment = objIdentification.Identification_Comment

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                End With
                                                objdb.Entry(objNewDirectorIdentification).State = Entity.EntityState.Added
                                                objdb.SaveChanges()

                                                objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objNewDirectorIdentification)
                                            Next
                                        End If
                                    Next
                                End If
                            End If
                        Next

                        'Save Indicator
                        If ListIndicator IsNot Nothing AndAlso ListIndicator.Count > 0 Then
                            For Each item In ListIndicator
                                Dim objNewIndicator = New goAML_Report_Indicator
                                With objNewIndicator
                                    .FK_Report = obj_goAML_Report.PK_Report_ID
                                    .FK_Indicator = item.FK_Indicator

                                    .Active = 1
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                End With

                                objdb.Entry(objNewIndicator).State = Entity.EntityState.Added
                                objdb.SaveChanges()

                                objaudittrailheader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objNewIndicator)

                            Next
                        End If
                    End If

                    objdb.SaveChanges()
                    objtrans.Commit()

                    'Update Table ODM Generate STRSAR field FK_Report_ID
                    Dim strSQL As String = "UPDATE goAML_ODM_Generate_STR_SAR SET Fk_Report_ID = " & lngReportID & " WHERE PK_goAML_ODM_Generate_STR_SAR = " & lngSARID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL, Nothing)

                    'Validate Report
                    Dim param(0) As SqlParameter
                    param(0) = New SqlParameter
                    param(0).ParameterName = "@PK_Report_ID"
                    param(0).Value = lngReportID
                    param(0).DbType = SqlDbType.BigInt
                    NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_ValidasiSatuan", param)

                    'Recalculate List of Generated XML
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_UpdategoAML_Generate_XML", Nothing)

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Function getGlobalParameterValueByID(strID As String) As String
        Try
            Dim strResult As String = Nothing
            Dim drTemp As DataRow = getDataRowByID("goAML_Ref_ReportGlobalParameter", "PK_GlobalReportParameter_ID", strID)
            If drTemp IsNot Nothing Then
                strResult = drTemp("ParameterValue")
            End If

            Return strResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function getDataRowByID(strTable As String, strTableKeyField As String, strKey As String) As DataRow
        Try
            Dim strSQL As String = "SELECT TOP 1 * FROM " & strTable & " WHERE " & strTableKeyField & "='" & strKey & "'"
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

            Return drResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function
#End Region
End Class
