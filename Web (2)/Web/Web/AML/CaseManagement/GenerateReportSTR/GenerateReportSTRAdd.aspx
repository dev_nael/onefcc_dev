﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="GenerateReportSTRAdd.aspx.vb" Inherits="GenerateReportSTRAdd" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        Ext.grid.plugin.SelectionMemory.override({
            memoryRestoreState: function () {
                this.selModel.suspendEvents();
                this.callParent();
                this.selModel.resumeEvents();
            }
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:Window ID="WindowReportIndicator" Layout="AnchorLayout" Title="Indikator" runat="server" Modal="true" Hidden="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:ComboBox ID="sar_reportIndicator" runat="server" FieldLabel="Indikator" DisplayField="Keterangan" ValueField="Kode" EmptyText="Pilih Salah Satu" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreReportIndicator" OnReadData="ReportIndicator_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server" ID="Model356">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.8, height: size.height * 0.6});" />

            <Resize Handler="#{WindowReportIndicator}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="BtnsaveReportIndicator" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">

                <DirectEvents>
                    <Click OnEvent="btnSaveReportIndicator_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancelReportIndicator" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancelreportIndicator_Click">
                        <EventMask ShowMask="true" Msg="Close window..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:Window ID="WindowAttachment" Layout="AnchorLayout" Title="Dokumen" runat="server" Modal="true" Hidden="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:FileUploadField ID="FileDoc" runat="server" FieldLabel="File Document" AnchorHorizontal="100%">
            </ext:FileUploadField>
            <ext:DisplayField ID="txtFileName" runat="server" FieldLabel="File Name" AnchorHorizontal="100%">
            </ext:DisplayField>
            <ext:TextArea ID="txtKeterangan" runat="server" FieldLabel="Description" AnchorHorizontal="100%">
            </ext:TextArea>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.8, height: size.height * 0.6});" />

            <Resize Handler="#{WindowAttachment}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="BtnsaveAttachment" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">

                <DirectEvents>
                    <Click OnEvent="btnSaveAttachment_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancelAttachment" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancelAttachment_Click">
                        <EventMask ShowMask="true" Msg="Close window..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:FormPanel ID="FormPanelInput" runat="server" ButtonAlign="Center" Title="" BodyStyle="padding:10px" AutoScroll="true">
        <Items>

            <ext:Panel runat="server" ID="pnlGeneral" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Case Information" BodyStyle="padding:10px" Collapsible="true" MarginSpec="0 0 10 0">
                <Items>
                    <ext:TextField ID="text_CaseID" runat="server" FieldLabel="Case ID" AnchorHorizontal="80%" Hidden="false" Editable ="false"/>
                    <ext:TextField ID="txt_CaseDescription" runat="server" FieldLabel="Case Description" AnchorHorizontal="80%" Hidden="false" Editable ="false" />
                    <ext:TextField ID="txt_CIFNo" runat="server" FieldLabel="CIF" AnchorHorizontal="80%" Hidden="false" Editable ="false" />
                    <ext:TextField ID="txt_CustomerName" runat="server" FieldLabel="Customer Name" AnchorHorizontal="80%" Hidden="false" Editable ="false" />
                </Items>
            </ext:Panel>

            <%-- 24-Aug-2022 Adi : Tambah Grid Panel Case Alert Transaction & Activity --%>
            <ext:GridPanel ID="gp_CaseAlert_Transaction" runat="server" Title="Case Alerts - Transaction" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200" ColumnWidth="1" AutoScroll="true">
                <Features>
                    <ext:GroupingSummary
                        ID="gs_Alert"
                        runat="server"
                        GroupHeaderTplString='Account No. {name} ({rows.length} Transaction{[values.rows.length > 1 ? "s" : ""]})'
                        HideGroupedHeader="false"
                        StartCollapsed="true" ShowSummaryRow="false"
                        >
                    </ext:GroupingSummary>
                </Features>
                <Store>
                    <ext:Store ID="store9" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Transaction"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static" GroupField="Alert_Group">
                        <Model>
                            <ext:Model runat="server" ID="Model16">
                                <Fields>
                                    <ext:ModelField Name="Alert_Group" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn16" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:DateColumn ID="DateColumn12" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column117" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column118" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                        <ext:Column ID="Column124" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column125" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                        <ext:Column ID="Column126" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn13" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:NumberColumn ID="NumberColumn14" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:Column ID="Column127" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column128" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                        <ext:Column ID="Column129" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                        <ext:Column ID="Column130" runat="server" DataIndex="Account_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column131" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column132" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column133" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column134" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader6" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar16" runat="server" HideRefresh="true" />
                </BottomBar>
            </ext:GridPanel>

            <ext:GridPanel ID="gp_CaseAlert_Activity" runat="server" Title="Case Alerts - Activity" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200" ColumnWidth="1" AutoScroll="true">
                <Features>
                    <ext:GroupingSummary
                        ID="GroupingSummary2"
                        runat="server"
                        GroupHeaderTplString='Account No. {name} ({rows.length} Activity{[values.rows.length > 1 ? "s" : ""]})'
                        HideGroupedHeader="false"
                        StartCollapsed="true" ShowSummaryRow="false"
                        >
                    </ext:GroupingSummary>
                </Features>
                <Store>
                    <ext:Store ID="store11" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Activity"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static" GroupField="Alert_Group">
                        <Model>
                            <ext:Model runat="server" ID="Model18">
                                <Fields>
                                    <ext:ModelField Name="Alert_Group" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Date_Activity" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Activity_Description" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn18" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:DateColumn ID="DateColumn14" runat="server" DataIndex="Date_Activity" Text="Activity Date" MinWidth="150" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column152" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                        <ext:Column ID="Column151" runat="server" DataIndex="Activity_Description" Text="Remark" MinWidth="250" CellWrap="true" Flex="1"></ext:Column>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader8" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar18" runat="server" HideRefresh="true" />
                </BottomBar>
            </ext:GridPanel>
            <%-- End of 24-Aug-2022 Adi : Tambah Grid Panel Case Alert Transaction & Activity --%>
            
            <%--<ext:Panel runat="server" ID="pnl_GenerateHistory" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="History Generate" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                <Items>--%>
            <ext:GridPanel ID="grid_GenerateHistory" Title="History Generate STR/SAR" runat="server" AutoScroll="true" Border="true" EmptyText="No Available Data" MarginSpec="0 0 10 0">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="StoreGenerateHistory" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model ID="Model3" runat="server">
                                <Fields>
                                    <ext:ModelField Name="userID" Type="Auto"></ext:ModelField>
                                    <ext:ModelField Name="GenerateDate" Type="Date"></ext:ModelField>
                                    <ext:ModelField Name="ReportedAs" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_Report_ID" Type="Auto"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column22" runat="server" DataIndex="userID" Text="User ID" Width="150"></ext:Column>
                        <ext:DateColumn ID="DateColumn4" runat="server" DataIndex="GenerateDate" Text="Tanggal Generate" Width="160" Format="dd-MMM-yyyy hh:mm:ss"></ext:DateColumn>
                        <ext:Column ID="Column23" runat="server" DataIndex="ReportedAs" Text="Reported As" Width="150"></ext:Column>
                        <ext:NumberColumn ID="Column24" runat="server" DataIndex="FK_Report_ID" Text="GoAML Report ID" Width="150" Format="##0" Align="Right"></ext:NumberColumn>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader runat="server"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
                <%--</Items>
            </ext:Panel>--%>
            
           <%-- <ext:ComboBox ID="sar_CIF" runat="server" FieldLabel="CIF No" DisplayField="Keterangan" ValueField="Kode" EmptyText="Pilih Salah Satu" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreCIF" OnReadData="CIF_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server" ID="Model3">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
            </ext:ComboBox>--%>

            <ext:Panel runat="server" ID="pnlSTRSAR" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="STR/SAR Report Information" BodyStyle="padding:10px" Collapsible="true" MarginSpec="0 0 10 0">
                <Items>
                    <ext:ComboBox runat="server" ID="cboReportAs" ForceSelection="true" Editable="false" FieldLabel="Report As" AllowBlank="false">
                        <Items>
                            <ext:ListItem Text="STR" Value="STR"></ext:ListItem>
                            <ext:ListItem Text="SAR" Value="SAR"></ext:ListItem>
                        </Items>
                        <DirectEvents>
                            <Change OnEvent="cboReportAs_Change"></Change>
                        </DirectEvents>
                    </ext:ComboBox>

                    <ext:FieldSet runat="server" id="fs_STRSAR_General" Padding="10" Border="true">
                        <Items>
                            <ext:DateField ID="TanggalLaporan" runat="server" Format="dd-MMM-yyyy" AllowBlank="false" FieldLabel="Tanggal Laporan" AnchorHorizontal="40%" />
                            <ext:ComboBox ID="sar_jenisLaporan" runat="server" FieldLabel="Jenis Laporan" DisplayField="Keterangan" ValueField="Kode" EmptyText="Pilih Salah Satu" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreJenisLaporan" OnReadData="JenisLaporan_ReadData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model8">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Change OnEvent="sar_jenisLaporan_DirectSelect"></Change>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:TextField ID="NoRefPPATK" runat="server" FieldLabel="No Ref PPATK" AnchorHorizontal="80%" Hidden="true" />
                            <ext:TextArea ID="alasan" runat="server" FieldLabel="Alasan" AnchorHorizontal="80%" AllowBlank="false" />
                        </Items>
                    </ext:FieldSet>

                    <%-- 24-Aug-2022 Adi : Pindah ke Sini --%>
                    <ext:Panel runat="server" ID="pnlTransaction" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transction to be Included in STR" BodyStyle="padding:10px" Collapsible="true" MarginSpec="0 0 10 0">
                        <Items>
                            <ext:Panel ID="pnl_TrnFilterBy" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <NDS:NDSDropDownField ID="dd_TrnFilterBy" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="vw_OneFCC_TransactionFilter" Label="Transaction" AnchorHorizontal="100%"  OnOnValueChanged="dd_TrnFilterBy_ValueChanged"/>
                                     </Content>
                            </ext:Panel>
                            <%--<ext:RadioGroup runat="server" ID="rg_TrnFilterBy" AnchorHorizontal="100%" FieldLabel="Transaction" ColumnsNumber="1">
                                <Items>
                                    <ext:Radio runat="server" ID="rb_TrnHitByCaseAlert" BoxLabel="Transactions Hit By Case Alert" InputValue="1" ValidationGroup="TransactionFilter" Checked="true">
                                        <DirectEvents>
                                            <Change OnEvent="rb_CustomDate_click" />
                                        </DirectEvents>
                                    </ext:Radio>

                                    <ext:Radio runat="server" ID="rb_Trn5Year" BoxLabel="Transaction 5 years back, starts from H-1" InputValue="2" ValidationGroup="TransactionFilter" >
                                        <DirectEvents>
                                            <Change OnEvent="rb_CustomDate_click" />
                                        </DirectEvents>
                                    </ext:Radio>

                                    <ext:Radio runat="server" ID="rb_CustomDate" BoxLabel="Custom Date" InputValue="3" ValidationGroup="TransactionFilter" >
                                        <DirectEvents>
                                            <Change OnEvent="rb_CustomDate_click" />
                                        </DirectEvents>
                                    </ext:Radio>
                                </Items>
                            </ext:RadioGroup>--%>
                            <ext:DateField runat="server" ID="sar_DateFrom" FieldLabel="Date From" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" Hidden="true">
                                <%--<DirectEvents>
                                    <Change OnEvent="sar_DateFrom_DirectSelect"></Change>
                                </DirectEvents>--%>
                            </ext:DateField>
                            <ext:DateField runat="server" ID="sar_DateTo" FieldLabel="Date To" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" Hidden="true" >
                                <%--<DirectEvents>
                                    <Change OnEvent="sar_DateTo_DirectSelect"></Change>
                                </DirectEvents>--%>
                            </ext:DateField>
                            <ext:Button ID="BtnSaveDetail" runat="server" Text="Search">
                                <DirectEvents>
                                    <Click OnEvent="BtnSearch_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:DisplayField ID="TextField5" runat="server" AnchorHorizontal="80%" />
                            <ext:Checkbox ID="sar_IsSelectedAll" runat="server" FieldLabel="Pilih Semua Transaksi? " />
                            <ext:Panel runat="server" ID="PanelChecked" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaction Data" BodyStyle="padding:10px" Margin="0" Collapsible="true">
                                <Items>
                                    <ext:GridPanel ID="GridPaneldetail" runat="server" AutoScroll="true" EmptyText="No Available Data">
                                        <View>
                                            <ext:GridView runat="server" EnableTextSelection="true" />
                                        </View>
                                        <Store>
                                            <ext:Store ID="StoreTransaction" runat="server" IsPagingStore="true" PageSize="10">
                                                <Model>
                                                    <ext:Model ID="ModelDetail" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="NO_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Valid" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NoTran" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NoRefTran" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TipeTran" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LokasiTran" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="KeteranganTran" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DateTransaction" Type="Date"></ext:ModelField>
                                                            <ext:ModelField Name="AccountNo" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NamaTeller" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NamaPejabat" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TglPembukuan" Type="Date"></ext:ModelField>
                                                            <ext:ModelField Name="CaraTranDilakukan" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CaraTranLain" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DebitCredit" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="OriginalAmount" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IDR" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn28" runat="server" Text="No"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column8" runat="server" DataIndex="Valid" Text="Valid Counter Party" Width="150"></ext:Column>
                                                <ext:Column ID="colStoreProcedure" runat="server" DataIndex="NoTran" Text="Nomor Transaksi" Width="150"></ext:Column>
                                                <ext:Column ID="colIsuseProcessDate" runat="server" DataIndex="NoRefTran" Text="No Ref Transaksi" Width="150"></ext:Column>
                                                <ext:Column ID="colTipeTran" runat="server" DataIndex="TipeTran" Text="Tipe Transaksi" Width="150"></ext:Column>
                                                <ext:Column runat="server" DataIndex="LokasiTran" Text="Lokasi Transaksi" Width="150"></ext:Column>
                                                <ext:Column ID="Colorder" runat="server" DataIndex="KeteranganTran" Text="Keterangan Transaksi" Width="170"></ext:Column>
                                                <ext:DateColumn ID="Column2" runat="server" DataIndex="DateTransaction" Text="Tanggal Transaksi" Width="150"></ext:DateColumn>
                                                <ext:Column ID="colAccountNo" runat="server" DataIndex="AccountNo" Text="No Akun" Width="150"></ext:Column>
                                                <ext:Column runat="server" DataIndex="NamaTeller" Text="Nama Teller" Width="120"></ext:Column>
                                                <ext:Column ID="Column1" runat="server" DataIndex="NamaPejabat" Text="Nama Pejabat" Width="120"></ext:Column>
                                                <ext:DateColumn ID="Column3" runat="server" DataIndex="TglPembukuan" Text="Tanggal Pembukuan" Width="160"></ext:DateColumn>
                                                <ext:Column runat="server" DataIndex="CaraTranDilakukan" Text="Cara Transaksi Dilakukan" Width="185"></ext:Column>
                                                <ext:Column ID="Column4" runat="server" DataIndex="CaraTranLain" Text="Cara Transaksi Lain" Width="150"></ext:Column>
                                                <ext:Column ID="Column9" runat="server" DataIndex="DebitCredit" Text="Debit Credit" Width="150"></ext:Column>
                                                <ext:Column ID="Column10" runat="server" DataIndex="OriginalAmount" Text="Original Amount" Width="150"></ext:Column>
                                                <ext:Column ID="Column5" runat="server" DataIndex="IDR" Text="Nilai Transaksi(IDR)" Width="150"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <Plugins>
                                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                                        </Plugins>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                        <TopBar>
                                            <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                                                <%--                                <Items>
                                                       <ext:Button runat="server" ID="BtnExport" Text="Export" AutoPostBack="true" OnClick="CreateExcel2007WithData"  />
                                                </Items>--%>
                                                <Items>
                                                    <ext:Button runat="server" ID="BtnExport" Text="Export" Icon="Disk">
                                                        <DirectEvents>
                                                            <Click OnEvent="CreateExcel2007WithData">
                                                            </Click>
                                                        </DirectEvents>
                                                    </ext:Button>
                                                </Items>
                                            </ext:Toolbar>
                                        </TopBar>
                                        <SelectionModel>
                                            <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi">
                                                <DirectEvents>
                                                    <Select OnEvent="OnSelect_DirectEvent">
                                                        <ExtraParams>
                                                            <ext:Parameter Name="NO_ID" Value="record.data.NO_ID" Mode="Raw" />
                                                        </ExtraParams>
                                                    </Select>
                                                    <Deselect OnEvent="OnDeSelect_DirectEvent">
                                                        <ExtraParams>
                                                            <ext:Parameter Name="NO_ID" Value="record.data.NO_ID" Mode="Raw" />
                                                        </ExtraParams>
                                                    </Deselect>
                                                </DirectEvents>
                                            </ext:CheckboxSelectionModel>
                                        </SelectionModel>
                                    </ext:GridPanel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel runat="server" ID="PanelnoCheked" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaction Data" BodyStyle="padding:10px" Margin="0" Collapsible="true" Hidden="true">
                                <Items>
                                    <ext:GridPanel ID="GridPanel1" runat="server" AutoScroll="true" EmptyText="No Available Data">
                                        <View>
                                            <ext:GridView runat="server" EnableTextSelection="true" />
                                        </View>
                                        <Store>
                                            <ext:Store ID="StoreTransaksiNoChecked" runat="server" IsPagingStore="true" PageSize="10">
                                                <Model>
                                                    <ext:Model ID="Model2" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="NO_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Valid" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NoTran" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NoRefTran" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TipeTran" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LokasiTran" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="KeteranganTran" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DateTransaction" Type="Date"></ext:ModelField>
                                                            <ext:ModelField Name="AccountNo" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NamaTeller" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NamaPejabat" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TglPembukuan" Type="Date"></ext:ModelField>
                                                            <ext:ModelField Name="CaraTranDilakukan" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CaraTranLain" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DebitCredit" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="OriginalAmount" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IDR" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column11" runat="server" DataIndex="Valid" Text="Valid Counter Party" Width="150"></ext:Column>
                                                <ext:Column ID="Column12" runat="server" DataIndex="NoTran" Text="Nomor Transaksi" Width="150"></ext:Column>
                                                <ext:Column ID="Column13" runat="server" DataIndex="NoRefTran" Text="No Ref Transaksi" Width="150"></ext:Column>
                                                <ext:Column ID="Column14" runat="server" DataIndex="TipeTran" Text="Tipe Transaksi" Width="150"></ext:Column>
                                                <ext:Column runat="server" DataIndex="LokasiTran" Text="Lokasi Transaksi" Width="150"></ext:Column>
                                                <ext:Column ID="Column15" runat="server" DataIndex="KeteranganTran" Text="Keterangan Transaksi" Width="170"></ext:Column>
                                                <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="DateTransaction" Text="Tanggal Transaksi" Width="150"></ext:DateColumn>
                                                <ext:Column ID="Column16" runat="server" DataIndex="AccountNo" Text="No Akun" Width="150"></ext:Column>
                                                <ext:Column runat="server" DataIndex="NamaTeller" Text="Nama Teller" Width="120"></ext:Column>
                                                <ext:Column ID="Column17" runat="server" DataIndex="NamaPejabat" Text="Nama Pejabat" Width="120"></ext:Column>
                                                <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="TglPembukuan" Text="Tanggal Pembukuan" Width="160"></ext:DateColumn>
                                                <ext:Column runat="server" DataIndex="CaraTranDilakukan" Text="Cara Transaksi Dilakukan" Width="185"></ext:Column>
                                                <ext:Column ID="Column18" runat="server" DataIndex="CaraTranLain" Text="Cara Transaksi Lain" Width="150"></ext:Column>
                                                <ext:Column ID="Column19" runat="server" DataIndex="DebitCredit" Text="Debit Credit" Width="150"></ext:Column>
                                                <ext:Column ID="Column20" runat="server" DataIndex="OriginalAmount" Text="Original Amount" Width="150"></ext:Column>
                                                <ext:Column ID="Column21" runat="server" DataIndex="IDR" Text="Nilai Transaksi(IDR)" Width="150"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <Plugins>
                                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                                        </Plugins>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                        <TopBar>
                                            <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true">
                                                <%--                                <Items>
                                                       <ext:Button runat="server" ID="BtnExport" Text="Export" AutoPostBack="true" OnClick="CreateExcel2007WithData"  />
                                                </Items>--%>
                                                <Items>
                                                    <ext:Button runat="server" ID="BtnExportNoChecked" Text="Export" Icon="Disk">
                                                        <DirectEvents>
                                                            <Click OnEvent="CreateExcel2007WithDataNoChecked">
                                                            </Click>
                                                        </DirectEvents>
                                                    </ext:Button>
                                                </Items>
                                            </ext:Toolbar>
                                        </TopBar>
                                    </ext:GridPanel>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                    <%-- End of 24-Aug-2022 Adi : Pindah ke Sini --%>


                    <%-- 24-Aug-2022 Adi : Tambah GridPanel untuk SAR Activity --%>
                    <ext:Panel runat="server" ID="pnlActivity" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Activities to be Included in SAR" BodyStyle="padding:0px" Collapsible="true" MarginSpec="0 0 10 0">
                        <Items>
                            <ext:GridPanel ID="gp_Activity" runat="server" AutoScroll="true" EmptyText="No Available Data">
                                <View>
                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                </View>
                                <Store>
                                    <ext:Store ID="Store2" runat="server" IsPagingStore="true" PageSize="10">
                                        <Model>
                                            <ext:Model ID="Model5" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="ACCOUNT_NO" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Significance" Type="Int"></ext:ModelField>
                                                    <ext:ModelField Name="Reason" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No"></ext:RowNumbererColumn>

                                        <ext:CommandColumn ID="cc_Activity" runat="server" Text="Action" Width="70">
                                            <Commands>
                                                <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                                    <ToolTip Text="Edit"></ToolTip>
                                                </ext:GridCommand>
                                            </Commands>

                                            <DirectEvents>
                                                <Command OnEvent="gc_Activity">
                                                    <EventMask ShowMask="true"></EventMask>
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.ACCOUNT_NO" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>

                                        <ext:Column ID="Column34" runat="server" DataIndex="ACCOUNT_NO" Text="Account No." Width="180"></ext:Column>
                                        <ext:NumberColumn ID="Column35" runat="server" DataIndex="Significance" Text="Significance" Width="120" Format="#,##0" Align="Right"></ext:NumberColumn>
                                        <ext:Column ID="Column36" runat="server" DataIndex="Reason" Text="Reason" Width="300" CellWrap="true"></ext:Column>
                                        <ext:Column ID="Column37" runat="server" DataIndex="Comments" Text="Comments" Flex="1" CellWrap="true"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                        </Items>
                    </ext:Panel>
                    <%-- End 24-Aug-2022 Adi : Tambah GridPanel untuk SAR Activity --%>


                    <ext:Panel runat="server" ID="PanelReportIndicator" Hidden="true" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Indikator" BodyStyle="padding:10px" MarginSpec="0 0 10 0" Collapsible="true">
                        <Items>
                            <ext:GridPanel ID="GridPanelReportIndicator" runat="server" EmptyText="No Available Data">
                                <View>
                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                </View>
                                <TopBar>
                                    <ext:Toolbar runat="server">
                                        <Items>
                                            <ext:Button runat="server" ID="BtnAddReportIndikator" Text="Tambah Indicator" Icon="Add">
                                                <DirectEvents>
                                                    <Click OnEvent="btn_addIndicator_click">
                                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="500"></EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="StoreReportIndicatorData" runat="server">
                                        <Model>
                                            <ext:Model runat="server" ID="Model355" IDProperty="PK_Report_Indicator">
                                                <Fields>
                                                    <ext:ModelField Name="PK_Report_Indicator" Type="Int"></ext:ModelField>
                                                    <ext:ModelField Name="FK_Indicator" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn88" runat="server" Text="No"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column312" runat="server" DataIndex="FK_Indicator" Text="Kode" Flex="1"></ext:Column>
                                        <ext:Column ID="Column313" runat="server" DataIndex="Keterangan" Text="Keterangan" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn87" runat="server" Text="Action" Flex="1">

                                            <Commands>
                                                <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                                    <ToolTip Text="Edit"></ToolTip>
                                                </ext:GridCommand>
                                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                                    <ToolTip Text="Detail"></ToolTip>
                                                </ext:GridCommand>
                                                <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationViewDetail" MinWidth="70">
                                                    <ToolTip Text="Delete"></ToolTip>
                                                </ext:GridCommand>
                                            </Commands>

                                            <DirectEvents>

                                                <Command OnEvent="GridcommandReportIndicator">
                                                    <EventMask ShowMask="true"></EventMask>
                                                    <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>

                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Report_Indicator" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>



                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                        </Items>
                    </ext:Panel>

                    <ext:Panel runat="server" ID="PanelAttachment" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Attachment Document" BodyStyle="padding:10px" Margin="0" Collapsible="true">
                        <Items>
                            <ext:GridPanel ID="GridPanelAttachment" runat="server" EmptyText="No Available Data">
                                <View>
                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                </View>
                                <TopBar>
                                    <ext:Toolbar runat="server">
                                        <Items>
                                            <ext:Button runat="server" ID="ButtonAttachment" Text="Tambah Dokumen" Icon="Add">
                                                <DirectEvents>
                                                    <Click OnEvent="btn_addAttachment_click">
                                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="500"></EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="StoreAttachment" runat="server">
                                        <Model>
                                            <ext:Model runat="server" ID="Model1" IDProperty="PK_ID">
                                                <Fields>
                                                    <ext:ModelField Name="PK_ID" Type="Int"></ext:ModelField>
                                                    <ext:ModelField Name="FileName" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column6" runat="server" DataIndex="FileName" Text="File Name" Flex="1">
                                            <Commands>
                                                <ext:ImageCommand Icon="DiskDownload" CommandName="Download" ToolTip-Text="Download Dokumen"></ext:ImageCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GridcommandAttachment">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                </Command>
                                            </DirectEvents>
                                        </ext:Column>
                                        <ext:Column ID="Column7" runat="server" DataIndex="Keterangan" Text="Description" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumnAttachment" runat="server" Text="Action" Flex="1">

                                            <Commands>
                                                <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                                    <ToolTip Text="Edit"></ToolTip>
                                                </ext:GridCommand>
                                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                                    <ToolTip Text="Detail"></ToolTip>
                                                </ext:GridCommand>
                                                <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationViewDetail" MinWidth="70">
                                                    <ToolTip Text="Delete"></ToolTip>
                                                </ext:GridCommand>
                                            </Commands>

                                            <DirectEvents>

                                                <Command OnEvent="GridcommandAttachment">
                                                    <EventMask ShowMask="true"></EventMask>
                                                    <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>

                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Panel>
        </Items>

        <Buttons>
            <ext:Button ID="Button1" runat="server" Icon="Disk" Text="Generate">
                <Listeners>
                    <Click Handler="if (!#{FormPanelInput}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="BtnGenerate_DirectEvent">
                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancelDetail" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_DirectEvent">
                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

        </Buttons>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>

        </Items>
        <Buttons>

            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>


    <%-- 24-Aug-2022 Adi : Window Edit Activity --%>
    <%-- ================== ACTIVITY ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_Activity" Layout="AnchorLayout" Title="Activity" runat="server" Modal="true" Hidden="true" Maximizable="true" BodyPadding="10" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:InfoPanel ID="info_ValidationResultActivity" ClientIDMode="Static" runat="server" UI="Warning" Layout="AnchorLayout" Title="Validation Result" AutoScroll="true" IconCls="#Error" MarginSpec="10 0" Border="True" Hidden="true" Collapsible="true"></ext:InfoPanel>

            <ext:Panel runat="server" ID="Panel2" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <ext:DisplayField runat="server" LabelWidth="200" ID="txt_ACCOUNT_NO" FieldLabel="Account No." AnchorHorizontal="70%" />
                    <ext:NumberField runat="server" LabelWidth="200" ID="txt_Activity_Significance" FieldLabel="Significance" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Activity_Reason" FieldLabel="Reason" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"/>
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_Activity_Comment" FieldLabel="Comments" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"  />
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Activity_Save" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">
                <DirectEvents>
                    <Click OnEvent="btn_Activity_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Activity_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Activity_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.80, height: size.height * 0.80});" />
            <Resize Handler="#{Window_Activity}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF MULTIPARTY ENTRY WINDOWS ========================== --%>


</asp:Content>
