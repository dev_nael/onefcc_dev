﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.master" AutoEventWireup="false" CodeFile="CaseManagementAlertView_Questionaire.aspx.vb" Inherits="CaseManagementAlertView_Questionaire" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };

        var prepareCommandCollection = function (grid, toolbar, rowIndex, record) {
            var EditButton = toolbar.items.get(1);
            //var PrintMemoButton = toolbar.items.get(2);
            //var GenerateSTRButton = toolbar.items.get(3);
            var wf = record.data.FK_CaseStatus_ID

            if (EditButton != undefined) {
                //if (wf == "3" || wf == "4" || wf == "6" || wf == "7") {
                //    EditButton.setDisabled(true)
                //} else {
                //    EditButton.setDisabled(false)
                //}
                //30-Mar-2022 Adi : yang bisa edit harusnya hanya status 0=New, 5=Need Correction, 8=Re-Open
                if (wf == "0" || wf == "5" || wf == "8") {
                    EditButton.setDisabled(false)
                } else {
                    EditButton.setDisabled(true)
                }
            }
           // if (PrintMemoButton != undefined) {
            //    if (wf == "3") {
            //        PrintMemoButton.setDisabled(false)
            //    } else {
            //        PrintMemoButton.setDisabled(true)
            //    }
           // }
           // if (GenerateSTRButton != undefined) {
            //    if (wf == "3") {
            //        GenerateSTRButton.setDisabled(false)
            //    } else {
            //        GenerateSTRButton.setDisabled(true)
            //    }
            //}
        }

        /*var prepareCommandCollection_Checker = function (grid, toolbar, rowIndex, record) {
            var PrintMemoButton = toolbar.items.get(1);
            var GenerateSTRButton = toolbar.items.get(2);
            var wf = record.data.FK_CaseStatus_ID
            if (PrintMemoButton != undefined) {
                if (wf == "3") {
                    PrintMemoButton.setDisabled(false)
                } else {
                    PrintMemoButton.setDisabled(true)
                }
            }
            if (GenerateSTRButton != undefined) {
                if (wf == "3") {
                    GenerateSTRButton.setDisabled(false)
                } else {
                    GenerateSTRButton.setDisabled(true)
                }
            }
        }*/

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <ext:GridPanel ID="GridpanelView" ClientIDMode="Static" runat="server" Title="Title" Width="2000">
        <View>
            <ext:GridView runat="server" EnableTextSelection="true" />
        </View>
        <Store>
            <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadData">

                <Sorters>
                    <%--<ext:DataSorter Property="" Direction="ASC" />--%>
                </Sorters>
                <Proxy>
                    <ext:PageProxy />
                </Proxy>
            </ext:Store>
        </Store>
        <SelectionModel>
            <ext:CheckboxSelectionModel runat="server" Mode="Multi" CheckOnly="true">
            </ext:CheckboxSelectionModel>
        </SelectionModel>
        <Plugins>
            <%--<ext:GridFilters ID="GridFilters1" runat="server" />--%>
            <ext:FilterHeader ID="grdiheaderfilter" runat="server" Remote="true"></ext:FilterHeader>

        </Plugins>
        <BottomBar>

            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />

        </BottomBar>

        <%--Begin Update Penambahan Advance Filter--%>
        <DockedItems>
            <%--End Update Penambahan Advance Filter--%>
            <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                <Items>

                    <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export :">
                        <Items>
                            <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                            <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                        </Items>
                    </ext:ComboBox>
                    <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" AutoPostBack="true" OnClick="ExportExcel" ClientIDMode="Static" />
                    <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page" AutoPostBack="true" OnClick="ExportAllExcel" />
                    <%--<ext:Button ID="Button1" runat="server" Text="Print Current Page" Icon="Printer" Handler="this.up('grid').print();" />--%>
                    <ext:Button ID="BtnAdd" runat="server" Text="Add New Record" Icon="Add" Handler="NawadataDirect.BtnAdd_Click()" hidden="true" />
                    <%--Begin Update Penambahan Advance Filter--%>
                    <ext:Button ID="AdvancedFilter" runat="server" Text="Advanced Filter" Icon="Magnifier" Handler="NawadataDirect.BtnAdvancedFilter_Click()" />
                    <%--End Update Penambahan Advance Filter--%>

                </Items>
            </ext:Toolbar>
            <%--Begin Update Penambahan Advance Filter--%>
            <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true" Dock="Top" Hidden="true">
                <Items>
                    <ext:HyperlinkButton ID="HyperlinkButton1" runat="server" Text="Clear Advanced Filter">
                        <DirectEvents>
                            <Click OnEvent="btnClear_Click"></Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                    <ext:Label ID="LblAdvancedFilter" runat="server" Text="">
                    </ext:Label>
                </Items>
            </ext:Toolbar>
        </DockedItems>
        <%--End Update Penambahan Advance Filter--%>

        <Listeners>
            <ViewReady Handler="columnAutoResize(this);
                    this.getStore().on('load', Ext.bind(columnAutoResize, null, [this]));"
                Delay="10" />
        </Listeners>
        <%--<SelectionModel>
            <ext:CheckboxSelectionModel runat="server" Mode="Single">
            </ext:CheckboxSelectionModel>
        </SelectionModel>--%>
    </ext:GridPanel>
     <%--Begin Update Penambahan Advance Filter--%>
    <ext:Panel ID="Panel1" runat="server" Hidden="true">
        <Content>
            <uc1:AdvancedFilter runat="server" ID="AdvancedFilter1" />
        </Content>
        <Items>
        </Items>
    </ext:Panel>
    <%--End Update Penambahan Advance Filter--%>
</asp:Content>

