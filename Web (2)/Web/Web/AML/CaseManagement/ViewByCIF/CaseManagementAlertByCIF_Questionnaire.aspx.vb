﻿Imports System.Data
Imports System.Globalization
Imports Elmah
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL
Imports OfficeOpenXml
Partial Class AML_CaseManagement_ViewByCIF_CaseManagementAlertByCIF_Questionnaire
    Inherits ParentPage
    Public Property IDUnik() As String
        Get
            Return Session("CaseManagementAlertByCIF_Questionnaire.IDUnik")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertByCIF_Questionnaire.IDUnik") = value
        End Set
    End Property

    Private Sub AML_CaseManagement_ViewByCIF_CaseManagementAlertByCIF_Questionnaire_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                IDUnik = Session("CaseIDCSAlertCIF")
                LoadToFormPanel(FormPanelQuestionnaire, Nothing)
                Session("CaseIDCSAlertCIF") = Nothing

            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub AML_CaseManagement_ViewByCIF_CaseManagementAlertByCIF_Questionnaire_Init(sender As Object, e As EventArgs) Handles Me.Init
        Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AML_CaseManagement_ViewByCIF_CaseManagementAlertByCIF_Questionnaire_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.view
    End Sub

    Protected Function ExtText(pn As Ext.Net.FormPanel, strLabel As String, strFieldName As String, bRequired As Boolean, intMaxSize As Integer, intgridpos As Integer, StrJawaban As String) As Ext.Net.TextField
        Dim objTextField As New Ext.Net.TextField
        objTextField.ID = strFieldName
        objTextField.ClientIDMode = Web.UI.ClientIDMode.Static
        objTextField.FieldLabel = strLabel

        objTextField.ID = strFieldName
        objTextField.Name = strFieldName
        objTextField.AllowBlank = Not bRequired
        objTextField.BlankText = strLabel & " is required."
        objTextField.MaxLength = intMaxSize
        objTextField.LabelWidth = 500
        objTextField.MinWidth = 900

        objTextField.AnchorHorizontal = "80%"
        objTextField.ReadOnly = True

        objTextField.Value = StrJawaban

        pn.Add(objTextField)
        Return objTextField
    End Function
    '------- LOAD COMPONENT
    Protected Function ExtPanel(pn As FormPanel, strName As String, strTitle As String, isRiskRating As Boolean) As Ext.Net.Panel
        Try
            Dim objPanel As New Ext.Net.Panel
            With objPanel
                .ID = strName
                .ClientIDMode = Web.UI.ClientIDMode.Static
                .Title = strTitle
                .MarginSpec = "0 0 10 0"
                If isRiskRating Then
                    .BodyStyle = "background-color: #33ff3a;"
                    .Border = False
                    .Layout = "ColumnLayout"
                Else
                    .Collapsible = True
                    .Border = True
                    .BodyPadding = 10
                End If
            End With

            pn.Add(objPanel)
            Return objPanel

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Protected Sub Load_Questionnaire(objpanelinput As Ext.Net.FormPanel, customerType As String)
        Try
            Dim strQuery As String = ""
            Dim intSequence As Integer


            'Load Question Group 
            'strQuery = "SELECT DISTINCT ofqg.PK_OneFCC_Question_Group_ID, ofqg.Question_Group_Name, ofqg.Sequence"
            'strQuery = strQuery & " FROM OneFCC_Questionnaire ofq"
            'strQuery = strQuery & " JOIN OneFCC_Question_Group AS ofqg ON ofq.FK_OneFCC_Question_Group_ID = ofqg.PK_OneFCC_Question_Group_ID"
            'strQuery = strQuery & " WHERE ofqg.FK_OneFCC_Question_Type_Code = 'EDD'"
            'strQuery = strQuery & " ORDER BY ofqg.Sequence"
            strQuery = "select * from OneFCC_CaseManagement_Questionaire_Answer where FK_CASEMANAGEMENT_ID = " & Session("CaseIDCSAlertCIF") & " and FK_OneFCC_Question_Parent_ID is null"

            Dim dtQuestionGroup As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
            For Each row In dtQuestionGroup.Rows
                intSequence = row("SEQUENCE")
                Dim isRequired As Boolean = False
                ExtText(objpanelinput, intSequence & ". " & row("QUESTION"), "ANSWER_" & row("PK_CM_QUESTIONAIRE_ANSWER_ID"), isRequired, 8000, intSequence, row("ANSWER"))
                Dim strQueryChild As String = "select * from OneFCC_CaseManagement_Questionaire_Answer where FK_CASEMANAGEMENT_ID = " & Session("CaseIDCSAlertCIF") & " and FK_OneFCC_Question_Parent_ID is not null"
                Dim dtQuestionChild As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQueryChild, Nothing)
                Dim intSequenceChild As Integer = 0
                For Each child In dtQuestionChild.Rows
                    If child("FK_OneFCC_Question_Parent_ID") = row("FK_OneFCC_Question_ID") Then
                        intSequenceChild = intSequenceChild + 1
                        Dim strPrefix As String = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & intSequenceChild
                        ExtText(objpanelinput, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_CM_QUESTIONAIRE_ANSWER_ID"), isRequired, 8000, intSequenceChild, child("ANSWER"))
                    End If
                Next
            Next
                If dtQuestionGroup.Rows.Count = 0 Then

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadToFormPanel(objpanelinput As Ext.Net.FormPanel, customerType As String)
        Try
            objpanelinput.Title = "Question"
            Load_Questionnaire(objpanelinput, Nothing)
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportQuestionnaireCaseAlert_PerCIF(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            Dim StrCIFNO = Session("CIFNoCSAlertCIF")
            Session("CIFNoCSAlertCIF") = Nothing
            Dim strQuery As String = "select * from OneFCC_CaseManagement_Questionaire_Answer where FK_CASEMANAGEMENT_ID = " & IDUnik

            'Dim strQuery As String = "SELECT ofcmtt.Date_Transaction"
            'strQuery &= " ,ofcmtt.Ref_Num"
            'strQuery &= " ,ofcmtt.Transmode_Code"
            'strQuery &= " ,ofcmtt.Transaction_Location"
            'strQuery &= " ,ofcmtt.Debit_Credit"
            'strQuery &= " ,ofcmtt.Currency"
            'strQuery &= " ,ofcmtt.Original_Amount"
            'strQuery &= " ,ofcmtt.IDR_Amount"
            'strQuery &= " ,ofcmtt.Transaction_Remark"
            'strQuery &= " ,ofcmtt.Account_NO"
            'strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'CIF' END AS CounterPartyType"
            'strQuery &= " ,ofcmtt.Account_No_Lawan"
            'strQuery &= " ,ofcmtt.CIF_No_Lawan"
            'strQuery &= " ,ofcmtt.CIF_No_Lawan"
            'strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            'strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            'strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            'strQuery &= " LEFT JOIN goAML_Ref_CIF AS garw"
            'strQuery &= " ON ofcmtt.CIF_No_Lawan = garw.CIF_No"
            'strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            'strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            'strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If
            Dim strSQL = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & StrCIFNO & "'"
            Dim drCIF As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            Dim StrCIF As String = ""
            Dim StrCIFName As String = ""
            Dim StrCIFDOB As String = ""
            Dim StrCIFOpeningDate As String = ""
            Dim StrCIFPOB As String = ""
            Dim StrCIFGender As String = ""
            Dim StrCIFPEP As String = ""
            Dim StrCIFBranch As String = ""
            Dim StrCIFRiskCode As String = ""

            If drCIF IsNot Nothing Then

                StrCIF = StrCIFNO


                Dim strQuery22 As String = "SELECT CustomerName FROM AML_CUSTOMER WHERE CIFNo='" & StrCIF & "'"
                Dim drCIFName As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery22)
                If drCIFName IsNot Nothing Then
                    If Not IsDBNull(drCIFName("CustomerName")) Then
                        StrCIFName = drCIFName("CustomerName")
                    End If
                End If

                If Not IsDBNull(drCIF("DATEOFBIRTH")) Then
                    StrCIFDOB = CDate(drCIF("DATEOFBIRTH")).ToString("dd-MMM-yyyy")
                End If

                StrCIFPOB = drCIF("PLACEOFBIRTH")

                If Not IsDBNull(drCIF("FK_AML_JenisKelamin_Code")) Then
                    strSQL = "SELECT TOP 1 * FROM goAML_Ref_Jenis_Kelamin WHERE Kode='" & drCIF("FK_AML_JenisKelamin_Code") & "'"
                    Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drGender IsNot Nothing Then
                        StrCIFGender = drGender("Kode") & " - " & drGender("Keterangan")
                    Else
                        StrCIFGender = drCIF("FK_AML_JenisKelamin_Code")
                    End If
                End If

                If Not IsDBNull(drCIF("FK_AML_Creation_Branch_Code")) Then
                    strSQL = "SELECT TOP 1 * FROM AML_BRANCH WHERE FK_AML_BRANCH_CODE='" & drCIF("FK_AML_Creation_Branch_Code") & "'"
                    Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drGender IsNot Nothing Then
                        StrCIFBranch = drGender("FK_AML_BRANCH_CODE") & " - " & drGender("BRANCH_NAME")
                    Else
                        StrCIFBranch = drCIF("FK_AML_Creation_Branch_Code")
                    End If
                End If

                If Not IsDBNull(drCIF("OpeningDate")) Then
                    StrCIFOpeningDate = CDate(drCIF("OpeningDate")).ToString("dd-MMM-yyyy")
                End If

                If Not IsDBNull(drCIF("IS_PEP")) Then
                    If drCIF("IS_PEP") = 0 Then
                        StrCIFPEP = "No"
                    Else
                        StrCIFPEP = "Yes"
                    End If
                Else
                    StrCIFPEP = "N/A"
                End If

                If Not IsDBNull(drCIF("FK_AML_RISK_CODE")) Then
                    strSQL = "SELECT TOP 1 * FROM AML_RISK_RATING WHERE RISK_RATING_CODE ='" & drCIF("FK_AML_RISK_CODE") & "'"
                    Dim drRisk As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drRisk IsNot Nothing Then
                        StrCIFRiskCode = drRisk("RISK_RATING_CODE") & " - " & drRisk("RISK_RATING_NAME")
                    Else
                        StrCIFRiskCode = drCIF("FK_AML_RISK_CODE")
                    End If


                End If
            End If

            'Get Data CM
            'strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            'Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))

            Dim strQuery2 As String = " select * from OneFCC_CaseManagement_Questionaire_Answer where FK_CASEMANAGEMENT_ID = " & IDUnik


            Dim dtTypologyTransaction As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2)
            If dtTypologyTransaction Is Nothing Then
                dtTypologyTransaction = New DataTable
            End If

            GridQuestionnaire.GetStore.DataSource = dtTypologyTransaction
            GridQuestionnaire.GetStore.DataBind()
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In GridQuestionnaire.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("QuestionnairePerCaseID")

                    'Display Header of CM

                    ws.Cells("A1").Value = "CIF No"
                    ws.Cells("B1").Value = StrCIF

                    ws.Cells("A3").Value = "Customer Name"
                    ws.Cells("B3").Value = StrCIFName

                    ws.Cells("A4").Value = "Date of Birth"
                    ws.Cells("B4").Value = StrCIFDOB

                    ws.Cells("A5").Value = "Place of Birth"
                    ws.Cells("B5").Value = StrCIFPOB

                    ws.Cells("A6").Value = "Gender"
                    ws.Cells("B6").Value = StrCIFGender

                    ws.Cells("A7").Value = "PEP"
                    ws.Cells("B7").Value = StrCIFPEP

                    ws.Cells("A2").Value = "CaseID"
                    ws.Cells("B2").Value = IDUnik

                    'ws.Cells("A8").Value = "Branch"
                    'ws.Cells("B8").Value = StrCIFBranch

                    'ws.Cells("A9").Value = "Opening Date"
                    'ws.Cells("B9").Value = StrCIFOpeningDate

                    'ws.Cells("A10").Value = "Risk Rating"
                    'ws.Cells("B10").Value = StrCIFRiskCode



                    'objtbl.Columns("Case_ID").SetOrdinal(0)
                    'objtbl.Columns("CIF_No").SetOrdinal(1)
                    'objtbl.Columns("CIF_Name").SetOrdinal(2)
                    'objtbl.Columns("Alert_Type").SetOrdinal(3)
                    'objtbl.Columns("Case_Status").SetOrdinal(4)
                    'objtbl.Columns("Workflow_Step").SetOrdinal(5)
                    'objtbl.Columns("Created_Date").SetOrdinal(6)
                    'objtbl.Columns("Last_Update_Date").SetOrdinal(7)
                    'objtbl.Columns("PIC").SetOrdinal(8)
                    'objtbl.Columns("Aging").SetOrdinal(9)
                    'objtbl.Columns("Last_Proposed_Action").SetOrdinal(10)
                    'objtbl.Columns("Count_Same_Case").SetOrdinal(11)
                    'objtbl.Columns("Is_Closed").SetOrdinal(12)

                    'objtbl.Columns("Case_ID").ColumnName = "Case ID"
                    'objtbl.Columns("CIF_No").ColumnName = "CIF No"
                    'objtbl.Columns("CIF_Name").ColumnName = "Name"
                    'objtbl.Columns("Alert_Type").ColumnName = "Alert Type"
                    'objtbl.Columns("Case_Status").ColumnName = "Status"
                    'objtbl.Columns("Workflow_Step").ColumnName = "Step"
                    'objtbl.Columns("Created_Date").ColumnName = "Create Date"
                    'objtbl.Columns("Last_Update_Date").ColumnName = "Last Update Date"
                    'objtbl.Columns("PIC").ColumnName = "PIC"
                    'objtbl.Columns("Aging").ColumnName = "Aging"
                    'objtbl.Columns("Last_Proposed_Action").ColumnName = "Last Proposed Action"
                    'objtbl.Columns("Count_Same_Case").ColumnName = "Count Same Case"
                    'objtbl.Columns("Is_Closed").ColumnName = "Is Closed"

                    ws.Cells("A11").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Management Questionnaire Per CaseID (CaseID " & IDUnik & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

End Class
