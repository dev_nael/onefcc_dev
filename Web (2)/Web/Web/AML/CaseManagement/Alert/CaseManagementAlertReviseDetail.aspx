﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CaseManagementAlertReviseDetail.aspx.vb" Inherits="CaseManagementAlertReviseDetail" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true">
        <Items>
            <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="20 20 10 20">
                <Content>
                    <NDS:NDSDropDownField ID="nDSDropDownField_AlertType" ValueField="PK_CaseManagement_AlertType_ID" DisplayField="Alert_Type" runat="server" StringField="PK_CaseManagement_AlertType_ID,Alert_Type" StringTable="OneFCC_CaseManagement_AlertType" Label="Alert Type" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" OnOnValueChanged="NDSDropDownField_AlertType_OnValueChanged"/>
                    <NDS:NDSDropDownField ID="nDSDropDownField_RuleBasic" ValueField="PK_Rule_Basic_ID" DisplayField="Rule_Basic_Name" runat="server" StringField="PK_Rule_Basic_ID,Rule_Basic_Name" StringTable="OneFcc_MS_Rule_Basic" Label="Rule Basic" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" />
                    <ext:TextArea ID="textArea_CaseDescription" runat="server" FieldLabel="Case Description" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200"/>
                   
                    <%--Add 05-Jan-2023--%>
                    <ext:GridPanel ID="gp_CaseAlert_Previous_Typology_Transaction" runat="server" Title="Previous Proposed Transaction" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200" ColumnWidth="1" Hidden="true">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <%--<ext:Button runat="server" ID="BtnExportAll_CaseAlert_Typology" Text="Export to Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="ExportAll_CaseAlert_Typology" IsUpload="true"/>
                                        </DirectEvents>
                                    </ext:Button>--%>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Previous_Typology_Transaction" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Typology_Transaction"
                                RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                                <Model>
                                    <ext:Model runat="server" ID="Model3" IDProperty="PK_OneFCC_CaseManagement_Typology_Transaction_ID">
                                        <Fields>
                                            <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters></Sorters>
                                <Proxy>
                                    <ext:PageProxy />
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:DateColumn ID="DateColumn3" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column73" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                                <ext:Column ID="Column77" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                                <ext:Column ID="Column78" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                                <ext:Column ID="Column79" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn1" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                <ext:NumberColumn ID="NumberColumn6" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column8" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                                <ext:Column ID="Column80" runat="server" DataIndex="Account_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column81" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column82" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader ID="FilterHeader2" runat="server" Remote="true"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="true" />
                        </BottomBar>
                    </ext:GridPanel>

                    
                    <ext:GridPanel ID="gp_CaseAlert_Previous_Typology_Activity" runat="server" Title="Previous Proposed Activity" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="300" ColumnWidth="1" Hidden="true">
                        <Features>
                            <ext:GroupingSummary
                                ID="GroupingSummary2"
                                runat="server"
                                GroupHeaderTplString='Account No. {name} ({rows.length} Activity{[values.rows.length > 1 ? "s" : ""]})'
                                HideGroupedHeader="false"
                                StartCollapsed="true" ShowSummaryRow="false"
                                >
                            </ext:GroupingSummary>
                        </Features>
                        <Store>
                            <ext:Store ID="store_Previous_Typology_Activity" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Activity"
                                RemoteFilter="true" RemoteSort="true" ClientIDMode="Static" GroupField="Alert_Group">
                                <Model>
                                    <ext:Model runat="server" ID="Model18">
                                        <Fields>
                                            <ext:ModelField Name="Alert_Group" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Date_Activity" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Activity_Description" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters></Sorters>
                                <Proxy>
                                    <ext:PageProxy />
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn18" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:DateColumn ID="DateColumn14" runat="server" DataIndex="Date_Activity" Text="Activity Date" MinWidth="150" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column152" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                                <ext:Column ID="Column151" runat="server" DataIndex="Activity_Description" Text="Remark" MinWidth="250" CellWrap="true" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader ID="FilterHeader8" runat="server" Remote="true"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar18" runat="server" HideRefresh="true" />
                        </BottomBar>
                    </ext:GridPanel>
                            
                    <ext:GridPanel ID="gp_WorkflowHistory" runat="server" Title="Workflow History" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="store_WorkflowHistory" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_WorkflowHistory"
                                RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                                <Model>
                                    <ext:Model runat="server" ID="Model10" IDProperty="PK_CaseManagement_WorkflowHistory_ID">
                                        <Fields>
                                            <%--<ext:ModelField Name="Workflow_Step" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                            <%--<ext:ModelField Name="Proposed_Action" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="Analysis_Result" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters></Sorters>
                                <Proxy>
                                    <ext:PageProxy />
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server" Text="No"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="Column52" runat="server" DataIndex="Workflow_Step" Text="Step" Width="50" Align="Center"></ext:Column>--%>
                                <ext:Column ID="Column53" runat="server" DataIndex="CreatedBy" Text="Created By" Width="150"></ext:Column>
                                <%--<ext:Column ID="Column54" runat="server" DataIndex="Proposed_Action" Text="Proposed Status" Width="150"></ext:Column>--%>
                                <ext:Column ID="Column55" runat="server" DataIndex="Analysis_Result" Text="Notes" Width="650" CellWrap="true"></ext:Column>
                                <ext:DateColumn ID="DateColumn9" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end 05-Jan-2023--%>

                    <ext:Button ID="btn_Import_Customer" runat="server" Icon="Zoom" Text="Search Data Customer" MarginSpec="0 10 10 0">
                        <DirectEvents>
                            <Click OnEvent="Btn_Import_Customer_Click">
                                <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:TextField ID="txt_CIFNo_WICNo" LabelWidth="200" runat="server" FieldLabel="CIF" AnchorHorizontal="70%" ReadOnly="true" FieldStyle="background-color: #ddd"></ext:TextField> 
                    <ext:TextField ID="txt_Name" LabelWidth="200" runat="server" FieldLabel="Nama" AnchorHorizontal="70%" ReadOnly="true" FieldStyle="background-color: #ddd"></ext:TextField>
                    <ext:Button ID="btn_OpenWindowCustomer" runat="server" Icon="ApplicationViewDetail" Text="More Detail" MarginSpec="0 10 10 0" Hidden="true">
                        <DirectEvents>
                            <Click OnEvent="Btn_OpenWindowCustomer_Click">
                                <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                    <ext:Checkbox
                        runat="server"
                        ID="checkbox_IsTransaction"
                        FieldLabel="Is Transaction ?" 
                        Hidden="false"
                        LabelWidth="200"
                        Checked="true">
                        <DirectEvents>
                            <Change OnEvent="Checkbox_IsTransaction_click">
                                <EventMask ShowMask="true" Msg="Please Wait..."></EventMask>
                            </Change>
                        </DirectEvents>
                    </ext:Checkbox>

                    <ext:DateField runat="server" ID="dateField_SearchFrom" AnchorHorizontal="70%" FieldLabel="Date From" LabelWidth ="200" Format="dd-MMM-yyyy" AllowBlank="false"></ext:DateField>
                    <ext:DateField runat="server" ID="dateField_SearchTo" AnchorHorizontal="70%" FieldLabel="Date To" LabelWidth ="200" Format="dd-MMM-yyyy" AllowBlank="false"></ext:DateField>
                    <ext:Button runat="server" ID="button_SearchTransaction" Text="Search Transaction" Icon="Magnifier">
                        <DirectEvents>
                            <Click OnEvent="Button_SearchTransaction_Click">
                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Checkbox ID="checkbox_IsSelectedAll" runat="server" FieldLabel="Choose All Transactions ? " MarginSpec="20 0 0 0" LabelWidth="200"/>
                </Content>
            </ext:Panel>
            <ext:Panel ID="panel_Transaction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="0 20 0 20" Collapsible="true" Title="New Transaction Data" Border="true">
                <TopBar>
                    <ext:Toolbar runat="server">
                        <Items>
                            <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export " labelStyle="width:80px" AnchorHorizontal="50%" MarginSpec="0 0 0 0" >
                                <Items>
                                    <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                                    <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                                </Items>
                            </ext:ComboBox>
                            <%--<ext:Button runat="server" ID="BtnExport" Text="Export Current Page" MarginSpec="0 0 0 10" >
                                <DirectEvents>
                                    <Click OnEvent="ExportExcel" IsUpload="true">
                                        <ExtraParams>
                                            <ext:Parameter Name="currentPage" Value="App.gp_SearchTransaction.getStore().currentPage" Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>--%>
                            <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page"   MarginSpec="0 0 0 10">
                                <DirectEvents>
                                    <Click OnEvent="ExportAllExcel" IsUpload="true"/>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="BtnExportSelected" Text="Export Selected Data"  MarginSpec="0 0 0 10">
                                <DirectEvents>
                                    <Click OnEvent="ExportSelectedExcel" IsUpload="true"/>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Content>
                    <ext:GridPanel ID="gp_SearchTransaction" runat="server" MarginSpec="0 0 10 0" MinHeight="250">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="store_SearchTransaction" runat="server" IsPagingStore="true" PageSize="10" OnReadData="Store_ReadData_Search_Transaction"
                                RemoteFilter="true" RemoteSort="true" ClientIDMode="Static" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model2" IDProperty="NO_ID">
                                        <Fields>
                                            <ext:ModelField Name="Date_Transaction" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Transaction_Remark" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters></Sorters>
                                <Proxy>
                                    <ext:PageProxy />
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column11" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn2" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                <ext:NumberColumn ID="NumberColumn8" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column90" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                                <ext:Column ID="Column91" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                                <ext:Column ID="Column92" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                                <ext:Column ID="Column93" runat="server" DataIndex="Account_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column94" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column95" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column96" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                                <ext:Column ID="Column114" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader ID="filter_SearchTransaction" runat="server" Remote="true"></ext:FilterHeader>
                        </Plugins>
                        <SelectionModel>
                            <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi">
                            </ext:CheckboxSelectionModel>
                        </SelectionModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Content>
            </ext:Panel>
            <ext:Panel ID="panel_Activity" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="0 20 0 20" Collapsible="true" Title="New Activity Data" Border="true" Hidden="true">
                <Content>
                    <ext:GridPanel ID="GridPanel_Activity" runat="server" AutoScroll="true" MarginSpec="0 0 10 0" MinHeight="250">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_AddActivity" Text="Add Activity" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="Btn_AddActivity_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="500"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <%--<ext:Store ID="StoreActivity" runat="server" IsPagingStore="true" PageSize="10">--%>
                            <ext:Store ID="StoreActivity" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model1" IDProperty="PK_OneFCC_CaseManagement_Typology_NonTransaction_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_OneFCC_CaseManagement_Typology_NonTransaction_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Date_Activity" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Activity_Description" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumnActivity" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandActivity_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_OneFCC_CaseManagement_Typology_NonTransaction_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="ColumnActivityAccountNo" runat="server" DataIndex="Account_NO" Text="Account No" MinWidth="80" Flex="1"></ext:Column>
                                <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="Date_Activity" Text="Activity Date" MinWidth="110" Format="dd-MMM-yyyy" Flex="1">
                                    <Items>
                                        <ext:DateField ID="DatePickerColumnDate_Activity" runat="server" Format="dd-MMM-yyyy">
                                            <Plugins>
                                                <ext:ClearButton ID="Column16_ClearButton1">
                                                </ext:ClearButton>
                                            </Plugins>
                                        </ext:DateField>
                                    </Items>
                                    <Filter>
                                        <ext:DateFilter>
                                        </ext:DateFilter>
                                    </Filter>
                                </ext:DateColumn>
                                <ext:Column ID="Column2" runat="server" DataIndex="Activity_Description" Text="Activity Description" MinWidth="80" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader ID="FilterHeader1" runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Save" runat="server" Icon="Disk" Text="Submit">
                <DirectEvents>
                    <Click OnEvent="Btn_Save_Click">
                        <EventMask ShowMask="true" Msg="Submitting Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="Btn_Cancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="Accept">
                <DirectEvents>
                    <Click OnEvent="Btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    
    <ext:Window ID="WindowConfirmation" Layout="AnchorLayout" Title="Confirmation" runat="server" Modal="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center" Hidden="true">
        <Content>
            <ext:Label ID="WindowConfirmation_Label" runat="server" Align="center" Html="There are some Manually Selected Transaction Data,<br/>but also Checklist all transactions as True.<br/>Would you like to continue selecting all Transaction Data?<br/>(If Not Please Uncheck Select All Transactions)" Anchor="100%"></ext:Label>
        </Content>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.4, height: size.height * 0.4});" />
            <Resize Handler="#{WindowConfirmation}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="WindowConfirmation_Yes" runat="server" Icon="Disk" Text="Yes">
                <DirectEvents>
                    <Click OnEvent="WindowConfirmation_Yes_Click">
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="WindowConfirmation_No" runat="server" Icon="Cancel" Text="No">
                <DirectEvents>
                    <Click OnEvent="WindowConfirmation_No_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    
    <ext:Window ID="window_Import_Customer" runat="server" Modal="true" Hidden="true" BodyStyle="padding:0px" AutoScroll="true" ButtonAlign="Center" MinHeight="430">
        <Items>
            <ext:GridPanel ID="gp_Import_Customer" runat="server" ClientIDMode="Static">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store_Import_Customer" runat="server" IsPagingStore="true" RemoteFilter="true"  RemoteSort="true" 
                        OnReadData="Store_ReadData_Import_Customer" RemotePaging="true" ClientIDMode="Static" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="model_Import_Customer" IDProperty="CIFNo">
                                <Fields>
                                    <ext:ModelField Name="CIFNo_WICNo" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NamaLengkap" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Customer_Type" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="filterHeader_Import_Customer" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumber_Import_Customer" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="col_Import_Customer_CIFNo" runat="server" DataIndex="CIFNo_WICNo" Text="Number" Width="150"></ext:Column>
                        <ext:Column ID="col_Import_Customer_CUSTOMERNAME" runat="server" DataIndex="NamaLengkap" Text="Name" Width="150" ></ext:Column>
                        <ext:Column ID="col_Import_Customer_FK_AML_RISK_CODE" runat="server" DataIndex="Customer_Type" Text="Customer Type" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="cc_Import_Customer" runat="server" Text="Action">

                        <Commands>
                            <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
                                <ToolTip Text="Select"></ToolTip>
                            </ext:GridCommand>
                        </Commands>

                        <DirectEvents>
                            <Command OnEvent="gc_Import_Customer">
                                <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                                <ExtraParams>
                                    <ext:Parameter Name="CIFNo_WICNo" Value="record.data.CIFNo_WICNo" Mode="Raw"></ext:Parameter>
                                    <ext:Parameter Name="NamaLengkap" Value="record.data.NamaLengkap" Mode="Raw"></ext:Parameter>
                                </ExtraParams>
                            </Command>
                        </DirectEvents>
                    </ext:CommandColumn>
                </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_Import_Customer" runat="server" HideRefresh="false" >
                        <Items>  
                            <%--<ext:Button runat="server" Text="Close Picker" ID="Button5">
                                <Listeners>
                                    <Click Handler="#{window_Import_Party}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>--%>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.9});" />
            <Resize Handler="#{window_Import_Party}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_Import_Customer_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="Btn_Import_Customer_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <ext:Window runat="server" ID="WindowPopUpDataCustomer" Maximized="true" Modal="true" Layout="FitLayout" ButtonAlign="Center" Title="Data Customer Profile" Hidden="true">
        <Items>
            <ext:FormPanel ID="FormPanel1" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" AutoScroll="true">
                <Items>
		           <ext:Panel ID="Panel1" runat="server" Layout="ColumnLayout" AnchorHorizontal="100%">
                        <Content>
                            <ext:Container runat="server" ColumnWidth="1">
                                <Items>
                                    <ext:DisplayField ID="txt_CustomerDetail_CIF" runat="server" AnchorHorizontal="100%" FieldLabel="CIF" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Name" runat="server" AnchorHorizontal="100%" FieldLabel="Customer Name" LabelWidth="120" />
                                </Items>
                            </ext:Container>
                            <ext:FieldSet runat="server" ColumnWidth="0.495" Border="true" Padding="10">
                                <Items>
                                    <ext:DisplayField ID="txt_CustomerDetail_TypeCode" runat="server" AnchorHorizontal="100%" FieldLabel="Type Code" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_POB" runat="server" AnchorHorizontal="100%" FieldLabel="Place of Birth" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_DOB" runat="server" AnchorHorizontal="100%" FieldLabel="Date of Birth" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Nationality" runat="server" AnchorHorizontal="100%" FieldLabel="Nationality" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Occupation" runat="server" AnchorHorizontal="100%" FieldLabel="Occupation" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Employer" runat="server" AnchorHorizontal="100%" FieldLabel="Employer" LabelWidth="100" />
                                </Items>
                            </ext:FieldSet>
                            <ext:FieldSet runat="server" ColumnWidth="0.495" Border="true" Padding="10">
                                <Items>
                                    <ext:DisplayField ID="txt_CustomerDetail_LegalForm" runat="server" AnchorHorizontal="100%" FieldLabel="Legal Form" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Industry" runat="server" AnchorHorizontal="100%" FieldLabel="Industry" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_NPWP" runat="server" AnchorHorizontal="100%" FieldLabel="Tax Number" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Income" runat="server" AnchorHorizontal="100%" FieldLabel="Income Level" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_PurposeOfFund" runat="server" AnchorHorizontal="100%" FieldLabel="Purpose of Fund" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_SourceOfFund" runat="server" AnchorHorizontal="100%" FieldLabel="Source of Fund" LabelWidth="120" />
                                </Items>
                            </ext:FieldSet>

                            <%-- Customer Address Information --%>
                            <ext:GridPanel ID="gridAddressInfo" runat="server" Title="Address" AutoScroll="true" Border="true" PaddingSpec="10 0 0 0" ColumnWidth="1">
                                <Store>
                                    <ext:Store ID="Store_gridAddressInfo" runat="server" IsPagingStore="true" PageSize="4">
                                        <Model>
                                            <ext:Model ID="Model11" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="AML_ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CUSTOMER_ADDRESS" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="FK_AML_COUNTRY_CODE" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="KODEPOS" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="KECAMATAN" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="KOTAKABUPATEN" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column57" runat="server" DataIndex="AML_ADDRESS_TYPE_NAME" Text="Type" MinWidth="50"></ext:Column>
                                        <ext:Column ID="Column58" runat="server" DataIndex="CUSTOMER_ADDRESS" Text="Address" MinWidth="250" CellWrap="true" Flex="1"></ext:Column>
                                        <ext:Column ID="Column59" runat="server" DataIndex="KECAMATAN" Text="Town" MinWidth="120" CellWrap="true" Flex="1"></ext:Column>
                                        <ext:Column ID="Column60" runat="server" DataIndex="KOTAKABUPATEN" Text="City" MinWidth="120" CellWrap="true" Flex="1"></ext:Column>
                                        <ext:Column ID="Column61" runat="server" DataIndex="FK_AML_COUNTRY_CODE" Text="Country Code" MinWidth="100" Hidden="true"></ext:Column>
                                        <ext:Column ID="Column62" runat="server" DataIndex="COUNTRY_NAME" Text="Country" MinWidth="100"></ext:Column>
                                        <ext:Column ID="Column63" runat="server" DataIndex="KODEPOS" Text="Postal Code" MinWidth="100"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%-- End of Customer Address Information --%>

                            <%-- Customer Phone Information --%>
                            <ext:GridPanel ID="gridPhoneInfo" runat="server" Title="Phone" AutoScroll="true" Border="true" PaddingSpec="10 0 0 0" ColumnWidth="1">
                                <Store>
                                    <ext:Store ID="Store_PhoneInfo" runat="server" IsPagingStore="true" PageSize="4">
                                        <Model>
                                            <ext:Model ID="Model13" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="AML_CONTACT_TYPE_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="FK_AML_COMMUNICATION_TYPE_CODE" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="AREA_CODE" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CUSTOMER_CONTACT_NUMBER" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="EXTENTION" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="NOTES" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn14" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column69" runat="server" DataIndex="AML_CONTACT_TYPE_NAME" Text="Type" Width="100"></ext:Column>
                                        <ext:Column ID="Column76" runat="server" DataIndex="FK_AML_COMMUNICATION_TYPE_CODE" Text="Comm.Type" Width="100"></ext:Column>
                                        <ext:Column ID="Column70" runat="server" DataIndex="AREA_CODE" Text="Area Code" Width="100"></ext:Column>
                                        <ext:Column ID="Column71" runat="server" DataIndex="CUSTOMER_CONTACT_NUMBER" Text="Number" Width="250"></ext:Column>
                                        <ext:Column ID="Column74" runat="server" DataIndex="EXTENTION" Text="Ext." Width="100"></ext:Column>
                                        <ext:Column ID="Column75" runat="server" DataIndex="NOTES" Text="Notes" MinWidth="100" Flex="1"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%-- End of Customer Phone Information --%>

                            <%-- Customer Identity Information --%>
                            <ext:GridPanel ID="gridIdentityInfo" runat="server" Title="Identification" AutoScroll="true" Border="true" PaddingSpec="10 0 0 0" ColumnWidth="1">
                                <Store>
                                    <ext:Store ID="Store_gridIdentityInfo" runat="server" IsPagingStore="true" PageSize="4">
                                        <Model>
                                            <ext:Model ID="Model12" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="CIFNO" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CUSTOMER_IDENTITY_NUMBER" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="FK_AML_IDENTITY_TYPE_CODE" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="DATE_OF_ISSUE" Type="Date" ></ext:ModelField>
                                                    <ext:ModelField Name="DATE_OF_EXPIRED" Type="Date"></ext:ModelField>
                                                    <ext:ModelField Name="ISSUE_By" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="fk_aml_countryissue_code" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="NOTES" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="IDENTITY_TYPE_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn13" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column64" runat="server" DataIndex="FK_AML_IDENTITY_TYPE_CODE" Text="Type" MinWidth="50"></ext:Column>
                                        <ext:Column ID="Column65" runat="server" DataIndex="CUSTOMER_IDENTITY_NUMBER" Text="Identification Number" MinWidth="150" CellWrap="true" Flex="1"></ext:Column>
                                        <ext:DateColumn ID="DateColumn10" runat="server" DataIndex="DATE_OF_ISSUE" Text="Issue Date" Format="dd-MMM-yyyy" Width="120" CellWrap="true"></ext:DateColumn>
                                        <ext:DateColumn ID="DateColumn11" runat="server" DataIndex="DATE_OF_EXPIRED" Text="Expired Date" Format="dd-MMM-yyyy" Width="120" CellWrap="true"></ext:DateColumn>
                                        <ext:Column ID="Column66" runat="server" DataIndex="ISSUE_By" Text="Issue By" MinWidth="100" Hidden="true"></ext:Column>
                                        <ext:Column ID="Column67" runat="server" DataIndex="COUNTRY_NAME" Text="Issued Country" Width="150"></ext:Column>
                                        <ext:Column ID="Column68" runat="server" DataIndex="NOTES" Text="Comments" MinWidth="100"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar13" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%-- End of Customer Identity Information --%>
                        </Content>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_CustomerDetail_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="Btn_CustomerDetail_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    
    <ext:Window ID="WindowActivity" runat="server" Modal="true" Hidden="true" BodyStyle="padding:0px" AutoScroll="true" ButtonAlign="Center" Layout="AnchorLayout" Title="Activity">
        <Items>
            <ext:FormPanel runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                <Items>
                    <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="100%" Padding="10">
                        <Content>
                            <NDS:NDSDropDownField runat="server" ID="nDSDropDownField_AccountNo" ValueField="Account_No" DisplayField="Account_No" StringField="Account_No, Account_Name, Branch, balance, date_balance" StringTable="goAML_Ref_Account" Label="Account No" AnchorHorizontal="100%" AllowBlank="false" LabelWidth ="200" IsHidden="true"/>
                            <ext:DateField runat="server" ID="activity_Date" AnchorHorizontal="100%" FieldLabel="Date Activity" LabelWidth ="200" Format="dd-MMM-yyyy" AllowBlank="false"></ext:DateField>
                            <ext:TextArea runat="server" ID="activity_Desc" FieldLabel="Activity Description" LabelWidth ="200" AnchorHorizontal="100%" EnforceMaxLength="True" MaxLength="500" />
                        </Content>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{window_Import_Party}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_Activity_Submit" runat="server" Icon="Disk" Text="Submit">
                <DirectEvents>
                    <Click OnEvent="Btn_Activity_Submit_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Activity_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="Btn_Activity_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
</asp:Content>

