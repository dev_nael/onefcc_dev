﻿Imports Ext
Imports Elmah
Imports System.Data
Imports CasemanagementBLL
Imports CasemanagementDAL
Imports System.Data.SqlClient
Imports OfficeOpenXml

'******************************************************************************
'Major changes 20-Apr-2022 Adi :
'Alert grouped by CIF_No and ProcessDate
'Before : Alert grouped by CIF_No, ProcessDate, Alert_Type and FK_Rule_Basic_ID
'******************************************************************************

Partial Class CaseManagementAlertEdit
    Inherits ParentPage

    Const PATH_TEMP_DIR As String = "~\temp\"

    Public Property IDModule() As String
        Get
            Return Session("CaseManagementAlertEdit.IDModule")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertEdit.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("CaseManagementAlertEdit.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertEdit.IDUnik") = value
        End Set
    End Property

    Public Property CIFNo() As String
        Get
            Return Session("CaseManagementAlertEdit.CIFNo")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertEdit.CIFNo") = value
        End Set
    End Property

    Public Property Workflow_Step() As Integer
        Get
            Return Session("CaseManagementAlertEdit.Workflow_Step")
        End Get
        Set(ByVal value As Integer)
            Session("CaseManagementAlertEdit.Workflow_Step") = value
        End Set
    End Property

    Public Property Workflow_Step_Total() As Integer
        Get
            Return Session("CaseManagementAlertEdit.Workflow_Step_Total")
        End Get
        Set(ByVal value As Integer)
            Session("CaseManagementAlertEdit.Workflow_Step_Total") = value
        End Set
    End Property

    Public Property Workflow_ID() As Integer
        Get
            Return Session("CaseManagementAlertEdit.Workflow_ID")
        End Get
        Set(ByVal value As Integer)
            Session("CaseManagementAlertEdit.Workflow_ID") = value
        End Set
    End Property

    Public Property CaseID_OtherCase() As Long
        Get
            If Session("CaseManagementAlertEdit.CaseID_OtherCase") Is Nothing Then
                Session("CaseManagementAlertEdit.CaseID_OtherCase") = 0
            End If
            Return Session("CaseManagementAlertEdit.CaseID_OtherCase")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertEdit.CaseID_OtherCase") = value
        End Set
    End Property

    Public Property RFI_ID() As Long
        Get
            Return Session("CaseManagementAlertEdit.RFI_ID")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertEdit.RFI_ID") = value
        End Set
    End Property

    Private Sub CaseManagementAlertEdit_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Update
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As New Integer
            If IDModule IsNot Nothing Then
                intModuleID = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = ObjModule.ModuleLabel & " - Edit"

                'Load Data
                LoadDataCaseManagement()

                'Hide unused objects
                txt_LastModifiedDate.Hidden = True
                txt_Account_No.Hidden = True
                txt_Proposed_Action.Hidden = True
                txt_Proposed_By.Hidden = True

                Dim intmaxfilesize As Double = NawaBLL.SystemParameterBLL.GetMaxFileSize
                Dim strmaxfilesize As String = (intmaxfilesize / 1048576) & " MB"
                'txt_EmailAttachment.MaxLength = intmaxfilesize
                txt_EmailAttachment.EmptyText = "Select a file with max size " & strmaxfilesize

                SetCommandColumnLocation()

                '20-Apr-2022 Adi
                txt_Alert_Type.Hidden = True
                txt_Case_Description.Hidden = True
                txt_Account_No.Hidden = True
                txt_WindowOther_Alert_Type.Hidden = True
                txt_WindowOther_Case_Description.Hidden = True

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub LoadDataCaseManagement()
        Try
            Dim strSQL As String = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            If drCM IsNot Nothing Then
                Me.CIFNo = drCM("CIF_No")
                Me.Workflow_Step = drCM("Workflow_Step")
                Me.Workflow_Step_Total = drCM("Workflow_Step_Total")
                Me.Workflow_ID = drCM("FK_CaseManagement_Workflow_ID")

                'General Information
                txt_PK_CaseManagement_ID.Value = drCM("PK_CaseManagement_ID")
                ' 8 Maret 2023 Ari : update untuk set Unique CM ID
                If Not IsDBNull(drCM("Unique_CM_ID")) Then
                    txt_Unique_CM_ID.Value = drCM("Unique_CM_ID")
                End If
                txt_Case_Description.Value = drCM("Case_Description")
                txt_Alert_Type.Value = drCM("Alert_Type")

                If Not IsDBNull(drCM("FK_CaseStatus_ID")) Then
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_ProposedAction WHERE PK_OneFCC_CaseManagement_ProposedAction_ID=" & drCM("FK_CaseStatus_ID")
                    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drTemp IsNot Nothing Then
                        txt_FK_Case_Status_ID.Value = drTemp("PK_OneFCC_CaseManagement_ProposedAction_ID") & " - " & drTemp("Proposed_Action")
                    Else
                        txt_FK_Case_Status_ID.Value = "New Case"
                    End If
                Else
                    txt_FK_Case_Status_ID.Value = "New Case"
                End If

                If Not IsDBNull(drCM("CreatedDate")) Then
                    txt_CreatedDate.Value = CDate(drCM("CreatedDate")).ToString("dd-MMM-yyyy HH:mm:ss")
                End If
                If Not IsDBNull(drCM("LastUpdateDate")) Then
                    txt_LastModifiedDate.Value = CDate(drCM("LastUpdateDate")).ToString("dd-MMM-yyyy HH:mm:ss")
                End If
                If Not IsDBNull(drCM("FK_Proposed_Status_ID")) Then
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_ProposedAction WHERE PK_OneFCC_CaseManagement_ProposedAction_ID=" & drCM("FK_Proposed_Status_ID")
                    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drTemp IsNot Nothing Then
                        txt_FK_Case_Status_ID.Value = drCM("FK_Proposed_Status_ID") & " - " & drTemp("PROPOSED_ACTION")
                    End If
                End If

                '20-Apr-2022 Adi : Display Process Date
                If Not IsDBNull(drCM("ProcessDate")) Then
                    txt_ProcessDate.Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                End If

                txt_Proposed_By.Value = drCM("Proposed_By")
                txt_Workflow_Step.Value = drCM("Workflow_Step_Of")
                txt_PIC.Value = drCM("PIC")

                '20-Jun-2022 Other Information
                '23-Aug-2022 Adi : Commented untuk BSIM
                'txt_Branch.Value = drCM("BRANCH_NAME")
                'txt_BusinessUnit.Value = drCM("BUSINESSUNIT_NAME")
                'txt_PredictStatus.Value = drCM("PredictStatus")
                'txt_ProbabilitySTR.Value = drCM("Probability4")
                'txt_ProbabilityNotSTR.Value = drCM("Probability3")
                'End of 20-Jun-2022 Other Information

                'Customer Information
                txt_CIF_No.Value = drCM("CIF_No")
                txt_Account_No.Value = drCM("Account_No")
                txt_Customer_Name.Value = drCM("Customer_Name")

                '20-Apr-2022 Adi
                FormPanelInput.Title = ObjModule.ModuleLabel & " - Edit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ " & drCM("CIF_No") & " - " & drCM("Customer_Name") & " ]"

                If Not IsDBNull(drCM("CIF_No")) Then
                    strSQL = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & drCM("CIF_No") & "'"
                    Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drCustomer IsNot Nothing Then
                        If Not IsDBNull(drCustomer("DATEOFBIRTH")) Then
                            txt_DOB.Value = CDate(drCustomer("DATEOFBIRTH")).ToString("dd-MMM-yyyy")
                        End If

                        txt_POB.Value = drCustomer("PLACEOFBIRTH")

                        If Not IsDBNull(drCustomer("FK_AML_JenisKelamin_Code")) Then
                            strSQL = "SELECT TOP 1 * FROM goAML_Ref_Jenis_Kelamin WHERE Kode='" & drCustomer("FK_AML_JenisKelamin_Code") & "'"
                            Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                            If drGender IsNot Nothing Then
                                txt_Gender.Value = drGender("Kode") & " - " & drGender("Keterangan")
                            Else
                                txt_Gender.Value = drCustomer("FK_AML_JenisKelamin_Code")
                            End If
                        End If

                        If Not IsDBNull(drCustomer("FK_AML_Creation_Branch_Code")) Then
                            strSQL = "SELECT TOP 1 * FROM AML_BRANCH WHERE FK_AML_BRANCH_CODE='" & drCustomer("FK_AML_Creation_Branch_Code") & "'"
                            Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                            If drGender IsNot Nothing Then
                                txt_CIF_Opening_Branch.Value = drGender("FK_AML_BRANCH_CODE") & " - " & drGender("BRANCH_NAME")
                            Else
                                txt_CIF_Opening_Branch.Value = drCustomer("FK_AML_Creation_Branch_Code")
                            End If
                        End If

                        If Not IsDBNull(drCustomer("OpeningDate")) Then
                            txt_CIF_Opening_Date.Value = CDate(drCustomer("OpeningDate")).ToString("dd-MMM-yyyy")
                        End If

                        If Not IsDBNull(drCustomer("IS_PEP")) Then
                            If drCustomer("IS_PEP") = 0 Then
                                txt_Is_PEP.Value = "No"
                            Else
                                txt_Is_PEP.Value = "Yes"
                            End If
                        Else
                            txt_Is_PEP.Value = "N/A"
                        End If

                        If Not IsDBNull(drCustomer("FK_AML_RISK_CODE")) Then
                            strSQL = "SELECT TOP 1 * FROM AML_RISK_RATING WHERE RISK_RATING_CODE ='" & drCustomer("FK_AML_RISK_CODE") & "'"
                            Dim drRisk As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                            If drRisk IsNot Nothing Then
                                txt_Risk_Code.Value = drRisk("RISK_RATING_CODE") & " - " & drRisk("RISK_RATING_NAME")
                            Else
                                txt_Risk_Code.Value = drCustomer("FK_AML_RISK_CODE")
                            End If

                            If drCustomer("FK_AML_RISK_CODE") = "H" Then
                                txt_Risk_Code.FieldStyle = "Color:Red;font-weight:Bold;"
                            Else
                                txt_Risk_Code.FieldStyle = "Color:Black;font-weight:Normal;"
                            End If
                        End If

                    End If
                End If

                'List of Accounts
                LoadDataAccounts(drCM("CIF_No"))

                '29-Mar-2022 Adi : Load data Loan
                LoadDataLoans(drCM("CIF_No"))

                'Bind RFI sent
                BindDataRFI()


                'Filter untuk level 1 hanya bisa Submitted as an issue or Submitted as non-issue
                If drCM("Workflow_Step") = 1 Then
                    cmb_FK_Proposed_Status_ID.StringFilter = "PK_OneFCC_CaseManagement_ProposedAction_ID<=2"
                Else
                    cmb_FK_Proposed_Status_ID.StringFilter = ""
                End If

                'Switch between Typology/Outlier Transaction based on Alert_Type
                gp_CaseAlert_Typology_Transaction.Hidden = True
                pnl_CaseAlert_Outlier_Transaction.Hidden = True

                If Not IsDBNull(drCM("Alert_Type")) Then
                    Select Case drCM("Alert_Type")
                        Case "Typology Risk"
                            gp_CaseAlert_Typology_Transaction.Hidden = False
                        Case "Financial Risk"
                            pnl_CaseAlert_Outlier_Transaction.Hidden = False

                            'Display Mean dan Modus
                            strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=2"
                            Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                            If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                                fs_CaseAlert_Outlier_Transaction.Title = "<b>Financial Statistics in past " & drParam("ParameterValue") & " month(s)</b>"
                            End If

                            strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Outlier WHERE FK_CaseManagement_ID=" & IDUnik
                            Dim drCMO As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                            If drCMO IsNot Nothing Then
                                txt_MeanDebit.Value = CDbl(drCMO("Mean_Debit")).ToString("#,##0.00")
                                txt_MeanCredit.Value = CDbl(drCMO("Mean_Credit")).ToString("#,##0.00")

                                txt_ModusDebit.Value = CDbl(drCMO("Modus_Debit")).ToString("#,##0.00")
                                txt_ModusCredit.Value = CDbl(drCMO("Modus_Credit")).ToString("#,##0.00")
                            End If
                    End Select
                End If


                'Hide button Save jika status <> 0 (New Case)
                'Hide button Save jika user yg login bukan PIC
                Dim isAllowedSave As Boolean = True

                '0=New, 5=Need Correction, 8=Re-Open
                If Not IsDBNull(drCM("FK_CaseStatus_ID")) AndAlso Not (drCM("FK_CaseStatus_ID") = 0 Or drCM("FK_CaseStatus_ID") = 5 Or drCM("FK_CaseStatus_ID") = 8) Then
                    isAllowedSave = False
                End If
                If Not CheckIfStringExists(NawaBLL.Common.SessionCurrentUser.UserID, drCM("PIC")) Then
                    isAllowedSave = False
                End If

                '2-Mar-2022 Adi : Jika belum di assign belum bisa save
                btn_AssignToMe.Hidden = True
                btn_Save.Hidden = True
                btn_ChangeAssignment.Hidden = True
                pnl_AnalysisResult.Hidden = True
                tb_RFI.Hidden = True

                If isAllowedSave Then
                    'Jika PIC > 1 user ID maka munculkan button assign to me
                    If drCM("PIC").ToString.Contains(",") Then
                        btn_AssignToMe.Hidden = False
                    Else
                        btn_ChangeAssignment.Hidden = False
                        btn_Save.Hidden = False

                        pnl_AnalysisResult.Hidden = False
                        tb_RFI.Hidden = False
                    End If
                End If

                'Hide button send email RFI
                If Not IsDBNull(drCM("FK_CaseStatus_ID")) AndAlso (drCM("FK_CaseStatus_ID") = 3 Or drCM("FK_CaseStatus_ID") = 4) Then
                    tb_RFI.Hidden = True
                End If

                '22-Feb-2022 Adi : Last workflow step tidak bisa send RFI
                If Me.Workflow_Step = Me.Workflow_Step_Total Then
                    tb_RFI.Hidden = True
                End If


                '23-Aug-2022 Adi : Hide/Show Gridpanel Transaction and Activity
                strSQL = "SELECT COUNT(1) FROM vw_OneFCC_CaseManagement_Alert_Transaction WHERE FK_CaseManagement_ID=" & IDUnik
                Dim intTotalTransaction As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL, Nothing)
                If intTotalTransaction = 0 Then
                    gp_CaseAlert_Transaction.Hidden = True
                Else
                    gp_CaseAlert_Transaction.Hidden = False
                End If

                strSQL = "SELECT COUNT(1) FROM vw_OneFCC_CaseManagement_Alert_Activity WHERE FK_CaseManagement_ID=" & IDUnik
                Dim intTotalActivity As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL, Nothing)
                If intTotalActivity = 0 Then
                    gp_CaseAlert_Activity.Hidden = True
                Else
                    gp_CaseAlert_Activity.Hidden = False
                End If
                'End of 23-Aug-2022 Adi : Hide/Show Gridpanel Transaction and Activity


            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDataAccounts(strCIFNo As String)
        Try
            Dim strQuery As String = "SELECT acc.*, card.FK_AML_CARD_NO, card.LIMITCREDITCARD"
            strQuery &= " FROM AML_ACCOUNT acc"
            strQuery &= " LEFT JOIN AML_CARD card ON acc.ACCOUNT_NO = card.ACCOUNT_NO"
            strQuery &= " WHERE acc.CIFNO='" & strCIFNo & "'"

            Dim dtAccount As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If dtAccount Is Nothing Then
                dtAccount = New DataTable
            End If

            If dtAccount IsNot Nothing Then
                'Running the following script for additional columns
                dtAccount.Columns.Add(New DataColumn("BRANCH_NAME", GetType(String)))
                dtAccount.Columns.Add(New DataColumn("ACCOUNT_TYPE", GetType(String)))
                dtAccount.Columns.Add(New DataColumn("PRODUCT_NAME", GetType(String)))

                'Get reference Table
                strQuery = "SELECT * FROM AML_BRANCH"
                Dim dtBranch As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                strQuery = "SELECT * FROM AML_ACCOUNT_TYPE"
                Dim dtAccountType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                strQuery = "SELECT * FROM AML_PRODUCT"
                Dim dtProduct As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                For Each row As DataRow In dtAccount.Rows
                    If dtBranch IsNot Nothing AndAlso Not IsDBNull(row("FK_AML_BRANCH_CODE")) Then
                        Dim drCek = dtBranch.Select("FK_AML_BRANCH_CODE='" & row("FK_AML_BRANCH_CODE") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("BRANCH_NAME") = drCek("BRANCH_NAME")
                        End If
                    End If
                    If dtAccountType IsNot Nothing AndAlso Not IsDBNull(row("FK_AML_ACCOUNT_TYPE_CODE")) Then
                        Dim drCek = dtAccountType.Select("FK_AML_ACCOUNT_TYPE_CODE='" & row("FK_AML_ACCOUNT_TYPE_CODE") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("ACCOUNT_TYPE") = drCek("ACCOUNT_TYPE_NAME")
                        End If
                    End If
                    If dtProduct IsNot Nothing AndAlso Not IsDBNull(row("FK_AML_PRODUCT_CODE")) Then
                        Dim drCek = dtProduct.Select("FK_AML_PRODUCT_CODE='" & row("FK_AML_PRODUCT_CODE") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("PRODUCT_NAME") = drCek("PRODUCT_NAME")
                        End If
                    End If

                Next

                'Bind to gridpanel
                gp_Account.GetStore.DataSource = dtAccount
                gp_Account.GetStore.DataBind()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    '29-Mar-2022 Adi
    Protected Sub LoadDataLoans(strCIFNo As String)
        Try
            Dim strQuery As String = "SELECT al.*, ac.COLLATERAL_TYPE"
            strQuery &= " FROM AML_LOAN al"
            strQuery &= " LEFT JOIN AML_COLLATERAL_LOAN acl ON acl.LOAN_NUMBER = al.LOAN_NUMBER"
            strQuery &= " LEFT JOIN AML_COLLATERAL ac ON ac.COLLATERAL_NUMBER = acl.COLLATERAL_NUMBER"
            strQuery &= " WHERE al.CIF='" & strCIFNo & "'"

            Dim dtLoan As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If dtLoan Is Nothing Then
                dtLoan = New DataTable
            End If

            If dtLoan IsNot Nothing Then
                'Running the following script for additional columns
                dtLoan.Columns.Add(New DataColumn("BRANCH_NAME", GetType(String)))

                'Get reference Table
                strQuery = "SELECT * FROM AML_BRANCH"
                Dim dtBranch As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                For Each row As DataRow In dtLoan.Rows
                    If dtBranch IsNot Nothing AndAlso Not IsDBNull(row("BRANCH_CODE")) Then
                        Dim drCek = dtBranch.Select("FK_AML_BRANCH_CODE='" & row("BRANCH_CODE") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("BRANCH_NAME") = drCek("BRANCH_NAME")
                        End If
                    End If
                Next

                'Bind to gridpanel
                gp_Loan.GetStore.DataSource = dtLoan
                gp_Loan.GetStore.DataBind()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Typology_Transaction(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            'If strsort = "" Then
            '    strsort = "CIF asc, Account_Name asc"
            'End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("Vw_Account", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)

            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Typology_Transaction.GetStore.DataSource = DataPaging
            gp_CaseAlert_Typology_Transaction.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Outlier_Transaction(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            'If strsort = "" Then
            '    strsort = "CIF asc, Account_Name asc"
            'End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("Vw_Account", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)


            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Outlier AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Outlier_Transaction.GetStore.DataSource = DataPaging
            gp_CaseAlert_Outlier_Transaction.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_Search_Transaction(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            'Dim strQuery As String = "SELECT * FROM goAML_ODM_Transaksi"
            'strQuery += " WHERE CIF_NO = '" & Me.CIFNo & "'"

            '29-Mar-2022 Adi
            'Dim strQuery As String = "SELECT gaot.*"
            'strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            'strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            'strQuery &= " FROM goAML_ODM_Transaksi AS gaot"
            'strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            'strQuery &= " ON gaot.CIF_No_Lawan = garc.CIF"
            'strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            'strQuery &= " ON gaot.WIC_No_Lawan = garw.WIC_No"
            'strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            'strQuery &= " ON gaot.country_code_lawan = ctry.Kode"
            'strQuery &= " WHERE CIF_NO = '" & Me.CIFNo & "'"

            '23-Aug-2022 Adi : Coba tuning jangan Select *
            Dim strQuery As String = "SELECT gaot.Date_Transaction"
            strQuery &= " ,gaot.Ref_Num"
            strQuery &= " ,gaot.Transmode_Code"
            strQuery &= " ,gaot.Transaction_Location"
            strQuery &= " ,gaot.Debit_Credit"
            strQuery &= " ,gaot.Currency"
            strQuery &= " ,gaot.Original_Amount"
            strQuery &= " ,gaot.IDR_Amount"
            strQuery &= " ,gaot.Transaction_Remark"
            strQuery &= " ,gaot.Account_NO"
            strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,gaot.Account_No_Lawan"
            strQuery &= " ,gaot.CIF_No_Lawan"
            strQuery &= " ,gaot.WIC_No_Lawan"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM goAML_ODM_Transaksi AS gaot"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON gaot.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON gaot.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON gaot.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE gaot.CIF_NO = '" & Me.CIFNo & "'"


            Dim DataPaging As Data.DataTable
            If Not (txt_SearchTransaction_From.SelectedDate = DateTime.MinValue And txt_SearchTransaction_To.SelectedDate = DateTime.MinValue) Then
                strQuery += " AND Date_Transaction >= '" & CDate(txt_SearchTransaction_From.SelectedDate).ToString("yyyy-MM-dd") & "'"
                strQuery += " AND Date_Transaction <= '" & CDate(txt_SearchTransaction_To.SelectedDate).ToString("yyyy-MM-dd") & "'"

                If Not String.IsNullOrEmpty(strfilter) Then
                    strQuery += " AND " & strfilter
                End If

                DataPaging = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)
            Else
                DataPaging = New DataTable
            End If

            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_SearchTransaction.GetStore.DataSource = DataPaging
            gp_SearchTransaction.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_WorkflowHistory(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort &= item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT history.*, proposed.Proposed_Action FROM onefcc_casemanagement_workflowhistory history"
            strQuery &= " LEFT JOIN OneFCC_CaseManagement_ProposedAction proposed ON history.FK_Proposed_Status_ID = proposed.PK_OneFCC_CaseManagement_ProposedAction_ID"
            strQuery &= " WHERE FK_CaseManagement_ID = " & Me.IDUnik

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)

            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_WorkflowHistory.GetStore.DataSource = DataPaging
            gp_WorkflowHistory.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btn_Search_Transaction_Click(sender As Object, e As DirectEventArgs)
        Try
            If txt_SearchTransaction_From.SelectedDate = DateTime.MinValue Or txt_SearchTransaction_To.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException("Please fill Date From and Date To for searching transaction!")
            End If

            store_SearchTransaction.Reload()

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Back_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Save_Click()
        Try
            If String.IsNullOrEmpty(cmb_FK_Proposed_Status_ID.StringValue) Then
                tabpanel_Alert.SetActiveTab(0)
                cmb_FK_Proposed_Status_ID.Focus()
                Throw New ApplicationException("Proposed Status is required.")
            End If
            If String.IsNullOrWhiteSpace(txt_Analysis_Result.Value) Then
                tabpanel_Alert.SetActiveTab(0)
                txt_Analysis_Result.Focus()
                Throw New ApplicationException("Analysis Result is required.")
            End If

            'Get PIC from workflow Detail
            Dim strPIC As String = ""
            Dim strQuery As String = "SELECT * FROM OneFCC_CaseManagement_WorkflowDetail"
            strQuery &= " WHERE Workflow_Step=" & (Me.Workflow_Step + 1)
            strQuery &= " AND FK_CaseManagement_Workflow_ID=" & Me.Workflow_ID
            Dim drPIC = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            If drPIC IsNot Nothing AndAlso Not IsDBNull(drPIC("PIC")) Then
                strPIC = drPIC("PIC")
            End If

            'Update Case Management Status and Proposed Status
            strQuery = "UPDATE OneFCC_CaseManagement SET FK_Proposed_Status_ID=" & cmb_FK_Proposed_Status_ID.SelectedItemValue
            strQuery &= ", FK_CaseStatus_ID=" & cmb_FK_Proposed_Status_ID.SelectedItemValue
            strQuery &= ", Workflow_Step=" & (Me.Workflow_Step + 1)
            strQuery &= ", PIC='" & strPIC & "'"
            strQuery &= ", LastUpdateBy='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery &= ", LastUpdateDate=GETDATE()"
            strQuery &= " WHERE PK_CaseManagement_ID=" & Me.IDUnik
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Insert into Workflow History
            strQuery = "INSERT INTO OneFCC_CaseManagement_WorkflowHistory ("
            strQuery &= "FK_CaseManagement_ID,"
            strQuery &= "Workflow_Step,"
            strQuery &= "FK_Proposed_Status_ID,"
            strQuery &= "Analysis_Result,"
            strQuery &= "CreatedBy,"
            strQuery &= "CreatedDate) "
            strQuery &= "VALUES ("
            strQuery &= Me.IDUnik.ToString & ","
            strQuery &= Me.Workflow_Step & ","
            strQuery &= cmb_FK_Proposed_Status_ID.SelectedItemValue.ToString & ","
            strQuery &= "'" & txt_Analysis_Result.Value & "',"
            strQuery &= "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
            strQuery &= "GETDATE())"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


            'Show Confirmation
            LblConfirmation.Text = "Case Management Follow Up has been submitted for approval."
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Function CheckIfStringExists(strPart As String, strFull As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim dt As DataTable = New DataTable

            Dim strsplit As String() = strFull.Split(","c)
            For Each item As String In strsplit
                If strPart = item Then
                    Return True
                End If
            Next

            Return bolResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#Region "9-Feb-2022 : Send Email RFI (Request For Information)"
    Protected Sub btn_Send_New_RFI_Click()
        Try
            CleanWindowSendRFI()
            btn_SendRFI_Save.Hidden = False

            '04-Mar-2022 Adi : Jadikan semua detail ReadOnly
            Me.RFI_ID = 0
            txt_EmailAttachment.Hidden = False
            ctr_RFI_Attachment.Hidden = True

            '29-Mar-2022 Adi : Penambahan Notes RFI
            txt_EmailNotes.Hidden = True
            btn_SendRFI_SaveNotes.Hidden = True

            Window_SendRFI.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_RFI(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            If strCommandName = "Detail" Then
                LoadDataRFI(ID)
            ElseIf strCommandName = "Download" Then
                DownloadRFI(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DownloadRFI(id As Long)
        Using objdb As New CasemanagementEntities
            Dim objRFI = objdb.OneFCC_CaseManagement_RFI.Where(Function(x) x.PK_OneFCC_CaseManagement_RFI_ID = id).FirstOrDefault
            If Not objRFI Is Nothing Then
                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & objRFI.AttachmentName)
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(objRFI.Attachment)
                Response.End()
            End If
        End Using
    End Sub

    Protected Sub btn_SendRFI_Save_Click()
        Try
            If String.IsNullOrWhiteSpace(txt_EmailTo.Value) Then
                Throw New ApplicationException(txt_EmailTo.FieldLabel & " is required.")
            End If
            If String.IsNullOrWhiteSpace(txt_EmailSubject.Value) Then
                Throw New ApplicationException(txt_EmailSubject.FieldLabel & " is required.")
            End If
            If String.IsNullOrWhiteSpace(txt_EmailContent.Value) Then
                Throw New ApplicationException(txt_EmailContent.FieldLabel & " is required.")
            End If

            'Validate email address
            If Not String.IsNullOrWhiteSpace(txt_EmailTo.Value) Then
                Dim strValidateEmail As String = ValidateEmailList(txt_EmailTo.Value)
                If Not String.IsNullOrEmpty(strValidateEmail) Then
                    Throw New ApplicationException(strValidateEmail)
                End If
            End If

            'Validate file upload max size
            If txt_EmailAttachment.HasFile Then
                Dim intmaxfilesize As Double = NawaBLL.SystemParameterBLL.GetMaxFileSize
                Dim strmaxfilesize As String = (intmaxfilesize / 1048576) & " MB"

                Dim intFileSizeUpload As Double = txt_EmailAttachment.FileBytes.Length
                Dim strFileUploadsize As String = (intFileSizeUpload / 1048576).ToString("#,##0.00") & " MB"

                If intFileSizeUpload > intmaxfilesize Then
                    Throw New ApplicationException("The attachment " & strFileUploadsize & " exceeds the max file size " & strmaxfilesize)
                End If
            End If

            'Populate Data
            Dim objRFI As New OneFCC_CaseManagement_RFI
            With objRFI
                .FK_CaseManagement_ID = IDUnik
                .EmailTo = txt_EmailTo.Value
                .EmailCc = txt_EmailCC.Value
                .EmailBcc = txt_EmailBCC.Value
                .Subject = txt_EmailSubject.Value

                Dim Val = txt_EmailContent.RawValue.ToString.Replace("%3E%u200B%3C", "%3E%3C").Replace("+", "%2B")
                If String.IsNullOrEmpty(Val) Then
                    .Content = txt_EmailContent.Value
                Else
                    .Content = Server.UrlDecode(Val)
                End If

                If txt_EmailAttachment.HasFile Then
                    .AttachmentName = txt_EmailAttachment.FileName
                    .Attachment = txt_EmailAttachment.FileBytes
                End If

                .FK_EmailStatus_ID = 1  'Preparation

                .Active = 1
                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                .CreatedDate = DateTime.Now
            End With

            'Insert to DB
            Using objDb As New CasemanagementEntities
                Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                    Try
                        'Save objData
                        objDb.Entry(objRFI).State = Entity.EntityState.Added
                        objDb.SaveChanges()

                        'AuditTrail
                        Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, "OneFCC_CaseManagement_RFI")

                        'AuditTrailDetail
                        NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, objRFI)

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                    End Try
                End Using
            End Using

            'Execute Send Email
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_OneFCC_CaseManagement_RFI_ID"
            param(0).Value = objRFI.PK_OneFCC_CaseManagement_RFI_ID
            param(0).DbType = SqlDbType.BigInt

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_OneFCC_TriggerEmailRFI", param)

            'Bind to GridPanel
            BindDataRFI()

            'Hide window
            Window_SendRFI.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SendRFI_Back_Click()
        Try
            Window_SendRFI.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub CleanWindowSendRFI()
        Try
            txt_EmailTo.Value = Nothing
            txt_EmailCC.Value = Nothing
            txt_EmailBCC.Value = Nothing
            txt_EmailSubject.Value = Nothing
            txt_EmailContent.Value = Nothing
            txt_EmailAttachment.Value = Nothing

            txt_EmailAttachment.Reset()

            'Fields Readonly
            txt_EmailTo.ReadOnly = False
            txt_EmailCC.ReadOnly = False
            txt_EmailBCC.ReadOnly = False
            txt_EmailSubject.ReadOnly = False
            txt_EmailContent.ReadOnly = False
            txt_EmailAttachment.ReadOnly = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindDataRFI()
        Try
            Dim strQuery As String = "SELECT * FROM OneFCC_CaseManagement_RFI WHERE FK_CaseManagement_ID=" & Me.IDUnik
            Dim dtRFI As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If dtRFI Is Nothing Then
                dtRFI = New DataTable
            End If

            If dtRFI IsNot Nothing Then
                'Running the following script for additional columns
                dtRFI.Columns.Add(New DataColumn("EmailStatusName", GetType(String)))

                'Get reference Table
                strQuery = "SELECT * FROM EmailStatus"
                Dim dtEmailStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                For Each row As DataRow In dtRFI.Rows
                    If dtEmailStatus IsNot Nothing AndAlso Not IsDBNull(row("FK_EmailStatus_ID")) Then
                        Dim drCek = dtEmailStatus.Select("PK_EmailStatus_ID='" & row("FK_EmailStatus_ID") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("EmailStatusName") = drCek("EmailStatusName")
                        End If
                    End If
                Next

                'Bind to gridpanel
                gp_RFI.GetStore.DataSource = dtRFI
                gp_RFI.GetStore.DataBind()

            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub LoadDataRFI(strRFI_ID As String)
        Try
            Using objdb As New CasemanagementEntities
                With objdb
                    Dim objRFI = objdb.OneFCC_CaseManagement_RFI.Where(Function(x) x.PK_OneFCC_CaseManagement_RFI_ID = strRFI_ID).FirstOrDefault
                    If objRFI IsNot Nothing Then
                        txt_EmailTo.Value = objRFI.EmailTo
                        txt_EmailCC.Value = objRFI.EmailCc
                        txt_EmailBCC.Value = objRFI.EmailBcc
                        txt_EmailSubject.Value = objRFI.Subject
                        txt_EmailContent.Value = objRFI.Content
                        txt_EmailAttachment.Value = objRFI.AttachmentName
                        df_EmailAttachment.Value = objRFI.AttachmentName

                        '29-Mar-2022 Adi : Penambahan Notes RFI
                        Dim strQuery As String = "SELECT * FROM OneFCC_CaseManagement_RFI WHERE PK_OneFCC_CaseManagement_RFI_ID=" & strRFI_ID
                        Dim drRFI As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        If drRFI IsNot Nothing Then
                            txt_EmailNotes.Value = drRFI("Notes")
                        End If

                        '29-Mar-2022 Adi : Penambahan Notes RFI. Hanya user yang kirim RFI yg boleh tambahin Notes
                        txt_EmailNotes.Hidden = False
                        If objRFI.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID Then
                            txt_EmailNotes.ReadOnly = False
                            btn_SendRFI_SaveNotes.Hidden = False
                        Else
                            txt_EmailNotes.ReadOnly = True
                            btn_SendRFI_SaveNotes.Hidden = True
                        End If

                    End If
                End With
            End Using

            'Fields Readonly
            txt_EmailTo.ReadOnly = True
            txt_EmailCC.ReadOnly = True
            txt_EmailBCC.ReadOnly = True
            txt_EmailSubject.ReadOnly = True
            txt_EmailContent.ReadOnly = True
            txt_EmailAttachment.ReadOnly = True

            '04-Mar-2022 Adi : Jadikan semua detail ReadOnly
            Me.RFI_ID = strRFI_ID
            txt_EmailAttachment.Hidden = True
            ctr_RFI_Attachment.Hidden = False

            If String.IsNullOrEmpty(df_EmailAttachment.Value) Then
                btn_DownloadEmailAttachment.Hidden = True
            Else
                btn_DownloadEmailAttachment.Hidden = False
            End If

            btn_SendRFI_Save.Hidden = True
            Window_SendRFI.Hidden = False

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ValidateEmailList(emailAddress As String) As String
        Dim strEmail As String() = emailAddress.Split(";")
        For Each item In strEmail
            If Not ValidateEmail(item) Then
                Return "Email address '" & item & "' is not valid!"
            End If
        Next

        Return ""
    End Function

    Function ValidateEmail(emailAddress As String) As Boolean
        If String.IsNullOrEmpty(emailAddress) Then
            Return True
        End If
        Dim email As New Regex("([\w-+]+(?:\.[\w-+]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7})")
        If email.IsMatch(emailAddress) Then
            Return True
        Else
            Return False
        End If
    End Function

#End Region

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        If buttonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub

    Sub SetCommandColumnLocation()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim buttonPosition As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            buttonPosition = objParamSettingbutton.SettingValue
        End If

        ColumnActionLocation(gp_RFI, cc_RFI, buttonPosition)
        ColumnActionLocation(gp_OtherCase, cc_OtherCase, buttonPosition)

    End Sub


#Region "10-Feb-2022 : Export Hit/Search Transaction to CSV/Excel"
    Protected Sub ExportAll_CaseAlert_Typology(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            'Get Data Transaction
            'Dim strQuery As String = "SELECT ofcmtt.Date_Transaction, ofcmtt.Account_NO, ofcmtt.Transmode_Code, ofcmtt.Transaction_Remark, ofcmtt.IDR_Amount,"
            'strQuery &= " ofcmtt.country_code, ofcmtt.country_code_lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            '29-Mar-2022 Adi
            Dim strQuery As String = "SELECT ofcmtt.Date_Transaction"
            strQuery &= " ,ofcmtt.Ref_Num"
            strQuery &= " ,ofcmtt.Transmode_Code"
            strQuery &= " ,ofcmtt.Transaction_Location"
            strQuery &= " ,ofcmtt.Debit_Credit"
            strQuery &= " ,ofcmtt.Currency"
            strQuery &= " ,ofcmtt.Original_Amount"
            strQuery &= " ,ofcmtt.IDR_Amount"
            strQuery &= " ,ofcmtt.Transaction_Remark"
            strQuery &= " ,ofcmtt.Account_NO"
            strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,ofcmtt.Account_No_Lawan"
            strQuery &= " ,ofcmtt.CIF_No_Lawan"
            strQuery &= " ,ofcmtt.WIC_No_Lawan"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            'Get Data CM
            strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Typology_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CaseManagementTypologyTrx")

                    'Display Header of CM
                    If drCM IsNot Nothing Then
                        ws.Cells("A1").Value = "Case Management ID"
                        ws.Cells("B1").Value = drCM("PK_CaseManagement_ID").ToString

                        ws.Cells("A2").Value = "CIF"
                        ws.Cells("B2").Value = drCM("CIF_No")

                        ws.Cells("A3").Value = "Customer Name"
                        ws.Cells("B3").Value = drCM("Customer_Name")

                        ws.Cells("A4").Value = "Alert Type"
                        ws.Cells("B4").Value = drCM("Alert_Type")

                        ws.Cells("A5").Value = "Case Description"
                        ws.Cells("B5").Value = drCM("Case_Description")

                        ws.Cells("A6").Value = "Process Date"
                        ws.Cells("B6").Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                    End If

                    'objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    'objtbl.Columns("Account_NO").SetOrdinal(1)
                    'objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    'objtbl.Columns("Transaction_Remark").SetOrdinal(3)
                    'objtbl.Columns("IDR_Amount").SetOrdinal(4)
                    'objtbl.Columns("country_code").SetOrdinal(5)
                    'objtbl.Columns("country_code_lawan").SetOrdinal(6)

                    'objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                    'objtbl.Columns("Account_NO").ColumnName = "Account No"
                    'objtbl.Columns("Transmode_Code").ColumnName = "Transmode Code"
                    'objtbl.Columns("Transaction_Remark").ColumnName = "Transaction Remark"
                    'objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    'objtbl.Columns("country_code").ColumnName = "Country Code"
                    'objtbl.Columns("country_code_lawan").ColumnName = "Opp. Country Code"

                    '29-Mar-2022 Adi : Perubahan skema informasi yang ditampilkan
                    objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    objtbl.Columns("Ref_Num").SetOrdinal(1)
                    objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    objtbl.Columns("Transaction_Location").SetOrdinal(3)
                    objtbl.Columns("Debit_Credit").SetOrdinal(4)
                    objtbl.Columns("Currency").SetOrdinal(5)
                    objtbl.Columns("Original_Amount").SetOrdinal(6)
                    objtbl.Columns("IDR_Amount").SetOrdinal(7)
                    objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                    objtbl.Columns("Account_NO").SetOrdinal(9)
                    objtbl.Columns("CounterPartyType").SetOrdinal(10)
                    objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                    objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                    objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                    objtbl.Columns("CounterPartyName").SetOrdinal(14)
                    objtbl.Columns("Country_Lawan").SetOrdinal(15)

                    objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
                    objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
                    objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
                    objtbl.Columns("Transaction_Location").ColumnName = "Location"
                    objtbl.Columns("Debit_Credit").ColumnName = "D/C"
                    objtbl.Columns("Currency").ColumnName = "CCY"
                    objtbl.Columns("Original_Amount").ColumnName = "Amount"
                    objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                    objtbl.Columns("Account_NO").ColumnName = "Account No."
                    objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                    objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                    objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                    objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                    objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                    objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"

                    ws.Cells("A8").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Management Typology Transaction (Case ID " & drCM("PK_CaseManagement_ID") & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportAll_CaseAlert_Outlier(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            ''Get Data Transaction
            'Dim strQuery As String = "SELECT ofcmot.Date_Transaction, ofcmot.Account_NO, ofcmot.Transmode_Code, ofcmot.Transaction_Remark, ofcmot.IDR_Amount,"
            'strQuery &= " ofcmot.country_code, ofcmot.country_code_lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmot"
            'strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmo ON ofcmot.FK_OneFCC_CaseManagement_Outlier_ID = ofcmo.PK_OneFCC_CaseManagement_Outlier_ID"
            'strQuery &= " WHERE ofcmo.FK_CaseManagement_ID = " & IDUnik

            '29-Mar-2022 Adi
            Dim strQuery As String = "SELECT ofcmtt.Date_Transaction"
            strQuery &= " ,ofcmtt.Ref_Num"
            strQuery &= " ,ofcmtt.Transmode_Code"
            strQuery &= " ,ofcmtt.Transaction_Location"
            strQuery &= " ,ofcmtt.Debit_Credit"
            strQuery &= " ,ofcmtt.Currency"
            strQuery &= " ,ofcmtt.Original_Amount"
            strQuery &= " ,ofcmtt.IDR_Amount"
            strQuery &= " ,ofcmtt.Transaction_Remark"
            strQuery &= " ,ofcmtt.Account_NO"
            strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,ofcmtt.Account_No_Lawan"
            strQuery &= " ,ofcmtt.CIF_No_Lawan"
            strQuery &= " ,ofcmtt.WIC_No_Lawan"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            'Get Data CM
            strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Outlier_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CaseManagementOutlierTrx")

                    'Display Header of CM
                    If drCM IsNot Nothing Then
                        ws.Cells("A1").Value = "Case Management ID"
                        ws.Cells("B1").Value = drCM("PK_CaseManagement_ID").ToString

                        ws.Cells("A2").Value = "CIF"
                        ws.Cells("B2").Value = drCM("CIF_No")

                        ws.Cells("A3").Value = "Customer Name"
                        ws.Cells("B3").Value = drCM("Customer_Name")

                        ws.Cells("A4").Value = "Alert Type"
                        ws.Cells("B4").Value = drCM("Alert_Type")

                        ws.Cells("A5").Value = "Case Description"
                        ws.Cells("B5").Value = drCM("Case_Description")

                        ws.Cells("A6").Value = "Process Date"
                        ws.Cells("B6").Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                    End If

                    'Display Financial Statistics
                    Dim strSQL As String = ""
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=2"
                    Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                        ws.Cells("A8").Value = "Financial Statistics in past " & drParam("ParameterValue") & " month(s)"
                    End If

                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Outlier WHERE FK_CaseManagement_ID=" & IDUnik
                    Dim drCMO As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drCMO IsNot Nothing Then
                        ws.Cells("A9").Value = "Mean Debit"
                        ws.Cells("B9").Value = CDbl(drCMO("Mean_Debit")).ToString("#,##0.00")

                        ws.Cells("A10").Value = "Mean Credit"
                        ws.Cells("B10").Value = CDbl(drCMO("Mean_Credit")).ToString("#,##0.00")

                        ws.Cells("A11").Value = "Modus Debit"
                        ws.Cells("B11").Value = CDbl(drCMO("Modus_Debit")).ToString("#,##0.00")

                        ws.Cells("A12").Value = "Modus Credit"
                        ws.Cells("B12").Value = CDbl(drCMO("Modus_Credit")).ToString("#,##0.00")
                    End If

                    'objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    'objtbl.Columns("Account_NO").SetOrdinal(1)
                    'objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    'objtbl.Columns("Transaction_Remark").SetOrdinal(3)
                    'objtbl.Columns("IDR_Amount").SetOrdinal(4)
                    'objtbl.Columns("country_code").SetOrdinal(5)
                    'objtbl.Columns("country_code_lawan").SetOrdinal(6)

                    'objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                    'objtbl.Columns("Account_NO").ColumnName = "Account No"
                    'objtbl.Columns("Transmode_Code").ColumnName = "Transmode Code"
                    'objtbl.Columns("Transaction_Remark").ColumnName = "Transaction Remark"
                    'objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    'objtbl.Columns("country_code").ColumnName = "Country Code"
                    'objtbl.Columns("country_code_lawan").ColumnName = "Opp. Country Code"

                    '29-Mar-2022 Adi : Perubahan skema informasi yang ditampilkan
                    objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    objtbl.Columns("Ref_Num").SetOrdinal(1)
                    objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    objtbl.Columns("Transaction_Location").SetOrdinal(3)
                    objtbl.Columns("Debit_Credit").SetOrdinal(4)
                    objtbl.Columns("Currency").SetOrdinal(5)
                    objtbl.Columns("Original_Amount").SetOrdinal(6)
                    objtbl.Columns("IDR_Amount").SetOrdinal(7)
                    objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                    objtbl.Columns("Account_NO").SetOrdinal(9)
                    objtbl.Columns("CounterPartyType").SetOrdinal(10)
                    objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                    objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                    objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                    objtbl.Columns("CounterPartyName").SetOrdinal(14)
                    objtbl.Columns("Country_Lawan").SetOrdinal(15)

                    objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
                    objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
                    objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
                    objtbl.Columns("Transaction_Location").ColumnName = "Location"
                    objtbl.Columns("Debit_Credit").ColumnName = "D/C"
                    objtbl.Columns("Currency").ColumnName = "CCY"
                    objtbl.Columns("Original_Amount").ColumnName = "Amount"
                    objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                    objtbl.Columns("Account_NO").ColumnName = "Account No."
                    objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                    objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                    objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                    objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                    objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                    objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"

                    ws.Cells("A14").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Management Outlier Transaction (Case ID " & drCM("PK_CaseManagement_ID") & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportAll_SearchTransaction(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            'Date From and Date To mandatory
            If txt_SearchTransaction_From.SelectedDate = DateTime.MinValue Or txt_SearchTransaction_To.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException("Please fill Date From and Date To for searching transaction!")
            End If

            'Get Data CM
            Dim strQuery As String = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)


            'Get Data Transaction
            'strQuery = "SELECT gaot.Date_Transaction, gaot.Account_NO, gaot.Transmode_Code, gaot.Transaction_Remark, gaot.IDR_Amount,"
            'strQuery &= " gaot.country_code, gaot.country_code_lawan"
            'strQuery &= " FROM goAML_ODM_Transaksi AS gaot"
            'strQuery &= " WHERE gaot.CIF_NO = '" & Me.CIFNo & "'"

            '29-Mar-2022 Adi
            strQuery = "SELECT gaot.Date_Transaction"
            strQuery &= " ,gaot.Ref_Num"
            strQuery &= " ,gaot.Transmode_Code"
            strQuery &= " ,gaot.Transaction_Location"
            strQuery &= " ,gaot.Debit_Credit"
            strQuery &= " ,gaot.Currency"
            strQuery &= " ,gaot.Original_Amount"
            strQuery &= " ,gaot.IDR_Amount"
            strQuery &= " ,gaot.Transaction_Remark"
            strQuery &= " ,gaot.Account_NO"
            strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,gaot.Account_No_Lawan"
            strQuery &= " ,gaot.CIF_No_Lawan"
            strQuery &= " ,gaot.WIC_No_Lawan"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM goAML_ODM_Transaksi AS gaot"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON gaot.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON gaot.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON gaot.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE gaot.CIF_NO = '" & Me.CIFNo & "'"

            Dim dtTransactionAll As Data.DataTable
            If Not (txt_SearchTransaction_From.SelectedDate = DateTime.MinValue And txt_SearchTransaction_To.SelectedDate = DateTime.MinValue) Then
                strQuery += " And Date_Transaction >= '" & CDate(txt_SearchTransaction_From.SelectedDate).ToString("yyyy-MM-dd") & "'"
                strQuery += " AND Date_Transaction <= '" & CDate(txt_SearchTransaction_To.SelectedDate).ToString("yyyy-MM-dd") & "'"

                dtTransactionAll = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            Else
                dtTransactionAll = New DataTable
            End If

            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Typology_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("ListOfTransaction")

                    'Display Header of CM
                    If drCM IsNot Nothing Then
                        ws.Cells("A1").Value = "List of Customer's Transactions"

                        ws.Cells("A2").Value = "CIF"
                        ws.Cells("B2").Value = drCM("CIF_No")

                        ws.Cells("A3").Value = "Customer Name"
                        ws.Cells("B3").Value = drCM("Customer_Name")

                        ws.Cells("A4").Value = "From Date"
                        ws.Cells("B4").Value = CDate(txt_SearchTransaction_From.SelectedDate).ToString("dd-MMM-yyyy")

                        ws.Cells("A5").Value = "To Date"
                        ws.Cells("B5").Value = CDate(txt_SearchTransaction_To.SelectedDate).ToString("dd-MMM-yyyy")

                        ws.Cells("A6").Value = "Generate Date"
                        ws.Cells("B6").Value = CDate(DateTime.Now).ToString("dd-MMM-yyyy HH:mm:ss")
                    End If

                    'objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    'objtbl.Columns("Account_NO").SetOrdinal(1)
                    'objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    'objtbl.Columns("Transaction_Remark").SetOrdinal(3)
                    'objtbl.Columns("IDR_Amount").SetOrdinal(4)
                    'objtbl.Columns("country_code").SetOrdinal(5)
                    'objtbl.Columns("country_code_lawan").SetOrdinal(6)

                    'objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                    'objtbl.Columns("Account_NO").ColumnName = "Account No"
                    'objtbl.Columns("Transmode_Code").ColumnName = "Transmode Code"
                    'objtbl.Columns("Transaction_Remark").ColumnName = "Transaction Remark"
                    'objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    'objtbl.Columns("country_code").ColumnName = "Country Code"
                    'objtbl.Columns("country_code_lawan").ColumnName = "Opp. Country Code"

                    '29-Mar-2022 Adi : Perubahan skema informasi yang ditampilkan
                    objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    objtbl.Columns("Ref_Num").SetOrdinal(1)
                    objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    objtbl.Columns("Transaction_Location").SetOrdinal(3)
                    objtbl.Columns("Debit_Credit").SetOrdinal(4)
                    objtbl.Columns("Currency").SetOrdinal(5)
                    objtbl.Columns("Original_Amount").SetOrdinal(6)
                    objtbl.Columns("IDR_Amount").SetOrdinal(7)
                    objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                    objtbl.Columns("Account_NO").SetOrdinal(9)
                    objtbl.Columns("CounterPartyType").SetOrdinal(10)
                    objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                    objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                    objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                    objtbl.Columns("CounterPartyName").SetOrdinal(14)
                    objtbl.Columns("Country_Lawan").SetOrdinal(15)

                    objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
                    objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
                    objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
                    objtbl.Columns("Transaction_Location").ColumnName = "Location"
                    objtbl.Columns("Debit_Credit").ColumnName = "D/C"
                    objtbl.Columns("Currency").ColumnName = "CCY"
                    objtbl.Columns("Original_Amount").ColumnName = "Amount"
                    objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                    objtbl.Columns("Account_NO").ColumnName = "Account No."
                    objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                    objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                    objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                    objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                    objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                    objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"

                    ws.Cells("A8").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=List of Transaction (CIF No " & drCM("CIF_No") & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Update 25-Feb-2022 Adi : Other Case Alerts related to Customer"
    Protected Sub store_ReadData_CaseAlert_Other(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort &= item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT * FROM vw_OneFCC_CaseManagement"
            strQuery &= " WHERE PK_CaseManagement_ID <> " & Me.IDUnik
            strQuery &= " AND CIF_No = '" & Me.CIFNo & "'"

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery &= " AND " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_OtherCase.GetStore.DataSource = DataPaging
            gp_OtherCase.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_OtherCase(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            If strCommandName = "Detail" Then
                LoadDataOtherCase(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDataOtherCase(strCaseID As String)
        Try
            'To Do Load Data Other Case
            Dim strSQL As String = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & strCaseID
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            If drCM IsNot Nothing Then

                Me.CaseID_OtherCase = drCM("PK_CaseManagement_ID")

                'General Information
                txt_WindowOther_PK_CaseManagement_ID.Value = drCM("PK_CaseManagement_ID")
                txt_WindowOther_UniqueCMID.Value = drCM("Unique_CM_ID") ' 8 Maret 2023 Ari : update untuk set Unique CM ID
                txt_WindowOther_Case_Description.Value = drCM("Case_Description")
                txt_WindowOther_Alert_Type.Value = drCM("Alert_Type")

                If Not IsDBNull(drCM("FK_CaseStatus_ID")) Then
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_ProposedAction WHERE PK_OneFCC_CaseManagement_ProposedAction_ID=" & drCM("FK_CaseStatus_ID")
                    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drTemp IsNot Nothing Then
                        txt_WindowOther_FK_Case_Status_ID.Value = drTemp("PK_OneFCC_CaseManagement_ProposedAction_ID") & " - " & drTemp("Proposed_Action")
                    Else
                        txt_WindowOther_FK_Case_Status_ID.Value = "New Case"
                    End If
                Else
                    txt_WindowOther_FK_Case_Status_ID.Value = "New Case"
                End If

                If Not IsDBNull(drCM("CreatedDate")) Then
                    txt_WindowOther_CreatedDate.Value = CDate(drCM("CreatedDate")).ToString("dd-MMM-yyyy HH:mm:ss")
                End If

                txt_WindowOther_Workflow_Step.Value = drCM("Workflow_Step_Of")
                txt_WindowOther_PIC.Value = drCM("PIC")

                'Bind RFI sent
                BindDataRFI_OtherCase(drCM("PK_CaseManagement_ID"))

                'Reload GP Transaction
                gp_CaseAlert_Typology_Transaction_OtherCase.Hidden = True
                pnl_CaseAlert_Outlier_Transaction_OtherCase.Hidden = True

                'Load Transaction
                If Not IsDBNull(drCM("Alert_Type")) Then
                    If drCM("Alert_Type") = "Typology Risk" Then
                        gp_CaseAlert_Typology_Transaction_OtherCase.GetStore.Reload()
                        gp_CaseAlert_Typology_Transaction_OtherCase.Hidden = False
                    ElseIf drCM("Alert_Type") = "Financial Risk" Then
                        gp_CaseAlert_Outlier_Transaction_OtherCase.GetStore.Reload()
                        pnl_CaseAlert_Outlier_Transaction_OtherCase.Hidden = False

                        'Display Mean dan Modus
                        strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=2"
                        Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                        If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                            fs_CaseAlert_Outlier_Transaction_OtherCase.Title = "<b>Financial Statistics in past " & drParam("ParameterValue") & " month(s)</b>"
                        End If

                        strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Outlier WHERE FK_CaseManagement_ID=" & drCM("PK_CaseManagement_ID")
                        Dim drCMO As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                        If drCMO IsNot Nothing Then
                            txt_MeanDebit_OtherCase.Value = CDbl(drCMO("Mean_Debit")).ToString("#,##0.00")
                            txt_MeanCredit_OtherCase.Value = CDbl(drCMO("Mean_Credit")).ToString("#,##0.00")

                            txt_ModusDebit_OtherCase.Value = CDbl(drCMO("Modus_Debit")).ToString("#,##0.00")
                            txt_ModusCredit_OtherCase.Value = CDbl(drCMO("Modus_Credit")).ToString("#,##0.00")
                        End If
                    End If
                End If

                'Load Workflow History
                gp_WorkflowHistory_OtherCase.GetStore.Reload()

                'Reload Gridpanel Transaction
                gp_CaseAlert_Transaction_OtherCase.GetStore.Reload()

                '23-Aug-2022 Adi : Tambah Gridpanel Activity
                gp_CaseAlert_Activity_OtherCase.GetStore.Reload()

                '23-Aug-2022 Adi : Hide/Show Gridpanel Transaction and Activity
                strSQL = "SELECT COUNT(1) FROM vw_OneFCC_CaseManagement_Alert_Transaction WHERE FK_CaseManagement_ID=" & strCaseID
                Dim intTotalTransaction As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL, Nothing)
                If intTotalTransaction = 0 Then
                    gp_CaseAlert_Transaction_OtherCase.Hidden = True
                Else
                    gp_CaseAlert_Transaction_OtherCase.Hidden = False
                End If

                strSQL = "SELECT COUNT(1) FROM vw_OneFCC_CaseManagement_Alert_Activity WHERE FK_CaseManagement_ID=" & strCaseID
                Dim intTotalActivity As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL, Nothing)
                If intTotalActivity = 0 Then
                    gp_CaseAlert_Activity_OtherCase.Hidden = True
                Else
                    gp_CaseAlert_Activity_OtherCase.Hidden = False
                End If
                'End of 23-Aug-2022 Adi : Hide/Show Gridpanel Transaction and Activity
            End If

            'Show Window
            window_OtherCase.Hidden = False

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_OtherCase_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            window_OtherCase.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BindDataRFI_OtherCase(strCaseID As String)
        Try
            Dim strQuery As String = "SELECT * FROM OneFCC_CaseManagement_RFI WHERE FK_CaseManagement_ID=" & strCaseID
            Dim dtRFI As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If dtRFI Is Nothing Then
                dtRFI = New DataTable
            End If

            If dtRFI IsNot Nothing Then
                'Running the following script for additional columns
                dtRFI.Columns.Add(New DataColumn("EmailStatusName", GetType(String)))

                'Get reference Table
                strQuery = "SELECT * FROM EmailStatus"
                Dim dtEmailStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                For Each row As DataRow In dtRFI.Rows
                    If dtEmailStatus IsNot Nothing AndAlso Not IsDBNull(row("FK_EmailStatus_ID")) Then
                        Dim drCek = dtEmailStatus.Select("PK_EmailStatus_ID='" & row("FK_EmailStatus_ID") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("EmailStatusName") = drCek("EmailStatusName")
                        End If
                    End If
                Next

                'Bind to gridpanel
                gp_RFI_OtherCase.GetStore.DataSource = dtRFI
                gp_RFI_OtherCase.GetStore.DataBind()

            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub gc_RFI_OtherCase(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            If strCommandName = "Download" Then
                DownloadRFI(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Outlier_Transaction_OtherCase(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Outlier AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_OtherCase

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_OtherCase

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " AND " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Outlier_Transaction_OtherCase.GetStore.DataSource = DataPaging
            gp_CaseAlert_Outlier_Transaction_OtherCase.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Typology_Transaction_OtherCase(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_OtherCase

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_OtherCase

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " AND " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Typology_Transaction_OtherCase.GetStore.DataSource = DataPaging
            gp_CaseAlert_Typology_Transaction_OtherCase.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_WorkflowHistory_OtherCase(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort &= item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT history.*, proposed.Proposed_Action FROM onefcc_casemanagement_workflowhistory history"
            strQuery &= " LEFT JOIN OneFCC_CaseManagement_ProposedAction proposed ON history.FK_Proposed_Status_ID = proposed.PK_OneFCC_CaseManagement_ProposedAction_ID"
            strQuery &= " WHERE FK_CaseManagement_ID = " & Me.CaseID_OtherCase

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)

            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_WorkflowHistory_OtherCase.GetStore.DataSource = DataPaging
            gp_WorkflowHistory_OtherCase.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#End Region


#Region "Update 01-Mar-2022 Adi : Case Assignment"

    Protected Sub btn_AssignToMe_Click(sender As Object, e As DirectEventArgs)
        Try
            'Validasi dulu apakah Case sudah di assign ke user lain
            Dim strSQL As String = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            If drCM IsNot Nothing Then
                If Not IsDBNull(drCM("PIC")) AndAlso Not drCM("PIC").ToString.Contains(",") Then
                    Throw New ApplicationException("This Case Alert has been assigned to " & drCM("PIC"))
                End If
            End If

            'Selected PIC
            Dim strSelectedPIC As String = NawaBLL.Common.SessionCurrentUser.UserID

            'Change the PIC with current user
            Dim strQuery As String = "UPDATE OneFCC_CaseManagement SET"
            strQuery &= " PIC = '" & strSelectedPIC & "'"
            strQuery &= ", LastUpdateBy='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery &= ", LastUpdateDate=GETDATE()"
            strQuery &= " WHERE PK_CaseManagement_ID=" & IDUnik
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            txt_PIC.Value = strSelectedPIC

            'Show button Save and Change Assignment
            btn_AssignToMe.Hidden = True
            btn_Save.Hidden = False
            btn_ChangeAssignment.Hidden = False

            'Insert into Workflow History
            strQuery = "INSERT INTO OneFCC_CaseManagement_WorkflowHistory ("
            strQuery &= "FK_CaseManagement_ID,"
            strQuery &= "Workflow_Step,"
            strQuery &= "FK_Proposed_Status_ID,"
            strQuery &= "Analysis_Result,"
            strQuery &= "CreatedBy,"
            strQuery &= "CreatedDate) "
            strQuery &= "VALUES ("
            strQuery &= Me.IDUnik & ","
            strQuery &= Me.Workflow_Step & ","
            strQuery &= "NULL,"
            strQuery &= "'Case Assigned to " & strSelectedPIC & "',"
            strQuery &= "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
            strQuery &= "GETDATE())"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            gp_WorkflowHistory.GetStore.Reload()

            'Show button send RFI and Panel Analysis
            pnl_AnalysisResult.Hidden = False
            tb_RFI.Hidden = False

            'Show information
            Throw New ApplicationException("Case has been successfully assigned to " & strSelectedPIC & ".")

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_ChangeAssignment_Click(sender As Object, e As DirectEventArgs)
        Try
            'Populate Combobox Assign To
            Dim strQuery As String = "SELECT a.PK_CaseManagement_WorkflowDetail_ID, a.FK_CaseManagement_Workflow_ID, a.Workflow_Step, b.String AS PIC"
            strQuery &= " FROM OneFCC_CaseManagement_WorkflowDetail a"
            strQuery &= " CROSS APPLY dbo.ufn_CSVToTable( a.PIC, ',' ) b"
            strQuery &= " WHERE a.FK_CaseManagement_Workflow_ID = " & Me.Workflow_ID & " AND a.Workflow_Step = " & Me.Workflow_Step
            strQuery &= " AND b.String <> '" & NawaBLL.Common.SessionCurrentUser.UserID & "'"

            store_AssignTo.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
            store_AssignTo.DataBind()
            cbo_AssignTo.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
            store_AssignTo.PageSize = NawaBLL.SystemParameterBLL.GetPageSize

            cbo_AssignTo.ClearValue()

            window_ChangeAssignment.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_ChangeAssignment_Save_Click(sender As Object, e As DirectEventArgs)
        Try
            'Validate 
            If String.IsNullOrEmpty(cbo_AssignTo.SelectedItem.Value) Then
                Throw New ApplicationException(cbo_AssignTo.FieldLabel & " is required.")
            End If
            If String.IsNullOrWhiteSpace(txt_ChangeAssignmentReason.Value) Then
                Throw New ApplicationException(txt_ChangeAssignmentReason.FieldLabel & " is required.")
            End If

            'Selected PIC
            Dim strSelectedPIC As String = cbo_AssignTo.SelectedItem.Value

            'Change the PIC with current user
            Dim strQuery As String = "UPDATE OneFCC_CaseManagement SET"
            strQuery &= " PIC = '" & strSelectedPIC & "'"
            strQuery &= ", LastUpdateBy='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery &= ", LastUpdateDate=GETDATE()"
            strQuery &= " WHERE PK_CaseManagement_ID=" & IDUnik
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            txt_PIC.Value = strSelectedPIC

            'Show button Save and Change Assignment
            btn_AssignToMe.Hidden = True
            btn_Save.Hidden = True
            btn_ChangeAssignment.Hidden = True
            pnl_AnalysisResult.Hidden = True

            'Insert into Workflow History
            strQuery = "INSERT INTO OneFCC_CaseManagement_WorkflowHistory ("
            strQuery &= "FK_CaseManagement_ID,"
            strQuery &= "Workflow_Step,"
            strQuery &= "FK_Proposed_Status_ID,"
            strQuery &= "Analysis_Result,"
            strQuery &= "CreatedBy,"
            strQuery &= "CreatedDate) "
            strQuery &= "VALUES ("
            strQuery &= Me.IDUnik & ","
            strQuery &= Me.Workflow_Step & ","
            strQuery &= "NULL,"
            strQuery &= "'Case Assignment Changed to " & strSelectedPIC & "<br>Reason : " & txt_ChangeAssignmentReason.Value & "',"
            strQuery &= "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
            strQuery &= "GETDATE())"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            gp_WorkflowHistory.GetStore.Reload()

            'Show button send RFI and Panel Analysis
            pnl_AnalysisResult.Hidden = False
            tb_RFI.Hidden = False

            'Hide window
            window_ChangeAssignment.Hidden = True

            'Show information
            Throw New ApplicationException("Case has been successfully assigned to " & strSelectedPIC & ".")

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_ChangeAssignment_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            window_ChangeAssignment.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Update 04-Mar-2022 Adi : Download RFI Attachment from window detail"
    Protected Sub btn_DownloadEmailAttachment_Click()
        Try
            DownloadRFI(RFI_ID)
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Update 24-Mar-2022 Adi"
    Protected Sub btn_CustomerDetail_Click()
        Try
            LoadCustomerDetail()

            window_CustomerDetail.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_CustomerDetail_Back_Click()
        Try
            window_CustomerDetail.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadCustomerDetail()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_COUNTRY"
            Dim dtCountry As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Get Customer Information
            txt_CustomerDetail_CIF.Value = txt_CIF_No.Value
            txt_CustomerDetail_Name.Value = txt_Customer_Name.Value

            strQuery = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & Me.CIFNo & "'"
            Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If drCustomer IsNot Nothing Then
                txt_CustomerDetail_TypeCode.Value = drCustomer("FK_AML_Customer_Type_Code")
                txt_CustomerDetail_POB.Value = drCustomer("PLACEOFBIRTH")
                If Not IsDBNull(drCustomer("DATEOFBIRTH")) Then
                    txt_CustomerDetail_DOB.Value = CDate(drCustomer("DATEOFBIRTH")).ToString("dd-MMM-yyyy")
                End If

                txt_CustomerDetail_Nationality.Value = drCustomer("FK_AML_CITIZENSHIP_CODE")
                If dtCountry IsNot Nothing AndAlso Not IsDBNull(drCustomer("FK_AML_CITIZENSHIP_CODE")) Then
                    Dim drCek = dtCountry.Select("FK_AML_COUNTRY_CODE='" & drCustomer("FK_AML_CITIZENSHIP_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        txt_CustomerDetail_Nationality.Value = drCustomer("FK_AML_CITIZENSHIP_CODE") & " - " & drCek("AML_COUNTRY_Name")
                    End If
                End If

                If Not IsDBNull(drCustomer("FK_AML_PEKERJAAN_CODE")) Then
                    strQuery = "SELECT TOP 1 * FROM AML_PEKERJAAN WHERE FK_AML_PEKERJAAN_CODE='" & drCustomer("FK_AML_PEKERJAAN_CODE") & "'"
                    Dim drOccupation As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                    If drOccupation IsNot Nothing Then
                        txt_CustomerDetail_Occupation.Value = drCustomer("FK_AML_PEKERJAAN_CODE") & " - " & drOccupation("AML_PEKERJAAN_Name")
                    Else
                        txt_CustomerDetail_Occupation.Value = drCustomer("FK_AML_PEKERJAAN_CODE")
                    End If
                End If

                txt_CustomerDetail_Employer.Value = drCustomer("WORK_PLACE")

                txt_CustomerDetail_LegalForm.Value = drCustomer("FK_AML_BENTUK_BADAN_USAHA_CODE")
                If Not IsDBNull(drCustomer("FK_AML_INDUSTRY_CODE")) Then
                    strQuery = "SELECT TOP 1 * FROM AML_INDUSTRY WHERE FK_AML_INDUSTRY_CODE='" & drCustomer("FK_AML_INDUSTRY_CODE") & "'"
                    Dim drIndustry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                    If drIndustry IsNot Nothing Then
                        txt_CustomerDetail_Occupation.Value = drCustomer("FK_AML_INDUSTRY_CODE") & " - " & drIndustry("AML_INDUSTRY_Name")
                    Else
                        txt_CustomerDetail_Occupation.Value = drCustomer("FK_AML_INDUSTRY_CODE")
                    End If
                End If

                txt_CustomerDetail_NPWP.Value = drCustomer("NPWP")
                If Not IsDBNull(drCustomer("INCOME_LEVEL")) Then
                    txt_CustomerDetail_Income.Value = CLng(drCustomer("INCOME_LEVEL")).ToString("#,##0.00")
                End If
                txt_CustomerDetail_PurposeOfFund.Value = drCustomer("TUJUAN_DANA")
                txt_CustomerDetail_SourceOfFund.Value = drCustomer("SOURCE_OF_FUND")
            End If

            'Binding Address
            BindCustomerAddress()

            'Binding Contact/Phone
            BindCustomerContact()

            'Binding Identity
            BindCustomerIdentity()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindCustomerAddress()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_COUNTRY"
            Dim dtCountry As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            strQuery = "SELECT * FROM AML_ADDRESS_TYPE"
            Dim dtAddressType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding Address
            strQuery = "SELECT * FROM AML_CUSTOMER_ADDRESS WHERE CIFNO='" & Me.CIFNo & "'"
            Dim dtAddress As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtAddress Is Nothing Then
                dtAddress = New DataTable
            End If
            dtAddress.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))
            dtAddress.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))

            For Each item In dtAddress.Rows
                item("AML_ADDRESS_TYPE_NAME") = item("FK_AML_ADDRESS_TYPE_CODE")
                If dtAddressType IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_ADDRESS_TYPE_CODE")) Then
                    Dim drCek = dtAddressType.Select("FK_AML_ADDRES_TYPE_CODE='" & item("FK_AML_ADDRESS_TYPE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("AML_ADDRESS_TYPE_NAME") = item("FK_AML_ADDRESS_TYPE_CODE") & " - " & drCek("ADDRESS_TYPE_NAME")
                    End If
                End If

                item("COUNTRY_NAME") = item("FK_AML_COUNTRY_CODE")
                If dtCountry IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_COUNTRY_CODE")) Then
                    Dim drCek = dtCountry.Select("FK_AML_COUNTRY_CODE='" & item("FK_AML_COUNTRY_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("COUNTRY_NAME") = item("FK_AML_COUNTRY_CODE") & " - " & drCek("AML_COUNTRY_Name")
                    End If
                End If
            Next

            gridAddressInfo.GetStore.DataSource = dtAddress
            gridAddressInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindCustomerContact()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_CONTACT_TYPE"
            Dim dtContactType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding Contact
            strQuery = "SELECT * FROM AML_CUSTOMER_CONTACT WHERE CIFNO='" & Me.CIFNo & "'"
            Dim dtContact As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtContact Is Nothing Then
                dtContact = New DataTable
            End If
            dtContact.Columns.Add(New DataColumn("AML_CONTACT_TYPE_NAME", GetType(String)))

            For Each item In dtContact.Rows
                item("AML_CONTACT_TYPE_NAME") = item("FK_AML_CONTACT_TYPE_CODE")
                If dtContactType IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_CONTACT_TYPE_CODE")) Then
                    Dim drCek = dtContactType.Select("FK_AML_CONTACT_TYPE_CODE='" & item("FK_AML_CONTACT_TYPE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("AML_CONTACT_TYPE_NAME") = item("FK_AML_CONTACT_TYPE_CODE") & " - " & drCek("CONTACT_TYPE_NAME")
                    End If
                End If
            Next

            gridPhoneInfo.GetStore.DataSource = dtContact
            gridPhoneInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindCustomerIdentity()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_IDENTITY_TYPE"
            Dim dtIDENTITYType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            strQuery = "SELECT * FROM AML_COUNTRY"
            Dim dtCountry As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding IDENTITY
            strQuery = "SELECT * FROM AML_CUSTOMER_IDENTITY WHERE CIFNO='" & Me.CIFNo & "'"
            Dim dtIDENTITY As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtIDENTITY Is Nothing Then
                dtIDENTITY = New DataTable
            End If
            dtIDENTITY.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
            dtIDENTITY.Columns.Add(New DataColumn("IDENTITY_TYPE_NAME", GetType(String)))

            For Each item In dtIDENTITY.Rows
                item("IDENTITY_TYPE_NAME") = item("FK_AML_IDENTITY_TYPE_CODE")
                If dtIDENTITYType IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_IDENTITY_TYPE_CODE")) Then
                    Dim drCek = dtIDENTITYType.Select("FK_AML_IDENTITY_TYPE_CODE='" & item("FK_AML_IDENTITY_TYPE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("IDENTITY_TYPE_NAME") = item("FK_AML_IDENTITY_TYPE_CODE") & " - " & drCek("IDENTITY_TYPE_NAME")
                    End If
                End If
                item("COUNTRY_NAME") = item("FK_AML_COUNTRYISSUE_CODE")
                If dtCountry IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_COUNTRYISSUE_CODE")) Then
                    Dim drCek = dtCountry.Select("FK_AML_COUNTRY_CODE='" & item("FK_AML_COUNTRYISSUE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("COUNTRY_NAME") = item("FK_AML_COUNTRYISSUE_CODE") & " - " & drCek("AML_COUNTRY_Name")
                    End If
                End If
            Next

            gridIdentityInfo.GetStore.DataSource = dtIDENTITY
            gridIdentityInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "29-Mar-2022 Adi : Add RFI Notes"
    Protected Sub btn_SendRFI_SaveNotes_Click()
        Try
            If String.IsNullOrWhiteSpace(txt_EmailNotes.Value) Then
                Throw New ApplicationException(txt_EmailNotes.FieldLabel & " is required.")
            End If

            Dim strQuery As String = ""

            'Update Case Management Status and Proposed Status
            strQuery = "UPDATE OneFCC_CaseManagement_RFI SET Notes='" & txt_EmailNotes.Value & "'"
            strQuery &= ",LastUpdateBy='" & NawaBLL.Common.SessionCurrentUser.UserID & "', LastUpdateDate=GETDATE()"
            strQuery &= " WHERE PK_OneFCC_CaseManagement_RFI_ID=" & Me.RFI_ID
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Bind to GridPanel
            BindDataRFI()

            'Close window
            Window_SendRFI.Hidden = True

            'Show messagebox
            Throw New ApplicationException("RFI Notes has been saved successfully.")

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "20-Apr-2022 Adi : Alerts grouped by ProcessDate and CIF_No"
    Protected Sub store_ReadData_CaseAlert_Transaction(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT * FROM vw_OneFCC_CaseManagement_Alert_Transaction"
            strQuery &= " WHERE FK_CaseManagement_ID = " & IDUnik

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Transaction.GetStore.DataSource = DataPaging
            gp_CaseAlert_Transaction.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportAll_CaseAlert_Transaction(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            'Get Data Transaction
            'Dim strQuery As String = "SELECT ofcmtt.Date_Transaction, ofcmtt.Account_NO, ofcmtt.Transmode_Code, ofcmtt.Transaction_Remark, ofcmtt.IDR_Amount,"
            'strQuery &= " ofcmtt.country_code, ofcmtt.country_code_lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            '29-Mar-2022 Adi
            Dim strQuery As String = "SELECT Case_Description"
            strQuery &= " ,Date_Transaction"
            strQuery &= " ,Ref_Num"
            strQuery &= " ,Transmode_Code"
            strQuery &= " ,Transaction_Location"
            strQuery &= " ,Debit_Credit"
            strQuery &= " ,Currency"
            strQuery &= " ,Original_Amount"
            strQuery &= " ,IDR_Amount"
            strQuery &= " ,Transaction_Remark"
            strQuery &= " ,Account_NO"
            strQuery &= " ,CounterPartyType"
            strQuery &= " ,Account_No_Lawan"
            strQuery &= " ,CIF_No_Lawan"
            strQuery &= " ,WIC_No_Lawan"
            strQuery &= " ,CounterPartyName"
            strQuery &= " ,Country_Lawan"
            strQuery &= " FROM vw_OneFCC_CaseManagement_Alert_Transaction"
            strQuery &= " WHERE FK_CaseManagement_ID = " & IDUnik
            'strQuery &= " ORDER BY Account_No, Case_Description"

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            'Get Data CM
            strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Typology_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CaseManagementTypologyTrx")

                    'Display Header of CM
                    If drCM IsNot Nothing Then
                        ws.Cells("A1").Value = "Case Management ID"
                        ws.Cells("B1").Value = drCM("PK_CaseManagement_ID").ToString

                        ws.Cells("A2").Value = "CIF"
                        ws.Cells("B2").Value = drCM("CIF_No")

                        ws.Cells("A3").Value = "Customer Name"
                        ws.Cells("B3").Value = drCM("Customer_Name")

                        ws.Cells("A4").Value = "Process Date"
                        ws.Cells("B4").Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                    End If

                    objtbl.Columns("Case_Description").SetOrdinal(0)
                    objtbl.Columns("Date_Transaction").SetOrdinal(1)
                    objtbl.Columns("Ref_Num").SetOrdinal(2)
                    objtbl.Columns("Transmode_Code").SetOrdinal(3)
                    objtbl.Columns("Transaction_Location").SetOrdinal(4)
                    objtbl.Columns("Debit_Credit").SetOrdinal(5)
                    objtbl.Columns("Currency").SetOrdinal(6)
                    objtbl.Columns("Original_Amount").SetOrdinal(7)
                    objtbl.Columns("IDR_Amount").SetOrdinal(8)
                    objtbl.Columns("Transaction_Remark").SetOrdinal(9)
                    objtbl.Columns("Account_NO").SetOrdinal(10)
                    objtbl.Columns("CounterPartyType").SetOrdinal(11)
                    objtbl.Columns("Account_No_Lawan").SetOrdinal(12)
                    objtbl.Columns("CIF_No_Lawan").SetOrdinal(13)
                    objtbl.Columns("WIC_No_Lawan").SetOrdinal(14)
                    objtbl.Columns("CounterPartyName").SetOrdinal(15)
                    objtbl.Columns("Country_Lawan").SetOrdinal(16)

                    objtbl.Columns("Case_Description").ColumnName = "Case Description"
                    objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
                    objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
                    objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
                    objtbl.Columns("Transaction_Location").ColumnName = "Location"
                    objtbl.Columns("Debit_Credit").ColumnName = "D/C"
                    objtbl.Columns("Currency").ColumnName = "CCY"
                    objtbl.Columns("Original_Amount").ColumnName = "Amount"
                    objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                    objtbl.Columns("Account_NO").ColumnName = "Account No."
                    objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                    objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                    objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                    objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                    objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                    objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"

                    ws.Cells("A6").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Alert Transaction (Case ID " & drCM("PK_CaseManagement_ID") & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Transaction_OtherCase(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT * FROM vw_OneFCC_CaseManagement_Alert_Transaction"
            strQuery &= " WHERE FK_CaseManagement_ID = " & Me.CaseID_OtherCase

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Transaction_OtherCase.GetStore.DataSource = DataPaging
            gp_CaseAlert_Transaction_OtherCase.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#End Region

#Region "Update 22-Aug-2022 Adi : Penambahan Dynamic Grid Activity"
    Protected Sub store_ReadData_CaseAlert_Activity(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT * FROM vw_OneFCC_CaseManagement_Alert_Activity"
            strQuery &= " WHERE FK_CaseManagement_ID = " & IDUnik

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Activity.GetStore.DataSource = DataPaging
            gp_CaseAlert_Activity.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Activity_OtherCase(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT * FROM vw_OneFCC_CaseManagement_Alert_Activity"
            strQuery &= " WHERE FK_CaseManagement_ID = " & Me.CaseID_OtherCase

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Activity_OtherCase.GetStore.DataSource = DataPaging
            gp_CaseAlert_Activity_OtherCase.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

End Class