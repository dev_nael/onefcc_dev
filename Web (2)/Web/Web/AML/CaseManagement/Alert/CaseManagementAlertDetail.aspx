﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CaseManagementAlertDetail.aspx.vb" Inherits="CaseManagementAlertDetail" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="SiPendar Profile Data">
        <Items>
            <ext:Panel runat="server" ID="pnlGeneralInformation" Title="General Information" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="10">
                <Items>
                    <%--<ext:DisplayField runat="server" ID="txt_PK_CaseManagement_ID" AnchorHorizontal="100%" FieldLabel="Case Management ID" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_Alert_Type" AnchorHorizontal="100%" FieldLabel="Alert Type" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_Case_Description" AnchorHorizontal="100%" FieldLabel="Case Description" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_FK_Case_Status_ID" AnchorHorizontal="100%" FieldLabel="Case Status" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_ProcessDate" AnchorHorizontal="100%" FieldLabel="Alert Date" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_LastModifiedDate" AnchorHorizontal="100%" FieldLabel="Last Modified Date" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_Proposed_Action" AnchorHorizontal="100%" FieldLabel="Last Proposed Action" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_Proposed_By" AnchorHorizontal="100%" FieldLabel="Proposed By" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_Workflow_Step" AnchorHorizontal="100%" FieldLabel="Workflow Step" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_PIC" AnchorHorizontal="100%" FieldLabel="PIC" LabelWidth="150"></ext:DisplayField>--%>
                    <ext:FieldSet runat="server" Border="true" Padding="10" AnchorHorizontal="100%" Layout="ColumnLayout" ColumnWidth="1" Title="General Information">
                        <Items>
                            <ext:Container runat="server" ColumnWidth="0.5" Layout="AnchorLayout">
                                <Items>
                                    <ext:DisplayField runat="server" ID="txt_PK_CaseManagement_ID" AnchorHorizontal="100%" FieldLabel="Case Management ID" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_Unique_CM_ID" AnchorHorizontal="100%" FieldLabel="Unique CM ID" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_Alert_Type" AnchorHorizontal="100%" FieldLabel="Alert Type" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_RuleBasic" AnchorHorizontal="100%" FieldLabel="Rule Basic" LabelWidth="150" Hidden="true"></ext:DisplayField>
                                    <%--<ext:DisplayField runat="server" ID="txt_FK_Case_Status_ID" AnchorHorizontal="100%" FieldLabel="Case Status" LabelWidth="150"></ext:DisplayField>--%>
                                    <ext:DisplayField runat="server" ID="txt_FK_Case_Status_ID" AnchorHorizontal="100%" FieldLabel="Proposed Action" LabelWidth="150"></ext:DisplayField>
                                </Items>
                            </ext:Container>
                            <ext:Container runat="server" ColumnWidth="0.5" Layout="AnchorLayout">
                                <Items>
                                     <ext:DisplayField runat="server" ID="checkbox_IsTransaction" AnchorHorizontal="100%" FieldLabel="Is Transaction ?" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_ProcessDate" AnchorHorizontal="100%" FieldLabel="Process Date" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_LastModifiedDate" AnchorHorizontal="100%" FieldLabel="Last Modified Date" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_Case_Description" AnchorHorizontal="100%" FieldLabel="Case Description" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_Workflow_Step" AnchorHorizontal="100%" FieldLabel="Workflow Step" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_PIC" AnchorHorizontal="100%" FieldLabel="PIC" LabelWidth="150"></ext:DisplayField>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:Panel>
            <ext:Panel runat="server" ID="pnlCustomerInformation" Title="Customer Information" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="ColumnLayout" BodyPadding="10">
                <Items>
                    <ext:Container runat="server" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField runat="server" ID="txt_CIF_No" AnchorHorizontal="100%" FieldLabel="CIF" LabelWidth="150"></ext:DisplayField>
                            <%--<ext:DisplayField runat="server" ID="txt_Account_No" AnchorHorizontal="100%" FieldLabel="Account No." LabelWidth="150"></ext:DisplayField>--%>
                            <ext:DisplayField runat="server" ID="txt_Customer_Name" AnchorHorizontal="100%" FieldLabel="Customer Name" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_DOB" AnchorHorizontal="100%" FieldLabel="Date of Birth" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_POB" AnchorHorizontal="100%" FieldLabel="Place of Birth" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Gender" AnchorHorizontal="100%" FieldLabel="Gender" LabelWidth="150"></ext:DisplayField>
                        </Items>
                    </ext:Container>
                    <ext:Container runat="server" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField runat="server" ID="txt_CIF_Opening_Branch" AnchorHorizontal="100%" FieldLabel="Opening Branch" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_CIF_Opening_Date" AnchorHorizontal="100%" FieldLabel="Opening Date" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Is_PEP" AnchorHorizontal="100%" FieldLabel="PEP" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Risk_Code" AnchorHorizontal="100%" FieldLabel="Customer Risk" LabelWidth="150"></ext:DisplayField>

                            <ext:Button ID="btn_CustomerDetail" runat="server" Icon="ApplicationViewDetail" Text="More Details">
                                <DirectEvents>
                                    <Click OnEvent="btn_CustomerDetail_Click">
                                        <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Panel>

            <ext:GridPanel ID="gp_Account" runat="server" Title="Account Information" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store_Account" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="Model43" IDProperty="ACCOUNT_NO">
                                <Fields>
                                    <ext:ModelField Name="ACCOUNT_NO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="DATEOPENING" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="BRANCH_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_TYPE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="PRODUCT_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CURRENT_BALANCE" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_CURRENCY_CODE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_ACCOUNT_STATUS_CODE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="LIMITCREDITCARD" Type="Float"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column5" runat="server" DataIndex="ACCOUNT_TYPE" Text="Acc Type" Width="100" Locked="true"></ext:Column>
                        <ext:Column ID="Column412" runat="server" DataIndex="ACCOUNT_NO" Text="Account No." Width="150" Locked="true"></ext:Column>
                        <ext:Column ID="Column72" runat="server" DataIndex="ACCOUNT_NAME" Text="Name" Width="150" Locked="true"></ext:Column>
                        <ext:Column ID="Column7" runat="server" DataIndex="FK_AML_ACCOUNT_STATUS_CODE" Text="Status" Width="100"></ext:Column>
                        <ext:Column ID="Column2" runat="server" DataIndex="BRANCH_NAME" Text="Branch" Width="150"></ext:Column>
                        <ext:DateColumn ID="Column1" runat="server" DataIndex="DATEOPENING" Text="Opening Date" Width="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:NumberColumn ID="Column3" runat="server" DataIndex="CURRENT_BALANCE" Text="Current Balance" Width="150" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:NumberColumn ID="NumberColumn11" runat="server" DataIndex="LIMITCREDITCARD" Text="CC Limit" Width="150" Format="#,###.00" Align="Right"></ext:NumberColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

            <ext:GridPanel ID="gp_Loan" runat="server" Title="Loan Information" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="150">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store8" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="Model14" IDProperty="ACCOUNT_NO">
                                <Fields>
                                    <ext:ModelField Name="LOAN_NUMBER" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="LOAN_TYPE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="SEGMENT_LOAN" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="TENOR" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="LOAN_AMOUNT" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="COLLATERAL_TYPE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="BRANCH_NAME" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn15" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column122" runat="server" DataIndex="LOAN_NUMBER" Text="Loan Number" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column120" runat="server" DataIndex="LOAN_TYPE" Text="Loan Type" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column121" runat="server" DataIndex="SEGMENT_LOAN" Text="Sector" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column119" runat="server" DataIndex="TENOR" Text="Tenor" MinWidth="100"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn12" runat="server" DataIndex="LOAN_AMOUNT" Text="Loan Amount" Width="150" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:Column ID="Column123" runat="server" DataIndex="COLLATERAL_TYPE" Text="Collateral Type" MinWidth="100"></ext:Column>
                        <ext:Column ID="Column4" runat="server" DataIndex="BRANCH_NAME" Text="Branch Name" MinWidth="150"></ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar15" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

            <ext:GridPanel ID="gp_OtherCase" runat="server" Title="Same Case History" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store4" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Other"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                        <Model>
                            <ext:Model runat="server" ID="Model6" IDProperty="PK_CaseManagement_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_CaseManagement_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Alert_Type" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Case_Description" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CaseStatus" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ProcessDate" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column56" runat="server" DataIndex="PK_CaseManagement_ID" Text="Case ID" Width="70"></ext:Column>
                        <ext:Column ID="Column33" runat="server" DataIndex="Alert_Type" Text="Alert Type" Width="120"></ext:Column>
                        <ext:Column ID="Column34" runat="server" DataIndex="Case_Description" Text="Case Description" MinWidth="400"></ext:Column>
                        <ext:Column ID="Column35" runat="server" DataIndex="CaseStatus" Text="Status" Width="150"></ext:Column>
                        <ext:DateColumn ID="DateColumn5" runat="server" DataIndex="ProcessDate" Text="Alert Date" Width="110" Format="dd-MMM-yyyy"></ext:DateColumn>

                        <ext:CommandColumn ID="cc_OtherCase" runat="server" Text="Action" Width="70">
                            <Commands>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                            </Commands>

                            <DirectEvents>
                                <Command OnEvent="gc_OtherCase">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_CaseManagement_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader3" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="true" />
                </BottomBar>
            </ext:GridPanel>

            <ext:GridPanel ID="gp_CaseAlert_Typology_Transaction" runat="server" Title="Case Alert Typology Transaction" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <TopBar>
                    <ext:Toolbar runat="server">
                        <Items>
                            <%--<ext:ComboBox runat="server" ID="cboExport_CaseAlert_Typology" Editable="false" FieldLabel="Export " labelStyle="width:80px" AnchorHorizontal="50%" >
                                <Items>
                                    <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                                    <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                                </Items>
                            </ext:ComboBox>--%>
                            <%--<ext:Button runat="server" ID="BtnExport_CaseAlert_Typology" Text="Export Current Page" MarginSpec="0 0 0 10"  >
                               <DirectEvents>
                                    <Click OnEvent="Export_CaseAlert_Typology" IsUpload="true">
                                        <ExtraParams>
                                            <ext:Parameter Name="currentPage" Value="App.gp_CaseAlert_Typology_Transaction.getStore().currentPage" Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>--%>
                            <ext:Button runat="server" ID="BtnExportAll_CaseAlert_Typology" Text="Export to Excel" Icon="PageExcel">
                                <DirectEvents>
                                    <Click OnEvent="ExportAll_CaseAlert_Typology" IsUpload="true"/>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="store1" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Typology_Transaction"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                        <Model>
                            <ext:Model runat="server" ID="Model1" IDProperty="PK_OneFCC_CaseManagement_Typology_Transaction_ID">
                                <Fields>
                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column73" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column10" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                        <ext:Column ID="Column77" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column78" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                        <ext:Column ID="Column79" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn1" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:NumberColumn ID="NumberColumn6" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:Column ID="Column8" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column9" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                        <ext:Column ID="Column12" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                        <ext:Column ID="Column80" runat="server" DataIndex="ACCOUNT_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column81" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column13" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column82" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column6" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader1" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="true" />
                </BottomBar>
            </ext:GridPanel>

            <ext:Panel runat="server" ID="pnl_CaseAlert_Outlier_Transaction" Title="Case Alert Outlier Transaction" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="ColumnLayout" BodyPadding="10">
                <Items>
                    <ext:FieldSet runat="server" ID="fs_CaseAlert_Outlier_Transaction" ColumnWidth="1" Title="Financial Statistics" Padding="5" Layout="ColumnLayout">
                        <Items>
                            <ext:Container runat="server" ColumnWidth="0.5" PaddingSpec="0 10">
                                <Items>
                                    <ext:DisplayField runat="server" ID="txt_MeanDebit" AnchorHorizontal="100%" FieldLabel="Mean Debit (IDR)" LabelWidth="100"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_MeanCredit" AnchorHorizontal="100%" FieldLabel="Mean Credit (IDR)" LabelWidth="100"></ext:DisplayField>
                                </Items>
                            </ext:Container>
                            <ext:Container runat="server" ColumnWidth="0.5" PaddingSpec="0 10">
                                <Items>
                                    <ext:DisplayField runat="server" ID="txt_ModusDebit" AnchorHorizontal="100%" FieldLabel="Modus Debit (IDR)" LabelWidth="100"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_ModusCredit" AnchorHorizontal="100%" FieldLabel="Modus Credit (IDR)" LabelWidth="100"></ext:DisplayField>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:FieldSet>

                    <ext:Container runat="server" ColumnWidth="1">
                        <Items>
                            <ext:GridPanel ID="gp_CaseAlert_Outlier_Transaction" runat="server" Title="" MarginSpec="0 0 10 0" Border="true" MinHeight="200">
                                <View>
                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                </View>
                                <TopBar>
                                    <ext:Toolbar runat="server">
                                        <Items>
                                            <ext:Button runat="server" ID="btnExportAll_CaseAlert_Outlier" Text="Export to Excel" Icon="PageExcel">
                                                <DirectEvents>
                                                    <Click OnEvent="ExportAll_CaseAlert_Outlier" IsUpload="true"/>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="store3" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Outlier_Transaction"
                                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                                        <Model>
                                            <ext:Model runat="server" ID="Model5" IDProperty="PK_OneFCC_CaseManagement_Outlier_Transaction_ID">
                                                <Fields>
                                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                                    <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                                    <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Sorters></Sorters>
                                        <Proxy>
                                            <ext:PageProxy />
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No"></ext:RowNumbererColumn>
                                        <ext:DateColumn ID="DateColumn4" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                                        <ext:Column ID="Column28" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column29" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                                        <ext:Column ID="Column30" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                                        <ext:Column ID="Column31" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                                        <ext:Column ID="Column32" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                                        <ext:NumberColumn ID="NumberColumn3" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                        <ext:NumberColumn ID="NumberColumn7" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                        <ext:Column ID="Column83" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                                        <ext:Column ID="Column84" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column85" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                                        <ext:Column ID="Column86" runat="server" DataIndex="ACCOUNT_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column87" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column88" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column89" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                                        <ext:Column ID="Column111" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <Plugins>
                                    <ext:FilterHeader ID="FilterHeader2" runat="server" Remote="true"></ext:FilterHeader>
                                </Plugins>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Panel>
            
            <ext:Panel ID="panel_Activity" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="0 0 10 0" BodyPadding="10" Collapsible="true" Title="Activity Data" Border="true" Hidden="true">
                <Content>
                    <ext:GridPanel ID="GridPanel_Activity" runat="server" AutoScroll="true" MarginSpec="0 0 10 0" MinHeight="250">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <%--<ext:Store ID="StoreActivity" runat="server" IsPagingStore="true" PageSize="10">--%>
                            <ext:Store ID="StoreActivity" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model15" IDProperty="PK_OneFCC_CaseManagement_Typology_NonTransaction_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_OneFCC_CaseManagement_Typology_NonTransaction_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Date_Activity" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Activity_Description" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn16" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumnActivity" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandActivity_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_OneFCC_CaseManagement_Typology_NonTransaction_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="ColumnActivityAccountNo" runat="server" DataIndex="Account_NO" Text="Account No" MinWidth="80" Flex="1"></ext:Column>
                                <ext:DateColumn ID="DateColumn12" runat="server" DataIndex="Date_Activity" Text="Activity Date" MinWidth="110" Format="dd-MMM-yyyy" Flex="1">
                                    <Items>
                                        <ext:DateField ID="DatePickerColumnDate_Activity" runat="server" Format="dd-MMM-yyyy">
                                            <Plugins>
                                                <ext:ClearButton ID="DateColumn12_ClearButton1">
                                                </ext:ClearButton>
                                            </Plugins>
                                        </ext:DateField>
                                    </Items>
                                    <Filter>
                                        <ext:DateFilter>
                                        </ext:DateFilter>
                                    </Filter>
                                </ext:DateColumn>
                                <ext:Column ID="Column117" runat="server" DataIndex="Activity_Description" Text="Activity Description" MinWidth="80" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader ID="FilterHeader6" runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar16" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Content>
            </ext:Panel>

            <ext:GridPanel ID="gp_SearchTransaction" runat="server" Title="Search Transaction" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="250">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <TopBar>
                    <ext:Toolbar runat="server">
                        <Items>
                            <ext:DateField runat="server" ID="txt_SearchTransaction_From" AnchorHorizontal="100%" FieldLabel="From Date" LabelWidth="80" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:DateField runat="server" ID="txt_SearchTransaction_To" AnchorHorizontal="100%" FieldLabel="To" MarginSpec="0 20" LabelWidth="50" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Button runat="server" ID="btn_Search_Transaction" Text="Search" Icon="Magnifier">
                                <DirectEvents>
                                    <Click OnEvent="btn_Search_Transaction_Click">
                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                            <ext:Button runat="server" ID="btnExportAll_SearchTransaction" Text="Export to Excel" MarginSpec="0 30" Icon="PageExcel">
                                <DirectEvents>
                                    <Click OnEvent="ExportAll_SearchTransaction" IsUpload="true"/>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="store_SearchTransaction" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_Search_Transaction"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                        <Model>
                            <ext:Model runat="server" ID="Model2" IDProperty="NO_ID">
                                <Fields>
                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column11" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column14" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                        <ext:Column ID="Column15" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column16" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                        <ext:Column ID="Column17" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn2" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:NumberColumn ID="NumberColumn8" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:Column ID="Column90" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column91" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                        <ext:Column ID="Column92" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                        <ext:Column ID="Column93" runat="server" DataIndex="ACCOUNT_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column94" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column95" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column96" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column114" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader ID="filter_SearchTransaction" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

            <ext:GridPanel ID="gp_RFI" runat="server" Title="Request For Information (RFI)" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <TopBar>
                    <ext:Toolbar runat="server" ID="tb_RFI">
                        <Items>
                            <ext:Button runat="server" ID="btn_Send_New_RFI" Text="Send New RFI" Icon="EmailAdd">
                                <DirectEvents>
                                    <Click OnEvent="btn_Send_New_RFI_Click">
                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="store_RFI" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="Model4" IDProperty="PK_OneFCC_CaseManagement_RFI_ID">
                                <Fields>
                                    <ext:ModelField Name="EmailTo" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="EmailCc" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Subject" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="EmailStatusName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedDate" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="AttachmentName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Notes" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column21" runat="server" DataIndex="EmailTo" Text="Email To" MinWidth="150" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column22" runat="server" DataIndex="EmailCc" Text="CC" MinWidth="150" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column25" runat="server" DataIndex="Subject" Text="Subject" Width="200" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column24" runat="server" DataIndex="EmailStatusName" Text="Status" Width="100"></ext:Column>
                        <ext:Column ID="Column27" runat="server" DataIndex="CreatedBy" Text="Created By" Width="100"></ext:Column>
                        <ext:Column ID="Column26" runat="server" DataIndex="AttachmentName" Text="Attachment" MinWidth="200">
                            <Commands>
                                <ext:ImageCommand Icon="DiskDownload" CommandName="Download" ToolTip-Text="Download Report File"></ext:ImageCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_RFI">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_OneFCC_CaseManagement_RFI_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:Column>
                        <ext:DateColumn ID="DateColumn3" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column116" runat="server" DataIndex="Notes" Text="Notes" MinWidth="150" CellWrap="true"></ext:Column>

                        <ext:CommandColumn ID="cc_RFI" runat="server" Text="Action" Width="70">
                            <Commands>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                            </Commands>

                            <DirectEvents>
                                <Command OnEvent="gc_RFI">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_OneFCC_CaseManagement_RFI_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

            <ext:GridPanel ID="gp_WorkflowHistory" runat="server" Title="Workflow History" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store2" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_WorkflowHistory"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                        <Model>
                            <ext:Model runat="server" ID="Model3" IDProperty="PK_CaseManagement_WorkflowHistory_ID">
                                <Fields>
                                    <ext:ModelField Name="Workflow_Step" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Proposed_Action" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Analysis_Result" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedDate" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column18" runat="server" DataIndex="Workflow_Step" Text="Step" MinWidth="100" Align="Center"></ext:Column>
                        <ext:Column ID="Column23" runat="server" DataIndex="CreatedBy" Text="Created By" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column19" runat="server" DataIndex="Proposed_Action" Text="Proposed Status" Width="150"></ext:Column>
                        <ext:Column ID="Column20" runat="server" DataIndex="Analysis_Result" Text="Analysis Result" Width="400" CellWrap="true"></ext:Column>
                        <ext:DateColumn ID="DateColumn_CreateDate" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

            <ext:Panel runat="server" ID="pnl_AnalysisResult" Title="Analysis Result" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="10">
                <Content>
                    <NDS:NDSDropDownField ID="cmb_FK_Proposed_Status_ID" LabelWidth="150" ValueField="PK_OneFCC_CaseManagement_ProposedAction_ID" DisplayField="Proposed_Action" runat="server" StringField="PK_OneFCC_CaseManagement_ProposedAction_ID, Proposed_Action" StringTable="OneFCC_CaseManagement_ProposedAction" Label="Proposed Status" AnchorHorizontal="50%" AllowBlank="false" />
                    <ext:TextArea ID="txt_Analysis_Result" AllowBlank="false" LabelWidth="150" runat="server" FieldLabel="Analysis Result" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true" Height="150"></ext:TextArea>
                </Content>
            </ext:Panel>

        </Items>

        <Buttons>
            <ext:Button ID="btn_Save" runat="server" Icon="Disk" Text="Submit">
                <DirectEvents>
                    <Click OnEvent="btn_Save_Click">
                        <EventMask ShowMask="true" Msg="Submitting Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_ReOpen" runat="server" Icon="DoorOpen" Text="Re-Open Alert">
                <DirectEvents>
                    <Click OnEvent="btn_ReOpen_Click">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="Accept">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    
    <ext:Window ID="WindowActivity" runat="server" Modal="true" Hidden="true" BodyStyle="padding:0px" AutoScroll="true" ButtonAlign="Center" Layout="AnchorLayout" Title="Activity">
        <Items>
            <ext:FormPanel runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                <Items>
                    <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="100%" Padding="10">
                        <Content>
                            <ext:DisplayField runat="server" ID="nDSDropDownField_AccountNo" Label="Account No" AnchorHorizontal="100%" LabelWidth ="200" IsHidden="true"/>
                            <ext:DisplayField runat="server" ID="activity_Date" AnchorHorizontal="100%" FieldLabel="Date Activity" LabelWidth ="200" ></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="activity_Desc" FieldLabel="Activity Description" LabelWidth ="200" AnchorHorizontal="100%" />
                        </Content>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{window_Import_Party}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_Activity_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="Btn_Activity_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <%-- ================== SEND RFI WINDOW ========================== --%>
    <ext:Window ID="Window_SendRFI" Layout="AnchorLayout" Title="Send Email Request For Information (RFI)" runat="server" Modal="true" Hidden="true" Maximizable="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
		   <ext:Panel ID="pnl_SendRFI" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                <Content>
                    <ext:TextField ID="txt_EmailTo" runat="server" AnchorHorizontal="100%" AllowBlank="false" FieldLabel="To" MaxLength="1000" EnforceMaxLength="true" EmptyText="Email address(es) separated with ; eg. user1@test.com;user2@test.com" />
                    <ext:TextField ID="txt_EmailCC" runat="server" AnchorHorizontal="100%" AllowBlank="true" FieldLabel="CC" MaxLength="1000" EnforceMaxLength="true" />
                    <ext:TextField ID="txt_EmailBCC" runat="server" AnchorHorizontal="100%" AllowBlank="true" FieldLabel="BCC" MaxLength="1000" EnforceMaxLength="true" />
                    <ext:TextField ID="txt_EmailSubject" runat="server" AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Subject" MaxLength="1000" EnforceMaxLength="true" />
                    <ext:HtmlEditor ID="txt_EmailContent" runat="server" FieldLabel="Content" AllowBlank="false" Height="150" AutoScroll="true" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true" FieldStyle="background-color:#ffe4c4 !important;" ></ext:HtmlEditor>
                    <ext:FileUploadField ID="txt_EmailAttachment" runat="server" FieldLabel="Attachment" AnchorHorizontal="100%">
                        <RightButtons>
                            <ext:Button ID="btnClear" runat="server" Text="" Icon="Erase" ClientIDMode="Static">
                                <Listeners>
                                    <Click Handler="#{txt_EmailAttachment}.reset();#{LblFileReport}.setValue(#{txt_EmailAttachment}.value);"></Click>
                                </Listeners>
                            </ext:Button>
                        </RightButtons>
                    </ext:FileUploadField>

                    <ext:Container runat="server" ID="ctr_RFI_Attachment" AnchorHorizontal="100%" Layout="ColumnLayout">
                        <Items>
                            <ext:DisplayField ID="df_EmailAttachment" runat="server" ColumnWidth="0.75" AllowBlank="false" FieldLabel="Attachment" MaxLength="1000" EnforceMaxLength="true" Icon="DiskDownload">
                            </ext:DisplayField>
                            <ext:Button runat="server" ID="btn_DownloadEmailAttachment" Icon="DiskDownload" ColumnWidth="0.25" Text="Download Attachment">
                                <DirectEvents>
                                    <Click OnEvent="btn_DownloadEmailAttachment_Click"></Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Container>

                    <%-- 29-Mar-2022 Adi : Tambah RFI Notes --%>
                    <ext:TextArea ID="txt_EmailNotes" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Notes" AnchorHorizontal="100%" MaxLength="500" EnforceMaxLength="true" Height="100" MarginSpec="20 0"></ext:TextArea>

                </Content>
            </ext:Panel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.8, height: size.height * 0.9});" />
            <Resize Handler="#{Window_SendRFI}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_SendRFI_Save" runat="server" Icon="EmailGo" Text="Send">
                <DirectEvents>
                    <Click OnEvent="btn_SendRFI_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_SendRFI_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_SendRFI_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%-- ================== END OF SEND RFI WINDOW ========================== --%>


    <%-- ================== WINDOW OTHER CASE RELATED TO CUSTOMER ========================== --%>
    <ext:Window ID="window_OtherCase" Layout="AnchorLayout" Title="Other Case Related to Customer" runat="server" Hidden="true" Maximizable="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:Panel runat="server" ID="Panel2" Title="Case Alert Information" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="10">
                <Items>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_PK_CaseManagement_ID" AnchorHorizontal="100%" FieldLabel="Case Management ID" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_UniqueCMID" AnchorHorizontal="100%" FieldLabel="Unique CM ID" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_Alert_Type" AnchorHorizontal="100%" FieldLabel="Alert Type" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_Case_Description" AnchorHorizontal="100%" FieldLabel="Case Description" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_FK_Case_Status_ID" AnchorHorizontal="100%" FieldLabel="Case Status" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_ProcessDate" AnchorHorizontal="100%" FieldLabel="Alert Date" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_Workflow_Step" AnchorHorizontal="100%" FieldLabel="Workflow Step" LabelWidth="150" Hidden="true"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_PIC" AnchorHorizontal="100%" FieldLabel="PIC" LabelWidth="150" Hidden="true"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_checkbox_IsTransaction" AnchorHorizontal="100%" FieldLabel="Is Transaction ?" LabelWidth="150"></ext:DisplayField>
                </Items>
            </ext:Panel>

            <ext:GridPanel ID="gp_CaseAlert_Typology_Transaction_OtherCase" runat="server" Title="Case Alert Typology Transaction" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store5" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Typology_Transaction_OtherCase"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                        <Model>
                            <ext:Model runat="server" ID="Model8" IDProperty="PK_OneFCC_CaseManagement_Typology_Transaction_ID">
                                <Fields>
                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:DateColumn ID="DateColumn7" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column42" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column43" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                        <ext:Column ID="Column44" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column45" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                        <ext:Column ID="Column46" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn4" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:NumberColumn ID="NumberColumn10" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:Column ID="Column104" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column105" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                        <ext:Column ID="Column106" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                        <ext:Column ID="Column107" runat="server" DataIndex="ACCOUNT_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column108" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column109" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column110" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column113" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader4" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="true" />
                </BottomBar>
            </ext:GridPanel>

            <ext:Panel runat="server" ID="pnl_CaseAlert_Outlier_Transaction_OtherCase" Title="Case Alert Outlier Transaction" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="ColumnLayout" BodyPadding="10">
                <Items>
                    <ext:FieldSet runat="server" ID="fs_CaseAlert_Outlier_Transaction_OtherCase" ColumnWidth="1" Title="Financial Statistics" Padding="5" Layout="ColumnLayout">
                        <Items>
                            <ext:Container runat="server" ColumnWidth="0.5" PaddingSpec="0 10" Layout="AnchorLayout"> 
                                <Items>
                                    <ext:DisplayField runat="server" ID="txt_MeanDebit_OtherCase" AnchorHorizontal="100%" FieldLabel="Mean Debit (IDR)" LabelWidth="130"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_MeanCredit_OtherCase" AnchorHorizontal="100%" FieldLabel="Mean Credit (IDR)" LabelWidth="130"></ext:DisplayField>
                                </Items>
                            </ext:Container>
                            <ext:Container runat="server" ColumnWidth="0.5" PaddingSpec="0 10" Layout="AnchorLayout">
                                <Items>
                                    <ext:DisplayField runat="server" ID="txt_ModusDebit_OtherCase" AnchorHorizontal="100%" FieldLabel="Modus Debit (IDR)" LabelWidth="130"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_ModusCredit_OtherCase" AnchorHorizontal="100%" FieldLabel="Modus Credit (IDR)" LabelWidth="130"></ext:DisplayField>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:FieldSet>

                    <ext:Container runat="server" ColumnWidth="1">
                        <Items>
                            <ext:GridPanel ID="gp_CaseAlert_Outlier_Transaction_OtherCase" runat="server" Title="" MarginSpec="0 0 10 0" Border="true" MinHeight="200">
                                <View>
                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                </View>
                                <Store>
                                    <ext:Store ID="store6" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Outlier_Transaction_OtherCase"
                                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                                        <Model>
                                            <ext:Model runat="server" ID="Model9" IDProperty="PK_OneFCC_CaseManagement_Outlier_Transaction_ID">
                                                <Fields>
                                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                                    <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                                    <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Sorters></Sorters>
                                        <Proxy>
                                            <ext:PageProxy />
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server" Text="No"></ext:RowNumbererColumn>
                                        <ext:DateColumn ID="DateColumn8" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                                        <ext:Column ID="Column47" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column48" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                                        <ext:Column ID="Column49" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                                        <ext:Column ID="Column50" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                                        <ext:Column ID="Column51" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                                        <ext:NumberColumn ID="NumberColumn5" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                        <ext:NumberColumn ID="NumberColumn9" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                        <ext:Column ID="Column97" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                                        <ext:Column ID="Column98" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column99" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                                        <ext:Column ID="Column100" runat="server" DataIndex="ACCOUNT_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column101" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column102" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column103" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                                        <ext:Column ID="Column112" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <Plugins>
                                    <ext:FilterHeader ID="FilterHeader5" runat="server" Remote="true"></ext:FilterHeader>
                                </Plugins>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Panel>
            
            <ext:Panel ID="panel_Activity_OtherCase" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="0 0 10 0" BodyPadding="10" Collapsible="true" Title="Activity Data" Border="true" Hidden="true">
                <Content>
                    <ext:GridPanel ID="GridPanel_Activity_OtherCase" runat="server" AutoScroll="true" MarginSpec="0 0 10 0" MinHeight="250">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <%--<ext:Store ID="StoreActivity" runat="server" IsPagingStore="true" PageSize="10">--%>
                            <ext:Store ID="StoreActivity_OtherCase" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model16" IDProperty="PK_OneFCC_CaseManagement_Typology_NonTransaction_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_OneFCC_CaseManagement_Typology_NonTransaction_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Date_Activity" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Activity_Description" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn17" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="ColumnActivityAccountNo_OtherChase" runat="server" DataIndex="Account_NO" Text="Account No" MinWidth="80" Flex="1"></ext:Column>
                                <ext:DateColumn ID="DateColumn13" runat="server" DataIndex="Date_Activity" Text="Activity Date" MinWidth="110" Format="dd-MMM-yyyy" Flex="1">
                                    <Items>
                                        <ext:DateField ID="DateField1" runat="server" Format="dd-MMM-yyyy">
                                            <Plugins>
                                                <ext:ClearButton ID="Column16_ClearButton1">
                                                </ext:ClearButton>
                                            </Plugins>
                                        </ext:DateField>
                                    </Items>
                                    <Filter>
                                        <ext:DateFilter>
                                        </ext:DateFilter>
                                    </Filter>
                                </ext:DateColumn>
                                <ext:Column ID="Column124" runat="server" DataIndex="Activity_Description" Text="Activity Description" MinWidth="80" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader ID="FilterHeader7" runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar17" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Content>
            </ext:Panel>

            <ext:GridPanel ID="gp_RFI_OtherCase" runat="server" Title="Request For Information (RFI)" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store_RFI_OtherCase" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="Model7" IDProperty="PK_OneFCC_CaseManagement_RFI_ID">
                                <Fields>
                                    <ext:ModelField Name="EmailTo" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="EmailCc" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Subject" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="EmailStatusName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedDate" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="AttachmentName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Notes" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column36" runat="server" DataIndex="EmailTo" Text="Email To" MinWidth="150" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column37" runat="server" DataIndex="EmailCc" Text="CC" MinWidth="150" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column38" runat="server" DataIndex="Subject" Text="Subject" Width="200" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column39" runat="server" DataIndex="EmailStatusName" Text="Status" Width="100"></ext:Column>
                        <ext:Column ID="Column40" runat="server" DataIndex="CreatedBy" Text="Created By" Width="100"></ext:Column>
                        <ext:Column ID="Column41" runat="server" DataIndex="AttachmentName" Text="Attachment" MinWidth="200">
                            <Commands>
                                <ext:ImageCommand Icon="DiskDownload" CommandName="Download" ToolTip-Text="Download Report File"></ext:ImageCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_RFI_OtherCase">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_OneFCC_CaseManagement_RFI_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:Column>                        
                        <ext:DateColumn ID="DateColumn6" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column115" runat="server" DataIndex="Notes" Text="Notes" MinWidth="150" CellWrap="true"></ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

            <ext:GridPanel ID="gp_WorkflowHistory_OtherCase" runat="server" Title="Workflow History" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store7" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_WorkflowHistory_OtherCase"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                        <Model>
                            <ext:Model runat="server" ID="Model10" IDProperty="PK_CaseManagement_WorkflowHistory_ID">
                                <Fields>
                                    <ext:ModelField Name="Workflow_Step" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Proposed_Action" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Analysis_Result" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedDate" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column52" runat="server" DataIndex="Workflow_Step" Text="Step" Width="50" Align="Center"></ext:Column>
                        <ext:Column ID="Column53" runat="server" DataIndex="CreatedBy" Text="Created By" Width="150"></ext:Column>
                        <ext:Column ID="Column54" runat="server" DataIndex="Proposed_Action" Text="Proposed Status" Width="150"></ext:Column>
                        <ext:Column ID="Column55" runat="server" DataIndex="Analysis_Result" Text="Analysis Result" Width="300" CellWrap="true"></ext:Column>
                        <ext:DateColumn ID="DateColumn9" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.9, height: size.height * 0.9});" />
            <Resize Handler="#{window_OtherCase}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_OtherCase_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_OtherCase_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%-- ================== END OF WINDOW OTHER CASE RELATED TO CUSTOMER ========================== --%>


    <%-- ================== CUSTOMER DETAIL WINDOW ========================== --%>
    <ext:Window ID="window_CustomerDetail" Layout="AnchorLayout" Title="Customer Detail Information" runat="server" Modal="true" Hidden="true" Maximizable="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:FormPanel ID="FormPanel1" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" AutoScroll="true">
                <Items>
                    <ext:Panel ID="Panel1" runat="server" Layout="ColumnLayout" AnchorHorizontal="100%">
                        <Content>
                            <ext:Container runat="server" ColumnWidth="1">
                                <Items>
                                    <ext:DisplayField ID="txt_CustomerDetail_CIF" runat="server" AnchorHorizontal="100%" FieldLabel="CIF" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Name" runat="server" AnchorHorizontal="100%" FieldLabel="Customer Name" LabelWidth="120" />
                                </Items>
                            </ext:Container>
                            <ext:FieldSet runat="server" ColumnWidth="0.495" Border="true" Padding="10">
                                <Items>
                                    <%--<ext:DisplayField ID="txt_CustomerDetail_TypeCode" runat="server" AnchorHorizontal="100%" FieldLabel="Type Code" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_POB" runat="server" AnchorHorizontal="100%" FieldLabel="Place of Birth" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_DOB" runat="server" AnchorHorizontal="100%" FieldLabel="Date of Birth" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Nationality" runat="server" AnchorHorizontal="100%" FieldLabel="Nationality" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Occupation" runat="server" AnchorHorizontal="100%" FieldLabel="Occupation" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Employer" runat="server" AnchorHorizontal="100%" FieldLabel="Employer" LabelWidth="100" />--%>

                                    <ext:DisplayField ID="txt_CustomerDetail_DOB" runat="server" AnchorHorizontal="100%" FieldLabel="Date of Birth" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_POB" runat="server" AnchorHorizontal="100%" FieldLabel="Place of Birth" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_MotherName" runat="server" AnchorHorizontal="100%" FieldLabel="Mother Name" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Gender" runat="server" AnchorHorizontal="100%" FieldLabel="Gender" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_MaritalStatus" runat="server" AnchorHorizontal="100%" FieldLabel="Marital Status" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Nationality" runat="server" AnchorHorizontal="100%" FieldLabel="Citizenship" LabelWidth="100" />
                                        <ext:DisplayField ID="txt_CustomerDetail_Occupation" runat="server" AnchorHorizontal="100%" FieldLabel="Occupation" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Employer" runat="server" AnchorHorizontal="100%" FieldLabel="Work Place" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_TypeCode" runat="server" AnchorHorizontal="100%" FieldLabel="Customer Type" LabelWidth="100" />
                                    <ext:DisplayField ID="txt_CustomerDetail_SubCustomerType" runat="server" AnchorHorizontal="100%" FieldLabel="Customer Sub Type" LabelWidth="100" />
                                </Items>
                            </ext:FieldSet>
                            <ext:FieldSet runat="server" ColumnWidth="0.495" Border="true" Padding="10">
                                <Items>
                                    <%--<ext:DisplayField ID="txt_CustomerDetail_LegalForm" runat="server" AnchorHorizontal="100%" FieldLabel="Legal Form" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Industry" runat="server" AnchorHorizontal="100%" FieldLabel="Industry" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_NPWP" runat="server" AnchorHorizontal="100%" FieldLabel="Tax Number" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Income" runat="server" AnchorHorizontal="100%" FieldLabel="Income Level" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_PurposeOfFund" runat="server" AnchorHorizontal="100%" FieldLabel="Purpose of Fund" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_SourceOfFund" runat="server" AnchorHorizontal="100%" FieldLabel="Source of Fund" LabelWidth="120" />--%>
                                    <ext:DisplayField ID="txt_CustomerDetail_LegalForm" runat="server" AnchorHorizontal="100%" FieldLabel="Business Type" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Industry" runat="server" AnchorHorizontal="100%" FieldLabel="Industry" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_OpeningDate" runat="server" AnchorHorizontal="100%" FieldLabel="Opening Date" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_CreationEntity" runat="server" AnchorHorizontal="100%" FieldLabel="Creation Entity" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_CreationBranch" runat="server" AnchorHorizontal="100%" FieldLabel="Creation Branch" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_SalesOfficer" runat="server" AnchorHorizontal="100%" FieldLabel="Sales Officer" LabelWidth="120" />
                                        <ext:DisplayField ID="txt_CustomerDetail_NPWP" runat="server" AnchorHorizontal="100%" FieldLabel="Tax Number" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_SourceOfFund" runat="server" AnchorHorizontal="100%" FieldLabel="Source of Fund" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_PurposeOfFund" runat="server" AnchorHorizontal="100%" FieldLabel="Purpose of Fund" LabelWidth="120" />
                                    <ext:DisplayField ID="txt_CustomerDetail_Income" runat="server" AnchorHorizontal="100%" FieldLabel="Income Level" LabelWidth="120" />
                                </Items>
                            </ext:FieldSet>

                            <%-- Customer Address Information --%>
                            <ext:GridPanel ID="gridAddressInfo" runat="server" Title="Address" AutoScroll="true" Border="true" PaddingSpec="10 0 0 0" ColumnWidth="1">
                                <Store>
                                    <ext:Store ID="Store_gridAddressInfo" runat="server" IsPagingStore="true" PageSize="4">
                                        <Model>
                                            <ext:Model ID="Model11" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="AML_ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CUSTOMER_ADDRESS" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="FK_AML_COUNTRY_CODE" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="KODEPOS" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="KECAMATAN" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="KOTAKABUPATEN" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column57" runat="server" DataIndex="AML_ADDRESS_TYPE_NAME" Text="Type" MinWidth="50"></ext:Column>
                                        <ext:Column ID="Column58" runat="server" DataIndex="CUSTOMER_ADDRESS" Text="Address" MinWidth="250" CellWrap="true" Flex="1"></ext:Column>
                                        <ext:Column ID="Column59" runat="server" DataIndex="KECAMATAN" Text="Town" MinWidth="120" CellWrap="true" Flex="1"></ext:Column>
                                        <ext:Column ID="Column60" runat="server" DataIndex="KOTAKABUPATEN" Text="City" MinWidth="120" CellWrap="true" Flex="1"></ext:Column>
                                        <ext:Column ID="Column61" runat="server" DataIndex="FK_AML_COUNTRY_CODE" Text="Country Code" MinWidth="100" Hidden="true"></ext:Column>
                                        <ext:Column ID="Column62" runat="server" DataIndex="COUNTRY_NAME" Text="Country" MinWidth="100"></ext:Column>
                                        <ext:Column ID="Column63" runat="server" DataIndex="KODEPOS" Text="Postal Code" MinWidth="100"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%-- End of Customer Address Information --%>

                            <%-- Customer Phone Information --%>
                            <ext:GridPanel ID="gridPhoneInfo" runat="server" Title="Phone" AutoScroll="true" Border="true" PaddingSpec="10 0 0 0" ColumnWidth="1">
                                <Store>
                                    <ext:Store ID="Store_PhoneInfo" runat="server" IsPagingStore="true" PageSize="4">
                                        <Model>
                                            <ext:Model ID="Model13" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="AML_CONTACT_TYPE_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="FK_AML_COMMUNICATION_TYPE_CODE" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="AREA_CODE" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CUSTOMER_CONTACT_NUMBER" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="EXTENTION" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="NOTES" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn14" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column69" runat="server" DataIndex="AML_CONTACT_TYPE_NAME" Text="Type" Width="100"></ext:Column>
                                        <ext:Column ID="Column76" runat="server" DataIndex="FK_AML_COMMUNICATION_TYPE_CODE" Text="Comm.Type" Width="100"></ext:Column>
                                        <ext:Column ID="Column70" runat="server" DataIndex="AREA_CODE" Text="Area Code" Width="100"></ext:Column>
                                        <ext:Column ID="Column71" runat="server" DataIndex="CUSTOMER_CONTACT_NUMBER" Text="Number" Width="250"></ext:Column>
                                        <ext:Column ID="Column74" runat="server" DataIndex="EXTENTION" Text="Ext." Width="100"></ext:Column>
                                        <ext:Column ID="Column75" runat="server" DataIndex="NOTES" Text="Notes" MinWidth="100" Flex="1"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%-- End of Customer Phone Information --%>

                            <%-- Customer Identity Information --%>
                            <ext:GridPanel ID="gridIdentityInfo" runat="server" Title="Identification" AutoScroll="true" Border="true" PaddingSpec="10 0 0 0" ColumnWidth="1">
                                <Store>
                                    <ext:Store ID="Store_gridIdentityInfo" runat="server" IsPagingStore="true" PageSize="4">
                                        <Model>
                                            <ext:Model ID="Model12" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="CIFNO" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CUSTOMER_IDENTITY_NUMBER" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="FK_AML_IDENTITY_TYPE_CODE" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="DATE_OF_ISSUE" Type="Date" ></ext:ModelField>
                                                    <ext:ModelField Name="DATE_OF_EXPIRED" Type="Date"></ext:ModelField>
                                                    <ext:ModelField Name="ISSUE_By" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="fk_aml_countryissue_code" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="NOTES" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="IDENTITY_TYPE_NAME" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn13" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column64" runat="server" DataIndex="FK_AML_IDENTITY_TYPE_CODE" Text="Type" MinWidth="50"></ext:Column>
                                        <ext:Column ID="Column65" runat="server" DataIndex="CUSTOMER_IDENTITY_NUMBER" Text="Identification Number" MinWidth="150" CellWrap="true" Flex="1"></ext:Column>
                                        <ext:DateColumn ID="DateColumn10" runat="server" DataIndex="DATE_OF_ISSUE" Text="Issue Date" Format="dd-MMM-yyyy" Width="120" CellWrap="true"></ext:DateColumn>
                                        <ext:DateColumn ID="DateColumn11" runat="server" DataIndex="DATE_OF_EXPIRED" Text="Expired Date" Format="dd-MMM-yyyy" Width="120" CellWrap="true"></ext:DateColumn>
                                        <ext:Column ID="Column66" runat="server" DataIndex="ISSUE_By" Text="Issue By" MinWidth="100" Hidden="true"></ext:Column>
                                        <ext:Column ID="Column67" runat="server" DataIndex="COUNTRY_NAME" Text="Issued Country" Width="150"></ext:Column>
                                        <ext:Column ID="Column68" runat="server" DataIndex="NOTES" Text="Comments" MinWidth="100"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar13" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%-- End of Customer Identity Information --%>
                        </Content>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.9, height: size.height * 0.9});" />
            <Resize Handler="#{window_CustomerDetail}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_CustomerDetail_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_CustomerDetail_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%-- ================== END OF CUSTOMER DETAIL WINDOW ========================== --%>


    <%-- ================== WINDOW RE-OPEN ========================== --%>
    <ext:Window ID="window_ReOpen" Layout="AnchorLayout" Title="Re-Open Alert" runat="server" Modal="true" Hidden="true" Maximizable="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
		   <ext:Panel ID="Panel3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                <Content>
                    <ext:TextArea ID="txt_ReOpenReason" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Reason" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true" Height="120"></ext:TextArea>
                </Content>
            </ext:Panel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.6, height: size.height * 0.6});" />
            <Resize Handler="#{window_ReOpen}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_ReOpen_Save" runat="server" Icon="Accept" Text="OK">
                <DirectEvents>
                    <Click OnEvent="btn_ReOpen_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_ReOpen_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_ReOpen_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%-- ================== END OF WINDOW REOPEN ========================== --%>
</asp:Content>

