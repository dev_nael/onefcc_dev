﻿Imports Ext
Imports Elmah
Imports System.Data
Imports CasemanagementBLL
Imports CasemanagementDAL
Imports System.Data.SqlClient
Imports OfficeOpenXml

Partial Class CaseManagementAlertDetail
    Inherits ParentPage

    Public Property IDModule() As String
        Get
            Return Session("CaseManagementAlertDetail.IDModule")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertDetail.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("CaseManagementAlertDetail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertDetail.IDUnik") = value
        End Set
    End Property

    Public Property CIFNo() As String
        Get
            Return Session("CaseManagementAlertDetail.CIFNo")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertDetail.CIFNo") = value
        End Set
    End Property

    Public Property Workflow_Step() As Integer
        Get
            Return Session("CaseManagementAlertDetail.Workflow_Step")
        End Get
        Set(ByVal value As Integer)
            Session("CaseManagementAlertDetail.Workflow_Step") = value
        End Set
    End Property

    Public Property Workflow_Step_Total() As Integer
        Get
            Return Session("CaseManagementAlertDetail.Workflow_Step_Total")
        End Get
        Set(ByVal value As Integer)
            Session("CaseManagementAlertDetail.Workflow_Step_Total") = value
        End Set
    End Property

    Public Property Workflow_ID() As Integer
        Get
            Return Session("CaseManagementAlertDetail.Workflow_ID")
        End Get
        Set(ByVal value As Integer)
            Session("CaseManagementAlertDetail.Workflow_ID") = value
        End Set
    End Property

    Public Property CaseID_OtherCase() As Long
        Get
            If Session("CaseManagementAlertDetail.CaseID_OtherCase") Is Nothing Then
                Session("CaseManagementAlertDetail.CaseID_OtherCase") = 0
            End If
            Return Session("CaseManagementAlertDetail.CaseID_OtherCase")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertDetail.CaseID_OtherCase") = value
        End Set
    End Property

    Public Property RFI_ID() As Long
        Get
            Return Session("CaseManagementAlertDetail.RFI_ID")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertDetail.RFI_ID") = value
        End Set
    End Property

    Public Property CustomerType() As String
        Get
            Return Session("CaseManagementAlertDetail.CustomerType")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertDetail.CustomerType") = value
        End Set
    End Property

    Public Property DataTabelActivity As DataTable
        Get
            Return Session("CaseManagementAlertDetail.DataTabelActivity")
        End Get
        Set(ByVal value As DataTable)
            Session("CaseManagementAlertDetail.DataTabelActivity") = value
        End Set
    End Property

    Public Property ActionActivity() As enumActionForm
        Get
            Return Session("CaseManagementAlertApprovalDetail.ActionActivity")
        End Get
        Set(ByVal value As enumActionForm)
            Session("CaseManagementAlertApprovalDetail.ActionActivity") = value
        End Set
    End Property

    Public Property PK_Activity() As Long
        Get
            Return Session("CaseManagementAlertApprovalDetail.PK_Activity")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertApprovalDetail.PK_Activity") = value
        End Set
    End Property

    Enum enumActionForm
        Detail = 0
        Add = 1
        Edit = 2
        Delete = 3
        Accept = 4
        Reject = 5
    End Enum

    Private Sub CaseManagementAlertDetail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            IDModule = Request.Params("ModuleID")
            CustomerType = Request.Params("Type")
            Dim intModuleID As New Integer
            If IDModule IsNot Nothing Then
                intModuleID = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = ObjModule.ModuleLabel & " - Detail"

                DataTabelActivity = New DataTable
                LoadColumnDataTabelActivity()
                If CustomerType.ToLower() = "customer" Then
                    txt_CIF_No.FieldLabel = "CIF No"
                    txt_Customer_Name.FieldLabel = "Customer Name"
                    nDSDropDownField_AccountNo.Hidden = False
                    ColumnActivityAccountNo.Hidden = False
                    ColumnActivityAccountNo_OtherChase.Hidden = False
                ElseIf CustomerType.ToLower() = "wic" Then
                    txt_CIF_No.FieldLabel = "WIC No"
                    txt_Customer_Name.FieldLabel = "WIC Name"
                    checkbox_IsTransaction.Hidden = True
                    nDSDropDownField_AccountNo.Hidden = True
                    ColumnActivityAccountNo.Hidden = True
                    ColumnActivityAccountNo_OtherChase.Hidden = True
                    pnlCustomerInformation.Title = "WIC Information"
                    window_CustomerDetail.Title = "Data WIC Information"
                    txt_RuleBasic.Hidden = False
                    txt_DOB.Hidden = True
                    txt_POB.Hidden = True
                    txt_Gender.Hidden = True
                    txt_CIF_Opening_Branch.Hidden = True
                    txt_CIF_Opening_Date.Hidden = True
                    txt_Is_PEP.Hidden = True
                    txt_Risk_Code.Hidden = True
                End If
                'Load Data
                LoadDataCaseManagement()


                'Hide unused objects
                txt_LastModifiedDate.Hidden = True
                'txt_Account_No.Hidden = True
                'txt_Proposed_Action.Hidden = True
                'txt_Proposed_By.Hidden = True

                gp_SearchTransaction.Hidden = True
                pnl_AnalysisResult.Hidden = True
                btn_Save.Hidden = True

                SetCommandColumnLocation()

                '29-Mar-2022 Adi : Add fitur Re-Open
                SetReOpenAccess()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub LoadDataCaseManagement()
        Try
            Dim strSQL As String = ""
            If CustomerType.ToLower() = "customer" Then
                strSQL = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            ElseIf CustomerType.ToLower() = "wic" Then
                strSQL = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & IDUnik
            End If
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            If drCM IsNot Nothing Then
                If CustomerType.ToLower() = "customer" Then
                    If Not IsDBNull(drCM("CIF_No")) Then
                        Me.CIFNo = drCM("CIF_No")
                        txt_CIF_No.Value = drCM("CIF_No")
                    End If
                    If Not IsDBNull(drCM("Customer_Name")) Then
                        txt_Customer_Name.Value = drCM("Customer_Name")
                    End If

                    If Not IsDBNull(drCM("CIF_No")) Then
                        strSQL = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & drCM("CIF_No") & "'"
                        Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                        If drCustomer IsNot Nothing Then
                            If Not IsDBNull(drCustomer("DATEOFBIRTH")) Then
                                txt_DOB.Value = CDate(drCustomer("DATEOFBIRTH")).ToString("dd-MMM-yyyy")
                            End If
                            If Not IsDBNull(drCustomer("PLACEOFBIRTH")) Then
                                txt_POB.Value = drCustomer("PLACEOFBIRTH")
                            End If

                            If Not IsDBNull(drCustomer("FK_AML_JenisKelamin_Code")) Then
                                strSQL = "SELECT TOP 1 * FROM AML_Jenis_Kelamin WHERE FK_AML_Jenis_Kelamin_Code='" & drCustomer("FK_AML_JenisKelamin_Code") & "'"
                                Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                                If drGender IsNot Nothing Then
                                    txt_Gender.Value = drGender("FK_AML_Jenis_Kelamin_Code") & " - " & drGender("Jenis_Kelamin_Name")
                                Else
                                    txt_Gender.Value = drCustomer("FK_AML_JenisKelamin_Code")
                                End If
                            End If

                            If Not IsDBNull(drCustomer("FK_AML_Creation_Branch_Code")) Then
                                strSQL = "SELECT TOP 1 * FROM AML_BRANCH WHERE FK_AML_BRANCH_CODE='" & drCustomer("FK_AML_Creation_Branch_Code") & "'"
                                Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                                If drGender IsNot Nothing Then
                                    txt_CIF_Opening_Branch.Value = drGender("FK_AML_BRANCH_CODE") & " - " & drGender("BRANCH_NAME")
                                Else
                                    txt_CIF_Opening_Branch.Value = drCustomer("FK_AML_Creation_Branch_Code")
                                End If
                            End If

                            If Not IsDBNull(drCustomer("OpeningDate")) Then
                                txt_CIF_Opening_Date.Value = CDate(drCustomer("OpeningDate")).ToString("dd-MMM-yyyy")
                            End If

                            If Not IsDBNull(drCustomer("IS_PEP")) Then
                                If drCustomer("IS_PEP") = 0 Then
                                    txt_Is_PEP.Value = "No"
                                Else
                                    txt_Is_PEP.Value = "Yes"
                                End If
                            Else
                                txt_Is_PEP.Value = "N/A"
                            End If

                            If Not IsDBNull(drCustomer("FK_AML_RISK_CODE")) Then
                                strSQL = "SELECT TOP 1 * FROM AML_RISK_RATING WHERE RISK_RATING_CODE ='" & drCustomer("FK_AML_RISK_CODE") & "'"
                                Dim drRisk As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                                If drRisk IsNot Nothing Then
                                    txt_Risk_Code.Value = drRisk("RISK_RATING_CODE") & " - " & drRisk("RISK_RATING_NAME")
                                Else
                                    txt_Risk_Code.Value = drCustomer("FK_AML_RISK_CODE")
                                End If

                                If drCustomer("FK_AML_RISK_CODE") = "H" Then
                                    txt_Risk_Code.FieldStyle = "Color:Red;font-weight:Bold;"
                                Else
                                    txt_Risk_Code.FieldStyle = "Color:Black;font-weight:Normal;"
                                End If
                            End If

                        End If
                    End If

                    'List of Accounts
                    LoadDataAccounts(drCM("CIF_No"))

                    '29-Mar-2022 Adi : Load data Loan
                    LoadDataLoans(drCM("CIF_No"))
                ElseIf CustomerType.ToLower() = "wic" Then
                    If Not IsDBNull(drCM("WIC_No")) Then
                        Me.CIFNo = drCM("WIC_No")
                        txt_CIF_No.Value = drCM("WIC_No")
                    End If
                    If Not IsDBNull(drCM("WIC_Name")) Then
                        txt_Customer_Name.Value = drCM("WIC_Name")
                    End If

                    gp_Account.Hidden = True
                    gp_Loan.Hidden = True
                End If

                If Not IsDBNull(drCM("Workflow_Step")) Then
                    Me.Workflow_Step = drCM("Workflow_Step")
                End If
                If Not IsDBNull(drCM("Workflow_Step_Total")) Then
                    Me.Workflow_Step_Total = drCM("Workflow_Step_Total")
                End If
                If Not IsDBNull(drCM("FK_CaseManagement_Workflow_ID")) Then
                    Me.Workflow_ID = drCM("FK_CaseManagement_Workflow_ID")
                End If

                'General Information
                If Not IsDBNull(drCM("PK_CaseManagement_ID")) Then
                    txt_PK_CaseManagement_ID.Value = drCM("PK_CaseManagement_ID")
                End If
                ' 8 Maret 2023 Ari : update untuk set Unique CM ID
                If Not IsDBNull(drCM("Unique_CM_ID")) Then
                    txt_Unique_CM_ID.Value = drCM("Unique_CM_ID")
                End If

                If Not IsDBNull(drCM("Case_Description")) Then
                    txt_Case_Description.Value = drCM("Case_Description")
                End If

                If Not IsDBNull(drCM("Alert_Type")) Then
                    txt_Alert_Type.Value = drCM("Alert_Type")
                Else
                    Throw New ApplicationException("Alert Type is Mandatory But Returned NULL Please Report this to admin.")
                End If
                'txt_PK_CaseManagement_ID.Value = drCM("PK_CaseManagement_ID")
                'txt_Case_Description.Value = drCM("Case_Description")
                'txt_Alert_Type.Value = drCM("Alert_Type")

                If Not IsDBNull(drCM("FK_CaseStatus_ID")) Then
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_ProposedAction WHERE PK_OneFCC_CaseManagement_ProposedAction_ID=" & drCM("FK_CaseStatus_ID")
                    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drTemp IsNot Nothing Then
                        txt_FK_Case_Status_ID.Value = drTemp("PK_OneFCC_CaseManagement_ProposedAction_ID") & " - " & drTemp("Proposed_Action")
                    Else
                        txt_FK_Case_Status_ID.Value = "New Case"
                    End If
                Else
                    txt_FK_Case_Status_ID.Value = "New Case"
                End If

                If Not IsDBNull(drCM("ProcessDate")) Then
                    txt_ProcessDate.Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                End If
                If Not IsDBNull(drCM("LastUpdateDate")) Then
                    txt_LastModifiedDate.Value = CDate(drCM("LastUpdateDate")).ToString("dd-MMM-yyyy HH:mm:ss")
                End If
                If Not IsDBNull(drCM("FK_Proposed_Status_ID")) Then
                    Dim strTempStatus As String = drCM("FK_Proposed_Status_ID")
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_ProposedAction WHERE PK_OneFCC_CaseManagement_ProposedAction_ID=" & drCM("FK_Proposed_Status_ID")
                    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drTemp IsNot Nothing AndAlso Not IsDBNull(drTemp("PROPOSED_ACTION")) Then
                        strTempStatus = strTempStatus & " - " & drTemp("PROPOSED_ACTION")
                    End If
                    txt_FK_Case_Status_ID.Value = strTempStatus
                End If

                'txt_Proposed_By.Value = drCM("Proposed_By")
                If Not IsDBNull(drCM("Workflow_Step_Of")) Then
                    txt_Workflow_Step.Value = drCM("Workflow_Step_Of")
                End If
                If Not IsDBNull(drCM("PIC")) Then
                    txt_PIC.Value = drCM("PIC")
                End If
                'txt_Workflow_Step.Value = drCM("Workflow_Step_Of")
                'txt_PIC.Value = drCM("PIC")


                'Customer Information
                'txt_CIF_No.Value = drCM("CIF_No")
                'txt_Account_No.Value = drCM("Account_No")
                'txt_Customer_Name.Value = drCM("Customer_Name")


                'Bind RFI sent
                BindDataRFI()


                'Filter untuk level 1 hanya bisa Submitted as an issue or Submitted as non-issue
                If Not IsDBNull(drCM("Workflow_Step")) AndAlso drCM("Workflow_Step") = 1 Then
                    cmb_FK_Proposed_Status_ID.StringFilter = "PK_OneFCC_CaseManagement_ProposedAction_ID<=2"
                Else
                    cmb_FK_Proposed_Status_ID.StringFilter = ""
                End If

                'Switch between Typology/Outlier Transaction based on Alert_Type
                gp_CaseAlert_Typology_Transaction.Hidden = True
                pnl_CaseAlert_Outlier_Transaction.Hidden = True

                If Not IsDBNull(drCM("Alert_Type")) Then
                    Select Case drCM("Alert_Type")
                        Case "Typology Risk"
                            Dim strSQLTypology As String = "SELECT * FROM OneFCC_CaseManagement_Typology WHERE FK_CaseManagement_ID = " & IDUnik
                            Dim drCMTypology As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQLTypology)
                            If drCMTypology Is Nothing Then
                                Throw New ApplicationException("Could not find Case Manageent Typology, Please  report this to admin.")
                            End If
                            If IsDBNull(drCMTypology("Is_Transaction")) OrElse IsDBNull(drCMTypology("PK_OneFCC_CaseManagement_Typology_ID")) Then
                                Throw New ApplicationException("Is Transaction Or ID OneFCC_CaseManagement_Typology is null, please report this to admin.")
                            End If
                            If drCMTypology("Is_Transaction") = True Then
                                checkbox_IsTransaction.Value = "True"
                                gp_CaseAlert_Typology_Transaction.Hidden = False
                            ElseIf drCMTypology("Is_Transaction") = False Then
                                checkbox_IsTransaction.Value = "False"
                                Dim strSQLActivity As String = " SELECT PK_OneFCC_CaseManagement_Typology_NonTransaction_ID, Date_Activity, Activity_Description, Account_NO "
                                strSQLActivity = strSQLActivity & " FROM OneFCC_CaseManagement_Typology_NonTransaction WHERE FK_OneFCC_CaseManagement_Typology_ID = " & drCMTypology("PK_OneFCC_CaseManagement_Typology_ID")
                                DataTabelActivity = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQLActivity, Nothing)
                                BindTableActivity()
                                panel_Activity.Hidden = False
                            End If
                            txt_RuleBasic.Hidden = False
                            If Not IsDBNull(drCM("FK_Rule_Basic_ID")) Then
                                strSQL = "SELECT TOP 1 Rule_Basic_Name from OneFcc_MS_Rule_Basic where PK_Rule_Basic_ID = " & drCM("FK_Rule_Basic_ID")
                                Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                                If drTemp IsNot Nothing Then
                                    txt_RuleBasic.Value = drCM("FK_Rule_Basic_ID") & " - " & drTemp("Rule_Basic_Name")
                                End If
                            End If
                        Case "Financial Risk"
                            pnl_CaseAlert_Outlier_Transaction.Hidden = False

                            'Display Mean dan Modus
                            strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=2"
                            Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                            If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                                fs_CaseAlert_Outlier_Transaction.Title = "<b>Financial Statistics in past " & drParam("ParameterValue") & " month(s)</b>"
                            End If

                            strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Outlier WHERE FK_CaseManagement_ID=" & IDUnik
                            Dim drCMO As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                            If drCMO IsNot Nothing Then
                                If Not IsDBNull(drCMO("Mean_Debit")) Then
                                    txt_MeanDebit.Value = CDbl(drCMO("Mean_Debit")).ToString("#,##0.00")
                                End If
                                If Not IsDBNull(drCMO("Mean_Credit")) Then
                                    txt_MeanCredit.Value = CDbl(drCMO("Mean_Credit")).ToString("#,##0.00")
                                End If
                                If Not IsDBNull(drCMO("Modus_Debit")) Then
                                    txt_ModusDebit.Value = CDbl(drCMO("Modus_Debit")).ToString("#,##0.00")
                                End If
                                If Not IsDBNull(drCMO("Modus_Credit")) Then
                                    txt_ModusCredit.Value = CDbl(drCMO("Modus_Credit")).ToString("#,##0.00")
                                End If
                            End If
                            '' Add 05-Jan-2023, Felix, untuk Alert Type selain itu
                        Case Else
                            Dim strSQLTypology As String = "SELECT * FROM OneFCC_CaseManagement_Typology WHERE FK_CaseManagement_ID = " & IDUnik
                            Dim drCMTypology As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQLTypology)
                            If drCMTypology Is Nothing Then
                                Throw New ApplicationException("Could not find Case Manageent Typology, Please  report this to admin.")
                            End If
                            If IsDBNull(drCMTypology("Is_Transaction")) OrElse IsDBNull(drCMTypology("PK_OneFCC_CaseManagement_Typology_ID")) Then
                                Throw New ApplicationException("Is Transaction Or ID OneFCC_CaseManagement_Typology is null, please report this to admin.")
                            End If
                            If drCMTypology("Is_Transaction") = True Then
                                checkbox_IsTransaction.Value = "True"
                                gp_CaseAlert_Typology_Transaction.Hidden = False
                            ElseIf drCMTypology("Is_Transaction") = False Then
                                checkbox_IsTransaction.Value = "False"
                                Dim strSQLActivity As String = " SELECT PK_OneFCC_CaseManagement_Typology_NonTransaction_ID, Date_Activity, Activity_Description, Account_NO "
                                strSQLActivity = strSQLActivity & " FROM OneFCC_CaseManagement_Typology_NonTransaction WHERE FK_OneFCC_CaseManagement_Typology_ID = " & drCMTypology("PK_OneFCC_CaseManagement_Typology_ID")
                                DataTabelActivity = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQLActivity, Nothing)
                                BindTableActivity()
                                panel_Activity.Hidden = False
                            End If
                            'txt_RuleBasic.Hidden = False
                            'If Not IsDBNull(drCM("FK_Rule_Basic_ID")) Then
                            '    strSQL = "SELECT TOP 1 Rule_Basic_Name from OneFcc_MS_Rule_Basic where PK_Rule_Basic_ID = " & drCM("FK_Rule_Basic_ID")
                            '    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                            '    If drTemp IsNot Nothing Then
                            '        txt_RuleBasic.Value = drCM("FK_Rule_Basic_ID") & " - " & drTemp("Rule_Basic_Name")
                            '    End If
                            'End If
                            '' End 05-Jan-2023
                    End Select
                End If

                btn_Save.Hidden = True
                pnl_AnalysisResult.Hidden = True
                tb_RFI.Hidden = True

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadDataAccounts(strCIFNo As String)
        Try
            Dim strQuery As String = "SELECT acc.*, card.FK_AML_CARD_NO, card.LIMITCREDITCARD"
            strQuery &= " FROM AML_ACCOUNT acc"
            strQuery &= " LEFT JOIN AML_CARD card ON acc.ACCOUNT_NO = card.ACCOUNT_NO"
            strQuery &= " WHERE acc.CIFNO='" & strCIFNo & "'"

            Dim dtAccount As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If dtAccount Is Nothing Then
                dtAccount = New DataTable
            End If

            If dtAccount IsNot Nothing Then
                'Running the following script for additional columns
                dtAccount.Columns.Add(New DataColumn("BRANCH_NAME", GetType(String)))
                dtAccount.Columns.Add(New DataColumn("ACCOUNT_TYPE", GetType(String)))
                dtAccount.Columns.Add(New DataColumn("PRODUCT_NAME", GetType(String)))

                'Get reference Table
                strQuery = "SELECT * FROM AML_BRANCH"
                Dim dtBranch As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                strQuery = "SELECT * FROM AML_ACCOUNT_TYPE"
                Dim dtAccountType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                strQuery = "SELECT * FROM AML_PRODUCT"
                Dim dtProduct As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                For Each row As DataRow In dtAccount.Rows
                    If dtBranch IsNot Nothing AndAlso Not IsDBNull(row("FK_AML_BRANCH_CODE")) Then
                        Dim drCek = dtBranch.Select("FK_AML_BRANCH_CODE='" & row("FK_AML_BRANCH_CODE") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("BRANCH_NAME") = drCek("BRANCH_NAME")
                        End If
                    End If
                    If dtAccountType IsNot Nothing AndAlso Not IsDBNull(row("FK_AML_ACCOUNT_TYPE_CODE")) Then
                        Dim drCek = dtAccountType.Select("FK_AML_ACCOUNT_TYPE_CODE='" & row("FK_AML_ACCOUNT_TYPE_CODE") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("ACCOUNT_TYPE") = drCek("ACCOUNT_TYPE_NAME")
                        End If
                    End If
                    If dtProduct IsNot Nothing AndAlso Not IsDBNull(row("FK_AML_PRODUCT_CODE")) Then
                        Dim drCek = dtProduct.Select("FK_AML_PRODUCT_CODE='" & row("FK_AML_PRODUCT_CODE") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("PRODUCT_NAME") = drCek("PRODUCT_NAME")
                        End If
                    End If

                Next

                'Bind to gridpanel
                gp_Account.GetStore.DataSource = dtAccount
                gp_Account.GetStore.DataBind()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    '29-Mar-2022 Adi
    Protected Sub LoadDataLoans(strCIFNo As String)
        Try
            Dim strQuery As String = "SELECT al.*, ac.COLLATERAL_TYPE"
            strQuery &= " FROM AML_LOAN al"
            strQuery &= " LEFT JOIN AML_COLLATERAL_LOAN acl ON acl.LOAN_NUMBER = al.LOAN_NUMBER"
            strQuery &= " LEFT JOIN AML_COLLATERAL ac ON ac.COLLATERAL_NUMBER = acl.COLLATERAL_NUMBER"
            strQuery &= " WHERE al.CIF='" & strCIFNo & "'"

            Dim dtLoan As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If dtLoan Is Nothing Then
                dtLoan = New DataTable
            End If

            If dtLoan IsNot Nothing Then
                'Running the following script for additional columns
                dtLoan.Columns.Add(New DataColumn("BRANCH_NAME", GetType(String)))

                'Get reference Table
                strQuery = "SELECT * FROM AML_BRANCH"
                Dim dtBranch As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                For Each row As DataRow In dtLoan.Rows
                    If dtBranch IsNot Nothing AndAlso Not IsDBNull(row("BRANCH_CODE")) Then
                        Dim drCek = dtBranch.Select("FK_AML_BRANCH_CODE='" & row("BRANCH_CODE") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("BRANCH_NAME") = drCek("BRANCH_NAME")
                        End If
                    End If
                Next

                'Bind to gridpanel
                gp_Loan.GetStore.DataSource = dtLoan
                gp_Loan.GetStore.DataBind()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Typology_Transaction(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            'If strsort = "" Then
            '    strsort = "CIF asc, Account_Name asc"
            'End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("Vw_Account", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)

            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " and ofcmtt.CIF_No_Lawan is null "
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Typology_Transaction.GetStore.DataSource = DataPaging
            gp_CaseAlert_Typology_Transaction.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Outlier_Transaction(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            'If strsort = "" Then
            '    strsort = "CIF asc, Account_Name asc"
            'End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("Vw_Account", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)


            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Outlier AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " and ofcmtt.CIF_No_Lawan is null "
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Outlier_Transaction.GetStore.DataSource = DataPaging
            gp_CaseAlert_Outlier_Transaction.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_Search_Transaction(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            'Dim strQuery As String = "SELECT * FROM goAML_ODM_Transaksi"
            'strQuery += " WHERE CIF_NO = '" & Me.CIFNo & "'"

            '29-Mar-2022 Adi
            Dim strQuery As String = "SELECT gaot.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM goAML_ODM_Transaksi AS gaot"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON gaot.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON gaot.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " and gaot.CIF_No_Lawan is null "
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON gaot.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE CIF_NO = '" & Me.CIFNo & "'"

            Dim DataPaging As Data.DataTable
            If Not (txt_SearchTransaction_From.SelectedDate = DateTime.MinValue And txt_SearchTransaction_To.SelectedDate = DateTime.MinValue) Then
                strQuery += " AND Date_Transaction >= '" & CDate(txt_SearchTransaction_From.SelectedDate).ToString("yyyy-MM-dd") & "'"
                strQuery += " AND Date_Transaction <= '" & CDate(txt_SearchTransaction_To.SelectedDate).ToString("yyyy-MM-dd") & "'"

                If Not String.IsNullOrEmpty(strfilter) Then
                    strQuery += " AND " & strfilter
                End If

                DataPaging = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)
            Else
                DataPaging = New DataTable
            End If

            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_SearchTransaction.GetStore.DataSource = DataPaging
            gp_SearchTransaction.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_WorkflowHistory(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT history.*, proposed.Proposed_Action FROM onefcc_casemanagement_workflowhistory history"
            strQuery += " LEFT JOIN OneFCC_CaseManagement_ProposedAction proposed ON history.FK_Proposed_Status_ID = proposed.PK_OneFCC_CaseManagement_ProposedAction_ID"
            strQuery += " WHERE FK_CaseManagement_ID = " & Me.IDUnik

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)

            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_WorkflowHistory.GetStore.DataSource = DataPaging
            gp_WorkflowHistory.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btn_Search_Transaction_Click(sender As Object, e As DirectEventArgs)
        Try
            If txt_SearchTransaction_From.SelectedDate = DateTime.MinValue Or txt_SearchTransaction_To.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException("Please fill Date From and Date To for searching transaction!")
            End If

            store_SearchTransaction.Reload()

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Back_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Save_Click()
        Try
            If String.IsNullOrEmpty(cmb_FK_Proposed_Status_ID.StringValue) Then
                Throw New ApplicationException("Proposed Status is required.")
            End If
            If String.IsNullOrWhiteSpace(txt_Analysis_Result.Value) Then
                Throw New ApplicationException("Analysis Result is required.")
            End If

            'Get PIC from workflow Detail
            Dim strPIC As String = ""
            Dim strQuery As String = "SELECT * FROM OneFCC_CaseManagement_WorkflowDetail"
            strQuery += " WHERE Workflow_Step=" & (Me.Workflow_Step + 1)
            Dim drPIC = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            If drPIC IsNot Nothing AndAlso Not IsDBNull(drPIC("PIC")) Then
                strPIC = drPIC("PIC")
            End If

            'Update Case Management Status and Proposed Status
            strQuery = "UPDATE OneFCC_CaseManagement SET FK_Proposed_Status_ID=" & cmb_FK_Proposed_Status_ID.SelectedItemValue
            strQuery += ", FK_CaseStatus_ID=" & cmb_FK_Proposed_Status_ID.SelectedItemValue
            strQuery += ", Workflow_Step=" & (Me.Workflow_Step + 1)
            strQuery += ", PIC='" & strPIC & "'"
            strQuery += " WHERE PK_CaseManagement_ID=" & Me.IDUnik
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Insert into Workflow History
            strQuery = "INSERT INTO OneFCC_CaseManagement_WorkflowHistory ("
            strQuery += "FK_CaseManagement_ID,"
            strQuery += "Workflow_Step,"
            strQuery += "FK_Proposed_Status_ID,"
            strQuery += "Analysis_Result,"
            strQuery += "CreatedBy,"
            strQuery += "CreatedDate) "
            strQuery += "VALUES ("
            strQuery += Me.IDUnik.ToString & ","
            strQuery += Me.Workflow_Step & ","
            strQuery += cmb_FK_Proposed_Status_ID.SelectedItemValue.ToString & ","
            strQuery += "'" & txt_Analysis_Result.Value & "',"
            strQuery += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
            strQuery += "GETDATE())"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


            'Show Confirmation
            LblConfirmation.Text = "Case Management Follow Up has been submitted for approval."
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Function CheckIfStringExists(strPart As String, strFull As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim dt As DataTable = New DataTable

            Dim strsplit As String() = strFull.Split(","c)
            For Each item As String In strsplit
                If strPart = item Then
                    Return True
                End If
            Next

            Return bolResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#Region "9-Feb-2022 : Send Email RFI (Request For Information)"
    Protected Sub btn_Send_New_RFI_Click()
        Try
            CleanWindowSendRFI()
            btn_SendRFI_Save.Hidden = False

            '04-Mar-2022 Adi : Jadikan semua detail ReadOnly
            Me.RFI_ID = 0
            txt_EmailAttachment.Hidden = False
            ctr_RFI_Attachment.Hidden = True

            Window_SendRFI.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_RFI(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            If strCommandName = "Detail" Then
                LoadDataRFI(ID)
            ElseIf strCommandName = "Download" Then
                DownloadRFI(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DownloadRFI(id As Long)
        Using objdb As New CasemanagementEntities
            Dim objRFI = objdb.OneFCC_CaseManagement_RFI.Where(Function(x) x.PK_OneFCC_CaseManagement_RFI_ID = id).FirstOrDefault
            If Not objRFI Is Nothing Then
                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & objRFI.AttachmentName)
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(objRFI.Attachment)
                Response.End()
            End If
        End Using
    End Sub

    Protected Sub btn_SendRFI_Save_Click()
        Try
            If String.IsNullOrWhiteSpace(txt_EmailTo.Value) Then
                Throw New ApplicationException(txt_EmailTo.FieldLabel & " is required.")
            End If
            If String.IsNullOrWhiteSpace(txt_EmailSubject.Value) Then
                Throw New ApplicationException(txt_EmailSubject.FieldLabel & " is required.")
            End If
            If String.IsNullOrWhiteSpace(txt_EmailContent.Value) Then
                Throw New ApplicationException(txt_EmailContent.FieldLabel & " is required.")
            End If

            'Validate email address
            If Not String.IsNullOrWhiteSpace(txt_EmailTo.Value) Then
                Dim strValidateEmail As String = ValidateEmailList(txt_EmailTo.Value)
                If Not String.IsNullOrEmpty(strValidateEmail) Then
                    Throw New ApplicationException(strValidateEmail)
                End If
            End If

            'Validate file upload max size
            If txt_EmailAttachment.HasFile Then
                Dim intmaxfilesize As Double = NawaBLL.SystemParameterBLL.GetMaxFileSize
                Dim strmaxfilesize As String = (intmaxfilesize / 1048576) & " MB"

                Dim intFileSizeUpload As Double = txt_EmailAttachment.FileBytes.Length
                Dim strFileUploadsize As String = (intFileSizeUpload / 1048576).ToString("#,##0.00") & " MB"

                If intFileSizeUpload > intmaxfilesize Then
                    Throw New ApplicationException("The attachment " & strFileUploadsize & " exceeds the max file size " & strmaxfilesize)
                End If
            End If

            'Populate Data
            Dim objRFI As New OneFCC_CaseManagement_RFI
            With objRFI
                .FK_CaseManagement_ID = IDUnik
                .EmailTo = txt_EmailTo.Value
                .EmailCc = txt_EmailCC.Value
                .EmailBcc = txt_EmailBCC.Value
                .Subject = txt_EmailSubject.Value

                Dim Val = txt_EmailContent.RawValue.ToString.Replace("%3E%u200B%3C", "%3E%3C").Replace("+", "%2B")
                If String.IsNullOrEmpty(Val) Then
                    .Content = txt_EmailContent.Value
                Else
                    .Content = Server.UrlDecode(Val)
                End If

                If txt_EmailAttachment.HasFile Then
                    .AttachmentName = txt_EmailAttachment.FileName
                    .Attachment = txt_EmailAttachment.FileBytes
                End If

                .FK_EmailStatus_ID = 1  'Preparation

                .Active = 1
                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                .CreatedDate = DateTime.Now
            End With

            'Insert to DB
            Using objDb As New CasemanagementEntities
                Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                    Try
                        'Save objData
                        objDb.Entry(objRFI).State = Entity.EntityState.Added
                        objDb.SaveChanges()

                        'AuditTrail
                        Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, "OneFCC_CaseManagement_RFI")

                        'AuditTrailDetail
                        NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, objRFI)

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                    End Try
                End Using
            End Using

            'Execute Send Email
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_OneFCC_CaseManagement_RFI_ID"
            param(0).Value = objRFI.PK_OneFCC_CaseManagement_RFI_ID
            param(0).DbType = SqlDbType.BigInt

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_OneFCC_TriggerEmailRFI", param)

            'Bind to GridPanel
            BindDataRFI()

            'Hide window
            Window_SendRFI.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_SendRFI_Back_Click()
        Try
            Window_SendRFI.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub CleanWindowSendRFI()
        Try
            txt_EmailTo.Value = Nothing
            txt_EmailCC.Value = Nothing
            txt_EmailBCC.Value = Nothing
            txt_EmailSubject.Value = Nothing
            txt_EmailContent.Value = Nothing
            txt_EmailAttachment.Value = Nothing

            txt_EmailAttachment.Reset()

            'Fields Readonly
            txt_EmailTo.ReadOnly = False
            txt_EmailCC.ReadOnly = False
            txt_EmailBCC.ReadOnly = False
            txt_EmailSubject.ReadOnly = False
            txt_EmailContent.ReadOnly = False
            txt_EmailAttachment.ReadOnly = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindDataRFI()
        Try
            Dim strQuery As String = "SELECT * FROM OneFCC_CaseManagement_RFI WHERE FK_CaseManagement_ID=" & Me.IDUnik
            Dim dtRFI As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If dtRFI Is Nothing Then
                dtRFI = New DataTable
            End If

            If dtRFI IsNot Nothing Then
                'Running the following script for additional columns
                dtRFI.Columns.Add(New DataColumn("EmailStatusName", GetType(String)))

                'Get reference Table
                strQuery = "SELECT * FROM EmailStatus"
                Dim dtEmailStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                For Each row As DataRow In dtRFI.Rows
                    If dtEmailStatus IsNot Nothing AndAlso Not IsDBNull(row("FK_EmailStatus_ID")) Then
                        Dim drCek = dtEmailStatus.Select("PK_EmailStatus_ID='" & row("FK_EmailStatus_ID") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("EmailStatusName") = drCek("EmailStatusName")
                        End If
                    End If
                Next

                'Bind to gridpanel
                gp_RFI.GetStore.DataSource = dtRFI
                gp_RFI.GetStore.DataBind()

            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub LoadDataRFI(strRFI_ID As String)
        Try
            Using objdb As New CasemanagementEntities
                With objdb
                    Dim objRFI = objdb.OneFCC_CaseManagement_RFI.Where(Function(x) x.PK_OneFCC_CaseManagement_RFI_ID = strRFI_ID).FirstOrDefault
                    If objRFI IsNot Nothing Then
                        txt_EmailTo.Value = objRFI.EmailTo
                        txt_EmailCC.Value = objRFI.EmailCc
                        txt_EmailBCC.Value = objRFI.EmailBcc
                        txt_EmailSubject.Value = objRFI.Subject
                        txt_EmailContent.Value = objRFI.Content
                        txt_EmailAttachment.Value = objRFI.AttachmentName
                        df_EmailAttachment.Value = objRFI.AttachmentName

                        '29-Mar-2022 Adi : Penambahan Notes RFI
                        Dim strQuery As String = "SELECT * FROM OneFCC_CaseManagement_RFI WHERE PK_OneFCC_CaseManagement_RFI_ID=" & strRFI_ID
                        Dim drRFI As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        If drRFI IsNot Nothing Then
                            txt_EmailNotes.Value = drRFI("Notes")
                        End If

                        '29-Mar-2022 Adi : Penambahan Notes RFI. Hanya user yang kirim RFI yg boleh tambahin Notes
                        txt_EmailNotes.Hidden = False
                        txt_EmailNotes.ReadOnly = True
                    End If
                End With
            End Using

            'Fields Readonly
            txt_EmailTo.ReadOnly = True
            txt_EmailCC.ReadOnly = True
            txt_EmailBCC.ReadOnly = True
            txt_EmailSubject.ReadOnly = True
            txt_EmailContent.ReadOnly = True
            txt_EmailAttachment.ReadOnly = True

            '04-Mar-2022 Adi : Jadikan semua detail ReadOnly
            Me.RFI_ID = strRFI_ID
            txt_EmailAttachment.Hidden = True
            ctr_RFI_Attachment.Hidden = False

            If String.IsNullOrEmpty(df_EmailAttachment.Value) Then
                btn_DownloadEmailAttachment.Hidden = True
            Else
                btn_DownloadEmailAttachment.Hidden = False
            End If

            btn_SendRFI_Save.Hidden = True
            Window_SendRFI.Hidden = False

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ValidateEmailList(emailAddress As String) As String
        Dim strEmail As String() = emailAddress.Split(";")
        For Each item In strEmail
            If Not ValidateEmail(item) Then
                Return "Email address '" & item & "' is not valid!"
            End If
        Next

        Return ""
    End Function

    Function ValidateEmail(emailAddress As String) As Boolean
        If String.IsNullOrEmpty(emailAddress) Then
            Return True
        End If
        Dim email As New Regex("([\w-+]+(?:\.[\w-+]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7})")
        If email.IsMatch(emailAddress) Then
            Return True
        Else
            Return False
        End If
    End Function

#End Region

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        If buttonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub

    Sub SetCommandColumnLocation()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim buttonPosition As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            buttonPosition = objParamSettingbutton.SettingValue
        End If

        ColumnActionLocation(gp_RFI, cc_RFI, buttonPosition)
        ColumnActionLocation(gp_OtherCase, cc_OtherCase, buttonPosition)

    End Sub


#Region "10-Feb-2022 : Export Hit/Search Transaction to CSV/Excel"
    Protected Sub ExportAll_CaseAlert_Typology(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            'Get Data Transaction
            'Dim strQuery As String = "SELECT ofcmtt.Date_Transaction, ofcmtt.Account_NO, ofcmtt.Transmode_Code, ofcmtt.Transaction_Remark, ofcmtt.IDR_Amount,"
            'strQuery &= " ofcmtt.country_code, ofcmtt.country_code_lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            '29-Mar-2022 Adi
            Dim strQuery As String = "SELECT ofcmtt.Date_Transaction"
            strQuery &= " ,ofcmtt.Ref_Num"
            strQuery &= " ,ofcmtt.Transmode_Code"
            strQuery &= " ,ofcmtt.Transaction_Location"
            strQuery &= " ,ofcmtt.Debit_Credit"
            strQuery &= " ,ofcmtt.Currency"
            strQuery &= " ,ofcmtt.Original_Amount"
            strQuery &= " ,ofcmtt.IDR_Amount"
            strQuery &= " ,ofcmtt.Transaction_Remark"
            strQuery &= " ,ofcmtt.Account_NO"
            strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,ofcmtt.Account_No_Lawan"
            strQuery &= " ,ofcmtt.CIF_No_Lawan"
            strQuery &= " ,ofcmtt.WIC_No_Lawan"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " and ofcmtt.CIF_No_Lawan is null "
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            'Get Data CM
            If CustomerType.ToLower() = "customer" Then
                strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            ElseIf CustomerType.ToLower() = "wic" Then
                strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & IDUnik
            End If
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Typology_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CaseManagementTypologyTrx")

                    'Display Header of CM
                    If drCM IsNot Nothing Then
                        ws.Cells("A1").Value = "Case Management ID"
                        ws.Cells("B1").Value = drCM("PK_CaseManagement_ID").ToString

                        ws.Cells("A2").Value = "CIF"
                        ws.Cells("B2").Value = drCM("CIF_No")

                        ws.Cells("A3").Value = "Customer Name"
                        ws.Cells("B3").Value = drCM("Customer_Name")

                        ws.Cells("A4").Value = "Alert Type"
                        ws.Cells("B4").Value = drCM("Alert_Type")

                        ws.Cells("A5").Value = "Case Description"
                        ws.Cells("B5").Value = drCM("Case_Description")

                        ws.Cells("A6").Value = "Process Date"
                        If Not IsDBNull(drCM("ProcessDate")) Then
                            ws.Cells("B6").Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                        End If
                    End If

                    'objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    'objtbl.Columns("Account_NO").SetOrdinal(1)
                    'objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    'objtbl.Columns("Transaction_Remark").SetOrdinal(3)
                    'objtbl.Columns("IDR_Amount").SetOrdinal(4)
                    'objtbl.Columns("country_code").SetOrdinal(5)
                    'objtbl.Columns("country_code_lawan").SetOrdinal(6)

                    'objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                    'objtbl.Columns("Account_NO").ColumnName = "Account No"
                    'objtbl.Columns("Transmode_Code").ColumnName = "Transmode Code"
                    'objtbl.Columns("Transaction_Remark").ColumnName = "Transaction Remark"
                    'objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    'objtbl.Columns("country_code").ColumnName = "Country Code"
                    'objtbl.Columns("country_code_lawan").ColumnName = "Opp. Country Code"

                    '29-Mar-2022 Adi : Perubahan skema informasi yang ditampilkan
                    objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    objtbl.Columns("Ref_Num").SetOrdinal(1)
                    objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    objtbl.Columns("Transaction_Location").SetOrdinal(3)
                    objtbl.Columns("Debit_Credit").SetOrdinal(4)
                    objtbl.Columns("Currency").SetOrdinal(5)
                    objtbl.Columns("Original_Amount").SetOrdinal(6)
                    objtbl.Columns("IDR_Amount").SetOrdinal(7)
                    objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                    objtbl.Columns("Account_NO").SetOrdinal(9)
                    objtbl.Columns("CounterPartyType").SetOrdinal(10)
                    objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                    objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                    objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                    objtbl.Columns("CounterPartyName").SetOrdinal(14)
                    objtbl.Columns("Country_Lawan").SetOrdinal(15)

                    objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
                    objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
                    objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
                    objtbl.Columns("Transaction_Location").ColumnName = "Location"
                    objtbl.Columns("Debit_Credit").ColumnName = "D/C"
                    objtbl.Columns("Currency").ColumnName = "CCY"
                    objtbl.Columns("Original_Amount").ColumnName = "Amount"
                    objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                    objtbl.Columns("Account_NO").ColumnName = "Account No."
                    objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                    objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                    objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                    objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                    objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                    objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"

                    ws.Cells("A8").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Management Typology Transaction (Case ID " & drCM("PK_CaseManagement_ID") & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportAll_CaseAlert_Outlier(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            ''Get Data Transaction
            'Dim strQuery As String = "SELECT ofcmot.Date_Transaction, ofcmot.Account_NO, ofcmot.Transmode_Code, ofcmot.Transaction_Remark, ofcmot.IDR_Amount,"
            'strQuery &= " ofcmot.country_code, ofcmot.country_code_lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmot"
            'strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmo ON ofcmot.FK_OneFCC_CaseManagement_Outlier_ID = ofcmo.PK_OneFCC_CaseManagement_Outlier_ID"
            'strQuery &= " WHERE ofcmo.FK_CaseManagement_ID = " & IDUnik

            '29-Mar-2022 Adi
            Dim strQuery As String = "SELECT ofcmtt.Date_Transaction"
            strQuery &= " ,ofcmtt.Ref_Num"
            strQuery &= " ,ofcmtt.Transmode_Code"
            strQuery &= " ,ofcmtt.Transaction_Location"
            strQuery &= " ,ofcmtt.Debit_Credit"
            strQuery &= " ,ofcmtt.Currency"
            strQuery &= " ,ofcmtt.Original_Amount"
            strQuery &= " ,ofcmtt.IDR_Amount"
            strQuery &= " ,ofcmtt.Transaction_Remark"
            strQuery &= " ,ofcmtt.Account_NO"
            strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,ofcmtt.Account_No_Lawan"
            strQuery &= " ,ofcmtt.CIF_No_Lawan"
            strQuery &= " ,ofcmtt.WIC_No_Lawan"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " and ofcmtt.CIF_No_Lawan is null "
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            'Get Data CM
            If CustomerType.ToLower() = "customer" Then
                strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            ElseIf CustomerType.ToLower() = "wic" Then
                strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & IDUnik
            End If
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Outlier_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CaseManagementOutlierTrx")

                    'Display Header of CM
                    If drCM IsNot Nothing Then
                        ws.Cells("A1").Value = "Case Management ID"
                        ws.Cells("B1").Value = drCM("PK_CaseManagement_ID").ToString

                        ws.Cells("A2").Value = "CIF"
                        ws.Cells("B2").Value = drCM("CIF_No")

                        ws.Cells("A3").Value = "Customer Name"
                        ws.Cells("B3").Value = drCM("Customer_Name")

                        ws.Cells("A4").Value = "Alert Type"
                        ws.Cells("B4").Value = drCM("Alert_Type")

                        ws.Cells("A5").Value = "Case Description"
                        ws.Cells("B5").Value = drCM("Case_Description")

                        ws.Cells("A6").Value = "Process Date"
                        If Not IsDBNull(drCM("ProcessDate")) Then
                            ws.Cells("B6").Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                        End If
                    End If

                    'Display Financial Statistics
                    Dim strSQL As String = ""
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=2"
                    Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                        ws.Cells("A8").Value = "Financial Statistics in past " & drParam("ParameterValue") & " month(s)"
                    End If

                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Outlier WHERE FK_CaseManagement_ID=" & IDUnik
                    Dim drCMO As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drCMO IsNot Nothing Then
                        ws.Cells("A9").Value = "Mean Debit"
                        If Not IsDBNull(drCMO("Mean_Debit")) Then
                            ws.Cells("B9").Value = CDbl(drCMO("Mean_Debit")).ToString("#,##0.00")
                        End If

                        ws.Cells("A10").Value = "Mean Credit"
                        If Not IsDBNull(drCMO("Mean_Credit")) Then
                            ws.Cells("B10").Value = CDbl(drCMO("Mean_Credit")).ToString("#,##0.00")
                        End If

                        ws.Cells("A11").Value = "Modus Debit"
                        If Not IsDBNull(drCMO("Modus_Debit")) Then
                            ws.Cells("B11").Value = CDbl(drCMO("Modus_Debit")).ToString("#,##0.00")
                        End If

                        ws.Cells("A12").Value = "Modus Credit"
                        If Not IsDBNull(drCMO("Modus_Credit")) Then
                            ws.Cells("B12").Value = CDbl(drCMO("Modus_Credit")).ToString("#,##0.00")
                        End If
                    End If

                    'objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    'objtbl.Columns("Account_NO").SetOrdinal(1)
                    'objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    'objtbl.Columns("Transaction_Remark").SetOrdinal(3)
                    'objtbl.Columns("IDR_Amount").SetOrdinal(4)
                    'objtbl.Columns("country_code").SetOrdinal(5)
                    'objtbl.Columns("country_code_lawan").SetOrdinal(6)

                    'objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                    'objtbl.Columns("Account_NO").ColumnName = "Account No"
                    'objtbl.Columns("Transmode_Code").ColumnName = "Transmode Code"
                    'objtbl.Columns("Transaction_Remark").ColumnName = "Transaction Remark"
                    'objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    'objtbl.Columns("country_code").ColumnName = "Country Code"
                    'objtbl.Columns("country_code_lawan").ColumnName = "Opp. Country Code"

                    '29-Mar-2022 Adi : Perubahan skema informasi yang ditampilkan
                    objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    objtbl.Columns("Ref_Num").SetOrdinal(1)
                    objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    objtbl.Columns("Transaction_Location").SetOrdinal(3)
                    objtbl.Columns("Debit_Credit").SetOrdinal(4)
                    objtbl.Columns("Currency").SetOrdinal(5)
                    objtbl.Columns("Original_Amount").SetOrdinal(6)
                    objtbl.Columns("IDR_Amount").SetOrdinal(7)
                    objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                    objtbl.Columns("Account_NO").SetOrdinal(9)
                    objtbl.Columns("CounterPartyType").SetOrdinal(10)
                    objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                    objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                    objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                    objtbl.Columns("CounterPartyName").SetOrdinal(14)
                    objtbl.Columns("Country_Lawan").SetOrdinal(15)

                    objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
                    objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
                    objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
                    objtbl.Columns("Transaction_Location").ColumnName = "Location"
                    objtbl.Columns("Debit_Credit").ColumnName = "D/C"
                    objtbl.Columns("Currency").ColumnName = "CCY"
                    objtbl.Columns("Original_Amount").ColumnName = "Amount"
                    objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                    objtbl.Columns("Account_NO").ColumnName = "Account No."
                    objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                    objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                    objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                    objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                    objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                    objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"

                    ws.Cells("A14").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Management Outlier Transaction (Case ID " & drCM("PK_CaseManagement_ID") & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportAll_SearchTransaction(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            'Date From and Date To mandatory
            If txt_SearchTransaction_From.SelectedDate = DateTime.MinValue Or txt_SearchTransaction_To.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException("Please fill Date From and Date To for searching transaction!")
            End If

            'Get Data CM
            Dim strQuery As String = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)


            'Get Data Transaction
            'strQuery = "SELECT gaot.Date_Transaction, gaot.Account_NO, gaot.Transmode_Code, gaot.Transaction_Remark, gaot.IDR_Amount,"
            'strQuery &= " gaot.country_code, gaot.country_code_lawan"
            'strQuery &= " FROM goAML_ODM_Transaksi AS gaot"
            'strQuery &= " WHERE gaot.CIF_NO = '" & Me.CIFNo & "'"

            '29-Mar-2022 Adi
            strQuery = "SELECT gaot.Date_Transaction"
            strQuery &= " ,gaot.Ref_Num"
            strQuery &= " ,gaot.Transmode_Code"
            strQuery &= " ,gaot.Transaction_Location"
            strQuery &= " ,gaot.Debit_Credit"
            strQuery &= " ,gaot.Currency"
            strQuery &= " ,gaot.Original_Amount"
            strQuery &= " ,gaot.IDR_Amount"
            strQuery &= " ,gaot.Transaction_Remark"
            strQuery &= " ,gaot.Account_NO"
            strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,gaot.Account_No_Lawan"
            strQuery &= " ,gaot.CIF_No_Lawan"
            strQuery &= " ,gaot.WIC_No_Lawan"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM goAML_ODM_Transaksi AS gaot"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON gaot.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON gaot.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON gaot.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE gaot.CIF_NO = '" & Me.CIFNo & "'"

            Dim dtTransactionAll As Data.DataTable
            If Not (txt_SearchTransaction_From.SelectedDate = DateTime.MinValue And txt_SearchTransaction_To.SelectedDate = DateTime.MinValue) Then
                strQuery += " And Date_Transaction >= '" & CDate(txt_SearchTransaction_From.SelectedDate).ToString("yyyy-MM-dd") & "'"
                strQuery += " AND Date_Transaction <= '" & CDate(txt_SearchTransaction_To.SelectedDate).ToString("yyyy-MM-dd") & "'"

                dtTransactionAll = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            Else
                dtTransactionAll = New DataTable
            End If

            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Typology_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("ListOfTransaction")

                    'Display Header of CM
                    If drCM IsNot Nothing Then
                        ws.Cells("A1").Value = "List of Customer's Transactions"

                        ws.Cells("A2").Value = "CIF"
                        ws.Cells("B2").Value = drCM("CIF_No")

                        ws.Cells("A3").Value = "Customer Name"
                        ws.Cells("B3").Value = drCM("Customer_Name")

                        ws.Cells("A4").Value = "From Date"
                        ws.Cells("B4").Value = CDate(txt_SearchTransaction_From.SelectedDate).ToString("dd-MMM-yyyy")

                        ws.Cells("A5").Value = "To Date"
                        ws.Cells("B5").Value = CDate(txt_SearchTransaction_To.SelectedDate).ToString("dd-MMM-yyyy")

                        ws.Cells("A6").Value = "Generate Date"
                        ws.Cells("B6").Value = CDate(DateTime.Now).ToString("dd-MMM-yyyy HH:mm:ss")
                    End If

                    'objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    'objtbl.Columns("Account_NO").SetOrdinal(1)
                    'objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    'objtbl.Columns("Transaction_Remark").SetOrdinal(3)
                    'objtbl.Columns("IDR_Amount").SetOrdinal(4)
                    'objtbl.Columns("country_code").SetOrdinal(5)
                    'objtbl.Columns("country_code_lawan").SetOrdinal(6)

                    'objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                    'objtbl.Columns("Account_NO").ColumnName = "Account No"
                    'objtbl.Columns("Transmode_Code").ColumnName = "Transmode Code"
                    'objtbl.Columns("Transaction_Remark").ColumnName = "Transaction Remark"
                    'objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    'objtbl.Columns("country_code").ColumnName = "Country Code"
                    'objtbl.Columns("country_code_lawan").ColumnName = "Opp. Country Code"

                    '29-Mar-2022 Adi : Perubahan skema informasi yang ditampilkan
                    objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    objtbl.Columns("Ref_Num").SetOrdinal(1)
                    objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    objtbl.Columns("Transaction_Location").SetOrdinal(3)
                    objtbl.Columns("Debit_Credit").SetOrdinal(4)
                    objtbl.Columns("Currency").SetOrdinal(5)
                    objtbl.Columns("Original_Amount").SetOrdinal(6)
                    objtbl.Columns("IDR_Amount").SetOrdinal(7)
                    objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                    objtbl.Columns("Account_NO").SetOrdinal(9)
                    objtbl.Columns("CounterPartyType").SetOrdinal(10)
                    objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                    objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                    objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                    objtbl.Columns("CounterPartyName").SetOrdinal(14)
                    objtbl.Columns("Country_Lawan").SetOrdinal(15)

                    objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
                    objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
                    objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
                    objtbl.Columns("Transaction_Location").ColumnName = "Location"
                    objtbl.Columns("Debit_Credit").ColumnName = "D/C"
                    objtbl.Columns("Currency").ColumnName = "CCY"
                    objtbl.Columns("Original_Amount").ColumnName = "Amount"
                    objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                    objtbl.Columns("Account_NO").ColumnName = "Account No."
                    objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                    objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                    objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                    objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                    objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                    objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"

                    ws.Cells("A8").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=List of Transaction (CIF No " & drCM("CIF_No") & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Update 25-Feb-2022 Adi : Other Case Alerts related to Customer"
    Protected Sub store_ReadData_CaseAlert_Other(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            'Dim strQuery As String = "SELECT * FROM vw_OneFCC_CaseManagement"
            'strQuery += " WHERE PK_CaseManagement_ID <> " & Me.IDUnik
            'strQuery += " AND CIF_No = '" & Me.CIFNo & "'"

            '20-Jul-2022 Adi
            Dim strSQL As String = ""
            If CustomerType.ToLower() = "customer" Then
                strSQL = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            ElseIf CustomerType.ToLower() = "wic" Then
                strSQL = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & IDUnik
            End If
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

            Dim strQuery As String = ""
            If drCM IsNot Nothing Then
                If CustomerType.ToLower() = "customer" Then
                    strQuery = "SELECT * FROM vw_OneFCC_CaseManagement"
                ElseIf CustomerType.ToLower() = "wic" Then
                    strQuery = "SELECT * FROM vw_OneFCC_CaseManagement_For_WIC"
                End If
                strQuery += " WHERE PK_CaseManagement_ID <> " & Me.IDUnik
                If CustomerType.ToLower() = "customer" Then
                    strQuery += " AND CIF_No = '" & Me.CIFNo & "'"
                ElseIf CustomerType.ToLower() = "wic" Then
                    strQuery += " AND WIC_No = '" & Me.CIFNo & "'"
                End If

                If Not IsDBNull(drCM("Alert_Type")) Then
                    strQuery += " AND Alert_Type = '" & drCM("Alert_Type") & "'"
                End If

                If Not IsDBNull(drCM("FK_Rule_Basic_ID")) Then
                    strQuery += " AND FK_Rule_Basic_ID = '" & drCM("FK_Rule_Basic_ID") & "'"
                End If

            End If

            'End of 20-Jul-2022 Adi

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_OtherCase.GetStore.DataSource = DataPaging
            gp_OtherCase.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_OtherCase(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            If strCommandName = "Detail" Then
                LoadDataOtherCase(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDataOtherCase(strCaseID As String)
        Try
            'To Do Load Data Other Case
            Dim strSQL As String = ""
            If CustomerType.ToLower() = "customer" Then
                strSQL = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & strCaseID
            ElseIf CustomerType.ToLower() = "wic" Then
                strSQL = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & strCaseID
            End If
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            If drCM IsNot Nothing Then

                Me.CaseID_OtherCase = drCM("PK_CaseManagement_ID")

                'General Information
                txt_WindowOther_PK_CaseManagement_ID.Value = drCM("PK_CaseManagement_ID")
                txt_WindowOther_UniqueCMID.Value = drCM("Unique_CM_ID") ' 8 Maret 2023 Ari : update untuk set Unique CM ID

                txt_WindowOther_Case_Description.Value = drCM("Case_Description")
                txt_WindowOther_Alert_Type.Value = drCM("Alert_Type")

                If Not IsDBNull(drCM("FK_CaseStatus_ID")) Then
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_ProposedAction WHERE PK_OneFCC_CaseManagement_ProposedAction_ID=" & drCM("FK_CaseStatus_ID")
                    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drTemp IsNot Nothing Then
                        txt_WindowOther_FK_Case_Status_ID.Value = drTemp("PK_OneFCC_CaseManagement_ProposedAction_ID") & " - " & drTemp("Proposed_Action")
                    Else
                        txt_WindowOther_FK_Case_Status_ID.Value = "New Case"
                    End If
                Else
                    txt_WindowOther_FK_Case_Status_ID.Value = "New Case"
                End If

                If Not IsDBNull(drCM("ProcessDate")) Then
                    txt_WindowOther_ProcessDate.Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                End If

                txt_WindowOther_Workflow_Step.Value = drCM("Workflow_Step_Of")
                txt_WindowOther_PIC.Value = drCM("PIC")

                'Bind RFI sent
                BindDataRFI_OtherCase(drCM("PK_CaseManagement_ID"))

                'Reload GP Transaction
                gp_CaseAlert_Typology_Transaction_OtherCase.Hidden = True
                pnl_CaseAlert_Outlier_Transaction_OtherCase.Hidden = True

                'Load Transaction
                If drCM("Alert_Type") = "Typology Risk" Then
                    Dim strSQLTypology As String = "SELECT * FROM OneFCC_CaseManagement_Typology WHERE FK_CaseManagement_ID = " & Me.CaseID_OtherCase
                    Dim drCMTypology As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQLTypology)
                    If drCMTypology Is Nothing Then
                        Throw New ApplicationException("Could not find Case Manageent Typology, Please  report this to admin.")
                    End If
                    If IsDBNull(drCMTypology("Is_Transaction")) OrElse IsDBNull(drCMTypology("PK_OneFCC_CaseManagement_Typology_ID")) Then
                        Throw New ApplicationException("Is Transaction Or ID OneFCC_CaseManagement_Typology is null, please report this to admin.")
                    End If
                    If drCMTypology("Is_Transaction") = True Then
                        txt_WindowOther_checkbox_IsTransaction.Value = "True"
                        gp_CaseAlert_Typology_Transaction_OtherCase.GetStore.Reload()
                        gp_CaseAlert_Typology_Transaction_OtherCase.Hidden = False
                    ElseIf drCMTypology("Is_Transaction") = False Then
                        txt_WindowOther_checkbox_IsTransaction.Value = "False"
                        Dim strSQLActivity As String = " SELECT PK_OneFCC_CaseManagement_Typology_NonTransaction_ID, Date_Activity, Activity_Description, Account_NO "
                        strSQLActivity = strSQLActivity & " FROM OneFCC_CaseManagement_Typology_NonTransaction WHERE FK_OneFCC_CaseManagement_Typology_ID = " & drCMTypology("PK_OneFCC_CaseManagement_Typology_ID")
                        Dim dtActivity As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQLActivity, Nothing)
                        GridPanel_Activity_OtherCase.GetStore.DataSource = dtActivity
                        GridPanel_Activity_OtherCase.GetStore.DataBind()
                        panel_Activity_OtherCase.Hidden = False
                    End If
                ElseIf drCM("Alert_Type") = "Financial Risk" Then
                    gp_CaseAlert_Outlier_Transaction_OtherCase.GetStore.Reload()
                    pnl_CaseAlert_Outlier_Transaction_OtherCase.Hidden = False

                    'Display Mean dan Modus
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=2"
                    Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                        fs_CaseAlert_Outlier_Transaction_OtherCase.Title = "<b>Financial Statistics in past " & drParam("ParameterValue") & " month(s)</b>"
                    End If

                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Outlier WHERE FK_CaseManagement_ID=" & drCM("PK_CaseManagement_ID")
                    Dim drCMO As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drCMO IsNot Nothing Then
                        txt_MeanDebit_OtherCase.Value = CDbl(drCMO("Mean_Debit")).ToString("#,##0.00")
                        txt_MeanCredit_OtherCase.Value = CDbl(drCMO("Mean_Credit")).ToString("#,##0.00")

                        txt_ModusDebit_OtherCase.Value = CDbl(drCMO("Modus_Debit")).ToString("#,##0.00")
                        txt_ModusCredit_OtherCase.Value = CDbl(drCMO("Modus_Credit")).ToString("#,##0.00")
                    End If
                End If

                'Load Workflow History
                gp_WorkflowHistory_OtherCase.GetStore.Reload()

            End If

            'Show Window
            window_OtherCase.Hidden = False

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_OtherCase_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            window_OtherCase.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BindDataRFI_OtherCase(strCaseID As String)
        Try
            Dim strQuery As String = "SELECT * FROM OneFCC_CaseManagement_RFI WHERE FK_CaseManagement_ID=" & strCaseID
            Dim dtRFI As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If dtRFI Is Nothing Then
                dtRFI = New DataTable
            End If

            If dtRFI IsNot Nothing Then
                'Running the following script for additional columns
                dtRFI.Columns.Add(New DataColumn("EmailStatusName", GetType(String)))

                'Get reference Table
                strQuery = "SELECT * FROM EmailStatus"
                Dim dtEmailStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                For Each row As DataRow In dtRFI.Rows
                    If dtEmailStatus IsNot Nothing AndAlso Not IsDBNull(row("FK_EmailStatus_ID")) Then
                        Dim drCek = dtEmailStatus.Select("PK_EmailStatus_ID='" & row("FK_EmailStatus_ID") & "'").FirstOrDefault
                        If drCek IsNot Nothing Then
                            row("EmailStatusName") = drCek("EmailStatusName")
                        End If
                    End If
                Next

                'Bind to gridpanel
                gp_RFI_OtherCase.GetStore.DataSource = dtRFI
                gp_RFI_OtherCase.GetStore.DataBind()

            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub gc_RFI_OtherCase(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            If strCommandName = "Download" Then
                DownloadRFI(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Outlier_Transaction_OtherCase(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Outlier AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_OtherCase

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_OtherCase

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " AND " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Outlier_Transaction_OtherCase.GetStore.DataSource = DataPaging
            gp_CaseAlert_Outlier_Transaction_OtherCase.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Typology_Transaction_OtherCase(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_OtherCase

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_OtherCase

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " AND " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Typology_Transaction_OtherCase.GetStore.DataSource = DataPaging
            gp_CaseAlert_Typology_Transaction_OtherCase.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_WorkflowHistory_OtherCase(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT history.*, proposed.Proposed_Action FROM onefcc_casemanagement_workflowhistory history"
            strQuery += " LEFT JOIN OneFCC_CaseManagement_ProposedAction proposed ON history.FK_Proposed_Status_ID = proposed.PK_OneFCC_CaseManagement_ProposedAction_ID"
            strQuery += " WHERE FK_CaseManagement_ID = " & Me.CaseID_OtherCase

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)

            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_WorkflowHistory_OtherCase.GetStore.DataSource = DataPaging
            gp_WorkflowHistory_OtherCase.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#End Region

#Region "Update 04-Mar-2022 Adi : Download RFI Attachment from window detail"
    Protected Sub btn_DownloadEmailAttachment_Click()
        Try
            DownloadRFI(RFI_ID)
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Update 24-Mar-2022 Adi"
    Protected Sub btn_CustomerDetail_Click()
        Try
            If Not String.IsNullOrEmpty(Me.CIFNo) Then
                Dim strQuery As String = ""
                If CustomerType.ToLower() = "customer" Then
                    strQuery = "SELECT COUNT(1) AS JumCust FROM AML_CUSTOMER WHERE CIFNo = '" & Me.CIFNo & "'"
                ElseIf CustomerType.ToLower() = "wic" Then
                    strQuery = "SELECT COUNT(1) AS JumCust FROM goAML_Ref_WIC WHERE WIC_No = '" & Me.CIFNo & "'"
                End If
                If String.IsNullOrEmpty(strQuery) Then
                    Throw New ApplicationException("Customer Type Cannot be recognized, please report this to admin")
                End If
                Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                If drResult IsNot Nothing AndAlso Not IsDBNull(drResult("JumCust")) Then
                    If drResult("JumCust") > 0 Then
                        LoadCustomerDetail()
                        window_CustomerDetail.Hidden = False
                        'WindowPopUpDataCustomer.Hidden = False
                        'LoadDataAMLCustomerByCIF(txt_CIFNo_WICNo.Value)
                    Else
                        If CustomerType.ToLower() = "customer" Then
                            Throw New ApplicationException("No AML Customer Found With CIF No = " & Me.CIFNo)
                        ElseIf CustomerType.ToLower() = "wic" Then
                            Throw New ApplicationException("No WIC Found With WIC No = " & Me.CIFNo)
                        End If
                    End If
                Else
                    If CustomerType.ToLower() = "customer" Then
                        Throw New ApplicationException("There is Something wrong when seaching Data AML Customer, Please Report this to Admin")
                    ElseIf CustomerType.ToLower() = "wic" Then
                        Throw New ApplicationException("There is Something wrong when seaching Data WIC, Please Report this to Admin")
                    End If
                End If
            Else
                If CustomerType.ToLower() = "customer" Then
                    Throw New ApplicationException("CIF is Empty")
                ElseIf CustomerType.ToLower() = "wic" Then
                    Throw New ApplicationException("WIC is Empty")
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_CustomerDetail_Back_Click()
        Try
            window_CustomerDetail.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadCustomerDetail()
        Try

            'Get Customer Information
            txt_CustomerDetail_CIF.Value = Me.CIFNo
            txt_CustomerDetail_Name.Value = txt_Customer_Name.Value

            If CustomerType.ToLower() = "customer" Then
                txt_CustomerDetail_CIF.FieldLabel = "CIF No"
                txt_CustomerDetail_Name.FieldLabel = "Customer Name"
                txt_CustomerDetail_POB.FieldLabel = "Place of Birth"
                txt_CustomerDetail_DOB.FieldLabel = "Date of Birth"
                txt_CustomerDetail_Nationality.FieldLabel = "Citizenship"
                'txt_CustomerDetail_Occupation.FieldLabel = "Occupation"

                txt_CustomerDetail_Employer.FieldLabel = "Work Place" '"Employer"
                txt_CustomerDetail_LegalForm.FieldLabel = "Business Type" ' "Legal Form"
                txt_CustomerDetail_Industry.FieldLabel = "Industry"
                'txt_CustomerDetail_NPWP.FieldLabel = "Tax Number"
                txt_CustomerDetail_NPWP.Hidden = True
                txt_CustomerDetail_Income.FieldLabel = "Income Level"
                txt_CustomerDetail_PurposeOfFund.FieldLabel = "Purpose of Fund"
                txt_CustomerDetail_SourceOfFund.FieldLabel = "Source of Fund"

                txt_CustomerDetail_MotherName.FieldLabel = "Mother Name"
                txt_CustomerDetail_Gender.FieldLabel = "Gender"
                txt_CustomerDetail_MaritalStatus.FieldLabel = "Marital Status"
                txt_CustomerDetail_SubCustomerType.FieldLabel = "Customer Sub Type"
                txt_CustomerDetail_Industry.FieldLabel = "Industry"
                txt_CustomerDetail_OpeningDate.FieldLabel = "Opening Date"
                txt_CustomerDetail_CreationEntity.FieldLabel = "Creation Entity"
                txt_CustomerDetail_CreationBranch.FieldLabel = "Creation Branch"
                txt_CustomerDetail_SalesOfficer.FieldLabel = "Sales Officer"

                ''Hide untuk Customer  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                txt_CustomerDetail_Occupation.Hidden = True
                txt_CustomerDetail_NPWP.Hidden = True

                ''Show untuk Customer  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                txt_CustomerDetail_MotherName.Hidden = False
                txt_CustomerDetail_Gender.Hidden = False
                txt_CustomerDetail_MaritalStatus.Hidden = False
                txt_CustomerDetail_SubCustomerType.Hidden = False
                txt_CustomerDetail_Industry.Hidden = False
                txt_CustomerDetail_OpeningDate.Hidden = False
                txt_CustomerDetail_CreationEntity.Hidden = False
                txt_CustomerDetail_CreationBranch.Hidden = False
                txt_CustomerDetail_SalesOfficer.Hidden = False

                gridIdentityInfo.Hidden = False
                Dim strDateFormat As String = NawaBLL.SystemParameterBLL.GetDateFormat()
                Dim strQuery As String = ""

                strQuery = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & Me.CIFNo & "'"
                Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                If drCustomer IsNot Nothing Then
                    ''Display Customer Type  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                    If Not IsDBNull(drCustomer("FK_AML_Customer_Type_Code")) Then
                        strQuery = "SELECT TOP 1 Customer_Type_Name FROM AML_CUSTOMER_TYPE WHERE FK_AML_Customer_Type_Code ='" & drCustomer("FK_AML_Customer_Type_Code") & "'"
                        Dim drCustomerType As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundCustomerType As String = drCustomer("FK_AML_Customer_Type_Code")
                        If drCustomerType IsNot Nothing AndAlso Not IsDBNull(drCustomerType("Customer_Type_Name")) Then
                            strTempCompoundCustomerType = strTempCompoundCustomerType & " - " & drCustomerType("Customer_Type_Name")
                        End If
                        txt_CustomerDetail_TypeCode.Value = strTempCompoundCustomerType
                    Else
                        txt_CustomerDetail_TypeCode.Value = ""
                    End If

                    ''Display Customer Sub Type  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                    If Not IsDBNull(drCustomer("FK_AML_Customer_SUB_Type_Code")) Then
                        strQuery = "SELECT TOP 1 CUSTOMER_SUBTYPE_Name FROM AML_CUSTOMER_SUBTYPE WHERE FK_AML_CUSTOMER_SUBTYPE_Code ='" & drCustomer("FK_AML_Customer_SUB_Type_Code") & "'"
                        Dim drSubCustomerType As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundSubCustomerType As String = drCustomer("FK_AML_Customer_SUB_Type_Code")
                        If drSubCustomerType IsNot Nothing AndAlso Not IsDBNull(drSubCustomerType("CUSTOMER_SUBTYPE_Name")) Then
                            strTempCompoundSubCustomerType = strTempCompoundSubCustomerType & " - " & drSubCustomerType("CUSTOMER_SUBTYPE_Name")
                        End If
                        txt_CustomerDetail_SubCustomerType.Value = strTempCompoundSubCustomerType
                    Else
                        txt_CustomerDetail_SubCustomerType.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("PLACEOFBIRTH")) Then
                        txt_CustomerDetail_POB.Value = drCustomer("PLACEOFBIRTH")
                    Else
                        txt_CustomerDetail_POB.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("DATEOFBIRTH")) Then
                        txt_CustomerDetail_DOB.Value = CDate(drCustomer("DATEOFBIRTH")).ToString(strDateFormat)
                    Else
                        txt_CustomerDetail_DOB.Value = ""
                    End If

                    'txt_CustomerDetail_Nationality.Value = drCustomer("FK_AML_CITIZENSHIP_CODE")
                    If Not IsDBNull(drCustomer("FK_AML_CITIZENSHIP_CODE")) Then
                        strQuery = "SELECT TOP 1 * FROM AML_COUNTRY WHERE FK_AML_COUNTRY_Code ='" & drCustomer("FK_AML_CITIZENSHIP_CODE") & "'"
                        Dim drCountry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundCountry As String = drCustomer("FK_AML_CITIZENSHIP_CODE")
                        If drCountry IsNot Nothing AndAlso Not IsDBNull(drCountry("AML_COUNTRY_Name")) Then
                            strTempCompoundCountry = strTempCompoundCountry & " - " & drCountry("AML_COUNTRY_Name")
                        End If
                        txt_CustomerDetail_Nationality.Value = strTempCompoundCountry
                    Else
                        txt_CustomerDetail_Nationality.Value = ""
                    End If

                    'If Not IsDBNull(drCustomer("FK_AML_PEKERJAAN_CODE")) Then
                    '    strQuery = "SELECT TOP 1 * FROM AML_PEKERJAAN WHERE FK_AML_PEKERJAAN_CODE='" & drCustomer("FK_AML_PEKERJAAN_CODE") & "'"
                    '    Dim drOccupation As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                    '    If drOccupation IsNot Nothing Then
                    '        txt_CustomerDetail_Occupation.Value = drCustomer("FK_AML_PEKERJAAN_CODE") & " - " & drOccupation("AML_PEKERJAAN_Name")
                    '    Else
                    '        txt_CustomerDetail_Occupation.Value = drCustomer("FK_AML_PEKERJAAN_CODE")
                    '    End If
                    'Else
                    '    txt_CustomerDetail_Occupation.Value = ""
                    'End If

                    ''Display Mother Maiden  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                    If Not IsDBNull(drCustomer("MOTHERMAIDEN")) Then
                        txt_CustomerDetail_MotherName.Value = drCustomer("MOTHERMAIDEN")
                    Else
                        txt_CustomerDetail_MotherName.Value = ""
                    End If

                    ''Display Jenis Kelamin  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                    If Not IsDBNull(drCustomer("FK_AML_JenisKelamin_Code")) Then
                        strQuery = "SELECT TOP 1 Jenis_Kelamin_Name FROM AML_JENIS_KELAMIN WHERE FK_AML_Jenis_Kelamin_Code ='" & drCustomer("FK_AML_JenisKelamin_Code") & "'"
                        Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundGender As String = drCustomer("FK_AML_JenisKelamin_Code")
                        If drGender IsNot Nothing AndAlso Not IsDBNull(drGender("Jenis_Kelamin_Name")) Then
                            strTempCompoundGender = strTempCompoundGender & " - " & drGender("Jenis_Kelamin_Name")
                        End If
                        txt_CustomerDetail_Gender.Value = strTempCompoundGender
                    Else
                        txt_CustomerDetail_Gender.Value = ""
                    End If

                    ''Display Marital Status  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                    If Not IsDBNull(drCustomer("FK_AML_MARITAL_STATUS")) Then
                        strQuery = "SELECT TOP 1 MARITAL_STATUS_NAME FROM AML_MARITAL_STATUS WHERE FK_AML_MARITAL_STATUS_CODE ='" & drCustomer("FK_AML_MARITAL_STATUS") & "'"
                        Dim drMaritalStatus As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundMaritalStatus As String = drCustomer("FK_AML_MARITAL_STATUS")
                        If drMaritalStatus IsNot Nothing AndAlso Not IsDBNull(drMaritalStatus("MARITAL_STATUS_NAME")) Then
                            strTempCompoundMaritalStatus = strTempCompoundMaritalStatus & " - " & drMaritalStatus("MARITAL_STATUS_NAME")
                        End If
                        txt_CustomerDetail_MaritalStatus.Value = strTempCompoundMaritalStatus
                    Else
                        txt_CustomerDetail_MaritalStatus.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("WORK_PLACE")) Then
                        txt_CustomerDetail_Employer.Value = drCustomer("WORK_PLACE")
                    Else
                        txt_CustomerDetail_Employer.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("FK_AML_BENTUK_BADAN_USAHA_CODE")) Then
                        strQuery = "SELECT TOP 1 AML_BENTUK_BADAN_USAHA_Name FROM AML_BENTUK_BADAN_USAHA WHERE FK_AML_BENTUK_BADAN_USAHA_Code='" & drCustomer("FK_AML_BENTUK_BADAN_USAHA_CODE") & "'"
                        Dim drLegalForm As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                        If drLegalForm IsNot Nothing Then
                            txt_CustomerDetail_LegalForm.Value = drCustomer("FK_AML_BENTUK_BADAN_USAHA_CODE") & " - " & drLegalForm("AML_BENTUK_BADAN_USAHA_Name")
                        Else
                            txt_CustomerDetail_LegalForm.Value = drCustomer("FK_AML_BENTUK_BADAN_USAHA_CODE")
                        End If
                    Else
                        txt_CustomerDetail_LegalForm.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("FK_AML_INDUSTRY_CODE")) Then
                        strQuery = "SELECT TOP 1 * FROM AML_INDUSTRY WHERE FK_AML_INDUSTRY_CODE='" & drCustomer("FK_AML_INDUSTRY_CODE") & "'"
                        Dim drIndustry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                        If drIndustry IsNot Nothing Then
                            txt_CustomerDetail_Industry.Value = drCustomer("FK_AML_INDUSTRY_CODE") & " - " & drIndustry("AML_INDUSTRY_Name")
                        Else
                            txt_CustomerDetail_Industry.Value = drCustomer("FK_AML_INDUSTRY_CODE")
                        End If
                    Else
                        txt_CustomerDetail_Industry.Value = ""
                    End If

                    ''Remark NPWP  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                    'If Not IsDBNull(drCustomer("NPWP")) Then
                    '    txt_CustomerDetail_NPWP.Value = drCustomer("NPWP")
                    'Else
                    '    txt_CustomerDetail_NPWP.Value = ""
                    'End If

                    ''Display Income Level  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                    If Not IsDBNull(drCustomer("INCOME_LEVEL")) Then
                        strQuery = "SELECT TOP 1 AML_INCOME_VALUE_Name FROM AML_INCOME_VALUE WHERE FK_AML_INCOME_VALUE_Code ='" & drCustomer("INCOME_LEVEL") & "'"
                        Dim drIncomeLevel As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundIncomeLevel As String = drCustomer("INCOME_LEVEL")
                        If drIncomeLevel IsNot Nothing AndAlso Not IsDBNull(drIncomeLevel("AML_INCOME_VALUE_Name")) Then
                            strTempCompoundIncomeLevel = strTempCompoundIncomeLevel & " ( " & drIncomeLevel("AML_INCOME_VALUE_Name") & " ) "
                        End If
                        txt_CustomerDetail_Income.Value = strTempCompoundIncomeLevel ''CLng(drCustomer("INCOME_LEVEL")).ToString("#,##0.00")
                    Else
                        txt_CustomerDetail_Income.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("TUJUAN_DANA")) Then
                        txt_CustomerDetail_PurposeOfFund.Value = drCustomer("TUJUAN_DANA")
                    Else
                        txt_CustomerDetail_PurposeOfFund.Value = ""
                    End If

                    'Edit 2023-Jan-24, tambah refer ke AML_INCOME_TYPE
                    If Not IsDBNull(drCustomer("SOURCE_OF_FUND")) Then
                        strQuery = "SELECT TOP 1 AML_INCOME_TYPE_Name FROM AML_INCOME_TYPE WHERE FK_AML_INCOME_TYPE_Code ='" & drCustomer("SOURCE_OF_FUND") & "'"
                        Dim drIncomeType As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundIncomeLevel As String = drCustomer("SOURCE_OF_FUND")
                        If drIncomeType IsNot Nothing AndAlso Not IsDBNull(drIncomeType("AML_INCOME_TYPE_Name")) Then
                            strTempCompoundIncomeLevel = strTempCompoundIncomeLevel & " ( " & drIncomeType("AML_INCOME_TYPE_Name") & " ) "
                        End If
                        txt_CustomerDetail_SourceOfFund.Value = strTempCompoundIncomeLevel
                    Else
                        txt_CustomerDetail_SourceOfFund.Value = ""
                    End If

                    ''Display Opening Date Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                    If Not IsDBNull(drCustomer("OpeningDate")) Then
                        txt_CustomerDetail_OpeningDate.Value = CDate(drCustomer("OpeningDate")).ToString(strDateFormat)
                    Else
                        txt_CustomerDetail_OpeningDate.Value = ""
                    End If

                    ''Display Creation Entity  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                    If Not IsDBNull(drCustomer("FK_AML_Entity_Code")) Then
                        strQuery = "SELECT TOP 1 Keterangan FROM AML_Entity WHERE FK_AML_ENTITY_CODE ='" & drCustomer("FK_AML_Entity_Code") & "'"
                        Dim drCreationEntity As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundCreationEntity As String = drCustomer("FK_AML_Entity_Code")
                        If drCreationEntity IsNot Nothing AndAlso Not IsDBNull(drCreationEntity("Keterangan")) Then
                            strTempCompoundCreationEntity = strTempCompoundCreationEntity & " - " & drCreationEntity("Keterangan")
                        End If
                        txt_CustomerDetail_CreationEntity.Value = strTempCompoundCreationEntity
                    Else
                        txt_CustomerDetail_CreationEntity.Value = ""
                    End If

                    ''Display Creation Branch  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                    If Not IsDBNull(drCustomer("FK_AML_Creation_Branch_Code")) Then
                        strQuery = "SELECT TOP 1 BRANCH_NAME FROM AML_BRANCH WHERE FK_AML_BRANCH_CODE ='" & drCustomer("FK_AML_Creation_Branch_Code") & "'"
                        Dim drCreationBranch As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundCreationBranch As String = drCustomer("FK_AML_Creation_Branch_Code")
                        If drCreationBranch IsNot Nothing AndAlso Not IsDBNull(drCreationBranch("BRANCH_NAME")) Then
                            strTempCompoundCreationBranch = strTempCompoundCreationBranch & " - " & drCreationBranch("BRANCH_NAME")
                        End If
                        txt_CustomerDetail_CreationBranch.Value = strTempCompoundCreationBranch
                    Else
                        txt_CustomerDetail_CreationBranch.Value = ""
                    End If

                    ''Display Sales Officer  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                    If Not IsDBNull(drCustomer("FK_AML_SALES_OFFICER_CODE")) Then
                        strQuery = "SELECT TOP 1 SALESOFFICER_NAME FROM AML_SALESOFFICER WHERE FK_AML_SALESOFFICER_CODE ='" & drCustomer("FK_AML_SALES_OFFICER_CODE") & "'"
                        Dim drSalesOfficer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundSalesOfficer As String = drCustomer("FK_AML_SALES_OFFICER_CODE")
                        If drSalesOfficer IsNot Nothing AndAlso Not IsDBNull(drSalesOfficer("SALESOFFICER_NAME")) Then
                            strTempCompoundSalesOfficer = strTempCompoundSalesOfficer & " - " & drSalesOfficer("SALESOFFICER_NAME")
                        End If
                        txt_CustomerDetail_SalesOfficer.Value = strTempCompoundSalesOfficer
                    Else
                        txt_CustomerDetail_SalesOfficer.Value = ""
                    End If
                End If

                'Binding Address
                BindCustomerAddress()

                'Binding Contact/Phone
                BindCustomerContact()

                'Binding Identity
                BindCustomerIdentity()

            ElseIf CustomerType.ToLower() = "wic" Then
                txt_CustomerDetail_CIF.FieldLabel = "WIC No"
                txt_CustomerDetail_Name.FieldLabel = "WIC Name"
                Dim strQuery As String = "SELECT TOP 1 * FROM goAML_Ref_WIC WHERE WIC_No = '" & Me.CIFNo & "'"
                Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                If drResult IsNot Nothing Then
                    Dim strDateFormat As String = NawaBLL.SystemParameterBLL.GetDateFormat()

                    If Not IsDBNull(drResult("FK_Customer_Type_ID")) Then
                        strQuery = "SELECT TOP 1 * FROM goAML_Ref_Customer_Type WHERE PK_Customer_Type_ID = '" & drResult("FK_Customer_Type_ID") & "'"
                        Dim drCustomerType As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundCustomerType As String = drResult("FK_Customer_Type_ID")
                        If drCustomerType IsNot Nothing AndAlso Not IsDBNull(drCustomerType("Description")) Then
                            strTempCompoundCustomerType = strTempCompoundCustomerType & " - " & drCustomerType("Description")
                        End If
                        txt_CustomerDetail_TypeCode.Value = strTempCompoundCustomerType

                        If drResult("FK_Customer_Type_ID") = 1 Then
                            txt_CustomerDetail_POB.FieldLabel = "Place of Birth"
                            txt_CustomerDetail_DOB.FieldLabel = "Date of Birth"
                            txt_CustomerDetail_Nationality.FieldLabel = "Nationality"
                            txt_CustomerDetail_Occupation.FieldLabel = "Occupation"
                            txt_CustomerDetail_Employer.FieldLabel = "Employer"
                            txt_CustomerDetail_LegalForm.FieldLabel = "Gender"
                            txt_CustomerDetail_Industry.FieldLabel = "NIK"
                            txt_CustomerDetail_NPWP.FieldLabel = "Tax Number"
                            txt_CustomerDetail_Income.FieldLabel = "Is PEP ?"
                            txt_CustomerDetail_PurposeOfFund.FieldLabel = "Email"
                            txt_CustomerDetail_SourceOfFund.FieldLabel = "Source of Fund"
                            gridIdentityInfo.Hidden = False

                            ''Show untuk WIC  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                            txt_CustomerDetail_Occupation.Hidden = False
                            txt_CustomerDetail_NPWP.Hidden = False

                            ''Hide untuk WIC  Edit 17-Jan-2023 , Felix. FAMA minta yg Customer samakan dengan Customer Profile.
                            txt_CustomerDetail_MotherName.Hidden = True
                            txt_CustomerDetail_Gender.Hidden = True
                            txt_CustomerDetail_MaritalStatus.Hidden = True
                            txt_CustomerDetail_SubCustomerType.Hidden = True
                            txt_CustomerDetail_Industry.Hidden = True
                            txt_CustomerDetail_OpeningDate.Hidden = True
                            txt_CustomerDetail_CreationEntity.Hidden = True
                            txt_CustomerDetail_CreationBranch.Hidden = True
                            txt_CustomerDetail_SalesOfficer.Hidden = True

                            If Not IsDBNull(drResult("INDV_Birth_Place")) Then
                                txt_CustomerDetail_POB.Value = drResult("INDV_Birth_Place")
                            Else
                                txt_CustomerDetail_POB.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_BirthDate")) Then
                                txt_CustomerDetail_DOB.Value = CDate(drResult("INDV_BirthDate")).ToString(strDateFormat)
                            Else
                                txt_CustomerDetail_DOB.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Nationality1")) Then
                                strQuery = "SELECT TOP 1 * FROM goAML_Ref_Nama_Negara WHERE Kode ='" & drResult("INDV_Nationality1") & "'"
                                Dim drCountry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                Dim strTempCompoundCountry As String = drResult("INDV_Nationality1")
                                If drCountry IsNot Nothing AndAlso Not IsDBNull(drCountry("Keterangan")) Then
                                    strTempCompoundCountry = strTempCompoundCountry & " - " & drCountry("Keterangan")
                                End If
                                txt_CustomerDetail_Nationality.Value = strTempCompoundCountry
                            Else
                                txt_CustomerDetail_Nationality.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Occupation")) Then
                                txt_CustomerDetail_Occupation.Value = drResult("INDV_Occupation")
                            Else
                                txt_CustomerDetail_Occupation.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Employer_Name")) Then
                                txt_CustomerDetail_Employer.Value = drResult("INDV_Employer_Name")
                            Else
                                txt_CustomerDetail_Employer.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Gender")) Then
                                strQuery = "SELECT TOP 1 * FROM goAML_Ref_Jenis_Kelamin WHERE Kode ='" & drResult("INDV_Gender") & "'"
                                Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                Dim strTempCompoundGender As String = drResult("INDV_Gender")
                                If drGender IsNot Nothing AndAlso Not IsDBNull(drGender("Keterangan")) Then
                                    strTempCompoundGender = strTempCompoundGender & " - " & drGender("Keterangan")
                                End If
                                txt_CustomerDetail_LegalForm.Value = strTempCompoundGender
                            Else
                                txt_CustomerDetail_LegalForm.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_SSN")) Then
                                txt_CustomerDetail_Industry.Value = drResult("INDV_SSN")
                            Else
                                txt_CustomerDetail_Industry.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Tax_Number")) Then
                                txt_CustomerDetail_NPWP.Value = drResult("INDV_Tax_Number")
                            Else
                                txt_CustomerDetail_NPWP.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Tax_Reg_Number")) Then
                                If drResult("INDV_Tax_Reg_Number") = 1 Then
                                    txt_CustomerDetail_Income.Value = "True"
                                Else
                                    txt_CustomerDetail_Income.Value = "False"
                                End If
                            Else
                                txt_CustomerDetail_Income.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Email")) Then
                                txt_CustomerDetail_PurposeOfFund.Value = drResult("INDV_Email")
                            Else
                                txt_CustomerDetail_PurposeOfFund.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_SumberDana")) Then
                                txt_CustomerDetail_SourceOfFund.Value = drResult("INDV_SumberDana")
                            Else
                                txt_CustomerDetail_SourceOfFund.Value = ""
                            End If

                        ElseIf drResult("FK_Customer_Type_ID") = 2 Then
                            txt_CustomerDetail_POB.FieldLabel = "Province of Establishment"
                            txt_CustomerDetail_DOB.FieldLabel = "Date of Establishment"
                            txt_CustomerDetail_Nationality.FieldLabel = "Country of Establishment"
                            txt_CustomerDetail_Occupation.FieldLabel = "Is Closed ?"
                            txt_CustomerDetail_Employer.FieldLabel = "Closed Date"
                            txt_CustomerDetail_LegalForm.FieldLabel = "Legal Form"
                            txt_CustomerDetail_Industry.FieldLabel = "Industry"
                            txt_CustomerDetail_NPWP.FieldLabel = "Tax Number"
                            txt_CustomerDetail_Income.FieldLabel = "Incorporation Number"
                            txt_CustomerDetail_PurposeOfFund.FieldLabel = "Corporation Website"
                            txt_CustomerDetail_SourceOfFund.FieldLabel = "Corporation Email"
                            gridIdentityInfo.Hidden = True

                            If Not IsDBNull(drResult("Corp_Incorporation_State")) Then
                                txt_CustomerDetail_POB.Value = drResult("Corp_Incorporation_State")
                            Else
                                txt_CustomerDetail_POB.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Incorporation_Date")) Then
                                txt_CustomerDetail_DOB.Value = CDate(drResult("Corp_Incorporation_Date")).ToString(strDateFormat)
                            Else
                                txt_CustomerDetail_DOB.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Incorporation_Country_Code")) Then
                                strQuery = "SELECT TOP 1 * FROM goAML_Ref_Nama_Negara WHERE Kode ='" & drResult("Corp_Incorporation_Country_Code") & "'"
                                Dim drCountry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                Dim strTempCompoundCountry As String = drResult("Corp_Incorporation_Country_Code")
                                If drCountry IsNot Nothing AndAlso Not IsDBNull(drCountry("Keterangan")) Then
                                    strTempCompoundCountry = strTempCompoundCountry & " - " & drCountry("Keterangan")
                                End If
                                txt_CustomerDetail_Nationality.Value = strTempCompoundCountry
                            Else
                                txt_CustomerDetail_Nationality.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Business_Closed")) Then
                                If drResult("Corp_Business_Closed") = 1 Then
                                    txt_CustomerDetail_Occupation.Value = "True"
                                Else
                                    txt_CustomerDetail_Occupation.Value = "False"
                                End If
                            Else
                                txt_CustomerDetail_Occupation.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Date_Business_Closed")) Then
                                txt_CustomerDetail_Employer.Value = CDate(drResult("Corp_Date_Business_Closed")).ToString(strDateFormat)
                            Else
                                txt_CustomerDetail_Employer.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Incorporation_Legal_Form")) Then
                                strQuery = "SELECT TOP 1 * FROM goAML_Ref_Bentuk_Badan_Usaha WHERE Kode ='" & drResult("Corp_Incorporation_Legal_Form") & "'"
                                Dim drBadanUsaha As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                Dim strTempCompoundBadanUsaha As String = drResult("Corp_Incorporation_Legal_Form")
                                If drBadanUsaha IsNot Nothing AndAlso Not IsDBNull(drBadanUsaha("Keterangan")) Then
                                    strTempCompoundBadanUsaha = strTempCompoundBadanUsaha & " - " & drBadanUsaha("Keterangan")
                                End If
                                txt_CustomerDetail_LegalForm.Value = strTempCompoundBadanUsaha
                            Else
                                txt_CustomerDetail_LegalForm.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Business")) Then
                                txt_CustomerDetail_Industry.Value = drResult("Corp_Business")
                            Else
                                txt_CustomerDetail_Industry.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Tax_Number")) Then
                                txt_CustomerDetail_NPWP.Value = drResult("Corp_Tax_Number")
                            Else
                                txt_CustomerDetail_NPWP.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Incorporation_Number")) Then
                                txt_CustomerDetail_Income.Value = drResult("Corp_Incorporation_Number")
                            Else
                                txt_CustomerDetail_Income.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Url")) Then
                                txt_CustomerDetail_PurposeOfFund.Value = drResult("Corp_Url")
                            Else
                                txt_CustomerDetail_PurposeOfFund.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Email")) Then
                                txt_CustomerDetail_SourceOfFund.Value = drResult("Corp_Email")
                            Else
                                txt_CustomerDetail_SourceOfFund.Value = ""
                            End If
                        End If
                    Else
                        txt_CustomerDetail_TypeCode.Value = ""
                    End If


                    'Binding Address
                    BindWICAddress(drResult("PK_Customer_ID"))

                    'Binding Contact/Phone
                    BindWICContact(drResult("PK_Customer_ID"))

                    'Binding Identity
                    BindWICIdentity(drResult("PK_Customer_ID"))
                Else
                    Throw New ApplicationException("No WIC Data Found With WIC No = " & Me.CIFNo)
                End If
            Else
                Throw New ApplicationException("Customer Type are not identified, please report this to admin")
            End If

            'Dim strQuery As String = ""

            ''Reference Table
            'strQuery = "SELECT * FROM AML_COUNTRY"
            'Dim dtCountry As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            ''Get Customer Information
            'txt_CustomerDetail_CIF.Value = txt_CIF_No.Value
            'txt_CustomerDetail_Name.Value = txt_Customer_Name.Value

            'strQuery = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & Me.CIFNo & "'"
            'Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            'If drCustomer IsNot Nothing Then
            '    txt_CustomerDetail_TypeCode.Value = drCustomer("FK_AML_Customer_Type_Code")
            '    txt_CustomerDetail_POB.Value = drCustomer("PLACEOFBIRTH")
            '    If Not IsDBNull(drCustomer("DATEOFBIRTH")) Then
            '        txt_CustomerDetail_DOB.Value = CDate(drCustomer("DATEOFBIRTH")).ToString("dd-MMM-yyyy")
            '    End If

            '    txt_CustomerDetail_Nationality.Value = drCustomer("FK_AML_CITIZENSHIP_CODE")
            '    If dtCountry IsNot Nothing AndAlso Not IsDBNull(drCustomer("FK_AML_CITIZENSHIP_CODE")) Then
            '        Dim drCek = dtCountry.Select("FK_AML_COUNTRY_CODE='" & drCustomer("FK_AML_CITIZENSHIP_CODE") & "'").FirstOrDefault
            '        If drCek IsNot Nothing Then
            '            txt_CustomerDetail_Nationality.Value = drCustomer("FK_AML_CITIZENSHIP_CODE") & " - " & drCek("AML_COUNTRY_Name")
            '        End If
            '    End If

            '    If Not IsDBNull(drCustomer("FK_AML_PEKERJAAN_CODE")) Then
            '        strQuery = "SELECT TOP 1 * FROM AML_PEKERJAAN WHERE FK_AML_PEKERJAAN_CODE='" & drCustomer("FK_AML_PEKERJAAN_CODE") & "'"
            '        Dim drOccupation As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            '        If drOccupation IsNot Nothing Then
            '            txt_CustomerDetail_Occupation.Value = drCustomer("FK_AML_PEKERJAAN_CODE") & " - " & drOccupation("AML_PEKERJAAN_Name")
            '        Else
            '            txt_CustomerDetail_Occupation.Value = drCustomer("FK_AML_PEKERJAAN_CODE")
            '        End If
            '    End If

            '    txt_CustomerDetail_Employer.Value = drCustomer("WORK_PLACE")

            '    txt_CustomerDetail_LegalForm.Value = drCustomer("FK_AML_BENTUK_BADAN_USAHA_CODE")
            '    If Not IsDBNull(drCustomer("FK_AML_INDUSTRY_CODE")) Then
            '        strQuery = "SELECT TOP 1 * FROM AML_INDUSTRY WHERE FK_AML_INDUSTRY_CODE='" & drCustomer("FK_AML_INDUSTRY_CODE") & "'"
            '        Dim drIndustry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            '        If drIndustry IsNot Nothing Then
            '            txt_CustomerDetail_Industry.Value = drCustomer("FK_AML_INDUSTRY_CODE") & " - " & drIndustry("AML_INDUSTRY_Name")
            '        Else
            '            txt_CustomerDetail_Industry.Value = drCustomer("FK_AML_INDUSTRY_CODE")
            '        End If
            '    End If

            '    txt_CustomerDetail_NPWP.Value = drCustomer("NPWP")
            '    If Not IsDBNull(drCustomer("INCOME_LEVEL")) Then
            '        txt_CustomerDetail_Income.Value = CLng(drCustomer("INCOME_LEVEL")).ToString("#,##0.00")
            '    End If
            '    txt_CustomerDetail_PurposeOfFund.Value = drCustomer("TUJUAN_DANA")
            '    txt_CustomerDetail_SourceOfFund.Value = drCustomer("SOURCE_OF_FUND")
            'End If

            ''Binding Address
            'BindCustomerAddress()

            ''Binding Contact/Phone
            'BindCustomerContact()

            ''Binding Identity
            'BindCustomerIdentity()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindCustomerAddress()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_COUNTRY"
            Dim dtCountry As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            strQuery = "SELECT * FROM AML_ADDRESS_TYPE"
            Dim dtAddressType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding Address
            strQuery = "SELECT * FROM AML_CUSTOMER_ADDRESS WHERE CIFNO='" & Me.CIFNo & "'"
            Dim dtAddress As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtAddress Is Nothing Then
                dtAddress = New DataTable
            End If
            dtAddress.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))
            dtAddress.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))

            For Each item In dtAddress.Rows
                item("AML_ADDRESS_TYPE_NAME") = item("FK_AML_ADDRESS_TYPE_CODE")
                If dtAddressType IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_ADDRESS_TYPE_CODE")) Then
                    Dim drCek = dtAddressType.Select("FK_AML_ADDRES_TYPE_CODE='" & item("FK_AML_ADDRESS_TYPE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("AML_ADDRESS_TYPE_NAME") = item("FK_AML_ADDRESS_TYPE_CODE") & " - " & drCek("ADDRESS_TYPE_NAME")
                    End If
                End If

                item("COUNTRY_NAME") = item("FK_AML_COUNTRY_CODE")
                If dtCountry IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_COUNTRY_CODE")) Then
                    Dim drCek = dtCountry.Select("FK_AML_COUNTRY_CODE='" & item("FK_AML_COUNTRY_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("COUNTRY_NAME") = item("FK_AML_COUNTRY_CODE") & " - " & drCek("AML_COUNTRY_Name")
                    End If
                End If
            Next

            gridAddressInfo.GetStore.DataSource = dtAddress
            gridAddressInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindCustomerContact()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_CONTACT_TYPE"
            Dim dtContactType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding Contact
            strQuery = "SELECT * FROM AML_CUSTOMER_CONTACT WHERE CIFNO='" & Me.CIFNo & "'"
            Dim dtContact As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtContact Is Nothing Then
                dtContact = New DataTable
            End If
            dtContact.Columns.Add(New DataColumn("AML_CONTACT_TYPE_NAME", GetType(String)))

            For Each item In dtContact.Rows
                item("AML_CONTACT_TYPE_NAME") = item("FK_AML_CONTACT_TYPE_CODE")
                If dtContactType IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_CONTACT_TYPE_CODE")) Then
                    Dim drCek = dtContactType.Select("FK_AML_CONTACT_TYPE_CODE='" & item("FK_AML_CONTACT_TYPE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("AML_CONTACT_TYPE_NAME") = item("FK_AML_CONTACT_TYPE_CODE") & " - " & drCek("CONTACT_TYPE_NAME")
                    End If
                End If
            Next

            gridPhoneInfo.GetStore.DataSource = dtContact
            gridPhoneInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindCustomerIdentity()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_IDENTITY_TYPE"
            Dim dtIDENTITYType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            strQuery = "SELECT * FROM AML_COUNTRY"
            Dim dtCountry As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding IDENTITY
            strQuery = "SELECT * FROM AML_CUSTOMER_IDENTITY WHERE CIFNO='" & Me.CIFNo & "'"
            Dim dtIDENTITY As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtIDENTITY Is Nothing Then
                dtIDENTITY = New DataTable
            End If
            dtIDENTITY.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
            dtIDENTITY.Columns.Add(New DataColumn("IDENTITY_TYPE_NAME", GetType(String)))

            For Each item In dtIDENTITY.Rows
                item("IDENTITY_TYPE_NAME") = item("FK_AML_IDENTITY_TYPE_CODE")
                If dtIDENTITYType IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_IDENTITY_TYPE_CODE")) Then
                    Dim drCek = dtIDENTITYType.Select("FK_AML_IDENTITY_TYPE_CODE='" & item("FK_AML_IDENTITY_TYPE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("IDENTITY_TYPE_NAME") = item("FK_AML_IDENTITY_TYPE_CODE") & " - " & drCek("IDENTITY_TYPE_NAME")
                    End If
                End If
                item("COUNTRY_NAME") = item("FK_AML_COUNTRYISSUE_CODE")
                If dtCountry IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_COUNTRYISSUE_CODE")) Then
                    Dim drCek = dtCountry.Select("FK_AML_COUNTRY_CODE='" & item("FK_AML_COUNTRYISSUE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("COUNTRY_NAME") = item("FK_AML_COUNTRYISSUE_CODE") & " - " & drCek("AML_COUNTRY_Name")
                    End If
                End If
            Next

            gridIdentityInfo.GetStore.DataSource = dtIDENTITY
            gridIdentityInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Update 29-Mar-2022 Adi : Add Re-Open"
    Protected Sub SetReOpenAccess()
        Try
            btn_ReOpen.Hidden = True

            Dim strSQL As String = ""
            Dim bolAllowedReOpen As Boolean = False

            'Cek Parameter for Re-Opening Authorized users
            strSQL = "SELECT * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=14"
            Dim drAuthorized As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            If drAuthorized IsNot Nothing Then
                If Not IsDBNull(drAuthorized("ParameterValue")) Then
                    If CheckIfStringExists(NawaBLL.Common.SessionCurrentUser.UserID, drAuthorized("ParameterValue")) Then
                        bolAllowedReOpen = True
                    End If
                End If

                'Cek Status CM. Yang bisa Re-Open hanya status 4=Non Issue dan 7=Closed by System
                If CustomerType.ToLower() = "customer" Then
                    strSQL = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
                ElseIf CustomerType.ToLower() = "wic" Then
                    strSQL = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & IDUnik
                End If
                Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                If drCM IsNot Nothing Then
                    If drCM("FK_CaseStatus_ID") = 4 Or drCM("FK_CaseStatus_ID") = 7 Then
                        bolAllowedReOpen = True
                    Else
                        bolAllowedReOpen = False
                    End If
                Else
                    bolAllowedReOpen = False
                End If
            End If

            If bolAllowedReOpen Then
                btn_ReOpen.Hidden = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_ReOpen_Click()
        Try
            txt_ReOpenReason.Value = Nothing
            window_ReOpen.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_ReOpen_Save_Click()
        Try
            'Validate 
            If String.IsNullOrWhiteSpace(txt_ReOpenReason.Value) Then
                Throw New ApplicationException(txt_ReOpenReason.FieldLabel & " is required.")
            End If

            'Local Variables
            Dim strSelectedPIC As String = ""
            Dim strQuery As String = ""

            'Get PIC for Workflow Step 1
            strQuery = "SELECT * FROM OneFCC_CaseManagement_WorkflowDetail"
            strQuery &= " WHERE FK_CaseManagement_Workflow_ID=" & Me.Workflow_ID
            strQuery &= " AND Workflow_Step=1"

            Dim drPIC = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            If drPIC IsNot Nothing AndAlso Not IsDBNull(drPIC("PIC")) Then
                strSelectedPIC = drPIC("PIC")
            End If

            'Change the PIC with current user
            strQuery = "UPDATE OneFCC_CaseManagement SET"
            strQuery &= " FK_CaseStatus_ID = 8"
            strQuery &= ", FK_Proposed_Status_ID = 8"
            strQuery &= ", Workflow_Step = 1"
            strQuery &= ", PIC = '" & strSelectedPIC & "'"
            strQuery &= ", LastUpdateBy='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery &= ", LastUpdateDate=GETDATE()"
            strQuery &= " WHERE PK_CaseManagement_ID=" & IDUnik
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            txt_PIC.Value = strSelectedPIC

            'Insert into Workflow History
            strQuery = "INSERT INTO OneFCC_CaseManagement_WorkflowHistory ("
            strQuery &= "FK_CaseManagement_ID,"
            strQuery &= "Workflow_Step,"
            strQuery &= "FK_Proposed_Status_ID,"
            strQuery &= "Analysis_Result,"
            strQuery &= "CreatedBy,"
            strQuery &= "CreatedDate) "
            strQuery &= "VALUES ("
            strQuery &= Me.IDUnik & ","
            strQuery &= Me.Workflow_Step & ","
            strQuery &= "8,"
            strQuery &= "'Case Re-Open<br>Reason : " & txt_ReOpenReason.Value & "',"
            strQuery &= "'" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
            strQuery &= "GETDATE())"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Hide window
            window_ReOpen.Hidden = True

            'Show Confirmation
            LblConfirmation.Text = "Case has been successfully Re-Opened and back to Workflow Step 1."
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_ReOpen_Back_Click()
        Try
            window_ReOpen.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "14-Nov-2022 Penambahan WIC(Non-Customer)"
    Protected Sub LoadColumnDataTabelActivity()
        Try
            DataTabelActivity.Columns.Add(New DataColumn("PK_OneFCC_CaseManagement_Typology_NonTransaction_ID", GetType(Long)))
            DataTabelActivity.Columns.Add(New DataColumn("Date_Activity", GetType(Date)))
            DataTabelActivity.Columns.Add(New DataColumn("Activity_Description", GetType(String)))
            DataTabelActivity.Columns.Add(New DataColumn("Account_NO", GetType(String)))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub GridCommandActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            PK_Activity = CLng(ID)
            If e.ExtraParams(1).Value = "Delete" Then
                Dim strselect As String = "PK_OneFCC_CaseManagement_Typology_NonTransaction_ID = " & PK_Activity
                Dim dtrow As DataRow = DataTabelActivity.Select(strselect).FirstOrDefault()
                Dim indexrow As Integer = DataTabelActivity.Rows.IndexOf(dtrow)
                DataTabelActivity.Rows(indexrow).Delete()
                BindTableActivity()
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                ActionActivity = enumActionForm.Edit
                ClearWindowActivity()
                LoadDataActivity()
                WindowActivity.Hidden = False
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                ActionActivity = enumActionForm.Detail
                ClearWindowActivity()
                LoadDataActivity()
                WindowActivity.Hidden = False
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ClearWindowActivity()
        Try
            nDSDropDownField_AccountNo.Value = ""
            activity_Date.Value = ""
            activity_Desc.Value = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadDataActivity()
        Try
            Dim strDateFormat As String = NawaBLL.SystemParameterBLL.GetDateFormat()
            Dim strselect As String = "PK_OneFCC_CaseManagement_Typology_NonTransaction_ID = " & PK_Activity
            Dim dtrow As DataRow = DataTabelActivity.Select(strselect).FirstOrDefault()
            If Not IsDBNull(dtrow("Date_Activity")) Then
                activity_Date.Value = CDate(dtrow("Date_Activity")).ToString(strDateFormat)
            End If
            If Not IsDBNull(dtrow("Activity_Description")) Then
                activity_Desc.Value = dtrow("Activity_Description")
            End If
            If CustomerType.ToLower() = "customer" Then
                If Not IsDBNull(dtrow("Account_NO")) Then
                    nDSDropDownField_AccountNo.Value = dtrow("Account_NO")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindTableActivity()
        Try
            GridPanel_Activity.GetStore.DataSource = DataTabelActivity
            GridPanel_Activity.GetStore.DataBind()
            'GridPanel_Activity.Update()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Btn_Activity_Back_Click()
        Try
            WindowActivity.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BindWICAddress(PK_WIC As Long)
        Try
            Dim strQuery As String = ""

            'Binding Address
            strQuery = " SELECT "
            strQuery = strQuery & " addresstype.Keterangan AS AML_ADDRESS_TYPE_NAME "
            strQuery = strQuery & " , refaddress.Address AS CUSTOMER_ADDRESS "
            strQuery = strQuery & " , refaddress.Country_Code AS FK_AML_COUNTRY_CODE "
            strQuery = strQuery & " , namanegara.Keterangan AS COUNTRY_NAME "
            strQuery = strQuery & " , refaddress.Zip AS KODEPOS "
            strQuery = strQuery & " , refaddress.Town AS KECAMATAN "
            strQuery = strQuery & " , refaddress.City AS KOTAKABUPATEN "
            strQuery = strQuery & " FROM goaml_ref_address refaddress "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Kategori_Kontak addresstype "
            strQuery = strQuery & " ON refaddress.Address_Type = addresstype.Kode "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Nama_Negara namanegara "
            strQuery = strQuery & " ON refaddress.Country_Code = namanegara.Kode "
            strQuery = strQuery & " WHERE refaddress.FK_Ref_Detail_Of = 3 "
            strQuery = strQuery & " AND refaddress.FK_To_Table_ID = " & PK_WIC
            Dim dtAddress As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            'If dtAddress Is Nothing Then
            '    dtAddress = New DataTable
            'End If

            gridAddressInfo.GetStore.DataSource = dtAddress
            gridAddressInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindWICContact(PK_WIC As Long)
        Try
            Dim strQuery As String = ""

            'Binding Contact
            strQuery = " SELECT "
            strQuery = strQuery & " kontaktype.Keterangan AS AML_CONTACT_TYPE_NAME "
            strQuery = strQuery & " , alatkomunikasi.Keterangan AS FK_AML_COMMUNICATION_TYPE_CODE "
            strQuery = strQuery & " , refphone.tph_country_prefix AS AREA_CODE "
            strQuery = strQuery & " , refphone.tph_number AS CUSTOMER_CONTACT_NUMBER "
            strQuery = strQuery & " , refphone.tph_extension AS EXTENTION "
            strQuery = strQuery & " , refphone.comments AS NOTES "
            strQuery = strQuery & " FROM goAML_Ref_Phone refphone "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Kategori_Kontak kontaktype "
            strQuery = strQuery & " ON refphone.Tph_Contact_Type = kontaktype.Kode "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Jenis_Alat_Komunikasi alatkomunikasi "
            strQuery = strQuery & " ON refphone.Tph_Communication_Type = alatkomunikasi.Kode "
            strQuery = strQuery & " WHERE FK_Ref_Detail_Of = 3 "
            strQuery = strQuery & " AND FK_for_Table_ID = " & PK_WIC
            Dim dtContact As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            'If dtContact Is Nothing Then
            '    dtContact = New DataTable
            'End If

            gridPhoneInfo.GetStore.DataSource = dtContact
            gridPhoneInfo.GetStore.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindWICIdentity(PK_WIC As Long)
        Try
            Dim strQuery As String = ""

            'Binding IDENTITY
            strQuery = " SELECT "
            strQuery = strQuery & " identification.Number AS CUSTOMER_IDENTITY_NUMBER "
            strQuery = strQuery & " , identification.Type AS FK_AML_IDENTITY_TYPE_CODE "
            strQuery = strQuery & " , identification.Issue_Date AS DATE_OF_ISSUE "
            strQuery = strQuery & " , identification.Expiry_Date AS DATE_OF_EXPIRED "
            strQuery = strQuery & " , identification.Issued_By AS ISSUE_By "
            strQuery = strQuery & " , identification.Issued_Country AS fk_aml_countryissue_code "
            strQuery = strQuery & " , identification.Identification_Comment AS NOTES "
            strQuery = strQuery & " , identificationtype.Keterangan AS IDENTITY_TYPE_NAME "
            strQuery = strQuery & " , namanegara.Keterangan AS COUNTRY_NAME "
            strQuery = strQuery & " FROM goAML_Person_Identification identification "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Jenis_Dokumen_Identitas identificationtype "
            strQuery = strQuery & " ON identification.Type = identificationtype.Kode "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Nama_Negara namanegara "
            strQuery = strQuery & " ON identification.Issued_Country = namanegara.Kode "
            strQuery = strQuery & " WHERE FK_Person_Type = 8 "
            strQuery = strQuery & " AND FK_Person_ID = " & PK_WIC
            Dim dtIDENTITY As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            'If dtIDENTITY Is Nothing Then
            '    dtIDENTITY = New DataTable
            'End If

            gridIdentityInfo.GetStore.DataSource = dtIDENTITY
            gridIdentityInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub GridCommandActivity_OtherCase_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                ClearWindowActivity()
                Dim strDateFormat As String = NawaBLL.SystemParameterBLL.GetDateFormat()
                Dim strSQLActivity As String = " SELECT PK_OneFCC_CaseManagement_Typology_NonTransaction_ID, Date_Activity, Activity_Description, Account_NO "
                strSQLActivity = strSQLActivity & " FROM OneFCC_CaseManagement_Typology_NonTransaction WHERE PK_OneFCC_CaseManagement_Typology_NonTransaction_ID = " & ID
                Dim drActivity As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQLActivity)
                If drActivity IsNot Nothing Then
                    If Not IsDBNull(drActivity("Date_Activity")) Then
                        activity_Date.Value = CDate(drActivity("Date_Activity")).ToString(strDateFormat)
                    End If
                    If Not IsDBNull(drActivity("Activity_Description")) Then
                        activity_Desc.Value = drActivity("Activity_Description")
                    End If
                    If CustomerType.ToLower() = "customer" Then
                        If Not IsDBNull(drActivity("Account_NO")) Then
                            nDSDropDownField_AccountNo.Value = drActivity("Account_NO")
                        End If
                    End If
                End If
                WindowActivity.Hidden = False
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class