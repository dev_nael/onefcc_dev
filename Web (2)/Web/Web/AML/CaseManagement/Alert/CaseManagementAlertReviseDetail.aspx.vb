﻿Imports Ext
Imports Elmah
Imports System.Data
Imports CasemanagementBLL
Imports CasemanagementDAL
Imports System.Data.SqlClient
Imports OfficeOpenXml
Imports System.Reflection

Partial Class CaseManagementAlertReviseDetail
    Inherits ParentPage

    Public Property IDModule() As String
        Get
            Return Session("CaseManagementAlertReviseDetail.IDModule")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertReviseDetail.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("CaseManagementAlertReviseDetail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertReviseDetail.IDUnik") = value
        End Set
    End Property

    Public Property IntJumlahDataTransaksi() As Integer
        Get
            Return Session("CaseManagementAlertReviseDetail.IntJumlahDataTransaksi")
        End Get
        Set(ByVal value As Integer)
            Session("CaseManagementAlertReviseDetail.IntJumlahDataTransaksi") = value
        End Set
    End Property

    Public Property CustomerType() As String
        Get
            Return Session("CaseManagementAlertReviseDetail.CustomerType")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertReviseDetail.CustomerType") = value
        End Set
    End Property

    Public Property DataTabelActivity As DataTable
        Get
            Return Session("CaseManagementAlertReviseDetail.DataTabelActivity")
        End Get
        Set(ByVal value As DataTable)
            Session("CaseManagementAlertReviseDetail.DataTabelActivity") = value
        End Set
    End Property

    Public Property ActionActivity() As enumActionForm
        Get
            Return Session("CaseManagementAlertReviseDetail.ActionActivity")
        End Get
        Set(ByVal value As enumActionForm)
            Session("CaseManagementAlertReviseDetail.ActionActivity") = value
        End Set
    End Property

    Public Property PK_Activity_NewID() As Long
        Get
            Return Session("CaseManagementAlertReviseDetail.PK_Activity_NewID")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertReviseDetail.PK_Activity_NewID") = value
        End Set
    End Property

    Public Property PK_Activity() As Long
        Get
            Return Session("CaseManagementAlertReviseDetail.PK_Activity")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertReviseDetail.PK_Activity") = value
        End Set
    End Property

    Enum enumActionForm
        Detail = 0
        Add = 1
        Edit = 2
        Delete = 3
        Accept = 4
        Reject = 5
    End Enum

    Private Sub CaseManagementAlertReviseDetail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If
            CustomerType = Request.Params("Type")

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As New Integer
            If IDModule IsNot Nothing Then
                intModuleID = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = ObjModule.ModuleLabel
                cboExportExcel.Value = "Excel"
                IntJumlahDataTransaksi = 0
                PK_Activity = 0
                DataTabelActivity = New DataTable
                LoadColumnDataTabelActivity()
                If CustomerType.ToLower() = "customer" Then
                    txt_CIFNo_WICNo.FieldLabel = "CIF No"
                    txt_Name.FieldLabel = "Customer Name"
                    btn_Import_Customer.Text = "Search Data Customer"
                    nDSDropDownField_AccountNo.IsHidden = False
                    ColumnActivityAccountNo.Hidden = False
                    col_Import_Customer_CIFNo.Text = "CIF No"

                    nDSDropDownField_AccountNo.StringFilter = " 1=2 "
                ElseIf CustomerType.ToLower() = "wic" Then
                    txt_CIFNo_WICNo.FieldLabel = "WIC No"
                    txt_Name.FieldLabel = "WIC Name"
                    btn_Import_Customer.Text = "Search Data WIC"
                    nDSDropDownField_AlertType.IsReadOnly = True
                    nDSDropDownField_AlertType.SetTextWithTextValue("Typology Risk", "Typology Risk")
                    nDSDropDownField_RuleBasic.IsHidden = False
                    If String.IsNullOrEmpty(textArea_CaseDescription.Value) OrElse textArea_CaseDescription.Value = "default value transaction abnormal" Then
                        textArea_CaseDescription.Value = "default value rule basic description"
                    End If
                    checkbox_IsTransaction.Hidden = True
                    checkbox_IsTransaction.Checked = False
                    Checkbox_IsTransaction_click()

                    col_Import_Customer_CIFNo.Text = "WIC No"
                    nDSDropDownField_AccountNo.IsHidden = True
                    ColumnActivityAccountNo.Hidden = True
                    WindowPopUpDataCustomer.Title = "Data WIC Information"
                End If
                'LoadStoreComboBox()

                'SetCommandColumnLocation()

                '' Add 05-Jan-2023
                LoadReviseData(IDUnik)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    'Sub LoadStoreComboBox()
    '    Try
    '        comboBox_CIF.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
    '        StoreCIF.Reload()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Protected Sub Store_ReadData_Search_Transaction(sender As Object, e As StoreReadDataEventArgs)
        Try
            If String.IsNullOrWhiteSpace(txt_CIFNo_WICNo.Value) Then
                Throw New ApplicationException("CIF Number is Empty, Cannot Find Transaction Data Without CIF Number")
            End If
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            'Dim strQuery As String = "SELECT * FROM goAML_ODM_Transaksi"
            'strQuery += " WHERE CIF_NO = '" & Me.CIFNo & "'"

            '29-Mar-2022 Adi
            Dim strQuery As String = "SELECT gaot.NO_ID "
            strQuery &= " ,gaot.Date_Transaction "
            strQuery &= " ,gaot.Ref_Num "
            strQuery &= " ,gaot.Transmode_Code "
            strQuery &= " ,gaot.Transaction_Location "
            strQuery &= " ,gaot.Debit_Credit "
            strQuery &= " ,gaot.Currency "
            strQuery &= " ,gaot.Exchange_Rate "
            strQuery &= " ,gaot.Transaction_Remark "
            strQuery &= " ,gaot.Original_Amount "
            strQuery &= " ,gaot.IDR_Amount "
            strQuery &= " ,gaot.Account_NO "
            strQuery &= " ,gaot.ACCOUNT_No_Lawan "
            strQuery &= " ,gaot.CIF_No_Lawan "
            strQuery &= " ,gaot.WIC_No_Lawan "
            strQuery &= " ,CASE WHEN gaot.CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN gaot.CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM goAML_ODM_Transaksi AS gaot"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON gaot.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON gaot.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " and gaot.CIF_No_Lawan is null "
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON gaot.country_code_lawan = ctry.Kode"
            'strQuery &= " WHERE CIF_NO = '" & nDSDropDownField_CIF.SelectedItemValue & "'"
            strQuery &= " WHERE CIF_NO = '" & txt_CIFNo_WICNo.Value & "'"

            Dim DataPaging As Data.DataTable
            If Not (dateField_SearchFrom.SelectedDate = DateTime.MinValue And dateField_SearchTo.SelectedDate = DateTime.MinValue) Then
                strQuery += " AND Date_Transaction >= '" & CDate(dateField_SearchFrom.SelectedDate).ToString("yyyy-MM-dd") & "'"
                strQuery += " AND Date_Transaction <= '" & CDate(dateField_SearchTo.SelectedDate).ToString("yyyy-MM-dd") & "'"

                If Not String.IsNullOrEmpty(strfilter) Then
                    strQuery += " AND " & strfilter
                End If

                DataPaging = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)
            Else
                DataPaging = New DataTable
            End If

            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            IntJumlahDataTransaksi = inttotalRecord
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_SearchTransaction.GetStore.DataSource = DataPaging
            gp_SearchTransaction.GetStore.DataBind()

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Button_SearchTransaction_Click(sender As Object, e As DirectEventArgs)
        Try
            If String.IsNullOrWhiteSpace(txt_CIFNo_WICNo.Value) Then
                Throw New ApplicationException("Please choose CIF for searching transaction.")
            End If
            'If String.IsNullOrWhiteSpace(nDSDropDownField_CIF.SelectedItemValue) Then
            '    Throw New ApplicationException("Please choose CIF for searching transaction.")
            'End If
            If dateField_SearchFrom.SelectedDate = DateTime.MinValue Or dateField_SearchTo.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException("Please fill Date From and Date To for searching transaction.")
            End If

            store_SearchTransaction.Reload()
            'store_SearchTransaction_CheckAll.Reload()

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Cancel_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Save_Click()
        Try
            ValidateInput()
            If checkbox_IsTransaction.Checked Then
                Dim rsm As RowSelectionModel = gp_SearchTransaction.SelectionModel.Primary

                If checkbox_IsSelectedAll.Value AndAlso rsm.SelectedRows.Count > 0 Then
                    WindowConfirmation.Hidden = False
                Else
                    If checkbox_IsSelectedAll.Value Then
                        'Dim strQuery As String = "SELECT count(1) as JumlahData "
                        'strQuery &= " FROM goAML_ODM_Transaksi "
                        'strQuery &= " WHERE CIF_NO = '" & comboBox_CIF.SelectedItem.Value & "'"
                        'strQuery += " AND Date_Transaction >= '" & CDate(dateField_SearchFrom.SelectedDate).ToString("yyyy-MM-dd") & "'"
                        'strQuery += " AND Date_Transaction <= '" & CDate(dateField_SearchTo.SelectedDate).ToString("yyyy-MM-dd") & "'"
                        'Dim tempDataRow As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                        'If tempDataRow("JumlahData") = 0 Then
                        '    Throw New ApplicationException("Transaction Data is required, and no transactions from the currently selected CIF and Transaction Range Date.")
                        'Else
                        '    SavingData(True, "")
                        'End If
                        SavingData(True, "")
                    Else
                        If rsm.SelectedRows.Count = 0 Then
                            Throw New ApplicationException("Transaction Data required. Please select at least one Transaction Data.")
                        Else
                            Dim tempStringPKDataSelected As String = Nothing
                            For Each itemrow As SelectedRow In rsm.SelectedRows
                                If itemrow.RecordID IsNot Nothing Then
                                    If tempStringPKDataSelected Is Nothing Then
                                        tempStringPKDataSelected = itemrow.RecordID
                                    Else
                                        tempStringPKDataSelected = tempStringPKDataSelected & "," & itemrow.RecordID
                                    End If
                                End If
                            Next
                            SavingData(False, tempStringPKDataSelected)
                        End If
                    End If
                End If
            Else
                SavingData(True, "")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Function CheckIfStringExists(strPart As String, strFull As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim dt As DataTable = New DataTable

            Dim strsplit As String() = strFull.Split(","c)
            For Each item As String In strsplit
                If strPart = item Then
                    Return True
                End If
            Next

            Return bolResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        Try
            If buttonPosition = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub CIF_readData(sender As Object, e As StoreReadDataEventArgs)
    '    Try
    '        Dim query As String = e.Parameters("query")
    '        If query Is Nothing Then query = ""

    '        Dim strfilter As String = ""
    '        If query.Length > 0 Then

    '            strfilter = " Keterangan like '%" & query & "%'"

    '        End If
    '        'If strfilter.Length > 0 Then
    '        '    strfilter += " and active=1"
    '        'Else
    '        '    strfilter += "active=1"
    '        'End If

    '        'Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

    '        StoreCIF.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Customer", "Kode, Keterangan, active", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
    '        StoreCIF.DataBind()
    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

    '    End Try
    'End Sub

    Protected Sub NDSDropDownField_AlertType_OnValueChanged(sender As Object, e As EventArgs)
        Try
            'If nDSDropDownField_AlertType.SelectedItemValue = "Typology Risk" Then
            If nDSDropDownField_AlertType.SelectedItemText = "Typology Risk" Then
                nDSDropDownField_RuleBasic.IsHidden = False
                'nDSDropDownField_RuleBasic.Visible = True
                If String.IsNullOrEmpty(textArea_CaseDescription.Value) OrElse textArea_CaseDescription.Value = "default value transaction abnormal" Then
                    textArea_CaseDescription.Value = "default value rule basic description"
                End If
                checkbox_IsTransaction.Hidden = False
                checkbox_IsTransaction.Checked = False
                Checkbox_IsTransaction_click()
                'ElseIf nDSDropDownField_AlertType.SelectedItemValue = "Financial Risk" Then
            ElseIf nDSDropDownField_AlertType.SelectedItemText = "Financial Risk" Then
                nDSDropDownField_RuleBasic.IsHidden = True
                'nDSDropDownField_RuleBasic.Visible = False
                If String.IsNullOrEmpty(textArea_CaseDescription.Value) OrElse textArea_CaseDescription.Value = "default value rule basic description" Then
                    textArea_CaseDescription.Value = "default value transaction abnormal"
                End If
                checkbox_IsTransaction.Hidden = True
                checkbox_IsTransaction.Checked = True
                Checkbox_IsTransaction_click()
                '' Add 04-Jan-2023, Felix. Tambah Else untuk other Alert Type
            Else
                nDSDropDownField_RuleBasic.IsHidden = True
                'nDSDropDownField_RuleBasic.Visible = False
                If String.IsNullOrEmpty(textArea_CaseDescription.Value) OrElse textArea_CaseDescription.Value = "default value rule basic description" Then
                    textArea_CaseDescription.Value = ""
                End If
                checkbox_IsTransaction.Hidden = False
                checkbox_IsTransaction.Checked = False
                Checkbox_IsTransaction_click()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ValidateInput()
        Try
            If String.IsNullOrEmpty(nDSDropDownField_AlertType.SelectedItemValue) Then
                Throw New ApplicationException(nDSDropDownField_AlertType.Label & " is required.")
            End If
            'If nDSDropDownField_AlertType.SelectedItemValue = "Typology Risk" Then
            If nDSDropDownField_AlertType.SelectedItemText = "Typology Risk" Then
                If String.IsNullOrEmpty(nDSDropDownField_RuleBasic.SelectedItemValue) Then
                    Throw New ApplicationException(nDSDropDownField_RuleBasic.Label & " is required.")
                End If
            End If
            If Not checkbox_IsTransaction.Checked AndAlso DataTabelActivity.Rows.Count = 0 Then
                Throw New ApplicationException("Data Activity is required.")
            End If

            If String.IsNullOrEmpty(textArea_CaseDescription.Value) Then
                Throw New ApplicationException(textArea_CaseDescription.FieldLabel & " is required.")
            End If

            If String.IsNullOrWhiteSpace(txt_CIFNo_WICNo.Value) Then
                Throw New ApplicationException(txt_CIFNo_WICNo.FieldLabel & " is required.")
            End If
            'If String.IsNullOrWhiteSpace(nDSDropDownField_CIF.SelectedItemValue) Then
            '    Throw New ApplicationException("Please choose CIF for searching transaction.")
            'End If
            If checkbox_IsTransaction.Checked Then
                If dateField_SearchFrom.SelectedDate = DateTime.MinValue OrElse dateField_SearchTo.SelectedDate = DateTime.MinValue Then
                    Throw New ApplicationException(dateField_SearchFrom.FieldLabel & " And " & dateField_SearchTo.FieldLabel & " is required.")
                End If
                If IntJumlahDataTransaksi = 0 Then
                    Throw New ApplicationException("No Data Transaction, Please Search Transaction and make sure there is Transaction Data available from selected CIF and Date.")
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub WindowConfirmation_Yes_Click()
        Try
            WindowConfirmation.Hidden = True
            SavingData(True, "")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub WindowConfirmation_No_Click()
        Try
            WindowConfirmation.Hidden = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SavingData(selectAllData As Boolean, combinedSelectedPKData As String)
        Try
            'Dim PKCaseManagementHeader As Long = SavingDataHeader()
            UpdateDataHeader(IDUnik) '' Ganti jd Update

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@DataDate"
            param(0).Value = Now
            param(0).SqlDbType = SqlDbType.DateTime
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CaseManagement_AssignWorkflowByQuery", param)

            '' Add 05-Jan-2023, Felix. Hapus dulu sebelum insert ulang.
            Dim strQuery As String = ""
            strQuery &= " declare @PK_CaseManagement_typology as bigint "
            strQuery &= " select @PK_CaseManagement_typology = PK_OneFCC_CaseManagement_Typology_ID from OneFCC_CaseManagement_Typology "
            strQuery &= " where FK_CaseManagement_ID = " & IDUnik
            'strQuery &= " delete from OneFCC_CaseManagement "
            'strQuery &= " where PK_CaseManagement_ID = " & IDUnik
            strQuery &= " delete from OneFCC_CaseManagement_Typology "
            strQuery &= " where FK_CaseManagement_ID = " & IDUnik
            strQuery &= " delete from OneFCC_CaseManagement_Typology_Transaction "
            strQuery &= " where FK_OneFCC_CaseManagement_Typology_ID = @PK_CaseManagement_typology "

            strQuery &= " declare @PK_CaseManagement_outlier as bigint "
            strQuery &= " select @PK_CaseManagement_outlier = PK_OneFCC_CaseManagement_Outlier_ID from OneFCC_CaseManagement_Outlier "
            strQuery &= " where FK_CaseManagement_ID = " & IDUnik
            'strQuery &= " delete from OneFCC_CaseManagement "
            'strQuery &= " where PK_CaseManagement_ID = " & IDUnik
            strQuery &= " delete from OneFCC_CaseManagement_Outlier "
            strQuery &= " where FK_CaseManagement_ID = " & IDUnik
            strQuery &= " delete from OneFCC_CaseManagement_Outlier_Transaction "
            strQuery &= " where FK_OneFCC_CaseManagement_Outlier_ID = @PK_CaseManagement_outlier "

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            '' End 05-Jan-2023

            If checkbox_IsTransaction.Checked Then
                Dim paramSaveTransaction(7) As SqlParameter
                paramSaveTransaction(0) = New SqlParameter
                paramSaveTransaction(0).ParameterName = "@PKCaseManagement"
                paramSaveTransaction(0).Value = IDUnik
                paramSaveTransaction(0).SqlDbType = SqlDbType.BigInt

                paramSaveTransaction(1) = New SqlParameter
                paramSaveTransaction(1).ParameterName = "@CIF"
                paramSaveTransaction(1).Value = txt_CIFNo_WICNo.Value
                paramSaveTransaction(1).SqlDbType = SqlDbType.VarChar

                paramSaveTransaction(2) = New SqlParameter
                paramSaveTransaction(2).ParameterName = "@DateParamFrom"
                paramSaveTransaction(2).Value = dateField_SearchFrom.SelectedDate
                paramSaveTransaction(2).SqlDbType = SqlDbType.Date

                paramSaveTransaction(3) = New SqlParameter
                paramSaveTransaction(3).ParameterName = "@DateParamTo"
                paramSaveTransaction(3).Value = dateField_SearchTo.SelectedDate
                paramSaveTransaction(3).SqlDbType = SqlDbType.Date

                paramSaveTransaction(4) = New SqlParameter
                paramSaveTransaction(4).ParameterName = "@IsSelectedAll"
                paramSaveTransaction(4).Value = selectAllData
                paramSaveTransaction(4).SqlDbType = SqlDbType.Bit

                paramSaveTransaction(5) = New SqlParameter
                paramSaveTransaction(5).ParameterName = "@IntAlertType"
                'If nDSDropDownField_AlertType.SelectedItemValue = "Typology Risk" Then
                '    paramSaveTransaction(5).Value = 1
                'ElseIf nDSDropDownField_AlertType.SelectedItemValue = "Financial Risk" Then
                '    paramSaveTransaction(5).Value = 2
                'End If
                paramSaveTransaction(5).Value = nDSDropDownField_AlertType.SelectedItemValue
                paramSaveTransaction(5).SqlDbType = SqlDbType.Int

                paramSaveTransaction(6) = New SqlParameter
                paramSaveTransaction(6).ParameterName = "@CombinedPKTransaction"
                paramSaveTransaction(6).Value = combinedSelectedPKData
                paramSaveTransaction(6).SqlDbType = SqlDbType.VarChar

                paramSaveTransaction(7) = New SqlParameter
                paramSaveTransaction(7).ParameterName = "@UserID"
                paramSaveTransaction(7).Value = NawaBLL.Common.SessionCurrentUser.UserID
                paramSaveTransaction(7).SqlDbType = SqlDbType.VarChar
                NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CaseManagement_Alert_SaveDataTransaction_FromODM", paramSaveTransaction)
            Else
                Dim paramSaveActivity(2) As SqlParameter

                paramSaveActivity(0) = New SqlParameter
                paramSaveActivity(0).ParameterName = "@PKCaseManagement"
                paramSaveActivity(0).Value = IDUnik
                paramSaveActivity(0).SqlDbType = SqlDbType.BigInt

                paramSaveActivity(1) = New SqlParameter
                paramSaveActivity(1).ParameterName = "@DataActivity"
                paramSaveActivity(1).Value = DataTabelActivity
                paramSaveActivity(1).SqlDbType = SqlDbType.Structured
                paramSaveActivity(1).TypeName = "dbo.udt_AML_CaseManagement_Alert_Activity"

                paramSaveActivity(2) = New SqlParameter
                paramSaveActivity(2).ParameterName = "@UserID"
                paramSaveActivity(2).Value = NawaBLL.Common.SessionCurrentUser.UserID
                paramSaveActivity(2).SqlDbType = SqlDbType.VarChar

                NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CaseManagement_Alert_SaveDataActivity", paramSaveActivity)
            End If

            Dim paramAuditTrail(2) As SqlParameter
            paramAuditTrail(0) = New SqlParameter
            paramAuditTrail(0).ParameterName = "@PK_CaseManagement"
            paramAuditTrail(0).Value = IDUnik
            paramAuditTrail(0).SqlDbType = SqlDbType.BigInt

            paramAuditTrail(1) = New SqlParameter
            paramAuditTrail(1).ParameterName = "@ModuleLabel"
            paramAuditTrail(1).Value = ObjModule.ModuleLabel
            paramAuditTrail(1).SqlDbType = SqlDbType.VarChar

            paramAuditTrail(2) = New SqlParameter
            paramAuditTrail(2).ParameterName = "@Action"
            paramAuditTrail(2).Value = 1
            paramAuditTrail(2).SqlDbType = SqlDbType.Int
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CaseManagement_Alert_AuditTrail", paramAuditTrail)
            'Show Confirmation

            '' Remark 06-Jan-2023, krn kalo Sysadmin tidak masuk Approval & Revise.
            'If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
            '    LblConfirmation.Text = "Data Saved into Database"
            'Else
            LblConfirmation.Text = "Data Saved into Pending Approval"
            'End If
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function SavingDataHeader() As Long
        Try
            Dim newPK As Long
            Using objdb As New CasemanagementDAL.CasemanagementEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                    Try
                        Dim newCaseManagementData As New CasemanagementDAL.OneFCC_CaseManagement
                        With newCaseManagementData
                            .PK_CaseManagement_ID = -1
                            '.Alert_Type = nDSDropDownField_AlertType.SelectedItemValue
                            .Alert_Type = nDSDropDownField_AlertType.SelectedItemText
                            If .Alert_Type = "Typology Risk" Then
                                .FK_Rule_Basic_ID = nDSDropDownField_RuleBasic.SelectedItemValue
                            End If
                            .Case_Description = textArea_CaseDescription.Value

                            If CustomerType.ToLower() = "customer" Then
                                .Is_Customer = True
                                .CIF_No = txt_CIFNo_WICNo.Value
                                .Customer_Name = txt_Name.Value
                            ElseIf CustomerType.ToLower() = "wic" Then
                                .Is_Customer = False
                                .WIC_No = txt_CIFNo_WICNo.Value
                                .WIC_Name = txt_Name.Value
                            End If
                            'Save Data With/Without Approval
                            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
                                .FK_CaseStatus_ID = 0
                            Else
                                .FK_CaseStatus_ID = -1
                            End If
                            .ProcessDate = Now
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = Now
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                .Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            End If
                        End With
                        objdb.Entry(newCaseManagementData).State = Entity.EntityState.Added
                        objdb.SaveChanges()
                        objTrans.Commit()
                        newPK = newCaseManagementData.PK_CaseManagement_ID
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using
            End Using
            Return newPK
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub Checkbox_IsSelectedAll_CheckedChanged(sender As Object, e As EventArgs) Handles checkbox_IsSelectedAll.DirectCheck
        Try
            If checkbox_IsSelectedAll.Checked Then
                BtnExportSelected.Hidden = True
            Else
                BtnExportSelected.Hidden = False
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#Region "03-Aug-2022 Penambahan Export Data"
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            If IntJumlahDataTransaksi = 0 Then
                Throw New ApplicationException("No Data Transaction, Please Search Transaction and make sure there is Transaction Data available from selected CIF and Date.")
            End If

            If String.IsNullOrWhiteSpace(txt_CIFNo_WICNo.Value) Then
                Throw New ApplicationException(txt_CIFNo_WICNo.FieldLabel & " is required.")
            End If

            If dateField_SearchFrom.SelectedDate = DateTime.MinValue OrElse dateField_SearchTo.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException(dateField_SearchFrom.FieldLabel & " And " & dateField_SearchTo.FieldLabel & " is required.")
            End If

            Dim strQuery As String = ""
            strQuery &= " SELECT gaot.NO_ID "
            strQuery &= " ,gaot.Date_Transaction "
            strQuery &= " ,gaot.Ref_Num "
            strQuery &= " ,gaot.Transmode_Code "
            strQuery &= " ,gaot.Transaction_Location "
            strQuery &= " ,gaot.Debit_Credit "
            strQuery &= " ,gaot.Currency "
            strQuery &= " ,gaot.Transaction_Remark "
            strQuery &= " ,gaot.Original_Amount "
            strQuery &= " ,gaot.IDR_Amount "
            strQuery &= " ,gaot.Account_NO "
            strQuery &= " ,gaot.ACCOUNT_No_Lawan "
            strQuery &= " ,gaot.CIF_No_Lawan "
            strQuery &= " ,gaot.WIC_No_Lawan "
            strQuery &= " ,CASE WHEN gaot.CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN gaot.CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM goAML_ODM_Transaksi AS gaot"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON gaot.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON gaot.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " and gaot.CIF_No_Lawan is null "
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON gaot.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE CIF_NO = '" & txt_CIFNo_WICNo.Value & "'"
            strQuery += " AND Date_Transaction >= '" & CDate(dateField_SearchFrom.SelectedDate).ToString("yyyy-MM-dd") & "'"
            strQuery += " AND Date_Transaction <= '" & CDate(dateField_SearchTo.SelectedDate).ToString("yyyy-MM-dd") & "'"

            Dim DataScreeningAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery,)

            If DataScreeningAll.Rows.Count < 1 Then
                Throw New ApplicationException("No Transaction Data")
            End If

            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))

                    Using objtbl As Data.DataTable = DataScreeningAll

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_SearchTransaction.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If
                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("Data Transaction")
                            objtbl.Columns("Date_Transaction").SetOrdinal(0)
                            objtbl.Columns("Ref_Num").SetOrdinal(1)
                            objtbl.Columns("Transmode_Code").SetOrdinal(2)
                            objtbl.Columns("Transaction_Location").SetOrdinal(3)
                            objtbl.Columns("Debit_Credit").SetOrdinal(4)
                            objtbl.Columns("Currency").SetOrdinal(5)
                            objtbl.Columns("Original_Amount").SetOrdinal(6)
                            objtbl.Columns("IDR_Amount").SetOrdinal(7)
                            objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                            objtbl.Columns("Account_NO").SetOrdinal(9)
                            objtbl.Columns("CounterPartyType").SetOrdinal(10)
                            objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                            objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                            objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                            objtbl.Columns("CounterPartyName").SetOrdinal(14)
                            objtbl.Columns("Country_Lawan").SetOrdinal(15)

                            objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                            objtbl.Columns("Ref_Num").ColumnName = "Reference Number"
                            objtbl.Columns("Transmode_Code").ColumnName = "Transaction Code"
                            objtbl.Columns("Transaction_Location").ColumnName = "Location"
                            objtbl.Columns("Debit_Credit").ColumnName = "Debit / Credit"
                            objtbl.Columns("Currency").ColumnName = "Currency"
                            objtbl.Columns("Original_Amount").ColumnName = "Amount"
                            objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                            objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                            objtbl.Columns("Account_NO").ColumnName = "Account No."
                            objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                            objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                            objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                            objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                            objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                            objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Data_Transaction.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = DataScreeningAll                    'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_SearchTransaction.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        objtbl.Columns("Date_Transaction").SetOrdinal(0)
                        objtbl.Columns("Ref_Num").SetOrdinal(1)
                        objtbl.Columns("Transmode_Code").SetOrdinal(2)
                        objtbl.Columns("Transaction_Location").SetOrdinal(3)
                        objtbl.Columns("Debit_Credit").SetOrdinal(4)
                        objtbl.Columns("Currency").SetOrdinal(5)
                        objtbl.Columns("Original_Amount").SetOrdinal(6)
                        objtbl.Columns("IDR_Amount").SetOrdinal(7)
                        objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                        objtbl.Columns("Account_NO").SetOrdinal(9)
                        objtbl.Columns("CounterPartyType").SetOrdinal(10)
                        objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                        objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                        objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                        objtbl.Columns("CounterPartyName").SetOrdinal(14)
                        objtbl.Columns("Country_Lawan").SetOrdinal(15)

                        objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                        objtbl.Columns("Ref_Num").ColumnName = "Reference Number"
                        objtbl.Columns("Transmode_Code").ColumnName = "Transaction Code"
                        objtbl.Columns("Transaction_Location").ColumnName = "Location"
                        objtbl.Columns("Debit_Credit").ColumnName = "Debit / Credit"
                        objtbl.Columns("Currency").ColumnName = "Currency"
                        objtbl.Columns("Original_Amount").ColumnName = "Amount"
                        objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                        objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                        objtbl.Columns("Account_NO").ColumnName = "Account No."
                        objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                        objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                        objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                        objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                        objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                        objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Data_Transaction.csv")
                        Response.Charset = ""
                        Me.EnableViewState = False
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportExcel(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If (dateField_SearchFrom.SelectedDate = DateTime.MinValue OrElse dateField_SearchTo.SelectedDate = DateTime.MinValue) Then
                Throw New ApplicationException("Date From and To is Mandatory")
            End If

            If String.IsNullOrWhiteSpace(txt_CIFNo_WICNo.Value) Then
                Throw New ApplicationException(txt_CIFNo_WICNo.FieldLabel & " is required.")
            End If

            Dim sorter = gp_SearchTransaction.GetStore().Sorters.Item(0)
            Dim sort = sorter.Property
            Dim Dir = sorter.Direction

            Dim filter = gp_SearchTransaction.GetStore().Filters

            Dim pagecurrent As Integer = e.ExtraParams("currentPage")

            Dim strQuery As String = ""
            strQuery &= " DECLARE @PageNumber AS INT,  "
            strQuery &= " @RowspPage AS INT "
            strQuery &= " SET @PageNumber = " & pagecurrent
            strQuery &= " SET @RowspPage = 10 "
            strQuery &= " SELECT gaot.NO_ID "
            strQuery &= " ,gaot.Date_Transaction "
            strQuery &= " ,gaot.Ref_Num "
            strQuery &= " ,gaot.Transmode_Code "
            strQuery &= " ,gaot.Transaction_Location "
            strQuery &= " ,gaot.Debit_Credit "
            strQuery &= " ,gaot.Currency "
            strQuery &= " ,gaot.Original_Amount "
            strQuery &= " ,gaot.IDR_Amount "
            strQuery &= " ,gaot.Account_NO "
            strQuery &= " ,gaot.ACCOUNT_No_Lawan "
            strQuery &= " ,gaot.CIF_No_Lawan "
            strQuery &= " ,gaot.WIC_No_Lawan "
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM goAML_ODM_Transaksi AS gaot"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON gaot.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON gaot.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " and gaot.CIF_No_Lawan is null "
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON gaot.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE CIF_NO = '" & txt_CIFNo_WICNo.Value & "'"
            strQuery += " AND Date_Transaction >= '" & CDate(dateField_SearchFrom.SelectedDate).ToString("yyyy-MM-dd") & "'"
            strQuery += " AND Date_Transaction <= '" & CDate(dateField_SearchTo.SelectedDate).ToString("yyyy-MM-dd") & "'"
            strQuery &= " OFFSET ((@PageNumber - 1) * @RowspPage) ROWS FETCH NEXT @RowspPage ROWS ONLY "

            'Dim DataScreeningCurrentPage As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "DECLARE @PageNumber AS INT, @RowspPage AS INT SET @PageNumber =" & pagecurrent & " SET @RowspPage = 10 SELECT Nama,DOB,Birth_Place,Stakeholder_Role,case when Stakeholder_Role Is null then 'Customer' else 'Non Customer' end as customerinformation,CONVERT(varchar(4000),STUFF((SELECT ', ' + CIF  AS [text()] FROM goAML_Ref_Customer rf WHERE GCN = result.GCN FOR XML PATH ('')),1,2,'')) as CIF,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODE,Identity_Number,FK_WATCHLIST_ID_PPATK,GCN,NAMACUSTOMER,DOBCustomer,Birth_PlaceCustomer,FK_goAML_Ref_Jenis_Dokumen_Identitas_CODECustomer,Identity_NumberCustomer, (TotalSimilarity*100) AS TotalSimilarityPct FROM SIPENDAR_SCREENING_REQUEST_RESULT result WHERE PK_SIPENDAR_SCREENING_REQUEST_ID=" & PK_Request_ID & " ORDER BY TotalSimilarityPct Desc, GCN, Nama OFFSET ((@PageNumber - 1) * @RowspPage) ROWS FETCH NEXT @RowspPage ROWS ONLY",)
            Dim DataScreeningCurrentPage As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery,)
            '' 15-Dec-2021
            If DataScreeningCurrentPage.Rows.Count < 1 Then
                Throw New ApplicationException("No Transaction Data")
            End If

            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))

                    Using objtbl As Data.DataTable = DataScreeningCurrentPage
                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_SearchTransaction.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("Data Transaction")
                            objtbl.Columns("Date_Transaction").SetOrdinal(0)
                            objtbl.Columns("Ref_Num").SetOrdinal(1)
                            objtbl.Columns("Transmode_Code").SetOrdinal(2)
                            objtbl.Columns("Transaction_Location").SetOrdinal(3)
                            objtbl.Columns("Debit_Credit").SetOrdinal(4)
                            objtbl.Columns("Currency").SetOrdinal(5)
                            objtbl.Columns("Original_Amount").SetOrdinal(6)
                            objtbl.Columns("IDR_Amount").SetOrdinal(7)
                            objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                            objtbl.Columns("Account_NO").SetOrdinal(9)
                            objtbl.Columns("CounterPartyType").SetOrdinal(10)
                            objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                            objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                            objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                            objtbl.Columns("CounterPartyName").SetOrdinal(14)
                            objtbl.Columns("Country_Lawan").SetOrdinal(15)

                            objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                            objtbl.Columns("Ref_Num").ColumnName = "Reference Number"
                            objtbl.Columns("Transmode_Code").ColumnName = "Transaction Code"
                            objtbl.Columns("Transaction_Location").ColumnName = "Location"
                            objtbl.Columns("Debit_Credit").ColumnName = "Debit / Credit"
                            objtbl.Columns("Currency").ColumnName = "Currency"
                            objtbl.Columns("Original_Amount").ColumnName = "Amount"
                            objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                            objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                            objtbl.Columns("Account_NO").ColumnName = "Account No."
                            objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                            objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                            objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                            objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                            objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                            objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)

                    Using objtbl As Data.DataTable = DataScreeningCurrentPage

                        For Each item As Ext.Net.ColumnBase In gp_SearchTransaction.ColumnModel.Columns
                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        objtbl.Columns("Date_Transaction").SetOrdinal(0)
                        objtbl.Columns("Ref_Num").SetOrdinal(1)
                        objtbl.Columns("Transmode_Code").SetOrdinal(2)
                        objtbl.Columns("Transaction_Location").SetOrdinal(3)
                        objtbl.Columns("Debit_Credit").SetOrdinal(4)
                        objtbl.Columns("Currency").SetOrdinal(5)
                        objtbl.Columns("Original_Amount").SetOrdinal(6)
                        objtbl.Columns("IDR_Amount").SetOrdinal(7)
                        objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                        objtbl.Columns("Account_NO").SetOrdinal(9)
                        objtbl.Columns("CounterPartyType").SetOrdinal(10)
                        objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                        objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                        objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                        objtbl.Columns("CounterPartyName").SetOrdinal(14)
                        objtbl.Columns("Country_Lawan").SetOrdinal(15)

                        objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                        objtbl.Columns("Ref_Num").ColumnName = "Reference Number"
                        objtbl.Columns("Transmode_Code").ColumnName = "Transaction Code"
                        objtbl.Columns("Transaction_Location").ColumnName = "Location"
                        objtbl.Columns("Debit_Credit").ColumnName = "Debit / Credit"
                        objtbl.Columns("Currency").ColumnName = "Currency"
                        objtbl.Columns("Original_Amount").ColumnName = "Amount"
                        objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                        objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                        objtbl.Columns("Account_NO").ColumnName = "Account No."
                        objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                        objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                        objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                        objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                        objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                        objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Sipendar_Screening_Request.csv")
                        Response.Charset = ""
                        Me.EnableViewState = False
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    Protected Sub ExportSelectedExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            Dim smScreeningResult As RowSelectionModel = TryCast(gp_SearchTransaction.GetSelectionModel(), RowSelectionModel)
            Dim listStrID As String = ""
            For Each item As SelectedRow In smScreeningResult.SelectedRows
                Dim recordID = item.RecordID.ToString
                If Not String.IsNullOrEmpty(recordID) Then
                    If listStrID = "" Then
                        listStrID = recordID
                    Else
                        listStrID = listStrID & "," & recordID
                    End If
                End If
            Next
            If String.IsNullOrEmpty(listStrID) Then
                Throw New Exception("must choose at least 1 data")
            End If

            Dim strQuery As String = "SELECT gaot.NO_ID "
            strQuery &= " ,gaot.Date_Transaction "
            strQuery &= " ,gaot.Ref_Num "
            strQuery &= " ,gaot.Transmode_Code "
            strQuery &= " ,gaot.Transaction_Location "
            strQuery &= " ,gaot.Debit_Credit "
            strQuery &= " ,gaot.Currency "
            strQuery &= " ,gaot.Transaction_Remark "
            strQuery &= " ,gaot.Original_Amount "
            strQuery &= " ,gaot.IDR_Amount "
            strQuery &= " ,gaot.Account_NO "
            strQuery &= " ,gaot.ACCOUNT_No_Lawan "
            strQuery &= " ,gaot.CIF_No_Lawan "
            strQuery &= " ,gaot.WIC_No_Lawan "
            strQuery &= " ,CASE WHEN gaot.CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN gaot.CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM goAML_ODM_Transaksi AS gaot"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON gaot.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON gaot.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " and gaot.CIF_No_Lawan is null "
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON gaot.country_code_lawan = ctry.Kode"
            'strQuery &= " WHERE CIF_NO = '" & nDSDropDownField_CIF.SelectedItemValue & "'"
            strQuery &= " WHERE gaot.NO_ID in (" & listStrID & ")"

            '' End 15-Dec-2021
            Dim dataSelectedTransaction As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery,)

            If dataSelectedTransaction.Rows.Count < 1 Then
                Throw New ApplicationException("No Transaction Data")
            End If

            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = dataSelectedTransaction

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_SearchTransaction.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("Data Transaction")
                            objtbl.Columns("Date_Transaction").SetOrdinal(0)
                            objtbl.Columns("Ref_Num").SetOrdinal(1)
                            objtbl.Columns("Transmode_Code").SetOrdinal(2)
                            objtbl.Columns("Transaction_Location").SetOrdinal(3)
                            objtbl.Columns("Debit_Credit").SetOrdinal(4)
                            objtbl.Columns("Currency").SetOrdinal(5)
                            objtbl.Columns("Original_Amount").SetOrdinal(6)
                            objtbl.Columns("IDR_Amount").SetOrdinal(7)
                            objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                            objtbl.Columns("Account_NO").SetOrdinal(9)
                            objtbl.Columns("CounterPartyType").SetOrdinal(10)
                            objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                            objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                            objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                            objtbl.Columns("CounterPartyName").SetOrdinal(14)
                            objtbl.Columns("Country_Lawan").SetOrdinal(15)

                            objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                            objtbl.Columns("Ref_Num").ColumnName = "Reference Number"
                            objtbl.Columns("Transmode_Code").ColumnName = "Transaction Code"
                            objtbl.Columns("Transaction_Location").ColumnName = "Location"
                            objtbl.Columns("Debit_Credit").ColumnName = "Debit / Credit"
                            objtbl.Columns("Currency").ColumnName = "Currency"
                            objtbl.Columns("Original_Amount").ColumnName = "Amount"
                            objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                            objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                            objtbl.Columns("Account_NO").ColumnName = "Account No."
                            objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                            objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                            objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                            objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                            objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                            objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=Data_Transaction.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = dataSelectedTransaction

                        'objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In gp_SearchTransaction.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        objtbl.Columns("Date_Transaction").SetOrdinal(0)
                        objtbl.Columns("Ref_Num").SetOrdinal(1)
                        objtbl.Columns("Transmode_Code").SetOrdinal(2)
                        objtbl.Columns("Transaction_Location").SetOrdinal(3)
                        objtbl.Columns("Debit_Credit").SetOrdinal(4)
                        objtbl.Columns("Currency").SetOrdinal(5)
                        objtbl.Columns("Original_Amount").SetOrdinal(6)
                        objtbl.Columns("IDR_Amount").SetOrdinal(7)
                        objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                        objtbl.Columns("Account_NO").SetOrdinal(9)
                        objtbl.Columns("CounterPartyType").SetOrdinal(10)
                        objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                        objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                        objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                        objtbl.Columns("CounterPartyName").SetOrdinal(14)
                        objtbl.Columns("Country_Lawan").SetOrdinal(15)

                        objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                        objtbl.Columns("Ref_Num").ColumnName = "Reference Number"
                        objtbl.Columns("Transmode_Code").ColumnName = "Transaction Code"
                        objtbl.Columns("Transaction_Location").ColumnName = "Location"
                        objtbl.Columns("Debit_Credit").ColumnName = "Debit / Credit"
                        objtbl.Columns("Currency").ColumnName = "Currency"
                        objtbl.Columns("Original_Amount").ColumnName = "Amount"
                        objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                        objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                        objtbl.Columns("Account_NO").ColumnName = "Account No."
                        objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                        objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                        objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                        objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                        objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                        objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=Data_Transaction.csv")
                        Response.Charset = ""
                        Me.EnableViewState = False
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub
#End Region

#Region "14-Nov-2022 Penambahan WIC(Non-Customer)"
    Protected Sub LoadColumnDataTabelActivity()
        Try
            DataTabelActivity.Columns.Add(New DataColumn("PK_OneFCC_CaseManagement_Typology_NonTransaction_ID", GetType(Long)))
            DataTabelActivity.Columns.Add(New DataColumn("Date_Activity", GetType(Date)))
            DataTabelActivity.Columns.Add(New DataColumn("Activity_Description", GetType(String)))
            DataTabelActivity.Columns.Add(New DataColumn("Account_NO", GetType(String)))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Checkbox_IsTransaction_click()
        Try
            If checkbox_IsTransaction.Checked Then
                'comboBox_CIF.Hidden = False
                dateField_SearchFrom.Hidden = False
                dateField_SearchTo.Hidden = False
                button_SearchTransaction.Hidden = False
                checkbox_IsSelectedAll.Hidden = False
                panel_Transaction.Hidden = False

                panel_Activity.Hidden = True
            Else
                'comboBox_CIF.Hidden = True
                dateField_SearchFrom.Hidden = True
                dateField_SearchTo.Hidden = True
                button_SearchTransaction.Hidden = True
                checkbox_IsSelectedAll.Hidden = True
                panel_Transaction.Hidden = True

                panel_Activity.Hidden = False
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadData_Import_Customer(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            'If strsort = "" Then
            '    strsort = "CIF asc"
            'End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            Dim strTableName As String = ""
            If CustomerType.ToLower() = "customer" Then
                strTableName = "vw_AML_CaseManagement_Alert_goAML_Ref_Customer"
            ElseIf CustomerType.ToLower() = "wic" Then
                strTableName = "vw_AML_CaseManagement_Alert_goAML_Ref_WIC"
            End If
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(strTableName, "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            store_Import_Customer.DataSource = DataPaging
            store_Import_Customer.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Import_Customer_Click()
        Try
            window_Import_Customer.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_OpenWindowCustomer_Click()
        Try
            If Not String.IsNullOrEmpty(txt_CIFNo_WICNo.Value) Then
                Dim strQuery As String = ""
                If CustomerType.ToLower() = "customer" Then
                    strQuery = "SELECT COUNT(1) AS JumCust FROM AML_CUSTOMER WHERE CIFNo = '" & txt_CIFNo_WICNo.Value & "'"
                ElseIf CustomerType.ToLower() = "wic" Then
                    strQuery = "SELECT COUNT(1) AS JumCust FROM goAML_Ref_WIC WHERE WIC_No = '" & txt_CIFNo_WICNo.Value & "'"
                End If
                If String.IsNullOrEmpty(strQuery) Then
                    Throw New ApplicationException("Customer Type Cannot be recognized, please report this to admin")
                End If
                Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                If drResult IsNot Nothing AndAlso Not IsDBNull(drResult("JumCust")) Then
                    If drResult("JumCust") > 0 Then
                        WindowPopUpDataCustomer.Hidden = False
                        LoadDataAMLCustomerByCIF(txt_CIFNo_WICNo.Value)
                    Else
                        If CustomerType.ToLower() = "customer" Then
                            Throw New ApplicationException("No AML Customer Found With CIF No = " & txt_CIFNo_WICNo.Value)
                        ElseIf CustomerType.ToLower() = "wic" Then
                            Throw New ApplicationException("No WIC Found With WIC No = " & txt_CIFNo_WICNo.Value)
                        End If
                    End If
                Else
                    Throw New ApplicationException("There is Something wrong when seaching Data AML Customer, Please Report this to Admin")
                End If
            Else
                Throw New ApplicationException("CIF is Empty")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_Import_Customer(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim strCIFNo As String = e.ExtraParams(0).Value
            Dim strCustomerName As String = e.ExtraParams(1).Value
            If CustomerType.ToLower() = "customer" Then
                If txt_CIFNo_WICNo.Value <> strCIFNo Then
                    DataTabelActivity.Clear()
                    BindTableActivity()
                End If
                nDSDropDownField_AccountNo.StringFilter = " client_number = '" & strCIFNo & "' "
            End If
            txt_CIFNo_WICNo.Value = strCIFNo
            txt_Name.Value = strCustomerName
            window_Import_Customer.Hidden = True
            btn_OpenWindowCustomer.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Import_Customer_Back_Click()
        Try
            window_Import_Customer.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDataAMLCustomerByCIF(CIF As String)
        Try
            'Get Customer Information
            txt_CustomerDetail_CIF.Value = txt_CIFNo_WICNo.Value
            txt_CustomerDetail_Name.Value = txt_Name.Value

            If CustomerType.ToLower() = "customer" Then
                txt_CustomerDetail_CIF.FieldLabel = "CIF No"
                txt_CustomerDetail_Name.FieldLabel = "Customer Name"
                txt_CustomerDetail_POB.FieldLabel = "Place of Birth"
                txt_CustomerDetail_DOB.FieldLabel = "Date of Birth"
                txt_CustomerDetail_Nationality.FieldLabel = "Nationality"
                txt_CustomerDetail_Occupation.FieldLabel = "Occupation"
                txt_CustomerDetail_Employer.FieldLabel = "Employer"
                txt_CustomerDetail_LegalForm.FieldLabel = "Legal Form"
                txt_CustomerDetail_Industry.FieldLabel = "Industry"
                txt_CustomerDetail_NPWP.FieldLabel = "Tax Number"
                txt_CustomerDetail_Income.FieldLabel = "Income Level"
                txt_CustomerDetail_PurposeOfFund.FieldLabel = "Purpose of Fund"
                txt_CustomerDetail_SourceOfFund.FieldLabel = "Source of Fund"
                gridIdentityInfo.Hidden = False
                Dim strDateFormat As String = NawaBLL.SystemParameterBLL.GetDateFormat()
                Dim strQuery As String = ""

                strQuery = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & txt_CIFNo_WICNo.Value & "'"
                Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                If drCustomer IsNot Nothing Then
                    If Not IsDBNull(drCustomer("FK_AML_Customer_Type_Code")) Then
                        txt_CustomerDetail_TypeCode.Value = drCustomer("FK_AML_Customer_Type_Code")
                    Else
                        txt_CustomerDetail_TypeCode.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("PLACEOFBIRTH")) Then
                        txt_CustomerDetail_POB.Value = drCustomer("PLACEOFBIRTH")
                    Else
                        txt_CustomerDetail_POB.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("DATEOFBIRTH")) Then
                        txt_CustomerDetail_DOB.Value = CDate(drCustomer("DATEOFBIRTH")).ToString(strDateFormat)
                    Else
                        txt_CustomerDetail_DOB.Value = ""
                    End If

                    'txt_CustomerDetail_Nationality.Value = drCustomer("FK_AML_CITIZENSHIP_CODE")
                    If Not IsDBNull(drCustomer("FK_AML_CITIZENSHIP_CODE")) Then
                        strQuery = "SELECT TOP 1 * FROM AML_COUNTRY WHERE FK_AML_COUNTRY_Code ='" & drCustomer("FK_AML_CITIZENSHIP_CODE") & "'"
                        Dim drCountry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundCountry As String = drCustomer("FK_AML_CITIZENSHIP_CODE")
                        If drCountry IsNot Nothing AndAlso Not IsDBNull(drCountry("AML_COUNTRY_Name")) Then
                            strTempCompoundCountry = strTempCompoundCountry & " - " & drCountry("AML_COUNTRY_Name")
                        End If
                        txt_CustomerDetail_Nationality.Value = strTempCompoundCountry
                    Else
                        txt_CustomerDetail_Nationality.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("FK_AML_PEKERJAAN_CODE")) Then
                        strQuery = "SELECT TOP 1 * FROM AML_PEKERJAAN WHERE FK_AML_PEKERJAAN_CODE='" & drCustomer("FK_AML_PEKERJAAN_CODE") & "'"
                        Dim drOccupation As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                        If drOccupation IsNot Nothing Then
                            txt_CustomerDetail_Occupation.Value = drCustomer("FK_AML_PEKERJAAN_CODE") & " - " & drOccupation("AML_PEKERJAAN_Name")
                        Else
                            txt_CustomerDetail_Occupation.Value = drCustomer("FK_AML_PEKERJAAN_CODE")
                        End If
                    Else
                        txt_CustomerDetail_Occupation.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("WORK_PLACE")) Then
                        txt_CustomerDetail_Employer.Value = drCustomer("WORK_PLACE")
                    Else
                        txt_CustomerDetail_Employer.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("FK_AML_BENTUK_BADAN_USAHA_CODE")) Then
                        txt_CustomerDetail_LegalForm.Value = drCustomer("FK_AML_BENTUK_BADAN_USAHA_CODE")
                    Else
                        txt_CustomerDetail_LegalForm.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("FK_AML_INDUSTRY_CODE")) Then
                        strQuery = "SELECT TOP 1 * FROM AML_INDUSTRY WHERE FK_AML_INDUSTRY_CODE='" & drCustomer("FK_AML_INDUSTRY_CODE") & "'"
                        Dim drIndustry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                        If drIndustry IsNot Nothing Then
                            txt_CustomerDetail_Industry.Value = drCustomer("FK_AML_INDUSTRY_CODE") & " - " & drIndustry("AML_INDUSTRY_Name")
                        Else
                            txt_CustomerDetail_Industry.Value = drCustomer("FK_AML_INDUSTRY_CODE")
                        End If
                    Else
                        txt_CustomerDetail_Industry.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("NPWP")) Then
                        txt_CustomerDetail_NPWP.Value = drCustomer("NPWP")
                    Else
                        txt_CustomerDetail_NPWP.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("INCOME_LEVEL")) Then
                        txt_CustomerDetail_Income.Value = CLng(drCustomer("INCOME_LEVEL")).ToString("#,##0.00")
                    Else
                        txt_CustomerDetail_Income.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("TUJUAN_DANA")) Then
                        txt_CustomerDetail_PurposeOfFund.Value = drCustomer("TUJUAN_DANA")
                    Else
                        txt_CustomerDetail_PurposeOfFund.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("SOURCE_OF_FUND")) Then
                        txt_CustomerDetail_SourceOfFund.Value = drCustomer("SOURCE_OF_FUND")
                    Else
                        txt_CustomerDetail_SourceOfFund.Value = ""
                    End If
                End If

                'Binding Address
                BindCustomerAddress()

                'Binding Contact/Phone
                BindCustomerContact()

                'Binding Identity
                BindCustomerIdentity()

            ElseIf CustomerType.ToLower() = "wic" Then
                txt_CustomerDetail_CIF.FieldLabel = "WIC No"
                txt_CustomerDetail_Name.FieldLabel = "WIC Name"
                Dim strQuery As String = "SELECT TOP 1 * FROM goAML_Ref_WIC WHERE WIC_No = '" & CIF & "'"
                Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                If drResult IsNot Nothing Then
                    Dim strDateFormat As String = NawaBLL.SystemParameterBLL.GetDateFormat()

                    If Not IsDBNull(drResult("FK_Customer_Type_ID")) Then
                        strQuery = "SELECT TOP 1 * FROM goAML_Ref_Customer_Type WHERE PK_Customer_Type_ID = '" & drResult("FK_Customer_Type_ID") & "'"
                        Dim drCustomerType As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundCustomerType As String = drResult("FK_Customer_Type_ID")
                        If drCustomerType IsNot Nothing AndAlso Not IsDBNull(drCustomerType("Description")) Then
                            strTempCompoundCustomerType = strTempCompoundCustomerType & " - " & drCustomerType("Description")
                        End If
                        txt_CustomerDetail_TypeCode.Value = strTempCompoundCustomerType

                        If drResult("FK_Customer_Type_ID") = 1 Then
                            txt_CustomerDetail_POB.FieldLabel = "Place of Birth"
                            txt_CustomerDetail_DOB.FieldLabel = "Date of Birth"
                            txt_CustomerDetail_Nationality.FieldLabel = "Nationality"
                            txt_CustomerDetail_Occupation.FieldLabel = "Occupation"
                            txt_CustomerDetail_Employer.FieldLabel = "Employer"
                            txt_CustomerDetail_LegalForm.FieldLabel = "Gender"
                            txt_CustomerDetail_Industry.FieldLabel = "NIK"
                            txt_CustomerDetail_NPWP.FieldLabel = "Tax Number"
                            txt_CustomerDetail_Income.FieldLabel = "Is PEP ?"
                            txt_CustomerDetail_PurposeOfFund.FieldLabel = "Email"
                            txt_CustomerDetail_SourceOfFund.FieldLabel = "Source of Fund"
                            gridIdentityInfo.Hidden = False

                            If Not IsDBNull(drResult("INDV_Birth_Place")) Then
                                txt_CustomerDetail_POB.Value = drResult("INDV_Birth_Place")
                            Else
                                txt_CustomerDetail_POB.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_BirthDate")) Then
                                txt_CustomerDetail_DOB.Value = CDate(drResult("INDV_BirthDate")).ToString(strDateFormat)
                            Else
                                txt_CustomerDetail_DOB.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Nationality1")) Then
                                strQuery = "SELECT TOP 1 * FROM goAML_Ref_Nama_Negara WHERE Kode ='" & drResult("INDV_Nationality1") & "'"
                                Dim drCountry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                Dim strTempCompoundCountry As String = drResult("INDV_Nationality1")
                                If drCountry IsNot Nothing AndAlso Not IsDBNull(drCountry("Keterangan")) Then
                                    strTempCompoundCountry = strTempCompoundCountry & " - " & drCountry("Keterangan")
                                End If
                                txt_CustomerDetail_Nationality.Value = strTempCompoundCountry
                            Else
                                txt_CustomerDetail_Nationality.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Occupation")) Then
                                txt_CustomerDetail_Occupation.Value = drResult("INDV_Occupation")
                            Else
                                txt_CustomerDetail_Occupation.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Employer_Name")) Then
                                txt_CustomerDetail_Employer.Value = drResult("INDV_Employer_Name")
                            Else
                                txt_CustomerDetail_Employer.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Gender")) Then
                                strQuery = "SELECT TOP 1 * FROM goAML_Ref_Jenis_Kelamin WHERE Kode ='" & drResult("INDV_Gender") & "'"
                                Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                Dim strTempCompoundGender As String = drResult("INDV_Gender")
                                If drGender IsNot Nothing AndAlso Not IsDBNull(drGender("Keterangan")) Then
                                    strTempCompoundGender = strTempCompoundGender & " - " & drGender("Keterangan")
                                End If
                                txt_CustomerDetail_LegalForm.Value = strTempCompoundGender
                            Else
                                txt_CustomerDetail_LegalForm.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_SSN")) Then
                                txt_CustomerDetail_Industry.Value = drResult("INDV_SSN")
                            Else
                                txt_CustomerDetail_Industry.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Tax_Number")) Then
                                txt_CustomerDetail_NPWP.Value = drResult("INDV_Tax_Number")
                            Else
                                txt_CustomerDetail_NPWP.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Tax_Reg_Number")) Then
                                If drResult("INDV_Tax_Reg_Number") = 1 Then
                                    txt_CustomerDetail_Income.Value = "True"
                                Else
                                    txt_CustomerDetail_Income.Value = "False"
                                End If
                            Else
                                txt_CustomerDetail_Income.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Email")) Then
                                txt_CustomerDetail_PurposeOfFund.Value = drResult("INDV_Email")
                            Else
                                txt_CustomerDetail_PurposeOfFund.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_SumberDana")) Then
                                txt_CustomerDetail_SourceOfFund.Value = drResult("INDV_SumberDana")
                            Else
                                txt_CustomerDetail_SourceOfFund.Value = ""
                            End If

                        ElseIf drResult("FK_Customer_Type_ID") = 2 Then
                            txt_CustomerDetail_POB.FieldLabel = "Province of Establishment"
                            txt_CustomerDetail_DOB.FieldLabel = "Date of Establishment"
                            txt_CustomerDetail_Nationality.FieldLabel = "Country of Establishment"
                            txt_CustomerDetail_Occupation.FieldLabel = "Is Closed ?"
                            txt_CustomerDetail_Employer.FieldLabel = "Closed Date"
                            txt_CustomerDetail_LegalForm.FieldLabel = "Legal Form"
                            txt_CustomerDetail_Industry.FieldLabel = "Industry"
                            txt_CustomerDetail_NPWP.FieldLabel = "Tax Number"
                            txt_CustomerDetail_Income.FieldLabel = "Incorporation Number"
                            txt_CustomerDetail_PurposeOfFund.FieldLabel = "Corporation Website"
                            txt_CustomerDetail_SourceOfFund.FieldLabel = "Corporation Email"
                            gridIdentityInfo.Hidden = True

                            If Not IsDBNull(drResult("Corp_Incorporation_State")) Then
                                txt_CustomerDetail_POB.Value = drResult("Corp_Incorporation_State")
                            Else
                                txt_CustomerDetail_POB.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Incorporation_Date")) Then
                                txt_CustomerDetail_DOB.Value = CDate(drResult("Corp_Incorporation_Date")).ToString(strDateFormat)
                            Else
                                txt_CustomerDetail_DOB.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Incorporation_Country_Code")) Then
                                strQuery = "SELECT TOP 1 * FROM goAML_Ref_Nama_Negara WHERE Kode ='" & drResult("Corp_Incorporation_Country_Code") & "'"
                                Dim drCountry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                Dim strTempCompoundCountry As String = drResult("Corp_Incorporation_Country_Code")
                                If drCountry IsNot Nothing AndAlso Not IsDBNull(drCountry("Keterangan")) Then
                                    strTempCompoundCountry = strTempCompoundCountry & " - " & drCountry("Keterangan")
                                End If
                                txt_CustomerDetail_Nationality.Value = strTempCompoundCountry
                            Else
                                txt_CustomerDetail_Nationality.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Business_Closed")) Then
                                If drResult("Corp_Business_Closed") = 1 Then
                                    txt_CustomerDetail_Occupation.Value = "True"
                                Else
                                    txt_CustomerDetail_Occupation.Value = "False"
                                End If
                            Else
                                txt_CustomerDetail_Occupation.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Date_Business_Closed")) Then
                                txt_CustomerDetail_Employer.Value = CDate(drResult("Corp_Date_Business_Closed")).ToString(strDateFormat)
                            Else
                                txt_CustomerDetail_Employer.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Incorporation_Legal_Form")) Then
                                strQuery = "SELECT TOP 1 * FROM goAML_Ref_Bentuk_Badan_Usaha WHERE Kode ='" & drResult("Corp_Incorporation_Legal_Form") & "'"
                                Dim drBadanUsaha As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                Dim strTempCompoundBadanUsaha As String = drResult("Corp_Incorporation_Legal_Form")
                                If drBadanUsaha IsNot Nothing AndAlso Not IsDBNull(drBadanUsaha("Keterangan")) Then
                                    strTempCompoundBadanUsaha = strTempCompoundBadanUsaha & " - " & drBadanUsaha("Keterangan")
                                End If
                                txt_CustomerDetail_LegalForm.Value = strTempCompoundBadanUsaha
                            Else
                                txt_CustomerDetail_LegalForm.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Business")) Then
                                txt_CustomerDetail_Industry.Value = drResult("Corp_Business")
                            Else
                                txt_CustomerDetail_Industry.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Tax_Number")) Then
                                txt_CustomerDetail_NPWP.Value = drResult("Corp_Tax_Number")
                            Else
                                txt_CustomerDetail_NPWP.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Incorporation_Number")) Then
                                txt_CustomerDetail_Income.Value = drResult("Corp_Incorporation_Number")
                            Else
                                txt_CustomerDetail_Income.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Url")) Then
                                txt_CustomerDetail_PurposeOfFund.Value = drResult("Corp_Url")
                            Else
                                txt_CustomerDetail_PurposeOfFund.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Email")) Then
                                txt_CustomerDetail_SourceOfFund.Value = drResult("Corp_Email")
                            Else
                                txt_CustomerDetail_SourceOfFund.Value = ""
                            End If
                        End If
                    Else
                        txt_CustomerDetail_TypeCode.Value = ""
                    End If


                    'Binding Address
                    BindWICAddress(drResult("PK_Customer_ID"))

                    'Binding Contact/Phone
                    BindWICContact(drResult("PK_Customer_ID"))

                    'Binding Identity
                    BindWICIdentity(drResult("PK_Customer_ID"))
                Else
                    Throw New ApplicationException("No AML Customer Data Found With CIF = " & CIF)
                End If
            Else
                Throw New ApplicationException("Customer Type are not identified, please report this to admin")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Btn_CustomerDetail_Back_Click()
        Try
            WindowPopUpDataCustomer.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub SetCommandColumnLocation()
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If objParamSettingbutton IsNot Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If

            ColumnActionLocation(GridPanel_Activity, CommandColumnActivity, bsettingRight)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Btn_AddActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            ActionActivity = enumActionForm.Add
            ClearWindowActivity()
            ActivityChangeShowAsAction()
            WindowActivity.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridCommandActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            PK_Activity = CLng(ID)
            If e.ExtraParams(1).Value = "Delete" Then
                Dim strselect As String = "PK_OneFCC_CaseManagement_Typology_NonTransaction_ID = " & PK_Activity
                Dim dtrow As DataRow = DataTabelActivity.Select(strselect).FirstOrDefault()
                Dim indexrow As Integer = DataTabelActivity.Rows.IndexOf(dtrow)
                DataTabelActivity.Rows(indexrow).Delete()
                BindTableActivity()
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                ActionActivity = enumActionForm.Edit
                ClearWindowActivity()
                ActivityChangeShowAsAction()
                LoadDataActivity()
                WindowActivity.Hidden = False
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                ActionActivity = enumActionForm.Detail
                ClearWindowActivity()
                ActivityChangeShowAsAction()
                LoadDataActivity()
                WindowActivity.Hidden = False
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Activity_Back_Click()
        Try
            WindowActivity.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Activity_Submit_Click()
        Try
            If activity_Date.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException(activity_Date.FieldLabel & " is Mandatory")
            End If
            If String.IsNullOrWhiteSpace(activity_Desc.Value) Then
                Throw New ApplicationException(activity_Desc.FieldLabel & " is Mandatory")
            End If
            If CustomerType.ToLower() = "customer" AndAlso String.IsNullOrWhiteSpace(nDSDropDownField_AccountNo.SelectedItemValue) Then
                Throw New ApplicationException(nDSDropDownField_AccountNo.Label & " is Mandatory For Customer")
            End If
            Dim AccountNo As String = ""
            If ActionActivity = enumActionForm.Add Then
                PK_Activity_NewID = PK_Activity_NewID - 1
                Dim tempDataRow As DataRow = DataTabelActivity.NewRow()
                tempDataRow("PK_OneFCC_CaseManagement_Typology_NonTransaction_ID") = PK_Activity_NewID
                tempDataRow("Date_Activity") = activity_Date.SelectedDate
                tempDataRow("Activity_Description") = activity_Desc.Value
                If CustomerType.ToLower() = "customer" Then
                    tempDataRow("Account_NO") = nDSDropDownField_AccountNo.SelectedItemValue
                End If
                DataTabelActivity.Rows.Add(tempDataRow)
            ElseIf ActionActivity = enumActionForm.Edit Then
                Dim strselect As String = "PK_OneFCC_CaseManagement_Typology_NonTransaction_ID = " & PK_Activity
                Dim dtrow As DataRow = DataTabelActivity.Select(strselect).FirstOrDefault()
                Dim indexrow As Integer = DataTabelActivity.Rows.IndexOf(dtrow)
                DataTabelActivity.Rows(indexrow)("Date_Activity") = activity_Date.SelectedDate
                DataTabelActivity.Rows(indexrow)("Activity_Description") = activity_Desc.Value
                If CustomerType.ToLower() = "customer" Then
                    DataTabelActivity.Rows(indexrow)("Account_NO") = nDSDropDownField_AccountNo.SelectedItemValue
                End If
            End If
            BindTableActivity()
            WindowActivity.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ClearWindowActivity()
        Try
            nDSDropDownField_AccountNo.SetTextValue("")
            activity_Date.Clear()
            activity_Desc.Value = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadDataActivity()
        Try
            Dim strselect As String = "PK_OneFCC_CaseManagement_Typology_NonTransaction_ID = " & PK_Activity
            Dim dtrow As DataRow = DataTabelActivity.Select(strselect).FirstOrDefault()
            If Not IsDBNull(dtrow("Date_Activity")) Then
                activity_Date.SelectedDate = dtrow("Date_Activity")
            End If
            If Not IsDBNull(dtrow("Activity_Description")) Then
                activity_Desc.Value = dtrow("Activity_Description")
            End If
            If CustomerType.ToLower() = "customer" Then
                If Not IsDBNull(dtrow("Account_NO")) Then
                    nDSDropDownField_AccountNo.SetTextWithTextValue(dtrow("Account_NO"), dtrow("Account_NO"))
                End If
            End If
            'Dim indexrow As Integer = DataTabelActivity.Rows.IndexOf(dtrow)
            'If Not IsDBNull(DataTabelActivity.Rows(indexrow)("Date_Activity")) Then
            '    activity_Date.SelectedDate = DataTabelActivity.Rows(indexrow)("Date_Activity")
            'End If
            'If Not IsDBNull(DataTabelActivity.Rows(indexrow)("Activity_Description")) Then
            '    activity_Desc.Value = DataTabelActivity.Rows(indexrow)("Activity_Description")
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ActivityChangeShowAsAction()
        Try
            If ActionActivity = enumActionForm.Add OrElse ActionActivity = enumActionForm.Edit Then
                btn_Activity_Submit.Hidden = False
                If CustomerType.ToLower() = "customer" Then
                    nDSDropDownField_AccountNo.StringFieldStyle = "background-color: #FFE4C4"
                    nDSDropDownField_AccountNo.IsReadOnly = False
                End If
                activity_Desc.ReadOnly = False
                activity_Desc.FieldStyle = "background-color: #FFE4C4"
                activity_Date.ReadOnly = False
                activity_Date.FieldStyle = "background-color: #FFE4C4"
                btn_Activity_Back.Icon = Icon.Cancel
                btn_Activity_Back.Text = "Cancel"
            ElseIf ActionActivity = enumActionForm.Detail Then
                btn_Activity_Submit.Hidden = True
                If CustomerType.ToLower() = "customer" Then
                    nDSDropDownField_AccountNo.StringFieldStyle = "background-color: #ddd"
                    nDSDropDownField_AccountNo.IsReadOnly = True
                End If
                activity_Desc.ReadOnly = True
                activity_Desc.FieldStyle = "background-color: #ddd"
                activity_Date.ReadOnly = True
                activity_Date.FieldStyle = "background-color: #ddd"
                btn_Activity_Back.Icon = Icon.PageBack
                btn_Activity_Back.Text = "Back"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindTableActivity()
        Try
            GridPanel_Activity.GetStore.DataSource = DataTabelActivity
            GridPanel_Activity.GetStore.DataBind()
            'GridPanel_Activity.Update()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindCustomerAddress()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_COUNTRY"
            Dim dtCountry As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            strQuery = "SELECT * FROM AML_ADDRESS_TYPE"
            Dim dtAddressType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding Address
            strQuery = "SELECT * FROM AML_CUSTOMER_ADDRESS WHERE CIFNO='" & txt_CIFNo_WICNo.Value & "'"
            Dim dtAddress As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtAddress Is Nothing Then
                dtAddress = New DataTable
            End If
            dtAddress.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))
            dtAddress.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))

            For Each item In dtAddress.Rows
                item("AML_ADDRESS_TYPE_NAME") = item("FK_AML_ADDRESS_TYPE_CODE")
                If dtAddressType IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_ADDRESS_TYPE_CODE")) Then
                    Dim drCek = dtAddressType.Select("FK_AML_ADDRES_TYPE_CODE='" & item("FK_AML_ADDRESS_TYPE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("AML_ADDRESS_TYPE_NAME") = item("FK_AML_ADDRESS_TYPE_CODE") & " - " & drCek("ADDRESS_TYPE_NAME")
                    End If
                End If

                item("COUNTRY_NAME") = item("FK_AML_COUNTRY_CODE")
                If dtCountry IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_COUNTRY_CODE")) Then
                    Dim drCek = dtCountry.Select("FK_AML_COUNTRY_CODE='" & item("FK_AML_COUNTRY_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("COUNTRY_NAME") = item("FK_AML_COUNTRY_CODE") & " - " & drCek("AML_COUNTRY_Name")
                    End If
                End If
            Next

            gridAddressInfo.GetStore.DataSource = dtAddress
            gridAddressInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindCustomerContact()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_CONTACT_TYPE"
            Dim dtContactType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding Contact
            strQuery = "SELECT * FROM AML_CUSTOMER_CONTACT WHERE CIFNO='" & txt_CIFNo_WICNo.Value & "'"
            Dim dtContact As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtContact Is Nothing Then
                dtContact = New DataTable
            End If
            dtContact.Columns.Add(New DataColumn("AML_CONTACT_TYPE_NAME", GetType(String)))

            For Each item In dtContact.Rows
                item("AML_CONTACT_TYPE_NAME") = item("FK_AML_CONTACT_TYPE_CODE")
                If dtContactType IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_CONTACT_TYPE_CODE")) Then
                    Dim drCek = dtContactType.Select("FK_AML_CONTACT_TYPE_CODE='" & item("FK_AML_CONTACT_TYPE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("AML_CONTACT_TYPE_NAME") = item("FK_AML_CONTACT_TYPE_CODE") & " - " & drCek("CONTACT_TYPE_NAME")
                    End If
                End If
            Next

            gridPhoneInfo.GetStore.DataSource = dtContact
            gridPhoneInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindCustomerIdentity()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_IDENTITY_TYPE"
            Dim dtIDENTITYType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            strQuery = "SELECT * FROM AML_COUNTRY"
            Dim dtCountry As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding IDENTITY
            strQuery = "SELECT * FROM AML_CUSTOMER_IDENTITY WHERE CIFNO='" & txt_CIFNo_WICNo.Value & "'"
            Dim dtIDENTITY As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtIDENTITY Is Nothing Then
                dtIDENTITY = New DataTable
            End If
            dtIDENTITY.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
            dtIDENTITY.Columns.Add(New DataColumn("IDENTITY_TYPE_NAME", GetType(String)))

            For Each item In dtIDENTITY.Rows
                item("IDENTITY_TYPE_NAME") = item("FK_AML_IDENTITY_TYPE_CODE")
                If dtIDENTITYType IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_IDENTITY_TYPE_CODE")) Then
                    Dim drCek = dtIDENTITYType.Select("FK_AML_IDENTITY_TYPE_CODE='" & item("FK_AML_IDENTITY_TYPE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("IDENTITY_TYPE_NAME") = item("FK_AML_IDENTITY_TYPE_CODE") & " - " & drCek("IDENTITY_TYPE_NAME")
                    End If
                End If
                item("COUNTRY_NAME") = item("FK_AML_COUNTRYISSUE_CODE")
                If dtCountry IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_COUNTRYISSUE_CODE")) Then
                    Dim drCek = dtCountry.Select("FK_AML_COUNTRY_CODE='" & item("FK_AML_COUNTRYISSUE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("COUNTRY_NAME") = item("FK_AML_COUNTRYISSUE_CODE") & " - " & drCek("AML_COUNTRY_Name")
                    End If
                End If
            Next

            gridIdentityInfo.GetStore.DataSource = dtIDENTITY
            gridIdentityInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindWICAddress(PK_WIC As Long)
        Try
            Dim strQuery As String = ""

            'Binding Address
            strQuery = " SELECT "
            strQuery = strQuery & " addresstype.Keterangan AS AML_ADDRESS_TYPE_NAME "
            strQuery = strQuery & " , refaddress.Address AS CUSTOMER_ADDRESS "
            strQuery = strQuery & " , refaddress.Country_Code AS FK_AML_COUNTRY_CODE "
            strQuery = strQuery & " , namanegara.Keterangan AS COUNTRY_NAME "
            strQuery = strQuery & " , refaddress.Zip AS KODEPOS "
            strQuery = strQuery & " , refaddress.Town AS KECAMATAN "
            strQuery = strQuery & " , refaddress.City AS KOTAKABUPATEN "
            strQuery = strQuery & " FROM goaml_ref_address refaddress "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Kategori_Kontak addresstype "
            strQuery = strQuery & " ON refaddress.Address_Type = addresstype.Kode "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Nama_Negara namanegara "
            strQuery = strQuery & " ON refaddress.Country_Code = namanegara.Kode "
            strQuery = strQuery & " WHERE refaddress.FK_Ref_Detail_Of = 3 "
            strQuery = strQuery & " AND refaddress.FK_To_Table_ID = " & PK_WIC
            Dim dtAddress As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            'If dtAddress Is Nothing Then
            '    dtAddress = New DataTable
            'End If

            gridAddressInfo.GetStore.DataSource = dtAddress
            gridAddressInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindWICContact(PK_WIC As Long)
        Try
            Dim strQuery As String = ""

            'Binding Contact
            strQuery = " SELECT "
            strQuery = strQuery & " kontaktype.Keterangan AS AML_CONTACT_TYPE_NAME "
            strQuery = strQuery & " , alatkomunikasi.Keterangan AS FK_AML_COMMUNICATION_TYPE_CODE "
            strQuery = strQuery & " , refphone.tph_country_prefix AS AREA_CODE "
            strQuery = strQuery & " , refphone.tph_number AS CUSTOMER_CONTACT_NUMBER "
            strQuery = strQuery & " , refphone.tph_extension AS EXTENTION "
            strQuery = strQuery & " , refphone.comments AS NOTES "
            strQuery = strQuery & " FROM goAML_Ref_Phone refphone "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Kategori_Kontak kontaktype "
            strQuery = strQuery & " ON refphone.Tph_Contact_Type = kontaktype.Kode "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Jenis_Alat_Komunikasi alatkomunikasi "
            strQuery = strQuery & " ON refphone.Tph_Communication_Type = alatkomunikasi.Kode "
            strQuery = strQuery & " WHERE FK_Ref_Detail_Of = 3 "
            strQuery = strQuery & " AND FK_for_Table_ID = " & PK_WIC
            Dim dtContact As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            'If dtContact Is Nothing Then
            '    dtContact = New DataTable
            'End If

            gridPhoneInfo.GetStore.DataSource = dtContact
            gridPhoneInfo.GetStore.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindWICIdentity(PK_WIC As Long)
        Try
            Dim strQuery As String = ""

            'Binding IDENTITY
            strQuery = " SELECT "
            strQuery = strQuery & " identification.Number AS CUSTOMER_IDENTITY_NUMBER "
            strQuery = strQuery & " , identification.Type AS FK_AML_IDENTITY_TYPE_CODE "
            strQuery = strQuery & " , identification.Issue_Date AS DATE_OF_ISSUE "
            strQuery = strQuery & " , identification.Expiry_Date AS DATE_OF_EXPIRED "
            strQuery = strQuery & " , identification.Issued_By AS ISSUE_By "
            strQuery = strQuery & " , identification.Issued_Country AS fk_aml_countryissue_code "
            strQuery = strQuery & " , identification.Identification_Comment AS NOTES "
            strQuery = strQuery & " , identificationtype.Keterangan AS IDENTITY_TYPE_NAME "
            strQuery = strQuery & " , namanegara.Keterangan AS COUNTRY_NAME "
            strQuery = strQuery & " FROM goAML_Person_Identification identification "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Jenis_Dokumen_Identitas identificationtype "
            strQuery = strQuery & " ON identification.Type = identificationtype.Kode "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Nama_Negara namanegara "
            strQuery = strQuery & " ON identification.Issued_Country = namanegara.Kode "
            strQuery = strQuery & " WHERE FK_Person_Type = 8 "
            strQuery = strQuery & " AND FK_Person_ID = " & PK_WIC
            Dim dtIDENTITY As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            'If dtIDENTITY Is Nothing Then
            '    dtIDENTITY = New DataTable
            'End If

            gridIdentityInfo.GetStore.DataSource = dtIDENTITY
            gridIdentityInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub StoreActivity_ReadData(sender As Object, e As StoreReadDataEventArgs)
    '    Try
    '        Dim intStart As Integer = e.Start
    '        Dim intLimit As Integer = e.Limit
    '        Dim inttotalRecord As Integer
    '        Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

    '        Dim strsort As String = ""
    '        For Each item As DataSorter In e.Sort
    '            If strsort = "" Then
    '                strsort += item.Property & " " & item.Direction.ToString
    '            Else
    '                strsort += " , " & item.Property & " " & item.Direction.ToString
    '            End If
    '        Next

    '        Dim dataTabel As DataTable = DataTabelActivity

    '        Dim limit As Integer = e.Limit
    '        If (e.Start + e.Limit) > inttotalRecord Then
    '            limit = inttotalRecord - e.Start
    '        End If

    '        e.Total = inttotalRecord

    '        GridPanel_Activity.GetStore.DataSource = dataTabel
    '        GridPanel_Activity.GetStore.DataBind()
    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
#End Region

#Region "05-Jan-2023 Felix load Revision"
    Protected Sub LoadReviseData(PK As String)
        Try
            Dim strSQL As String = "SELECT a.Is_Transaction, a.PK_OneFCC_CaseManagement_Typology_ID, b.* FROM onefcc_casemanagement_typology a inner join OneFCC_CaseManagement b on a.FK_CaseManagement_ID = b.PK_CaseManagement_ID WHERE PK_CaseManagement_ID=" & IDUnik
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            If drCM IsNot Nothing Then
                If Not IsDBNull(drCM("Alert_Type")) Then
                    Dim RowAlertType As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_CaseManagement_AlertType_ID, Alert_Type FROM OneFCC_CaseManagement_AlertType where Alert_Type = '" & drCM("Alert_Type") & "'", Nothing)
                    If RowAlertType IsNot Nothing Then
                        nDSDropDownField_AlertType.SetTextWithTextValue(RowAlertType("PK_CaseManagement_AlertType_ID"), RowAlertType("Alert_Type"))
                    End If

                End If
                If Not IsDBNull(drCM("FK_Rule_Basic_ID")) Then
                    Dim RowRuleBasic As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_Rule_Basic_ID, Rule_Basic_Name FROM onefcc_ms_rule_basic where PK_Rule_Basic_ID = '" & drCM("FK_Rule_Basic_ID") & "'", Nothing)
                    If RowRuleBasic IsNot Nothing Then
                        nDSDropDownField_RuleBasic.SetTextWithTextValue(RowRuleBasic("PK_Rule_Basic_ID"), RowRuleBasic("Rule_Basic_Name"))
                    End If
                    nDSDropDownField_RuleBasic.IsHidden = False
                Else
                    nDSDropDownField_RuleBasic.IsHidden = True
                End If
                If Not IsDBNull(drCM("Case_Description")) Then
                    textArea_CaseDescription.Value = drCM("Case_Description")
                End If
                If Not IsDBNull(drCM("CIF_No")) Then
                    txt_CIFNo_WICNo.Value = drCM("CIF_No")

                    nDSDropDownField_AccountNo.StringFilter = " client_number = '" & drCM("CIF_No") & "' "
                    btn_OpenWindowCustomer.Hidden = False
                End If
                If Not IsDBNull(drCM("Customer_Name")) Then
                    txt_Name.Value = drCM("Customer_Name")
                End If
                If Not IsDBNull(drCM("WIC_No")) Then
                    txt_CIFNo_WICNo.Value = drCM("WIC_No")
                    btn_OpenWindowCustomer.Hidden = False
                End If
                If Not IsDBNull(drCM("WIC_Name")) Then
                    txt_Name.Value = drCM("WIC_Name")
                End If

                If Not IsDBNull(drCM("PK_OneFCC_CaseManagement_Typology_ID")) Then
                    If Not IsDBNull(drCM("Is_Transaction")) Then
                        If drCM("Is_Transaction") = True Then

                            checkbox_IsTransaction.Checked = True
                            Checkbox_IsTransaction_click()

                            'Dim strQueryTransaction As String = "select * from onefcc_casemanagement_typology_transaction where FK_OneFCC_CaseManagement_Typology_ID = " & drCM("PK_OneFCC_CaseManagement_Typology_ID")
                            'Dim dtPreviousTransaction As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryTransaction,)

                            'gp_CaseAlert_Previous_Typology_Transaction.GetStore.DataSource = dtPreviousTransaction
                            'gp_CaseAlert_Previous_Typology_Transaction.DataBind()

                            gp_CaseAlert_Previous_Typology_Transaction.Hidden = False
                            gp_CaseAlert_Previous_Typology_Activity.Hidden = True
                        Else
                            checkbox_IsTransaction.Checked = False
                            Checkbox_IsTransaction_click()

                            'Dim strQueryActivity As String = "select * from onefcc_casemanagement_typology_NonTransaction where FK_OneFCC_CaseManagement_Typology_ID = " & drCM("PK_OneFCC_CaseManagement_Typology_ID")
                            'Dim dtPreviousActivity As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryActivity,)

                            'gp_CaseAlert_Previous_Typology_Activity.GetStore.DataSource = dtPreviousActivity
                            'gp_CaseAlert_Previous_Typology_Activity.DataBind()

                            gp_CaseAlert_Previous_Typology_Transaction.Hidden = True
                            gp_CaseAlert_Previous_Typology_Activity.Hidden = False
                        End If
                    End If
                End If


            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Typology_Transaction(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Previous_Typology_Transaction.GetStore.DataSource = DataPaging
            gp_CaseAlert_Previous_Typology_Transaction.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Activity(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT "
            strQuery &= "	ofcmt.FK_CaseManagement_ID "
            strQuery &= "	,ofcmtnt.Account_NO + ' - ' + ofmrb.Rule_Basic_Name AS Alert_Group "
            strQuery &= "	,ofmrb.Rule_Basic_Name As Case_Description "
            strQuery &= "	,ofcmtnt.CIF_NO "
            strQuery &= "	,ofcmtnt.Account_NO "
            strQuery &= "	,ofcmtnt.Date_Activity "
            strQuery &= "	,ofcmtnt.Activity_Description "
            strQuery &= "FROM OneFCC_CaseManagement_Typology_NonTransaction AS ofcmtnt "
            strQuery &= "JOIN OneFCC_CaseManagement_Typology As ofcmt "
            strQuery &= "	ON ofcmtnt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " Left Join OneFcc_MS_Rule_Basic AS ofmrb "
            strQuery &= " On ofcmt.FK_Rule_Basic_ID = ofmrb.PK_Rule_Basic_ID "
            strQuery &= " WHERE FK_CaseManagement_ID = " & IDUnik

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Previous_Typology_Activity.GetStore.DataSource = DataPaging
            gp_CaseAlert_Previous_Typology_Activity.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub UpdateDataHeader(pk As Long)
        Try
            Try
                Dim strQuery As String = ""
                strQuery &= " update OneFCC_CaseManagement "
                strQuery &= " set Alert_Type = '" & nDSDropDownField_AlertType.SelectedItemText & "'"
                If nDSDropDownField_AlertType.SelectedItemText = "Typology Risk" Then
                    strQuery &= " , FK_Rule_Basic_ID = '" & nDSDropDownField_RuleBasic.SelectedItemValue & "' "
                Else
                    strQuery &= " , FK_Rule_Basic_ID = null "
                End If
                strQuery &= " , Case_Description = '" & textArea_CaseDescription.Value & "'"
                If CustomerType.ToLower() = "customer" Then
                    strQuery &= " , Is_Customer = 1 "
                    strQuery &= " , CIF_No = '" & txt_CIFNo_WICNo.Value & "'"
                    strQuery &= " , Customer_Name = '" & txt_Name.Value & "'"
                ElseIf CustomerType.ToLower() = "wic" Then
                    strQuery &= " , Is_Customer = 0 "
                    strQuery &= " , WIC_No = '" & txt_CIFNo_WICNo.Value & "'"
                    strQuery &= " , WIC_Name = '" & txt_Name.Value & "'"
                End If
                strQuery &= " , FK_CaseStatus_ID = -1 "
                strQuery &= " , Alternateby = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                strQuery &= " , LastUpdateDate = getdate() "
                strQuery &= " where PK_CaseManagement_ID = " & IDUnik

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Catch ex As Exception
                Throw ex
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub store_ReadData_WorkflowHistory(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort &= item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT history.* FROM onefcc_casemanagement_workflowhistory history"
            strQuery &= " WHERE FK_CaseManagement_ID = " & IDUnik

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)

            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_WorkflowHistory.GetStore.DataSource = DataPaging
            gp_WorkflowHistory.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class