﻿Imports Ext
Imports Elmah
Imports System.Data
Imports CasemanagementBLL
Imports CasemanagementDAL
Imports System.Data.SqlClient
Imports OfficeOpenXml

Partial Class CaseManagementAlertApprovalDetail
    Inherits ParentPage

    Public Property IDModule() As String
        Get
            Return Session("CaseManagementAlertApprovalDetail.IDModule")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertApprovalDetail.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("CaseManagementAlertApprovalDetail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertApprovalDetail.IDUnik") = value
        End Set
    End Property

    Public Property CIFNo() As String
        Get
            Return Session("CaseManagementAlertApprovalDetail.CIFNo")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertApprovalDetail.CIFNo") = value
        End Set
    End Property

    Public Property AlertType() As String
        Get
            Return Session("CaseManagementAlertApprovalDetail.AlertType")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertApprovalDetail.AlertType") = value
        End Set
    End Property

    Public Property CustomerType() As String
        Get
            Return Session("CaseManagementAlertApprovalDetail.CustomerType")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertApprovalDetail.CustomerType") = value
        End Set
    End Property

    Public Property DataTabelActivity As DataTable
        Get
            Return Session("CaseManagementAlertApprovalDetail.DataTabelActivity")
        End Get
        Set(ByVal value As DataTable)
            Session("CaseManagementAlertApprovalDetail.DataTabelActivity") = value
        End Set
    End Property

    Public Property ActionActivity() As enumActionForm
        Get
            Return Session("CaseManagementAlertApprovalDetail.ActionActivity")
        End Get
        Set(ByVal value As enumActionForm)
            Session("CaseManagementAlertApprovalDetail.ActionActivity") = value
        End Set
    End Property

    Public Property PK_Activity_NewID() As Long
        Get
            Return Session("CaseManagementAlertApprovalDetail.PK_Activity_NewID")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertApprovalDetail.PK_Activity_NewID") = value
        End Set
    End Property

    Public Property PK_Activity() As Long
        Get
            Return Session("CaseManagementAlertApprovalDetail.PK_Activity")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertApprovalDetail.PK_Activity") = value
        End Set
    End Property

    Enum enumActionForm
        Detail = 0
        Add = 1
        Edit = 2
        Delete = 3
        Accept = 4
        Reject = 5
    End Enum

    Private Sub CaseManagementAlertApprovalDetail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        Try
            ActionType = NawaBLL.Common.ModuleActionEnum.Approval
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            IDModule = Request.Params("ModuleID")
            CustomerType = Request.Params("Type")

            Dim intModuleID As New Integer
            If IDModule IsNot Nothing Then
                intModuleID = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()
                FormPanelInput.Title = ObjModule.ModuleLabel & " - Approval Detail"

                PK_Activity = 0
                DataTabelActivity = New DataTable
                LoadColumnDataTabelActivity()
                If CustomerType.ToLower() = "customer" Then
                    txt_CIFNo_WICNo.FieldLabel = "CIF No"
                    txt_Name.FieldLabel = "Customer Name"
                    nDSDropDownField_AccountNo.Hidden = False
                    ColumnActivityAccountNo.Hidden = False
                    Customer_Information.Title = "Customer Information"
                ElseIf CustomerType.ToLower() = "wic" Then
                    txt_CIFNo_WICNo.FieldLabel = "WIC No"
                    txt_Name.FieldLabel = "WIC Name"
                    txt_RuleBasic.Hidden = False
                    checkbox_IsTransaction.Hidden = True

                    nDSDropDownField_AccountNo.Hidden  = True
                    ColumnActivityAccountNo.Hidden = True
                    Customer_Information.Title = "WIC Information"
                    WindowPopUpDataCustomer.Title = "Data WIC Information"
                End If

                'Load Data
                LoadDataCaseManagement()

                '20-Apr-2022 Adi
                'txt_Alert_Type.Hidden = True
                'txt_Case_Description.Hidden = True

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub ClearSession()
        Try
            'CIFNo = Nothing
            AlertType = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadDataCaseManagement()
        Try
            Dim strSQL As String = ""
            If CustomerType.ToLower() = "customer" Then
                strSQL = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            ElseIf CustomerType.ToLower() = "wic" Then
                strSQL = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & IDUnik
            End If
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            If drCM IsNot Nothing Then

                If CustomerType.ToLower() = "customer" Then
                    'If Not IsDBNull(drCM("CIF_No")) Then
                    '    Me.CIFNo = drCM("CIF_No")
                    'End If
                    If Not IsDBNull(drCM("CIF_No")) Then
                        Me.CIFNo = drCM("CIF_No")
                        txt_CIFNo_WICNo.Value = drCM("CIF_No")
                    End If
                    If Not IsDBNull(drCM("Customer_Name")) Then
                        txt_Name.Value = drCM("Customer_Name")
                    End If
                ElseIf CustomerType.ToLower() = "wic" Then
                    If Not IsDBNull(drCM("WIC_No")) Then
                        Me.CIFNo = drCM("WIC_No")
                        txt_CIFNo_WICNo.Value = drCM("WIC_No")
                    End If
                    If Not IsDBNull(drCM("WIC_Name")) Then
                        txt_Name.Value = drCM("WIC_Name")
                    End If
                End If

                'General Information
                If Not IsDBNull(drCM("PK_CaseManagement_ID")) Then
                    txt_PK_CaseManagement_ID.Value = drCM("PK_CaseManagement_ID")
                End If

                If Not IsDBNull(drCM("Case_Description")) Then
                    txt_Case_Description.Value = drCM("Case_Description")
                End If

                If Not IsDBNull(drCM("Alert_Type")) Then
                    txt_Alert_Type.Value = drCM("Alert_Type")
                    Me.AlertType = drCM("Alert_Type")
                Else
                    Throw New ApplicationException("Alert Type is Mandatory But Returned NULL Please Report this to admin.")
                End If

                If Not IsDBNull(drCM("FK_CaseStatus_ID")) Then
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_ProposedAction WHERE PK_OneFCC_CaseManagement_ProposedAction_ID=" & drCM("FK_CaseStatus_ID")
                    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drTemp IsNot Nothing Then
                        txt_FK_Case_Status_ID.Value = drTemp("PK_OneFCC_CaseManagement_ProposedAction_ID") & " - " & drTemp("Proposed_Action")
                    Else
                        txt_FK_Case_Status_ID.Value = "New Case"
                    End If
                Else
                    txt_FK_Case_Status_ID.Value = "New Case"
                End If

                If Not IsDBNull(drCM("CreatedDate")) Then
                    txt_CreatedDate.Value = CDate(drCM("CreatedDate")).ToString("dd-MMM-yyyy HH:mm:ss")
                End If

                If Not IsDBNull(drCM("FK_Proposed_Status_ID")) Then
                    Dim strTempStatus As String = drCM("FK_Proposed_Status_ID")
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_ProposedAction WHERE PK_OneFCC_CaseManagement_ProposedAction_ID=" & drCM("FK_Proposed_Status_ID")
                    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drTemp IsNot Nothing AndAlso Not IsDBNull(drTemp("PROPOSED_ACTION")) Then
                        strTempStatus = strTempStatus & " - " & drTemp("PROPOSED_ACTION")
                    End If
                    txt_FK_Case_Status_ID.Value = strTempStatus
                End If

                '20-Apr-2022 Adi : Display Process Date
                If Not IsDBNull(drCM("ProcessDate")) Then
                    txt_ProcessDate.Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                End If

                '20-Apr-2022 Adi
                If CustomerType.ToLower() = "customer" Then
                    FormPanelInput.Title = ObjModule.ModuleLabel & " - Approval Detail&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ " & drCM("CIF_No") & " - " & drCM("Customer_Name") & " ]"
                ElseIf CustomerType.ToLower() = "wic" Then
                    FormPanelInput.Title = ObjModule.ModuleLabel & " - Approval Detail&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ " & drCM("WIC_No") & " - " & drCM("WIC_Name") & " ]"
                End If

                'Switch between Typology/Outlier Transaction based on Alert_Type
                gp_CaseAlert_Typology_Transaction.Hidden = True
                pnl_CaseAlert_Outlier_Transaction.Hidden = True

                If Not IsDBNull(drCM("Alert_Type")) Then
                    Select Case drCM("Alert_Type")
                        Case "Typology Risk"
                            Dim strSQLTypology As String = "SELECT * FROM OneFCC_CaseManagement_Typology WHERE FK_CaseManagement_ID = " & IDUnik
                            Dim drCMTypology As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQLTypology)
                            If drCMTypology Is Nothing Then
                                Throw New ApplicationException("Could not find Case Manageent Typology, Please  report this to admin.")
                            End If
                            If IsDBNull(drCMTypology("Is_Transaction")) OrElse IsDBNull(drCMTypology("PK_OneFCC_CaseManagement_Typology_ID")) Then
                                Throw New ApplicationException("Is Transaction Or ID OneFCC_CaseManagement_Typology is null, please report this to admin.")
                            End If
                            If drCMTypology("Is_Transaction") = True Then
                                checkbox_IsTransaction.Value = "True"
                                gp_CaseAlert_Typology_Transaction.Hidden = False
                            ElseIf drCMTypology("Is_Transaction") = False Then
                                checkbox_IsTransaction.Value = "False"
                                Dim strSQLActivity As String = " SELECT PK_OneFCC_CaseManagement_Typology_NonTransaction_ID, Date_Activity, Activity_Description, Account_NO "
                                strSQLActivity = strSQLActivity & " FROM OneFCC_CaseManagement_Typology_NonTransaction WHERE FK_OneFCC_CaseManagement_Typology_ID = " & drCMTypology("PK_OneFCC_CaseManagement_Typology_ID")
                                DataTabelActivity = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQLActivity, Nothing)
                                BindTableActivity()
                                panel_Activity.Hidden = False
                            End If
                            txt_RuleBasic.Hidden = False
                            If Not IsDBNull(drCM("FK_Rule_Basic_ID")) Then
                                strSQL = "SELECT TOP 1 Rule_Basic_Name from OneFcc_MS_Rule_Basic where PK_Rule_Basic_ID = " & drCM("FK_Rule_Basic_ID")
                                Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                                If drTemp IsNot Nothing Then
                                    txt_RuleBasic.Value = drCM("FK_Rule_Basic_ID") & " - " & drTemp("Rule_Basic_Name")
                                End If
                            End If
                        Case "Financial Risk"
                            pnl_CaseAlert_Outlier_Transaction.Hidden = False

                            'Display Mean dan Modus
                            strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=2"
                            Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                            If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                                fs_CaseAlert_Outlier_Transaction.Title = "<b>Financial Statistics in past " & drParam("ParameterValue") & " month(s)</b>"
                            End If

                            strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Outlier WHERE FK_CaseManagement_ID=" & IDUnik
                            Dim drCMO As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                            If drCMO IsNot Nothing Then
                                If Not IsDBNull(drCMO("Mean_Debit")) Then
                                    txt_MeanDebit.Value = CDbl(drCMO("Mean_Debit")).ToString("#,##0.00")
                                End If
                                If Not IsDBNull(drCMO("Mean_Credit")) Then
                                    txt_MeanCredit.Value = CDbl(drCMO("Mean_Credit")).ToString("#,##0.00")
                                End If
                                If Not IsDBNull(drCMO("Modus_Debit")) Then
                                    txt_ModusDebit.Value = CDbl(drCMO("Modus_Debit")).ToString("#,##0.00")
                                End If
                                If Not IsDBNull(drCMO("Modus_Credit")) Then
                                    txt_ModusCredit.Value = CDbl(drCMO("Modus_Credit")).ToString("#,##0.00")
                                End If
                            End If
                            '' Add 05-Jan-2023, Felix, untuk Alert Type selain itu
                        Case Else
                            Dim strSQLTypology As String = "SELECT * FROM OneFCC_CaseManagement_Typology WHERE FK_CaseManagement_ID = " & IDUnik
                            Dim drCMTypology As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQLTypology)
                            If drCMTypology Is Nothing Then
                                Throw New ApplicationException("Could not find Case Manageent Typology, Please  report this to admin.")
                            End If
                            If IsDBNull(drCMTypology("Is_Transaction")) OrElse IsDBNull(drCMTypology("PK_OneFCC_CaseManagement_Typology_ID")) Then
                                Throw New ApplicationException("Is Transaction Or ID OneFCC_CaseManagement_Typology is null, please report this to admin.")
                            End If
                            If drCMTypology("Is_Transaction") = True Then
                                checkbox_IsTransaction.Value = "True"
                                gp_CaseAlert_Typology_Transaction.Hidden = False
                            ElseIf drCMTypology("Is_Transaction") = False Then
                                checkbox_IsTransaction.Value = "False"
                                Dim strSQLActivity As String = " SELECT PK_OneFCC_CaseManagement_Typology_NonTransaction_ID, Date_Activity, Activity_Description, Account_NO "
                                strSQLActivity = strSQLActivity & " FROM OneFCC_CaseManagement_Typology_NonTransaction WHERE FK_OneFCC_CaseManagement_Typology_ID = " & drCMTypology("PK_OneFCC_CaseManagement_Typology_ID")
                                DataTabelActivity = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQLActivity, Nothing)
                                BindTableActivity()
                                panel_Activity.Hidden = False
                            End If
                            'txt_RuleBasic.Hidden = False
                            'If Not IsDBNull(drCM("FK_Rule_Basic_ID")) Then
                            '    strSQL = "SELECT TOP 1 Rule_Basic_Name from OneFcc_MS_Rule_Basic where PK_Rule_Basic_ID = " & drCM("FK_Rule_Basic_ID")
                            '    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                            '    If drTemp IsNot Nothing Then
                            '        txt_RuleBasic.Value = drCM("FK_Rule_Basic_ID") & " - " & drTemp("Rule_Basic_Name")
                            '    End If
                            'End If
                            '' End 05-Jan-2023
                    End Select
                End If
                If Not IsDBNull(drCM("Workflow_Step_Of")) Then
                    txt_Workflow_Step.Value = drCM("Workflow_Step_Of")
                End If
                If Not IsDBNull(drCM("PIC")) Then
                    txt_PIC.Value = drCM("PIC")
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadData_CaseAlert_Typology_Transaction(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            'If strsort = "" Then
            '    strsort = "CIF asc, Account_Name asc"
            'End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("Vw_Account", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)

            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " and ofcmtt.CIF_No_Lawan is null "
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Typology_Transaction.GetStore.DataSource = DataPaging
            gp_CaseAlert_Typology_Transaction.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadData_CaseAlert_Outlier_Transaction(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            'If strsort = "" Then
            '    strsort = "CIF asc, Account_Name asc"
            'End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("Vw_Account", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)


            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Outlier AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " and ofcmtt.CIF_No_Lawan is null "
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Outlier_Transaction.GetStore.DataSource = DataPaging
            gp_CaseAlert_Outlier_Transaction.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Back_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Save_Click()
        Try
            ValidateData()
            Dim strQuery As String = " update OneFCC_CaseManagement "
            strQuery &= " set FK_CaseStatus_ID = 0 "
            strQuery &= " , ApprovedBy = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery &= " , ApprovedDate = GETDATE() "
            strQuery &= " where PK_CaseManagement_ID = " & IDUnik
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@DataDate"
            param(0).Value = Now
            param(0).SqlDbType = SqlDbType.DateTime
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CaseManagement_AssignWorkflowByQuery", param)

            SaveAuditTrail(2)
            LblConfirmation.Text = "Data Approved."
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False
            ObjModule.ModuleLabel = ""
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Reject_Click()
        Try
            ValidateData()
            SaveAuditTrail(3)
            Dim strQuery As String = ""
            Select Case AlertType
                Case "Typology Risk"
                    strQuery &= " declare @PK_CaseManagement_typology as bigint "
                    strQuery &= " select @PK_CaseManagement_typology = PK_OneFCC_CaseManagement_Typology_ID from OneFCC_CaseManagement_Typology "
                    strQuery &= " where FK_CaseManagement_ID = " & IDUnik
                    strQuery &= " delete from OneFCC_CaseManagement "
                    strQuery &= " where PK_CaseManagement_ID = " & IDUnik
                    strQuery &= " delete from OneFCC_CaseManagement_Typology "
                    strQuery &= " where FK_CaseManagement_ID = " & IDUnik
                    strQuery &= " delete from OneFCC_CaseManagement_Typology_Transaction "
                    strQuery &= " where FK_OneFCC_CaseManagement_Typology_ID = @PK_CaseManagement_typology"
                Case "Financial Risk"
                    strQuery &= "declare @PK_CaseManagement_outlier as bigint "
                    strQuery &= " select @PK_CaseManagement_outlier = PK_OneFCC_CaseManagement_Outlier_ID from OneFCC_CaseManagement_Outlier "
                    strQuery &= " where FK_CaseManagement_ID = " & IDUnik
                    strQuery &= " delete from OneFCC_CaseManagement "
                    strQuery &= " where PK_CaseManagement_ID = " & IDUnik
                    strQuery &= " delete from OneFCC_CaseManagement_Outlier "
                    strQuery &= " where FK_CaseManagement_ID = " & IDUnik
                    strQuery &= " delete from OneFCC_CaseManagement_Outlier_Transaction "
                    strQuery &= " where FK_OneFCC_CaseManagement_Outlier_ID = @PK_CaseManagement_outlier"
                    '' Add 04-Jan-2023, Felix, agar support new Alert Type. Hapus Typology & Outlier untuk PK CM tsb
                Case Else
                    strQuery &= " declare @PK_CaseManagement_typology as bigint "
                    strQuery &= " select @PK_CaseManagement_typology = PK_OneFCC_CaseManagement_Typology_ID from OneFCC_CaseManagement_Typology "
                    strQuery &= " where FK_CaseManagement_ID = " & IDUnik
                    strQuery &= " delete from OneFCC_CaseManagement "
                    strQuery &= " where PK_CaseManagement_ID = " & IDUnik
                    strQuery &= " delete from OneFCC_CaseManagement_Typology "
                    strQuery &= " where FK_CaseManagement_ID = " & IDUnik
                    strQuery &= " delete from OneFCC_CaseManagement_Typology_Transaction "
                    strQuery &= " where FK_OneFCC_CaseManagement_Typology_ID = @PK_CaseManagement_typology "


                    strQuery &= " declare @PK_CaseManagement_outlier as bigint "
                    strQuery &= " select @PK_CaseManagement_outlier = PK_OneFCC_CaseManagement_Outlier_ID from OneFCC_CaseManagement_Outlier "
                    strQuery &= " where FK_CaseManagement_ID = " & IDUnik
                    strQuery &= " delete from OneFCC_CaseManagement "
                    strQuery &= " where PK_CaseManagement_ID = " & IDUnik
                    strQuery &= " delete from OneFCC_CaseManagement_Outlier "
                    strQuery &= " where FK_CaseManagement_ID = " & IDUnik
                    strQuery &= " delete from OneFCC_CaseManagement_Outlier_Transaction "
                    strQuery &= " where FK_OneFCC_CaseManagement_Outlier_ID = @PK_CaseManagement_outlier "
                    '' End 04-Jan-2023
            End Select
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            LblConfirmation.Text = "Data Rejected."
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ValidateData()
        Try
            If AlertType Is Nothing Then
                Throw New ApplicationException("Alert Type is Mandatory But Returned NULL Please Report this to admin.")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SaveAuditTrail(action As Integer)
        Try
            Dim paramAuditTrail(2) As SqlParameter
            paramAuditTrail(0) = New SqlParameter
            paramAuditTrail(0).ParameterName = "@PK_CaseManagement"
            paramAuditTrail(0).Value = IDUnik
            paramAuditTrail(0).SqlDbType = SqlDbType.BigInt

            paramAuditTrail(1) = New SqlParameter
            paramAuditTrail(1).ParameterName = "@ModuleLabel"
            paramAuditTrail(1).Value = ObjModule.ModuleLabel
            paramAuditTrail(1).SqlDbType = SqlDbType.VarChar

            paramAuditTrail(2) = New SqlParameter
            paramAuditTrail(2).ParameterName = "@Action"
            paramAuditTrail(2).Value = action
            paramAuditTrail(2).SqlDbType = SqlDbType.Int
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CaseManagement_Alert_AuditTrail", paramAuditTrail)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Function CheckIfStringExists(strPart As String, strFull As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim dt As DataTable = New DataTable

            Dim strsplit As String() = strFull.Split(","c)
            For Each item As String In strsplit
                If strPart = item Then
                    Return True
                End If
            Next

            Return bolResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        If buttonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub



#Region "10-Feb-2022 : Export Hit/Search Transaction to CSV/Excel"
    Protected Sub ExportAll_CaseAlert_Typology(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            'Get Data Transaction
            'Dim strQuery As String = "SELECT ofcmtt.Date_Transaction, ofcmtt.Account_NO, ofcmtt.Transmode_Code, ofcmtt.Transaction_Remark, ofcmtt.IDR_Amount,"
            'strQuery &= " ofcmtt.country_code, ofcmtt.country_code_lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            '29-Mar-2022 Adi
            Dim strQuery As String = "SELECT ofcmtt.Date_Transaction"
            strQuery &= " ,ofcmtt.Ref_Num"
            strQuery &= " ,ofcmtt.Transmode_Code"
            strQuery &= " ,ofcmtt.Transaction_Location"
            strQuery &= " ,ofcmtt.Debit_Credit"
            strQuery &= " ,ofcmtt.Currency"
            strQuery &= " ,ofcmtt.Original_Amount"
            strQuery &= " ,ofcmtt.IDR_Amount"
            strQuery &= " ,ofcmtt.Transaction_Remark"
            strQuery &= " ,ofcmtt.Account_NO"
            strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,ofcmtt.Account_No_Lawan"
            strQuery &= " ,ofcmtt.CIF_No_Lawan"
            strQuery &= " ,ofcmtt.WIC_No_Lawan"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No "
            strQuery &= " and ofcmtt.CIF_No_Lawan is null "
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            'Get Data CM
            If CustomerType.ToLower() = "customer" Then
                strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            ElseIf CustomerType.ToLower() = "wic" Then
                strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & IDUnik
            End If
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Typology_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CaseManagementTypologyTrx")

                    'Display Header of CM
                    If drCM IsNot Nothing Then
                        ws.Cells("A1").Value = "Case Management ID"
                        ws.Cells("B1").Value = drCM("PK_CaseManagement_ID").ToString

                        ws.Cells("A2").Value = "CIF"
                        ws.Cells("B2").Value = drCM("CIF_No")

                        ws.Cells("A3").Value = "Customer Name"
                        ws.Cells("B3").Value = drCM("Customer_Name")

                        ws.Cells("A4").Value = "Alert Type"
                        ws.Cells("B4").Value = drCM("Alert_Type")

                        ws.Cells("A5").Value = "Case Description"
                        ws.Cells("B5").Value = drCM("Case_Description")

                        ws.Cells("A6").Value = "Process Date"
                        If Not IsDBNull(drCM("ProcessDate")) Then
                            ws.Cells("B6").Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                        End If
                    End If

                        'objtbl.Columns("Date_Transaction").SetOrdinal(0)
                        'objtbl.Columns("Account_NO").SetOrdinal(1)
                        'objtbl.Columns("Transmode_Code").SetOrdinal(2)
                        'objtbl.Columns("Transaction_Remark").SetOrdinal(3)
                        'objtbl.Columns("IDR_Amount").SetOrdinal(4)
                        'objtbl.Columns("country_code").SetOrdinal(5)
                        'objtbl.Columns("country_code_lawan").SetOrdinal(6)

                        'objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                        'objtbl.Columns("Account_NO").ColumnName = "Account No"
                        'objtbl.Columns("Transmode_Code").ColumnName = "Transmode Code"
                        'objtbl.Columns("Transaction_Remark").ColumnName = "Transaction Remark"
                        'objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                        'objtbl.Columns("country_code").ColumnName = "Country Code"
                        'objtbl.Columns("country_code_lawan").ColumnName = "Opp. Country Code"

                        '29-Mar-2022 Adi : Perubahan skema informasi yang ditampilkan
                        objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    objtbl.Columns("Ref_Num").SetOrdinal(1)
                    objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    objtbl.Columns("Transaction_Location").SetOrdinal(3)
                    objtbl.Columns("Debit_Credit").SetOrdinal(4)
                    objtbl.Columns("Currency").SetOrdinal(5)
                    objtbl.Columns("Original_Amount").SetOrdinal(6)
                    objtbl.Columns("IDR_Amount").SetOrdinal(7)
                    objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                    objtbl.Columns("Account_NO").SetOrdinal(9)
                    objtbl.Columns("CounterPartyType").SetOrdinal(10)
                    objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                    objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                    objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                    objtbl.Columns("CounterPartyName").SetOrdinal(14)
                    objtbl.Columns("Country_Lawan").SetOrdinal(15)

                    objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
                    objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
                    objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
                    objtbl.Columns("Transaction_Location").ColumnName = "Location"
                    objtbl.Columns("Debit_Credit").ColumnName = "D/C"
                    objtbl.Columns("Currency").ColumnName = "CCY"
                    objtbl.Columns("Original_Amount").ColumnName = "Amount"
                    objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                    objtbl.Columns("Account_NO").ColumnName = "Account No."
                    objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                    objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                    objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                    objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                    objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                    objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"

                    ws.Cells("A8").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Management Typology Transaction (Case ID " & drCM("PK_CaseManagement_ID") & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportAll_CaseAlert_Outlier(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            ''Get Data Transaction
            'Dim strQuery As String = "SELECT ofcmot.Date_Transaction, ofcmot.Account_NO, ofcmot.Transmode_Code, ofcmot.Transaction_Remark, ofcmot.IDR_Amount,"
            'strQuery &= " ofcmot.country_code, ofcmot.country_code_lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmot"
            'strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmo ON ofcmot.FK_OneFCC_CaseManagement_Outlier_ID = ofcmo.PK_OneFCC_CaseManagement_Outlier_ID"
            'strQuery &= " WHERE ofcmo.FK_CaseManagement_ID = " & IDUnik

            '29-Mar-2022 Adi
            Dim strQuery As String = "SELECT ofcmtt.Date_Transaction"
            strQuery &= " ,ofcmtt.Ref_Num"
            strQuery &= " ,ofcmtt.Transmode_Code"
            strQuery &= " ,ofcmtt.Transaction_Location"
            strQuery &= " ,ofcmtt.Debit_Credit"
            strQuery &= " ,ofcmtt.Currency"
            strQuery &= " ,ofcmtt.Original_Amount"
            strQuery &= " ,ofcmtt.IDR_Amount"
            strQuery &= " ,ofcmtt.Transaction_Remark"
            strQuery &= " ,ofcmtt.Account_NO"
            strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,ofcmtt.Account_No_Lawan"
            strQuery &= " ,ofcmtt.CIF_No_Lawan"
            strQuery &= " ,ofcmtt.WIC_No_Lawan"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " and ofcmtt.CIF_No_Lawan is null "
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            'Get Data CM
            If CustomerType.ToLower() = "customer" Then
                strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            ElseIf CustomerType.ToLower() = "wic" Then
                strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & IDUnik
            End If
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Outlier_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CaseManagementOutlierTrx")

                    'Display Header of CM
                    If drCM IsNot Nothing Then
                        ws.Cells("A1").Value = "Case Management ID"
                        ws.Cells("B1").Value = drCM("PK_CaseManagement_ID").ToString

                        ws.Cells("A2").Value = "CIF"
                        ws.Cells("B2").Value = drCM("CIF_No")

                        ws.Cells("A3").Value = "Customer Name"
                        ws.Cells("B3").Value = drCM("Customer_Name")

                        ws.Cells("A4").Value = "Alert Type"
                        ws.Cells("B4").Value = drCM("Alert_Type")

                        ws.Cells("A5").Value = "Case Description"
                        ws.Cells("B5").Value = drCM("Case_Description")

                        ws.Cells("A6").Value = "Process Date"
                        If Not IsDBNull(drCM("ProcessDate")) Then
                            ws.Cells("B6").Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                        End If
                    End If

                    'Display Financial Statistics
                    Dim strSQL As String = ""
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=2"
                    Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                        ws.Cells("A8").Value = "Financial Statistics in past " & drParam("ParameterValue") & " month(s)"
                    End If

                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Outlier WHERE FK_CaseManagement_ID=" & IDUnik
                    Dim drCMO As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drCMO IsNot Nothing Then
                        ws.Cells("A9").Value = "Mean Debit"
                        If Not IsDBNull(drCMO("Mean_Debit")) Then
                            ws.Cells("B9").Value = CDbl(drCMO("Mean_Debit")).ToString("#,##0.00")
                        End If

                        ws.Cells("A10").Value = "Mean Credit"
                        If Not IsDBNull(drCMO("Mean_Credit")) Then
                            ws.Cells("B10").Value = CDbl(drCMO("Mean_Credit")).ToString("#,##0.00")
                        End If

                        ws.Cells("A11").Value = "Modus Debit"
                        If Not IsDBNull(drCMO("Modus_Debit")) Then
                            ws.Cells("B11").Value = CDbl(drCMO("Modus_Debit")).ToString("#,##0.00")
                        End If

                        ws.Cells("A12").Value = "Modus Credit"
                        If Not IsDBNull(drCMO("Modus_Credit")) Then
                            ws.Cells("B12").Value = CDbl(drCMO("Modus_Credit")).ToString("#,##0.00")
                        End If
                    End If

                        'objtbl.Columns("Date_Transaction").SetOrdinal(0)
                        'objtbl.Columns("Account_NO").SetOrdinal(1)
                        'objtbl.Columns("Transmode_Code").SetOrdinal(2)
                        'objtbl.Columns("Transaction_Remark").SetOrdinal(3)
                        'objtbl.Columns("IDR_Amount").SetOrdinal(4)
                        'objtbl.Columns("country_code").SetOrdinal(5)
                        'objtbl.Columns("country_code_lawan").SetOrdinal(6)

                        'objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                        'objtbl.Columns("Account_NO").ColumnName = "Account No"
                        'objtbl.Columns("Transmode_Code").ColumnName = "Transmode Code"
                        'objtbl.Columns("Transaction_Remark").ColumnName = "Transaction Remark"
                        'objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                        'objtbl.Columns("country_code").ColumnName = "Country Code"
                        'objtbl.Columns("country_code_lawan").ColumnName = "Opp. Country Code"

                        '29-Mar-2022 Adi : Perubahan skema informasi yang ditampilkan
                        objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    objtbl.Columns("Ref_Num").SetOrdinal(1)
                    objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    objtbl.Columns("Transaction_Location").SetOrdinal(3)
                    objtbl.Columns("Debit_Credit").SetOrdinal(4)
                    objtbl.Columns("Currency").SetOrdinal(5)
                    objtbl.Columns("Original_Amount").SetOrdinal(6)
                    objtbl.Columns("IDR_Amount").SetOrdinal(7)
                    objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                    objtbl.Columns("Account_NO").SetOrdinal(9)
                    objtbl.Columns("CounterPartyType").SetOrdinal(10)
                    objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                    objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                    objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                    objtbl.Columns("CounterPartyName").SetOrdinal(14)
                    objtbl.Columns("Country_Lawan").SetOrdinal(15)

                    objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
                    objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
                    objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
                    objtbl.Columns("Transaction_Location").ColumnName = "Location"
                    objtbl.Columns("Debit_Credit").ColumnName = "D/C"
                    objtbl.Columns("Currency").ColumnName = "CCY"
                    objtbl.Columns("Original_Amount").ColumnName = "Amount"
                    objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                    objtbl.Columns("Account_NO").ColumnName = "Account No."
                    objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                    objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                    objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                    objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                    objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                    objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"

                    ws.Cells("A14").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Management Outlier Transaction (Case ID " & drCM("PK_CaseManagement_ID") & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#End Region

#Region "20-Apr-2022 Adi : Alerts grouped by ProcessDate and CIF_No"
    Protected Sub ExportAll_CaseAlert_Transaction(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            'Get Data Transaction
            'Dim strQuery As String = "SELECT ofcmtt.Date_Transaction, ofcmtt.Account_NO, ofcmtt.Transmode_Code, ofcmtt.Transaction_Remark, ofcmtt.IDR_Amount,"
            'strQuery &= " ofcmtt.country_code, ofcmtt.country_code_lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            '29-Mar-2022 Adi
            Dim strQuery As String = "SELECT Case_Description"
            strQuery &= " ,Date_Transaction"
            strQuery &= " ,Ref_Num"
            strQuery &= " ,Transmode_Code"
            strQuery &= " ,Transaction_Location"
            strQuery &= " ,Debit_Credit"
            strQuery &= " ,Currency"
            strQuery &= " ,Original_Amount"
            strQuery &= " ,IDR_Amount"
            strQuery &= " ,Transaction_Remark"
            strQuery &= " ,Account_NO"
            strQuery &= " ,CounterPartyType"
            strQuery &= " ,Account_No_Lawan"
            strQuery &= " ,CIF_No_Lawan"
            strQuery &= " ,WIC_No_Lawan"
            strQuery &= " ,CounterPartyName"
            strQuery &= " ,Country_Lawan"
            strQuery &= " FROM vw_OneFCC_CaseManagement_Alert"
            strQuery &= " WHERE FK_CaseManagement_ID = " & IDUnik
            'strQuery &= " ORDER BY Account_No, Case_Description"

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            'Get Data CM
            strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Typology_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CaseManagementTypologyTrx")

                    'Display Header of CM
                    If drCM IsNot Nothing Then
                        ws.Cells("A1").Value = "Case Management ID"
                        ws.Cells("B1").Value = drCM("PK_CaseManagement_ID").ToString

                        ws.Cells("A2").Value = "CIF"
                        ws.Cells("B2").Value = drCM("CIF_No")

                        ws.Cells("A3").Value = "Customer Name"
                        ws.Cells("B3").Value = drCM("Customer_Name")

                        ws.Cells("A4").Value = "Process Date"
                        ws.Cells("B4").Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                    End If

                    objtbl.Columns("Case_Description").SetOrdinal(0)
                    objtbl.Columns("Date_Transaction").SetOrdinal(1)
                    objtbl.Columns("Ref_Num").SetOrdinal(2)
                    objtbl.Columns("Transmode_Code").SetOrdinal(3)
                    objtbl.Columns("Transaction_Location").SetOrdinal(4)
                    objtbl.Columns("Debit_Credit").SetOrdinal(5)
                    objtbl.Columns("Currency").SetOrdinal(6)
                    objtbl.Columns("Original_Amount").SetOrdinal(7)
                    objtbl.Columns("IDR_Amount").SetOrdinal(8)
                    objtbl.Columns("Transaction_Remark").SetOrdinal(9)
                    objtbl.Columns("Account_NO").SetOrdinal(10)
                    objtbl.Columns("CounterPartyType").SetOrdinal(11)
                    objtbl.Columns("Account_No_Lawan").SetOrdinal(12)
                    objtbl.Columns("CIF_No_Lawan").SetOrdinal(13)
                    objtbl.Columns("WIC_No_Lawan").SetOrdinal(14)
                    objtbl.Columns("CounterPartyName").SetOrdinal(15)
                    objtbl.Columns("Country_Lawan").SetOrdinal(16)

                    objtbl.Columns("Case_Description").ColumnName = "Case Description"
                    objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
                    objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
                    objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
                    objtbl.Columns("Transaction_Location").ColumnName = "Location"
                    objtbl.Columns("Debit_Credit").ColumnName = "D/C"
                    objtbl.Columns("Currency").ColumnName = "CCY"
                    objtbl.Columns("Original_Amount").ColumnName = "Amount"
                    objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                    objtbl.Columns("Account_NO").ColumnName = "Account No."
                    objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                    objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                    objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                    objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                    objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                    objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"

                    ws.Cells("A6").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Alert Transaction (Case ID " & drCM("PK_CaseManagement_ID") & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#End Region

#Region "14-Nov-2022 Penambahan WIC(Non-Customer)"

    Protected Sub LoadColumnDataTabelActivity()
        Try
            DataTabelActivity.Columns.Add(New DataColumn("PK_OneFCC_CaseManagement_Typology_NonTransaction_ID", GetType(Long)))
            DataTabelActivity.Columns.Add(New DataColumn("Date_Activity", GetType(Date)))
            DataTabelActivity.Columns.Add(New DataColumn("Activity_Description", GetType(String)))
            DataTabelActivity.Columns.Add(New DataColumn("Account_NO", GetType(String)))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Btn_OpenWindowCustomer_Click()
        Try
            If Not String.IsNullOrEmpty(Me.CIFNo) Then
                Dim strQuery As String = ""
                If CustomerType.ToLower() = "customer" Then
                    strQuery = "SELECT COUNT(1) AS JumCust FROM AML_CUSTOMER WHERE CIFNo = '" & Me.CIFNo & "'"
                ElseIf CustomerType.ToLower() = "wic" Then
                    strQuery = "SELECT COUNT(1) AS JumCust FROM goAML_Ref_WIC WHERE WIC_No = '" & Me.CIFNo & "'"
                End If
                If String.IsNullOrEmpty(strQuery) Then
                    Throw New ApplicationException("Customer Type Cannot be recognized, please report this to admin")
                End If
                Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                If drResult IsNot Nothing AndAlso Not IsDBNull(drResult("JumCust")) Then
                    If drResult("JumCust") > 0 Then
                        WindowPopUpDataCustomer.Hidden = False
                        LoadDataAMLCustomerByCIF(Me.CIFNo)
                    Else
                        If CustomerType.ToLower() = "customer" Then
                            Throw New ApplicationException("No AML Customer Found With CIF No = " & Me.CIFNo)
                        ElseIf CustomerType.ToLower() = "wic" Then
                            Throw New ApplicationException("No WIC Found With WIC No = " & Me.CIFNo)
                        End If
                    End If
                Else
                    If CustomerType.ToLower() = "customer" Then
                        Throw New ApplicationException("There is Something wrong when seaching Data AML Customer, Please Report this to Admin")
                    ElseIf CustomerType.ToLower() = "wic" Then
                        Throw New ApplicationException("There is Something wrong when seaching Data WIC, Please Report this to Admin")
                    End If
                End If
            Else
                If CustomerType.ToLower() = "customer" Then
                    Throw New ApplicationException("CIF is Empty")
                ElseIf CustomerType.ToLower() = "wic" Then
                    Throw New ApplicationException("WIC is Empty")
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDataAMLCustomerByCIF(CIF As String)
        Try
            'Get Customer Information
            txt_CustomerDetail_CIF.Value = Me.CIFNo
            txt_CustomerDetail_Name.Value = txt_Name.Value

            If CustomerType.ToLower() = "customer" Then
                txt_CustomerDetail_CIF.FieldLabel = "CIF No"
                txt_CustomerDetail_Name.FieldLabel = "Customer Name"
                txt_CustomerDetail_POB.FieldLabel = "Place of Birth"
                txt_CustomerDetail_DOB.FieldLabel = "Date of Birth"
                txt_CustomerDetail_Nationality.FieldLabel = "Nationality"
                txt_CustomerDetail_Occupation.FieldLabel = "Occupation"
                txt_CustomerDetail_Employer.FieldLabel = "Employer"
                txt_CustomerDetail_LegalForm.FieldLabel = "Legal Form"
                txt_CustomerDetail_Industry.FieldLabel = "Industry"
                txt_CustomerDetail_NPWP.FieldLabel = "Tax Number"
                txt_CustomerDetail_Income.FieldLabel = "Income Level"
                txt_CustomerDetail_PurposeOfFund.FieldLabel = "Purpose of Fund"
                txt_CustomerDetail_SourceOfFund.FieldLabel = "Source of Fund"
                gridIdentityInfo.Hidden = False
                Dim strDateFormat As String = NawaBLL.SystemParameterBLL.GetDateFormat()
                Dim strQuery As String = ""

                strQuery = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & Me.CIFNo & "'"
                Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                If drCustomer IsNot Nothing Then
                    If Not IsDBNull(drCustomer("FK_AML_Customer_Type_Code")) Then
                        txt_CustomerDetail_TypeCode.Value = drCustomer("FK_AML_Customer_Type_Code")
                    Else
                        txt_CustomerDetail_TypeCode.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("PLACEOFBIRTH")) Then
                        txt_CustomerDetail_POB.Value = drCustomer("PLACEOFBIRTH")
                    Else
                        txt_CustomerDetail_POB.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("DATEOFBIRTH")) Then
                        txt_CustomerDetail_DOB.Value = CDate(drCustomer("DATEOFBIRTH")).ToString(strDateFormat)
                    Else
                        txt_CustomerDetail_DOB.Value = ""
                    End If

                    'txt_CustomerDetail_Nationality.Value = drCustomer("FK_AML_CITIZENSHIP_CODE")
                    If Not IsDBNull(drCustomer("FK_AML_CITIZENSHIP_CODE")) Then
                        strQuery = "SELECT TOP 1 * FROM AML_COUNTRY WHERE FK_AML_COUNTRY_Code ='" & drCustomer("FK_AML_CITIZENSHIP_CODE") & "'"
                        Dim drCountry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundCountry As String = drCustomer("FK_AML_CITIZENSHIP_CODE")
                        If drCountry IsNot Nothing AndAlso Not IsDBNull(drCountry("AML_COUNTRY_Name")) Then
                            strTempCompoundCountry = strTempCompoundCountry & " - " & drCountry("AML_COUNTRY_Name")
                        End If
                        txt_CustomerDetail_Nationality.Value = strTempCompoundCountry
                    Else
                        txt_CustomerDetail_Nationality.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("FK_AML_PEKERJAAN_CODE")) Then
                        strQuery = "SELECT TOP 1 * FROM AML_PEKERJAAN WHERE FK_AML_PEKERJAAN_CODE='" & drCustomer("FK_AML_PEKERJAAN_CODE") & "'"
                        Dim drOccupation As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                        If drOccupation IsNot Nothing Then
                            txt_CustomerDetail_Occupation.Value = drCustomer("FK_AML_PEKERJAAN_CODE") & " - " & drOccupation("AML_PEKERJAAN_Name")
                        Else
                            txt_CustomerDetail_Occupation.Value = drCustomer("FK_AML_PEKERJAAN_CODE")
                        End If
                    Else
                        txt_CustomerDetail_Occupation.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("WORK_PLACE")) Then
                        txt_CustomerDetail_Employer.Value = drCustomer("WORK_PLACE")
                    Else
                        txt_CustomerDetail_Employer.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("FK_AML_BENTUK_BADAN_USAHA_CODE")) Then
                        txt_CustomerDetail_LegalForm.Value = drCustomer("FK_AML_BENTUK_BADAN_USAHA_CODE")
                    Else
                        txt_CustomerDetail_LegalForm.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("FK_AML_INDUSTRY_CODE")) Then
                        strQuery = "SELECT TOP 1 * FROM AML_INDUSTRY WHERE FK_AML_INDUSTRY_CODE='" & drCustomer("FK_AML_INDUSTRY_CODE") & "'"
                        Dim drIndustry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                        If drIndustry IsNot Nothing Then
                            txt_CustomerDetail_Industry.Value = drCustomer("FK_AML_INDUSTRY_CODE") & " - " & drIndustry("AML_INDUSTRY_Name")
                        Else
                            txt_CustomerDetail_Industry.Value = drCustomer("FK_AML_INDUSTRY_CODE")
                        End If
                    Else
                        txt_CustomerDetail_Industry.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("NPWP")) Then
                        txt_CustomerDetail_NPWP.Value = drCustomer("NPWP")
                    Else
                        txt_CustomerDetail_NPWP.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("INCOME_LEVEL")) Then
                        txt_CustomerDetail_Income.Value = CLng(drCustomer("INCOME_LEVEL")).ToString("#,##0.00")
                    Else
                        txt_CustomerDetail_Income.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("TUJUAN_DANA")) Then
                        txt_CustomerDetail_PurposeOfFund.Value = drCustomer("TUJUAN_DANA")
                    Else
                        txt_CustomerDetail_PurposeOfFund.Value = ""
                    End If

                    If Not IsDBNull(drCustomer("SOURCE_OF_FUND")) Then
                        txt_CustomerDetail_SourceOfFund.Value = drCustomer("SOURCE_OF_FUND")
                    Else
                        txt_CustomerDetail_SourceOfFund.Value = ""
                    End If
                End If

                'Binding Address
                BindCustomerAddress()

                'Binding Contact/Phone
                BindCustomerContact()

                'Binding Identity
                BindCustomerIdentity()

            ElseIf CustomerType.ToLower() = "wic" Then
                txt_CustomerDetail_CIF.FieldLabel = "WIC No"
                txt_CustomerDetail_Name.FieldLabel = "WIC Name"
                Dim strQuery As String = "SELECT TOP 1 * FROM goAML_Ref_WIC WHERE WIC_No = '" & CIF & "'"
                Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                If drResult IsNot Nothing Then
                    Dim strDateFormat As String = NawaBLL.SystemParameterBLL.GetDateFormat()

                    If Not IsDBNull(drResult("FK_Customer_Type_ID")) Then
                        strQuery = "SELECT TOP 1 * FROM goAML_Ref_Customer_Type WHERE PK_Customer_Type_ID = '" & drResult("FK_Customer_Type_ID") & "'"
                        Dim drCustomerType As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Dim strTempCompoundCustomerType As String = drResult("FK_Customer_Type_ID")
                        If drCustomerType IsNot Nothing AndAlso Not IsDBNull(drCustomerType("Description")) Then
                            strTempCompoundCustomerType = strTempCompoundCustomerType & " - " & drCustomerType("Description")
                        End If
                        txt_CustomerDetail_TypeCode.Value = strTempCompoundCustomerType

                        If drResult("FK_Customer_Type_ID") = 1 Then
                            txt_CustomerDetail_POB.FieldLabel = "Place of Birth"
                            txt_CustomerDetail_DOB.FieldLabel = "Date of Birth"
                            txt_CustomerDetail_Nationality.FieldLabel = "Nationality"
                            txt_CustomerDetail_Occupation.FieldLabel = "Occupation"
                            txt_CustomerDetail_Employer.FieldLabel = "Employer"
                            txt_CustomerDetail_LegalForm.FieldLabel = "Gender"
                            txt_CustomerDetail_Industry.FieldLabel = "NIK"
                            txt_CustomerDetail_NPWP.FieldLabel = "Tax Number"
                            txt_CustomerDetail_Income.FieldLabel = "Is PEP ?"
                            txt_CustomerDetail_PurposeOfFund.FieldLabel = "Email"
                            txt_CustomerDetail_SourceOfFund.FieldLabel = "Source of Fund"
                            gridIdentityInfo.Hidden = False

                            If Not IsDBNull(drResult("INDV_Birth_Place")) Then
                                txt_CustomerDetail_POB.Value = drResult("INDV_Birth_Place")
                            Else
                                txt_CustomerDetail_POB.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_BirthDate")) Then
                                txt_CustomerDetail_DOB.Value = CDate(drResult("INDV_BirthDate")).ToString(strDateFormat)
                            Else
                                txt_CustomerDetail_DOB.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Nationality1")) Then
                                strQuery = "SELECT TOP 1 * FROM goAML_Ref_Nama_Negara WHERE Kode ='" & drResult("INDV_Nationality1") & "'"
                                Dim drCountry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                Dim strTempCompoundCountry As String = drResult("INDV_Nationality1")
                                If drCountry IsNot Nothing AndAlso Not IsDBNull(drCountry("Keterangan")) Then
                                    strTempCompoundCountry = strTempCompoundCountry & " - " & drCountry("Keterangan")
                                End If
                                txt_CustomerDetail_Nationality.Value = strTempCompoundCountry
                            Else
                                txt_CustomerDetail_Nationality.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Occupation")) Then
                                txt_CustomerDetail_Occupation.Value = drResult("INDV_Occupation")
                            Else
                                txt_CustomerDetail_Occupation.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Employer_Name")) Then
                                txt_CustomerDetail_Employer.Value = drResult("INDV_Employer_Name")
                            Else
                                txt_CustomerDetail_Employer.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Gender")) Then
                                strQuery = "SELECT TOP 1 * FROM goAML_Ref_Jenis_Kelamin WHERE Kode ='" & drResult("INDV_Gender") & "'"
                                Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                Dim strTempCompoundGender As String = drResult("INDV_Gender")
                                If drGender IsNot Nothing AndAlso Not IsDBNull(drGender("Keterangan")) Then
                                    strTempCompoundGender = strTempCompoundGender & " - " & drGender("Keterangan")
                                End If
                                txt_CustomerDetail_LegalForm.Value = strTempCompoundGender
                            Else
                                txt_CustomerDetail_LegalForm.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_SSN")) Then
                                txt_CustomerDetail_Industry.Value = drResult("INDV_SSN")
                            Else
                                txt_CustomerDetail_Industry.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Tax_Number")) Then
                                txt_CustomerDetail_NPWP.Value = drResult("INDV_Tax_Number")
                            Else
                                txt_CustomerDetail_NPWP.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Tax_Reg_Number")) Then
                                If drResult("INDV_Tax_Reg_Number") = 1 Then
                                    txt_CustomerDetail_Income.Value = "True"
                                Else
                                    txt_CustomerDetail_Income.Value = "False"
                                End If
                            Else
                                txt_CustomerDetail_Income.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_Email")) Then
                                txt_CustomerDetail_PurposeOfFund.Value = drResult("INDV_Email")
                            Else
                                txt_CustomerDetail_PurposeOfFund.Value = ""
                            End If

                            If Not IsDBNull(drResult("INDV_SumberDana")) Then
                                txt_CustomerDetail_SourceOfFund.Value = drResult("INDV_SumberDana")
                            Else
                                txt_CustomerDetail_SourceOfFund.Value = ""
                            End If

                        ElseIf drResult("FK_Customer_Type_ID") = 2 Then
                            txt_CustomerDetail_POB.FieldLabel = "Province of Establishment"
                            txt_CustomerDetail_DOB.FieldLabel = "Date of Establishment"
                            txt_CustomerDetail_Nationality.FieldLabel = "Country of Establishment"
                            txt_CustomerDetail_Occupation.FieldLabel = "Is Closed ?"
                            txt_CustomerDetail_Employer.FieldLabel = "Closed Date"
                            txt_CustomerDetail_LegalForm.FieldLabel = "Legal Form"
                            txt_CustomerDetail_Industry.FieldLabel = "Industry"
                            txt_CustomerDetail_NPWP.FieldLabel = "Tax Number"
                            txt_CustomerDetail_Income.FieldLabel = "Incorporation Number"
                            txt_CustomerDetail_PurposeOfFund.FieldLabel = "Corporation Website"
                            txt_CustomerDetail_SourceOfFund.FieldLabel = "Corporation Email"
                            gridIdentityInfo.Hidden = True

                            If Not IsDBNull(drResult("Corp_Incorporation_State")) Then
                                txt_CustomerDetail_POB.Value = drResult("Corp_Incorporation_State")
                            Else
                                txt_CustomerDetail_POB.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Incorporation_Date")) Then
                                txt_CustomerDetail_DOB.Value = CDate(drResult("Corp_Incorporation_Date")).ToString(strDateFormat)
                            Else
                                txt_CustomerDetail_DOB.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Incorporation_Country_Code")) Then
                                strQuery = "SELECT TOP 1 * FROM goAML_Ref_Nama_Negara WHERE Kode ='" & drResult("Corp_Incorporation_Country_Code") & "'"
                                Dim drCountry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                Dim strTempCompoundCountry As String = drResult("Corp_Incorporation_Country_Code")
                                If drCountry IsNot Nothing AndAlso Not IsDBNull(drCountry("Keterangan")) Then
                                    strTempCompoundCountry = strTempCompoundCountry & " - " & drCountry("Keterangan")
                                End If
                                txt_CustomerDetail_Nationality.Value = strTempCompoundCountry
                            Else
                                txt_CustomerDetail_Nationality.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Business_Closed")) Then
                                If drResult("Corp_Business_Closed") = 1 Then
                                    txt_CustomerDetail_Occupation.Value = "True"
                                Else
                                    txt_CustomerDetail_Occupation.Value = "False"
                                End If
                            Else
                                txt_CustomerDetail_Occupation.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Date_Business_Closed")) Then
                                txt_CustomerDetail_Employer.Value = CDate(drResult("Corp_Date_Business_Closed")).ToString(strDateFormat)
                            Else
                                txt_CustomerDetail_Employer.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Incorporation_Legal_Form")) Then
                                strQuery = "SELECT TOP 1 * FROM goAML_Ref_Bentuk_Badan_Usaha WHERE Kode ='" & drResult("Corp_Incorporation_Legal_Form") & "'"
                                Dim drBadanUsaha As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                Dim strTempCompoundBadanUsaha As String = drResult("Corp_Incorporation_Legal_Form")
                                If drBadanUsaha IsNot Nothing AndAlso Not IsDBNull(drBadanUsaha("Keterangan")) Then
                                    strTempCompoundBadanUsaha = strTempCompoundBadanUsaha & " - " & drBadanUsaha("Keterangan")
                                End If
                                txt_CustomerDetail_LegalForm.Value = strTempCompoundBadanUsaha
                            Else
                                txt_CustomerDetail_LegalForm.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Business")) Then
                                txt_CustomerDetail_Industry.Value = drResult("Corp_Business")
                            Else
                                txt_CustomerDetail_Industry.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Tax_Number")) Then
                                txt_CustomerDetail_NPWP.Value = drResult("Corp_Tax_Number")
                            Else
                                txt_CustomerDetail_NPWP.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Incorporation_Number")) Then
                                txt_CustomerDetail_Income.Value = drResult("Corp_Incorporation_Number")
                            Else
                                txt_CustomerDetail_Income.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Url")) Then
                                txt_CustomerDetail_PurposeOfFund.Value = drResult("Corp_Url")
                            Else
                                txt_CustomerDetail_PurposeOfFund.Value = ""
                            End If

                            If Not IsDBNull(drResult("Corp_Email")) Then
                                txt_CustomerDetail_SourceOfFund.Value = drResult("Corp_Email")
                            Else
                                txt_CustomerDetail_SourceOfFund.Value = ""
                            End If
                        End If
                    Else
                        txt_CustomerDetail_TypeCode.Value = ""
                    End If


                    'Binding Address
                    BindWICAddress(drResult("PK_Customer_ID"))

                    'Binding Contact/Phone
                    BindWICContact(drResult("PK_Customer_ID"))

                    'Binding Identity
                    BindWICIdentity(drResult("PK_Customer_ID"))
                Else
                    Throw New ApplicationException("No AML Customer Data Found With CIF = " & CIF)
                End If
            Else
                Throw New ApplicationException("Customer Type are not identified, please report this to admin")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindCustomerAddress()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_COUNTRY"
            Dim dtCountry As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            strQuery = "SELECT * FROM AML_ADDRESS_TYPE"
            Dim dtAddressType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding Address
            strQuery = "SELECT * FROM AML_CUSTOMER_ADDRESS WHERE CIFNO='" & Me.CIFNo & "'"
            Dim dtAddress As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtAddress Is Nothing Then
                dtAddress = New DataTable
            End If
            dtAddress.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))
            dtAddress.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))

            For Each item In dtAddress.Rows
                item("AML_ADDRESS_TYPE_NAME") = item("FK_AML_ADDRESS_TYPE_CODE")
                If dtAddressType IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_ADDRESS_TYPE_CODE")) Then
                    Dim drCek = dtAddressType.Select("FK_AML_ADDRES_TYPE_CODE='" & item("FK_AML_ADDRESS_TYPE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("AML_ADDRESS_TYPE_NAME") = item("FK_AML_ADDRESS_TYPE_CODE") & " - " & drCek("ADDRESS_TYPE_NAME")
                    End If
                End If

                item("COUNTRY_NAME") = item("FK_AML_COUNTRY_CODE")
                If dtCountry IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_COUNTRY_CODE")) Then
                    Dim drCek = dtCountry.Select("FK_AML_COUNTRY_CODE='" & item("FK_AML_COUNTRY_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("COUNTRY_NAME") = item("FK_AML_COUNTRY_CODE") & " - " & drCek("AML_COUNTRY_Name")
                    End If
                End If
            Next

            gridAddressInfo.GetStore.DataSource = dtAddress
            gridAddressInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindCustomerContact()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_CONTACT_TYPE"
            Dim dtContactType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding Contact
            strQuery = "SELECT * FROM AML_CUSTOMER_CONTACT WHERE CIFNO='" & Me.CIFNo & "'"
            Dim dtContact As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtContact Is Nothing Then
                dtContact = New DataTable
            End If
            dtContact.Columns.Add(New DataColumn("AML_CONTACT_TYPE_NAME", GetType(String)))

            For Each item In dtContact.Rows
                item("AML_CONTACT_TYPE_NAME") = item("FK_AML_CONTACT_TYPE_CODE")
                If dtContactType IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_CONTACT_TYPE_CODE")) Then
                    Dim drCek = dtContactType.Select("FK_AML_CONTACT_TYPE_CODE='" & item("FK_AML_CONTACT_TYPE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("AML_CONTACT_TYPE_NAME") = item("FK_AML_CONTACT_TYPE_CODE") & " - " & drCek("CONTACT_TYPE_NAME")
                    End If
                End If
            Next

            gridPhoneInfo.GetStore.DataSource = dtContact
            gridPhoneInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindCustomerIdentity()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_IDENTITY_TYPE"
            Dim dtIDENTITYType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            strQuery = "SELECT * FROM AML_COUNTRY"
            Dim dtCountry As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding IDENTITY
            strQuery = "SELECT * FROM AML_CUSTOMER_IDENTITY WHERE CIFNO='" & Me.CIFNo & "'"
            Dim dtIDENTITY As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtIDENTITY Is Nothing Then
                dtIDENTITY = New DataTable
            End If
            dtIDENTITY.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
            dtIDENTITY.Columns.Add(New DataColumn("IDENTITY_TYPE_NAME", GetType(String)))

            For Each item In dtIDENTITY.Rows
                item("IDENTITY_TYPE_NAME") = item("FK_AML_IDENTITY_TYPE_CODE")
                If dtIDENTITYType IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_IDENTITY_TYPE_CODE")) Then
                    Dim drCek = dtIDENTITYType.Select("FK_AML_IDENTITY_TYPE_CODE='" & item("FK_AML_IDENTITY_TYPE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("IDENTITY_TYPE_NAME") = item("FK_AML_IDENTITY_TYPE_CODE") & " - " & drCek("IDENTITY_TYPE_NAME")
                    End If
                End If
                item("COUNTRY_NAME") = item("FK_AML_COUNTRYISSUE_CODE")
                If dtCountry IsNot Nothing AndAlso Not IsDBNull(item("FK_AML_COUNTRYISSUE_CODE")) Then
                    Dim drCek = dtCountry.Select("FK_AML_COUNTRY_CODE='" & item("FK_AML_COUNTRYISSUE_CODE") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("COUNTRY_NAME") = item("FK_AML_COUNTRYISSUE_CODE") & " - " & drCek("AML_COUNTRY_Name")
                    End If
                End If
            Next

            gridIdentityInfo.GetStore.DataSource = dtIDENTITY
            gridIdentityInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindWICAddress(PK_WIC As Long)
        Try
            Dim strQuery As String = ""

            'Binding Address
            strQuery = " SELECT "
            strQuery = strQuery & " addresstype.Keterangan AS AML_ADDRESS_TYPE_NAME "
            strQuery = strQuery & " , refaddress.Address AS CUSTOMER_ADDRESS "
            strQuery = strQuery & " , refaddress.Country_Code AS FK_AML_COUNTRY_CODE "
            strQuery = strQuery & " , namanegara.Keterangan AS COUNTRY_NAME "
            strQuery = strQuery & " , refaddress.Zip AS KODEPOS "
            strQuery = strQuery & " , refaddress.Town AS KECAMATAN "
            strQuery = strQuery & " , refaddress.City AS KOTAKABUPATEN "
            strQuery = strQuery & " FROM goaml_ref_address refaddress "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Kategori_Kontak addresstype "
            strQuery = strQuery & " ON refaddress.Address_Type = addresstype.Kode "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Nama_Negara namanegara "
            strQuery = strQuery & " ON refaddress.Country_Code = namanegara.Kode "
            strQuery = strQuery & " WHERE refaddress.FK_Ref_Detail_Of = 3 "
            strQuery = strQuery & " AND refaddress.FK_To_Table_ID = " & PK_WIC
            Dim dtAddress As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            'If dtAddress Is Nothing Then
            '    dtAddress = New DataTable
            'End If

            gridAddressInfo.GetStore.DataSource = dtAddress
            gridAddressInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindWICContact(PK_WIC As Long)
        Try
            Dim strQuery As String = ""

            'Binding Contact
            strQuery = " SELECT "
            strQuery = strQuery & " kontaktype.Keterangan AS AML_CONTACT_TYPE_NAME "
            strQuery = strQuery & " , alatkomunikasi.Keterangan AS FK_AML_COMMUNICATION_TYPE_CODE "
            strQuery = strQuery & " , refphone.tph_country_prefix AS AREA_CODE "
            strQuery = strQuery & " , refphone.tph_number AS CUSTOMER_CONTACT_NUMBER "
            strQuery = strQuery & " , refphone.tph_extension AS EXTENTION "
            strQuery = strQuery & " , refphone.comments AS NOTES "
            strQuery = strQuery & " FROM goAML_Ref_Phone refphone "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Kategori_Kontak kontaktype "
            strQuery = strQuery & " ON refphone.Tph_Contact_Type = kontaktype.Kode "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Jenis_Alat_Komunikasi alatkomunikasi "
            strQuery = strQuery & " ON refphone.Tph_Communication_Type = alatkomunikasi.Kode "
            strQuery = strQuery & " WHERE FK_Ref_Detail_Of = 3 "
            strQuery = strQuery & " AND FK_for_Table_ID = " & PK_WIC
            Dim dtContact As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            'If dtContact Is Nothing Then
            '    dtContact = New DataTable
            'End If

            gridPhoneInfo.GetStore.DataSource = dtContact
            gridPhoneInfo.GetStore.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindWICIdentity(PK_WIC As Long)
        Try
            Dim strQuery As String = ""

            'Binding IDENTITY
            strQuery = " SELECT "
            strQuery = strQuery & " identification.Number AS CUSTOMER_IDENTITY_NUMBER "
            strQuery = strQuery & " , identification.Type AS FK_AML_IDENTITY_TYPE_CODE "
            strQuery = strQuery & " , identification.Issue_Date AS DATE_OF_ISSUE "
            strQuery = strQuery & " , identification.Expiry_Date AS DATE_OF_EXPIRED "
            strQuery = strQuery & " , identification.Issued_By AS ISSUE_By "
            strQuery = strQuery & " , identification.Issued_Country AS fk_aml_countryissue_code "
            strQuery = strQuery & " , identification.Identification_Comment AS NOTES "
            strQuery = strQuery & " , identificationtype.Keterangan AS IDENTITY_TYPE_NAME "
            strQuery = strQuery & " , namanegara.Keterangan AS COUNTRY_NAME "
            strQuery = strQuery & " FROM goAML_Person_Identification identification "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Jenis_Dokumen_Identitas identificationtype "
            strQuery = strQuery & " ON identification.Type = identificationtype.Kode "
            strQuery = strQuery & " LEFT JOIN goAML_Ref_Nama_Negara namanegara "
            strQuery = strQuery & " ON identification.Issued_Country = namanegara.Kode "
            strQuery = strQuery & " WHERE FK_Person_Type = 8 "
            strQuery = strQuery & " AND FK_Person_ID = " & PK_WIC
            Dim dtIDENTITY As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            'If dtIDENTITY Is Nothing Then
            '    dtIDENTITY = New DataTable
            'End If

            gridIdentityInfo.GetStore.DataSource = dtIDENTITY
            gridIdentityInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub GridCommandActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            PK_Activity = CLng(ID)
            If e.ExtraParams(1).Value = "Delete" Then
                Dim strselect As String = "PK_OneFCC_CaseManagement_Typology_NonTransaction_ID = " & PK_Activity
                Dim dtrow As DataRow = DataTabelActivity.Select(strselect).FirstOrDefault()
                Dim indexrow As Integer = DataTabelActivity.Rows.IndexOf(dtrow)
                DataTabelActivity.Rows(indexrow).Delete()
                BindTableActivity()
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                ActionActivity = enumActionForm.Edit
                ClearWindowActivity()
                LoadDataActivity()
                WindowActivity.Hidden = False
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                ActionActivity = enumActionForm.Detail
                ClearWindowActivity()
                LoadDataActivity()
                WindowActivity.Hidden = False
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ClearWindowActivity()
        Try
            nDSDropDownField_AccountNo.Value = ""
            activity_Date.Value = ""
            activity_Desc.Value = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadDataActivity()
        Try
            Dim strDateFormat As String = NawaBLL.SystemParameterBLL.GetDateFormat()
            Dim strselect As String = "PK_OneFCC_CaseManagement_Typology_NonTransaction_ID = " & PK_Activity
            Dim dtrow As DataRow = DataTabelActivity.Select(strselect).FirstOrDefault()
            If Not IsDBNull(dtrow("Date_Activity")) Then
                activity_Date.Value = CDate(dtrow("Date_Activity")).ToString(strDateFormat)
            End If
            If Not IsDBNull(dtrow("Activity_Description")) Then
                activity_Desc.Value = dtrow("Activity_Description")
            End If
            If CustomerType.ToLower() = "customer" Then
                If Not IsDBNull(dtrow("Account_NO")) Then
                    nDSDropDownField_AccountNo.Value = dtrow("Account_NO")
                End If
            End If
            'Dim indexrow As Integer = DataTabelActivity.Rows.IndexOf(dtrow)
            'If Not IsDBNull(DataTabelActivity.Rows(indexrow)("Date_Activity")) Then
            '    activity_Date.SelectedDate = DataTabelActivity.Rows(indexrow)("Date_Activity")
            'End If
            'If Not IsDBNull(DataTabelActivity.Rows(indexrow)("Activity_Description")) Then
            '    activity_Desc.Value = DataTabelActivity.Rows(indexrow)("Activity_Description")
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindTableActivity()
        Try
            GridPanel_Activity.GetStore.DataSource = DataTabelActivity
            GridPanel_Activity.GetStore.DataBind()
            'GridPanel_Activity.Update()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Btn_CustomerDetail_Back_Click()
        Try
            WindowPopUpDataCustomer.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Activity_Back_Click()
        Try
            WindowActivity.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "05-Jan-2023 Felix : Tambah Revise"
    Protected Sub btn_Revise_Click()
        Try
            If String.IsNullOrWhiteSpace(txt_ReviseNotes.Value) Then
                Throw New ApplicationException("Action Revise requires Revise Note.")
            End If

            ValidateData()
            SaveAuditTrail(3)
            Dim strQuery As String = " update OneFCC_CaseManagement "
            strQuery &= " set FK_CaseStatus_ID = -2 " '' revised
            strQuery &= " , ApprovedBy = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery &= " , ApprovedDate = GETDATE() "
            strQuery &= " where PK_CaseManagement_ID = " & IDUnik
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            strQuery = ""
            strQuery &= " INSERT INTO OneFCC_CaseManagement_WorkflowHistory (  "
            strQuery &= " FK_CaseManagement_ID, "
            'strQuery &= " Workflow_Step, "
            'strQuery &= " FK_Proposed_Status_ID, "
            strQuery &= " Analysis_Result, "
            strQuery &= " CreatedBy, "
            strQuery &= " CreatedDate "
            'strQuery &= " Attachment, "
            'strQuery &= " AttachmentName   "
            strQuery &= " )   "
            strQuery &= " VALUES( "
            strQuery &= " " & IDUnik & ", "
            'strQuery &= " @Current_WorkflowStep, "
            'strQuery &= " @ProposedActionID, "
            strQuery &= " ' " & txt_ReviseNotes.Value & " ', "
            strQuery &= " '" & NawaBLL.Common.SessionCurrentUser.UserID & "', "
            strQuery &= " GETDATE()"
            'strQuery &= " @FileBinary,"
            'strQuery &= " case when @FileName <> ' ' then @FileName else null end  "
            strQuery &= " ) "
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Dim param(0) As SqlParameter
            'param(0) = New SqlParameter
            'param(0).ParameterName = "@DataDate"
            'param(0).Value = Now
            'param(0).SqlDbType = SqlDbType.DateTime
            'NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CaseManagement_AssignWorkflowByQuery", param)

            LblConfirmation.Text = "Data Revised."
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False
            ObjModule.ModuleLabel = ""
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_WorkflowHistory(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort &= item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT history.* FROM onefcc_casemanagement_workflowhistory history"
            strQuery &= " WHERE FK_CaseManagement_ID = " & IDUnik

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)

            If DataPaging.Rows.Count = 0 Then
                gp_WorkflowHistory.Hidden = True
            Else
                gp_WorkflowHistory.Hidden = False
            End If

            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_WorkflowHistory.GetStore.DataSource = DataPaging
            gp_WorkflowHistory.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class