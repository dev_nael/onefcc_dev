﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CaseManagementAlertByWIC_Detail.aspx.vb" Inherits="CaseManagementAlertByWIC_Detail" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .row-disabled-checkbox .x-grid-row-checker {
            display: none;
        }

        /*Edit 14-Nov-2022, diminta ubah warna background jg*/
        .row-disabled-color-row .x-grid-row-checker, .row-disabled-color-row .x-grid-cell , .row-disabled-color-row .x-grid-rowwrap-div {
            background-color: #BEBDB8 !important;
        }
        /* End 14-Nov-2022*/
    </style>

    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };

        var getRowClass = function (record, index, rowParams, store) {
            // ||
            //var s = record.get("PIC");
            //var s2 = record.get("AML_Risk");
            //record.get("Is_Closed") === "true" ||
            /*Edit 14-Nov-2022, diminta ubah warna background jg*/
            //if (  '1' == '1' ) {
            //    return "row-disabled-checkbox";
            //}

            if (  record.get('Is_Closed') == 'Yes' ||  record.get('PIC').indexOf( record.get('Current_Login') ) < 0  ) {
                //return "row-disabled-checkbox";
                return "row-disabled-color-row";
            }
            /* End 14-Nov-2022*/
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" Title="" Scrollable="Vertical" AutoScroll="true">
        <Items>
            <ext:Panel runat="server" ID="pnlWICInformation" Title="WIC Information" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="false" Layout="ColumnLayout" BodyPadding="10" Scrollable="Vertical" AutoScroll="true">
                <Items>
                    <ext:FieldSet runat="server" Border="true" Padding="10" AnchorHorizontal="100%" Layout="ColumnLayout" ColumnWidth="1">
                        <Items>
                            <ext:Container runat="server" ColumnWidth="0.5">
                                <Items>
                                    <ext:DisplayField runat="server" ID="txt_WIC_No" AnchorHorizontal="100%" FieldLabel="WIC" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_WIC_Name" AnchorHorizontal="100%" FieldLabel="WIC Name" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_DOB" AnchorHorizontal="100%" FieldLabel="Date of Birth" LabelWidth="150"></ext:DisplayField>
                                </Items>
                            </ext:Container>
                            <ext:Container runat="server" ColumnWidth="0.5">
                                <Items>
                                    <ext:DisplayField runat="server" ID="txt_Is_PEP" AnchorHorizontal="100%" FieldLabel="PEP" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_POB" AnchorHorizontal="100%" FieldLabel="Place of Birth" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_Gender" AnchorHorizontal="100%" FieldLabel="Gender" LabelWidth="150"></ext:DisplayField>

                                    <ext:Button ID="btn_WICDetail" runat="server" Icon="ApplicationViewDetail" Text="More Details">
                                        <DirectEvents>
                                            <Click OnEvent="btn_WICDetail_Click">
            
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:Panel>
            
            <ext:GridPanel ID="gp_Summary" runat="server" Title="Summary" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="100" Scrollable="Vertical" AutoScroll="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store_Summary" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="Model15" IDProperty="Test">
                                <Fields>
                                    <ext:ModelField Name="CASE_STATUS" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="COUNT" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ALERT_TYPE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="AGING" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column122" runat="server" DataIndex="CASE_STATUS" Text="Case Status" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column120" runat="server" DataIndex="COUNT" Text="Count" MinWidth="50"></ext:Column>
                        <ext:Column ID="Column121" runat="server" DataIndex="ALERT_TYPE" Text="Alert Type" MinWidth="250"></ext:Column>
                        <ext:Column ID="Column119" runat="server" DataIndex="AGING" Text="Aging" MinWidth="100"></ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar15" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            
            <ext:GridPanel ID="gp_CaseAlert_perWIC" runat="server" Title="Case Alert" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" >
                        <GetRowClass Fn="getRowClass" />
                    </ext:GridView>
                </View>
                <TopBar>
                    <ext:Toolbar runat="server">
                        <Items>
                            <%--<ext:ComboBox runat="server" ID="cboExport_CaseAlert_Typology" Editable="false" FieldLabel="Export " labelStyle="width:80px" AnchorHorizontal="50%" >
                                <Items>
                                    <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                                    <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                                </Items>
                            </ext:ComboBox>--%>
                            <%--<ext:Button runat="server" ID="BtnExport_CaseAlert_Typology" Text="Export Current Page" MarginSpec="0 0 0 10"  >
                               <DirectEvents>
                                    <Click OnEvent="Export_CaseAlert_Typology" IsUpload="true">
                                        <ExtraParams>
                                            <ext:Parameter Name="currentPage" Value="App.gp_CaseAlert_Typology_Transaction.getStore().currentPage" Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>--%>
                            <ext:Button runat="server" ID="BtnExportAll_CaseAlert_PerWIC" Text="Export to Excel" Icon="PageExcel">
                                <DirectEvents>
                                    <Click OnEvent="ExportAll_CaseAlert_PerWIC" IsUpload="true"/>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="store_CaseAlert_perWIC" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_PerWIC"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                        <%--<ext:Store ID="store2" runat="server" IsPagingStore="true" PageSize="10">--%>
                        <Model>
                            <ext:Model runat="server" ID="Model1" IDProperty="Case_ID">
                                <Fields>
                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="WIC_No" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="WIC_Name" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Alert_Type" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Case_Status" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Workflow_Step" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Created_Date" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Last_Update_Date" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="PIC" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Aging" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Aging_Escalation" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Last_Proposed_Action" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Count_Same_Case" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Is_Closed" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Current_Login" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="TRN_ACT" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Unique_CM_ID" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column91" runat="server" DataIndex="Case_ID" Text="Case ID" MinWidth="110"></ext:Column>
                        <ext:Column ID="Column39" runat="server" DataIndex="Unique_CM_ID" Text="Unique Case ID" MinWidth="200"></ext:Column>
                        <ext:Column ID="Column12" runat="server" DataIndex="PIC" Text="PIC" Width="200"></ext:Column>
                        <ext:Column ID="Column36" runat="server" DataIndex="Is_Closed" Text="Is Closed" MinWidth="30"></ext:Column>
                        <ext:Column ID="Column33" runat="server" DataIndex="TRN_ACT" Text="TRN / ACT" MinWidth="30"></ext:Column>
                        <ext:Column ID="Column73" runat="server" DataIndex="WIC_No" Text="WIC No" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column10" runat="server" DataIndex="WIC_Name" Text="WIC Name" Width="200"></ext:Column>
                        <ext:Column ID="Column77" runat="server" DataIndex="Alert_Type" Text="Alert Type" Width="100" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column78" runat="server" DataIndex="Case_Status" Text="Case Status" Width="100"></ext:Column>
                        <ext:Column ID="Column79" runat="server" DataIndex="Workflow_Step" Text="Step" Width="50"></ext:Column>
                        <ext:DateColumn ID="DateColumn5" runat="server" DataIndex="Created_Date" Text="Created Date" Width="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="Last_Update_Date" Text="Last Update Date" Width="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column81" runat="server" DataIndex="Aging" Text="Aging" MinWidth="50"></ext:Column>
                        <ext:Column ID="Column1" runat="server" DataIndex="Aging_Escalation" Text="Aging Escalation" MinWidth="50"></ext:Column>
                        <ext:Column ID="Column13" runat="server" DataIndex="Last_Proposed_Action" Text="Last Proposed Action" MinWidth="150"></ext:Column>
                        
                        <ext:Column ID="Column82" runat="server" DataIndex="Count_Same_Case" Text="Count Same Case" MinWidth="90" CellWrap="true"></ext:Column>
                        <ext:CommandColumn ID="cc_CaseAlert_PerWIC" runat="server" Text="Action" MinWidth="20">
                            <Commands>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="20">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                                </Commands>

                            <DirectEvents>
                                <Command OnEvent="gc_CaseAlert_PerWIC">
                                    <EventMask ShowMask="true"></EventMask>
                                    <%--<Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>--%>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.Case_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                 <SelectionModel>
                    <ext:CheckboxSelectionModel runat="server" Mode="Multi" CheckOnly="true">
                        <Listeners>
                            <%--<BeforeSelect Handler="return record.get('Is_Closed') !== 'true' ;" />--%>
                         <%--   <BeforeSelect Handler="if ((record.get('Is_Closed') == 'true') ||
                                             !(e.record.get('PIC').includes(e.record.get('Current_Login')) ) ) {
                                        
                                             return false;          
                                         }" />--%>
                               <BeforeSelect Handler="if ((record.get('Is_Closed') == 'Yes') || ( record.get('PIC').indexOf( record.get('Current_Login') ) < 0 )  ) {
                                        
                                             return false;          
                                         }" />
                        </Listeners>
                    </ext:CheckboxSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader1" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="true" />
                </BottomBar>
            </ext:GridPanel>

            <ext:panel runat="server" ID="fpReportHeader"  BodyPadding="10" ButtonAlign="Center"  Title="Analysis Result" Layout="AnchorLayout" Border="true">
                <Content>
                    <ext:TextArea ID="txt_InvestigationNotes" runat="server"  AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Investigation Notes" MaxLength="4000" EnforceMaxLength="true" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_ProposedAction" ValueField="PK_OneFCC_CaseManagement_ProposedAction_ID" DisplayField="Proposed_Action" StringField="PK_OneFCC_CaseManagement_ProposedAction_ID, Proposed_Action" StringTable="vw_ProposedAction_CMFollowUp" Label="Proposed Action" AnchorHorizontal="80%" AllowBlank="false" IsReadOnly="false"/>   
                    
                    <ext:FileUploadField ID="file_Attachment" runat="server" FieldLabel="Attachment" AnchorHorizontal="100%">
                    </ext:FileUploadField>
                </Content>
            </ext:panel>

        </Items>

        <Buttons>
            <ext:Button ID="btn_Save" runat="server" Icon="Disk" Text="Submit">
                <DirectEvents>
                    <Click OnEvent="btn_Save_Click">
                        <EventMask ShowMask="true" Msg="Submitting Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>


    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="Accept">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>


    


    <%-- ================== WINDOW OTHER CASE RELATED TO CUSTOMER ========================== --%>
    <ext:Window ID="window_OtherCase" Layout="AnchorLayout" Title="Other Case Related to Customer" runat="server" Modal="true" Hidden="true" Maximizable="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:Panel runat="server" ID="Panel2" Title="Case Alert Information" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="10">
                <Items>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_PK_CaseManagement_ID" AnchorHorizontal="100%" FieldLabel="Case Management ID" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_Alert_Type" AnchorHorizontal="100%" FieldLabel="Alert Type" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_Case_Description" AnchorHorizontal="100%" FieldLabel="Case Description" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_FK_Case_Status_ID" AnchorHorizontal="100%" FieldLabel="Case Status" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_CreatedDate" AnchorHorizontal="100%" FieldLabel="Created Date" LabelWidth="150"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_Workflow_Step" AnchorHorizontal="100%" FieldLabel="Workflow Step" LabelWidth="150" Hidden="true"></ext:DisplayField>
                    <ext:DisplayField runat="server" ID="txt_WindowOther_PIC" AnchorHorizontal="100%" FieldLabel="PIC" LabelWidth="150" Hidden="true"></ext:DisplayField>
                </Items>
            </ext:Panel>

           <%-- <ext:GridPanel ID="gp_CaseAlert_Transaction_OtherCase" runat="server" Title="Case Alerts" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="300" ColumnWidth="1">
                <Features>
                    <ext:GroupingSummary
                        ID="GroupingSummary1"
                        runat="server"
                        GroupHeaderTplString='Account No. {name} ({rows.length} Transaction{[values.rows.length > 1 ? "s" : ""]})'
                        HideGroupedHeader="false"
                        StartCollapsed="true" ShowSummaryRow="false"
                        >
                    </ext:GroupingSummary>
                </Features>
                <Store>
                    <ext:Store ID="store10" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Transaction_OtherCase"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static" GroupField="Alert_Group">
                        <Model>
                            <ext:Model runat="server" ID="Model17">
                                <Fields>
                                    <ext:ModelField Name="Alert_Group" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="Transaction_Remark" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn17" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:DateColumn ID="DateColumn13" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column33" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column34" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                        <ext:Column ID="Column135" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column136" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                        <ext:Column ID="Column137" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn15" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:NumberColumn ID="NumberColumn16" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:Column ID="Column138" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column139" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                        <ext:Column ID="Column140" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                        <ext:Column ID="Column141" runat="server" DataIndex="Account_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column142" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column143" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column144" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column145" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader7" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar17" runat="server" HideRefresh="true" />
                </BottomBar>
            </ext:GridPanel>--%>


            <ext:GridPanel ID="gp_CaseAlert_Typology_Transaction_OtherCase" runat="server" Title="Case Alert Typology Transaction" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store5" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Typology_Transaction_OtherCase"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                        <Model>
                            <ext:Model runat="server" ID="Model8" IDProperty="PK_OneFCC_CaseManagement_Typology_Transaction_ID">
                                <Fields>
                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="Transaction_Remark" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:DateColumn ID="DateColumn7" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column42" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column43" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                        <ext:Column ID="Column44" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column45" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                        <ext:Column ID="Column46" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn4" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:NumberColumn ID="NumberColumn10" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                        <ext:Column ID="Column104" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column105" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                        <ext:Column ID="Column106" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                        <ext:Column ID="Column107" runat="server" DataIndex="Account_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column108" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column109" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                        <ext:Column ID="Column110" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                        <ext:Column ID="Column113" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader4" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="true" />
                </BottomBar>
            </ext:GridPanel>
          
            <ext:Panel runat="server" ID="pnl_CaseAlert_Outlier_Transaction_OtherCase" Title="Case Alert Outlier Transaction" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="ColumnLayout" BodyPadding="10">
                <Items>
                    <ext:FieldSet runat="server" ID="fs_CaseAlert_Outlier_Transaction_OtherCase" ColumnWidth="1" Title="Financial Statistics" Padding="5" Layout="ColumnLayout">
                        <Items>
                            <ext:Container runat="server" ColumnWidth="0.5" PaddingSpec="0 10" Layout="AnchorLayout"> 
                                <Items>
                                    <ext:DisplayField runat="server" ID="txt_MeanDebit_OtherCase" AnchorHorizontal="100%" FieldLabel="Mean Debit (IDR)" LabelWidth="130"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_MeanCredit_OtherCase" AnchorHorizontal="100%" FieldLabel="Mean Credit (IDR)" LabelWidth="130"></ext:DisplayField>
                                </Items>
                            </ext:Container>
                            <ext:Container runat="server" ColumnWidth="0.5" PaddingSpec="0 10" Layout="AnchorLayout">
                                <Items>
                                    <ext:DisplayField runat="server" ID="txt_ModusDebit_OtherCase" AnchorHorizontal="100%" FieldLabel="Modus Debit (IDR)" LabelWidth="130"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="txt_ModusCredit_OtherCase" AnchorHorizontal="100%" FieldLabel="Modus Credit (IDR)" LabelWidth="130"></ext:DisplayField>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:FieldSet>

                    <ext:Container runat="server" ColumnWidth="1">
                        <Items>
                            <ext:GridPanel ID="gp_CaseAlert_Outlier_Transaction_OtherCase" runat="server" Title="" MarginSpec="0 0 10 0" Border="true" MinHeight="200">
                                <View>
                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                </View>
                                <Store>
                                    <ext:Store ID="store6" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_CaseAlert_Outlier_Transaction_OtherCase"
                                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                                        <Model>
                                            <ext:Model runat="server" ID="Model9" IDProperty="PK_OneFCC_CaseManagement_Outlier_Transaction_ID">
                                                <Fields>
                                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                                    <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                                    <ext:ModelField Name="Transaction_Remark" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Sorters></Sorters>
                                        <Proxy>
                                            <ext:PageProxy />
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server" Text="No"></ext:RowNumbererColumn>
                                        <ext:DateColumn ID="DateColumn8" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                                        <ext:Column ID="Column47" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column48" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                                        <ext:Column ID="Column49" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                                        <ext:Column ID="Column50" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                                        <ext:Column ID="Column51" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                                        <ext:NumberColumn ID="NumberColumn5" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                        <ext:NumberColumn ID="NumberColumn9" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                        <ext:Column ID="Column97" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                                        <ext:Column ID="Column98" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column99" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                                        <ext:Column ID="Column100" runat="server" DataIndex="Account_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column101" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column102" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                                        <ext:Column ID="Column103" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                                        <ext:Column ID="Column112" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <Plugins>
                                    <ext:FilterHeader ID="FilterHeader5" runat="server" Remote="true"></ext:FilterHeader>
                                </Plugins>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Panel>

            <ext:GridPanel ID="gp_WorkflowHistory_OtherCase" runat="server" Title="Workflow History" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store7" runat="server" IsPagingStore="true" PageSize="10" OnReadData="store_ReadData_WorkflowHistory_OtherCase"
                        RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                        <Model>
                            <ext:Model runat="server" ID="Model10" IDProperty="PK_CaseManagement_WorkflowHistory_ID">
                                <Fields>
                                    <ext:ModelField Name="Workflow_Step" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Proposed_Action" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Analysis_Result" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CreatedDate" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column52" runat="server" DataIndex="Workflow_Step" Text="Step" Width="50" Align="Center"></ext:Column>
                        <ext:Column ID="Column53" runat="server" DataIndex="CreatedBy" Text="Created By" Width="150"></ext:Column>
                        <ext:Column ID="Column54" runat="server" DataIndex="Proposed_Action" Text="Proposed Status" Width="150"></ext:Column>
                        <ext:Column ID="Column55" runat="server" DataIndex="Analysis_Result" Text="Analysis Result" Width="300" CellWrap="true"></ext:Column>
                        <ext:DateColumn ID="DateColumn9" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.9, height: size.height * 0.9});" />
            <Resize Handler="#{window_OtherCase}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_OtherCase_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_OtherCase_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%-- ================== END OF WINDOW OTHER CASE RELATED TO CUSTOMER ========================== --%>



    <%-- ================== WIC DETAIL WINDOW ========================== --%>
    <ext:Window ID="window_WICDetail" Layout="AnchorLayout" Title="WIC Detail Information" runat="server" Modal="true" Hidden="true" Maximizable="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
		   <ext:Panel ID="Panel3" runat="server" Layout="ColumnLayout" AnchorHorizontal="100%">
                <Content>
                    <ext:Container runat="server" ColumnWidth="1">
                        <Items>
                            <ext:DisplayField ID="txt_WICDetail_WICNo" runat="server" AnchorHorizontal="100%" FieldLabel="CIF" LabelWidth="120" />
                            <ext:DisplayField ID="txt_WICDetail_Name" runat="server" AnchorHorizontal="100%" FieldLabel="Customer Name" LabelWidth="120" />
                        </Items>
                    </ext:Container>
                    <ext:FieldSet runat="server" ColumnWidth="0.45" Border="true" Padding="10">
                        <Items>
                            <ext:DisplayField ID="txt_WICDetail_TypeCode" runat="server" AnchorHorizontal="100%" FieldLabel="Type Code" LabelWidth="100" />
                            <ext:DisplayField ID="txt_WICDetail_POB" runat="server" AnchorHorizontal="100%" FieldLabel="Place of Birth" LabelWidth="100" />
                            <ext:DisplayField ID="txt_WICDetail_DOB" runat="server" AnchorHorizontal="100%" FieldLabel="Date of Birth" LabelWidth="100" />
                            <ext:DisplayField ID="txt_WICDetail_Nationality" runat="server" AnchorHorizontal="100%" FieldLabel="Nationality" LabelWidth="100" />
                            <ext:DisplayField ID="txt_WICDetail_Occupation" runat="server" AnchorHorizontal="100%" FieldLabel="Occupation" LabelWidth="100" />
                            <ext:DisplayField ID="txt_WICDetail_Employer" runat="server" AnchorHorizontal="100%" FieldLabel="Employer" LabelWidth="100" />
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet runat="server" ColumnWidth="0.45" Border="true" Padding="10">
                        <Items>
                            <ext:DisplayField ID="txt_WICDetail_LegalForm" runat="server" AnchorHorizontal="100%" FieldLabel="Legal Form" LabelWidth="120" />
                            <ext:DisplayField ID="txt_WICDetail_Industry" runat="server" AnchorHorizontal="100%" FieldLabel="Industry" LabelWidth="120" />
                            <ext:DisplayField ID="txt_WICDetail_NPWP" runat="server" AnchorHorizontal="100%" FieldLabel="Tax Number" LabelWidth="120" />
                            <ext:DisplayField ID="txt_WICDetail_Income" runat="server" AnchorHorizontal="100%" FieldLabel="Income Level" LabelWidth="120" />
                            <%--<ext:DisplayField ID="txt_WICDetail_PurposeOfFund" runat="server" AnchorHorizontal="100%" FieldLabel="Purpose of Fund" LabelWidth="120" />--%>
                            <ext:DisplayField ID="txt_WICDetail_SourceOfFund" runat="server" AnchorHorizontal="100%" FieldLabel="Source of Fund" LabelWidth="120" />
                        </Items>
                    </ext:FieldSet>

                    <%-- WIC Address Information --%>
                    <ext:GridPanel ID="gridAddressInfo" runat="server" Title="Address" AutoScroll="true" Border="true" PaddingSpec="10 0 0 0" ColumnWidth="1">
                        <Store>
                            <ext:Store ID="Store_gridAddressInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model12" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="KATEGORI_KONTAK_KETERANGAN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country_Code" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column57" runat="server" DataIndex="KATEGORI_KONTAK_KETERANGAN" Text="Type" MinWidth="50"></ext:Column>
                                <ext:Column ID="Column58" runat="server" DataIndex="Address" Text="Address" MinWidth="250" CellWrap="true" Flex="1"></ext:Column>
                                <ext:Column ID="Column59" runat="server" DataIndex="Town" Text="Town" MinWidth="120" CellWrap="true" Flex="1"></ext:Column>
                                <ext:Column ID="Column60" runat="server" DataIndex="City" Text="City" MinWidth="120" CellWrap="true" Flex="1"></ext:Column>
                                <ext:Column ID="Column61" runat="server" DataIndex="Country_Code" Text="Country Code" MinWidth="100" Hidden="true"></ext:Column>
                                <ext:Column ID="Column62" runat="server" DataIndex="COUNTRY_NAME" Text="Country" MinWidth="100"></ext:Column>
                                <ext:Column ID="Column63" runat="server" DataIndex="Zip" Text="Postal Code" MinWidth="100"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of WIC Address Information --%>

                    <%-- WIC Phone Information --%>
                    <ext:GridPanel ID="gridPhoneInfo" runat="server" Title="Phone" AutoScroll="true" Border="true" PaddingSpec="10 0 0 0" ColumnWidth="1">
                        <Store>
                            <ext:Store ID="Store_PhoneInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model13" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="JENIS_ALAT_KOMUNIKASI_KETERANGAN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn14" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column69" runat="server" DataIndex="JENIS_ALAT_KOMUNIKASI_KETERANGAN" Text="Type" Width="100"></ext:Column>
                                <ext:Column ID="Column76" runat="server" DataIndex="Tph_Communication_Type" Text="Comm.Type" Width="100"></ext:Column>
                                <ext:Column ID="Column70" runat="server" DataIndex="tph_country_prefix" Text="Area Code" Width="100"></ext:Column>
                                <ext:Column ID="Column71" runat="server" DataIndex="tph_number" Text="Number" Width="250"></ext:Column>
                                <ext:Column ID="Column74" runat="server" DataIndex="tph_extension" Text="Ext." Width="100"></ext:Column>
                                <ext:Column ID="Column75" runat="server" DataIndex="comments" Text="Notes" MinWidth="100" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of WIC Phone Information --%>

                    <%-- WIC Identity Information --%>
                    <ext:GridPanel ID="gridIdentityInfo" runat="server" Title="Identification" AutoScroll="true" Border="true" PaddingSpec="10 0 0 0" ColumnWidth="1">
                        <Store>
                            <ext:Store ID="Store_gridIdentityInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model14" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="WIC_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Issue_Date" Type="Date" ></ext:ModelField>
                                            <ext:ModelField Name="Expiry_Date" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Issued_By" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Issued_Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identification_Comment" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDENTITY_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn13" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column64" runat="server" DataIndex="Type" Text="Type" MinWidth="50"></ext:Column>
                                <ext:Column ID="Column65" runat="server" DataIndex="Number" Text="Identification Number" MinWidth="150" CellWrap="true" Flex="1"></ext:Column>
                                <ext:DateColumn ID="DateColumn10" runat="server" DataIndex="Issue_Date" Text="Issue Date" Format="dd-MMM-yyyy" Width="120" CellWrap="true"></ext:DateColumn>
                                <ext:DateColumn ID="DateColumn11" runat="server" DataIndex="Expiry_Date" Text="Expired Date" Format="dd-MMM-yyyy" Width="120" CellWrap="true"></ext:DateColumn>
                                <ext:Column ID="Column66" runat="server" DataIndex="Issued_By" Text="Issue By" MinWidth="100" Hidden="true"></ext:Column>
                                <ext:Column ID="Column67" runat="server" DataIndex="COUNTRY_NAME" Text="Issued Country" Width="150"></ext:Column>
                                <ext:Column ID="Column68" runat="server" DataIndex="Identification_Comment" Text="Comments" MinWidth="100"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar13" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of WIC Identity Information --%>
                    <%-- WIC Account Information --%>
              <%--      <ext:GridPanel ID="gp_Account" runat="server" Title="Account Information" MarginSpec="10 0 0 0" Collapsible="true" Border="true" MinHeight="200" ColumnWidth="1">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="store_Account" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model43" IDProperty="ACCOUNT_NO">
                                        <Fields>
                                            <ext:ModelField Name="ACCOUNT_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ACCOUNT_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DATEOPENING" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="BRANCH_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ACCOUNT_TYPE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PRODUCT_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CURRENT_BALANCE" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="FK_AML_CURRENCY_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_AML_ACCOUNT_STATUS_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LIMITCREDITCARD" Type="Float"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column5" runat="server" DataIndex="ACCOUNT_TYPE" Text="Acc Type" Width="100" Locked="true"></ext:Column>
                                <ext:Column ID="Column412" runat="server" DataIndex="ACCOUNT_NO" Text="Account No." Width="150" Locked="true"></ext:Column>
                                <ext:Column ID="Column72" runat="server" DataIndex="ACCOUNT_NAME" Text="Name" Width="150" Locked="true"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="FK_AML_ACCOUNT_STATUS_CODE" Text="Status" Width="100"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="BRANCH_NAME" Text="Branch" Width="150"></ext:Column>
                                <ext:DateColumn ID="Column1" runat="server" DataIndex="DATEOPENING" Text="Opening Date" Width="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:NumberColumn ID="Column3" runat="server" DataIndex="CURRENT_BALANCE" Text="Current Balance" Width="150" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                <ext:NumberColumn ID="NumberColumn11" runat="server" DataIndex="LIMITCREDITCARD" Text="CC Limit" Width="150" Format="#,###.00" Align="Right"></ext:NumberColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>--%>
                    <%-- End of WIC Account Information --%>
                </Content>
            </ext:Panel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.9, height: size.height * 0.9});" />
            <Resize Handler="#{window_WICDetail}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_WICDetail_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_WICDetail_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%-- ================== END OF WIC DETAIL WINDOW ========================== --%>

     <%-- ================== CASE ALERT DETAIL WINDOW ========================== --%>  
    <ext:Window ID="window_CaseAlertDetail" Layout="AnchorLayout" Title="Case Alert Detail Information" runat="server" Modal="true" Hidden="true" Maximizable="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:Panel ID="Panel1" runat="server" Layout="ColumnLayout" AnchorHorizontal="100%">
                <Content>
                    <ext:Panel runat="server" ID="pnlGeneralInformation" Title="General Information" PaddingSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="10" ColumnWidth="1">
                        <Items>
                            <ext:DisplayField runat="server" ID="txt_PK_CaseManagement_ID" AnchorHorizontal="100%" FieldLabel="Case Management ID" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Unique_CM_ID" AnchorHorizontal="100%" FieldLabel="Unique CM ID" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Alert_Type" AnchorHorizontal="100%" FieldLabel="Alert Type" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Case_Description" AnchorHorizontal="100%" FieldLabel="Case Description" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_FK_Case_Status_ID" AnchorHorizontal="100%" FieldLabel="Case Status" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_ProcessDate" AnchorHorizontal="100%" FieldLabel="Alert Date" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_LastModifiedDate" AnchorHorizontal="100%" FieldLabel="Last Modified Date" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Proposed_Action" AnchorHorizontal="100%" FieldLabel="Last Proposed Action" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Proposed_By" AnchorHorizontal="100%" FieldLabel="Proposed By" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_Workflow_Step" AnchorHorizontal="100%" FieldLabel="Workflow Step" LabelWidth="150"></ext:DisplayField>
                            <ext:DisplayField runat="server" ID="txt_PIC" AnchorHorizontal="100%" FieldLabel="PIC" LabelWidth="150"></ext:DisplayField>
                        </Items>
                    </ext:Panel>                    

                    <ext:GridPanel ID="gp_CaseAlert_Typology_Transaction" runat="server" Title="Case Alert Typology Transaction" AutoScroll="true" Border="true" PaddingSpec="0 0 10 0" ColumnWidth="1">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="BtnExportAll_CaseAlert_Typology" Text="Export to Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="ExportAll_CaseAlert_Typology" IsUpload="true"/>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_CaseAlert_Typology_Transaction" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="Transaction_Remark" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters></Sorters>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:DateColumn ID="DateColumn4" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column8" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn1" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                <ext:NumberColumn ID="NumberColumn6" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column17" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="ACCOUNT_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column21" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column23" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:Panel runat="server" ID="pnl_CaseAlert_Outlier_Transaction" Title="Case Alert Outlier Transaction" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="ColumnLayout" BodyPadding="10" ColumnWidth="1">
                        <Items>
                            <ext:FieldSet runat="server" ID="fs_CaseAlert_Outlier_Transaction" ColumnWidth="1" Title="Financial Statistics" Padding="5" Layout="ColumnLayout">
                                <Items>
                                    <ext:Container runat="server" ColumnWidth="0.5" PaddingSpec="0 10">
                                        <Items>
                                            <ext:DisplayField runat="server" ID="txt_MeanDebit" AnchorHorizontal="100%" FieldLabel="Mean Debit (IDR)" LabelWidth="100"></ext:DisplayField>
                                            <ext:DisplayField runat="server" ID="txt_MeanCredit" AnchorHorizontal="100%" FieldLabel="Mean Credit (IDR)" LabelWidth="100"></ext:DisplayField>
                                        </Items>
                                    </ext:Container>
                                    <ext:Container runat="server" ColumnWidth="0.5" PaddingSpec="0 10">
                                        <Items>
                                            <ext:DisplayField runat="server" ID="txt_ModusDebit" AnchorHorizontal="100%" FieldLabel="Modus Debit (IDR)" LabelWidth="100"></ext:DisplayField>
                                            <ext:DisplayField runat="server" ID="txt_ModusCredit" AnchorHorizontal="100%" FieldLabel="Modus Credit (IDR)" LabelWidth="100"></ext:DisplayField>
                                        </Items>
                                    </ext:Container>
                                </Items>
                            </ext:FieldSet>

                            <ext:Container runat="server" ColumnWidth="1">
                                <Items>
                                    <ext:GridPanel ID="gp_CaseAlert_Outlier_Transaction" runat="server" Title="" MarginSpec="0 0 10 0" Border="true" MinHeight="200">
                                        <View>
                                            <ext:GridView runat="server" EnableTextSelection="true" />
                                        </View>
                                        <TopBar>
                                            <ext:Toolbar runat="server">
                                                <Items>
                                                    <ext:Button runat="server" ID="btnExportAll_CaseAlert_Outlier" Text="Export to Excel" Icon="PageExcel">
                                                        <DirectEvents>
                                                            <Click OnEvent="ExportAll_CaseAlert_Outlier" IsUpload="true"/>
                                                        </DirectEvents>
                                                    </ext:Button>
                                                </Items>
                                            </ext:Toolbar>
                                        </TopBar>
                                        <Store>
                                            <ext:Store ID="store3" runat="server" IsPagingStore="true" PageSize="10" RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                                                <Model>
                                                    <ext:Model runat="server" ID="Model5" IDProperty="PK_OneFCC_CaseManagement_Outlier_Transaction_ID">
                                                        <Fields>
                                                            <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Debit_Credit" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Currency" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Exchange_Rate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Original_Amount" Type="Float"></ext:ModelField>
                                                            <ext:ModelField Name="IDR_Amount" Type="Float"></ext:ModelField>
                                                            <ext:ModelField Name="Transaction_Remark" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ACCOUNT_No_Lawan" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CounterPartyType" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CIF_No_Lawan" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="WIC_No_Lawan" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CounterPartyName" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Country_Lawan" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                                <Sorters></Sorters>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No"></ext:RowNumbererColumn>
                                                <ext:DateColumn ID="DateColumn12" runat="server" DataIndex="Date_Transaction" Text="Trx Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                                                <ext:Column ID="Column28" runat="server" DataIndex="Ref_Num" Text="Ref Number" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column29" runat="server" DataIndex="Transmode_Code" Text="Trx Code" Width="80"></ext:Column>
                                                <ext:Column ID="Column30" runat="server" DataIndex="Transaction_Location" Text="Location" Width="100" CellWrap="true"></ext:Column>
                                                <ext:Column ID="Column31" runat="server" DataIndex="Debit_Credit" Text="D/C" Width="50"></ext:Column>
                                                <ext:Column ID="Column32" runat="server" DataIndex="Currency" Text="CCY" Width="50"></ext:Column>
                                                <ext:NumberColumn ID="NumberColumn3" runat="server" DataIndex="Original_Amount" Text="Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                                <ext:NumberColumn ID="NumberColumn7" runat="server" DataIndex="IDR_Amount" Text="IDR Amount" MinWidth="120" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                                <ext:Column ID="Column83" runat="server" DataIndex="Transaction_Remark" Text="Remark" Width="250" CellWrap="true"></ext:Column>
                                                <ext:Column ID="Column84" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column85" runat="server" DataIndex="CounterPartyType" Text="Tipe Pihak Lawan" Width="130"></ext:Column>
                                                <ext:Column ID="Column86" runat="server" DataIndex="Account_No_Lawan" Text="Account No. Lawan" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column87" runat="server" DataIndex="CIF_No_Lawan" Text="CIF Lawan" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column88" runat="server" DataIndex="WIC_No_Lawan" Text="WIC Lawan" MinWidth="150"></ext:Column>
                                                <ext:Column ID="Column89" runat="server" DataIndex="CounterPartyName" Text="Nama Pihak Lawan" MinWidth="200" CellWrap="true"></ext:Column>
                                                <ext:Column ID="Column111" runat="server" DataIndex="Country_Lawan" Text="Country Lawan" MinWidth="200"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <Plugins>
                                            <ext:FilterHeader ID="FilterHeader6" runat="server" Remote="true"></ext:FilterHeader>
                                        </Plugins>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Panel>

                    <%--Add 11-Nov-2022 Case Alert Typology Non Transaction--%>
                    <ext:GridPanel ID="gp_CaseAlert_Typology_NonTransaction" runat="server" Title="Case Alert Typology Non Transaction" AutoScroll="true" Border="true" PaddingSpec="0 0 10 0" ColumnWidth="1">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="BtnExportAll_CaseAlert_Typology_NonTrn" Text="Export to Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="ExportAll_CaseAlert_Typology_NonTrn" IsUpload="true"/>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_CaseAlert_Typology_NonTransaction" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <%--<ext:ModelField Name="Account_NO" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="Date_Activity" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Activity_Description" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters></Sorters>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="Column117" runat="server" DataIndex="Account_NO" Text="Account No." MinWidth="150"></ext:Column>--%>
                                <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="Date_Activity" Text="Act Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column34" runat="server" DataIndex="Activity_Description" Text="Description" MinWidth="150"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end 11-Nov-2022 Case Alert Typology Non Transaction--%>

                         <ext:Panel ID="pnlContent" runat="server"  MinHeight="200" MarginSpec="0 0 10 0" ColumnWidth="1" >

                <Loader Url="CaseManagementAlertByWIC_Questionnare.aspx" Mode="Frame" NoCache="true" runat="server" AutoLoad="false" >
                    <LoadMask ShowMask="true"></LoadMask>
                    <%--<Params>
                        <ext:Parameter Name="fileName" Mode="Raw" Value="0">
                        </ext:Parameter>
                    </Params>--%>
                </Loader>
                             
                                               </ext:Panel>

                    <ext:Panel ID="pnl_noquestion" runat="server" Title="Question" MarginSpec="0 0 10 0" Collapsible="true" Border="true"  ColumnWidth="1" Hidden="true">
                        <Content>
                            <ext:Label ID="lblnoquestion" runat="server" Align="center" >
                            </ext:Label>
                        </Content>
                    </ext:Panel>

                    <ext:GridPanel ID="gp_OtherCase" runat="server" Title="Same Case History" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200" ColumnWidth="1">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="store4" runat="server" IsPagingStore="true" PageSize="10" RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                                <Model>
                                    <ext:Model runat="server" ID="Model6" IDProperty="PK_CaseManagement_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CaseManagement_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Alert_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Case_Description" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CaseStatus" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ProcessDate" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters></Sorters>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column56" runat="server" DataIndex="PK_CaseManagement_ID" Text="Case ID" Width="70"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="Alert_Type" Text="Alert Type" Width="120"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="Case_Description" Text="Case Description" MinWidth="400"></ext:Column>
                                <ext:Column ID="Column35" runat="server" DataIndex="CaseStatus" Text="Status" Width="150"></ext:Column>
                                <ext:DateColumn ID="DateColumn3" runat="server" DataIndex="ProcessDate" Text="Alert Date" Width="110" Format="dd-MMM-yyyy"></ext:DateColumn>

                                <ext:CommandColumn ID="cc_OtherCase" runat="server" Text="Action" Width="75">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="75">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="gc_OtherCase">
                                            <EventMask ShowMask="true"></EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CaseManagement_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader ID="FilterHeader3" runat="server" Remote="true"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="true" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="gp_WorkflowHistory" runat="server" Title="Workflow History" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200" ColumnWidth="1">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="store2" runat="server" IsPagingStore="true" PageSize="10" RemoteFilter="true" RemoteSort="true" ClientIDMode="Static">
                                <Model>
                                    <ext:Model runat="server" ID="Model3" IDProperty="PK_CaseManagement_WorkflowHistory_ID">
                                        <Fields>
                                            <ext:ModelField Name="Workflow_Step" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Proposed_Action" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Analysis_Result" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AttachmentName" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters></Sorters>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column25" runat="server" DataIndex="Workflow_Step" Text="Step" MinWidth="100" Align="Center"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="CreatedBy" Text="Created By" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column27" runat="server" DataIndex="Proposed_Action" Text="Proposed Status" Width="150"></ext:Column>
                                <ext:Column ID="Column90" runat="server" DataIndex="Analysis_Result" Text="Analysis Result" Width="400" CellWrap="true"></ext:Column>
                                <ext:DateColumn ID="DateColumn_CreateDate" runat="server" DataIndex="CreatedDate" Text="Created Date" MinWidth="110" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column37" runat="server" DataIndex="AttachmentName" Text="Attachment Name" Width="400">
                            <Commands>
                                <ext:ImageCommand Icon="DiskDownload" CommandName="Download" ToolTip-Text="Download Attachment"></ext:ImageCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridcommandAttachment">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_CaseManagement_WorkflowHistory_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Content>
            </ext:Panel>
        
            
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.9, height: size.height * 0.9});" />
            <Resize Handler="#{window_CustomerDetail}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_CaseAlertDetail_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_CaseAlertDetail_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%-- ================== END OF CASE ALERT DETAIL WINDOW ========================== --%>

</asp:Content>

