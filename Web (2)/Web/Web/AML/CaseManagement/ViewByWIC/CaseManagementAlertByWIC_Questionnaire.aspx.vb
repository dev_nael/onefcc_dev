﻿Imports System.Data
Imports System.Globalization
Imports Elmah
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL
Imports OfficeOpenXml
Partial Class AML_CaseManagement_ViewByWIC_CaseManagementAlertByWIC_Questionnaire
    Inherits ParentPage
    Public Property IDUnik() As String
        Get
            Return Session("CaseManagementAlertByWIC_Questionnaire.IDUnik")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertByWIC_Questionnaire.IDUnik") = value
        End Set
    End Property

    Private Sub AML_CaseManagement_ViewByWIC_CaseManagementAlertByWIC_Questionnaire_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                IDUnik = Session("CaseIDCSAlertWIC")
                LoadToFormPanel(FormPanelQuestionnaire, Nothing)
                Session("CaseIDCSAlertWIC") = Nothing

            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub AML_CaseManagement_ViewByWIC_CaseManagementAlertByWIC_Questionnaire_Init(sender As Object, e As EventArgs) Handles Me.Init
        Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AML_CaseManagement_ViewByWIC_CaseManagementAlertByWIC_Questionnaire_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.view
    End Sub

    Protected Function ExtText(pn As Ext.Net.FormPanel, strLabel As String, strFieldName As String, bRequired As Boolean, intMaxSize As Integer, intgridpos As Integer, StrJawaban As String) As Ext.Net.TextField
        Dim objTextField As New Ext.Net.TextField
        objTextField.ID = strFieldName
        objTextField.ClientIDMode = Web.UI.ClientIDMode.Static
        objTextField.FieldLabel = strLabel

        objTextField.ID = strFieldName
        objTextField.Name = strFieldName
        objTextField.AllowBlank = Not bRequired
        objTextField.BlankText = strLabel & " is required."
        objTextField.MaxLength = intMaxSize
        objTextField.LabelWidth = 500
        objTextField.MinWidth = 900

        objTextField.AnchorHorizontal = "80%"
        objTextField.ReadOnly = True

        objTextField.Value = StrJawaban

        pn.Add(objTextField)
        Return objTextField
    End Function
    '------- LOAD COMPONENT
    Protected Function ExtPanel(pn As FormPanel, strName As String, strTitle As String, isRiskRating As Boolean) As Ext.Net.Panel
        Try
            Dim objPanel As New Ext.Net.Panel
            With objPanel
                .ID = strName
                .ClientIDMode = Web.UI.ClientIDMode.Static
                .Title = strTitle
                .MarginSpec = "0 0 10 0"
                If isRiskRating Then
                    .BodyStyle = "background-color: #33ff3a;"
                    .Border = False
                    .Layout = "ColumnLayout"
                Else
                    .Collapsible = True
                    .Border = True
                    .BodyPadding = 10
                End If
            End With

            pn.Add(objPanel)
            Return objPanel

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Protected Sub Load_Questionnaire(objpanelinput As Ext.Net.FormPanel, customerType As String)
        Try
            Dim strQuery As String = ""
            Dim intSequence As Integer


            'Load Question Group 
            'strQuery = "SELECT DISTINCT ofqg.PK_OneFCC_Question_Group_ID, ofqg.Question_Group_Name, ofqg.Sequence"
            'strQuery = strQuery & " FROM OneFCC_Questionnaire ofq"
            'strQuery = strQuery & " JOIN OneFCC_Question_Group AS ofqg ON ofq.FK_OneFCC_Question_Group_ID = ofqg.PK_OneFCC_Question_Group_ID"
            'strQuery = strQuery & " WHERE ofqg.FK_OneFCC_Question_Type_Code = 'EDD'"
            'strQuery = strQuery & " ORDER BY ofqg.Sequence"
            strQuery = "select * from OneFCC_CaseManagement_Questionaire_Answer where FK_CASEMANAGEMENT_ID = " & Session("CaseIDCSAlertWIC") & " and FK_OneFCC_Question_Parent_ID is null"
            Dim intSequenceParent As Integer = 0
            Dim dtQuestionGroup As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
            For Each row In dtQuestionGroup.Rows
                intSequenceParent = intSequenceParent + 1
                intSequence = intSequenceParent
                Dim isRequired As Boolean = False
                ExtText(objpanelinput, intSequence & ". " & row("QUESTION"), "ANSWER_" & row("PK_CM_QUESTIONAIRE_ANSWER_ID"), isRequired, 8000, intSequence, row("ANSWER"))
                Dim strQueryChild As String = "select * from OneFCC_CaseManagement_Questionaire_Answer where FK_CASEMANAGEMENT_ID = " & Session("CaseIDCSAlertWIC") & " and FK_OneFCC_Question_Parent_ID is not null"
                Dim dtQuestionChild As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQueryChild, Nothing)
                Dim intSequenceChild As Integer = 0
                For Each child In dtQuestionChild.Rows
                    If child("FK_OneFCC_Question_Parent_ID") = row("FK_OneFCC_Question_ID") Then
                        intSequenceChild = intSequenceChild + 1
                        Dim strPrefix As String = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & intSequenceChild
                        ExtText(objpanelinput, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_CM_QUESTIONAIRE_ANSWER_ID"), isRequired, 8000, intSequenceChild, child("ANSWER"))
                    End If
                Next
            Next
            If dtQuestionGroup.Rows.Count = 0 Then

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadToFormPanel(objpanelinput As Ext.Net.FormPanel, customerType As String)
        Try
            objpanelinput.Title = "Question"
            Load_Questionnaire(objpanelinput, Nothing)
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportQuestionnaireCaseAlert_PerWIC(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            Dim StrWICNO = Session("WICNoCSAlertWIC")
            Session("WICNoCSAlertWIC") = Nothing
            Dim strQuery As String = "select * from OneFCC_CaseManagement_Questionaire_Answer where FK_CASEMANAGEMENT_ID = " & IDUnik

            'Dim strQuery As String = "SELECT ofcmtt.Date_Transaction"
            'strQuery &= " ,ofcmtt.Ref_Num"
            'strQuery &= " ,ofcmtt.Transmode_Code"
            'strQuery &= " ,ofcmtt.Transaction_Location"
            'strQuery &= " ,ofcmtt.Debit_Credit"
            'strQuery &= " ,ofcmtt.Currency"
            'strQuery &= " ,ofcmtt.Original_Amount"
            'strQuery &= " ,ofcmtt.IDR_Amount"
            'strQuery &= " ,ofcmtt.Transaction_Remark"
            'strQuery &= " ,ofcmtt.Account_NO"
            'strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            'strQuery &= " ,ofcmtt.Account_No_Lawan"
            'strQuery &= " ,ofcmtt.CIF_No_Lawan"
            'strQuery &= " ,ofcmtt.WIC_No_Lawan"
            'strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            'strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            'strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            'strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            'strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            'strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            'strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            'strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If
            Dim strSQL = "SELECT TOP 1 * FROM goaml_ref_wic WHERE WIC_No='" & StrWICNO & "'"
            Dim drWIC As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            Dim StrWIC As String = ""
            Dim StrWICName As String = ""
            Dim StrWICDOB As String = ""
            Dim StrWICPOB As String = ""
            Dim StrWICGender As String = ""
            Dim StrWICPEP As String = ""

            If drWIC IsNot Nothing Then

                StrWIC = StrWICNO


                Dim strQuery3 As String = "SELECT case when FK_Customer_Type_ID = 1 then INDV_Last_Name when FK_Customer_Type_ID = 2 then Corp_Name end as WIC_Name FROM goaml_ref_wic WHERE WIC_No ='" & StrWICNO & "'"
                Dim drWICName As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery3)
                If drWICName IsNot Nothing Then
                    If Not IsDBNull(drWICName("WIC_Name")) Then
                        StrWICName = drWICName("WIC_Name")
                    End If
                End If
                If Not IsDBNull(drWIC("INDV_BirthDate")) Then
                    StrWICDOB = CDate(drWIC("INDV_BirthDate")).ToString("dd-MMM-yyyy")
                ElseIf Not IsDBNull(drWIC("Corp_Incorporation_Date")) Then
                    StrWICDOB = CDate(drWIC("Corp_Incorporation_Date")).ToString("dd-MMM-yyyy")
                End If

                If Not IsDBNull(drWIC("INDV_Birth_Place")) Then
                    StrWICPOB = drWIC("INDV_Birth_Place")
                Else
                    StrWICPOB = ""
                End If


                If Not IsDBNull(drWIC("INDV_Gender")) Then
                    strSQL = "SELECT TOP 1 * FROM goAML_Ref_Jenis_Kelamin WHERE Kode='" & drWIC("INDV_Gender") & "'"
                    Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drGender IsNot Nothing Then
                        StrWICGender = drGender("Kode") & " - " & drGender("Keterangan")
                    Else
                        StrWICGender = drWIC("INDV_Gender")
                    End If

                Else
                    StrWICGender = ""
                End If

                If Not IsDBNull(drWIC("Corp_Tax_Registeration_Number")) Then
                    If drWIC("Corp_Tax_Registeration_Number") = 0 Then
                        StrWICPEP = "No"
                    Else
                        StrWICPEP = "Yes"
                    End If
                Else
                    StrWICPEP = "N/A"
                End If
            End If

            'Get Data CM
            'strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            'Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))

            Dim strQuery2 As String = " select * from OneFCC_CaseManagement_Questionaire_Answer where FK_CASEMANAGEMENT_ID = " & IDUnik


            Dim dtTypologyTransaction As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2)
            If dtTypologyTransaction Is Nothing Then
                dtTypologyTransaction = New DataTable
            End If

            GridQuestionnaire.GetStore.DataSource = dtTypologyTransaction
            GridQuestionnaire.GetStore.DataBind()
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In GridQuestionnaire.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("QuestionnairePerCaseID")

                    'Display Header of CM

                    ws.Cells("A1").Value = "WIC No"
                    ws.Cells("B1").Value = StrWIC

                    ws.Cells("A3").Value = "Customer Name"
                    ws.Cells("B3").Value = StrWICName

                    ws.Cells("A4").Value = "Date of Birth"
                    ws.Cells("B4").Value = StrWICDOB

                    ws.Cells("A5").Value = "Place of Birth"
                    ws.Cells("B5").Value = StrWICPOB

                    ws.Cells("A6").Value = "Gender"
                    ws.Cells("B6").Value = StrWICGender

                    ws.Cells("A7").Value = "PEP"
                    ws.Cells("B7").Value = StrWICPEP

                    ws.Cells("A2").Value = "CaseID"
                    ws.Cells("B2").Value = IDUnik


                    'objtbl.Columns("Case_ID").SetOrdinal(0)
                    'objtbl.Columns("WIC_No").SetOrdinal(1)
                    'objtbl.Columns("WIC_Name").SetOrdinal(2)
                    'objtbl.Columns("Alert_Type").SetOrdinal(3)
                    'objtbl.Columns("Case_Status").SetOrdinal(4)
                    'objtbl.Columns("Workflow_Step").SetOrdinal(5)
                    'objtbl.Columns("Created_Date").SetOrdinal(6)
                    'objtbl.Columns("Last_Update_Date").SetOrdinal(7)
                    'objtbl.Columns("PIC").SetOrdinal(8)
                    'objtbl.Columns("Aging").SetOrdinal(9)
                    'objtbl.Columns("Last_Proposed_Action").SetOrdinal(10)
                    'objtbl.Columns("Count_Same_Case").SetOrdinal(11)
                    'objtbl.Columns("Is_Closed").SetOrdinal(12)

                    'objtbl.Columns("Case_ID").ColumnName = "Case ID"
                    'objtbl.Columns("WIC_No").ColumnName = "WIC No"
                    'objtbl.Columns("WIC_Name").ColumnName = "Name"
                    'objtbl.Columns("Alert_Type").ColumnName = "Alert Type"
                    'objtbl.Columns("Case_Status").ColumnName = "Status"
                    'objtbl.Columns("Workflow_Step").ColumnName = "Step"
                    'objtbl.Columns("Created_Date").ColumnName = "Create Date"
                    'objtbl.Columns("Last_Update_Date").ColumnName = "Last Update Date"
                    'objtbl.Columns("PIC").ColumnName = "PIC"
                    'objtbl.Columns("Aging").ColumnName = "Aging"
                    'objtbl.Columns("Last_Proposed_Action").ColumnName = "Last Proposed Action"
                    'objtbl.Columns("Count_Same_Case").ColumnName = "Count Same Case"
                    'objtbl.Columns("Is_Closed").ColumnName = "Is Closed"

                    ws.Cells("A11").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Management Questionnaire Per CaseID (CaseID " & IDUnik & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

End Class
