﻿Imports Ext
Imports Elmah
Imports System.Data
Imports CasemanagementBLL
Imports CasemanagementDAL
Imports System.Data.SqlClient
Imports OfficeOpenXml
Imports System.IO


Partial Class CaseManagementAlertByWIC_Detail
    Inherits ParentPage

    Const PATH_TEMP_DIR As String = "~\temp\"

    Public Property IDModule() As String
        Get
            Return Session("CaseManagementAlertByWIC_Detail.IDModule")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertByWIC_Detail.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As String
        Get
            Return Session("CaseManagementAlertByWIC_Detail.IDUnik")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertByWIC_Detail.IDUnik") = value
        End Set
    End Property

    Public Property WICNo() As String
        Get
            Return Session("CaseManagementAlertByWIC_Detail.WICNo")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertByWIC_Detail.WICNo") = value
        End Set
    End Property

    Public Property Workflow_Step() As Integer
        Get
            Return Session("CaseManagementAlertByWIC_Detail.Workflow_Step")
        End Get
        Set(ByVal value As Integer)
            Session("CaseManagementAlertByWIC_Detail.Workflow_Step") = value
        End Set
    End Property

    'Public Property Workflow_Step_Total() As Integer
    '    Get
    '        Return Session("CaseManagementAlertByWIC_Detail.Workflow_Step_Total")
    '    End Get
    '    Set(ByVal value As Integer)
    '        Session("CaseManagementAlertByWIC_Detail.Workflow_Step_Total") = value
    '    End Set
    'End Property
    Public Property Workflow_Step_Total() As String
        Get
            Return Session("CaseManagementAlertByWIC_Detail.Workflow_Step_Total")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertByWIC_Detail.Workflow_Step_Total") = value
        End Set
    End Property


    Public Property Workflow_ID() As Integer
        Get
            Return Session("CaseManagementAlertByWIC_Detail.Workflow_ID")
        End Get
        Set(ByVal value As Integer)
            Session("CaseManagementAlertByWIC_Detail.Workflow_ID") = value
        End Set
    End Property

    Public Property CaseID_OtherCase() As Long
        Get
            If Session("CaseManagementAlertByWIC_Detail.CaseID_OtherCase") Is Nothing Then
                Session("CaseManagementAlertByWIC_Detail.CaseID_OtherCase") = 0
            End If
            Return Session("CaseManagementAlertByWIC_Detail.CaseID_OtherCase")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertByWIC_Detail.CaseID_OtherCase") = value
        End Set
    End Property

    Public Property CaseID_WindowCaseAlertDetail() As Long
        Get
            If Session("CaseManagementAlertByWIC_Detail.CaseID_WindowCaseAlertDetail") Is Nothing Then
                Session("CaseManagementAlertByWIC_Detail.CaseID_WindowCaseAlertDetail") = 0
            End If
            Return Session("CaseManagementAlertByWIC_Detail.CaseID_WindowCaseAlertDetail")
        End Get
        Set(ByVal value As Long)
            Session("CaseManagementAlertByWIC_Detail.CaseID_WindowCaseAlertDetail") = value
        End Set
    End Property

    'Public Property RFI_ID() As Long
    '    Get
    '        Return Session("CaseManagementAlertByWIC_Detail.RFI_ID")
    '    End Get
    '    Set(ByVal value As Long)
    '        Session("CaseManagementAlertByWIC_Detail.RFI_ID") = value
    '    End Set
    'End Property

    Public Property Filetodownload() As String
        Get
            Return Session("CaseManagementAlertByWIC_Detail.Filetodownload")
        End Get
        Set(ByVal value As String)
            Session("CaseManagementAlertByWIC_Detail.Filetodownload") = value
        End Set
    End Property

    Private Sub CaseManagementAlertByWIC_Detail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Me.WICNo = IDUnik
            End If

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As New Integer
            If IDModule IsNot Nothing Then
                intModuleID = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = ObjModule.ModuleLabel & " - Detail"

                'Load Data
                LoadDataCaseAlertPerWIC()
                LoadSummary()

                Dim intmaxfilesize As Double = NawaBLL.SystemParameterBLL.GetMaxFileSize
                Dim strmaxfilesize As String = (intmaxfilesize / 1048576) & " MB"

                SetCommandColumnLocation()

                txt_WindowOther_Alert_Type.Hidden = True
                txt_WindowOther_Case_Description.Hidden = True

            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub



    Protected Sub Load_AnswerCustomerType(objpanelinput As Ext.Net.FormPanel, customerType As String)
        'Dim Customer_Type_ID As String = customerType
        'Dim Customer_Segment_ID As String = objOneFCC_EDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Segment_Code
        'Dim EDD_Type_ID As String = objOneFCC_EDD_CLASS.objEDD.FK_OneFCC_EDD_Type_Code

        'Load Questionnaire
        Dim strQuery As String
        strQuery = " select OneFCC_CaseManagement_Questionaire_Answer where FK_CASEMANAGEMENT_ID =  " & Session("CaseIDCSAlertWIC")

        Dim dtQuestion As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
        For Each item In dtQuestion.Rows
            Dim intQuestionID As Integer = item("SEQUENCE")

            Dim strAnswer As String = ""
            If Not IsDBNull(item("Answer")) Then
                strAnswer = item("Answer")
            End If

            Dim objfield As TextField = objpanelinput.FindControl("ANSWER_" & intQuestionID)
            If Not objfield Is Nothing Then
                objfield.Value = strAnswer
            End If

        Next
    End Sub

    Protected Sub LoadDataCaseAlertPerWIC()
        Try
            Dim strSQL As String

            If Not IsDBNull(Me.WICNo) Then
                txt_WIC_No.Value = Me.WICNo
            End If

            Dim strQuery As String = "SELECT case when FK_Customer_Type_ID = 1 then INDV_Last_Name when FK_Customer_Type_ID = 2 then Corp_Name end as WIC_Name FROM goaml_ref_wic WHERE WIC_No ='" & Me.WICNo & "'"
            Dim drWICName As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If drWICName IsNot Nothing Then
                If Not IsDBNull(drWICName("WIC_Name")) Then
                    txt_WIC_Name.Value = drWICName("WIC_Name")
                End If
            End If

            If Not IsDBNull(Me.WICNo) Then
                strSQL = "SELECT TOP 1 * FROM goaml_ref_wic WHERE WIC_No='" & Me.WICNo & "'"
                Dim drWIC As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                If drWIC IsNot Nothing Then
                    If Not IsDBNull(drWIC("INDV_BirthDate")) Then
                        txt_DOB.Value = CDate(drWIC("INDV_BirthDate")).ToString("dd-MMM-yyyy")
                    ElseIf Not IsDBNull(drWIC("Corp_Incorporation_Date")) Then
                        txt_DOB.Value = CDate(drWIC("Corp_Incorporation_Date")).ToString("dd-MMM-yyyy")
                    End If

                    If Not IsDBNull(drWIC("INDV_Birth_Place")) Then
                        txt_POB.Value = drWIC("INDV_Birth_Place")
                    Else
                        txt_POB.Value = ""
                        txt_POB.Hidden = True
                    End If


                    If Not IsDBNull(drWIC("INDV_Gender")) Then
                        strSQL = "SELECT TOP 1 * FROM goAML_Ref_Jenis_Kelamin WHERE Kode='" & drWIC("INDV_Gender") & "'"
                        Dim drGender As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                        If drGender IsNot Nothing Then
                            txt_Gender.Value = drGender("Kode") & " - " & drGender("Keterangan")
                        Else
                            txt_Gender.Value = drWIC("INDV_Gender")
                        End If
                        txt_Gender.Hidden = False
                    Else
                        txt_Gender.Value = ""
                        txt_Gender.Hidden = True
                    End If

                    If Not IsDBNull(drWIC("Corp_Tax_Registeration_Number")) Then
                        If drWIC("Corp_Tax_Registeration_Number") = 0 Then
                            txt_Is_PEP.Value = "No"
                        Else
                            txt_Is_PEP.Value = "Yes"
                        End If
                    Else
                        txt_Is_PEP.Value = "N/A"
                    End If
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Protected Sub LoadDataAccounts(strCIFNo As String)
    '    Try
    '        Dim strQuery As String = "SELECT acc.*, card.FK_AML_CARD_NO, card.LIMITCREDITCARD"
    '        strQuery &= " FROM AML_ACCOUNT acc"
    '        strQuery &= " LEFT JOIN AML_CARD card ON acc.ACCOUNT_NO = card.ACCOUNT_NO"
    '        strQuery &= " WHERE acc.CIFNO='" & strCIFNo & "'"

    '        Dim dtAccount As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

    '        If dtAccount Is Nothing Then
    '            dtAccount = New DataTable
    '        End If

    '        If dtAccount IsNot Nothing Then
    '            'Running the following script for additional columns
    '            dtAccount.Columns.Add(New DataColumn("BRANCH_NAME", GetType(String)))
    '            dtAccount.Columns.Add(New DataColumn("ACCOUNT_TYPE", GetType(String)))
    '            dtAccount.Columns.Add(New DataColumn("PRODUCT_NAME", GetType(String)))

    '            'Get reference Table
    '            strQuery = "SELECT * FROM AML_BRANCH"
    '            Dim dtBranch As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

    '            strQuery = "SELECT * FROM AML_ACCOUNT_TYPE"
    '            Dim dtAccountType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

    '            strQuery = "SELECT * FROM AML_PRODUCT"
    '            Dim dtProduct As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

    '            For Each row As DataRow In dtAccount.Rows
    '                If dtBranch IsNot Nothing AndAlso Not IsDBNull(row("FK_AML_BRANCH_CODE")) Then
    '                    Dim drCek = dtBranch.Select("FK_AML_BRANCH_CODE='" & row("FK_AML_BRANCH_CODE") & "'").FirstOrDefault
    '                    If drCek IsNot Nothing Then
    '                        row("BRANCH_NAME") = drCek("BRANCH_NAME")
    '                    End If
    '                End If
    '                If dtAccountType IsNot Nothing AndAlso Not IsDBNull(row("FK_AML_ACCOUNT_TYPE_CODE")) Then
    '                    Dim drCek = dtAccountType.Select("FK_AML_ACCOUNT_TYPE_CODE='" & row("FK_AML_ACCOUNT_TYPE_CODE") & "'").FirstOrDefault
    '                    If drCek IsNot Nothing Then
    '                        row("ACCOUNT_TYPE") = drCek("ACCOUNT_TYPE_NAME")
    '                    End If
    '                End If
    '                If dtProduct IsNot Nothing AndAlso Not IsDBNull(row("FK_AML_PRODUCT_CODE")) Then
    '                    Dim drCek = dtProduct.Select("FK_AML_PRODUCT_CODE='" & row("FK_AML_PRODUCT_CODE") & "'").FirstOrDefault
    '                    If drCek IsNot Nothing Then
    '                        row("PRODUCT_NAME") = drCek("PRODUCT_NAME")
    '                    End If
    '                End If

    '            Next

    '            'Bind to gridpanel
    '            gp_Account.GetStore.DataSource = dtAccount
    '            gp_Account.GetStore.DataBind()

    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub


    Protected Sub LoadSummary()
        Try
            Dim paramGetSummary(0) As SqlParameter
            paramGetSummary(0) = New SqlParameter
            paramGetSummary(0).ParameterName = "@WICNo"
            paramGetSummary(0).Value = Me.WICNo
            paramGetSummary(0).DbType = SqlDbType.VarChar

            Dim dtSummary As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_OneFCC_CaseManagement_GetCMSummarybyWIC", paramGetSummary)

            If dtSummary Is Nothing Then
                dtSummary = New DataTable
            End If

            If dtSummary IsNot Nothing Then

                'Bind to gridpanel
                gp_Summary.GetStore.DataSource = dtSummary
                gp_Summary.GetStore.DataBind()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_PerWIC(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = " Created_Date Desc "
            For Each item As DataSorter In e.Sort
                strsort += ", " & item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "select cm.PK_CaseManagement_ID as 'Case_ID' "

            strQuery &= "	,cm.PIC as 'PIC' "
            strQuery &= "   ,case when isnull(sts.PK_OneFCC_CaseManagement_CaseStatus_ID,1) in (1,2) then 'No' else 'Yes' End as 'Is_Closed' "
            'strQuery &= "   , trnact.Template_Category_Parameter_Name as 'TRN_ACT' " '' Edit 09-Jan-2023
            strQuery &= "   , case when typology.Is_Transaction = 1 then 'TRN' "
            strQuery &= "          when typology.Is_Transaction = 0 then 'ACT' else 'TRN' end as 'TRN_ACT' " '' Edit 31-Jan-2023 tambah Else 'TRN'

            strQuery &= "	,cm.WIC_No as 'WIC_No' "
            strQuery &= "	,cm.Unique_CM_ID as 'Unique_CM_ID' "
            strQuery &= "	,cm.WIC_Name as 'WIC_Name' "
            strQuery &= "	,cm.Alert_Type as 'Alert_Type' "
            'strQuery &= "	,isNull(act.Proposed_Action ,'New Case') as 'Case_Status' "
            strQuery &= "	,isNull(sts.CaseStatus_Name ,'New Case') as 'Case_Status' "
            strQuery &= "	,cm.Workflow_Step as 'Workflow_Step' "
            strQuery &= "	,cm.CreatedDate as 'Created_Date' "
            strQuery &= "	,cm.LastUpdateDate as 'Last_Update_Date' "
            strQuery &= "	, case when isnull(PK_OneFCC_CaseManagement_CaseStatus_ID,1) in (0,1,2) then dbo.fn_GetWorkdays(cm.ProcessDate, GETDATE()) else 0 end as 'Aging' "
            strQuery &= "	, case when isnull(PK_OneFCC_CaseManagement_CaseStatus_ID,1) in (2) then dbo.fn_GetWorkdays(cm.LastUpdateDate, GETDATE()) else 0 end as 'Aging_Escalation' " '' Add 2023-Jan-24, Tambah Aging Escalation
            strQuery &= "	, isNull(Proposed_Act.Proposed_Action,'New Case') as 'Last_Proposed_Action' "
            strQuery &= "	, count(sameCase.PK_CaseManagement_ID) as 'Count_Same_Case' "

            strQuery &= "   ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "' as Current_Login "
            strQuery &= "	FROM oneFCC_CaseManagement cm "
            'strQuery &= "	left join OneFCC_CaseManagement_ProposedAction act "
            'strQuery &= "	on cm.FK_CaseStatus_ID = act.PK_OneFCC_CaseManagement_ProposedAction_ID "
            strQuery &= "	left join onefcc_casemanagement_CaseStatus sts "
            strQuery &= "	on cm.FK_CaseStatus_ID = sts.PK_OneFCC_CaseManagement_CaseStatus_ID "
            strQuery &= "	left join OneFCC_CaseManagement_ProposedAction Proposed_Act "
            strQuery &= "	on cm.FK_Proposed_Status_ID = Proposed_Act.PK_OneFCC_CaseManagement_ProposedAction_ID "
            strQuery &= "	left join OneFCC_CaseManagement sameCase "
            strQuery &= "	on cm.Alert_Type = sameCase.Alert_Type "
            strQuery &= "	and cm.WIC_No = sameCase.WIC_No "
            strQuery &= "	and cm.FK_Rule_Basic_ID = sameCase.FK_Rule_Basic_ID "
            strQuery &= "	and cm.PK_CaseManagement_ID <> sameCase.PK_CaseManagement_ID "
            '' add 11-Nov-2022
            strQuery &= "	left join onefcc_ms_rule_basic rb "
            strQuery &= "	On cm.FK_Rule_Basic_ID = rb.PK_Rule_Basic_ID "
            strQuery &= "	left join OneFcc_MS_Rule_Template rt "
            strQuery &= "	On rb.FK_Rule_Template_ID = rt.PK_Rule_Template_ID "
            strQuery &= "	left join OneFCC_MS_Template_Category_Parameter trnact "
            strQuery &= "	On rt.FK_Template_Category_Parameter = trnact.PK_Template_Category_Parameter "
            '' End 11-Nov-2022
            '' Add 09-Jan-2023
            'strQuery &= "	inner join OneFCC_CaseManagement_Typology typology "
            strQuery &= "	left join OneFCC_CaseManagement_Typology typology " '' Edit 31-Jan-2023 Update jadi left, agar Financial Risk (ga ada di typology), ttp muncul
            strQuery &= "	On cm.PK_CaseManagement_ID = typology.FK_CaseManagement_ID "
            '' End 09-Jan-2023
            strQuery &= "	where cm.WIC_No = '" & Me.WICNo & "'"
            strQuery &= "   and cm.FK_CaseStatus_ID not in (-1,-2) "


            '' Add 26-Sep-2022 Replace filter name
            strfilter = Replace(strfilter, "[Case_ID]", "cm.PK_CaseManagement_ID")
            strfilter = Replace(strfilter, "[WIC_No]", "cm.WIC_No")
            strfilter = Replace(strfilter, "[Customer_Name]", "cm.Customer_Name")
            strfilter = Replace(strfilter, "[Alert_Type]", "cm.Alert_Type")
            strfilter = Replace(strfilter, "[Case_Status]", "isNull(sts.CaseStatus_Name ,'New Case')")
            strfilter = Replace(strfilter, "[Workflow_Step]", "cm.Workflow_Step")
            strfilter = Replace(strfilter, "[Created_Date]", "cm.CreatedDate")
            strfilter = Replace(strfilter, "[Last_Update_Date]", "cm.LastUpdateDate")
            strfilter = Replace(strfilter, "[PIC]", "cm.[PIC]")
            strfilter = Replace(strfilter, "[Aging]", "case when isnull(PK_OneFCC_CaseManagement_CaseStatus_ID,1) in (0,1,2) then dbo.fn_GetWorkdays(cm.ProcessDate, GETDATE()) else 0 end")
            strfilter = Replace(strfilter, "[Aging_Escalation]", "case when isnull(PK_OneFCC_CaseManagement_CaseStatus_ID,1) in (2) then dbo.fn_GetWorkdays(cm.LastUpdateDate, GETDATE()) else 0 end") '' Add 2023-Jan-24, Tambah Aging Escalation
            strfilter = Replace(strfilter, "[Last_Proposed_Action]", "isNull(Proposed_Act.Proposed_Action,'New Case')")
            strfilter = Replace(strfilter, "[Count_Same_Case]", "count(sameCase.PK_CaseManagement_ID)")
            strfilter = Replace(strfilter, "[Is_Closed]", "case when isnull(sts.PK_OneFCC_CaseManagement_CaseStatus_ID,1) in (1,2) then 'No' else 'Yes' End")

            'strfilter = Replace(strfilter, "[TRN_ACT]", "trnact.Template_Category_Parameter_Name")
            'strfilter = Replace(strfilter, "[TRN_ACT]", "trnact.Template_Category_Parameter_Name")
            strfilter = Replace(strfilter, "[TRN_ACT]", " case when typology.Is_Transaction = 1 then 'TRN' when typology.Is_Transaction = 0 then 'ACT' else 'TRN' end ") '' Edit 09-Jan-2023
            '' End 26-Sep-2022


            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " And " & strfilter
            End If

            '' Edit 26-Sep-2022
            strQuery &= "	group by cm.PK_CaseManagement_ID ,cm.WIC_No ,cm.WIC_Name,cm.Alert_Type "
            strQuery &= "	,isNull(sts.CaseStatus_Name ,'New Case') "
            strQuery &= "	,cm.Workflow_Step "
            strQuery &= "	,cm.CreatedDate "
            strQuery &= "	,cm.Unique_CM_ID "
            strQuery &= "	,cm.LastUpdateDate "
            strQuery &= "	,cm.PIC "
            strQuery &= "	, case when isnull(PK_OneFCC_CaseManagement_CaseStatus_ID,1) in (0,1,2) then dbo.fn_GetWorkdays(cm.ProcessDate, GETDATE()) else 0 end "
            strQuery &= "	, case when isnull(PK_OneFCC_CaseManagement_CaseStatus_ID,1) in (2) then dbo.fn_GetWorkdays(cm.LastUpdateDate, GETDATE()) else 0 end " '' Add 2023-Jan-24, Tambah Aging Escalation
            strQuery &= "	, isNull(Proposed_Act.Proposed_Action,'New Case') "
            strQuery &= "   ,sts.PK_OneFCC_CaseManagement_CaseStatus_ID"
            'strQuery &= "   ,trnact.Template_Category_Parameter_Name"
            strQuery &= "   ,case when typology.Is_Transaction = 1 then 'TRN' when typology.Is_Transaction = 0 then 'ACT' else 'TRN' end" '' Edit 09-Jan-2023
            '' Edit 26-Sep-2022
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_perWIC.GetStore.DataSource = DataPaging
            gp_CaseAlert_perWIC.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_CaseAlert_PerWIC(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            If strCommandName = "Detail" Then
                Dim StrQueryJumlahQuestionnaire As String = "select count(*) from OneFCC_CaseManagement_Questionaire_Answer where FK_CASEMANAGEMENT_ID = " & ID
                Dim JumlahQuestionnaire As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryJumlahQuestionnaire, Nothing)
                If JumlahQuestionnaire > 0 Then
                    pnl_noquestion.Hidden = True
                    pnlContent.Hidden = False
                    Session("CaseIDCSAlertWIC") = Nothing
                    Session("CaseIDCSAlertWIC") = ID
                    Dim strSQL As String = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & ID
                    Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    Dim StrWICNo As String = ""
                    If drCM IsNot Nothing Then
                        StrWICNo = drCM("WIC_No")
                    End If
                    Session("WICNoCSAlertWIC") = StrWICNo
                    pnlContent.ClearContent()
                    pnlContent.AnimCollapse = False
                    pnlContent.Loader.SuspendScripting()
                    pnlContent.Loader.Url = "CaseManagementAlertByWIC_Questionnaire.aspx" & "?ModuleID=" & IDModule
                    pnlContent.Loader.Params.Clear()
                    pnlContent.LoadContent()

                Else
                    pnl_noquestion.Hidden = False
                    pnlContent.Hidden = True
                    lblnoquestion.Text = "No Questionaire Answer"
                End If


                LoadDataCaseManagement(ID)
                window_CaseAlertDetail.Hidden = False




            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Function ExtText(pn As Ext.Net.FormPanel, strLabel As String, strFieldName As String, bRequired As Boolean, intMaxSize As Integer, intgridpos As Integer, StrJawaban As String) As Ext.Net.TextField
        Dim objTextField As New Ext.Net.TextField
        objTextField.ID = strFieldName
        objTextField.ClientIDMode = Web.UI.ClientIDMode.Static
        objTextField.FieldLabel = strLabel

        objTextField.ID = strFieldName
        objTextField.Name = strFieldName
        objTextField.AllowBlank = Not bRequired
        objTextField.BlankText = strLabel & " is required."
        objTextField.MaxLength = intMaxSize
        objTextField.LabelWidth = 500
        objTextField.MinWidth = 900

        objTextField.AnchorHorizontal = "80%"
        objTextField.ReadOnly = True

        objTextField.Value = StrJawaban

        pn.Add(objTextField)
        Return objTextField
    End Function
    '------- LOAD COMPONENT
    Protected Function ExtPanel(pn As FormPanel, strName As String, strTitle As String, isRiskRating As Boolean) As Ext.Net.Panel
        Try
            Dim objPanel As New Ext.Net.Panel
            With objPanel
                .ID = strName
                .ClientIDMode = Web.UI.ClientIDMode.Static
                .Title = strTitle
                .MarginSpec = "0 0 10 0"
                If isRiskRating Then
                    .BodyStyle = "background-color: #33ff3a;"
                    .Border = False
                    .Layout = "ColumnLayout"
                Else
                    .Collapsible = True
                    .Border = True
                    .BodyPadding = 10
                End If
            End With

            pn.Add(objPanel)
            Return objPanel

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Protected Sub Load_Questionnaire(objpanelinput As Ext.Net.FormPanel, customerType As String)
        Try
            Dim strQuery As String = ""
            Dim intSequence As Integer


            'Load Question Group 
            'strQuery = "SELECT DISTINCT ofqg.PK_OneFCC_Question_Group_ID, ofqg.Question_Group_Name, ofqg.Sequence"
            'strQuery = strQuery & " FROM OneFCC_Questionnaire ofq"
            'strQuery = strQuery & " JOIN OneFCC_Question_Group AS ofqg ON ofq.FK_OneFCC_Question_Group_ID = ofqg.PK_OneFCC_Question_Group_ID"
            'strQuery = strQuery & " WHERE ofqg.FK_OneFCC_Question_Type_Code = 'EDD'"
            'strQuery = strQuery & " ORDER BY ofqg.Sequence"
            strQuery = "select OneFCC_CaseManagement_Questionaire_Answer where FK_CASEMANAGEMENT_ID =  " & Session("CaseIDCSAlertWIC")

            Dim dtQuestionGroup As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
            For Each row In dtQuestionGroup.Rows
                intSequence = row("SEQUENCE")
                Dim isRequired As Boolean = False
                ExtText(objpanelinput, intSequence & ". " & row("QUESTION"), "ANSWER_" & row("PK_CM_QUESTIONAIRE_ANSWER_ID"), isRequired, 8000, intSequence, row("ANSWER"))
            Next
            If dtQuestionGroup.Rows.Count = 0 Then

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadToFormPanel(objpanelinput As Ext.Net.FormPanel, customerType As String)
        Try
            Dim typeclassCustomer As CasemanagementDAL.OneFCC_Question_Customer_Type = OneFCC_EDD_BLL.GetEDDCustomerTypeByCode(customerType)
            If typeclassCustomer IsNot Nothing Then
                objpanelinput.Title = "Customer Question"
                Load_Questionnaire(objpanelinput, Nothing)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDataCaseManagement(strCaseID As String)
        Try

            CaseID_WindowCaseAlertDetail = strCaseID

            Dim strSQL As String = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & strCaseID
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            If drCM IsNot Nothing Then
                Me.WICNo = drCM("WIC_No")
                Me.Workflow_Step = drCM("Workflow_Step")
                Me.Workflow_Step_Total = drCM("Workflow_Step_Total")
                Me.Workflow_ID = drCM("FK_CaseManagement_Workflow_ID")

                'General Information
                txt_PK_CaseManagement_ID.Value = drCM("PK_CaseManagement_ID")
                txt_Unique_CM_ID.Value = drCM("Unique_CM_ID")
                txt_Case_Description.Value = drCM("Case_Description")
                txt_Alert_Type.Value = drCM("Alert_Type")

                If Not IsDBNull(drCM("FK_CaseStatus_ID")) Then
                    'strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_ProposedAction WHERE PK_OneFCC_CaseManagement_ProposedAction_ID=" & drCM("FK_CaseStatus_ID")
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_CaseStatus WHERE PK_OneFCC_CaseManagement_CaseStatus_ID=" & drCM("FK_CaseStatus_ID")
                    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drTemp IsNot Nothing Then
                        txt_FK_Case_Status_ID.Value = drTemp("PK_OneFCC_CaseManagement_CaseStatus_ID") & " - " & drTemp("CaseStatus_Name")
                    Else
                        txt_FK_Case_Status_ID.Value = "New Case"
                    End If
                Else
                    txt_FK_Case_Status_ID.Value = "New Case"
                End If

                If Not IsDBNull(drCM("ProcessDate")) Then
                    txt_ProcessDate.Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                End If
                If Not IsDBNull(drCM("LastUpdateDate")) Then
                    txt_LastModifiedDate.Value = CDate(drCM("LastUpdateDate")).ToString("dd-MMM-yyyy HH:mm:ss")
                End If
                If Not IsDBNull(drCM("FK_Proposed_Status_ID")) Then
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_ProposedAction WHERE PK_OneFCC_CaseManagement_ProposedAction_ID=" & drCM("FK_Proposed_Status_ID")
                    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drTemp IsNot Nothing Then
                        txt_Proposed_Action.Value = drCM("FK_Proposed_Status_ID") & " - " & drTemp("PROPOSED_ACTION")
                    Else
                        txt_Proposed_Action.Value = ""
                    End If
                End If

                txt_Proposed_By.Value = drCM("Proposed_By")
                txt_Workflow_Step.Value = drCM("Workflow_Step_Of")
                txt_PIC.Value = drCM("PIC")

                'Switch between Typology/Outlier Transaction based on Alert_Type
                gp_CaseAlert_Typology_Transaction.Hidden = True
                pnl_CaseAlert_Outlier_Transaction.Hidden = True
                gp_CaseAlert_Typology_NonTransaction.Hidden = True '' Added 11-Nov-2022

                If Not IsDBNull(drCM("Alert_Type")) Then
                    Select Case drCM("Alert_Type")
                        Case "Typology Risk"

                            '' Add 11-Nov-2022, pembagian TRN / ACT''

                            'gp_CaseAlert_Typology_Transaction.Hidden = False
                            'BindTypologyTransaction(strCaseID)

                            Dim trn_act_SQL As String = " select trnact.Template_Category_Parameter_Name  "
                            trn_act_SQL &= " FROM oneFCC_CaseManagement cm "
                            trn_act_SQL &= " inner join onefcc_ms_rule_basic rb "
                            trn_act_SQL &= " On cm.FK_Rule_Basic_ID = rb.PK_Rule_Basic_ID "
                            trn_act_SQL &= " inner join OneFcc_MS_Rule_Template rt "
                            trn_act_SQL &= " On rb.FK_Rule_Template_ID = rt.PK_Rule_Template_ID "
                            trn_act_SQL &= " inner join OneFCC_MS_Template_Category_Parameter trnact "
                            trn_act_SQL &= " On rt.FK_Template_Category_Parameter = trnact.PK_Template_Category_Parameter "
                            trn_act_SQL &= " where cm.PK_CaseManagement_ID = " & strCaseID

                            Dim strTRNACT As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, trn_act_SQL, Nothing)

                            If strTRNACT IsNot Nothing Then
                                If strTRNACT = "Transaction" Then
                                    gp_CaseAlert_Typology_Transaction.Hidden = False
                                    BindTypologyTransaction(strCaseID)
                                ElseIf strTRNACT = "Activity" Then
                                    gp_CaseAlert_Typology_NonTransaction.Hidden = False
                                    BindTypologyNonTransaction(strCaseID)
                                End If
                            End If
                            '' End 11-Nov-2022

                            Load_CaseAlert_Other(strCaseID)
                        Case "Financial Risk"
                            pnl_CaseAlert_Outlier_Transaction.Hidden = False
                            BindOutlierTransaction(strCaseID)

                            'Display Mean dan Modus
                            strSQL = "Select TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=2"
                            Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                            If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                                fs_CaseAlert_Outlier_Transaction.Title = "<b>Financial Statistics In past " & drParam("ParameterValue") & " month(s)</b>"
                            End If

                            strSQL = "Select TOP 1 * FROM OneFCC_CaseManagement_Outlier WHERE FK_CaseManagement_ID=" & strCaseID
                            Dim drCMO As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                            If drCMO IsNot Nothing Then

                                If Not IsDBNull(drCMO("Mean_Debit")) Then
                                    txt_MeanDebit.Value = CDbl(drCMO("Mean_Debit")).ToString("#,##0.00")
                                End If

                                If Not IsDBNull(drCMO("Mean_Credit")) Then
                                    txt_MeanCredit.Value = CDbl(drCMO("Mean_Credit")).ToString("#,##0.00")
                                End If

                                If Not IsDBNull(drCMO("Modus_Debit")) Then
                                    txt_ModusDebit.Value = CDbl(drCMO("Modus_Debit")).ToString("#,##0.00")
                                End If

                                If Not IsDBNull(drCMO("Modus_Credit")) Then
                                    txt_ModusCredit.Value = CDbl(drCMO("Modus_Credit")).ToString("#,##0.00")
                                End If

                            End If
                    End Select
                End If

                BindWorkflowHistory(strCaseID)

                'btn_Save.Hidden = True

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BindTypologyTransaction(strCaseID As String)
        Try

            Dim strQuery As String = "Select ofcmtt.*"
            strQuery &= " ,Case When CIF_No_Lawan Is Not NULL Then 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & strCaseID

            Dim dtTypologyTransaction As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTypologyTransaction Is Nothing Then
                dtTypologyTransaction = New DataTable
            End If

            gp_CaseAlert_Typology_Transaction.GetStore.DataSource = dtTypologyTransaction
            gp_CaseAlert_Typology_Transaction.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindOutlierTransaction(strCaseID As String)
        Try

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & strCaseID

            Dim dtOutlierTransaction As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtOutlierTransaction Is Nothing Then
                dtOutlierTransaction = New DataTable
            End If

            gp_CaseAlert_Outlier_Transaction.GetStore.DataSource = dtOutlierTransaction
            gp_CaseAlert_Outlier_Transaction.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindWorkflowHistory(strCaseID As String)
        Try

            Dim strQuery As String = "SELECT history.*, proposed.Proposed_Action FROM onefcc_casemanagement_workflowhistory history"
            strQuery += " LEFT JOIN OneFCC_CaseManagement_ProposedAction proposed ON history.FK_Proposed_Status_ID = proposed.PK_OneFCC_CaseManagement_ProposedAction_ID"
            strQuery += " WHERE FK_CaseManagement_ID = " & strCaseID

            Dim dtWorkflowHistory As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtWorkflowHistory Is Nothing Then
                dtWorkflowHistory = New DataTable
            End If

            gp_WorkflowHistory.GetStore.DataSource = dtWorkflowHistory
            gp_WorkflowHistory.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub BindOtherCase(strCaseID As String)
    '    Try

    '        Dim strSQL As String = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
    '        Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

    '        Dim strQuery As String = ""
    '        If drCM IsNot Nothing Then
    '            strQuery = "SELECT * FROM vw_OneFCC_CaseManagement"
    '            strQuery += " WHERE PK_CaseManagement_ID <> " & strCaseID
    '            strQuery += " AND CIF_No = '" & Me.CIFNo & "'"

    '            If Not IsDBNull(drCM("Alert_Type")) Then
    '                strQuery += " AND Alert_Type = '" & drCM("Alert_Type") & "'"
    '            End If

    '            If Not IsDBNull(drCM("FK_Rule_Basic_ID")) Then
    '                strQuery += " AND FK_Rule_Basic_ID = '" & drCM("FK_Rule_Basic_ID") & "'"
    '            End If

    '        End If

    '        Dim dtOtherCase As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
    '        If dtOtherCase Is Nothing Then
    '            dtOtherCase = New DataTable
    '        End If

    '        gp_OtherCase.GetStore.DataSource = dtOtherCase
    '        gp_OtherCase.GetStore.DataBind()

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Protected Sub GridcommandAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Download" Then
                DownloadFile(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DownloadFile(id As Long)

        Dim path As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select parametervalue from OneFCC_CaseManagement_Parameter where PK_GlobalReportParameter_ID = 19 ", Nothing)
        path = path & "\FolderExport\"

        Dim Dirpath = path & NawaBLL.Common.SessionCurrentUser.UserID & "\"
        If Not Directory.Exists(Dirpath) Then
            Directory.CreateDirectory(Dirpath)
        End If

        'Dim objdownload As NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment = ListDokumen.Find(Function(x) x.PK_ID = id)

        Dim dtHistoryAttachment As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select Attachment, AttachmentName FROM OneFCC_CaseManagement_WorkflowHistory where PK_CaseManagement_WorkflowHistory_ID = " & id & "", Nothing)

        If dtHistoryAttachment IsNot Nothing Then
            For Each row As DataRow In dtHistoryAttachment.Rows
                If row.Item("Attachment") IsNot Nothing And Not (IsDBNull(row.Item("Attachment"))) And Not (IsDBNull(row.Item("AttachmentName"))) Then
                    Dim listZip As New List(Of String)
                    listZip.Clear()

                    Dim fileattachmentname As String = Dirpath & row(1)
                    IO.File.WriteAllBytes(fileattachmentname, row(0))
                    listZip.Add(fileattachmentname)

                    Filetodownload = listZip.Item(0).ToString

                    If Filetodownload Is Nothing Then
                        Throw New Exception("Not Exists Report")

                    End If
                End If
            Next
        End If

        If Not Filetodownload Is Nothing Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(Filetodownload))
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            'Response.ContentType = "ContentType"
            Response.ContentType = MimeMapping.GetMimeMapping(IO.Path.GetFileName(Filetodownload))
            Response.BinaryWrite(IO.File.ReadAllBytes(Filetodownload))
            Response.End()

            If IO.File.Exists(Filetodownload) Then
                IO.File.Delete(Filetodownload)
            End If

            Filetodownload = ""
        End If
    End Sub

    Protected Sub btn_Back_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Save_Click()
        Try

            '' Validate Proposed Status
            If String.IsNullOrEmpty(cmb_ProposedAction.StringValue) Then
                cmb_ProposedAction.Focus()
                Throw New ApplicationException("Proposed Action is required.")
            End If

            '' Validate Investigation Note
            If String.IsNullOrWhiteSpace(txt_InvestigationNotes.Value) Then
                cmb_ProposedAction.Focus()
                Throw New ApplicationException("Investigation Note is required.")
            End If

            '' Validate Ada yg di checklist
            Dim smCaseAlert As RowSelectionModel = TryCast(gp_CaseAlert_perWIC.GetSelectionModel(), RowSelectionModel)
            Dim selected As RowSelectionModel = gp_CaseAlert_perWIC.SelectionModel.Primary
            If selected.SelectedRows.Count = 0 Then

                Throw New Exception("Please select minimum 1 Case Alert.")
            End If

            '' Validate Proposed Action based on Workflow step
            For Each item As SelectedRow In smCaseAlert.SelectedRows
                Dim recordID = item.RecordID.ToString

                Dim objParam(1) As SqlParameter
                objParam(0) = New SqlParameter
                objParam(0).ParameterName = "@CaseID"
                objParam(0).Value = recordID
                objParam(0).DbType = SqlDbType.VarChar

                objParam(1) = New SqlParameter
                objParam(1).ParameterName = "@ProposedAction"
                objParam(1).Value = cmb_ProposedAction.SelectedItemValue.ToString
                objParam(1).DbType = SqlDbType.VarChar

                Dim Message As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_OneFCC_CaseManagement_ValidateProposedAction_ByCaseID", objParam)

                If Message IsNot Nothing Then
                    If Message <> "Valid" Then
                        Throw New ApplicationException(Message)
                    End If
                End If

            Next


            '' Save Data
            For Each item As SelectedRow In smCaseAlert.SelectedRows
                Dim recordID = item.RecordID.ToString

                Dim objParam(5) As SqlParameter
                objParam(0) = New SqlParameter
                objParam(0).ParameterName = "@CaseID"
                objParam(0).Value = recordID
                objParam(0).DbType = SqlDbType.VarChar

                objParam(1) = New SqlParameter
                objParam(1).ParameterName = "@ProposedActionID"
                objParam(1).Value = cmb_ProposedAction.SelectedItemValue.ToString
                objParam(1).DbType = SqlDbType.VarChar

                objParam(2) = New SqlParameter
                objParam(2).ParameterName = "@UserID"
                objParam(2).Value = NawaBLL.Common.SessionCurrentUser.UserID
                objParam(2).DbType = SqlDbType.VarChar

                objParam(3) = New SqlParameter
                objParam(3).ParameterName = "@Notes"
                objParam(3).Value = txt_InvestigationNotes.Value
                objParam(3).DbType = SqlDbType.VarChar

                objParam(4) = New SqlParameter
                objParam(4).ParameterName = "@FileBinary"
                objParam(4).Value = file_Attachment.FileBytes
                objParam(4).DbType = SqlDbType.Binary

                objParam(5) = New SqlParameter
                objParam(5).ParameterName = "@FileName"
                objParam(5).Value = file_Attachment.FileName
                objParam(5).DbType = SqlDbType.VarChar

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_OneFCC_CaseManagement_SaveCMPerCIF_byCaseID", objParam)

            Next

            'Show Confirmation
            LblConfirmation.Text = "Case Management Follow Up has been submitted."
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Function CheckIfStringExists(strPart As String, strFull As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim dt As DataTable = New DataTable

            Dim strsplit As String() = strFull.Split(","c)
            For Each item As String In strsplit
                If strPart = item Then
                    Return True
                End If
            Next

            Return bolResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        If buttonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub

    Sub SetCommandColumnLocation()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim buttonPosition As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            buttonPosition = objParamSettingbutton.SettingValue
        End If

        ColumnActionLocation(gp_CaseAlert_perWIC, cc_CaseAlert_PerWIC, buttonPosition)
        ColumnActionLocation(gp_OtherCase, cc_OtherCase, buttonPosition)

    End Sub


#Region "10-Feb-2022 : Export Hit/Search Transaction to CSV/Excel"
    Protected Sub ExportAll_CaseAlert_Typology(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            'Get Data Transaction
            'Dim strQuery As String = "SELECT ofcmtt.Date_Transaction, ofcmtt.Account_NO, ofcmtt.Transmode_Code, ofcmtt.Transaction_Remark, ofcmtt.IDR_Amount,"
            'strQuery &= " ofcmtt.country_code, ofcmtt.country_code_lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            '29-Mar-2022 Adi
            Dim strQuery As String = "SELECT ofcmtt.Date_Transaction"
            strQuery &= " ,ofcmtt.Ref_Num"
            strQuery &= " ,ofcmtt.Transmode_Code"
            strQuery &= " ,ofcmtt.Transaction_Location"
            strQuery &= " ,ofcmtt.Debit_Credit"
            strQuery &= " ,ofcmtt.Currency"
            strQuery &= " ,ofcmtt.Original_Amount"
            strQuery &= " ,ofcmtt.IDR_Amount"
            strQuery &= " ,ofcmtt.Transaction_Remark"
            strQuery &= " ,ofcmtt.Account_NO"
            strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,ofcmtt.Account_No_Lawan"
            strQuery &= " ,ofcmtt.CIF_No_Lawan"
            strQuery &= " ,ofcmtt.WIC_No_Lawan"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = '" & CaseID_WindowCaseAlertDetail & "'"

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            'Get Data CM
            strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & CaseID_WindowCaseAlertDetail
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Typology_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CaseManagementTypologyTrx")

                    'Display Header of CM
                    If drCM IsNot Nothing Then
                        ws.Cells("A1").Value = "Case Management ID"
                        ws.Cells("B1").Value = drCM("PK_CaseManagement_ID").ToString

                        ws.Cells("A2").Value = "WIC"
                        ws.Cells("B2").Value = drCM("WIC_No")

                        ws.Cells("A3").Value = "Customer Name"
                        ws.Cells("B3").Value = drCM("Customer_Name")

                        ws.Cells("A4").Value = "Alert Type"
                        ws.Cells("B4").Value = drCM("Alert_Type")

                        ws.Cells("A5").Value = "Case Description"
                        ws.Cells("B5").Value = drCM("Case_Description")

                        ws.Cells("A6").Value = "Process Date"
                        ws.Cells("B6").Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                    End If

                    'objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    'objtbl.Columns("Account_NO").SetOrdinal(1)
                    'objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    'objtbl.Columns("Transaction_Remark").SetOrdinal(3)
                    'objtbl.Columns("IDR_Amount").SetOrdinal(4)
                    'objtbl.Columns("country_code").SetOrdinal(5)
                    'objtbl.Columns("country_code_lawan").SetOrdinal(6)

                    'objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                    'objtbl.Columns("Account_NO").ColumnName = "Account No"
                    'objtbl.Columns("Transmode_Code").ColumnName = "Transmode Code"
                    'objtbl.Columns("Transaction_Remark").ColumnName = "Transaction Remark"
                    'objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    'objtbl.Columns("country_code").ColumnName = "Country Code"
                    'objtbl.Columns("country_code_lawan").ColumnName = "Opp. Country Code"

                    '29-Mar-2022 Adi : Perubahan skema informasi yang ditampilkan
                    objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    objtbl.Columns("Ref_Num").SetOrdinal(1)
                    objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    objtbl.Columns("Transaction_Location").SetOrdinal(3)
                    objtbl.Columns("Debit_Credit").SetOrdinal(4)
                    objtbl.Columns("Currency").SetOrdinal(5)
                    objtbl.Columns("Original_Amount").SetOrdinal(6)
                    objtbl.Columns("IDR_Amount").SetOrdinal(7)
                    objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                    objtbl.Columns("Account_NO").SetOrdinal(9)
                    objtbl.Columns("CounterPartyType").SetOrdinal(10)
                    objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                    objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                    objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                    objtbl.Columns("CounterPartyName").SetOrdinal(14)
                    objtbl.Columns("Country_Lawan").SetOrdinal(15)

                    objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
                    objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
                    objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
                    objtbl.Columns("Transaction_Location").ColumnName = "Location"
                    objtbl.Columns("Debit_Credit").ColumnName = "D/C"
                    objtbl.Columns("Currency").ColumnName = "CCY"
                    objtbl.Columns("Original_Amount").ColumnName = "Amount"
                    objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                    objtbl.Columns("Account_NO").ColumnName = "Account No."
                    objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                    objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                    objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                    objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                    objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                    objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"

                    ws.Cells("A8").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Management Typology Transaction (Case ID " & CaseID_WindowCaseAlertDetail & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportAll_CaseAlert_Outlier(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            ''Get Data Transaction
            'Dim strQuery As String = "SELECT ofcmot.Date_Transaction, ofcmot.Account_NO, ofcmot.Transmode_Code, ofcmot.Transaction_Remark, ofcmot.IDR_Amount,"
            'strQuery &= " ofcmot.country_code, ofcmot.country_code_lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmot"
            'strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmo ON ofcmot.FK_OneFCC_CaseManagement_Outlier_ID = ofcmo.PK_OneFCC_CaseManagement_Outlier_ID"
            'strQuery &= " WHERE ofcmo.FK_CaseManagement_ID = " & IDUnik

            '29-Mar-2022 Adi
            Dim strQuery As String = "SELECT ofcmtt.Date_Transaction"
            strQuery &= " ,ofcmtt.Ref_Num"
            strQuery &= " ,ofcmtt.Transmode_Code"
            strQuery &= " ,ofcmtt.Transaction_Location"
            strQuery &= " ,ofcmtt.Debit_Credit"
            strQuery &= " ,ofcmtt.Currency"
            strQuery &= " ,ofcmtt.Original_Amount"
            strQuery &= " ,ofcmtt.IDR_Amount"
            strQuery &= " ,ofcmtt.Transaction_Remark"
            strQuery &= " ,ofcmtt.Account_NO"
            strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,ofcmtt.Account_No_Lawan"
            strQuery &= " ,ofcmtt.CIF_No_Lawan"
            strQuery &= " ,ofcmtt.WIC_No_Lawan"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = '" & CaseID_WindowCaseAlertDetail & "'"

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            'Get Data CM
            strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & CaseID_WindowCaseAlertDetail
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Outlier_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CaseManagementOutlierTrx")

                    'Display Header of CM
                    If drCM IsNot Nothing Then
                        ws.Cells("A1").Value = "Case Management ID"
                        ws.Cells("B1").Value = drCM("PK_CaseManagement_ID").ToString

                        ws.Cells("A2").Value = "CIF"
                        ws.Cells("B2").Value = drCM("CIF_No")

                        ws.Cells("A3").Value = "Customer Name"
                        ws.Cells("B3").Value = drCM("Customer_Name")

                        ws.Cells("A4").Value = "Alert Type"
                        ws.Cells("B4").Value = drCM("Alert_Type")

                        ws.Cells("A5").Value = "Case Description"
                        ws.Cells("B5").Value = drCM("Case_Description")

                        ws.Cells("A6").Value = "Process Date"
                        ws.Cells("B6").Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                    End If

                    'Display Financial Statistics
                    Dim strSQL As String = ""
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=2"
                    Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                        ws.Cells("A8").Value = "Financial Statistics in past " & drParam("ParameterValue") & " month(s)"
                    End If

                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Outlier WHERE FK_CaseManagement_ID=" & IDUnik
                    Dim drCMO As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drCMO IsNot Nothing Then
                        ws.Cells("A9").Value = "Mean Debit"
                        ws.Cells("B9").Value = CDbl(drCMO("Mean_Debit")).ToString("#,##0.00")

                        ws.Cells("A10").Value = "Mean Credit"
                        ws.Cells("B10").Value = CDbl(drCMO("Mean_Credit")).ToString("#,##0.00")

                        ws.Cells("A11").Value = "Modus Debit"
                        ws.Cells("B11").Value = CDbl(drCMO("Modus_Debit")).ToString("#,##0.00")

                        ws.Cells("A12").Value = "Modus Credit"
                        ws.Cells("B12").Value = CDbl(drCMO("Modus_Credit")).ToString("#,##0.00")
                    End If

                    'objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    'objtbl.Columns("Account_NO").SetOrdinal(1)
                    'objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    'objtbl.Columns("Transaction_Remark").SetOrdinal(3)
                    'objtbl.Columns("IDR_Amount").SetOrdinal(4)
                    'objtbl.Columns("country_code").SetOrdinal(5)
                    'objtbl.Columns("country_code_lawan").SetOrdinal(6)

                    'objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                    'objtbl.Columns("Account_NO").ColumnName = "Account No"
                    'objtbl.Columns("Transmode_Code").ColumnName = "Transmode Code"
                    'objtbl.Columns("Transaction_Remark").ColumnName = "Transaction Remark"
                    'objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    'objtbl.Columns("country_code").ColumnName = "Country Code"
                    'objtbl.Columns("country_code_lawan").ColumnName = "Opp. Country Code"

                    '29-Mar-2022 Adi : Perubahan skema informasi yang ditampilkan
                    objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    objtbl.Columns("Ref_Num").SetOrdinal(1)
                    objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    objtbl.Columns("Transaction_Location").SetOrdinal(3)
                    objtbl.Columns("Debit_Credit").SetOrdinal(4)
                    objtbl.Columns("Currency").SetOrdinal(5)
                    objtbl.Columns("Original_Amount").SetOrdinal(6)
                    objtbl.Columns("IDR_Amount").SetOrdinal(7)
                    objtbl.Columns("Transaction_Remark").SetOrdinal(8)
                    objtbl.Columns("Account_NO").SetOrdinal(9)
                    objtbl.Columns("CounterPartyType").SetOrdinal(10)
                    objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
                    objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
                    objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
                    objtbl.Columns("CounterPartyName").SetOrdinal(14)
                    objtbl.Columns("Country_Lawan").SetOrdinal(15)
                    objtbl.Columns("Unique_CM_ID").SetOrdinal(16)

                    objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
                    objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
                    objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
                    objtbl.Columns("Transaction_Location").ColumnName = "Location"
                    objtbl.Columns("Debit_Credit").ColumnName = "D/C"
                    objtbl.Columns("Currency").ColumnName = "CCY"
                    objtbl.Columns("Original_Amount").ColumnName = "Amount"
                    objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
                    objtbl.Columns("Account_NO").ColumnName = "Account No."
                    objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
                    objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
                    objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
                    objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
                    objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
                    objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"
                    objtbl.Columns("Unique_CM_ID").ColumnName = "Unique Case ID"

                    ws.Cells("A14").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Management Outlier Transaction (Case ID " & CaseID_WindowCaseAlertDetail & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportAll_CaseAlert_PerWIC(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            Dim strQuery As String = "select cm.PK_CaseManagement_ID as 'Case_ID' "
            strQuery &= "	,cm.WIC_No as 'WIC_No' "
            strQuery &= "	,cm.WIC_Name as 'WIC_Name' "
            strQuery &= "	,cm.Alert_Type as 'Alert_Type' "
            strQuery &= "	,cm.Unique_CM_ID as 'Unique_CM_ID' " '' 9 Maret 2023 Ari : Penambahan Unique CM ID
            'strQuery &= "	,isNull(act.Proposed_Action ,'New Case') as 'Case_Status' "
            strQuery &= "	,isNull(sts.CaseStatus_Name ,'New Case') as 'Case_Status' "
            strQuery &= "	,cm.Workflow_Step as 'Workflow_Step' "
            strQuery &= "	,cm.CreatedDate as 'Created_Date' "
            strQuery &= "	,cm.LastUpdateDate as 'Last_Update_Date' "
            strQuery &= "	,cm.PIC as 'PIC' "
            strQuery &= "	, case when isnull(PK_OneFCC_CaseManagement_CaseStatus_ID,1) in (1,2) then dbo.fn_GetWorkdays(cm.ProcessDate, GETDATE()) else 0 end as 'Aging' "
            strQuery &= "	, isNull(Proposed_Act.Proposed_Action,'New Case') as 'Last_Proposed_Action' "
            strQuery &= "	, count(sameCase.PK_CaseManagement_ID) as 'Count_Same_Case' "
            strQuery &= "   , case when isnull(sts.PK_OneFCC_CaseManagement_CaseStatus_ID,1) in (1,2) then 'No' else 'Yes' End as 'Is_Closed' "
            strQuery &= "   ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "' as Current_Login "
            strQuery &= "	FROM oneFCC_CaseManagement cm "
            'strQuery &= "	left join OneFCC_CaseManagement_ProposedAction act "
            'strQuery &= "	on cm.FK_CaseStatus_ID = act.PK_OneFCC_CaseManagement_ProposedAction_ID "
            strQuery &= "	left join onefcc_casemanagement_CaseStatus sts "
            strQuery &= "	on cm.FK_CaseStatus_ID = sts.PK_OneFCC_CaseManagement_CaseStatus_ID "
            strQuery &= "	left join OneFCC_CaseManagement_ProposedAction Proposed_Act "
            strQuery &= "	on cm.FK_Proposed_Status_ID = Proposed_Act.PK_OneFCC_CaseManagement_ProposedAction_ID "
            strQuery &= "	left join OneFCC_CaseManagement sameCase "
            strQuery &= "	on cm.Alert_Type = sameCase.Alert_Type "
            strQuery &= "	and cm.WIC_No = sameCase.WIC_No "
            strQuery &= "	and cm.FK_Rule_Basic_ID = sameCase.FK_Rule_Basic_ID "
            strQuery &= "	and cm.PK_CaseManagement_ID <> sameCase.PK_CaseManagement_ID "
            strQuery &= "	where cm.WIC_No = '" & Me.WICNo & "'"
            strQuery &= "   and cm.FK_CaseStatus_ID <> -1 "
            strQuery &= "	group by cm.PK_CaseManagement_ID ,cm.WIC_No ,cm.WIC_Name,cm.Alert_Type "
            strQuery &= "	,isNull(sts.CaseStatus_Name ,'New Case') "
            strQuery &= "	,cm.Workflow_Step "
            strQuery &= "	,cm.CreatedDate "
            strQuery &= "	,cm.Unique_CM_ID " '' 9 Maret 2023 Ari : Penambahan Unique CM ID
            strQuery &= "	,cm.LastUpdateDate "
            strQuery &= "	,cm.PIC "
            strQuery &= "	, case when isnull(PK_OneFCC_CaseManagement_CaseStatus_ID,1) in (1,2) then dbo.fn_GetWorkdays(cm.ProcessDate, GETDATE()) else 0 end "
            strQuery &= "	, isNull(Proposed_Act.Proposed_Action,'New Case') "
            strQuery &= "   ,sts.PK_OneFCC_CaseManagement_CaseStatus_ID"

            'Dim strQuery As String = "SELECT ofcmtt.Date_Transaction"
            'strQuery &= " ,ofcmtt.Ref_Num"
            'strQuery &= " ,ofcmtt.Transmode_Code"
            'strQuery &= " ,ofcmtt.Transaction_Location"
            'strQuery &= " ,ofcmtt.Debit_Credit"
            'strQuery &= " ,ofcmtt.Currency"
            'strQuery &= " ,ofcmtt.Original_Amount"
            'strQuery &= " ,ofcmtt.IDR_Amount"
            'strQuery &= " ,ofcmtt.Transaction_Remark"
            'strQuery &= " ,ofcmtt.Account_NO"
            'strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            'strQuery &= " ,ofcmtt.Account_No_Lawan"
            'strQuery &= " ,ofcmtt.CIF_No_Lawan"
            'strQuery &= " ,ofcmtt.WIC_No_Lawan"
            'strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            'strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            'strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            'strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            'strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            'strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            'strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            'strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            'Get Data CM
            'strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
            'Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Typology_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CaseManagementPerCIF")

                    'Display Header of CM

                    ws.Cells("A1").Value = "WIC No"
                    ws.Cells("B1").Value = txt_WIC_No.Value

                    ws.Cells("A2").Value = "Customer Name"
                    ws.Cells("B2").Value = txt_WIC_Name.Value

                    ws.Cells("A3").Value = "Date of Birth"
                    ws.Cells("B3").Value = txt_DOB.Value

                    ws.Cells("A4").Value = "Place of Birth"
                    ws.Cells("B4").Value = txt_POB.Value

                    ws.Cells("A5").Value = "Gender"
                    ws.Cells("B5").Value = txt_Gender.Value

                    ws.Cells("A6").Value = "PEP"
                    ws.Cells("B6").Value = txt_Is_PEP.Value


                    objtbl.Columns("Case_ID").SetOrdinal(0)
                    objtbl.Columns("WIC_No").SetOrdinal(1)
                    objtbl.Columns("WIC_Name").SetOrdinal(2)
                    objtbl.Columns("Unique_CM_ID").SetOrdinal(3)
                    objtbl.Columns("Alert_Type").SetOrdinal(4)
                    objtbl.Columns("Case_Status").SetOrdinal(5)
                    objtbl.Columns("Workflow_Step").SetOrdinal(6)
                    objtbl.Columns("Created_Date").SetOrdinal(7)
                    objtbl.Columns("Last_Update_Date").SetOrdinal(8)
                    objtbl.Columns("PIC").SetOrdinal(9)
                    objtbl.Columns("Aging").SetOrdinal(10)
                    objtbl.Columns("Last_Proposed_Action").SetOrdinal(11)
                    objtbl.Columns("Count_Same_Case").SetOrdinal(12)
                    objtbl.Columns("Is_Closed").SetOrdinal(13)

                    objtbl.Columns("Case_ID").ColumnName = "Case ID"
                    objtbl.Columns("WIC_No").ColumnName = "WIC No"
                    objtbl.Columns("WIC_Name").ColumnName = "Name"
                    objtbl.Columns("Unique_CM_ID").ColumnName = "Unique Case ID"
                    objtbl.Columns("Alert_Type").ColumnName = "Alert Type"
                    objtbl.Columns("Case_Status").ColumnName = "Status"
                    objtbl.Columns("Workflow_Step").ColumnName = "Step"
                    objtbl.Columns("Created_Date").ColumnName = "Create Date"
                    objtbl.Columns("Last_Update_Date").ColumnName = "Last Update Date"
                    objtbl.Columns("PIC").ColumnName = "PIC"
                    objtbl.Columns("Aging").ColumnName = "Aging"
                    objtbl.Columns("Last_Proposed_Action").ColumnName = "Last Proposed Action"
                    objtbl.Columns("Count_Same_Case").ColumnName = "Count Same Case"
                    objtbl.Columns("Is_Closed").ColumnName = "Is Closed"

                    ws.Cells("A11").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Management Per WIC (WIC No " & Me.WICNo & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Protected Sub ExportAll_SearchTransaction(sender As Object, e As EventArgs)
    '    Dim objfileinfo As IO.FileInfo
    '    Try
    '        'Date From and Date To mandatory
    '        If txt_SearchTransaction_From.SelectedDate = DateTime.MinValue Or txt_SearchTransaction_To.SelectedDate = DateTime.MinValue Then
    '            Throw New ApplicationException("Please fill Date From and Date To for searching transaction!")
    '        End If

    '        'Get Data CM
    '        Dim strQuery As String = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
    '        Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)


    '        'Get Data Transaction
    '        'strQuery = "SELECT gaot.Date_Transaction, gaot.Account_NO, gaot.Transmode_Code, gaot.Transaction_Remark, gaot.IDR_Amount,"
    '        'strQuery &= " gaot.country_code, gaot.country_code_lawan"
    '        'strQuery &= " FROM goAML_ODM_Transaksi AS gaot"
    '        'strQuery &= " WHERE gaot.CIF_NO = '" & Me.CIFNo & "'"

    '        '29-Mar-2022 Adi
    '        strQuery = "SELECT gaot.Date_Transaction"
    '        strQuery &= " ,gaot.Ref_Num"
    '        strQuery &= " ,gaot.Transmode_Code"
    '        strQuery &= " ,gaot.Transaction_Location"
    '        strQuery &= " ,gaot.Debit_Credit"
    '        strQuery &= " ,gaot.Currency"
    '        strQuery &= " ,gaot.Original_Amount"
    '        strQuery &= " ,gaot.IDR_Amount"
    '        strQuery &= " ,gaot.Transaction_Remark"
    '        strQuery &= " ,gaot.Account_NO"
    '        strQuery &= " ,CASE WHEN CIF_No_Lawan Is Not NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
    '        strQuery &= " ,gaot.Account_No_Lawan"
    '        strQuery &= " ,gaot.CIF_No_Lawan"
    '        strQuery &= " ,gaot.WIC_No_Lawan"
    '        strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
    '        strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
    '        strQuery &= " FROM goAML_ODM_Transaksi AS gaot"
    '        strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
    '        strQuery &= " ON gaot.CIF_No_Lawan = garc.CIF"
    '        strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
    '        strQuery &= " ON gaot.WIC_No_Lawan = garw.WIC_No"
    '        strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
    '        strQuery &= " ON gaot.country_code_lawan = ctry.Kode"
    '        strQuery &= " WHERE gaot.CIF_NO = '" & Me.CIFNo & "'"

    '        Dim dtTransactionAll As Data.DataTable
    '        If Not (txt_SearchTransaction_From.SelectedDate = DateTime.MinValue And txt_SearchTransaction_To.SelectedDate = DateTime.MinValue) Then
    '            strQuery += " And Date_Transaction >= '" & CDate(txt_SearchTransaction_From.SelectedDate).ToString("yyyy-MM-dd") & "'"
    '            strQuery += " AND Date_Transaction <= '" & CDate(txt_SearchTransaction_To.SelectedDate).ToString("yyyy-MM-dd") & "'"

    '            dtTransactionAll = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
    '        Else
    '            dtTransactionAll = New DataTable
    '        End If

    '        If dtTransactionAll.Rows.Count < 1 Then
    '            Throw New Exception("Data Kosong!")
    '        ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
    '            Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
    '        End If

    '        Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
    '        objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
    '        Using objtbl As Data.DataTable = dtTransactionAll

    '            'objFormModuleView.changeHeader(objtbl)
    '            For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Typology_Transaction.ColumnModel.Columns

    '                If item.Hidden Then
    '                    If objtbl.Columns.Contains(item.DataIndex) Then
    '                        objtbl.Columns.Remove(item.DataIndex)
    '                    End If

    '                End If
    '            Next

    '            Using resource As New ExcelPackage(objfileinfo)
    '                Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("ListOfTransaction")

    '                'Display Header of CM
    '                If drCM IsNot Nothing Then
    '                    ws.Cells("A1").Value = "List of Customer's Transactions"

    '                    ws.Cells("A2").Value = "CIF"
    '                    ws.Cells("B2").Value = drCM("CIF_No")

    '                    ws.Cells("A3").Value = "Customer Name"
    '                    ws.Cells("B3").Value = drCM("Customer_Name")

    '                    ws.Cells("A4").Value = "From Date"
    '                    ws.Cells("B4").Value = CDate(txt_SearchTransaction_From.SelectedDate).ToString("dd-MMM-yyyy")

    '                    ws.Cells("A5").Value = "To Date"
    '                    ws.Cells("B5").Value = CDate(txt_SearchTransaction_To.SelectedDate).ToString("dd-MMM-yyyy")

    '                    ws.Cells("A6").Value = "Generate Date"
    '                    ws.Cells("B6").Value = CDate(DateTime.Now).ToString("dd-MMM-yyyy HH:mm:ss")
    '                End If

    '                'objtbl.Columns("Date_Transaction").SetOrdinal(0)
    '                'objtbl.Columns("Account_NO").SetOrdinal(1)
    '                'objtbl.Columns("Transmode_Code").SetOrdinal(2)
    '                'objtbl.Columns("Transaction_Remark").SetOrdinal(3)
    '                'objtbl.Columns("IDR_Amount").SetOrdinal(4)
    '                'objtbl.Columns("country_code").SetOrdinal(5)
    '                'objtbl.Columns("country_code_lawan").SetOrdinal(6)

    '                'objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
    '                'objtbl.Columns("Account_NO").ColumnName = "Account No"
    '                'objtbl.Columns("Transmode_Code").ColumnName = "Transmode Code"
    '                'objtbl.Columns("Transaction_Remark").ColumnName = "Transaction Remark"
    '                'objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
    '                'objtbl.Columns("country_code").ColumnName = "Country Code"
    '                'objtbl.Columns("country_code_lawan").ColumnName = "Opp. Country Code"

    '                '29-Mar-2022 Adi : Perubahan skema informasi yang ditampilkan
    '                objtbl.Columns("Date_Transaction").SetOrdinal(0)
    '                objtbl.Columns("Ref_Num").SetOrdinal(1)
    '                objtbl.Columns("Transmode_Code").SetOrdinal(2)
    '                objtbl.Columns("Transaction_Location").SetOrdinal(3)
    '                objtbl.Columns("Debit_Credit").SetOrdinal(4)
    '                objtbl.Columns("Currency").SetOrdinal(5)
    '                objtbl.Columns("Original_Amount").SetOrdinal(6)
    '                objtbl.Columns("IDR_Amount").SetOrdinal(7)
    '                objtbl.Columns("Transaction_Remark").SetOrdinal(8)
    '                objtbl.Columns("Account_NO").SetOrdinal(9)
    '                objtbl.Columns("CounterPartyType").SetOrdinal(10)
    '                objtbl.Columns("Account_No_Lawan").SetOrdinal(11)
    '                objtbl.Columns("CIF_No_Lawan").SetOrdinal(12)
    '                objtbl.Columns("WIC_No_Lawan").SetOrdinal(13)
    '                objtbl.Columns("CounterPartyName").SetOrdinal(14)
    '                objtbl.Columns("Country_Lawan").SetOrdinal(15)

    '                objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
    '                objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
    '                objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
    '                objtbl.Columns("Transaction_Location").ColumnName = "Location"
    '                objtbl.Columns("Debit_Credit").ColumnName = "D/C"
    '                objtbl.Columns("Currency").ColumnName = "CCY"
    '                objtbl.Columns("Original_Amount").ColumnName = "Amount"
    '                objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
    '                objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
    '                objtbl.Columns("Account_NO").ColumnName = "Account No."
    '                objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
    '                objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
    '                objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
    '                objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
    '                objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
    '                objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"

    '                ws.Cells("A8").LoadFromDataTable(objtbl, True)

    '                Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
    '                Dim intcolnumber As Integer = 1
    '                For Each item As System.Data.DataColumn In objtbl.Columns
    '                    If item.DataType = GetType(Date) Then
    '                        ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
    '                    End If
    '                    intcolnumber = intcolnumber + 1
    '                Next
    '                ws.Cells(ws.Dimension.Address).AutoFitColumns()
    '                resource.Save()
    '                Response.Clear()
    '                Response.ClearHeaders()
    '                Response.ContentType = "application/vnd.ms-excel"
    '                Response.AddHeader("content-disposition", "attachment;filename=List of Transaction (CIF No " & drCM("CIF_No") & ").xlsx")
    '                Response.Charset = ""
    '                Response.AddHeader("cache-control", "max-age=0")
    '                Me.EnableViewState = False
    '                Response.ContentType = "ContentType"
    '                Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
    '                Response.End()
    '            End Using
    '        End Using

    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
#End Region


    Protected Sub Load_CaseAlert_Other(strCaseID As String)
        Try
            Dim strSQL As String = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & strCaseID
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

            Dim strQuery As String = ""
            If drCM IsNot Nothing Then
                strQuery = "SELECT * FROM vw_OneFCC_CaseManagement_For_WIC"
                strQuery += " WHERE PK_CaseManagement_ID <> " & strCaseID
                strQuery += " AND WIC_No = '" & Me.WICNo & "'"

                If Not IsDBNull(drCM("Alert_Type")) Then
                    strQuery += " AND Alert_Type = '" & drCM("Alert_Type") & "'"
                End If

                If Not IsDBNull(drCM("FK_Rule_Basic_ID")) Then
                    strQuery += " AND FK_Rule_Basic_ID = '" & drCM("FK_Rule_Basic_ID") & "'"
                End If

            End If

            Dim dtCaseAlert_Other As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If dtCaseAlert_Other Is Nothing Then
                dtCaseAlert_Other = New DataTable
            End If

            If dtCaseAlert_Other IsNot Nothing Then

                'Bind to gridpanel
                gp_OtherCase.GetStore.DataSource = dtCaseAlert_Other
                gp_OtherCase.GetStore.DataBind()

            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_OtherCase(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            If strCommandName = "Detail" Then
                LoadDataOtherCase(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDataOtherCase(strCaseID As String)
        Try
            'To Do Load Data Other Case
            Dim strSQL As String = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & strCaseID
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            If drCM IsNot Nothing Then

                Me.CaseID_OtherCase = drCM("PK_CaseManagement_ID")

                'General Information
                txt_WindowOther_PK_CaseManagement_ID.Value = drCM("PK_CaseManagement_ID")
                txt_WindowOther_Case_Description.Value = drCM("Case_Description")
                txt_WindowOther_Alert_Type.Value = drCM("Alert_Type")

                If Not IsDBNull(drCM("FK_CaseStatus_ID")) Then
                    strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_ProposedAction WHERE PK_OneFCC_CaseManagement_ProposedAction_ID=" & drCM("FK_CaseStatus_ID")
                    Dim drTemp As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                    If drTemp IsNot Nothing Then
                        txt_WindowOther_FK_Case_Status_ID.Value = drTemp("PK_OneFCC_CaseManagement_ProposedAction_ID") & " - " & drTemp("Proposed_Action")
                    Else
                        txt_WindowOther_FK_Case_Status_ID.Value = "New Case"
                    End If
                Else
                    txt_WindowOther_FK_Case_Status_ID.Value = "New Case"
                End If

                If Not IsDBNull(drCM("CreatedDate")) Then
                    txt_WindowOther_CreatedDate.Value = CDate(drCM("CreatedDate")).ToString("dd-MMM-yyyy HH:mm:ss")
                End If

                txt_WindowOther_Workflow_Step.Value = drCM("Workflow_Step_Of")
                txt_WindowOther_PIC.Value = drCM("PIC")

                'Reload GP Transaction
                gp_CaseAlert_Typology_Transaction_OtherCase.Hidden = True
                pnl_CaseAlert_Outlier_Transaction_OtherCase.Hidden = True

                'Load Transaction
                If Not IsDBNull(drCM("Alert_Type")) Then
                    If drCM("Alert_Type") = "Typology Risk" Then
                        gp_CaseAlert_Typology_Transaction_OtherCase.GetStore.Reload()
                        gp_CaseAlert_Typology_Transaction_OtherCase.Hidden = False
                    ElseIf drCM("Alert_Type") = "Financial Risk" Then
                        gp_CaseAlert_Outlier_Transaction_OtherCase.GetStore.Reload()
                        pnl_CaseAlert_Outlier_Transaction_OtherCase.Hidden = False

                        'Display Mean dan Modus
                        strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Parameter WHERE PK_GlobalReportParameter_ID=2"
                        Dim drParam As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                        If drParam IsNot Nothing AndAlso Not IsDBNull(drParam("ParameterValue")) Then
                            fs_CaseAlert_Outlier_Transaction_OtherCase.Title = "<b>Financial Statistics in past " & drParam("ParameterValue") & " month(s)</b>"
                        End If

                        strSQL = "SELECT TOP 1 * FROM OneFCC_CaseManagement_Outlier WHERE FK_CaseManagement_ID=" & drCM("PK_CaseManagement_ID")
                        Dim drCMO As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
                        If drCMO IsNot Nothing Then
                            If Not IsDBNull(drCMO("Mean_Debit")) Then
                                txt_MeanDebit.Value = CDbl(drCMO("Mean_Debit")).ToString("#,##0.00")
                            End If
                            If Not IsDBNull(drCMO("Mean_Credit")) Then
                                txt_MeanCredit.Value = CDbl(drCMO("Mean_Credit")).ToString("#,##0.00")
                            End If
                            If Not IsDBNull(drCMO("Modus_Debit")) Then
                                txt_ModusDebit.Value = CDbl(drCMO("Modus_Debit")).ToString("#,##0.00")
                            End If
                            If Not IsDBNull(drCMO("Modus_Credit")) Then
                                txt_ModusCredit.Value = CDbl(drCMO("Modus_Credit")).ToString("#,##0.00")
                            End If
                        End If
                    End If
                End If

                'Load Workflow History
                gp_WorkflowHistory_OtherCase.GetStore.Reload()

                'Reload Gridpanel Transaction
                'gp_CaseAlert_Transaction_OtherCase.GetStore.Reload()

            End If

            'Show Window
            window_OtherCase.Hidden = False

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_OtherCase_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            window_OtherCase.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Outlier_Transaction_OtherCase(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Outlier AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_OtherCase

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Outlier_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Outlier AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Outlier_ID = ofcmt.PK_OneFCC_CaseManagement_Outlier_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_OtherCase

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " AND " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Outlier_Transaction_OtherCase.GetStore.DataSource = DataPaging
            gp_CaseAlert_Outlier_Transaction_OtherCase.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_CaseAlert_Typology_Transaction_OtherCase(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            '28-Mar-2022 Adi : Penyesuaian column yang ditampilkan
            'Dim strQuery As String = "SELECT ofcmtt.* FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery += " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery += " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_OtherCase

            Dim strQuery As String = "SELECT ofcmtt.*"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN CIF_No_Lawan IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            'strQuery &= " ,CASE WHEN Debit_Credit='D' THEN ISNULL(ctry1.Keterangan,'N/A') + ' to ' + ISNULL(ctry2.Keterangan,'N/A') ELSE ISNULL(ctry2.Keterangan,'N/A') + ' to ' + ISNULL(ctry1.Keterangan,'N/A') END AS Country"
            strQuery &= " ,ISNULL(ctry.Keterangan,'N/A') AS Country_Lawan"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmtt.CIF_No_Lawan = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmtt.WIC_No_Lawan = garw.WIC_No"
            strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & CaseID_OtherCase

            If Not String.IsNullOrEmpty(strfilter) Then
                strQuery += " AND " & strfilter
            End If

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_CaseAlert_Typology_Transaction_OtherCase.GetStore.DataSource = DataPaging
            gp_CaseAlert_Typology_Transaction_OtherCase.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub store_ReadData_WorkflowHistory_OtherCase(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort &= item.Property & " " & item.Direction.ToString
            Next

            Dim strQuery As String = "SELECT history.*, proposed.Proposed_Action FROM onefcc_casemanagement_workflowhistory history"
            strQuery &= " LEFT JOIN OneFCC_CaseManagement_ProposedAction proposed ON history.FK_Proposed_Status_ID = proposed.PK_OneFCC_CaseManagement_ProposedAction_ID"
            strQuery &= " WHERE FK_CaseManagement_ID = " & Me.CaseID_OtherCase

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)

            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            gp_WorkflowHistory_OtherCase.GetStore.DataSource = DataPaging
            gp_WorkflowHistory_OtherCase.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


#Region "Update 24-Mar-2022 Adi"
    Protected Sub btn_WICDetail_Click()
        Try
            LoadWICDetail()

            window_WICDetail.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_WICDetail_Back_Click()
        Try
            window_WICDetail.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_CaseAlertDetail_Back_Click()
        Try
            window_CaseAlertDetail.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadWICDetail()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM AML_COUNTRY"
            Dim dtCountry As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Get Customer Information
            txt_WICDetail_WICNo.Value = txt_WIC_No.Value
            txt_WICDetail_Name.Value = txt_WIC_Name.Value

            strQuery = "SELECT TOP 1 * FROM goaml_ref_wic WHERE WIC_No='" & Me.WICNo & "'"
            Dim drWIC As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If drWIC IsNot Nothing Then
                txt_WICDetail_TypeCode.Value = drWIC("FK_Customer_Type_ID")
                txt_WICDetail_POB.Value = drWIC("INDV_Birth_Place")
                If Not IsDBNull(drWIC("INDV_BirthDate")) Then
                    txt_WICDetail_DOB.Value = CDate(drWIC("INDV_BirthDate")).ToString("dd-MMM-yyyy")
                ElseIf Not IsDBNull(drWIC("Corp_Incorporation_Date")) Then
                    txt_WICDetail_DOB.Value = CDate(drWIC("Corp_Incorporation_Date")).ToString("dd-MMM-yyyy")
                End If

                If dtCountry IsNot Nothing AndAlso Not IsDBNull(drWIC("INDV_Nationality1")) Then
                    Dim drCek = dtCountry.Select("FK_AML_COUNTRY_CODE='" & drWIC("INDV_Nationality1") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        txt_WICDetail_Nationality.Value = drWIC("INDV_Nationality1") & " - " & drCek("AML_COUNTRY_Name")
                    End If
                End If

                If Not IsDBNull(drWIC("INDV_Occupation")) Then
                    txt_WICDetail_Occupation.Value = drWIC("INDV_Occupation")
                End If

                txt_WICDetail_Employer.Value = drWIC("INDV_Employer_Name")

                txt_WICDetail_LegalForm.Value = drWIC("Corp_Incorporation_Legal_Form")
                If Not IsDBNull(drWIC("Corp_Incorporation_Legal_Form")) Then
                    strQuery = "SELECT TOP 1 * FROM goAML_Ref_Bentuk_Badan_Usaha WHERE Kode='" & drWIC("Corp_Incorporation_Legal_Form") & "'"
                    Dim drIndustry As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                    If drIndustry IsNot Nothing Then
                        txt_WICDetail_Occupation.Value = drWIC("Corp_Incorporation_Legal_Form") & " - " & drIndustry("Keterangan")
                    Else
                        txt_WICDetail_Occupation.Value = drWIC("Corp_Incorporation_Legal_Form")
                    End If
                End If

                If Not IsDBNull(drWIC("FK_Customer_Type_ID")) Then
                    If drWIC("FK_Customer_Type_ID") = "1" Then
                        txt_WICDetail_NPWP.Value = drWIC("INDV_Tax_Number")
                    ElseIf drWIC("FK_Customer_Type_ID") = "2" Then
                        txt_WICDetail_NPWP.Value = drWIC("Corp_Tax_Number")
                    End If
                End If


                'txt_WICDetail_PurposeOfFund.Value = drWIC("TUJUAN_DANA")
                txt_WICDetail_SourceOfFund.Value = drWIC("INDV_SumberDana")
            End If

            'Binding Address
            BindWICAddress()

            'Binding Contact/Phone
            BindWICContact()

            'Binding Identity
            BindWICIdentity()

            'LoadDataAccounts(Me.WICNo) '' WIC tidak ada Account

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindWICAddress()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM goAML_Ref_Nama_Negara"
            Dim dtCountry As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            strQuery = "SELECT * FROM goAML_Ref_Kategori_Kontak"
            Dim dtAddressType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding Address
            strQuery = "SELECT a.* FROM goAML_Ref_Address a inner join goaml_ref_wic b on a.FK_To_Table_ID = b.PK_Customer_ID and a.FK_Ref_Detail_Of = '3' WHERE WIC_NO='" & Me.WICNo & "'"
            Dim dtAddress As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtAddress Is Nothing Then
                dtAddress = New DataTable
            End If
            dtAddress.Columns.Add(New DataColumn("KATEGORI_KONTAK_KETERANGAN", GetType(String)))
            dtAddress.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))

            For Each item In dtAddress.Rows
                item("KATEGORI_KONTAK_KETERANGAN") = item("Address_Type")
                If dtAddressType IsNot Nothing AndAlso Not IsDBNull(item("Address_Type")) Then
                    Dim drCek = dtAddressType.Select("Kode='" & item("Address_Type") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("KATEGORI_KONTAK_KETERANGAN") = item("Address_Type") & " - " & drCek("Keterangan")
                    End If
                End If

                item("COUNTRY_NAME") = item("Country_Code")
                If dtCountry IsNot Nothing AndAlso Not IsDBNull(item("Country_Code")) Then
                    Dim drCek = dtCountry.Select("Kode='" & item("Country_Code") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("COUNTRY_NAME") = item("Country_Code") & " - " & drCek("Keterangan")
                    End If
                End If
            Next

            gridAddressInfo.GetStore.DataSource = dtAddress
            gridAddressInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindWICContact()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM goAML_Ref_Jenis_Alat_Komunikasi"
            Dim dtContactType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding Contact
            strQuery = "SELECT a.* FROM goAML_Ref_Phone a inner join goaml_ref_wic b on a.FK_for_Table_ID = b.PK_Customer_ID and a.FK_Ref_Detail_Of = '3' WHERE WIC_NO='" & Me.WICNo & "'"
            Dim dtContact As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtContact Is Nothing Then
                dtContact = New DataTable
            End If
            dtContact.Columns.Add(New DataColumn("JENIS_ALAT_KOMUNIKASI_KETERANGAN", GetType(String)))

            For Each item In dtContact.Rows
                item("JENIS_ALAT_KOMUNIKASI_KETERANGAN") = item("Tph_Communication_Type")
                If dtContactType IsNot Nothing AndAlso Not IsDBNull(item("Tph_Communication_Type")) Then
                    Dim drCek = dtContactType.Select("Kode='" & item("Tph_Communication_Type") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("JENIS_ALAT_KOMUNIKASI_KETERANGAN") = item("Tph_Communication_Type") & " - " & drCek("Keterangan")
                    End If
                End If
            Next

            gridPhoneInfo.GetStore.DataSource = dtContact
            gridPhoneInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindWICIdentity()
        Try
            Dim strQuery As String = ""

            'Reference Table
            strQuery = "SELECT * FROM goAML_Ref_Jenis_Dokumen_Identitas"
            Dim dtIDENTITYType As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            strQuery = "SELECT * FROM goAML_Ref_Nama_Negara"
            Dim dtCountry As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            'Binding IDENTITY
            strQuery = "SELECT a.* FROM goAML_Person_Identification a inner join goaml_ref_wic b on a.FK_Person_ID = b.PK_Customer_ID and a.FK_Person_Type = '8' WHERE WIC_NO='" & Me.WICNo & "'"
            Dim dtIDENTITY As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtIDENTITY Is Nothing Then
                dtIDENTITY = New DataTable
            End If
            dtIDENTITY.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
            dtIDENTITY.Columns.Add(New DataColumn("IDENTITY_TYPE_NAME", GetType(String)))

            For Each item In dtIDENTITY.Rows
                item("IDENTITY_TYPE_NAME") = item("Type")
                If dtIDENTITYType IsNot Nothing AndAlso Not IsDBNull(item("Type")) Then
                    Dim drCek = dtIDENTITYType.Select("Kode='" & item("Type") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("IDENTITY_TYPE_NAME") = item("Type") & " - " & drCek("Keterangan")
                    End If
                End If
                item("COUNTRY_NAME") = item("Issued_Country")
                If dtCountry IsNot Nothing AndAlso Not IsDBNull(item("Issued_Country")) Then
                    Dim drCek = dtCountry.Select("Kode='" & item("Issued_Country") & "'").FirstOrDefault
                    If drCek IsNot Nothing Then
                        item("COUNTRY_NAME") = item("Issued_Country") & " - " & drCek("Keterangan")
                    End If
                End If
            Next

            gridIdentityInfo.GetStore.DataSource = dtIDENTITY
            gridIdentityInfo.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "20-Apr-2022 Adi : Alerts grouped by ProcessDate and CIF_No"
    'Protected Sub store_ReadData_CaseAlert_Transaction(sender As Object, e As StoreReadDataEventArgs)
    '    Try
    '        Dim intStart As Integer = e.Start
    '        Dim intLimit As Int16 = e.Limit
    '        Dim inttotalRecord As Integer
    '        Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

    '        intLimit = 10

    '        'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

    '        Dim strsort As String = ""
    '        For Each item As DataSorter In e.Sort
    '            strsort += item.Property & " " & item.Direction.ToString
    '        Next

    '        Dim strQuery As String = "SELECT * FROM vw_OneFCC_CaseManagement_Alert"
    '        strQuery &= " WHERE FK_CaseManagement_ID = " & IDUnik

    '        If Not String.IsNullOrEmpty(strfilter) Then
    '            strQuery += " And " & strfilter
    '        End If

    '        Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


    '        ''-- start paging ------------------------------------------------------------
    '        Dim limit As Integer = e.Limit
    '        If (e.Start + e.Limit) > inttotalRecord Then
    '            limit = inttotalRecord - e.Start
    '        End If
    '        'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
    '        ''-- end paging ------------------------------------------------------------
    '        e.Total = inttotalRecord
    '        gp_CaseAlert_Transaction.GetStore.DataSource = DataPaging
    '        gp_CaseAlert_Transaction.GetStore.DataBind()

    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub ExportAll_CaseAlert_Transaction(sender As Object, e As EventArgs)
    '    Dim objfileinfo As IO.FileInfo
    '    Try

    '        'Get Data Transaction
    '        'Dim strQuery As String = "SELECT ofcmtt.Date_Transaction, ofcmtt.Account_NO, ofcmtt.Transmode_Code, ofcmtt.Transaction_Remark, ofcmtt.IDR_Amount,"
    '        'strQuery &= " ofcmtt.country_code, ofcmtt.country_code_lawan"
    '        'strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
    '        'strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
    '        'strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

    '        '29-Mar-2022 Adi
    '        Dim strQuery As String = "SELECT Case_Description"
    '        strQuery &= " ,Date_Transaction"
    '        strQuery &= " ,Ref_Num"
    '        strQuery &= " ,Transmode_Code"
    '        strQuery &= " ,Transaction_Location"
    '        strQuery &= " ,Debit_Credit"
    '        strQuery &= " ,Currency"
    '        strQuery &= " ,Original_Amount"
    '        strQuery &= " ,IDR_Amount"
    '        strQuery &= " ,Transaction_Remark"
    '        strQuery &= " ,Account_NO"
    '        strQuery &= " ,CounterPartyType"
    '        strQuery &= " ,Account_No_Lawan"
    '        strQuery &= " ,CIF_No_Lawan"
    '        strQuery &= " ,WIC_No_Lawan"
    '        strQuery &= " ,CounterPartyName"
    '        strQuery &= " ,Country_Lawan"
    '        strQuery &= " FROM vw_OneFCC_CaseManagement_Alert"
    '        strQuery &= " WHERE FK_CaseManagement_ID = " & IDUnik
    '        'strQuery &= " ORDER BY Account_No, Case_Description"

    '        Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
    '        If dtTransactionAll.Rows.Count < 1 Then
    '            Throw New Exception("Data Kosong!")
    '        ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
    '            Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
    '        End If

    '        'Get Data CM
    '        strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement WHERE PK_CaseManagement_ID=" & IDUnik
    '        Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

    '        Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
    '        objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
    '        Using objtbl As Data.DataTable = dtTransactionAll

    '            'objFormModuleView.changeHeader(objtbl)
    '            For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Typology_Transaction.ColumnModel.Columns

    '                If item.Hidden Then
    '                    If objtbl.Columns.Contains(item.DataIndex) Then
    '                        objtbl.Columns.Remove(item.DataIndex)
    '                    End If

    '                End If
    '            Next

    '            Using resource As New ExcelPackage(objfileinfo)
    '                Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CaseManagementTypologyTrx")

    '                'Display Header of CM
    '                If drCM IsNot Nothing Then
    '                    ws.Cells("A1").Value = "Case Management ID"
    '                    ws.Cells("B1").Value = drCM("PK_CaseManagement_ID").ToString

    '                    ws.Cells("A2").Value = "CIF"
    '                    ws.Cells("B2").Value = drCM("CIF_No")

    '                    ws.Cells("A3").Value = "Customer Name"
    '                    ws.Cells("B3").Value = drCM("Customer_Name")

    '                    ws.Cells("A4").Value = "Process Date"
    '                    ws.Cells("B4").Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
    '                End If

    '                objtbl.Columns("Case_Description").SetOrdinal(0)
    '                objtbl.Columns("Date_Transaction").SetOrdinal(1)
    '                objtbl.Columns("Ref_Num").SetOrdinal(2)
    '                objtbl.Columns("Transmode_Code").SetOrdinal(3)
    '                objtbl.Columns("Transaction_Location").SetOrdinal(4)
    '                objtbl.Columns("Debit_Credit").SetOrdinal(5)
    '                objtbl.Columns("Currency").SetOrdinal(6)
    '                objtbl.Columns("Original_Amount").SetOrdinal(7)
    '                objtbl.Columns("IDR_Amount").SetOrdinal(8)
    '                objtbl.Columns("Transaction_Remark").SetOrdinal(9)
    '                objtbl.Columns("Account_NO").SetOrdinal(10)
    '                objtbl.Columns("CounterPartyType").SetOrdinal(11)
    '                objtbl.Columns("Account_No_Lawan").SetOrdinal(12)
    '                objtbl.Columns("CIF_No_Lawan").SetOrdinal(13)
    '                objtbl.Columns("WIC_No_Lawan").SetOrdinal(14)
    '                objtbl.Columns("CounterPartyName").SetOrdinal(15)
    '                objtbl.Columns("Country_Lawan").SetOrdinal(16)

    '                objtbl.Columns("Case_Description").ColumnName = "Case Description"
    '                objtbl.Columns("Date_Transaction").ColumnName = "Trx Date"
    '                objtbl.Columns("Ref_Num").ColumnName = "Ref Number"
    '                objtbl.Columns("Transmode_Code").ColumnName = "Trx Code"
    '                objtbl.Columns("Transaction_Location").ColumnName = "Location"
    '                objtbl.Columns("Debit_Credit").ColumnName = "D/C"
    '                objtbl.Columns("Currency").ColumnName = "CCY"
    '                objtbl.Columns("Original_Amount").ColumnName = "Amount"
    '                objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
    '                objtbl.Columns("Transaction_Remark").ColumnName = "Remark"
    '                objtbl.Columns("Account_NO").ColumnName = "Account No."
    '                objtbl.Columns("CounterPartyType").ColumnName = "Tipe Pihak Lawan"
    '                objtbl.Columns("Account_No_Lawan").ColumnName = "Account No. Lawan"
    '                objtbl.Columns("CIF_No_Lawan").ColumnName = "CIF Lawan"
    '                objtbl.Columns("WIC_No_Lawan").ColumnName = "WIC Lawan"
    '                objtbl.Columns("CounterPartyName").ColumnName = "Nama Pihak Lawan"
    '                objtbl.Columns("Country_Lawan").ColumnName = "Country Lawan"

    '                ws.Cells("A6").LoadFromDataTable(objtbl, True)

    '                Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
    '                Dim intcolnumber As Integer = 1
    '                For Each item As System.Data.DataColumn In objtbl.Columns
    '                    If item.DataType = GetType(Date) Then
    '                        ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
    '                    End If
    '                    intcolnumber = intcolnumber + 1
    '                Next
    '                ws.Cells(ws.Dimension.Address).AutoFitColumns()
    '                resource.Save()
    '                Response.Clear()
    '                Response.ClearHeaders()
    '                Response.ContentType = "application/vnd.ms-excel"
    '                Response.AddHeader("content-disposition", "attachment;filename=Case Alert Transaction (Case ID " & drCM("PK_CaseManagement_ID") & ").xlsx")
    '                Response.Charset = ""
    '                Response.AddHeader("cache-control", "max-age=0")
    '                Me.EnableViewState = False
    '                Response.ContentType = "ContentType"
    '                Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
    '                Response.End()
    '            End Using
    '        End Using

    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub store_ReadData_CaseAlert_Transaction_OtherCase(sender As Object, e As StoreReadDataEventArgs)
    '    Try
    '        Dim intStart As Integer = e.Start
    '        Dim intLimit As Int16 = e.Limit
    '        Dim inttotalRecord As Integer
    '        Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

    '        intLimit = 10

    '        'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

    '        Dim strsort As String = ""
    '        For Each item As DataSorter In e.Sort
    '            strsort += item.Property & " " & item.Direction.ToString
    '        Next

    '        Dim strQuery As String = "SELECT * FROM vw_OneFCC_CaseManagement_Alert"
    '        strQuery &= " WHERE FK_CaseManagement_ID = " & Me.CaseID_OtherCase

    '        If Not String.IsNullOrEmpty(strfilter) Then
    '            strQuery += " And " & strfilter
    '        End If

    '        Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuery, strsort, intStart, intLimit, inttotalRecord)


    '        ''-- start paging ------------------------------------------------------------
    '        Dim limit As Integer = e.Limit
    '        If (e.Start + e.Limit) > inttotalRecord Then
    '            limit = inttotalRecord - e.Start
    '        End If
    '        'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
    '        ''-- end paging ------------------------------------------------------------
    '        e.Total = inttotalRecord
    '        gp_CaseAlert_Transaction_OtherCase.GetStore.DataSource = DataPaging
    '        gp_CaseAlert_Transaction_OtherCase.GetStore.DataBind()

    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

#End Region

#Region "11-Nov-2022 Felix : Tambah Activity"

    '' Add 11-Nov-2022 
    Protected Sub BindTypologyNonTransaction(strCaseID As String)
        Try

            Dim strQuery As String = "Select ofcmtt.*"
            strQuery &= " ,Case When ofcmt.CIF_No Is Not NULL Then 'Customer' ELSE 'WIC' END AS CounterPartyType"
            strQuery &= " ,CASE WHEN ofcmt.CIF_No IS NOT NULL THEN IIF(garc.FK_Customer_Type_ID=1, garc.INDV_Last_Name, garc.Corp_Name) ELSE IIF(garw.FK_Customer_Type_ID=1, garw.INDV_Last_Name, garw.Corp_Name) END AS CounterPartyName"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_NonTransaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " LEFT JOIN goAML_Ref_Customer AS garc"
            strQuery &= " ON ofcmt.CIF_No = garc.CIF"
            strQuery &= " LEFT JOIN goAML_Ref_WIC AS garw"
            strQuery &= " ON ofcmt.WIC_No = garw.WIC_No"
            'strQuery &= " LEFT JOIN goAML_Ref_Nama_Negara AS ctry"
            'strQuery &= " ON ofcmtt.country_code_lawan = ctry.Kode"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & strCaseID

            Dim dtTypologyNonTransaction As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTypologyNonTransaction Is Nothing Then
                dtTypologyNonTransaction = New DataTable
            End If

            gp_CaseAlert_Typology_NonTransaction.GetStore.DataSource = dtTypologyNonTransaction
            gp_CaseAlert_Typology_NonTransaction.GetStore.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ExportAll_CaseAlert_Typology_NonTrn(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try

            'Get Data Transaction
            'Dim strQuery As String = "SELECT ofcmtt.Date_Transaction, ofcmtt.Account_NO, ofcmtt.Transmode_Code, ofcmtt.Transaction_Remark, ofcmtt.IDR_Amount,"
            'strQuery &= " ofcmtt.country_code, ofcmtt.country_code_lawan"
            'strQuery &= " FROM OneFCC_CaseManagement_Typology_Transaction AS ofcmtt"
            'strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            'strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = " & IDUnik

            '29-Mar-2022 Adi
            Dim strQuery As String = "SELECT ofcmtt.Account_NO"
            strQuery &= " ,ofcmtt.Date_Activity"
            strQuery &= " ,ofcmtt.Activity_Description"
            strQuery &= " FROM OneFCC_CaseManagement_Typology_NonTransaction AS ofcmtt"
            strQuery &= " JOIN OneFCC_CaseManagement_Typology AS ofcmt"
            strQuery &= " ON ofcmtt.FK_OneFCC_CaseManagement_Typology_ID = ofcmt.PK_OneFCC_CaseManagement_Typology_ID"
            strQuery &= " WHERE ofcmt.FK_CaseManagement_ID = '" & CaseID_WindowCaseAlertDetail & "'"

            Dim dtTransactionAll As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If dtTransactionAll.Rows.Count < 1 Then
                Throw New Exception("Data Kosong!")
            ElseIf dtTransactionAll.Rows.Count > 1048550 Then   'Max xlsx rows 1,048,576
                Throw New Exception("Total Data Rows " & dtTransactionAll.Rows.Count & " exceed the capacity of Excel file.")
            End If

            'Get Data CM
            strQuery = "SELECT TOP 1 * FROM vw_OneFCC_CaseManagement_For_WIC WHERE PK_CaseManagement_ID=" & CaseID_WindowCaseAlertDetail
            Dim drCM As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            Using objtbl As Data.DataTable = dtTransactionAll

                'objFormModuleView.changeHeader(objtbl)
                For Each item As Ext.Net.ColumnBase In gp_CaseAlert_Typology_Transaction.ColumnModel.Columns

                    If item.Hidden Then
                        If objtbl.Columns.Contains(item.DataIndex) Then
                            objtbl.Columns.Remove(item.DataIndex)
                        End If

                    End If
                Next

                Using resource As New ExcelPackage(objfileinfo)
                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CaseManagementTypologyTrx")

                    'Display Header of CM
                    If drCM IsNot Nothing Then
                        ws.Cells("A1").Value = "Case Management ID"
                        ws.Cells("B1").Value = drCM("PK_CaseManagement_ID").ToString

                        ws.Cells("A2").Value = "CIF"
                        ws.Cells("B2").Value = drCM("CIF_No")

                        ws.Cells("A3").Value = "Customer Name"
                        ws.Cells("B3").Value = drCM("Customer_Name")

                        ws.Cells("A4").Value = "Alert Type"
                        ws.Cells("B4").Value = drCM("Alert_Type")

                        ws.Cells("A5").Value = "Case Description"
                        ws.Cells("B5").Value = drCM("Case_Description")

                        ws.Cells("A6").Value = "Process Date"
                        ws.Cells("B6").Value = CDate(drCM("ProcessDate")).ToString("dd-MMM-yyyy")
                    End If

                    'objtbl.Columns("Date_Transaction").SetOrdinal(0)
                    'objtbl.Columns("Account_NO").SetOrdinal(1)
                    'objtbl.Columns("Transmode_Code").SetOrdinal(2)
                    'objtbl.Columns("Transaction_Remark").SetOrdinal(3)
                    'objtbl.Columns("IDR_Amount").SetOrdinal(4)
                    'objtbl.Columns("country_code").SetOrdinal(5)
                    'objtbl.Columns("country_code_lawan").SetOrdinal(6)

                    'objtbl.Columns("Date_Transaction").ColumnName = "Transaction Date"
                    'objtbl.Columns("Account_NO").ColumnName = "Account No"
                    'objtbl.Columns("Transmode_Code").ColumnName = "Transmode Code"
                    'objtbl.Columns("Transaction_Remark").ColumnName = "Transaction Remark"
                    'objtbl.Columns("IDR_Amount").ColumnName = "IDR Amount"
                    'objtbl.Columns("country_code").ColumnName = "Country Code"
                    'objtbl.Columns("country_code_lawan").ColumnName = "Opp. Country Code"

                    '29-Mar-2022 Adi : Perubahan skema informasi yang ditampilkan
                    objtbl.Columns("Account_NO").SetOrdinal(0)
                    objtbl.Columns("Date_Activity").SetOrdinal(1)
                    objtbl.Columns("Activity_Description").SetOrdinal(2)

                    objtbl.Columns("Account_NO").ColumnName = "Account No"
                    objtbl.Columns("Date_Activity").ColumnName = "Act Date"
                    objtbl.Columns("Activity_Description").ColumnName = "Description"

                    ws.Cells("A8").LoadFromDataTable(objtbl, True)

                    Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    Dim intcolnumber As Integer = 1
                    For Each item As System.Data.DataColumn In objtbl.Columns
                        If item.DataType = GetType(Date) Then
                            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                        End If
                        intcolnumber = intcolnumber + 1
                    Next
                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    resource.Save()
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=Case Management Typology Transaction (Case ID " & CaseID_WindowCaseAlertDetail & ").xlsx")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "ContentType"
                    Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            End Using

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '' End 11-Nov-2022
#End Region
End Class