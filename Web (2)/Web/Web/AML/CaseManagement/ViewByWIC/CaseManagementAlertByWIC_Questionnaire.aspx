﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CaseManagementAlertByWIC_Questionnaire.aspx.vb" Inherits="AML_CaseManagement_ViewByWIC_CaseManagementAlertByWIC_Questionnaire" %>

<%--Begin Update Penambahan Advance Filter--%>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>
<%--End Update Penambahan Advance Filter--%>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        var onKeyUp = function (combo, e) {
            var v = combo.getRawValue();
            combo.store.filter(combo.displayField, new RegExp(v, "i"));
            combo.onLoad();
        };

        var onInitialize = function (editor) {

            var styles = {
                "background-image": "none",
                "background-color": "#FFE4C4"
            };
            Ext.DomHelper.applyStyles(editor.getEditorBody(), styles);
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
      <style type="text/css">

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
             <ext:FormPanel ID="FormPanelQuestionnaire" BodyPadding="10" runat="server" ClientIDMode="Static" Layout="AnchorLayout" Border="true" ButtonAlign="Center" AutoScroll="true" Collapsible="true" MarginSpec="0 0 10 0"  MinHeight="200" ColumnWidth="1" >
                  <TopBar>
                    <ext:Toolbar runat="server">
                        <Items>                
                        <ext:Button runat="server" ID="BtnExportQuestionnaireCaseAlert_PerWIC" Text="Export to Excel" Icon="PageExcel">
                                <DirectEvents>
                                    <Click OnEvent="ExportQuestionnaireCaseAlert_PerWIC" IsUpload="true"/>
                                </DirectEvents>
                            </ext:Button>
                            </Items>
                        </ext:Toolbar>
                        </TopBar>
                 <Items>
                                </Items>

                
           </ext:FormPanel>
      <ext:GridPanel ID="GridQuestionnaire" runat="server" Hidden="true">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                           
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Questionnaire" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model43" IDProperty="PK_CM_QUESTIONAIRE_ANSWER_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CM_QUESTIONAIRE_ANSWER_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="FK_CASEMANAGEMENT_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SEQUENCE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="QUESTION" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ANSWER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                                 <ext:Column ID="Column17" runat="server" DataIndex="FK_CASEMANAGEMENT_ID" Text="CaseID" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column412" runat="server" DataIndex="SEQUENCE" Text="Sequence" MinWidth="200"></ext:Column>
                                
                                <ext:Column ID="Column2" runat="server" DataIndex="QUESTION" Text="Question" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="ANSWER" Text="Answer" MinWidth="150"></ext:Column>
                                  <ext:DateColumn runat="server" Text="Created Date" DataIndex="CreatedDate" CellWrap="true" flex="1"  Format="dd-MMM-yyyy hh:mm:ss"/>
                              

                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                  
             </asp:Content>

