﻿Imports System.Data
Imports System.Data.SqlClient
Imports Elmah

Partial Class AML_EDD_MappingPointsDelete
    Inherits ParentPage

    Public Property IDModule() As String
        Get
            Return Session("AML_EDD_MappingPointsDelete.IDModule")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_MappingPointsDelete.IDModule") = value
        End Set
    End Property

    Public Property FK_ExtType_ID() As EnumExtType
        Get
            Return Session("AML_EDD_MappingPointsDelete.FK_ExtType_ID")
        End Get
        Set(ByVal value As EnumExtType)
            Session("AML_EDD_MappingPointsDelete.FK_ExtType_ID") = value
        End Set
    End Property

    Public Property StringCondition() As String
        Get
            Return Session("AML_EDD_MappingPointsDelete.StringCondition")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_MappingPointsDelete.StringCondition") = value
        End Set
    End Property

    Public Property StringDateFormat() As String
        Get
            Return Session("AML_EDD_MappingPointsDelete.StringDateFormat")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_MappingPointsDelete.StringDateFormat") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("AML_EDD_MappingPointsDelete.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AML_EDD_MappingPointsDelete.IDUnik") = value
        End Set
    End Property

    Enum EnumExtType
        DateField = 1
        DropDownField = 2
        NumberField = 4
        TextField = 5
        Radio = 6
    End Enum

    Private Sub AML_EDD_MappingPointsDelete_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Delete
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As New Integer
            If IDModule IsNot Nothing Then
                intModuleID = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = ObjModule.ModuleLabel & " - DELETE"

                Dim strQuery As String = " SELECT TOP 1 PK_ModuleApproval_ID FROM ModuleApproval "
                strQuery = strQuery & " WHERE ModuleName = '" & ObjModule.ModuleName & "'"
                strQuery = strQuery & " AND ModuleKey = '" & IDUnik & "'"
                strQuery = strQuery & " AND PK_ModuleAction_ID <> 1"
                Dim checkDataInApproval As DataRow = GetDataRowFromWithStringQuery(strQuery)
                If checkDataInApproval IsNot Nothing Then
                    LblConfirmation.Text = "The system detects that there is already an Delete or Update Request in Pending Approval For this Mapping, please Accept or Reject Request in Approval."
                    FormPanelInput.Hidden = True
                    Panelconfirmation.Hidden = False
                Else
                    StringDateFormat = NawaBLL.SystemParameterBLL.GetDateFormat
                    inputConditionDate1.Format = StringDateFormat
                    inputConditionDate2.Format = StringDateFormat
                    LoadSavedData()
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadSavedData()
        Try
            inputPoint1.FieldLabel = "Point"
            inputPoint1.Hidden = True
            inputPoint2.Hidden = True
            inputPoint1.Value = 0
            inputPoint2.Value = 0
            nDSDropDownField_Condition.IsHidden = True
            inputConditionNumberic1.FieldLabel = "Condition"
            inputConditionNumberic1.Hidden = True
            inputConditionNumberic2.Hidden = True
            inputConditionNumberic3.Hidden = True
            inputConditionDate1.FieldLabel = "Condition"
            inputConditionDate1.Hidden = True
            inputConditionDate2.Hidden = True
            inputConditionText.Hidden = True
            nDSDropDownField_ReferenceTable.IsHidden = True
            inputReferenceTableAlias.Hidden = True
            nDSDropDownField_ReferenceFieldKey.IsHidden = True
            labelReferenceFieldScoreInfo.Hidden = True
            nDSDropDownField_ReferenceFieldScore.IsHidden = True
            nDSDropDownField_Condition.SetTextValue("")
            nDSDropDownField_ReferenceFieldKey.StringFilter = " 1=2 "
            nDSDropDownField_ReferenceFieldScore.StringFilter = " 1=2 "

            Dim strQuery As String = " select * from OneFCC_EDD_Mapping_Question_Answer_Point "
            strQuery = strQuery & " where PK_OneFCC_EDD_Mapping_Question_Answer_Point_ID = " & IDUnik
            Dim getDataMappingPoint As DataRow = GetDataRowFromWithStringQuery(strQuery)
            If getDataMappingPoint IsNot Nothing Then
                If Not IsDBNull(getDataMappingPoint("FK_OneFCC_Question_Mapping_EDD_ID")) Then
                    strQuery = " select "
                    strQuery = strQuery & " question.FK_ExtType_ID "
                    strQuery = strQuery & " ,question.Question "
                    strQuery = strQuery & " ,EDDType.EDD_Type_Name AS [EDD Question Type]"
                    strQuery = strQuery & " ,customertype.Description AS [Customer Type]"
                    strQuery = strQuery & " ,customersegment.Description AS [Customer Segment]"
                    strQuery = strQuery & " ,questiongroup.Question_Group_Name AS [Question Group]"
                    strQuery = strQuery & " ,exttype.ExtTypeName AS [Answer Type]"
                    strQuery = strQuery & " from OneFCC_Question_Mapping_EDD mappingedd "
                    strQuery = strQuery & " join OneFCC_Question question "
                    strQuery = strQuery & " on mappingedd.FK_OneFCC_Question_ID = question.PK_OneFCC_Question_ID "
                    strQuery = strQuery & " INNER JOIN MExtType exttype"
                    strQuery = strQuery & " ON question.FK_ExtType_ID = exttype.PK_ExtType_ID"
                    strQuery = strQuery & " INNER JOIN OneFCC_EDD_Type EDDType"
                    strQuery = strQuery & " ON mappingedd.FK_OneFCC_Question_Type_Code = EDDType.EDD_Type_Code"
                    strQuery = strQuery & " INNER JOIN OneFCC_Question_Customer_Type customertype"
                    strQuery = strQuery & " ON mappingedd.FK_OneFCC_Question_Customer_Type_Code = customertype.Customer_Type_Code"
                    strQuery = strQuery & " INNER JOIN OneFCC_Question_Customer_Segment customersegment"
                    strQuery = strQuery & " ON mappingedd.FK_OneFCC_Question_Customer_Segment_Code = customersegment.Customer_Segment_Code"
                    strQuery = strQuery & " INNER JOIN OneFCC_Question_Group questiongroup"
                    strQuery = strQuery & " ON mappingedd.FK_OneFCC_Question_Group_Code = questiongroup.Question_Group_Code"
                    strQuery = strQuery & " where PK_OneFCC_Question_Mapping_EDD_ID = " & getDataMappingPoint("FK_OneFCC_Question_Mapping_EDD_ID")
                    Dim getDataQuestion As DataRow = GetDataRowFromWithStringQuery(strQuery)
                    If getDataQuestion IsNot Nothing Then
                        If Not IsDBNull(getDataQuestion("Question")) Then
                            nDSDropDownField_MappingQuestion.SetTextWithTextValue(getDataMappingPoint("FK_OneFCC_Question_Mapping_EDD_ID"), getDataQuestion("Question"))
                        End If
                        If Not IsDBNull(getDataQuestion("EDD Question Type")) Then
                            displayField_Question_Type.Text = getDataQuestion("EDD Question Type")
                        Else
                            displayField_Question_Type.Text = ""
                        End If
                        If Not IsDBNull(getDataQuestion("Customer Type")) Then
                            displayField_Customer_Type.Text = getDataQuestion("Customer Type")
                        Else
                            displayField_Customer_Type.Text = ""
                        End If
                        If Not IsDBNull(getDataQuestion("Customer Segment")) Then
                            displayField_Customer_Segment.Text = getDataQuestion("Customer Segment")
                        Else
                            displayField_Customer_Segment.Text = ""
                        End If
                        If Not IsDBNull(getDataQuestion("Question Group")) Then
                            displayField_Question_Group.Text = getDataQuestion("Question Group")
                        Else
                            displayField_Question_Group.Text = ""
                        End If
                        If Not IsDBNull(getDataQuestion("Answer Type")) Then
                            displayField_Answer_Type.Text = getDataQuestion("Answer Type")
                        Else
                            displayField_Answer_Type.Text = ""
                        End If
                        If Not IsDBNull(getDataQuestion("FK_ExtType_ID")) Then
                            FK_ExtType_ID = getDataQuestion("FK_ExtType_ID")
                        Else
                            Throw New ApplicationException("Internal Error, System Could not get Question Type")
                        End If
                    Else
                        Throw New ApplicationException("Internal Error, System Could not get Question Data")
                    End If
                    If FK_ExtType_ID <> Nothing Then
                        Select Case FK_ExtType_ID
                            Case EnumExtType.DateField
                                inputPoint1.Hidden = False
                                nDSDropDownField_Condition.IsHidden = False
                                nDSDropDownField_Condition.StringFilter = " FK_AdvanceFilter_ID = 3 "
                                inputConditionDate1.Hidden = False
                                If Not IsDBNull(getDataMappingPoint("Other_Condition")) Then
                                    StringCondition = getDataMappingPoint("Other_Condition")
                                    nDSDropDownField_Condition.SetTextWithTextValue(StringCondition, StringCondition)
                                    If StringCondition = "Between" OrElse StringCondition = "Not Between" Then
                                        inputConditionDate1.FieldLabel = "From Condition"
                                        inputConditionDate2.Hidden = False
                                        If Not IsDBNull(getDataMappingPoint("Other_Value")) Then
                                            Dim tempDate As Date = DateTime.ParseExact(getDataMappingPoint("Other_Value"), "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture)
                                            inputConditionDate1.Value = tempDate
                                        End If
                                        If Not IsDBNull(getDataMappingPoint("Other_Value_Helper")) Then
                                            Dim tempDate As Date = DateTime.ParseExact(getDataMappingPoint("Other_Value_Helper"), "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture)
                                            inputConditionDate2.Value = tempDate
                                        End If
                                    ElseIf StringCondition = "NULL" OrElse StringCondition = "Not NULL" Then
                                        inputConditionDate1.Hidden = True
                                        inputConditionDate2.Hidden = True
                                    Else
                                        inputConditionDate1.FieldLabel = "Condition"
                                        inputConditionDate2.Hidden = True
                                        If Not IsDBNull(getDataMappingPoint("Other_Value")) Then
                                            Dim tempDate As Date = DateTime.ParseExact(getDataMappingPoint("Other_Value"), "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture)
                                            inputConditionDate1.Value = tempDate
                                        End If
                                    End If
                                Else
                                    Throw New ApplicationException("Internal Error, System Could not get Question Condition")
                                End If
                                If Not IsDBNull(getDataMappingPoint("Other_Score_Int")) Then
                                    Dim tempInt As Integer = Integer.Parse(getDataMappingPoint("Other_Score_Int"))
                                    inputPoint1.Value = tempInt
                                Else
                                    Throw New ApplicationException("Internal Error, System Could not get Question Point")
                                End If

                            Case EnumExtType.DropDownField
                                nDSDropDownField_ReferenceTable.IsHidden = False
                                inputReferenceTableAlias.Hidden = False
                                nDSDropDownField_ReferenceFieldKey.IsHidden = False
                                labelReferenceFieldScoreInfo.Hidden = False
                                nDSDropDownField_ReferenceFieldScore.IsHidden = False
                                If Not IsDBNull(getDataMappingPoint("Table_Reference_Name")) Then
                                    nDSDropDownField_ReferenceTable.SetTextWithTextValue(getDataMappingPoint("Table_Reference_Name"), getDataMappingPoint("Table_Reference_Name"))
                                End If
                                If Not IsDBNull(getDataMappingPoint("Table_Reference_Alias")) Then
                                    inputReferenceTableAlias.Value = getDataMappingPoint("Table_Reference_Alias")
                                End If
                                If Not IsDBNull(getDataMappingPoint("Table_Reference_Field_Key")) Then
                                    nDSDropDownField_ReferenceFieldKey.SetTextWithTextValue(getDataMappingPoint("Table_Reference_Field_Key"), getDataMappingPoint("Table_Reference_Field_Key"))
                                End If
                                If Not IsDBNull(getDataMappingPoint("Table_Reference_Field_Score")) Then
                                    nDSDropDownField_ReferenceFieldScore.SetTextWithTextValue(getDataMappingPoint("Table_Reference_Field_Score"), getDataMappingPoint("Table_Reference_Field_Score"))
                                End If

                            Case EnumExtType.TextField
                                inputPoint1.Hidden = False
                                nDSDropDownField_Condition.IsHidden = False
                                nDSDropDownField_Condition.StringFilter = " FK_AdvanceFilter_ID = 1 "
                                inputConditionText.Hidden = False
                                If Not IsDBNull(getDataMappingPoint("Other_Condition")) Then
                                    StringCondition = getDataMappingPoint("Other_Condition")
                                    nDSDropDownField_Condition.SetTextWithTextValue(StringCondition, StringCondition)
                                    If StringCondition = "NULL" OrElse StringCondition = "Not NULL" OrElse StringCondition = "Empty" OrElse StringCondition = "Not Empty" Then
                                        inputConditionText.Hidden = True
                                    Else
                                        inputConditionText.Hidden = False
                                        If Not IsDBNull(getDataMappingPoint("Other_Value")) Then
                                            inputConditionText.Value = getDataMappingPoint("Other_Value")
                                        End If
                                    End If
                                Else
                                    Throw New ApplicationException("Internal Error, System Could not get Question Condition")
                                End If
                                If Not IsDBNull(getDataMappingPoint("Other_Score_Int")) Then
                                    Dim tempInt As Integer = Integer.Parse(getDataMappingPoint("Other_Score_Int"))
                                    inputPoint1.Value = tempInt
                                Else
                                    Throw New ApplicationException("Internal Error, System Could not get Question Point")
                                End If

                            Case EnumExtType.NumberField
                                inputPoint1.Hidden = False
                                nDSDropDownField_Condition.IsHidden = False
                                nDSDropDownField_Condition.StringFilter = " FK_AdvanceFilter_ID = 2 "
                                inputConditionNumberic1.Hidden = False
                                If Not IsDBNull(getDataMappingPoint("Other_Condition")) Then
                                    StringCondition = getDataMappingPoint("Other_Condition")
                                    nDSDropDownField_Condition.SetTextWithTextValue(StringCondition, StringCondition)
                                    If StringCondition = "Between" OrElse StringCondition = "Not Between" Then
                                        inputConditionNumberic1.FieldLabel = "From Condition"
                                        inputConditionNumberic2.Hidden = False
                                        If Not IsDBNull(getDataMappingPoint("Other_Value")) Then
                                            Dim tempInt As Long = CLng(getDataMappingPoint("Other_Value"))
                                            inputConditionNumberic1.Value = tempInt
                                        End If
                                        If Not IsDBNull(getDataMappingPoint("Other_Value_Helper")) Then
                                            Dim tempInt As Long = CLng(getDataMappingPoint("Other_Value_Helper"))
                                            inputConditionNumberic2.Value = tempInt
                                        End If
                                    ElseIf StringCondition = "NULL" OrElse StringCondition = "Not NULL" Then
                                        inputConditionNumberic1.Hidden = True
                                        inputConditionNumberic2.Hidden = True
                                    ElseIf StringCondition = "in" OrElse StringCondition = "Not In" Then
                                        inputConditionNumberic1.Hidden = True
                                        inputConditionNumberic2.Hidden = True
                                        inputConditionNumberic3.Hidden = False
                                        If Not IsDBNull(getDataMappingPoint("Other_Value")) Then
                                            inputConditionNumberic3.Value = getDataMappingPoint("Other_Value")
                                        End If
                                    Else
                                        inputConditionNumberic1.FieldLabel = "Condition"
                                        inputConditionNumberic2.Hidden = True
                                        If Not IsDBNull(getDataMappingPoint("Other_Value")) Then
                                            Dim tempInt As Long = CLng(getDataMappingPoint("Other_Value"))
                                            inputConditionNumberic1.Value = tempInt
                                        End If
                                    End If
                                Else
                                    Throw New ApplicationException("Internal Error, System Could not get Question Condition")
                                End If
                                If Not IsDBNull(getDataMappingPoint("Other_Score_Int")) Then
                                    Dim tempInt As Integer = Integer.Parse(getDataMappingPoint("Other_Score_Int"))
                                    inputPoint1.Value = tempInt
                                Else
                                    Throw New ApplicationException("Internal Error, System Could not get Question Point")
                                End If

                            Case EnumExtType.Radio
                                inputPoint1.FieldLabel = "Point For Yes"
                                inputPoint1.Hidden = False
                                inputPoint2.Hidden = False
                                If Not IsDBNull(getDataMappingPoint("Radio_Yes_Score_Int")) Then
                                    Dim tempInt As Integer = Integer.Parse(getDataMappingPoint("Radio_Yes_Score_Int"))
                                    inputPoint1.Value = tempInt
                                Else
                                    Throw New ApplicationException("Internal Error, System Could not get Question Point")
                                End If
                                If Not IsDBNull(getDataMappingPoint("Radio_No_Score_Int")) Then
                                    Dim tempInt As Integer = Integer.Parse(getDataMappingPoint("Radio_No_Score_Int"))
                                    inputPoint2.Value = tempInt
                                Else
                                    Throw New ApplicationException("Internal Error, System Could not get Question Point")
                                End If
                        End Select
                    Else
                        Throw New ApplicationException("Cannot Find Question Data Type, Please Fix Question Mapping or Contact Admin about this issue")
                    End If
                Else
                    Throw New ApplicationException("Internal Error, System Could not get Mapping Question Data")
                End If
            Else
                Throw New ApplicationException("Internal Error, System Could not get Mapping Point Data")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Btn_Cancel_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Save_Click()
        Try
            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
                Dim strQuery As String = " SELECT TOP 1 PK_ModuleApproval_ID FROM ModuleApproval "
                strQuery = strQuery & " WHERE ModuleName = '" & ObjModule.ModuleName & "'"
                strQuery = strQuery & " AND ModuleKey = '" & IDUnik & "'"
                strQuery = strQuery & " AND PK_ModuleAction_ID <> 1"
                Dim checkDataInApproval As DataRow = GetDataRowFromWithStringQuery(strQuery)
                If checkDataInApproval IsNot Nothing Then
                    Throw New ApplicationException("The system detects that there is already an Delete or Update Request in Pending Approval For this Mapping, please Accept or Reject Request in Approval.")
                    'WindowConfirmation.Hidden = False
                Else
                    'Without Approval
                    SavingData(False)
                End If
            Else
                'With Approval
                SavingData(True)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub WindowConfirmation_Yes_Click()
        Try
            WindowConfirmation.Hidden = True
            'Without Approval
            SavingData(False)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub WindowConfirmation_No_Click()
        Try
            WindowConfirmation.Hidden = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub NDSDropDownField_MappingQuestion_OnValueChanged(sender As Object, e As EventArgs)
        Try
            inputPoint1.FieldLabel = "Point"
            inputPoint1.Hidden = True
            inputPoint2.Hidden = True
            inputPoint1.Value = 0
            inputPoint2.Value = 0
            nDSDropDownField_Condition.IsHidden = True
            inputConditionNumberic1.FieldLabel = "Condition"
            inputConditionNumberic1.Hidden = True
            inputConditionNumberic2.Hidden = True
            inputConditionNumberic3.Hidden = True
            inputConditionDate1.FieldLabel = "Condition"
            inputConditionDate1.Hidden = True
            inputConditionDate2.Hidden = True
            inputConditionText.Hidden = True
            nDSDropDownField_ReferenceTable.IsHidden = True
            inputReferenceTableAlias.Hidden = True
            nDSDropDownField_ReferenceFieldKey.IsHidden = True
            labelReferenceFieldScoreInfo.Hidden = True
            nDSDropDownField_ReferenceFieldScore.IsHidden = True
            nDSDropDownField_Condition.SetTextValue("")
            nDSDropDownField_ReferenceFieldKey.StringFilter = " 1=2 "
            nDSDropDownField_ReferenceFieldScore.StringFilter = " 1=2 "
            If Not String.IsNullOrWhiteSpace(nDSDropDownField_MappingQuestion.SelectedItemValue) Then
                Dim intPKMappingQuestion As Integer = Convert.ToInt32(nDSDropDownField_MappingQuestion.SelectedItemValue)
                Dim strQuery As String = " select"
                strQuery = strQuery & " mappingedd.PK_OneFCC_Question_Mapping_EDD_ID"
                strQuery = strQuery & " ,question.FK_ExtType_ID"
                strQuery = strQuery & " ,question.Table_Reference_Name"
                strQuery = strQuery & " ,question.Table_Reference_Alias"
                strQuery = strQuery & " ,question.Table_Reference_Field_Key"
                strQuery = strQuery & " ,question.Table_Reference_Field_Display_Name"
                strQuery = strQuery & " ,EDDType.EDD_Type_Name AS [EDD Question Type]"
                strQuery = strQuery & " ,customertype.Description AS [Customer Type]"
                strQuery = strQuery & " ,customersegment.Description AS [Customer Segment]"
                strQuery = strQuery & " ,questiongroup.Question_Group_Name AS [Question Group]"
                strQuery = strQuery & " ,exttype.ExtTypeName AS [Answer Type]"
                strQuery = strQuery & " from OneFCC_Question_Mapping_EDD mappingedd"
                strQuery = strQuery & " join OneFCC_Question question"
                strQuery = strQuery & " on mappingedd.FK_OneFCC_Question_ID = question.PK_OneFCC_Question_ID"
                strQuery = strQuery & " INNER JOIN MExtType exttype"
                strQuery = strQuery & " ON question.FK_ExtType_ID = exttype.PK_ExtType_ID"
                strQuery = strQuery & " INNER JOIN OneFCC_EDD_Type EDDType"
                strQuery = strQuery & " ON mappingedd.FK_OneFCC_Question_Type_Code = EDDType.EDD_Type_Code"
                strQuery = strQuery & " INNER JOIN OneFCC_Question_Customer_Type customertype"
                strQuery = strQuery & " ON mappingedd.FK_OneFCC_Question_Customer_Type_Code = customertype.Customer_Type_Code"
                strQuery = strQuery & " INNER JOIN OneFCC_Question_Customer_Segment customersegment"
                strQuery = strQuery & " ON mappingedd.FK_OneFCC_Question_Customer_Segment_Code = customersegment.Customer_Segment_Code"
                strQuery = strQuery & " INNER JOIN OneFCC_Question_Group questiongroup"
                strQuery = strQuery & " ON mappingedd.FK_OneFCC_Question_Group_Code = questiongroup.Question_Group_Code"
                strQuery = strQuery & " where PK_OneFCC_Question_Mapping_EDD_ID = " & nDSDropDownField_MappingQuestion.SelectedItemValue
                Dim getDataMappingQuestion As DataRow = GetDataRowFromWithStringQuery(strQuery)
                If getDataMappingQuestion IsNot Nothing Then
                    If Not IsDBNull(getDataMappingQuestion("EDD Question Type")) Then
                        displayField_Question_Type.Text = getDataMappingQuestion("EDD Question Type")
                    Else
                        displayField_Question_Type.Text = ""
                    End If
                    If Not IsDBNull(getDataMappingQuestion("Customer Type")) Then
                        displayField_Customer_Type.Text = getDataMappingQuestion("Customer Type")
                    Else
                        displayField_Customer_Type.Text = ""
                    End If
                    If Not IsDBNull(getDataMappingQuestion("Customer Segment")) Then
                        displayField_Customer_Segment.Text = getDataMappingQuestion("Customer Segment")
                    Else
                        displayField_Customer_Segment.Text = ""
                    End If
                    If Not IsDBNull(getDataMappingQuestion("Question Group")) Then
                        displayField_Question_Group.Text = getDataMappingQuestion("Question Group")
                    Else
                        displayField_Question_Group.Text = ""
                    End If
                    If Not IsDBNull(getDataMappingQuestion("Answer Type")) Then
                        displayField_Answer_Type.Text = getDataMappingQuestion("Answer Type")
                    Else
                        displayField_Answer_Type.Text = ""
                    End If
                End If
                If getDataMappingQuestion IsNot Nothing AndAlso Not IsDBNull(getDataMappingQuestion("FK_ExtType_ID")) Then
                    FK_ExtType_ID = getDataMappingQuestion("FK_ExtType_ID")
                    Select Case FK_ExtType_ID
                        Case EnumExtType.DateField
                            inputPoint1.Hidden = False
                            nDSDropDownField_Condition.IsHidden = False
                            nDSDropDownField_Condition.StringFilter = " FK_AdvanceFilter_ID = 3 "
                            inputConditionDate1.Hidden = False

                        Case EnumExtType.DropDownField
                            nDSDropDownField_ReferenceTable.IsHidden = False
                            inputReferenceTableAlias.Hidden = False
                            nDSDropDownField_ReferenceFieldKey.IsHidden = False
                            labelReferenceFieldScoreInfo.Hidden = False
                            nDSDropDownField_ReferenceFieldScore.IsHidden = False

                        Case EnumExtType.TextField
                            inputPoint1.Hidden = False
                            nDSDropDownField_Condition.IsHidden = False
                            nDSDropDownField_Condition.StringFilter = " FK_AdvanceFilter_ID = 1 "
                            inputConditionText.Hidden = False

                        Case EnumExtType.NumberField
                            inputPoint1.Hidden = False
                            nDSDropDownField_Condition.IsHidden = False
                            nDSDropDownField_Condition.StringFilter = " FK_AdvanceFilter_ID = 2 "
                            inputConditionNumberic1.Hidden = False

                        Case EnumExtType.Radio
                            inputPoint1.FieldLabel = "Point For Yes"
                            inputPoint1.Hidden = False
                            inputPoint2.Hidden = False
                    End Select
                Else
                    Throw New ApplicationException("Cannot Find Question Data Type, Please Fix Question Mapping or Contact Admin about this issue")
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub NDSDropDownField_Condition_OnValueChanged(sender As Object, e As EventArgs)
        Try
            If Not String.IsNullOrWhiteSpace(nDSDropDownField_Condition.SelectedItemValue) Then
                StringCondition = nDSDropDownField_Condition.SelectedItemValue
                Select Case FK_ExtType_ID
                    Case EnumExtType.DateField
                        If nDSDropDownField_Condition.SelectedItemValue = "Between" OrElse nDSDropDownField_Condition.SelectedItemValue = "Not Between" Then
                            inputConditionDate1.FieldLabel = "From Condition"
                            inputConditionDate2.Hidden = False
                        ElseIf nDSDropDownField_Condition.SelectedItemValue = "NULL" OrElse nDSDropDownField_Condition.SelectedItemValue = "Not NULL" Then
                            inputConditionDate1.Hidden = True
                            inputConditionDate2.Hidden = True
                        Else
                            inputConditionDate1.FieldLabel = "Condition"
                            inputConditionDate2.Hidden = True
                        End If

                    Case EnumExtType.NumberField
                        If nDSDropDownField_Condition.SelectedItemValue = "Between" OrElse nDSDropDownField_Condition.SelectedItemValue = "Not Between" Then
                            inputConditionNumberic1.FieldLabel = "From Condition"
                            inputConditionNumberic2.Hidden = False
                            inputConditionNumberic3.Hidden = True
                        ElseIf nDSDropDownField_Condition.SelectedItemValue = "NULL" OrElse nDSDropDownField_Condition.SelectedItemValue = "Not NULL" Then
                            inputConditionNumberic1.Hidden = True
                            inputConditionNumberic2.Hidden = True
                            inputConditionNumberic3.Hidden = True
                        ElseIf nDSDropDownField_Condition.SelectedItemValue = "in" OrElse nDSDropDownField_Condition.SelectedItemValue = "Not In" Then
                            inputConditionNumberic3.Hidden = False
                            inputConditionNumberic1.Hidden = True
                            inputConditionNumberic2.Hidden = True
                        Else
                            inputConditionNumberic1.FieldLabel = "Condition"
                            inputConditionNumberic2.Hidden = True
                            inputConditionNumberic3.Hidden = True
                        End If

                    Case EnumExtType.TextField
                        If nDSDropDownField_Condition.SelectedItemValue = "NULL" OrElse nDSDropDownField_Condition.SelectedItemValue = "Not NULL" OrElse
                            nDSDropDownField_Condition.SelectedItemValue = "Empty" OrElse nDSDropDownField_Condition.SelectedItemValue = "Not Empty" Then
                            inputConditionText.Hidden = True
                        Else
                            inputConditionText.Hidden = False
                        End If

                End Select
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub NDSDropDownField_ReferenceTable_OnValueChanged(sender As Object, e As EventArgs)
        Try
            If Not String.IsNullOrWhiteSpace(nDSDropDownField_ReferenceTable.SelectedItemValue) Then
                SetUpReferenceTableField(nDSDropDownField_ReferenceTable.SelectedItemValue)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub SetUpReferenceTableField(tableName As String)
        Try
            nDSDropDownField_ReferenceFieldKey.SetTextValue("")
            nDSDropDownField_ReferenceFieldScore.SetTextValue("")
            nDSDropDownField_ReferenceFieldKey.StringFilter = " TABLE_NAME = '" & tableName & "' "
            nDSDropDownField_ReferenceFieldScore.StringFilter = " TABLE_NAME = '" & tableName & "' AND DATA_TYPE = 'int' "
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ValidateInput()
        Try
            If String.IsNullOrEmpty(nDSDropDownField_MappingQuestion.SelectedItemValue) Then
                Throw New ApplicationException(nDSDropDownField_MappingQuestion.Label & " is required.")
            End If

            Select Case FK_ExtType_ID
                Case EnumExtType.DateField
                    If String.IsNullOrEmpty(nDSDropDownField_Condition.SelectedItemValue) Then
                        Throw New ApplicationException(nDSDropDownField_Condition.Label & " is required.")
                    End If
                    If nDSDropDownField_Condition.SelectedItemValue = "Between" OrElse nDSDropDownField_Condition.SelectedItemValue = "Not Between" Then
                        If CDate(inputConditionDate1.Value) = DateTime.MinValue Then
                            Throw New ApplicationException(inputConditionDate1.FieldLabel & " is required.")
                        End If
                        If CDate(inputConditionDate2.Value) = DateTime.MinValue Then
                            Throw New ApplicationException(inputConditionDate2.FieldLabel & " is required.")
                        End If
                    ElseIf nDSDropDownField_Condition.SelectedItemValue = "NULL" OrElse nDSDropDownField_Condition.SelectedItemValue = "Not NULL" Then

                    Else
                        If CDate(inputConditionDate1.Value) = DateTime.MinValue Then
                            Throw New ApplicationException(inputConditionDate1.FieldLabel & " is required.")
                        End If
                    End If
                    If String.IsNullOrEmpty(inputPoint1.Text) Or inputPoint1.Text = "N/A" Or inputPoint1.Text = "NA" Then
                        Throw New ApplicationException(inputPoint1.FieldLabel & " is required.")
                    End If

                Case EnumExtType.DropDownField
                    If String.IsNullOrEmpty(nDSDropDownField_ReferenceTable.SelectedItemValue) Then
                        Throw New ApplicationException(nDSDropDownField_ReferenceTable.Label & " is required.")
                    End If
                    If String.IsNullOrEmpty(nDSDropDownField_ReferenceFieldKey.SelectedItemValue) Then
                        Throw New ApplicationException(nDSDropDownField_ReferenceFieldKey.Label & " is required.")
                    End If
                    If String.IsNullOrEmpty(nDSDropDownField_ReferenceFieldScore.SelectedItemValue) Then
                        Throw New ApplicationException(nDSDropDownField_ReferenceFieldScore.Label & " is required.")
                    End If

                Case EnumExtType.TextField
                    If String.IsNullOrEmpty(nDSDropDownField_Condition.SelectedItemValue) Then
                        Throw New ApplicationException(nDSDropDownField_Condition.Label & " is required.")
                    End If
                    If String.IsNullOrEmpty(inputPoint1.Text) Or inputPoint1.Text = "N/A" Or inputPoint1.Text = "NA" Then
                        Throw New ApplicationException(inputPoint1.FieldLabel & " is required.")
                    End If

                Case EnumExtType.NumberField
                    If String.IsNullOrEmpty(nDSDropDownField_Condition.SelectedItemValue) Then
                        Throw New ApplicationException(nDSDropDownField_Condition.Label & " is required.")
                    End If
                    If nDSDropDownField_Condition.SelectedItemValue = "Between" OrElse nDSDropDownField_Condition.SelectedItemValue = "Not Between" Then
                        If String.IsNullOrEmpty(inputConditionNumberic1.Text) Or inputConditionNumberic1.Text = "N/A" Or inputConditionNumberic1.Text = "NA" Then
                            Throw New ApplicationException(inputConditionNumberic1.FieldLabel & " is required.")
                        End If
                        If String.IsNullOrEmpty(inputConditionNumberic2.Text) Or inputConditionNumberic2.Text = "N/A" Or inputConditionNumberic2.Text = "NA" Then
                            Throw New ApplicationException(inputConditionNumberic2.FieldLabel & " is required.")
                        End If
                    ElseIf nDSDropDownField_Condition.SelectedItemValue = "NULL" OrElse nDSDropDownField_Condition.SelectedItemValue = "Not NULL" Then

                    ElseIf nDSDropDownField_Condition.SelectedItemValue = "in" OrElse nDSDropDownField_Condition.SelectedItemValue = "Not In" Then
                        If String.IsNullOrEmpty(inputConditionNumberic3.Text) Then
                            Throw New ApplicationException(inputConditionNumberic3.FieldLabel & " is required.")
                        End If
                    Else
                        If String.IsNullOrEmpty(inputConditionNumberic1.Text) Or inputConditionNumberic1.Text = "N/A" Or inputConditionNumberic1.Text = "NA" Then
                            Throw New ApplicationException(inputConditionNumberic1.FieldLabel & " is required.")
                        End If
                    End If
                    If String.IsNullOrEmpty(inputPoint1.Text) Or inputPoint1.Text = "N/A" Or inputPoint1.Text = "NA" Then
                        Throw New ApplicationException(inputPoint1.FieldLabel & " is required.")
                    End If

                Case EnumExtType.Radio
                    If String.IsNullOrEmpty(inputPoint1.Text) Or inputPoint1.Text = "N/A" Or inputPoint1.Text = "NA" Then
                        Throw New ApplicationException("Point For Yes is required.")
                    End If
                    If String.IsNullOrEmpty(inputPoint2.Text) Or inputPoint2.Text = "N/A" Or inputPoint2.Text = "NA" Then
                        Throw New ApplicationException(inputPoint2.FieldLabel & " is required.")
                    End If

            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SavingData(isWithApproval As Boolean)
        Try
            Dim paramSP(16) As SqlParameter

            paramSP(0) = New SqlParameter
            paramSP(0).ParameterName = "@ModuleName"
            paramSP(0).Value = ObjModule.ModuleName
            paramSP(0).SqlDbType = SqlDbType.VarChar

            '1  Insert
            '2  Update
            '3  Delete
            '4  Approve
            '5  Reject
            paramSP(1) = New SqlParameter
            paramSP(1).ParameterName = "@Action"
            paramSP(1).Value = 3
            paramSP(1).SqlDbType = SqlDbType.Int

            paramSP(2) = New SqlParameter
            paramSP(2).ParameterName = "@UserID"
            paramSP(2).Value = NawaBLL.Common.SessionCurrentUser.UserID
            paramSP(2).SqlDbType = SqlDbType.VarChar

            paramSP(3) = New SqlParameter
            paramSP(3).ParameterName = "@isWithApproval"
            paramSP(3).Value = isWithApproval
            paramSP(3).SqlDbType = SqlDbType.Bit

            paramSP(4) = New SqlParameter
            paramSP(4).ParameterName = "@PK_OneFCC_EDD_Mapping_Question_Answer_Point_ID"
            paramSP(4).Value = IDUnik
            paramSP(4).SqlDbType = SqlDbType.BigInt

            paramSP(5) = New SqlParameter
            paramSP(5).ParameterName = "@PK_ModuleApproval_ID"
            paramSP(5).Value = DBNull.Value
            paramSP(5).SqlDbType = SqlDbType.BigInt

            paramSP(6) = New SqlParameter
            paramSP(6).ParameterName = "@FK_OneFCC_Question_Mapping_EDD_ID"
            paramSP(6).Value = DBNull.Value
            paramSP(6).SqlDbType = SqlDbType.BigInt

            paramSP(7) = New SqlParameter
            paramSP(7).ParameterName = "@Table_Reference_Name"
            paramSP(7).Value = DBNull.Value
            paramSP(7).SqlDbType = SqlDbType.VarChar

            paramSP(8) = New SqlParameter
            paramSP(8).ParameterName = "@Table_Reference_Alias"
            paramSP(8).Value = DBNull.Value
            paramSP(8).SqlDbType = SqlDbType.VarChar

            paramSP(9) = New SqlParameter
            paramSP(9).ParameterName = "@Table_Reference_Field_Key"
            paramSP(9).Value = DBNull.Value
            paramSP(9).SqlDbType = SqlDbType.VarChar

            paramSP(10) = New SqlParameter
            paramSP(10).ParameterName = "@Table_Reference_Field_Score"
            paramSP(10).Value = DBNull.Value
            paramSP(10).SqlDbType = SqlDbType.VarChar

            paramSP(11) = New SqlParameter
            paramSP(11).ParameterName = "@Radio_Yes_Score_Int"
            paramSP(11).Value = DBNull.Value
            paramSP(11).SqlDbType = SqlDbType.Int

            paramSP(12) = New SqlParameter
            paramSP(12).ParameterName = "@Radio_No_Score_Int"
            paramSP(12).Value = DBNull.Value
            paramSP(12).SqlDbType = SqlDbType.Int

            paramSP(13) = New SqlParameter
            paramSP(13).ParameterName = "@Other_Condition"
            paramSP(13).Value = DBNull.Value
            paramSP(13).SqlDbType = SqlDbType.VarChar

            paramSP(14) = New SqlParameter
            paramSP(14).ParameterName = "@Other_Value"
            paramSP(14).Value = DBNull.Value
            paramSP(14).SqlDbType = SqlDbType.VarChar

            paramSP(15) = New SqlParameter
            paramSP(15).ParameterName = "@Other_Score_Int"
            paramSP(15).Value = DBNull.Value
            paramSP(15).SqlDbType = SqlDbType.Int

            paramSP(16) = New SqlParameter
            paramSP(16).ParameterName = "@Other_Value_Helper"
            paramSP(16).Value = DBNull.Value
            paramSP(16).SqlDbType = SqlDbType.VarChar

            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_EDD_Mapping_Answers_Point_SaveData", paramSP)
            'Show Confirmation
            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
                LblConfirmation.Text = "Data Saved into Database"
            Else
                LblConfirmation.Text = "Data Saved into Pending Approval"
            End If
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function GetDataRowFromWithStringQuery(strQuery As String) As DataRow
        Try
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Return drResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class