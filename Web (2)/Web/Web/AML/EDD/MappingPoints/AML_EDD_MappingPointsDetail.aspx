﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AML_EDD_MappingPointsDetail.aspx.vb" Inherits="AML_EDD_MappingPointsDetail" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true">
        <Items>
            <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="20 20 10 20">
                <Content>
                    <NDS:NDSDropDownField ID="nDSDropDownField_MappingQuestion" ValueField="ID" DisplayField="Question" runat="server" StringField="*" StringTable="vw_OneFCC_EDD_Mapping_Question_Without_Answer" Label="Mapping Question" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" OnOnValueChanged="NDSDropDownField_MappingQuestion_OnValueChanged" IsReadOnly="true" />
                    <ext:DisplayField ID="displayField_Question_Type" runat="server" LabelWidth="200" FieldLabel="Question Type"/>
                    <ext:DisplayField ID="displayField_Customer_Type" runat="server" LabelWidth="200" FieldLabel="Customer Type"/>
                    <ext:DisplayField ID="displayField_Customer_Segment" runat="server" LabelWidth="200" FieldLabel="Customer Segment"/>
                    <ext:DisplayField ID="displayField_Question_Group" runat="server" LabelWidth="200" FieldLabel="Question Group"/>
                    <ext:DisplayField ID="displayField_Answer_Type" runat="server" LabelWidth="200" FieldLabel="Answer Type"/>
                    <NDS:NDSDropDownField ID="nDSDropDownField_Condition" ValueField="FilterWhereClause" DisplayField="FilterWhereClause" runat="server" StringField="FilterWhereClause, FilterWhereFormat" StringTable="AdvancedFilterWhereClause" Label="Condition Clause" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" OnOnValueChanged="NDSDropDownField_Condition_OnValueChanged" IsHidden="true" IsReadOnly="true"/>
                    
                    <NDS:NDSDropDownField ID="nDSDropDownField_ReferenceTable" ValueField="TABLE_NAME" DisplayField="TABLE_NAME" runat="server" StringField="TABLE_NAME, TABLE_TYPE" StringTable="INFORMATION_SCHEMA.TABLES" Label="Table Reference Name" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" OnOnValueChanged="NDSDropDownField_ReferenceTable_OnValueChanged" IsHidden="true" IsReadOnly="true"/>
                    <ext:TextField ID="inputReferenceTableAlias" runat="server" FieldLabel="Table Alias" AnchorHorizontal="70%" AllowBlank="true" LabelWidth ="200" Hidden="true" ReadOnly="true" FieldStyle="background-color: #ddd"/>
                    <NDS:NDSDropDownField ID="nDSDropDownField_ReferenceFieldKey" ValueField="COLUMN_NAME" DisplayField="COLUMN_NAME" runat="server" StringField="COLUMN_NAME, DATA_TYPE" StringTable="INFORMATION_SCHEMA.COLUMNS" Label="Table Reference Field Key" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" IsHidden="true" IsReadOnly="true"/>
                    <ext:Label ID="labelReferenceFieldScoreInfo" runat="server" Text="*Field Score Can Only Choose Field With Data Type INT" Hidden="true" MarginSpec="10 0 0 0"/>
                    <NDS:NDSDropDownField ID="nDSDropDownField_ReferenceFieldScore" ValueField="COLUMN_NAME" DisplayField="COLUMN_NAME" runat="server" StringField="COLUMN_NAME, DATA_TYPE" StringTable="INFORMATION_SCHEMA.COLUMNS" Label="Table Reference Field Score" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" IsHidden="true" IsReadOnly="true"/>
                    <ext:TextField ID="inputConditionText" runat="server" FieldLabel="Condition" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" Hidden="true" ReadOnly="true" FieldStyle="background-color: #ddd"/>
                    <ext:DateField ID="inputConditionDate1" runat="server" FieldLabel="Condition" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" Hidden="true" ReadOnly="true" FieldStyle="background-color: #ddd"/>
                    <ext:DateField ID="inputConditionDate2" runat="server" FieldLabel="To Condition" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" Hidden="true" ReadOnly="true" FieldStyle="background-color: #ddd"/>
                    <ext:NumberField ID="inputConditionNumberic1" runat="server" FieldLabel="Condition" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" Hidden="true" FieldStyle="text-align:left;background-color: #ddd" ReadOnly="true"/>
                    <ext:NumberField ID="inputConditionNumberic2" runat="server" FieldLabel="To Condition" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" Hidden="true" FieldStyle="text-align:left;background-color: #ddd" ReadOnly="true"/>
                    <ext:TextField ID="inputConditionNumberic3" runat="server" FieldLabel="Condition" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" Hidden="true" MaskRe="[0-9,]"/>
                    <ext:NumberField ID="inputPoint1" runat="server" FieldLabel="Point" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" Hidden="true" DecimalPrecision="0" FieldStyle="text-align:left;background-color: #ddd" ReadOnly="true"/>
                    <ext:NumberField ID="inputPoint2" runat="server" FieldLabel="Point For No" AnchorHorizontal="70%" AllowBlank="false" LabelWidth ="200" Hidden="true" DecimalPrecision="0" FieldStyle="text-align:left;background-color: #ddd" ReadOnly="true"/>
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Cancel" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="Btn_Cancel_Click">
                        <EventMask ShowMask="true" Msg="Loading..." ></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="Accept">
                <DirectEvents>
                    <Click OnEvent="Btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>

