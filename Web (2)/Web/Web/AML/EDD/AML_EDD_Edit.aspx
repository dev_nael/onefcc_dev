﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AML_EDD_Edit.aspx.vb" Inherits="AML_EDD_Edit" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="Watch List Data">
        <Items>
            <ext:FieldSet runat="server" ID="fs_Individual" Collapsible="true" Border="true" Layout="AnchorLayout" ColumnWidth="1"  MarginSpec="10 0" Padding="10">
                <Content>
                    <ext:TabPanel ID="TabPanel1" runat="server" ActiveTabIndex="0" Plain="false" Layout="AnchorLayout" AnchorHorizontal="100%" BodyStyle="padding:10px">
                        <Items>
                            <ext:Panel runat="server" Title="General Information" Layout="AnchorLayout" AnchorHorizontal="100%" ID="panelGeneralInformation" Border="true" BodyStyle="padding:10px" MarginSpec="0 0 10 0">
                                <Content>
                                    <NDS:NDSDropDownField ID="cmb_EDD_TYPE_ID" LabelWidth="200" ValueField="EDD_Type_Code" DisplayField="EDD_Type_Name" runat="server" StringField="EDD_Type_Code, EDD_Type_Name" StringTable="OneFCC_EDD_Type" Label="EDD Type" AnchorHorizontal="80%" AllowBlank="false"/>
                    
                                    <ext:Button ID="btn_Import_Customer" runat="server" Icon="Zoom" Text="Search Data Customer" MarginSpec="0 10 10 0" Hidden="true">
                                        <DirectEvents>
                                            <Click OnEvent="Btn_Import_Customer_Click">
                                                <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <%--<ext:TextField ID="txt_CIFNo" AllowBlank="true" LabelWidth="200" runat="server" FieldLabel="CIF" AnchorHorizontal="80%" MaxLength="50" EnforceMaxLength="true" Hidden="true" ReadOnly="true" FieldStyle="background-color: #ddd"></ext:TextField>--%> 
                                    <%--<NDS:NDSDropDownField ID="nDSDropDownField_CIFNo" LabelWidth="250" ValueField="CIFNo" DisplayField="CIFNo" runat="server" StringField="CIFNo, CUSTOMERNAME, FK_AML_RISK_CODE" StringTable="AML_CUSTOMER" Label="CIF" AnchorHorizontal="80%" AllowBlank="false" IsHidden="true" OnOnValueChanged="nDSDropDownField_CIFNo_OnValueChanged"/>--%>
                                    <%--<ext:TextField ID="txt_Nama" AllowBlank="false" LabelWidth="200" runat="server" FieldLabel="Nama" AnchorHorizontal="80%" MaxLength="250" EnforceMaxLength="true"></ext:TextField>--%>
                                    
                                    <ext:Panel runat="server" Layout="ColumnLayout" AnchorHorizontal="80%" ID="Panel1" Border="false">
                                        <Items>
                                            <ext:Panel runat="server" ColumnWidth="0.6" Border="false" Layout="AnchorLayout">
                                                <Content>
                                                    <ext:TextField ID="txt_CIFNo" AllowBlank="true" LabelWidth="200" runat="server" FieldLabel="CIF" AnchorHorizontal="90%" MaxLength="50" EnforceMaxLength="true" Hidden="true" ReadOnly="true" FieldStyle="background-color: #ddd"></ext:TextField> 
                                                    <ext:TextField ID="txt_Nama" AllowBlank="false" LabelWidth="200" runat="server" FieldLabel="Nama" AnchorHorizontal="90%" MaxLength="250" EnforceMaxLength="true" Hidden="true" ReadOnly="true" FieldStyle="background-color: #ddd"></ext:TextField>
                                                    <ext:TextField ID="customer_NIK" AllowBlank="false" LabelWidth="200" runat="server" FieldLabel="NIK" AnchorHorizontal="90%" MaxLength="25" EnforceMaxLength="true" Hidden="true" ReadOnly="true" FieldStyle="background-color: #ddd"></ext:TextField>
                                                    <NDS:NDSDropDownField ID="customer_Nationality" LabelWidth="200" ValueField="FK_AML_COUNTRY_Code" DisplayField="AML_COUNTRY_Name" runat="server" StringField="FK_AML_COUNTRY_Code, AML_COUNTRY_Name" StringTable="AML_COUNTRY" Label="Nationality" AnchorHorizontal="90%" AllowBlank="false" IsHidden="true" IsReadOnly="true" StringFieldStyle="background-color: #ddd"></NDS:NDSDropDownField>
                                                </Content>
                                            </ext:Panel>
                                            <ext:Panel runat="server" ColumnWidth="0.4" Border="false" Layout="AnchorLayout">
                                                <Content>
                                                    <ext:Button ID="btn_OpenWindowCustomer" runat="server" Icon="ApplicationViewDetail" Text="Data Customer Profile" MarginSpec="0 10 10 0" Hidden="true">
                                                        <DirectEvents>
                                                            <Click OnEvent="Btn_OpenWindowCustomer_Click">
                                                                <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                                                            </Click>
                                                        </DirectEvents>
                                                    </ext:Button>
                                                    <ext:DateField ID="customer_DOB" AllowBlank="false" LabelWidth="150" runat="server" FieldLabel="Date Of Birth" AnchorHorizontal="100%" Hidden="true" ReadOnly="true" FieldStyle="background-color: #ddd"></ext:DateField>
                                                    <ext:TextArea ID="customer_POB" AllowBlank="false" LabelWidth="150" runat="server" FieldLabel="Place Of Birth" AnchorHorizontal="100%" MaxLength="200" EnforceMaxLength="true" Hidden="true" ReadOnly="true" FieldStyle="background-color: #ddd"></ext:TextArea>
                                                </Content>
                                            </ext:Panel>
                                        </Items>
                                    </ext:Panel>
                                    <NDS:NDSDropDownField ID="cmb_Customer_Type_ID" LabelWidth="200" ValueField="Customer_Type_Code" DisplayField="Description" runat="server" StringField="Customer_Type_Code, Description" StringTable="OneFCC_Question_Customer_Type" Label="Customer Type" AnchorHorizontal="80%" AllowBlank="false" IsReadOnly="true"/>
                                    <ext:Checkbox
                                        runat="server"
                                        ID="checkbox_beneficialowner"
                                        FieldLabel="Have Beneficial Owner ?" 
                                        LabelWidth="200">
                                        <DirectEvents>
                                            <Change OnEvent="Checkbox_click">
                                                <EventMask ShowMask="true" Msg="Please Wait..."></EventMask>
                                            </Change>
                                        </DirectEvents>
                                    </ext:Checkbox>
                                    <NDS:NDSDropDownField ID="cmb_Customer_Segment_ID" LabelWidth="200" ValueField="Customer_Segment_Code" DisplayField="Description" runat="server" StringField="Customer_Segment_Code, Description" StringTable="OneFCC_Question_Customer_Segment" Label="Customer Segment" AnchorHorizontal="80%" AllowBlank="false"/>
                                    <!-- Add 27-Jan-2023 , Felix Display Risk Rating Score -->
                                    <ext:DisplayField ID="txt_RiskRatingScore" LabelWidth="200" runat="server" FieldLabel="Risk Rating Score" AnchorHorizontal="90%" MaxLength="50" EnforceMaxLength="true" ReadOnly="true"></ext:DisplayField>
                                    <ext:GridPanel ID="gp_RiskRatingDetail" runat="server" Title="Risk Rating Detail" MarginSpec="0 0 10 0" Collapsible="true" Border="true" MinHeight="200" Scrollable="Vertical" AutoScroll="true">
                                        <View>
                                            <ext:GridView runat="server" EnableTextSelection="true" />
                                        </View>
                                        <Store>
                                            <ext:Store ID="store_RiskRatingDetail" runat="server" IsPagingStore="true" PageSize="10">
                                                <Model>
                                                    <ext:Model runat="server" ID="Model15" IDProperty="Test">
                                                        <Fields>
                                                            <ext:ModelField Name="RISK_RATING_QUERY_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RISK_RATING_DETAIL_VALUE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RISK_RATING_SCORE" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column122" runat="server" DataIndex="RISK_RATING_QUERY_NAME" Text="Risk Rating Name" MinWidth="100" CellWrap="true" flex="1"></ext:Column>
                                                <ext:Column ID="Column120" runat="server" DataIndex="RISK_RATING_DETAIL_VALUE" Text="Risk Rating Value" MinWidth="100" CellWrap="true" flex="1"></ext:Column>
                                                <ext:Column ID="Column121" runat="server" DataIndex="RISK_RATING_SCORE" Text="Risk Rating Score" MinWidth="250" CellWrap="true" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar15" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <!-- Emd 27-Jan-2023 -->
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel2" runat="server" Title="Notes History" BodyPadding="6" AutoScroll="true" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <ext:GridPanel ID="GridPanelNotesHistory" runat="server" EmptyText="No Available Data" AnchorHorizontal="100%">
                                        <View>
                                            <ext:GridView runat="server" EnableTextSelection="true" />
                                        </View>
                                        <Store>
                                            <ext:Store ID="StoreNotesHistory" runat="server">
                                                <Model>
                                                    <ext:Model runat="server" ID="Model355" IDProperty="PK_ONEFCC_EDD_NOTES_HISTORY_ID">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_ONEFCC_EDD_NOTES_HISTORY_ID" Type="Int"></ext:ModelField>
                                                            <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                                            <ext:ModelField Name="CreatedBy" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NOTES" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FILE_ATTACHMENTName" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn88" runat="server" Text="No"></ext:RowNumbererColumn>
                                                <ext:DateColumn ID="Column312" runat="server" DataIndex="CreatedDate" Text="Created Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                                <ext:Column ID="Column2" runat="server" DataIndex="CreatedBy" Text="User Name" Flex="1"></ext:Column>
                                                <ext:Column ID="Column3" runat="server" DataIndex="NOTES" Text="Note" Flex="1"></ext:Column>
                                                <ext:Column ID="Column1" runat="server" DataIndex="FILE_ATTACHMENTName" Text="File Name" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn105" runat="server" Text="Action" Flex="1">
                                                    <Commands>
                                                        <ext:GridCommand Text="Download Attachment" CommandName="Download" Icon="ApplicationEdit" MinWidth="70">
                                                            <ToolTip Text="Download Attachment"></ToolTip>
                                                        </ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridcommandDownloadAttachment" IsUpload="true" Success="NawadataDirect.DownloadFileAttachmentFromHistory_Direct({isUpload : true});">
                                                            <EventMask ShowMask="true"></EventMask>
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_ONEFCC_EDD_NOTES_HISTORY_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                    </ext:GridPanel>
                                </Content>
                                <Loader runat="server" Mode="Frame"  TriggerEvent="Activate" ReloadOnEvent="true" DisableCaching="true">
                                    <LoadMask ShowMask="true" />
                                </Loader>
                            </ext:Panel>
                        </Items>
                    </ext:TabPanel>
                </Content>
            </ext:FieldSet>
            
            <ext:FormPanel ID="FormPanelCustomer" BodyPadding="10" runat="server" ClientIDMode="Static" Layout="AnchorLayout" Border="true" ButtonAlign="Center" AutoScroll="true" Collapsible="true" MarginSpec="0 0 10 0">
                <Items>
                </Items>
            </ext:FormPanel>
            
            <ext:FormPanel ID="FormPanelBenificialOwner" BodyPadding="10" runat="server" ClientIDMode="Static" Layout="AnchorLayout" Border="true" ButtonAlign="Center" AutoScroll="true" Hidden="true" Collapsible="true" MarginSpec="0 0 10 0">
                <Items>
                </Items>
            </ext:FormPanel>
            
            <ext:Panel runat="server" Layout="AnchorLayout" ID="PanelNotes" Border="false" Title="Notes" ButtonAlign="Left" Collapsible="true" MarginSpec="0 0 10 0" BodyStyle="padding:10px">
                <Items>
                    <ext:TextArea runat="server" ID="txtNotes" AllowBlank="true" AnchorHorizontal="80%" LabelWidth="200" FieldLabel="Notes"/>
                    <ext:DisplayField runat="server" ID="File_Attachment"  FieldLabel="File Attachment" AnchorHorizontal="80%" AllowBlank="true" LabelWidth="200"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button runat="server" ID="BtnFileAttachment" Icon="DiskDownload" Hidden="false" Text="Attach File" MarginSpec="0 0 0 200">
                        <DirectEvents>
                            <Click OnEvent="BtnFileAttachment_Click">
                                <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button runat="server" ID="BtnDownloadFile" Icon="DiskDownload" Hidden="true" Text="Download File">
                        <DirectEvents>
                            <Click OnEvent="DownloadFileUploaded" IsUpload="true" Success="NawadataDirect.DownloadFileUploaded_Direct({isUpload : true});">
                                <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_EDD_Submit" runat="server" Icon="Disk" Text="Submit">
                <DirectEvents>
                    <Click OnEvent="btn_EDD_Submit_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_EDD_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_EDD_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <%--<ext:Window runat="server" ID="WindowPopUpNote" Maximized="true" Title="Saving With WorkFlow Notes" Modal="true" Layout="FitLayout" ButtonAlign="Center" Hidden="true">
        <Items>
            <%--<ext:TextArea runat="server" ID="txtNotes" Padding="20" AllowBlank="false" BlankText="Correction notes is required" AnchorHorizontal="80%" />--%>
            <%--<ext:TextArea runat="server" ID="txtNotes" Padding="20" AllowBlank="true" AnchorHorizontal="80%" />
        </Items>
        <Buttons>
            <ext:Button runat="server" ID="BtnSaveNotes" Text="Save">
                <DirectEvents>
                    <Click OnEvent="BtnSaveNotes_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.7});" />
            <Resize Handler="#{windowpopup}.center()" />
        </Listeners>
    </ext:Window>--%>
    
    <ext:Window runat="server" ID="WindowPopUpAttachment" Title="Attachment" Layout="AnchorLayout" Width="600" ButtonAlign="Center" Hidden="true" Closable="false">
        <Items>
            <ext:Panel ID="pnlContent" runat="server" BodyPadding="0" MaxHeight="390">
                <Loader Url="AML_EDD_UploadAttachment.aspx" Mode="Frame" NoCache="true" runat="server" AutoLoad="false" >
                    <%--<Params>
                        <ext:Parameter Name="fileName" Mode="Raw" Value="0">
                        </ext:Parameter>
                    </Params>--%>
                </Loader>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_EDD_Attachment_Submit" runat="server" Icon="Disk" Text="Save Attachment" Hidden="true" >
                <DirectEvents>
                    <Click OnEvent="Btn_EDD_Attachment_Submit_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_EDD_Attachment_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="Btn_EDD_Attachment_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    
    <ext:Window ID="window_Import_Customer" runat="server" Modal="true" Hidden="true" BodyStyle="padding:0px" AutoScroll="true" ButtonAlign="Center" MinHeight="430">
        <Items>
            <ext:GridPanel ID="gp_Import_Customer" runat="server" ClientIDMode="Static">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store_Import_Customer" runat="server" IsPagingStore="true" RemoteFilter="true"  RemoteSort="true" 
                        OnReadData="Store_ReadData_Import_Customer" RemotePaging="true" ClientIDMode="Static" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="model_Import_Customer" IDProperty="CIFNo">
                                <Fields>
                                    <ext:ModelField Name="CIFNo" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CUSTOMERNAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="RISK_RATING_NAME" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="filterHeader_Import_Customer" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumber_Import_Customer" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="col_Import_Customer_CIFNo" runat="server" DataIndex="CIFNo" Text="CIF No" Width="150"></ext:Column>
                        <ext:Column ID="col_Import_Customer_CUSTOMERNAME" runat="server" DataIndex="CUSTOMERNAME" Text="Customer Name" Width="150" ></ext:Column>
                        <ext:Column ID="col_Import_Customer_FK_AML_RISK_CODE" runat="server" DataIndex="RISK_RATING_NAME" Text="Risk Rating" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="cc_Import_Customer" runat="server" Text="Action">

                        <Commands>
                            <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
                                <ToolTip Text="Select"></ToolTip>
                            </ext:GridCommand>
                        </Commands>

                        <DirectEvents>
                            <Command OnEvent="gc_Import_Customer">
                                <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                                <ExtraParams>
                                    <ext:Parameter Name="CIFNo" Value="record.data.CIFNo" Mode="Raw"></ext:Parameter>
                                    <ext:Parameter Name="CUSTOMERNAME" Value="record.data.CUSTOMERNAME" Mode="Raw"></ext:Parameter>
                                </ExtraParams>
                            </Command>
                        </DirectEvents>
                    </ext:CommandColumn>
                </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_Import_Customer" runat="server" HideRefresh="false" >
                        <Items>  
                            <%--<ext:Button runat="server" Text="Close Picker" ID="Button5">
                                <Listeners>
                                    <Click Handler="#{window_Import_Party}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>--%>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.9});" />
            <Resize Handler="#{window_Import_Party}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_Import_Customer_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="Btn_Import_Customer_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    
    <ext:Window runat="server" ID="WindowPopUpDataCustomer" Maximized="true" Modal="true" Layout="FitLayout" ButtonAlign="Center" Title="Data Customer Profile" Hidden="true">
        <Items>
            <%--<ext:Panel ID="pnlContentCustomerProfile" runat="server">
                <Loader Url="CustomerProfileDetail.aspx" Mode="Frame" NoCache="true" runat="server" AutoLoad="false">
                    <LoadMask ShowMask="true"></LoadMask>
                </Loader>
            </ext:Panel>--%>
            <ext:FormPanel ID="FormPanel1" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" AutoScroll="true">
                <Items>
                    <ext:Panel runat="server" Layout="ColumnLayout" AnchorHorizontal="100%" ID="Panel3" Border="true">
                        <Items>
                            <ext:Panel runat="server" ColumnWidth="0.48" Border="false" Layout="AnchorLayout" BodyPadding="10">
                                <Content>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_CIF" FieldLabel="CIF" AnchorHorizontal="80%" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_GCN" FieldLabel="GCN" AnchorHorizontal="80%" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_CustomerName" FieldLabel="Customer Name" AnchorHorizontal="80%" LabelWidth="150"></ext:DisplayField>
                                </Content>
                            </ext:Panel>
                            <ext:Panel runat="server" ColumnWidth="0.48" Border="false" Layout="AnchorLayout" BodyPadding="10">
                                <Content>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_DateOfData" FieldLabel="Date of Data" AnchorHorizontal="80%" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_LastDueDiligence" FieldLabel="Date Last Due Diligence" AnchorHorizontal="80%" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_Status" FieldLabel="Status" AnchorHorizontal="80%" LabelWidth="150"></ext:DisplayField>
                                </Content>
                            </ext:Panel>
                            <%--<ext:Panel runat="server" ColumnWidth="0.33" Border="false" Layout="AnchorLayout" BodyPadding="10">
                                <Content>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_AMLRiskScore" FieldLabel="AML Risk Score" AnchorHorizontal="80%" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_AMLRisk" FieldLabel="AML Risk" AnchorHorizontal="80%" LabelWidth="150"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_blank1" FieldLabel="" AnchorHorizontal="80%" LabelWidth="200" LabelSeparator=""></ext:DisplayField>
                                </Content>
                            </ext:Panel>--%>
                        </Items>
                    </ext:Panel>
                    <ext:Panel runat="server" Layout="ColumnLayout" AnchorHorizontal="100%" ID="Panel4" Border="true">
                        <Items>
                            <ext:Panel runat="server" ColumnWidth="0.495" Border="false" Layout="AnchorLayout" BodyPadding="10">
                                <Content>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_DateOfBirth" FieldLabel="Date of Birth" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_PlaceOfBirth" FieldLabel="Place of Birth" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_MotherName" FieldLabel="Mother Name" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_Gender" FieldLabel="Gender" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_MartialStatus" FieldLabel="Martial Status" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_Citizenship" FieldLabel="Citizenship" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_WorkPlace" FieldLabel="Work Place" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_CustomerType" FieldLabel="Customer Type" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_CustomerSubType" FieldLabel="Customer Sub Type" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                </Content>
                            </ext:Panel>
                            <ext:Panel runat="server" ColumnWidth="0.495" Border="false" Layout="AnchorLayout" BodyPadding="10">
                                <Content>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_BusinessType" FieldLabel="Business Type" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_Industry" FieldLabel="Industry" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_OpeningDate" FieldLabel="Opening Date" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_CreationEntity" FieldLabel="Creation Entity" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_CreationBranch" FieldLabel="Creation Branch" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_SalesOfficer" FieldLabel="Sales Officer" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_SourceOfFound" FieldLabel="Source of Fund" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_PurposeOfFound" FieldLabel="Purpose of Fund" AnchorHorizontal="80%" LabelWidth="200"></ext:DisplayField>
                                    <ext:DisplayField runat="server" ID="CustomerDetail_blank2" FieldLabel="" AnchorHorizontal="80%" LabelWidth="200" LabelSeparator=""></ext:DisplayField>
                                </Content>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_EDD_CustomerDetail_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="Btn_EDD_CustomerDetail_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
</asp:Content>

