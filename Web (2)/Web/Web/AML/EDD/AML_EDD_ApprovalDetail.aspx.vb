﻿Imports Ext
Imports CasemanagementDAL
Imports Elmah
Imports System.Data
Imports CasemanagementBLL

Partial Class AML_EDD_ApprovalDetail
    Inherits ParentPage

    Public Property IDModule() As String
        Get
            Return Session("AML_EDD_ApprovalDetail.IDModule")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_ApprovalDetail.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("AML_EDD_ApprovalDetail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AML_EDD_ApprovalDetail.IDUnik") = value
        End Set
    End Property

    Public Property objApproval() As NawaDAL.ModuleApproval
        Get
            Return Session("AML_EDD_ApprovalDetail.objApproval")
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            Session("AML_EDD_ApprovalDetail.objApproval") = value
        End Set
    End Property

    Public Property objEDD_CLASS() As CasemanagementBLL.OneFCC_EDD_CLASS
        Get
            Return Session("AML_EDD_ApprovalDetail.objEDD_CLASS")
        End Get
        Set(ByVal value As CasemanagementBLL.OneFCC_EDD_CLASS)
            Session("AML_EDD_ApprovalDetail.objEDD_CLASS") = value
        End Set
    End Property

    Public Property objEDD_CLASS_BEFORE() As CasemanagementBLL.OneFCC_EDD_CLASS
        Get
            Return Session("AML_EDD_ApprovalDetail.objEDD_CLASS_BEFORE")
        End Get
        Set(ByVal value As CasemanagementBLL.OneFCC_EDD_CLASS)
            Session("AML_EDD_ApprovalDetail.objEDD_CLASS_BEFORE") = value
        End Set
    End Property

    Public Property IsFromWorkFlow() As Boolean
        Get
            Return Session("AML_EDD_ApprovalDetail.IsFromWorkFlow")
        End Get
        Set(ByVal value As Boolean)
            Session("AML_EDD_ApprovalDetail.IsFromWorkFlow") = value
        End Set
    End Property

    Public Property WorkFlowProgress() As MWorkFlow_Progress
        Get
            Return Session("AML_EDD_ApprovalDetail.WorkFlowProgress")
        End Get
        Set(ByVal value As MWorkFlow_Progress)
            Session("AML_EDD_ApprovalDetail.WorkFlowProgress") = value
        End Set
    End Property

    Public Property intWorkflowDecision() As Integer
        Get
            Return Session("AML_EDD_ApprovalDetail.intWorkflowDecision")
        End Get
        Set(ByVal value As Integer)
            Session("AML_EDD_ApprovalDetail.intWorkflowDecision") = value
        End Set
    End Property

    Public Property WorkFlowHistory() As List(Of MWorkFlow_History)
        Get
            Return Session("AML_EDD_ApprovalDetail.WorkFlowHistory")
        End Get
        Set(ByVal value As List(Of MWorkFlow_History))
            Session("AML_EDD_ApprovalDetail.WorkFlowHistory") = value
        End Set
    End Property

    'Public Property ValdaterMapping() As Boolean
    '    Get
    '        Return Session("AML_EDD_ApprovalDetail.ValdaterMapping")
    '    End Get
    '    Set(ByVal value As Boolean)
    '        Session("AML_EDD_ApprovalDetail.ValdaterMapping") = value
    '    End Set
    'End Property

    Public Property DataTabelEDDScore As DataTable
        Get
            Return Session("AML_EDD_ApprovalDetail.DataTabelEDDScore")
        End Get
        Set(ByVal value As DataTable)
            Session("AML_EDD_ApprovalDetail.DataTabelEDDScore") = value
        End Set
    End Property

    Public Property DataTabelEDDScoreBefore As DataTable
        Get
            Return Session("AML_EDD_ApprovalDetail.DataTabelEDDScoreBefore")
        End Get
        Set(ByVal value As DataTable)
            Session("AML_EDD_ApprovalDetail.DataTabelEDDScoreBefore") = value
        End Set
    End Property

    Public Property InputNullPronoun() As String
        Get
            Return Session("AML_EDD_ApprovalDetail.InputNullPronoun")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_ApprovalDetail.InputNullPronoun") = value
        End Set
    End Property

    Public Property Filetodownload() As String
        Get
            Return Session("AML_EDD_ApprovalDetail.Filetodownload")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_ApprovalDetail.Filetodownload") = value
        End Set
    End Property

    Public Property FileAttachmentName() As String
        Get
            Return Session("AML_EDD_UploadAttachment.FileAttachmentName")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_UploadAttachment.FileAttachmentName") = value
        End Set
    End Property

    Public Property FileAttachmentByte() As Byte()
        Get
            Return Session("AML_EDD_UploadAttachment.FileAttachmentByte")
        End Get
        Set(ByVal value As Byte())
            Session("AML_EDD_UploadAttachment.FileAttachmentByte") = value
        End Set
    End Property

    Public Property IsUsingScorePointAffectingAMLCustomer() As String
        Get
            Return Session("AML_EDD_ApprovalDetail.IsUsingScorePointAffectingAMLCustomer")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_ApprovalDetail.IsUsingScorePointAffectingAMLCustomer") = value
        End Set
    End Property

    Sub ClearSession()
        'ValdaterMapping = False
        objApproval = Nothing
        objEDD_CLASS = New OneFCC_EDD_CLASS
        objEDD_CLASS_BEFORE = New OneFCC_EDD_CLASS
        WorkFlowHistory = New List(Of MWorkFlow_History)
        intWorkflowDecision = False
        Filetodownload = Nothing
        FileAttachmentName = Nothing
        FileAttachmentByte = Nothing

        DataTabelEDDScore = New DataTable
        DataTabelEDDScoreBefore = New DataTable
        LoadColumnDataTabelEDDScore()
        InputNullPronoun = Nothing
        IsUsingScorePointAffectingAMLCustomer = Nothing

        Dim strQuery = " SELECT ParameterValue FROM AML_Global_Parameter WHERE PK_GlobalReportParameter_ID = 4 "
        Dim dataRowGetParameter As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQuery)
        If dataRowGetParameter IsNot Nothing AndAlso Not IsDBNull(dataRowGetParameter("ParameterValue")) Then
            InputNullPronoun = dataRowGetParameter("ParameterValue")
        End If

        strQuery = " SELECT ParameterValue FROM AML_Global_Parameter WHERE PK_GlobalReportParameter_ID = 6 "
        Dim dataRowGetParameterAffectingCustomer As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQuery)
        If dataRowGetParameterAffectingCustomer IsNot Nothing AndAlso Not IsDBNull(dataRowGetParameterAffectingCustomer("ParameterValue")) Then
            IsUsingScorePointAffectingAMLCustomer = dataRowGetParameterAffectingCustomer("ParameterValue")
        End If
    End Sub

    Protected Sub LoadColumnDataTabelEDDScore()
        Try
            DataTabelEDDScore.Columns.Add(New DataColumn("Question_ID", GetType(Long)))
            DataTabelEDDScore.Columns.Add(New DataColumn("Score", GetType(Integer)))
            DataTabelEDDScore.Columns.Add(New DataColumn("Type", GetType(String)))

            DataTabelEDDScoreBefore.Columns.Add(New DataColumn("Question_ID", GetType(Long)))
            DataTabelEDDScoreBefore.Columns.Add(New DataColumn("Score", GetType(Integer)))
            DataTabelEDDScoreBefore.Columns.Add(New DataColumn("Type", GetType(String)))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If

                IDModule = Request.Params("ModuleID")
                Dim intModuleID As Integer = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                ClearSession()

                FormPanelInput.Title = ObjModule.ModuleLabel & " - Approval"

                LoadModuleApproval()

                'If ValdaterMapping Then
                '    Load_Questionnaire()
                '    Load_Answer_Before()
                '    Load_Answer()
                'Else
                '    If objEDD_CLASS IsNot Nothing Then
                '        Load_Questionnaire_After()
                '        Load_Answer()
                '    End If
                '    If objEDD_CLASS_BEFORE IsNot Nothing Then
                '        Load_Questionnaire_Before()
                '        Load_Answer_Before()
                '    End If
                'End If

                If objEDD_CLASS IsNot Nothing Then
                    LoadToFormPanel(FormPanelCustomer, "Customer", "new")
                    If objEDD_CLASS.objEDD.IsHaveBeneficialOwner Then
                        LoadToFormPanel(FormPanelBenificialOwner, "BO", "new")
                        FormPanelBenificialOwner.Hidden = False
                    End If
                    Load_Answer()
                End If
                If objEDD_CLASS_BEFORE IsNot Nothing Then
                    If objEDD_CLASS_BEFORE.objEDD.PK_OneFCC_EDD_ID <> Nothing Then
                        LoadToFormPanel(FormPanelCustomer_Before, "Customer", "old")
                        If objEDD_CLASS_BEFORE.objEDD.IsHaveBeneficialOwner Then
                            LoadToFormPanel(FormPanelBenificialOwner_Before, "BO", "old")
                            FormPanelBenificialOwner_Before.Hidden = False
                        End If
                        Load_Answer_Before()
                    End If
                End If

                WorkFlowProgress = OneFCC_EDD_BLL.IsFromWorkFlowByStatus(ObjModule, objApproval.ModuleKey, 3)
                If WorkFlowProgress IsNot Nothing Then
                    IsFromWorkFlow = True
                End If

                btn_EDD_Revise.Hidden = (Not IsFromWorkFlow)
                Panel2.Hidden = (Not IsFromWorkFlow)
                If IsFromWorkFlow Then
                    WorkFlowHistory = OneFCC_EDD_BLL.GetDataWorkFlowHistory(ObjModule, objApproval.ModuleKey, objApproval.PK_ModuleApproval_ID)
                    BindWorkFlowHistory(StoreWorkFlowHistory, WorkFlowHistory)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Enum EnumExtType
        DateField = 1
        DropDownField = 2
        NumberField = 4
        TextField = 5
        Radio = 6
        DisplayField = 7
        FileUpload = 8
        Password = 9
        HTMLEditor = 10
        FormulaField = 11
        MultiCombo = 12
    End Enum

    '------- LOAD MODULE APPROVAL
    Protected Sub LoadModuleApproval()
        Try
            objApproval = NawaBLL.ModuleApprovalBLL.GetModuleApprovalByID(IDUnik)
            If Not objApproval Is Nothing Then
                txt_ModuleApproval_ID.Value = objApproval.PK_ModuleApproval_ID
                Dim objModuleToApprove = NawaBLL.ModuleBLL.GetModuleByModuleName(objApproval.ModuleName)
                If objModuleToApprove IsNot Nothing Then
                    txt_ModuleLabel.Value = objModuleToApprove.ModuleLabel
                End If
                txt_ModuleKey.Text = objApproval.ModuleKey
                txt_Action.Text = NawaBLL.ModuleBLL.GetModuleActionNamebyID(objApproval.PK_ModuleAction_ID)

                Dim objUser = NawaBLL.MUserBLL.GetMuserbyUSerId(objApproval.CreatedBy)
                If objUser IsNot Nothing Then
                    txt_CreatedBy.Text = objApproval.CreatedBy & " - " & objUser.UserName
                Else
                    txt_CreatedBy.Text = objApproval.CreatedBy
                End If
                txt_CreatedDate.Text = objApproval.CreatedDate.Value.ToString("dd-MMM-yyyy")

                'Set Panel Visibility
                'If objApproval.PK_ModuleAction_ID = 1 Then
                '    pnlDataBefore.Hidden = True
                '    pnlDataAfter.ColumnWidth = 1
                '    pnlDataAfter.Title = "New Data"
                'End If

                Dim strQuery As String = " SELECT PK_ONEFCC_EDD_NOTES_HISTORY_ID, NOTES, FILE_ATTACHMENTName, CreatedBy, CreatedDate "
                strQuery = strQuery & " FROM OneFCC_EDD_Notes_History WHERE UNIQUE_KEY = '" & objApproval.ModuleKey & "' ORDER BY CreatedDate DESC "
                'Dim strQuery As String = " select history.PK_MWorkflow_History_ID,history.CreatedDate,history.ResponseDate,history.UserNameExecute,history.Notes, "
                'strQuery = strQuery & " case  when approval.PK_ModuleApproval_ID is not null then 'In Approval' end as HistoryStatus "
                'strQuery = strQuery & " from MWorkFlow_History history left join ModuleApproval approval on history.FK_ModuleApproval_ID = approval.PK_ModuleApproval_ID "
                'strQuery = strQuery & " where FK_Module_ID = 30017 and FK_Unik_ID = '" & objApproval.ModuleKey & "' order by ResponseDate desc "
                Dim dataNoteHistory As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery,)
                StoreNotesHistory.DataSource = dataNoteHistory
                StoreNotesHistory.DataBind()

                Select Case objApproval.PK_ModuleAction_ID
                    Case NawaBLL.Common.ModuleActionEnum.Insert
                        pnlDataBefore.Hidden = True
                        pnlDataAfter.ColumnWidth = 1
                        pnlDataAfter.Title = "New Data"
                        If Not String.IsNullOrEmpty(objApproval.ModuleField) Then
                            objEDD_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(CasemanagementBLL.OneFCC_EDD_CLASS))
                        End If
                    Case NawaBLL.Common.ModuleActionEnum.Update
                        If Not String.IsNullOrEmpty(objApproval.ModuleFieldBefore) Then
                            objEDD_CLASS_BEFORE = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(CasemanagementBLL.OneFCC_EDD_CLASS))
                        End If
                        If Not String.IsNullOrEmpty(objApproval.ModuleField) Then
                            objEDD_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(CasemanagementBLL.OneFCC_EDD_CLASS))
                        End If
                        'If objEDD_CLASS IsNot Nothing AndAlso objEDD_CLASS_BEFORE IsNot Nothing AndAlso objEDD_CLASS_BEFORE.objEDD IsNot Nothing AndAlso objEDD_CLASS.objEDD IsNot Nothing AndAlso
                        '    objEDD_CLASS.objEDD.FK_OneFCC_EDD_Type_Code = objEDD_CLASS_BEFORE.objEDD.FK_OneFCC_EDD_Type_Code AndAlso
                        '    objEDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Segment_Code = objEDD_CLASS_BEFORE.objEDD.FK_OneFCC_Question_Customer_Segment_Code AndAlso
                        '    objEDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Type_Code = objEDD_CLASS_BEFORE.objEDD.FK_OneFCC_Question_Customer_Type_Code Then
                        '    ValdaterMapping = True
                        'End If
                        cmb_EDD_TYPE_ID.AnchorHorizontal = "100%"
                        txt_CIFNo.AnchorHorizontal = "100%"
                        txt_Nama.AnchorHorizontal = "100%"
                        customer_NIK.AnchorHorizontal = "100%"
                        customer_Nationality.AnchorHorizontal = "100%"
                        customer_DOB.AnchorHorizontal = "100%"
                        customer_POB.AnchorHorizontal = "100%"
                        cmb_Customer_Type_ID.AnchorHorizontal = "100%"
                        cmb_Customer_Segment_ID.AnchorHorizontal = "100%"
                    Case NawaBLL.Common.ModuleActionEnum.Delete
                        pnlDataBefore.Hidden = True
                        pnlDataAfter.ColumnWidth = 1
                        pnlDataAfter.Title = "Deleted Data"
                        If Not String.IsNullOrEmpty(objApproval.ModuleField) Then
                            objEDD_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(CasemanagementBLL.OneFCC_EDD_CLASS))
                        End If
                End Select

                '' Add 27-Jan-2023 Display Risk Rating Score
                Dim RiskRatingScore As String = ""

                Dim strRiskRatingScore As String = "select isnull(cast ( Risk_Rating_Total_Score as varchar(50)),'') + ' ( ' + b.RISK_RATING_NAME + ' ) ' as RiskRatingScore"
                strRiskRatingScore += " FROM onefCC_EDD a "
                strRiskRatingScore += " JOIN AML_RISK_RATING As b  "
                strRiskRatingScore += " On a.FK_RISK_RATING_CODE = b.RISK_RATING_CODE "
                strRiskRatingScore += " where PK_OneFCC_EDD_ID = '" & objApproval.ModuleKey & "'"

                RiskRatingScore = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strRiskRatingScore, Nothing)
                txt_RiskRatingScore.Value = RiskRatingScore


                Dim paramRiskRatingDetail(0) As System.Data.SqlClient.SqlParameter
                paramRiskRatingDetail(0) = New System.Data.SqlClient.SqlParameter
                paramRiskRatingDetail(0).ParameterName = "@PK_EDD_ID"
                paramRiskRatingDetail(0).Value = objApproval.ModuleKey
                paramRiskRatingDetail(0).DbType = SqlDbType.VarChar

                Dim dtRiskRatingDetail As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_OneFCC_CaseManagement_GetRiskRatingDetailbyPKEDD", paramRiskRatingDetail)

                If dtRiskRatingDetail Is Nothing Then
                    dtRiskRatingDetail = New DataTable
                End If

                If dtRiskRatingDetail IsNot Nothing Then

                    'Bind to gridpanel
                    gp_RiskRatingDetail.GetStore.DataSource = dtRiskRatingDetail
                    gp_RiskRatingDetail.GetStore.DataBind()

                End If
                '' End 27-Jan-2023
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    '------- END OF LOAD MODULE APPROVAL

    '------- LOAD DATA BEFORE
    'Protected Sub LoadDataBefore()
    '    Try
    '        'If Not String.IsNullOrEmpty(objApproval.ModuleFieldBefore) Then
    '        '    objEDD_CLASS_BEFORE = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(CasemanagementBLL.OneFCC_EDD_CLASS))
    '        'End If

    '        Load_Answer_Before()

    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub Load_Questionnaire_Before(objpanelinput As Ext.Net.FormPanel, customerType As String)
        Try
            Dim strQuery As String = ""
            Dim intSequence As Integer
            Dim intSequenceChild As Integer

            Dim Customer_Type_ID As String = customerType
            Dim Customer_Segment_ID As String = objEDD_CLASS_BEFORE.objEDD.FK_OneFCC_Question_Customer_Segment_Code
            Dim EDD_Type_ID As String = objEDD_CLASS_BEFORE.objEDD.FK_OneFCC_EDD_Type_Code

            strQuery = "SELECT DISTINCT questiongroup.PK_OneFCC_Question_Group_ID, questiongroup.Question_Group_Name, questiongroup.Sequence, questiongroup.Question_Group_Code"
            strQuery = strQuery & " FROM OneFCC_Question question"
            strQuery = strQuery & " JOIN OneFCC_Question_Mapping_EDD mapping ON question.PK_OneFCC_Question_ID = mapping.FK_OneFCC_Question_ID"
            strQuery = strQuery & " JOIN OneFCC_Question_Group questiongroup ON questiongroup.Question_Group_Code = mapping.FK_OneFCC_Question_Group_Code"
            strQuery = strQuery & " WHERE mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
            strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
            strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
            strQuery = strQuery & " ORDER BY questiongroup.Sequence"

            Dim dtQuestionGroup As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
            For Each row In dtQuestionGroup.Rows
                strQuery = "SELECT mapping.PK_OneFCC_Question_Mapping_EDD_ID, question.PK_OneFCC_Question_ID, question.QUESTION, question.IS_REQUIRED, question.FK_ExtType_ID,"
                strQuery = strQuery & " question.Table_Reference_Name, question.Table_Reference_Field_Key, question.Table_Reference_Field_Display_Name, question.Table_Reference_Filter"
                strQuery = strQuery & " ,point.PK_OneFCC_EDD_Mapping_Question_Answer_Point_ID"
                strQuery = strQuery & " FROM OneFCC_Question question"
                strQuery = strQuery & " JOIN OneFCC_Question_Mapping_EDD mapping on question.PK_OneFCC_Question_ID = mapping.FK_OneFCC_Question_ID"
                strQuery = strQuery & " left join OneFCC_EDD_Mapping_Question_Answer_Point point on mapping.PK_OneFCC_Question_Mapping_EDD_ID = point.FK_OneFCC_Question_Mapping_EDD_ID"
                strQuery = strQuery & " WHERE mapping.FK_OneFCC_Question_Group_Code = '" & row("Question_Group_Code") & "'"
                strQuery = strQuery & " AND ISNULL(mapping.FK_OneFCC_Question_Parent_ID,'')=''"
                strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
                strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
                strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
                strQuery = strQuery & " ORDER BY mapping.Sequence"

                Dim dtQuestion As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
                intSequence = 1

                If dtQuestion.Rows.Count > 0 Then
                    Dim objPanel = ExtPanel(objpanelinput, objpanelinput.ID & "_Panel_" & row("PK_OneFCC_Question_Group_ID"), row("Sequence") & ". " & row("Question_Group_Name"), False)
                    For Each question In dtQuestion.Rows
                        Dim isRequired As Boolean = False
                        If Not IsDBNull(question("IS_REQUIRED")) Then
                            isRequired = question("IS_REQUIRED")
                        Else
                            isRequired = False
                        End If

                        Select Case CType(question("FK_ExtType_ID"), EnumExtType)
                            Case EnumExtType.TextField
                                ExtText(objPanel, intSequence & ". " & question("QUESTION"), "ANSWERBefore_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequence)
                            Case EnumExtType.NumberField
                                ExtNumber(objPanel, intSequence & ". " & question("QUESTION"), "ANSWERBefore_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 0, intSequence, Long.MinValue, Long.MaxValue)
                            Case EnumExtType.DateField
                                ExtDate(objPanel, intSequence & ". " & question("QUESTION"), "ANSWERBefore_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 150, intSequence)
                            Case EnumExtType.DropDownField
                                Dim question_Table_Reference_Filter As String
                                If Not IsDBNull(question("Table_Reference_Filter")) Then
                                    question_Table_Reference_Filter = question("Table_Reference_Filter")
                                Else
                                    question_Table_Reference_Filter = ""
                                End If
                                If Not IsDBNull(question("Table_Reference_Name")) AndAlso Not IsDBNull(question("Table_Reference_Field_Key")) AndAlso Not IsDBNull(question("Table_Reference_Field_Display_Name")) Then
                                    'ExtCombo(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequence, question("Table_Reference_Name"), question("Table_Reference_Field_Key"), question("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "", isUsingTriger, customerType)
                                    ExtCombo(objPanel, intSequence & ". " & question("QUESTION"), "ANSWERBefore_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequence, question("Table_Reference_Name"), question("Table_Reference_Field_Key"), question("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "")
                                Else
                                    Throw New ApplicationException("Incomplete Question Data On Question ID " & question("PK_OneFCC_Question_ID") & ", Please Report this issue to Admin")
                                End If
                            Case EnumExtType.Radio
                                ExtRadio(objPanel, intSequence & ". " & question("QUESTION"), "ANSWERBefore_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequence)
                        End Select

                        intSequence += 1

                        strQuery = "SELECT mapping.PK_OneFCC_Question_Mapping_EDD_ID, ofq.PK_OneFCC_Question_ID, ofq.QUESTION, ofq.IS_REQUIRED, ofq.FK_ExtType_ID, ofq.Table_Reference_Name, ofq.Table_Reference_Field_Key, ofq.Table_Reference_Field_Display_Name, ofq.Table_Reference_Filter"
                        strQuery = strQuery & " ,point.PK_OneFCC_EDD_Mapping_Question_Answer_Point_ID"
                        strQuery = strQuery & " FROM OneFCC_Question ofq"
                        strQuery = strQuery & " JOIN OneFCC_Question_Mapping_EDD mapping"
                        strQuery = strQuery & " left join OneFCC_EDD_Mapping_Question_Answer_Point point on mapping.PK_OneFCC_Question_Mapping_EDD_ID = point.FK_OneFCC_Question_Mapping_EDD_ID"
                        strQuery = strQuery & " on ofq.PK_OneFCC_Question_ID = mapping.FK_OneFCC_Question_ID"
                        strQuery = strQuery & " WHERE ISNULL(mapping.FK_OneFCC_Question_Parent_ID,'')=" & question("PK_OneFCC_Question_Mapping_EDD_ID")
                        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Group_Code = '" & row("Question_Group_Code") & "'"
                        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
                        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
                        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
                        strQuery = strQuery & " ORDER BY mapping.Sequence"

                        Dim dtQuestionChild As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
                        intSequenceChild = 1
                        For Each child In dtQuestionChild.Rows
                            'isRequired = child("IS_REQUIRED")
                            If Not IsDBNull(child("IS_REQUIRED")) Then
                                isRequired = child("IS_REQUIRED")
                            Else
                                isRequired = False
                            End If
                            Dim strPrefix As String = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & LCase(Convert.ToChar(intSequenceChild + 64))
                            Select Case CType(child("FK_ExtType_ID"), EnumExtType)
                                Case EnumExtType.TextField
                                    ExtText(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWERBefore_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequenceChild)
                                Case EnumExtType.NumberField
                                    ExtNumber(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWERBefore_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 0, intSequenceChild, Long.MinValue, Long.MaxValue)
                                Case EnumExtType.DateField
                                    ExtDate(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWERBefore_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 150, intSequenceChild)
                                Case EnumExtType.DropDownField
                                    Dim question_Table_Reference_Filter As String
                                    If Not IsDBNull(child("Table_Reference_Filter")) Then
                                        question_Table_Reference_Filter = child("Table_Reference_Filter")
                                    Else
                                        question_Table_Reference_Filter = ""
                                    End If
                                    If Not IsDBNull(child("Table_Reference_Name")) AndAlso Not IsDBNull(child("Table_Reference_Field_Key")) AndAlso Not IsDBNull(child("Table_Reference_Field_Display_Name")) Then
                                        'ExtCombo(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequenceChild, child("Table_Reference_Name"), child("Table_Reference_Field_Key"), child("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "", isUsingTriger, customerType)
                                        ExtCombo(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWERBefore_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequenceChild, child("Table_Reference_Name"), child("Table_Reference_Field_Key"), child("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "")
                                    Else
                                        Throw New ApplicationException("Incomplete Question Data On Question ID " & child("PK_OneFCC_Question_ID") & ", Please Report this issue to Admin")
                                    End If
                                Case EnumExtType.Radio
                                    ExtRadio(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWERBefore_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequenceChild)
                            End Select

                            intSequenceChild += 1
                        Next
                    Next
                End If
            Next
            If IsUsingScorePointAffectingAMLCustomer = "1" Then
                Dim objPanelRisk = ExtPanel(objpanelinput, objpanelinput.ID & "_Panel_Risk", "", True)
                Dim typeclass As CasemanagementDAL.OneFCC_Question_Customer_Type = OneFCC_EDD_BLL.GetEDDCustomerTypeByCode(customerType)
                ExtLabel(objPanelRisk, objPanelRisk.ID & "_" & customerType & "_Label", "Risk Rating " & typeclass.Description, 0.5)
                ExtLabel(objPanelRisk, objPanelRisk.ID & "_" & customerType & "_Score", "0", 0.1)
                ExtLabel(objPanelRisk, objPanelRisk.ID & "_" & customerType & "_RiskRating", "Low", 0.4)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Load_Answer_Before()
        Try
            If Ext.Net.X.IsAjaxRequest Then
                Exit Sub
            End If

            If objEDD_CLASS_BEFORE IsNot Nothing AndAlso objEDD_CLASS_BEFORE.objEDD IsNot Nothing Then
                'Load Header
                Dim Customer_Type_ID As String = objEDD_CLASS_BEFORE.objEDD.FK_OneFCC_Question_Customer_Type_Code
                Dim Customer_Segment_ID As String = objEDD_CLASS_BEFORE.objEDD.FK_OneFCC_Question_Customer_Segment_Code
                Dim EDD_Type_ID As String = objEDD_CLASS_BEFORE.objEDD.FK_OneFCC_EDD_Type_Code

                With objEDD_CLASS_BEFORE.objEDD
                    Dim segementclass As CasemanagementDAL.OneFCC_Question_Customer_Segment = OneFCC_EDD_BLL.GetEDDCustomerSegmentByCode(Customer_Segment_ID)
                    Dim typeclass As CasemanagementDAL.OneFCC_Question_Customer_Type = OneFCC_EDD_BLL.GetEDDCustomerTypeByCode(Customer_Type_ID)
                    Dim objEDDType As CasemanagementDAL.OneFCC_EDD_Type = OneFCC_EDD_BLL.GetEDDTypeByCode(EDD_Type_ID)

                    If segementclass IsNot Nothing AndAlso typeclass IsNot Nothing AndAlso objEDDType IsNot Nothing Then
                        cmb_Customer_Type_ID_Before.SetTextWithTextValue(typeclass.Customer_Type_Code, typeclass.Description)
                        cmb_Customer_Segment_ID_Before.SetTextWithTextValue(segementclass.Customer_Segment_Code, segementclass.Description)
                        cmb_EDD_TYPE_ID_Before.SetTextWithTextValue(objEDDType.EDD_Type_Code, objEDDType.EDD_Type_Name)
                        If Not String.IsNullOrEmpty(cmb_EDD_TYPE_ID_Before.StringValue) Then
                            If cmb_EDD_TYPE_ID_Before.StringValue = "C" Then
                                'nDSDropDownField_CIFNo.IsHidden = False
                                'btn_Import_Customer.Hidden = False
                                txt_CIFNo_Before.Hidden = False
                                txt_Nama_Before.Hidden = False
                                txt_Nama_Before.ReadOnly = True
                                txt_Nama_Before.FieldStyle = "background-color: #ddd"
                                btn_OpenWindowCustomer_Before.Hidden = False

                                txt_CIFNo_Before.Value = .CIFNo
                            ElseIf cmb_EDD_TYPE_ID_Before.StringValue = "CP" Then
                                txt_Nama_Before.Hidden = False
                                customer_NIK_Before.Hidden = False
                                customer_Nationality_Before.IsHidden = False
                                customer_DOB_Before.Hidden = False
                                customer_POB_Before.Hidden = False
                                customer_DOB_Before.Format = NawaBLL.SystemParameterBLL.GetDateFormat()

                                If objApproval IsNot Nothing AndAlso objApproval.ModuleKey IsNot Nothing Then
                                    Dim strQuery As String = "SELECT TOP 1 * FROM OneFCC_EDD_Additional_Info_Customer_Prospect WHERE UNIQUE_KEY = '" & objApproval.ModuleKey & "'"
                                    Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                    If drResult IsNot Nothing Then
                                        If Not IsDBNull(drResult("NIK")) Then
                                            customer_NIK_Before.Value = drResult("NIK")
                                        End If
                                        If Not IsDBNull(drResult("DATEOFBIRTH")) Then
                                            customer_DOB_Before.Value = drResult("DATEOFBIRTH")
                                        End If
                                        If Not IsDBNull(drResult("PLACEOFBIRTH")) Then
                                            customer_POB_Before.Value = drResult("PLACEOFBIRTH")
                                        End If
                                        If Not IsDBNull(drResult("Nationality")) Then
                                            Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_COUNTRY WHERE FK_AML_COUNTRY_Code = '" & drResult("Nationality") & "'"
                                            Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                                            If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("FK_AML_COUNTRY_Code")) AndAlso Not IsDBNull(tempDrResult("AML_COUNTRY_Name")) Then
                                                customer_Nationality_Before.SetTextWithTextValue(tempDrResult("FK_AML_COUNTRY_Code"), tempDrResult("AML_COUNTRY_Name"))
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Else
                        Throw New ApplicationException("Unable to find EDD Type Or Customer Type Or Customer Segment Before, Please contact admin to check this issue.")
                    End If

                    'If objEDDType IsNot Nothing Then
                    '    cmb_EDD_TYPE_ID_Before.SetTextWithTextValue(objEDDType.EDD_Type_Code, objEDDType.EDD_Type_Name)
                    'End If
                    'If typeclass IsNot Nothing Then
                    '    cmb_Customer_Type_ID_Before.SetTextWithTextValue(typeclass.Customer_Type_Code, typeclass.Description)
                    'End If
                    'If segementclass IsNot Nothing Then
                    '    cmb_Customer_Segment_ID_Before.SetTextWithTextValue(segementclass.Customer_Segment_Code, segementclass.Description)
                    'End If
                    'If .FK_OneFCC_EDD_Type_Code IsNot Nothing Then
                    'Dim objEDDType = OneFCC_EDD_BLL.GetEDDTypeByCode(.FK_OneFCC_EDD_Type_Code)
                    '    If objEDDType IsNot Nothing Then
                    '        cmb_EDD_TYPE_ID_Before.SetTextWithTextValue(objEDDType.EDD_Type_Code, objEDDType.EDD_Type_Name)
                    '    End If
                    'End If
                    'txt_CIFNo_Before.Value = .CIFNo
                    'If Not String.IsNullOrEmpty(cmb_EDD_TYPE_ID_Before.StringValue) Then
                    '    If cmb_EDD_TYPE_ID_Before.StringValue = "C" Then
                    '        'nDSDropDownField_CIFNo_Before.IsHidden = False
                    '        txt_CIFNo_Before.Hidden = False
                    '        txt_Nama_Before.Hidden = False
                    '        txt_Nama_Before.ReadOnly = True
                    '        txt_Nama_Before.FieldStyle = "background-color: #ddd"
                    '        'nDSDropDownField_CIFNo_Before.SetTextWithTextValue(.CIFNo, .CIFNo)
                    '        txt_CIFNo_Before.Value = .CIFNo
                    '    ElseIf cmb_EDD_TYPE_ID_Before.StringValue = "CP" Then
                    '        txt_Nama_Before.Hidden = False
                    '    End If
                    'End If
                    txt_Nama_Before.Value = .Nama

                    'Load Answer
                    Load_AnswerCustomerType_Before(FormPanelCustomer_Before, "Customer")
                    If .IsHaveBeneficialOwner Then
                        checkbox_beneficialowner_Before.Checked = .IsHaveBeneficialOwner
                        Load_AnswerCustomerType_Before(FormPanelBenificialOwner_Before, "BO")
                    End If
                End With
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Load_AnswerCustomerType_Before(objpanelinput As Ext.Net.FormPanel, customerType As String)
        Dim Customer_Type_ID As String = customerType
        Dim Customer_Segment_ID As String = objEDD_CLASS_BEFORE.objEDD.FK_OneFCC_Question_Customer_Segment_Code
        Dim EDD_Type_ID As String = objEDD_CLASS_BEFORE.objEDD.FK_OneFCC_EDD_Type_Code

        'Load Answer
        Dim strQuery As String
        strQuery = " select mapping.PK_OneFCC_Question_Mapping_EDD_ID, question.FK_ExtType_ID, question.Question "
        strQuery = strQuery & " from OneFCC_Question_Mapping_EDD mapping"
        strQuery = strQuery & " join OneFCC_Question question"
        strQuery = strQuery & " on mapping.FK_OneFCC_Question_ID = question.PK_OneFCC_Question_ID"
        strQuery = strQuery & " where mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
        strQuery = strQuery & " and mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
        strQuery = strQuery & " and mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
        strQuery = strQuery & " and mapping.FK_OneFCC_Question_Parent_ID is null"

        Dim dtQuestion As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
        For Each item In dtQuestion.Rows
            Dim intQuestionID As Integer = item("PK_OneFCC_Question_Mapping_EDD_ID")
            Dim strQuestion As String = item("Question")

            Dim strAnswer As String = ""
            'If Not IsDBNull(item("Answer")) Then
            '    strAnswer = item("Answer")
            'End If
            Dim objAnswer = objEDD_CLASS_BEFORE.objListEDD_Detail.Where(Function(x) x.FK_OneFCC_Question_ID = intQuestionID And x.FK_OneFCC_Question_Parent_ID Is Nothing And x.Question = strQuestion).FirstOrDefault
            If objAnswer IsNot Nothing Then
                If Not String.IsNullOrEmpty(objAnswer.Answer) Then
                    strAnswer = objAnswer.Answer
                End If
                Select Case CType(item("FK_ExtType_ID"), EnumExtType)
                    Case EnumExtType.TextField
                        Dim objfield As TextField = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID)
                        If Not objfield Is Nothing Then
                            objfield.Value = strAnswer
                        End If
                    Case EnumExtType.NumberField
                        Dim objfield As NumberField = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID)
                        If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                            objfield.Value = CLng(strAnswer)
                        End If
                    Case EnumExtType.DateField
                        Dim objfield As DateField = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID)
                        If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                            objfield.Value = CDate(strAnswer)
                        End If
                    Case EnumExtType.DropDownField
                        Dim objfield As ComboBox = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID)
                        If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                            objfield.SetValueAndFireSelect(strAnswer)
                        End If
                    Case EnumExtType.Radio
                        Dim objfield As RadioGroup = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID)
                        Dim objFieldYes As Radio = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID & "_Yes")
                        Dim objFieldNo As Radio = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID & "_No")
                        If objfield IsNot Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                            If strAnswer = "Yes" Then
                                objFieldYes.Checked = True
                                objFieldNo.Checked = False
                            ElseIf strAnswer = "No" Then
                                objFieldYes.Checked = False
                                objFieldNo.Checked = True
                            End If
                        End If
                End Select
                If IsUsingScorePointAffectingAMLCustomer = "1" Then
                    ProgressScoring(intQuestionID, CType(item("FK_ExtType_ID"), EnumExtType), strAnswer, customerType, "old")
                End If
            End If

                strQuery = " select mapping.PK_OneFCC_Question_Mapping_EDD_ID, question.FK_ExtType_ID, question.Question "
            strQuery = strQuery & " from OneFCC_Question_Mapping_EDD mapping"
            strQuery = strQuery & " join OneFCC_Question question"
            strQuery = strQuery & " on mapping.FK_OneFCC_Question_ID = question.PK_OneFCC_Question_ID"
            strQuery = strQuery & " where mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
            strQuery = strQuery & " and mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
            strQuery = strQuery & " and mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
            strQuery = strQuery & " and mapping.FK_OneFCC_Question_Parent_ID = " & intQuestionID.ToString()

            Dim dtQuestionChild As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
            For Each child In dtQuestionChild.Rows
                Dim intQuestionParentID As Integer = intQuestionID
                intQuestionID = child("PK_OneFCC_Question_Mapping_EDD_ID")
                strQuestion = child("Question")

                strAnswer = ""
                'If Not IsDBNull(child("Answer")) Then
                '    strAnswer = child("Answer")
                'End If
                Dim objAnswerChild = objEDD_CLASS_BEFORE.objListEDD_Detail.Where(Function(x) x.FK_OneFCC_Question_ID = intQuestionID And x.FK_OneFCC_Question_Parent_ID = intQuestionParentID And x.Question = strQuestion).FirstOrDefault
                If objAnswerChild IsNot Nothing Then
                    If Not String.IsNullOrEmpty(objAnswerChild.Answer) Then
                        strAnswer = objAnswerChild.Answer
                    End If
                    Select Case CType(child("FK_ExtType_ID"), EnumExtType)
                        Case EnumExtType.TextField
                            Dim objfield As TextField = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID)
                            If Not objfield Is Nothing Then
                                objfield.Value = strAnswer
                            End If
                        Case EnumExtType.NumberField
                            Dim objfield As NumberField = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID)
                            If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                                objfield.Value = CLng(strAnswer)
                            End If
                        Case EnumExtType.DateField
                            Dim objfield As DateField = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID)
                            If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                                objfield.Value = CDate(strAnswer)
                            End If
                        Case EnumExtType.DropDownField
                            Dim objfield As ComboBox = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID)
                            If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                                objfield.SetValueAndFireSelect(strAnswer)
                            End If
                        Case EnumExtType.Radio
                            Dim objfield As RadioGroup = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID)
                            Dim objFieldYes As Radio = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID & "_Yes")
                            Dim objFieldNo As Radio = objpanelinput.FindControl("ANSWERBefore_" & intQuestionID & "_No")
                            If objfield IsNot Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                                If strAnswer = "Yes" Then
                                    objFieldYes.Checked = True
                                    objFieldNo.Checked = False
                                ElseIf strAnswer = "No" Then
                                    objFieldYes.Checked = False
                                    objFieldNo.Checked = True
                                End If
                            End If
                    End Select
                    If IsUsingScorePointAffectingAMLCustomer = "1" Then
                        ProgressScoring(intQuestionID, CType(child("FK_ExtType_ID"), EnumExtType), strAnswer, customerType, "old")
                    End If
                End If
            Next
        Next
    End Sub

    '------- END OF LOAD DATA BEFORE

    '------- LOAD DATA AFTER
    'Protected Sub LoadDataAfter()
    '    Try
    '        'objEDD_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(CasemanagementBLL.OneFCC_EDD_CLASS))

    '        Load_Answer()

    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub Load_Questionnaire()
    '    Try

    '        Dim strQuery As String = ""
    '        Dim intSequence As Integer
    '        Dim intSequenceChild As Integer

    '        Dim Customer_Type_ID As String = objEDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Type_Code
    '        Dim Customer_Segment_ID As String = objEDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Segment_Code
    '        Dim EDD_Type_ID As String = objEDD_CLASS.objEDD.FK_OneFCC_EDD_Type_Code

    '        'Load Question Group
    '        'strQuery = "SELECT DISTINCT ofqg.PK_OneFCC_Question_Group_ID, ofqg.Question_Group_Name, ofqg.Sequence"
    '        'strQuery = strQuery & " FROM OneFCC_Questionnaire ofq"
    '        'strQuery = strQuery & " JOIN OneFCC_Question_Group AS ofqg ON ofq.FK_OneFCC_Question_Group_ID = ofqg.PK_OneFCC_Question_Group_ID"
    '        'strQuery = strQuery & " WHERE ofqg.FK_OneFCC_Question_Type_Code = 'EDD'"
    '        'strQuery = strQuery & " ORDER BY ofqg.Sequence"
    '        strQuery = "SELECT DISTINCT questiongroup.PK_OneFCC_Question_Group_ID, questiongroup.Question_Group_Name, questiongroup.Sequence, questiongroup.Question_Group_Code"
    '        strQuery = strQuery & " FROM OneFCC_Question question"
    '        strQuery = strQuery & " JOIN OneFCC_Question_Mapping_EDD mapping ON question.PK_OneFCC_Question_ID = mapping.FK_OneFCC_Question_ID"
    '        strQuery = strQuery & " JOIN OneFCC_Question_Group questiongroup ON questiongroup.Question_Group_Code = mapping.FK_OneFCC_Question_Group_Code"
    '        strQuery = strQuery & " WHERE mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
    '        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
    '        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
    '        strQuery = strQuery & " ORDER BY questiongroup.Sequence"

    '        Dim dtQuestionGroup As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
    '        For Each row In dtQuestionGroup.Rows
    '            'Dim objPanel = ExtPanel(pnlDataAfter, "Panel_" & row("PK_OneFCC_Question_Group_ID"), row("Sequence") & ". " & row("Question_Group_Name"))
    '            'Dim objPanelBefore = ExtPanel(pnlDataBefore, "Panel_" & row("PK_OneFCC_Question_Group_ID") & "_Before", row("Sequence") & ". " & row("Question_Group_Name"))

    '            'Load Input
    '            'strQuery = "SELECT ofq.PK_OneFCC_Questionnaire_ID, ofq.QUESTION, ofq.IS_REQUIRED, ofq.FK_ExtType_ID, ofq.Table_Reference_Name, ofq.Table_Reference_Field_Key, ofq.Table_Reference_Field_Display_Name, ofq.Table_Reference_Filter"
    '            'strQuery = strQuery & " FROM OneFCC_Questionnaire ofq"
    '            'strQuery = strQuery & " WHERE ofq.FK_OneFCC_Question_Group_ID = " & row("PK_OneFCC_Question_Group_ID")
    '            'strQuery = strQuery & " AND ISNULL(ofq.FK_OneFCC_Questionnaire_Parent_ID,'')=''"    'Tampilkan dulu pertanyaan parent
    '            'strQuery = strQuery & " ORDER BY ofq.SEQUENCE"
    '            strQuery = "SELECT mapping.PK_OneFCC_Question_Mapping_EDD_ID, question.PK_OneFCC_Question_ID, question.QUESTION, question.IS_REQUIRED, question.FK_ExtType_ID,"
    '            strQuery = strQuery & " question.Table_Reference_Name, question.Table_Reference_Field_Key, question.Table_Reference_Field_Display_Name, question.Table_Reference_Filter"
    '            strQuery = strQuery & " FROM OneFCC_Question question"
    '            strQuery = strQuery & " JOIN OneFCC_Question_Mapping_EDD mapping on question.PK_OneFCC_Question_ID = mapping.FK_OneFCC_Question_ID"
    '            strQuery = strQuery & " WHERE mapping.FK_OneFCC_Question_Group_Code = '" & row("Question_Group_Code") & "'"
    '            strQuery = strQuery & " AND ISNULL(mapping.FK_OneFCC_Question_Parent_ID,'')=''"
    '            strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
    '            strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
    '            strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
    '            strQuery = strQuery & " ORDER BY mapping.Sequence"

    '            Dim dtQuestion As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
    '            intSequence = 1

    '            If dtQuestion.Rows.Count > 0 Then
    '                Dim objPanel = ExtPanel(FormPanelCustomer, "Panel_" & row("PK_OneFCC_Question_Group_ID"), row("Sequence") & ". " & row("Question_Group_Name"), False)
    '                Dim objPanelBefore = ExtPanel(FormPanelCustomer_Before, "Panel_" & row("PK_OneFCC_Question_Group_ID") & "_Before", row("Sequence") & ". " & row("Question_Group_Name"), False)
    '                For Each question In dtQuestion.Rows
    '                    Dim isRequired As Boolean = False
    '                    If Not IsDBNull(question("IS_REQUIRED")) Then
    '                        isRequired = question("IS_REQUIRED")
    '                    Else
    '                        isRequired = False
    '                    End If

    '                    Select Case CType(question("FK_ExtType_ID"), EnumExtType)
    '                        'Case EnumExtType.TextField
    '                        '    ExtText(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID"), isRequired, 8000, intSequence)
    '                        '    ExtText(objPanelBefore, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID") & "_Before", isRequired, 8000, intSequence)
    '                        'Case EnumExtType.NumberField
    '                        '    ExtNumber(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID"), isRequired, 0, intSequence, Long.MinValue, Long.MaxValue)
    '                        '    ExtNumber(objPanelBefore, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID") & "_Before", isRequired, 0, intSequence, Long.MinValue, Long.MaxValue)
    '                        'Case EnumExtType.DateField
    '                        '    ExtDate(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID"), isRequired, 150, intSequence)
    '                        '    ExtDate(objPanelBefore, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID") & "_Before", isRequired, 150, intSequence)
    '                        'Case EnumExtType.DropDownField
    '                        '    ExtCombo(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID"), isRequired, intSequence, question("Table_Reference_Name"), question("Table_Reference_Field_Key"), question("Table_Reference_Field_Display_Name"), question("Table_Reference_Filter"), "")
    '                        '    ExtCombo(objPanelBefore, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID") & "_Before", isRequired, intSequence, question("Table_Reference_Name"), question("Table_Reference_Field_Key"), question("Table_Reference_Field_Display_Name"), question("Table_Reference_Filter"), "")
    '                        'Case EnumExtType.Radio
    '                        '    ExtRadio(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID"), isRequired, 8000, intSequence)
    '                        '    ExtRadio(objPanelBefore, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID") & "_Before", isRequired, 8000, intSequence)

    '                            '    'Case EnumExtType.MultiCombo
    '                            '    '    ExtMultiCombo(FormParameter, item.Variable_Description, item.Variable_Name.Replace("@", ""), True, IntSequence, item.Tabel_Reference_Name, item.Table_Reference_Field_Key, item.Table_Reference_Field_Display_Name, item.Table_Reference_Filter, item.Tabel_Reference_Name_Alias)

    '                        Case EnumExtType.TextField
    '                            ExtText(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequence)
    '                            ExtText(objPanelBefore, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID") & "_Before", isRequired, 8000, intSequence)
    '                        Case EnumExtType.NumberField
    '                            ExtNumber(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 0, intSequence, Long.MinValue, Long.MaxValue)
    '                            ExtNumber(objPanelBefore, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID") & "_Before", isRequired, 0, intSequence, Long.MinValue, Long.MaxValue)
    '                        Case EnumExtType.DateField
    '                            ExtDate(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 150, intSequence)
    '                            ExtDate(objPanelBefore, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID") & "_Before", isRequired, 150, intSequence)
    '                        Case EnumExtType.DropDownField
    '                            Dim question_Table_Reference_Filter As String
    '                            If Not IsDBNull(question("Table_Reference_Filter")) Then
    '                                question_Table_Reference_Filter = question("Table_Reference_Filter")
    '                            Else
    '                                question_Table_Reference_Filter = ""
    '                            End If
    '                            If Not IsDBNull(question("Table_Reference_Name")) AndAlso Not IsDBNull(question("Table_Reference_Field_Key")) AndAlso Not IsDBNull(question("Table_Reference_Field_Display_Name")) Then
    '                                'ExtCombo(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequence, question("Table_Reference_Name"), question("Table_Reference_Field_Key"), question("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "", isUsingTriger, customerType)
    '                                ExtCombo(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequence, question("Table_Reference_Name"), question("Table_Reference_Field_Key"), question("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "")
    '                                ExtCombo(objPanelBefore, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID") & "_Before", isRequired, intSequence, question("Table_Reference_Name"), question("Table_Reference_Field_Key"), question("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "")
    '                            Else
    '                                Throw New ApplicationException("Incomplete Question Data On Question ID " & question("PK_OneFCC_Question_ID") & ", Please Report this issue to Admin")
    '                            End If
    '                        Case EnumExtType.Radio
    '                            ExtRadio(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequence)
    '                            ExtRadio(objPanelBefore, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID") & "_Before", isRequired, 8000, intSequence)
    '                    End Select

    '                    intSequence += 1
    '                    'Tampilkan pertanyaan Anak
    '                    'strQuery = "SELECT ofq.PK_OneFCC_Questionnaire_ID, ofq.QUESTION, ofq.IS_REQUIRED, ofq.FK_ExtType_ID, ofq.Table_Reference_Name, ofq.Table_Reference_Field_Key, ofq.Table_Reference_Field_Display_Name, ofq.Table_Reference_Filter"
    '                    'strQuery = strQuery & " FROM OneFCC_Questionnaire ofq"
    '                    'strQuery = strQuery & " WHERE ISNULL(ofq.FK_OneFCC_Questionnaire_Parent_ID,'')<>''"
    '                    'strQuery = strQuery & " And ISNULL(ofq.FK_OneFCC_Questionnaire_Parent_ID,'')=" & question("PK_OneFCC_Questionnaire_ID")
    '                    'strQuery = strQuery & " ORDER BY ofq.SEQUENCE"
    '                    strQuery = "SELECT mapping.PK_OneFCC_Question_Mapping_EDD_ID, ofq.PK_OneFCC_Question_ID, ofq.QUESTION, ofq.IS_REQUIRED, ofq.FK_ExtType_ID, ofq.Table_Reference_Name, ofq.Table_Reference_Field_Key, ofq.Table_Reference_Field_Display_Name, ofq.Table_Reference_Filter"
    '                    strQuery = strQuery & " FROM OneFCC_Question ofq"
    '                    strQuery = strQuery & " JOIN OneFCC_Question_Mapping_EDD mapping"
    '                    strQuery = strQuery & " on ofq.PK_OneFCC_Question_ID = mapping.FK_OneFCC_Question_ID"
    '                    strQuery = strQuery & " WHERE ISNULL(mapping.FK_OneFCC_Question_Parent_ID,'')=" & question("PK_OneFCC_Question_Mapping_EDD_ID")
    '                    strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Group_Code = '" & row("Question_Group_Code") & "'"
    '                    strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
    '                    strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
    '                    strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
    '                    strQuery = strQuery & " ORDER BY mapping.Sequence"

    '                    Dim dtQuestionChild As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
    '                    intSequenceChild = 1
    '                    For Each child In dtQuestionChild.Rows
    '                        'isRequired = child("IS_REQUIRED")
    '                        If Not IsDBNull(child("IS_REQUIRED")) Then
    '                            isRequired = child("IS_REQUIRED")
    '                        Else
    '                            isRequired = False
    '                        End If
    '                        Dim strPrefix As String = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & LCase(Convert.ToChar(intSequenceChild + 64))
    '                        Select Case CType(child("FK_ExtType_ID"), EnumExtType)
    '                            'Case EnumExtType.TextField
    '                            '    ExtText(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID"), isRequired, 8000, intSequence)
    '                            '    ExtText(objPanelBefore, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID") & "_Before", isRequired, 8000, intSequence)
    '                            'Case EnumExtType.NumberField
    '                            '    ExtNumber(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID"), isRequired, 0, intSequence, Long.MinValue, Long.MaxValue)
    '                            '    ExtNumber(objPanelBefore, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID") & "_Before", isRequired, 0, intSequence, Long.MinValue, Long.MaxValue)
    '                            'Case EnumExtType.DateField
    '                            '    ExtDate(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID"), isRequired, 150, intSequence)
    '                            '    ExtDate(objPanelBefore, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID") & "_Before", isRequired, 150, intSequence)
    '                            'Case EnumExtType.DropDownField
    '                            '    ExtCombo(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID"), isRequired, intSequence, child("Table_Reference_Name"), child("Table_Reference_Field_Key"), child("Table_Reference_Field_Display_Name"), child("Table_Reference_Filter"), "")
    '                            '    ExtCombo(objPanelBefore, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID") & "_Before", isRequired, intSequence, child("Table_Reference_Name"), child("Table_Reference_Field_Key"), child("Table_Reference_Field_Display_Name"), child("Table_Reference_Filter"), "")
    '                            'Case EnumExtType.Radio
    '                            '    ExtRadio(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID"), isRequired, 8000, intSequence)
    '                            '    ExtRadio(objPanelBefore, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID") & "_Before", isRequired, 8000, intSequence)

    '                            Case EnumExtType.TextField
    '                                ExtText(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID") & "_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequenceChild)
    '                                ExtText(objPanelBefore, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID") & "_" & question("PK_OneFCC_Question_Mapping_EDD_ID") & "_Before", isRequired, 8000, intSequenceChild)
    '                            Case EnumExtType.NumberField
    '                                ExtNumber(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID") & "_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 0, intSequenceChild, Long.MinValue, Long.MaxValue)
    '                                ExtNumber(objPanelBefore, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID") & "_" & question("PK_OneFCC_Question_Mapping_EDD_ID") & "_Before", isRequired, 0, intSequenceChild, Long.MinValue, Long.MaxValue)
    '                            Case EnumExtType.DateField
    '                                ExtDate(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID") & "_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 150, intSequenceChild)
    '                                ExtDate(objPanelBefore, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID") & "_" & question("PK_OneFCC_Question_Mapping_EDD_ID") & "_Before", isRequired, 150, intSequenceChild)
    '                            Case EnumExtType.DropDownField
    '                                Dim question_Table_Reference_Filter As String
    '                                If Not IsDBNull(child("Table_Reference_Filter")) Then
    '                                    question_Table_Reference_Filter = child("Table_Reference_Filter")
    '                                Else
    '                                    question_Table_Reference_Filter = ""
    '                                End If
    '                                If Not IsDBNull(child("Table_Reference_Name")) AndAlso Not IsDBNull(child("Table_Reference_Field_Key")) AndAlso Not IsDBNull(child("Table_Reference_Field_Display_Name")) Then
    '                                    'ExtCombo(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequenceChild, child("Table_Reference_Name"), child("Table_Reference_Field_Key"), child("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "", isUsingTriger, customerType)
    '                                    ExtCombo(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID") & "_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequenceChild, child("Table_Reference_Name"), child("Table_Reference_Field_Key"), child("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "")
    '                                    ExtCombo(objPanelBefore, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID") & "_" & question("PK_OneFCC_Question_Mapping_EDD_ID") & "_Before", isRequired, intSequenceChild, child("Table_Reference_Name"), child("Table_Reference_Field_Key"), child("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "")
    '                                Else
    '                                    Throw New ApplicationException("Incomplete Question Data On Question ID " & child("PK_OneFCC_Question_ID") & ", Please Report this issue to Admin")
    '                                End If
    '                            Case EnumExtType.Radio
    '                                ExtRadio(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID") & "_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequenceChild)
    '                                ExtRadio(objPanelBefore, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID") & "_" & question("PK_OneFCC_Question_Mapping_EDD_ID") & "_Before", isRequired, 8000, intSequenceChild)
    '                        End Select

    '                        intSequenceChild += 1
    '                    Next

    '                Next
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Protected Sub Load_Questionnaire_After(objpanelinput As Ext.Net.FormPanel, customerType As String)
        Try
            Dim strQuery As String = ""
            Dim intSequence As Integer
            Dim intSequenceChild As Integer

            Dim Customer_Type_ID As String = customerType
            Dim Customer_Segment_ID As String = objEDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Segment_Code
            Dim EDD_Type_ID As String = objEDD_CLASS.objEDD.FK_OneFCC_EDD_Type_Code

            strQuery = "SELECT DISTINCT questiongroup.PK_OneFCC_Question_Group_ID, questiongroup.Question_Group_Name, questiongroup.Sequence, questiongroup.Question_Group_Code"
            strQuery = strQuery & " FROM OneFCC_Question question"
            strQuery = strQuery & " JOIN OneFCC_Question_Mapping_EDD mapping ON question.PK_OneFCC_Question_ID = mapping.FK_OneFCC_Question_ID"
            strQuery = strQuery & " JOIN OneFCC_Question_Group questiongroup ON questiongroup.Question_Group_Code = mapping.FK_OneFCC_Question_Group_Code"
            strQuery = strQuery & " WHERE mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
            strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
            strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
            strQuery = strQuery & " ORDER BY questiongroup.Sequence"

            Dim dtQuestionGroup As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
            For Each row In dtQuestionGroup.Rows
                strQuery = "SELECT mapping.PK_OneFCC_Question_Mapping_EDD_ID, question.PK_OneFCC_Question_ID, question.QUESTION, question.IS_REQUIRED, question.FK_ExtType_ID,"
                strQuery = strQuery & " question.Table_Reference_Name, question.Table_Reference_Field_Key, question.Table_Reference_Field_Display_Name, question.Table_Reference_Filter"
                strQuery = strQuery & " ,point.PK_OneFCC_EDD_Mapping_Question_Answer_Point_ID"
                strQuery = strQuery & " FROM OneFCC_Question question"
                strQuery = strQuery & " JOIN OneFCC_Question_Mapping_EDD mapping on question.PK_OneFCC_Question_ID = mapping.FK_OneFCC_Question_ID"
                strQuery = strQuery & " left join OneFCC_EDD_Mapping_Question_Answer_Point point on mapping.PK_OneFCC_Question_Mapping_EDD_ID = point.FK_OneFCC_Question_Mapping_EDD_ID"
                strQuery = strQuery & " WHERE mapping.FK_OneFCC_Question_Group_Code = '" & row("Question_Group_Code") & "'"
                strQuery = strQuery & " AND ISNULL(mapping.FK_OneFCC_Question_Parent_ID,'')=''"
                strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
                strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
                strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
                strQuery = strQuery & " ORDER BY mapping.Sequence"

                Dim dtQuestion As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
                intSequence = 1

                If dtQuestion.Rows.Count > 0 Then
                    Dim objPanel = ExtPanel(objpanelinput, objpanelinput.ID & "_Panel_" & row("PK_OneFCC_Question_Group_ID"), row("Sequence") & ". " & row("Question_Group_Name"), False)
                    For Each question In dtQuestion.Rows
                        Dim isRequired As Boolean = False
                        If Not IsDBNull(question("IS_REQUIRED")) Then
                            isRequired = question("IS_REQUIRED")
                        Else
                            isRequired = False
                        End If

                        Select Case CType(question("FK_ExtType_ID"), EnumExtType)
                            Case EnumExtType.TextField
                                ExtText(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequence)
                            Case EnumExtType.NumberField
                                ExtNumber(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 0, intSequence, Long.MinValue, Long.MaxValue)
                            Case EnumExtType.DateField
                                ExtDate(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 150, intSequence)
                            Case EnumExtType.DropDownField
                                Dim question_Table_Reference_Filter As String
                                If Not IsDBNull(question("Table_Reference_Filter")) Then
                                    question_Table_Reference_Filter = question("Table_Reference_Filter")
                                Else
                                    question_Table_Reference_Filter = ""
                                End If
                                If Not IsDBNull(question("Table_Reference_Name")) AndAlso Not IsDBNull(question("Table_Reference_Field_Key")) AndAlso Not IsDBNull(question("Table_Reference_Field_Display_Name")) Then
                                    'ExtCombo(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequence, question("Table_Reference_Name"), question("Table_Reference_Field_Key"), question("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "", isUsingTriger, customerType)
                                    ExtCombo(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequence, question("Table_Reference_Name"), question("Table_Reference_Field_Key"), question("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "")
                                Else
                                    Throw New ApplicationException("Incomplete Question Data On Question ID " & question("PK_OneFCC_Question_ID") & ", Please Report this issue to Admin")
                                End If
                            Case EnumExtType.Radio
                                ExtRadio(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequence)
                        End Select

                        intSequence += 1

                        strQuery = "SELECT mapping.PK_OneFCC_Question_Mapping_EDD_ID, ofq.PK_OneFCC_Question_ID, ofq.QUESTION, ofq.IS_REQUIRED, ofq.FK_ExtType_ID, ofq.Table_Reference_Name, ofq.Table_Reference_Field_Key, ofq.Table_Reference_Field_Display_Name, ofq.Table_Reference_Filter"
                        strQuery = strQuery & " ,point.PK_OneFCC_EDD_Mapping_Question_Answer_Point_ID"
                        strQuery = strQuery & " FROM OneFCC_Question ofq"
                        strQuery = strQuery & " JOIN OneFCC_Question_Mapping_EDD mapping"
                        strQuery = strQuery & " left join OneFCC_EDD_Mapping_Question_Answer_Point point on mapping.PK_OneFCC_Question_Mapping_EDD_ID = point.FK_OneFCC_Question_Mapping_EDD_ID"
                        strQuery = strQuery & " on ofq.PK_OneFCC_Question_ID = mapping.FK_OneFCC_Question_ID"
                        strQuery = strQuery & " WHERE ISNULL(mapping.FK_OneFCC_Question_Parent_ID,'')=" & question("PK_OneFCC_Question_Mapping_EDD_ID")
                        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Group_Code = '" & row("Question_Group_Code") & "'"
                        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
                        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
                        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
                        strQuery = strQuery & " ORDER BY mapping.Sequence"

                        Dim dtQuestionChild As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
                        intSequenceChild = 1
                        For Each child In dtQuestionChild.Rows
                            'isRequired = child("IS_REQUIRED")
                            If Not IsDBNull(child("IS_REQUIRED")) Then
                                isRequired = child("IS_REQUIRED")
                            Else
                                isRequired = False
                            End If
                            Dim strPrefix As String = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & LCase(Convert.ToChar(intSequenceChild + 64))
                            Select Case CType(child("FK_ExtType_ID"), EnumExtType)
                                Case EnumExtType.TextField
                                    ExtText(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequenceChild)
                                Case EnumExtType.NumberField
                                    ExtNumber(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 0, intSequenceChild, Long.MinValue, Long.MaxValue)
                                Case EnumExtType.DateField
                                    ExtDate(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 150, intSequenceChild)
                                Case EnumExtType.DropDownField
                                    Dim question_Table_Reference_Filter As String
                                    If Not IsDBNull(child("Table_Reference_Filter")) Then
                                        question_Table_Reference_Filter = child("Table_Reference_Filter")
                                    Else
                                        question_Table_Reference_Filter = ""
                                    End If
                                    If Not IsDBNull(child("Table_Reference_Name")) AndAlso Not IsDBNull(child("Table_Reference_Field_Key")) AndAlso Not IsDBNull(child("Table_Reference_Field_Display_Name")) Then
                                        'ExtCombo(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequenceChild, child("Table_Reference_Name"), child("Table_Reference_Field_Key"), child("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "", isUsingTriger, customerType)
                                        ExtCombo(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequenceChild, child("Table_Reference_Name"), child("Table_Reference_Field_Key"), child("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "")
                                    Else
                                        Throw New ApplicationException("Incomplete Question Data On Question ID " & child("PK_OneFCC_Question_ID") & ", Please Report this issue to Admin")
                                    End If
                                Case EnumExtType.Radio
                                    ExtRadio(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequenceChild)
                            End Select

                            intSequenceChild += 1
                        Next
                    Next
                End If
            Next

            If IsUsingScorePointAffectingAMLCustomer = "1" Then
                Dim objPanelRisk = ExtPanel(objpanelinput, objpanelinput.ID & "_Panel_Risk", "", True)
                Dim typeclass As CasemanagementDAL.OneFCC_Question_Customer_Type = OneFCC_EDD_BLL.GetEDDCustomerTypeByCode(customerType)
                ExtLabel(objPanelRisk, objPanelRisk.ID & "_" & customerType & "_Label", "Risk Rating " & typeclass.Description, 0.5)
                ExtLabel(objPanelRisk, objPanelRisk.ID & "_" & customerType & "_Score", "0", 0.1)
                ExtLabel(objPanelRisk, objPanelRisk.ID & "_" & customerType & "_RiskRating", "Low", 0.4)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Load_Answer()
        Try
            If Ext.Net.X.IsAjaxRequest Then
                Exit Sub
            End If

            If objEDD_CLASS IsNot Nothing AndAlso objEDD_CLASS.objEDD IsNot Nothing Then
                'Load Header
                Dim Customer_Type_ID As String = objEDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Type_Code
                Dim Customer_Segment_ID As String = objEDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Segment_Code
                Dim EDD_Type_ID As String = objEDD_CLASS.objEDD.FK_OneFCC_EDD_Type_Code

                With objEDD_CLASS.objEDD
                    Dim segementclass As CasemanagementDAL.OneFCC_Question_Customer_Segment = OneFCC_EDD_BLL.GetEDDCustomerSegmentByCode(Customer_Segment_ID)
                    Dim typeclass As CasemanagementDAL.OneFCC_Question_Customer_Type = OneFCC_EDD_BLL.GetEDDCustomerTypeByCode(Customer_Type_ID)
                    Dim objEDDType As CasemanagementDAL.OneFCC_EDD_Type = OneFCC_EDD_BLL.GetEDDTypeByCode(EDD_Type_ID)
                    If segementclass IsNot Nothing AndAlso typeclass IsNot Nothing AndAlso objEDDType IsNot Nothing Then
                        cmb_Customer_Type_ID.SetTextWithTextValue(typeclass.Customer_Type_Code, typeclass.Description)
                        cmb_Customer_Segment_ID.SetTextWithTextValue(segementclass.Customer_Segment_Code, segementclass.Description)
                        cmb_EDD_TYPE_ID.SetTextWithTextValue(objEDDType.EDD_Type_Code, objEDDType.EDD_Type_Name)
                        If Not String.IsNullOrEmpty(cmb_EDD_TYPE_ID.StringValue) Then
                            If cmb_EDD_TYPE_ID.StringValue = "C" Then
                                'nDSDropDownField_CIFNo.IsHidden = False
                                'btn_Import_Customer.Hidden = False
                                txt_CIFNo.Hidden = False
                                txt_Nama.Hidden = False
                                txt_Nama.ReadOnly = True
                                txt_Nama.FieldStyle = "background-color: #ddd"
                                btn_OpenWindowCustomer.Hidden = False

                                txt_CIFNo.Value = .CIFNo
                            ElseIf cmb_EDD_TYPE_ID.StringValue = "CP" Then
                                txt_Nama.Hidden = False
                                customer_NIK.Hidden = False
                                customer_Nationality.IsHidden = False
                                customer_DOB.Hidden = False
                                customer_POB.Hidden = False
                                customer_DOB.Format = NawaBLL.SystemParameterBLL.GetDateFormat()

                                If objApproval IsNot Nothing AndAlso objApproval.ModuleKey IsNot Nothing Then
                                    Dim strQuery As String = "SELECT TOP 1 * FROM OneFCC_EDD_Additional_Info_Customer_Prospect WHERE UNIQUE_KEY = '" & objApproval.ModuleKey & "'"
                                    Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                    If drResult IsNot Nothing Then
                                        If Not IsDBNull(drResult("NIK")) Then
                                            customer_NIK.Value = drResult("NIK")
                                        End If
                                        If Not IsDBNull(drResult("DATEOFBIRTH")) Then
                                            customer_DOB.Value = drResult("DATEOFBIRTH")
                                        End If
                                        If Not IsDBNull(drResult("PLACEOFBIRTH")) Then
                                            customer_POB.Value = drResult("PLACEOFBIRTH")
                                        End If
                                        If Not IsDBNull(drResult("Nationality")) Then
                                            Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_COUNTRY WHERE FK_AML_COUNTRY_Code = '" & drResult("Nationality") & "'"
                                            Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                                            If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("FK_AML_COUNTRY_Code")) AndAlso Not IsDBNull(tempDrResult("AML_COUNTRY_Name")) Then
                                                customer_Nationality.SetTextWithTextValue(tempDrResult("FK_AML_COUNTRY_Code"), tempDrResult("AML_COUNTRY_Name"))
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Else
                        Throw New ApplicationException("Unable to find EDD Type Or Customer Type Or Customer Segment, Please contact admin to check this issue.")
                    End If
                    'If objEDDType IsNot Nothing Then
                    '    cmb_EDD_TYPE_ID.SetTextWithTextValue(objEDDType.EDD_Type_Code, objEDDType.EDD_Type_Name)
                    'End If
                    'If typeclass IsNot Nothing Then
                    '    cmb_Customer_Type_ID.SetTextWithTextValue(typeclass.Customer_Type_Code, typeclass.Description)
                    'End If
                    'If segementclass IsNot Nothing Then
                    '    cmb_Customer_Segment_ID.SetTextWithTextValue(segementclass.Customer_Segment_Code, segementclass.Description)
                    'End If

                    'If .FK_OneFCC_EDD_Type_Code IsNot Nothing Then
                    '    Dim objEDDType = OneFCC_EDD_BLL.GetEDDTypeByCode(.FK_OneFCC_EDD_Type_Code)
                    '    If objEDDType IsNot Nothing Then
                    '        cmb_EDD_TYPE_ID.SetTextWithTextValue(objEDDType.EDD_Type_Code, objEDDType.EDD_Type_Name)
                    '    End If
                    'End If
                    'txt_CIFNo.Value = .CIFNo
                    'If Not String.IsNullOrEmpty(cmb_EDD_TYPE_ID.StringValue) Then
                    '    If cmb_EDD_TYPE_ID.StringValue = "C" Then
                    '        'nDSDropDownField_CIFNo.IsHidden = False
                    '        txt_CIFNo.Hidden = False
                    '        txt_Nama.Hidden = False
                    '        txt_Nama.ReadOnly = True
                    '        txt_Nama.FieldStyle = "background-color: #ddd"
                    '        'nDSDropDownField_CIFNo.SetTextWithTextValue(.CIFNo, .CIFNo)
                    '        txt_CIFNo.Value = .CIFNo
                    '    ElseIf cmb_EDD_TYPE_ID.StringValue = "CP" Then
                    '        txt_Nama.Hidden = False
                    '    End If
                    'End If
                    txt_Nama.Value = .Nama
                    If .FILE_ATTACHMENTName IsNot Nothing Then
                        File_Attachment.Value = .FILE_ATTACHMENTName
                        BtnDownloadFile.Hidden = False
                        'FileAttachmentName = .FILE_ATTACHMENTName
                    Else
                        File_Attachment.Value = "No File Attached"
                    End If
                    'If .FILE_ATTACHMENT IsNot Nothing Then
                    '    FileAttachmentByte = .FILE_ATTACHMENT
                    'End If

                    'Load Answer
                    Load_AnswerCustomerType(FormPanelCustomer, "Customer")
                    If objEDD_CLASS.objEDD.IsHaveBeneficialOwner Then
                        checkbox_beneficialowner.Checked = .IsHaveBeneficialOwner
                        Load_AnswerCustomerType(FormPanelBenificialOwner, "BO")
                    End If
                End With
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Load_AnswerCustomerType(objpanelinput As Ext.Net.FormPanel, customerType As String)
        Dim Customer_Type_ID As String = customerType
        Dim Customer_Segment_ID As String = objEDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Segment_Code
        Dim EDD_Type_ID As String = objEDD_CLASS.objEDD.FK_OneFCC_EDD_Type_Code

        'Load Answer
        Dim strQuery As String
        strQuery = " select mapping.PK_OneFCC_Question_Mapping_EDD_ID, question.FK_ExtType_ID, question.Question "
        strQuery = strQuery & " from OneFCC_Question_Mapping_EDD mapping"
        strQuery = strQuery & " join OneFCC_Question question"
        strQuery = strQuery & " on mapping.FK_OneFCC_Question_ID = question.PK_OneFCC_Question_ID"
        strQuery = strQuery & " where mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
        strQuery = strQuery & " and mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
        strQuery = strQuery & " and mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
        strQuery = strQuery & " and mapping.FK_OneFCC_Question_Parent_ID is null"

        Dim dtQuestion As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
        For Each item In dtQuestion.Rows
            Dim intQuestionID As Integer = item("PK_OneFCC_Question_Mapping_EDD_ID")
            Dim strQuestion As String = item("Question")

            Dim strAnswer As String = ""
            'If Not IsDBNull(item("Answer")) Then
            '    strAnswer = item("Answer")
            'End If
            Dim objAnswer = objEDD_CLASS.objListEDD_Detail.Where(Function(x) x.FK_OneFCC_Question_ID = intQuestionID And x.FK_OneFCC_Question_Parent_ID Is Nothing And x.Question = strQuestion).FirstOrDefault
            If objAnswer IsNot Nothing Then
                If Not String.IsNullOrEmpty(objAnswer.Answer) Then
                    strAnswer = objAnswer.Answer
                End If
                Select Case CType(item("FK_ExtType_ID"), EnumExtType)
                    Case EnumExtType.TextField
                        Dim objfield As TextField = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                        If Not objfield Is Nothing Then
                            objfield.Value = strAnswer
                        End If
                    Case EnumExtType.NumberField
                        Dim objfield As NumberField = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                        If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                            objfield.Value = CLng(strAnswer)
                        End If
                    Case EnumExtType.DateField
                        Dim objfield As DateField = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                        If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                            objfield.Value = CDate(strAnswer)
                        End If
                    Case EnumExtType.DropDownField
                        Dim objfield As ComboBox = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                        If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                            objfield.SetValueAndFireSelect(strAnswer)
                        End If
                    Case EnumExtType.Radio
                        Dim objfield As RadioGroup = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                        Dim objFieldYes As Radio = objpanelinput.FindControl("ANSWER_" & intQuestionID & "_Yes")
                        Dim objFieldNo As Radio = objpanelinput.FindControl("ANSWER_" & intQuestionID & "_No")
                        If objfield IsNot Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                            If strAnswer = "Yes" Then
                                objFieldYes.Checked = True
                                objFieldNo.Checked = False
                            ElseIf strAnswer = "No" Then
                                objFieldYes.Checked = False
                                objFieldNo.Checked = True
                            End If
                        End If
                End Select
                If IsUsingScorePointAffectingAMLCustomer = "1" Then
                    ProgressScoring(intQuestionID, CType(item("FK_ExtType_ID"), EnumExtType), strAnswer, customerType, "new")
                End If
            End If

                strQuery = " select mapping.PK_OneFCC_Question_Mapping_EDD_ID, question.FK_ExtType_ID, question.Question "
            strQuery = strQuery & " from OneFCC_Question_Mapping_EDD mapping"
            strQuery = strQuery & " join OneFCC_Question question"
            strQuery = strQuery & " on mapping.FK_OneFCC_Question_ID = question.PK_OneFCC_Question_ID"
            strQuery = strQuery & " where mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
            strQuery = strQuery & " and mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
            strQuery = strQuery & " and mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
            strQuery = strQuery & " and mapping.FK_OneFCC_Question_Parent_ID = " & intQuestionID.ToString()

            Dim dtQuestionChild As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
            For Each child In dtQuestionChild.Rows
                Dim intQuestionParentID As Integer = intQuestionID
                intQuestionID = child("PK_OneFCC_Question_Mapping_EDD_ID")
                strQuestion = child("Question")

                strAnswer = ""
                'If Not IsDBNull(child("Answer")) Then
                '    strAnswer = child("Answer")
                'End If
                Dim objAnswerChild = objEDD_CLASS.objListEDD_Detail.Where(Function(x) x.FK_OneFCC_Question_ID = intQuestionID And x.FK_OneFCC_Question_Parent_ID = intQuestionParentID And x.Question = strQuestion).FirstOrDefault
                If objAnswerChild IsNot Nothing Then
                    If Not String.IsNullOrEmpty(objAnswerChild.Answer) Then
                        strAnswer = objAnswerChild.Answer
                    End If
                    Select Case CType(child("FK_ExtType_ID"), EnumExtType)
                        Case EnumExtType.TextField
                            Dim objfield As TextField = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                            If Not objfield Is Nothing Then
                                objfield.Value = strAnswer
                            End If
                        Case EnumExtType.NumberField
                            Dim objfield As NumberField = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                            If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                                objfield.Value = CLng(strAnswer)
                            End If
                        Case EnumExtType.DateField
                            Dim objfield As DateField = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                            If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                                objfield.Value = CDate(strAnswer)
                            End If
                        Case EnumExtType.DropDownField
                            Dim objfield As ComboBox = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                            If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                                objfield.SetValueAndFireSelect(strAnswer)
                            End If
                        Case EnumExtType.Radio
                            Dim objfield As RadioGroup = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                            Dim objFieldYes As Radio = objpanelinput.FindControl("ANSWER_" & intQuestionID & "_Yes")
                            Dim objFieldNo As Radio = objpanelinput.FindControl("ANSWER_" & intQuestionID & "_No")
                            If objfield IsNot Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                                If strAnswer = "Yes" Then
                                    objFieldYes.Checked = True
                                    objFieldNo.Checked = False
                                ElseIf strAnswer = "No" Then
                                    objFieldYes.Checked = False
                                    objFieldNo.Checked = True
                                End If
                            End If
                    End Select
                    If IsUsingScorePointAffectingAMLCustomer = "1" Then
                        ProgressScoring(intQuestionID, CType(child("FK_ExtType_ID"), EnumExtType), strAnswer, customerType, "new")
                    End If
                End If
            Next
        Next
    End Sub
    '------- END OF LOAD DATA AFTER

    '------- DOCUMENT APPROVE/REJECT
    Protected Sub Btn_EDD_Approve_Click()
        Try
            If IsFromWorkFlow Then
                intWorkflowDecision = 1 'decisionnya accept
                BtnSaveNotes_Click()
                'WindowPopUpNote.Hidden = False
            Else
                'Approve Data
                Dim iDReturned As Long = Accept(objApproval.PK_ModuleApproval_ID)
                Dim strNotes As String = "(Accepted) "
                If txtNotes.Value IsNot Nothing Then
                    strNotes = strNotes & txtNotes.Value.ToString()
                End If
                'SavingNotesInWorkflow("(Rejected) " & txtNotes.Text.ToString(), objApproval.ModuleKey)
                'SavingNotesInWorkflow(strNotes, objApproval.ModuleKey)
                SavingDataHistory(iDReturned.ToString(), strNotes, FileAttachmentName, FileAttachmentByte)
                SaveEDDIntoRiskRating(iDReturned)
                LblConfirmation.Text = "Data Approved"
                ShowPanelConfirmation()
                Dim strQueryUpdateStatusAdd As String = " UPDATE OneFCC_EDD SET STATUS = 'Done' WHERE PK_OneFCC_EDD_ID = " & iDReturned.ToString()
                NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryUpdateStatusAdd, Nothing)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_EDD_Reject_Click()
        Try
            If IsFromWorkFlow Then
                intWorkflowDecision = 2 'decisionnya reject
                BtnSaveNotes_Click()
                'WindowPopUpNote.Hidden = False
            Else
                'Reject Data
                OneFCC_EDD_BLL.Reject(objApproval.PK_ModuleApproval_ID)
                Dim strNotes As String = "(Rejected) "
                If txtNotes.Value IsNot Nothing Then
                    strNotes = strNotes & txtNotes.Value.ToString()
                End If
                'SavingNotesInWorkflow("(Rejected) " & txtNotes.Text.ToString(), objApproval.ModuleKey)
                'SavingNotesInWorkflow(strNotes, objApproval.ModuleKey)
                SavingDataHistory(objApproval.ModuleKey, strNotes, FileAttachmentName, FileAttachmentByte)
                If objApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update Then
                    SaveChangedDataAttachment(objEDD_CLASS)
                End If
                LblConfirmation.Text = "Data Rejected"
                ShowPanelConfirmation()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnFileAttachment_Click()
        Try
            WindowPopUpAttachment.Hidden = False
            pnlContent.ClearContent()
            pnlContent.AnimCollapse = False
            pnlContent.Loader.SuspendScripting()
            pnlContent.Loader.Url = "AML_EDD_UploadAttachment.aspx" & "?ModuleID=" & IDModule
            pnlContent.Loader.Params.Clear()
            pnlContent.LoadContent()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_EDD_Attachment_Submit_Click()
        Try
            If FileAttachmentName IsNot Nothing AndAlso FileAttachmentByte IsNot Nothing Then
                With objEDD_CLASS.objEDD
                    .FILE_ATTACHMENT = FileAttachmentByte
                    .FILE_ATTACHMENTName = FileAttachmentName
                End With
                File_Attachment.Value = FileAttachmentName
                WindowPopUpAttachment.Hidden = True
            Else
                Throw New ApplicationException("No Files Uploaded")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_EDD_Attachment_Back_Click()
        Try
            If FileAttachmentName IsNot Nothing AndAlso FileAttachmentByte IsNot Nothing Then
                With objEDD_CLASS.objEDD
                    .FILE_ATTACHMENT = FileAttachmentByte
                    .FILE_ATTACHMENTName = FileAttachmentName
                End With
                File_Attachment.Value = FileAttachmentName
                BtnDownloadFile.Hidden = False
            End If
            WindowPopUpAttachment.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub DownloadFileUploaded()
        Try
            Filetodownload = "1"
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Sub DownloadFileUploaded_Direct()
        Try
            If String.IsNullOrEmpty(Filetodownload) Then
                Exit Sub
            End If

            If objEDD_CLASS IsNot Nothing AndAlso objEDD_CLASS.objEDD IsNot Nothing AndAlso objEDD_CLASS.objEDD.FILE_ATTACHMENT IsNot Nothing AndAlso objEDD_CLASS.objEDD.FILE_ATTACHMENTName IsNot Nothing Then
                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & objEDD_CLASS.objEDD.FILE_ATTACHMENTName)
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(objEDD_CLASS.objEDD.FILE_ATTACHMENT)
                Response.End()
            Else
                Throw New ApplicationException("No Files to Download")
            End If

            Filetodownload = Nothing
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_EDD_Revise_Click()
        intWorkflowDecision = 3 'decisionnya Revise
        'WindowPopUpNote.Hidden = False
        BtnSaveNotes_Click()
    End Sub

    Protected Sub Btn_EDD_Back_Click()
        Try
            If InStr(ObjModule.UrlApproval, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF DOCUMENT SAVE

    Protected Sub ShowPanelConfirmation()
        Panelconfirmation.Hidden = False
        FormPanelInput.Hidden = True
        'WindowPopUpNote.Hidden = True
    End Sub

    Protected Sub BtnSaveNotes_Click()
        Try
            Using objdb As New CasemanagementDAL.CasemanagementEntities
                Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()

                    'If String.IsNullOrEmpty(txtNotes.Text) Then
                    '    Throw New Exception("Please Entry Notes")
                    '    txtNotes.Focus()
                    'End If
                    'ada isinya dan boleh di simpan
                    Dim UnikID As String = objApproval.ModuleKey
                    Dim strNotes As String = ""

                    If intWorkflowDecision = 1 Then 'Accept
                        strNotes = "(Accepted) "
                        If txtNotes.Value IsNot Nothing Then
                            strNotes = strNotes & txtNotes.Value.ToString()
                        End If
                        SavingDataHistory(UnikID, strNotes, FileAttachmentName, FileAttachmentByte)
                        Dim isFinalAccept As Boolean = False
                        'isFinalAccept = OneFCC_EDD_BLL.SaveWorkflow(objdb, objtrans, objApproval.PK_ModuleApproval_ID, ObjModule.PK_Module_ID, UnikID, "(Accepted) " & txtNotes.Text.ToString(), 1)
                        isFinalAccept = OneFCC_EDD_BLL.SaveWorkflow(objdb, objtrans, objApproval.PK_ModuleApproval_ID, ObjModule.PK_Module_ID, UnikID, strNotes, 1)
                        If isFinalAccept Then
                            'OneFCC_EDD_BLL.Accept(objApproval.PK_ModuleApproval_ID)
                            Dim iDReturned As Long = Accept(objApproval.PK_ModuleApproval_ID)
                            SaveEDDIntoRiskRating(iDReturned)
                            Dim strQueryUpdateStatusAcceptFinal As String = " UPDATE OneFCC_EDD SET STATUS = 'Done' WHERE PK_OneFCC_EDD_ID = " & iDReturned.ToString
                            NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryUpdateStatusAcceptFinal, Nothing)
                            LblConfirmation.Text = "Data Approved"
                        Else
                            If FileAttachmentName IsNot Nothing AndAlso FileAttachmentByte IsNot Nothing Then
                                SaveDataChange(objEDD_CLASS, objApproval)
                            End If
                            LblConfirmation.Text = "Data Approved and continue to next Approval."
                        End If
                    ElseIf intWorkflowDecision = 2 Then 'Reject
                        strNotes = "(Rejected) "
                        If txtNotes.Value IsNot Nothing Then
                            strNotes = strNotes & txtNotes.Value.ToString()
                        End If
                        SavingDataHistory(UnikID, strNotes, FileAttachmentName, FileAttachmentByte)
                        'OneFCC_EDD_BLL.SaveWorkflow(objdb, objtrans, objApproval.PK_ModuleApproval_ID, ObjModule.PK_Module_ID, UnikID, "(Rejected) " & txtNotes.Text.ToString(), 2)
                        OneFCC_EDD_BLL.SaveWorkflow(objdb, objtrans, objApproval.PK_ModuleApproval_ID, ObjModule.PK_Module_ID, UnikID, strNotes, 2)
                        OneFCC_EDD_BLL.Reject(objApproval.PK_ModuleApproval_ID)
                        If FileAttachmentName IsNot Nothing AndAlso FileAttachmentByte IsNot Nothing Then
                            SaveDataChange(objEDD_CLASS, objApproval)
                        End If
                        If objApproval IsNot Nothing AndAlso objApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update Then
                            Dim strQueryUpdateStatusRejected As String = " UPDATE OneFCC_EDD SET STATUS = 'Rejected' WHERE PK_OneFCC_EDD_ID = " & UnikID
                            NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryUpdateStatusRejected, Nothing)
                        End If
                        LblConfirmation.Text = "Data Rejected"
                    ElseIf intWorkflowDecision = 3 Then 'Revise
                        strNotes = "(Revise) "
                        If txtNotes.Value IsNot Nothing Then
                            strNotes = strNotes & txtNotes.Value.ToString()
                        End If
                        SavingDataHistory(UnikID, strNotes, FileAttachmentName, FileAttachmentByte)
                        '30029 merupakan module khusus untuk coreection
                        'OneFCC_EDD_BLL.SaveWorkflow(objdb, objtrans, objApproval.PK_ModuleApproval_ID, ObjModule.PK_Module_ID, UnikID, "(Revise) " & txtNotes.Text.ToString(), 3)
                        OneFCC_EDD_BLL.SaveWorkflow(objdb, objtrans, objApproval.PK_ModuleApproval_ID, ObjModule.PK_Module_ID, UnikID, strNotes, 3)
                        If FileAttachmentName IsNot Nothing AndAlso FileAttachmentByte IsNot Nothing Then
                            SaveDataChange(objEDD_CLASS, objApproval)
                        End If
                        If objApproval IsNot Nothing AndAlso objApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update Then
                            Dim strQueryUpdateStatusRevise As String = " UPDATE OneFCC_EDD SET STATUS = 'Need To Correction' WHERE PK_OneFCC_EDD_ID = " & UnikID
                            NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryUpdateStatusRevise, Nothing)
                        End If
                        LblConfirmation.Text = "Data Save to Revise. Click Ok to Back To Module Approval."
                    Else
                        Throw New Exception("Something Wrong With System, Please Report to Admin")
                    End If
                End Using
            End Using
            'OneFCC_EDD_BLL.SaveAddWithApprovalWorkFlow(ObjModule, objEDDForWorkFlow, txtNotes.Value.ToString())
            ShowPanelConfirmation()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub SaveEDDIntoRiskRating(IDUnikReturn As Long)
        Try
            If IsUsingScorePointAffectingAMLCustomer = "1" Then
                Dim totalscore As Integer = 0
                Dim tempComputeScore As Object = DataTabelEDDScore.Compute("SUM(Score)", "Type = 'Customer'")
                If Not IsDBNull(tempComputeScore) Then
                    totalscore = Convert.ToInt32(tempComputeScore)
                End If
                If checkbox_beneficialowner.Checked Then
                    Dim totalscorebo As Integer = 0
                    tempComputeScore = DataTabelEDDScore.Compute("SUM(Score)", "Type = 'BO'")
                    If Not IsDBNull(tempComputeScore) Then
                        totalscorebo = Convert.ToInt32(tempComputeScore)
                    End If
                    If totalscorebo > totalscore Then
                        totalscore = totalscorebo
                    End If
                End If
                Dim strquery As String = "SELECT * FROM AML_RISK_RATING a WHERE " & totalscore & " BETWEEN a.RISK_RATING_SCORE_FROM AND a.RISK_RATING_SCORE_TO"
                Dim datarowpoint As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strquery)
                Dim riskRatingCode As String = Nothing
                Dim riskRatingName As String = Nothing
                If datarowpoint IsNot Nothing Then
                    If Not IsDBNull(datarowpoint("RISK_RATING_CODE")) Then
                        riskRatingCode = datarowpoint("RISK_RATING_CODE")
                    End If
                    If Not IsDBNull(datarowpoint("RISK_RATING_NAME")) Then
                        riskRatingName = datarowpoint("RISK_RATING_NAME")
                    End If
                End If

                If Not String.IsNullOrEmpty(cmb_EDD_TYPE_ID.SelectedItemValue) AndAlso cmb_EDD_TYPE_ID.SelectedItemValue = "C" Then
                    Dim strQueryInsertHistory As String = " INSERT INTO AML_RISK_RATING_RESULT_HISTORY "
                    strQueryInsertHistory = strQueryInsertHistory & " ( "
                    strQueryInsertHistory = strQueryInsertHistory & " CIF_NO, "
                    strQueryInsertHistory = strQueryInsertHistory & " RISK_RATING_TOTAL_SCORE, "
                    strQueryInsertHistory = strQueryInsertHistory & " RISK_RATING_NAME, "
                    strQueryInsertHistory = strQueryInsertHistory & " CreatedBy, "
                    strQueryInsertHistory = strQueryInsertHistory & " LastUpdateBy, "
                    strQueryInsertHistory = strQueryInsertHistory & " CreatedDate, "
                    strQueryInsertHistory = strQueryInsertHistory & " LastUpdateDate "
                    strQueryInsertHistory = strQueryInsertHistory & " ) "
                    strQueryInsertHistory = strQueryInsertHistory & " VALUES "
                    strQueryInsertHistory = strQueryInsertHistory & " ( "
                    strQueryInsertHistory = strQueryInsertHistory & " '" & txt_CIFNo.Value & "' "
                    strQueryInsertHistory = strQueryInsertHistory & " ," & totalscore & " "
                    strQueryInsertHistory = strQueryInsertHistory & " ,'" & riskRatingName & "' "
                    strQueryInsertHistory = strQueryInsertHistory & " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "' "
                    strQueryInsertHistory = strQueryInsertHistory & " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "' "
                    strQueryInsertHistory = strQueryInsertHistory & " ,GETDATE() "
                    strQueryInsertHistory = strQueryInsertHistory & " ,GETDATE() "
                    strQueryInsertHistory = strQueryInsertHistory & " ) "
                    strQueryInsertHistory = strQueryInsertHistory & " declare @ID as bigint = SCOPE_IDENTITY() "
                    strQueryInsertHistory = strQueryInsertHistory & " INSERT INTO AML_RISK_RATING_RESULT_DETAIL_HISTORY "
                    strQueryInsertHistory = strQueryInsertHistory & " ( "
                    strQueryInsertHistory = strQueryInsertHistory & " CIF_NO, "
                    strQueryInsertHistory = strQueryInsertHistory & " RISK_RATING_SCORE, "
                    strQueryInsertHistory = strQueryInsertHistory & " RISK_RATING_DETAIL_VALUE, "
                    strQueryInsertHistory = strQueryInsertHistory & " CreatedBy, "
                    strQueryInsertHistory = strQueryInsertHistory & " LastUpdateBy, "
                    strQueryInsertHistory = strQueryInsertHistory & " CreatedDate, "
                    strQueryInsertHistory = strQueryInsertHistory & " LastUpdateDate, "
                    strQueryInsertHistory = strQueryInsertHistory & " FK_AML_RISK_RATING_RESULT_HISTORY "
                    strQueryInsertHistory = strQueryInsertHistory & " ) "
                    strQueryInsertHistory = strQueryInsertHistory & " VALUES "
                    strQueryInsertHistory = strQueryInsertHistory & " ( "
                    strQueryInsertHistory = strQueryInsertHistory & " '" & txt_CIFNo.Value & "' "
                    strQueryInsertHistory = strQueryInsertHistory & " ," & totalscore & " "
                    strQueryInsertHistory = strQueryInsertHistory & " ,'(From EDD " & IDUnikReturn & ")' "
                    'If objEDD_CLASS IsNot Nothing AndAlso objEDD_CLASS.objEDD IsNot Nothing AndAlso objEDD_CLASS.objEDD.PK_OneFCC_EDD_ID <> Nothing Then
                    '    strQueryInsertHistory = strQueryInsertHistory & " 	,'(From EDD " & objEDD_CLASS.objEDD.PK_OneFCC_EDD_ID & ")' --RISK_RATING_DETAIL_VALUE, "
                    'Else
                    '    strQueryInsertHistory = strQueryInsertHistory & " 	,'(From EDD For CIF " & nDSDropDownField_CIFNo.StringValue & " Created By " & NawaBLL.Common.SessionCurrentUser.UserID & ", Added From Approval " & objApproval.PK_ModuleApproval_ID & ")' --RISK_RATING_DETAIL_VALUE, "
                    'End If
                    strQueryInsertHistory = strQueryInsertHistory & " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "' "
                    strQueryInsertHistory = strQueryInsertHistory & " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "' "
                    strQueryInsertHistory = strQueryInsertHistory & " ,GETDATE() "
                    strQueryInsertHistory = strQueryInsertHistory & " ,GETDATE() "
                    strQueryInsertHistory = strQueryInsertHistory & " ,@ID "
                    strQueryInsertHistory = strQueryInsertHistory & " ) "
                    NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryInsertHistory, Nothing)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlApproval, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AML_EDD_ApprovalDetail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Approval
    End Sub

    Protected Sub LoadToFormPanel(objpanelinput As Ext.Net.FormPanel, customerType As String, oldornew As String)
        Try
            Dim typeclassCustomer As CasemanagementDAL.OneFCC_Question_Customer_Type = OneFCC_EDD_BLL.GetEDDCustomerTypeByCode(customerType)
            If typeclassCustomer IsNot Nothing Then
                objpanelinput.Title = typeclassCustomer.Description & " Question"
                If oldornew = "old" Then
                    Load_Questionnaire_Before(objpanelinput, typeclassCustomer.Customer_Type_Code)
                ElseIf oldornew = "new" Then
                    Load_Questionnaire_After(objpanelinput, typeclassCustomer.Customer_Type_Code)
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ProgressScoring(mappingID As Long, enumType As EnumExtType, stringkeyvalue As String, customertype As String, oldornew As String)
        Try
            Dim totalscore As Integer = 0
            'Dim dtrow = DataTabelEDDScore.AsEnumerable().Select(Function(x) New With {
            '    .Question_ID = x.Field(Of Long)("Question_ID"),
            '    .Score = x.Field(Of Integer)("Score"),
            '    .Type = x.Field(Of String)("Type")
            '                                                       }).Where(Function(x) x.Question_ID = mappingID).FirstOrDefault
            Dim strquery As String = "select top 1 * from OneFCC_EDD_Mapping_Question_Answer_Point where FK_OneFCC_Question_Mapping_EDD_ID = " & mappingID
            Dim datarowpoint As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strquery)
            Dim scorepoint As Integer = 0
            If datarowpoint IsNot Nothing Then
                Select Case enumType
                    Case EnumExtType.TextField
                        If Not IsDBNull(datarowpoint("Other_Condition")) AndAlso Not IsDBNull(datarowpoint("Other_Score_Int")) Then
                            Dim strQueryCondition As String = " select top 1 FilterWhereFormat from AdvancedFilterWhereClause where FK_AdvanceFilter_ID = 1 "
                            strQueryCondition = strQueryCondition & " and FilterWhereClause = '" & datarowpoint("Other_Condition") & "'"
                            Dim dataRowConditionFormat As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQueryCondition)
                            If dataRowConditionFormat IsNot Nothing AndAlso Not IsDBNull(dataRowConditionFormat("FilterWhereFormat")) Then
                                Dim strQueryChecker As String = " select 'True' as Answer where " & dataRowConditionFormat("FilterWhereFormat")
                                If datarowpoint("Other_Condition") = "Empty" OrElse datarowpoint("Other_Condition") = "Not Empty" OrElse
                                    datarowpoint("Other_Condition") = "NULL" OrElse datarowpoint("Other_Condition") = "Not NULL" Then
                                    If stringkeyvalue IsNot Nothing Then
                                        If stringkeyvalue = InputNullPronoun Then
                                            strQueryChecker = String.Format(strQueryChecker, "NULL")
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "'" & stringkeyvalue & "'")
                                        End If
                                    Else
                                        strQueryChecker = String.Format(strQueryChecker, "NULL")
                                    End If
                                    'ElseIf dataRowPoint("Other_Condition") = "in" OrElse dataRowPoint("Other_Condition") = "Not In" Then
                                    '    If Not IsDBNull(dataRowPoint("Other_Value")) Then
                                    '        If stringKeyValue IsNot Nothing Then
                                    '            If stringKeyValue = InputNullPronoun Then
                                    '                strQueryChecker = String.Format(strQueryChecker, "NULL", dataRowPoint("Other_Value"))
                                    '            Else
                                    '                strQueryChecker = String.Format(strQueryChecker, "'" & stringKeyValue & "'", dataRowPoint("Other_Value"))
                                    '            End If
                                    '        Else
                                    '            strQueryChecker = String.Format(strQueryChecker, "NULL", dataRowPoint("Other_Value"))
                                    '        End If
                                    '    End If
                                Else
                                    If Not IsDBNull(datarowpoint("Other_Value")) Then
                                        If stringkeyvalue IsNot Nothing Then
                                            If stringkeyvalue = InputNullPronoun Then
                                                strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"))
                                            Else
                                                strQueryChecker = String.Format(strQueryChecker, "'" & stringkeyvalue & "'", datarowpoint("Other_Value"))
                                            End If
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"))
                                        End If
                                    End If
                                End If
                                Dim dataRowValidateAnswer As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQueryChecker)
                                If dataRowValidateAnswer IsNot Nothing Then
                                    scorepoint = datarowpoint("Other_Score_Int")
                                End If
                            End If
                        End If
                        'If Not IsDBNull(datarowpoint("Other_Condition")) AndAlso Not IsDBNull(datarowpoint("Other_Value")) AndAlso Not IsDBNull(datarowpoint("Other_Score_Int")) AndAlso stringkeyvalue IsNot Nothing Then
                        '    If datarowpoint("Other_Condition").ToString() = "Contain" Then
                        '        If stringkeyvalue.Contains(datarowpoint("Other_Value").ToString()) Then
                        '            scorepoint = datarowpoint("Other_Score_Int")
                        '        End If
                        '    End If
                        'End If

                    Case EnumExtType.NumberField
                        If Not IsDBNull(datarowpoint("Other_Condition")) AndAlso Not IsDBNull(datarowpoint("Other_Score_Int")) Then
                            Dim strQueryCondition As String = " select top 1 FilterWhereFormat from AdvancedFilterWhereClause where FK_AdvanceFilter_ID = 2 "
                            strQueryCondition = strQueryCondition & " and FilterWhereClause = '" & datarowpoint("Other_Condition") & "'"
                            Dim dataRowConditionFormat As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQueryCondition)
                            If dataRowConditionFormat IsNot Nothing AndAlso Not IsDBNull(dataRowConditionFormat("FilterWhereFormat")) Then
                                Dim strQueryChecker As String = " select 'True' as Answer where " & dataRowConditionFormat("FilterWhereFormat")
                                If datarowpoint("Other_Condition") = "NULL" OrElse datarowpoint("Other_Condition") = "Not NULL" Then
                                    If Not String.IsNullOrWhiteSpace(stringkeyvalue) Then
                                        If stringkeyvalue = InputNullPronoun Then
                                            strQueryChecker = String.Format(strQueryChecker, "NULL")
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, stringkeyvalue)
                                        End If
                                    Else
                                        strQueryChecker = String.Format(strQueryChecker, "NULL")
                                    End If
                                ElseIf datarowpoint("Other_Condition") = "Between" OrElse datarowpoint("Other_Condition") = "Not Between" Then
                                    If Not IsDBNull(datarowpoint("Other_Value")) AndAlso Not IsDBNull(datarowpoint("Other_Value_Helper")) Then
                                        If Not String.IsNullOrWhiteSpace(stringkeyvalue) Then
                                            If stringkeyvalue = InputNullPronoun Then
                                                strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"), datarowpoint("Other_Value_Helper"))
                                            Else
                                                strQueryChecker = String.Format(strQueryChecker, stringkeyvalue, datarowpoint("Other_Value"), datarowpoint("Other_Value_Helper"))
                                            End If
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"), datarowpoint("Other_Value_Helper"))
                                        End If
                                    End If
                                Else
                                    If Not IsDBNull(datarowpoint("Other_Value")) Then
                                        If Not String.IsNullOrWhiteSpace(stringkeyvalue) Then
                                            If stringkeyvalue = InputNullPronoun Then
                                                strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"))
                                            Else
                                                strQueryChecker = String.Format(strQueryChecker, stringkeyvalue, datarowpoint("Other_Value"))
                                            End If
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"))
                                        End If
                                    End If
                                End If
                                Dim dataRowValidateAnswer As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQueryChecker)
                                If dataRowValidateAnswer IsNot Nothing Then
                                    scorepoint = datarowpoint("Other_Score_Int")
                                End If
                            End If
                        End If
                        'If Not IsDBNull(datarowpoint("Other_Condition")) AndAlso Not IsDBNull(datarowpoint("Other_Value")) AndAlso Not IsDBNull(datarowpoint("Other_Score_Int")) AndAlso Not String.IsNullOrWhiteSpace(stringkeyvalue) Then
                        '    If datarowpoint("Other_Condition").ToString() = "Equal" Then
                        '        If CInt(datarowpoint("Other_Value")) = CInt(stringkeyvalue) Then
                        '            scorepoint = datarowpoint("Other_Score_Int")
                        '        End If
                        '    End If
                        'End If

                    Case EnumExtType.DateField
                        If Not IsDBNull(datarowpoint("Other_Condition")) AndAlso Not IsDBNull(datarowpoint("Other_Score_Int")) Then
                            Dim strQueryCondition As String = " select top 1 FilterWhereFormat from AdvancedFilterWhereClause where FK_AdvanceFilter_ID = 3 "
                            strQueryCondition = strQueryCondition & " and FilterWhereClause = '" & datarowpoint("Other_Condition") & "'"
                            Dim dataRowConditionFormat As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQueryCondition)
                            If dataRowConditionFormat IsNot Nothing AndAlso Not IsDBNull(dataRowConditionFormat("FilterWhereFormat")) Then
                                Dim strQueryChecker As String = " select 'True' as Answer where " & dataRowConditionFormat("FilterWhereFormat")
                                If datarowpoint("Other_Condition") = "NULL" OrElse datarowpoint("Other_Condition") = "Not NULL" Then
                                    If stringkeyvalue IsNot Nothing Then
                                        If stringkeyvalue = InputNullPronoun Then
                                            strQueryChecker = String.Format(strQueryChecker, "NULL")
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "'" & stringkeyvalue & "'")
                                        End If
                                    Else
                                        strQueryChecker = String.Format(strQueryChecker, "NULL")
                                    End If
                                ElseIf datarowpoint("Other_Condition") = "Between" OrElse datarowpoint("Other_Condition") = "Not Between" Then
                                    If Not IsDBNull(datarowpoint("Other_Value")) AndAlso Not IsDBNull(datarowpoint("Other_Value_Helper")) Then
                                        If stringkeyvalue IsNot Nothing Then
                                            If stringkeyvalue = InputNullPronoun Then
                                                strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"), datarowpoint("Other_Value_Helper"))
                                            Else
                                                strQueryChecker = String.Format(strQueryChecker, "'" & stringkeyvalue & "'", datarowpoint("Other_Value"), datarowpoint("Other_Value_Helper"))
                                            End If
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"), datarowpoint("Other_Value_Helper"))
                                        End If
                                        'If stringKeyValue IsNot Nothing Then
                                        '    If stringKeyValue = InputNullPronoun Then
                                        '        strQueryChecker = String.Format(strQueryChecker, "NULL", "CONVERT(DATE,'" & dataRowPoint("Other_Value") & "',111)", "CONVERT(DATE,'" & dataRowPoint("Other_Value_Helper") & "',111)")
                                        '    Else
                                        '        strQueryChecker = String.Format(strQueryChecker, "'" & stringKeyValue & "'", "CONVERT(DATE,'" & dataRowPoint("Other_Value") & "',111)", "CONVERT(DATE,'" & dataRowPoint("Other_Value_Helper") & "',111)")
                                        '    End If
                                        'Else
                                        '    strQueryChecker = String.Format(strQueryChecker, "NULL", "CONVERT(DATE,'" & dataRowPoint("Other_Value") & "',111)", "CONVERT(DATE,'" & dataRowPoint("Other_Value_Helper") & "',111)")
                                        'End If
                                    End If
                                Else
                                    If Not IsDBNull(datarowpoint("Other_Value")) Then
                                        If stringkeyvalue IsNot Nothing Then
                                            If stringkeyvalue = InputNullPronoun Then
                                                strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"))
                                            Else
                                                strQueryChecker = String.Format(strQueryChecker, "'" & stringkeyvalue & "'", datarowpoint("Other_Value"))
                                            End If
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"))
                                        End If
                                        'If stringKeyValue IsNot Nothing Then
                                        '    If stringKeyValue = InputNullPronoun Then
                                        '        strQueryChecker = String.Format(strQueryChecker, "NULL", "CONVERT(DATE,'" & dataRowPoint("Other_Value") & "',111)")
                                        '    Else
                                        '        strQueryChecker = String.Format(strQueryChecker, "'" & stringKeyValue & "'", "CONVERT(DATE,'" & dataRowPoint("Other_Value") & "',111)")
                                        '    End If
                                        'Else
                                        '    strQueryChecker = String.Format(strQueryChecker, "NULL", "CONVERT(DATE,'" & dataRowPoint("Other_Value") & "',111)")
                                        'End If
                                    End If
                                End If
                                Dim dataRowValidateAnswer As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQueryChecker)
                                If dataRowValidateAnswer IsNot Nothing Then
                                    scorepoint = datarowpoint("Other_Score_Int")
                                End If
                            End If
                        End If
                        'If Not IsDBNull(datarowpoint("Other_Condition")) AndAlso Not IsDBNull(datarowpoint("Other_Value")) AndAlso Not IsDBNull(datarowpoint("Other_Score_Int")) AndAlso Not String.IsNullOrWhiteSpace(stringkeyvalue) Then
                        '    Dim datevalue As Date = CDate(stringkeyvalue)
                        '    Dim datecondition As Date = CDate(datarowpoint("Other_Value"))
                        '    If datarowpoint("Other_Condition").ToString() = "Equal" Then
                        '        If DateDiff(DateInterval.Day, datevalue, datecondition) = 0 Then
                        '            scorepoint = datarowpoint("Other_Score_Int")
                        '        End If
                        '    End If
                        'End If

                    Case EnumExtType.DropDownField
                        stringkeyvalue = stringkeyvalue.Replace("'", "''")
                        If Not IsDBNull(datarowpoint("Table_Reference_Name")) AndAlso Not IsDBNull(datarowpoint("Table_Reference_Field_Key")) AndAlso Not IsDBNull(datarowpoint("Table_Reference_Field_Score")) AndAlso stringkeyvalue IsNot Nothing Then
                            Dim strinnerquery As String = "select top 1 " & datarowpoint("Table_Reference_Field_Score") & " from " & datarowpoint("Table_Reference_Name") &
                                " where " & datarowpoint("Table_Reference_Field_Key") & " = '" & stringkeyvalue & "'"
                            Dim datarowdropdown As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strinnerquery)
                            If datarowdropdown IsNot Nothing AndAlso Not IsDBNull(datarowdropdown(datarowpoint("Table_Reference_Field_Score"))) Then
                                scorepoint = datarowdropdown(datarowpoint("Table_Reference_Field_Score"))
                            End If
                        End If
                    Case EnumExtType.Radio
                        If stringkeyvalue = "Yes" Then
                            If Not IsDBNull(datarowpoint("Radio_Yes_Score_Int")) Then
                                scorepoint = datarowpoint("Radio_Yes_Score_Int")
                            End If
                        ElseIf stringkeyvalue = "No" Then
                            If Not IsDBNull(datarowpoint("Radio_No_Score_Int")) Then
                                scorepoint = datarowpoint("Radio_No_Score_Int")
                            End If
                        End If
                End Select
                Dim strselect As String = "Question_ID = " & mappingID
                If oldornew = "old" Then
                    Dim dtrow As DataRow = DataTabelEDDScoreBefore.Select(strselect).FirstOrDefault()
                    If dtrow Is Nothing Then
                        Dim objdatarow As DataRow = DataTabelEDDScoreBefore.NewRow()
                        objdatarow("Question_ID") = mappingID
                        objdatarow("Type") = customertype
                        objdatarow("Score") = scorepoint
                        DataTabelEDDScoreBefore.Rows.Add(objdatarow)
                    Else
                        Dim indexrow As Integer = DataTabelEDDScoreBefore.Rows.IndexOf(dtrow)
                        DataTabelEDDScoreBefore.Rows(indexrow)("Score") = scorepoint
                    End If
                    totalscore = Convert.ToInt32(DataTabelEDDScoreBefore.Compute("SUM(Score)", "Type = '" & customertype & "'"))
                ElseIf oldornew = "new" Then
                    Dim dtrow As DataRow = DataTabelEDDScore.Select(strselect).FirstOrDefault()
                    If dtrow Is Nothing Then
                        Dim objdatarow As DataRow = DataTabelEDDScore.NewRow()
                        objdatarow("Question_ID") = mappingID
                        objdatarow("Type") = customertype
                        objdatarow("Score") = scorepoint
                        DataTabelEDDScore.Rows.Add(objdatarow)
                    Else
                        Dim indexrow As Integer = DataTabelEDDScore.Rows.IndexOf(dtrow)
                        DataTabelEDDScore.Rows(indexrow)("Score") = scorepoint
                    End If
                    totalscore = Convert.ToInt32(DataTabelEDDScore.Compute("SUM(Score)", "Type = '" & customertype & "'"))
                End If
                UpdateRiskScoreUI(customertype, totalscore, oldornew)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub UpdateRiskScoreUI(customertype As String, totalscore As Integer, oldornew As String)
        Try
            Dim objPanel As New Ext.Net.Panel
            Dim objlabelRisk As New Ext.Net.Label
            Dim objlabelScore As New Ext.Net.Label
            If customertype = "Customer" Then
                If oldornew = "old" Then
                    objPanel = FormPanelCustomer_Before.FindControl(FormPanelCustomer_Before.ID & "_Panel_Risk")
                ElseIf oldornew = "new" Then
                    objPanel = FormPanelCustomer.FindControl(FormPanelCustomer.ID & "_Panel_Risk")
                End If
                objlabelRisk = objPanel.FindControl(objPanel.ID & "_Customer_RiskRating")
                objlabelScore = objPanel.FindControl(objPanel.ID & "_Customer_Score")
            ElseIf customertype = "BO" Then
                If oldornew = "old" Then
                    objPanel = FormPanelBenificialOwner_Before.FindControl(FormPanelBenificialOwner_Before.ID & "_Panel_Risk")
                ElseIf oldornew = "new" Then
                    objPanel = FormPanelBenificialOwner.FindControl(FormPanelBenificialOwner.ID & "_Panel_Risk")
                End If
                objlabelRisk = objPanel.FindControl(objPanel.ID & "_BO_RiskRating")
                objlabelScore = objPanel.FindControl(objPanel.ID & "_BO_Score")
            End If
            objlabelScore.Text = totalscore.ToString()

            Dim strquery As String = "SELECT * FROM AML_RISK_RATING a WHERE " & totalscore & " BETWEEN a.RISK_RATING_SCORE_FROM AND a.RISK_RATING_SCORE_TO"
            Dim datarowpoint As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strquery)
            If datarowpoint IsNot Nothing AndAlso Not IsDBNull(datarowpoint("RISK_RATING_NAME")) Then
                objlabelRisk.Text = datarowpoint("RISK_RATING_NAME")
                If datarowpoint("RISK_RATING_NAME") = "LOW" Then
                    objPanel.BodyStyle = "background-color: #33ff3a;"
                ElseIf datarowpoint("RISK_RATING_NAME") = "MEDIUM" Then
                    objPanel.BodyStyle = "background-color: #ffee33;"
                ElseIf datarowpoint("RISK_RATING_NAME") = "HIGH" Then
                    objPanel.BodyStyle = "background-color: #ff3333;"
                ElseIf datarowpoint("RISK_RATING_NAME") = "PEPS" Then
                    objPanel.BodyStyle = "background-color: #ff0000;"
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '------- LOAD COMPONENT
    Protected Function ExtPanel(pn As FormPanel, strName As String, strTitle As String, isRiskRating As Boolean) As Ext.Net.Panel
        Try
            Dim objPanel As New Ext.Net.Panel
            With objPanel
                .ID = strName
                .ClientIDMode = Web.UI.ClientIDMode.Static
                .Title = strTitle
                .MarginSpec = "0 0 10 0"
                If isRiskRating Then
                    .BodyStyle = "background-color: #33ff3a;"
                    .Border = False
                    .Layout = "ColumnLayout"
                Else
                    .Collapsible = True
                    .Border = True
                    .BodyPadding = 10
                End If
            End With

            pn.Add(objPanel)
            Return objPanel

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ExtDate(pn As Ext.Net.Panel, strLabel As String, strFieldName As String, bRequired As Boolean, intMaxSize As Integer, intgridpos As Integer) As Ext.Net.DateField
        Dim objDateField As New Ext.Net.DateField
        objDateField.ID = strFieldName
        objDateField.ClientIDMode = Web.UI.ClientIDMode.Static

        objDateField.FieldLabel = strLabel
        objDateField.LabelStyle = "word-wrap: break-word"
        'objDateField.LabelWidth = 150 'intLabelWidth
        objDateField.Name = strFieldName
        objDateField.ID = strFieldName
        objDateField.AllowBlank = Not bRequired
        objDateField.BlankText = strLabel & " is required."
        'objDateField.MaxLength = intMaxSize
        objDateField.Width = objDateField.LabelWidth + 150
        objDateField.Format = NawaBLL.SystemParameterBLL.GetDateFormat
        objDateField.AnchorHorizontal = "40%"
        If objApproval IsNot Nothing AndAlso objApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update Then
            objDateField.LabelWidth = 200
        Else
            objDateField.LabelWidth = 500
        End If
        'objDateField.LabelWidth = 200
        objDateField.MinWidth = objDateField.LabelWidth + 150

        objDateField.ReadOnly = True

        pn.Add(objDateField)
        Return objDateField
    End Function

    Protected Function ExtText(pn As Ext.Net.Panel, strLabel As String, strFieldName As String, bRequired As Boolean, intMaxSize As Integer, intgridpos As Integer) As Ext.Net.TextField
        Dim objTextField As New Ext.Net.TextField
        objTextField.ID = strFieldName
        objTextField.ClientIDMode = Web.UI.ClientIDMode.Static
        objTextField.FieldLabel = strLabel

        objTextField.ID = strFieldName
        objTextField.Name = strFieldName
        objTextField.AllowBlank = Not bRequired
        objTextField.BlankText = strLabel & " is required."
        objTextField.MaxLength = intMaxSize
        If objApproval IsNot Nothing AndAlso objApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update Then
            objTextField.LabelWidth = 200
        Else
            objTextField.LabelWidth = 500
        End If
        'objTextField.LabelWidth = 200
        objTextField.MinWidth = objTextField.LabelWidth + 200

        objTextField.AnchorHorizontal = "80%"
        objTextField.ReadOnly = True

        pn.Add(objTextField)
        Return objTextField
    End Function

    Protected Function ExtRadio(pn As Ext.Net.Panel, strLabel As String, strFieldName As String, bRequired As Boolean, intMaxSize As Integer, intgridpos As Integer) As Ext.Net.RadioGroup
        Dim objRadioGroup As New Ext.Net.RadioGroup
        Dim objRadioYes As New Ext.Net.Radio
        Dim objRadioNo As New Ext.Net.Radio

        With objRadioGroup
            .FieldLabel = strLabel
            .ID = strFieldName
            .AllowBlank = Not bRequired
            .BlankText = strLabel & " is required."
            If objApproval IsNot Nothing AndAlso objApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update Then
                .LabelWidth = 200
            Else
                .LabelWidth = 500
            End If
            '.LabelWidth = 200
            .Width = .LabelWidth + 150
        End With

        With objRadioYes
            .ID = strFieldName & "_Yes"
            .BoxLabel = "Yes"
            .Value = "Yes"
            .Checked = False
        End With

        With objRadioNo
            .ID = strFieldName & "_No"
            .BoxLabel = "No"
            .Value = "No"
            .Checked = False
        End With

        objRadioGroup.Add(objRadioYes)
        objRadioGroup.Add(objRadioNo)

        objRadioYes.ReadOnly = True
        objRadioNo.ReadOnly = True

        pn.Add(objRadioGroup)
        Return objRadioGroup
    End Function

    Public Function ExtNumber(pn As Ext.Net.Panel, strLabel As String, strFieldName As String, bRequired As Boolean, intDecimalPrecition As Integer, intgridpos As Integer, dminvalue As Double, dmaxvalue As Double) As Ext.Net.NumberField
        Dim objNumberField As New Ext.Net.NumberField
        objNumberField.ID = strFieldName
        objNumberField.ClientIDMode = Web.UI.ClientIDMode.Static

        objNumberField.FieldLabel = strLabel
        objNumberField.LabelStyle = "word-wrap: break-word"
        objNumberField.LabelWidth = 150 'intLabelWidth
        'objNumberField.ID = strFieldName
        objNumberField.Name = strFieldName
        objNumberField.AllowBlank = Not bRequired
        objNumberField.BlankText = strLabel & " is required."
        objNumberField.DecimalPrecision = intDecimalPrecition
        objNumberField.MinValue = dminvalue
        objNumberField.MaxValue = dmaxvalue
        objNumberField.Width = objNumberField.LabelWidth + 150
        objNumberField.AnchorHorizontal = "40%"
        If objApproval IsNot Nothing AndAlso objApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update Then
            objNumberField.LabelWidth = 200
        Else
            objNumberField.LabelWidth = 500
        End If
        'objNumberField.LabelWidth = 200
        objNumberField.MinWidth = objNumberField.LabelWidth + 150

        objNumberField.ReadOnly = True

        pn.Add(objNumberField)
        Return objNumberField
    End Function

    Public Function ExtCombo(pn As Ext.Net.Panel, strLabel As String, strFieldName As String, bRequired As Boolean, intgridpos As Integer, strTableRef As String, strFieldKey As String, strFieldDisplay As String, strFilterField As String, strTableRefAlias As String) As Ext.Net.ComboBox
        Using objcombo As New Ext.Net.ComboBox
            objcombo.ID = strFieldName
            objcombo.ClientIDMode = Web.UI.ClientIDMode.Static

            objcombo.FieldLabel = strLabel

            If objApproval IsNot Nothing AndAlso objApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update Then
                objcombo.LabelWidth = 200
            Else
                objcombo.LabelWidth = 500
            End If
            'objcombo.LabelWidth = 200 'intLabelWidth
            objcombo.AnchorHorizontal = "80%"
            objcombo.ID = strFieldName
            objcombo.Name = strFieldName
            objcombo.AllowBlank = Not bRequired
            objcombo.BlankText = strLabel & " is required."
            objcombo.Width = objcombo.LabelWidth + 200
            objcombo.MatchFieldWidth = True
            objcombo.MinChars = "0"
            objcombo.ForceSelection = True

            'objcombo.TypeAhead = True
            'objcombo.EnableRegEx = True
            objcombo.AnyMatch = True

            objcombo.QueryMode = DataLoadMode.Local
            objcombo.ValueField = strFieldKey
            objcombo.DisplayField = strFieldDisplay
            objcombo.TriggerAction = Ext.Net.TriggerAction.All

            Dim objFieldtrigger As New Ext.Net.FieldTrigger
            objFieldtrigger.Icon = Ext.Net.TriggerIcon.Clear
            objFieldtrigger.Hidden = True
            objFieldtrigger.Weight = "-1"
            objcombo.Triggers.Add(objFieldtrigger)

            objcombo.Listeners.Select.Handler = "this.getTrigger(0).show();"

            objcombo.Listeners.TriggerClick.Handler = "if (index == 0) {  this.clearValue(); this.getTrigger(0).hide();}"

            'buat store dan modelnya

            Using objStore As New Ext.Net.Store
                objStore.ID = "_Store_" + objcombo.ID
                objStore.ClientIDMode = Web.UI.ClientIDMode.Static

                Using objModel As New Ext.Net.Model
                    objModel.Fields.Add(strFieldKey, Ext.Net.ModelFieldType.String)
                    objModel.Fields.Add(strFieldDisplay, Ext.Net.ModelFieldType.String)
                    objStore.Model.Add(objModel)
                End Using

                objcombo.Store.Add(objStore)
                objStore.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, System.Data.CommandType.Text, GetQueryRef(strTableRef & " " & strTableRefAlias, strFieldKey, strFieldDisplay, strFilterField), Nothing)
                objStore.DataBind()
            End Using

            objcombo.ReadOnly = True

            pn.Add(objcombo)
            Return objcombo
        End Using
    End Function

    Function GetQueryRef(strTable As String, strfieldkey As String, strfielddisplay As String, strfilter As String) As String
        Dim strquery As String
        strquery = "select " & strfieldkey & ", convert(Varchar(1000),[" & strfieldkey & "])+ ' - '+ convert(varchar(1000), [" & strfielddisplay & "]) as [" & strfielddisplay & "] from " & strTable

        strfilter = strfilter.Replace("@userid", NawaBLL.Common.SessionCurrentUser.UserID)
        strfilter = strfilter.Replace("@PK_MUser_ID", NawaBLL.Common.SessionCurrentUser.PK_MUser_ID)

        If strfilter.Trim.Length > 0 Then
            strquery = strquery & " where " & strfilter
        End If
        Return strquery
    End Function

    Protected Function ExtLabel(pn As Ext.Net.Panel, strID As String, strLabel As String, columnwidth As Double) As Ext.Net.Label
        Dim objLabel As New Ext.Net.Label
        objLabel.ID = strID
        objLabel.ClientIDMode = Web.UI.ClientIDMode.Static
        objLabel.ColumnWidth = columnwidth
        objLabel.Text = strLabel
        objLabel.Margin = 10

        pn.Add(objLabel)
        Return objLabel
    End Function
    '------- END OF LOAD COMPONENT

    Protected Sub BindWorkFlowHistory(store As Ext.Net.Store, listHistory As List(Of MWorkFlow_History))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listHistory)
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Shared Function Accept(ID As Long) As Long
        Dim IDUnikReturn As Long = Nothing
        Using objdb As New CasemanagementEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = NawaBLL.ModuleBLL.GetModuleByModuleName(objApproval.ModuleName)
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            Dim objData As OneFCC_EDD_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(OneFCC_EDD_CLASS))
                            IDUnikReturn = OneFCC_EDD_BLL.SaveAddWithoutApproval(objModule, objData)

                            Dim strQuery As String = " UPDATE MWorkFlow_History SET FK_Unik_ID = '" & IDUnikReturn & "' "
                            strQuery = strQuery & " WHERE FK_Module_ID = 30017 AND FK_Unik_ID = '" & objApproval.ModuleKey & "' "
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                            Dim strQueryNoteHistory As String = " UPDATE OneFCC_EDD_Notes_History SET UNIQUE_KEY = '" & IDUnikReturn & "' "
                            strQueryNoteHistory = strQueryNoteHistory & " WHERE UNIQUE_KEY = '" & objApproval.ModuleKey & "' "
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryNoteHistory, Nothing)

                            Dim strQueryAdditionalInfo As String = " UPDATE OneFCC_EDD_Additional_Info_Customer_Prospect SET UNIQUE_KEY = '" & IDUnikReturn & "' "
                            strQueryAdditionalInfo = strQueryAdditionalInfo & " WHERE UNIQUE_KEY = '" & objApproval.ModuleKey & "' "
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryAdditionalInfo, Nothing)

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objData As OneFCC_EDD_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(OneFCC_EDD_CLASS))

                            'Save data dan Audit Trail sama dengan Without Approval
                            OneFCC_EDD_BLL.SaveEditWithoutApproval(objModule, objData)

                            '25-May-2021 IDUnik ambil dati objData
                            IDUnikReturn = objData.objEDD.PK_OneFCC_EDD_ID

                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objData_Old As OneFCC_EDD_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(OneFCC_EDD_CLASS))

                            'Save data dan Audit Trail sama dengan Without Approval
                            OneFCC_EDD_BLL.SaveDeleteWithoutApproval(objModule, objData_Old)

                            '25-May-2021 IDUnik ambil dati objData
                            IDUnikReturn = objData_Old.objEDD.PK_OneFCC_EDD_ID

                    End Select

                    'Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()

                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                    Return Nothing
                End Try
            End Using
        End Using
        Return IDUnikReturn
    End Function

    'Protected Sub nDSDropDownField_CIFNo_Before_OnValueChanged(sender As Object, e As EventArgs)
    '    Try
    '        If Not String.IsNullOrWhiteSpace(nDSDropDownField_CIFNo_Before.StringValue) Then
    '            Dim strQuery As String = " select CUSTOMERNAME from AML_CUSTOMER where CIFNo = '" & nDSDropDownField_CIFNo_Before.StringValue & "' "
    '            Dim dtRow As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
    '            If dtRow IsNot Nothing AndAlso Not IsDBNull(dtRow("CUSTOMERNAME")) Then
    '                txt_Nama_Before.Value = dtRow("CUSTOMERNAME")
    '            End If
    '        End If
    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub nDSDropDownField_CIFNo_OnValueChanged(sender As Object, e As EventArgs)
    '    Try
    '        If Not String.IsNullOrWhiteSpace(nDSDropDownField_CIFNo.StringValue) Then
    '            Dim strQuery As String = " select CUSTOMERNAME from AML_CUSTOMER where CIFNo = '" & nDSDropDownField_CIFNo.StringValue & "' "
    '            Dim dtRow As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
    '            If dtRow IsNot Nothing AndAlso Not IsDBNull(dtRow("CUSTOMERNAME")) Then
    '                txt_Nama.Value = dtRow("CUSTOMERNAME")
    '            End If
    '        End If
    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub SavingNotesInWorkflow(notes As String, unikkey As String)
        Try
            Using objDB As New CasemanagementDAL.CasemanagementEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        Dim objWorkFlowHistory As New CasemanagementDAL.MWorkFlow_History
                        With objWorkFlowHistory
                            .FK_Module_ID = 30017
                            .Notes = notes
                            .FK_Unik_ID = unikkey
                            .ResponseDate = Now
                            .CreatedDate = Now
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .UserNameExecute = NawaBLL.Common.SessionCurrentUser.UserID
                            '.RoleName = NawaBLL.Common.SessionCurrentUser.MRole.RoleName
                            .FK_MRoleId = NawaBLL.Common.SessionCurrentUser.FK_MRole_ID
                        End With
                        objDB.Entry(objWorkFlowHistory).State = Entity.EntityState.Added
                        objDB.SaveChanges()

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Sub SaveDataChange(objData As OneFCC_EDD_CLASS, objApproval As NawaDAL.ModuleApproval)
        Using objDB As New NawaDAL.NawaDataEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)

                    With objApproval
                        .ModuleField = objXMLData
                    End With


                    objDB.Entry(objApproval).State = Entity.EntityState.Modified
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Protected Sub SaveChangedDataAttachment(objData As OneFCC_EDD_CLASS)
        Try
            Using objDB As New CasemanagementDAL.CasemanagementEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        objDB.Entry(objData).State = Entity.EntityState.Modified
                        objDB.SaveChanges()

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Btn_OpenWindowCustomer_Click()
        Try
            If Not String.IsNullOrEmpty(txt_CIFNo.Value) Then
                Dim strQuery As String = "SELECT COUNT(1) AS JumCust FROM vw_AML_Customer_For_EDD WHERE CIFNo = '" & txt_CIFNo.Value & "'"
                Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                If drResult IsNot Nothing AndAlso Not IsDBNull(drResult("JumCust")) Then
                    If drResult("JumCust") > 0 Then
                        WindowPopUpDataCustomer.Hidden = False
                        LoadDataAMLCustomerByCIF(txt_CIFNo.Value)
                    Else
                        Throw New ApplicationException("No AML Customer Found With CIF = " & txt_CIFNo.Value)
                    End If
                Else
                    Throw New ApplicationException("There is Something wrong when seaching Data AML Customer, Please Report this to Admin")
                End If
            Else
                Throw New ApplicationException("CIF is Empty")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_OpenWindowCustomer_Before_Click()
        Try
            If Not String.IsNullOrEmpty(txt_CIFNo_Before.Value) Then
                Dim strQuery As String = "SELECT COUNT(1) AS JumCust FROM vw_AML_Customer_For_EDD WHERE CIFNo = '" & txt_CIFNo_Before.Value & "'"
                Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                If drResult IsNot Nothing AndAlso Not IsDBNull(drResult("JumCust")) Then
                    If drResult("JumCust") > 0 Then
                        WindowPopUpDataCustomer.Hidden = False
                        LoadDataAMLCustomerByCIF(txt_CIFNo_Before.Value)
                    Else
                        Throw New ApplicationException("No AML Customer Found With CIF = " & txt_CIFNo_Before.Value)
                    End If
                Else
                    Throw New ApplicationException("There is Something wrong when seaching Data AML Customer, Please Report this to Admin")
                End If
            Else
                Throw New ApplicationException("CIF is Empty")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_EDD_CustomerDetail_Back_Click()
        Try
            WindowPopUpDataCustomer.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDataAMLCustomerByCIF(CIF As String)
        Try
            Dim strQuery As String = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo = '" & CIF & "'"
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            If drResult IsNot Nothing Then
                Dim strDateFormat As String = NawaBLL.SystemParameterBLL.GetDateFormat()
                If Not IsDBNull(drResult("CIFNo")) Then
                    CustomerDetail_CIF.Value = drResult("CIFNo")

                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_RISK_RATING_RESULT_HISTORY WHERE CIF_NO = '" & drResult("CIFNo") & "' ORDER BY CreatedDate DESC"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    'If tempDrResult IsNot Nothing Then
                    '    If Not IsDBNull(tempDrResult("RISK_RATING_TOTAL_SCORE")) Then
                    '        CustomerDetail_AMLRiskScore.Value = tempDrResult("RISK_RATING_TOTAL_SCORE")
                    '    End If
                    '    If Not IsDBNull(tempDrResult("RISK_RATING_NAME")) Then
                    '        CustomerDetail_AMLRisk.Value = tempDrResult("RISK_RATING_NAME")
                    '    End If
                    'End If
                Else
                    CustomerDetail_CIF.Value = ""
                    'CustomerDetail_AMLRiskScore.Value = ""
                    'CustomerDetail_AMLRisk.Value = ""
                End If

                If Not IsDBNull(drResult("GlobalCustomerNumber")) Then
                    CustomerDetail_GCN.Value = drResult("GlobalCustomerNumber")
                Else
                    CustomerDetail_GCN.Value = ""
                End If

                If Not IsDBNull(drResult("CUSTOMERNAME")) Then
                    CustomerDetail_CustomerName.Value = drResult("CUSTOMERNAME")
                Else
                    CustomerDetail_CustomerName.Value = ""
                End If

                If Not IsDBNull(drResult("DATEOFDATA")) Then
                    Dim tempDate As Date = drResult("DATEOFDATA")
                    Dim tempDateString As String = tempDate.ToString(strDateFormat)
                    CustomerDetail_DateOfData.Value = tempDateString
                Else
                    CustomerDetail_DateOfData.Value = ""
                End If

                If Not IsDBNull(drResult("DATEOFLASTDUEDILIGENCE")) Then
                    Dim tempDate As Date = drResult("DATEOFLASTDUEDILIGENCE")
                    Dim tempDateString As String = tempDate.ToString(strDateFormat)
                    CustomerDetail_LastDueDiligence.Value = tempDateString
                Else
                    CustomerDetail_LastDueDiligence.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_CUSTOMER_STATUS_CODE")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_CUSTOMER_STATUS WHERE FK_AML_CUSTOMER_STATUS_Code = '" & drResult("FK_AML_CUSTOMER_STATUS_CODE") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("AML_CUSTOMER_STATUS_Name")) Then
                        CustomerDetail_Status.Value = tempDrResult("AML_CUSTOMER_STATUS_Name")
                    Else
                        CustomerDetail_Status.Value = ""
                    End If
                Else
                    CustomerDetail_Status.Value = ""
                End If

                If Not IsDBNull(drResult("DATEOFBIRTH")) Then
                    Dim tempDate As Date = drResult("DATEOFBIRTH")
                    Dim tempDateString As String = tempDate.ToString(strDateFormat)
                    CustomerDetail_DateOfBirth.Value = tempDateString
                Else
                    CustomerDetail_DateOfBirth.Value = ""
                End If

                If Not IsDBNull(drResult("PLACEOFBIRTH")) Then
                    CustomerDetail_PlaceOfBirth.Value = drResult("PLACEOFBIRTH")
                Else
                    CustomerDetail_PlaceOfBirth.Value = ""
                End If

                If Not IsDBNull(drResult("MOTHERMAIDEN")) Then
                    CustomerDetail_MotherName.Value = drResult("MOTHERMAIDEN")
                Else
                    CustomerDetail_MotherName.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_JenisKelamin_Code")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_JENIS_KELAMIN WHERE FK_AML_Jenis_Kelamin_Code ='" & drResult("FK_AML_JenisKelamin_Code") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("Jenis_Kelamin_Name")) Then
                        CustomerDetail_Gender.Value = tempDrResult("Jenis_Kelamin_Name")
                    Else
                        CustomerDetail_Gender.Value = ""
                    End If
                Else
                    CustomerDetail_Gender.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_MARITAL_STATUS")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_MARITAL_STATUS WHERE FK_AML_MARITAL_STATUS_CODE = '" & drResult("FK_AML_MARITAL_STATUS") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("MARITAL_STATUS_NAME")) Then
                        CustomerDetail_MartialStatus.Value = tempDrResult("MARITAL_STATUS_NAME")
                    Else
                        CustomerDetail_MartialStatus.Value = ""
                    End If
                Else
                    CustomerDetail_MartialStatus.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_CITIZENSHIP_CODE")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_COUNTRY WHERE FK_AML_COUNTRY_Code = '" & drResult("FK_AML_CITIZENSHIP_CODE") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("AML_COUNTRY_Name")) Then
                        CustomerDetail_Citizenship.Value = tempDrResult("AML_COUNTRY_Name")
                    Else
                        CustomerDetail_Citizenship.Value = ""
                    End If
                Else
                    CustomerDetail_Citizenship.Value = ""
                End If

                If Not IsDBNull(drResult("WORK_PLACE")) Then
                    CustomerDetail_WorkPlace.Value = drResult("WORK_PLACE")
                Else
                    CustomerDetail_WorkPlace.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_Customer_Type_Code")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_CUSTOMER_TYPE WHERE FK_AML_Customer_Type_Code = '" & drResult("FK_AML_Customer_Type_Code") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("Customer_Type_Name")) Then
                        CustomerDetail_CustomerType.Value = tempDrResult("Customer_Type_Name")
                    Else
                        CustomerDetail_CustomerType.Value = ""
                    End If
                Else
                    CustomerDetail_CustomerType.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_Customer_SUB_Type_Code")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_CUSTOMER_SUBTYPE WHERE FK_AML_CUSTOMER_SUBTYPE_Code = '" & drResult("FK_AML_Customer_SUB_Type_Code") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("CUSTOMER_SUBTYPE_Name")) Then
                        CustomerDetail_CustomerSubType.Value = tempDrResult("CUSTOMER_SUBTYPE_Name")
                    Else
                        CustomerDetail_CustomerSubType.Value = ""
                    End If
                Else
                    CustomerDetail_CustomerSubType.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_PEKERJAAN_CODE")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_PEKERJAAN WHERE FK_AML_PEKERJAAN_Code = '" & drResult("FK_AML_PEKERJAAN_CODE") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("AML_PEKERJAAN_Name")) Then
                        CustomerDetail_BusinessType.Value = tempDrResult("AML_PEKERJAAN_Name")
                    Else
                        CustomerDetail_BusinessType.Value = ""
                    End If
                Else
                    CustomerDetail_BusinessType.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_INDUSTRY_CODE")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_INDUSTRY WHERE FK_AML_INDUSTRY_Code = '" & drResult("FK_AML_INDUSTRY_CODE") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("AML_INDUSTRY_Name")) Then
                        CustomerDetail_Industry.Value = tempDrResult("AML_INDUSTRY_Name")
                    Else
                        CustomerDetail_Industry.Value = ""
                    End If
                Else
                    CustomerDetail_Industry.Value = ""
                End If

                If Not IsDBNull(drResult("OpeningDate")) Then
                    Dim tempDate As Date = drResult("OpeningDate")
                    Dim tempDateString As String = tempDate.ToString(strDateFormat)
                    CustomerDetail_OpeningDate.Value = tempDateString
                Else
                    CustomerDetail_OpeningDate.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_Entity_Code")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_Entity WHERE FK_AML_ENTITY_CODE = '" & drResult("FK_AML_Entity_Code") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("Entity_Name")) Then
                        CustomerDetail_CreationEntity.Value = tempDrResult("Entity_Name")
                    Else
                        CustomerDetail_CreationEntity.Value = ""
                    End If
                Else
                    CustomerDetail_CreationEntity.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_Creation_Branch_Code")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_BRANCH WHERE FK_AML_BRANCH_CODE ='" & drResult("FK_AML_Creation_Branch_Code") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("BRANCH_NAME")) Then
                        CustomerDetail_CreationBranch.Value = tempDrResult("BRANCH_NAME")
                    Else
                        CustomerDetail_CreationBranch.Value = ""
                    End If
                Else
                    CustomerDetail_CreationBranch.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_SALES_OFFICER_CODE")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_SALESOFFICER WHERE FK_AML_SALESOFFICER_CODE ='" & drResult("FK_AML_SALES_OFFICER_CODE") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("SALESOFFICER_NAME")) Then
                        CustomerDetail_SalesOfficer.Value = tempDrResult("SALESOFFICER_NAME")
                    Else
                        CustomerDetail_SalesOfficer.Value = ""
                    End If
                Else
                    CustomerDetail_SalesOfficer.Value = ""
                End If

                '' Edit 22-Feb-2023
                'If Not IsDBNull(drResult("SOURCE_OF_FUND")) Then
                '    CustomerDetail_SourceOfFound.Value = drResult("SOURCE_OF_FUND")
                'Else
                '    CustomerDetail_SourceOfFound.Value = ""
                'End If

                If Not IsDBNull(drResult("SOURCE_OF_FUND")) Then
                    strQuery = "SELECT TOP 1 AML_INCOME_TYPE_Name FROM AML_INCOME_TYPE WHERE FK_AML_INCOME_TYPE_Code ='" & drResult("SOURCE_OF_FUND") & "'"
                    Dim drIncomeType As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                    Dim strTempSourceOfFund As String = drResult("SOURCE_OF_FUND")
                    If drIncomeType IsNot Nothing AndAlso Not IsDBNull(drIncomeType("AML_INCOME_TYPE_Name")) Then
                        strTempSourceOfFund = strTempSourceOfFund & " ( " & drIncomeType("AML_INCOME_TYPE_Name") & " ) "
                    End If
                    CustomerDetail_SourceOfFound.Value = strTempSourceOfFund
                Else
                    CustomerDetail_SourceOfFound.Value = ""
                End If
                '' End 22-Feb-2023

                If Not IsDBNull(drResult("TUJUAN_DANA")) Then
                    CustomerDetail_PurposeOfFound.Value = drResult("TUJUAN_DANA")
                Else
                    CustomerDetail_PurposeOfFound.Value = ""
                End If
            Else
                Throw New ApplicationException("No AML Customer Data Found With CIF = " & CIF)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SavingDataHistory(unikID As String, notes As String, fileName As String, fileData As Byte())
        Try
            Dim objParam(4) As System.Data.SqlClient.SqlParameter

            objParam(0) = New System.Data.SqlClient.SqlParameter
            objParam(0).ParameterName = "@UnikID"
            objParam(0).Value = unikID
            objParam(0).SqlDbType = SqlDbType.VarChar

            objParam(1) = New System.Data.SqlClient.SqlParameter
            objParam(1).ParameterName = "@Notes"
            objParam(1).Value = notes
            objParam(1).SqlDbType = SqlDbType.VarChar

            objParam(2) = New System.Data.SqlClient.SqlParameter
            objParam(2).ParameterName = "@FileName"
            If fileName IsNot Nothing Then
                objParam(2).Value = fileName
            Else
                objParam(2).Value = DBNull.Value
            End If
            objParam(2).SqlDbType = SqlDbType.VarChar

            objParam(3) = New System.Data.SqlClient.SqlParameter
            objParam(3).ParameterName = "@FileData"
            If fileData IsNot Nothing Then
                objParam(3).Value = fileData
            Else
                objParam(3).Value = DBNull.Value
            End If
            objParam(3).SqlDbType = SqlDbType.VarBinary

            objParam(4) = New System.Data.SqlClient.SqlParameter
            objParam(4).ParameterName = "@UserID"
            objParam(4).Value = NawaBLL.Common.SessionCurrentUser.UserID
            objParam(4).SqlDbType = SqlDbType.VarChar

            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_EDD_NoteHistory_SaveData", objParam)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub GridcommandDownloadAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Download" Then
                Filetodownload = ID
            Else
                Throw New ApplicationException("The command could not be identified, please report this problem to admin")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Sub DownloadFileAttachmentFromHistory_Direct()
        Try
            If String.IsNullOrEmpty(Filetodownload) Then
                Exit Sub
            End If

            Dim strQuery As String = " SELECT TOP 1 FILE_ATTACHMENTName, FILE_ATTACHMENT FROM OneFCC_EDD_Notes_History WHERE PK_ONEFCC_EDD_NOTES_HISTORY_ID = " & Filetodownload
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            If drResult IsNot Nothing AndAlso Not IsDBNull(drResult("FILE_ATTACHMENTName")) AndAlso Not IsDBNull(drResult("FILE_ATTACHMENT")) Then
                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & drResult("FILE_ATTACHMENTName"))
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(drResult("FILE_ATTACHMENT"))
                Response.End()
            Else
                Throw New ApplicationException("No Files to Download")
            End If

            Filetodownload = Nothing
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class