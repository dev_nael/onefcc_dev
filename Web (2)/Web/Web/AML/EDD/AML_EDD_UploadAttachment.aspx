﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AML_EDD_UploadAttachment.aspx.vb" Inherits="AML_EDD_UploadAttachment" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" AutoScroll="true">
        <Items>
            <ext:FileUploadField runat="server" ID="File_Attachment"  FieldLabel="File Attachment" AnchorHorizontal="100%" AllowBlank="true" LabelWidth="100">
                <%--<Listeners>
                    <Change Handler="FileUploadField_ChangeHandler"></Change>
                </Listeners>--%>
                <RightButtons>
                    <ext:Button ID="btnClear" runat="server" Text="" Icon="Erase" ClientIDMode="Static">
                        <Listeners>
                            <Click Handler="#{File_Attachment}.reset();"></Click>
                        </Listeners>
                    </ext:Button>
                </RightButtons>
            </ext:FileUploadField>
            <ext:Button runat="server" ID="btn_EDD_Attachment_Submit" Icon="DiskDownload" Hidden="false" Text="Save Uploaded File As Attachment">
                <DirectEvents>
                    <Click OnEvent="Btn_EDD_Attachment_Submit_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Items>
    </ext:FormPanel>
</asp:Content>

