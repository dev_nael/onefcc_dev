﻿Imports Ext
Imports Elmah
Imports System.Data
Imports CasemanagementBLL

Partial Class AML_EDD_Detail
    Inherits ParentPage

    Public Property IDModule() As String
        Get
            Return Session("AML_EDD_Detail.IDModule")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_Detail.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("AML_EDD_Detail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AML_EDD_Detail.IDUnik") = value
        End Set
    End Property

    Public Property objOneFCC_EDD_CLASS() As OneFCC_EDD_CLASS
        Get
            Return Session("AML_EDD_Detail.objOneFCC_EDD_CLASS")
        End Get
        Set(ByVal value As OneFCC_EDD_CLASS)
            Session("AML_EDD_Detail.objOneFCC_EDD_CLASS") = value
        End Set
    End Property

    Public Property DataTabelEDDScore As DataTable
        Get
            Return Session("AML_EDD_Detail.DataTabelEDDScore")
        End Get
        Set(ByVal value As DataTable)
            Session("AML_EDD_Detail.DataTabelEDDScore") = value
        End Set
    End Property

    Public Property InputNullPronoun() As String
        Get
            Return Session("AML_EDD_Detail.InputNullPronoun")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_Detail.InputNullPronoun") = value
        End Set
    End Property

    Public Property Filetodownload() As String
        Get
            Return Session("AML_EDD_Detail.Filetodownload")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_Detail.Filetodownload") = value
        End Set
    End Property

    Public Property IsUsingScorePointAffectingAMLCustomer() As String
        Get
            Return Session("AML_EDD_Detail.IsUsingScorePointAffectingAMLCustomer")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_Detail.IsUsingScorePointAffectingAMLCustomer") = value
        End Set
    End Property

    Sub ClearSession()
        cmb_EDD_TYPE_ID.SetTextValue("")
        cmb_Customer_Type_ID.SetTextValue("")
        cmb_Customer_Segment_ID.SetTextValue("")
        objOneFCC_EDD_CLASS = New OneFCC_EDD_CLASS
        DataTabelEDDScore = New DataTable
        LoadColumnDataTabelEDDScore()
        InputNullPronoun = Nothing
        Filetodownload = Nothing
        IsUsingScorePointAffectingAMLCustomer = Nothing

        Dim strQuery = " SELECT ParameterValue FROM AML_Global_Parameter WHERE PK_GlobalReportParameter_ID = 4 "
        Dim dataRowGetParameter As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQuery)
        If dataRowGetParameter IsNot Nothing AndAlso Not IsDBNull(dataRowGetParameter("ParameterValue")) Then
            InputNullPronoun = dataRowGetParameter("ParameterValue")
        End If

        strQuery = " SELECT ParameterValue FROM AML_Global_Parameter WHERE PK_GlobalReportParameter_ID = 6 "
        Dim dataRowGetParameterAffectingCustomer As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQuery)
        If dataRowGetParameterAffectingCustomer IsNot Nothing AndAlso Not IsDBNull(dataRowGetParameterAffectingCustomer("ParameterValue")) Then
            IsUsingScorePointAffectingAMLCustomer = dataRowGetParameterAffectingCustomer("ParameterValue")
        End If
    End Sub

    Protected Sub LoadColumnDataTabelEDDScore()
        Try
            DataTabelEDDScore.Columns.Add(New DataColumn("Question_ID", GetType(Long)))
            DataTabelEDDScore.Columns.Add(New DataColumn("Score", GetType(Integer)))
            DataTabelEDDScore.Columns.Add(New DataColumn("Type", GetType(String)))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub AML_EDD_Detail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As Integer = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)


            If Not Ext.Net.X.IsAjaxRequest Then
                'Tampilkan notifikasi jika ada di bucket Pending Approval
                'If OneFCC_EDD_BLL.IsExistsInApproval(ObjModule.ModuleName, IDUnik) Then
                '    LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."

                '    Panelconfirmation.Hidden = False
                '    FormPanelInput.Hidden = True
                'End If

                ClearSession()

                FormPanelInput.Title = ObjModule.ModuleLabel & " - Detail"

                'SetCommandColumnLocation()

                objOneFCC_EDD_CLASS = OneFCC_EDD_BLL.GetEDDClassByID(IDUnik)

                Dim strQuery As String = " SELECT PK_ONEFCC_EDD_NOTES_HISTORY_ID, NOTES, FILE_ATTACHMENTName, CreatedBy, CreatedDate "
                strQuery = strQuery & " FROM OneFCC_EDD_Notes_History WHERE UNIQUE_KEY = '" & IDUnik & "' ORDER BY CreatedDate DESC "
                'Dim strQuery As String = " select history.PK_MWorkflow_History_ID,history.CreatedDate,history.ResponseDate,history.UserNameExecute,history.Notes, "
                'strQuery = strQuery & " case  when approval.PK_ModuleApproval_ID is not null then 'In Approval' end as HistoryStatus "
                'strQuery = strQuery & " from MWorkFlow_History history left join ModuleApproval approval on history.FK_ModuleApproval_ID = approval.PK_ModuleApproval_ID "
                'strQuery = strQuery & " where FK_Module_ID = 30017 and FK_Unik_ID = '" & IDUnik & "' order by ResponseDate desc "
                Dim dataNoteHistory As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery,)
                StoreNotesHistory.DataSource = dataNoteHistory
                StoreNotesHistory.DataBind()

                If objOneFCC_EDD_CLASS IsNot Nothing AndAlso
                    objOneFCC_EDD_CLASS.objEDD IsNot Nothing AndAlso
                    objOneFCC_EDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Segment_Code IsNot Nothing AndAlso
                    objOneFCC_EDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Type_Code IsNot Nothing AndAlso
                    objOneFCC_EDD_CLASS.objEDD.FK_OneFCC_EDD_Type_Code IsNot Nothing Then
                    'Harus di luar sini biar kebaca saat FindControl
                    'Load_Questionnaire()
                    'Load Answer
                    'Load_Answer()

                    LoadToFormPanel(FormPanelCustomer, "Customer")
                    If objOneFCC_EDD_CLASS.objEDD.IsHaveBeneficialOwner Then
                        LoadToFormPanel(FormPanelBenificialOwner, "BO")
                        FormPanelBenificialOwner.Hidden = False
                    End If
                    Load_Answer()
                Else
                    'LblConfirmation.Text = "There Is some issue While checking data, Please contact admin To check this issue. \r\n" &
                    '        "FK_OneFCC_Questionnaire_Customer_Segment_Code, FK_OneFCC_Questionnaire_Customer_Type_Code, FK_OneFCC_EDD_Type_Code Is Required And shouldn't be null"
                    'Panelconfirmation.Hidden = False
                    'FormPanelInput.Hidden = True
                    Throw New ApplicationException("Unable to find EDD Type, Customer Type Or Customer Segment, Please contact admin to check this issue.")
                End If
            End If
            'Harus di luar sini biar kebaca saat FindControl
            'Load_Questionnaire()

            'Load Answer
            'Load_Answer()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    '------- LOAD QUESTIONNAIRE
    Enum EnumExtType
        DateField = 1
        DropDownField = 2
        NumberField = 4
        TextField = 5
        Radio = 6
        DisplayField = 7
        FileUpload = 8
        Password = 9
        HTMLEditor = 10
        FormulaField = 11
        MultiCombo = 12
    End Enum

    Protected Sub Load_Questionnaire(objpanelinput As Ext.Net.FormPanel, customerType As String)
        Try
            Dim strQuery As String = ""
            Dim intSequence As Integer
            Dim intSequenceChild As Integer

            Dim Customer_Type_ID As String = customerType
            Dim Customer_Segment_ID As String = objOneFCC_EDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Segment_Code
            Dim EDD_Type_ID As String = objOneFCC_EDD_CLASS.objEDD.FK_OneFCC_EDD_Type_Code

            'Load Question Group 
            'strQuery = "SELECT DISTINCT ofqg.PK_OneFCC_Question_Group_ID, ofqg.Question_Group_Name, ofqg.Sequence"
            'strQuery = strQuery & " FROM OneFCC_Questionnaire ofq"
            'strQuery = strQuery & " JOIN OneFCC_Question_Group AS ofqg ON ofq.FK_OneFCC_Question_Group_ID = ofqg.PK_OneFCC_Question_Group_ID"
            'strQuery = strQuery & " WHERE ofqg.FK_OneFCC_Question_Type_Code = 'EDD'"
            'strQuery = strQuery & " ORDER BY ofqg.Sequence"
            strQuery = "SELECT DISTINCT questiongroup.PK_OneFCC_Question_Group_ID, questiongroup.Question_Group_Name, questiongroup.Sequence, questiongroup.Question_Group_Code"
            strQuery = strQuery & " FROM OneFCC_Question question"
            strQuery = strQuery & " JOIN OneFCC_Question_Mapping_EDD mapping ON question.PK_OneFCC_Question_ID = mapping.FK_OneFCC_Question_ID"
            strQuery = strQuery & " JOIN OneFCC_Question_Group questiongroup ON questiongroup.Question_Group_Code = mapping.FK_OneFCC_Question_Group_Code"
            strQuery = strQuery & " WHERE mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
            strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
            strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
            strQuery = strQuery & " ORDER BY questiongroup.Sequence"

            Dim dtQuestionGroup As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
            For Each row In dtQuestionGroup.Rows
                'Dim objPanel = ExtPanel(FormPanelInput, "Panel_" & row("PK_OneFCC_Question_Group_ID"), row("Sequence") & ". " & row("Question_Group_Name"))

                'Load Input
                'strQuery = "SELECT ofq.PK_OneFCC_Questionnaire_ID, ofq.QUESTION, ofq.IS_REQUIRED, ofq.FK_ExtType_ID, ofq.Table_Reference_Name, ofq.Table_Reference_Field_Key, ofq.Table_Reference_Field_Display_Name, ofq.Table_Reference_Filter"
                'strQuery = strQuery & " FROM OneFCC_Questionnaire ofq"
                'strQuery = strQuery & " WHERE ofq.FK_OneFCC_Question_Group_ID = " & row("PK_OneFCC_Question_Group_ID")
                'strQuery = strQuery & " AND ISNULL(ofq.FK_OneFCC_Questionnaire_Parent_ID,'')=''"    'Tampilkan dulu pertanyaan parent
                'strQuery = strQuery & " ORDER BY ofq.SEQUENCE"
                strQuery = "SELECT mapping.PK_OneFCC_Question_Mapping_EDD_ID, question.PK_OneFCC_Question_ID, question.QUESTION, question.IS_REQUIRED, question.FK_ExtType_ID,"
                strQuery = strQuery & " question.Table_Reference_Name, question.Table_Reference_Field_Key, question.Table_Reference_Field_Display_Name, question.Table_Reference_Filter"
                strQuery = strQuery & " ,point.PK_OneFCC_EDD_Mapping_Question_Answer_Point_ID"
                strQuery = strQuery & " FROM OneFCC_Question question"
                strQuery = strQuery & " JOIN OneFCC_Question_Mapping_EDD mapping on question.PK_OneFCC_Question_ID = mapping.FK_OneFCC_Question_ID"
                strQuery = strQuery & " left join OneFCC_EDD_Mapping_Question_Answer_Point point on mapping.PK_OneFCC_Question_Mapping_EDD_ID = point.FK_OneFCC_Question_Mapping_EDD_ID"
                strQuery = strQuery & " WHERE mapping.FK_OneFCC_Question_Group_Code = '" & row("Question_Group_Code") & "'"
                strQuery = strQuery & " AND ISNULL(mapping.FK_OneFCC_Question_Parent_ID,'')=''"
                strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
                strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
                strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
                strQuery = strQuery & " ORDER BY mapping.Sequence"

                Dim dtQuestion As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
                intSequence = 1

                If dtQuestion.Rows.Count > 0 Then
                    Dim objPanel = ExtPanel(objpanelinput, objpanelinput.ID & "_Panel_" & row("PK_OneFCC_Question_Group_ID"), row("Sequence") & ". " & row("Question_Group_Name"), False)
                    For Each question In dtQuestion.Rows
                        Dim isRequired As Boolean = False
                        If Not IsDBNull(question("IS_REQUIRED")) Then
                            isRequired = question("IS_REQUIRED")
                        Else
                            isRequired = False
                        End If

                        Select Case CType(question("FK_ExtType_ID"), EnumExtType)
                            'Case EnumExtType.TextField
                            '    ExtText(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID"), isRequired, 8000, intSequence)
                            'Case EnumExtType.NumberField
                            '    ExtNumber(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID"), isRequired, 0, intSequence, Long.MinValue, Long.MaxValue)
                            'Case EnumExtType.DateField
                            '    ExtDate(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID"), isRequired, 150, intSequence)
                            'Case EnumExtType.DropDownField
                            '    ExtCombo(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID"), isRequired, intSequence, question("Table_Reference_Name"), question("Table_Reference_Field_Key"), question("Table_Reference_Field_Display_Name"), question("Table_Reference_Filter"), "")
                            'Case EnumExtType.Radio
                            '    ExtRadio(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Questionnaire_ID"), isRequired, 8000, intSequence)

                            Case EnumExtType.TextField
                                ExtText(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequence)
                            Case EnumExtType.NumberField
                                ExtNumber(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 0, intSequence, Long.MinValue, Long.MaxValue)
                            Case EnumExtType.DateField
                                ExtDate(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 150, intSequence)
                            Case EnumExtType.DropDownField
                                Dim question_Table_Reference_Filter As String
                                If Not IsDBNull(question("Table_Reference_Filter")) Then
                                    question_Table_Reference_Filter = question("Table_Reference_Filter")
                                Else
                                    question_Table_Reference_Filter = ""
                                End If
                                If Not IsDBNull(question("Table_Reference_Name")) AndAlso Not IsDBNull(question("Table_Reference_Field_Key")) AndAlso Not IsDBNull(question("Table_Reference_Field_Display_Name")) Then
                                    'ExtCombo(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequence, question("Table_Reference_Name"), question("Table_Reference_Field_Key"), question("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "", isUsingTriger, customerType)
                                    ExtCombo(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequence, question("Table_Reference_Name"), question("Table_Reference_Field_Key"), question("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "")
                                Else
                                    Throw New ApplicationException("Incomplete Question Data On Question ID " & question("PK_OneFCC_Question_ID") & ", Please Report this issue to Admin")
                                End If
                            Case EnumExtType.Radio
                                ExtRadio(objPanel, intSequence & ". " & question("QUESTION"), "ANSWER_" & question("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequence)
                        End Select

                        intSequence += 1

                        'Tampilkan pertanyaan Anak
                        'strQuery = "SELECT ofq.PK_OneFCC_Questionnaire_ID, ofq.QUESTION, ofq.IS_REQUIRED, ofq.FK_ExtType_ID, ofq.Table_Reference_Name, ofq.Table_Reference_Field_Key, ofq.Table_Reference_Field_Display_Name, ofq.Table_Reference_Filter"
                        'strQuery = strQuery & " FROM OneFCC_Questionnaire ofq"
                        'strQuery = strQuery & " WHERE ISNULL(ofq.FK_OneFCC_Questionnaire_Parent_ID,'')<>''"
                        'strQuery = strQuery & " And ISNULL(ofq.FK_OneFCC_Questionnaire_Parent_ID,'')=" & question("PK_OneFCC_Questionnaire_ID")
                        'strQuery = strQuery & " ORDER BY ofq.SEQUENCE"
                        strQuery = "SELECT mapping.PK_OneFCC_Question_Mapping_EDD_ID, ofq.PK_OneFCC_Question_ID, ofq.QUESTION, ofq.IS_REQUIRED, ofq.FK_ExtType_ID, ofq.Table_Reference_Name, ofq.Table_Reference_Field_Key, ofq.Table_Reference_Field_Display_Name, ofq.Table_Reference_Filter"
                        strQuery = strQuery & " ,point.PK_OneFCC_EDD_Mapping_Question_Answer_Point_ID"
                        strQuery = strQuery & " FROM OneFCC_Question ofq"
                        strQuery = strQuery & " JOIN OneFCC_Question_Mapping_EDD mapping"
                        strQuery = strQuery & " left join OneFCC_EDD_Mapping_Question_Answer_Point point on mapping.PK_OneFCC_Question_Mapping_EDD_ID = point.FK_OneFCC_Question_Mapping_EDD_ID"
                        strQuery = strQuery & " on ofq.PK_OneFCC_Question_ID = mapping.FK_OneFCC_Question_ID"
                        strQuery = strQuery & " WHERE ISNULL(mapping.FK_OneFCC_Question_Parent_ID,'')=" & question("PK_OneFCC_Question_Mapping_EDD_ID")
                        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Group_Code = '" & row("Question_Group_Code") & "'"
                        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
                        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
                        strQuery = strQuery & " AND mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
                        strQuery = strQuery & " ORDER BY mapping.Sequence"

                        Dim dtQuestionChild As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
                        intSequenceChild = 1
                        For Each child In dtQuestionChild.Rows
                            'isRequired = child("IS_REQUIRED")
                            If Not IsDBNull(child("IS_REQUIRED")) Then
                                isRequired = child("IS_REQUIRED")
                            Else
                                isRequired = False
                            End If
                            Dim strPrefix As String = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & LCase(Convert.ToChar(intSequenceChild + 64))
                            Select Case CType(child("FK_ExtType_ID"), EnumExtType)
                                'Case EnumExtType.TextField
                                '    ExtText(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID"), isRequired, 8000, intSequence)
                                'Case EnumExtType.NumberField
                                '    ExtNumber(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID"), isRequired, 0, intSequence, Long.MinValue, Long.MaxValue)
                                'Case EnumExtType.DateField
                                '    ExtDate(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID"), isRequired, 150, intSequence)
                                'Case EnumExtType.DropDownField
                                '    ExtCombo(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID"), isRequired, intSequence, child("Table_Reference_Name"), child("Table_Reference_Field_Key"), child("Table_Reference_Field_Display_Name"), child("Table_Reference_Filter"), "")
                                'Case EnumExtType.Radio
                                '    ExtRadio(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Questionnaire_ID"), isRequired, 8000, intSequence)
                                Case EnumExtType.TextField
                                    ExtText(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequenceChild)
                                Case EnumExtType.NumberField
                                    ExtNumber(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 0, intSequenceChild, Long.MinValue, Long.MaxValue)
                                Case EnumExtType.DateField
                                    ExtDate(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 150, intSequenceChild)
                                Case EnumExtType.DropDownField
                                    Dim question_Table_Reference_Filter As String
                                    If Not IsDBNull(child("Table_Reference_Filter")) Then
                                        question_Table_Reference_Filter = child("Table_Reference_Filter")
                                    Else
                                        question_Table_Reference_Filter = ""
                                    End If
                                    If Not IsDBNull(child("Table_Reference_Name")) AndAlso Not IsDBNull(child("Table_Reference_Field_Key")) AndAlso Not IsDBNull(child("Table_Reference_Field_Display_Name")) Then
                                        'ExtCombo(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequenceChild, child("Table_Reference_Name"), child("Table_Reference_Field_Key"), child("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "", isUsingTriger, customerType)
                                        ExtCombo(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, intSequenceChild, child("Table_Reference_Name"), child("Table_Reference_Field_Key"), child("Table_Reference_Field_Display_Name"), question_Table_Reference_Filter, "")
                                    Else
                                        Throw New ApplicationException("Incomplete Question Data On Question ID " & child("PK_OneFCC_Question_ID") & ", Please Report this issue to Admin")
                                    End If
                                Case EnumExtType.Radio
                                    ExtRadio(objPanel, strPrefix & ". " & child("QUESTION"), "ANSWER_" & child("PK_OneFCC_Question_Mapping_EDD_ID"), isRequired, 8000, intSequenceChild)
                            End Select

                            intSequenceChild += 1
                        Next
                    Next
                End If
            Next
            If IsUsingScorePointAffectingAMLCustomer = "1" Then
                Dim objPanelRisk = ExtPanel(objpanelinput, objpanelinput.ID & "_Panel_Risk", "", True)
                Dim typeclass As CasemanagementDAL.OneFCC_Question_Customer_Type = OneFCC_EDD_BLL.GetEDDCustomerTypeByCode(customerType)
                ExtLabel(objPanelRisk, objPanelRisk.ID & "_" & customerType & "_Label", "Risk Rating " & typeclass.Description, 0.5)
                ExtLabel(objPanelRisk, objPanelRisk.ID & "_" & customerType & "_Score", "0", 0.1)
                ExtLabel(objPanelRisk, objPanelRisk.ID & "_" & customerType & "_RiskRating", "Low", 0.4)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    '------- END OF LOAD QUESTIONNAIRE

    'Protected Sub btn_EDD_Submit_Click()
    '    Try
    '        'Validate Data
    '        If String.IsNullOrEmpty(cmb_EDD_TYPE_ID.SelectedItemValue) Then
    '            Throw New ApplicationException("EDD Type is required.")
    '        Else
    '            If cmb_EDD_TYPE_ID.SelectedItemValue = "C" And String.IsNullOrWhiteSpace(txt_CIFNo.Value) Then
    '                Throw New ApplicationException("CIF is required if EDD Type is Customer.")
    '            End If
    '        End If
    '        If String.IsNullOrEmpty(txt_Nama.Value) Or String.IsNullOrWhiteSpace(txt_Nama.Value) Then
    '            Throw New ApplicationException("Nama is required.")
    '        End If

    '        'Create New object
    '        With objOneFCC_EDD_CLASS.objEDD
    '            .FK_OneFCC_EDD_Type_Code = cmb_EDD_TYPE_ID.SelectedItemValue
    '            If cmb_EDD_TYPE_ID.SelectedItemValue = "C" Then
    '                .CIFNo = txt_CIFNo.Value
    '            Else
    '                .CIFNo = Nothing
    '            End If
    '            .Nama = txt_Nama.Value
    '            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
    '            .LastUpdateDate = Now
    '        End With

    '        'Validate and Collect data in one step. Jadi gak 2x looping
    '        Dim strQuery As String = ""

    '        strQuery = "SELECT DISTINCT ofqg.PK_OneFCC_Question_Group_ID, ofqg.Question_Group_Name, ofqg.Sequence"
    '        strQuery = strQuery & " FROM OneFCC_Questionnaire ofq"
    '        strQuery = strQuery & " JOIN OneFCC_Question_Group AS ofqg ON ofq.FK_OneFCC_Question_Group_ID = ofqg.PK_OneFCC_Question_Group_ID"
    '        strQuery = strQuery & " WHERE ofqg.FK_OneFCC_Question_Type_Code = 'EDD'"
    '        strQuery = strQuery & " ORDER BY ofqg.Sequence"

    '        Dim dtQuestionGroup As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
    '        For Each row In dtQuestionGroup.Rows
    '            Dim objPanel = FormPanelInput.FindControl("Panel_" & row("PK_OneFCC_Question_Group_ID"))
    '            If objPanel Is Nothing Then
    '                Continue For
    '            End If

    '            strQuery = "SELECT ofq.PK_OneFCC_Questionnaire_ID, ofq.QUESTION, ofq.IS_REQUIRED, ofq.FK_ExtType_ID, ofq.Table_Reference_Name, ofq.Table_Reference_Field_Key, ofq.Table_Reference_Field_Display_Name, ofq.Table_Reference_Filter"
    '            strQuery = strQuery & " FROM OneFCC_Questionnaire ofq"
    '            strQuery = strQuery & " JOIN OneFCC_Question_Group AS ofqg ON ofq.FK_OneFCC_Question_Group_ID = ofqg.PK_OneFCC_Question_Group_ID"
    '            strQuery = strQuery & " WHERE ofqg.PK_OneFCC_Question_Group_ID = " & row("PK_OneFCC_Question_Group_ID")
    '            strQuery = strQuery & " ORDER BY ofq.SEQUENCE"

    '            Dim dtQuestion As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)

    '            For Each item In dtQuestion.Rows
    '                Dim intQuestionID As Integer = item("PK_OneFCC_Questionnaire_ID")
    '                Dim strQuestion As String = item("QUESTION")
    '                Dim strAnswer As String = Nothing
    '                Dim isRequired As Boolean = item("IS_REQUIRED")
    '                Dim intPK As Long = -1

    '                Select Case CType(item("FK_ExtType_ID"), EnumExtType)
    '                    Case EnumExtType.TextField
    '                        Dim objfield As TextField = objPanel.FindControl("ANSWER_" & intQuestionID)
    '                        If Not objfield Is Nothing Then
    '                            If isRequired And String.IsNullOrEmpty(objfield.Text) Then
    '                                Throw New ApplicationException(objfield.FieldLabel & " is required.")
    '                            End If

    '                            If Not String.IsNullOrEmpty(objfield.Text) Then
    '                                strAnswer = objfield.Text.Trim
    '                            End If
    '                        End If
    '                    Case EnumExtType.NumberField
    '                        Dim objfield As NumberField = objPanel.FindControl("ANSWER_" & intQuestionID)
    '                        If Not objfield Is Nothing Then
    '                            If isRequired And (String.IsNullOrEmpty(objfield.Text) Or objfield.Text = "N/A" Or objfield.Text = "NA") Then
    '                                Throw New ApplicationException(objfield.FieldLabel & " is required.")
    '                            End If

    '                            If Not (String.IsNullOrEmpty(objfield.Text) Or objfield.Text = "N/A" Or objfield.Text = "NA") Then
    '                                strAnswer = CLng(objfield.Value).ToString
    '                            End If
    '                        End If
    '                    Case EnumExtType.DateField
    '                        Dim objfield As DateField = objPanel.FindControl("ANSWER_" & intQuestionID)
    '                        If Not objfield Is Nothing Then
    '                            If isRequired And CDate(objfield.Value) = DateTime.MinValue Then
    '                                Throw New ApplicationException(objfield.FieldLabel & " is required.")
    '                            End If

    '                            If Not CDate(objfield.Value) = DateTime.MinValue Then
    '                                strAnswer = NawaBLL.Common.ConvertToDate(NawaBLL.SystemParameterBLL.GetDateFormat, objfield.RawText).ToString("yyyy-MM-dd")
    '                            End If
    '                        End If
    '                    Case EnumExtType.DropDownField
    '                        Dim objfield As ComboBox = objPanel.FindControl("ANSWER_" & intQuestionID)
    '                        If Not objfield Is Nothing Then
    '                            If isRequired And objfield.SelectedItem.Value Is Nothing Then
    '                                Throw New ApplicationException(objfield.FieldLabel & " is required.")
    '                            End If

    '                            If objfield.SelectedItem.Value IsNot Nothing Then
    '                                strAnswer = objfield.SelectedItem.Value
    '                            End If
    '                        End If
    '                    Case EnumExtType.Radio
    '                        Dim objfield As RadioGroup = objPanel.FindControl("ANSWER_" & intQuestionID)
    '                        Dim objFieldYes As Radio = objPanel.FindControl("ANSWER_" & intQuestionID & "_Yes")
    '                        Dim objFieldNo As Radio = objPanel.FindControl("ANSWER_" & intQuestionID & "_No")

    '                        If objfield IsNot Nothing Then
    '                            If isRequired And Not (objFieldYes.Checked Or objFieldNo.Checked) Then
    '                                Throw New ApplicationException(objfield.FieldLabel & " is required.")
    '                            End If

    '                            If objFieldYes.Checked Then
    '                                strAnswer = "Yes"
    '                            ElseIf objFieldNo.Checked Then
    '                                strAnswer = "No"
    '                            End If
    '                        End If
    '                End Select

    '                'Cek apakah pertanyaan baru atau sudah pernah ada
    '                Dim objEdit As CasemanagementDAL.OneFCC_EDD_Detail = objOneFCC_EDD_CLASS.objListEDD_Detail.Where(Function(x) x.FK_OneFCC_Question_ID = intQuestionID).FirstOrDefault
    '                If objEdit IsNot Nothing Then
    '                    With objEdit
    '                        .Answer = strAnswer
    '                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                        .LastUpdateDate = Now
    '                    End With
    '                Else
    '                    Dim objNew As New CasemanagementDAL.OneFCC_EDD_Detail
    '                    With objNew
    '                        .PK_OneFCC_EDD_Detail_ID = intPK
    '                        .FK_OneFCC_EDD_ID = objOneFCC_EDD_CLASS.objEDD.PK_OneFCC_EDD_ID
    '                        .FK_OneFCC_Question_ID = intQuestionID
    '                        .Question = strQuestion
    '                        .Answer = strAnswer
    '                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                        .CreatedDate = Now
    '                    End With

    '                    objOneFCC_EDD_CLASS.objListEDD_Detail.Add(objNew)

    '                    intPK = intPK - 1
    '                End If
    '            Next
    '        Next

    '        'Save with or without approval
    '        If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
    '            OneFCC_EDD_BLL.SaveEditWithoutApproval(ObjModule, objOneFCC_EDD_CLASS)
    '            LblConfirmation.Text = "Data Saved into Database"
    '        Else
    '            OneFCC_EDD_BLL.SaveEditWithApproval(ObjModule, objOneFCC_EDD_CLASS)
    '            LblConfirmation.Text = "Data Saved into Pending Approval"
    '        End If

    '        Panelconfirmation.Hidden = False
    '        FormPanelInput.Hidden = True

    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub DownloadFileUploaded()
        Try
            Filetodownload = "1"
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Sub DownloadFileUploaded_Direct()
        Try
            If String.IsNullOrEmpty(Filetodownload) Then
                Exit Sub
            End If

            If objOneFCC_EDD_CLASS IsNot Nothing AndAlso objOneFCC_EDD_CLASS.objEDD IsNot Nothing AndAlso objOneFCC_EDD_CLASS.objEDD.FILE_ATTACHMENT IsNot Nothing AndAlso objOneFCC_EDD_CLASS.objEDD.FILE_ATTACHMENTName IsNot Nothing Then
                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & objOneFCC_EDD_CLASS.objEDD.FILE_ATTACHMENTName)
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(objOneFCC_EDD_CLASS.objEDD.FILE_ATTACHMENT)
                Response.End()
            Else
                Throw New ApplicationException("No Files to Download")
            End If

            Filetodownload = Nothing
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_EDD_Back_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadToFormPanel(objpanelinput As Ext.Net.FormPanel, customerType As String)
        Try
            Dim typeclassCustomer As CasemanagementDAL.OneFCC_Question_Customer_Type = OneFCC_EDD_BLL.GetEDDCustomerTypeByCode(customerType)
            If typeclassCustomer IsNot Nothing Then
                objpanelinput.Title = typeclassCustomer.Description & " Question"
                Load_Questionnaire(objpanelinput, typeclassCustomer.Customer_Type_Code)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ProgressScoring(mappingID As Long, enumType As EnumExtType, stringkeyvalue As String, customertype As String)
        Try
            Dim totalscore As Integer = 0
            'Dim dtrow = DataTabelEDDScore.AsEnumerable().Select(Function(x) New With {
            '    .Question_ID = x.Field(Of Long)("Question_ID"),
            '    .Score = x.Field(Of Integer)("Score"),
            '    .Type = x.Field(Of String)("Type")
            '                                                       }).Where(Function(x) x.Question_ID = mappingID).FirstOrDefault
            Dim strquery As String = "select top 1 * from OneFCC_EDD_Mapping_Question_Answer_Point where FK_OneFCC_Question_Mapping_EDD_ID = " & mappingID
            Dim datarowpoint As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strquery)
            Dim scorepoint As Integer = 0
            If datarowpoint IsNot Nothing Then
                Select Case enumType
                    Case EnumExtType.TextField
                        If Not IsDBNull(datarowpoint("Other_Condition")) AndAlso Not IsDBNull(datarowpoint("Other_Score_Int")) Then
                            Dim strQueryCondition As String = " select top 1 FilterWhereFormat from AdvancedFilterWhereClause where FK_AdvanceFilter_ID = 1 "
                            strQueryCondition = strQueryCondition & " and FilterWhereClause = '" & datarowpoint("Other_Condition") & "'"
                            Dim dataRowConditionFormat As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQueryCondition)
                            If dataRowConditionFormat IsNot Nothing AndAlso Not IsDBNull(dataRowConditionFormat("FilterWhereFormat")) Then
                                Dim strQueryChecker As String = " select 'True' as Answer where " & dataRowConditionFormat("FilterWhereFormat")
                                If datarowpoint("Other_Condition") = "Empty" OrElse datarowpoint("Other_Condition") = "Not Empty" OrElse
                                    datarowpoint("Other_Condition") = "NULL" OrElse datarowpoint("Other_Condition") = "Not NULL" Then
                                    If stringkeyvalue IsNot Nothing Then
                                        If stringkeyvalue = InputNullPronoun Then
                                            strQueryChecker = String.Format(strQueryChecker, "NULL")
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "'" & stringkeyvalue & "'")
                                        End If
                                    Else
                                        strQueryChecker = String.Format(strQueryChecker, "NULL")
                                    End If
                                    'ElseIf dataRowPoint("Other_Condition") = "in" OrElse dataRowPoint("Other_Condition") = "Not In" Then
                                    '    If Not IsDBNull(dataRowPoint("Other_Value")) Then
                                    '        If stringKeyValue IsNot Nothing Then
                                    '            If stringKeyValue = InputNullPronoun Then
                                    '                strQueryChecker = String.Format(strQueryChecker, "NULL", dataRowPoint("Other_Value"))
                                    '            Else
                                    '                strQueryChecker = String.Format(strQueryChecker, "'" & stringKeyValue & "'", dataRowPoint("Other_Value"))
                                    '            End If
                                    '        Else
                                    '            strQueryChecker = String.Format(strQueryChecker, "NULL", dataRowPoint("Other_Value"))
                                    '        End If
                                    '    End If
                                Else
                                    If Not IsDBNull(datarowpoint("Other_Value")) Then
                                        If stringkeyvalue IsNot Nothing Then
                                            If stringkeyvalue = InputNullPronoun Then
                                                strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"))
                                            Else
                                                strQueryChecker = String.Format(strQueryChecker, "'" & stringkeyvalue & "'", datarowpoint("Other_Value"))
                                            End If
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"))
                                        End If
                                    End If
                                End If
                                Dim dataRowValidateAnswer As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQueryChecker)
                                If dataRowValidateAnswer IsNot Nothing Then
                                    scorepoint = datarowpoint("Other_Score_Int")
                                End If
                            End If
                        End If
                        'If Not IsDBNull(dataRowPoint("Other_Condition")) AndAlso Not IsDBNull(dataRowPoint("Other_Value")) AndAlso
                        '    Not IsDBNull(dataRowPoint("Other_Score_Int")) AndAlso stringKeyValue IsNot Nothing Then
                        '    strQuery = ""
                        '    If dataRowPoint("Other_Condition").ToString() = "Contain" Then
                        '        If stringKeyValue.Contains(dataRowPoint("Other_Value").ToString()) Then
                        '            scorepoint = dataRowPoint("Other_Score_Int")
                        '        End If
                        '    End If
                        'End If

                    Case EnumExtType.NumberField
                        If Not IsDBNull(datarowpoint("Other_Condition")) AndAlso Not IsDBNull(datarowpoint("Other_Score_Int")) Then
                            Dim strQueryCondition As String = " select top 1 FilterWhereFormat from AdvancedFilterWhereClause where FK_AdvanceFilter_ID = 2 "
                            strQueryCondition = strQueryCondition & " and FilterWhereClause = '" & datarowpoint("Other_Condition") & "'"
                            Dim dataRowConditionFormat As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQueryCondition)
                            If dataRowConditionFormat IsNot Nothing AndAlso Not IsDBNull(dataRowConditionFormat("FilterWhereFormat")) Then
                                Dim strQueryChecker As String = " select 'True' as Answer where " & dataRowConditionFormat("FilterWhereFormat")
                                If datarowpoint("Other_Condition") = "NULL" OrElse datarowpoint("Other_Condition") = "Not NULL" Then
                                    If Not String.IsNullOrWhiteSpace(stringkeyvalue) Then
                                        If stringkeyvalue = InputNullPronoun Then
                                            strQueryChecker = String.Format(strQueryChecker, "NULL")
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, stringkeyvalue)
                                        End If
                                    Else
                                        strQueryChecker = String.Format(strQueryChecker, "NULL")
                                    End If
                                ElseIf datarowpoint("Other_Condition") = "Between" OrElse datarowpoint("Other_Condition") = "Not Between" Then
                                    If Not IsDBNull(datarowpoint("Other_Value")) AndAlso Not IsDBNull(datarowpoint("Other_Value_Helper")) Then
                                        If Not String.IsNullOrWhiteSpace(stringkeyvalue) Then
                                            If stringkeyvalue = InputNullPronoun Then
                                                strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"), datarowpoint("Other_Value_Helper"))
                                            Else
                                                strQueryChecker = String.Format(strQueryChecker, stringkeyvalue, datarowpoint("Other_Value"), datarowpoint("Other_Value_Helper"))
                                            End If
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"), datarowpoint("Other_Value_Helper"))
                                        End If
                                    End If
                                Else
                                    If Not IsDBNull(datarowpoint("Other_Value")) Then
                                        If Not String.IsNullOrWhiteSpace(stringkeyvalue) Then
                                            If stringkeyvalue = InputNullPronoun Then
                                                strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"))
                                            Else
                                                strQueryChecker = String.Format(strQueryChecker, stringkeyvalue, datarowpoint("Other_Value"))
                                            End If
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"))
                                        End If
                                    End If
                                End If
                                Dim dataRowValidateAnswer As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQueryChecker)
                                If dataRowValidateAnswer IsNot Nothing Then
                                    scorepoint = datarowpoint("Other_Score_Int")
                                End If
                            End If
                        End If
                        'If Not IsDBNull(dataRowPoint("Other_Condition")) AndAlso Not IsDBNull(dataRowPoint("Other_Value")) AndAlso
                        '    Not IsDBNull(dataRowPoint("Other_Score_Int")) AndAlso stringKeyValue IsNot Nothing Then
                        '    If dataRowPoint("Other_Condition").ToString() = "Equal" Then
                        '        If CInt(dataRowPoint("Other_Value")) = CInt(stringKeyValue) Then
                        '            scorepoint = dataRowPoint("Other_Score_Int")
                        '        End If
                        '    End If
                        'End If

                    Case EnumExtType.DateField
                        If Not IsDBNull(datarowpoint("Other_Condition")) AndAlso Not IsDBNull(datarowpoint("Other_Score_Int")) Then
                            Dim strQueryCondition As String = " select top 1 FilterWhereFormat from AdvancedFilterWhereClause where FK_AdvanceFilter_ID = 3 "
                            strQueryCondition = strQueryCondition & " and FilterWhereClause = '" & datarowpoint("Other_Condition") & "'"
                            Dim dataRowConditionFormat As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQueryCondition)
                            If dataRowConditionFormat IsNot Nothing AndAlso Not IsDBNull(dataRowConditionFormat("FilterWhereFormat")) Then
                                Dim strQueryChecker As String = " select 'True' as Answer where " & dataRowConditionFormat("FilterWhereFormat")
                                If datarowpoint("Other_Condition") = "NULL" OrElse datarowpoint("Other_Condition") = "Not NULL" Then
                                    If stringkeyvalue IsNot Nothing Then
                                        If stringkeyvalue = InputNullPronoun Then
                                            strQueryChecker = String.Format(strQueryChecker, "NULL")
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "'" & stringkeyvalue & "'")
                                        End If
                                    Else
                                        strQueryChecker = String.Format(strQueryChecker, "NULL")
                                    End If
                                ElseIf datarowpoint("Other_Condition") = "Between" OrElse datarowpoint("Other_Condition") = "Not Between" Then
                                    If Not IsDBNull(datarowpoint("Other_Value")) AndAlso Not IsDBNull(datarowpoint("Other_Value_Helper")) Then
                                        If stringkeyvalue IsNot Nothing Then
                                            If stringkeyvalue = InputNullPronoun Then
                                                strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"), datarowpoint("Other_Value_Helper"))
                                            Else
                                                strQueryChecker = String.Format(strQueryChecker, "'" & stringkeyvalue & "'", datarowpoint("Other_Value"), datarowpoint("Other_Value_Helper"))
                                            End If
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"), datarowpoint("Other_Value_Helper"))
                                        End If
                                        'If stringKeyValue IsNot Nothing Then
                                        '    If stringKeyValue = InputNullPronoun Then
                                        '        strQueryChecker = String.Format(strQueryChecker, "NULL", "CONVERT(DATE,'" & dataRowPoint("Other_Value") & "',111)", "CONVERT(DATE,'" & dataRowPoint("Other_Value_Helper") & "',111)")
                                        '    Else
                                        '        strQueryChecker = String.Format(strQueryChecker, "'" & stringKeyValue & "'", "CONVERT(DATE,'" & dataRowPoint("Other_Value") & "',111)", "CONVERT(DATE,'" & dataRowPoint("Other_Value_Helper") & "',111)")
                                        '    End If
                                        'Else
                                        '    strQueryChecker = String.Format(strQueryChecker, "NULL", "CONVERT(DATE,'" & dataRowPoint("Other_Value") & "',111)", "CONVERT(DATE,'" & dataRowPoint("Other_Value_Helper") & "',111)")
                                        'End If
                                    End If
                                Else
                                    If Not IsDBNull(datarowpoint("Other_Value")) Then
                                        If stringkeyvalue IsNot Nothing Then
                                            If stringkeyvalue = InputNullPronoun Then
                                                strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"))
                                            Else
                                                strQueryChecker = String.Format(strQueryChecker, "'" & stringkeyvalue & "'", datarowpoint("Other_Value"))
                                            End If
                                        Else
                                            strQueryChecker = String.Format(strQueryChecker, "NULL", datarowpoint("Other_Value"))
                                        End If
                                        'If stringKeyValue IsNot Nothing Then
                                        '    If stringKeyValue = InputNullPronoun Then
                                        '        strQueryChecker = String.Format(strQueryChecker, "NULL", "CONVERT(DATE,'" & dataRowPoint("Other_Value") & "',111)")
                                        '    Else
                                        '        strQueryChecker = String.Format(strQueryChecker, "'" & stringKeyValue & "'", "CONVERT(DATE,'" & dataRowPoint("Other_Value") & "',111)")
                                        '    End If
                                        'Else
                                        '    strQueryChecker = String.Format(strQueryChecker, "NULL", "CONVERT(DATE,'" & dataRowPoint("Other_Value") & "',111)")
                                        'End If
                                    End If
                                End If
                                Dim dataRowValidateAnswer As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQueryChecker)
                                If dataRowValidateAnswer IsNot Nothing Then
                                    scorepoint = datarowpoint("Other_Score_Int")
                                End If
                            End If
                        End If
                        'If Not IsDBNull(dataRowPoint("Other_Condition")) AndAlso Not IsDBNull(dataRowPoint("Other_Value")) AndAlso
                        '    Not IsDBNull(dataRowPoint("Other_Score_Int")) AndAlso stringKeyValue IsNot Nothing Then
                        '    Dim datevalue As Date = CDate(stringKeyValue)
                        '    Dim datecondition As Date = CDate(dataRowPoint("Other_Value"))
                        '    If dataRowPoint("Other_Condition").ToString() = "Equal" Then
                        '        If DateDiff(DateInterval.Day, datevalue, datecondition) = 0 Then
                        '            scorepoint = dataRowPoint("Other_Score_Int")
                        '        End If
                        '    End If
                        'End If

                    Case EnumExtType.DropDownField
                        stringkeyvalue = stringkeyvalue.Replace("'", "''")
                        If Not IsDBNull(datarowpoint("Table_Reference_Name")) AndAlso Not IsDBNull(datarowpoint("Table_Reference_Field_Key")) AndAlso Not IsDBNull(datarowpoint("Table_Reference_Field_Score")) AndAlso stringkeyvalue IsNot Nothing Then
                            Dim strinnerquery As String = "select top 1 " & datarowpoint("Table_Reference_Field_Score") & " from " & datarowpoint("Table_Reference_Name") &
                                " where " & datarowpoint("Table_Reference_Field_Key") & " = '" & stringkeyvalue & "'"
                            Dim datarowdropdown As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strinnerquery)
                            If datarowdropdown IsNot Nothing AndAlso Not IsDBNull(datarowdropdown(datarowpoint("Table_Reference_Field_Score"))) Then
                                scorepoint = datarowdropdown(datarowpoint("Table_Reference_Field_Score"))
                            End If
                        End If

                    Case EnumExtType.Radio
                        If stringkeyvalue = "Yes" Then
                            If Not IsDBNull(datarowpoint("Radio_Yes_Score_Int")) Then
                                scorepoint = datarowpoint("Radio_Yes_Score_Int")
                            End If
                        ElseIf stringkeyvalue = "No" Then
                            If Not IsDBNull(datarowpoint("Radio_No_Score_Int")) Then
                                scorepoint = datarowpoint("Radio_No_Score_Int")
                            End If
                        End If

                End Select
                Dim strselect As String = "Question_ID = " & mappingID
                Dim dtrow As DataRow = DataTabelEDDScore.Select(strselect).FirstOrDefault()
                If dtrow Is Nothing Then
                    Dim objdatarow As DataRow = DataTabelEDDScore.NewRow()
                    objdatarow("Question_ID") = mappingID
                    objdatarow("Type") = customertype
                    objdatarow("Score") = scorepoint
                    DataTabelEDDScore.Rows.Add(objdatarow)
                Else
                    Dim indexrow As Integer = DataTabelEDDScore.Rows.IndexOf(dtrow)
                    DataTabelEDDScore.Rows(indexrow)("Score") = scorepoint
                End If
                totalscore = Convert.ToInt32(DataTabelEDDScore.Compute("SUM(Score)", "Type = '" & customertype & "'"))
                UpdateRiskScoreUI(customertype, totalscore)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub UpdateRiskScoreUI(customertype As String, totalscore As Integer)
        Try
            Dim objPanel As New Ext.Net.Panel
            Dim objlabelRisk As New Ext.Net.Label
            Dim objlabelScore As New Ext.Net.Label
            If customertype = "Customer" Then
                objPanel = FormPanelCustomer.FindControl(FormPanelCustomer.ID & "_Panel_Risk")
                objlabelRisk = objPanel.FindControl(objPanel.ID & "_Customer_RiskRating")
                objlabelScore = objPanel.FindControl(objPanel.ID & "_Customer_Score")
            ElseIf customertype = "BO" Then
                objPanel = FormPanelBenificialOwner.FindControl(FormPanelBenificialOwner.ID & "_Panel_Risk")
                objlabelRisk = objPanel.FindControl(objPanel.ID & "_BO_RiskRating")
                objlabelScore = objPanel.FindControl(objPanel.ID & "_BO_Score")
            End If
            objlabelScore.Text = totalscore.ToString()

            Dim strquery As String = "SELECT * FROM AML_RISK_RATING a WHERE " & totalscore & " BETWEEN a.RISK_RATING_SCORE_FROM AND a.RISK_RATING_SCORE_TO"
            Dim datarowpoint As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strquery)
            If datarowpoint IsNot Nothing AndAlso Not IsDBNull(datarowpoint("RISK_RATING_NAME")) Then
                objlabelRisk.Text = datarowpoint("RISK_RATING_NAME")
                If datarowpoint("RISK_RATING_NAME") = "LOW" Then
                    objPanel.BodyStyle = "background-color: #33ff3a;"
                ElseIf datarowpoint("RISK_RATING_NAME") = "MEDIUM" Then
                    objPanel.BodyStyle = "background-color: #ffee33;"
                ElseIf datarowpoint("RISK_RATING_NAME") = "HIGH" Then
                    objPanel.BodyStyle = "background-color: #ff3333;"
                ElseIf datarowpoint("RISK_RATING_NAME") = "PEPS" Then
                    objPanel.BodyStyle = "background-color: #ff0000;"
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '------- LOAD COMPONENT
    Protected Function ExtPanel(pn As FormPanel, strName As String, strTitle As String, isRiskRating As Boolean) As Ext.Net.Panel
        Try
            Dim objPanel As New Ext.Net.Panel
            With objPanel
                .ID = strName
                .ClientIDMode = Web.UI.ClientIDMode.Static
                .Title = strTitle
                .MarginSpec = "0 0 10 0"
                If isRiskRating Then
                    .BodyStyle = "background-color: #33ff3a;"
                    .Border = False
                    .Layout = "ColumnLayout"
                Else
                    .Collapsible = True
                    .Border = True
                    .BodyPadding = 10
                End If
            End With

            pn.Add(objPanel)
            Return objPanel

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ExtDate(pn As Ext.Net.Panel, strLabel As String, strFieldName As String, bRequired As Boolean, intMaxSize As Integer, intgridpos As Integer) As Ext.Net.DateField
        Dim objDateField As New Ext.Net.DateField
        objDateField.ID = strFieldName
        objDateField.ClientIDMode = Web.UI.ClientIDMode.Static

        objDateField.FieldLabel = strLabel
        objDateField.LabelStyle = "word-wrap: break-word"
        'objDateField.LabelWidth = 150 'intLabelWidth
        objDateField.Name = strFieldName
        objDateField.ID = strFieldName
        objDateField.AllowBlank = Not bRequired
        objDateField.BlankText = strLabel & " is required."
        'objDateField.MaxLength = intMaxSize
        objDateField.Width = objDateField.LabelWidth + 150
        objDateField.Format = NawaBLL.SystemParameterBLL.GetDateFormat
        objDateField.AnchorHorizontal = "40%"
        objDateField.LabelWidth = 500
        objDateField.MinWidth = 650

        objDateField.ReadOnly = True

        pn.Add(objDateField)
        Return objDateField
    End Function

    Protected Function ExtText(pn As Ext.Net.Panel, strLabel As String, strFieldName As String, bRequired As Boolean, intMaxSize As Integer, intgridpos As Integer) As Ext.Net.TextField
        Dim objTextField As New Ext.Net.TextField
        objTextField.ID = strFieldName
        objTextField.ClientIDMode = Web.UI.ClientIDMode.Static
        objTextField.FieldLabel = strLabel

        objTextField.ID = strFieldName
        objTextField.Name = strFieldName
        objTextField.AllowBlank = Not bRequired
        objTextField.BlankText = strLabel & " is required."
        objTextField.MaxLength = intMaxSize
        objTextField.LabelWidth = 500
        objTextField.MinWidth = 900

        objTextField.AnchorHorizontal = "80%"
        objTextField.ReadOnly = True

        pn.Add(objTextField)
        Return objTextField
    End Function

    Protected Function ExtRadio(pn As Ext.Net.Panel, strLabel As String, strFieldName As String, bRequired As Boolean, intMaxSize As Integer, intgridpos As Integer) As Ext.Net.RadioGroup
        Dim objRadioGroup As New Ext.Net.RadioGroup
        Dim objRadioYes As New Ext.Net.Radio
        Dim objRadioNo As New Ext.Net.Radio

        With objRadioGroup
            .FieldLabel = strLabel
            .ID = strFieldName
            .AllowBlank = Not bRequired
            .BlankText = strLabel & " is required."
            .LabelWidth = 500
            .Width = .LabelWidth + 150
        End With

        With objRadioYes
            .ID = strFieldName & "_Yes"
            .BoxLabel = "Yes"
            .Value = "Yes"
            .Checked = False
        End With

        With objRadioNo
            .ID = strFieldName & "_No"
            .BoxLabel = "No"
            .Value = "No"
            .Checked = False
        End With

        objRadioGroup.Add(objRadioYes)
        objRadioGroup.Add(objRadioNo)

        objRadioYes.ReadOnly = True
        objRadioNo.ReadOnly = True

        pn.Add(objRadioGroup)
        Return objRadioGroup
    End Function

    Public Function ExtNumber(pn As Ext.Net.Panel, strLabel As String, strFieldName As String, bRequired As Boolean, intDecimalPrecition As Integer, intgridpos As Integer, dminvalue As Double, dmaxvalue As Double) As Ext.Net.NumberField
        Dim objNumberField As New Ext.Net.NumberField
        objNumberField.ID = strFieldName
        objNumberField.ClientIDMode = Web.UI.ClientIDMode.Static

        objNumberField.FieldLabel = strLabel
        objNumberField.LabelStyle = "word-wrap: break-word"
        objNumberField.LabelWidth = 150 'intLabelWidth
        'objNumberField.ID = strFieldName
        objNumberField.Name = strFieldName
        objNumberField.AllowBlank = Not bRequired
        objNumberField.BlankText = strLabel & " is required."
        objNumberField.DecimalPrecision = intDecimalPrecition
        objNumberField.MinValue = dminvalue
        objNumberField.MaxValue = dmaxvalue
        objNumberField.Width = objNumberField.LabelWidth + 150
        objNumberField.AnchorHorizontal = "40%"
        objNumberField.LabelWidth = 500
        objNumberField.MinWidth = 650

        objNumberField.ReadOnly = True

        pn.Add(objNumberField)
        Return objNumberField
    End Function

    Public Function ExtCombo(pn As Ext.Net.Panel, strLabel As String, strFieldName As String, bRequired As Boolean, intgridpos As Integer, strTableRef As String, strFieldKey As String, strFieldDisplay As String, strFilterField As String, strTableRefAlias As String) As Ext.Net.ComboBox
        Using objcombo As New Ext.Net.ComboBox
            objcombo.ID = strFieldName
            objcombo.ClientIDMode = Web.UI.ClientIDMode.Static

            objcombo.FieldLabel = strLabel

            objcombo.LabelWidth = 500 'intLabelWidth
            objcombo.AnchorHorizontal = "80%"
            objcombo.ID = strFieldName
            objcombo.Name = strFieldName
            objcombo.AllowBlank = Not bRequired
            objcombo.BlankText = strLabel & " is required."
            objcombo.Width = objcombo.LabelWidth + 400
            objcombo.MatchFieldWidth = True
            objcombo.MinChars = "0"
            objcombo.ForceSelection = True

            'objcombo.TypeAhead = True
            'objcombo.EnableRegEx = True
            objcombo.AnyMatch = True

            objcombo.QueryMode = DataLoadMode.Local
            objcombo.ValueField = strFieldKey
            objcombo.DisplayField = strFieldDisplay
            objcombo.TriggerAction = Ext.Net.TriggerAction.All

            Dim objFieldtrigger As New Ext.Net.FieldTrigger
            objFieldtrigger.Icon = Ext.Net.TriggerIcon.Clear
            objFieldtrigger.Hidden = True
            objFieldtrigger.Weight = "-1"
            objcombo.Triggers.Add(objFieldtrigger)

            objcombo.Listeners.Select.Handler = "this.getTrigger(0).show();"

            objcombo.Listeners.TriggerClick.Handler = "if (index == 0) {  this.clearValue(); this.getTrigger(0).hide();}"

            'buat store dan modelnya

            Using objStore As New Ext.Net.Store
                objStore.ID = "_Store_" + objcombo.ID
                objStore.ClientIDMode = Web.UI.ClientIDMode.Static

                Using objModel As New Ext.Net.Model
                    objModel.Fields.Add(strFieldKey, Ext.Net.ModelFieldType.String)
                    objModel.Fields.Add(strFieldDisplay, Ext.Net.ModelFieldType.String)
                    objStore.Model.Add(objModel)
                End Using

                objcombo.Store.Add(objStore)
                objStore.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, System.Data.CommandType.Text, GetQueryRef(strTableRef & " " & strTableRefAlias, strFieldKey, strFieldDisplay, strFilterField), Nothing)
                objStore.DataBind()
            End Using

            objcombo.ReadOnly = True

            pn.Add(objcombo)
            Return objcombo
        End Using
    End Function

    Function GetQueryRef(strTable As String, strfieldkey As String, strfielddisplay As String, strfilter As String) As String
        Dim strquery As String
        strquery = "select " & strfieldkey & ", convert(Varchar(1000),[" & strfieldkey & "])+ ' - '+ convert(varchar(1000), [" & strfielddisplay & "]) as [" & strfielddisplay & "] from " & strTable

        strfilter = strfilter.Replace("@userid", NawaBLL.Common.SessionCurrentUser.UserID)
        strfilter = strfilter.Replace("@PK_MUser_ID", NawaBLL.Common.SessionCurrentUser.PK_MUser_ID)

        If strfilter.Trim.Length > 0 Then
            strquery = strquery & " where " & strfilter
        End If
        Return strquery
    End Function

    Protected Function ExtLabel(pn As Ext.Net.Panel, strID As String, strLabel As String, columnwidth As Double) As Ext.Net.Label
        Dim objLabel As New Ext.Net.Label
        objLabel.ID = strID
        objLabel.ClientIDMode = Web.UI.ClientIDMode.Static
        objLabel.ColumnWidth = columnwidth
        objLabel.Text = strLabel
        objLabel.Margin = 10

        pn.Add(objLabel)
        Return objLabel
    End Function
    '------- END OF LOAD COMPONENT

    Protected Sub Load_Answer()
        Try
            If Ext.Net.X.IsAjaxRequest Then
                Exit Sub
            End If

            'objOneFCC_EDD_CLASS = OneFCC_EDD_BLL.GetEDDClassByID(IDUnik)
            If objOneFCC_EDD_CLASS IsNot Nothing Then

                Dim Customer_Type_ID As String = objOneFCC_EDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Type_Code
                Dim Customer_Segment_ID As String = objOneFCC_EDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Segment_Code
                Dim EDD_Type_ID As String = objOneFCC_EDD_CLASS.objEDD.FK_OneFCC_EDD_Type_Code

                'Load Header
                With objOneFCC_EDD_CLASS.objEDD
                    Dim segementclass As CasemanagementDAL.OneFCC_Question_Customer_Segment = OneFCC_EDD_BLL.GetEDDCustomerSegmentByCode(.FK_OneFCC_Question_Customer_Segment_Code)
                    Dim typeclass As CasemanagementDAL.OneFCC_Question_Customer_Type = OneFCC_EDD_BLL.GetEDDCustomerTypeByCode(.FK_OneFCC_Question_Customer_Type_Code)
                    Dim objEDDType As CasemanagementDAL.OneFCC_EDD_Type = OneFCC_EDD_BLL.GetEDDTypeByCode(.FK_OneFCC_EDD_Type_Code)

                    If segementclass IsNot Nothing AndAlso typeclass IsNot Nothing AndAlso objEDDType IsNot Nothing Then
                        cmb_Customer_Type_ID.SetTextWithTextValue(typeclass.Customer_Type_Code, typeclass.Description)
                        cmb_Customer_Segment_ID.SetTextWithTextValue(segementclass.Customer_Segment_Code, segementclass.Description)
                        cmb_EDD_TYPE_ID.SetTextWithTextValue(objEDDType.EDD_Type_Code, objEDDType.EDD_Type_Name)
                        If Not String.IsNullOrEmpty(cmb_EDD_TYPE_ID.StringValue) Then
                            If cmb_EDD_TYPE_ID.StringValue = "C" Then
                                'nDSDropDownField_CIFNo.IsHidden = False
                                'btn_Import_Customer.Hidden = False
                                txt_CIFNo.Hidden = False
                                txt_Nama.Hidden = False
                                txt_Nama.ReadOnly = True
                                txt_Nama.FieldStyle = "background-color: #ddd"
                                btn_OpenWindowCustomer.Hidden = False

                                txt_CIFNo.Value = .CIFNo
                            ElseIf cmb_EDD_TYPE_ID.StringValue = "CP" Then
                                txt_Nama.Hidden = False
                                customer_NIK.Hidden = False
                                customer_Nationality.IsHidden = False
                                customer_DOB.Hidden = False
                                customer_POB.Hidden = False
                                customer_DOB.Format = NawaBLL.SystemParameterBLL.GetDateFormat()

                                Dim strQuery As String = "SELECT TOP 1 * FROM OneFCC_EDD_Additional_Info_Customer_Prospect WHERE UNIQUE_KEY = '" & IDUnik.ToString() & "'"
                                Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                If drResult IsNot Nothing Then
                                    If Not IsDBNull(drResult("NIK")) Then
                                        customer_NIK.Value = drResult("NIK")
                                    End If
                                    If Not IsDBNull(drResult("DATEOFBIRTH")) Then
                                        customer_DOB.Value = drResult("DATEOFBIRTH")
                                    End If
                                    If Not IsDBNull(drResult("PLACEOFBIRTH")) Then
                                        customer_POB.Value = drResult("PLACEOFBIRTH")
                                    End If
                                    If Not IsDBNull(drResult("Nationality")) Then
                                        Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_COUNTRY WHERE FK_AML_COUNTRY_Code = '" & drResult("Nationality") & "'"
                                        Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                                        If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("FK_AML_COUNTRY_Code")) AndAlso Not IsDBNull(tempDrResult("AML_COUNTRY_Name")) Then
                                            customer_Nationality.SetTextWithTextValue(tempDrResult("FK_AML_COUNTRY_Code"), tempDrResult("AML_COUNTRY_Name"))
                                        End If
                                    End If
                                End If
                            End If
                        End If

                        '' Add 27-Jan-2023 Display Risk Rating Score
                        Dim RiskRatingScore As String = ""

                        Dim strRiskRatingScore As String = "select isnull(cast ( Risk_Rating_Total_Score as varchar(50)),'') + ' ( ' + b.RISK_RATING_NAME + ' ) ' as RiskRatingScore"
                        strRiskRatingScore += " FROM onefCC_EDD a "
                        strRiskRatingScore += " JOIN AML_RISK_RATING As b  "
                        strRiskRatingScore += " On a.FK_RISK_RATING_CODE = b.RISK_RATING_CODE "
                        strRiskRatingScore += " where PK_OneFCC_EDD_ID = '" & IDUnik & "'"

                        RiskRatingScore = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strRiskRatingScore, Nothing)
                        txt_RiskRatingScore.Value = RiskRatingScore


                        Dim paramRiskRatingDetail(0) As System.Data.SqlClient.SqlParameter
                        paramRiskRatingDetail(0) = New System.Data.SqlClient.SqlParameter
                        paramRiskRatingDetail(0).ParameterName = "@PK_EDD_ID"
                        paramRiskRatingDetail(0).Value = IDUnik
                        paramRiskRatingDetail(0).DbType = SqlDbType.VarChar

                        Dim dtRiskRatingDetail As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_OneFCC_CaseManagement_GetRiskRatingDetailbyPKEDD", paramRiskRatingDetail)

                        If dtRiskRatingDetail Is Nothing Then
                            dtRiskRatingDetail = New DataTable
                        End If

                        If dtRiskRatingDetail IsNot Nothing Then

                            'Bind to gridpanel
                            gp_RiskRatingDetail.GetStore.DataSource = dtRiskRatingDetail
                            gp_RiskRatingDetail.GetStore.DataBind()

                        End If
                        '' End 27-Jan-2023
                    Else
                        'cmb_EDD_TYPE_ID.IsReadOnly = True
                        'cmb_Customer_Type_ID.IsReadOnly = True
                        'cmb_Customer_Segment_ID.IsReadOnly = True
                        'btn_EDD_Submit.Hidden = True
                        Throw New ApplicationException("Unable to find EDD Type Or Customer Type Or Customer Segment, Please contact admin to check this issue.")
                    End If
                    'If objEDDType IsNot Nothing Then
                    '    cmb_EDD_TYPE_ID.SetTextWithTextValue(objEDDType.EDD_Type_Code, objEDDType.EDD_Type_Name)
                    'End If
                    'If typeclass IsNot Nothing Then
                    '    cmb_Customer_Type_ID.SetTextWithTextValue(typeclass.Customer_Type_Code, typeclass.Description)
                    'End If
                    'If segementclass IsNot Nothing Then
                    '    cmb_Customer_Segment_ID.SetTextWithTextValue(segementclass.Customer_Segment_Code, segementclass.Description)
                    'End If
                    'If .FK_OneFCC_EDD_Type_Code IsNot Nothing Then
                    '    Dim objEDDType = OneFCC_EDD_BLL.GetEDDTypeByCode(.FK_OneFCC_EDD_Type_Code)
                    '    If objEDDType IsNot Nothing Then
                    '        cmb_EDD_TYPE_ID.SetTextWithTextValue(objEDDType.EDD_Type_Code, objEDDType.EDD_Type_Name)
                    '    End If
                    'End If
                    'txt_CIFNo.Value = .CIFNo
                    'If Not String.IsNullOrEmpty(cmb_EDD_TYPE_ID.StringValue) Then
                    '    If cmb_EDD_TYPE_ID.StringValue = "C" Then
                    '        'nDSDropDownField_CIFNo.IsHidden = False
                    '        'btn_Import_Customer.Hidden = False
                    '        txt_CIFNo.Hidden = False
                    '        txt_Nama.Hidden = False
                    '        txt_Nama.ReadOnly = True
                    '        txt_Nama.FieldStyle = "background-color: #ddd"
                    '        btn_OpenWindowCustomer.Hidden = False

                    '        txt_CIFNo.Value = .CIFNo
                    '    ElseIf cmb_EDD_TYPE_ID.StringValue = "CP" Then
                    '        txt_Nama.Hidden = False
                    '        customer_NIK.Hidden = False
                    '        customer_Nationality.IsHidden = False
                    '        customer_DOB.Hidden = False
                    '        customer_POB.Hidden = False
                    '        customer_DOB.Format = NawaBLL.SystemParameterBLL.GetDateFormat()


                    '    End If
                    'End If
                    txt_Nama.Value = .Nama
                    If .FILE_ATTACHMENTName IsNot Nothing Then
                        File_Attachment.Value = .FILE_ATTACHMENTName
                        BtnDownloadFile.Hidden = False
                    Else
                        File_Attachment.Value = "No File Attached"
                    End If

                    'Load Answer
                    Load_AnswerCustomerType(FormPanelCustomer, "Customer")
                    If .IsHaveBeneficialOwner Then
                        checkbox_beneficialowner.Checked = .IsHaveBeneficialOwner
                        Load_AnswerCustomerType(FormPanelBenificialOwner, "BO")
                    End If
                End With
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Load_AnswerCustomerType(objpanelinput As Ext.Net.FormPanel, customerType As String)
        Dim Customer_Type_ID As String = customerType
        Dim Customer_Segment_ID As String = objOneFCC_EDD_CLASS.objEDD.FK_OneFCC_Question_Customer_Segment_Code
        Dim EDD_Type_ID As String = objOneFCC_EDD_CLASS.objEDD.FK_OneFCC_EDD_Type_Code

        'Load Answer
        Dim strQuery As String
        strQuery = " select detail.FK_OneFCC_Question_ID, detail.FK_OneFCC_Question_Parent_ID, detail.Answer, question.FK_ExtType_ID "
        strQuery = strQuery & " from OneFCC_EDD_Detail detail"
        strQuery = strQuery & " join OneFCC_Question_Mapping_EDD mapping"
        strQuery = strQuery & " on mapping.PK_OneFCC_Question_Mapping_EDD_ID = detail.FK_OneFCC_Question_ID"
        strQuery = strQuery & " join OneFCC_Question question"
        strQuery = strQuery & " on mapping.FK_OneFCC_Question_ID = question.PK_OneFCC_Question_ID"
        strQuery = strQuery & " and detail.Question = question.QUESTION"
        strQuery = strQuery & " where detail.FK_OneFCC_EDD_ID = " & objOneFCC_EDD_CLASS.objEDD.PK_OneFCC_EDD_ID.ToString()
        strQuery = strQuery & " and detail.FK_OneFCC_Question_Parent_ID is null"
        strQuery = strQuery & " and mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
        strQuery = strQuery & " and mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
        strQuery = strQuery & " and mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
        strQuery = strQuery & " and detail.Answer is not null"


        Dim dtQuestion As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
        For Each item In dtQuestion.Rows
            Dim intQuestionID As Integer = item("FK_OneFCC_Question_ID")

            Dim strAnswer As String = ""
            If Not IsDBNull(item("Answer")) Then
                strAnswer = item("Answer")
            End If
            Select Case CType(item("FK_ExtType_ID"), EnumExtType)
                Case EnumExtType.TextField
                    Dim objfield As TextField = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                    If Not objfield Is Nothing Then
                        objfield.Value = strAnswer
                    End If
                Case EnumExtType.NumberField
                    Dim objfield As NumberField = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                    If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                        objfield.Value = CLng(strAnswer)
                    End If
                Case EnumExtType.DateField
                    Dim objfield As DateField = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                    If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                        objfield.Value = CDate(strAnswer)
                    End If
                Case EnumExtType.DropDownField
                    Dim objfield As ComboBox = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                    If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                        objfield.SetValueAndFireSelect(strAnswer)
                    End If
                Case EnumExtType.Radio
                    Dim objfield As RadioGroup = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                    Dim objFieldYes As Radio = objpanelinput.FindControl("ANSWER_" & intQuestionID & "_Yes")
                    Dim objFieldNo As Radio = objpanelinput.FindControl("ANSWER_" & intQuestionID & "_No")
                    If objfield IsNot Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                        If strAnswer = "Yes" Then
                            objFieldYes.Checked = True
                            objFieldNo.Checked = False
                        ElseIf strAnswer = "No" Then
                            objFieldYes.Checked = False
                            objFieldNo.Checked = True
                        End If
                    End If
            End Select

            If IsUsingScorePointAffectingAMLCustomer = "1" Then
                ProgressScoring(item("FK_OneFCC_Question_ID"), CType(item("FK_ExtType_ID"), EnumExtType), strAnswer, customerType)
            End If

            strQuery = " select detail.FK_OneFCC_Question_ID, detail.FK_OneFCC_Question_Parent_ID, detail.Answer, question.FK_ExtType_ID "
            strQuery = strQuery & " from OneFCC_EDD_Detail detail"
            strQuery = strQuery & " join OneFCC_Question_Mapping_EDD mapping"
            strQuery = strQuery & " on mapping.PK_OneFCC_Question_Mapping_EDD_ID = detail.FK_OneFCC_Question_ID"
            strQuery = strQuery & " join OneFCC_Question question"
            strQuery = strQuery & " on mapping.FK_OneFCC_Question_ID = question.PK_OneFCC_Question_ID"
            strQuery = strQuery & " and detail.Question = question.QUESTION"
            strQuery = strQuery & " where detail.FK_OneFCC_EDD_ID = " & objOneFCC_EDD_CLASS.objEDD.PK_OneFCC_EDD_ID.ToString()
            strQuery = strQuery & " and detail.FK_OneFCC_Question_Parent_ID = " & intQuestionID.ToString()
            strQuery = strQuery & " and mapping.FK_OneFCC_Question_Customer_Segment_Code = '" & Customer_Segment_ID & "'"
            strQuery = strQuery & " and mapping.FK_OneFCC_Question_Customer_Type_Code = '" & Customer_Type_ID & "'"
            strQuery = strQuery & " and mapping.FK_OneFCC_Question_Type_Code = '" & EDD_Type_ID & "'"
            strQuery = strQuery & " and detail.Answer is not null"

            Dim dtQuestionChild As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, strQuery, Nothing)
            For Each child In dtQuestionChild.Rows
                Dim intQuestionParentID As Integer = intQuestionID
                intQuestionID = child("FK_OneFCC_Question_ID")

                strAnswer = ""
                If Not IsDBNull(child("Answer")) Then
                    strAnswer = child("Answer")
                End If

                Select Case CType(child("FK_ExtType_ID"), EnumExtType)
                    Case EnumExtType.TextField
                        Dim objfield As TextField = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                        If Not objfield Is Nothing Then
                            objfield.Value = strAnswer
                        End If
                    Case EnumExtType.NumberField
                        Dim objfield As NumberField = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                        If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                            objfield.Value = CLng(strAnswer)
                        End If
                    Case EnumExtType.DateField
                        Dim objfield As DateField = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                        If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                            objfield.Value = CDate(strAnswer)
                        End If
                    Case EnumExtType.DropDownField
                        Dim objfield As ComboBox = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                        If Not objfield Is Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                            objfield.SetValueAndFireSelect(strAnswer)
                        End If
                    Case EnumExtType.Radio
                        Dim objfield As RadioGroup = objpanelinput.FindControl("ANSWER_" & intQuestionID)
                        Dim objFieldYes As Radio = objpanelinput.FindControl("ANSWER_" & intQuestionID & "_Yes")
                        Dim objFieldNo As Radio = objpanelinput.FindControl("ANSWER_" & intQuestionID & "_No")
                        If objfield IsNot Nothing AndAlso Not String.IsNullOrEmpty(strAnswer) Then
                            If strAnswer = "Yes" Then
                                objFieldYes.Checked = True
                                objFieldNo.Checked = False
                            ElseIf strAnswer = "No" Then
                                objFieldYes.Checked = False
                                objFieldNo.Checked = True
                            End If
                        End If
                End Select

                If IsUsingScorePointAffectingAMLCustomer = "1" Then
                    ProgressScoring(child("FK_OneFCC_Question_ID"), CType(child("FK_ExtType_ID"), EnumExtType), strAnswer, customerType)
                End If
            Next
        Next
    End Sub

    'Protected Sub nDSDropDownField_CIFNo_OnValueChanged(sender As Object, e As EventArgs)
    '    Try
    '        If Not String.IsNullOrWhiteSpace(nDSDropDownField_CIFNo.StringValue) Then
    '            Dim strQuery As String = " select CUSTOMERNAME from AML_CUSTOMER where CIFNo = '" & nDSDropDownField_CIFNo.StringValue & "' "
    '            Dim dtRow As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
    '            If dtRow IsNot Nothing AndAlso Not IsDBNull(dtRow("CUSTOMERNAME")) Then
    '                txt_Nama.Value = dtRow("CUSTOMERNAME")
    '            End If
    '        End If
    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub Btn_Import_Customer_Click()
        Try
            window_Import_Customer.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadData_Import_Customer(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            If strsort = "" Then
                strsort = "CIFNo asc"
            End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_AML_Customer_For_EDD", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            store_Import_Customer.DataSource = DataPaging
            store_Import_Customer.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_Import_Customer(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim strCIFNo As String = e.ExtraParams(0).Value
            Dim strCustomerName As String = e.ExtraParams(1).Value
            txt_CIFNo.Value = strCIFNo
            txt_Nama.Value = strCustomerName
            window_Import_Customer.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Import_Customer_Back_Click()
        Try
            window_Import_Customer.Hidden = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Btn_OpenWindowCustomer_Click()
        Try
            If Not String.IsNullOrEmpty(txt_CIFNo.Value) Then
                Dim strQuery As String = "SELECT COUNT(1) AS JumCust FROM vw_AML_Customer_For_EDD WHERE CIFNo = '" & txt_CIFNo.Value & "'"
                Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                If drResult IsNot Nothing AndAlso Not IsDBNull(drResult("JumCust")) Then
                    If drResult("JumCust") > 0 Then
                        WindowPopUpDataCustomer.Hidden = False
                        LoadDataAMLCustomerByCIF(txt_CIFNo.Value)
                    Else
                        Throw New ApplicationException("No AML Customer Found With CIF = " & txt_CIFNo.Value)
                    End If
                Else
                    Throw New ApplicationException("There is Something wrong when seaching Data AML Customer, Please Report this to Admin")
                End If
            Else
                Throw New ApplicationException("CIF is Empty")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_EDD_CustomerDetail_Back_Click()
        Try
            WindowPopUpDataCustomer.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDataAMLCustomerByCIF(CIF As String)
        Try
            Dim strQuery As String = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo = '" & CIF & "'"
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            If drResult IsNot Nothing Then
                Dim strDateFormat As String = NawaBLL.SystemParameterBLL.GetDateFormat()
                If Not IsDBNull(drResult("CIFNo")) Then
                    CustomerDetail_CIF.Value = drResult("CIFNo")

                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_RISK_RATING_RESULT_HISTORY WHERE CIF_NO = '" & drResult("CIFNo") & "' ORDER BY CreatedDate DESC"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    'If tempDrResult IsNot Nothing Then
                    '    If Not IsDBNull(tempDrResult("RISK_RATING_TOTAL_SCORE")) Then
                    '        CustomerDetail_AMLRiskScore.Value = tempDrResult("RISK_RATING_TOTAL_SCORE")
                    '    End If
                    '    If Not IsDBNull(tempDrResult("RISK_RATING_NAME")) Then
                    '        CustomerDetail_AMLRisk.Value = tempDrResult("RISK_RATING_NAME")
                    '    End If
                    'End If
                Else
                    CustomerDetail_CIF.Value = ""
                    'CustomerDetail_AMLRiskScore.Value = ""
                    'CustomerDetail_AMLRisk.Value = ""
                End If

                If Not IsDBNull(drResult("GlobalCustomerNumber")) Then
                    CustomerDetail_GCN.Value = drResult("GlobalCustomerNumber")
                Else
                    CustomerDetail_GCN.Value = ""
                End If

                If Not IsDBNull(drResult("CUSTOMERNAME")) Then
                    CustomerDetail_CustomerName.Value = drResult("CUSTOMERNAME")
                Else
                    CustomerDetail_CustomerName.Value = ""
                End If

                If Not IsDBNull(drResult("DATEOFDATA")) Then
                    Dim tempDate As Date = drResult("DATEOFDATA")
                    Dim tempDateString As String = tempDate.ToString(strDateFormat)
                    CustomerDetail_DateOfData.Value = tempDateString
                Else
                    CustomerDetail_DateOfData.Value = ""
                End If

                If Not IsDBNull(drResult("DATEOFLASTDUEDILIGENCE")) Then
                    Dim tempDate As Date = drResult("DATEOFLASTDUEDILIGENCE")
                    Dim tempDateString As String = tempDate.ToString(strDateFormat)
                    CustomerDetail_LastDueDiligence.Value = tempDateString
                Else
                    CustomerDetail_LastDueDiligence.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_CUSTOMER_STATUS_CODE")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_CUSTOMER_STATUS WHERE FK_AML_CUSTOMER_STATUS_Code = '" & drResult("FK_AML_CUSTOMER_STATUS_CODE") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("AML_CUSTOMER_STATUS_Name")) Then
                        CustomerDetail_Status.Value = tempDrResult("AML_CUSTOMER_STATUS_Name")
                    Else
                        CustomerDetail_Status.Value = ""
                    End If
                Else
                    CustomerDetail_Status.Value = ""
                End If

                If Not IsDBNull(drResult("DATEOFBIRTH")) Then
                    Dim tempDate As Date = drResult("DATEOFBIRTH")
                    Dim tempDateString As String = tempDate.ToString(strDateFormat)
                    CustomerDetail_DateOfBirth.Value = tempDateString
                Else
                    CustomerDetail_DateOfBirth.Value = ""
                End If

                If Not IsDBNull(drResult("PLACEOFBIRTH")) Then
                    CustomerDetail_PlaceOfBirth.Value = drResult("PLACEOFBIRTH")
                Else
                    CustomerDetail_PlaceOfBirth.Value = ""
                End If

                If Not IsDBNull(drResult("MOTHERMAIDEN")) Then
                    CustomerDetail_MotherName.Value = drResult("MOTHERMAIDEN")
                Else
                    CustomerDetail_MotherName.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_JenisKelamin_Code")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_JENIS_KELAMIN WHERE FK_AML_Jenis_Kelamin_Code ='" & drResult("FK_AML_JenisKelamin_Code") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("Jenis_Kelamin_Name")) Then
                        CustomerDetail_Gender.Value = tempDrResult("Jenis_Kelamin_Name")
                    Else
                        CustomerDetail_Gender.Value = ""
                    End If
                Else
                    CustomerDetail_Gender.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_MARITAL_STATUS")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_MARITAL_STATUS WHERE FK_AML_MARITAL_STATUS_CODE = '" & drResult("FK_AML_MARITAL_STATUS") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("MARITAL_STATUS_NAME")) Then
                        CustomerDetail_MartialStatus.Value = tempDrResult("MARITAL_STATUS_NAME")
                    Else
                        CustomerDetail_MartialStatus.Value = ""
                    End If
                Else
                    CustomerDetail_MartialStatus.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_CITIZENSHIP_CODE")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_COUNTRY WHERE FK_AML_COUNTRY_Code = '" & drResult("FK_AML_CITIZENSHIP_CODE") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("AML_COUNTRY_Name")) Then
                        CustomerDetail_Citizenship.Value = tempDrResult("AML_COUNTRY_Name")
                    Else
                        CustomerDetail_Citizenship.Value = ""
                    End If
                Else
                    CustomerDetail_Citizenship.Value = ""
                End If

                If Not IsDBNull(drResult("WORK_PLACE")) Then
                    CustomerDetail_WorkPlace.Value = drResult("WORK_PLACE")
                Else
                    CustomerDetail_WorkPlace.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_Customer_Type_Code")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_CUSTOMER_TYPE WHERE FK_AML_Customer_Type_Code = '" & drResult("FK_AML_Customer_Type_Code") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("Customer_Type_Name")) Then
                        CustomerDetail_CustomerType.Value = tempDrResult("Customer_Type_Name")
                    Else
                        CustomerDetail_CustomerType.Value = ""
                    End If
                Else
                    CustomerDetail_CustomerType.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_Customer_SUB_Type_Code")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_CUSTOMER_SUBTYPE WHERE FK_AML_CUSTOMER_SUBTYPE_Code = '" & drResult("FK_AML_Customer_SUB_Type_Code") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("CUSTOMER_SUBTYPE_Name")) Then
                        CustomerDetail_CustomerSubType.Value = tempDrResult("CUSTOMER_SUBTYPE_Name")
                    Else
                        CustomerDetail_CustomerSubType.Value = ""
                    End If
                Else
                    CustomerDetail_CustomerSubType.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_PEKERJAAN_CODE")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_PEKERJAAN WHERE FK_AML_PEKERJAAN_Code = '" & drResult("FK_AML_PEKERJAAN_CODE") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("AML_PEKERJAAN_Name")) Then
                        CustomerDetail_BusinessType.Value = tempDrResult("AML_PEKERJAAN_Name")
                    Else
                        CustomerDetail_BusinessType.Value = ""
                    End If
                Else
                    CustomerDetail_BusinessType.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_INDUSTRY_CODE")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_INDUSTRY WHERE FK_AML_INDUSTRY_Code = '" & drResult("FK_AML_INDUSTRY_CODE") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("AML_INDUSTRY_Name")) Then
                        CustomerDetail_Industry.Value = tempDrResult("AML_INDUSTRY_Name")
                    Else
                        CustomerDetail_Industry.Value = ""
                    End If
                Else
                    CustomerDetail_Industry.Value = ""
                End If

                If Not IsDBNull(drResult("OpeningDate")) Then
                    Dim tempDate As Date = drResult("OpeningDate")
                    Dim tempDateString As String = tempDate.ToString(strDateFormat)
                    CustomerDetail_OpeningDate.Value = tempDateString
                Else
                    CustomerDetail_OpeningDate.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_Entity_Code")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_Entity WHERE FK_AML_ENTITY_CODE = '" & drResult("FK_AML_Entity_Code") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("Entity_Name")) Then
                        CustomerDetail_CreationEntity.Value = tempDrResult("Entity_Name")
                    Else
                        CustomerDetail_CreationEntity.Value = ""
                    End If
                Else
                    CustomerDetail_CreationEntity.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_Creation_Branch_Code")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_BRANCH WHERE FK_AML_BRANCH_CODE ='" & drResult("FK_AML_Creation_Branch_Code") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("BRANCH_NAME")) Then
                        CustomerDetail_CreationBranch.Value = tempDrResult("BRANCH_NAME")
                    Else
                        CustomerDetail_CreationBranch.Value = ""
                    End If
                Else
                    CustomerDetail_CreationBranch.Value = ""
                End If

                If Not IsDBNull(drResult("FK_AML_SALES_OFFICER_CODE")) Then
                    Dim tempStrQuery As String = "SELECT TOP 1 * FROM AML_SALESOFFICER WHERE FK_AML_SALESOFFICER_CODE ='" & drResult("FK_AML_SALES_OFFICER_CODE") & "'"
                    Dim tempDrResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, tempStrQuery, Nothing)
                    If tempDrResult IsNot Nothing AndAlso Not IsDBNull(tempDrResult("SALESOFFICER_NAME")) Then
                        CustomerDetail_SalesOfficer.Value = tempDrResult("SALESOFFICER_NAME")
                    Else
                        CustomerDetail_SalesOfficer.Value = ""
                    End If
                Else
                    CustomerDetail_SalesOfficer.Value = ""
                End If

                '' Edit 22-Feb-2023
                'If Not IsDBNull(drResult("SOURCE_OF_FUND")) Then
                '    CustomerDetail_SourceOfFound.Value = drResult("SOURCE_OF_FUND")
                'Else
                '    CustomerDetail_SourceOfFound.Value = ""
                'End If

                If Not IsDBNull(drResult("SOURCE_OF_FUND")) Then
                    strQuery = "SELECT TOP 1 AML_INCOME_TYPE_Name FROM AML_INCOME_TYPE WHERE FK_AML_INCOME_TYPE_Code ='" & drResult("SOURCE_OF_FUND") & "'"
                    Dim drIncomeType As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                    Dim strTempSourceOfFund As String = drResult("SOURCE_OF_FUND")
                    If drIncomeType IsNot Nothing AndAlso Not IsDBNull(drIncomeType("AML_INCOME_TYPE_Name")) Then
                        strTempSourceOfFund = strTempSourceOfFund & " ( " & drIncomeType("AML_INCOME_TYPE_Name") & " ) "
                    End If
                    CustomerDetail_SourceOfFound.Value = strTempSourceOfFund
                Else
                    CustomerDetail_SourceOfFound.Value = ""
                End If
                '' End 22-Feb-2023

                If Not IsDBNull(drResult("TUJUAN_DANA")) Then
                    CustomerDetail_PurposeOfFound.Value = drResult("TUJUAN_DANA")
                Else
                    CustomerDetail_PurposeOfFound.Value = ""
                End If
            Else
                Throw New ApplicationException("No AML Customer Data Found With CIF = " & CIF)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SavingDataHistory(unikID As String, notes As String, fileName As String, fileData As Byte())
        Try
            Dim objParam(4) As System.Data.SqlClient.SqlParameter

            objParam(0) = New System.Data.SqlClient.SqlParameter
            objParam(0).ParameterName = "@UnikID"
            objParam(0).Value = unikID
            objParam(0).SqlDbType = SqlDbType.VarChar

            objParam(1) = New System.Data.SqlClient.SqlParameter
            objParam(1).ParameterName = "@Notes"
            objParam(1).Value = notes
            objParam(1).SqlDbType = SqlDbType.VarChar

            objParam(2) = New System.Data.SqlClient.SqlParameter
            objParam(2).ParameterName = "@FileName"
            If fileName IsNot Nothing Then
                objParam(2).Value = fileName
            Else
                objParam(2).Value = DBNull.Value
            End If
            objParam(2).SqlDbType = SqlDbType.VarChar

            objParam(3) = New System.Data.SqlClient.SqlParameter
            objParam(3).ParameterName = "@FileData"
            If fileData IsNot Nothing Then
                objParam(3).Value = fileData
            Else
                objParam(3).Value = DBNull.Value
            End If
            objParam(3).SqlDbType = SqlDbType.VarBinary

            objParam(4) = New System.Data.SqlClient.SqlParameter
            objParam(4).ParameterName = "@UserID"
            objParam(4).Value = NawaBLL.Common.SessionCurrentUser.UserID
            objParam(4).SqlDbType = SqlDbType.VarChar

            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_EDD_NoteHistory_SaveData", objParam)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub GridcommandDownloadAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Download" Then
                Filetodownload = ID
            Else
                Throw New ApplicationException("The command could not be identified, please report this problem to admin")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Sub DownloadFileAttachmentFromHistory_Direct()
        Try
            If String.IsNullOrEmpty(Filetodownload) Then
                Exit Sub
            End If

            Dim strQuery As String = " SELECT TOP 1 FILE_ATTACHMENTName, FILE_ATTACHMENT FROM OneFCC_EDD_Notes_History WHERE PK_ONEFCC_EDD_NOTES_HISTORY_ID = " & Filetodownload
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            If drResult IsNot Nothing AndAlso Not IsDBNull(drResult("FILE_ATTACHMENTName")) AndAlso Not IsDBNull(drResult("FILE_ATTACHMENT")) Then
                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & drResult("FILE_ATTACHMENTName"))
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(drResult("FILE_ATTACHMENT"))
                Response.End()
            Else
                Throw New ApplicationException("No Files to Download")
            End If

            Filetodownload = Nothing
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class