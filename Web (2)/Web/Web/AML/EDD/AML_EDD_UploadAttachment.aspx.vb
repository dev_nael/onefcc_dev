﻿Imports Ext
Imports Elmah
Imports System.Data
Imports CasemanagementBLL

Partial Class AML_EDD_UploadAttachment
    Inherits Parent

    Public Property IDModule() As String
        Get
            Return Session("AML_EDD_UploadAttachment.IDModule")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_UploadAttachment.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("AML_EDD_UploadAttachment.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AML_EDD_UploadAttachment.IDUnik") = value
        End Set
    End Property

    Public Property FileAttachmentName() As String
        Get
            Return Session("AML_EDD_UploadAttachment.FileAttachmentName")
        End Get
        Set(ByVal value As String)
            Session("AML_EDD_UploadAttachment.FileAttachmentName") = value
        End Set
    End Property

    Public Property FileAttachmentByte() As Byte()
        Get
            Return Session("AML_EDD_UploadAttachment.FileAttachmentByte")
        End Get
        Set(ByVal value As Byte())
            Session("AML_EDD_UploadAttachment.FileAttachmentByte") = value
        End Set
    End Property

    Sub ClearSession()
        IDModule = Nothing
        IDUnik = Nothing
    End Sub

    'Private Sub AML_EDD_Detail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
    '    ActionType = NawaBLL.Common.ModuleActionEnum.view
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then

                ClearSession()

                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If

                IDModule = Request.Params("ModuleID")
                Dim intModuleID As Integer = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                'FormPanelInput.Title = ObjModule.ModuleLabel & " - Attachment"
                If FileAttachmentName IsNot Nothing Then
                    File_Attachment.Value = FileAttachmentName
                End If

            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub FileUploadField_ChangeHandler()
        Try
            If File_Attachment.HasFile Then
                FileAttachmentByte = File_Attachment.FileBytes
                FileAttachmentName = File_Attachment.FileName
            Else
                FileAttachmentByte = Nothing
                FileAttachmentName = Nothing
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_EDD_Attachment_Submit_Click()
        Try
            If File_Attachment.HasFile Then
                FileAttachmentByte = File_Attachment.FileBytes
                FileAttachmentName = File_Attachment.FileName

                Throw New ApplicationException("File Uploaded, Please Back To Continue")
            Else
                FileAttachmentByte = Nothing
                FileAttachmentName = Nothing

                Throw New ApplicationException("No File Uploaded")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class