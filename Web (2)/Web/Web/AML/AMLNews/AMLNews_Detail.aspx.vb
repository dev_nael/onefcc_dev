﻿Imports CasemanagementDAL
Imports CasemanagementBLL
Imports System.IO

Partial Class AML_AMLNews_AMLNews_Detail
    Inherits ParentPage
    Public objFormModuleDetail As NawaBLL.FormModuleDetail
    Const PATH_TEMP_DIR As String = "~\temp\"

    Public Property objNewsClass As AML_News_BLL.AML_News_Class
        Get
            If Session("AML_News.objNewsClass") Is Nothing Then
                Session("AML_News.objNewsClass") = New AML_News_BLL.AML_News_Class
            End If

            Return Session("AML_News.objNewsClass")
        End Get
        Set(value As AML_News_BLL.AML_News_Class)
            Session("AML_News.objNewsClass") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("AMLNews.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AMLNews.IDUnik") = value
        End Set
    End Property

    Private Sub AML_News_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                'Clear Session
                ClearSession()

                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If

                Load_News()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Load_News()
        objNewsClass = AML_News_BLL.getNewsClassByID(IDUnik)

        If objNewsClass IsNot Nothing Then
            With objNewsClass.objNews
                txtTitle.Value = .News_Title
                txtContent.Value = .News_Content
                txtContentShort.Value = .News_Content_Short
                txtValidDateFrom.Value = .ValidFrom
                txtValidDateTo.Value = .ValidTo
                If .IsPublic = True Then
                    chkIsPublic.Checked = True
                Else
                    chkIsPublic.Checked = False
                End If

                txtNewsImage.Value = .News_ImageName
                If .News_Image IsNot Nothing Then
                    imgNewsImage.Visible = True

                    'Load the image
                    'Create Directory if not exists
                    If Not Directory.Exists(Server.MapPath(PATH_TEMP_DIR)) Then
                        Directory.CreateDirectory(Server.MapPath(PATH_TEMP_DIR))
                    End If

                    'Filename and location for output template
                    Dim tempImage As String = "AML_News_" & .PK_AML_News_ID & "_" & .News_ImageName 'Guid.NewGuid.ToString & .News_ImageName
                    Dim objfileinfo = New FileInfo(Server.MapPath(PATH_TEMP_DIR & tempImage))
                    Dim data() As Byte = .News_Image
                    File.WriteAllBytes(objfileinfo.FullName, data)

                    imgNewsImage.ImageUrl = PATH_TEMP_DIR & tempImage
                Else
                    imgNewsImage.Visible = False
                End If

                txtNewsAttachment.Text = .News_AttachmentName
                If .News_Attachment IsNot Nothing Then
                    btnDownloadAttachment.Visible = True
                Else
                    btnDownloadAttachment.Visible = False
                End If

                'txttImage.Value = .News_ImageName
                'txtAttachment.Value = .News_AttachmentName

                'txttImage.SetText(.News_ImageName)

                txtTitle.ReadOnly = True
                txtContent.ReadOnly = True
                txtContentShort.ReadOnly = True
                txtValidDateFrom.ReadOnly = True
                txtValidDateTo.ReadOnly = True
                chkIsPublic.ReadOnly = True


            End With
        End If
    End Sub

    Private Sub ClearSession()
        objNewsClass = New AML_News_BLL.AML_News_Class
    End Sub


    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AML_AMLNews_AMLNews_Detail_Init(sender As Object, e As EventArgs) Handles Me.Init
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    Private Sub AML_AMLNews_AMLNews_Detail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        objFormModuleDetail = New NawaBLL.FormModuleDetail(fpMain, Nothing, Nothing, Nothing)
    End Sub

    'Protected Sub StoreListStock_ReadData(sender As Object, e As StoreReadDataEventArgs)
    '    StoreListStock.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM TV_WH_Master_Product as x where x.active = 1", Nothing)
    '    StoreListStock.DataBind()
    'End Sub

    Protected Sub btnDownloadAttachment_Click()
        Try
            Response.Clear()
            Response.ClearHeaders()
            'Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("content-disposition", "attachment;filename=" & objNewsClass.objNews.News_AttachmentName)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "ContentType"
            Response.BinaryWrite(objNewsClass.objNews.News_Attachment)
            Response.End()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
