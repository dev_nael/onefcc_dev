﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AMLNews_Detail.aspx.vb" Inherits="AML_AMLNews_AMLNews_Detail" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        var onKeyUp = function (combo, e) {
            var v = combo.getRawValue();
            combo.store.filter(combo.displayField, new RegExp(v, "i"));
            combo.onLoad();
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel runat="server" ID="fpMain" BodyPadding="10" ButtonAlign="Center" Scrollable="Both" Title="AML News Detail">
        <Items>

<%--            <ext:DisplayField ID="txtTitle" runat="server" AnchorHorizontal="30%" AllowBlank="false"  FieldStyle="text-align: center;width : 1000px;font-size : 30px;height:100px;line-height:1.5"  />
            <ext:DisplayField runat="server"></ext:DisplayField>
            <ext:DisplayField ID="txtValidDateFrom" runat="server"  AnchorHorizontal="30%" AllowBlank="false" FieldStyle="width : 1000px;font-size : 15px;color : grey;text-align: right;" />--%>
		<%--	<ext:DisplayField ID="txtValidDateTo" runat="server"  AnchorHorizontal="30%" AllowBlank="false" FieldStyle="width : 1000px;font-size : 15px;color : grey;" />--%>
<%--			<ext:DisplayField ID="txtContent" runat="server"  AnchorHorizontal="30%" AllowBlank="false" FieldStyle="width : 1000px;font-size : 15px;color : black;text-align: justify;line-height: 2em;text-indent: 50px"/>
			<ext:DisplayField ID="DisplayField1" runat="server"  AnchorHorizontal="30%" AllowBlank="false" FieldStyle="width : 1000px;font-size : 15px;color : black;text-align: justify;line-height: 2em;text-indent: 50px"/>

             <ext:DisplayField runat="server"></ext:DisplayField>
			<ext:DisplayField ID="txtCreatedBy" runat="server"  AnchorHorizontal="30%" AllowBlank="false" FieldStyle="width : 1000px;font-size : 15px;color : grey;text-align: left;"/>--%>

            <ext:TextField ID="txtTitle" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Title" MaxLength="500" EnforceMaxLength="true" />
            <ext:HtmlEditor ID="txtContent" runat="server" FieldLabel="Content" AllowBlank="false" Height="200" AutoScroll="true" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true" FieldStyle="background-color:#ffe4c4 !important;" >
                <Listeners>
                    <Initialize Handler="this.getToolbar().hide();" />
                </Listeners>
            </ext:HtmlEditor>
            <ext:TextArea ID="txtContentShort" runat="server"  AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Content Short" MaxLength="500" EnforceMaxLength="true" />

            <ext:DisplayField ID="txtNewsImage" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="News Image" />
            <ext:Image runat="server" ID="imgNewsImage" ImageUrl="" AriaLabel="News Image" Style="margin-left:155px;width:200px;margin-bottom:10px;margin-top:-10px;" MinWidth="200"></ext:Image>

            <ext:DisplayField ID="txtNewsAttachment" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Attachment" />
            <ext:Button ID="btnDownloadAttachment" runat="server" Text="Download" Icon="DiskDownload" Style="margin-left:155px;margin-bottom:10px;margin-top:-10px;">
                <DirectEvents>
                    <Click OnEvent="btnDownloadAttachment_Click" IsUpload="true" />
                </DirectEvents>
            </ext:Button>

            <ext:DateField ID="txtValidDateFrom" runat="server"  AnchorHorizontal="30%" AllowBlank="false" FieldLabel="Valid From"    Format="dd-MMM-yyyy" />
            <ext:DateField ID="txtValidDateTo" runat="server"  AnchorHorizontal="30%" AllowBlank="false" FieldLabel="Valid To"   Format="dd-MMM-yyyy"/>
            <ext:Checkbox ID="chkIsPublic"  runat="server" FieldLabel="Is Public">
                <DirectEvents>
                    <%--<Change OnEvent="chkIsPublic_Change"></Change>--%>
                </DirectEvents>
            </ext:Checkbox>

        </Items>            
        <Buttons>
          
            <ext:Button ID="BtnCancel" runat="server" Text="Cancel" Icon="Cancel">
                <Listeners>
                    <Click Handler="#{BtnCancel}.disable()" />
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="btnCancel_DirectEvent">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="500">
                        </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
