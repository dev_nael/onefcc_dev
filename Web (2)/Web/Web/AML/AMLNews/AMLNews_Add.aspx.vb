﻿Imports CasemanagementDAL
Imports CasemanagementBLL

Partial Class AML_AMLNews_AMLNews_Add
    Inherits ParentPage
    Public objFormModuleAdd As NawaBLL.FormModuleAdd

    Public Property objNewsClass As AML_News_BLL.AML_News_Class
        Get
            If Session("AMLNews_Add.objNewsClass") Is Nothing Then
                Session("AMLNews_Add.objNewsClass") = New AML_News_BLL.AML_News_Class
            End If

            Return Session("AMLNews_Add.objNewsClass")
        End Get
        Set(value As AML_News_BLL.AML_News_Class)
            Session("AMLNews_Add.objNewsClass") = value
        End Set
    End Property

    Private Sub AMLNews_Add_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()
                Dim intmaxfilesize As Double = NawaBLL.SystemParameterBLL.GetMaxFileSize
                Dim strmaxfilesize As String = (intmaxfilesize / 1048576) & " MB"
                txtAttachment.Listeners.Change.Handler = "#{LblFileReport}.setValue(#{txtAttachment}.value);if(!UpdateUploadInfo(this.button.fileInputEl.dom," & intmaxfilesize & ",'" & strmaxfilesize & "')) {this.reset();};  "

            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ClearSession()
        objNewsClass = New AML_News_BLL.AML_News_Class
    End Sub

    Private Sub isDataValid()
        If txtTitle.Text.Length > 100 Then
            Throw New ApplicationException("Title Max Length 100 character")
        End If
        If txtContent.Text.Length > 8000 Then
            Throw New ApplicationException("Content Length Max 8000 character")
        End If


    End Sub
    'Protected Sub chkIsPublic_Change(sender As Object, e As EventArgs)
    '    Try
    '        If chkIsPublic.Checked = True Then
    '            objNews.IsPublic = 1
    '        Else
    '            objNews.IsPublic = 0
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub btnSave_Click(sender As Object, e As DirectEventArgs)
        Try

            If CDate(txtValidDateTo.Value) < CDate(txtValidDateFrom.Value) Then
                Throw New ApplicationException("Valid From tidak Melebihi Valid To")
            End If
            If String.IsNullOrWhiteSpace(txtTitle.Value) Then
                Throw New ApplicationException(txtTitle.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrWhiteSpace(txtContent.Value) Then
                Throw New ApplicationException(txtContent.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrWhiteSpace(txtContentShort.Value) Then
                Throw New ApplicationException(txtContentShort.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(CDate(txtValidDateTo.Value)) Then
                Throw New ApplicationException(txtValidDateTo.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(CDate(txtValidDateFrom.Value)) Then
                Throw New ApplicationException(txtValidDateFrom.FieldLabel & " harus diisi.")
            End If

            'Your code to upload data
            'Validasi jika belum ada file yang diupload
            'If Not txtAttachment.HasFile Then
            '    Throw New ApplicationException("Belum ada file yang diupload.")
            'End If

            ''Validasi hanya terima xlsx
            'If Not txtAttachment.FileName.EndsWith(".xml") Then
            '    Throw New ApplicationException("Format yang valid hanya .xml")
            'End If


            isDataValid()
            objNewsClass.objNews.News_Title = txtTitle.Text
            'objNews.News_Content = txtContent.Text.ToString
            Dim Val = txtContent.RawValue.ToString.Replace("%3E%u200B%3C", "%3E%3C").Replace("+", "%2B")
            If String.IsNullOrEmpty(Val) Then
                objNewsClass.objNews.News_Content = txtContent.Value
            Else
                objNewsClass.objNews.News_Content = Server.UrlDecode(Val)
            End If
            objNewsClass.objNews.News_Content_Short = txtContentShort.Text
            objNewsClass.objNews.ValidFrom = txtValidDateFrom.Text
            objNewsClass.objNews.ValidTo = txtValidDateTo.Text
            objNewsClass.objNews.News_Image = txttImage.FileBytes
            objNewsClass.objNews.News_ImageName = txttImage.FileName
            If chkIsPublic.Checked = True Then
                objNewsClass.objNews.IsPublic = 1
            Else
                objNewsClass.objNews.IsPublic = 0
            End If
            objNewsClass.objNews.News_AttachmentName = txtAttachment.FileName
            objNewsClass.objNews.News_Attachment = txtAttachment.FileBytes

            'Dim itemBytes = CType((i), Byte())
            '    Dim ItemHex = BitConverter.ToString(itemBytes)

            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                AML_News_BLL.SaveAddTanpaApproval(objNewsClass, ObjModule)
                Panelconfirmation.Hidden = False
                fpMain.Hidden = True
                LblConfirmation.Text = "Data Saved into Database"
            Else
                AML_News_BLL.SaveAddWithApproval(objNewsClass, ObjModule)
                Panelconfirmation.Hidden = False
                fpMain.Hidden = True
                LblConfirmation.Text = "Data Saved into Pending Approval"
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AMLNews_Add_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Insert
    End Sub

    Private Sub AMLNews_Add_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleAdd = New NawaBLL.FormModuleAdd(fpMain, Panelconfirmation, LblConfirmation)
    End Sub

End Class
