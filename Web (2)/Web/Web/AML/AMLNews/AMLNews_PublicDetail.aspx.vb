﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports NawaBLL
Imports NawaDAL

Partial Class AMLNews_PublicDetail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                Load_AML_News()
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#Region "Update 20-Aug-2021 : AML News"
    Protected Sub Load_AML_News()
        Try

            Dim strEncNewsID As String = Request.Params("NewsID")
            Dim strNewsID As String = NawaBLL.Common.DecryptQueryString(strEncNewsID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            Const PATH_TEMP_DIR As String = "~\temp\"
            'Create Directory if not exists
            If Not Directory.Exists(Server.MapPath(PATH_TEMP_DIR)) Then
                Directory.CreateDirectory(Server.MapPath(PATH_TEMP_DIR))
            End If

            'Get News Page
            Dim Query As String = "SELECT PK_AML_News_ID,News_Title,News_Content,IsPublic,ValidTo,Active,CreatedBy,format(createddate,  'dddd, MMMM dd yyyy') as Dates,News_Content_Short,ValidFrom,News_Image,News_ImageName,News_AttachmentName, CASE WHEN datalength(news_attachment) <= 1024 THEN   CONVERT(NVARCHAR, datalength(news_attachment)) + ' BYTES' WHEN datalength(news_attachment) >1024 AND datalength(news_attachment) <= 1048576 THEN   CONVERT(NVARCHAR, datalength(news_attachment)/1024) + ' KB' WHEN datalength(news_attachment) >1048576 AND datalength(news_attachment) <= 1073741824  THEN   CONVERT(NVARCHAR, datalength(news_attachment)/1048576) + ' MB' WHEN datalength(news_attachment) >1073741824 AND datalength(news_attachment) <= 109951162777    THEN   CONVERT(NVARCHAR, datalength(news_attachment)/1073741824) + ' GB' ELSE CONVERT(NVARCHAR, datalength(news_attachment)/109951162777) + ' TB' END as size_file FROM AML_News WHERE IsPublic=1 AND PK_AML_News_ID = " & strNewsID

            Dim dtNews As New Data.DataTable
            dtNews = SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, Query, Nothing)

            '13-Sep-2021 Add column image untuk menampung URL
            'Running the following script for additional columns
            dtNews.Columns.Add(New DataColumn("News_Image_URL", GetType(String)))
            If dtNews.Rows.Count > 0 Then
                For Each item As Data.DataRow In dtNews.Rows
                    If Not IsDBNull(item("News_Image")) Then
                        'Filename and location for output template
                        Dim tempImage As String = "AML_News_" & item("PK_AML_News_ID") & "_" & item("News_ImageName") 'Guid.NewGuid.ToString & .News_ImageName
                        Dim objfileinfo = New FileInfo(Server.MapPath(PATH_TEMP_DIR & tempImage))

                        Dim data() As Byte = item("News_Image")
                        File.WriteAllBytes(objfileinfo.FullName, data)

                        item("News_Image_URL") = "../../Temp/" & tempImage
                    End If
                Next
            End If

            'Convert to PageDataSource
            If dtNews IsNot Nothing Then
                Dim pdsNews As New PagedDataSource
                Dim dvNews As DataView = New DataView(dtNews)
                pdsNews.DataSource = dvNews
                pdsNews.AllowPaging = True
                pdsNews.PageSize = 1
                pdsNews.CurrentPageIndex = 0

                'Bind to repeater
                rptNews.DataSource = pdsNews
                rptNews.DataBind()

                rptNews.Visible = True
            Else
                rptNews.Visible = False
            End If
            Dim query2 As String = ""
            Dim intNewsPerPage2 As Integer = 1

            'Get News limit on login form
            query2 = "SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID=8008"
            Dim intNewsLimit2 As Integer = SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query2, Nothing)
            If Not IsNothing(intNewsLimit2) Then
                intNewsPerPage2 = intNewsLimit2
            End If

            'Get News Page
            query2 = "SELECT  TOP 1 an.PK_AML_News_ID,an.News_Title,an.News_Content,an.IsPublic,an.ValidTo,an.Active,an.CreatedBy,convert(varchar, an.createddate, 106) as Dates,an.News_Content_Short,an.ValidFrom,an.News_Image,an.News_ImageName FROM AML_News as an join (select ROW_NUMBER() OVER(order by pk_aml_news_id) AS num_row,pk_aml_news_id from aml_news WHERE IsPublic=1 and ValidFrom <= getdate() and  ValidTo >= getdate()) t on t.pk_aml_news_id = an.pk_aml_news_id WHERE an.IsPublic=1 and t.num_row%2 = 1 AND an.PK_AML_News_ID != " & strNewsID
            query2 = query2 + " And ValidFrom <= '" & DateTime.Now.ToString("yyyy-MM-dd") & "' AND ValidTo >= '" & DateTime.Now.ToString("yyyy-MM-dd") & "'"
            query2 = query2 + " ORDER BY PK_AML_News_ID DESC"
            Dim dtNews2 As New Data.DataTable
            dtNews2 = SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query2, Nothing)

            dtNews2.Columns.Add(New DataColumn("News_Image_URL", GetType(String)))
            If dtNews2.Rows.Count > 0 Then
                For Each item As Data.DataRow In dtNews2.Rows
                    If Not IsDBNull(item("News_Image")) Then
                        'Filename and location for output template
                        Dim tempImage As String = "AML_News_" & item("PK_AML_News_ID") & "_" & item("News_ImageName") 'Guid.NewGuid.ToString & .News_ImageName
                        Dim objfileinfo = New FileInfo(Server.MapPath(PATH_TEMP_DIR & tempImage))

                        Dim data() As Byte = item("News_Image")
                        File.WriteAllBytes(objfileinfo.FullName, data)

                        item("News_Image_URL") = "../../Temp/" & tempImage
                    End If
                Next
            End If

            'Convert to PageDataSource
            If dtNews2 IsNot Nothing And dtNews2.Rows.Count > 0 Then
                Dim pdsNews2 As New PagedDataSource
                Dim dvNews2 As DataView = New DataView(dtNews2)
                pdsNews2.DataSource = dvNews2

                If ViewState("PageNumber") IsNot Nothing Then
                    pdsNews2.CurrentPageIndex = Convert.ToInt32(ViewState("PageNumber"))
                Else
                    pdsNews2.CurrentPageIndex = 0
                End If

                'Bind to repeater
                rptNews2.DataSource = pdsNews2
                rptNews2.DataBind()
                rptNews2.Visible = True
            Else
            End If

            Dim query3 As String = ""
            Dim intNewsPerPage3 As Integer = 1

            'Get News limit on login form
            query3 = "SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID=8008"
            Dim intNewsLimit3 As Integer = SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query3, Nothing)
            If Not IsNothing(intNewsLimit3) Then
                intNewsPerPage3 = intNewsLimit3
            End If

            'Get News Page
            query3 = "SELECT  TOP 1 an.PK_AML_News_ID,an.News_Title,an.News_Content,an.IsPublic,an.ValidTo,an.Active,an.CreatedBy,convert(varchar, an.createddate, 106) as Dates,an.News_Content_Short,an.ValidFrom,an.News_Image,an.News_ImageName  FROM AML_News as an join (select ROW_NUMBER() OVER(order by pk_aml_news_id) AS num_row,pk_aml_news_id from aml_news WHERE IsPublic=1 and ValidFrom <= getdate() and  ValidTo >= getdate()) t on t.pk_aml_news_id = an.pk_aml_news_id WHERE an.IsPublic=1 and t.num_row%2 = 0 AND an.PK_AML_News_ID != " & strNewsID
            query3 = query3 + " AND ValidFrom <= '" & DateTime.Now.ToString("yyyy-MM-dd") & "' AND ValidTo >= '" & DateTime.Now.ToString("yyyy-MM-dd") & "'"
            query3 = query3 + " ORDER BY PK_AML_News_ID DESC"
            Dim dtNews3 As New Data.DataTable
            dtNews3 = SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query3, Nothing)

            dtNews3.Columns.Add(New DataColumn("News_Image_URL", GetType(String)))
            If dtNews3.Rows.Count > 0 Then
                For Each item As Data.DataRow In dtNews3.Rows
                    If Not IsDBNull(item("News_Image")) Then
                        'Filename and location for output template
                        Dim tempImage As String = "AML_News_" & item("PK_AML_News_ID") & "_" & item("News_ImageName") 'Guid.NewGuid.ToString & .News_ImageName
                        Dim objfileinfo = New FileInfo(Server.MapPath(PATH_TEMP_DIR & tempImage))

                        Dim data() As Byte = item("News_Image")
                        File.WriteAllBytes(objfileinfo.FullName, data)

                        item("News_Image_URL") = "../../Temp/" & tempImage
                    End If
                Next
            End If


            'Convert to PageDataSource
            If dtNews3 IsNot Nothing And dtNews3.Rows.Count > 0 Then
                Dim pdsNews3 As New PagedDataSource
                Dim dvNews3 As DataView = New DataView(dtNews3)
                pdsNews3.DataSource = dvNews3

                If ViewState("PageNumber") IsNot Nothing Then
                    pdsNews3.CurrentPageIndex = Convert.ToInt32(ViewState("PageNumber"))
                Else
                    pdsNews3.CurrentPageIndex = 0
                End If

                'Bind to repeater
                rptNews3.DataSource = pdsNews3
                rptNews3.DataBind()
                rptNews3.Visible = True
            Else
            End If

        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnDownload_Click(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptNews.ItemCommand
        Try
            If e.CommandName = "remove" Then
                Dim id As Integer = Convert.ToInt32(e.CommandArgument)
                Dim bytes As Byte()
                Dim fileName As String, contentType As String
                Dim conn As New System.Data.SqlClient.SqlConnection(NawaDAL.SQLHelper.strConnectionString)
                Using conn
                    Using cmd As New SqlCommand()
                        cmd.CommandText = "select news_attachment,News_AttachmentName from aml_news where pk_aml_news_id =@Id"
                        cmd.Parameters.AddWithValue("@Id", id)
                        cmd.Connection = conn
                        conn.Open()
                        Using sdr As SqlDataReader = cmd.ExecuteReader()
                            sdr.Read()
                            bytes = DirectCast(sdr("News_AttachmentName"), Byte())
                            'contentType = sdr("ContentType").ToString()
                            fileName = sdr("News_AttachmentName").ToString()
                        End Using
                        conn.Close()
                    End Using
                End Using
                Response.Clear()
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = contentType
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName)
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnBackHome_Click()
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "~/Login.aspx"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnBackNews_Click()
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "~/AML/AMLNews/AMLNews_PublicView.aspx"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub rptNews2_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptNews2.ItemCommand
        Try
            If e.CommandName = "remove" Then
                Dim intNewsID As Integer = Convert.ToInt32(e.CommandArgument)
                Dim strEncNewsID As String = NawaBLL.Common.EncryptQueryString(intNewsID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & "~/AML/AMLNews/AMLNews_PublicDetail.aspx?NewsID=" & strEncNewsID)

            End If

        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub rptNews3_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptNews3.ItemCommand
        Try
            If e.CommandName = "remove" Then
                Dim intNewsID As Integer = Convert.ToInt32(e.CommandArgument)
                Dim strEncNewsID As String = NawaBLL.Common.EncryptQueryString(intNewsID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & "~/AML/AMLNews/AMLNews_PublicDetail.aspx?NewsID=" & strEncNewsID)

            End If

        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

    Protected Sub linkButtonLogin_Click()
        Try
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & "~/Login.aspx")
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub linkButtonNews_Click()
        Try
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & "~/AML/AMLNews/AMLNews_PublicView.aspx")
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

End Class
