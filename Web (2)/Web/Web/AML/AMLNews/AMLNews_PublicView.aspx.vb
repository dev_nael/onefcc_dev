﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports System.Net
Imports System.Security.Policy
Imports Excel.Log
Imports NawaBLL
Imports NawaDAL

Partial Class AMLNews_PublicView
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                Load_AML_News()
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#Region "Update 20-Aug-2021 : AML News"
    Protected Sub Load_AML_News()
        Try

            Dim query As String = ""
            Dim query2 As String = ""
            Dim intNewsPerPage As Integer = 4
            Dim intNewsPerPage2 As Integer = 4

            Const PATH_TEMP_DIR As String = "~\temp\"
            'Create Directory if not exists
            If Not Directory.Exists(Server.MapPath(PATH_TEMP_DIR)) Then
                Directory.CreateDirectory(Server.MapPath(PATH_TEMP_DIR))
            End If

            'Get News limit on login form
            query = "SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID=8008"
            Dim intNewsLimit As Integer = SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
            If Not IsNothing(intNewsLimit) Then
                intNewsPerPage = intNewsLimit
            End If

            'Get News limit on login form
            query2 = "SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID=8008"
            Dim intNewsLimit2 As Integer = SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query2, Nothing)
            If Not IsNothing(intNewsLimit2) Then
                intNewsPerPage2 = intNewsLimit2
            End If

            'Get News Page Dengan Nilai Row Ganjil
            'query = "SELECT * FROM AML_News WHERE IsPublic=1 ORDER BY PK_AML_News_ID OFFSET " & intOffset & " ROWS FETCH NEXT " & intNewsPerPage & " ROWS ONLY"
            query = "SELECT an.PK_AML_News_ID,an.News_Title,an.News_Content,an.IsPublic,an.ValidTo,an.Active,an.CreatedBy,convert(varchar, an.createddate, 106) as Dates,an.News_Content_Short,an.ValidFrom,an.News_Image,an.News_ImageName  FROM AML_News as an join (select ROW_NUMBER() OVER(order by pk_aml_news_id) AS num_row,pk_aml_news_id from aml_news WHERE IsPublic=1 and ValidFrom <= getdate() and  ValidTo >= getdate()) t on t.pk_aml_news_id = an.pk_aml_news_id WHERE an.IsPublic=1 and t.num_row%2 = 1 "
            query = query + " AND an.ValidFrom <= '" & DateTime.Now.ToString("yyyy-MM-dd") & "' AND an.ValidTo >= '" & DateTime.Now.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture) & "'"
            query = query + " ORDER BY an.PK_AML_News_ID DESC"

            'Get News Page Dengan Nilai Row Genap
            'query = "SELECT * FROM AML_News WHERE IsPublic=1 ORDER BY PK_AML_News_ID OFFSET " & intOffset & " ROWS FETCH NEXT " & intNewsPerPage & " ROWS ONLY"
            query2 = "SELECT an.PK_AML_News_ID,an.News_Title,an.News_Content,an.IsPublic,an.ValidTo,an.Active,an.CreatedBy,convert(varchar, an.createddate, 106) as Dates,an.News_Content_Short,an.ValidFrom,an.News_Image,an.News_ImageName  FROM AML_News as an join (select ROW_NUMBER() OVER(order by pk_aml_news_id) AS num_row,pk_aml_news_id from aml_news WHERE IsPublic=1 and ValidFrom <= getdate() and  ValidTo >= getdate()) t on t.pk_aml_news_id = an.pk_aml_news_id WHERE an.IsPublic=1 and t.num_row%2 = 0 "
            query2 = query2 + " AND an.ValidFrom <= '" & DateTime.Now.ToString("yyyy-MM-dd") & "' AND an.ValidTo >= '" & DateTime.Now.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture) & "'"
            query2 = query2 + " ORDER BY an.PK_AML_News_ID DESC"

            'query = "SELECT * FROM AML_News WHERE IsPublic=1"
            Dim dtNews As New Data.DataTable
            dtNews = SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)

            'query2 = "SELECT * FROM AML_News WHERE IsPublic=1"
            Dim dtNews2 As New Data.DataTable
            dtNews2 = SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query2, Nothing)

            '13-Sep-2021 Add column image untuk menampung URL
            'Running the following script for additional columns
            dtNews.Columns.Add(New DataColumn("News_Image_URL", GetType(String)))
            If dtNews.Rows.Count > 0 Then
                For Each item As Data.DataRow In dtNews.Rows
                    If Not IsDBNull(item("News_Image")) Then
                        'Filename and location for output template
                        Dim tempImage As String = "AML_News_" & item("PK_AML_News_ID") & "_" & item("News_ImageName") 'Guid.NewGuid.ToString & .News_ImageName
                        Dim objfileinfo = New FileInfo(Server.MapPath(PATH_TEMP_DIR & tempImage))

                        Dim data() As Byte = item("News_Image")
                        File.WriteAllBytes(objfileinfo.FullName, data)

                        item("News_Image_URL") = "../../Temp/" & tempImage
                    End If
                Next
            End If

            dtNews2.Columns.Add(New DataColumn("News_Image_URL", GetType(String)))
            If dtNews2.Rows.Count > 0 Then
                For Each item As Data.DataRow In dtNews2.Rows
                    If Not IsDBNull(item("News_Image")) Then
                        'Filename and location for output template
                        Dim tempImage As String = "AML_News_" & item("PK_AML_News_ID") & "_" & item("News_ImageName") 'Guid.NewGuid.ToString & .News_ImageName
                        Dim objfileinfo = New FileInfo(Server.MapPath(PATH_TEMP_DIR & tempImage))

                        Dim data() As Byte = item("News_Image")
                        File.WriteAllBytes(objfileinfo.FullName, data)

                        item("News_Image_URL") = "../../Temp/" & tempImage
                    End If
                Next
            End If

            'Convert to PageDataSource
            If dtNews IsNot Nothing Then
                'For Each row As DataRow In dtNews.Rows
                '    Dim searchedValue As String = Nothing
                '    searchedValue = row.Item("News_Image").ToString()

                '    If IsNoImage(searchedValue) Then
                '    Else
                '        row.Item("News_Image") = Nothing
                '    End If
                'Next row
                Dim pdsNews As New PagedDataSource
                Dim dvNews As DataView = New DataView(dtNews)
                pdsNews.DataSource = dvNews
                pdsNews.AllowPaging = True
                pdsNews.PageSize = intNewsPerPage
                pdsNews.CurrentPageIndex = 0

                If ViewState("PageNumber") IsNot Nothing Then
                    pdsNews.CurrentPageIndex = Convert.ToInt32(ViewState("PageNumber"))
                Else
                    pdsNews.CurrentPageIndex = 0
                End If

                If pdsNews.PageCount > 1 Then
                    rptPaging.Visible = True
                    Dim pages As New ArrayList()
                    For i As Integer = 0 To pdsNews.PageCount - 1
                        pages.Add((i + 1).ToString())
                    Next
                    rptPaging.DataSource = pages
                    rptPaging.DataBind()
                Else
                    rptPaging.Visible = False
                End If

                'Bind to repeater
                rptNews.DataSource = pdsNews
                rptNews.DataBind()

                rptNews.Visible = True
            Else
                rptNews.Visible = False
                Load_No_News()
            End If

            'Convert to PageDataSource
            If dtNews2 IsNot Nothing Then
                Dim pdsNews2 As New PagedDataSource
                Dim dvNews2 As DataView = New DataView(dtNews2)
                pdsNews2.DataSource = dvNews2
                pdsNews2.AllowPaging = True
                pdsNews2.PageSize = intNewsPerPage2
                pdsNews2.CurrentPageIndex = 0

                If ViewState("PageNumber") IsNot Nothing Then
                    pdsNews2.CurrentPageIndex = Convert.ToInt32(ViewState("PageNumber"))
                Else
                    pdsNews2.CurrentPageIndex = 0
                End If

                If pdsNews2.PageCount > 1 Then
                    rptPaging.Visible = True
                    Dim pages As New ArrayList()
                    For i As Integer = 0 To pdsNews2.PageCount - 1
                        pages.Add((i + 1).ToString())
                    Next
                    rptPaging.DataSource = pages
                    rptPaging.DataBind()
                Else
                    rptPaging.Visible = False
                End If

                'Bind to repeater
                rptNews2.DataSource = pdsNews2
                rptNews2.DataBind()

                rptNews2.Visible = True
            Else
                rptNews2.Visible = False
                Load_No_News()
            End If

        Catch ex As Exception
            'Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            'Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
            Load_No_News()
        End Try
    End Sub

    Protected Sub rptPaging_ItemCommand(ByVal source As Object, ByVal e As RepeaterCommandEventArgs)
        ViewState("PageNumber") = Convert.ToInt32(e.CommandArgument) - 1
        Load_AML_News()
    End Sub

    Private Declare Function URLDownloadToFile Lib "urlmon" Alias "URLDownloadToFileA" _
 (ByVal pCaller As Long,
  ByVal szURL As String, ByVal _
  szFileName As String,
  ByVal dwReserved As Long,
  ByVal lpfnCB As Long) As Long




    Protected Sub Load_No_News()
        Try
            rowNews.Visible = False
            'rowMoreNews.Visible = False
            rptPaging.Visible = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnBackHome_Click()
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "~/Login.aspx"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub rptNews_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptNews.ItemCommand
        Try
            If e.CommandName = "remove" Then
                Dim intNewsID As Integer = Convert.ToInt32(e.CommandArgument)
                Dim strEncNewsID As String = NawaBLL.Common.EncryptQueryString(intNewsID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & "~/AML/AMLNews/AMLNews_PublicDetail.aspx?NewsID=" & strEncNewsID)

            End If

        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub rptNews2_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptNews2.ItemCommand
        Try
            If e.CommandName = "remove2" Then
                Dim intNewsID As Integer = Convert.ToInt32(e.CommandArgument)
                Dim strEncNewsID As String = NawaBLL.Common.EncryptQueryString(intNewsID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & "~/AML/AMLNews/AMLNews_PublicDetail.aspx?NewsID=" & strEncNewsID)

            End If

        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

    Protected Sub linkButtonLogin_Click()
        Try
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & "~/Login.aspx")
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class
