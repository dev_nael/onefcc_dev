﻿Imports CasemanagementDAL
Imports CasemanagementBLL
Imports System.Globalization

Partial Class AML_AMLNews_AMLNews_Edit
    Inherits ParentPage

    Public objFormModuleEdit As NawaBLL.FormModuleEdit

    Public Property objNewsClass As AML_News_BLL.AML_News_Class
        Get
            If Session("AML_News.objNewsClass") Is Nothing Then
                Session("AML_News.objNewsClass") = New AML_News_BLL.AML_News_Class
            End If

            Return Session("AML_News.objNewsClass")
        End Get
        Set(value As AML_News_BLL.AML_News_Class)
            Session("AML_News.objNewsClass") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("AMLNews.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AMLNews.IDUnik") = value
        End Set
    End Property

    Private Sub AML_News_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                'Tampilkan notifikasi jika ada di bucket Pending Approval
                If AML_News_BLL.IsExistsInApproval(ObjModule.ModuleName, IDUnik) Then
                    LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."

                    Panelconfirmation.Hidden = False
                    fpMain.Hidden = True
                End If

                'Clear Session
                ClearSession()

                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If

                Load_News()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Load_News()
        objNewsClass = AML_News_BLL.getNewsClassByID(IDUnik)

        If objNewsClass IsNot Nothing Then
            With objNewsClass.objNews
                txtTitle.Value = .News_Title
                txtContent.Value = .News_Content
                txtContentShort.Value = .News_Content_Short
                txtValidDateFrom.Value = .ValidFrom
                txtValidDateTo.Value = .ValidTo
                If .IsPublic = True Then
                    chkIsPublic.Checked = True
                Else
                    chkIsPublic.Checked = False
                End If

                txttImage.Value = .News_ImageName
                txtAttachment.Value = .News_AttachmentName

                'txttImage.SetText(.News_ImageName)
            End With
        End If
    End Sub

    Private Sub ClearSession()
        objNewsClass = New AML_News_BLL.AML_News_Class
    End Sub

    Private Sub isDataValid()
        If txtTitle.Text.Length > 100 Then
            Throw New ApplicationException("Title Max Length 100 character")
        End If
        If txtContent.Text.Length > 8000 Then
            Throw New ApplicationException("Content Length Max 8000 character")
        End If
        If CDate(txtValidDateTo.Value) < CDate(txtValidDateFrom.Value) Then
            Throw New ApplicationException("Valid From tidak Melebihi Valid To")
        End If

    End Sub

    Private Function IsNoChanges() As Boolean
        Dim objNews_Class_Old = AML_News_BLL.getNewsClassByID(IDUnik)

        Dim total As Integer = 0
        If objNews_Class_Old IsNot Nothing Then
            With objNews_Class_Old.objNews
                If .News_Title = txtTitle.Value Then
                Else

                    total = total + 1
                End If

                Dim image2 As Byte() = .News_Image
                If txttImage.HasFile Then
                    If image2 IsNot Nothing AndAlso image2.SequenceEqual(txttImage.FileBytes) Then
                    Else
                        total = total + 1
                    End If
                Else
                    If .News_ImageName IsNot Nothing Then
                        If txttImage.Value = .News_ImageName Then
                        Else
                            total = total + 1
                        End If
                    Else
                        total = total + 1
                    End If

                End If
                image2 = Nothing

                Dim attach2 As Byte() = .News_Attachment
                If txtAttachment.HasFile Then
                    If attach2 IsNot Nothing AndAlso attach2.SequenceEqual(txtAttachment.FileBytes) Then
                    Else

                        total = total + 1
                    End If
                Else
                    If .News_AttachmentName IsNot Nothing Then
                        If txtAttachment.Value = .News_AttachmentName Then
                        Else
                            total = total + 1
                        End If
                    Else
                        total = total + 1
                    End If

                End If
                attach2 = Nothing

                If .News_Content = txtContent.Value Then
                Else

                    total = total + 1
                End If

                If .News_Content_Short = txtContentShort.Value Then
                Else

                    total = total + 1
                End If

                Dim cekispublic As String
                If chkIsPublic.Checked = True Then
                    cekispublic = "True"
                Else
                    cekispublic = "False"
                End If

                Dim ispublicstring As String
                If .IsPublic = 1 Or .IsPublic = True Then
                    ispublicstring = "True"
                Else
                    ispublicstring = "False"
                End If
                If cekispublic = ispublicstring Then
                Else
                    total = total + 1
                End If

                Dim tanggalform As String = CDate(.ValidFrom).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture)
                Dim tanggalformNow As String = CDate(txtValidDateFrom.Value).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture)
                If tanggalform = tanggalformNow Then
                Else
                    total = total + 1
                End If

                Dim tanggalto As String = CDate(.ValidTo).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture)
                Dim tanggaltoNow As String = CDate(txtValidDateTo.Value).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture)

                If tanggalto = tanggaltoNow Then
                Else
                    total = total + 1
                End If

                If total > 0 Then
                    total = 0
                    Return False
                Else
                    total = 0
                    Return True
                End If
            End With


        End If

    End Function

    'Protected Sub chkIsPublic_Change(sender As Object, e As EventArgs)
    '    Try
    '        If chkIsPublic.Checked = True Then
    '            objnews.IsPublic = True
    '        Else
    '            objnews.IsPublic = False
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub btnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            If CDate(txtValidDateTo.Value) < CDate(txtValidDateFrom.Value) Then
                Throw New ApplicationException("Valid From tidak Melebihi Valid To")
            End If
            If String.IsNullOrWhiteSpace(txtTitle.Value) Then
                Throw New ApplicationException(txtTitle.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrWhiteSpace(txtContent.Value) Then
                Throw New ApplicationException(txtContent.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrWhiteSpace(txtContentShort.Value) Then
                Throw New ApplicationException(txtContentShort.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(CDate(txtValidDateTo.Value)) Then
                Throw New ApplicationException(txtValidDateTo.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(CDate(txtValidDateFrom.Value)) Then
                Throw New ApplicationException(txtValidDateFrom.FieldLabel & " harus diisi.")
            End If

            isDataValid()

            'Collect data
            With objNewsClass.objNews
                .News_Title = txtTitle.Text
                'objnews.News_Content = txtContent.Text
                Dim Val = txtContent.RawValue.ToString.Replace("%3E%u200B%3C", "%3E%3C").Replace("+", "%2B")

                If String.IsNullOrEmpty(Val) Then
                    .News_Content = txtContent.Value
                Else
                    .News_Content = Server.UrlDecode(Val)
                End If
                .News_Content_Short = txtContentShort.Text
                .ValidFrom = txtValidDateFrom.Text
                .ValidTo = txtValidDateTo.Text
                .IsPublic = chkIsPublic.Checked

                If txttImage.HasFile Then
                    .News_ImageName = txttImage.FileName
                    .News_Image = txttImage.FileBytes
                End If

                If txtAttachment.HasFile Then
                    .News_AttachmentName = txtAttachment.FileName
                    .News_Attachment = txtAttachment.FileBytes
                End If
            End With


            If IsNoChanges() Then
                Throw New ApplicationException("There are no changes in data. Please edit data before save.")
            End If
            txtAttachment.Clear()

            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                AML_News_BLL.SaveEditTanpaApproval(objNewsClass, ObjModule)
                Panelconfirmation.Hidden = False
                fpMain.Hidden = True
                LblConfirmation.Text = "Data Successfully Updated"
            Else
                AML_News_BLL.SaveEditWithApproval(objNewsClass, ObjModule)
                Panelconfirmation.Hidden = False
                fpMain.Hidden = True
                LblConfirmation.Text = "Data Updated into Pending Approval"
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AML_News_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Update
    End Sub

    Private Sub AML_News_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleEdit = New NawaBLL.FormModuleEdit(fpMain, Panelconfirmation, LblConfirmation)
    End Sub
End Class
