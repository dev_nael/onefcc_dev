﻿Imports CasemanagementDAL
Imports CasemanagementBLL
Imports System.IO

Partial Class AML_AMLNews_AMLNews_Delete
    Inherits ParentPage

    Public objFormModuleDel As NawaBLL.FormModuleDelete
    Const PATH_TEMP_DIR As String = "~\temp\"

    Public Property objNewsClass As AML_News_BLL.AML_News_Class
        Get
            If Session("AMLNews_Delete.objNewsClass") Is Nothing Then
                Session("AMLNews_Delete.objNewsClass") = New AML_News_BLL.AML_News_Class
            End If

            Return Session("AMLNews_Delete.objNewsClass")
        End Get
        Set(value As AML_News_BLL.AML_News_Class)
            Session("AMLNews_Delete.objNewsClass") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("AMLNews.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AMLNews.IDUnik") = value
        End Set
    End Property


    Private Sub AMLNews_Delete_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                'Tampilkan notifikasi jika ada di bucket Pending Approval
                If AML_News_BLL.IsExistsInApproval(ObjModule.ModuleName, IDUnik) Then
                    LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."

                    Panelconfirmation.Hidden = False
                    fpMain.Hidden = True
                End If

                'Clear Session
                ClearSession()

                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If

                Load_News()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Load_News()
        objNewsClass = AML_News_BLL.getNewsClassByID(IDUnik)

        If objNewsClass IsNot Nothing Then
            With objNewsClass.objNews
                txtTitle.Value = .News_Title
                txtContent.Value = .News_Content
                txtContentShort.Value = .News_Content_Short
                txtValidDateFrom.Value = .ValidFrom
                txtValidDateTo.Value = .ValidTo
                If .IsPublic = True Then
                    chkIsPublic.Checked = True
                Else
                    chkIsPublic.Checked = False
                End If

                txtNewsImage.Value = .News_ImageName
                If .News_Image IsNot Nothing Then
                    imgNewsImage.Visible = True

                    'Load the image
                    'Create Directory if not exists
                    If Not Directory.Exists(Server.MapPath(PATH_TEMP_DIR)) Then
                        Directory.CreateDirectory(Server.MapPath(PATH_TEMP_DIR))
                    End If

                    'Filename and location for output template
                    Dim tempImage As String = "AML_News_" & .PK_AML_News_ID & "_" & .News_ImageName 'Guid.NewGuid.ToString & .News_ImageName
                    Dim objfileinfo = New FileInfo(Server.MapPath(PATH_TEMP_DIR & tempImage))
                    Dim data() As Byte = .News_Image
                    File.WriteAllBytes(objfileinfo.FullName, data)

                    imgNewsImage.ImageUrl = PATH_TEMP_DIR & tempImage
                Else
                    imgNewsImage.Visible = False
                End If

                txtNewsAttachment.Text = .News_AttachmentName
                If .News_Attachment IsNot Nothing Then
                    btnDownloadAttachment.Visible = False   'True
                Else
                    btnDownloadAttachment.Visible = False
                End If

                'txttImage.Value = .News_ImageName
                'txtAttachment.Value = .News_AttachmentName

                'txttImage.SetText(.News_ImageName)

                txtTitle.ReadOnly = True
                txtContent.ReadOnly = True
                txtContentShort.ReadOnly = True
                txtValidDateFrom.ReadOnly = True
                txtValidDateTo.ReadOnly = True
                chkIsPublic.ReadOnly = True


            End With
        End If
    End Sub

    Private Sub ClearSession()
        objNewsClass = New AML_News_BLL.AML_News_Class
    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AMLNews_Delete_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Delete
    End Sub

    Private Sub AMLNews_Delete_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleDel = New NawaBLL.FormModuleDelete(fpMain, Panelconfirmation, LblConfirmation)
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                AML_News_BLL.DeleteTanpaApproval(objNewsClass, ObjModule)
                Panelconfirmation.Hidden = False
                fpMain.Hidden = True
                LblConfirmation.Text = "Data Successfully Deleted from Database"
            Else
                AML_News_BLL.DeleteWithApproval(objNewsClass, ObjModule)
                Panelconfirmation.Hidden = False
                fpMain.Hidden = True
                LblConfirmation.Text = "Data Deleted into Pending Approval"
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnDownloadAttachment_Click()
        Try
            Response.Clear()
            Response.ClearHeaders()
            'Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("content-disposition", "attachment;filename=" & objNewsClass.objNews.News_AttachmentName)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "ContentType"
            Response.BinaryWrite(objNewsClass.objNews.News_Attachment)
            Response.End()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
