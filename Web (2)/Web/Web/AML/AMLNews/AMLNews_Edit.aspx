﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AMLNews_Edit.aspx.vb" Inherits="AML_AMLNews_AMLNews_Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        var onKeyUp = function (combo, e) {
            var v = combo.getRawValue();
            combo.store.filter(combo.TextField, new RegExp(v, "i"));
            combo.onLoad();
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel runat="server" ID="fpMain" BodyPadding="10" ButtonAlign="Center" Scrollable="Both" Title="AML News Edit">
        <Items>
            <ext:TextField ID="txtTitle" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Title" MaxLength="500" EnforceMaxLength="true" />
            <ext:HtmlEditor ID="txtContent" runat="server" FieldLabel="Content" AllowBlank="false" Height="200" AutoScroll="true" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true" FieldStyle="background-color:#ffe4c4 !important;" ></ext:HtmlEditor>
            <ext:TextArea ID="txtContentShort" runat="server"  AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Content Short" MaxLength="500" EnforceMaxLength="true" />
            <ext:FileUploadField ID="txttImage" runat="server" FieldLabel="Image" AnchorHorizontal="80%" ButtonOnly="false" FieldStyle="text-align:left;">
                <%--  <Listeners>
                    <Change Handler="#{LblFileReport}.setValue(#{FileReport}.value);"></Change>
                </Listeners>--%>
                <RightButtons>
                    <ext:Button ID="Button1" runat="server" Text="" Icon="Erase" ClientIDMode="Static">
                        <Listeners>
                            <Click Handler="#{txttImage}.reset();#{LblFileReport}.setValue(#{txttImage}.value);"></Click>
                        </Listeners>
                    </ext:Button>
                </RightButtons>
            </ext:FileUploadField>
            <ext:FileUploadField ID="txtAttachment" runat="server" FieldLabel="Attachment" AnchorHorizontal="80%"  >
                <%--  <Listeners>
                    <Change Handler="#{LblFileReport}.setValue(#{FileReport}.value);"></Change>
                </Listeners>--%>
                <RightButtons>
                    <ext:Button ID="btnClear" runat="server" Text="" Icon="Erase" ClientIDMode="Static">
                        <Listeners>
                            <Click Handler="#{txtAttachment}.reset();#{LblFileReport}.setValue(#{txtAttachment}.value);"></Click>
                        </Listeners>
                    </ext:Button>
                </RightButtons>
            </ext:FileUploadField>
            <ext:DateField ID="txtValidDateFrom" runat="server"  AnchorHorizontal="30%" AllowBlank="false" FieldLabel="Valid From"    Format="dd-MMM-yyyy" />
            <ext:DateField ID="txtValidDateTo" runat="server"  AnchorHorizontal="30%" AllowBlank="false" FieldLabel="Valid To"   Format="dd-MMM-yyyy"/>
            <ext:Checkbox ID="chkIsPublic"  runat="server" FieldLabel="Is Public">
                <DirectEvents>
                    <%--<Change OnEvent="chkIsPublic_Change"></Change>--%>
                </DirectEvents>
            </ext:Checkbox>
        </Items>
        <Buttons>
            <ext:Button runat="server" ID="btnSave" Text="Save" Icon="Disk">
                <DirectEvents>
                    <Click OnEvent="btnSave_Click">
                        <EventMask Msg="Processing..." MinDelay="500" ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancel" runat="server" Text="Cancel" Icon="Cancel">
                <Listeners>
                    <Click Handler="#{BtnCancel}.disable()" />
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="btnCancel_DirectEvent">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="500">
                        </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" runat="server" ClientIDMode="Static" Title="Confirmation" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
    <Defaults>
        <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
    </Defaults>
    <LayoutConfig>
        <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
    </LayoutConfig>
    <Items>
        <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel" Tex="aa">
        </ext:Label>
    </Items>

    <Buttons>
        <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
            <DirectEvents>
                <Click OnEvent="BtnConfirmation_DirectClick"></Click>
            </DirectEvents>
        </ext:Button>
    </Buttons>
</ext:FormPanel>
</asp:Content>



