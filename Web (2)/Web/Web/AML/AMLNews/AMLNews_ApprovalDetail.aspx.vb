﻿Imports CasemanagementDAL
Imports CasemanagementBLL

Partial Class AML_AMLNews_AMLNews_ApprovalDetail
    Inherits ParentPage
    Public objFormModuleApprovalDetail As NawaBLL.FormModuleApprovalDetail

    Private _IDReq As Long
    Const PATH_TEMP_DIR As String = "~\temp\"

    Public Property IDReq() As Long
        Get
            Return _IDReq
        End Get
        Set(ByVal value As Long)
            _IDReq = value
        End Set
    End Property

    Public Property objSchemaModule() As NawaDAL.Module
        Get
            Return Session("AMLNews_ApprovalDetail.objSchemaModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AMLNews_ApprovalDetail.objSchemaModule") = value
        End Set
    End Property

    Private _objApproval As NawaDAL.ModuleApproval
    Public Property ObjApproval() As NawaDAL.ModuleApproval
        Get
            Return _objApproval
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            _objApproval = value
        End Set
    End Property

    Private Sub AML_AMLNews_AMLNews_ApprovalDetail(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim strid As String = Request.Params("ID")
            Dim strModuleid As String = Request.Params("ModuleID")
            Try
                IDReq = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim moduleid As Integer = NawaBLL.Common.DecryptQueryString(strModuleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                objSchemaModule = NawaBLL.ModuleBLL.GetModuleByModuleID(moduleid)
            Catch ex As Exception
                Throw New Exception("Invalid ID Approval.")
            End Try


            If Not Ext.Net.X.IsAjaxRequest Then
                ObjApproval = NawaBLL.ModuleApprovalBLL.GetModuleApprovalByID(IDReq)
                If Not ObjApproval Is Nothing Then
                    PanelInfo.Title = "AML News Approval"
                    lblModuleName.Text = "AML News"
                    lblModuleKey.Text = ObjApproval.ModuleKey
                    lblAction.Text = NawaBLL.ModuleBLL.GetModuleActionNamebyID(ObjApproval.PK_ModuleAction_ID)
                    LblCreatedBy.Text = ObjApproval.CreatedBy
                    lblCreatedDate.Text = ObjApproval.CreatedDate.Value.ToString("dd-MMM-yyyy")
                End If

                Select Case ObjApproval.PK_ModuleAction_ID
                    Case NawaBLL.Common.ModuleActionEnum.Insert
                        Dim unikkey As String = Guid.NewGuid.ToString

                        AML_News_BLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkey)

                        FormPanelOld.Visible = False
                    Case NawaBLL.Common.ModuleActionEnum.Delete
                        Dim unikkeyNew As String = Guid.NewGuid.ToString

                        'Dim objNewMGroupAccess As New NawaDevBLL.UserTestBLL
                        AML_News_BLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkeyNew)

                        FormPanelOld.Visible = False
                    Case NawaBLL.Common.ModuleActionEnum.Update
                        Dim unikkeyOld As String = Guid.NewGuid.ToString
                        Dim unikkeyNew As String = Guid.NewGuid.ToString

                        Dim objNewsClass As AML_News_BLL.AML_News_Class = NawaBLL.Common.Deserialize(ObjApproval.ModuleField, GetType(AML_News_BLL.AML_News_Class))
                        Dim objNews As AML_News = objNewsClass.objNews
                        Dim objNewsClass_Old As AML_News_BLL.AML_News_Class = NawaBLL.Common.Deserialize(ObjApproval.ModuleFieldBefore, GetType(AML_News_BLL.AML_News_Class))
                        Dim objNewsOld As AML_News = objNewsClass_Old.objNews

                        'Dim objNewMGroupAccess As New NawaDevBLL.UserTestBLL
                        AML_News_BLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkeyNew)
                        AML_News_BLL.LoadPanel(FormPanelOld, ObjApproval.ModuleFieldBefore, ObjApproval.ModuleName, unikkeyOld)
                        AML_News_BLL.SettingColor(FormPanelOld, FormPanelNew, unikkeyOld, unikkeyNew)

                    Case NawaBLL.Common.ModuleActionEnum.Activation
                        Dim unikkeyNew As String = Guid.NewGuid.ToString
                        'Dim objNewMGroupAccess As New NawaDevBLL.UserTestBLL
                        AML_News_BLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkeyNew)

                        FormPanelOld.Visible = False
                End Select
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSave_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            AML_News_BLL.Accept(Me.IDReq)
            LblConfirmation.Text = "Data Approved. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnReject_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            AML_News_BLL.Reject(Me.IDReq)
            LblConfirmation.Text = "Data Rejected. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objSchemaModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs) Handles BtnConfirmation.DirectClick
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objSchemaModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Private Sub AML_AMLNews_AMLNews_Detail_Init(sender As Object, e As EventArgs) Handles Me.Init
        ActionType = NawaBLL.Common.ModuleActionEnum.Approval
    End Sub

    Private Sub AML_AMLNews_AMLNews_Detail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        objFormModuleApprovalDetail = New NawaBLL.FormModuleApprovalDetail()
    End Sub


End Class
