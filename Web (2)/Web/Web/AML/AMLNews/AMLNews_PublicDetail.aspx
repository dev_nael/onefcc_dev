﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AMLNews_PublicDetail.aspx.vb" Inherits="AMLNews_PublicDetail" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   
    <title>AML News</title>

  

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../../styles/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../styles/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../styles/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../styles/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../../styles/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <style>
        .btn-paging {height:50px !important; background-color:darkred !important; border-radius:10px !important;}
        .suggestcard{
            /* Frame 15 */


/* Auto Layout */

display: flex;
flex-direction: column;
align-items: flex-start;
padding: 0px;

position: static;
width: 880px;
height: 242px;
left: 0px;
top: 1019px;


/* Inside Auto Layout */

flex: none;
order: 1;
flex-grow: 0;
margin: 60px 0
        }
        .suggestcardinside{
            /* Frame 13 */


/* Auto Layout */

display: flex;
flex-direction: row;
align-items: flex-start;
padding: 0px;

position: static;
width: 880px;
height: 175px;
left: 0px;
top: 67px;


/* Inside Auto Layout */

flex: none;
order: 2;
flex-grow: 0;
margin: 20px 0px;
        }
        .cardimagesuggest{
            /* Frame 14 */


position: static;
width: 202px;
height: 175px;
left: 678px;
top: 0px;


/* Inside Auto Layout */

flex: none;
order: 3;
flex-grow: 0;
margin: 0px 24px;
        }

        .textsuggest{
            /* Frame 10 */


/* Auto Layout */

display: flex;
flex-direction: row;
align-items: flex-start;
padding: 8px;

position: absolute;
width: 174px;
height: 80px;
left: 14px;
top: 95px;

/* border */

background: #EEEEEE;
        }

        .imagecard{
/* image 14 */


position: absolute;
width: 202px;
height: 131px;
left: 0px;
top: 0px;

background: url(image.png);
        }
     </style>
</head>
<body class="hold-transition <%--login-page--%>">
    <form id="form1" runat="server" autocomplete="off">
        <ext:ResourceManager ID="ResourceManager1" runat="server" Theme="Crisp" />

    <!-- news-box -->
    <div class="col-lg-12 col-sm-12" style="padding-left:200px;padding-right:200px;">
        <div class="row" style="padding:10px;margin-bottom:20px;margin-top:20px;">
            <div class="col-lg-12 col-sm-12">
                <%--<h3><i class="fa fa-newspaper-o"></i> AML News</h3>--%>
                <h5 Style="color:#465672 !important;"><%--<i class="fa fa-newspaper-o"></i> &nbsp;--%><asp:LinkButton ID="linkButtonLogin" runat="server"  CausesValidation="false"  Style="color:gray;" OnClick="linkButtonLogin_Click">LOGIN</asp:LinkButton>  &nbsp;>&nbsp;  <asp:LinkButton ID="linkButtonNews" runat="server"  CausesValidation="false"  Style="color:gray;" OnClick="linkButtonNews_Click">AML NEWS</asp:LinkButton>  &nbsp;>&nbsp;  <span style="color:#82171d;"><strong>ARTICLE</strong></span></h5>
            </div>
        </div>
        <div class="row" runat="server" id="rowNews" style="margin-top:20px">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <!-- begin post -->
                <asp:Repeater ID="rptNews" runat="server">
                    <ItemTemplate>
                        <div class="col-lg-12 col-sm-12">
                          
                                <div class="box-header no-margin no-pad-top"   >
                                     <div class="box-body no-pad-top" style="text-align:center;line-height: 2em;">
                                    <h3 style="color:#465672;font-weight:bold"><%# Eval("News_Title") %></h3>
                                         </div>
                                     <div class="row no-padding">
                                        <div class="col-lg-12 col-md-8 col-sm-8"  style="text-align:center">
                                            <strong style="color:#3266B4;font-size:16px;"><%# Eval("CreatedBy") %> - AML News</strong>
                                            <p style="font-size:12px;"><%# Eval("Dates") %></p>
                                        </div>
                                    </div>
                                    <div runat="server" visible='<%# Eval("News_Image").ToString.Trim.Length > 0 %>'>
                                        <img style="width:600px;height:auto; display: block; margin-left: auto; margin-right: auto; margin-top:10px; margin-bottom:10px;" src="<%# Eval("News_Image_URL") %>" alt="News Picture">
                                        </div>

                                </div>
                                <div class="box-body no-pad-top" style="text-align:justify;line-height: 2em;">
                                    <%# Eval("News_Content") %>
                                </div>
                            
        
            <!-- /.box-header -->
     
         
              <ul class="mailbox-attachments clearfix pull-right" runat="server" visible='<%# Eval("News_AttachmentName").ToString.Trim.Length > 0 %>'>
                <li>
                  
                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <%# Eval("News_AttachmentName") %></a>
                        <span class="mailbox-attachment-size">
                          <%# Eval("size_file") %>
                               <a  class="pull-right">
                          <%--<a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-download"></i></a>--%>
                                              <asp:LinkButton ID="LinkButton2" runat="server"  class="btn btn-sm pull-right" Icon="DiskDownload" style="background-color:white;border-color:lightgrey" CausesValidation="false" CommandName="remove" CommandArgument='<%#Eval("PK_AML_News_ID") %>'><i class="fa fa-download pull-right"></i></asp:LinkButton>
                    <%--  <a>
                               <ext:Button runat="server" class="btn btn-sm" Icon="DiskDownload" style="background-color:white;border-color:lightgrey" CommandArgument='<%# Eval("PK_AML_News_ID") %>'>
                            <DirectEvents>
                                <Click OnEvent="BtnDownload_Click" >
                                    <EventMask ShowMask="true" Msg="Loading..." ></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>--%>
                          </a>
                             </span>
                    
                     
                  </div>
                </li>
             
              </ul>
        
       
                             
                            </div>
                       
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
         <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top:10px;margin-bottom:10px;border-bottom:1px solid lightgrey">
             <p style="color:#465672;font-size:15px;font-family:Arial">Suggestions</p>
             </div>
             <div class="col-lg-6 col-md-12 col-sm-12">
                <!-- begin post -->
                <asp:Repeater ID="rptNews2" runat="server">
                    <ItemTemplate>
                      <%--     <div class="col-lg-12 col-sm-12" style="width:200px" runat="server" visible='<%# Eval("News_Image").ToString.Trim.Length > 0 %>'>
                                        <div class="card-block">
        <img style="width:200px;height:200px;" src="<%# Eval("News_Image") %>" alt="News Picture">
                </div>
                            </div>--%>
                        <div class="col-lg-12 col-sm-12" style="margin-left:-20px;" runat="server" >
                             <div class="box box-solid">
                    <div class="box-body">
                  
                        <div class="media">
                            <%--<div class="media-left" runat="server" visible='<%# Eval("News_Image").ToString.Trim.Length > 0 %>'>
                               <img src="<%# Eval("News_Image") %>" alt="News Picture" class="media-object" style="width: auto;height: 100px;position: static;width: 75px;height: 75px;left: 0px;top: 0px;flex: none;order: 0;flex-grow: 0;margin: 0px 10px;border-radius: 50%;">
                            </div>--%>

                            <div class="media-left" runat="server" visible='<%# Not IsDBNull(Eval("News_Image_URL"))%>'>
                               <img src="<%# Eval("News_Image_URL") %>" class="media-object" style="position: static;width: 75px;height: 75px;left: 0px;top: 0px;flex: none;order: 0;flex-grow: 0;margin: 0px 10px;border-radius: 50%;">
                            </div>
                            <div class="media-left" runat="server" visible='<%# IsDBNull(Eval("News_Image_URL"))%>'>
                                <span class="info-box-icon bg-grey" style="width: 75px;height: 75px;left: 0px;top: 0px;flex: none;order: 0;flex-grow: 0;margin: 0px 10px;border-radius: 50%; font-size:30px; color:white;"><i class="fa fa-picture-o" style="padding-top:-10px;"></i></span>                            
                            </div>

                            <div class="media-body">
                                <div class="clearfix">
                                   
   <h4 class="text-primary" style="margin-top: 0">  <asp:LinkButton ID="LinkButton2" runat="server"  CommandName="remove" CommandArgument='<%#Eval("PK_AML_News_ID") %>' Style="color:#465672;"><%# Eval("News_Title") %></asp:LinkButton></h4>


                                   
                                    <span style="margin-bottom:0;color:grey;">
                                        <%--<%# Eval("CreatedBy") %> - <small class="text-muted"><%# Eval("Dates") %></small>--%>
                                        <%# Eval("Dates") %>
                                    </span>
                                     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                      
                   
                    </div>
                          <%--  <div class="box" style="height:201px">
                                <div class="box-header no-margin no-pad-top">
                                    <h4 class="text-primary"><%# Eval("News_Title") %></h4>
                                </div>
                                <div class="box-body no-pad-top"  style="height:100px">
                                    <%# Eval("News_Content_Short") %>
                                </div>
                                <div class="box-footer">
                                    <div class="row no-padding">
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <%# Eval("CreatedBy") %>
                                            <p class="text-muted"><small><%# Eval("Dates") %></small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 text-right" style="margin-left:-8px">
                                            <asp:LinkButton ID="lnkReadMore" runat="server" Text="Read more" style="background-color:darkred"  CausesValidation="false" CssClass="btn btn-success btn-xs" CommandName="remove" CommandArgument='<%#Eval("PK_AML_News_ID") %>'></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                                <div class="col-lg-12 col-sm-12" style="width:1150px" runat="server" visible='<%# Eval("News_Image").ToString.Trim.Length = 0 %>'>
                           
                            <div class="box" style="height:201px">
                                <div class="box-header no-margin no-pad-top">
                                    <h4 class="text-primary"><%# Eval("News_Title") %></h4>
                                </div>
                                <div class="box-body no-pad-top"  style="height:100px">
                                    <%# Eval("News_Content_Short") %>
                                </div>
                                <div class="box-footer">
                                    <div class="row no-padding">
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <%# Eval("CreatedBy") %>
                                            <p class="text-muted"><small><%# Eval("Dates") %></small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 text-right"  >
                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Read more" style="background-color:darkred" CausesValidation="false" CssClass="btn btn-success btn-xs" CommandName="remove" CommandArgument='<%#Eval("PK_AML_News_ID") %>'></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <!-- begin post -->
                <asp:Repeater ID="rptNews3" runat="server">
                    <ItemTemplate>
                      <%--     <div class="col-lg-12 col-sm-12" style="width:200px" runat="server" visible='<%# Eval("News_Image").ToString.Trim.Length > 0 %>'>
                                        <div class="card-block">
        <img style="width:200px;height:200px;" src="<%# Eval("News_Image") %>" alt="News Picture">
                </div>
                            </div>--%>
                        <div class="col-lg-12 col-sm-12" style="margin-left:-20px;" runat="server" >
                             <div class="box box-solid">
                    <div class="box-body">
                  
                        <div class="media">
                            <%--<div class="media-left" runat="server" visible='<%# Eval("News_Image").ToString.Trim.Length > 0 %>'>
                               <img src="<%# Eval("News_Image") %>" alt="News Picture" class="media-object" style="width: auto;height: 100px;position: static;width: 75px;height: 75px;left: 0px;top: 0px;flex: none;order: 0;flex-grow: 0;margin: 0px 10px;border-radius: 50%;">
                            </div>--%>

                            <div class="media-left" runat="server" visible='<%# Not IsDBNull(Eval("News_Image_URL"))%>'>
                               <img src="<%# Eval("News_Image_URL") %>" class="media-object" style="position: static;width: 75px;height: 75px;left: 0px;top: 0px;flex: none;order: 0;flex-grow: 0;margin: 0px 10px;border-radius: 50%;">
                            </div>
                            <div class="media-left" runat="server" visible='<%# IsDBNull(Eval("News_Image_URL"))%>'>
                                <span class="info-box-icon bg-grey" style="width: 75px;height: 75px;left: 0px;top: 0px;flex: none;order: 0;flex-grow: 0;margin: 0px 10px;border-radius: 50%; font-size:30px; color:white;"><i class="fa fa-picture-o" style="padding-top:-10px;"></i></span>                            
                            </div>

                            <div class="media-body">
                                <div class="clearfix">
                                   
   <h4 class="text-primary" style="margin-top: 0">  <asp:LinkButton ID="LinkButton2" runat="server"  CommandName="remove" CommandArgument='<%#Eval("PK_AML_News_ID") %>' Style="color:#465672;"><%# Eval("News_Title") %></asp:LinkButton></h4>


                                   
                                    <span style="margin-bottom:0;color:grey;">
                                        <%--<%# Eval("CreatedBy") %> - <small class="text-muted"><%# Eval("Dates") %></small>--%>
                                        <%# Eval("Dates") %>
                                    </span>
                                     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                      
                   
                    </div>
                          <%--  <div class="box" style="height:201px">
                                <div class="box-header no-margin no-pad-top">
                                    <h4 class="text-primary"><%# Eval("News_Title") %></h4>
                                </div>
                                <div class="box-body no-pad-top"  style="height:100px">
                                    <%# Eval("News_Content_Short") %>
                                </div>
                                <div class="box-footer">
                                    <div class="row no-padding">
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <%# Eval("CreatedBy") %>
                                            <p class="text-muted"><small><%# Eval("Dates") %></small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 text-right" style="margin-left:-8px">
                                            <asp:LinkButton ID="lnkReadMore" runat="server" Text="Read more" style="background-color:darkred"  CausesValidation="false" CssClass="btn btn-success btn-xs" CommandName="remove" CommandArgument='<%#Eval("PK_AML_News_ID") %>'></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                                <div class="col-lg-12 col-sm-12" style="width:1150px" runat="server" visible='<%# Eval("News_Image").ToString.Trim.Length = 0 %>'>
                           
                            <div class="box" style="height:201px">
                                <div class="box-header no-margin no-pad-top">
                                    <h4 class="text-primary"><%# Eval("News_Title") %></h4>
                                </div>
                                <div class="box-body no-pad-top"  style="height:100px">
                                    <%# Eval("News_Content_Short") %>
                                </div>
                                <div class="box-footer">
                                    <div class="row no-padding">
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <%# Eval("CreatedBy") %>
                                            <p class="text-muted"><small><%# Eval("Dates") %></small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 text-right"  >
                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Read more" style="background-color:darkred" CausesValidation="false" CssClass="btn btn-success btn-xs" CommandName="remove" CommandArgument='<%#Eval("PK_AML_News_ID") %>'></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        <div class="row" runat="server" id="rowMoreNews" hidden="hidden">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <ext:Panel runat="server" style="margin-left:15px;margin-bottom:40px">
                    <Items>
                        <ext:Button runat="server" class="btn btn-primary btn-sm" ID="btnBackHome" Icon="ApplicationHome" Text="Back to Login" style="background-color:#82171d;">
                            <DirectEvents>
                                <Click OnEvent="btnBackHome_Click">
                                    <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button runat="server" class="btn btn-success btn-sm" ID="btnBackNews" Icon="Newspaper" Text="Back to News" style="margin-left:10px;background-color:#82171d;">
                            <DirectEvents>
                                <Click OnEvent="btnBackNews_Click">
                                    <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Items>
                </ext:Panel>
            </div>
<%--            <div class="col-lg-8 col-md-8 col-sm-8 text-right" style="padding-right:30px;margin-top:-5px;">
                <asp:Repeater ID="rptPaging" runat="server" onitemcommand="rptPaging_ItemCommand">
                    <ItemTemplate>
                        <asp:LinkButton CssClass="btn-paging" ID="lnkPage" style="padding:8px; margin:2px; background:#B34C00; border:solid 1px #666; color:#fff; font-weight:bold"
                            CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                            runat="server" Font-Bold="True"><%# Container.DataItem %>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:Repeater>
            </div>--%>
        </div>

    </div>
    <!-- /.news-box -->
    </form>


    <!-- jQuery 2.2.3 -->
    <script src="../../styles/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../styles/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="../../styles/plugins/iCheck/icheck.min.js"></script>

</body>
</html>
