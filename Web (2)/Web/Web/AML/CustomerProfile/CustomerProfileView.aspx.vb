﻿
Partial Class CustomerProfileView
    Inherits ParentPage
    Public Property IsFromCustomerProfiling As String
        Get
            Return Session("CustomerProfileView.IsFromCustomerProfiling")
        End Get
        Set(ByVal value As String)
            Session("CustomerProfileView.IsFromCustomerProfiling") = value
        End Set
    End Property
    Private Sub CustomerProfileView_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        Me.ActionType = NawaBLL.Common.ModuleActionEnum.view
    End Sub

    Private Sub CustomerProfileView_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            IsFromCustomerProfiling = Nothing
            IsFromCustomerProfiling = Request.Params("IsCP")
            Dim intModuleid As Integer


            If Not IsPostBack Then
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                NDSGridView.ModuleID = intModuleid
                NDSGridView.NDsQuery = "select DISTINCT CIFNo, CUSTOMERNAME as [Customer Name], PLACEOFBIRTH as [Place of Birth], DATEOFBIRTH FROM vw_AML_CUSTOMER"
                NDSGridView.NDSPrimarykeyField = "CIFNo"
                NDSGridView.NDsNotShowInView = ""
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class
