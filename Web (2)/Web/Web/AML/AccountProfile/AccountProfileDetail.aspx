﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AccountProfileDetail.aspx.vb" Inherits="AccountProfileDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <ext:FormPanel ID="RootPanel" runat="server" Title="Customer Info Detail" Layout="FitLayout" ButtonAlign="Center" AnchorHorizontal="90%" BodyPadding="5" AutoScroll="true">
        <LayoutConfig>
            <ext:VBoxLayoutConfig Align="Stretch"  />
        </LayoutConfig>
        <Items>
         
        </Items>
        <Buttons>
            <ext:Button ID="btnBack" runat="server" Icon="pageback" Text="Back" >
               
                <DirectEvents>
                    <Click OnEvent="btnback_Click">
                         <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                 
                </DirectEvents>
                
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>

