﻿
Partial Class AccountProfileDetail
    Inherits ParentPage

    Public Property FormCase As String
        Get
            Return Session("AccountProfileDetail.FormCase")
        End Get
        Set(ByVal value As String)
            Session("AccountProfileDetail.FormCase") = value
        End Set
    End Property
    Public Property ModuleCaseses As NawaDAL.Module
        Get
            Return Session("AccountProfileDetail.ModuleCaseses")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AccountProfileDetail.ModuleCaseses") = value
        End Set
    End Property
    Public Property ModuleidCase As String
        Get
            Return Session("AccountProfileDetail.ModuleidCase")
        End Get
        Set(ByVal value As String)
            Session("AccountProfileDetail.ModuleidCase") = value
        End Set
    End Property
    Public Property IDUnikCaseses As String
        Get
            Return Session("AccountProfileDetail.IDUnikCaseses")
        End Get
        Set(ByVal value As String)
            Session("AccountProfileDetail.IDUnikCaseses") = value
        End Set
    End Property

    Public Property ModuleidCustomerProfiling As String
        Get
            Return Session("AccountProfileDetail.ModuleidCustomerProfiling")
        End Get
        Set(ByVal value As String)
            Session("AccountProfileDetail.ModuleidCustomerProfiling") = value
        End Set
    End Property
    Public Property ObjModuleCustomerProfiling As NawaDAL.Module
        Get
            Return Session("AccountProfileDetail.ObjModuleCustomerProfiling")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AccountProfileDetail.ObjModuleCustomerProfiling") = value
        End Set
    End Property
    Public Property IsFromCustomerProfiling As String
        Get
            Return Session("CustomerInformationView.IsFromCustomerProfiling")
        End Get
        Set(ByVal value As String)
            Session("CustomerInformationView.IsFromCustomerProfiling") = value
        End Set
    End Property
    Public Property IDCPDetail() As String
        Get
            Return Session("AccountProfileDetail.IDCPDetail")
        End Get
        Set(ByVal value As String)
            Session("AccountProfileDetail.IDCPDetail") = value
        End Set

    End Property


    Private Sub AccountProfileDetail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        Try
            Me.ActionType = NawaBLL.Common.ModuleActionEnum.Detail
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ClearSession()

    End Sub
    Private Sub AccountProfileDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()
                RootPanel.Title = ObjModule.ModuleLabel & " Detail"
            End If
            Dim ID As String = Request.Params("ID")
            Dim IDDecrypt As String = NawaBLL.Common.DecryptQueryString(ID, NawaBLL.SystemParameterBLL.GetEncriptionKey) 'Convert.ToInt64(strid)
            NDSCompBLL.NDSPanelBLL.ProcessNdsPanel(ObjModule.PK_Module_ID, RootPanel, IDDecrypt)

            IDCPDetail = ID

            FormCase = Request.Params("FormCase")
            ModuleidCase = Request.Params("ModuleCase")
            If ModuleidCase IsNot Nothing Then
                Dim Moduleiddec = NawaBLL.Common.DecryptQueryString(ModuleidCase, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                ModuleCaseses = NawaBLL.ModuleBLL.GetModuleByModuleID(Moduleiddec)
                IsFromCustomerProfiling = "1"

            End If
            IDUnikCaseses = Request.Params("IDUnikCase")
            ModuleidCustomerProfiling = Request.Params("ModuleID")
            ObjModuleCustomerProfiling = ObjModule
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AccountProfileDetail_Init(sender As Object, e As EventArgs) Handles Me.Init
        Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Public Sub btnback_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        If FormCase IsNot Nothing Then
            If FormCase.ToLower = "detail" Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ModuleCaseses.UrlDetail & "?ModuleID=" & ModuleidCase & "&ID=" & IDUnikCaseses)
            ElseIf FormCase.ToLower = "edit" Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ModuleCaseses.UrlEdit & "?ModuleID=" & ModuleidCase & "&ID=" & IDUnikCaseses)
            ElseIf FormCase.ToLower = "approval" Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ModuleCaseses.UrlApprovalDetail & "?ModuleID=" & ModuleidCase & "&ID=" & IDUnikCaseses)
            End If
        Else

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & ModuleidCustomerProfiling)
        End If
    End Sub
End Class
