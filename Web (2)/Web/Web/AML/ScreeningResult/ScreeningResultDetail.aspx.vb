﻿Imports System.Data
Imports System.Data.SqlClient
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDAL
Imports NawaDevBLL
Imports NawaDevDAL
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevBLL.GlobalReportFunctionBLL
Imports System.Text
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.IO

Public Module Extenders
    <System.Runtime.CompilerServices.Extension>
    Public Function ToDataTable(Of T)(collection As IEnumerable(Of T), tableName As String) As DataTable
        Dim tbl As DataTable = ToDataTable(collection)
        tbl.TableName = tableName
        Return tbl
    End Function

    <System.Runtime.CompilerServices.Extension>
    Public Function ToDataTable(Of T)(collection As IEnumerable(Of T)) As DataTable
        Dim dt As New DataTable()
        Dim tt As Type = GetType(T)
        Dim pia As PropertyInfo() = tt.GetProperties()
        'Create the columns in the DataTable
        For Each pi As PropertyInfo In pia
            Dim a =
            If(Nullable.GetUnderlyingType(pi.PropertyType), pi.PropertyType)
            dt.Columns.Add(pi.Name, If(Nullable.GetUnderlyingType(pi.PropertyType), pi.PropertyType))
            'If pi.PropertyType Is GetType(DateTime) Then

            '    ' pi.Columns.Add("columnName", TypeOf (datetime2)
            'Else

            'End If
        Next
        'Populate the table
        For Each item As T In collection
            Dim dr As DataRow = dt.NewRow()
            dr.BeginEdit()
            For Each pi As PropertyInfo In pia
                ' cek value apakah null?
                ' If pi.GetValue(item, Nothing) = Nothing Then
                If pi.GetValue(item, Nothing) Is Nothing Then
                    dr(pi.Name) = DBNull.Value
                Else
                    dr(pi.Name) = pi.GetValue(item, Nothing)
                End If

            Next
            dr.EndEdit()
            dt.Rows.Add(dr)
        Next
        Return dt
    End Function


End Module

Partial Class AML_ScreeningResult_ScreeningResultDetail
    Inherits Parent

#Region "Session"
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AML_ScreeningResult_ScreeningResultDetail.ObjModule") = value
        End Set
    End Property

    Public Property listjudgementitem As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.listjudgementitem")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL))
            Session("AML_ScreeningResult_ScreeningResultDetail.listjudgementitem") = value
        End Set
    End Property

    Public Property listjudgementitemchange As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.listjudgementitemchange")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL))
            Session("AML_ScreeningResult_ScreeningResultDetail.listjudgementitemchange") = value
        End Set
    End Property

    Public Property judgementheader As NawaDevDAL.AML_SCREENING_RESULT
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.judgementheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT)
            Session("AML_ScreeningResult_ScreeningResultDetail.judgementheader") = value
        End Set
    End Property
    Public Property judgementrequestforheader As NawaDevDAL.AML_SCREENING_REQUEST
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.judgementrequestforheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_REQUEST)
            Session("AML_ScreeningResult_ScreeningResultDetail.judgementrequestforheader") = value
        End Set
    End Property
    Public Property judgementdetail As NawaDevDAL.AML_SCREENING_RESULT_DETAIL
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.judgementdetail")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            Session("AML_ScreeningResult_ScreeningResultDetail.judgementdetail") = value
        End Set
    End Property
    Public Property judgementdetailchange As NawaDevDAL.AML_SCREENING_RESULT_DETAIL
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.judgementdetailchange")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            Session("AML_ScreeningResult_ScreeningResultDetail.judgementdetailchange") = value
        End Set
    End Property
    Public Property checkerjudgeitems As NawaDevDAL.AML_SCREENING_RESULT_DETAIL
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.checkerjudgeitems")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            Session("AML_ScreeningResult_ScreeningResultDetail.checkerjudgeitems") = value
        End Set
    End Property
    Public Property indexofjudgementitems As Integer
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.indexofjudgementitems")
        End Get
        Set(ByVal value As Integer)
            Session("AML_ScreeningResult_ScreeningResultDetail.indexofjudgementitems") = value
        End Set
    End Property

    Public Property indexofjudgementitemschange As Integer
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.indexofjudgementitemschange")
        End Get
        Set(ByVal value As Integer)
            Session("AML_ScreeningResult_ScreeningResultDetail.indexofjudgementitemschange") = value
        End Set
    End Property
    Public Property dataID As String
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.dataID")
        End Get
        Set(ByVal value As String)
            Session("AML_ScreeningResult_ScreeningResultDetail.dataID") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_CLASS() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.objAML_WATCHLIST_CLASS")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AML_ScreeningResult_ScreeningResultDetail.objAML_WATCHLIST_CLASS") = value
        End Set
    End Property

    Public Property judgementClass() As NawaDevBLL.AMLJudgementClass
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.judgementClass")
        End Get
        Set(ByVal value As NawaDevBLL.AMLJudgementClass)
            Session("AML_ScreeningResult_ScreeningResultDetail.judgementClass") = value
        End Set
    End Property

    '' Add 10-Feb-2023
    Public Property IDModuleWatchlistEncrypted() As String
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.IDModuleWatchlistEncrypted")
        End Get
        Set(ByVal value As String)
            Session("AML_ScreeningResult_ScreeningResultDetail.IDModuleWatchlistEncrypted") = value
        End Set
    End Property

    Public Property IDWatchlistEncrypted() As String
        Get
            Return Session("AML_ScreeningResult_ScreeningResultDetail.IDWatchlistEncrypted")
        End Get
        Set(ByVal value As String)
            Session("AML_ScreeningResult_ScreeningResultDetail.IDWatchlistEncrypted") = value
        End Set
    End Property
    '' End 10-Feb-2023
#End Region

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()
                Dim strmodule As String = Request.Params("ModuleID")
                Dim intmodule As Integer = Common.DecryptQueryString(strmodule, SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)
                If ObjModule IsNot Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If
                Dim dataStr As String = Request.Params("ID")
                If dataStr IsNot Nothing Then
                    dataID = Common.DecryptQueryString(dataStr, SystemParameterBLL.GetEncriptionKey)
                End If
                If dataID IsNot Nothing Then
                    If AML_WATCHLIST_BLL.IsExistsInApproval(ObjModule.ModuleName, dataID) Then
                        LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."

                        Panelconfirmation.Hidden = False
                        PanelInfo.Hidden = True
                    End If
                    loaddata(dataID)
                Else
                    Throw New ApplicationException("An Error Occurred While Loading the Data With Id")
                End If
                loadcolumn()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ClearSession()
        judgementheader = New AML_SCREENING_RESULT
        listjudgementitem = New List(Of AML_SCREENING_RESULT_DETAIL)
        judgementrequestforheader = New AML_SCREENING_REQUEST
        listjudgementitemchange = New List(Of AML_SCREENING_RESULT_DETAIL)
        ClearOBJDetail()
    End Sub

    Private Sub ClearOBJDetail()
        judgementdetail = New AML_SCREENING_RESULT_DETAIL
        indexofjudgementitems = Nothing
        judgementdetailchange = New AML_SCREENING_RESULT_DETAIL
        indexofjudgementitemschange = Nothing
        objAML_WATCHLIST_CLASS = New AML_WATCHLIST_CLASS

        Name_Score.Clear()
        DoB_Score.Clear()
        Nationality_Score.Clear()
        Identity_Score.Clear()
        Total_Score.Clear()

        WatchListID.Clear()
        WatchListCategory.Clear()
        WatchListType.Clear()
        FullName.Clear()
        PlaceofBirth.Clear()
        DateofBirth.Clear()
        YearofBirth.Clear()
        Nationality.Clear()

        Remark1.Clear()
        Remark2.Clear()
        Remark3.Clear()
        Remark4.Clear()
        Remark5.Clear()

        RBJudgementMatch.Checked = False
        RBJudgementNotMatch.Checked = False
        JudgementComment.Clear()
    End Sub

    Private Sub loadcolumn()
        ColumnActionLocation(gridJudgementItem, CommandColumnGridJudgementItem)
    End Sub

    Private Sub loaddata(idheader As String)
        judgementheader = AMLJudgementBLL.GetAMLJudgementHeader(idheader)
        If Not IsDBNull(judgementheader.FK_AML_SCREENING_REQUEST_ID) Then
            judgementrequestforheader = AMLJudgementBLL.GetRequestForHeader(judgementheader.FK_AML_SCREENING_REQUEST_ID)
        End If
        If judgementrequestforheader IsNot Nothing Then
            If judgementrequestforheader.REQUEST_ID IsNot Nothing Then
                display_Request_ID.Text = judgementrequestforheader.REQUEST_ID
            End If
            If judgementrequestforheader.SOURCE IsNot Nothing Then
                display_Request_Source.Text = judgementrequestforheader.SOURCE
            End If
            If judgementrequestforheader.SERVICE IsNot Nothing Then
                display_Request_Service.Text = judgementrequestforheader.SERVICE
            End If
            If judgementrequestforheader.OPERATION IsNot Nothing Then
                display_Request_Operation.Text = judgementrequestforheader.OPERATION
            End If
            If judgementheader.RESPONSE_ID IsNot Nothing Then
                display_Response_ID.Text = judgementheader.RESPONSE_ID
            End If
            If judgementrequestforheader.CreatedDate IsNot Nothing Then
                display_Request_Date.Text = judgementrequestforheader.CreatedDate.Value.ToString("dd-MMM-yyyy, hh:mm:ss tt")
            End If
            If judgementheader.FK_AML_RESPONSE_CODE_ID IsNot Nothing Then
                If judgementheader.FK_AML_RESPONSE_CODE_ID = 1 Then
                    display_Response_Code.Text = "1 - Found"
                ElseIf judgementheader.FK_AML_RESPONSE_CODE_ID = 2 Then
                    display_Response_Code.Text = "2 - Not Found"
                Else
                    display_Response_Code.Text = "3 - Error"
                End If
            End If
            If judgementrequestforheader.RESPONSE_DESCRIPTION IsNot Nothing Then
                display_Response_Description.Text = judgementrequestforheader.RESPONSE_DESCRIPTION
            End If
            If judgementrequestforheader.RESPONSE_DATE IsNot Nothing Then
                display_Response_Date.Text = judgementrequestforheader.RESPONSE_DATE.Value.ToString("dd-MMM-yyyy, hh:mm:ss tt")
            End If
            If judgementrequestforheader.CIF_NO IsNot Nothing Then
                display_Request_CIF.Text = judgementrequestforheader.CIF_NO
            End If
            If judgementrequestforheader.NAME IsNot Nothing Then
                display_Request_Name.Text = judgementrequestforheader.NAME
            End If
            If judgementrequestforheader.DOB IsNot Nothing Then
                display_Request_DOB.Text = judgementrequestforheader.DOB.Value.Date.ToString("dd-MMM-yyyy")
            End If
            If judgementrequestforheader.NATIONALITY IsNot Nothing Then
                display_Request_Nationality.Text = judgementrequestforheader.NATIONALITY
            End If
            If judgementrequestforheader.IDENTITY_NUMBER IsNot Nothing Then
                display_Request_Identity_Number.Text = judgementrequestforheader.IDENTITY_NUMBER
            End If

            '31-May-2023 Adi : Jika CIF, tambah display Alias Name
            If Not String.IsNullOrEmpty(judgementrequestforheader.CIF_NO) Then
                Dim strQuery As String = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & judgementrequestforheader.CIF_NO & "'"
                Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
                If drCustomer IsNot Nothing Then
                    If Not IsDBNull(drCustomer("ALIAS")) AndAlso Not String.IsNullOrWhiteSpace(drCustomer("ALIAS")) Then
                        display_Request_Alias.Value = drCustomer("ALIAS")
                        display_Request_Alias.Hidden = False
                    End If
                End If
            Else
                display_Request_Alias.Hidden = True
            End If
            'End of 31-May-2023 Adi : Jika CIF, tambah display Alias Name

        End If
            listjudgementitem = AMLJudgementBLL.GetAMLJudgementList(idheader)
        If listjudgementitem IsNot Nothing Then
            BindJudgement(judgement_Item, listjudgementitem)
        End If
    End Sub

    Private Sub BindJudgement(store As Store, list As List(Of AML_SCREENING_RESULT_DETAIL))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_CATEGORY", GetType(String)))
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_TYPE", GetType(String)))
            objtable.Columns.Add(New DataColumn("ISMATCH", GetType(String)))

            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    If Not IsDBNull(item("FK_AML_WATCHLIST_CATEGORY_ID")) Then
                        item("AML_WATCHLIST_CATEGORY") = AMLJudgementBLL.GetWatchlistCategoryByID(item("FK_AML_WATCHLIST_CATEGORY_ID"))
                    Else
                        item("AML_WATCHLIST_CATEGORY") = ""
                    End If
                    If Not IsDBNull(item("FK_AML_WATCHLIST_TYPE_ID")) Then
                        item("AML_WATCHLIST_TYPE") = AMLJudgementBLL.GetWatchlistTypeByID(item("FK_AML_WATCHLIST_TYPE_ID"))
                    Else
                        item("AML_WATCHLIST_TYPE") = ""
                    End If

                    If Not IsDBNull(item("JUDGEMENT_ISMATCH")) Then
                        If item("JUDGEMENT_ISMATCH") Then
                            item("ISMATCH") = "Match"
                        Else
                            item("ISMATCH") = "Not Match"
                        End If
                    Else
                        item("ISMATCH") = ""
                    End If
                Next
            End If
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ALIAS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS)
        gridAliasInfo.GetStore().DataSource = objtable
        gridAliasInfo.GetStore().DataBind()
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ADDRESS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS)

        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
                If objCountry IsNot Nothing Then
                    item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
                Else
                    item("COUNTRY_NAME") = Nothing
                End If

                Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
                If objAddressType IsNot Nothing Then
                    item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
                Else
                    item("AML_ADDRESS_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gridAddressInfo.GetStore().DataSource = objtable
        gridAddressInfo.GetStore().DataBind()
    End Sub
    Protected Sub Bind_AML_WATCHLIST_IDENTITY()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY)

        objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
                If objIDENTITYType IsNot Nothing Then
                    item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
                Else
                    item("AML_IDENTITY_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gridIdentityInfo.GetStore().DataSource = objtable
        gridIdentityInfo.GetStore().DataBind()
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandJudgementItem(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                ClearOBJDetail()
                WindowJudge.Hidden = False
                loadobjectJudgement(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
            ElseIf e.ExtraParams(1).Value = "Delete" Then
            ElseIf e.ExtraParams(1).Value = "Edit" Then
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub loadobjectJudgement(id As String)
        judgementdetail = listjudgementitem.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = id).FirstOrDefault
        If judgementdetail IsNot Nothing Then
            indexofjudgementitems = listjudgementitem.IndexOf(judgementdetail)
            If judgementdetail.JUDGEMENT_ISMATCH IsNot Nothing Then
                If judgementdetail.JUDGEMENT_ISMATCH Then
                    RBJudgementMatch.Checked = True
                Else
                    RBJudgementNotMatch.Checked = True
                End If
            End If
            If judgementdetail.JUDGEMENT_COMMENT IsNot Nothing Then
                JudgementComment.Text = judgementdetail.JUDGEMENT_COMMENT
            End If
            If judgementdetail.MATCH_SCORE_NAME IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_NAME = 0 Then
                    Name_Score.Text = "-"
                Else
                    Name_Score.Text = judgementdetail.MATCH_SCORE_NAME
                End If
            End If
            If judgementdetail.MATCH_SCORE_DOB IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_DOB = 0 Then
                    DoB_Score.Text = "-"
                Else
                    DoB_Score.Text = judgementdetail.MATCH_SCORE_DOB
                End If
            End If
            If judgementdetail.MATCH_SCORE_NATIONALITY IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_NATIONALITY = 0 Then
                    Nationality_Score.Text = "-"
                Else
                    Nationality_Score.Text = judgementdetail.MATCH_SCORE_NATIONALITY
                End If
            End If
            If judgementdetail.MATCH_SCORE_IDENTITY_NUMBER IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_IDENTITY_NUMBER = 0 Then
                    Identity_Score.Text = "-"
                Else
                    Identity_Score.Text = judgementdetail.MATCH_SCORE_IDENTITY_NUMBER
                End If
            End If
            If judgementdetail.MATCH_SCORE IsNot Nothing Then
                If judgementdetail.MATCH_SCORE = 0 Then
                    Total_Score.Text = "-"
                Else
                    Total_Score.Text = judgementdetail.MATCH_SCORE
                End If
            End If

            objAML_WATCHLIST_CLASS = AML_WATCHLIST_BLL.GetWatchlistClassByID(judgementdetail.FK_AML_WATCHLIST_ID)
            If objAML_WATCHLIST_CLASS IsNot Nothing Then
                With objAML_WATCHLIST_CLASS.objAML_WATCHLIST
                    If objAML_WATCHLIST_CLASS.objAML_WATCHLIST IsNot Nothing Then
                        If Not IsDBNull(.PK_AML_WATCHLIST_ID) Then
                            WatchListID.Text = .PK_AML_WATCHLIST_ID
                        End If

                        If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                            Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                            If objWatchlistCategory IsNot Nothing Then
                                WatchListCategory.Text = objWatchlistCategory.CATEGORY_NAME
                            End If

                            '' Add 10-Feb-2023, Display Button Display Watchlist Worldcheck
                            Dim PkWatchlistWorldcheckCategory As String = ""

                            Dim strPkWatchlistWorldcheckCategory As String = "select ParameterValue"
                            strPkWatchlistWorldcheckCategory += " FROM AML_Global_Parameter a "
                            strPkWatchlistWorldcheckCategory += " where PK_GlobalReportParameter_ID = '14'"

                            PkWatchlistWorldcheckCategory = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strPkWatchlistWorldcheckCategory, Nothing)

                            If PkWatchlistWorldcheckCategory IsNot Nothing Then
                                If .FK_AML_WATCHLIST_CATEGORY_ID = PkWatchlistWorldcheckCategory Then
                                    btn_OpenWindowWatchlist.Hidden = "false"

                                    Dim strIDCode As String = 16645 '' Module ID "AML_WATCHLIST_WORLDCHECK"
                                    IDModuleWatchlistEncrypted = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                                    Dim PkWatchlistWorldcheckID As String = ""

                                    Dim strPkWatchlistWorldcheckID As String = "select FK_AML_WATCHLIST_SOURCE_ID"
                                    strPkWatchlistWorldcheckID += " FROM AML_WATCHLIST a "
                                    strPkWatchlistWorldcheckID += " where PK_AML_WATCHLIST_ID = '" & .PK_AML_WATCHLIST_ID.ToString() & "'"

                                    PkWatchlistWorldcheckID = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strPkWatchlistWorldcheckID, Nothing)
                                    IDWatchlistEncrypted = Common.EncryptQueryString(PkWatchlistWorldcheckID, SystemParameterBLL.GetEncriptionKey)

                                    If IDModuleWatchlistEncrypted Is Nothing Or IDWatchlistEncrypted Is Nothing Then
                                        Throw New ApplicationException("Watchlist ID is not found.")
                                    End If
                                Else
                                    btn_OpenWindowWatchlist.Hidden = "true"
                                End If
                            End If
                            '' End 10-Feb-2023
                        End If
                        If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                            Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                            If objWatchlistType IsNot Nothing Then
                                WatchListType.Text = objWatchlistType.TYPE_NAME
                            End If
                        End If
                        If .NAME IsNot Nothing Then
                            FullName.Text = .NAME
                        End If
                        If .BIRTH_PLACE IsNot Nothing Then
                            PlaceofBirth.Text = .BIRTH_PLACE
                        End If
                        If .DATE_OF_BIRTH IsNot Nothing Then
                            DateofBirth.Text = .DATE_OF_BIRTH
                        End If
                        If .YOB IsNot Nothing Then
                            YearofBirth.Text = .YOB
                        End If
                        If .NATIONALITY IsNot Nothing Then
                            Nationality.Text = .NATIONALITY
                        End If
                        If .CUSTOM_REMARK_1 IsNot Nothing Then
                            Remark1.Text = .CUSTOM_REMARK_1
                        End If
                        If .CUSTOM_REMARK_2 IsNot Nothing Then
                            Remark2.Text = .CUSTOM_REMARK_2
                        End If
                        If .CUSTOM_REMARK_3 IsNot Nothing Then
                            Remark3.Text = .CUSTOM_REMARK_3
                        End If
                        If .CUSTOM_REMARK_4 IsNot Nothing Then
                            Remark4.Text = .CUSTOM_REMARK_4
                        End If
                        If .CUSTOM_REMARK_5 IsNot Nothing Then
                            Remark5.Text = .CUSTOM_REMARK_5
                        End If
                    End If

                End With
                Bind_AML_WATCHLIST_ALIAS()
                Bind_AML_WATCHLIST_ADDRESS()
                Bind_AML_WATCHLIST_IDENTITY()
            End If
        End If
    End Sub

    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            judgementClass = New AMLJudgementClass
            With judgementClass
                .AMLJudgementHeader = judgementheader
                .AMLListResult = listjudgementitemchange
            End With
            checkerjudgeitems = New AML_SCREENING_RESULT_DETAIL
            checkerjudgeitems = listjudgementitem.Where(Function(x) x.JUDGEMENT_ISMATCH Is Nothing).FirstOrDefault
            If listjudgementitemchange.Count = 0 Then
                Throw New ApplicationException("No Data Has Changed, Please Change Some Data Before Saving or Click the Cancel Button to Cancel the Process")
            ElseIf checkerjudgeitems IsNot Nothing Then
                Throw New ApplicationException("There are Some Data which have not been Judged, Please Judge All Data Before Saving")
            End If
            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not ObjModule.IsUseApproval Then
                'tanpa approval
                AMLJudgementBLL.SaveJudgementTanpaApproval(judgementClass, ObjModule)
                PanelInfo.Hide()
                Panelconfirmation.Show()
                LblConfirmation.Text = "Data Saved into Database"
            Else
                'with approval
                AMLJudgementBLL.SaveJudgementApproval(judgementClass, ObjModule)
                PanelInfo.Hide()
                Panelconfirmation.Show()
                LblConfirmation.Text = "Data Saved into Pending Approval"
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub loadjudgementitemchange(items As AML_SCREENING_RESULT_DETAIL)
        judgementdetailchange = listjudgementitemchange.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = items.PK_AML_SCREENING_RESULT_DETAIL_ID).FirstOrDefault
        If judgementdetailchange IsNot Nothing Then
            indexofjudgementitemschange = listjudgementitemchange.IndexOf(judgementdetailchange)
            listjudgementitemchange(indexofjudgementitemschange) = items
        Else
            listjudgementitemchange.Add(items)
        End If
    End Sub

    'Protected Sub rgjudgement_click(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim objradio As Ext.Net.Radio = CType(sender, Ext.Net.Radio)
    '        If JudgementComment.Text Is Nothing Or JudgementComment.Text = "" Or JudgementComment.Text = "This Data is Judged Match" Or JudgementComment.Text = "This Data is Judged Not Match" Then
    '            If objradio.InputValue = "1" And objradio.Checked Then
    '                JudgementComment.Text = "This Data is Judged Match"
    '            ElseIf objradio.InputValue = "0" And objradio.Checked Then
    '                JudgementComment.Text = "This Data is Judged Not Match"
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub BtnJudgementSave_Click(sender As Object, e As DirectEventArgs)
        Try
            If RBJudgementMatch.Checked Or RBJudgementNotMatch.Checked Then
                If judgementdetail IsNot Nothing Then
                    If judgementdetail.JUDGEMENT_ISMATCH <> RBJudgementMatch.Checked Or judgementdetail.JUDGEMENT_COMMENT <> JudgementComment.Text Or judgementdetail.JUDGEMENT_ISMATCH Is Nothing Then
                        If RBJudgementMatch.Checked Then
                            judgementdetail.JUDGEMENT_ISMATCH = True
                        Else
                            judgementdetail.JUDGEMENT_ISMATCH = False
                        End If
                        If JudgementComment.Text IsNot Nothing Then
                            judgementdetail.JUDGEMENT_COMMENT = JudgementComment.Text
                        End If
                        If Common.SessionCurrentUser.UserName IsNot Nothing Then
                            judgementdetail.JUDGEMENT_BY = Common.SessionCurrentUser.UserID
                        End If
                        judgementdetail.JUDGEMENT_DATE = Now()
                        If judgementdetail.PK_AML_SCREENING_RESULT_DETAIL_ID <> Nothing And listjudgementitem IsNot Nothing Then
                            loadjudgementitemchange(judgementdetail)
                            listjudgementitem(indexofjudgementitems) = judgementdetail
                            BindJudgement(judgement_Item, listjudgementitem)
                        Else
                            Throw New ApplicationException("Something Wrong While Saving Data")
                        End If
                    Else
                        Throw New ApplicationException("No Data Has Changed, Please Change Some Data Before Saving or Click the Cancel Button to Cancel the Process")
                    End If
                End If
            Else
                Throw New ApplicationException("Must Conduct Judgment Before Saving Data")
            End If
            WindowJudge.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnJudgementCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowJudge.Hidden = True
            ClearOBJDetail()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#Region "10-Feb-2023 , Felix. Tambah button Display Watchlist Worldcheck di Detail"
    Protected Sub btn_OpenWindowWatchlist_Click()
        Try
            WindowDisplayWatchlist.Hidden = False
            pnlContent.ClearContent()
            pnlContent.AnimCollapse = False
            pnlContent.Loader.SuspendScripting()
            pnlContent.Loader.Url = "~/Parameter/ParameterDetail.aspx?ID=" & IDWatchlistEncrypted & "&ModuleID=" & IDModuleWatchlistEncrypted & ""
            pnlContent.Loader.Params.Clear()
            pnlContent.LoadContent()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Display_Watchlist_Back_Click()
        Try
            WindowDisplayWatchlist.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class
