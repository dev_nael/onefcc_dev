﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="ScreeningResultDetail.aspx.vb" Inherits="AML_ScreeningResult_ScreeningResultDetail" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel ID="PanelInfo" runat="server" Title="Screening History" BodyStyle="padding:10px"  ButtonAlign="Center" Scrollable="Both">
        <Items>
            <ext:FormPanel ID="FormRequest" runat="server" Collapsible="true" BodyPadding="10" Title="Screening Request">
                <Items>
                    <ext:Panel runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="display_Request_ID" runat="server" FieldLabel="Request ID">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Source" runat="server" FieldLabel="Source">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Service" runat="server" FieldLabel="Service">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Operation" runat="server" FieldLabel="Operation">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="display_Request_CIF" runat="server" FieldLabel="CIF">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Name" runat="server" FieldLabel="Name">
                                    </ext:DisplayField>

                                    <%-- 31-May-2023 Adi : Tambah display Alias Name jika CIF --%>
                                    <ext:DisplayField ID="display_Request_Alias" runat="server" FieldLabel="Alias Name" Hidden="true">
                                    </ext:DisplayField>
                                    <%-- End of 31-May-2023 Adi : Tambah display Alias Name jika CIF --%>

                                    <ext:DisplayField ID="display_Request_DOB" runat="server" FieldLabel="Date Of Birth">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Nationality" runat="server" FieldLabel="Nationality">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Identity_Number" runat="server" FieldLabel="Identity Number">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
            <ext:FormPanel ID="FormResponse" runat="server" Collapsible="true" BodyPadding="10" Title="Screening Result">
                <Items>
                    <ext:Panel runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="display_Response_ID" runat="server" FieldLabel="Response ID">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Date" runat="server" FieldLabel="Request Date">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Code" runat="server" FieldLabel="Response Code">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Description" runat="server" FieldLabel="Response Description">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Date" runat="server" FieldLabel="Response Date">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                            <%--<ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="display_Response_Operation" runat="server" FieldLabel="Operation">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Code" runat="server" FieldLabel="Response Code">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Description" runat="server" FieldLabel="Response Description">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>--%>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
            <ext:GridPanel ID="gridJudgementItem" runat="server" MarginSpec="0 0 0 0">
                <Store>
                    <ext:Store ID="judgement_Item" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" IDProperty="PK_AML_SCREENING_RESULT_DETAIL_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_SCREENING_RESULT_DETAIL_ID" Type="Int"></ext:ModelField>
                                    <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NAME_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="AML_WATCHLIST_CATEGORY" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="AML_WATCHLIST_TYPE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="ISMATCH" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                        <ext:Column runat="server" Text="Name" DataIndex="NAME" CellWrap="true" flex="1" />
                        <ext:Column runat="server" Text="Name Watch List" DataIndex="NAME_WATCHLIST" CellWrap="true" flex="2"/>
                        <ext:Column runat="server" Text="Source Watch List" DataIndex="AML_WATCHLIST_CATEGORY" CellWrap="true" flex="1" />
                        <ext:Column runat="server" Text="Is Critical?" DataIndex="AML_WATCHLIST_TYPE" CellWrap="true" flex="1" />
                        <ext:NumberColumn Format="#,##0.00 %" runat="server" Text="Similarity (%)" DataIndex="MATCH_SCORE" CellWrap="true" flex="1"/>
                        <ext:Column runat="server" Text="Judgement Result" DataIndex="ISMATCH" CellWrap="true" flex="1" Hidden="true" />

                        <ext:CommandColumn ID="CommandColumnGridJudgementItem" runat="server" Text="Action" CellWrap="true">
                            <Commands>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridcommandJudgementItem">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btnSaveReport" runat="server" Icon="Disk" Text="Save" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="BtnSave_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancelReport" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:Window ID="WindowJudge" Title="Result Detail" runat="server" Modal="true" Maximizable="true" Hidden="true" BodyStyle="padding:20px" Scrollable="Both" ButtonAlign="Center" Width="1000" Height="500">
        <Items>
            <ext:FormPanel ID="ScoreDetail" runat="server" Collapsible="true" BodyPadding="10" Title="Search Score Detail">
                <Items>
                    <ext:DisplayField ID="Name_Score" runat="server" FieldLabel="Name Score">
                    </ext:DisplayField>
                    <ext:DisplayField ID="DoB_Score" runat="server" FieldLabel="Date of Birth Score">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Nationality_Score" runat="server" FieldLabel="Nationality Score">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Identity_Score" runat="server" FieldLabel="Identity Number Score">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Total_Score" runat="server" FieldLabel="Total Score">
                    </ext:DisplayField>
                </Items>
            </ext:FormPanel>
            
            <ext:FormPanel ID="GeneralInformation" runat="server" Collapsible="true" BodyPadding="10" Title="General Information">
                <Items>
                    <ext:DisplayField ID="WatchListID" runat="server" FieldLabel="Watch List ID">
                    </ext:DisplayField>
                    <ext:Panel runat="server" Layout="ColumnLayout" AnchorHorizontal="80%" ID="Panel1" Border="false">
                        <Items>
                            <ext:Panel runat="server" ColumnWidth="0.6" Border="false" Layout="AnchorLayout">
                                <Content>
                                    <ext:DisplayField ID="WatchListCategory" runat="server" FieldLabel="Watch List Category">
                                    </ext:DisplayField>
                                </Content>
                            </ext:Panel>
                            <ext:Panel runat="server" ColumnWidth="0.4" Border="false" Layout="AnchorLayout">
                                <Content>
                                    <ext:Button ID="btn_OpenWindowWatchlist" runat="server" Icon="ApplicationViewDetail" Text="Data Watchlist Worldcheck" MarginSpec="0 10 10 0" Hidden="true">
                                        <DirectEvents>
                                            <Click OnEvent="btn_OpenWindowWatchlist_Click">
                                                <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Content>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                    <%--Edit 10-Feb-2023--%>
                    <%--<ext:DisplayField ID="WatchListCategory" runat="server" FieldLabel="Watch List Category">
                    </ext:DisplayField>--%>
                    <%--End 10-Feb-2023--%>
                    <ext:DisplayField ID="WatchListType" runat="server" FieldLabel="Watch List Type">
                    </ext:DisplayField>
                    <ext:DisplayField ID="FullName" runat="server" FieldLabel="Full Name">
                    </ext:DisplayField>
                    <ext:DisplayField ID="PlaceofBirth" runat="server" FieldLabel="Place of Birth">
                    </ext:DisplayField>
                    <ext:DisplayField ID="DateofBirth" runat="server" FieldLabel="Date of Birth">
                    </ext:DisplayField>
                    <ext:DisplayField ID="YearofBirth" runat="server" FieldLabel="Year of Birth">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Nationality" runat="server" FieldLabel="Nationality">
                    </ext:DisplayField>
                    <%-- Watch List Alias --%>
                    <ext:GridPanel ID="gridAliasInfo" runat="server" Title="Alias(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <Store>
                            <ext:Store ID="Store_gridAliasInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ALIAS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ALIAS_NAME" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column8" runat="server" DataIndex="ALIAS_NAME" Text="Alias Name" MinWidth="300" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Alias --%>

                    <%-- Watch List Address --%>
                    <ext:GridPanel ID="gridAddressInfo" runat="server" Title="Address(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <Store>
                            <ext:Store ID="Store_gridAddressInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ADDRESS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_AML_COUNTRY_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="POSTAL_CODE" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column9" runat="server" DataIndex="AML_ADDRESS_TYPE_NAME" Text="Type" MinWidth="50"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="ADDRESS" Text="Address" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="FK_AML_COUNTRY_CODE" Text="Country Code" MinWidth="100" Hidden="true"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="COUNTRY_NAME" Text="Country Name" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="POSTAL_CODE" Text="Postal Code" MinWidth="100"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Address --%>

                    <%-- Watch List Identity --%>
                    <ext:GridPanel ID="gridIdentityInfo" runat="server" Title="Identity(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <Store>
                            <ext:Store ID="Store_gridIdentityInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_IDENTITY_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_IDENTITY_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDENTITYNUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column14" runat="server" DataIndex="AML_IDENTITY_TYPE_NAME" Text="Type" MinWidth="200" Flex="1"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="IDENTITYNUMBER" Text="Identity" MinWidth="300"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Identity --%>
                    <ext:DisplayField ID="Remark1" runat="server" FieldLabel="Remark 1">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark2" runat="server" FieldLabel="Remark 2">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark3" runat="server" FieldLabel="Remark 3">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark4" runat="server" FieldLabel="Remark 4">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark5" runat="server" FieldLabel="Remark 5">
                    </ext:DisplayField>
                </Items>
            </ext:FormPanel>
            <ext:Panel ID="PanelJudge" runat="server" Layout="AnchorLayout" Hidden="true">
                <Items>
                    <ext:RadioGroup runat="server" ID="RGJudgement" AnchorHorizontal="50%" FieldLabel="Is Result Match ?" LabelWidth="150">
                        <Items>
                            <ext:Radio runat="server" ID="RBJudgementMatch" BoxLabel="Match" InputValue="1">
                                <%--<DirectEvents>
                                    <Change OnEvent="rgjudgement_click" />
                                </DirectEvents>--%>
                            </ext:Radio>

                            <ext:Radio runat="server" ID="RBJudgementNotMatch" BoxLabel="Not Match" InputValue="0">
                                <%--<DirectEvents>
                                    <Change OnEvent="rgjudgement_click" />
                                </DirectEvents>--%>
                            </ext:Radio>
                        </Items>
                    </ext:RadioGroup>
                    <ext:TextArea runat="server" ID="JudgementComment" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="1000" LabelWidth="150" EnforceMaxLength="true"/>
                </Items>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="BtnJudgementSave" runat="server" Icon="Disk" Text="Save Result" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="BtnJudgementSave_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnJudgementCancel" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnJudgementCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%--Add 10-Feb-2023--%>
    <ext:Window runat="server" ID="WindowDisplayWatchlist" Title="Watchlist Information" Layout="AnchorLayout" ButtonAlign="Center" Hidden="true" Closable="false" BodyPadding="10" Maximized="true" Maximizable="true">
        <Items>
            <ext:Panel ID="pnlContent" runat="server" BodyPadding="0"  Height ="450">
                <Loader Mode="Frame" NoCache="true" runat="server" AutoLoad="false" >
                    <LoadMask ShowMask="true"></LoadMask>
                    <%--<Params>
                        <ext:Parameter Name="fileName" Mode="Raw" Value="0">
                        </ext:Parameter>
                    </Params>--%>
                </Loader>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Display_Watchlist_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_Display_Watchlist_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%--End 10-Feb-2023--%>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>

        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>



