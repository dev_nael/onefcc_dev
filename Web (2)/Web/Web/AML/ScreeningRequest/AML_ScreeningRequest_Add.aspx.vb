﻿Imports Ext
Imports NawaDevBLL
Imports NawaDevDAL
Imports Elmah
Imports System.Data
Imports NawaBLL
Imports System.Data.SqlClient
Imports System.Net.Http '' added 13 Apr 2021
Imports System.Threading.Tasks
Imports System.Timers
Imports System.Diagnostics
Imports OfficeOpenXml '' Added 21 May 2021
Imports System.IO '' Added 21 May 2021
Imports System.Net
Imports System.Security.Authentication

Partial Class AML_SCREENING_ADHOC
    Inherits ParentPage

    Private strQuery As String
    Public objFormModuleView As FormModuleView '' Added 21 May 2021

    Public Property IDModule() As String
        Get
            Return Session("AML_SCREENING_ADHOC.IDModule")
        End Get
        Set(ByVal value As String)
            Session("AML_SCREENING_ADHOC.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("AML_SCREENING_ADHOC.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AML_SCREENING_ADHOC.IDUnik") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_CLASS() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AML_SCREENING_ADHOC.objAML_WATCHLIST_CLASS")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AML_SCREENING_ADHOC.objAML_WATCHLIST_CLASS") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_ALIAS_Edit() As NawaDevDAL.AML_WATCHLIST_ALIAS
        Get
            Return Session("AML_SCREENING_ADHOC.objAML_WATCHLIST_ALIAS_Edit")
        End Get
        Set(ByVal value As NawaDevDAL.AML_WATCHLIST_ALIAS)
            Session("AML_SCREENING_ADHOC.objAML_WATCHLIST_ALIAS_Edit") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_ADDRESS_Edit() As NawaDevDAL.AML_WATCHLIST_ADDRESS
        Get
            Return Session("AML_SCREENING_ADHOC.objAML_WATCHLIST_ADDRESS_Edit")
        End Get
        Set(ByVal value As NawaDevDAL.AML_WATCHLIST_ADDRESS)
            Session("AML_SCREENING_ADHOC.objAML_WATCHLIST_ADDRESS_Edit") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_IDENTITY_Edit() As NawaDevDAL.AML_WATCHLIST_IDENTITY
        Get
            Return Session("AML_SCREENING_ADHOC.objAML_WATCHLIST_IDENTITY_Edit")
        End Get
        Set(ByVal value As NawaDevDAL.AML_WATCHLIST_IDENTITY)
            Session("AML_SCREENING_ADHOC.objAML_WATCHLIST_IDENTITY_Edit") = value
        End Set
    End Property

    'Public Property objAML_SCREENING_ADHOC_Edit() As AML_SCREENING_ADHOC_RESULT
    '    Get
    '        Return Session("AML_SCREENING_ADHOC.objAML_SCREENING_ADHOC_Edit")
    '    End Get
    '    Set(ByVal value As AML_SCREENING_ADHOC_RESULT)
    '        Session("AML_SCREENING_ADHOC.objAML_SCREENING_ADHOC_Edit") = value
    '    End Set
    'End Property

    Public Property ID_AML_Watchlist As String
        Get
            Return Session("AML_SCREENING_ADHOC.ID_AML_Watchlist")
        End Get
        Set(value As String)
            Session("AML_SCREENING_ADHOC.ID_AML_Watchlist") = value
        End Set
    End Property

    Public Property objdt_SEARCH_RESULT As DataTable
        Get
            Return Session("AML_SCREENING_ADHOC.objdt_SEARCH_RESULT")
        End Get
        Set(value As DataTable)
            Session("AML_SCREENING_ADHOC.objdt_SEARCH_RESULT") = value
        End Set
    End Property

    '' Added by Felix 13 Apr 2021
    Public Property objdt_SEARCH_RESULT_API As DataTable
        Get
            Return Session("AML_SCREENING_ADHOC.objdt_SEARCH_RESULT_API")
        End Get
        Set(value As DataTable)
            Session("AML_SCREENING_ADHOC.objdt_SEARCH_RESULT_API") = value
        End Set
    End Property

    Public Property requestID As String
        Get
            Return Session("AML_SCREENING_ADHOC.requestID")
        End Get
        Set(value As String)
            Session("AML_SCREENING_ADHOC.requestID") = value
        End Set
    End Property
    '' End of 13 Apr 2021

    '' Added by Felix 26 Mar 2021
    Public Property NAME_WEIGHT As Double
        Get
            Return Session("AML_SCREENING_ADHOC.NAME_WEIGHT")
        End Get
        Set(value As Double)
            Session("AML_SCREENING_ADHOC.NAME_WEIGHT") = value
        End Set
    End Property

    Public Property DOB_WEIGHT As Double
        Get
            Return Session("AML_SCREENING_ADHOC.DOB_WEIGHT")
        End Get
        Set(value As Double)
            Session("AML_SCREENING_ADHOC.DOB_WEIGHT") = value
        End Set
    End Property

    Public Property NATIONALITY_WEIGHT As Double
        Get
            Return Session("AML_SCREENING_ADHOC.NATIONALITY_WEIGHT")
        End Get
        Set(value As Double)
            Session("AML_SCREENING_ADHOC.NATIONALITY_WEIGHT") = value
        End Set
    End Property

    Public Property IDENTITY_WEIGHT As Double
        Get
            Return Session("AML_SCREENING_ADHOC.IDENTITY_WEIGHT")
        End Get
        Set(value As Double)
            Session("AML_SCREENING_ADHOC.IDENTITY_WEIGHT") = value
        End Set
    End Property
    '' End 26 Mar 2021

    Sub ClearSession()
        objAML_WATCHLIST_CLASS = New AML_WATCHLIST_CLASS

        objAML_WATCHLIST_ADDRESS_Edit = Nothing
        objAML_WATCHLIST_ALIAS_Edit = Nothing
        objAML_WATCHLIST_IDENTITY_Edit = Nothing
        IDWatchlistEncrypted = Nothing
        IDModuleWatchlistEncrypted = Nothing
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As Integer = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()

                FormPanelInput.Title = ObjModule.ModuleLabel

                SetCommandColumnLocation()

                '-- Added by Felix 26 Mar 2021
                'Dim NAME_WEIGHT As Double 'Decimal
                'Dim DOB_WEIGHT As Double 'Decimal
                'Dim NATIONALITY_WEIGHT As Double 'Decimal
                'Dim IDENTITY_WEIGHT As Double 'Decimal

                '-- nebeng pake data table ini
                Dim objdt_SEARCH_RESULT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select NAME_WEIGHT, DOB_WEIGHT,NATIONALITY_WEIGHT,IDENTITY_WEIGHT FROM AML_SCREENING_MATCH_PARAMETER", Nothing)
                If objdt_SEARCH_RESULT IsNot Nothing Then
                    For Each row As DataRow In objdt_SEARCH_RESULT.Rows
                        NAME_WEIGHT = row.Item("NAME_WEIGHT")
                        DOB_WEIGHT = row.Item("DOB_WEIGHT")
                        NATIONALITY_WEIGHT = row.Item("NATIONALITY_WEIGHT")
                        IDENTITY_WEIGHT = row.Item("IDENTITY_WEIGHT")
                    Next row
                End If
                '-- End of 26 Mar 2021

                '-- 3 Jun 2021 Adi -- Gridpanel disable button Detail
                Dim objcommandcol As Ext.Net.CommandColumn = gp_Screening_Result.ColumnModel.Columns.Find(Function(x) x.ID = "cc_Screening_Result")
                objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"
                '-- End of 3 Jun 2021 Adi -- Gridpanel disable button Detail

                txt_NAME_ALIAS_SEARCH.Hidden = True

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub



    '------- DOCUMENT SAVE
    Protected Sub btn_SCREENING_ADHOC_Save_Click()
        Try
            check_CIF_change()

            '29-Apr-2021 Adi : Insert ke EODLogSP untuk ukur performance
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Process Save Screening Result Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'If Not objdt_SEARCH_RESULT Is Nothing Then
            If Not objdt_SEARCH_RESULT_API Is Nothing Then
                Dim param(7) As SqlParameter
                param(0) = New SqlParameter
                param(0).ParameterName = "@userid"
                param(0).DbType = SqlDbType.VarChar
                param(0).Value = NawaBLL.Common.SessionCurrentUser.UserID

                param(1) = New SqlParameter
                param(1).ParameterName = "@TMP_udt_SEARCH_RESULT_ADHOC"
                param(1).SqlDbType = SqlDbType.Structured
                param(1).TypeName = "dbo.udt_SEARCH_RESULT_ADHOC"
                param(1).Value = objdt_SEARCH_RESULT_API

                param(2) = New SqlParameter
                param(2).ParameterName = "@Name_Search"
                param(2).SqlDbType = SqlDbType.VarChar
                param(2).Value = txt_NAME_SEARCH.Value

                param(3) = New SqlParameter
                param(3).ParameterName = "@DOB_Search"
                param(3).SqlDbType = SqlDbType.DateTime
                If CDate(txt_DATE_OF_BIRTH_SEARCH.Value) = DateTime.MinValue Then
                    param(3).Value = DBNull.Value
                Else
                    param(3).Value = CDate(txt_DATE_OF_BIRTH_SEARCH.Value)
                End If

                param(4) = New SqlParameter
                param(4).ParameterName = "@Nationality_Search"
                param(4).SqlDbType = SqlDbType.VarChar
                param(4).Value = txt_NATIONALITY_SEARCH.Value

                param(5) = New SqlParameter
                param(5).ParameterName = "@IdentityNumber_Search"
                param(5).SqlDbType = SqlDbType.VarChar
                param(5).Value = txt_IDENTITY_NUMBER_SEARCH.Value

                param(6) = New SqlParameter
                param(6).ParameterName = "@CIF_Search"
                param(6).SqlDbType = SqlDbType.VarChar
                param(6).Value = txt_CIF_SEARCH.Value

                param(7) = New SqlParameter
                param(7).ParameterName = "@requestID"
                param(7).SqlDbType = SqlDbType.VarChar
                param(7).Value = requestID

                'NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SAVE_SCREENING_ADHOC", param)
                'NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SAVE_SCREENING_ADHOC_NEW", param)
                '29-Apr-2021 Adi : ganti nama SP biar standard.
                NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Screening_Request_Adhoc_Save", param)

            Else
                Throw New ApplicationException("No data To save.")
            End If

            '29-Apr-2021 Adi : Insert ke EODLogSP untuk ukur performance
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Process Save Screening Result Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            '' To Do not found, but save
            LblConfirmation.Text = "Data Saved into Database"
            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_Screening_Adhoc_Back_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF DOCUMENT SAVE

    Protected Sub btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SetCommandColumnLocation()
        ColumnActionLocation(gp_AML_WATCHLIST_ALIAS, cc_AML_WATCHLIST_ALIAS)
        ColumnActionLocation(gp_AML_WATCHLIST_ADDRESS, cc_AML_WATCHLIST_ADDRESS)
        ColumnActionLocation(gp_AML_WATCHLIST_IDENTITY, cc_AML_WATCHLIST_IDENTITY)

        ColumnActionLocation(gp_Screening_Result, cc_Screening_Result)
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AML_WATCHLIST_ADD_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.view
    End Sub
    Protected Sub btn_Screening_Search_Click(sender As Object, e As EventArgs)
        Try
            If String.IsNullOrEmpty(txt_NAME_SEARCH.Value) Then
                Throw New ApplicationException(txt_NAME_SEARCH.FieldLabel & " Is required.")
            End If

            If Not String.IsNullOrEmpty(txt_DATE_OF_BIRTH_SEARCH.Value) Then
                If CDate(txt_DATE_OF_BIRTH_SEARCH.Value) = DateTime.MinValue Then
                    txt_DATE_OF_BIRTH_SEARCH.Value = Nothing
                End If
            End If

            check_CIF_change()
            ScreeningAdhoc()
            Lock_Window_AML_SCREENING_ADHOC()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ScreeningAdhoc_20230302()
        Try

            'Dim objParam(4) As SqlParameter
            'objParam(0) = New SqlParameter
            'objParam(0).ParameterName = "@Name"
            'objParam(0).Value = txt_NAME_SEARCH.Text

            'objParam(1) = New SqlParameter
            'objParam(1).ParameterName = "@DOB"
            'If CDate(txt_DATE_OF_BIRTH_SEARCH.Value) = DateTime.MinValue Then
            '    objParam(1).Value = DBNull.Value
            'Else
            '    objParam(1).Value = CDate(txt_DATE_OF_BIRTH_SEARCH.Value)
            'End If

            'objParam(2) = New SqlParameter
            'objParam(2).ParameterName = "@Nationality"
            'objParam(2).Value = txt_NATIONALITY_SEARCH.Text

            'objParam(3) = New SqlParameter
            'objParam(3).ParameterName = "@IdentityNumber"
            'objParam(3).Value = txt_IDENTITY_NUMBER_SEARCH.Text

            'objParam(4) = New SqlParameter
            'objParam(4).ParameterName = "@UserID"
            'objParam(4).Value = NawaBLL.Common.SessionCurrentUser.UserID
            ''NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Screening_Adhoc", objParam)
            ''Dim objdt As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Screening_Adhoc", objParam)
            'objdt_SEARCH_RESULT = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Screening_Adhoc", objParam)

            'If Not objdt_SEARCH_RESULT Is Nothing Then
            '    If objdt_SEARCH_RESULT.Rows.Count <> 0 Then
            '        gp_Screening_Result.GetStore.DataSource = objdt_SEARCH_RESULT
            '        gp_Screening_Result.GetStore.DataBind()
            '        txt_RESPONSE_SEARCH.Value = "Found"
            '    Else
            '        txt_RESPONSE_SEARCH.Value = "Not Found"
            '    End If
            'End If

            '29-Apr-2021 Adi : Insert ke EODLogSP untuk ukur performance
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Insert to Screening Request Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            '' Added by Felix 13 Apr 2021
            Dim objParam(5) As SqlParameter
            objParam(0) = New SqlParameter
            objParam(0).ParameterName = "@Name"
            objParam(0).Value = txt_NAME_SEARCH.Text

            objParam(1) = New SqlParameter
            objParam(1).ParameterName = "@DOB"
            If CDate(txt_DATE_OF_BIRTH_SEARCH.Value) = DateTime.MinValue Then
                objParam(1).Value = DBNull.Value
            Else
                objParam(1).Value = CDate(txt_DATE_OF_BIRTH_SEARCH.Value)
            End If

            objParam(2) = New SqlParameter
            objParam(2).ParameterName = "@Nationality"
            objParam(2).Value = txt_NATIONALITY_SEARCH.Text

            objParam(3) = New SqlParameter
            objParam(3).ParameterName = "@IdentityNumber"
            objParam(3).Value = txt_IDENTITY_NUMBER_SEARCH.Text

            objParam(4) = New SqlParameter
            objParam(4).ParameterName = "@UserID"
            objParam(4).Value = NawaBLL.Common.SessionCurrentUser.UserID

            objParam(5) = New SqlParameter
            objParam(5).ParameterName = "@CIF"
            objParam(5).Value = txt_CIF_SEARCH.Value

            'requestID = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_ADHOC_REQUEST_INSERT", objParam)
            '29-Apr-2021 Adi : change nama SP biar standard
            requestID = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Screening_Request_Adhoc_Insert", objParam)

            '29-Apr-2021 Adi : Insert ke EODLogSP untuk ukur performance
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Insert to Screening Request Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


            '29-Apr-2021 Adi : Insert ke EODLogSP untuk ukur performance
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Process Screening call Python API Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim client As HttpClient = New HttpClient
            Dim Timer As Stopwatch = Stopwatch.StartNew()

            'Dim requestID As String = ""

            '17-Mei-2021 Adi : Get URL Python and Service Time Out from System Parameter
            'URL for Python API
            'http://goaml.southeastasia.cloudapp.azure.com:1300
            Dim strURL_Python_API As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID=8000", Nothing)
            Dim Minutetimeout As Int32 = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID=8002", Nothing)
            client.Timeout = TimeSpan.FromMinutes(Minutetimeout)
            '--------------------------------------------------------

            'Dim responseName As Task(Of String) = client.GetStringAsync("http://goaml.southeastasia.cloudapp.azure.com:1300/Screening?requestid=" + requestID + "&fieldtocompare=NAME")
            'Dim responseNationality As Task(Of String) = client.GetStringAsync("http://goaml.southeastasia.cloudapp.azure.com:1300/Screening?requestid=" + requestID + "&fieldtocompare=NATIONALITY")
            'Dim responseIdentity As Task(Of String) = client.GetStringAsync("http://goaml.southeastasia.cloudapp.azure.com:1300/Screening?requestid=" + requestID + "&fieldtocompare=IDENTITY_NUMBER")

            Dim responseName As Task(Of String) = client.GetStringAsync(strURL_Python_API & "/Screening?requestid=" & requestID & "&fieldtocompare=NAME")
            Dim responseNationality As Task(Of String) = client.GetStringAsync(strURL_Python_API & "/Screening?requestid=" & requestID & "&fieldtocompare=NATIONALITY")
            Dim responseIdentity As Task(Of String) = client.GetStringAsync(strURL_Python_API & "/Screening?requestid=" & requestID & "&fieldtocompare=IDENTITY_NUMBER")

            Task.WaitAll(responseName, responseNationality, responseIdentity)

            Dim resultCodeName As String = responseName.Result.Replace("""", "").Trim()
            Dim resultCodeNationality As String = responseNationality.Result.Replace("""", "").Trim()
            Dim resultCodeIdentity As String = responseIdentity.Result.Replace("""", "").Trim()

            'Timer.Stop()
            Dim timespanResult As TimeSpan = Timer.Elapsed
            'Timer.Restart()

            '29-Apr-2021 Adi : Insert ke EODLogSP untuk ukur performance
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Process Screening call Python API Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


            '29-Apr-2021 Adi : Insert ke EODLogSP untuk ukur performance
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Process Calculate Screening Result Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim objParamAPI(3) As SqlParameter
            objParamAPI(0) = New SqlParameter
            objParamAPI(0).ParameterName = "@requestID"
            objParamAPI(0).Value = requestID

            objParamAPI(1) = New SqlParameter
            objParamAPI(1).ParameterName = "@resultCodeName"
            objParamAPI(1).Value = resultCodeName

            objParamAPI(2) = New SqlParameter
            objParamAPI(2).ParameterName = "@resultCodeNationality"
            objParamAPI(2).Value = resultCodeNationality

            objParamAPI(3) = New SqlParameter
            objParamAPI(3).ParameterName = "@resultCodeIdentity"
            objParamAPI(3).Value = resultCodeIdentity

            'Dim query As String = "SELECT ISNULL(ISNULL(SCREENING_NAME.LEFT_SIDE_INDEX, SCREENING_NATIONALITY.LEFT_SIDE_INDEX), SCREENING_IDENTITY_NUMBER.LEFT_SIDE_INDEX) LEFT_SIDE_INDEX,
            '	   ISNULL(ISNULL(SCREENING_NAME.RIGHT_SIDE_INDEX, SCREENING_NATIONALITY.RIGHT_SIDE_INDEX), SCREENING_IDENTITY_NUMBER.RIGHT_SIDE_INDEX) RIGHT_SIDE_INDEX,
            '	   ISNULL(SCREENING_NAME.SIMILARITY, 0) NAME_SIMILARITY,
            '	   ISNULL(SCREENING_NATIONALITY.SIMILARITY, 0) NATIONALITY_SIMILARITY,
            '	   ISNULL(SCREENING_IDENTITY_NUMBER.SIMILARITY, 0) IDENTITY_NUMBER_SIMILARITY
            'FROM (
            '		SELECT * FROM AML_SCREENING_RESULT_API
            '		WHERE RESULT_CODE = '" & resultCodeName & "'
            '	 ) SCREENING_NAME
            '	 FULL JOIN (
            '		SELECT * FROM AML_SCREENING_RESULT_API
            '		WHERE RESULT_CODE = '" & resultCodeNationality & "'
            '	 ) SCREENING_NATIONALITY
            '	 ON SCREENING_NAME.LEFT_SIDE_INDEX = SCREENING_NATIONALITY.LEFT_SIDE_INDEX
            '		AND SCREENING_NAME.RIGHT_SIDE_INDEX = SCREENING_NATIONALITY.RIGHT_SIDE_INDEX
            '	 FULL JOIN (
            '		SELECT * FROM AML_SCREENING_RESULT_API
            '		WHERE RESULT_CODE = '" & resultCodeIdentity & "'
            '	 ) SCREENING_IDENTITY_NUMBER
            '	 ON ISNULL(SCREENING_NAME.LEFT_SIDE_INDEX, SCREENING_NATIONALITY.LEFT_SIDE_INDEX) = SCREENING_IDENTITY_NUMBER.LEFT_SIDE_INDEX
            '		AND ISNULL(SCREENING_NAME.RIGHT_SIDE_INDEX, SCREENING_NATIONALITY.RIGHT_SIDE_INDEX) = SCREENING_IDENTITY_NUMBER.RIGHT_SIDE_INDEX
            '	 JOIN AML_SCREENING_REQUEST
            '	 ON ISNULL(ISNULL(SCREENING_NAME.LEFT_SIDE_INDEX, SCREENING_NATIONALITY.LEFT_SIDE_INDEX), SCREENING_IDENTITY_NUMBER.LEFT_SIDE_INDEX) = PK_AML_SCREENING_REQUEST_ID
            '	 JOIN AML_WATCHLIST_SCREENING
            '	 ON ISNULL(ISNULL(SCREENING_NAME.RIGHT_SIDE_INDEX, SCREENING_NATIONALITY.RIGHT_SIDE_INDEX), SCREENING_IDENTITY_NUMBER.RIGHT_SIDE_INDEX) = PK_AML_WATCHLIST_SCREENING_ID"

            '29-Apr-2021 Adi : ubah nama SP biar standard
            'objdt_SEARCH_RESULT_API = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Screening_ADHOC_API", objParamAPI)
            objdt_SEARCH_RESULT_API = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Screening_Request_Adhoc_Calculate", objParamAPI)

            '29-Apr-2021 Adi : Insert ke EODLogSP untuk ukur performance
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Process Calculate Screening Result Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Timer.[Stop]()
            Dim timespanSelect As TimeSpan = Timer.Elapsed
            '' End of Felix

            If Not objdt_SEARCH_RESULT_API Is Nothing Then
                If objdt_SEARCH_RESULT_API.Rows.Count <> 0 Then
                    '18-Apr-2021 Adi : Tambah column untuk Match Score Percentage
                    Dim objtable As Data.DataTable = objdt_SEARCH_RESULT_API

                    'Edited by Felix 20 Apr 2021
                    'objtable.Columns.Add(New DataColumn("Match_Score_Pct", GetType(Double)))
                    'For Each item As Data.DataRow In objtable.Rows
                    '    item("Match_Score_Pct") = CDbl(item("Match_Score")) * 100
                    'Next
                    '------------------------------------------------------------

                    gp_Screening_Result.GetStore.DataSource = objtable  'objdt_SEARCH_RESULT_API
                    gp_Screening_Result.GetStore.DataBind()
                    txt_RESPONSE_SEARCH.Value = "Found ( " & Math.Round(timespanSelect.TotalSeconds, 2) & " s )"
                    'txt_RESPONSE_SEARCH.Value = "Found ( " & Math.Round(timespanResult.TotalSeconds, 2) & " s )"

                    BtnExport.Hidden = False

                    '5-Jan-2023 Adi : Add Export PDF
                    btn_PreviewPDF.Hidden = False
                Else
                    '19-Apr-2021 Felix : Tambah column juga untuk yg not Match, biar udt nya ga error pas save
                    'Dim objtable As Data.DataTable = objdt_SEARCH_RESULT_API 'Edited by Felix 20 Apr 2021
                    'objtable.Columns.Add(New DataColumn("Match_Score_Pct", GetType(Double))) 'Edited by Felix 20 Apr 2021
                    txt_RESPONSE_SEARCH.Value = "Not Found ( " & Math.Round(timespanSelect.TotalSeconds, 2) & " s )"
                    'txt_RESPONSE_SEARCH.Value = "Not Found ( " & Math.Round(timespanResult.TotalSeconds, 2) & " s )"
                End If
            End If



        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

            txt_RESPONSE_SEARCH.Value = ex.Message
        End Try
    End Sub

#Region "21-Feb-2023 Adi : Ubah konsep call python end point ScreeningAPI"
    Sub ScreeningAdhoc()
        Try

            'Get Parameters
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Get Parameters Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'URL for Python API
            'http://goaml.southeastasia.cloudapp.azure.com:1300
            Dim strURL_Python_API As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID=8000", Nothing)

            'Max Response Time Python API (seconds)
            Dim intMaxResponseTime As Integer = Convert.ToInt32(NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID=8009", Nothing))

            'Retry Count if In Progress Refresh Matrix
            Dim intPythonRetryCount As Integer = Convert.ToInt32(NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID=8015", Nothing))

            '15-Mar-2023 Adi : Tambah handler untuk connection yang cross server dengan SSL/TLS
            'HTTP Request using SSL/TLS
            Dim IsTLS As Integer = Convert.ToInt32(NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID=8018", Nothing))
            Dim handler = New HttpClientHandler
            'If strURL_Python_API.Contains("https") Then
            If IsTLS = 0 Then
                handler.SslProtocols = SslProtocols.Tls
            ElseIf IsTLS = 1 Then
                handler.SslProtocols = SslProtocols.Tls11
            ElseIf IsTLS = 2 Then
                handler.SslProtocols = SslProtocols.Tls12
            ElseIf IsTLS = 3 Then
                handler.SslProtocols = SslProtocols.Tls13
            ElseIf IsTLS = 4 Then
                handler.SslProtocols = SslProtocols.Ssl2
            ElseIf IsTLS = 5 Then
                handler.SslProtocols = SslProtocols.Ssl3
            ElseIf IsTLS = 6 Then
                handler.SslProtocols = SslProtocols.None
            ElseIf IsTLS = 7 Then
                handler.SslProtocols = SslProtocols.[Default]
            End If
            'End If
            'End of 15-Mar-2023 Adi : Tambah handler untuk connection yang cross server dengan SSL/TLS

            Dim client As New HttpClient(handler)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Get Parameters Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


            'Call Python Screening
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Call Python Screening started.')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            client.Timeout = TimeSpan.FromSeconds(intMaxResponseTime)
            Dim Timer As Stopwatch = Stopwatch.StartNew()

            Dim CIFToSearch As String = ""
            Dim NameToSearch As String = ""
            Dim DOBToSearch As String = ""
            Dim NationalityToSearch As String = ""
            Dim IdentityToSearch As String = ""

            If Not String.IsNullOrWhiteSpace(txt_CIF_SEARCH.Value) Then
                CIFToSearch = txt_CIF_SEARCH.Value
            End If
            If Not String.IsNullOrWhiteSpace(txt_NAME_SEARCH.Value) Then
                NameToSearch = txt_NAME_SEARCH.Value
            End If
            If Not txt_DATE_OF_BIRTH_SEARCH.SelectedDate = DateTime.MinValue Then
                DOBToSearch = txt_DATE_OF_BIRTH_SEARCH.SelectedDate.ToString("yyyy-MM-dd")
            End If
            If Not String.IsNullOrWhiteSpace(txt_NATIONALITY_SEARCH.Value) Then
                NationalityToSearch = txt_NATIONALITY_SEARCH.Value
            End If
            If Not String.IsNullOrWhiteSpace(txt_IDENTITY_NUMBER_SEARCH.Value) Then
                IdentityToSearch = txt_IDENTITY_NUMBER_SEARCH.Value
            End If

            requestID = Guid.NewGuid.ToString()

            Dim strURL As String = strURL_Python_API + "/ScreeningAPI?pkrequestid=1&requestid=" + requestID + "&name=" + NameToSearch + "&dob=" + DOBToSearch + "&nationality=" + NationalityToSearch + "&identitynumber=" + IdentityToSearch
            'Return Response

            Dim responseBody As String = ""
            For i As Integer = 1 To intPythonRetryCount
                Dim taskResponse As Task(Of HttpResponseMessage) = client.GetAsync(strURL)
                Task.WaitAll(taskResponse)

                responseBody = taskResponse.Result.Content.ReadAsStringAsync().Result

                If Not responseBody.Contains("In Progress Refresh Matrix") Then
                    Exit For
                End If
            Next

            If responseBody.Contains("In Progress Refresh Matrix") Then
                Throw New Exception("In Progress Refresh Matrix")
            End If

            '26-May-2023 Adi : Tambah screening Alias Name (temuan FAMA)
            Dim responseBody_Alias As String = ""
            If Not String.IsNullOrWhiteSpace(txt_CIF_SEARCH.Value) Then
                Dim AliasToSearch As String = txt_NAME_ALIAS_SEARCH.Value
                Dim strURL_Alias As String = strURL_Python_API + "/ScreeningAPI?pkrequestid=1&requestid=" + requestID + "&name=" + AliasToSearch + "&dob=" + DOBToSearch + "&nationality=" + NationalityToSearch + "&identitynumber=" + IdentityToSearch

                For i As Integer = 1 To intPythonRetryCount
                    Dim taskResponse_Alias As Task(Of HttpResponseMessage) = client.GetAsync(strURL_Alias)
                    Task.WaitAll(taskResponse_Alias)

                    responseBody_Alias = taskResponse_Alias.Result.Content.ReadAsStringAsync().Result

                    If Not responseBody_Alias.Contains("In Progress Refresh Matrix") Then
                        Exit For
                    End If
                Next

                If responseBody_Alias.Contains("In Progress Refresh Matrix") Then
                    Throw New Exception("In Progress Refresh Matrix")
                End If
            End If
            'End of 26-May-2023 Adi : Tambah screening Alias Name (temuan FAMA)


            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Call Python Screening finished.')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


            'Save Screening Results
            SaveScreeningResult(responseBody)

            '26-May-2023 Adi : Tambah screening Alias Name (temuan FAMA)
            If Not String.IsNullOrWhiteSpace(responseBody_Alias) Then
                SaveScreeningResult(responseBody_Alias)
            End If
            '26-May-2023 Adi : Tambah screening Alias Name (temuan FAMA)

            'Get Screening Results
            GetScreeningResult()

            Timer.Stop()
            Dim timespanSelect As TimeSpan = Timer.Elapsed

            If Not objdt_SEARCH_RESULT_API Is Nothing Then
                If objdt_SEARCH_RESULT_API.Rows.Count <> 0 Then
                    Dim objtable As Data.DataTable = objdt_SEARCH_RESULT_API

                    gp_Screening_Result.GetStore.DataSource = objtable  'objdt_SEARCH_RESULT_API
                    gp_Screening_Result.GetStore.DataBind()
                    txt_RESPONSE_SEARCH.Value = "Found ( " & Math.Round(timespanSelect.TotalSeconds, 2) & " s )"

                    BtnExport.Hidden = False
                    btn_PreviewPDF.Hidden = False
                Else
                    txt_RESPONSE_SEARCH.Value = "Not Found ( " & Math.Round(timespanSelect.TotalSeconds, 2) & " s )"
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

            txt_RESPONSE_SEARCH.Value = "Screening Error : " & ex.Message
        End Try
    End Sub

    Sub SaveScreeningResult(jsonData As String)
        Try
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Save screening result started.')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim objParam(9) As SqlParameter
            objParam(0) = New SqlParameter
            objParam(0).ParameterName = "@RequestID"
            objParam(0).Value = requestID

            objParam(1) = New SqlParameter
            objParam(1).ParameterName = "@Source"
            objParam(1).Value = "OneFCC APP"

            objParam(2) = New SqlParameter
            objParam(2).ParameterName = "@Service"
            objParam(2).Value = "Screening Adhoc"

            objParam(3) = New SqlParameter
            objParam(3).ParameterName = "@Operation"
            objParam(3).Value = "Watchlist Screening"

            objParam(4) = New SqlParameter
            objParam(4).ParameterName = "@CIF_NO"
            objParam(4).Value = IIf(Not String.IsNullOrWhiteSpace(txt_CIF_SEARCH.Value), txt_CIF_SEARCH.Value, Nothing)

            objParam(5) = New SqlParameter
            objParam(5).ParameterName = "@Name"
            objParam(5).Value = txt_NAME_SEARCH.Value

            objParam(6) = New SqlParameter
            objParam(6).ParameterName = "@DOB"
            objParam(6).Value = IIf(txt_DATE_OF_BIRTH_SEARCH.SelectedDate <> DateTime.MinValue, txt_DATE_OF_BIRTH_SEARCH.SelectedDate, Nothing)

            objParam(7) = New SqlParameter
            objParam(7).ParameterName = "@Nationality"
            objParam(7).Value = IIf(Not String.IsNullOrWhiteSpace(txt_NATIONALITY_SEARCH.Value), txt_NATIONALITY_SEARCH.Value, Nothing)

            objParam(8) = New SqlParameter
            objParam(8).ParameterName = "@IdentityNumber"
            objParam(8).Value = IIf(Not String.IsNullOrWhiteSpace(txt_IDENTITY_NUMBER_SEARCH.Value), txt_IDENTITY_NUMBER_SEARCH.Value, Nothing)

            objParam(9) = New SqlParameter
            objParam(9).ParameterName = "@json"
            objParam(9).Value = jsonData

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_SCREENING_REQUEST_SATUAN_SaveResult_API", objParam)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Save screening result finished.')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '26-May-2023 Adi : Tambah screening Alias Name (temuan FAMA)
    Sub SaveScreeningResult_Alias(jsonData As String)
        Try
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Save screening result started.')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim objParam(9) As SqlParameter
            objParam(0) = New SqlParameter
            objParam(0).ParameterName = "@RequestID"
            objParam(0).Value = requestID

            objParam(1) = New SqlParameter
            objParam(1).ParameterName = "@Source"
            objParam(1).Value = "OneFCC APP"

            objParam(2) = New SqlParameter
            objParam(2).ParameterName = "@Service"
            objParam(2).Value = "Screening Adhoc"

            objParam(3) = New SqlParameter
            objParam(3).ParameterName = "@Operation"
            objParam(3).Value = "Watchlist Screening"

            objParam(4) = New SqlParameter
            objParam(4).ParameterName = "@CIF_NO"
            objParam(4).Value = IIf(Not String.IsNullOrWhiteSpace(txt_CIF_SEARCH.Value), txt_CIF_SEARCH.Value, Nothing)

            objParam(5) = New SqlParameter
            objParam(5).ParameterName = "@Name"
            objParam(5).Value = txt_NAME_SEARCH.Value

            objParam(6) = New SqlParameter
            objParam(6).ParameterName = "@DOB"
            objParam(6).Value = IIf(txt_DATE_OF_BIRTH_SEARCH.SelectedDate <> DateTime.MinValue, txt_DATE_OF_BIRTH_SEARCH.SelectedDate, Nothing)

            objParam(7) = New SqlParameter
            objParam(7).ParameterName = "@Nationality"
            objParam(7).Value = IIf(Not String.IsNullOrWhiteSpace(txt_NATIONALITY_SEARCH.Value), txt_NATIONALITY_SEARCH.Value, Nothing)

            objParam(8) = New SqlParameter
            objParam(8).ParameterName = "@IdentityNumber"
            objParam(8).Value = IIf(Not String.IsNullOrWhiteSpace(txt_IDENTITY_NUMBER_SEARCH.Value), txt_IDENTITY_NUMBER_SEARCH.Value, Nothing)

            objParam(9) = New SqlParameter
            objParam(9).ParameterName = "@json"
            objParam(9).Value = jsonData

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_SCREENING_REQUEST_SATUAN_SaveResult_API", objParam)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Save screening result finished.')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'End of 26-May-2023 Adi : Tambah screening Alias Name (temuan FAMA)

    Sub GetScreeningResult()
        Try
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Get screening result started.')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim objParam(0) As SqlParameter
            objParam(0) = New SqlParameter
            objParam(0).ParameterName = "@RequestID"
            objParam(0).Value = requestID

            objdt_SEARCH_RESULT_API = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_SCREENING_ADHOC_GetResultByRequestID", objParam)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Screening Adhoc', 'Get screening result finished.')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Class PythonResult
        Public Property REQUEST_ID As String
        Public Property RESPONSE_ID As String
        Public Property PK_AML_SCREENING_REQUEST_ID As Long
        Public Property PK_AML_WATCHLIST_SCREENING_ID As Long
        Public Property MATCH_SCORE_NAME As Single
        Public Property MATCH_SCORE_DOB As Single
        Public Property MATCH_SCORE_NATIONALITY As Single
        Public Property MATCH_SCORE_IDENTITY_NUMBER As Single
        Public Property MATCH_SCORE As Single
        Public Property NAME As String
        Public Property DOB As String
        Public Property YOB As String
        Public Property NATIONALITY As String
        Public Property IDENTITY_NUMBER As String
        Public Property NAME_WATCHLIST As String
        Public Property DOB_WATCHLIST As String
        Public Property YOB_WATCHLIST As String
        Public Property NATIONALITY_WATCHLIST As String
        Public Property IDENTITY_NUMBER_WATCHLIST As String
        Public Property CATEGORY_NAME As String
        Public Property TYPE_NAME As String
    End Class

#End Region

    Protected Sub btn_Screening_Clear_Click(sender As Object, e As EventArgs)
        Try
            Clean_Window_AML_SCREENING_ADHOC()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    '----- If Value is changed, Clear txt_CIF_SEARCH
    'Protected Sub txt_NAME_SEARCH_OnChanged()
    '    txt_CIF_SEARCH.Value = Nothing
    'End Sub

    'Protected Sub txt_DATE_OF_BIRTH_SEARCH_OnChanged()
    '    txt_CIF_SEARCH.Value = Nothing
    'End Sub

    'Protected Sub txt_NATIONALITY_SEARCH_OnChanged()
    '    txt_CIF_SEARCH.Value = Nothing
    'End Sub

    'Protected Sub txt_IDENTITY_NUMBER_SEARCH_OnChanged()
    '    txt_CIF_SEARCH.Value = Nothing
    'End Sub
    '----- END If Value is changed, Clear txt_CIF_SEARCH

    Protected Sub Clean_Window_AML_SCREENING_ADHOC()
        'Clean fields
        txt_NAME_SEARCH.Value = Nothing
        txt_DATE_OF_BIRTH_SEARCH.Value = Nothing
        txt_NATIONALITY_SEARCH.Value = Nothing
        txt_IDENTITY_NUMBER_SEARCH.Value = Nothing
        txt_RESPONSE_SEARCH.Value = Nothing
        txt_CIF_SEARCH.Value = Nothing

        '29-May-2023 Adi : Tambahan untuk screening Alias Name
        txt_NAME_ALIAS_SEARCH.Value = Nothing
        txt_NAME_ALIAS_SEARCH.Hidden = True
        'End of 29-May-2023 Adi : Tambahan untuk screening Alias Name

        'Set fields' ReadOnly
        txt_NAME_SEARCH.ReadOnly = False
        txt_DATE_OF_BIRTH_SEARCH.ReadOnly = False
        txt_NATIONALITY_SEARCH.ReadOnly = False
        txt_IDENTITY_NUMBER_SEARCH.ReadOnly = False
        'Show Buttons
        btn_Screening_Search.Hidden = False
        btn_Search_By_CIF.Hidden = False

        objdt_SEARCH_RESULT = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "Select top 0 0 As FK_AML_WATCHLIST_ID ,'' as [Name] ,'' as Name_Watch_list ,1 as FK_AML_WATCHLIST_CATEGORY_ID ,'' as Watch_list_Category ,1 as FK_AML_WATCHLIST_TYPE_ID ,'' as WATCH_LIST_TYPE ,getdate() as Dob ,'' as Nationality ,'' as Identity_number ,cast( 99.9 as float) as Match_Score ,cast( 99.9 as float) as MATCH_SCORE_NAME ,cast( 99.9 as float) as MATCH_SCORE_DOB ,cast( 99.9 as float) as MATCH_SCORE_NATIONALITY ,cast( 99.9 as float) as MATCH_SCORE_IDENTITY_NUMBER ")
        gp_Screening_Result.GetStore.DataSource = objdt_SEARCH_RESULT
        gp_Screening_Result.GetStore.DataBind()

        'Added on 14 Apr 2021
        requestID = Nothing

        'Added on 17 May 2021
        btn_Detail_CIF.Hidden = True '' Added by Felix on 17 May 2021
        display_Request_CIF.Value = Nothing
        display_Request_Name.Value = Nothing
        display_Request_Kode_Cabang.Value = Nothing
        display_Request_Nama_Cabang.Value = Nothing
        display_Request_Kantor_Wilayah.Value = Nothing
        'display_Request_Identity_Number.Value = Nothing
        display_Request_NPWP.Value = Nothing
        display_Request_Tipe_Nasabah.Value = Nothing

        display_Request_Tempat_Lahir.Value = Nothing
        display_Request_DOB.Value = Nothing
        display_Request_Pekerjaan.Value = Nothing
        'display_Request_Alamat.Value = Nothing
        display_Request_Nationality.Value = Nothing
        display_Request_Waktu_Proses.Value = Nothing
        display_Request_Tujuan_Dana.Value = Nothing '' Edited by Felix 18 Jun 2021 Ganti Watchlist -> Tujuan Dana
        display_Request_Profil_Risiko_T24.Value = Nothing
        display_Request_Profil_Risiko_oneFCC.Value = Nothing

        BtnExport.Hidden = True '' Added by Felix on\

        '5-Jan-2023 Adi : Add export PDF
        btn_PreviewPDF.Hidden = True

    End Sub

    Protected Sub Lock_Window_AML_SCREENING_ADHOC()

        'Set fields' ReadOnly
        txt_NAME_SEARCH.ReadOnly = True
        txt_DATE_OF_BIRTH_SEARCH.ReadOnly = True
        txt_NATIONALITY_SEARCH.ReadOnly = True
        txt_IDENTITY_NUMBER_SEARCH.ReadOnly = True
        'Show Buttons
        btn_Screening_Search.Hidden = True
        btn_Search_By_CIF.Hidden = True

    End Sub

    Protected Sub gc_Screening_Result(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            ID_AML_Watchlist = strID
            If strAction = "Detail" Then

                DetailScreeningAdhoc(ID_AML_Watchlist, "Detail")
                'Parameter to Search
                '' Added By Felix 09 Apr 2021
                txt_MATCH_SCORE_NAME_SEARCH.Value = txt_NAME_SEARCH.Value
                If CDate(txt_DATE_OF_BIRTH_SEARCH.Value) = DateTime.MinValue Then
                    txt_MATCH_SCORE_DOB_SEARCH.Value = "-"
                Else
                    txt_MATCH_SCORE_DOB_SEARCH.Value = Convert.ToDateTime(txt_DATE_OF_BIRTH_SEARCH.Value).ToString("dd-MMM-yyyy")
                End If
                txt_MATCH_SCORE_Nationality_SEARCH.Value = IIf(String.IsNullOrEmpty(txt_NATIONALITY_SEARCH.Value), "-", txt_NATIONALITY_SEARCH.Value)
                txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH.Value = IIf(String.IsNullOrEmpty(txt_IDENTITY_NUMBER_SEARCH.Value), "-", txt_IDENTITY_NUMBER_SEARCH.Value)
                '' End of 09 Apr 2021

                'Screening Result
                txt_MATCH_SCORE_NAME_WATCHLIST.Value = e.ExtraParams(7).Value
                If e.ExtraParams(8).Value = "" Then
                    txt_MATCH_SCORE_DOB_WATCHLIST.Value = ""
                Else
                    txt_MATCH_SCORE_DOB_WATCHLIST.Value = Convert.ToDateTime(e.ExtraParams(8).Value).ToString("dd-MMM-yyyy")
                End If
                txt_MATCH_SCORE_Nationality_WATCHLIST.Value = e.ExtraParams(9).Value
                txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST.Value = e.ExtraParams(10).Value

                'Screening Match Score
                Dim dblMatchScoreName As Double = CDbl(e.ExtraParams(3).Value) * CDbl(NAME_WEIGHT / 100)
                Dim dblMatchScoreDOB As Double = CDbl(e.ExtraParams(4).Value) * CDbl(DOB_WEIGHT / 100)
                Dim dblMatchScoreNationality As Double = CDbl(e.ExtraParams(5).Value) * CDbl(NATIONALITY_WEIGHT / 100)
                Dim dblMatchScoreIdentity As Double = CDbl(e.ExtraParams(6).Value) * CDbl(IDENTITY_WEIGHT / 100)
                Dim dblMatchScoreTotal = dblMatchScoreName + dblMatchScoreDOB + dblMatchScoreNationality + dblMatchScoreIdentity

                Dim strMatchScoreName As String = NAME_WEIGHT.ToString() & " % x " & CDbl(e.ExtraParams(3).Value).ToString("#,##0.00") & " % = " & dblMatchScoreName.ToString("#,##0.00") & " %"
                Dim strMatchScoreDOB As String = DOB_WEIGHT.ToString() & " % x " & CDbl(e.ExtraParams(4).Value).ToString("#,##0.00") & " % = " & dblMatchScoreDOB.ToString("#,##0.00") & " %"
                Dim strMatchScoreNationality As String = NATIONALITY_WEIGHT.ToString() & " % x " & CDbl(e.ExtraParams(5).Value).ToString("#,##0.00") & " % = " & dblMatchScoreNationality.ToString("#,##0.00") & " %"
                Dim strMatchScoreIdentity As String = IDENTITY_WEIGHT.ToString() & " % x " & CDbl(e.ExtraParams(6).Value).ToString("#,##0.00") & " % = " & dblMatchScoreIdentity.ToString("#,##0.00") & " %"

                'Show the Formula so the user clear about the score
                Dim strDividend As String = dblMatchScoreName.ToString("#,##0.00")
                Dim strDivisor As String = NAME_WEIGHT.ToString()
                If Not CDate(txt_DATE_OF_BIRTH_SEARCH.Value) = DateTime.MinValue Then
                    strDividend = strDividend & " + " & dblMatchScoreDOB.ToString("#,##0.00")
                    strDivisor = strDivisor & " + " & DOB_WEIGHT.ToString()
                End If
                If Not String.IsNullOrEmpty(txt_NATIONALITY_SEARCH.Value) Then
                    strDividend = strDividend & " + " & dblMatchScoreNationality.ToString("#,##0.00")
                    strDivisor = strDivisor & " + " & NATIONALITY_WEIGHT.ToString()
                End If
                If Not String.IsNullOrEmpty(txt_IDENTITY_NUMBER_SEARCH.Value) Then
                    strDividend = strDividend & " + " & dblMatchScoreIdentity.ToString("#,##0.00")
                    strDivisor = strDivisor & " + " & IDENTITY_WEIGHT.ToString()
                End If
                strDividend = "( " & strDividend & " % )"
                strDivisor = "( " & strDivisor & " % )"

                txt_MATCH_SCORE_NAME.Value = strMatchScoreName
                txt_MATCH_SCORE_DOB.Value = IIf(CDate(txt_DATE_OF_BIRTH_SEARCH.Value) = DateTime.MinValue, "-", strMatchScoreDOB)
                txt_MATCH_SCORE_Nationality.Value = IIf(String.IsNullOrEmpty(txt_NATIONALITY_SEARCH.Value), "-", strMatchScoreNationality)
                txt_MATCH_SCORE_IDENTITY_NUMBER.Value = IIf(String.IsNullOrEmpty(txt_IDENTITY_NUMBER_SEARCH.Value), "-", strMatchScoreIdentity)
                'txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(e.ExtraParams(2).Value).ToString("#,##0.00 %")
                txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(e.ExtraParams(2).Value).ToString("#,##0.00") & " %"
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DetailScreeningAdhoc(ID As String, command As String)
        Try

            ID_AML_Watchlist = ID

            Window_WATCHLIST_GENERAL_INFORMATION.Hidden = False

            LoadWatchList()

            SetControlReadOnly()

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Search_By_CIF_Click(sender As Object, e As DirectEventArgs)
        Try

            StoreSEARCH_CIF.Reload()
            window_SEARCH_CIF.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_SELECT_CIF(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            txt_NAME_SEARCH.Value = e.ExtraParams(1).Value
            txt_DATE_OF_BIRTH_SEARCH.Value = e.ExtraParams(2).Value
            txt_NATIONALITY_SEARCH.Value = e.ExtraParams(3).Value
            txt_IDENTITY_NUMBER_SEARCH.Value = e.ExtraParams(4).Value
            txt_RESPONSE_SEARCH.Value = Nothing
            txt_CIF_SEARCH.Value = e.ExtraParams(0).Value '' Assigned last bcs onChange to clear it when other text box is changed, will be triggered

            '29-May-2023 Adi : Tambahan untuk screening Alias Name
            Dim strQuery As String = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & txt_CIF_SEARCH.Value & "'"
            Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            If drCustomer IsNot Nothing Then
                If Not IsDBNull(drCustomer("ALIAS")) AndAlso Not String.IsNullOrWhiteSpace(drCustomer("ALIAS")) Then
                    txt_NAME_ALIAS_SEARCH.Value = drCustomer("ALIAS")
                    txt_NAME_ALIAS_SEARCH.Hidden = False
                Else
                    txt_NAME_ALIAS_SEARCH.Value = Nothing
                    txt_NAME_ALIAS_SEARCH.Hidden = True
                End If
            End If
            'End of 29-May-2023 Adi : Tambahan untuk screening Alias Name


            window_SEARCH_CIF.Hidden = True

            '' Added by Felix 17 May 2021
            btn_Detail_CIF.Hidden = False
            LoadDetailCIF(e.ExtraParams(0).Value)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '' Added by Felix 17 May 2021
    Sub LoadDetailCIF(CIF As String)
        Try

            'ID_AML_Watchlist = ID

            'Window_WATCHLIST_GENERAL_INFORMATION.Hidden = False

            'LoadWatchList()

            'SetControlReadOnly()

            Dim objParamGET_DETAIL_CIF(0) As SqlParameter
            objParamGET_DETAIL_CIF(0) = New SqlParameter
            objParamGET_DETAIL_CIF(0).ParameterName = "@CIFNo"
            objParamGET_DETAIL_CIF(0).Value = CIF

            Dim objdt_DETAIL_CIF As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_GET_DETAIL_CIF_ADHOC", objParamGET_DETAIL_CIF)
            If objdt_DETAIL_CIF IsNot Nothing Then
                For Each row As DataRow In objdt_DETAIL_CIF.Rows
                    'NAME_WEIGHT = row.Item("NAME_WEIGHT")
                    'DOB_WEIGHT = row.Item("DOB_WEIGHT")
                    'NATIONALITY_WEIGHT = row.Item("NATIONALITY_WEIGHT")
                    'IDENTITY_WEIGHT = row.Item("IDENTITY_WEIGHT")

                    display_Request_CIF.Value = row.Item("CIF")
                    display_Request_Name.Value = row.Item("Name")

                    '31-May-2023 Adi : tambah display alias name
                    If Not IsDBNull(row.Item("Alias_Name")) AndAlso Not String.IsNullOrWhiteSpace(row.Item("Alias_Name")) Then
                        display_Request_Alias.Value = row.Item("Alias_Name")
                        display_Request_Alias.Hidden = False
                    Else
                        display_Request_Alias.Value = Nothing
                        display_Request_Alias.Hidden = True
                    End If
                    'End of 31-May-2023 Adi : tambah display alias name

                    display_Request_Kode_Cabang.Value = row.Item("Kode_Cabang")
                    display_Request_Nama_Cabang.Value = row.Item("Nama_Cabang")
                    display_Request_Kantor_Wilayah.Value = row.Item("Kantor_Wilayah")
                    'display_Request_Identity_Number.Value = row.Item("Identity_Number")
                    display_Request_NPWP.Value = row.Item("NPWP")
                    display_Request_Tipe_Nasabah.Value = row.Item("Tipe_Nasabah")

                    display_Request_Tempat_Lahir.Value = row.Item("Tempat_Lahir")

                    '31-May-2023 Adi : tambah validasi datetime
                    If Not IsDBNull(row.Item("DOB")) AndAlso row.Item("DOB") <> DateTime.MinValue Then
                        display_Request_DOB.Value = Convert.ToDateTime(row.Item("DOB")).ToString("dd-MMM-yyyy")
                    Else
                        display_Request_DOB.Value = Nothing
                    End If
                    'End of 31-May-2023 Adi : tambah validasi datetime

                    display_Request_Pekerjaan.Value = row.Item("Pekerjaan")
                    'display_Request_Alamat.Value = row.Item("Alamat")
                    display_Request_Nationality.Value = row.Item("Nationality")

                    '31-May-2023 Adi : tambah validasi datetime
                    If Not IsDBNull(row.Item("Waktu_Proses")) AndAlso row.Item("Waktu_Proses") <> DateTime.MinValue Then
                        display_Request_Waktu_Proses.Value = Convert.ToDateTime(row.Item("Waktu_Proses")).ToString("dd-MMM-yyyy")
                    Else
                        display_Request_Waktu_Proses.Value = Nothing
                    End If
                    'End of 31-May-2023 Adi : tambah validasi datetime

                    display_Request_Tujuan_Dana.Value = row.Item("Tujuan_Dana") '' Edited by Felix 18 Jun 2021 Ganti Watchlist -> Tujuan Dana
                    display_Request_Profil_Risiko_T24.Value = row.Item("Profil_Risiko_T24")
                    display_Request_Profil_Risiko_oneFCC.Value = row.Item("Profil_Risiko_oneFCC")


                    Dim objParamGET_DETAIL_CIF_ADDRESS(0) As SqlParameter
                    objParamGET_DETAIL_CIF_ADDRESS(0) = New SqlParameter
                    objParamGET_DETAIL_CIF_ADDRESS(0).ParameterName = "@CIFNo"
                    objParamGET_DETAIL_CIF_ADDRESS(0).Value = CIF

                    Dim objdt_DETAIL_CIF_Address As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_GET_DETAIL_CIF_ADDRESS_ADHOC", objParamGET_DETAIL_CIF_ADDRESS)
                    If objdt_DETAIL_CIF_Address IsNot Nothing Then
                        gp_DETAIL_CIF_ADDRESS.GetStore.DataSource = objdt_DETAIL_CIF_Address
                        gp_DETAIL_CIF_ADDRESS.GetStore.DataBind()
                    End If

                    Dim objParamGET_DETAIL_CIF_IDENTITY(0) As SqlParameter
                    objParamGET_DETAIL_CIF_IDENTITY(0) = New SqlParameter
                    objParamGET_DETAIL_CIF_IDENTITY(0).ParameterName = "@CIFNo"
                    objParamGET_DETAIL_CIF_IDENTITY(0).Value = CIF

                    Dim objdt_DETAIL_CIF_Identity As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_GET_DETAIL_CIF_IDENTITY_ADHOC", objParamGET_DETAIL_CIF_IDENTITY)
                    If objdt_DETAIL_CIF_Identity IsNot Nothing Then
                        gp_DETAIL_CIF_IDENTITY.GetStore.DataSource = objdt_DETAIL_CIF_Identity
                        gp_DETAIL_CIF_IDENTITY.GetStore.DataBind()
                    End If
                Next row
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Detail_CIF_Click(sender As Object, e As DirectEventArgs)
        Try
            'LoadDetailCIF(txt_CIF_SEARCH.Value)
            window_DETAIL_CIF.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_DETAIL_CIF_Cancel_Click()
        Try
            'Hide window pop up
            window_DETAIL_CIF.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '' End of 17 May 2021

    Protected Sub check_CIF_change() '' Refresh CIF when text input are changed
        Try
            '17-Mei-2021 Adi : di comment aja semua. 1 tempat saja di button search CIF
            ''Dim CIF_Change As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF_NO FROM vw_SEARCH_CIF_SCREENING_ADHOC where [Name] = '" + txt_NAME_SEARCH.Value + "' and datediff(day,DOB,'" + txt_DATE_OF_BIRTH_SEARCH.Value + "') = 0 and Nationality = '" + txt_NATIONALITY_SEARCH.Value + "' and Identity_Number = '" + txt_IDENTITY_NUMBER_SEARCH.Value + "'")
            'Dim QueryCheckCIF As String = "Select CIF_NO FROM vw_SEARCH_CIF_SCREENING_ADHOC where [Name] = '" + txt_NAME_SEARCH.Value.Replace("'", "''") + "' "

            'If Not CDate(txt_DATE_OF_BIRTH_SEARCH.Value) = DateTime.MinValue Then
            '    QueryCheckCIF = QueryCheckCIF + " and datediff(day,DOB,try_convert(datetime,'" + txt_DATE_OF_BIRTH_SEARCH.Value + "')) = 0 "
            'End If

            'If txt_NATIONALITY_SEARCH.Value <> "" Then
            '    QueryCheckCIF = QueryCheckCIF + " and Nationality = '" + txt_NATIONALITY_SEARCH.Value + "' "
            'End If

            'If txt_IDENTITY_NUMBER_SEARCH.Value <> "" Then
            '    QueryCheckCIF = QueryCheckCIF + " and Identity_Number = '" + txt_IDENTITY_NUMBER_SEARCH.Value + "'"
            'End If

            ''Dim CIF_Change As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "select CIF_NO FROM vw_SEARCH_CIF_SCREENING_ADHOC where [Name] = '" + txt_NAME_SEARCH.Value + "' and datediff(day,DOB,'" + txt_DATE_OF_BIRTH_SEARCH.Value + "') = 0 and Nationality = '" + txt_NATIONALITY_SEARCH.Value + "' and Identity_Number = '" + txt_IDENTITY_NUMBER_SEARCH.Value + "'", Nothing)
            'Dim CIF_Change As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, QueryCheckCIF, Nothing)
            'If Not CIF_Change Is Nothing Then
            '    txt_CIF_SEARCH.Value = CIF_Change
            'Else
            '    txt_CIF_SEARCH.Value = Nothing
            'End If


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_SEARCH_CIF_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 20

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            If strsort = "" Then
                strsort = "CIFNo asc, CUSTOMERNAME asc"
            End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_AML_CUSTOMER_For_Screening", "CIFNo, CUSTOMERNAME, DATEOFBIRTH, Nationality, Identity_Number", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start <0 OrElse limit <0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            StoreSEARCH_CIF.DataSource = DataPaging
            StoreSEARCH_CIF.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    '------- LOAD WATCHLIST
    Protected Sub LoadWatchList()
        Try
            'Load Watchlist
            objAML_WATCHLIST_CLASS = AML_WATCHLIST_BLL.GetWatchlistClassByID(ID_AML_Watchlist)

            'Bind Data Header
            If objAML_WATCHLIST_CLASS.objAML_WATCHLIST IsNot Nothing Then
                With objAML_WATCHLIST_CLASS.objAML_WATCHLIST
                    txt_AML_WATCHLIST_ID.Value = .PK_AML_WATCHLIST_ID

                    txt_AML_WATCHLIST_CATEGORY.Value = .FK_AML_WATCHLIST_CATEGORY_ID
                    If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                        Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                        If objWatchlistCategory IsNot Nothing Then
                            txt_AML_WATCHLIST_CATEGORY.Value &= " - " & objWatchlistCategory.CATEGORY_NAME
                        End If
                        '' Add 10-Feb-2023 Ariswara, Display Button Display Watchlist Worldcheck
                        Dim PkWatchlistWorldcheckCategory As String = ""

                        Dim strPkWatchlistWorldcheckCategory As String = "select ParameterValue"
                        strPkWatchlistWorldcheckCategory += " FROM AML_Global_Parameter a "
                        strPkWatchlistWorldcheckCategory += " where PK_GlobalReportParameter_ID = '14'"

                        PkWatchlistWorldcheckCategory = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strPkWatchlistWorldcheckCategory, Nothing)

                        If PkWatchlistWorldcheckCategory IsNot Nothing Then
                            If .FK_AML_WATCHLIST_CATEGORY_ID = PkWatchlistWorldcheckCategory Then
                                btn_OpenWindowWatchlist.Hidden = "false"

                                Dim strIDCode As String = 16645 '' Module ID "AML_WATCHLIST_WORLDCHECK"
                                IDModuleWatchlistEncrypted = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                                Dim PkWatchlistWorldcheckID As String = ""

                                Dim strPkWatchlistWorldcheckID As String = "select FK_AML_WATCHLIST_SOURCE_ID"
                                strPkWatchlistWorldcheckID += " FROM AML_WATCHLIST a "
                                strPkWatchlistWorldcheckID += " where PK_AML_WATCHLIST_ID = '" & .PK_AML_WATCHLIST_ID.ToString() & "'"

                                PkWatchlistWorldcheckID = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strPkWatchlistWorldcheckID, Nothing)
                                IDWatchlistEncrypted = Common.EncryptQueryString(PkWatchlistWorldcheckID, SystemParameterBLL.GetEncriptionKey)

                                If IDModuleWatchlistEncrypted Is Nothing Or IDWatchlistEncrypted Is Nothing Then
                                    Throw New ApplicationException("Watchlist ID is not found.")
                                End If
                            Else
                                btn_OpenWindowWatchlist.Hidden = "true"
                            End If
                        End If
                        '' End 10-Feb-2023
                    End If

                    If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                        txt_AML_WATCHLIST_TYPE.Value = .FK_AML_WATCHLIST_TYPE_ID
                        Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                        If objWatchlistType IsNot Nothing Then
                            txt_AML_WATCHLIST_TYPE.Value &= " - " & objWatchlistType.TYPE_NAME
                        End If
                    End If

                    txt_NAME.Value = .NAME
                    If .DATE_OF_BIRTH.HasValue Then
                        txt_DATE_OF_BIRTH.Value = .DATE_OF_BIRTH.GetValueOrDefault().ToString("dd-MMM-yyyy")
                    End If
                    txt_PLACE_OF_BIRTH.Value = .BIRTH_PLACE
                    txt_YOB.Value = .YOB
                    txt_NATIONALITY.Value = .NATIONALITY

                    txt_CUSTOM_REMARK_1.Value = .CUSTOM_REMARK_1
                    txt_CUSTOM_REMARK_2.Value = .CUSTOM_REMARK_2
                    txt_CUSTOM_REMARK_3.Value = .CUSTOM_REMARK_3
                    txt_CUSTOM_REMARK_4.Value = .CUSTOM_REMARK_4
                    txt_CUSTOM_REMARK_5.Value = .CUSTOM_REMARK_5
                End With
            End If


            'Bind Data Detail to Grid Panel
            Bind_AML_WATCHLIST_ALIAS()
            Bind_AML_WATCHLIST_ADDRESS()
            Bind_AML_WATCHLIST_IDENTITY()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF LOAD WATCHLIST

    '------ Window Watchlist General Information -----------------------
    Protected Sub btn_AML_WATCHLIST_GENERAL_INFORMATION_Close_Click()
        Try
            'Hide window pop up
            Window_WATCHLIST_GENERAL_INFORMATION.Hidden = True
            'pnlSCREENINGADHOC.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------ Window Watchlist General Information -----------------------

    '------- WATCHLIST ALIAS
    Protected Sub btn_AML_WATCHLIST_ALIAS_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objAML_WATCHLIST_ALIAS_Edit = Nothing
            Clean_Window_AML_WATCHLIST_ALIAS()

            'Show window pop up
            Window_AML_WATCHLIST_ALIAS.Title = "Watch List Alias - Add"
            Window_AML_WATCHLIST_ALIAS.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_AML_WATCHLIST_ALIAS(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Find(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID = strID)
                If objToDelete IsNot Nothing Then
                    objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Remove(objToDelete)
                End If
                Bind_AML_WATCHLIST_ALIAS()
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objAML_WATCHLIST_ALIAS_Edit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Find(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID = strID)
                Load_Window_AML_WATCHLIST_ALIAS(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_ALIAS_Cancel_Click()
        Try
            'Hide window pop up
            Window_AML_WATCHLIST_ALIAS.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_ALIAS_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(txt_AML_WATCHLIST_ALIAS_NAME.Value) Then
                Throw New ApplicationException(txt_AML_WATCHLIST_ALIAS_NAME.FieldLabel & " harus diisi.")
            End If

            'Action save here
            Dim intPK As Long = -1

            If objAML_WATCHLIST_ALIAS_Edit Is Nothing Then  'Add
                Dim objAdd As New NawaDevDAL.AML_WATCHLIST_ALIAS
                If objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS IsNot Nothing AndAlso objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Count > 0 Then
                    intPK = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Min(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                With objAdd
                    .PK_AML_WATCHLIST_ALIAS_ID = intPK
                    .ALIAS_NAME = Trim(txt_AML_WATCHLIST_ALIAS_NAME.Value)
                End With

                objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Add(objAdd)
            Else    'Edit
                Dim objEdit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Find(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID = objAML_WATCHLIST_ALIAS_Edit.PK_AML_WATCHLIST_ALIAS_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        .ALIAS_NAME = Trim(txt_AML_WATCHLIST_ALIAS_NAME.Value)
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_AML_WATCHLIST_ALIAS()

            'Hide window popup
            Window_AML_WATCHLIST_ALIAS.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ALIAS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS)

        'Running the following script for additional columns
        'objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        'If objtable.Rows.Count > 0 Then
        '    For Each item As Data.DataRow In objtable.Rows
        '        item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
        '    Next
        'End If
        gp_AML_WATCHLIST_ALIAS.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_ALIAS.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_AML_WATCHLIST_ALIAS()
        'Clean fields
        txt_AML_WATCHLIST_ALIAS_NAME.Value = Nothing

        'Set fields' ReadOnly
        txt_AML_WATCHLIST_ALIAS_NAME.ReadOnly = False

        'Show Buttons
        btn_AML_WATCHLIST_ALIAS_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_AML_WATCHLIST_ALIAS(strAction As String)
        'Clean window pop up
        Clean_Window_AML_WATCHLIST_ALIAS()

        'Populate fields
        With objAML_WATCHLIST_ALIAS_Edit
            txt_AML_WATCHLIST_ALIAS_NAME.Value = .ALIAS_NAME
        End With

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            txt_AML_WATCHLIST_ALIAS_NAME.ReadOnly = False
        Else
            txt_AML_WATCHLIST_ALIAS_NAME.ReadOnly = True
            btn_AML_WATCHLIST_ALIAS_Save.Hidden = True
        End If

        'Show window pop up
        Window_AML_WATCHLIST_ALIAS.Title = "Watch List Alias - " & strAction
        Window_AML_WATCHLIST_ALIAS.Hidden = False
    End Sub
    '------- END OF WATCHLIST ALIAS

    '------- WATCHLIST ADDRESS
    Protected Sub btn_AML_WATCHLIST_ADDRESS_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objAML_WATCHLIST_ADDRESS_Edit = Nothing
            Clean_Window_AML_WATCHLIST_ADDRESS()

            'Show window pop up
            Window_AML_WATCHLIST_ADDRESS.Title = "Watch List Address - Add"
            Window_AML_WATCHLIST_ADDRESS.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_AML_WATCHLIST_ADDRESS(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS.Find(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID = strID)
                If objToDelete IsNot Nothing Then
                    objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS.Remove(objToDelete)
                End If
                Bind_AML_WATCHLIST_ADDRESS()
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objAML_WATCHLIST_ADDRESS_Edit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS.Find(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID = strID)
                Load_Window_AML_WATCHLIST_ADDRESS(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_ADDRESS_Cancel_Click()
        Try
            'Hide window pop up
            Window_AML_WATCHLIST_ADDRESS.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ADDRESS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS)

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
                If objCountry IsNot Nothing Then
                    item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
                Else
                    item("COUNTRY_NAME") = Nothing
                End If

                Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
                If objAddressType IsNot Nothing Then
                    item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
                Else
                    item("AML_ADDRESS_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gp_AML_WATCHLIST_ADDRESS.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_ADDRESS.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_AML_WATCHLIST_ADDRESS()
        'Clean fields
        txt_AML_WATCHLIST_ADDRESS_TYPE_CODE.Value = Nothing
        txt_AML_WATCHLIST_ADDRESS.Value = Nothing
        txt_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.Value = Nothing
        txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.Value = Nothing

        'Set fields' ReadOnly
        txt_AML_WATCHLIST_ADDRESS_TYPE_CODE.ReadOnly = False
        txt_AML_WATCHLIST_ADDRESS.ReadOnly = False
        txt_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.ReadOnly = False
        txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.ReadOnly = False

    End Sub

    Protected Sub Load_Window_AML_WATCHLIST_ADDRESS(strAction As String)
        'Clean window pop up
        Clean_Window_AML_WATCHLIST_ADDRESS()

        'Populate fields
        With objAML_WATCHLIST_ADDRESS_Edit
            txt_AML_WATCHLIST_ADDRESS.Value = .ADDRESS
            txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.Value = .POSTAL_CODE

            If Not IsNothing(.FK_AML_ADDRESS_TYPE_CODE) Then
                txt_AML_WATCHLIST_ADDRESS_TYPE_CODE.Value = .FK_AML_ADDRESS_TYPE_CODE
                Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(.FK_AML_ADDRESS_TYPE_CODE)
                If objAddressType IsNot Nothing Then
                    txt_AML_WATCHLIST_ADDRESS_TYPE_CODE.Value &= " - " & objAddressType.ADDRESS_TYPE_NAME
                End If
            End If

            If Not IsNothing(.FK_AML_COUNTRY_CODE) Then
                txt_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.Value = .FK_AML_COUNTRY_CODE
                Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(.FK_AML_COUNTRY_CODE)
                If objCountry IsNot Nothing Then
                    txt_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.Value &= " - " & objCountry.AML_COUNTRY_Name
                End If
            End If

        End With

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            txt_AML_WATCHLIST_ADDRESS_TYPE_CODE.ReadOnly = True
            txt_AML_WATCHLIST_ADDRESS.ReadOnly = True
            txt_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.ReadOnly = True
            txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.ReadOnly = True
        End If

        'Show window pop up
        Window_AML_WATCHLIST_ADDRESS.Title = "Watch List Address - " & strAction
        Window_AML_WATCHLIST_ADDRESS.Hidden = False
    End Sub
    '------- END OF WATCHLIST ADDRESS

    '------- WATCHLIST IDENTITY
    Protected Sub btn_AML_WATCHLIST_IDENTITY_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objAML_WATCHLIST_IDENTITY_Edit = Nothing
            Clean_Window_AML_WATCHLIST_IDENTITY()

            'Show window pop up
            Window_AML_WATCHLIST_IDENTITY.Title = "Watch List Identity - Add"
            Window_AML_WATCHLIST_IDENTITY.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_AML_WATCHLIST_IDENTITY(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY.Find(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID = strID)
                If objToDelete IsNot Nothing Then
                    objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY.Remove(objToDelete)
                End If
                Bind_AML_WATCHLIST_IDENTITY()
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objAML_WATCHLIST_IDENTITY_Edit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY.Find(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID = strID)
                Load_Window_AML_WATCHLIST_IDENTITY(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_IDENTITY_Cancel_Click()
        Try
            'Hide window pop up
            Window_AML_WATCHLIST_IDENTITY.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_IDENTITY()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY)

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
                If objIDENTITYType IsNot Nothing Then
                    item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
                Else
                    item("AML_IDENTITY_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gp_AML_WATCHLIST_IDENTITY.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_IDENTITY.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_AML_WATCHLIST_IDENTITY()
        'Clean fields
        txt_AML_WATCHLIST_IDENTITY_TYPE_CODE.Value = Nothing
        txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.Value = Nothing

        'Set fields' ReadOnly
        txt_AML_WATCHLIST_IDENTITY_TYPE_CODE.ReadOnly = False
        txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.ReadOnly = False

    End Sub

    Protected Sub Load_Window_AML_WATCHLIST_IDENTITY(strAction As String)
        'Clean window pop up
        Clean_Window_AML_WATCHLIST_IDENTITY()

        'Populate fields
        With objAML_WATCHLIST_IDENTITY_Edit
            txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.Value = .IDENTITYNUMBER


            If Not IsNothing(.FK_AML_IDENTITY_TYPE_CODE) Then
                txt_AML_WATCHLIST_IDENTITY_TYPE_CODE.Value = .FK_AML_IDENTITY_TYPE_CODE
                Dim objIdentityType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(.FK_AML_IDENTITY_TYPE_CODE)
                If objIdentityType IsNot Nothing Then
                    txt_AML_WATCHLIST_IDENTITY_TYPE_CODE.Value &= " - " & objIdentityType.IDENTITY_TYPE_NAME
                End If
            End If
        End With

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            txt_AML_WATCHLIST_IDENTITY_TYPE_CODE.ReadOnly = True
            txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.ReadOnly = True
        End If

        'Show window pop up
        Window_AML_WATCHLIST_IDENTITY.Title = "Watch List Identity - " & strAction
        Window_AML_WATCHLIST_IDENTITY.Hidden = False
    End Sub
    '------- END OF WATCHLIST IDENTITY

    Sub SetControlReadOnly()
        'Read Only property
        txt_AML_WATCHLIST_CATEGORY.ReadOnly = True
        txt_AML_WATCHLIST_TYPE.ReadOnly = True
        txt_NAME.ReadOnly = True
        txt_PLACE_OF_BIRTH.ReadOnly = True
        txt_DATE_OF_BIRTH.ReadOnly = True
        txt_YOB.ReadOnly = True
        txt_NATIONALITY.ReadOnly = True
        txt_CUSTOM_REMARK_1.ReadOnly = True
        txt_CUSTOM_REMARK_2.ReadOnly = True
        txt_CUSTOM_REMARK_3.ReadOnly = True
        txt_CUSTOM_REMARK_4.ReadOnly = True
        txt_CUSTOM_REMARK_5.ReadOnly = True

        'Background Color
        'txt_NAME.FieldStyle = "background-color:#ddd;"
        'txt_PLACE_OF_BIRTH.FieldStyle = "background-color:#ddd;"
        'txt_DATE_OF_BIRTH.FieldStyle = "background-color:#ddd;"
        'txt_YOB.FieldStyle = "background-color:#ddd;"
        'txt_NATIONALITY.FieldStyle = "background-color:#ddd;"
        'txt_CUSTOM_REMARK_1.FieldStyle = "background-color:#ddd;"
        'txt_CUSTOM_REMARK_2.FieldStyle = "background-color:#ddd;"
        'txt_CUSTOM_REMARK_3.FieldStyle = "background-color:#ddd;"
        'txt_CUSTOM_REMARK_4.FieldStyle = "background-color:#ddd;"
        'txt_CUSTOM_REMARK_5.FieldStyle = "background-color:#ddd;"

        'Hide Button
        'btn_WATCHLIST_Submit.Hidden = True

        gp_AML_WATCHLIST_ALIAS.TopBar.Item(0).Hidden = True
        'cc_AML_WATCHLIST_ALIAS.Commands.RemoveAt(1)
        'cc_AML_WATCHLIST_ALIAS.Commands.RemoveAt(1)

        gp_AML_WATCHLIST_ADDRESS.TopBar.Item(0).Hidden = True
        'cc_AML_WATCHLIST_ADDRESS.Commands.RemoveAt(1)
        'cc_AML_WATCHLIST_ADDRESS.Commands.RemoveAt(1)

        gp_AML_WATCHLIST_IDENTITY.TopBar.Item(0).Hidden = True
        'cc_AML_WATCHLIST_IDENTITY.Commands.RemoveAt(1)
        'cc_AML_WATCHLIST_IDENTITY.Commands.RemoveAt(1)

    End Sub

    '' Added by Felix on 21 May 2021 -- Export Result to Excel
    Protected Sub ExportResultToExcel(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try
            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            'Dim intTotalRow As Long
            Dim objSchemaModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(16696)
            Dim objSchemaModuleField As List(Of NawaDAL.ModuleField) = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(16696)
            Dim objSchemaModuleFieldDefault As New List(Of NawaDAL.ModuleFieldDefault)
            Dim objmdl As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(16696)

            'Dim cif As String = sar_CIF.SelectedItem.Value
            'Dim datefrom As Date = sar_DateFrom.SelectedDate
            'Dim dateto As Date = sar_DateTo.SelectedDate

            'Dim objList = NawaDevBLL.GenerateSarBLL.getListTransactionBySearch(cif, datefrom, dateto)



            objFormModuleView = New FormModuleView(gp_Screening_Result, BtnExport)
            objFormModuleView.ModuleID = objmdl.PK_Module_ID
            objFormModuleView.ModuleName = objmdl.ModuleName

            Dim delimitercheckboxparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(39)
            Dim strdelimiter As String = ";"
            If Not delimitercheckboxparam Is Nothing Then
                strdelimiter = delimitercheckboxparam.SettingValue
            End If

            Dim objtb As DataTable = objdt_SEARCH_RESULT_API
            Dim intmaxrow As Integer
            If objtb.Rows.Count + 1000 > 1048575 Then
                intmaxrow = 1048575
            Else
                intmaxrow = objtb.Rows.Count + 1000
            End If


            Using objtbl As Data.DataTable = objtb
                'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                objtbl.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                Dim intTotalPage As Integer = (intmaxrow + 1048575 - 1) / 1048575
                Dim dsTable As New Data.DataSet

                If intTotalPage = 1 Then
                    'objFormModuleView.changeHeader(objtbl)

                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In gp_Screening_Result.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    DataSetToExcel(dsTable, objfileinfo)

                    dsTable.Dispose()
                    dsTable = Nothing



                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If



                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")



                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()
                ElseIf intTotalPage > 1 Then
                    'objFormModuleView.changeHeader(objtbl)



                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In gp_Screening_Result.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    'ambil page 2 dst sampe page terakhir
                    For index = 1 To intTotalPage - 2
                        Using objtbltemp As Data.DataTable = objtb
                            objtbltemp.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                            dsTable.Tables.Add(objtbltemp.Copy)
                        End Using
                    Next
                    DataSetToExcel(dsTable, objfileinfo)
                    dsTable.Dispose()
                    dsTable = Nothing

                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If
                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()

                End If
            End Using

        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub

    Private Shared Sub DataSetToExcel(ByVal dataSet As DataSet, ByVal objfileinfo As FileInfo)
        Using pck As ExcelPackage = New ExcelPackage()

            For Each dataTable As DataTable In dataSet.Tables
                Dim workSheet As ExcelWorksheet = pck.Workbook.Worksheets.Add(dataTable.TableName)
                workSheet.Cells("A1").LoadFromDataTable(dataTable, True)

                Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                Dim intcolnumber As Integer = 1
                For Each item As System.Data.DataColumn In dataTable.Columns
                    If item.DataType = GetType(Date) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = dateformat
                    End If
                    If item.DataType = GetType(Integer) Or item.DataType = GetType(Double) Or item.DataType = GetType(Long) Or item.DataType = GetType(Decimal) Or item.DataType = GetType(Single) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = "#,##0.00"
                    End If

                    intcolnumber = intcolnumber + 1
                Next
                workSheet.Cells(workSheet.Dimension.Address).AutoFitColumns()
            Next

            pck.SaveAs(objfileinfo)
        End Using
    End Sub
    '' End of 21 May 2021
    'Public Class AML_SCREENING_ADHOC_RESULT

    '    Public FK_AML_WATCHLIST_ID As String
    '    Public Name As String
    '    Public Name_Watchlist As String
    '    Public Watchlist_Category As String
    '    Public Watchlist_Type As String
    '    Public Dob As DateTime
    '    Public Nationality As String
    '    Public Identity_number As String
    '    Public SIMILARITY As Decimal

    'End Class

#Region "5-Jan-2023 Adi : Tambah Download as PDF. Thanks to Source from BTPNS"
    Protected Sub btn_PreviewPDF_Click(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try

            Dim URL As String = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(13).SettingValue
            URL = URL.Substring(0, URL.Length - 1)

            'Penambahan Local Folder
            Dim ReportingServiceURL As String = ""
            'Dim LocalFolder As String = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(80).SettingValue
            Dim LocalFolder As String = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(15).SettingValue

            If LocalFolder <> """""" Then
                ReportingServiceURL = URL + "?/" + LocalFolder + "/Screening_Result"
            Else
                ReportingServiceURL = URL + "?/Screening_Result"
            End If
            'End Penambahan Local Folder
            Dim ContentType As String = ""
            Dim Format As String
            Format = "&rs:Format=PDF"
            ContentType = "application/pdf"


            Dim PK_ID As String = ""
            Dim CIF_NO As String = ""
            If requestID IsNot Nothing Then
                PK_ID = "&REQUEST_ID=" & requestID
            End If

            Dim GenerateReportingServiceURL As String = ReportingServiceURL & PK_ID & Format

            Dim userName As String = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(9).SettingValue
            Dim Password As String = NawaBLL.Common.DecryptRijndael(NawaBLL.SystemParameterBLL.GetSystemParameterByPk(10).SettingValue, NawaBLL.SystemParameterBLL.GetSystemParameterByPk(10).EncriptionKey)

            Dim net As New System.Net.WebClient
            net.Credentials = New NetworkCredential(userName, Password)
            Dim data() As Byte = net.DownloadData(GenerateReportingServiceURL)
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=Screening_Result.pdf")
            Response.Charset = ""
            Response.Buffer = True
            Response.ContentType = ContentType
            Response.BinaryWrite(data)
            Response.End()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
#End Region

    ' 22 Feb 2023 Ariswara penambahan untuk worldcheck
#Region "World Check"
    Public Property IDWatchlistEncrypted() As String
        Get
            Return Session("AML_SCREENING_ADHOC.IDWatchlistEncrypted")
        End Get
        Set(ByVal value As String)
            Session("AML_SCREENING_ADHOC.IDWatchlistEncrypted") = value
        End Set
    End Property
    Public Property IDModuleWatchlistEncrypted() As String
        Get
            Return Session("AMLJudgmentEdit.IDModuleWatchlistEncrypted")
        End Get
        Set(ByVal value As String)
            Session("AMLJudgmentEdit.IDModuleWatchlistEncrypted") = value
        End Set
    End Property
    Protected Sub btn_OpenWindowWatchlist_Click()
        Try
            WindowDisplayWatchlist.Hidden = False
            pnlContent.ClearContent()
            pnlContent.AnimCollapse = False
            pnlContent.Loader.SuspendScripting()
            pnlContent.Loader.Url = "~/Parameter/ParameterDetail.aspx?ID=" & IDWatchlistEncrypted & "&ModuleID=" & IDModuleWatchlistEncrypted & ""
            pnlContent.Loader.Params.Clear()
            pnlContent.LoadContent()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Display_Watchlist_Back_Click()
        Try
            WindowDisplayWatchlist.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class