﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AML_ScreeningRequest_Add.aspx.vb" Inherits="AML_Screening_Adhoc" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };

        Ext.grid.plugin.SelectionMemory.override({
            memoryRestoreState: function () {
                this.selModel.suspendEvents();
                this.callParent();
                this.selModel.resumeEvents();
            }
        });

        var prepareCommandCollection = function (grid, toolbar, rowIndex, record) {
            var DetailButton = toolbar.items.get(0);
            var wf = record.data.Name_Watch_List
            if (DetailButton != undefined) {
                if (wf != "") { /*Edited by Adi 3 Jun 2021 -- set Detail Disabled jika Nama Watch List kosong. Artinya screening result dari FATF dan OEDF (Hanya screening nationality)*/
                    DetailButton.setDisabled(false)
                } else {
                    DetailButton.setDisabled(true)
                }
            }
       }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="Watch List Data">
        <Items>
            <%-- Input for Screening Adhoc --%>
            <ext:Panel runat="server" Title="Searching Request" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlSCREENINGADHOC" Border="true" BodyStyle="padding:10px">
                <Content>

                    <ext:Button ID="btn_Search_By_CIF" runat="server" Icon="Magnifier" Text="Search by CIF" MarginSpec="0 10 0 0">
                        <DirectEvents>
                            <Click OnEvent="btn_Search_By_CIF_Click">
                                <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="btn_Detail_CIF" runat="server" Icon="ControlBlank" Text="Detail CIF" Hidden="true"> 
                        <DirectEvents>
                            <Click OnEvent="btn_Detail_CIF_Click">
                                <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:FieldSet runat="server" ID="FieldSet6" Collapsible="false" Title="" Border="false" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="10 0" Padding="5">
                        <Items>
                            <ext:TextField ID="txt_CIF_SEARCH" LabelWidth="250" runat="server" FieldLabel="CIF" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true" Editable="false"></ext:TextField>
                            <ext:TextField ID="txt_NAME_SEARCH" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Name" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true">
                                <%--<DirectEvents><Change OnEvent="txt_NAME_SEARCH_OnChanged" /></DirectEvents>--%>
                            </ext:TextField>

                            <%-- 26-May-2023 Adi : Tambah screening alias --%>
                            <ext:TextField ID="txt_NAME_ALIAS_SEARCH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Alias Name" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                            <%-- End of 26-May-2023 Adi : Tambah screening alias --%>

                            <ext:DateField ID="txt_DATE_OF_BIRTH_SEARCH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="50%" Format="dd-MMM-yyyy">
                                <%--<DirectEvents><Change OnEvent="txt_DATE_OF_BIRTH_SEARCH_OnChanged" /></DirectEvents>--%>
                            </ext:DateField>
                            <ext:TextField ID="txt_NATIONALITY_SEARCH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Nationality" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true">
                                <%--<DirectEvents><Change OnEvent="txt_NATIONALITY_SEARCH_OnChanged" /></DirectEvents>--%>
                            </ext:TextField>
                            <ext:TextField ID="txt_IDENTITY_NUMBER_SEARCH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Identity Number" AnchorHorizontal="50%" MaxLength="100" EnforceMaxLength="true">
                                <%--<DirectEvents><Change OnEvent="txt_IDENTITY_NUMBER_SEARCH_OnChanged" /></DirectEvents>--%>
                            </ext:TextField>
                            <ext:DisplayField ID="txt_RESPONSE_SEARCH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Response Code" AnchorHorizontal="100%"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>

                    <ext:Button ID="btn_Screening_Search" runat="server" Icon="Magnifier" Text="Search" MarginSpec="0 10 0 0">
                        <DirectEvents>
                            <Click OnEvent="btn_Screening_Search_Click">
                                <EventMask ShowMask="true" Msg="Searching Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="btn_Screening_Clear" runat="server" Icon="ControlBlank" Text="Clear Search"> 
                        <DirectEvents>
                            <Click OnEvent="btn_Screening_Clear_Click">
                                <EventMask ShowMask="true" Msg="Clearing Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Content>
            </ext:Panel>
            <%-- End of Watch List General Information --%>

            <%-- AML Screening Result --%>
            <ext:Panel runat="server" Title="Screening Result" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlScreeningResult" Border="true" BodyStyle="padding:10px" PaddingSpec="10 0">
                <Items>
                    <%--<ext:Button ID="btn_DOWNLOAD_RESULT" runat="server" Icon="Disk" Text="Download" Hidden="false">--%>
                        <%--Todo--%>
                       <%-- <DirectEvents>
                            <Click OnEvent=btn_DOWNLOAD_RESULT_Click">
                                <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>--%>
                    <%--</ext:Button>--%>
                    <%--Grid Screening Result--%>
                        <ext:GridPanel ID="gp_Screening_Result" runat="server">

                            <View>
                                <ext:GridView runat="server" EnableTextSelection="true" />
                            </View>
                            <Store>
                                <ext:Store ID="StoreScreeningResult" runat="server" IsPagingStore="true" PageSize="10">
                                    <Model>
                                        <ext:Model runat="server" ID="modelScreeningResult">
                                            <Fields>
                                                <ext:ModelField Name="FK_AML_WATCHLIST_ID" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="Name" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="Name_Watch_List" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="FK_AML_WATCHLIST_CATEGORY_ID" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="Watch_List_Category" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="FK_AML_WATCHLIST_TYPE_ID" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="Watch_List_Type" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="Date_Of_Birth" Type="Date"></ext:ModelField>
                                                <ext:ModelField Name="Nationality" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="Match_Score" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="MATCH_SCORE_NAME" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="MATCH_SCORE_DOB" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="MATCH_SCORE_NATIONALITY" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="MATCH_SCORE_IDENTITY_NUMBER" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="Match_Score_Pct" Type="String"></ext:ModelField>
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                                    <ext:Column ID="Column412" runat="server" DataIndex="Name" Text="Name" Flex="1"></ext:Column>
                                    <ext:Column ID="Column8" runat="server" DataIndex="Name_Watch_List" Text="Name Watch List" Flex="1"></ext:Column>
                                    <ext:Column ID="Column9" runat="server" DataIndex="Watch_List_Category" Text="Watch List Category" Flex="1"></ext:Column>
                                    <ext:Column ID="Column10" runat="server" DataIndex="Watch_List_Type" Text="Watch List Type" Flex="1"></ext:Column>
                                    <ext:DateColumn ID="Column11" runat="server" DataIndex="Date_Of_Birth" Text="Date Of Birth" Flex="1" format="dd-MMM-yyyy"></ext:DateColumn>
                                    <ext:Column ID="Column12" runat="server" DataIndex="Nationality" Text="Nationality" Flex="1"></ext:Column>
                                    <ext:Column ID="Column13" runat="server" DataIndex="Identity_Number" Text="ID Number" Flex="1"></ext:Column>
                                    <ext:Column ID="Column14" runat="server" DataIndex="Match_Score" Text="Match Score (%)" Flex="1" ></ext:Column>
                                    <ext:NumberColumn ID="Column15" runat="server" DataIndex="Match_Score_Pct" Text="Match Score" Flex="1" Format="#,##0.00 %" Hidden="true"></ext:NumberColumn>

                                    <%--Add 14-Dec-2022, Felix. Di hidden agar tidak terexport ke excel--%>
                                    <ext:Column ID="Column20" runat="server" DataIndex="FK_AML_WATCHLIST_ID" Text="FK_AML_WATCHLIST_ID" Flex="1" Hidden="true"></ext:Column>
                                    <ext:Column ID="Column26" runat="server" DataIndex="FK_AML_WATCHLIST_TYPE_ID" Text="FK_AML_WATCHLIST_TYPE_ID" Flex="1" Hidden="true"></ext:Column>
                                    <ext:Column ID="Column21" runat="server" DataIndex="FK_AML_WATCHLIST_CATEGORY_ID" Text="FK_AML_WATCHLIST_CATEGORY_ID" Flex="1" Hidden="true"></ext:Column>
                                    <ext:Column ID="Column22" runat="server" DataIndex="MATCH_SCORE_NAME" Text="MATCH_SCORE_NAME" Flex="1" Hidden="true"></ext:Column>
                                    <ext:Column ID="Column23" runat="server" DataIndex="MATCH_SCORE_DOB" Text="MATCH_SCORE_DOB" Flex="1" Hidden="true"></ext:Column>
                                    <ext:Column ID="Column24" runat="server" DataIndex="MATCH_SCORE_NATIONALITY" Text="MATCH_SCORE_NATIONALITY" Flex="1" Hidden="true"></ext:Column>
                                    <ext:Column ID="Column25" runat="server" DataIndex="MATCH_SCORE_IDENTITY_NUMBER" Text="MATCH_SCORE_IDENTITY_NUMBER" Flex="1" Hidden="true"></ext:Column>
                                    <%--End 14-Dec-2022--%>

                                    <ext:CommandColumn ID="cc_Screening_Result" runat="server" Text="Action" Flex="1">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationFormMagnify" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_Screening_Result">
                                            <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.FK_AML_WATCHLIST_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="total_score" Value="record.data.Match_Score" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="name_score" Value="record.data.MATCH_SCORE_NAME" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="dob_score" Value="record.data.MATCH_SCORE_DOB" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="nationality_score" Value="record.data.MATCH_SCORE_NATIONALITY" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="identity_number_score" Value="record.data.MATCH_SCORE_IDENTITY_NUMBER" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="name_watchlist" Value="record.data.Name_Watch_List" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="dob_watchlist" Value="record.data.Date_Of_Birth" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="nationality_watchlist" Value="record.data.Nationality" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="identity_number_watchlist" Value="record.data.Identity_Number" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <Plugins>
                                <ext:FilterHeader runat="server"></ext:FilterHeader>
                            </Plugins>
                            <BottomBar>
                                <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                            </BottomBar>
                            <TopBar>
                            <ext:Toolbar ID="Toolbar3" runat="server" EnableOverflow="true">
                                <%--                                <Items>
                                       <ext:Button runat="server" ID="BtnExport" Text="Export" AutoPostBack="true" OnClick="CreateExcel2007WithData"  />
                                </Items>--%>
                                <Items>
                                    <ext:Button runat="server" ID="BtnExport" Text="Download Excel" Icon="PageExcel" Hidden="true" >
                                        <DirectEvents>
                                            <Click OnEvent="ExportResultToExcel" isUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>

                                    <%-- 5-Jan-2023 Adi : Tambah Download PDF. Source dari BTPNS --%>
                                    <ext:Button runat="server" ID="btn_PreviewPDF" Hidden="true" Text="Download PDF" Icon="PageWhiteAcrobat">
                                        <DirectEvents>
                                            <Click OnEvent="btn_PreviewPDF_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>

                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        </ext:GridPanel>
                    <%--Grid Screening Result--%>
                    </Items>

            </ext:Panel>
            <%-- AML Screening Result --%>

        </Items>

        <Buttons>
            <ext:Button ID="btn_SCREENING_ADHOC_Save" runat="server" Icon="Disk" Text="Save" Hidden="true">
                <%--Todo--%>
                <DirectEvents>
                    <Click OnEvent="btn_SCREENING_ADHOC_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_SCREENING_ADHOC_Back" runat="server" Icon="PageBack" Text="Back" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btn_SCREENING_ADHOC_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <%--Pop Up Watchlist General Information--%>
    <%-- Pop Up Windows --%>
    <ext:Window ID="Window_WATCHLIST_GENERAL_INFORMATION" Title="Screening Result Detail" Layout="AnchorLayout" runat="server" Modal="true" Hidden="true" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" Maximizable="true" BodyPadding="10">
        <%--<ext:Window ID="Window_WATCHLIST_GENERAL_INFORMATION" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="1500" Height="1500" Layout="FitLayout">--%>
        <Items>
        <%-- Search Scores--%>
           <ext:FormPanel runat="server" Title="Search Score Detail" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlScores" Border="true" BodyStyle="padding:10px" MarginSpec="0 0 10 0">
                <Content>
                    <ext:FieldSet runat="server" ID="FieldSet5" Collapsible="false" Title="" Border="false" Layout="ColumnLayout" AnchorHorizontal="100%" Padding="0">
                        <Items>
                            <ext:FieldSet runat="server" ID="FieldSet3" Collapsible="false" ColumnWidth="0.38" Title="Search Data">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Name" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Nationality" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Identity Number" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>
                            <ext:FieldSet runat="server" ID="FieldSet2" Collapsible="false" ColumnWidth="0.35" Title="Search Result" MarginSpec="0 5">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>
                            <ext:FieldSet runat="server" ID="FieldSet1" Collapsible="false" ColumnWidth="0.25" Title="Search Score">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%" ></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>

                            <ext:FieldSet runat="server" ID="FieldSet4" Collapsible="false" ColumnWidth="1" Title="" Border="false">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_TOTAL" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Total Score" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>

                        </Items>
                    </ext:FieldSet>

               </Content>
            </ext:FormPanel>
        <%-- End ofSearch Scores--%>

        <%-- Watch List General Information --%>
            <ext:FormPanel  Title="Watch List Information" ID="pnlWATCHLIST" runat="server" Collapsible="true" BodyPadding="10" >
                <items>
                   <%--  <ext:DisplayField ID="txt_AML_WATCHLIST_ID" runat="server" FieldLabel="Watch List ID">
                    </ext:DisplayField>--%>
                         <ext:Panel runat="server" ColumnWidth="0.4" Border="false" Layout="AnchorLayout">
                                <Content>
                                    <ext:Button ID="btn_OpenWindowWatchlist" runat="server" Icon="ApplicationViewDetail" Text="Data Watchlist Worldcheck" MarginSpec="0 10 10 0" Hidden="true">
                                        <DirectEvents>
                                            <Click OnEvent="btn_OpenWindowWatchlist_Click">
                                                <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Content>
                            </ext:Panel>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_ID" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Watch List ID" AnchorHorizontal="80%"></ext:DisplayField>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_CATEGORY" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Watch List Category" AnchorHorizontal="80%" EnforceMaxLength="true"></ext:DisplayField>
                     
                    <ext:DisplayField ID="txt_AML_WATCHLIST_TYPE" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Watch List Type" AnchorHorizontal="80%" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_NAME" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_PLACE_OF_BIRTH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_DATE_OF_BIRTH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:DisplayField ID="txt_YOB" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Year of Birth" AnchorHorizontal="50%" MaxLength="8000" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:DisplayField>
                    <ext:DisplayField ID="txt_NATIONALITY" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Nationality" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>

                    <%-- Watch List Alias --%>
                    <ext:GridPanel ID="gp_AML_WATCHLIST_ALIAS" runat="server" Title="Alias(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <TopBar>
                            <ext:Toolbar ID="toolbar12" runat="server">
                                <Items>
                                    <ext:Button ID="btn_AML_WATCHLIST_ALIAS_Add" runat="server" Text="Add Alias" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_AML_WATCHLIST_ALIAS_Add_Click">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_AML_WATCHLIST_ALIAS" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model29" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ALIAS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ALIAS_NAME" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                                <ext:Column ID="Column75" runat="server" DataIndex="ALIAS_NAME" Text="Alias Name" MinWidth="300" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_AML_WATCHLIST_ALIAS" runat="server" Text="Action" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationFormMagnify" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <%--<ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>--%>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_AML_WATCHLIST_ALIAS">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_WATCHLIST_ALIAS_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Alias --%>

                    <%-- Watch List Address --%>
                    <ext:GridPanel ID="gp_AML_WATCHLIST_ADDRESS" runat="server" Title="Address(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <TopBar>
                            <ext:Toolbar ID="toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btn_AML_WATCHLIST_ADDRESS_Add" runat="server" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_AML_WATCHLIST_ADDRESS_Add_Click">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store1" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ADDRESS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_AML_COUNTRY_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="POSTAL_CODE" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                                <ext:Column ID="Column5" runat="server" DataIndex="AML_ADDRESS_TYPE_NAME" Text="Type" MinWidth="50"></ext:Column>
                                <ext:Column ID="Column1" runat="server" DataIndex="ADDRESS" Text="Address" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="FK_AML_COUNTRY_CODE" Text="Country Code" MinWidth="100" Hidden="true"></ext:Column>
                                <ext:Column ID="Column3" runat="server" DataIndex="COUNTRY_NAME" Text="Country Name" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="POSTAL_CODE" Text="Postal Code" MinWidth="100"></ext:Column>
                                <ext:CommandColumn ID="cc_AML_WATCHLIST_ADDRESS" runat="server" Text="Action" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationFormMagnify" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <%--<ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>--%>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_AML_WATCHLIST_ADDRESS">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_WATCHLIST_ADDRESS_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Address --%>

                    <%-- Watch List Identity --%>
                    <ext:GridPanel ID="gp_AML_WATCHLIST_IDENTITY" runat="server" Title="Identity(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <TopBar>
                            <ext:Toolbar ID="toolbar2" runat="server">
                                <Items>
                                    <ext:Button ID="btn_AML_WATCHLIST_IDENTITY_Add" runat="server" Text="Add Identity" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_AML_WATCHLIST_IDENTITY_Add_Click">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store2" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_IDENTITY_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_IDENTITY_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDENTITYNUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                                <ext:Column ID="Column6" runat="server" DataIndex="AML_IDENTITY_TYPE_NAME" Text="Type" MinWidth="200" Flex="1"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="IDENTITYNUMBER" Text="Identity" MinWidth="300"></ext:Column>
                                <ext:CommandColumn ID="cc_AML_WATCHLIST_IDENTITY" runat="server" Text="Action" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationFormMagnify" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <%--<ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>--%>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_AML_WATCHLIST_IDENTITY">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_WATCHLIST_IDENTITY_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Identity --%>


                    <ext:DisplayField ID="txt_CUSTOM_REMARK_1" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 1" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_CUSTOM_REMARK_2" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 2" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_CUSTOM_REMARK_3" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 3" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_CUSTOM_REMARK_4" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 4" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_CUSTOM_REMARK_5" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 5" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>

                </items>
            </ext:FormPanel>
            <%-- End of Watch List Remarks --%>
        </Items>
        <Buttons>
            <ext:Button ID="btn_AML_WATCHLIST_GENERAL_INFORMATION_Close" runat="server" Icon="Cancel" Text="Close">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_GENERAL_INFORMATION_Close_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.9, height: size.height * 0.9});" />

            <Resize Handler="#{Window_WATCHLIST_GENERAL_INFORMATION}.center()" />
        </Listeners>
    </ext:Window>
    

    <%--End of Pop Up wathclist General Information--%>

    <%-- Pop Up Window Alias --%>
    <ext:Window ID="Window_AML_WATCHLIST_ALIAS" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Maximizable="true">
        <Items>
            <ext:FormPanel ID="Window_Panel_AML_WATCHLIST_ALIAS" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_ALIAS_NAME" runat="server" FieldLabel="Alias Name" AllowBlank="false" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_AML_WATCHLIST_ALIAS_Save" runat="server" Icon="Disk" Text="Save Screening Result">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_ALIAS_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_AML_WATCHLIST_ALIAS_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_ALIAS_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.6});" />

            <Resize Handler="#{Window_AML_WATCHLIST_ALIAS}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Alias --%>

    <%-- Pop Up Window Address --%>
    <ext:Window ID="Window_AML_WATCHLIST_ADDRESS" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout" Maximizable="true">
        <Items>
            <ext:FormPanel ID="Window_Panel_AML_WATCHLIST_ADDRESS" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Content>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_ADDRESS_TYPE_CODE" AllowBlank="false" runat="server" FieldLabel="Watch List Address Type Code" LabelWidth="220" AnchorHorizontal="100%"/>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_ADDRESS" runat="server" FieldLabel="Address" LabelWidth="220" AllowBlank="false" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_ADDRESS_COUNTRY_CODE" AllowBlank="false" runat="server" FieldLabel="Watch List Address Country Code" LabelWidth="220" AnchorHorizontal="100%"/>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE" runat="server" FieldLabel="Postal Code" LabelWidth="220" AllowBlank="true" AnchorHorizontal="50%" MaxLength="8000" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:DisplayField>
                </Content>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_AML_WATCHLIST_ADDRESS_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_ADDRESS_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.6});" />

            <Resize Handler="#{Window_AML_WATCHLIST_ADDRESS}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Address --%>


    <%-- Pop Up Window Identity --%>
    <ext:Window ID="Window_AML_WATCHLIST_IDENTITY" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout" Maximizable="true">
        <Items>
            <ext:FormPanel ID="FormPanel1" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Content>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_IDENTITY_TYPE_CODE" AllowBlank="false" runat="server" FieldLabel="Identity Type" LabelWidth="150" AnchorHorizontal="100%"/>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER" runat="server" FieldLabel="Identity Number" LabelWidth="150" AllowBlank="false" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                </Content>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_AML_WATCHLIST_IDENTITY_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_IDENTITY_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.6});" />

            <Resize Handler="#{Window_AML_WATCHLIST_IDENTITY}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Identity --%>

    <%-- End of Pop Up Windows --%>

    <%-- Start of Pop Up Search CIF--%>
    <%-- Feature for searching CIF from AML_Customer --%>
    <ext:Window ID="window_SEARCH_CIF" runat="server" Modal="true" Hidden="true" BodyStyle="padding:5px" AutoScroll="true" ButtonAlign="Center" Maximizable="true">
        <Items>
            <ext:GridPanel ID="gp_SEARCH_CIF" runat="server" ClientIDMode="Static">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="StoreSEARCH_CIF" runat="server" IsPagingStore="true" RemoteFilter="true"  RemoteSort="true" 
                        OnReadData="Store_SEARCH_CIF_ReadData" RemotePaging="true" ClientIDMode="Static" PageSize="20">
                        <Model>
                            <ext:Model runat="server" ID="ModelSearchCounterPartyAccount" IDProperty="CIFNo">
                                <Fields>
                                    <ext:ModelField Name="CIFNo" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CUSTOMERNAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="DATEOFBIRTH" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Nationality" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                        </Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader_SEARCH_CIF" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumberCounterPartyAccount" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Col_CIF_NO" runat="server" DataIndex="CIFNo" Text="CIF No" Width="150"></ext:Column>
                        <ext:Column ID="Col_Name" runat="server" DataIndex="CUSTOMERNAME" Text="Name" Width="150" ></ext:Column>
                        <ext:DateColumn ID="Col_DOB" runat="server" DataIndex="DATEOFBIRTH" Text="DOB" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="col_Nationality" runat="server" DataIndex="Nationality" Text="Nationality" Flex="1"></ext:Column>
                        <ext:Column ID="col_Identity_Number" runat="server" DataIndex="Identity_Number" Text="Identity Number" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="cc_SEARCH_CIF" runat="server" Text="Action">

                            <Commands>
                                <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
                                    <ToolTip Text="Select"></ToolTip>
                                </ext:GridCommand>
                            </Commands>

                            <DirectEvents>

                                <Command OnEvent="gc_SELECT_CIF">
                                    <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="200"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="CIFNo" Value="record.data.CIFNo" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="CUSTOMERNAME" Value="record.data.CUSTOMERNAME" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="DATEOFBIRTH" Value="record.data.DATEOFBIRTH" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="Nationality" Value="record.data.Nationality" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="Identity_Number" Value="record.data.Identity_Number" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_SEARCH_CIF" runat="server" HideRefresh="false" >
                        <Items>  
                            <ext:Button runat="server" Text="Close Picker" ID="Button5">
                                <Listeners>
                                    <Click Handler="#{window_SEARCH_CIF}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.98});" />

            <Resize Handler="#{window_SEARCH_CIF}.center()" />
        </Listeners>
    </ext:Window>
    <%-- Feature for searching CIF from AML_Customer --%>
    <%--End of Pop Up Search CIF--%>
       
     <%-- Pop Up Window Detail CIF --%>
    <ext:Window ID="window_DETAIL_CIF" runat="server" Modal="true" Hidden="true" BodyStyle="padding:5px" AutoScroll="true" ButtonAlign="Center" Maximizable="true" Title="Customer Information">
        <Items>
            <ext:FormPanel ID="Window_Panel_DETAIL_CIF" runat="server" >
                <Items>
                    <ext:Panel runat="server" Layout="HBoxLayout" BodyPadding="10" Border="True" Margin="10">
                        <Items>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="display_Request_CIF" runat="server" FieldLabel="CIF">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Name" runat="server" FieldLabel="Name">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Alias" runat="server" FieldLabel="Alias Name">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Kode_Cabang" runat="server" FieldLabel="Branch Code">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Nama_Cabang" runat="server" FieldLabel="Branch Name">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Kantor_Wilayah" runat="server" FieldLabel="Regional Office">
                                    </ext:DisplayField>
                                    <%--<ext:DisplayField ID="display_Request_Identity_Number" runat="server" FieldLabel="Identity Number">
                                    </ext:DisplayField>--%>
                                    <ext:DisplayField ID="display_Request_NPWP" runat="server" FieldLabel="NPWP">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Tipe_Nasabah" runat="server" FieldLabel="Customer Type">
                                    </ext:DisplayField>
                                   <%-- <ext:DisplayField ID="display_Request_ID" runat="server" FieldLabel="Request ID">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Source" runat="server" FieldLabel="Source">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Service" runat="server" FieldLabel="Service">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Operation" runat="server" FieldLabel="Operation">
                                    </ext:DisplayField>--%>


                                </Items>
                            </ext:Panel>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="display_Request_Tempat_Lahir" runat="server" FieldLabel="Place Of Birth">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_DOB" runat="server" FieldLabel="Date Of Birth">
                                    </ext:DisplayField>
                                    <%--<ext:DisplayField ID="display_Request_Pekerjaan" runat="server" FieldLabel="Job">
                                    </ext:DisplayField>--%>
                                    <%--<ext:DisplayField ID="display_Request_Alamat" runat="server" FieldLabel="Address">
                                    </ext:DisplayField>--%>
                                    <ext:DisplayField ID="display_Request_Nationality" runat="server" FieldLabel="Nationality">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Pekerjaan" runat="server" FieldLabel="Job">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Waktu_Proses" runat="server" FieldLabel="Process Time">
                                    </ext:DisplayField>
                                    <%--<ext:DisplayField ID="display_Watchlist" runat="server" FieldLabel="Watchlist">
                                    </ext:DisplayField>--%> 
                                    <ext:DisplayField ID="display_Request_Tujuan_Dana" runat="server" FieldLabel="Tujuan Dana">
                                    </ext:DisplayField> <%--Added by Felix 18 Jun 2021--%>
                                    <ext:DisplayField ID="display_Request_Profil_Risiko_T24" runat="server" FieldLabel="Risk Profile T24" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Profil_Risiko_oneFCC" runat="server" FieldLabel="Risk Profile oneFCC">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>

                </Items>
            </ext:FormPanel>
             <%-- Grid Customer Detail Address --%>
            <ext:GridPanel ID="gp_DETAIL_CIF_ADDRESS" runat="server" Title="Address(es)" Border="True" Margin="10">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="Store_DETAIL_CIF_ADDRESS" runat="server" IsPagingStore="true" PageSize="20">
                        <Model>
                            <ext:Model runat="server" ID="mdl_DETAIL_CIF_ADDRESS">
                                <Fields>
                                    <ext:ModelField Name="CUSTOMER_ADDRESS" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ADDRESS_TYPE" Type="String"></ext:ModelField>
                                    <%--<ext:ModelField Name="KELURAHAN" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="KECAMATAN" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="KOTAKABUPATEN" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="PROPINSI" Type="String"></ext:ModelField>--%>
                                    <ext:ModelField Name="KODEPOS" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="COUNTRY_CODE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NOTES" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="rn_DETAIL_CIF_ADDRESS" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Col_Address" runat="server" DataIndex="CUSTOMER_ADDRESS" Text="Address" Width="150" ></ext:Column>
                        <ext:Column ID="Col_Address_Type" runat="server" DataIndex="ADDRESS_TYPE" Text="Address Type" Width="150" ></ext:Column>
                        <%--<ext:Column ID="Col_KELURAHAN" runat="server" DataIndex="KELURAHAN" Text="Kelurahan" Flex="1"></ext:Column>
                        <ext:Column ID="Col_KECAMATAN" runat="server" DataIndex="KECAMATAN" Text="Kecamatan" Flex="1"></ext:Column>
                        <ext:Column ID="Col_KOTAKABUPATEN" runat="server" DataIndex="KOTAKABUPATEN" Text="Kota Kabupaten" Flex="1"></ext:Column>
                        <ext:Column ID="Col_PROPINSI" runat="server" DataIndex="PROPINSI" Text="Propinsi" Flex="1"></ext:Column>--%>
                        <ext:Column ID="Col_KODEPOS" runat="server" DataIndex="KODEPOS" Text="Kode Pos" Flex="1"></ext:Column>
                        <ext:Column ID="Col_COUNTRY_CODE" runat="server" DataIndex="COUNTRY_CODE" Text="Country" Flex="1"></ext:Column>
                        <ext:Column ID="Col_NOTES" runat="server" DataIndex="NOTES" Text="Notes" Flex="1"></ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="false" >
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <%-- End of Grid Customer Detail Address --%>
            <%-- Grid Customer Detail Identity --%>
             <ext:GridPanel ID="gp_DETAIL_CIF_IDENTITY" runat="server" Title="Identity(es)" Border="True" Margin="10">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="Store_DETAIL_CIF_IDENTITY" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="mdl_DETAIL_CIF_IDENTITY">
                                <Fields>
                                    <ext:ModelField Name="CUSTOMER_IDENTITY" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="IDENTITY_TYPE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="COUNTRY_CODE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NOTES" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column16" runat="server" DataIndex="CUSTOMER_IDENTITY" Text="Identity" Flex="1"></ext:Column>
                        <ext:Column ID="Column17" runat="server" DataIndex="IDENTITY_TYPE" Text="Identity Type" Flex="1"></ext:Column>
                        <ext:Column ID="Column18" runat="server" DataIndex="COUNTRY_CODE" Text="Country" Flex="1"></ext:Column>
                        <ext:Column ID="Column19" runat="server" DataIndex="NOTES" Text="Notes" Flex="1"></ext:Column>

                    </Columns>
                </ColumnModel>
                <Plugins>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            <%-- End of Grid Customer Detail Identity --%>
        </Items>
        <Buttons>
            <ext:Button ID="Button1" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_DETAIL_CIF_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.8, height: size.height * 0.8});" />

            <Resize Handler="#{window_DETAIL_CIF}.center()" />
        </Listeners>
    </ext:Window>
        <%--Add 10-Feb-2023--%>
    <ext:Window runat="server" ID="WindowDisplayWatchlist" Title="Watchlist Information" Layout="AnchorLayout" ButtonAlign="Center" Hidden="true" Closable="false" BodyPadding="10" Maximized="true" Maximizable="true">
        <Items>
            <ext:Panel ID="pnlContent" runat="server" BodyPadding="0"  Height ="450">
                <Loader Mode="Frame" NoCache="true" runat="server" AutoLoad="false" >
                    <LoadMask ShowMask="true"></LoadMask>
                    <%--<Params>
                        <ext:Parameter Name="fileName" Mode="Raw" Value="0">
                        </ext:Parameter>
                    </Params>--%>
                </Loader>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Display_Watchlist_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_Display_Watchlist_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%--End 10-Feb-2023--%>
    <%-- End of Pop Up Window Identity --%>
</asp:Content>

