﻿
Imports System.Data
Imports System.Data.SqlClient
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDAL
Imports NawaDevBLL
Imports NawaDevDAL
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevBLL.GlobalReportFunctionBLL
Imports System.Text
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.IO

Public Module Extenders
    <System.Runtime.CompilerServices.Extension>
    Public Function ToDataTable(Of T)(collection As IEnumerable(Of T), tableName As String) As DataTable
        Dim tbl As DataTable = ToDataTable(collection)
        tbl.TableName = tableName
        Return tbl
    End Function

    <System.Runtime.CompilerServices.Extension>
    Public Function ToDataTable(Of T)(collection As IEnumerable(Of T)) As DataTable
        Dim dt As New DataTable()
        Dim tt As Type = GetType(T)
        Dim pia As PropertyInfo() = tt.GetProperties()
        'Create the columns in the DataTable
        For Each pi As PropertyInfo In pia
            Dim a =
            If(Nullable.GetUnderlyingType(pi.PropertyType), pi.PropertyType)
            dt.Columns.Add(pi.Name, If(Nullable.GetUnderlyingType(pi.PropertyType), pi.PropertyType))
            'If pi.PropertyType Is GetType(DateTime) Then

            '    ' pi.Columns.Add("columnName", TypeOf (datetime2)
            'Else

            'End If
        Next
        'Populate the table
        For Each item As T In collection
            Dim dr As DataRow = dt.NewRow()
            dr.BeginEdit()
            For Each pi As PropertyInfo In pia
                ' cek value apakah null?
                ' If pi.GetValue(item, Nothing) = Nothing Then
                If pi.GetValue(item, Nothing) Is Nothing Then
                    dr(pi.Name) = DBNull.Value
                Else
                    dr(pi.Name) = pi.GetValue(item, Nothing)
                End If

            Next
            dr.EndEdit()
            dt.Rows.Add(dr)
        Next
        Return dt
    End Function


End Module

Partial Class AML_ScreeningResultTransaction_ScreeningResultTransactionDetail
    Inherits Parent

#Region "Session"
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.ObjModule") = value
        End Set
    End Property

    Public Property listjudgementitemtransaction As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL))
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransaction") = value
        End Set
    End Property

    Public Property listjudgementitemtransactionchange As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransactionchange")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL))
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransactionchange") = value
        End Set
    End Property

    Public Property judgementheader As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementheader") = value
        End Set
    End Property
    Public Property judgementrequestforheader As NawaDevDAL.AML_SCREENING_REQUEST
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementrequestforheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_REQUEST)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementrequestforheader") = value
        End Set
    End Property
    Public Property judgementdetail As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementdetail")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementdetail") = value
        End Set
    End Property
    Public Property judgementdetailchange As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementdetailchange")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementdetailchange") = value
        End Set
    End Property
    Public Property checkerjudgeitems As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.checkerjudgeitems")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.checkerjudgeitems") = value
        End Set
    End Property
    Public Property indexofjudgementitems As Integer
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.indexofjudgementitems")
        End Get
        Set(ByVal value As Integer)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.indexofjudgementitems") = value
        End Set
    End Property

    Public Property indexofjudgementitemschange As Integer
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.indexofjudgementitemschange")
        End Get
        Set(ByVal value As Integer)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.indexofjudgementitemschange") = value
        End Set
    End Property
    Public Property dataID As String
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.dataID")
        End Get
        Set(ByVal value As String)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.dataID") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_CLASS() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.objAML_WATCHLIST_CLASS")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.objAML_WATCHLIST_CLASS") = value
        End Set
    End Property

    Public Property judgementClass() As NawaDevBLL.AMLJudgementClass
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementClass")
        End Get
        Set(ByVal value As NawaDevBLL.AMLJudgementClass)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementClass") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()
                Dim strmodule As String = Request.Params("ModuleID")
                Dim intmodule As Integer = Common.DecryptQueryString(strmodule, SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)
                If ObjModule IsNot Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If
                Dim dataStr As String = Request.Params("ID")
                If dataStr IsNot Nothing Then
                    dataID = Common.DecryptQueryString(dataStr, SystemParameterBLL.GetEncriptionKey)
                End If
                If dataID IsNot Nothing Then
                    If AML_WATCHLIST_BLL.IsExistsInApproval(ObjModule.ModuleName, dataID) Then
                        LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."

                        Panelconfirmation.Hidden = False
                        PanelInfo.Hidden = True
                    End If
                    loaddata(dataID)
                Else
                    Throw New ApplicationException("An Error Occurred While Loading the Data With Id")
                End If
                loadcolumn()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ClearSession()
        judgementheader = New AML_SCREENING_RESULT_TRANSACTION
        listjudgementitemtransaction = New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        judgementrequestforheader = New AML_SCREENING_REQUEST
        listjudgementitemtransactionchange = New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        ClearOBJDetail()
    End Sub

    Private Sub ClearOBJDetail()
        judgementdetail = New AML_SCREENING_RESULT_TRANSACTION_DETAIL
        indexofjudgementitems = Nothing
        judgementdetailchange = New AML_SCREENING_RESULT_TRANSACTION_DETAIL
        indexofjudgementitemschange = Nothing
        objAML_WATCHLIST_CLASS = New AML_WATCHLIST_CLASS

        Name_Score.Clear()
        DoB_Score.Clear()
        Nationality_Score.Clear()
        Identity_Score.Clear()
        Total_Score.Clear()

        WatchListID.Clear()
        WatchListCategory.Clear()
        WatchListType.Clear()
        FullName.Clear()
        PlaceofBirth.Clear()
        DateofBirth.Clear()
        YearofBirth.Clear()
        Nationality.Clear()

        Remark1.Clear()
        Remark2.Clear()
        Remark3.Clear()
        Remark4.Clear()
        Remark5.Clear()

        RBJudgementMatch.Checked = False
        RBJudgementNotMatch.Checked = False
        JudgementComment.Clear()
    End Sub

    Private Sub loadcolumn()
        ColumnActionLocation(gridJudgementItem, CommandColumnGridJudgementItem)
    End Sub

    Private Sub loaddata(idheader As String)
        judgementheader = AMLJudgementBLL.GetAMLJudgementTransactionHeader(idheader)
        If Not IsDBNull(judgementheader.FK_AML_SCREENING_REQUEST_ID) Then
            judgementrequestforheader = AMLJudgementBLL.GetRequestForHeader(judgementheader.FK_AML_SCREENING_REQUEST_ID)
        End If
        If judgementrequestforheader IsNot Nothing Then
            'If judgementrequestforheader.REQUEST_ID IsNot Nothing Then
            '    display_Request_ID.Text = judgementrequestforheader.REQUEST_ID
            'End If
            'If judgementrequestforheader.SOURCE IsNot Nothing Then
            '    display_Request_Source.Text = judgementrequestforheader.SOURCE
            'End If
            'If judgementrequestforheader.SERVICE IsNot Nothing Then
            '    display_Request_Service.Text = judgementrequestforheader.SERVICE
            'End If
            'If judgementrequestforheader.OPERATION IsNot Nothing Then
            '    display_Request_Operation.Text = judgementrequestforheader.OPERATION
            'End If
            'If judgementheader.RESPONSE_ID IsNot Nothing Then
            '    display_Response_ID.Text = judgementheader.RESPONSE_ID
            'End If
            'If judgementrequestforheader.CreatedDate IsNot Nothing Then
            '    display_Request_Date.Text = judgementrequestforheader.CreatedDate.Value.ToString("dd-MMM-yyyy, hh:mm:ss tt")
            'End If
            If judgementheader.FK_AML_RESPONSE_CODE_ID IsNot Nothing Then
                If judgementheader.FK_AML_RESPONSE_CODE_ID = 1 Then
                    display_Response_Code.Text = "1 - Found"
                ElseIf judgementheader.FK_AML_RESPONSE_CODE_ID = 2 Then
                    display_Response_Code.Text = "2 - Not Found"
                Else
                    display_Response_Code.Text = "3 - Error"
                End If
            End If
            'If judgementrequestforheader.RESPONSE_DESCRIPTION IsNot Nothing Then
            '    display_Response_Description.Text = judgementrequestforheader.RESPONSE_DESCRIPTION
            'End If
            'If judgementrequestforheader.RESPONSE_DATE IsNot Nothing Then
            '    display_Response_Date.Text = judgementrequestforheader.RESPONSE_DATE.Value.ToString("dd-MMM-yyyy, hh:mm:ss tt")
            'End If
            If judgementrequestforheader.CIF_NO IsNot Nothing Then
                display_Request_CIF.Text = judgementrequestforheader.CIF_NO
            End If
            If judgementrequestforheader.NAME IsNot Nothing Then
                display_Request_Name.Text = judgementrequestforheader.NAME
            End If
            If judgementrequestforheader.DOB IsNot Nothing Then
                display_Request_DOB.Text = judgementrequestforheader.DOB.Value.Date.ToString("dd-MMM-yyyy")
            End If
            If judgementrequestforheader.NATIONALITY IsNot Nothing Then
                display_Request_Nationality.Text = judgementrequestforheader.NATIONALITY
            End If
            If judgementrequestforheader.IDENTITY_NUMBER IsNot Nothing Then
                display_Request_Identity_Number.Text = judgementrequestforheader.IDENTITY_NUMBER
            End If
            If judgementheader.FK_DebitOrCredit_Code IsNot Nothing Then
                If judgementheader.FK_DebitOrCredit_Code = "D" Then
                    DisplayFieldDebitorCredit.Text = "Debit"
                ElseIf judgementheader.FK_DebitOrCredit_Code = "C" Then
                    DisplayFieldDebitorCredit.Text = "Credit"
                Else
                    DisplayFieldDebitorCredit.Text = Nothing
                End If
            End If
            If judgementheader.TRANSACTION_CODE IsNot Nothing Then
                DisplayFieldTransactionCode.Text = judgementheader.TRANSACTION_CODE
            End If
            If judgementheader.TRANSACTION_DATE IsNot Nothing Then
                DisplayFieldTransactionDate.Text = judgementheader.TRANSACTION_DATE.Value.Date.ToString("dd-MMM-yyyy")
            End If
            If judgementheader.TRANSACTION_NUMBER IsNot Nothing Then
                DisplayFieldTransactionNumber.Text = judgementheader.TRANSACTION_NUMBER
            End If

            Dim transaksi = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementTransaction(judgementheader.TRANSACTION_NUMBER)
                If transaksi IsNot Nothing Then
                    If transaksi.ACCOUNT_NO IsNot Nothing Then
                        DisplayFieldAccountNo.Text = transaksi.ACCOUNT_NO
                    End If
                    If transaksi.TRANSACTION_AMOUNT IsNot Nothing Then
                        DisplayFieldNominal.Text = transaksi.TRANSACTION_AMOUNT
                    End If
                    If transaksi.FK_AML_CURRENCY_CODE IsNot Nothing Then
                        Dim currency = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementCurrency(transaksi.FK_AML_CURRENCY_CODE)
                        If currency IsNot Nothing Then
                            If currency.CURRENCY_NAME IsNot Nothing Then
                                DisplayFieldCurrency.Text = currency.CURRENCY_NAME
                            End If
                        End If

                    End If

                    If transaksi.CIFNO_LAWAN IsNot Nothing And transaksi.WICNo_LAWAN Is Nothing Then
                        DisplayFieldCifNoCounterParty.Hidden = False
                        DisplayFieldAccountNoCounterParty.Hidden = False
                        DisplayFieldCustomerNameCounterParty.Hidden = False
                        DisplayFieldDOBCounterParty.Hidden = False
                        DisplayFieldNationalityCounterParty.Hidden = False
                        DisplayFieldIdentityNumCounterParty.Hidden = False
                        DisplayFieldCifNoCounterParty.Text = transaksi.CIFNO_LAWAN
                        Dim customers As AML_CUSTOMER = NawaDevBLL.AMLReportRiskRating.GetCustomerBYCIFNO(transaksi.CIFNO_LAWAN)
                        If transaksi.ACCOUNT_NO_LAWAN IsNot Nothing Then
                            DisplayFieldAccountNoCounterParty.Text = transaksi.ACCOUNT_NO_LAWAN
                        End If
                    If customers IsNot Nothing Then
                        If customers.CUSTOMERNAME IsNot Nothing Then
                            DisplayFieldCustomerNameCounterParty.Text = customers.CUSTOMERNAME
                        End If
                        If customers.FK_AML_CITIZENSHIP_CODE IsNot Nothing Then
                            Dim country As AML_COUNTRY = NawaDevBLL.AMLReportRiskRating.GetCountryByFK(customers.FK_AML_CITIZENSHIP_CODE)
                            If country IsNot Nothing Then
                                DisplayFieldNationalityCounterParty.Text = country.AML_COUNTRY_Name
                            End If
                        End If
                        If customers.DATEOFBIRTH IsNot Nothing Then
                            DisplayFieldDOBCounterParty.Text = customers.DATEOFBIRTH.Value.Date.ToString("dd-MMM-yyyy")
                        End If
                        Dim identity As AML_CUSTOMER_IDENTITY = NawaDevBLL.AMLReportRiskRating.GetIdentityNumberByCIFNo(customers.CIFNo)
                        If identity IsNot Nothing Then
                            DisplayFieldIdentityNumberCounductor.Text = identity.CUSTOMER_IDENTITY_NUMBER
                        End If
                    End If
                    Dim transresult = AMLJudgementBLL.GetAMLJudgementTransactionHeaderByCIFNO(transaksi.CIFNO_LAWAN)
                    If transresult IsNot Nothing Then
                        listjudgementitemtransaction = AMLJudgementBLL.GetAMLJudgementTransactionList(transresult.PK_AML_SCREENING_RESULT_TRANSACTION_ID)
                        If listjudgementitemtransaction IsNot Nothing Then
                            BindJudgement(Store2, listjudgementitemtransaction)
                        End If
                    End If

                ElseIf transaksi.CIFNO_LAWAN Is Nothing And transaksi.WICNo_LAWAN IsNot Nothing Then
                        DisplayField1WicNoCounterParty.Hidden = False
                        DisplayField1WicFirstNameCounterParty.Hidden = False
                        DisplayFieldWicMiddleNameCounterParty.Hidden = False
                        DisplayFieldWicLastNameCounterParty.Hidden = False
                        DisplayFieldDOBCounterParty.Hidden = False
                        DisplayFieldNationalityCounterParty.Hidden = False
                        DisplayFieldIdentityNumCounterParty.Hidden = False
                        DisplayField1WicNoCounterParty.Text = transaksi.WICNo_LAWAN
                        Dim watchlist = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementWatchlist(transaksi.WICNo_LAWAN)
                    If watchlist IsNot Nothing Then
                        If watchlist.INDV_First_Name IsNot Nothing Then
                            DisplayField1WicFirstNameCounterParty.Text = watchlist.INDV_First_Name
                        End If
                        If watchlist.INDV_Middle_Name IsNot Nothing Then
                            DisplayFieldWicMiddleNameCounterParty.Text = watchlist.INDV_Middle_Name
                        End If
                        If watchlist.INDV_Last_Name IsNot Nothing Then
                            DisplayFieldWicLastNameCounterParty.Text = watchlist.INDV_Last_Name
                        End If
                        If watchlist.INDV_BirthDate IsNot Nothing Then
                            DisplayFieldDOBCounterParty.Text = watchlist.INDV_BirthDate.Value.Date.ToString("dd-MMM-yyyy")
                        End If
                        If watchlist.INDV_Nationality1 IsNot Nothing Or watchlist.INDV_Nationality2 IsNot Nothing Or watchlist.INDV_Nationality3 IsNot Nothing Then
                            DisplayFieldNationalityCounterParty.Text = watchlist.INDV_Nationality1 + "," + watchlist.INDV_Nationality2 + "," + watchlist.INDV_Nationality3
                        End If
                        If watchlist.INDV_ID_Number IsNot Nothing Then
                            DisplayFieldIdentityNumCounterParty.Text = watchlist.INDV_ID_Number
                        End If
                    End If
                    Dim transresult = AMLJudgementBLL.GetAMLJudgementTransactionHeaderByWICNO(transaksi.WICNo_LAWAN)
                    If transresult IsNot Nothing Then
                        listjudgementitemtransaction = AMLJudgementBLL.GetAMLJudgementTransactionList(transresult.PK_AML_SCREENING_RESULT_TRANSACTION_ID)
                        If listjudgementitemtransaction IsNot Nothing Then
                            BindJudgement(Store2, listjudgementitemtransaction)
                        End If
                    End If


                End If

                    If IsNumeric(transaksi.WICNo_Conductor1) = True Then
                        DisplayFieldCIFNoCounductor.Hidden = False
                        DisplayFieldAccountNoCounductor.Hidden = False
                        DisplayFieldCustomerNameCounductor.Hidden = False
                        DisplayFieldDOBCounductor.Hidden = False
                        DisplayFieldNationalityCounductor.Hidden = False
                        DisplayFieldIdentityNumberCounductor.Hidden = False
                        DisplayFieldCIFNoCounductor.Text = transaksi.WICNo_Conductor1
                        Dim customers As AML_CUSTOMER = NawaDevBLL.AMLReportRiskRating.GetCustomerBYCIFNO(transaksi.WICNo_Conductor1)
                        If transaksi.ACCOUNT_NO_LAWAN IsNot Nothing Then
                            DisplayFieldAccountNoCounductor.Text = transaksi.ACCOUNT_NO_LAWAN
                        End If
                    If customers IsNot Nothing Then
                        If customers.CUSTOMERNAME IsNot Nothing Then
                            DisplayFieldCustomerNameCounductor.Text = customers.CUSTOMERNAME
                        End If
                        If customers.FK_AML_CITIZENSHIP_CODE IsNot Nothing Then
                            Dim country As AML_COUNTRY = NawaDevBLL.AMLReportRiskRating.GetCountryByFK(customers.FK_AML_CITIZENSHIP_CODE)
                            If country IsNot Nothing Then
                                DisplayFieldNationalityCounductor.Text = country.AML_COUNTRY_Name
                            End If
                        End If
                        If customers.DATEOFBIRTH IsNot Nothing Then
                            DisplayFieldDOBCounductor.Text = customers.DATEOFBIRTH.Value.Date.ToString("dd-MMM-yyyy")
                        End If
                        Dim identity As AML_CUSTOMER_IDENTITY = NawaDevBLL.AMLReportRiskRating.GetIdentityNumberByCIFNo(customers.CIFNo)
                        If identity IsNot Nothing Then
                            DisplayFieldIdentityNumberCounductor.Text = identity.CUSTOMER_IDENTITY_NUMBER
                        End If


                    End If
                    Dim transresult = AMLJudgementBLL.GetAMLJudgementTransactionHeaderByCIFNO(transaksi.WICNo_Conductor1)
                    If transresult IsNot Nothing Then
                        listjudgementitemtransaction = AMLJudgementBLL.GetAMLJudgementTransactionList(transresult.PK_AML_SCREENING_RESULT_TRANSACTION_ID)
                        If listjudgementitemtransaction IsNot Nothing Then
                            BindJudgement(Store1, listjudgementitemtransaction)
                        End If
                    End If

                ElseIf IsNumeric(transaksi.WICNo_Conductor1) = False Then
                        DisplayFieldWICNoCounductor.Hidden = False
                        DisplayField1WicFirstNameCounterParty.Hidden = False
                        DisplayFieldWICMiddleNameCounductor.Hidden = False
                        DisplayFieldWICLastNameCounductor.Hidden = False
                        DisplayFieldDOBCounductor.Hidden = False
                        DisplayFieldNationalityCounductor.Hidden = False
                        DisplayFieldIdentityNumberCounductor.Hidden = False
                        DisplayFieldWICNoCounductor.Text = transaksi.WICNo_Conductor1
                        Dim watchlist = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementWatchlist(transaksi.WICNo_Conductor1)
                    If watchlist IsNot Nothing Then
                        If watchlist.INDV_First_Name IsNot Nothing Then
                            DisplayFieldWICFirstNameCounductor.Text = watchlist.INDV_First_Name
                        End If
                        If watchlist.INDV_Middle_Name IsNot Nothing Then
                            DisplayFieldWICMiddleNameCounductor.Text = watchlist.INDV_Middle_Name
                        End If
                        If watchlist.INDV_Last_Name IsNot Nothing Then
                            DisplayFieldWICLastNameCounductor.Text = watchlist.INDV_Last_Name
                        End If
                        If watchlist.INDV_BirthDate IsNot Nothing Then
                            DisplayFieldDOBCounductor.Text = watchlist.INDV_BirthDate.Value.Date.ToString("dd-MMM-yyyy")
                        End If
                        If watchlist.INDV_Nationality1 IsNot Nothing Or watchlist.INDV_Nationality2 IsNot Nothing Or watchlist.INDV_Nationality3 IsNot Nothing Then
                            DisplayFieldNationalityCounductor.Text = watchlist.INDV_Nationality1 + "," + watchlist.INDV_Nationality2 + "," + watchlist.INDV_Nationality3
                        End If
                        If watchlist.INDV_ID_Number IsNot Nothing Then
                            DisplayFieldIdentityNumberCounductor.Text = watchlist.INDV_ID_Number
                        End If
                    End If
                    Dim transresult = AMLJudgementBLL.GetAMLJudgementTransactionHeaderByWICNO(transaksi.WICNo_Conductor1)
                    If transresult IsNot Nothing Then
                        listjudgementitemtransaction = AMLJudgementBLL.GetAMLJudgementTransactionList(transresult.PK_AML_SCREENING_RESULT_TRANSACTION_ID)
                        If listjudgementitemtransaction IsNot Nothing Then
                            BindJudgement(Store1, listjudgementitemtransaction)
                        End If
                    End If
                End If

                End If


            End If
        listjudgementitemtransaction = AMLJudgementBLL.GetAMLJudgementTransactionList(idheader)
        If listjudgementitemtransaction IsNot Nothing Then
                BindJudgement(judgement_Item, listjudgementitemtransaction)
            End If

    End Sub

    Private Sub BindJudgement(store As Store, list As List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            objtable.Columns.Add(New DataColumn("CATEGORY_NAME", GetType(String)))
            objtable.Columns.Add(New DataColumn("TYPE_NAME", GetType(String)))
            objtable.Columns.Add(New DataColumn("ISMATCH", GetType(String)))

            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    If Not IsDBNull(item("FK_AML_WATCHLIST_CATEGORY_ID")) Then
                        item("CATEGORY_NAME") = AMLJudgementBLL.GetWatchlistCategoryByID(item("FK_AML_WATCHLIST_CATEGORY_ID"))
                    Else
                        item("CATEGORY_NAME") = ""
                    End If
                    If Not IsDBNull(item("FK_AML_WATCHLIST_TYPE_ID")) Then
                        item("TYPE_NAME") = AMLJudgementBLL.GetWatchlistTypeByID(item("FK_AML_WATCHLIST_TYPE_ID"))
                    Else
                        item("TYPE_NAME") = ""
                    End If

                    If Not IsDBNull(item("JUDGEMENT_ISMATCH")) Then
                        If item("JUDGEMENT_ISMATCH") Then
                            item("ISMATCH") = "Match"
                        Else
                            item("ISMATCH") = "Not Match"
                        End If
                    Else
                        item("ISMATCH") = ""
                    End If
                Next
            End If
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ALIAS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS)
        gridAliasInfo.GetStore().DataSource = objtable
        gridAliasInfo.GetStore().DataBind()
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ADDRESS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS)

        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
                If objCountry IsNot Nothing Then
                    item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
                Else
                    item("COUNTRY_NAME") = Nothing
                End If

                Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
                If objAddressType IsNot Nothing Then
                    item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
                Else
                    item("AML_ADDRESS_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gridAddressInfo.GetStore().DataSource = objtable
        gridAddressInfo.GetStore().DataBind()
    End Sub
    Protected Sub Bind_AML_WATCHLIST_IDENTITY()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY)

        objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
                If objIDENTITYType IsNot Nothing Then
                    item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
                Else
                    item("AML_IDENTITY_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gridIdentityInfo.GetStore().DataSource = objtable
        gridIdentityInfo.GetStore().DataBind()
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandJudgementItem(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                ClearOBJDetail()
                WindowJudge.Hidden = False
                loadobjectJudgement(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
            ElseIf e.ExtraParams(1).Value = "Delete" Then
            ElseIf e.ExtraParams(1).Value = "Edit" Then
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub loadobjectJudgement(id As String)
        judgementdetail = listjudgementitemtransaction.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = id).FirstOrDefault
        If judgementdetail IsNot Nothing Then
            indexofjudgementitems = listjudgementitemtransaction.IndexOf(judgementdetail)
            If judgementdetail.JUDGEMENT_ISMATCH IsNot Nothing Then
                If judgementdetail.JUDGEMENT_ISMATCH Then
                    RBJudgementMatch.Checked = True
                Else
                    RBJudgementNotMatch.Checked = True
                End If
            End If
            If judgementdetail.JUDGEMENT_COMMENT IsNot Nothing Then
                JudgementComment.Text = judgementdetail.JUDGEMENT_COMMENT
            End If
            If judgementdetail.MATCH_SCORE_NAME IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_NAME = 0 Then
                    Name_Score.Text = "-"
                Else
                    Name_Score.Text = judgementdetail.MATCH_SCORE_NAME
                End If
            End If
            If judgementdetail.MATCH_SCORE_DOB IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_DOB = 0 Then
                    DoB_Score.Text = "-"
                Else
                    DoB_Score.Text = judgementdetail.MATCH_SCORE_DOB
                End If
            End If
            If judgementdetail.MATCH_SCORE_NATIONALITY IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_NATIONALITY = 0 Then
                    Nationality_Score.Text = "-"
                Else
                    Nationality_Score.Text = judgementdetail.MATCH_SCORE_NATIONALITY
                End If
            End If
            If judgementdetail.MATCH_SCORE_IDENTITY_NUMBER IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_IDENTITY_NUMBER = 0 Then
                    Identity_Score.Text = "-"
                Else
                    Identity_Score.Text = judgementdetail.MATCH_SCORE_IDENTITY_NUMBER
                End If
            End If
            If judgementdetail.MATCH_SCORE IsNot Nothing Then
                If judgementdetail.MATCH_SCORE = 0 Then
                    Total_Score.Text = "-"
                Else
                    Total_Score.Text = judgementdetail.MATCH_SCORE
                End If
            End If

            objAML_WATCHLIST_CLASS = AML_WATCHLIST_BLL.GetWatchlistClassByID(judgementdetail.FK_AML_WATCHLIST_ID)
            If objAML_WATCHLIST_CLASS IsNot Nothing Then
                With objAML_WATCHLIST_CLASS.objAML_WATCHLIST
                    WatchListID.Text = .PK_AML_WATCHLIST_ID
                    If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                        Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                        If objWatchlistCategory IsNot Nothing Then
                            WatchListCategory.Text = objWatchlistCategory.CATEGORY_NAME
                        End If
                    End If
                    If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                        Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                        If objWatchlistType IsNot Nothing Then
                            WatchListType.Text = objWatchlistType.TYPE_NAME
                        End If
                    End If
                    If .NAME IsNot Nothing Then
                        FullName.Text = .NAME
                    End If
                    If .BIRTH_PLACE IsNot Nothing Then
                        PlaceofBirth.Text = .BIRTH_PLACE
                    End If
                    If .DATE_OF_BIRTH IsNot Nothing Then
                        DateofBirth.Text = .DATE_OF_BIRTH
                    End If
                    If .YOB IsNot Nothing Then
                        YearofBirth.Text = .YOB
                    End If
                    If .NATIONALITY IsNot Nothing Then
                        Nationality.Text = .NATIONALITY
                    End If
                    If .CUSTOM_REMARK_1 IsNot Nothing Then
                        Remark1.Text = .CUSTOM_REMARK_1
                    End If
                    If .CUSTOM_REMARK_2 IsNot Nothing Then
                        Remark2.Text = .CUSTOM_REMARK_2
                    End If
                    If .CUSTOM_REMARK_3 IsNot Nothing Then
                        Remark3.Text = .CUSTOM_REMARK_3
                    End If
                    If .CUSTOM_REMARK_4 IsNot Nothing Then
                        Remark4.Text = .CUSTOM_REMARK_4
                    End If
                    If .CUSTOM_REMARK_5 IsNot Nothing Then
                        Remark5.Text = .CUSTOM_REMARK_5
                    End If
                End With
                Bind_AML_WATCHLIST_ALIAS()
                Bind_AML_WATCHLIST_ADDRESS()
                Bind_AML_WATCHLIST_IDENTITY()
            End If
        End If
    End Sub

    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            'judgementClass = New AMLJudgementClass
            'With judgementClass
            '    .AMLJudgementHeader = judgementheader
            '    .AMLListResult = listjudgementitemtransactionchange
            'End With
            'checkerjudgeitems = New AML_SCREENING_RESULT_TRANSACTION_DETAIL
            'checkerjudgeitems = listjudgementitemtransaction.Where(Function(x) x.JUDGEMENT_ISMATCH Is Nothing).FirstOrDefault
            'If listjudgementitemtransactionchange.Count = 0 Then
            '    Throw New ApplicationException("No Data Has Changed, Please Change Some Data Before Saving or Click the Cancel Button to Cancel the Process")
            'ElseIf checkerjudgeitems IsNot Nothing Then
            '    Throw New ApplicationException("There are Some Data which have not been Judged, Please Judge All Data Before Saving")
            'End If
            'If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not ObjModule.IsUseApproval Then
            '    'tanpa approval
            '    AMLJudgementBLL.SaveJudgementTanpaApproval(judgementClass, ObjModule)
            '    PanelInfo.Hide()
            '    Panelconfirmation.Show()
            '    LblConfirmation.Text = "Data Saved into Database"
            'Else
            '    'with approval
            '    AMLJudgementBLL.SaveJudgementApproval(judgementClass, ObjModule)
            '    PanelInfo.Hide()
            '    Panelconfirmation.Show()
            '    LblConfirmation.Text = "Data Saved into Pending Approval"
            'End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub loadjudgementitemchange(items As AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        judgementdetailchange = listjudgementitemtransactionchange.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = items.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID).FirstOrDefault
        If judgementdetailchange IsNot Nothing Then
            indexofjudgementitemschange = listjudgementitemtransactionchange.IndexOf(judgementdetailchange)
            listjudgementitemtransactionchange(indexofjudgementitemschange) = items
        Else
            listjudgementitemtransactionchange.Add(items)
        End If
    End Sub

    'Protected Sub rgjudgement_click(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim objradio As Ext.Net.Radio = CType(sender, Ext.Net.Radio)
    '        If JudgementComment.Text Is Nothing Or JudgementComment.Text = "" Or JudgementComment.Text = "This Data is Judged Match" Or JudgementComment.Text = "This Data is Judged Not Match" Then
    '            If objradio.InputValue = "1" And objradio.Checked Then
    '                JudgementComment.Text = "This Data is Judged Match"
    '            ElseIf objradio.InputValue = "0" And objradio.Checked Then
    '                JudgementComment.Text = "This Data is Judged Not Match"
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub BtnJudgementSave_Click(sender As Object, e As DirectEventArgs)
        Try
            If RBJudgementMatch.Checked Or RBJudgementNotMatch.Checked Then
                If judgementdetail IsNot Nothing Then
                    If judgementdetail.JUDGEMENT_ISMATCH <> RBJudgementMatch.Checked Or judgementdetail.JUDGEMENT_COMMENT <> JudgementComment.Text Or judgementdetail.JUDGEMENT_ISMATCH Is Nothing Then
                        If RBJudgementMatch.Checked Then
                            judgementdetail.JUDGEMENT_ISMATCH = True
                        Else
                            judgementdetail.JUDGEMENT_ISMATCH = False
                        End If
                        If JudgementComment.Text IsNot Nothing Then
                            judgementdetail.JUDGEMENT_COMMENT = JudgementComment.Text
                        End If
                        If Common.SessionCurrentUser.UserName IsNot Nothing Then
                            judgementdetail.JUDGEMENT_BY = Common.SessionCurrentUser.UserID
                        End If
                        judgementdetail.JUDGEMENT_DATE = Now()
                        If judgementdetail.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID <> Nothing And listjudgementitemtransaction IsNot Nothing Then
                            loadjudgementitemchange(judgementdetail)
                            listjudgementitemtransaction(indexofjudgementitems) = judgementdetail
                            BindJudgement(judgement_Item, listjudgementitemtransaction)
                        Else
                            Throw New ApplicationException("Something Wrong While Saving Data")
                        End If
                    Else
                        Throw New ApplicationException("No Data Has Changed, Please Change Some Data Before Saving or Click the Cancel Button to Cancel the Process")
                    End If
                End If
            Else
                Throw New ApplicationException("Must Conduct Judgment Before Saving Data")
            End If
            WindowJudge.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnJudgementCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowJudge.Hidden = True
            ClearOBJDetail()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class
