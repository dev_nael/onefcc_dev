﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AMLJudgementView.aspx.vb" Inherits="AMLScreeningJudgment_AMLJudgmentView" %>

<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <script type="text/javascript">
         var columnAutoResize = function (grid) {
             App.GridpanelView.columns.forEach(function (col) {
                 if (col.xtype != 'commandcolumn') {

                     col.autoSize();
                 }
             });
         };

         var prepareCommandCollection = function (grid, toolbar, rowIndex, record) {
             var EditButton = toolbar.items.get(1);
             var wf = record.data.STATUS
             if (EditButton != undefined) {
                 if (wf == "Already In Approval" || wf == "Closed") {
                     EditButton.setDisabled(true)
                 }
             };
         };

         Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
             return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
         };
         Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
             return {
                 type: "string",
                 op: "*",
                 value: value
             };
         };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:GridPanel ID="GridpanelView" ClientIDMode="Static" runat="server" Title="Title" Width="2000">
        <View>
            <ext:GridView runat="server" EnableTextSelection="true">
            </ext:GridView>
        </View>
        <Store>
            <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadData">

                <Sorters>
                    <%--<ext:DataSorter Property="" Direction="ASC" />--%>
                </Sorters>
                <Proxy>
                    <ext:PageProxy />
                </Proxy>
            </ext:Store>
        </Store>
        <Plugins>
            <%--<ext:GridFilters ID="GridFilters1" runat="server" />--%>
            <ext:FilterHeader ID="grdiheaderfilter" runat="server" Remote="true"></ext:FilterHeader>

        </Plugins>
        <BottomBar>

            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />

        </BottomBar>
               <DockedItems>
            <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                <Items>

                    <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export :">

                        <Items>
                            <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                            <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                        </Items>

                    </ext:ComboBox>
                    <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" AutoPostBack="true" OnClick="ExportExcel" ClientIDMode="Static" />
                    <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page" AutoPostBack="true" OnClick="ExportAllExcel" />
                    <%--
                    <ext:Button ID="Button1" runat="server" Text="Print Current Page" Icon="Printer" Handler="this.up('grid').print();" />
                    --%>
                    <ext:Button ID="BtnAdd" runat="server" Visible="true" Text="Add New Record" Icon="Add" Handler="NawadataDirect.BtnAdd_Click()" />
                    <ext:Button ID="AdvancedFilter" runat="server" Text="Advanced Filter" Icon="Add" Handler="NawadataDirect.BtnAdvancedFilter_Click()"></ext:Button>
                </Items>
            </ext:Toolbar>
             <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true" Dock="Top" Hidden="true">
                <Items>
                    <ext:HyperlinkButton ID="HyperlinkButton1" runat="server" Text="Clear Advanced Filter">
                        <DirectEvents>
                            <Click OnEvent="btnClear_Click"></Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                    <ext:Label ID="LblAdvancedFilter" runat="server" Text="">
                    </ext:Label>
                </Items>
            </ext:Toolbar>
             </DockedItems>
        <Listeners>
            <ViewReady Handler="columnAutoResize(this);
                    this.getStore().on('load', Ext.bind(columnAutoResize, null, [this]));"
                Delay="10" />
        </Listeners>
    </ext:GridPanel>
        <ext:Panel ID="Panel1" runat="server" Hidden="true">
        <Content>
            <uc1:AdvancedFilter runat="server" ID="AdvancedFilter1" />
        </Content>
        <Items>
        </Items>
    </ext:Panel>
</asp:Content>


