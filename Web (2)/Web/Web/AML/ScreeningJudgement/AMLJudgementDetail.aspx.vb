﻿Imports System.Data
Imports System.Data.SqlClient
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDAL
Imports NawaDevBLL
Imports NawaDevDAL
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevBLL.GlobalReportFunctionBLL
Imports System.Text
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.IO
Imports System.Globalization
Imports System.Data.Entity

Partial Class AML_ScreeningJudgement_AMLJudgementDetail
    Inherits Parent

#Region "Session"
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.ObjModule") = value
        End Set
    End Property

    Public Property listjudgementitem As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.listjudgementitem")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL))
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.listjudgementitem") = value
        End Set
    End Property

    Public Property listjudgementitemchange As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.listjudgementitemchange")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL))
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.listjudgementitemchange") = value
        End Set
    End Property

    Public Property judgementheader As NawaDevDAL.AML_SCREENING_RESULT
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.judgementheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT)
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.judgementheader") = value
        End Set
    End Property
    Public Property judgementrequestforheader As NawaDevDAL.AML_SCREENING_REQUEST
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.judgementrequestforheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_REQUEST)
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.judgementrequestforheader") = value
        End Set
    End Property
    Public Property judgementdetail As NawaDevDAL.AML_SCREENING_RESULT_DETAIL
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.judgementdetail")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.judgementdetail") = value
        End Set
    End Property
    Public Property judgementdetailchange As NawaDevDAL.AML_SCREENING_RESULT_DETAIL
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.judgementdetailchange")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.judgementdetailchange") = value
        End Set
    End Property
    Public Property checkerjudgeitems As NawaDevDAL.AML_SCREENING_RESULT_DETAIL
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.checkerjudgeitems")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.checkerjudgeitems") = value
        End Set
    End Property
    Public Property indexofjudgementitems As Integer
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.indexofjudgementitems")
        End Get
        Set(ByVal value As Integer)
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.indexofjudgementitems") = value
        End Set
    End Property

    Public Property indexofjudgementitemschange As Integer
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.indexofjudgementitemschange")
        End Get
        Set(ByVal value As Integer)
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.indexofjudgementitemschange") = value
        End Set
    End Property
    Public Property dataID As String
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.dataID")
        End Get
        Set(ByVal value As String)
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.dataID") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_CLASS() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.objAML_WATCHLIST_CLASS")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.objAML_WATCHLIST_CLASS") = value
        End Set
    End Property

    Public Property judgementClass() As NawaDevBLL.AMLJudgementClass
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.judgementClass")
        End Get
        Set(ByVal value As NawaDevBLL.AMLJudgementClass)
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.judgementClass") = value
        End Set
    End Property

    'daniel 20210419
    Public Property listwatchlistalreadymatch As List(Of NawaDevDAL.AML_SCREENING_CUSTOMER_MATCH)
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.listcutomeralreadyinwatchlist")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_CUSTOMER_MATCH))
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.listcutomeralreadyinwatchlist") = value
        End Set
    End Property

    Public Property listAffecteddatafromlistmatch As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.listAffecteddatafromlistmatch")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL))
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.listAffecteddatafromlistmatch") = value
        End Set
    End Property
    'end daniel 20210419
    'Public Property dataamlcustomer As AML_CUSTOMER
    '    Get
    '        Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.dataamlcustomer")
    '    End Get
    '    Set(ByVal value As AML_CUSTOMER)
    '        Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.dataamlcustomer") = value
    '    End Set
    'End Property
#End Region

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()
                Dim strmodule As String = Request.Params("ModuleID")
                Dim intmodule As Integer = Common.DecryptQueryString(strmodule, SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)
                If ObjModule IsNot Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If
                Dim dataStr As String = Request.Params("ID")
                If dataStr IsNot Nothing Then
                    dataID = Common.DecryptQueryString(dataStr, SystemParameterBLL.GetEncriptionKey)
                End If
                If dataID IsNot Nothing Then
                    If AML_WATCHLIST_BLL.IsExistsInApproval(ObjModule.ModuleName, dataID) Then
                        LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."

                        Panelconfirmation.Hidden = False
                        PanelInfo.Hidden = True
                    End If

                    'LoadData(dataID) '' 15-Jun-2023 Adi : Ini Load data by PK_AML_SCREENING_RESULT_ID
                    '15-Jun-2023 Adi : Ubah menjadi Load Data By CIF
                    LoadDataByCIF(dataID)

                Else
                    Throw New ApplicationException("An Error Occurred While Loading the Data With Id")
                End If

                'Comment dulu, jangan lupa uncomment
                LoadColumn()
                Dim objcommandcol As Ext.Net.CommandColumn = gridJudgementItem.ColumnModel.Columns.Find(Function(x) x.ID = "CommandColumnGridJudgementItem")
                objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ClearSession()
        Try
            judgementheader = New AML_SCREENING_RESULT
            listjudgementitem = New List(Of AML_SCREENING_RESULT_DETAIL)
            judgementrequestforheader = New AML_SCREENING_REQUEST
            listjudgementitemchange = New List(Of AML_SCREENING_RESULT_DETAIL)
            listwatchlistalreadymatch = New List(Of NawaDevDAL.AML_SCREENING_CUSTOMER_MATCH)
            listAffecteddatafromlistmatch = New List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            'dataamlcustomer = New AML_CUSTOMER
            IDWatchlistEncrypted = Nothing
            IDModuleWatchlistEncrypted = Nothing
            ClearOBJDetail()
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Private Sub ClearOBJDetail()
        Try
            judgementdetail = New AML_SCREENING_RESULT_DETAIL
            indexofjudgementitems = Nothing
            judgementdetailchange = New AML_SCREENING_RESULT_DETAIL
            indexofjudgementitemschange = Nothing
            objAML_WATCHLIST_CLASS = New AML_WATCHLIST_CLASS

            Name_Score.Clear()
            DoB_Score.Clear()
            Nationality_Score.Clear()
            Identity_Score.Clear()
            Total_Score.Clear()

            WatchListID.Clear()
            WatchListCategory.Clear()
            WatchListType.Clear()
            FullName.Clear()
            PlaceofBirth.Clear()
            DateofBirth.Clear()
            YearofBirth.Clear()
            Nationality.Clear()

            Remark1.Clear()
            Remark2.Clear()
            Remark3.Clear()
            Remark4.Clear()
            Remark5.Clear()

            LblDisplayIsMatch.Text = ""
            JudgementComment.Text = ""
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Private Sub LoadColumn()
        Try
            ColumnActionLocation(gridJudgementItem, CommandColumnGridJudgementItem)
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Private Sub LoadData(idheader As String)
        '31-May-2023 Adi : Ubah jangan pakai DAL untuk mendapatkan data customer
        Try
            judgementheader = AMLJudgementBLL.GetAMLJudgementHeader(idheader)
            If Not IsDBNull(judgementheader.FK_AML_SCREENING_REQUEST_ID) Then
                judgementrequestforheader = AMLJudgementBLL.GetRequestForHeader(judgementheader.FK_AML_SCREENING_REQUEST_ID)
            End If
            'daniel 20210419
            If Not IsDBNull(judgementheader.CIF_NO) Then
                listwatchlistalreadymatch = AMLJudgementBLL.GetWatchlistAlreadyMatchByCIF(judgementheader.CIF_NO)

                Dim strQuery As String = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & judgementheader.CIF_NO & "'"
                Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

                '2023-08-04, Nael
                Dim strQueryVw As String = "SELECT TOP 1 * FROM Vw_AML_Judgement WHERE CIF_NO='" & judgementheader.CIF_NO & "'"
                Dim drVwCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(
                    NawaDAL.SQLHelper.strConnectionString,
                    CommandType.Text,
                    strQueryVw
                )

                If drCustomer IsNot Nothing Then
                    If Not IsDBNull(drCustomer("FK_AML_Creation_Branch_Code")) Then
                        display_Request_Kode_Cabang.Text = drCustomer("FK_AML_Creation_Branch_Code")
                        Using objdb As New NawaDatadevEntities
                            Dim strBranchCode As String = drCustomer("FK_AML_Creation_Branch_Code")
                            Dim objbranch As AML_BRANCH = objdb.AML_BRANCH.Where(Function(x) x.FK_AML_BRANCH_CODE = strBranchCode).FirstOrDefault
                            If objbranch IsNot Nothing Then
                                If objbranch.BRANCH_NAME IsNot Nothing Then
                                    display_Request_Nama_Cabang.Text = objbranch.BRANCH_NAME
                                End If
                                If objbranch.REGION_NAME IsNot Nothing Then
                                    display_Request_Kantor_Wilayah.Text = objbranch.REGION_NAME
                                End If
                            End If
                        End Using
                    End If
                    If judgementheader.CreatedDate IsNot Nothing Then
                        display_Request_Waktu_Proses.Value = CDate(judgementheader.CreatedDate).ToString("dd-MMM-yyyy")
                    End If
                    If Not IsDBNull(drCustomer("FK_AML_Customer_Type_Code")) Then
                        Using objdb As New NawaDatadevEntities
                            Dim strCustomerType As String = drCustomer("FK_AML_Customer_Type_Code")
                            Dim objcustomertype As AML_CUSTOMER_TYPE = objdb.AML_CUSTOMER_TYPE.Where(Function(x) x.FK_AML_Customer_Type_Code = strCustomerType).FirstOrDefault
                            If objcustomertype IsNot Nothing Then
                                If objcustomertype.Customer_Type_Name IsNot Nothing Then
                                    display_Request_Tipe_Nasabah.Text = objcustomertype.Customer_Type_Name
                                End If
                            End If
                        End Using

                        '' Edit 21-Feb-2023, Felix. Baca FK_AML_Customer_Type_Code untuk Individu dari Global param, karena tiap bank bisa beda.
                        Dim Customer_Type_INDV As String = "I" '' Default dr product
                        Dim ParameterValue_PK15 As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue from AML_Global_Parameter where [PK_GlobalReportParameter_ID] = 15", Nothing)
                        If ParameterValue_PK15 IsNot Nothing Then
                            Customer_Type_INDV = ParameterValue_PK15
                        End If
                        ''If DataAMLCustomer.FK_AML_Customer_Type_Code.Equals("I") Then

                        If drCustomer("FK_AML_Customer_Type_Code").Equals(Customer_Type_INDV) Then
                            '' End 21-Feb-2023
                            If Not IsDBNull(drCustomer("PLACEOFBIRTH")) Then
                                display_Request_Tempat_Lahir.Text = drCustomer("PLACEOFBIRTH")
                            End If
                            If Not IsDBNull(drCustomer("DATEOFBIRTH")) AndAlso drCustomer("DATEOFBIRTH") <> DateTime.MinValue Then
                                display_Request_DOB.Text = CDate(drCustomer("DATEOFBIRTH")).ToString("dd-MMM-yyyy")
                            End If
                            If Not IsDBNull(drCustomer("FK_AML_PEKERJAAN_CODE")) Then
                                Using objdb As New NawaDatadevEntities
                                    Dim strPekerjaanCode As String = drCustomer("FK_AML_PEKERJAAN_CODE")
                                    Dim objcustomerpekerjaan As AML_PEKERJAAN = objdb.AML_PEKERJAAN.Where(Function(x) x.FK_AML_PEKERJAAN_Code = strPekerjaanCode).FirstOrDefault
                                    If objcustomerpekerjaan IsNot Nothing Then
                                        If objcustomerpekerjaan.AML_PEKERJAAN_Name IsNot Nothing Then
                                            display_Request_Pekerjaan.Text = objcustomerpekerjaan.AML_PEKERJAAN_Name
                                        End If
                                    End If
                                End Using
                            End If
                            '' Add 19-Dec-2022  , display jg POB, DOB, Pekerjaan
                            display_Request_Tempat_Lahir.Hidden = False
                            display_Request_DOB.Hidden = False
                            display_Request_Pekerjaan.Hidden = False
                            '' End 19-Dec-2022
                        Else
                            display_Request_Tempat_Lahir.Hide()
                            display_Request_DOB.Hide()
                            display_Request_Pekerjaan.Hide()
                        End If
                    End If
                    If Not IsDBNull(drCustomer("CIFNo")) Then
                        display_Request_CIF.Text = drCustomer("CIFNo")

                        Dim objParamID(0) As SqlParameter
                        objParamID(0) = New SqlParameter
                        objParamID(0).ParameterName = "@CIFNo"
                        objParamID(0).Value = drCustomer("CIFNo")
                        objParamID(0).DbType = SqlDbType.VarChar

                        Dim CUSTOMER_IDENTITY_NUMBER As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Get1CustomerIdentity_ByCIFNo", objParamID)

                        If CUSTOMER_IDENTITY_NUMBER IsNot Nothing Then
                            display_Request_Identity_Number.Value = CUSTOMER_IDENTITY_NUMBER
                        End If

                        Dim objParamAddress(0) As SqlParameter
                        objParamAddress(0) = New SqlParameter
                        objParamAddress(0).ParameterName = "@CIFNo"
                        objParamAddress(0).Value = drCustomer("CIFNo")
                        objParamAddress(0).DbType = SqlDbType.VarChar

                        Dim CUSTOMER_ADDRESS As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Get1CustomerAddress_ByCIFNo", objParamAddress)

                        If CUSTOMER_ADDRESS IsNot Nothing Then
                            display_Request_Alamat.Text = CUSTOMER_ADDRESS
                        End If
                        '' End 21-Feb-2023.
                    End If
                    If Not IsDBNull(drCustomer("CUSTOMERNAME")) Then
                        display_Request_Name.Text = drCustomer("CUSTOMERNAME")
                    End If
                    If Not IsDBNull(drCustomer("ALIAS")) Then
                        display_Request_Alias.Value = drCustomer("ALIAS")
                    End If
                    If Not IsDBNull(drCustomer("NPWP")) Then
                        display_Request_NPWP.Text = drCustomer("NPWP")
                    End If
                    If Not IsDBNull(drCustomer("FK_AML_CITIZENSHIP_CODE")) Then
                        Using objdb As New NawaDatadevEntities
                            Dim strCountryCode As String = drCustomer("FK_AML_CITIZENSHIP_CODE")
                            Dim objcustomercountry As AML_COUNTRY = objdb.AML_COUNTRY.Where(Function(x) x.FK_AML_COUNTRY_Code = strCountryCode).FirstOrDefault
                            If objcustomercountry IsNot Nothing Then
                                If objcustomercountry.AML_COUNTRY_Name IsNot Nothing Then
                                    display_Request_Nationality.Text = objcustomercountry.AML_COUNTRY_Name
                                End If
                            End If
                        End Using
                    End If

                    If Not IsDBNull(drCustomer("FK_AML_RISK_CODE")) Then
                        display_Request_Profil_Risiko_oneFCC.Text = drCustomer("FK_AML_RISK_CODE")
                    End If

                End If
            End If

            listjudgementitem = AMLJudgementBLL.GetAMLJudgementList(idheader)

            '2021/06/07 penambahan karena adanya auto judgement
            listjudgementitemchange = listjudgementitem

            UpdateListJudgementItem()

            If listjudgementitem IsNot Nothing Then
                BindJudgement(judgement_Item, listjudgementitem)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    'daniel 20210419
    Protected Sub UpdateListJudgementItem()
        Try
            For Each item In listwatchlistalreadymatch
                Dim judgementAffectedByWatchlist As List(Of AML_SCREENING_RESULT_DETAIL) = listjudgementitem.Where(Function(x) x.FK_AML_WATCHLIST_ID = item.FK_AML_WATCHLIST_ID).ToList
                For Each itemAffected In judgementAffectedByWatchlist
                    judgementdetailchange = listjudgementitem.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = itemAffected.PK_AML_SCREENING_RESULT_DETAIL_ID).FirstOrDefault
                    If judgementdetailchange IsNot Nothing Then
                        listAffecteddatafromlistmatch.Add(itemAffected)
                        If Not judgementdetailchange.JUDGEMENT_ISMATCH Or judgementdetailchange.JUDGEMENT_ISMATCH Is Nothing Then
                            itemAffected.JUDGEMENT_ISMATCH = True
                            indexofjudgementitemschange = listjudgementitem.IndexOf(judgementdetailchange)
                            listjudgementitem(indexofjudgementitemschange) = itemAffected
                            listjudgementitemchange.Add(itemAffected)
                        End If
                    End If
                Next
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'end daniel 20210419

    Private Sub BindJudgement(store As Store, list As List(Of AML_SCREENING_RESULT_DETAIL))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_CATEGORY", GetType(String)))
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_TYPE", GetType(String)))
            objtable.Columns.Add(New DataColumn("ISMATCH", GetType(String)))
            objtable.Columns.Add(New DataColumn("Similarity", GetType(Double)))
            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    If Not IsDBNull(item("FK_AML_WATCHLIST_CATEGORY_ID")) Then
                        item("AML_WATCHLIST_CATEGORY") = AMLJudgementBLL.GetWatchlistCategoryByID(item("FK_AML_WATCHLIST_CATEGORY_ID"))
                    Else
                        item("AML_WATCHLIST_CATEGORY") = ""
                    End If
                    If Not IsDBNull(item("FK_AML_WATCHLIST_TYPE_ID")) Then
                        item("AML_WATCHLIST_TYPE") = AMLJudgementBLL.GetWatchlistTypeByID(item("FK_AML_WATCHLIST_TYPE_ID"))
                    Else
                        item("AML_WATCHLIST_TYPE") = ""
                    End If
                    'daniel 20210419
                    'diremark karena udh diganti menjadi persentase dari DB
                    'If Not IsDBNull(item("MATCH_SCORE")) Then
                    '    item("Similarity") = item("MATCH_SCORE") * 100
                    'Else
                    '    item("Similarity") = 0
                    'End If
                    'If Not IsDBNull(item("JUDGEMENT_ISMATCH")) Then
                    '    If item("JUDGEMENT_ISMATCH") Then
                    '        item("ISMATCH") = "Match"
                    '    Else
                    '        item("ISMATCH") = "Not Match"
                    '    End If
                    'Else
                    '    item("ISMATCH") = ""
                    'End If
                    judgementdetailchange = listAffecteddatafromlistmatch.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = item("PK_AML_SCREENING_RESULT_DETAIL_ID")).FirstOrDefault
                    If judgementdetailchange IsNot Nothing Then
                        item("ISMATCH") = "Already Judged Match"
                    Else
                        If Not IsDBNull(item("JUDGEMENT_ISMATCH")) Then
                            If item("JUDGEMENT_ISMATCH") Then
                                item("ISMATCH") = "Match"
                            Else
                                item("ISMATCH") = "Not Match"
                            End If
                        Else
                            item("ISMATCH") = ""
                        End If
                    End If
                    'end daniel 20210419
                Next
            End If
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ALIAS()
        Try
            Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS)
            gridAliasInfo.GetStore().DataSource = objtable
            gridAliasInfo.GetStore().DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ADDRESS()
        Try
            Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS)

            objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
            objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
                    If objCountry IsNot Nothing Then
                        item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
                    Else
                        item("COUNTRY_NAME") = Nothing
                    End If

                    Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
                    If objAddressType IsNot Nothing Then
                        item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
                    Else
                        item("AML_ADDRESS_TYPE_NAME") = Nothing
                    End If
                Next
            End If

            gridAddressInfo.GetStore().DataSource = objtable
            gridAddressInfo.GetStore().DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub Bind_AML_WATCHLIST_IDENTITY()
        Try
            Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY)

            objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
                    If objIDENTITYType IsNot Nothing Then
                        item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
                    Else
                        item("AML_IDENTITY_TYPE_NAME") = Nothing
                    End If
                Next
            End If

            gridIdentityInfo.GetStore().DataSource = objtable
            gridIdentityInfo.GetStore().DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Protected Sub GridcommandJudgementItem(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Judge" Then
                ClearOBJDetail()
                WindowJudge.Hidden = False
                LoadObjectJudgement(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
            ElseIf e.ExtraParams(1).Value = "Delete" Then
            ElseIf e.ExtraParams(1).Value = "Edit" Then
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadObjectJudgement(id As String)
        Try
            'If judgementrequestforheader IsNot Nothing Then
            'If judgementrequestforheader.NAME IsNot Nothing Then
            '    display_JudgeName.Text = judgementrequestforheader.NAME
            'End If
            'If judgementrequestforheader.DOB IsNot Nothing Then
            '    display_JudgeDoB.Text = judgementrequestforheader.DOB
            'End If
            'If judgementrequestforheader.NATIONALITY IsNot Nothing Then
            '    display_JudgeNationality.Text = judgementrequestforheader.NATIONALITY
            'End If
            'If judgementrequestforheader.IDENTITY_NUMBER IsNot Nothing Then
            '    display_JudgeIdentity.Text = judgementrequestforheader.IDENTITY_NUMBER
            'End If
            'End If

            'Get Match Score Weight
            Dim NAME_WEIGHT As Double 'Decimal
            Dim DOB_WEIGHT As Double 'Decimal
            Dim NATIONALITY_WEIGHT As Double 'Decimal
            Dim IDENTITY_WEIGHT As Double 'Decimal

            Dim objdt_SEARCH_RESULT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select NAME_WEIGHT, DOB_WEIGHT,NATIONALITY_WEIGHT,IDENTITY_WEIGHT FROM AML_SCREENING_MATCH_PARAMETER", Nothing)
            If objdt_SEARCH_RESULT IsNot Nothing Then
                For Each row As DataRow In objdt_SEARCH_RESULT.Rows
                    NAME_WEIGHT = row.Item("NAME_WEIGHT")
                    DOB_WEIGHT = row.Item("DOB_WEIGHT")
                    NATIONALITY_WEIGHT = row.Item("NATIONALITY_WEIGHT")
                    IDENTITY_WEIGHT = row.Item("IDENTITY_WEIGHT")
                Next row
            End If

            'Load Detail
            judgementdetail = listjudgementitem.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = id).FirstOrDefault
            If judgementdetail IsNot Nothing Then
                indexofjudgementitems = listjudgementitem.IndexOf(judgementdetail)
                If judgementdetail.JUDGEMENT_ISMATCH IsNot Nothing Then
                    If judgementdetail.JUDGEMENT_ISMATCH Then
                        LblDisplayIsMatch.Text = "Match"
                    Else
                        LblDisplayIsMatch.Text = "Not Match"
                    End If
                End If
                If judgementdetail.JUDGEMENT_COMMENT IsNot Nothing Then
                    JudgementComment.Text = judgementdetail.JUDGEMENT_COMMENT
                End If

                If judgementrequestforheader IsNot Nothing Then
                    '15-Jun-2023 Adi : Commented. Diperbaiki di bawah.
                    'If judgementdetail.MATCH_SCORE_NAME IsNot Nothing And judgementrequestforheader.NAME IsNot Nothing Then
                    '    Name_Score.Text = String.Format("{0:0.00}", judgementdetail.MATCH_SCORE_NAME) & " %"
                    '    'diremark karena udh diganti menjadi persentase dari DB
                    '    'Name_Score.Text = Convert.ToString(Math.Round(Convert.ToDecimal(judgementdetail.MATCH_SCORE_NAME * 100), 2, MidpointRounding.AwayFromZero)) & " %"
                    'Else
                    '    Name_Score.Text = "-"
                    'End If

                    'If judgementdetail.MATCH_SCORE_DOB IsNot Nothing And judgementrequestforheader.DOB IsNot Nothing Then
                    '    DoB_Score.Text = String.Format("{0:0.00}", judgementdetail.MATCH_SCORE_DOB) & " %"
                    '    'diremark karena udh diganti menjadi persentase dari DB
                    '    'DoB_Score.Text = Convert.ToString(Math.Round(Convert.ToDecimal(judgementdetail.MATCH_SCORE_DOB * 100), 2, MidpointRounding.AwayFromZero)) & " %"
                    'Else
                    '    DoB_Score.Text = "-"
                    'End If

                    'If judgementdetail.MATCH_SCORE_NATIONALITY IsNot Nothing And judgementrequestforheader.NATIONALITY IsNot Nothing Then
                    '    Nationality_Score.Text = String.Format("{0:0.00}", judgementdetail.MATCH_SCORE_NATIONALITY) & " %"
                    '    'diremark karena udh diganti menjadi persentase dari DB
                    '    'Nationality_Score.Text = Convert.ToString(Math.Round(Convert.ToDecimal(judgementdetail.MATCH_SCORE_NATIONALITY * 100), 2, MidpointRounding.AwayFromZero)) & " %"
                    'Else
                    '    Nationality_Score.Text = "-"
                    'End If

                    'If judgementdetail.MATCH_SCORE_IDENTITY_NUMBER IsNot Nothing And judgementrequestforheader.IDENTITY_NUMBER IsNot Nothing Then
                    '    Identity_Score.Text = String.Format("{0:0.00}", judgementdetail.MATCH_SCORE_IDENTITY_NUMBER) & " %"
                    '    'diremark karena udh diganti menjadi persentase dari DB
                    '    'Identity_Score.Text = Convert.ToString(Math.Round(Convert.ToDecimal(judgementdetail.MATCH_SCORE_IDENTITY_NUMBER * 100), 2, MidpointRounding.AwayFromZero)) & " %"
                    'Else
                    '    Identity_Score.Text = "-"
                    'End If

                    'If judgementdetail.MATCH_SCORE IsNot Nothing Then
                    '    Total_Score.Text = String.Format("{0:0.00}", judgementdetail.MATCH_SCORE) & " %"
                    '    'diremark karena udh diganti menjadi persentase dari DB
                    '    'Total_Score.Text = Convert.ToString(Math.Round(Convert.ToDecimal(judgementdetail.MATCH_SCORE * 100), 2, MidpointRounding.AwayFromZero)) & " %"
                    'End If

                    'Display Searched Data
                    df_Name_Search.Value = judgementdetail.NAME
                    If judgementdetail.DOB IsNot Nothing AndAlso CDate(judgementdetail.DOB) <> DateTime.MinValue Then
                        df_DOB_Search.Value = CDate(judgementdetail.DOB).ToString("dd-MMM-yyyy")
                    End If
                    df_Nationality_Search.Value = judgementdetail.NATIONALITY
                    df_Identity_Search.Value = judgementdetail.IDENTITY_NUMBER

                    'Display Watchlist Data
                    df_Name_Result.Value = judgementdetail.NAME_WATCHLIST
                    If judgementdetail.DOB_WATCHLIST IsNot Nothing AndAlso CDate(judgementdetail.DOB_WATCHLIST) <> DateTime.MinValue Then
                        df_DOB_Result.Value = CDate(judgementdetail.DOB_WATCHLIST).ToString("dd-MMM-yyyy")
                    End If
                    df_Nationality_Result.Value = judgementdetail.NATIONALITY_WATCHLIST
                    df_Identity_Result.Value = judgementdetail.IDENTITY_NUMBER_WATCHLIST

                    'Screening Match Score
                    Dim dblMatchScoreName As Double = 0
                    Dim dblMatchScoreDOB As Double = 0
                    Dim dblMatchScoreNationality As Double = 0
                    Dim dblMatchScoreIdentity As Double = 0
                    Dim dblMatchScoreTotal = 0

                    Dim strMatchScoreName As String = "-"
                    Dim strMatchScoreDOB As String = "-"
                    Dim strMatchScoreNationality As String = "-"
                    Dim strMatchScoreIdentity As String = "-"

                    'Hitung Match Score x Bobot
                    If judgementdetail.MATCH_SCORE_NAME IsNot Nothing Then
                        dblMatchScoreName = CDbl(judgementdetail.MATCH_SCORE_NAME) * CDbl(NAME_WEIGHT / 100)
                        strMatchScoreName = NAME_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_NAME).ToString("#,##0.00") & " % = " & dblMatchScoreName.ToString("#,##0.00") & " %"
                    End If
                    If judgementdetail.MATCH_SCORE_DOB IsNot Nothing Then
                        dblMatchScoreDOB = CDbl(judgementdetail.MATCH_SCORE_DOB) * CDbl(DOB_WEIGHT / 100)
                        strMatchScoreDOB = DOB_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_DOB).ToString("#,##0.00") & " % = " & dblMatchScoreDOB.ToString("#,##0.00") & " %"
                    End If
                    If judgementdetail.MATCH_SCORE_NATIONALITY IsNot Nothing Then
                        dblMatchScoreNationality = CDbl(judgementdetail.MATCH_SCORE_NATIONALITY) * CDbl(DOB_WEIGHT / 100)
                        strMatchScoreNationality = NATIONALITY_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_NATIONALITY).ToString("#,##0.00") & " % = " & dblMatchScoreNationality.ToString("#,##0.00") & " %"
                    End If
                    If judgementdetail.MATCH_SCORE_IDENTITY_NUMBER IsNot Nothing Then
                        dblMatchScoreIdentity = CDbl(judgementdetail.MATCH_SCORE_IDENTITY_NUMBER) * CDbl(DOB_WEIGHT / 100)
                        strMatchScoreIdentity = IDENTITY_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_IDENTITY_NUMBER).ToString("#,##0.00") & " % = " & dblMatchScoreIdentity.ToString("#,##0.00") & " %"
                    End If
                    dblMatchScoreTotal = dblMatchScoreName + dblMatchScoreDOB + dblMatchScoreNationality + dblMatchScoreIdentity

                    'Show the Formula so the user clear about the score
                    Dim strDividend As String = dblMatchScoreName.ToString("#,##0.00")
                    Dim strDivisor As String = NAME_WEIGHT.ToString()
                    If judgementdetail.DOB IsNot Nothing AndAlso Not CDate(judgementdetail.DOB) = DateTime.MinValue Then
                        strDividend = strDividend & " + " & dblMatchScoreDOB.ToString("#,##0.00")
                        strDivisor = strDivisor & " + " & DOB_WEIGHT.ToString()
                    End If
                    If Not String.IsNullOrEmpty(judgementdetail.NATIONALITY) Then
                        strDividend = strDividend & " + " & dblMatchScoreNationality.ToString("#,##0.00")
                        strDivisor = strDivisor & " + " & NATIONALITY_WEIGHT.ToString()
                    End If
                    If Not String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER) Then
                        strDividend = strDividend & " + " & dblMatchScoreIdentity.ToString("#,##0.00")
                        strDivisor = strDivisor & " + " & IDENTITY_WEIGHT.ToString()
                    End If
                    strDividend = "( " & strDividend & " % )"
                    strDivisor = "( " & strDivisor & " % )"

                    'Show the Score
                    Name_Score.Value = strMatchScoreName
                    DoB_Score.Value = IIf(judgementdetail.DOB IsNot Nothing AndAlso CDate(judgementdetail.DOB) = DateTime.MinValue, "-", strMatchScoreDOB)
                    Nationality_Score.Value = IIf(String.IsNullOrEmpty(judgementdetail.NATIONALITY), "-", strMatchScoreNationality)
                    Identity_Score.Value = IIf(String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER), "-", strMatchScoreIdentity)
                    Total_Score.Value = strDividend & " / " & strDivisor & " = " & CDbl(judgementdetail.MATCH_SCORE).ToString("#,##0.00") & " %"

                End If

                objAML_WATCHLIST_CLASS = AML_WATCHLIST_BLL.GetWatchlistClassByID(judgementdetail.FK_AML_WATCHLIST_ID)
                If objAML_WATCHLIST_CLASS.objAML_WATCHLIST IsNot Nothing Then
                    With objAML_WATCHLIST_CLASS.objAML_WATCHLIST
                        WatchListID.Text = .PK_AML_WATCHLIST_ID
                        If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                            Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                            If objWatchlistCategory IsNot Nothing Then
                                WatchListCategory.Text = objWatchlistCategory.CATEGORY_NAME
                            End If
                            '' Add 22-Feb-2023, Display Button Display Watchlist Worldcheck
                            Dim PkWatchlistWorldcheckCategory As String = ""

                            Dim strPkWatchlistWorldcheckCategory As String = "select ParameterValue"
                            strPkWatchlistWorldcheckCategory += " FROM AML_Global_Parameter a "
                            strPkWatchlistWorldcheckCategory += " where PK_GlobalReportParameter_ID = '14'"

                            PkWatchlistWorldcheckCategory = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strPkWatchlistWorldcheckCategory, Nothing)

                            If PkWatchlistWorldcheckCategory IsNot Nothing Then
                                If .FK_AML_WATCHLIST_CATEGORY_ID = PkWatchlistWorldcheckCategory Then
                                    btn_OpenWindowWatchlist.Hidden = "false"

                                    Dim strIDCode As String = 16645 '' Module ID "AML_WATCHLIST_WORLDCHECK"
                                    IDModuleWatchlistEncrypted = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                                    Dim PkWatchlistWorldcheckID As String = ""

                                    Dim strPkWatchlistWorldcheckID As String = "select FK_AML_WATCHLIST_SOURCE_ID"
                                    strPkWatchlistWorldcheckID += " FROM AML_WATCHLIST a "
                                    strPkWatchlistWorldcheckID += " where PK_AML_WATCHLIST_ID = '" & .PK_AML_WATCHLIST_ID.ToString() & "'"

                                    PkWatchlistWorldcheckID = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strPkWatchlistWorldcheckID, Nothing)
                                    IDWatchlistEncrypted = Common.EncryptQueryString(PkWatchlistWorldcheckID, SystemParameterBLL.GetEncriptionKey)

                                    If IDModuleWatchlistEncrypted Is Nothing Or IDWatchlistEncrypted Is Nothing Then
                                        Throw New ApplicationException("Watchlist ID is not found.")
                                    End If
                                Else
                                    btn_OpenWindowWatchlist.Hidden = "true"
                                End If
                            End If
                        End If
                        If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                            Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                            If objWatchlistType IsNot Nothing Then
                                WatchListType.Text = objWatchlistType.TYPE_NAME
                            End If
                        End If
                        If .NAME IsNot Nothing Then
                            FullName.Text = .NAME
                        End If
                        If .BIRTH_PLACE IsNot Nothing Then
                            PlaceofBirth.Text = .BIRTH_PLACE
                        End If
                        If .DATE_OF_BIRTH IsNot Nothing Then
                            DateofBirth.Text = CDate(.DATE_OF_BIRTH).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture)
                            'DateofBirth.Text = .DATE_OF_BIRTH
                        End If
                        If .YOB IsNot Nothing Then
                            YearofBirth.Text = .YOB
                        End If
                        If .NATIONALITY IsNot Nothing Then
                            Nationality.Text = .NATIONALITY
                        End If
                        If .CUSTOM_REMARK_1 IsNot Nothing Then
                            Remark1.Text = .CUSTOM_REMARK_1
                        End If
                        If .CUSTOM_REMARK_2 IsNot Nothing Then
                            Remark2.Text = .CUSTOM_REMARK_2
                        End If
                        If .CUSTOM_REMARK_3 IsNot Nothing Then
                            Remark3.Text = .CUSTOM_REMARK_3
                        End If
                        If .CUSTOM_REMARK_4 IsNot Nothing Then
                            Remark4.Text = .CUSTOM_REMARK_4
                        End If
                        If .CUSTOM_REMARK_5 IsNot Nothing Then
                            Remark5.Text = .CUSTOM_REMARK_5
                        End If
                    End With
                    Bind_AML_WATCHLIST_ALIAS()
                    Bind_AML_WATCHLIST_ADDRESS()
                    Bind_AML_WATCHLIST_IDENTITY()
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BtnRequestReOpen_Click(sender As Object, e As DirectEventArgs)
        Try
            If AML_WATCHLIST_BLL.IsExistsInApproval(ObjModule.ModuleName, dataID) Then
                LblConfirmation.Text = "Sorry, this Data is already Updated By Other User and already in Pending Approval. Please Check Data in Pending Approval."

                Panelconfirmation.Hidden = False
                PanelInfo.Hidden = True
            Else
                '2023-08-01, Nael: Pas Request Reopen maka statusnya akan berubah dari 2 jadi 1
                judgementheader.STATUS_JUDGEMENT_CODE = "5"
                judgementClass = New AMLJudgementClass
                With judgementClass
                    .AMLJudgementHeader = judgementheader
                    .AMLListResult = listjudgementitemchange
                End With
                checkerjudgeitems = New AML_SCREENING_RESULT_DETAIL
                checkerjudgeitems = listjudgementitem.Where(Function(x) x.JUDGEMENT_ISMATCH Is Nothing).FirstOrDefault
                ' 2021/06/07 perubahan karena adanya auto judgement dimana apabila ada check perubahan keseluruhan maka akan terhambat dan tidak bisa save meskipun semua data telah ter judge
                'If listjudgementitemchange.Count = 0 Then
                '    Throw New ApplicationException("No Data Has Changed, Please Change Some Data Before Saving or Click the Cancel Button to Cancel the Process")
                'ElseIf checkerjudgeitems IsNot Nothing Then
                '    Throw New ApplicationException("There are Some Data which have not been Judged, Please Judge All Data Before Saving")
                'End If
                If checkerjudgeitems IsNot Nothing Then
                    Throw New ApplicationException("There are Some Data which have not been Judged, Please Judge All Data Before Saving")
                End If
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not ObjModule.IsUseApproval Then
                    'tanpa approval
                    AMLJudgementBLL.SaveJudgementTanpaApproval(judgementClass, ObjModule)
                    PanelInfo.Hide()
                    Panelconfirmation.Show()
                    LblConfirmation.Text = "Data Saved into Database"

                    '18-Jun-2023 Adi : Tambah ubah semua flag STATUS_JUDGEMENT_CODE di AML_SCREENING_RESULT jadi 2
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "UPDATE AML_SCREENING_RESULT SET STATUS_JUDGEMENT_CODE=5, LastUpdateBy='" & Common.SessionCurrentUser.UserID & "', LastUpdateDate=GETDATE() WHERE CIF_NO='" & judgementheader.CIF_NO & "'", Nothing)

                Else
                    'with approval
                    '2023-08-02, Nael: STATUS_JUDGEMENT_CODE di ubah jadi 5 Re-Open
                    AMLJudgementBLL.RequestUpdateStatusCode(judgementClass, ObjModule)
                    PanelInfo.Hide()
                    Panelconfirmation.Show()
                    LblConfirmation.Text = "Data Saved into Pending Approval"

                    '18-Jun-2023 Adi : Tambah ubah semua flag STATUS_JUDGEMENT_CODE di AML_SCREENING_RESULT jadi 1
                    '2023-08-02, Nael: Ubah jadi flag STATUS_JUDGEMENT_CODE di AML_SCREENING_RESULT jadi 4, Request Re Open
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "UPDATE AML_SCREENING_RESULT SET STATUS_JUDGEMENT_CODE=4, LastUpdateBy='" & Common.SessionCurrentUser.UserID & "', LastUpdateDate=GETDATE() WHERE CIF_NO='" & judgementheader.CIF_NO & "'", Nothing)
                    'Update ModuleKey di ModuleApproval jadi CIF_NO
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "UPDATE ModuleApproval SET ModuleKey='" & dataID & "' WHERE ModuleName='Vw_AML_Judgement' AND ModuleKey='" & judgementheader.PK_AML_SCREENING_RESULT_ID & "'", Nothing)

                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadJudgementItemChange(items As AML_SCREENING_RESULT_DETAIL)
        Try
            judgementdetailchange = listjudgementitemchange.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = items.PK_AML_SCREENING_RESULT_DETAIL_ID).FirstOrDefault
            If judgementdetailchange IsNot Nothing Then
                indexofjudgementitemschange = listjudgementitemchange.IndexOf(judgementdetailchange)
                listjudgementitemchange(indexofjudgementitemschange) = items
            Else
                listjudgementitemchange.Add(items)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub rgjudgement_click(sender As Object, e As DirectEventArgs)
        Try
            Dim objradio As Ext.Net.Radio = CType(sender, Ext.Net.Radio)
            If JudgementComment.Text Is Nothing Or JudgementComment.Text = "" Or JudgementComment.Text = "This Data is Judged Match" Or JudgementComment.Text = "This Data is Judged Not Match" Then
                If objradio.InputValue = "1" And objradio.Checked Then
                    JudgementComment.Text = "This Data is Judged Match"
                ElseIf objradio.InputValue = "0" And objradio.Checked Then
                    JudgementComment.Text = "This Data is Judged Not Match"
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnJudgementCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowJudge.Hidden = True
            ClearOBJDetail()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    ' 22 Feb 2023 Ari penambahan untuk worldcheck
#Region "World Check"
    Public Property IDWatchlistEncrypted() As String
        Get
            Return Session("AMLJudgmentEdit.IDWatchlistEncrypted")
        End Get
        Set(ByVal value As String)
            Session("AMLJudgmentEdit.IDWatchlistEncrypted") = value
        End Set
    End Property
    Public Property IDModuleWatchlistEncrypted() As String
        Get
            Return Session("AMLJudgmentEdit.IDModuleWatchlistEncrypted")
        End Get
        Set(ByVal value As String)
            Session("AMLJudgmentEdit.IDModuleWatchlistEncrypted") = value
        End Set
    End Property
    Protected Sub btn_OpenWindowWatchlist_Click()
        Try
            WindowDisplayWatchlist.Hidden = False
            pnlContent.ClearContent()
            pnlContent.AnimCollapse = False
            pnlContent.Loader.SuspendScripting()
            pnlContent.Loader.Url = "~/Parameter/ParameterDetail.aspx?ID=" & IDWatchlistEncrypted & "&ModuleID=" & IDModuleWatchlistEncrypted & ""
            pnlContent.Loader.Params.Clear()
            pnlContent.LoadContent()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Display_Watchlist_Back_Click()
        Try
            WindowDisplayWatchlist.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region


#Region "Update 15-Jun-2023 Adi : Load Data Judgement by CIF"
    Sub LoadDataByCIF(strCIF As String)
        Try
            'Ambil Top 1 from AML_SCREENING_RESULT dengan CIF yang sama dengan strCIF untuk judgementheader agar tidak banyak perubahan
            Dim idheader As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT TOP 1 PK_AML_SCREENING_RESULT_ID FROM AML_SCREENING_RESULT WHERE CIF_NO='" & strCIF & "'", Nothing)
            judgementheader = AMLJudgementBLL.GetAMLJudgementHeader(idheader)

            'Load Data Customer
            Dim strQuery As String = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & strCIF & "'"
            Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

            ' 2023-07-31, Nael: Mengambil status judged (closed, new, dll)
            Dim VwAMLJudgement As DataRow = NawaDAL.SQLHelper.ExecuteRow(
                NawaDAL.SQLHelper.strConnectionString,
                CommandType.Text,
                "SELECT TOP 1 * FROM Vw_AML_JUDGEMENT WHERE CIF_NO = '" & strCIF & "'"
            )

            If drCustomer IsNot Nothing Then
                If Not IsDBNull(drCustomer("FK_AML_Creation_Branch_Code")) Then
                    display_Request_Kode_Cabang.Text = drCustomer("FK_AML_Creation_Branch_Code")
                    Using objdb As New NawaDatadevEntities
                        Dim strBranchCode As String = drCustomer("FK_AML_Creation_Branch_Code")
                        Dim objbranch As AML_BRANCH = objdb.AML_BRANCH.Where(Function(x) x.FK_AML_BRANCH_CODE = strBranchCode).FirstOrDefault
                        If objbranch IsNot Nothing Then
                            If objbranch.BRANCH_NAME IsNot Nothing Then
                                display_Request_Nama_Cabang.Text = objbranch.BRANCH_NAME
                            End If
                            If objbranch.REGION_NAME IsNot Nothing Then
                                display_Request_Kantor_Wilayah.Text = objbranch.REGION_NAME
                            End If
                        End If
                    End Using
                End If
                If judgementheader.CreatedDate IsNot Nothing Then
                    display_Request_Waktu_Proses.Value = CDate(judgementheader.CreatedDate).ToString("dd-MMM-yyyy")
                End If
                If Not IsDBNull(drCustomer("FK_AML_Customer_Type_Code")) Then
                    Using objdb As New NawaDatadevEntities
                        Dim strCustomerType As String = drCustomer("FK_AML_Customer_Type_Code")
                        Dim objcustomertype As AML_CUSTOMER_TYPE = objdb.AML_CUSTOMER_TYPE.Where(Function(x) x.FK_AML_Customer_Type_Code = strCustomerType).FirstOrDefault
                        If objcustomertype IsNot Nothing Then
                            If objcustomertype.Customer_Type_Name IsNot Nothing Then
                                display_Request_Tipe_Nasabah.Text = objcustomertype.Customer_Type_Name
                            End If
                        End If
                    End Using

                    '' Edit 21-Feb-2023, Felix. Baca FK_AML_Customer_Type_Code untuk Individu dari Global param, karena tiap bank bisa beda.
                    Dim Customer_Type_INDV As String = "I" '' Default dr product
                    Dim ParameterValue_PK15 As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue from AML_Global_Parameter where [PK_GlobalReportParameter_ID] = 15", Nothing)
                    If ParameterValue_PK15 IsNot Nothing Then
                        Customer_Type_INDV = ParameterValue_PK15
                    End If
                    ''If DataAMLCustomer.FK_AML_Customer_Type_Code.Equals("I") Then

                    If drCustomer("FK_AML_Customer_Type_Code").Equals(Customer_Type_INDV) Then
                        '' End 21-Feb-2023
                        If Not IsDBNull(drCustomer("PLACEOFBIRTH")) Then
                            display_Request_Tempat_Lahir.Text = drCustomer("PLACEOFBIRTH")
                        End If
                        If Not IsDBNull(drCustomer("DATEOFBIRTH")) AndAlso drCustomer("DATEOFBIRTH") <> DateTime.MinValue Then
                            display_Request_DOB.Text = CDate(drCustomer("DATEOFBIRTH")).ToString("dd-MMM-yyyy")
                        End If
                        If Not IsDBNull(drCustomer("FK_AML_PEKERJAAN_CODE")) Then
                            Using objdb As New NawaDatadevEntities
                                Dim strPekerjaanCode As String = drCustomer("FK_AML_PEKERJAAN_CODE")
                                Dim objcustomerpekerjaan As AML_PEKERJAAN = objdb.AML_PEKERJAAN.Where(Function(x) x.FK_AML_PEKERJAAN_Code = strPekerjaanCode).FirstOrDefault
                                If objcustomerpekerjaan IsNot Nothing Then
                                    If objcustomerpekerjaan.AML_PEKERJAAN_Name IsNot Nothing Then
                                        display_Request_Pekerjaan.Text = objcustomerpekerjaan.AML_PEKERJAAN_Name
                                    End If
                                End If
                            End Using
                        End If
                        '' Add 19-Dec-2022  , display jg POB, DOB, Pekerjaan
                        display_Request_Tempat_Lahir.Hidden = False
                        display_Request_DOB.Hidden = False
                        display_Request_Pekerjaan.Hidden = False
                        '' End 19-Dec-2022
                    Else
                        display_Request_Tempat_Lahir.Hide()
                        display_Request_DOB.Hide()
                        display_Request_Pekerjaan.Hide()
                    End If
                End If
                If Not IsDBNull(drCustomer("CIFNo")) Then
                    display_Request_CIF.Text = drCustomer("CIFNo")

                    Dim objParamID(0) As SqlParameter
                    objParamID(0) = New SqlParameter
                    objParamID(0).ParameterName = "@CIFNo"
                    objParamID(0).Value = drCustomer("CIFNo")
                    objParamID(0).DbType = SqlDbType.VarChar

                    Dim CUSTOMER_IDENTITY_NUMBER As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Get1CustomerIdentity_ByCIFNo", objParamID)

                    If CUSTOMER_IDENTITY_NUMBER IsNot Nothing Then
                        display_Request_Identity_Number.Value = CUSTOMER_IDENTITY_NUMBER
                    End If

                    Dim objParamAddress(0) As SqlParameter
                    objParamAddress(0) = New SqlParameter
                    objParamAddress(0).ParameterName = "@CIFNo"
                    objParamAddress(0).Value = drCustomer("CIFNo")
                    objParamAddress(0).DbType = SqlDbType.VarChar

                    Dim CUSTOMER_ADDRESS As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Get1CustomerAddress_ByCIFNo", objParamAddress)

                    If CUSTOMER_ADDRESS IsNot Nothing Then
                        display_Request_Alamat.Text = CUSTOMER_ADDRESS
                    End If
                    '' End 21-Feb-2023.
                End If
                If Not IsDBNull(drCustomer("CUSTOMERNAME")) Then
                    display_Request_Name.Text = drCustomer("CUSTOMERNAME")
                End If
                If Not IsDBNull(drCustomer("ALIAS")) Then
                    display_Request_Alias.Value = drCustomer("ALIAS")
                End If
                If Not IsDBNull(drCustomer("NPWP")) Then
                    display_Request_NPWP.Text = drCustomer("NPWP")
                End If
                If Not IsDBNull(drCustomer("FK_AML_CITIZENSHIP_CODE")) Then
                    Using objdb As New NawaDatadevEntities
                        Dim strCountryCode As String = drCustomer("FK_AML_CITIZENSHIP_CODE")
                        Dim objcustomercountry As AML_COUNTRY = objdb.AML_COUNTRY.Where(Function(x) x.FK_AML_COUNTRY_Code = strCountryCode).FirstOrDefault
                        If objcustomercountry IsNot Nothing Then
                            If objcustomercountry.AML_COUNTRY_Name IsNot Nothing Then
                                display_Request_Nationality.Text = objcustomercountry.AML_COUNTRY_Name
                            End If
                        End If
                    End Using
                End If

                If Not IsDBNull(drCustomer("FK_AML_RISK_CODE")) Then
                    display_Request_Profil_Risiko_oneFCC.Text = drCustomer("FK_AML_RISK_CODE")
                End If

                If Not IsDBNull(VwAMLJudgement("STATUS")) Then
                    display_Request_Status_Judgement.Text = VwAMLJudgement("STATUS")

                    If VwAMLJudgement("STATUS").Equals("Closed") Then
                        btnRequestReOpen.Hidden = False
                    End If
                End If

                '2023-07-31, Nael: Cek apakah status closed, jika closed baru btnRequestOpen muncul

            End If

            'Get Data Already Judged
            listwatchlistalreadymatch = AMLJudgementBLL.GetWatchlistAlreadyMatchByCIF(strCIF)

            'Get Data Judgement
            listjudgementitem = GetAMLJudgementListByCIF(strCIF)

            '2021/06/07 penambahan karena adanya auto judgement
            listjudgementitemchange = listjudgementitem

            'Update List of Judgement Status
            UpdateListJudgementItem()

            'Bind data judgement to Grid Panel
            If listjudgementitem IsNot Nothing Then
                BindJudgement(judgement_Item, listjudgementitem)
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    '2023-08-02, Nael [Todo]: Lanjut ubah button judgement jadi detail ngikutin mockup
    Function GetAMLJudgementListByCIF(strCIF As String) As List(Of AML_SCREENING_RESULT_DETAIL)
        Try
            Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
                Try
                    Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_DETAIL Join y In NawaDataEntitiy.AML_SCREENING_RESULT On X.FK_AML_SCREENING_RESULT_ID Equals y.PK_AML_SCREENING_RESULT_ID Where y.CIF_NO = strCIF And X.FK_AML_WATCHLIST_ID IsNot Nothing Select X).ToList
                Catch ex As Exception
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Sub SaveJudgementApproval_ByCIF(data As NawaDevBLL.AMLJudgementClass, objModule As NawaDAL.Module)
        Using objDB As New NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    data.AMLJudgementHeader.LastUpdateBy = Common.SessionCurrentUser.UserID
                    data.AMLJudgementHeader.LastUpdateDate = Now
                    data.AMLJudgementHeader.STATUS_JUDGEMENT_CODE = "1"
                    objDB.Entry(data.AMLJudgementHeader).State = EntityState.Modified
                    objDB.SaveChanges()

                    Dim dataXML As String = Common.Serialize(data)

                    Dim headerbefore As New AML_SCREENING_RESULT
                    Dim listitembefore As New List(Of AML_SCREENING_RESULT_DETAIL)
                    headerbefore = objDB.AML_SCREENING_RESULT.Where(Function(x) x.PK_AML_SCREENING_RESULT_ID = data.AMLJudgementHeader.PK_AML_SCREENING_RESULT_ID).FirstOrDefault
                    objDB.Entry(headerbefore).State = EntityState.Detached
                    For Each item In data.AMLListResult
                        Dim itembefore As New AML_SCREENING_RESULT_DETAIL
                        itembefore = objDB.AML_SCREENING_RESULT_DETAIL.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = item.PK_AML_SCREENING_RESULT_DETAIL_ID).FirstOrDefault
                        If itembefore IsNot Nothing Then
                            listitembefore.Add(itembefore)
                        End If
                    Next

                    Dim databefore As New AMLJudgementClass
                    With databefore
                        .AMLJudgementHeader = headerbefore
                        .AMLListResult = listitembefore
                    End With
                    Dim dataXMLBefore As String = Common.Serialize(databefore)

                    Dim objModuleApproval As New NawaDevDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = data.AMLJudgementHeader.PK_AML_SCREENING_RESULT_ID
                        .ModuleField = dataXML
                        .ModuleFieldBefore = dataXMLBefore
                        .PK_ModuleAction_ID = Common.ModuleActionEnum.Update
                        .CreatedBy = Common.SessionCurrentUser.UserID
                        .CreatedDate = Now
                    End With
                    objDB.Entry(objModuleApproval).State = EntityState.Added
                    objDB.SaveChanges()
                    If data.AMLListResult.Count > 0 Then
                        data.AMLJudgementHeader.LastUpdateBy = Common.SessionCurrentUser.UserID
                        data.AMLJudgementHeader.LastUpdateDate = Now
                        objDB.Entry(data.AMLJudgementHeader).State = EntityState.Modified
                        objDB.SaveChanges()
                        Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                        Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                        Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                        Dim modulename As String = objModule.ModuleLabel
                        Dim ObjAuditTrailHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, data.AMLJudgementHeader)
                        objDB.SaveChanges()
                        For Each item As AML_SCREENING_RESULT_DETAIL In data.AMLListResult
                            Dim obcek As AML_SCREENING_RESULT_DETAIL = (From x In objDB.AML_SCREENING_RESULT_DETAIL
                                                                        Where x.PK_AML_SCREENING_RESULT_DETAIL_ID = item.PK_AML_SCREENING_RESULT_DETAIL_ID
                                                                        Select x).FirstOrDefault
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        Next
                    End If
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub


#End Region

End Class
