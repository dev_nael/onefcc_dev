﻿Imports Ext
Imports NawaBLL
Imports NawaDevBLL
Imports System.Data
Imports System.Data.SqlClient
Imports Elmah
Imports NawaDAL
Imports NawaDevDAL
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevBLL.GlobalReportFunctionBLL
Imports System.Text
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.IO
Imports System.Globalization

Partial Class AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail
    Inherits Parent

#Region "Session"
    Public Property ObjApproval As NawaDAL.ModuleApproval
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.ObjApproval")
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.ObjApproval") = value
        End Set
    End Property
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.ObjModule") = value
        End Set
    End Property
    Public Property IDModule() As String
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.IDModule")
        End Get
        Set(ByVal value As String)
            Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.IDModule") = value
        End Set
    End Property

    Public Property IDUnik As Long
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.IDUnik") = value
        End Set
    End Property
    Public Property AMLJudgementClassNew As NawaDevBLL.AMLJudgementClass
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.AMLJudgementClassNew")
        End Get
        Set(ByVal value As NawaDevBLL.AMLJudgementClass)
            Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.AMLJudgementClassNew") = value
        End Set
    End Property

    Public Property AMLJudgementClassBefore As NawaDevBLL.AMLJudgementClass
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.AMLJudgementClassBefore")
        End Get
        Set(ByVal value As NawaDevBLL.AMLJudgementClass)
            Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.AMLJudgementClassBefore") = value
        End Set
    End Property

    Public Property ObjAML_WATCHLIST_CLASS() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.ObjAML_WATCHLIST_CLASS")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.ObjAML_WATCHLIST_CLASS") = value
        End Set
    End Property

    Public Property JudgementDetail As NawaDevDAL.AML_SCREENING_RESULT_DETAIL
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.JudgementDetail")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.JudgementDetail") = value
        End Set
    End Property

    Public Property JudgementHeader As NawaDevDAL.AML_SCREENING_RESULT
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.JudgementHeader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT)
            Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.JudgementHeader") = value
        End Set
    End Property
    Public Property JudgementRequestForHeader As NawaDevDAL.AML_SCREENING_REQUEST
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.JudgementRequestForHeader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_REQUEST)
            Session("AML_AMLScreeningJudgment_AMLJudgmentApprovalDetail.JudgementRequestForHeader") = value
        End Set
    End Property

#End Region
    Sub ClearSession()
        Try
            ObjApproval = Nothing
            AMLJudgementClassNew = New AMLJudgementClass
            AMLJudgementClassBefore = New AMLJudgementClass
            ClearOBJDetail()
        Catch ex As Exception
            'Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Private Sub ClearOBJDetail()
        Try
            ObjAML_WATCHLIST_CLASS = New AML_WATCHLIST_CLASS
            JudgementDetail = New AML_SCREENING_RESULT_DETAIL
            JudgementHeader = New AML_SCREENING_RESULT
            Name_Score.Clear()
            DoB_Score.Clear()
            Nationality_Score.Clear()
            Identity_Score.Clear()
            Total_Score.Clear()

            WatchListID.Clear()
            WatchListCategory.Clear()
            WatchListType.Clear()
            FullName.Clear()
            PlaceofBirth.Clear()
            DateofBirth.Clear()
            YearofBirth.Clear()
            Nationality.Clear()

            Remark1.Clear()
            Remark2.Clear()
            Remark3.Clear()
            Remark4.Clear()
            Remark5.Clear()

            LblJudgementIsMatch.Text = ""
            JudgementComment.Clear()
        Catch ex As Exception
            'Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()
                Dim IDData As String = Request.Params("ID")

                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If

                IDModule = Request.Params("ModuleID")
                Ext.Net.X.Msg.Info("ModuleID: " & IDModule)
                Dim intModuleID As Integer = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = ModuleBLL.GetModuleByModuleID(intModuleID)
                If ObjModule IsNot Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Approval) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If
                PanelInfo.Title = ObjModule.ModuleLabel & " - Approval"

                LoadModuleApproval()
                If Not String.IsNullOrEmpty(ObjApproval.ModuleFieldBefore) Then
                    LoadDataBefore()
                End If

                If Not String.IsNullOrEmpty(ObjApproval.ModuleField) Then
                    LoadDataAfter()
                End If
                LoadJudgementHeader()
            End If
        Catch ex As Exception
            BtnSave.Visible = False
            BtnReject.Visible = False
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
    Protected Sub LoadModuleApproval()
        Try
            ObjApproval = NawaBLL.ModuleApprovalBLL.GetModuleApprovalByID(IDUnik)
            If Not ObjApproval Is Nothing Then
                If ObjApproval.PK_ModuleApproval_ID <> Nothing Then
                    lblModuleKey.Value = ObjApproval.PK_ModuleApproval_ID
                End If
                If ObjApproval.ModuleName IsNot Nothing Then
                    Dim objModuleToApprove = NawaBLL.ModuleBLL.GetModuleByModuleName(ObjApproval.ModuleName)
                    If objModuleToApprove IsNot Nothing Then
                        txt_ModuleLabel.Value = objModuleToApprove.ModuleLabel
                    End If
                End If
                If ObjApproval.ModuleKey IsNot Nothing Then
                    txt_ModuleKey.Text = ObjApproval.ModuleKey
                End If
                If ObjApproval.PK_ModuleAction_ID IsNot Nothing Then
                    Dim temp = NawaBLL.ModuleBLL.GetModuleActionNamebyID(ObjApproval.PK_ModuleAction_ID)
                    If temp IsNot Nothing Then
                        lblAction.Text = temp
                    End If
                End If
                If ObjApproval.CreatedBy IsNot Nothing Then
                    Dim objUser = NawaBLL.MUserBLL.GetMuserbyUSerId(ObjApproval.CreatedBy)
                    If objUser IsNot Nothing Then
                        LblCreatedBy.Text = ObjApproval.CreatedBy & " - " & objUser.UserName
                    Else
                        LblCreatedBy.Text = ObjApproval.CreatedBy
                    End If
                End If
                If ObjApproval.CreatedDate IsNot Nothing And ObjApproval.CreatedDate <> Date.MinValue Then
                    lblCreatedDate.Text = ObjApproval.CreatedDate.Value.ToString("dd-MMM-yyyy")
                End If
            End If
        Catch ex As Exception
            'Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Protected Sub LoadObjectJudgement(id As String, oldornew As String)
        Try
            If oldornew = "old" Then
                JudgementDetail = AMLJudgementClassBefore.AMLListResult.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = id).FirstOrDefault
            Else
                JudgementDetail = AMLJudgementClassNew.AMLListResult.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = id).FirstOrDefault
            End If
            If JudgementDetail IsNot Nothing Then
                If JudgementDetail.JUDGEMENT_ISMATCH IsNot Nothing Then
                    If JudgementDetail.JUDGEMENT_ISMATCH Then
                        LblJudgementIsMatch.Text = "Match"
                    Else
                        LblJudgementIsMatch.Text = "Not Match"
                    End If
                End If
                If JudgementDetail.JUDGEMENT_COMMENT IsNot Nothing Then
                    JudgementComment.Text = JudgementDetail.JUDGEMENT_COMMENT
                End If

                If JudgementRequestForHeader IsNot Nothing Then
                    If JudgementDetail.MATCH_SCORE_NAME IsNot Nothing And JudgementRequestForHeader.NAME IsNot Nothing Then
                        Name_Score.Text = String.Format("{0:0.00}", JudgementDetail.MATCH_SCORE_NAME) & " %"
                        'diremark karena udh diganti menjadi persentase dari DB
                        'Name_Score.Text = Convert.ToString(Math.Round(Convert.ToDecimal(JudgementDetail.MATCH_SCORE_NAME * 100), 2, MidpointRounding.AwayFromZero)) & " %"
                    Else
                        Name_Score.Text = "-"
                    End If

                    If JudgementDetail.MATCH_SCORE_DOB IsNot Nothing And JudgementRequestForHeader.DOB IsNot Nothing Then
                        DoB_Score.Text = String.Format("{0:0.00}", JudgementDetail.MATCH_SCORE_DOB) & " %"
                        'diremark karena udh diganti menjadi persentase dari DB
                        'DoB_Score.Text = Convert.ToString(Math.Round(Convert.ToDecimal(JudgementDetail.MATCH_SCORE_DOB * 100), 2, MidpointRounding.AwayFromZero)) & " %"
                    Else
                        DoB_Score.Text = "-"
                    End If

                    If JudgementDetail.MATCH_SCORE_NATIONALITY IsNot Nothing And JudgementRequestForHeader.NATIONALITY IsNot Nothing Then
                        Nationality_Score.Text = String.Format("{0:0.00}", JudgementDetail.MATCH_SCORE_NATIONALITY) & " %"
                        'diremark karena udh diganti menjadi persentase dari DB
                        'Nationality_Score.Text = Convert.ToString(Math.Round(Convert.ToDecimal(JudgementDetail.MATCH_SCORE_NATIONALITY * 100), 2, MidpointRounding.AwayFromZero)) & " %"
                    Else
                        Nationality_Score.Text = "-"
                    End If

                    If JudgementDetail.MATCH_SCORE_IDENTITY_NUMBER IsNot Nothing And JudgementRequestForHeader.IDENTITY_NUMBER IsNot Nothing Then
                        Identity_Score.Text = String.Format("{0:0.00}", JudgementDetail.MATCH_SCORE_IDENTITY_NUMBER) & " %"
                        'diremark karena udh diganti menjadi persentase dari DB
                        'Identity_Score.Text = Convert.ToString(Math.Round(Convert.ToDecimal(JudgementDetail.MATCH_SCORE_IDENTITY_NUMBER * 100), 2, MidpointRounding.AwayFromZero)) & " %"
                    Else
                        Identity_Score.Text = "-"
                    End If

                    If JudgementDetail.MATCH_SCORE IsNot Nothing Then
                        Total_Score.Text = String.Format("{0:0.00}", JudgementDetail.MATCH_SCORE) & " %"
                        'diremark karena udh diganti menjadi persentase dari DB
                        'Total_Score.Text = Convert.ToString(Math.Round(Convert.ToDecimal(JudgementDetail.MATCH_SCORE * 100), 2, MidpointRounding.AwayFromZero)) & " %"
                    End If
                End If

                ObjAML_WATCHLIST_CLASS = AML_WATCHLIST_BLL.GetWatchlistClassByID(JudgementDetail.FK_AML_WATCHLIST_ID)
                If ObjAML_WATCHLIST_CLASS.objAML_WATCHLIST IsNot Nothing Then
                    With ObjAML_WATCHLIST_CLASS.objAML_WATCHLIST
                        WatchListID.Text = .PK_AML_WATCHLIST_ID
                        If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                            Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                            If objWatchlistCategory IsNot Nothing Then
                                WatchListCategory.Text = objWatchlistCategory.CATEGORY_NAME
                                '' 2023-10-13, Nael: Add Display Button Display Watchlist Worldcheck
                                Dim PkWatchlistWorldcheckCategory As String = ""

                                Dim strPkWatchlistWorldcheckCategory As String = "select ParameterValue"
                                strPkWatchlistWorldcheckCategory += " FROM AML_Global_Parameter a "
                                strPkWatchlistWorldcheckCategory += " where PK_GlobalReportParameter_ID = '14'"

                                PkWatchlistWorldcheckCategory = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strPkWatchlistWorldcheckCategory, Nothing)

                                If PkWatchlistWorldcheckCategory IsNot Nothing Then
                                    If .FK_AML_WATCHLIST_CATEGORY_ID = PkWatchlistWorldcheckCategory Then
                                        btn_OpenWindowWatchlist.Hidden = "false"

                                        Dim strIDCode As String = 16645 '' Module ID "AML_WATCHLIST_WORLDCHECK"
                                        IDModuleWatchlistEncrypted = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                                        Dim PkWatchlistWorldcheckID As String = ""

                                        Dim strPkWatchlistWorldcheckID As String = "select FK_AML_WATCHLIST_SOURCE_ID"
                                        strPkWatchlistWorldcheckID += " FROM AML_WATCHLIST a "
                                        strPkWatchlistWorldcheckID += " where PK_AML_WATCHLIST_ID = '" & .PK_AML_WATCHLIST_ID.ToString() & "'"

                                        PkWatchlistWorldcheckID = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strPkWatchlistWorldcheckID, Nothing)
                                        IDWatchlistEncrypted = Common.EncryptQueryString(PkWatchlistWorldcheckID, SystemParameterBLL.GetEncriptionKey)

                                        If IDModuleWatchlistEncrypted Is Nothing Or IDWatchlistEncrypted Is Nothing Then
                                            Throw New ApplicationException("Watchlist ID is not found.")
                                        End If
                                    Else
                                        btn_OpenWindowWatchlist.Hidden = "true"
                                    End If
                                End If
                                '' 2023-10-13, Nael: End
                            End If
                        End If
                        If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                            Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                            If objWatchlistType IsNot Nothing Then
                                WatchListType.Text = objWatchlistType.TYPE_NAME
                            End If
                        End If
                        If .NAME IsNot Nothing Then
                            FullName.Text = .NAME
                        End If
                        If .BIRTH_PLACE IsNot Nothing Then
                            PlaceofBirth.Text = .BIRTH_PLACE
                        End If
                        If .DATE_OF_BIRTH IsNot Nothing Then
                            DateofBirth.Text = CDate(.DATE_OF_BIRTH).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture)
                            'DateofBirth.Text = .DATE_OF_BIRTH
                        End If
                        If .YOB IsNot Nothing Then
                            YearofBirth.Text = .YOB
                        End If
                        If .NATIONALITY IsNot Nothing Then
                            Nationality.Text = .NATIONALITY
                        End If
                        If .CUSTOM_REMARK_1 IsNot Nothing Then
                            Remark1.Text = .CUSTOM_REMARK_1
                        End If
                        If .CUSTOM_REMARK_2 IsNot Nothing Then
                            Remark2.Text = .CUSTOM_REMARK_2
                        End If
                        If .CUSTOM_REMARK_3 IsNot Nothing Then
                            Remark3.Text = .CUSTOM_REMARK_3
                        End If
                        If .CUSTOM_REMARK_4 IsNot Nothing Then
                            Remark4.Text = .CUSTOM_REMARK_4
                        End If
                        If .CUSTOM_REMARK_5 IsNot Nothing Then
                            Remark5.Text = .CUSTOM_REMARK_5
                        End If
                    End With
                    Bind_AML_WATCHLIST_ALIAS()
                    Bind_AML_WATCHLIST_ADDRESS()
                    Bind_AML_WATCHLIST_IDENTITY()
                End If
            End If
        Catch ex As Exception
            'Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Protected Sub LoadDataAfter()
        Try
            If Not String.IsNullOrEmpty(ObjApproval.ModuleField) Then
                AMLJudgementClassNew = NawaBLL.Common.Deserialize(ObjApproval.ModuleField, GetType(NawaDevBLL.AMLJudgementClass))
            End If
            If AMLJudgementClassNew.AMLListResult IsNot Nothing Then
                BindJudgement(judgement_Item_New, AMLJudgementClassNew.AMLListResult)
            End If
        Catch ex As Exception
            'Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Protected Sub LoadDataBefore()
        Try
            If Not String.IsNullOrEmpty(ObjApproval.ModuleFieldBefore) Then
                AMLJudgementClassBefore = NawaBLL.Common.Deserialize(ObjApproval.ModuleFieldBefore, GetType(NawaDevBLL.AMLJudgementClass))
            End If
            If AMLJudgementClassBefore.AMLListResult IsNot Nothing Then
                BindJudgement(judgement_Item_Old, AMLJudgementClassBefore.AMLListResult)
            End If
        Catch ex As Exception
            'Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Protected Sub LoadJudgementHeader()
        Try
            If AMLJudgementClassNew.AMLJudgementHeader IsNot Nothing Then
                JudgementHeader = AMLJudgementClassNew.AMLJudgementHeader
            ElseIf AMLJudgementClassBefore.AMLJudgementHeader IsNot Nothing Then
                JudgementHeader = AMLJudgementClassBefore.AMLJudgementHeader
            End If
            If JudgementHeader IsNot Nothing Then
                If Not IsDBNull(JudgementHeader.FK_AML_SCREENING_REQUEST_ID) Then
                    JudgementRequestForHeader = AMLJudgementBLL.GetRequestForHeader(JudgementHeader.FK_AML_SCREENING_REQUEST_ID)
                End If

                If Not IsDBNull(JudgementHeader.CIF_NO) Then

                    Dim strQuery As String = "SELECT TOP 1 * FROM AML_CUSTOMER WHERE CIFNo='" & JudgementHeader.CIF_NO & "'"
                    Dim drCustomer As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)

                    Dim VwAMLJudgement As DataRow = NawaDAL.SQLHelper.ExecuteRow(
                        NawaDAL.SQLHelper.strConnectionString,
                        CommandType.Text,
                        "SELECT TOP 1 * FROM Vw_AML_JUDGEMENT WHERE CIF_NO = '" & JudgementHeader.CIF_NO & "'"
                    )

                    If drCustomer IsNot Nothing Then
                        If Not IsDBNull(drCustomer("FK_AML_Creation_Branch_Code")) Then
                            display_Request_Kode_Cabang.Text = drCustomer("FK_AML_Creation_Branch_Code")
                            Using objdb As New NawaDatadevEntities
                                Dim strBranchCode As String = drCustomer("FK_AML_Creation_Branch_Code")
                                Dim objbranch As AML_BRANCH = objdb.AML_BRANCH.Where(Function(x) x.FK_AML_BRANCH_CODE = strBranchCode).FirstOrDefault
                                If objbranch IsNot Nothing Then
                                    If objbranch.BRANCH_NAME IsNot Nothing Then
                                        display_Request_Nama_Cabang.Text = objbranch.BRANCH_NAME
                                    End If
                                    If objbranch.REGION_NAME IsNot Nothing Then
                                        display_Request_Kantor_Wilayah.Text = objbranch.REGION_NAME
                                    End If
                                End If
                            End Using
                        End If
                        If JudgementHeader.CreatedDate IsNot Nothing Then
                            display_Request_Waktu_Proses.Value = CDate(JudgementHeader.CreatedDate).ToString("dd-MMM-yyyy")
                        End If
                        If Not IsDBNull(drCustomer("FK_AML_Customer_Type_Code")) Then
                            Using objdb As New NawaDatadevEntities
                                Dim strCustomerType As String = drCustomer("FK_AML_Customer_Type_Code")
                                Dim objcustomertype As AML_CUSTOMER_TYPE = objdb.AML_CUSTOMER_TYPE.Where(Function(x) x.FK_AML_Customer_Type_Code = strCustomerType).FirstOrDefault
                                If objcustomertype IsNot Nothing Then
                                    If objcustomertype.Customer_Type_Name IsNot Nothing Then
                                        display_Request_Tipe_Nasabah.Text = objcustomertype.Customer_Type_Name
                                    End If
                                End If
                            End Using

                            '' Edit 21-Feb-2023, Felix. Baca FK_AML_Customer_Type_Code untuk Individu dari Global param, karena tiap bank bisa beda.
                            Dim Customer_Type_INDV As String = "I" '' Default dr product
                            Dim ParameterValue_PK15 As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue from AML_Global_Parameter where [PK_GlobalReportParameter_ID] = 15", Nothing)
                            If ParameterValue_PK15 IsNot Nothing Then
                                Customer_Type_INDV = ParameterValue_PK15
                            End If
                            ''If DataAMLCustomer.FK_AML_Customer_Type_Code.Equals("I") Then

                            If drCustomer("FK_AML_Customer_Type_Code").Equals(Customer_Type_INDV) Then
                                '' End 21-Feb-2023
                                If Not IsDBNull(drCustomer("PLACEOFBIRTH")) Then
                                    display_Request_Tempat_Lahir.Text = drCustomer("PLACEOFBIRTH")
                                End If
                                If Not IsDBNull(drCustomer("DATEOFBIRTH")) AndAlso drCustomer("DATEOFBIRTH") <> DateTime.MinValue Then
                                    display_Request_DOB.Text = CDate(drCustomer("DATEOFBIRTH")).ToString("dd-MMM-yyyy")
                                End If
                                If Not IsDBNull(drCustomer("FK_AML_PEKERJAAN_CODE")) Then
                                    Using objdb As New NawaDatadevEntities
                                        Dim strPekerjaanCode As String = drCustomer("FK_AML_PEKERJAAN_CODE")
                                        Dim objcustomerpekerjaan As AML_PEKERJAAN = objdb.AML_PEKERJAAN.Where(Function(x) x.FK_AML_PEKERJAAN_Code = strPekerjaanCode).FirstOrDefault
                                        If objcustomerpekerjaan IsNot Nothing Then
                                            If objcustomerpekerjaan.AML_PEKERJAAN_Name IsNot Nothing Then
                                                display_Request_Pekerjaan.Text = objcustomerpekerjaan.AML_PEKERJAAN_Name
                                            End If
                                        End If
                                    End Using
                                End If
                                '' Add 19-Dec-2022  , display jg POB, DOB, Pekerjaan
                                display_Request_Tempat_Lahir.Hidden = False
                                display_Request_DOB.Hidden = False
                                display_Request_Pekerjaan.Hidden = False
                                '' End 19-Dec-2022
                            Else
                                display_Request_Tempat_Lahir.Hide()
                                display_Request_DOB.Hide()
                                display_Request_Pekerjaan.Hide()
                            End If
                        End If
                        If Not IsDBNull(drCustomer("CIFNo")) Then
                            display_Request_CIF.Text = drCustomer("CIFNo")

                            Dim objParamID(0) As SqlParameter
                            objParamID(0) = New SqlParameter
                            objParamID(0).ParameterName = "@CIFNo"
                            objParamID(0).Value = drCustomer("CIFNo")
                            objParamID(0).DbType = SqlDbType.VarChar

                            Dim CUSTOMER_IDENTITY_NUMBER As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Get1CustomerIdentity_ByCIFNo", objParamID)

                            If CUSTOMER_IDENTITY_NUMBER IsNot Nothing Then
                                display_Request_Identity_Number.Value = CUSTOMER_IDENTITY_NUMBER
                            End If


                            Dim objParamAddress(0) As SqlParameter
                            objParamAddress(0) = New SqlParameter
                            objParamAddress(0).ParameterName = "@CIFNo"
                            objParamAddress(0).Value = drCustomer("CIFNo")
                            objParamAddress(0).DbType = SqlDbType.VarChar

                            Dim CUSTOMER_ADDRESS As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Get1CustomerAddress_ByCIFNo", objParamAddress)

                            If CUSTOMER_ADDRESS IsNot Nothing Then
                                display_Request_Alamat.Text = CUSTOMER_ADDRESS
                            End If
                            '' End 21-Feb-2023.
                        End If
                        If Not IsDBNull(drCustomer("CUSTOMERNAME")) Then
                            display_Request_Name.Text = drCustomer("CUSTOMERNAME")
                        End If
                        If Not IsDBNull(drCustomer("ALIAS")) Then
                            display_Request_Alias.Value = drCustomer("ALIAS")
                        End If
                        If Not IsDBNull(drCustomer("NPWP")) Then
                            display_Request_NPWP.Text = drCustomer("NPWP")
                        End If
                        If Not IsDBNull(drCustomer("FK_AML_CITIZENSHIP_CODE")) Then
                            Using objdb As New NawaDatadevEntities
                                Dim strCountryCode As String = drCustomer("FK_AML_CITIZENSHIP_CODE")
                                Dim objcustomercountry As AML_COUNTRY = objdb.AML_COUNTRY.Where(Function(x) x.FK_AML_COUNTRY_Code = strCountryCode).FirstOrDefault
                                If objcustomercountry IsNot Nothing Then
                                    If objcustomercountry.AML_COUNTRY_Name IsNot Nothing Then
                                        display_Request_Nationality.Text = objcustomercountry.AML_COUNTRY_Name
                                    End If
                                End If
                            End Using
                        End If

                        If Not IsDBNull(drCustomer("FK_AML_RISK_CODE")) Then
                            display_Request_Profil_Risiko_oneFCC.Text = drCustomer("FK_AML_RISK_CODE")
                        End If

                        If Not IsDBNull(VwAMLJudgement("STATUS")) Then
                            display_Request_Status_Judgement.Text = VwAMLJudgement("STATUS")
                        End If

                    End If
                End If

            End If
        Catch ex As Exception
            'Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            'Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Private Sub LoadColumn()
        Try
            ColumnActionLocation(gridJudgementItemOld, CommandColumnGridJudgementItemOld)
            ColumnActionLocation(gridJudgementItemNew, CommandColumnGridJudgementItemNew)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ALIAS()
        Try
            Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(ObjAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS)
            gridAliasInfo.GetStore().DataSource = objtable
            gridAliasInfo.GetStore().DataBind()
        Catch ex As Exception
            'Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub
    Protected Sub Bind_AML_WATCHLIST_ADDRESS()
        Try
            Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(ObjAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS)

            objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
            objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
                    If objCountry IsNot Nothing Then
                        item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
                    Else
                        item("COUNTRY_NAME") = Nothing
                    End If

                    Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
                    If objAddressType IsNot Nothing Then
                        item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
                    Else
                        item("AML_ADDRESS_TYPE_NAME") = Nothing
                    End If
                Next
            End If

            gridAddressInfo.GetStore().DataSource = objtable
            gridAddressInfo.GetStore().DataBind()
        Catch ex As Exception
            'Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub
    Protected Sub Bind_AML_WATCHLIST_IDENTITY()
        Try
            Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(ObjAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY)

            objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
                    If objIDENTITYType IsNot Nothing Then
                        item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
                    Else
                        item("AML_IDENTITY_TYPE_NAME") = Nothing
                    End If
                Next
            End If

            gridIdentityInfo.GetStore().DataSource = objtable
            gridIdentityInfo.GetStore().DataBind()
        Catch ex As Exception
            'Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Private Sub BindJudgement(store As Store, list As List(Of AML_SCREENING_RESULT_DETAIL))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_CATEGORY", GetType(String)))
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_TYPE", GetType(String)))
            objtable.Columns.Add(New DataColumn("ISMATCH", GetType(String)))
            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    If Not IsDBNull(item("FK_AML_WATCHLIST_CATEGORY_ID")) Then
                        item("AML_WATCHLIST_CATEGORY") = AMLJudgementBLL.GetWatchlistCategoryByID(item("FK_AML_WATCHLIST_CATEGORY_ID"))
                    Else
                        item("AML_WATCHLIST_CATEGORY") = ""
                    End If
                    If Not IsDBNull(item("FK_AML_WATCHLIST_TYPE_ID")) Then
                        item("AML_WATCHLIST_TYPE") = AMLJudgementBLL.GetWatchlistTypeByID(item("FK_AML_WATCHLIST_TYPE_ID"))
                    Else
                        item("AML_WATCHLIST_TYPE") = ""
                    End If
                    If Not IsDBNull(item("JUDGEMENT_ISMATCH")) Then
                        If item("JUDGEMENT_ISMATCH") Then
                            item("ISMATCH") = "Match"
                        Else
                            item("ISMATCH") = "Not Match"
                        End If
                    Else
                        item("ISMATCH") = ""
                    End If
                Next
            End If
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

#Region "Direct Events"
    Protected Sub GridcommandJudgementItemOld(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Judge" Then
                ClearOBJDetail()
                WindowJudge.Hidden = False
                LoadObjectJudgement(id, "old")
            ElseIf e.ExtraParams(1).Value = "Detail" Then
            ElseIf e.ExtraParams(1).Value = "Delete" Then
            ElseIf e.ExtraParams(1).Value = "Edit" Then
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandJudgementItemNew(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Judge" Then
                ClearOBJDetail()
                WindowJudge.Hidden = False
                LoadObjectJudgement(id, "new")
            ElseIf e.ExtraParams(1).Value = "Detail" Then
            ElseIf e.ExtraParams(1).Value = "Delete" Then
            ElseIf e.ExtraParams(1).Value = "Edit" Then
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            AMLJudgementBLL.Accept(ObjApproval.PK_ModuleApproval_ID)

            '18-Jun-2023 Adi : Tambah ubah semua flag STATUS_JUDGEMENT_CODE di AML_SCREENING_RESULT jadi 2
            '2023-08-02, Nael: karena dari deserialize xml nya udh ada set status judgement code, maka code ini saya comment
            'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "UPDATE AML_SCREENING_RESULT SET STATUS_JUDGEMENT_CODE=2, ApprovedBy='" & Common.SessionCurrentUser.UserID & "', ApprovedDate=GETDATE() WHERE CIF_NO='" & JudgementHeader.CIF_NO & "'", Nothing)

            LblConfirmation.Text = "Data Approved. Click Ok to Back To " & ObjModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnReject_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            AMLJudgementBLL.Reject(ObjApproval.PK_ModuleApproval_ID)

            '18-Jun-2023 Adi : Tambah ubah semua flag STATUS_JUDGEMENT_CODE di AML_SCREENING_RESULT jadi 3
            '15-Aug-2023 Nael: Dicomment, karena sekarang ada 2 kasus untuk rejected (request reopen -> closed(2), request edit -> rejected (3))
            'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "UPDATE AML_SCREENING_RESULT SET STATUS_JUDGEMENT_CODE=3, LastUpdateBy='" & Common.SessionCurrentUser.UserID & "', LastUpdateDate=GETDATE() WHERE CIF_NO='" & JudgementHeader.CIF_NO & "'", Nothing)

            LblConfirmation.Text = "Data Rejected. Click Ok to Back To " & ObjModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim strURL As String
            strURL = String.Format(Common.GetApplicationPath & "/Parameter/WaitingApproval.aspx?ModuleID={0}", Request.Params("ModuleID"))

            If Not ObjApproval Is Nothing Then
                If ObjApproval.CreatedBy <> Common.SessionCurrentUser.UserID Then
                    strURL = String.Format(Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID"))
                End If
            End If

            Net.X.Redirect(strURL)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs) Handles BtnConfirmation.DirectClick
        Try
            Net.X.Redirect(String.Format(Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
#Region "Felix 13-Sep-2023, tambah juga display WorldCheck di Approval Detail (Added by Nael 2023-10-13)"
    Public Property IDWatchlistEncrypted() As String
        Get
            Return Session("AMLJudgmentEdit.IDWatchlistEncrypted")
        End Get
        Set(ByVal value As String)
            Session("AMLJudgmentEdit.IDWatchlistEncrypted") = value
        End Set
    End Property
    Public Property IDModuleWatchlistEncrypted() As String
        Get
            Return Session("AMLJudgmentEdit.IDModuleWatchlistEncrypted")
        End Get
        Set(ByVal value As String)
            Session("AMLJudgmentEdit.IDModuleWatchlistEncrypted") = value
        End Set
    End Property
    Protected Sub btn_OpenWindowWatchlist_Click()
        Try
            WindowDisplayWatchlist.Hidden = False
            pnlContent.ClearContent()
            pnlContent.AnimCollapse = False
            pnlContent.Loader.SuspendScripting()
            pnlContent.Loader.Url = "~/Parameter/ParameterDetail.aspx?ID=" & IDWatchlistEncrypted & "&ModuleID=" & IDModuleWatchlistEncrypted & ""
            pnlContent.Loader.Params.Clear()
            pnlContent.LoadContent()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Display_Watchlist_Back_Click()
        Try
            WindowDisplayWatchlist.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class



