﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AML_ScreeningUpload_Detail.aspx.vb" Inherits="AML_ScreeningUpload_Detail" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };

        var prepareCommandCollection = function (grid, toolbar, rowIndex, record) {
            var DetailButton = toolbar.items.get(0);
            var wf = record.data.RESPONSE_CODE_DESCRIPTION
            if (DetailButton != undefined) {
                if (wf == "Found") {
                    DetailButton.setDisabled(false)
                } else {
                    DetailButton.setDisabled(true)
                }
            }
        }

        var prepareCommandCollection_Result = function (grid, toolbar, rowIndex, record) {
            var DetailButton = toolbar.items.get(0);
            var wf = record.data.NAME_WATCHLIST
            if (DetailButton != undefined) {
                if (wf != "") { /*Edited by Adi 3 Jun 2021 -- set Detail Disabled jika Nama Watch List kosong. Artinya screening result dari FATF dan OEDF (Hanya screening nationality)*/
                    DetailButton.setDisabled(false)
                } else {
                    DetailButton.setDisabled(true)
                }
            }
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="Watch List Data">
        <Items>

            <%-- Input for Screening Adhoc --%>
            <ext:Panel runat="server" Title="Screening Request" Collapsible="true" Layout="ColumnLayout" AnchorHorizontal="100%" ID="Panel1" Border="true" BodyStyle="padding:10px" MarginSpec="0 0 10 0">
                <Content>

                    <ext:FieldSet runat="server" ID="FieldSet7" Collapsible="false" Title="" Border="false" Layout="AnchorLayout" AnchorHorizontal="100%" ColumnWidth="0.6" Padding="0">
                        <Items>
                            <ext:DisplayField ID="df_Request_ID" AllowBlank="false" LabelWidth="150" runat="server" FieldLabel="Request ID" AnchorHorizontal="80%"></ext:DisplayField>
                            <ext:DisplayField ID="df_Request_By" AllowBlank="false" LabelWidth="150" runat="server" FieldLabel="Requested By" AnchorHorizontal="80%"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet runat="server" ID="FieldSet8" Collapsible="false" Title="" Border="false" Layout="AnchorLayout" AnchorHorizontal="100%" ColumnWidth="0.4" Padding="0">
                        <Items>
                            <ext:DisplayField ID="df_Request_Date" AllowBlank="false" LabelWidth="150" runat="server" FieldLabel="Requested Date" AnchorHorizontal="80%"></ext:DisplayField>
                            <ext:DisplayField ID="df_Response_Date" AllowBlank="false" LabelWidth="150" runat="server" FieldLabel="Response Date" AnchorHorizontal="80%"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>


                    <%--Grid Screening Request--%>
                    <ext:GridPanel ID="gp_Screening_Request" runat="server" Title="List of Screening Request Uploaded" AnchorHorizontal="100%" ColumnWidth="1">

                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store3" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="model3" IDProperty="">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_SCREENING_REQUEST_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIF_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DOB" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="NATIONALITY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDENTITY_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RESPONSE_CODE_DESCRIPTION" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column17" runat="server" DataIndex="CIF_NO" Text="CIF" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="NAME" Text="Name" Flex="1"></ext:Column>
                                <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="DOB" Text="Date Of Birth" Flex="1" format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column20" runat="server" DataIndex="NATIONALITY" Text="Nationality" Flex="1"></ext:Column>
                                <ext:Column ID="Column21" runat="server" DataIndex="IDENTITY_NUMBER" Text="ID Number" Flex="1"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="RESPONSE_CODE_DESCRIPTION" Text="Response" Flex="1"></ext:Column>

                                <ext:CommandColumn ID="cc_Screening_Request" runat="server" Text="Action" Width="100">
                                <Commands>
                                    <ext:GridCommand CommandName="Detail" Icon="ApplicationFormMagnify" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gc_Screening_Request">
                                        <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_REQUEST_ID" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                            </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--Grid Screening Request--%>
                </Content>
            </ext:Panel>

            <%-- Input for Screening Adhoc --%>
            <ext:Panel runat="server" Title="Searching Request" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlSCREENINGADHOC" Border="true" BodyStyle="padding:10px" Hidden="true">
                <Content>

                    <ext:Button ID="btn_Search_By_CIF" runat="server" Icon="Magnifier" Text="Search by CIF">
                        <DirectEvents>
                            <Click OnEvent="btn_Search_By_CIF_Click">
                                <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:FieldSet runat="server" ID="FieldSet6" Collapsible="false" Title="" Border="true" Layout="AnchorLayout" AnchorHorizontal="100%" MarginSpec="10 0">
                        <Items>
                            <ext:DisplayField ID="txt_CIF_SEARCH" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="CIF" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true" MarginSpec="10 0 0 0"></ext:DisplayField>
                            <ext:TextField ID="txt_NAME_SEARCH" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Name" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true">
                                <%--<DirectEvents><Change OnEvent="txt_NAME_SEARCH_OnChanged" /></DirectEvents>--%>
                            </ext:TextField>
                            <ext:DateField ID="txt_DATE_OF_BIRTH_SEARCH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="50%" Format="dd-MMM-yyyy">
                                <%--<DirectEvents><Change OnEvent="txt_DATE_OF_BIRTH_SEARCH_OnChanged" /></DirectEvents>--%>
                            </ext:DateField>
                            <ext:TextField ID="txt_NATIONALITY_SEARCH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Nationality" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true">
                                <%--<DirectEvents><Change OnEvent="txt_NATIONALITY_SEARCH_OnChanged" /></DirectEvents>--%>
                            </ext:TextField>
                            <ext:TextField ID="txt_IDENTITY_NUMBER_SEARCH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Identity Number" AnchorHorizontal="50%" MaxLength="100" EnforceMaxLength="true">
                                <%--<DirectEvents><Change OnEvent="txt_IDENTITY_NUMBER_SEARCH_OnChanged" /></DirectEvents>--%>
                            </ext:TextField>
                            <ext:DisplayField ID="txt_RESPONSE_SEARCH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Response Code" AnchorHorizontal="100%"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>

                    <ext:Button ID="btn_Screening_Search" runat="server" Icon="Magnifier" Text="Search" MarginSpec="0 10 0 0">
                        <DirectEvents>
                            <Click OnEvent="btn_Screening_Search_Click">
                                <EventMask ShowMask="true" Msg="Searching Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="btn_Screening_Clear" runat="server" Icon="ControlBlank" Text="Clear Search"> 
                        <DirectEvents>
                            <Click OnEvent="btn_Screening_Clear_Click">
                                <EventMask ShowMask="true" Msg="Clearing Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Content>
            </ext:Panel>
            <%-- End of Watch List General Information --%>

            <%-- AML Screening Result --%>
            <ext:Panel runat="server" Title="Screening Result" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlScreeningResult" Border="true" BodyStyle="padding:10px" MarginSpec="0 0 10 0" Hidden="true">
                <Items>

                    <ext:FieldSet runat="server" ID="FieldSet9" Collapsible="false" Title="Data Searched" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Items>
                            <ext:DisplayField ID="df_CIF_NO" LabelWidth="150" runat="server" FieldLabel="CIF" AnchorHorizontal="80%"></ext:DisplayField>
                            <ext:DisplayField ID="df_Name" LabelWidth="150" runat="server" FieldLabel="Name" AnchorHorizontal="80%"></ext:DisplayField>
                            <ext:DisplayField ID="df_DOB" LabelWidth="150" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="80%"></ext:DisplayField>
                            <ext:DisplayField ID="df_Nationality" LabelWidth="150" runat="server" FieldLabel="Nationality" AnchorHorizontal="80%"></ext:DisplayField>
                            <ext:DisplayField ID="df_Identity_Number" LabelWidth="150" runat="server" FieldLabel="Identity Number" AnchorHorizontal="80%"></ext:DisplayField>
                        </Items>
                    </ext:FieldSet>

                    <%--Grid Screening Result--%>
                        <ext:GridPanel ID="gp_Screening_Result" runat="server">

                            <View>
                                <ext:GridView runat="server" EnableTextSelection="true" />
                            </View>
                            <Store>
                                <ext:Store ID="StoreScreeningResult" runat="server" IsPagingStore="true" PageSize="10">
                                    <Model>
                                        <ext:Model runat="server" ID="modelScreeningResult" IDProperty="PK_AML_SCREENING_RESULT_DETAIL_ID">
                                            <Fields>
                                                <ext:ModelField Name="PK_AML_SCREENING_RESULT_DETAIL_ID" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="FK_AML_WATCHLIST_ID" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="NAME_WATCHLIST" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="FK_AML_WATCHLIST_CATEGORY_ID" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="CATEGORY_NAME" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="FK_AML_WATCHLIST_TYPE_ID" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="TYPE_NAME" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="DOB_WATCHLIST" Type="Date"></ext:ModelField>
                                                <ext:ModelField Name="NATIONALITY_WATCHLIST" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="IDENTITY_NUMBER_WATCHLIST" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="MATCH_SCORE" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="MATCH_SCORE_NAME" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="MATCH_SCORE_DOB" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="MATCH_SCORE_NATIONALITY" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="MATCH_SCORE_IDENTITY_NUMBER" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="MATCH_SCORE_PCT" Type="String"></ext:ModelField>
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                                    <ext:Column ID="Column22" runat="server" DataIndex="PK_AML_SCREENING_RESULT_DETAIL_ID" Text="ID" Flex="1" Hidden="true"></ext:Column>
                                    <ext:Column ID="Column19" runat="server" DataIndex="FK_AML_WATCHLIST_ID" Text="Watchlist ID" Flex="1" Hidden="true"></ext:Column>
                                    <ext:Column ID="Column412" runat="server" DataIndex="NAME" Text="Name" Flex="1"></ext:Column>
                                    <ext:Column ID="Column8" runat="server" DataIndex="NAME_WATCHLIST" Text="Name Watch List" Flex="1"></ext:Column>
                                    <ext:Column ID="Column9" runat="server" DataIndex="CATEGORY_NAME" Text="Watch List Category" Flex="1"></ext:Column>
                                    <ext:Column ID="Column10" runat="server" DataIndex="TYPE_NAME" Text="Watch List Type" Flex="1"></ext:Column>
                                    <ext:DateColumn ID="Column11" runat="server" DataIndex="DOB_WATCHLIST" Text="Date Of Birth" Flex="1" format="dd-MMM-yyyy"></ext:DateColumn>
                                    <ext:Column ID="Column12" runat="server" DataIndex="NATIONALITY_WATCHLIST" Text="Nationality" Flex="1"></ext:Column>
                                    <ext:Column ID="Column13" runat="server" DataIndex="IDENTITY_NUMBER_WATCHLIST" Text="ID Number" Flex="1"></ext:Column>
                                    <ext:NumberColumn ID="Column14" runat="server" DataIndex="MATCH_SCORE" Text="Match Score (%)" Flex="1" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                                    <ext:NumberColumn ID="Column15" runat="server" DataIndex="MATCH_SCORE_PCT" Text="Match Score" Flex="1" Format="#,##0.00 %" Hidden="true"></ext:NumberColumn>

                                    <ext:CommandColumn ID="cc_Screening_Result" runat="server" Text="Action" Width="100">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationFormMagnify" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_Screening_Result">
                                            <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <Plugins>
                                <ext:FilterHeader runat="server"></ext:FilterHeader>
                            </Plugins>
                            <BottomBar>
                                <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                            </BottomBar>
                        </ext:GridPanel>
                    <%--Grid Screening Result--%>
                    </Items>

            </ext:Panel>
            <%-- AML Screening Result --%>

        </Items>

        <Buttons>
            <ext:Button ID="btn_SCREENING_ADHOC_Save" runat="server" Icon="Disk" Text="Save" Hidden="true">
                <%--Todo--%>
                <DirectEvents>
                    <Click OnEvent="btn_SCREENING_ADHOC_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_SCREENING_ADHOC_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_SCREENING_ADHOC_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="Accept">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <%--Pop Up Watchlist General Information--%>
    <%-- Pop Up Windows --%>
    <ext:Window ID="Window_WATCHLIST_GENERAL_INFORMATION" Title="Screening Result Detail" Layout="AnchorLayout" runat="server" Modal="true" Hidden="true" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" Maximizable="true" BodyPadding="10">
        <%--<ext:Window ID="Window_WATCHLIST_GENERAL_INFORMATION" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="1500" Height="1500" Layout="FitLayout">--%>
        <Items>
        <%-- Search Scores--%>
           <ext:FormPanel runat="server" Title="Search Score Detail" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlScores" Border="true" BodyStyle="padding:10px" MarginSpec="0 0 10 0">
                <Content>
                    <ext:FieldSet runat="server" ID="FieldSet5" Collapsible="false" Title="" Border="false" Layout="ColumnLayout" AnchorHorizontal="100%" Padding="0">
                        <Items>
                            <ext:FieldSet runat="server" ID="FieldSet3" Collapsible="false" ColumnWidth="0.38" Title="Search Data">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Name" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Nationality" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Identity Number" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>
                            <ext:FieldSet runat="server" ID="FieldSet2" Collapsible="false" ColumnWidth="0.35" Title="Search Result" MarginSpec="0 5">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>
                            <ext:FieldSet runat="server" ID="FieldSet1" Collapsible="false" ColumnWidth="0.25" Title="Search Score">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%" ></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>

                            <ext:FieldSet runat="server" ID="FieldSet4" Collapsible="false" ColumnWidth="1" Title="" Border="false">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_TOTAL" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Total Score" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>

                        </Items>
                    </ext:FieldSet>

               </Content>
            </ext:FormPanel>
        <%-- End ofSearch Scores--%>

        <%-- Watch List General Information --%>
            <ext:FormPanel runat="server" Title="Watch List Information" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlWATCHLIST" Border="true" BodyStyle="padding:10px">
                <Content>

                    <ext:DisplayField ID="txt_AML_WATCHLIST_ID" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Watch List ID" AnchorHorizontal="80%"></ext:DisplayField>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_CATEGORY" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Watch List Category" AnchorHorizontal="80%" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_TYPE" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Watch List Type" AnchorHorizontal="80%" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_NAME" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_PLACE_OF_BIRTH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_DATE_OF_BIRTH" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:DisplayField ID="txt_YOB" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Year of Birth" AnchorHorizontal="50%" MaxLength="8000" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:DisplayField>
                    <ext:DisplayField ID="txt_NATIONALITY" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Nationality" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>

                    <%-- Watch List Alias --%>
                    <ext:GridPanel ID="gp_AML_WATCHLIST_ALIAS" runat="server" Title="Alias(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <TopBar>
                            <ext:Toolbar ID="toolbar12" runat="server">
                                <Items>
                                    <ext:Button ID="btn_AML_WATCHLIST_ALIAS_Add" runat="server" Text="Add Alias" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_AML_WATCHLIST_ALIAS_Add_Click">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_AML_WATCHLIST_ALIAS" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model29" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ALIAS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ALIAS_NAME" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                                <ext:Column ID="Column75" runat="server" DataIndex="ALIAS_NAME" Text="Alias Name" MinWidth="300" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_AML_WATCHLIST_ALIAS" runat="server" Text="Action" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationFormMagnify" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <%--<ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>--%>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_AML_WATCHLIST_ALIAS">
                                            <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_WATCHLIST_ALIAS_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Alias --%>

                    <%-- Watch List Address --%>
                    <ext:GridPanel ID="gp_AML_WATCHLIST_ADDRESS" runat="server" Title="Address(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <TopBar>
                            <ext:Toolbar ID="toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btn_AML_WATCHLIST_ADDRESS_Add" runat="server" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_AML_WATCHLIST_ADDRESS_Add_Click">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store1" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ADDRESS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_AML_COUNTRY_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="POSTAL_CODE" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                                <ext:Column ID="Column5" runat="server" DataIndex="AML_ADDRESS_TYPE_NAME" Text="Type" MinWidth="50"></ext:Column>
                                <ext:Column ID="Column1" runat="server" DataIndex="ADDRESS" Text="Address" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="FK_AML_COUNTRY_CODE" Text="Country Code" MinWidth="100" Hidden="true"></ext:Column>
                                <ext:Column ID="Column3" runat="server" DataIndex="COUNTRY_NAME" Text="Country Name" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="POSTAL_CODE" Text="Postal Code" MinWidth="100"></ext:Column>
                                <ext:CommandColumn ID="cc_AML_WATCHLIST_ADDRESS" runat="server" Text="Action" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationFormMagnify" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <%--<ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>--%>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_AML_WATCHLIST_ADDRESS">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_WATCHLIST_ADDRESS_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Address --%>

                    <%-- Watch List Identity --%>
                    <ext:GridPanel ID="gp_AML_WATCHLIST_IDENTITY" runat="server" Title="Identity(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <TopBar>
                            <ext:Toolbar ID="toolbar2" runat="server">
                                <Items>
                                    <ext:Button ID="btn_AML_WATCHLIST_IDENTITY_Add" runat="server" Text="Add Identity" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_AML_WATCHLIST_IDENTITY_Add_Click">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store2" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_IDENTITY_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_IDENTITY_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDENTITYNUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="70"></ext:RowNumbererColumn>
                                <ext:Column ID="Column6" runat="server" DataIndex="AML_IDENTITY_TYPE_NAME" Text="Type" MinWidth="200" Flex="1"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="IDENTITYNUMBER" Text="Identity" MinWidth="300"></ext:Column>
                                <ext:CommandColumn ID="cc_AML_WATCHLIST_IDENTITY" runat="server" Text="Action" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationFormMagnify" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <%--<ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>--%>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_AML_WATCHLIST_IDENTITY">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_WATCHLIST_IDENTITY_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Identity --%>


                    <ext:DisplayField ID="txt_CUSTOM_REMARK_1" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 1" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_CUSTOM_REMARK_2" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 2" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_CUSTOM_REMARK_3" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 3" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_CUSTOM_REMARK_4" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 4" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_CUSTOM_REMARK_5" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Remark 5" AnchorHorizontal="80%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>

                </Content>
            </ext:FormPanel>
            <%-- End of Watch List Remarks --%>
        </Items>
        <Buttons>
            <ext:Button ID="btn_AML_WATCHLIST_GENERAL_INFORMATION_Close" runat="server" Icon="Cancel" Text="Close">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_GENERAL_INFORMATION_Close_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.9, height: size.height * 0.9});" />

            <Resize Handler="#{Window_WATCHLIST_GENERAL_INFORMATION}.center()" />
        </Listeners>
    </ext:Window>
    

    <%--End of Pop Up wathclist General Information--%>

    <%-- Pop Up Window Alias --%>
    <ext:Window ID="Window_AML_WATCHLIST_ALIAS" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Maximizable="true">
        <Items>
            <ext:FormPanel ID="Window_Panel_AML_WATCHLIST_ALIAS" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_ALIAS_NAME" runat="server" FieldLabel="Alias Name" AllowBlank="false" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_AML_WATCHLIST_ALIAS_Save" runat="server" Icon="Disk" Text="Save Screening Result">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_ALIAS_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_AML_WATCHLIST_ALIAS_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_ALIAS_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.6});" />

            <Resize Handler="#{Window_AML_WATCHLIST_ALIAS}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Alias --%>

    <%-- Pop Up Window Address --%>
    <ext:Window ID="Window_AML_WATCHLIST_ADDRESS" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout" Maximizable="true">
        <Items>
            <ext:FormPanel ID="Window_Panel_AML_WATCHLIST_ADDRESS" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Content>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_ADDRESS_TYPE_CODE" AllowBlank="false" runat="server" FieldLabel="Watch List Address Type Code" LabelWidth="220" AnchorHorizontal="100%"/>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_ADDRESS" runat="server" FieldLabel="Address" LabelWidth="220" AllowBlank="false" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_ADDRESS_COUNTRY_CODE" AllowBlank="false" runat="server" FieldLabel="Watch List Address Country Code" LabelWidth="220" AnchorHorizontal="100%"/>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE" runat="server" FieldLabel="Postal Code" LabelWidth="220" AllowBlank="true" AnchorHorizontal="50%" MaxLength="8000" EnforceMaxLength="true" MaskRe="/[0-9]/"></ext:DisplayField>
                </Content>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_AML_WATCHLIST_ADDRESS_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_ADDRESS_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.6});" />

            <Resize Handler="#{Window_AML_WATCHLIST_ADDRESS}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Address --%>


    <%-- Pop Up Window Identity --%>
    <ext:Window ID="Window_AML_WATCHLIST_IDENTITY" runat="server" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout" Maximizable="true">
        <Items>
            <ext:FormPanel ID="FormPanel1" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Content>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_IDENTITY_TYPE_CODE" AllowBlank="false" runat="server" FieldLabel="Identity Type" LabelWidth="150" AnchorHorizontal="100%"/>
                    <ext:DisplayField ID="txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER" runat="server" FieldLabel="Identity Number" LabelWidth="150" AllowBlank="false" AnchorHorizontal="100%" MaxLength="8000" EnforceMaxLength="true"></ext:DisplayField>
                </Content>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_AML_WATCHLIST_IDENTITY_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_AML_WATCHLIST_IDENTITY_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.6});" />

            <Resize Handler="#{Window_AML_WATCHLIST_IDENTITY}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Identity --%>

    <%-- End of Pop Up Windows --%>

    <%-- Start of Pop Up Search CIF--%>
    <%-- Feature for searching CIF from AML_Customer --%>
    <ext:Window ID="window_SEARCH_CIF" runat="server" Modal="true" Hidden="true" BodyStyle="padding:5px" AutoScroll="true" ButtonAlign="Center" Maximizable="true">
        <Items>
            <ext:GridPanel ID="gp_SEARCH_CIF" runat="server" ClientIDMode="Static">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="StoreSEARCH_CIF" runat="server" IsPagingStore="true" RemoteFilter="true"  RemoteSort="true" 
                        OnReadData="Store_SEARCH_CIF" RemotePaging="true" ClientIDMode="Static" PageSize="20">
                        <Model>
                            <ext:Model runat="server" ID="ModelSearchCounterPartyAccount" IDProperty="Account_No">
                                <Fields>
                                    <ext:ModelField Name="CIF_NO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Name" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="DOB" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Nationality" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Identity_Number" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                        </Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader_SEARCH_CIF" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumberCounterPartyAccount" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Col_CIF_NO" runat="server" DataIndex="CIF_NO" Text="CIF No" Width="150"></ext:Column>
                        <ext:Column ID="Col_Name" runat="server" DataIndex="Name" Text="Name" Width="150" ></ext:Column>
                        <ext:DateColumn ID="Col_DOB" runat="server" DataIndex="DOB" Text="DOB" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="col_Nationality" runat="server" DataIndex="Nationality" Text="Nationality" Flex="1"></ext:Column>
                        <ext:Column ID="col_Identity_Number" runat="server" DataIndex="Identity_Number" Text="Identity Number" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="cc_SEARCH_CIF" runat="server" Text="Action">

                            <Commands>
                                <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
                                    <ToolTip Text="Select"></ToolTip>
                                </ext:GridCommand>
                            </Commands>

                            <DirectEvents>

                                <Command OnEvent="gc_SELECT_CIF">
                                    <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="200"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="CIF_NO" Value="record.data.CIF_NO" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="Name" Value="record.data.Name" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="DOB" Value="record.data.DOB" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="Nationality" Value="record.data.Nationality" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="Identity_Number" Value="record.data.Identity_Number" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_SEARCH_CIF" runat="server" HideRefresh="false" >
                        <Items>  
                            <ext:Button runat="server" Text="Close Picker" ID="Button5">
                                <Listeners>
                                    <Click Handler="#{window_SEARCH_CIF}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.98});" />

            <Resize Handler="#{window_SEARCH_CIF}.center()" />
        </Listeners>
    </ext:Window>
    <%-- Feature for searching CIF from AML_Customer --%>
    <%--End of Pop Up Search CIF--%>

</asp:Content>

