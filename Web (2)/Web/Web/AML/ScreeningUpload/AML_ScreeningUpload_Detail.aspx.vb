﻿Imports Ext
Imports NawaDevBLL
Imports NawaDevDAL
Imports Elmah
Imports System.Data
Imports NawaBLL
Imports System.Data.SqlClient
Imports System.Net.Http '' added 13 Apr 2021
Imports System.Threading.Tasks
Imports System.Timers
Imports System.Diagnostics
Imports OfficeOpenXml
Imports System.IO
Imports System.Drawing
Imports OfficeOpenXml.Style
Imports System.Reflection
Imports System.Globalization

Partial Class AML_ScreeningUpload_Detail
    Inherits ParentPage

    Const PATH_TEMP_DIR As String = "~\temp\ScreeningTemplate\"

    Public Property IDModule() As String
        Get
            Return Session("AML_ScreeningUpload_Detail.IDModule")
        End Get
        Set(ByVal value As String)
            Session("AML_ScreeningUpload_Detail.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("AML_ScreeningUpload_Detail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AML_ScreeningUpload_Detail.IDUnik") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_CLASS() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AML_ScreeningUpload_Detail.objAML_WATCHLIST_CLASS")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AML_ScreeningUpload_Detail.objAML_WATCHLIST_CLASS") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_ALIAS_Edit() As NawaDevDAL.AML_WATCHLIST_ALIAS
        Get
            Return Session("AML_ScreeningUpload_Detail.objAML_WATCHLIST_ALIAS_Edit")
        End Get
        Set(ByVal value As NawaDevDAL.AML_WATCHLIST_ALIAS)
            Session("AML_ScreeningUpload_Detail.objAML_WATCHLIST_ALIAS_Edit") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_ADDRESS_Edit() As NawaDevDAL.AML_WATCHLIST_ADDRESS
        Get
            Return Session("AML_ScreeningUpload_Detail.objAML_WATCHLIST_ADDRESS_Edit")
        End Get
        Set(ByVal value As NawaDevDAL.AML_WATCHLIST_ADDRESS)
            Session("AML_ScreeningUpload_Detail.objAML_WATCHLIST_ADDRESS_Edit") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_IDENTITY_Edit() As NawaDevDAL.AML_WATCHLIST_IDENTITY
        Get
            Return Session("AML_ScreeningUpload_Detail.objAML_WATCHLIST_IDENTITY_Edit")
        End Get
        Set(ByVal value As NawaDevDAL.AML_WATCHLIST_IDENTITY)
            Session("AML_ScreeningUpload_Detail.objAML_WATCHLIST_IDENTITY_Edit") = value
        End Set
    End Property

    'Public Property objAML_ScreeningUpload_Detail_Edit() As AML_ScreeningUpload_Detail_RESULT
    '    Get
    '        Return Session("AML_ScreeningUpload_Detail.objAML_ScreeningUpload_Detail_Edit")
    '    End Get
    '    Set(ByVal value As AML_ScreeningUpload_Detail_RESULT)
    '        Session("AML_ScreeningUpload_Detail.objAML_ScreeningUpload_Detail_Edit") = value
    '    End Set
    'End Property

    Public Property ID_AML_Watchlist As String
        Get
            Return Session("AML_ScreeningUpload_Detail.ID_AML_Watchlist")
        End Get
        Set(value As String)
            Session("AML_ScreeningUpload_Detail.ID_AML_Watchlist") = value
        End Set
    End Property

    Public Property objdt_SEARCH_RESULT As DataTable
        Get
            Return Session("AML_ScreeningUpload_Detail.objdt_SEARCH_RESULT")
        End Get
        Set(value As DataTable)
            Session("AML_ScreeningUpload_Detail.objdt_SEARCH_RESULT") = value
        End Set
    End Property

    '' Added by Felix 13 Apr 2021
    Public Property objdt_SEARCH_RESULT_API As DataTable
        Get
            Return Session("AML_ScreeningUpload_Detail.objdt_SEARCH_RESULT_API")
        End Get
        Set(value As DataTable)
            Session("AML_ScreeningUpload_Detail.objdt_SEARCH_RESULT_API") = value
        End Set
    End Property

    Public Property requestID As String
        Get
            Return Session("AML_ScreeningUpload_Detail.requestID")
        End Get
        Set(value As String)
            Session("AML_ScreeningUpload_Detail.requestID") = value
        End Set
    End Property
    '' End of 13 Apr 2021

    '' Added by Felix 26 Mar 2021
    Public Property NAME_WEIGHT As Double
        Get
            Return Session("AML_ScreeningUpload_Detail.NAME_WEIGHT")
        End Get
        Set(value As Double)
            Session("AML_ScreeningUpload_Detail.NAME_WEIGHT") = value
        End Set
    End Property

    Public Property DOB_WEIGHT As Double
        Get
            Return Session("AML_ScreeningUpload_Detail.DOB_WEIGHT")
        End Get
        Set(value As Double)
            Session("AML_ScreeningUpload_Detail.DOB_WEIGHT") = value
        End Set
    End Property

    Public Property NATIONALITY_WEIGHT As Double
        Get
            Return Session("AML_ScreeningUpload_Detail.NATIONALITY_WEIGHT")
        End Get
        Set(value As Double)
            Session("AML_ScreeningUpload_Detail.NATIONALITY_WEIGHT") = value
        End Set
    End Property

    Public Property IDENTITY_WEIGHT As Double
        Get
            Return Session("AML_ScreeningUpload_Detail.IDENTITY_WEIGHT")
        End Get
        Set(value As Double)
            Session("AML_ScreeningUpload_Detail.IDENTITY_WEIGHT") = value
        End Set
    End Property
    '' End 26 Mar 2021

    Sub ClearSession()
        objAML_WATCHLIST_CLASS = New AML_WATCHLIST_CLASS

        objAML_WATCHLIST_ADDRESS_Edit = Nothing
        objAML_WATCHLIST_ALIAS_Edit = Nothing
        objAML_WATCHLIST_IDENTITY_Edit = Nothing
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim IDData As String = Request.Params("ID")
            If IDData IsNot Nothing Then
                IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            End If

            IDModule = Request.Params("ModuleID")
            Dim intModuleID As Integer = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()

                FormPanelInput.Title = "Screening Upload Result"    'ObjModule.ModuleLabel & " - Result"

                SetCommandColumnLocation()

                LoadScreeningRequest()

                '-- Added by Felix 26 Mar 2021
                'Dim NAME_WEIGHT As Double 'Decimal
                'Dim DOB_WEIGHT As Double 'Decimal
                'Dim NATIONALITY_WEIGHT As Double 'Decimal
                'Dim IDENTITY_WEIGHT As Double 'Decimal

                '-- nebeng pake data table ini
                Dim objdt_SEARCH_RESULT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select NAME_WEIGHT, DOB_WEIGHT,NATIONALITY_WEIGHT,IDENTITY_WEIGHT FROM AML_SCREENING_MATCH_PARAMETER", Nothing)
                If objdt_SEARCH_RESULT IsNot Nothing Then
                    For Each row As DataRow In objdt_SEARCH_RESULT.Rows
                        NAME_WEIGHT = row.Item("NAME_WEIGHT")
                        DOB_WEIGHT = row.Item("DOB_WEIGHT")
                        NATIONALITY_WEIGHT = row.Item("NATIONALITY_WEIGHT")
                        IDENTITY_WEIGHT = row.Item("IDENTITY_WEIGHT")
                    Next row
                End If
                '-- End of 26 Mar 2021

                '-- 17 May 2021 Adi : Enabled/Disabled button Detail jika status Found/Not Found
                Dim objcommandcol As Ext.Net.CommandColumn = gp_Screening_Request.ColumnModel.Columns.Find(Function(x) x.ID = "cc_Screening_Request")
                objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"

                '-- 3 Jun 2021 Adi -- Gridpanel disable button Detail jika Name Watchlistnya kosong (khusus screening country FATF dan OEDC)
                Dim objcommandcol_result As Ext.Net.CommandColumn = gp_Screening_Result.ColumnModel.Columns.Find(Function(x) x.ID = "cc_Screening_Result")
                objcommandcol_result.PrepareToolbar.Fn = "prepareCommandCollection_Result"
                '-- End of 3 Jun 2021 Adi -- Gridpanel disable button Detail

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub



    '------- DOCUMENT SAVE
    Protected Sub btn_SCREENING_ADHOC_Save_Click()
        Try
            check_CIF_change()

            'If Not objdt_SEARCH_RESULT Is Nothing Then
            If Not objdt_SEARCH_RESULT_API Is Nothing Then
                Dim param(7) As SqlParameter
                param(0) = New SqlParameter
                param(0).ParameterName = "@userid"
                param(0).DbType = SqlDbType.VarChar
                param(0).Value = NawaBLL.Common.SessionCurrentUser.UserID

                param(1) = New SqlParameter
                param(1).ParameterName = "@TMP_udt_SEARCH_RESULT_ADHOC"
                param(1).SqlDbType = SqlDbType.Structured
                param(1).TypeName = "dbo.udt_SEARCH_RESULT_ADHOC"
                param(1).Value = objdt_SEARCH_RESULT_API

                param(2) = New SqlParameter
                param(2).ParameterName = "@Name_Search"
                param(2).SqlDbType = SqlDbType.VarChar
                param(2).Value = txt_NAME_SEARCH.Value

                param(3) = New SqlParameter
                param(3).ParameterName = "@DOB_Search"
                param(3).SqlDbType = SqlDbType.DateTime
                If CDate(txt_DATE_OF_BIRTH_SEARCH.Value) = DateTime.MinValue Then
                    param(3).Value = DBNull.Value
                Else
                    param(3).Value = CDate(txt_DATE_OF_BIRTH_SEARCH.Value)
                End If

                param(4) = New SqlParameter
                param(4).ParameterName = "@Nationality_Search"
                param(4).SqlDbType = SqlDbType.VarChar
                param(4).Value = txt_NATIONALITY_SEARCH.Value

                param(5) = New SqlParameter
                param(5).ParameterName = "@IdentityNumber_Search"
                param(5).SqlDbType = SqlDbType.VarChar
                param(5).Value = txt_IDENTITY_NUMBER_SEARCH.Value

                param(6) = New SqlParameter
                param(6).ParameterName = "@CIF_Search"
                param(6).SqlDbType = SqlDbType.VarChar
                param(6).Value = txt_CIF_SEARCH.Value

                param(7) = New SqlParameter
                param(7).ParameterName = "@requestID"
                param(7).SqlDbType = SqlDbType.VarChar
                param(7).Value = requestID

                'NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SAVE_SCREENING_ADHOC", param)
                NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SAVE_SCREENING_ADHOC_NEW", param)

            Else
                Throw New ApplicationException("No data To save.")
            End If

            '' To Do not found, but save
            LblConfirmation.Text = "Data Saved into Database"
            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_Screening_Adhoc_Back_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF DOCUMENT SAVE

    Protected Sub btn_Confirmation_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SetCommandColumnLocation()
        ColumnActionLocation(gp_AML_WATCHLIST_ALIAS, cc_AML_WATCHLIST_ALIAS)
        ColumnActionLocation(gp_AML_WATCHLIST_ADDRESS, cc_AML_WATCHLIST_ADDRESS)
        ColumnActionLocation(gp_AML_WATCHLIST_IDENTITY, cc_AML_WATCHLIST_IDENTITY)

        ColumnActionLocation(gp_Screening_Result, cc_Screening_Result)
        ColumnActionLocation(gp_Screening_Request, cc_Screening_Request)
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub AML_WATCHLIST_ADD_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.view
    End Sub
    Protected Sub btn_Screening_Search_Click(sender As Object, e As EventArgs)
        Try
            If String.IsNullOrEmpty(txt_NAME_SEARCH.Value) Then
                Throw New ApplicationException(txt_NAME_SEARCH.FieldLabel & " Is required.")
            End If

            If Not String.IsNullOrEmpty(txt_DATE_OF_BIRTH_SEARCH.Value) Then
                If CDate(txt_DATE_OF_BIRTH_SEARCH.Value) = DateTime.MinValue Then
                    txt_DATE_OF_BIRTH_SEARCH.Value = Nothing
                End If
            End If

            check_CIF_change()
            ScreeningAdhoc()
            Lock_Window_AML_ScreeningUpload_Detail()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ScreeningAdhoc()
        Try

            'Dim objParam(4) As SqlParameter
            'objParam(0) = New SqlParameter
            'objParam(0).ParameterName = "@Name"
            'objParam(0).Value = txt_NAME_SEARCH.Text

            'objParam(1) = New SqlParameter
            'objParam(1).ParameterName = "@DOB"
            'If CDate(txt_DATE_OF_BIRTH_SEARCH.Value) = DateTime.MinValue Then
            '    objParam(1).Value = DBNull.Value
            'Else
            '    objParam(1).Value = CDate(txt_DATE_OF_BIRTH_SEARCH.Value)
            'End If

            'objParam(2) = New SqlParameter
            'objParam(2).ParameterName = "@Nationality"
            'objParam(2).Value = txt_NATIONALITY_SEARCH.Text

            'objParam(3) = New SqlParameter
            'objParam(3).ParameterName = "@IdentityNumber"
            'objParam(3).Value = txt_IDENTITY_NUMBER_SEARCH.Text

            'objParam(4) = New SqlParameter
            'objParam(4).ParameterName = "@UserID"
            'objParam(4).Value = NawaBLL.Common.SessionCurrentUser.UserID
            ''NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_ScreeningUpload_Detail", objParam)
            ''Dim objdt As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_ScreeningUpload_Detail", objParam)
            'objdt_SEARCH_RESULT = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_ScreeningUpload_Detail", objParam)

            'If Not objdt_SEARCH_RESULT Is Nothing Then
            '    If objdt_SEARCH_RESULT.Rows.Count <> 0 Then
            '        gp_Screening_Result.GetStore.DataSource = objdt_SEARCH_RESULT
            '        gp_Screening_Result.GetStore.DataBind()
            '        txt_RESPONSE_SEARCH.Value = "Found"
            '    Else
            '        txt_RESPONSE_SEARCH.Value = "Not Found"
            '    End If
            'End If

            '' Added by Felix 13 Apr 2021
            Dim objParam(5) As SqlParameter
            objParam(0) = New SqlParameter
            objParam(0).ParameterName = "@Name"
            objParam(0).Value = txt_NAME_SEARCH.Text

            objParam(1) = New SqlParameter
            objParam(1).ParameterName = "@DOB"
            If CDate(txt_DATE_OF_BIRTH_SEARCH.Value) = DateTime.MinValue Then
                objParam(1).Value = DBNull.Value
            Else
                objParam(1).Value = CDate(txt_DATE_OF_BIRTH_SEARCH.Value)
            End If

            objParam(2) = New SqlParameter
            objParam(2).ParameterName = "@Nationality"
            objParam(2).Value = txt_NATIONALITY_SEARCH.Text

            objParam(3) = New SqlParameter
            objParam(3).ParameterName = "@IdentityNumber"
            objParam(3).Value = txt_IDENTITY_NUMBER_SEARCH.Text

            objParam(4) = New SqlParameter
            objParam(4).ParameterName = "@UserID"
            objParam(4).Value = NawaBLL.Common.SessionCurrentUser.UserID

            objParam(5) = New SqlParameter
            objParam(5).ParameterName = "@CIF"
            objParam(5).Value = txt_CIF_SEARCH.Value

            requestID = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_ADHOC_REQUEST_INSERT", objParam)

            Dim client As HttpClient = New HttpClient
            Dim Timer As Stopwatch = Stopwatch.StartNew()

            'Dim requestID As String = ""

            Dim responseName As Task(Of String) = client.GetStringAsync("http://goaml.southeastasia.cloudapp.azure.com:1300/Screening?requestid=" + requestID + "&fieldtocompare=NAME")
            Dim responseNationality As Task(Of String) = client.GetStringAsync("http://goaml.southeastasia.cloudapp.azure.com:1300/Screening?requestid=" + requestID + "&fieldtocompare=NATIONALITY")
            Dim responseIdentity As Task(Of String) = client.GetStringAsync("http://goaml.southeastasia.cloudapp.azure.com:1300/Screening?requestid=" + requestID + "&fieldtocompare=IDENTITY_NUMBER")

            Task.WaitAll(responseName, responseNationality, responseIdentity)

            Dim resultCodeName As String = responseName.Result.Replace("""", "").Trim()
            Dim resultCodeNationality As String = responseNationality.Result.Replace("""", "").Trim()
            Dim resultCodeIdentity As String = responseIdentity.Result.Replace("""", "").Trim()

            Timer.Stop()
            Dim timespanResult As TimeSpan = Timer.Elapsed
            Timer.Restart()


            Dim objParamAPI(3) As SqlParameter
            objParamAPI(0) = New SqlParameter
            objParamAPI(0).ParameterName = "@requestID"
            objParamAPI(0).Value = requestID

            objParamAPI(1) = New SqlParameter
            objParamAPI(1).ParameterName = "@resultCodeName"
            objParamAPI(1).Value = resultCodeName

            objParamAPI(2) = New SqlParameter
            objParamAPI(2).ParameterName = "@resultCodeNationality"
            objParamAPI(2).Value = resultCodeNationality

            objParamAPI(3) = New SqlParameter
            objParamAPI(3).ParameterName = "@resultCodeIdentity"
            objParamAPI(3).Value = resultCodeIdentity

            'Dim query As String = "SELECT ISNULL(ISNULL(SCREENING_NAME.LEFT_SIDE_INDEX, SCREENING_NATIONALITY.LEFT_SIDE_INDEX), SCREENING_IDENTITY_NUMBER.LEFT_SIDE_INDEX) LEFT_SIDE_INDEX,
            '	   ISNULL(ISNULL(SCREENING_NAME.RIGHT_SIDE_INDEX, SCREENING_NATIONALITY.RIGHT_SIDE_INDEX), SCREENING_IDENTITY_NUMBER.RIGHT_SIDE_INDEX) RIGHT_SIDE_INDEX,
            '	   ISNULL(SCREENING_NAME.SIMILARITY, 0) NAME_SIMILARITY,
            '	   ISNULL(SCREENING_NATIONALITY.SIMILARITY, 0) NATIONALITY_SIMILARITY,
            '	   ISNULL(SCREENING_IDENTITY_NUMBER.SIMILARITY, 0) IDENTITY_NUMBER_SIMILARITY
            'FROM (
            '		SELECT * FROM AML_SCREENING_RESULT_API
            '		WHERE RESULT_CODE = '" & resultCodeName & "'
            '	 ) SCREENING_NAME
            '	 FULL JOIN (
            '		SELECT * FROM AML_SCREENING_RESULT_API
            '		WHERE RESULT_CODE = '" & resultCodeNationality & "'
            '	 ) SCREENING_NATIONALITY
            '	 ON SCREENING_NAME.LEFT_SIDE_INDEX = SCREENING_NATIONALITY.LEFT_SIDE_INDEX
            '		AND SCREENING_NAME.RIGHT_SIDE_INDEX = SCREENING_NATIONALITY.RIGHT_SIDE_INDEX
            '	 FULL JOIN (
            '		SELECT * FROM AML_SCREENING_RESULT_API
            '		WHERE RESULT_CODE = '" & resultCodeIdentity & "'
            '	 ) SCREENING_IDENTITY_NUMBER
            '	 ON ISNULL(SCREENING_NAME.LEFT_SIDE_INDEX, SCREENING_NATIONALITY.LEFT_SIDE_INDEX) = SCREENING_IDENTITY_NUMBER.LEFT_SIDE_INDEX
            '		AND ISNULL(SCREENING_NAME.RIGHT_SIDE_INDEX, SCREENING_NATIONALITY.RIGHT_SIDE_INDEX) = SCREENING_IDENTITY_NUMBER.RIGHT_SIDE_INDEX
            '	 JOIN AML_SCREENING_REQUEST
            '	 ON ISNULL(ISNULL(SCREENING_NAME.LEFT_SIDE_INDEX, SCREENING_NATIONALITY.LEFT_SIDE_INDEX), SCREENING_IDENTITY_NUMBER.LEFT_SIDE_INDEX) = PK_AML_SCREENING_REQUEST_ID
            '	 JOIN AML_WATCHLIST_SCREENING
            '	 ON ISNULL(ISNULL(SCREENING_NAME.RIGHT_SIDE_INDEX, SCREENING_NATIONALITY.RIGHT_SIDE_INDEX), SCREENING_IDENTITY_NUMBER.RIGHT_SIDE_INDEX) = PK_AML_WATCHLIST_SCREENING_ID"

            objdt_SEARCH_RESULT_API = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_ScreeningUpload_Detail_API", objParamAPI)

            Timer.[Stop]()
            Dim timespanSelect As TimeSpan = Timer.Elapsed
            '' End of Felix

            If Not objdt_SEARCH_RESULT_API Is Nothing Then
                If objdt_SEARCH_RESULT_API.Rows.Count <> 0 Then
                    '18-Apr-2021 Adi : Tambah column untuk Match Score Percentage
                    Dim objtable As Data.DataTable = objdt_SEARCH_RESULT_API

                    'Edited by Felix 20 Apr 2021
                    'objtable.Columns.Add(New DataColumn("Match_Score_Pct", GetType(Double)))
                    'For Each item As Data.DataRow In objtable.Rows
                    '    item("Match_Score_Pct") = CDbl(item("Match_Score")) * 100
                    'Next
                    '------------------------------------------------------------

                    gp_Screening_Result.GetStore.DataSource = objtable  'objdt_SEARCH_RESULT_API
                    gp_Screening_Result.GetStore.DataBind()
                    'txt_RESPONSE_SEARCH.Value = "Found ( " & Math.Round(timespanSelect.TotalSeconds, 2) & " s )"
                    txt_RESPONSE_SEARCH.Value = "Found ( " & Math.Round(timespanResult.TotalSeconds, 2) & " s )"
                Else
                    '19-Apr-2021 Felix : Tambah column juga untuk yg not Match, biar udt nya ga error pas save
                    'Dim objtable As Data.DataTable = objdt_SEARCH_RESULT_API 'Edited by Felix 20 Apr 2021
                    'objtable.Columns.Add(New DataColumn("Match_Score_Pct", GetType(Double))) 'Edited by Felix 20 Apr 2021
                    'txt_RESPONSE_SEARCH.Value = "Not Found ( " & Math.Round(timespanSelect.TotalSeconds, 2) & " s )"
                    txt_RESPONSE_SEARCH.Value = "Not Found ( " & Math.Round(timespanResult.TotalSeconds, 2) & " s )"
                End If
            End If



        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

            txt_RESPONSE_SEARCH.Value = ex.Message
        End Try
    End Sub

    Protected Sub btn_Screening_Clear_Click(sender As Object, e As EventArgs)
        Try
            Clean_Window_AML_ScreeningUpload_Detail()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    '----- If Value is changed, Clear txt_CIF_SEARCH
    'Protected Sub txt_NAME_SEARCH_OnChanged()
    '    txt_CIF_SEARCH.Value = Nothing
    'End Sub

    'Protected Sub txt_DATE_OF_BIRTH_SEARCH_OnChanged()
    '    txt_CIF_SEARCH.Value = Nothing
    'End Sub

    'Protected Sub txt_NATIONALITY_SEARCH_OnChanged()
    '    txt_CIF_SEARCH.Value = Nothing
    'End Sub

    'Protected Sub txt_IDENTITY_NUMBER_SEARCH_OnChanged()
    '    txt_CIF_SEARCH.Value = Nothing
    'End Sub
    '----- END If Value is changed, Clear txt_CIF_SEARCH

    Protected Sub Clean_Window_AML_ScreeningUpload_Detail()
        'Clean fields
        txt_NAME_SEARCH.Value = Nothing
        txt_DATE_OF_BIRTH_SEARCH.Value = Nothing
        txt_NATIONALITY_SEARCH.Value = Nothing
        txt_IDENTITY_NUMBER_SEARCH.Value = Nothing
        txt_RESPONSE_SEARCH.Value = Nothing
        txt_CIF_SEARCH.Value = Nothing

        'Set fields' ReadOnly
        txt_NAME_SEARCH.ReadOnly = False
        txt_DATE_OF_BIRTH_SEARCH.ReadOnly = False
        txt_NATIONALITY_SEARCH.ReadOnly = False
        txt_IDENTITY_NUMBER_SEARCH.ReadOnly = False
        'Show Buttons
        btn_Screening_Search.Hidden = False
        btn_Search_By_CIF.Hidden = False

        objdt_SEARCH_RESULT = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "Select top 0 0 As FK_AML_WATCHLIST_ID ,'' as [Name] ,'' as Name_Watch_list ,1 as FK_AML_WATCHLIST_CATEGORY_ID ,'' as Watch_list_Category ,1 as FK_AML_WATCHLIST_TYPE_ID ,'' as WATCH_LIST_TYPE ,getdate() as Dob ,'' as Nationality ,'' as Identity_number ,cast( 99.9 as float) as Match_Score ,cast( 99.9 as float) as MATCH_SCORE_NAME ,cast( 99.9 as float) as MATCH_SCORE_DOB ,cast( 99.9 as float) as MATCH_SCORE_NATIONALITY ,cast( 99.9 as float) as MATCH_SCORE_IDENTITY_NUMBER ")
        gp_Screening_Result.GetStore.DataSource = objdt_SEARCH_RESULT
        gp_Screening_Result.GetStore.DataBind()

        'Added on 14 Apr 2021
        requestID = Nothing
    End Sub

    Protected Sub Lock_Window_AML_ScreeningUpload_Detail()

        'Set fields' ReadOnly
        txt_NAME_SEARCH.ReadOnly = True
        txt_DATE_OF_BIRTH_SEARCH.ReadOnly = True
        txt_NATIONALITY_SEARCH.ReadOnly = True
        txt_IDENTITY_NUMBER_SEARCH.ReadOnly = True
        'Show Buttons
        btn_Screening_Search.Hidden = True
        btn_Search_By_CIF.Hidden = True

    End Sub

    Protected Sub gc_Screening_Result(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value        'PK_AML_SCREENING_RESULT_DETAIL_ID
            Dim strAction As String = e.ExtraParams(1).Value    'Action

            If strAction = "Detail" Then
                Using objdb As New NawaDatadevEntities
                    Dim objResultDetail = objdb.AML_SCREENING_RESULT_DETAIL.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = CLng(strID)).FirstOrDefault()
                    If Not objResultDetail Is Nothing Then
                        Dim objResult = objdb.AML_SCREENING_RESULT.Where(Function(x) x.PK_AML_SCREENING_RESULT_ID = objResultDetail.FK_AML_SCREENING_RESULT_ID).FirstOrDefault()
                        If Not objResult Is Nothing Then
                            Dim objRequest = objdb.AML_SCREENING_REQUEST.Where(Function(x) x.PK_AML_SCREENING_REQUEST_ID = objResult.FK_AML_SCREENING_REQUEST_ID).FirstOrDefault()
                            If Not objRequest Is Nothing Then
                                'Load Watchlist
                                ID_AML_Watchlist = objResultDetail.FK_AML_WATCHLIST_ID
                                DetailScreeningAdhoc(ID_AML_Watchlist, "Detail")

                                'Load Information to Search
                                txt_MATCH_SCORE_NAME_SEARCH.Value = objRequest.NAME
                                If IsNothing(objRequest.DOB) OrElse CDate(objRequest.DOB) = DateTime.MinValue Then
                                    txt_MATCH_SCORE_DOB_SEARCH.Value = "-"
                                Else
                                    txt_MATCH_SCORE_DOB_SEARCH.Value = Convert.ToDateTime(objRequest.DOB).ToString("dd-MMM-yyyy")
                                End If
                                txt_MATCH_SCORE_Nationality_SEARCH.Value = IIf(String.IsNullOrEmpty(objRequest.NATIONALITY), "-", objRequest.NATIONALITY)
                                txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH.Value = IIf(String.IsNullOrEmpty(objRequest.IDENTITY_NUMBER), "-", objRequest.IDENTITY_NUMBER)

                                'Screening Result
                                txt_MATCH_SCORE_NAME_WATCHLIST.Value = objResultDetail.NAME_WATCHLIST
                                If IsNothing(objRequest.DOB) OrElse CDate(objRequest.DOB) = DateTime.MinValue Then
                                    txt_MATCH_SCORE_DOB_WATCHLIST.Value = ""
                                Else
                                    txt_MATCH_SCORE_DOB_WATCHLIST.Value = Convert.ToDateTime(objResultDetail.DOB_WATCHLIST).ToString("dd-MMM-yyyy")
                                End If
                                txt_MATCH_SCORE_Nationality_WATCHLIST.Value = objResultDetail.NATIONALITY_WATCHLIST
                                txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST.Value = objResultDetail.IDENTITY_NUMBER_WATCHLIST

                                'Screening Match Score
                                Dim objParamMatchScore = objdb.AML_SCREENING_MATCH_PARAMETER.FirstOrDefault
                                If Not objParamMatchScore Is Nothing Then
                                    Dim dblMatchScoreName As Double = CDbl(objResultDetail.MATCH_SCORE_NAME) * CDbl(objParamMatchScore.NAME_WEIGHT / 100)
                                    Dim dblMatchScoreDOB As Double = CDbl(objResultDetail.MATCH_SCORE_DOB) * CDbl(objParamMatchScore.DOB_WEIGHT / 100)
                                    Dim dblMatchScoreNationality As Double = CDbl(objResultDetail.MATCH_SCORE_NATIONALITY) * CDbl(objParamMatchScore.NATIONALITY_WEIGHT / 100)
                                    Dim dblMatchScoreIdentity As Double = CDbl(objResultDetail.MATCH_SCORE_IDENTITY_NUMBER) * CDbl(objParamMatchScore.IDENTITY_WEIGHT / 100)
                                    Dim dblMatchScoreTotal = dblMatchScoreName + dblMatchScoreDOB + dblMatchScoreNationality + dblMatchScoreIdentity

                                    Dim strMatchScoreName As String = objParamMatchScore.NAME_WEIGHT.ToString() & " % x " & CDbl(objResultDetail.MATCH_SCORE_NAME).ToString("#,##0.00") & " % = " & dblMatchScoreName.ToString("#,##0.00") & " %"
                                    Dim strMatchScoreDOB As String = objParamMatchScore.DOB_WEIGHT.ToString() & " % x " & CDbl(objResultDetail.MATCH_SCORE_DOB).ToString("#,##0.00") & " % = " & dblMatchScoreDOB.ToString("#,##0.00") & " %"
                                    Dim strMatchScoreNationality As String = objParamMatchScore.NATIONALITY_WEIGHT.ToString() & " % x " & CDbl(objResultDetail.MATCH_SCORE_NATIONALITY).ToString("#,##0.00") & " % = " & dblMatchScoreNationality.ToString("#,##0.00") & " %"
                                    Dim strMatchScoreIdentity As String = objParamMatchScore.IDENTITY_WEIGHT.ToString() & " % x " & CDbl(objResultDetail.MATCH_SCORE_IDENTITY_NUMBER) & " % = " & dblMatchScoreIdentity.ToString("#,##0.00") & " %"

                                    'Show the Formula so the user clear about the score
                                    Dim strDividend As String = dblMatchScoreName.ToString("#,##0.00")
                                    Dim strDivisor As String = objParamMatchScore.NAME_WEIGHT.ToString()
                                    If Not IsNothing(objRequest.DOB) AndAlso CDate(objRequest.DOB) <> DateTime.MinValue Then
                                        strDividend = strDividend & " + " & dblMatchScoreDOB.ToString("#,##0.00")
                                        strDivisor = strDivisor & " + " & objParamMatchScore.DOB_WEIGHT.ToString()
                                    End If
                                    If Not String.IsNullOrEmpty(objRequest.NATIONALITY) Then
                                        strDividend = strDividend & " + " & dblMatchScoreNationality.ToString("#,##0.00")
                                        strDivisor = strDivisor & " + " & objParamMatchScore.NATIONALITY_WEIGHT.ToString()
                                    End If
                                    If Not String.IsNullOrEmpty(objRequest.IDENTITY_NUMBER) Then
                                        strDividend = strDividend & " + " & dblMatchScoreIdentity.ToString("#,##0.00")
                                        strDivisor = strDivisor & " + " & objParamMatchScore.IDENTITY_WEIGHT.ToString()
                                    End If
                                    strDividend = "( " & strDividend & " % )"
                                    strDivisor = "( " & strDivisor & " % )"

                                    txt_MATCH_SCORE_NAME.Value = strMatchScoreName
                                    txt_MATCH_SCORE_DOB.Value = IIf(IsNothing(objRequest.DOB) OrElse CDate(objRequest.DOB) = DateTime.MinValue, "-", strMatchScoreDOB)
                                    txt_MATCH_SCORE_Nationality.Value = IIf(String.IsNullOrEmpty(objRequest.NATIONALITY), "-", strMatchScoreNationality)
                                    txt_MATCH_SCORE_IDENTITY_NUMBER.Value = IIf(String.IsNullOrEmpty(objRequest.IDENTITY_NUMBER), "-", strMatchScoreIdentity)
                                    txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(objResultDetail.MATCH_SCORE).ToString("#,##0.00") & " %"
                                End If
                            End If
                        End If
                    End If
                End Using
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DetailScreeningAdhoc(ID As String, command As String)
        Try

            ID_AML_Watchlist = ID

            Window_WATCHLIST_GENERAL_INFORMATION.Hidden = False

            LoadWatchList()

            SetControlReadOnly()

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Search_By_CIF_Click(sender As Object, e As DirectEventArgs)
        Try

            StoreSEARCH_CIF.Reload()
            window_SEARCH_CIF.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_SELECT_CIF(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            txt_NAME_SEARCH.Value = e.ExtraParams(1).Value
            txt_DATE_OF_BIRTH_SEARCH.Value = e.ExtraParams(2).Value
            txt_NATIONALITY_SEARCH.Value = e.ExtraParams(3).Value
            txt_IDENTITY_NUMBER_SEARCH.Value = e.ExtraParams(4).Value
            txt_RESPONSE_SEARCH.Value = Nothing
            txt_CIF_SEARCH.Value = e.ExtraParams(0).Value '' Assigned last bcs onChange to clear it when other text box is changed, will be triggered

            window_SEARCH_CIF.Hidden = True

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub check_CIF_change() '' Refresh CIF when text input are changed
        Try
            'Dim CIF_Change As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF_NO FROM vw_SEARCH_CIF_SCREENING_ADHOC where [Name] = '" + txt_NAME_SEARCH.Value + "' and datediff(day,DOB,'" + txt_DATE_OF_BIRTH_SEARCH.Value + "') = 0 and Nationality = '" + txt_NATIONALITY_SEARCH.Value + "' and Identity_Number = '" + txt_IDENTITY_NUMBER_SEARCH.Value + "'")
            Dim QueryCheckCIF As String = "Select CIF_NO FROM vw_SEARCH_CIF_SCREENING_ADHOC where [Name] = '" + txt_NAME_SEARCH.Value + "' "

            If Not CDate(txt_DATE_OF_BIRTH_SEARCH.Value) = DateTime.MinValue Then
                QueryCheckCIF = QueryCheckCIF + " and datediff(day,DOB,try_convert(datetime,'" + txt_DATE_OF_BIRTH_SEARCH.Value + "')) = 0 "
            End If

            If txt_NATIONALITY_SEARCH.Value <> "" Then
                QueryCheckCIF = QueryCheckCIF + " and Nationality = '" + txt_NATIONALITY_SEARCH.Value + "' "
            End If

            If txt_IDENTITY_NUMBER_SEARCH.Value <> "" Then
                QueryCheckCIF = QueryCheckCIF + " and Identity_Number = '" + txt_IDENTITY_NUMBER_SEARCH.Value + "'"
            End If

            'Dim CIF_Change As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, "select CIF_NO FROM vw_SEARCH_CIF_SCREENING_ADHOC where [Name] = '" + txt_NAME_SEARCH.Value + "' and datediff(day,DOB,'" + txt_DATE_OF_BIRTH_SEARCH.Value + "') = 0 and Nationality = '" + txt_NATIONALITY_SEARCH.Value + "' and Identity_Number = '" + txt_IDENTITY_NUMBER_SEARCH.Value + "'", Nothing)
            Dim CIF_Change As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, Data.CommandType.Text, QueryCheckCIF, Nothing)
            If Not CIF_Change Is Nothing Then
                txt_CIF_SEARCH.Value = CIF_Change
            Else
                txt_CIF_SEARCH.Value = Nothing
            End If


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_SEARCH_CIF(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 20

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            If strsort = "" Then
                strsort = "CIF_NO asc, Name asc"
            End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_SEARCH_CIF_SCREENING_ADHOC", "CIF_NO, Name, DOB, Nationality, Identity_Number", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start <0 OrElse limit <0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            StoreSEARCH_CIF.DataSource = DataPaging
            StoreSEARCH_CIF.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    '------- LOAD WATCHLIST
    Protected Sub LoadWatchList()
        Try
            'Load Watchlist
            objAML_WATCHLIST_CLASS = AML_WATCHLIST_BLL.GetWatchlistClassByID(ID_AML_Watchlist)

            'Bind Data Header
            With objAML_WATCHLIST_CLASS.objAML_WATCHLIST
                txt_AML_WATCHLIST_ID.Value = .PK_AML_WATCHLIST_ID

                txt_AML_WATCHLIST_CATEGORY.Value = .FK_AML_WATCHLIST_CATEGORY_ID
                If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                    Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                    If objWatchlistCategory IsNot Nothing Then
                        txt_AML_WATCHLIST_CATEGORY.Value &= " - " & objWatchlistCategory.CATEGORY_NAME
                    End If
                End If

                If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                    txt_AML_WATCHLIST_TYPE.Value = .FK_AML_WATCHLIST_TYPE_ID
                    Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                    If objWatchlistType IsNot Nothing Then
                        txt_AML_WATCHLIST_TYPE.Value &= " - " & objWatchlistType.TYPE_NAME
                    End If
                End If

                txt_NAME.Value = .NAME
                If .DATE_OF_BIRTH.HasValue Then
                    txt_DATE_OF_BIRTH.Value = .DATE_OF_BIRTH.GetValueOrDefault().ToString("dd-MMM-yyyy")
                End If
                txt_PLACE_OF_BIRTH.Value = .BIRTH_PLACE
                txt_YOB.Value = .YOB
                txt_NATIONALITY.Value = .NATIONALITY

                txt_CUSTOM_REMARK_1.Value = .CUSTOM_REMARK_1
                txt_CUSTOM_REMARK_2.Value = .CUSTOM_REMARK_2
                txt_CUSTOM_REMARK_3.Value = .CUSTOM_REMARK_3
                txt_CUSTOM_REMARK_4.Value = .CUSTOM_REMARK_4
                txt_CUSTOM_REMARK_5.Value = .CUSTOM_REMARK_5
            End With

            'Bind Data Detail to Grid Panel
            Bind_AML_WATCHLIST_ALIAS()
            Bind_AML_WATCHLIST_ADDRESS()
            Bind_AML_WATCHLIST_IDENTITY()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------- END OF LOAD WATCHLIST

    '------ Window Watchlist General Information -----------------------
    Protected Sub btn_AML_WATCHLIST_GENERAL_INFORMATION_Close_Click()
        Try
            'Hide window pop up
            Window_WATCHLIST_GENERAL_INFORMATION.Hidden = True
            'pnlSCREENINGADHOC.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    '------ Window Watchlist General Information -----------------------

    '------- WATCHLIST ALIAS
    Protected Sub btn_AML_WATCHLIST_ALIAS_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objAML_WATCHLIST_ALIAS_Edit = Nothing
            Clean_Window_AML_WATCHLIST_ALIAS()

            'Show window pop up
            Window_AML_WATCHLIST_ALIAS.Title = "Watch List Alias - Add"
            Window_AML_WATCHLIST_ALIAS.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_AML_WATCHLIST_ALIAS(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Find(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID = strID)
                If objToDelete IsNot Nothing Then
                    objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Remove(objToDelete)
                End If
                Bind_AML_WATCHLIST_ALIAS()
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objAML_WATCHLIST_ALIAS_Edit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Find(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID = strID)
                Load_Window_AML_WATCHLIST_ALIAS(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_ALIAS_Cancel_Click()
        Try
            'Hide window pop up
            Window_AML_WATCHLIST_ALIAS.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_ALIAS_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(txt_AML_WATCHLIST_ALIAS_NAME.Value) Then
                Throw New ApplicationException(txt_AML_WATCHLIST_ALIAS_NAME.FieldLabel & " harus diisi.")
            End If

            'Action save here
            Dim intPK As Long = -1

            If objAML_WATCHLIST_ALIAS_Edit Is Nothing Then  'Add
                Dim objAdd As New NawaDevDAL.AML_WATCHLIST_ALIAS
                If objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS IsNot Nothing AndAlso objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Count > 0 Then
                    intPK = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Min(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID)
                    If intPK > 0 Then
                        intPK = -1
                    Else
                        intPK = intPK - 1
                    End If
                End If

                With objAdd
                    .PK_AML_WATCHLIST_ALIAS_ID = intPK
                    .ALIAS_NAME = Trim(txt_AML_WATCHLIST_ALIAS_NAME.Value)
                End With

                objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Add(objAdd)
            Else    'Edit
                Dim objEdit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS.Find(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID = objAML_WATCHLIST_ALIAS_Edit.PK_AML_WATCHLIST_ALIAS_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        .ALIAS_NAME = Trim(txt_AML_WATCHLIST_ALIAS_NAME.Value)
                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_AML_WATCHLIST_ALIAS()

            'Hide window popup
            Window_AML_WATCHLIST_ALIAS.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ALIAS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS)

        'Running the following script for additional columns
        'objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        'If objtable.Rows.Count > 0 Then
        '    For Each item As Data.DataRow In objtable.Rows
        '        item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
        '    Next
        'End If
        gp_AML_WATCHLIST_ALIAS.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_ALIAS.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_AML_WATCHLIST_ALIAS()
        'Clean fields
        txt_AML_WATCHLIST_ALIAS_NAME.Value = Nothing

        'Set fields' ReadOnly
        txt_AML_WATCHLIST_ALIAS_NAME.ReadOnly = False

        'Show Buttons
        btn_AML_WATCHLIST_ALIAS_Save.Hidden = False
    End Sub

    Protected Sub Load_Window_AML_WATCHLIST_ALIAS(strAction As String)
        'Clean window pop up
        Clean_Window_AML_WATCHLIST_ALIAS()

        'Populate fields
        With objAML_WATCHLIST_ALIAS_Edit
            txt_AML_WATCHLIST_ALIAS_NAME.Value = .ALIAS_NAME
        End With

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            txt_AML_WATCHLIST_ALIAS_NAME.ReadOnly = False
        Else
            txt_AML_WATCHLIST_ALIAS_NAME.ReadOnly = True
            btn_AML_WATCHLIST_ALIAS_Save.Hidden = True
        End If

        'Show window pop up
        Window_AML_WATCHLIST_ALIAS.Title = "Watch List Alias - " & strAction
        Window_AML_WATCHLIST_ALIAS.Hidden = False
    End Sub
    '------- END OF WATCHLIST ALIAS

    '------- WATCHLIST ADDRESS
    Protected Sub btn_AML_WATCHLIST_ADDRESS_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objAML_WATCHLIST_ADDRESS_Edit = Nothing
            Clean_Window_AML_WATCHLIST_ADDRESS()

            'Show window pop up
            Window_AML_WATCHLIST_ADDRESS.Title = "Watch List Address - Add"
            Window_AML_WATCHLIST_ADDRESS.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_AML_WATCHLIST_ADDRESS(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS.Find(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID = strID)
                If objToDelete IsNot Nothing Then
                    objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS.Remove(objToDelete)
                End If
                Bind_AML_WATCHLIST_ADDRESS()
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objAML_WATCHLIST_ADDRESS_Edit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS.Find(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID = strID)
                Load_Window_AML_WATCHLIST_ADDRESS(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_ADDRESS_Cancel_Click()
        Try
            'Hide window pop up
            Window_AML_WATCHLIST_ADDRESS.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ADDRESS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS)

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
                If objCountry IsNot Nothing Then
                    item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
                Else
                    item("COUNTRY_NAME") = Nothing
                End If

                Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
                If objAddressType IsNot Nothing Then
                    item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
                Else
                    item("AML_ADDRESS_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gp_AML_WATCHLIST_ADDRESS.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_ADDRESS.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_AML_WATCHLIST_ADDRESS()
        'Clean fields
        txt_AML_WATCHLIST_ADDRESS_TYPE_CODE.Value = Nothing
        txt_AML_WATCHLIST_ADDRESS.Value = Nothing
        txt_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.Value = Nothing
        txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.Value = Nothing

        'Set fields' ReadOnly
        txt_AML_WATCHLIST_ADDRESS_TYPE_CODE.ReadOnly = False
        txt_AML_WATCHLIST_ADDRESS.ReadOnly = False
        txt_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.ReadOnly = False
        txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.ReadOnly = False

    End Sub

    Protected Sub Load_Window_AML_WATCHLIST_ADDRESS(strAction As String)
        'Clean window pop up
        Clean_Window_AML_WATCHLIST_ADDRESS()

        'Populate fields
        With objAML_WATCHLIST_ADDRESS_Edit
            txt_AML_WATCHLIST_ADDRESS.Value = .ADDRESS
            txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.Value = .POSTAL_CODE

            If Not IsNothing(.FK_AML_ADDRESS_TYPE_CODE) Then
                txt_AML_WATCHLIST_ADDRESS_TYPE_CODE.Value = .FK_AML_ADDRESS_TYPE_CODE
                Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(.FK_AML_ADDRESS_TYPE_CODE)
                If objAddressType IsNot Nothing Then
                    txt_AML_WATCHLIST_ADDRESS_TYPE_CODE.Value &= " - " & objAddressType.ADDRESS_TYPE_NAME
                End If
            End If

            If Not IsNothing(.FK_AML_COUNTRY_CODE) Then
                txt_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.Value = .FK_AML_COUNTRY_CODE
                Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(.FK_AML_COUNTRY_CODE)
                If objCountry IsNot Nothing Then
                    txt_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.Value &= " - " & objCountry.AML_COUNTRY_Name
                End If
            End If

        End With

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            txt_AML_WATCHLIST_ADDRESS_TYPE_CODE.ReadOnly = True
            txt_AML_WATCHLIST_ADDRESS.ReadOnly = True
            txt_AML_WATCHLIST_ADDRESS_COUNTRY_CODE.ReadOnly = True
            txt_AML_WATCHLIST_ADDRESS_POSTAL_CODE.ReadOnly = True
        End If

        'Show window pop up
        Window_AML_WATCHLIST_ADDRESS.Title = "Watch List Address - " & strAction
        Window_AML_WATCHLIST_ADDRESS.Hidden = False
    End Sub
    '------- END OF WATCHLIST ADDRESS

    '------- WATCHLIST IDENTITY
    Protected Sub btn_AML_WATCHLIST_IDENTITY_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            objAML_WATCHLIST_IDENTITY_Edit = Nothing
            Clean_Window_AML_WATCHLIST_IDENTITY()

            'Show window pop up
            Window_AML_WATCHLIST_IDENTITY.Title = "Watch List Identity - Add"
            Window_AML_WATCHLIST_IDENTITY.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_AML_WATCHLIST_IDENTITY(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strAction = "Delete" Then
                Dim objToDelete = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY.Find(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID = strID)
                If objToDelete IsNot Nothing Then
                    objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY.Remove(objToDelete)
                End If
                Bind_AML_WATCHLIST_IDENTITY()
            ElseIf strAction = "Edit" Or strAction = "Detail" Then
                objAML_WATCHLIST_IDENTITY_Edit = objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY.Find(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID = strID)
                Load_Window_AML_WATCHLIST_IDENTITY(strAction)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_AML_WATCHLIST_IDENTITY_Cancel_Click()
        Try
            'Hide window pop up
            Window_AML_WATCHLIST_IDENTITY.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_IDENTITY()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY)

        'Running the following script for additional columns
        objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
                If objIDENTITYType IsNot Nothing Then
                    item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
                Else
                    item("AML_IDENTITY_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gp_AML_WATCHLIST_IDENTITY.GetStore().DataSource = objtable
        gp_AML_WATCHLIST_IDENTITY.GetStore().DataBind()
    End Sub

    Protected Sub Clean_Window_AML_WATCHLIST_IDENTITY()
        'Clean fields
        txt_AML_WATCHLIST_IDENTITY_TYPE_CODE.Value = Nothing
        txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.Value = Nothing

        'Set fields' ReadOnly
        txt_AML_WATCHLIST_IDENTITY_TYPE_CODE.ReadOnly = False
        txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.ReadOnly = False

    End Sub

    Protected Sub Load_Window_AML_WATCHLIST_IDENTITY(strAction As String)
        'Clean window pop up
        Clean_Window_AML_WATCHLIST_IDENTITY()

        'Populate fields
        With objAML_WATCHLIST_IDENTITY_Edit
            txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.Value = .IDENTITYNUMBER


            If Not IsNothing(.FK_AML_IDENTITY_TYPE_CODE) Then
                txt_AML_WATCHLIST_IDENTITY_TYPE_CODE.Value = .FK_AML_IDENTITY_TYPE_CODE
                Dim objIdentityType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(.FK_AML_IDENTITY_TYPE_CODE)
                If objIdentityType IsNot Nothing Then
                    txt_AML_WATCHLIST_IDENTITY_TYPE_CODE.Value &= " - " & objIdentityType.IDENTITY_TYPE_NAME
                End If
            End If
        End With

        'Set fields' ReadOnly
        If strAction = "Detail" Then
            txt_AML_WATCHLIST_IDENTITY_TYPE_CODE.ReadOnly = True
            txt_AML_WATCHLIST_IDENTITY_IDENTITYNUMBER.ReadOnly = True
        End If

        'Show window pop up
        Window_AML_WATCHLIST_IDENTITY.Title = "Watch List Identity - " & strAction
        Window_AML_WATCHLIST_IDENTITY.Hidden = False
    End Sub
    '------- END OF WATCHLIST IDENTITY

    Sub SetControlReadOnly()
        'Read Only property
        txt_AML_WATCHLIST_CATEGORY.ReadOnly = True
        txt_AML_WATCHLIST_TYPE.ReadOnly = True
        txt_NAME.ReadOnly = True
        txt_PLACE_OF_BIRTH.ReadOnly = True
        txt_DATE_OF_BIRTH.ReadOnly = True
        txt_YOB.ReadOnly = True
        txt_NATIONALITY.ReadOnly = True
        txt_CUSTOM_REMARK_1.ReadOnly = True
        txt_CUSTOM_REMARK_2.ReadOnly = True
        txt_CUSTOM_REMARK_3.ReadOnly = True
        txt_CUSTOM_REMARK_4.ReadOnly = True
        txt_CUSTOM_REMARK_5.ReadOnly = True

        'Background Color
        'txt_NAME.FieldStyle = "background-color:#ddd;"
        'txt_PLACE_OF_BIRTH.FieldStyle = "background-color:#ddd;"
        'txt_DATE_OF_BIRTH.FieldStyle = "background-color:#ddd;"
        'txt_YOB.FieldStyle = "background-color:#ddd;"
        'txt_NATIONALITY.FieldStyle = "background-color:#ddd;"
        'txt_CUSTOM_REMARK_1.FieldStyle = "background-color:#ddd;"
        'txt_CUSTOM_REMARK_2.FieldStyle = "background-color:#ddd;"
        'txt_CUSTOM_REMARK_3.FieldStyle = "background-color:#ddd;"
        'txt_CUSTOM_REMARK_4.FieldStyle = "background-color:#ddd;"
        'txt_CUSTOM_REMARK_5.FieldStyle = "background-color:#ddd;"

        'Hide Button
        'btn_WATCHLIST_Submit.Hidden = True

        gp_AML_WATCHLIST_ALIAS.TopBar.Item(0).Hidden = True
        'cc_AML_WATCHLIST_ALIAS.Commands.RemoveAt(1)
        'cc_AML_WATCHLIST_ALIAS.Commands.RemoveAt(1)

        gp_AML_WATCHLIST_ADDRESS.TopBar.Item(0).Hidden = True
        'cc_AML_WATCHLIST_ADDRESS.Commands.RemoveAt(1)
        'cc_AML_WATCHLIST_ADDRESS.Commands.RemoveAt(1)

        gp_AML_WATCHLIST_IDENTITY.TopBar.Item(0).Hidden = True
        'cc_AML_WATCHLIST_IDENTITY.Commands.RemoveAt(1)
        'cc_AML_WATCHLIST_IDENTITY.Commands.RemoveAt(1)

    End Sub
    'Public Class AML_ScreeningUpload_Detail_RESULT

    '    Public FK_AML_WATCHLIST_ID As String
    '    Public Name As String
    '    Public Name_Watchlist As String
    '    Public Watchlist_Category As String
    '    Public Watchlist_Type As String
    '    Public Dob As DateTime
    '    Public Nationality As String
    '    Public Identity_number As String
    '    Public SIMILARITY As Decimal

    'End Class

#Region "3-Mei-2021 : Detail hasil Screening Upload"
    Public Property listAML_ScreeningUpload_Detail As List(Of NawaDevDAL.AML_SCREENING_REQUEST)
        Get
            Return Session("AML_ScreeningRequest_Upload.listAML_ScreeningUpload_Detail")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_REQUEST))
            Session("AML_ScreeningRequest_Upload.listAML_ScreeningUpload_Detail") = value
        End Set
    End Property

    Sub LoadScreeningRequest()
        Try
            Using objdb As New NawaDatadevEntities
                Dim objRequest = objdb.AML_SCREENING_REQUEST.Where(Function(x) x.PK_AML_SCREENING_REQUEST_ID = IDUnik).FirstOrDefault
                If Not objRequest Is Nothing Then
                    df_Request_ID.Text = objRequest.REQUEST_ID
                    df_Request_By.Text = objRequest.CreatedBy
                    df_Request_Date.Value = CDate(objRequest.CreatedDate).ToString("dd-MMM-yyyy HH:mm:ss", CultureInfo.InvariantCulture)
                    If Not IsNothing(objRequest.RESPONSE_DATE) Then
                        df_Response_Date.Value = CDate(objRequest.RESPONSE_DATE).ToString("dd-MMM-yyyy HH:mm:ss", CultureInfo.InvariantCulture)
                    End If
                End If

                'Bind to Grid
                Dim dtRequest As DataTable = New DataTable
                If Not objRequest Is Nothing Then
                    dtRequest = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT req.*, arc.RESPONSE_CODE_DESCRIPTION FROM AML_SCREENING_REQUEST req JOIN AML_RESPONSE_CODE arc ON req.FK_AML_RESPONSE_CODE_ID = arc.PK_AML_RESPONSE_CODE_ID WHERE req.REQUEST_ID='" & objRequest.REQUEST_ID & "'")
                End If
                gp_Screening_Request.GetStore.DataSource = dtRequest
                gp_Screening_Request.GetStore.DataBind()

                'Auto focus ke display field CIF_NO
                gp_Screening_Result.Focus()

            End Using
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_Screening_Request(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value
            ID_AML_Watchlist = strID

            If strAction = "Detail" Then
                LoadScreeningResult(CLng(strID))
            End If

            pnlScreeningResult.Hidden = False

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadScreeningResult(pkRequestID As Long)
        Try
            'Kosongkan objects
            df_CIF_NO.Text = ""
            df_Name.Text = ""
            df_DOB.Text = ""
            df_Nationality.Text = ""
            df_Identity_Number.Text = ""

            'Load Requested Data
            Using objdb As New NawaDatadevEntities
                Dim objRequest = objdb.AML_SCREENING_REQUEST.Where(Function(x) x.PK_AML_SCREENING_REQUEST_ID = pkRequestID).FirstOrDefault
                If Not objRequest Is Nothing Then
                    If Not IsNothing(objRequest.CIF_NO) Then
                        df_CIF_NO.Text = objRequest.CIF_NO
                    End If
                    If Not IsNothing(objRequest.NAME) Then
                        df_Name.Text = objRequest.NAME
                    End If
                    If Not IsNothing(objRequest.DOB) Then
                        df_DOB.Text = CDate(objRequest.DOB).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture)
                    End If
                    If Not IsNothing(objRequest.NATIONALITY) Then
                        df_Nationality.Text = objRequest.NATIONALITY
                    End If
                    If Not IsNothing(objRequest.IDENTITY_NUMBER) Then
                        df_Identity_Number.Text = objRequest.IDENTITY_NUMBER
                    End If
                End If
            End Using

            'Bind to Grid
            Dim dtResult As DataTable = New DataTable
            dtResult = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM vw_AML_SCREENING_RESULT_DETAIL WHERE FK_AML_SCREENING_REQUEST_ID=" & pkRequestID)

            gp_Screening_Result.GetStore.DataSource = dtResult
            gp_Screening_Result.GetStore.DataBind()

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#End Region
End Class