﻿Imports System.Data
Imports System.Data.SqlClient
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDAL
Imports NawaDevBLL
Imports NawaDevDAL
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevBLL.GlobalReportFunctionBLL
Imports System.Text
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.IO
Imports System.Globalization
Imports System.Data.Entity


Partial Class AMLJudgementProspectApprovalDetail
    Inherits Parent

#Region "Session"
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("AMLJudgementProspectApprovalDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AMLJudgementProspectApprovalDetail.ObjModule") = value
        End Set
    End Property

    Public Property listJudgementItem As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
        Get
            Return Session("AMLJudgementProspectApprovalDetail.listJudgementItem")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL))
            Session("AMLJudgementProspectApprovalDetail.listJudgementItem") = value
        End Set
    End Property

    Public Property listJudgementItemchange As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
        Get
            Return Session("AMLJudgementProspectApprovalDetail.listJudgementItemchange")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL))
            Session("AMLJudgementProspectApprovalDetail.listJudgementItemchange") = value
        End Set
    End Property

    Public Property judgementheader As NawaDevDAL.AML_SCREENING_RESULT
        Get
            Return Session("AMLJudgementProspectApprovalDetail.judgementheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT)
            Session("AMLJudgementProspectApprovalDetail.judgementheader") = value
        End Set
    End Property
    Public Property judgementrequestforheader As NawaDevDAL.AML_SCREENING_REQUEST
        Get
            Return Session("AMLJudgementProspectApprovalDetail.judgementrequestforheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_REQUEST)
            Session("AMLJudgementProspectApprovalDetail.judgementrequestforheader") = value
        End Set
    End Property
    Public Property judgementdetail As NawaDevDAL.AML_SCREENING_RESULT_DETAIL
        Get
            Return Session("AMLJudgementProspectApprovalDetail.judgementdetail")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            Session("AMLJudgementProspectApprovalDetail.judgementdetail") = value
        End Set
    End Property
    Public Property judgementdetailchange As NawaDevDAL.AML_SCREENING_RESULT_DETAIL
        Get
            Return Session("AMLJudgementProspectApprovalDetail.judgementdetailchange")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            Session("AMLJudgementProspectApprovalDetail.judgementdetailchange") = value
        End Set
    End Property
    Public Property checkerjudgeitems As NawaDevDAL.AML_SCREENING_RESULT_DETAIL
        Get
            Return Session("AMLJudgementProspectApprovalDetail.checkerjudgeitems")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            Session("AMLJudgementProspectApprovalDetail.checkerjudgeitems") = value
        End Set
    End Property
    Public Property indexofjudgementitems As Integer
        Get
            Return Session("AMLJudgementProspectApprovalDetail.indexofjudgementitems")
        End Get
        Set(ByVal value As Integer)
            Session("AMLJudgementProspectApprovalDetail.indexofjudgementitems") = value
        End Set
    End Property

    Public Property indexofjudgementitemschange As Integer
        Get
            Return Session("AMLJudgementProspectApprovalDetail.indexofjudgementitemschange")
        End Get
        Set(ByVal value As Integer)
            Session("AMLJudgementProspectApprovalDetail.indexofjudgementitemschange") = value
        End Set
    End Property
    Public Property dataID As String
        Get
            Return Session("AMLJudgementProspectApprovalDetail.dataID")
        End Get
        Set(ByVal value As String)
            Session("AMLJudgementProspectApprovalDetail.dataID") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_CLASS() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AMLJudgementProspectApprovalDetail.objAML_WATCHLIST_CLASS")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AMLJudgementProspectApprovalDetail.objAML_WATCHLIST_CLASS") = value
        End Set
    End Property

    Public Property judgementClass() As NawaDevBLL.AMLJudgementClass
        Get
            Return Session("AMLJudgementProspectApprovalDetail.judgementClass")
        End Get
        Set(ByVal value As NawaDevBLL.AMLJudgementClass)
            Session("AMLJudgementProspectApprovalDetail.judgementClass") = value
        End Set
    End Property

    'daniel 20210419
    Public Property listwatchlistalreadymatch As List(Of NawaDevDAL.AML_SCREENING_CUSTOMER_MATCH)
        Get
            Return Session("AMLJudgementProspectApprovalDetail.listcutomeralreadyinwatchlist")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_CUSTOMER_MATCH))
            Session("AMLJudgementProspectApprovalDetail.listcutomeralreadyinwatchlist") = value
        End Set
    End Property

    Public Property listAffecteddatafromlistmatch As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
        Get
            Return Session("AMLJudgementProspectApprovalDetail.listAffecteddatafromlistmatch")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL))
            Session("AMLJudgementProspectApprovalDetail.listAffecteddatafromlistmatch") = value
        End Set
    End Property
    'end daniel 20210419
    Public Property dataamlcustomer As AML_CUSTOMER
        Get
            Return Session("AMLJudgementProspectApprovalDetail.dataamlcustomer")
        End Get
        Set(ByVal value As AML_CUSTOMER)
            Session("AMLJudgementProspectApprovalDetail.dataamlcustomer") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()
                Dim strmodule As String = Request.Params("ModuleID")
                Dim intmodule As Integer = Common.DecryptQueryString(strmodule, SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)
                If ObjModule IsNot Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Approval) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If
                Dim dataStr As String = Request.Params("ID")
                If dataStr IsNot Nothing Then
                    dataID = Common.DecryptQueryString(dataStr, SystemParameterBLL.GetEncriptionKey)
                End If
                If dataID IsNot Nothing Then
                    If AML_WATCHLIST_BLL.IsExistsInApproval(ObjModule.ModuleName, dataID) Then
                        LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."

                        Panelconfirmation.Hidden = False
                        PanelInfo.Hidden = True
                    End If
                    LoadData(dataID)
                Else
                    Throw New ApplicationException("An Error Occurred While Loading the Data With Id")
                End If
                LoadColumn()
                Dim objcommandcol As Ext.Net.CommandColumn = gridJudgementItem.ColumnModel.Columns.Find(Function(x) x.ID = "CommandColumnGridJudgementItem")
                objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ClearSession()
        Try
            judgementheader = New AML_SCREENING_RESULT
            listJudgementItem = New List(Of AML_SCREENING_RESULT_DETAIL)
            judgementrequestforheader = New AML_SCREENING_REQUEST
            listJudgementItemchange = New List(Of AML_SCREENING_RESULT_DETAIL)
            listwatchlistalreadymatch = New List(Of NawaDevDAL.AML_SCREENING_CUSTOMER_MATCH)
            listAffecteddatafromlistmatch = New List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
            dataamlcustomer = New AML_CUSTOMER
            ClearOBJDetail()
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Private Sub ClearOBJDetail()
        Try
            judgementdetail = New AML_SCREENING_RESULT_DETAIL
            indexofjudgementitems = Nothing
            judgementdetailchange = New AML_SCREENING_RESULT_DETAIL
            indexofjudgementitemschange = Nothing
            objAML_WATCHLIST_CLASS = New AML_WATCHLIST_CLASS

            'Name_Score.Clear()
            'DoB_Score.Clear()
            'Nationality_Score.Clear()
            'Identity_Score.Clear()
            'Total_Score.Clear()

            WatchListID.Clear()
            WatchListCategory.Clear()
            WatchListType.Clear()
            FullName.Clear()
            PlaceofBirth.Clear()
            DateofBirth.Clear()
            YearofBirth.Clear()
            Nationality.Clear()

            Remark1.Clear()
            Remark2.Clear()
            Remark3.Clear()
            Remark4.Clear()
            Remark5.Clear()

            RBJudgementMatch.Checked = False
            RBJudgementNotMatch.Checked = False
            JudgementComment.Clear()
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Private Sub LoadColumn()
        Try
            ColumnActionLocation(gridJudgementItem, CommandColumnGridJudgementItem)
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Private Sub LoadData(PK_Result_ID As String)
        Try
            Using objDb As New NawaDatadevEntities
                'Get Data Screening Request
                Dim objResult = objDb.AML_SCREENING_RESULT.Where(Function(x) x.PK_AML_SCREENING_RESULT_ID = PK_Result_ID).FirstOrDefault
                If objResult IsNot Nothing AndAlso objResult.FK_AML_SCREENING_REQUEST_ID IsNot Nothing Then
                    Dim objRequest = objDb.AML_SCREENING_REQUEST.Where(Function(x) x.PK_AML_SCREENING_REQUEST_ID = objResult.FK_AML_SCREENING_REQUEST_ID).FirstOrDefault
                    If objRequest IsNot Nothing Then
                        display_Request_Name.Text = objRequest.NAME
                        If objRequest.DOB.HasValue Then
                            display_Request_DOB.Text = objRequest.DOB.ToString("dd-MMM-yyyy")
                        End If
                        display_Request_Nationality.Text = objRequest.NATIONALITY
                        display_Request_Identity_Number.Text = objRequest.IDENTITY_NUMBER

                        display_JudgementBy.Text = objResult.LastUpdateBy
                        If objResult.LastUpdateDate.HasValue Then
                            display_JudgementDate.Text = CDate(objResult.LastUpdateDate).ToString("dd-MMM-yyyy HH:mm:ss")
                        End If

                    End If
                End If

                'Get data Screening Result to be judged
                listJudgementItem = objDb.AML_SCREENING_RESULT_DETAIL.Where(Function(x) x.FK_AML_SCREENING_RESULT_ID = PK_Result_ID).ToList
                If listJudgementItem Is Nothing Then
                    listJudgementItem = New List(Of AML_SCREENING_RESULT_DETAIL)
                End If
            End Using

            BindJudgement(judgement_Item, listJudgementItem)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'daniel 20210419
    Protected Sub UpdatelistJudgementItem()
        Try
            For Each item In listwatchlistalreadymatch
                Dim judgementAffectedByWatchlist As List(Of AML_SCREENING_RESULT_DETAIL) = listJudgementItem.Where(Function(x) x.FK_AML_WATCHLIST_ID = item.FK_AML_WATCHLIST_ID).ToList
                For Each itemAffected In judgementAffectedByWatchlist
                    judgementdetailchange = listJudgementItem.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = itemAffected.PK_AML_SCREENING_RESULT_DETAIL_ID).FirstOrDefault
                    If judgementdetailchange IsNot Nothing Then
                        listAffecteddatafromlistmatch.Add(itemAffected)
                        If Not judgementdetailchange.JUDGEMENT_ISMATCH Or judgementdetailchange.JUDGEMENT_ISMATCH Is Nothing Then
                            itemAffected.JUDGEMENT_ISMATCH = True
                            indexofjudgementitemschange = listJudgementItem.IndexOf(judgementdetailchange)
                            listJudgementItem(indexofjudgementitemschange) = itemAffected
                            listJudgementItemchange.Add(itemAffected)
                        End If
                    End If
                Next
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'end daniel 20210419

    Private Sub BindJudgement(store As Store, list As List(Of AML_SCREENING_RESULT_DETAIL))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_CATEGORY", GetType(String)))
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_TYPE", GetType(String)))
            objtable.Columns.Add(New DataColumn("ISMATCH", GetType(String)))
            objtable.Columns.Add(New DataColumn("Similarity", GetType(Double)))
            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    If Not IsDBNull(item("FK_AML_WATCHLIST_CATEGORY_ID")) Then
                        item("AML_WATCHLIST_CATEGORY") = AMLJudgementBLL.GetWatchlistCategoryByID(item("FK_AML_WATCHLIST_CATEGORY_ID"))
                    Else
                        item("AML_WATCHLIST_CATEGORY") = ""
                    End If
                    If Not IsDBNull(item("FK_AML_WATCHLIST_TYPE_ID")) Then
                        item("AML_WATCHLIST_TYPE") = AMLJudgementBLL.GetWatchlistTypeByID(item("FK_AML_WATCHLIST_TYPE_ID"))
                    Else
                        item("AML_WATCHLIST_TYPE") = ""
                    End If
                    'daniel 20210419
                    'diremark karena udh diganti menjadi persentase dari DB
                    'If Not IsDBNull(item("MATCH_SCORE")) Then
                    '    item("Similarity") = item("MATCH_SCORE") * 100
                    'Else
                    '    item("Similarity") = 0
                    'End If
                    'If Not IsDBNull(item("JUDGEMENT_ISMATCH")) Then
                    '    If item("JUDGEMENT_ISMATCH") Then
                    '        item("ISMATCH") = "Match"
                    '    Else
                    '        item("ISMATCH") = "Not Match"
                    '    End If
                    'Else
                    '    item("ISMATCH") = ""
                    'End If
                    judgementdetailchange = listAffecteddatafromlistmatch.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = item("PK_AML_SCREENING_RESULT_DETAIL_ID")).FirstOrDefault
                    If judgementdetailchange IsNot Nothing Then
                        item("ISMATCH") = "Already Judged Match"
                    Else
                        If Not IsDBNull(item("JUDGEMENT_ISMATCH")) Then
                            If item("JUDGEMENT_ISMATCH") Then
                                item("ISMATCH") = "Match"
                            Else
                                item("ISMATCH") = "Not Match"
                            End If
                        Else
                            item("ISMATCH") = ""
                        End If
                    End If
                    'end daniel 20210419
                Next
            End If
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ALIAS()
        Try
            Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS)
            gridAliasInfo.GetStore().DataSource = objtable
            gridAliasInfo.GetStore().DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ADDRESS()
        Try
            Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS)

            objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
            objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
                    If objCountry IsNot Nothing Then
                        item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
                    Else
                        item("COUNTRY_NAME") = Nothing
                    End If

                    Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
                    If objAddressType IsNot Nothing Then
                        item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
                    Else
                        item("AML_ADDRESS_TYPE_NAME") = Nothing
                    End If
                Next
            End If

            gridAddressInfo.GetStore().DataSource = objtable
            gridAddressInfo.GetStore().DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub Bind_AML_WATCHLIST_IDENTITY()
        Try
            Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY)

            objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
                    If objIDENTITYType IsNot Nothing Then
                        item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
                    Else
                        item("AML_IDENTITY_TYPE_NAME") = Nothing
                    End If
                Next
            End If

            gridIdentityInfo.GetStore().DataSource = objtable
            gridIdentityInfo.GetStore().DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            'ErrorSignal.FromCurrentContext.Raise(ex)
            'Net.X.Msg.Alert("Error", ex.Message).Show()
            Throw ex
        End Try
    End Sub

    Protected Sub GridcommandJudgementItem(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Judge" Then
                ClearOBJDetail()
                WindowJudge.Hidden = False
                loadobjectJudgement(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
            ElseIf e.ExtraParams(1).Value = "Delete" Then
            ElseIf e.ExtraParams(1).Value = "Edit" Then
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub loadobjectJudgement(id As String)
        Try
            Using objdb As New NawaDatadevEntities
                judgementdetail = listJudgementItem.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = id).FirstOrDefault
                If judgementdetail IsNot Nothing Then
                    'indexofjudgementitems = listScreeningResult.IndexOf(judgementdetail)
                    'If judgementdetail.JUDGEMENT_ISMATCH IsNot Nothing Then
                    '    If judgementdetail.JUDGEMENT_ISMATCH Then
                    '        RBJudgementMatch.Checked = True
                    '    Else
                    '        RBJudgementNotMatch.Checked = True
                    '    End If
                    'End If
                    'If judgementdetail.JUDGEMENT_COMMENT IsNot Nothing Then
                    '    JudgementComment.Text = judgementdetail.JUDGEMENT_COMMENT
                    'End If

                    'Load Information to Search
                    txt_MATCH_SCORE_NAME_SEARCH.Value = judgementdetail.NAME
                    If IsNothing(judgementdetail.DOB) OrElse CDate(judgementdetail.DOB) = DateTime.MinValue Then
                        txt_MATCH_SCORE_DOB_SEARCH.Value = "-"
                    Else
                        txt_MATCH_SCORE_DOB_SEARCH.Value = Convert.ToDateTime(judgementdetail.DOB).ToString("dd-MMM-yyyy")
                    End If
                    txt_MATCH_SCORE_Nationality_SEARCH.Value = IIf(String.IsNullOrEmpty(judgementdetail.NATIONALITY), "-", judgementdetail.NATIONALITY)
                    txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH.Value = IIf(String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER), "-", judgementdetail.IDENTITY_NUMBER)

                    'Screening Result
                    txt_MATCH_SCORE_NAME_WATCHLIST.Value = judgementdetail.NAME_WATCHLIST
                    If IsNothing(judgementdetail.DOB) OrElse CDate(judgementdetail.DOB) = DateTime.MinValue Then
                        txt_MATCH_SCORE_DOB_WATCHLIST.Value = ""
                    Else
                        txt_MATCH_SCORE_DOB_WATCHLIST.Value = Convert.ToDateTime(judgementdetail.DOB_WATCHLIST).ToString("dd-MMM-yyyy")
                    End If
                    txt_MATCH_SCORE_Nationality_WATCHLIST.Value = judgementdetail.NATIONALITY_WATCHLIST
                    txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST.Value = judgementdetail.IDENTITY_NUMBER_WATCHLIST

                    'Screening Match Score
                    Dim objParamMatchScore = objdb.AML_SCREENING_MATCH_PARAMETER.FirstOrDefault
                    If Not objParamMatchScore Is Nothing Then
                        Dim dblMatchScoreName As Double = CDbl(judgementdetail.MATCH_SCORE_NAME) * CDbl(objParamMatchScore.NAME_WEIGHT / 100)
                        Dim dblMatchScoreDOB As Double = CDbl(judgementdetail.MATCH_SCORE_DOB) * CDbl(objParamMatchScore.DOB_WEIGHT / 100)
                        Dim dblMatchScoreNationality As Double = CDbl(judgementdetail.MATCH_SCORE_NATIONALITY) * CDbl(objParamMatchScore.NATIONALITY_WEIGHT / 100)
                        Dim dblMatchScoreIdentity As Double = CDbl(judgementdetail.MATCH_SCORE_IDENTITY_NUMBER) * CDbl(objParamMatchScore.IDENTITY_WEIGHT / 100)
                        Dim dblMatchScoreTotal = dblMatchScoreName + dblMatchScoreDOB + dblMatchScoreNationality + dblMatchScoreIdentity

                        Dim strMatchScoreName As String = objParamMatchScore.NAME_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_NAME).ToString("#,##0.00") & " % = " & dblMatchScoreName.ToString("#,##0.00") & " %"
                        Dim strMatchScoreDOB As String = objParamMatchScore.DOB_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_DOB).ToString("#,##0.00") & " % = " & dblMatchScoreDOB.ToString("#,##0.00") & " %"
                        Dim strMatchScoreNationality As String = objParamMatchScore.NATIONALITY_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_NATIONALITY).ToString("#,##0.00") & " % = " & dblMatchScoreNationality.ToString("#,##0.00") & " %"
                        Dim strMatchScoreIdentity As String = objParamMatchScore.IDENTITY_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_IDENTITY_NUMBER) & " % = " & dblMatchScoreIdentity.ToString("#,##0.00") & " %"

                        'Show the Formula so the user clear about the score
                        Dim strDividend As String = dblMatchScoreName.ToString("#,##0.00")
                        Dim strDivisor As String = objParamMatchScore.NAME_WEIGHT.ToString()
                        If Not IsNothing(judgementdetail.DOB) AndAlso CDate(judgementdetail.DOB) <> DateTime.MinValue Then
                            strDividend = strDividend & " + " & dblMatchScoreDOB.ToString("#,##0.00")
                            strDivisor = strDivisor & " + " & objParamMatchScore.DOB_WEIGHT.ToString()
                        End If
                        If Not String.IsNullOrEmpty(judgementdetail.NATIONALITY) Then
                            strDividend = strDividend & " + " & dblMatchScoreNationality.ToString("#,##0.00")
                            strDivisor = strDivisor & " + " & objParamMatchScore.NATIONALITY_WEIGHT.ToString()
                        End If
                        If Not String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER) Then
                            strDividend = strDividend & " + " & dblMatchScoreIdentity.ToString("#,##0.00")
                            strDivisor = strDivisor & " + " & objParamMatchScore.IDENTITY_WEIGHT.ToString()
                        End If
                        strDividend = "( " & strDividend & " % )"
                        strDivisor = "( " & strDivisor & " % )"

                        txt_MATCH_SCORE_NAME.Value = strMatchScoreName
                        txt_MATCH_SCORE_DOB.Value = IIf(IsNothing(judgementdetail.DOB) OrElse CDate(judgementdetail.DOB) = DateTime.MinValue, "-", strMatchScoreDOB)
                        txt_MATCH_SCORE_Nationality.Value = IIf(String.IsNullOrEmpty(judgementdetail.NATIONALITY), "-", strMatchScoreNationality)
                        txt_MATCH_SCORE_IDENTITY_NUMBER.Value = IIf(String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER), "-", strMatchScoreIdentity)
                        txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(judgementdetail.MATCH_SCORE).ToString("#,##0.00") & " %"
                    End If


                    'Show Detail Watch List
                    objAML_WATCHLIST_CLASS = AML_WATCHLIST_BLL.GetWatchlistClassByID(judgementdetail.FK_AML_WATCHLIST_ID)
                    If objAML_WATCHLIST_CLASS IsNot Nothing AndAlso objAML_WATCHLIST_CLASS.objAML_WATCHLIST IsNot Nothing Then
                        With objAML_WATCHLIST_CLASS.objAML_WATCHLIST
                            WatchListID.Text = .PK_AML_WATCHLIST_ID
                            If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                                Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                                If objWatchlistCategory IsNot Nothing Then
                                    WatchListCategory.Text = objWatchlistCategory.CATEGORY_NAME
                                End If
                            End If
                            If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                                Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                                If objWatchlistType IsNot Nothing Then
                                    WatchListType.Text = objWatchlistType.TYPE_NAME
                                End If
                            End If
                            If .NAME IsNot Nothing Then
                                FullName.Text = .NAME
                            End If
                            If .BIRTH_PLACE IsNot Nothing Then
                                PlaceofBirth.Text = .BIRTH_PLACE
                            End If
                            If .DATE_OF_BIRTH IsNot Nothing Then
                                DateofBirth.Text = .DATE_OF_BIRTH
                            End If
                            If .YOB IsNot Nothing Then
                                YearofBirth.Text = .YOB
                            End If
                            If .NATIONALITY IsNot Nothing Then
                                Nationality.Text = .NATIONALITY
                            End If
                            If .CUSTOM_REMARK_1 IsNot Nothing Then
                                Remark1.Text = .CUSTOM_REMARK_1
                            End If
                            If .CUSTOM_REMARK_2 IsNot Nothing Then
                                Remark2.Text = .CUSTOM_REMARK_2
                            End If
                            If .CUSTOM_REMARK_3 IsNot Nothing Then
                                Remark3.Text = .CUSTOM_REMARK_3
                            End If
                            If .CUSTOM_REMARK_4 IsNot Nothing Then
                                Remark4.Text = .CUSTOM_REMARK_4
                            End If
                            If .CUSTOM_REMARK_5 IsNot Nothing Then
                                Remark5.Text = .CUSTOM_REMARK_5
                            End If
                        End With
                        Bind_AML_WATCHLIST_ALIAS()
                        Bind_AML_WATCHLIST_ADDRESS()
                        Bind_AML_WATCHLIST_IDENTITY()
                    End If

                    'Display Judgement Result
                    If judgementdetail.JUDGEMENT_ISMATCH = True Then
                        RBJudgementMatch.Checked = True
                        RBJudgementNotMatch.Checked = False
                    Else
                        RBJudgementMatch.Checked = False
                        RBJudgementNotMatch.Checked = True
                    End If

                    JudgementComment.Value = judgementdetail.JUDGEMENT_COMMENT

                    RBJudgementMatch.ReadOnly = True
                    RBJudgementNotMatch.ReadOnly = True
                    JudgementComment.ReadOnly = True
                End If
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            If AML_WATCHLIST_BLL.IsExistsInApproval(ObjModule.ModuleName, dataID) Then
                LblConfirmation.Text = "Sorry, this Data is already Updated By Other User and already in Pending Approval. Please Check Data in Pending Approval."

                Panelconfirmation.Hidden = False
                PanelInfo.Hidden = True
            Else
                judgementClass = New AMLJudgementClass
                With judgementClass
                    .AMLJudgementHeader = judgementheader
                    .AMLListResult = listJudgementItemchange
                End With
                checkerjudgeitems = New AML_SCREENING_RESULT_DETAIL
                checkerjudgeitems = listJudgementItem.Where(Function(x) x.JUDGEMENT_ISMATCH Is Nothing).FirstOrDefault
                ' 2021/06/07 perubahan karena adanya auto judgement dimana apabila ada check perubahan keseluruhan maka akan terhambat dan tidak bisa save meskipun semua data telah ter judge
                'If listJudgementItemchange.Count = 0 Then
                '    Throw New ApplicationException("No Data Has Changed, Please Change Some Data Before Saving or Click the Cancel Button to Cancel the Process")
                'ElseIf checkerjudgeitems IsNot Nothing Then
                '    Throw New ApplicationException("There are Some Data which have not been Judged, Please Judge All Data Before Saving")
                'End If
                If checkerjudgeitems IsNot Nothing Then
                    Throw New ApplicationException("There are Some Data which have not been Judged, Please Judge All Data Before Saving")
                End If
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not ObjModule.IsUseApproval Then
                    'tanpa approval
                    Me.SaveJudgement(0)
                    PanelInfo.Hide()
                    Panelconfirmation.Show()
                    LblConfirmation.Text = "Data Saved into Database"
                Else
                    'with approval
                    Me.SaveJudgement(1)
                    PanelInfo.Hide()
                    Panelconfirmation.Show()
                    LblConfirmation.Text = "Data Saved into Pending Approval"
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadJudgementItemChange(items As AML_SCREENING_RESULT_DETAIL)
        Try
            judgementdetailchange = listJudgementItemchange.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = items.PK_AML_SCREENING_RESULT_DETAIL_ID).FirstOrDefault
            If judgementdetailchange IsNot Nothing Then
                indexofjudgementitemschange = listJudgementItemchange.IndexOf(judgementdetailchange)
                listJudgementItemchange(indexofjudgementitemschange) = items
            Else
                listJudgementItemchange.Add(items)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub rgjudgement_click(sender As Object, e As DirectEventArgs)
        Try
            Dim objradio As Ext.Net.Radio = CType(sender, Ext.Net.Radio)
            If JudgementComment.Text Is Nothing Or JudgementComment.Text = "" Or JudgementComment.Text = "This Data is judged as Match with Sanction List" Or JudgementComment.Text = "This Data is judged as Not Match with Sanction List" Then
                If objradio.InputValue = "1" And objradio.Checked Then
                    JudgementComment.Text = "This Data is judged as Match with Sanction List"
                ElseIf objradio.InputValue = "0" And objradio.Checked Then
                    JudgementComment.Text = "This Data is judged as Not Match with Sanction List"
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnJudgementSave_Click(sender As Object, e As DirectEventArgs)
        Try
            If RBJudgementMatch.Checked Or RBJudgementNotMatch.Checked Then
                If judgementdetail IsNot Nothing Then
                    'If judgementdetail.JUDGEMENT_ISMATCH <> RBJudgementMatch.Checked Or judgementdetail.JUDGEMENT_COMMENT <> JudgementComment.Text Or judgementdetail.JUDGEMENT_ISMATCH Is Nothing Then
                    If judgementdetail.JUDGEMENT_ISMATCH <> RBJudgementMatch.Checked Or judgementdetail.JUDGEMENT_COMMENT <> JudgementComment.Text Then
                        If RBJudgementMatch.Checked Then
                            judgementdetail.JUDGEMENT_ISMATCH = True
                        Else
                            judgementdetail.JUDGEMENT_ISMATCH = False
                        End If
                        If JudgementComment.Text IsNot Nothing Then
                            judgementdetail.JUDGEMENT_COMMENT = JudgementComment.Text
                        End If
                        If Common.SessionCurrentUser.UserName IsNot Nothing Then
                            judgementdetail.JUDGEMENT_BY = Common.SessionCurrentUser.UserID
                        End If
                        judgementdetail.JUDGEMENT_DATE = Now()
                        If judgementdetail.PK_AML_SCREENING_RESULT_DETAIL_ID <> Nothing And listJudgementItem IsNot Nothing Then
                            Dim listitemchangewithpkwatchlist As List(Of AML_SCREENING_RESULT_DETAIL) = listJudgementItem.Where(Function(x) x.FK_AML_WATCHLIST_ID = judgementdetail.FK_AML_WATCHLIST_ID).ToList
                            For Each itemchange In listitemchangewithpkwatchlist
                                itemchange.JUDGEMENT_ISMATCH = judgementdetail.JUDGEMENT_ISMATCH
                                itemchange.JUDGEMENT_COMMENT = judgementdetail.JUDGEMENT_COMMENT
                                itemchange.JUDGEMENT_BY = judgementdetail.JUDGEMENT_BY
                                itemchange.JUDGEMENT_DATE = judgementdetail.JUDGEMENT_DATE
                                LoadJudgementItemChange(itemchange)
                                judgementdetailchange = listJudgementItem.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = itemchange.PK_AML_SCREENING_RESULT_DETAIL_ID).FirstOrDefault
                                If judgementdetailchange IsNot Nothing Then
                                    indexofjudgementitems = listJudgementItem.IndexOf(judgementdetailchange)
                                    listJudgementItem(indexofjudgementitems) = itemchange
                                End If
                            Next
                            BindJudgement(judgement_Item, listJudgementItem)
                        Else
                            Throw New ApplicationException("Something Wrong While Saving Data")
                        End If
                    Else
                        Throw New ApplicationException("No Data Has Changed, Please Change Some Data Before Saving or Click the Cancel Button to Cancel the Process")
                    End If
                End If
            Else
                Throw New ApplicationException("Must Conduct Judgment Before Saving Data")
            End If
            WindowJudge.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnJudgementCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowJudge.Hidden = True
            ClearOBJDetail()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#Region "Save Judgement"
    Protected Sub SaveJudgement(isUseApproval As Boolean)
        Dim objDataOld As AML_SCREENING_RESULT
        Dim listDataOld As List(Of AML_SCREENING_RESULT_DETAIL)
        Using objDBOld As New NawaDatadevEntities
            objDataOld = objDBOld.AML_SCREENING_RESULT.Where(Function(x) x.PK_AML_SCREENING_RESULT_ID = dataID).FirstOrDefault
            listDataOld = objDBOld.AML_SCREENING_RESULT_DETAIL.Where(Function(x) x.FK_AML_SCREENING_RESULT_ID = dataID).ToList
        End Using

        Using objDB As New NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    If listJudgementItem IsNot Nothing AndAlso listJudgementItem.Count > 0 Then

                        Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                        Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                        Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                        Dim modulename As String = ObjModule.ModuleLabel
                        Dim ObjAuditTrailHeader As NawaDevDAL.AuditTrailHeader

                        'Update Screening Result Status
                        Dim objResult = objDB.AML_SCREENING_RESULT.Where(Function(x) x.PK_AML_SCREENING_RESULT_ID = dataID).FirstOrDefault
                        If objResult IsNot Nothing Then
                            With objResult
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = Now

                                If isUseApproval Then
                                    .STATUS_JUDGEMENT_CODE = "1"
                                Else
                                    .STATUS_JUDGEMENT_CODE = "2"
                                End If
                            End With
                            objDB.Entry(objResult).State = EntityState.Modified
                            objDB.SaveChanges()

                            'Create Audit Trail
                            ObjAuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, objResult, objDataOld)
                        End If

                        For Each item In listJudgementItem
                            Dim objCek = objDB.AML_SCREENING_RESULT_DETAIL.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = item.PK_AML_SCREENING_RESULT_DETAIL_ID).FirstOrDefault
                            Dim objOld = listDataOld.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = item.PK_AML_SCREENING_RESULT_DETAIL_ID).FirstOrDefault

                            If objCek IsNot Nothing Then
                                With objCek
                                    .JUDGEMENT_ISMATCH = item.JUDGEMENT_ISMATCH
                                    .JUDGEMENT_COMMENT = item.JUDGEMENT_COMMENT
                                    .JUDGEMENT_BY = Common.SessionCurrentUser.UserID
                                    .JUDGEMENT_DATE = Now
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = Now
                                End With

                                objDB.Entry(objCek).State = EntityState.Modified
                                objDB.SaveChanges()

                                'Create Audit Trail
                                ObjAuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, objCek, objOld)
                            End If
                        Next

                        objDB.SaveChanges()
                        objTrans.Commit()
                    End If

                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

#End Region

#Region "Accept & Reject"
    Protected Sub btnAccept_Click(sender As Object, e As DirectEventArgs)
        Try

            Me.ApproveJudgement(True)
            PanelInfo.Hide()
            Panelconfirmation.Show()
            LblConfirmation.Text = "Data Approved."

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As DirectEventArgs)
        Try
            Me.ApproveJudgement(False)
            PanelInfo.Hide()
            Panelconfirmation.Show()
            LblConfirmation.Text = "Data Rejected."
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ApproveJudgement(isApproved As Boolean)
        Try
            Using objdb As New NawaDatadevEntities
                Dim objResult = objdb.AML_SCREENING_RESULT.Where(Function(x) x.PK_AML_SCREENING_RESULT_ID = dataID).FirstOrDefault

                If objResult IsNot Nothing Then
                    If objResult.STATUS_JUDGEMENT_CODE IsNot Nothing AndAlso (objResult.STATUS_JUDGEMENT_CODE = "2" Or objResult.STATUS_JUDGEMENT_CODE = "3") Then
                        Throw New Exception("Sorry, this Data is already Approved/Rejected.")
                    Else
                        With objResult
                            .ApprovedBy = Common.SessionCurrentUser.UserID
                            .ApprovedDate = Now

                            If isApproved Then
                                .STATUS_JUDGEMENT_CODE = "2"
                            Else
                                .STATUS_JUDGEMENT_CODE = "3"
                            End If
                        End With

                        objdb.Entry(objResult).State = EntityState.Modified
                        objdb.SaveChanges()
                    End If
                End If
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class
