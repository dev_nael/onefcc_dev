﻿Imports Ext.Net
Imports OfficeOpenXml

Partial Class AMLJudgementProspectApproval
    Inherits Parent
    Public objFormModuleView As NawaBLL.FormModuleView
    Public Property strWhereClause() As String
        Get
            Return Session("AMLJudgementProspectApproval.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("AMLJudgementProspectApproval.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("AMLJudgementProspectApproval.strSort")
        End Get
        Set(ByVal value As String)
            Session("AMLJudgementProspectApproval.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("AMLJudgementProspectApproval.indexStart")
        End Get
        Set(ByVal value As String)
            Session("AMLJudgementProspectApproval.indexStart") = value
        End Set
    End Property
    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("AMLJudgementProspectApproval.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AMLJudgementProspectApproval.ObjModule") = value
        End Set
    End Property
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    Private Sub AMLJudgementProspectApproval_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer

            Try
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)
                BtnAdd.Hidden = True

                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Approval) Then
                    Dim strIDCode As String = 1
                    BtnAdd.Hidden = True

                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If

                objFormModuleView.ModuleID = ObjModule.PK_Module_ID
                objFormModuleView.ModuleName = ObjModule.ModuleName
                objFormModuleView.AddField("PK_AML_SCREENING_RESULT_ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.BIGINTValue, , , , , )
                objFormModuleView.AddField("NAME", "Name", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue, , , ,, )
                objFormModuleView.AddField("DOB", "Date Of Birth", 4, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )
                objFormModuleView.AddField("NATIONALITY", "Nationality", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("IDENTITY_NUMBER", "Identity Number", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                'objFormModuleView.AddField("WatchlistCategory", "Watchlist", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("MATCH_SCORE", "Highest Match Score", 6, False, True, NawaBLL.Common.MFieldType.NUMERICDECIMALValue,,,,, 2)
                objFormModuleView.AddField("JudgementStatus", "Status", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.SettingFormView()

                GridpanelView.Title = ObjModule.ModuleLabel & " - Approval"

                Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")

                'Hide dulu semua command (jika ada) bawaaan framework
                For Each item As Ext.Net.GridCommand In objcommandcol.Commands
                    item.Hidden = True
                Next

                'Buat command button baru untuk Approval Detail
                Dim objApprovalDetail As New GridCommand
                objApprovalDetail.CommandName = "ApprovalDetail"
                objApprovalDetail.Icon = Icon.ApplicationViewDetail
                objApprovalDetail.Text = "Detail"
                objcommandcol.Commands.Add(objApprovalDetail)

                Dim extparam As New Ext.Net.Parameter
                extparam.Name = "unikkey"
                extparam.Value = "record.data.PK_AML_SCREENING_RESULT_ID"
                extparam.Mode = ParameterMode.Raw

                Dim extParamCommandName As New Ext.Net.Parameter
                extparam.Name = "command"
                extparam.Value = "command"
                extparam.Mode = ParameterMode.Raw
                objcommandcol.DirectEvents.Command.ExtraParams.Add(extparam)

                AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf RedirectToApprovalDetail

                'objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"

            Catch ex As Exception
            End Try

            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            Using objdb As New NawaDevDAL.NawaDatadevEntities
                'Begin Penambahan Advanced Filter
                If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                    Toolbar2.Hidden = True
                Else
                    Toolbar2.Hidden = False
                End If
                LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")

                Dim intStart As Integer = e.Start
                Dim intLimit As Int16 = e.Limit
                Dim inttotalRecord As Integer
                Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

                Dim strsort As String = ""
                For Each item As DataSorter In e.Sort
                    strsort += item.Property & " " & item.Direction.ToString
                Next
                Me.indexStart = intStart
                Me.strWhereClause = strfilter
                If strsort = "" Then
                    strsort = "PK_AML_SCREENING_RESULT_ID Desc"
                End If


                'Begin Update Penambahan Advance Filter
                If strWhereClause.Length > 0 Then
                    If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                        strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                        intStart = 0
                    End If
                Else
                    If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                        strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                        intStart = 0
                    End If
                End If

                'Define filter untuk yang statusnya 1 (In Approval)
                If String.IsNullOrEmpty(strWhereClause) Then
                    strWhereClause = "ISNULL(STATUS_JUDGEMENT_CODE,'')='1'"
                Else
                    strWhereClause += " AND ISNULL(STATUS_JUDGEMENT_CODE,'')='1'"
                End If

                Me.strOrder = strsort

                Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
                'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(" ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID inner join module on module.modulename=moduleapproval.modulename", "PK_ModuleApproval_ID, ModuleLabel, ModuleKey, moduleaction.ModuleActionName, ModuleApproval.CreatedBy, ModuleApproval.CreatedDate", strWhereClause, strsort, intStart, intLimit, inttotalRecord)
                ''-- start paging ------------------------------------------------------------
                Dim limit As Integer = e.Limit
                If (e.Start + e.Limit) > inttotalRecord Then
                    limit = inttotalRecord - e.Start
                End If
                'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
                ''-- end paging ------------------------------------------------------------
                e.Total = inttotalRecord
                GridpanelView.GetStore.DataSource = DataPaging
                GridpanelView.GetStore.DataBind()
            End Using
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub RedirectToApprovalDetail(sender As Object, e As DirectEventArgs)
        Try
            If e.ExtraParams("command") = "ApprovalDetail" Then
                Dim strEncModuleID As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                'Get Parameter PK Data ID
                Dim RowID As String = e.ExtraParams("unikkey")
                Dim strEncDataID As String = NawaBLL.Common.EncryptQueryString(RowID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                If InStr(ObjModule.UrlApprovalDetail, "?") > 0 Then
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApprovalDetail & "&ModuleID=" & strEncModuleID & "&ID=" & strEncDataID)
                Else
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApprovalDetail & "?ModuleID=" & strEncModuleID & "&ID=" & strEncDataID)
                End If
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

End Class
