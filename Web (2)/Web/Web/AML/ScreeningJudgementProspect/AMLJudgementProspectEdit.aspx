﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="AMLJudgementProspectEdit.aspx.vb" Inherits="AMLJudgementProspectEdit" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script type="text/javascript">
        var prepareCommandCollection = function (grid, toolbar, rowIndex, record) {
            var EditButton = toolbar.items.get(0);

            var wf = record.data.ISMATCH
            if (EditButton != undefined) {
                if (wf == "Already Judged Match") {
                    EditButton.setDisabled(true)
                }
            };

        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel ID="PanelInfo" runat="server" Title="Screening Judgement - Customer Prospect" BodyStyle="padding:10px"  ButtonAlign="Center" Scrollable="Both">
        <Items>
            <ext:Panel runat="server" Layout="AnchorLayout" Title="Screening Request" Border="true" BodyPadding="10">
                <Items>
                    <ext:DisplayField ID="display_Request_Name" runat="server" FieldLabel="Name" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:DisplayField ID="display_Request_DOB" runat="server" FieldLabel="Date Of Birth" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:DisplayField ID="display_Request_Nationality" runat="server" FieldLabel="Nationality" AnchorHorizontal="100%"></ext:DisplayField>
                    <ext:DisplayField ID="display_Request_Identity_Number" runat="server" FieldLabel="Identity Number" AnchorHorizontal="100%"></ext:DisplayField>
                </Items>
            </ext:Panel>
            <ext:GridPanel ID="gridJudgementItem" runat="server" MarginSpec="10 0 0 0" Title="Screening Result" Border="true">
                <Store>
                    <ext:Store ID="judgement_Item" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" IDProperty="PK_AML_SCREENING_RESULT_DETAIL_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_SCREENING_RESULT_DETAIL_ID" Type="Int"></ext:ModelField>
                                    <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NAME_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="AML_WATCHLIST_CATEGORY" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="AML_WATCHLIST_TYPE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE" Type="Float"></ext:ModelField>
                                    <ext:ModelField Name="ISMATCH" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="#" CellWrap="true" Width="30"></ext:RowNumbererColumn>
                        <ext:Column runat="server" Text="Name" DataIndex="NAME" CellWrap="true" MinWidth="150" flex="1" />
                        <ext:Column runat="server" Text="Name Watch List" DataIndex="NAME_WATCHLIST" CellWrap="true" flex="1" />
                        <ext:Column runat="server" Text="Source Watch List" DataIndex="AML_WATCHLIST_CATEGORY" flex="1" />
                        <ext:Column runat="server" Text="ID Watch List" DataIndex="FK_AML_WATCHLIST_ID" flex="1" Align="Right" />
                        <ext:Column runat="server" Text="Is Critical?" DataIndex="AML_WATCHLIST_TYPE" flex="1" />
                        <ext:NumberColumn runat="server" Text="Similarity(%)" DataIndex="MATCH_SCORE" flex="1" Align="Right" />
                        <ext:Column runat="server" Text="Judgement" DataIndex="ISMATCH" CellWrap="true" flex="1" />

                        <ext:CommandColumn ID="CommandColumnGridJudgementItem" runat="server" Text="Action" CellWrap="true" Width="70">
                            <Commands>
                                <ext:GridCommand Text="Judge" CommandName="Judge" Icon="ApplicationEdit" MinWidth="50">
                                    <ToolTip Text="Judge"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridcommandJudgementItem">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btnSaveReport" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="BtnSave_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancelReport" runat="server" Icon="Cancel" Text="Cancel Save">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:Window ID="WindowJudge" Title="Judgement Detail" runat="server" Modal="true" Maximizable="true" Hidden="true" BodyPadding="10" Scrollable="Both" Layout="AnchorLayout" ButtonAlign="Center" Width="1000" Height="500">
        <Items>
            <ext:FormPanel ID="ScoreDetail" runat="server" Collapsible="true" BodyPadding="10" Title="Screening Result Score Detail" AnchorHorizontal ="100%" Layout="AnchorLayout" Border="true">
                <Items>
                    <%--<ext:DisplayField ID="Name_Score" runat="server" FieldLabel="Name Score" AnchorHorizontal ="100%">
                    </ext:DisplayField>
                    <ext:DisplayField ID="DoB_Score" runat="server" FieldLabel="Date of Birth Score" AnchorHorizontal ="100%">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Nationality_Score" runat="server" FieldLabel="Nationality Score" AnchorHorizontal ="100%">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Identity_Score" runat="server" FieldLabel="Identity Number Score" AnchorHorizontal ="100%">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Total_Score" runat="server" FieldLabel="Total Score" AnchorHorizontal ="100%">
                    </ext:DisplayField>--%>

                    <ext:FieldSet runat="server" ID="FieldSet5" Collapsible="false" Title="" Border="false" Layout="ColumnLayout" AnchorHorizontal="100%" Padding="0">
                        <Items>
                            <ext:FieldSet runat="server" ID="FieldSet3" Collapsible="false" ColumnWidth="0.38" Title="Search Data">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Name" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Nationality" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Identity Number" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>
                            <ext:FieldSet runat="server" ID="FieldSet2" Collapsible="false" ColumnWidth="0.35" Title="Search Result" MarginSpec="0 5">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>
                            <ext:FieldSet runat="server" ID="FieldSet1" Collapsible="false" ColumnWidth="0.25" Title="Search Score">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%" ></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>

                            <ext:FieldSet runat="server" ID="FieldSet4" Collapsible="false" ColumnWidth="0.9" Title="" Border="false" Layout="AnchorLayout">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_TOTAL" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Total Score" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>

                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:FormPanel>
            
            <ext:FormPanel ID="GeneralInformation" runat="server" Collapsible="true" BodyPadding="10" Title="Watchlist Information" AnchorHorizontal ="100%" Layout="AnchorLayout" Border="true">
                <Items>
                     <ext:Panel runat="server" Layout="ColumnLayout" AnchorHorizontal="100%">
                                                <Content>
                                                    <ext:Button ID="btn_OpenWindowWatchlist" runat="server" Icon="ApplicationViewDetail" Text="Data Watchlist Worldcheck" MarginSpec="0 10 10 0" Hidden="true">
                                                        <DirectEvents>
                                                            <Click OnEvent="btn_OpenWindowWatchlist_Click">
                                                                <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                                                            </Click>
                                                        </DirectEvents>
                                                    </ext:Button>
                                                </Content>
                                            </ext:Panel>
                    <ext:Panel runat="server" Layout="ColumnLayout" AnchorHorizontal="100%">
                        <Items>
                            <ext:Panel runat="server" ColumnWidth="0.5" Layout="AnchorLayout">
                                <Items>
                                    <ext:DisplayField ID="WatchListID" runat="server" FieldLabel="Watch List ID" AnchorHorizontal ="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="WatchListCategory" runat="server" FieldLabel="Watch List Category" AnchorHorizontal ="100%"></ext:DisplayField>
                                 <%--    <ext:Panel runat="server" Layout="ColumnLayout" AnchorHorizontal="80%" ID="Panel1" Border="false">
                                        <Items>
                                            <ext:Panel runat="server" ColumnWidth="0.6" Border="false" Layout="AnchorLayout">
                                                <Content>
                                                    <ext:DisplayField ID="WatchListCategory" runat="server" FieldLabel="Watch List Category">
                                                    </ext:DisplayField>
                                                </Content>
                                            </ext:Panel>
                                            <ext:Panel runat="server" ColumnWidth="0.4" Border="false" Layout="AnchorLayout">
                                                <Content>
                                                    <ext:Button ID="btn_OpenWindowWatchlist" runat="server" Icon="ApplicationViewDetail" Text="Data Watchlist Worldcheck" MarginSpec="0 10 10 0" Hidden="true">
                                                        <DirectEvents>
                                                            <Click OnEvent="btn_OpenWindowWatchlist_Click">
                                                                <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                                                            </Click>
                                                        </DirectEvents>
                                                    </ext:Button>
                                                </Content>
                                            </ext:Panel>
                                        </Items>
                                    </ext:Panel>--%>
                                    <ext:DisplayField ID="WatchListType" runat="server" FieldLabel="Watch List Type" AnchorHorizontal ="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="FullName" runat="server" FieldLabel="Full Name" AnchorHorizontal ="100%"></ext:DisplayField>
                                </Items>
                            </ext:Panel>
                            <ext:Panel runat="server" ColumnWidth="0.5" Layout="AnchorLayout">
                                <Items>
                                    <ext:DisplayField ID="PlaceofBirth" runat="server" FieldLabel="Place of Birth" AnchorHorizontal ="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="DateofBirth" runat="server" FieldLabel="Date of Birth" AnchorHorizontal ="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="YearofBirth" runat="server" FieldLabel="Year of Birth" AnchorHorizontal ="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="Nationality" runat="server" FieldLabel="Nationality" AnchorHorizontal ="100%"></ext:DisplayField>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>

                    <%-- Watch List Alias --%>
                    <ext:GridPanel ID="gridAliasInfo" runat="server" Title="Alias(es)" AutoScroll="true" Border="true" PaddingSpec="10 0" AnchorHorizontal ="100%">
                        <Store>
                            <ext:Store ID="Store_gridAliasInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ALIAS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ALIAS_NAME" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column8" runat="server" DataIndex="ALIAS_NAME" Text="Alias Name" MinWidth="300" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Alias --%>

                    <%-- Watch List Address --%>
                    <ext:GridPanel ID="gridAddressInfo" runat="server" Title="Address(es)" AutoScroll="true" Border="true" PaddingSpec="10 0" AnchorHorizontal ="100%">
                        <Store>
                            <ext:Store ID="Store_gridAddressInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ADDRESS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_AML_COUNTRY_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="POSTAL_CODE" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column9" runat="server" DataIndex="AML_ADDRESS_TYPE_NAME" Text="Type" MinWidth="50"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="ADDRESS" Text="Address" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="FK_AML_COUNTRY_CODE" Text="Country Code" MinWidth="100" Hidden="true"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="COUNTRY_NAME" Text="Country Name" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="POSTAL_CODE" Text="Postal Code" MinWidth="100"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Address --%>

                    <%-- Watch List Identity --%>
                    <ext:GridPanel ID="gridIdentityInfo" runat="server" Title="Identity(es)" AutoScroll="true" Border="true" PaddingSpec="10 0" AnchorHorizontal ="100%">
                        <Store>
                            <ext:Store ID="Store_gridIdentityInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_IDENTITY_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_IDENTITY_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDENTITYNUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column14" runat="server" DataIndex="AML_IDENTITY_TYPE_NAME" Text="Type" MinWidth="200" Flex="1"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="IDENTITYNUMBER" Text="Identity" MinWidth="300"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Identity --%>
                    <ext:DisplayField ID="Remark1" runat="server" FieldLabel="Remark 1" AnchorHorizontal ="100%">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark2" runat="server" FieldLabel="Remark 2" AnchorHorizontal ="100%">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark3" runat="server" FieldLabel="Remark 3" AnchorHorizontal ="100%">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark4" runat="server" FieldLabel="Remark 4" AnchorHorizontal ="100%">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark5" runat="server" FieldLabel="Remark 5" AnchorHorizontal ="100%">
                    </ext:DisplayField>
                </Items>
            </ext:FormPanel>
            <ext:Panel ID="PanelJudge" runat="server" Layout="AnchorLayout" Title="Judgement" BodyPadding="10" Border="true">
                <Items>
                    <ext:RadioGroup runat="server" ID="RGJudgement" AnchorHorizontal="50%" FieldLabel="Your Judgement" LabelWidth="150">
                        <Items>
                            <ext:Radio runat="server" ID="RBJudgementMatch" BoxLabel="Match" InputValue="1">
                                <DirectEvents>
                                    <Change OnEvent="rgjudgement_click" />
                                </DirectEvents>
                            </ext:Radio>

                            <ext:Radio runat="server" ID="RBJudgementNotMatch" BoxLabel="Not Match" InputValue="0">
                                <DirectEvents>
                                    <Change OnEvent="rgjudgement_click" />
                                </DirectEvents>
                            </ext:Radio>
                        </Items>
                    </ext:RadioGroup>
                    <ext:TextArea runat="server" ID="JudgementComment" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="1000" LabelWidth="150" EnforceMaxLength="true"/>
                </Items>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="BtnJudgementSave" runat="server" Icon="Disk" Text="Save Judgement">
                <DirectEvents>
                    <Click OnEvent="BtnJudgementSave_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnJudgementCancel" runat="server" Icon="Cancel" Text="Cancel Save Judgement">
                <DirectEvents>
                    <Click OnEvent="BtnJudgementCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.9, height: size.height * 0.9});" />
            <Resize Handler="#{WindowJudge}.center()" />
        </Listeners>
    </ext:Window>
     <ext:Window runat="server" ID="WindowDisplayWatchlist" Title="Watchlist Information" Layout="AnchorLayout" ButtonAlign="Center" Hidden="true" Closable="false" BodyPadding="10" Maximized="true" Maximizable="true">
        <Items>
            <ext:Panel ID="pnlContent" runat="server" BodyPadding="0"  Height ="450">
                <Loader Mode="Frame" NoCache="true" runat="server" AutoLoad="false" >
                    <LoadMask ShowMask="true"></LoadMask>
                    <%--<Params>
                        <ext:Parameter Name="fileName" Mode="Raw" Value="0">
                        </ext:Parameter>
                    </Params>--%>
                </Loader>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Display_Watchlist_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_Display_Watchlist_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>

        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>

