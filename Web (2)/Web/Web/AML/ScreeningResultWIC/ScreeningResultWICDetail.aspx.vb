﻿Imports System.Data
Imports System.Data.SqlClient
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDAL
Imports NawaDevBLL
Imports NawaDevDAL
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevBLL.GlobalReportFunctionBLL
Imports System.Text
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.IO

Public Module Extenders
    <System.Runtime.CompilerServices.Extension>
    Public Function ToDataTable(Of T)(collection As IEnumerable(Of T), tableName As String) As DataTable
        Dim tbl As DataTable = ToDataTable(collection)
        tbl.TableName = tableName
        Return tbl
    End Function

    <System.Runtime.CompilerServices.Extension>
    Public Function ToDataTable(Of T)(collection As IEnumerable(Of T)) As DataTable
        Dim dt As New DataTable()
        Dim tt As Type = GetType(T)
        Dim pia As PropertyInfo() = tt.GetProperties()
        'Create the columns in the DataTable
        For Each pi As PropertyInfo In pia
            Dim a =
            If(Nullable.GetUnderlyingType(pi.PropertyType), pi.PropertyType)
            dt.Columns.Add(pi.Name, If(Nullable.GetUnderlyingType(pi.PropertyType), pi.PropertyType))
            'If pi.PropertyType Is GetType(DateTime) Then

            '    ' pi.Columns.Add("columnName", TypeOf (datetime2)
            'Else

            'End If
        Next
        'Populate the table
        For Each item As T In collection
            Dim dr As DataRow = dt.NewRow()
            dr.BeginEdit()
            For Each pi As PropertyInfo In pia
                ' cek value apakah null?
                ' If pi.GetValue(item, Nothing) = Nothing Then
                If pi.GetValue(item, Nothing) Is Nothing Then
                    dr(pi.Name) = DBNull.Value
                Else
                    dr(pi.Name) = pi.GetValue(item, Nothing)
                End If

            Next
            dr.EndEdit()
            dt.Rows.Add(dr)
        Next
        Return dt
    End Function


End Module

Partial Class AML_ScreeningResult_ScreeningResultWICDetail
    Inherits Parent

#Region "Session"
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("AML_ScreeningResult_ScreeningResultWICDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AML_ScreeningResult_ScreeningResultWICDetail.ObjModule") = value
        End Set
    End Property

    Public Property listjudgementitem As List(Of NawaDevDAL.AML_SCREENING_RESULT_WIC_DETAIL)
        Get
            Return Session("AML_ScreeningResult_ScreeningResultWICDetail.listjudgementitem")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_WIC_DETAIL))
            Session("AML_ScreeningResult_ScreeningResultWICDetail.listjudgementitem") = value
        End Set
    End Property

    Public Property listjudgementitemchange As List(Of NawaDevDAL.AML_SCREENING_RESULT_WIC_DETAIL)
        Get
            Return Session("AML_ScreeningResult_ScreeningResultWICDetail.listjudgementitemchange")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_WIC_DETAIL))
            Session("AML_ScreeningResult_ScreeningResultWICDetail.listjudgementitemchange") = value
        End Set
    End Property

    Public Property judgementheader As NawaDevDAL.AML_SCREENING_RESULT_WIC
        Get
            Return Session("AML_ScreeningResult_ScreeningResultWICDetail.judgementheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_WIC)
            Session("AML_ScreeningResult_ScreeningResultWICDetail.judgementheader") = value
        End Set
    End Property
    Public Property judgementrequestforheader As NawaDevDAL.AML_SCREENING_REQUEST
        Get
            Return Session("AML_ScreeningResult_ScreeningResultWICDetail.judgementrequestforheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_REQUEST)
            Session("AML_ScreeningResult_ScreeningResultWICDetail.judgementrequestforheader") = value
        End Set
    End Property
    Public Property judgementdetail As NawaDevDAL.AML_SCREENING_RESULT_WIC_DETAIL
        Get
            Return Session("AML_ScreeningResult_ScreeningResultWICDetail.judgementdetail")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_WIC_DETAIL)
            Session("AML_ScreeningResult_ScreeningResultWICDetail.judgementdetail") = value
        End Set
    End Property
    Public Property judgementdetailchange As NawaDevDAL.AML_SCREENING_RESULT_WIC_DETAIL
        Get
            Return Session("AML_ScreeningResult_ScreeningResultWICDetail.judgementdetailchange")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_WIC_DETAIL)
            Session("AML_ScreeningResult_ScreeningResultWICDetail.judgementdetailchange") = value
        End Set
    End Property
    Public Property checkerjudgeitems As NawaDevDAL.AML_SCREENING_RESULT_WIC_DETAIL
        Get
            Return Session("AML_ScreeningResult_ScreeningResultWICDetail.checkerjudgeitems")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_WIC_DETAIL)
            Session("AML_ScreeningResult_ScreeningResultWICDetail.checkerjudgeitems") = value
        End Set
    End Property
    Public Property indexofjudgementitems As Integer
        Get
            Return Session("AML_ScreeningResult_ScreeningResultWICDetail.indexofjudgementitems")
        End Get
        Set(ByVal value As Integer)
            Session("AML_ScreeningResult_ScreeningResultWICDetail.indexofjudgementitems") = value
        End Set
    End Property

    Public Property indexofjudgementitemschange As Integer
        Get
            Return Session("AML_ScreeningResult_ScreeningResultWICDetail.indexofjudgementitemschange")
        End Get
        Set(ByVal value As Integer)
            Session("AML_ScreeningResult_ScreeningResultWICDetail.indexofjudgementitemschange") = value
        End Set
    End Property
    Public Property dataID As String
        Get
            Return Session("AML_ScreeningResult_ScreeningResultWICDetail.dataID")
        End Get
        Set(ByVal value As String)
            Session("AML_ScreeningResult_ScreeningResultWICDetail.dataID") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_CLASS() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AML_ScreeningResult_ScreeningResultWICDetail.objAML_WATCHLIST_CLASS")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AML_ScreeningResult_ScreeningResultWICDetail.objAML_WATCHLIST_CLASS") = value
        End Set
    End Property

    Public Property judgementClass() As NawaDevBLL.AMLJudgementClass
        Get
            Return Session("AML_ScreeningResult_ScreeningResultWICDetail.judgementClass")
        End Get
        Set(ByVal value As NawaDevBLL.AMLJudgementClass)
            Session("AML_ScreeningResult_ScreeningResultWICDetail.judgementClass") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()
                Dim strmodule As String = Request.Params("ModuleID")
                Dim intmodule As Integer = Common.DecryptQueryString(strmodule, SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)
                If ObjModule IsNot Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If
                Dim dataStr As String = Request.Params("ID")
                If dataStr IsNot Nothing Then
                    dataID = Common.DecryptQueryString(dataStr, SystemParameterBLL.GetEncriptionKey)
                End If
                If dataID IsNot Nothing Then
                    If AML_WATCHLIST_BLL.IsExistsInApproval(ObjModule.ModuleName, dataID) Then
                        LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."

                        Panelconfirmation.Hidden = False
                        PanelInfo.Hidden = True
                    End If
                    loaddata(dataID)
                Else
                    Throw New ApplicationException("An Error Occurred While Loading the Data With Id")
                End If
                loadcolumn()

                '-- 3 Jun 2021 Adi -- Gridpanel disable button Detail jika Name Watchlistnya kosong (khusus screening country FATF dan OEDC)
                Dim objcommandcol As Ext.Net.CommandColumn = gridJudgementItem.ColumnModel.Columns.Find(Function(x) x.ID = "CommandColumnGridJudgementItem")
                objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"
                '-- End of 3 Jun 2021 Adi -- Gridpanel disable button Detail

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ClearSession()
        judgementheader = New AML_SCREENING_RESULT_WIC
        listjudgementitem = New List(Of AML_SCREENING_RESULT_WIC_DETAIL)
        judgementrequestforheader = New AML_SCREENING_REQUEST
        listjudgementitemchange = New List(Of AML_SCREENING_RESULT_WIC_DETAIL)
        IDWatchlistEncrypted = Nothing
        IDModuleWatchlistEncrypted = Nothing
        ClearOBJDetail()
    End Sub

    Private Sub ClearOBJDetail()
        judgementdetail = New AML_SCREENING_RESULT_WIC_DETAIL
        indexofjudgementitems = Nothing
        judgementdetailchange = New AML_SCREENING_RESULT_WIC_DETAIL
        indexofjudgementitemschange = Nothing
        objAML_WATCHLIST_CLASS = New AML_WATCHLIST_CLASS

        'Name_Score.Clear()
        'DoB_Score.Clear()
        'Nationality_Score.Clear()
        'Identity_Score.Clear()
        'Total_Score.Clear()

        WatchListID.Clear()
        WatchListCategory.Clear()
        WatchListType.Clear()
        FullName.Clear()
        PlaceofBirth.Clear()
        DateofBirth.Clear()
        YearofBirth.Clear()
        Nationality.Clear()

        Remark1.Clear()
        Remark2.Clear()
        Remark3.Clear()
        Remark4.Clear()
        Remark5.Clear()

        RBJudgementMatch.Checked = False
        RBJudgementNotMatch.Checked = False
        JudgementComment.Clear()
    End Sub

    Private Sub loadcolumn()
        ColumnActionLocation(gridJudgementItem, CommandColumnGridJudgementItem)
    End Sub

    Private Sub loaddata(idheader As String)
        Try
            listjudgementitem = New List(Of AML_SCREENING_RESULT_WIC_DETAIL)

            Using objdb As New NawaDatadevEntities
                Dim objResult = objdb.AML_SCREENING_RESULT_WIC.Where(Function(x) x.PK_AML_SCREENING_RESULT_WIC_ID = dataID).FirstOrDefault
                If Not objResult Is Nothing AndAlso Not IsNothing(objResult.FK_AML_SCREENING_REQUEST_ID) Then
                    If objResult.REQUEST_ID IsNot Nothing Then
                        display_Request_ID.Text = objResult.REQUEST_ID
                    End If
                    If objResult.SOURCE IsNot Nothing Then
                        display_Request_Source.Text = objResult.SOURCE
                    End If
                    If objResult.SERVICE IsNot Nothing Then
                        display_Request_Service.Text = objResult.SERVICE
                    End If
                    If objResult.OPERATION IsNot Nothing Then
                        display_Request_Operation.Text = objResult.OPERATION
                    End If
                    If objResult.FK_AML_RESPONSE_CODE_ID IsNot Nothing Then
                        display_Response_ID.Text = objResult.RESPONSE_ID
                    End If

                    Dim objResponse = objdb.AML_RESPONSE_CODE.Where(Function(x) x.PK_AML_RESPONSE_CODE_ID = objResult.FK_AML_RESPONSE_CODE_ID).FirstOrDefault
                    If Not objResponse Is Nothing Then
                        display_Response_Description.Text = objResponse.RESPONSE_CODE_DESCRIPTION
                    End If
                    If objResult.CreatedDate IsNot Nothing Then
                        display_Response_Date.Text = objResult.CreatedDate.Value.ToString("dd-MMM-yyyy, hh:mm:ss tt")
                    End If
                    If objResult.WIC_NO IsNot Nothing Then
                        display_Request_CIF.Text = objResult.WIC_NO
                    End If

                    'Get Searched Information from AML_SCREENING_REQUEST
                    Dim objRequest = objdb.AML_SCREENING_REQUEST.Where(Function(x) x.PK_AML_SCREENING_REQUEST_ID = objResult.FK_AML_SCREENING_REQUEST_ID).FirstOrDefault
                    If Not objRequest Is Nothing Then
                        If objRequest.CreatedDate IsNot Nothing Then
                            display_Request_Date.Text = objRequest.CreatedDate.Value.ToString("dd-MMM-yyyy, hh:mm:ss tt")
                        End If
                        If objRequest.NAME IsNot Nothing Then
                            display_Request_Name.Text = objRequest.NAME
                        End If
                        If objRequest.DOB IsNot Nothing Then
                            display_Request_DOB.Text = objRequest.DOB.Value.Date.ToString("dd-MMM-yyyy")
                        End If
                        If objRequest.NATIONALITY IsNot Nothing Then
                            display_Request_Nationality.Text = objRequest.NATIONALITY
                        End If
                        If objRequest.IDENTITY_NUMBER IsNot Nothing Then
                            display_Request_Identity_Number.Text = objRequest.IDENTITY_NUMBER
                        End If

                        '3-Jun-2021 Adi - samakan tampilan grid dengan Screening Adhoc
                        If Not objRequest Is Nothing Then
                            Dim dtResult As DataTable = New DataTable
                            dtResult = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM vw_AML_SCREENING_RESULT_WIC_DETAIL WHERE FK_AML_SCREENING_REQUEST_ID=" & objResult.FK_AML_SCREENING_REQUEST_ID)

                            gridJudgementItem.GetStore.DataSource = dtResult
                            gridJudgementItem.GetStore.DataBind()
                        End If
                        'End of 3-Jun-2021 Adi - samakan tampilan grid dengan Screening Adhoc

                    End If

                    listjudgementitem = objdb.AML_SCREENING_RESULT_WIC_DETAIL.Where(Function(x) x.FK_AML_SCREENING_RESULT_WIC_ID = objResult.PK_AML_SCREENING_RESULT_WIC_ID).ToList
                    'If listjudgementitem IsNot Nothing Then
                    '    BindJudgement(judgement_Item, listjudgementitem)
                    'End If
                End If

            End Using
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindJudgement(store As Store, list As List(Of AML_SCREENING_RESULT_WIC_DETAIL))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_CATEGORY", GetType(String)))
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_TYPE", GetType(String)))
            objtable.Columns.Add(New DataColumn("ISMATCH", GetType(String)))

            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    If Not IsDBNull(item("FK_AML_WATCHLIST_CATEGORY_ID")) Then
                        item("AML_WATCHLIST_CATEGORY") = AMLJudgementBLL.GetWatchlistCategoryByID(item("FK_AML_WATCHLIST_CATEGORY_ID"))
                    Else
                        item("AML_WATCHLIST_CATEGORY") = ""
                    End If
                    If Not IsDBNull(item("FK_AML_WATCHLIST_TYPE_ID")) Then
                        item("AML_WATCHLIST_TYPE") = AMLJudgementBLL.GetWatchlistTypeByID(item("FK_AML_WATCHLIST_TYPE_ID"))
                    Else
                        item("AML_WATCHLIST_TYPE") = ""
                    End If

                    If Not IsDBNull(item("JUDGEMENT_ISMATCH")) Then
                        If item("JUDGEMENT_ISMATCH") Then
                            item("ISMATCH") = "Match"
                        Else
                            item("ISMATCH") = "Not Match"
                        End If
                    Else
                        item("ISMATCH") = ""
                    End If
                Next
            End If
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ALIAS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS)
        gridAliasInfo.GetStore().DataSource = objtable
        gridAliasInfo.GetStore().DataBind()
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ADDRESS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS)

        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
                If objCountry IsNot Nothing Then
                    item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
                Else
                    item("COUNTRY_NAME") = Nothing
                End If

                Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
                If objAddressType IsNot Nothing Then
                    item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
                Else
                    item("AML_ADDRESS_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gridAddressInfo.GetStore().DataSource = objtable
        gridAddressInfo.GetStore().DataBind()
    End Sub
    Protected Sub Bind_AML_WATCHLIST_IDENTITY()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY)

        objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
                If objIDENTITYType IsNot Nothing Then
                    item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
                Else
                    item("AML_IDENTITY_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gridIdentityInfo.GetStore().DataSource = objtable
        gridIdentityInfo.GetStore().DataBind()
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandJudgementItem(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                ClearOBJDetail()
                WindowJudge.Hidden = False
                loadobjectJudgement(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
            ElseIf e.ExtraParams(1).Value = "Edit" Then
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub loadobjectJudgement(id As String)
        Using objdb As New NawaDatadevEntities
            judgementdetail = listjudgementitem.Where(Function(x) x.PK_AML_SCREENING_RESULT_WIC_DETAIL_ID = id).FirstOrDefault
            If judgementdetail IsNot Nothing Then
                'indexofjudgementitems = listjudgementitem.IndexOf(judgementdetail)
                'If judgementdetail.JUDGEMENT_ISMATCH IsNot Nothing Then
                '    If judgementdetail.JUDGEMENT_ISMATCH Then
                '        RBJudgementMatch.Checked = True
                '    Else
                '        RBJudgementNotMatch.Checked = True
                '    End If
                'End If
                'If judgementdetail.JUDGEMENT_COMMENT IsNot Nothing Then
                '    JudgementComment.Text = judgementdetail.JUDGEMENT_COMMENT
                'End If

                'Load Information to Search
                txt_MATCH_SCORE_NAME_SEARCH.Value = judgementdetail.NAME
                If IsNothing(judgementdetail.DOB) OrElse CDate(judgementdetail.DOB) = DateTime.MinValue Then
                    txt_MATCH_SCORE_DOB_SEARCH.Value = "-"
                Else
                    txt_MATCH_SCORE_DOB_SEARCH.Value = Convert.ToDateTime(judgementdetail.DOB).ToString("dd-MMM-yyyy")
                End If
                txt_MATCH_SCORE_Nationality_SEARCH.Value = IIf(String.IsNullOrEmpty(judgementdetail.NATIONALITY), "-", judgementdetail.NATIONALITY)
                txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH.Value = IIf(String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER), "-", judgementdetail.IDENTITY_NUMBER)

                'Screening Result
                txt_MATCH_SCORE_NAME_WATCHLIST.Value = judgementdetail.NAME_WATCHLIST
                If IsNothing(judgementdetail.DOB) OrElse CDate(judgementdetail.DOB) = DateTime.MinValue Then
                    txt_MATCH_SCORE_DOB_WATCHLIST.Value = ""
                Else
                    txt_MATCH_SCORE_DOB_WATCHLIST.Value = Convert.ToDateTime(judgementdetail.DOB_WATCHLIST).ToString("dd-MMM-yyyy")
                End If
                txt_MATCH_SCORE_Nationality_WATCHLIST.Value = judgementdetail.NATIONALITY_WATCHLIST
                txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST.Value = judgementdetail.IDENTITY_NUMBER_WATCHLIST

                'Screening Match Score
                Dim objParamMatchScore = objdb.AML_SCREENING_MATCH_PARAMETER.FirstOrDefault
                If Not objParamMatchScore Is Nothing Then
                    Dim dblMatchScoreName As Double = CDbl(judgementdetail.MATCH_SCORE_NAME) * CDbl(objParamMatchScore.NAME_WEIGHT / 100)
                    Dim dblMatchScoreDOB As Double = CDbl(judgementdetail.MATCH_SCORE_DOB) * CDbl(objParamMatchScore.DOB_WEIGHT / 100)
                    Dim dblMatchScoreNationality As Double = CDbl(judgementdetail.MATCH_SCORE_NATIONALITY) * CDbl(objParamMatchScore.NATIONALITY_WEIGHT / 100)
                    Dim dblMatchScoreIdentity As Double = CDbl(judgementdetail.MATCH_SCORE_IDENTITY_NUMBER) * CDbl(objParamMatchScore.IDENTITY_WEIGHT / 100)
                    Dim dblMatchScoreTotal = dblMatchScoreName + dblMatchScoreDOB + dblMatchScoreNationality + dblMatchScoreIdentity

                    Dim strMatchScoreName As String = objParamMatchScore.NAME_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_NAME).ToString("#,##0.00") & " % = " & dblMatchScoreName.ToString("#,##0.00") & " %"
                    Dim strMatchScoreDOB As String = objParamMatchScore.DOB_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_DOB).ToString("#,##0.00") & " % = " & dblMatchScoreDOB.ToString("#,##0.00") & " %"
                    Dim strMatchScoreNationality As String = objParamMatchScore.NATIONALITY_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_NATIONALITY).ToString("#,##0.00") & " % = " & dblMatchScoreNationality.ToString("#,##0.00") & " %"
                    Dim strMatchScoreIdentity As String = objParamMatchScore.IDENTITY_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_IDENTITY_NUMBER) & " % = " & dblMatchScoreIdentity.ToString("#,##0.00") & " %"

                    'Show the Formula so the user clear about the score
                    Dim strDividend As String = dblMatchScoreName.ToString("#,##0.00")
                    Dim strDivisor As String = objParamMatchScore.NAME_WEIGHT.ToString()
                    If Not IsNothing(judgementdetail.DOB) AndAlso CDate(judgementdetail.DOB) <> DateTime.MinValue Then
                        strDividend = strDividend & " + " & dblMatchScoreDOB.ToString("#,##0.00")
                        strDivisor = strDivisor & " + " & objParamMatchScore.DOB_WEIGHT.ToString()
                    End If
                    If Not String.IsNullOrEmpty(judgementdetail.NATIONALITY) Then
                        strDividend = strDividend & " + " & dblMatchScoreNationality.ToString("#,##0.00")
                        strDivisor = strDivisor & " + " & objParamMatchScore.NATIONALITY_WEIGHT.ToString()
                    End If
                    If Not String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER) Then
                        strDividend = strDividend & " + " & dblMatchScoreIdentity.ToString("#,##0.00")
                        strDivisor = strDivisor & " + " & objParamMatchScore.IDENTITY_WEIGHT.ToString()
                    End If
                    strDividend = "( " & strDividend & " % )"
                    strDivisor = "( " & strDivisor & " % )"

                    txt_MATCH_SCORE_NAME.Value = strMatchScoreName
                    txt_MATCH_SCORE_DOB.Value = IIf(IsNothing(judgementdetail.DOB) OrElse CDate(judgementdetail.DOB) = DateTime.MinValue, "-", strMatchScoreDOB)
                    txt_MATCH_SCORE_Nationality.Value = IIf(String.IsNullOrEmpty(judgementdetail.NATIONALITY), "-", strMatchScoreNationality)
                    txt_MATCH_SCORE_IDENTITY_NUMBER.Value = IIf(String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER), "-", strMatchScoreIdentity)
                    txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(judgementdetail.MATCH_SCORE).ToString("#,##0.00") & " %"
                End If


                'Show Detail Watch List
                objAML_WATCHLIST_CLASS = AML_WATCHLIST_BLL.GetWatchlistClassByID(judgementdetail.FK_AML_WATCHLIST_ID)
                If objAML_WATCHLIST_CLASS.objAML_WATCHLIST IsNot Nothing AndAlso objAML_WATCHLIST_CLASS.objAML_WATCHLIST IsNot Nothing Then
                    With objAML_WATCHLIST_CLASS.objAML_WATCHLIST
                        WatchListID.Text = .PK_AML_WATCHLIST_ID
                        If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                            Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                            If objWatchlistCategory IsNot Nothing Then
                                WatchListCategory.Text = objWatchlistCategory.CATEGORY_NAME
                            End If
                            '' Add 10-Feb-2023, Display Button Display Watchlist Worldcheck
                            Dim PkWatchlistWorldcheckCategory As String = ""

                            Dim strPkWatchlistWorldcheckCategory As String = "select ParameterValue"
                            strPkWatchlistWorldcheckCategory += " FROM AML_Global_Parameter a "
                            strPkWatchlistWorldcheckCategory += " where PK_GlobalReportParameter_ID = '14'"

                            PkWatchlistWorldcheckCategory = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strPkWatchlistWorldcheckCategory, Nothing)

                            If PkWatchlistWorldcheckCategory IsNot Nothing Then
                                If .FK_AML_WATCHLIST_CATEGORY_ID = PkWatchlistWorldcheckCategory Then
                                    btn_OpenWindowWatchlist.Hidden = "false"

                                    Dim strIDCode As String = 16645 '' Module ID "AML_WATCHLIST_WORLDCHECK"
                                    IDModuleWatchlistEncrypted = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                                    Dim PkWatchlistWorldcheckID As String = ""

                                    Dim strPkWatchlistWorldcheckID As String = "select FK_AML_WATCHLIST_SOURCE_ID"
                                    strPkWatchlistWorldcheckID += " FROM AML_WATCHLIST a "
                                    strPkWatchlistWorldcheckID += " where PK_AML_WATCHLIST_ID = '" & .PK_AML_WATCHLIST_ID.ToString() & "'"

                                    PkWatchlistWorldcheckID = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strPkWatchlistWorldcheckID, Nothing)
                                    IDWatchlistEncrypted = Common.EncryptQueryString(PkWatchlistWorldcheckID, SystemParameterBLL.GetEncriptionKey)

                                    If IDModuleWatchlistEncrypted Is Nothing Or IDWatchlistEncrypted Is Nothing Then
                                        Throw New ApplicationException("Watchlist ID is not found.")
                                    End If
                                Else
                                    btn_OpenWindowWatchlist.Hidden = "true"
                                End If
                            End If
                            '' End 10-Feb-2023
                        End If
                        If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                            Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                            If objWatchlistType IsNot Nothing Then
                                WatchListType.Text = objWatchlistType.TYPE_NAME
                            End If
                        End If
                        If .NAME IsNot Nothing Then
                            FullName.Text = .NAME
                        End If
                        If .BIRTH_PLACE IsNot Nothing Then
                            PlaceofBirth.Text = .BIRTH_PLACE
                        End If
                        If .DATE_OF_BIRTH IsNot Nothing Then
                            DateofBirth.Text = .DATE_OF_BIRTH
                        End If
                        If .YOB IsNot Nothing Then
                            YearofBirth.Text = .YOB
                        End If
                        If .NATIONALITY IsNot Nothing Then
                            Nationality.Text = .NATIONALITY
                        End If
                        If .CUSTOM_REMARK_1 IsNot Nothing Then
                            Remark1.Text = .CUSTOM_REMARK_1
                        End If
                        If .CUSTOM_REMARK_2 IsNot Nothing Then
                            Remark2.Text = .CUSTOM_REMARK_2
                        End If
                        If .CUSTOM_REMARK_3 IsNot Nothing Then
                            Remark3.Text = .CUSTOM_REMARK_3
                        End If
                        If .CUSTOM_REMARK_4 IsNot Nothing Then
                            Remark4.Text = .CUSTOM_REMARK_4
                        End If
                        If .CUSTOM_REMARK_5 IsNot Nothing Then
                            Remark5.Text = .CUSTOM_REMARK_5
                        End If
                    End With
                    Bind_AML_WATCHLIST_ALIAS()
                    Bind_AML_WATCHLIST_ADDRESS()
                    Bind_AML_WATCHLIST_IDENTITY()
                End If
            End If
        End Using
    End Sub

    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            'judgementClass = New AMLJudgementClass
            'With judgementClass
            '    .AMLJudgementHeader = judgementheader
            '    .AMLListResult = listjudgementitemchange
            'End With
            'checkerjudgeitems = New AML_SCREENING_RESULT_WIC_DETAIL
            'checkerjudgeitems = listjudgementitem.Where(Function(x) x.JUDGEMENT_ISMATCH Is Nothing).FirstOrDefault
            'If listjudgementitemchange.Count = 0 Then
            '    Throw New ApplicationException("No Data Has Changed, Please Change Some Data Before Saving or Click the Cancel Button to Cancel the Process")
            'ElseIf checkerjudgeitems IsNot Nothing Then
            '    Throw New ApplicationException("There are Some Data which have not been Judged, Please Judge All Data Before Saving")
            'End If
            'If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not ObjModule.IsUseApproval Then
            '    'tanpa approval
            '    AMLJudgementBLL.SaveJudgementTanpaApproval(judgementClass, ObjModule)
            '    PanelInfo.Hide()
            '    Panelconfirmation.Show()
            '    LblConfirmation.Text = "Data Saved into Database"
            'Else
            '    'with approval
            '    AMLJudgementBLL.SaveJudgementApproval(judgementClass, ObjModule)
            '    PanelInfo.Hide()
            '    Panelconfirmation.Show()
            '    LblConfirmation.Text = "Data Saved into Pending Approval"
            'End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub loadjudgementitemchange(items As AML_SCREENING_RESULT_WIC_DETAIL)
        judgementdetailchange = listjudgementitemchange.Where(Function(x) x.PK_AML_SCREENING_RESULT_WIC_DETAIL_ID = items.PK_AML_SCREENING_RESULT_WIC_DETAIL_ID).FirstOrDefault
        If judgementdetailchange IsNot Nothing Then
            indexofjudgementitemschange = listjudgementitemchange.IndexOf(judgementdetailchange)
            listjudgementitemchange(indexofjudgementitemschange) = items
        Else
            listjudgementitemchange.Add(items)
        End If
    End Sub

    'Protected Sub rgjudgement_click(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim objradio As Ext.Net.Radio = CType(sender, Ext.Net.Radio)
    '        If JudgementComment.Text Is Nothing Or JudgementComment.Text = "" Or JudgementComment.Text = "This Data is Judged Match" Or JudgementComment.Text = "This Data is Judged Not Match" Then
    '            If objradio.InputValue = "1" And objradio.Checked Then
    '                JudgementComment.Text = "This Data is Judged Match"
    '            ElseIf objradio.InputValue = "0" And objradio.Checked Then
    '                JudgementComment.Text = "This Data is Judged Not Match"
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub BtnJudgementSave_Click(sender As Object, e As DirectEventArgs)
        Try
            If RBJudgementMatch.Checked Or RBJudgementNotMatch.Checked Then
                If judgementdetail IsNot Nothing Then
                    If judgementdetail.JUDGEMENT_ISMATCH <> RBJudgementMatch.Checked Or judgementdetail.JUDGEMENT_COMMENT <> JudgementComment.Text Or judgementdetail.JUDGEMENT_ISMATCH Is Nothing Then
                        If RBJudgementMatch.Checked Then
                            judgementdetail.JUDGEMENT_ISMATCH = True
                        Else
                            judgementdetail.JUDGEMENT_ISMATCH = False
                        End If
                        If JudgementComment.Text IsNot Nothing Then
                            judgementdetail.JUDGEMENT_COMMENT = JudgementComment.Text
                        End If
                        If Common.SessionCurrentUser.UserName IsNot Nothing Then
                            judgementdetail.JUDGEMENT_BY = Common.SessionCurrentUser.UserID
                        End If
                        judgementdetail.JUDGEMENT_DATE = Now()
                        If judgementdetail.PK_AML_SCREENING_RESULT_WIC_DETAIL_ID <> Nothing And listjudgementitem IsNot Nothing Then
                            loadjudgementitemchange(judgementdetail)
                            listjudgementitem(indexofjudgementitems) = judgementdetail
                            BindJudgement(judgement_Item, listjudgementitem)
                        Else
                            Throw New ApplicationException("Something Wrong While Saving Data")
                        End If
                    Else
                        Throw New ApplicationException("No Data Has Changed, Please Change Some Data Before Saving or Click the Cancel Button to Cancel the Process")
                    End If
                End If
            Else
                Throw New ApplicationException("Must Conduct Judgment Before Saving Data")
            End If
            WindowJudge.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnJudgementCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowJudge.Hidden = True
            ClearOBJDetail()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    ' 22 Feb 2023 Ari penambahan untuk worldcheck
#Region "World Check"
    Public Property IDWatchlistEncrypted() As String
        Get
            Return Session("ScreeningResultWICDetail.IDWatchlistEncrypted")
        End Get
        Set(ByVal value As String)
            Session("ScreeningResultWICDetail.IDWatchlistEncrypted") = value
        End Set
    End Property
    Public Property IDModuleWatchlistEncrypted() As String
        Get
            Return Session("ScreeningResultWICDetail.IDModuleWatchlistEncrypted")
        End Get
        Set(ByVal value As String)
            Session("ScreeningResultWICDetail.IDModuleWatchlistEncrypted") = value
        End Set
    End Property
    Protected Sub btn_OpenWindowWatchlist_Click()
        Try
            WindowDisplayWatchlist.Hidden = False
            pnlContent.ClearContent()
            pnlContent.AnimCollapse = False
            pnlContent.Loader.SuspendScripting()
            pnlContent.Loader.Url = "~/Parameter/ParameterDetail.aspx?ID=" & IDWatchlistEncrypted & "&ModuleID=" & IDModuleWatchlistEncrypted & ""
            pnlContent.Loader.Params.Clear()
            pnlContent.LoadContent()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Display_Watchlist_Back_Click()
        Try
            WindowDisplayWatchlist.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class
