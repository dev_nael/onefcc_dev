﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="ScreeningTransactionJudgmentApprovalDetail.aspx.vb" Inherits="AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:Container ID="container" runat="server" Layout="VBoxLayout"  BodyStyle="padding:10px" AutoScroll="true">
        <LayoutConfig>
            <ext:VBoxLayoutConfig Align="Stretch"></ext:VBoxLayoutConfig>
        </LayoutConfig>
        <Items>
            <ext:FormPanel ID="PanelInfo" runat="server" Title="Module Approval" BodyStyle="padding:10px"  ButtonAlign="Center">
                <Items>
                    <ext:Panel runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="lblModuleKey" runat="server" FieldLabel="ID">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="txt_ModuleLabel" runat="server" FieldLabel="Module Label">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="txt_ModuleKey" runat="server" FieldLabel="Module Key">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="LblCreatedBy" runat="server" FieldLabel="Last Update By">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="lblCreatedDate" runat="server" FieldLabel="Last Update Date">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="lblAction" runat="server" FieldLabel="Status">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                     
                    <ext:FormPanel ID="FormPanel1" runat="server" Title="Transaction Information" BodyStyle="padding:10px"  ButtonAlign="Center">
                <Items>
            <ext:Panel runat="server" Layout="HBoxLayout">
                <Items>
            <ext:Panel runat="server" Flex="1">
                                <Items>

                                     <ext:DisplayField ID="DisplayFieldTransactionNumber" runat="server" FieldLabel="Transaction Number">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldTransactionDate" runat="server" FieldLabel="Transaction Date" >
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldDebitorCredit" runat="server" FieldLabel="Debit/Credit">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldTransactionCode" runat="server" FieldLabel="Transaction Code" >
                                    </ext:DisplayField>
                                    
                                    </Items>
                                    </ext:Panel>
             <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="DisplayFieldCurrency" runat="server" FieldLabel="Currency">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldNominal" runat="server" FieldLabel="Nominal">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldBranchCode" runat="server" FieldLabel="Branch Code">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldBranchName" runat="server" FieldLabel="Branch Name" >
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldRegionCode" runat="server" FieldLabel="Region Code">
                                    </ext:DisplayField>
                                    </Items>
                                    </ext:Panel>
                    </Items>
                 </ext:Panel>
                    </Items>
                        </ext:FormPanel>
            <ext:FormPanel ID="FormRequest" runat="server" Collapsible="true" BodyPadding="10" Title="Rekening">
                <Items>
               
                    <ext:Panel runat="server" Layout="HBoxLayout">
                       
                        <Items>

                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                     <ext:DisplayField ID="display_Request_CIF" runat="server" FieldLabel="CIF">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldAccountNo" runat="server" FieldLabel="Account No">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Name" runat="server" FieldLabel="Customer Name">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldNationality" runat="server" FieldLabel="Nationality" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldDOB" runat="server" FieldLabel="Date Of Birth" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldIdentityNumber" runat="server" FieldLabel="Identity Number" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_ID" runat="server" FieldLabel="Request ID" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Source" runat="server" FieldLabel="Source" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Service" runat="server" FieldLabel="Service" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Operation" runat="server" FieldLabel="Operation" Hidden="true">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                   
                                    <ext:DisplayField ID="display_Request_DOB" runat="server" FieldLabel="Date Of Birth" >
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Nationality" runat="server" FieldLabel="Nationality">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Identity_Number" runat="server" FieldLabel="Identity Number">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                    <ext:Panel ID="Panel1" runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:FormPanel ID="FormPanelOld" runat="server" Title="Old Data" Flex="1" BodyPadding="10" MarginSpec="0 5 0 0" Border="true" Collapsible="true" ComponentCls="xPanelContainer">
                                <Items>
                                    <ext:GridPanel ID="gridJudgementItemOld" runat="server" MarginSpec="0 0 0 0">
                                        <Store>
                                            <ext:Store ID="judgement_Item_Old" runat="server" IsPagingStore="true" PageSize="10">
                                                <Model>
                                                    <ext:Model runat="server" IDProperty="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Type="Int"></ext:ModelField>
                                                            <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NAME_WATCHLIST" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="AML_WATCHLIST_CATEGORY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="AML_WATCHLIST_TYPE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MATCH_SCORE" Type="Float"></ext:ModelField>
                                                            <ext:ModelField Name="ISMATCH" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel runat="server">
                                            <Columns>
                                                <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                                                <ext:Column runat="server" Text="Name" DataIndex="NAME" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Name Watch List" DataIndex="NAME_WATCHLIST" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Source Watch List" DataIndex="AML_WATCHLIST_CATEGORY" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Is Critical?" DataIndex="AML_WATCHLIST_TYPE" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Similarity" DataIndex="MATCH_SCORE" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Judgement Result" DataIndex="ISMATCH" CellWrap="true" flex="1" />

                                                <ext:CommandColumn ID="CommandColumnGridJudgementItemOld" runat="server" Text="Action" CellWrap="true">
                                                    <Commands>
                                                        <ext:GridCommand Text="Detail" CommandName="Judge" Icon="ApplicationEdit" MinWidth="70">
                                                            <ToolTip Text="Detail"></ToolTip>
                                                        </ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridcommandJudgementItemOld">
                                                            <EventMask ShowMask="true"></EventMask>
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                </Items>
                            </ext:FormPanel>
                            <ext:FormPanel ID="FormPanelNew" runat="server" Title="New Data" Flex="1" BodyPadding="10" Border="true" Collapsible="true" ComponentCls="xPanelContainer">
                                <Items>
                                    <ext:GridPanel ID="gridJudgementItemNew" runat="server" MarginSpec="0 0 0 0">
                                        <Store>
                                            <ext:Store ID="judgement_Item_New" runat="server" IsPagingStore="true" PageSize="10">
                                                <Model>
                                                    <ext:Model runat="server" IDProperty="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Type="Int"></ext:ModelField>
                                                            <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NAME_WATCHLIST" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="AML_WATCHLIST_CATEGORY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="AML_WATCHLIST_TYPE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MATCH_SCORE" Type="Float"></ext:ModelField>
                                                            <ext:ModelField Name="ISMATCH" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel runat="server">
                                            <Columns>
                                                <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                                                <ext:Column runat="server" Text="Name" DataIndex="NAME" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Name Watch List" DataIndex="NAME_WATCHLIST" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Source Watch List" DataIndex="AML_WATCHLIST_CATEGORY" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Is Critical?" DataIndex="AML_WATCHLIST_TYPE" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Similarity" DataIndex="MATCH_SCORE" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Judgement Result" DataIndex="ISMATCH" CellWrap="true" flex="1" />
                                                <ext:CommandColumn ID="CommandColumnGridJudgementItemNew" runat="server" Text="Action" CellWrap="true">
                                                    <Commands>
                                                        <ext:GridCommand Text="Detail" CommandName="Judge" Icon="ApplicationEdit" MinWidth="70">
                                                            <ToolTip Text="Detail"></ToolTip>
                                                        </ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridcommandJudgementItemNew">
                                                            <EventMask ShowMask="true"></EventMask>
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                </Items>
                            </ext:FormPanel>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
            
            <ext:FormPanel ID="FormResponse" runat="server" Collapsible="true" BodyPadding="10" Title="Counter Party">
                <Items>
                    <ext:Panel runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="display_Response_ID" runat="server" FieldLabel="Response ID" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Date" runat="server" FieldLabel="Request Date" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Code" runat="server" FieldLabel="Response Code" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Description" runat="server" FieldLabel="Response Description" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Date" runat="server" FieldLabel="Response Date" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldCifNoCounterParty" runat="server" FieldLabel="CIF No" Hidden="true">
                                    </ext:DisplayField>
                                      <ext:DisplayField ID="DisplayFieldAccountNoCounterParty" runat="server" FieldLabel="Account No" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldCustomerNameCounterParty" runat="server" FieldLabel="Customer Name" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField1WicNoCounterParty" runat="server" FieldLabel="WIC No" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField1WicFirstNameCounterParty" runat="server" FieldLabel="WIC First Name" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldWicMiddleNameCounterParty" runat="server" FieldLabel="WIC Middle Name" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldWicLastNameCounterParty" runat="server" FieldLabel="WIC Last Name" Hidden="true">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldNationalityCounterParty" runat="server" FieldLabel="Nationality" Hidden="true">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldDOBCounterParty" runat="server" FieldLabel="Date Of Birth" Hidden="true">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldIdentityNumCounterParty" runat="server" FieldLabel="Identity Number" Hidden="true">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                            <%--<ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="display_Response_Operation" runat="server" FieldLabel="Operation">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Code" runat="server" FieldLabel="Response Code">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Description" runat="server" FieldLabel="Response Description">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>--%>
                        </Items>
                    </ext:Panel>
                    <ext:Panel ID="Panel2" runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:FormPanel ID="FormPanel3" runat="server" Title="Old Data" Flex="1" BodyPadding="10" MarginSpec="0 5 0 0" Border="true" Collapsible="true" ComponentCls="xPanelContainer">
                                <Items>
                                    <ext:GridPanel ID="GridPanel1" runat="server" MarginSpec="0 0 0 0">
                                        <Store>
                                            <ext:Store ID="judgement_CounterPary_Item_Old" runat="server" IsPagingStore="true" PageSize="10">
                                                <Model>
                                                    <ext:Model runat="server" IDProperty="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Type="Int"></ext:ModelField>
                                                            <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NAME_WATCHLIST" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="AML_WATCHLIST_CATEGORY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="AML_WATCHLIST_TYPE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MATCH_SCORE" Type="Float"></ext:ModelField>
                                                            <ext:ModelField Name="ISMATCH" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel runat="server">
                                            <Columns>
                                                <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                                                <ext:Column runat="server" Text="Name" DataIndex="NAME" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Name Watch List" DataIndex="NAME_WATCHLIST" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Source Watch List" DataIndex="AML_WATCHLIST_CATEGORY" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Is Critical?" DataIndex="AML_WATCHLIST_TYPE" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Similarity" DataIndex="MATCH_SCORE" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Judgement Result" DataIndex="ISMATCH" CellWrap="true" flex="1" />

                                                <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" CellWrap="true">
                                                    <Commands>
                                                        <ext:GridCommand Text="Detail" CommandName="Judge" Icon="ApplicationEdit" MinWidth="70">
                                                            <ToolTip Text="Detail"></ToolTip>
                                                        </ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridcommandJudgementItemOld">
                                                            <EventMask ShowMask="true"></EventMask>
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                </Items>
                            </ext:FormPanel>
                            <ext:FormPanel ID="FormPanel4" runat="server" Title="New Data" Flex="1" BodyPadding="10" Border="true" Collapsible="true" ComponentCls="xPanelContainer">
                                <Items>
                                    <ext:GridPanel ID="GridPanel2" runat="server" MarginSpec="0 0 0 0">
                                        <Store>
                                            <ext:Store ID="judgement_CounterPary_Item_New" runat="server" IsPagingStore="true" PageSize="10">
                                                <Model>
                                                    <ext:Model runat="server" IDProperty="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Type="Int"></ext:ModelField>
                                                            <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NAME_WATCHLIST" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="AML_WATCHLIST_CATEGORY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="AML_WATCHLIST_TYPE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MATCH_SCORE" Type="Float"></ext:ModelField>
                                                            <ext:ModelField Name="ISMATCH" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel runat="server">
                                            <Columns>
                                                <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                                                <ext:Column runat="server" Text="Name" DataIndex="NAME" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Name Watch List" DataIndex="NAME_WATCHLIST" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Source Watch List" DataIndex="AML_WATCHLIST_CATEGORY" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Is Critical?" DataIndex="AML_WATCHLIST_TYPE" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Similarity" DataIndex="MATCH_SCORE" CellWrap="true" flex="1" />
                                                <ext:Column runat="server" Text="Judgement Result" DataIndex="ISMATCH" CellWrap="true" flex="1" />
                                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" CellWrap="true">
                                                    <Commands>
                                                        <ext:GridCommand Text="Detail" CommandName="Judge" Icon="ApplicationEdit" MinWidth="70">
                                                            <ToolTip Text="Detail"></ToolTip>
                                                        </ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridcommandJudgementItemNew">
                                                            <EventMask ShowMask="true"></EventMask>
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                </Items>
                            </ext:FormPanel>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
           
            <ext:FormPanel ID="FormPanel2" runat="server" Collapsible="true" BodyPadding="10" Title="Conductor" Hidden="true">
                <Items>
                    <ext:Panel runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                     <ext:DisplayField ID="DisplayFieldCIFNoCounductor" runat="server" FieldLabel="CIF" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldAccountNoCounductor" runat="server" FieldLabel="Account No" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldCustomerNameCounductor" runat="server" FieldLabel="Customer Name" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldWICNoCounductor" runat="server" FieldLabel="WIC No" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldWICFirstNameCounductor" runat="server" FieldLabel="WIC First Name" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldWICMiddleNameCounductor" runat="server" FieldLabel="WIC Middle Name" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldWICLastNameCounductor" runat="server" FieldLabel="WIC Last Name" Hidden="true">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldNationalityCounductor" runat="server" FieldLabel="Nationality" Hidden="true">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldDOBCounductor" runat="server" FieldLabel="Date Of Birth" Hidden="true">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldIdentityNumberCounductor" runat="server" FieldLabel="Identity Number" Hidden="true">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
            
               <ext:FormPanel ID="FormPanelNoUnik" runat="server" Collapsible="true" BodyPadding="10" Title="Account No Lawan">
                <Items>
                    <ext:Panel runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                     <ext:DisplayField ID="DisplayFieldNamaNomorUnik" runat="server" FieldLabel="Name">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                     <ext:Panel ID="Panel3" runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:FormPanel ID="FormPanelAccountNoLawan" runat="server" Title="Result Account No Lawan" Flex="1" BodyPadding="10" MarginSpec="0 5 0 0" Border="true" Collapsible="true" ComponentCls="xPanelContainer">
                                <Items>
                                    <ext:GridPanel ID="GridPanelNoUnik" runat="server" MarginSpec="0 0 0 0">
                <Store>
                    <ext:Store ID="StoreAccountNoLawan" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" IDProperty="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NAME_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_CATEGORY_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CATEGORY_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_TYPE_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="TYPE_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="DOB_WATCHLIST" Type="Date"></ext:ModelField>
                                    <ext:ModelField Name="NATIONALITY_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="IDENTITY_NUMBER_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE" Type="String"></ext:ModelField>
                                     <ext:ModelField Name="ISMATCH" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_DOB" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_NATIONALITY" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_IDENTITY_NUMBER" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_PCT" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                        <ext:Column ID="Column29" runat="server" DataIndex="FK_AML_WATCHLIST_ID" Text="Watchlist ID" Flex="1" Hidden="true"></ext:Column>
                        <ext:Column ID="Column30" runat="server" DataIndex="NAME" Text="Name" Flex="1"></ext:Column>
                        <ext:Column ID="Column31" runat="server" DataIndex="NAME_WATCHLIST" Text="Name Watch List" Flex="1"></ext:Column>
                        <ext:Column ID="Column32" runat="server" DataIndex="CATEGORY_NAME" Text="Watch List Category" Flex="1"></ext:Column>
                        <ext:Column ID="Column33" runat="server" DataIndex="TYPE_NAME" Text="Watch List Type" Flex="1"></ext:Column>
                        <ext:DateColumn ID="DateColumn4" runat="server" DataIndex="DOB_WATCHLIST" Text="Date Of Birth" Flex="1" format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column34" runat="server" DataIndex="NATIONALITY_WATCHLIST" Text="Nationality" Flex="1"></ext:Column>
                        <ext:Column ID="Column35" runat="server" DataIndex="IDENTITY_NUMBER_WATCHLIST" Text="ID Number" Flex="1"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn7" runat="server" DataIndex="MATCH_SCORE" Text="Match Score (%)" Flex="1" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                        <ext:Column runat="server" Text="Judgement Result" DataIndex="ISMATCH" CellWrap="true" flex="1" />
                        <ext:NumberColumn ID="NumberColumn8" runat="server" DataIndex="MATCH_SCORE_PCT" Text="Match Score" Flex="1" Format="#,##0.00 %" Hidden="true"></ext:NumberColumn>


                       
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
                                </Items>
                                </ext:FormPanel>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
           
                    
                </Items>
                <Buttons>
                    <ext:Button ID="BtnSave" runat="server" Text="Approve" Icon="DiskBlack">
                        <DirectEvents>
                            <Click OnEvent="BtnSave_Click">
                                <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"> </EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnReject" runat="server" Text="Reject" Icon="Decline">
                        <DirectEvents>
                            <Click OnEvent="BtnReject_Click">
                                <EventMask ShowMask="true" Msg="Saving Reject Data..." MinDelay="500"> </EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnCancel" runat="server" Text="Back to List" Icon="PageBack">
                        <DirectEvents>
                            <Click OnEvent="BtnCancel_Click">
                                <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"> </EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Container>
    <ext:Window ID="WindowJudge" Title="Judgement Detail" runat="server" Modal="true" Maximizable="true" Hidden="true" BodyStyle="padding:20px" Scrollable="Both" ButtonAlign="Center" Width="1000" Height="500">
        <Items>
            <ext:FormPanel ID="ScoreDetail" runat="server" Collapsible="true" BodyPadding="10" Title="Search Score Detail">
                <Items>
                    <ext:DisplayField ID="Name_Score" runat="server" FieldLabel="Name Score">
                    </ext:DisplayField>
                    <ext:DisplayField ID="DoB_Score" runat="server" FieldLabel="Date of Birth Score">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Nationality_Score" runat="server" FieldLabel="Nationality Score">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Identity_Score" runat="server" FieldLabel="Identity Number Score">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Total_Score" runat="server" FieldLabel="Total Score">
                    </ext:DisplayField>
                </Items>
            </ext:FormPanel>
            
            <ext:FormPanel ID="GeneralInformation" runat="server" Collapsible="true" BodyPadding="10" Title="General Information">
                <Items>
                    <ext:DisplayField ID="WatchListID" runat="server" FieldLabel="Watch List ID">
                    </ext:DisplayField>
                    <ext:DisplayField ID="WatchListCategory" runat="server" FieldLabel="Watch List Category">
                    </ext:DisplayField>
                    <ext:DisplayField ID="WatchListType" runat="server" FieldLabel="Watch List Type">
                    </ext:DisplayField>
                    <ext:DisplayField ID="FullName" runat="server" FieldLabel="Full Name">
                    </ext:DisplayField>
                    <ext:DisplayField ID="PlaceofBirth" runat="server" FieldLabel="Place of Birth">
                    </ext:DisplayField>
                    <ext:DisplayField ID="DateofBirth" runat="server" FieldLabel="Date of Birth">
                    </ext:DisplayField>
                    <ext:DisplayField ID="YearofBirth" runat="server" FieldLabel="Year of Birth">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Nationality" runat="server" FieldLabel="Nationality">
                    </ext:DisplayField>
                    <%-- Watch List Alias --%>
                    <ext:GridPanel ID="gridAliasInfo" runat="server" Title="Alias(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <Store>
                            <ext:Store ID="Store_gridAliasInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ALIAS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ALIAS_NAME" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column8" runat="server" DataIndex="ALIAS_NAME" Text="Alias Name" MinWidth="300" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Alias --%>

                    <%-- Watch List Address --%>
                    <ext:GridPanel ID="gridAddressInfo" runat="server" Title="Address(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <Store>
                            <ext:Store ID="Store_gridAddressInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ADDRESS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_AML_COUNTRY_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="POSTAL_CODE" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column9" runat="server" DataIndex="AML_ADDRESS_TYPE_NAME" Text="Type" MinWidth="50"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="ADDRESS" Text="Address" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="FK_AML_COUNTRY_CODE" Text="Country Code" MinWidth="100" Hidden="true"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="COUNTRY_NAME" Text="Country Name" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="POSTAL_CODE" Text="Postal Code" MinWidth="100"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Address --%>

                    <%-- Watch List Identity --%>
                    <ext:GridPanel ID="gridIdentityInfo" runat="server" Title="Identity(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <Store>
                            <ext:Store ID="Store_gridIdentityInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_IDENTITY_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_IDENTITY_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDENTITYNUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column14" runat="server" DataIndex="AML_IDENTITY_TYPE_NAME" Text="Type" MinWidth="200" Flex="1"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="IDENTITYNUMBER" Text="Identity" MinWidth="300"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Identity --%>
                    <ext:DisplayField ID="Remark1" runat="server" FieldLabel="Remark 1">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark2" runat="server" FieldLabel="Remark 2">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark3" runat="server" FieldLabel="Remark 3">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark4" runat="server" FieldLabel="Remark 4">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark5" runat="server" FieldLabel="Remark 5">
                    </ext:DisplayField>
                </Items>
            </ext:FormPanel>
            <ext:Panel ID="PanelJudge" runat="server" Layout="AnchorLayout">
                <Items>
                    <ext:RadioGroup runat="server" ID="RGJudgement" AnchorHorizontal="50%" FieldLabel="Is Judgement Match ?" LabelWidth="150">
                        <Items>
                            <ext:Radio runat="server" ID="RBJudgementMatch" BoxLabel="Match" InputValue="1" ReadOnly="true">
                            </ext:Radio>

                            <ext:Radio runat="server" ID="RBJudgementNotMatch" BoxLabel="Not Match" InputValue="0" ReadOnly="true">
                            </ext:Radio>
                        </Items>
                    </ext:RadioGroup>
                    <ext:TextArea runat="server" ID="JudgementComment" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLengthText="1000" LabelWidth="150" Editable="false"/>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Window>
    <ext:FormPanel ID="Panelconfirmation" runat="server" ClientIDMode="Static" Title="Confirmation" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>



