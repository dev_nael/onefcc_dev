﻿Imports System.Data
Imports System.Data.SqlClient
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDAL
Imports NawaDevBLL
Imports NawaDevDAL
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevBLL.GlobalReportFunctionBLL
Imports System.Text
Imports System.Reflection
Imports System.Drawing
Imports System.Runtime.CompilerServices
Imports System.IO

Public Module Extenders
    <System.Runtime.CompilerServices.Extension>
    Public Function ToDataTable(Of T)(collection As IEnumerable(Of T), tableName As String) As DataTable
        Dim tbl As DataTable = ToDataTable(collection)
        tbl.TableName = tableName
        Return tbl
    End Function

    <System.Runtime.CompilerServices.Extension>
    Public Function ToDataTable(Of T)(collection As IEnumerable(Of T)) As DataTable
        Dim dt As New DataTable()
        Dim tt As Type = GetType(T)
        Dim pia As PropertyInfo() = tt.GetProperties()
        'Create the columns in the DataTable
        For Each pi As PropertyInfo In pia
            Dim a =
            If(Nullable.GetUnderlyingType(pi.PropertyType), pi.PropertyType)
            dt.Columns.Add(pi.Name, If(Nullable.GetUnderlyingType(pi.PropertyType), pi.PropertyType))
            'If pi.PropertyType Is GetType(DateTime) Then

            '    ' pi.Columns.Add("columnName", TypeOf (datetime2)
            'Else

            'End If
        Next
        'Populate the table
        For Each item As T In collection
            Dim dr As DataRow = dt.NewRow()
            dr.BeginEdit()
            For Each pi As PropertyInfo In pia
                ' cek value apakah null?
                ' If pi.GetValue(item, Nothing) = Nothing Then
                If pi.GetValue(item, Nothing) Is Nothing Then
                    dr(pi.Name) = DBNull.Value
                Else
                    dr(pi.Name) = pi.GetValue(item, Nothing)
                End If

            Next
            dr.EndEdit()
            dt.Rows.Add(dr)
        Next
        Return dt
    End Function


End Module

Partial Class AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentEdit
    Inherits Parent

#Region "Session"
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.ObjModule") = value
        End Set
    End Property

    Public Property listjudgementitemtransaction As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL))
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransaction") = value
        End Set
    End Property

    Public Property listjudgementitemtransactionCounteryParty As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransactionCounteryParty")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL))
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransactionCounteryParty") = value
        End Set
    End Property

    Public Property listjudgementitemtransactionAccountNoLawan As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransactionAccountNoLawan")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL))
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransactionAccountNoLawan") = value
        End Set
    End Property

    Public Property listjudgementitemtransactionchange As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransactionchange")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL))
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransactionchange") = value
        End Set
    End Property

    Public Property judgementheader As NawaDevDAL.AML_TRANSACTION
        Get
            Return Session("AML_TransactionResultDetail.judgementheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_TRANSACTION)
            Session("AML_TransactionResultDetail.judgementheader") = value
        End Set
    End Property

    Public Property screeningtransaction As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION)
        Get
            Return Session("AML_Screening_TransactionResultDetail.judgementheader")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION))
            Session("AML_Screening_TransactionResultDetail.judgementheader") = value
        End Set
    End Property

    Public Property screeningtransactionCounterParty As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION)
        Get
            Return Session("AML_Screening_TransactionResultDetailCounterParty.judgementheader")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION))
            Session("AML_Screening_TransactionResultDetailCounterParty.judgementheader") = value
        End Set
    End Property
    Public Property judgementrequestforheader As NawaDevDAL.AML_SCREENING_REQUEST
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementrequestforheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_REQUEST)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementrequestforheader") = value
        End Set
    End Property
    Public Property judgementdetail As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementdetail")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementdetail") = value
        End Set
    End Property
    Public Property judgementdetailchange As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementdetailchange")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementdetailchange") = value
        End Set
    End Property
    Public Property checkerjudgeitems As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.checkerjudgeitems")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.checkerjudgeitems") = value
        End Set
    End Property
    Public Property indexofjudgementitems As Integer
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.indexofjudgementitems")
        End Get
        Set(ByVal value As Integer)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.indexofjudgementitems") = value
        End Set
    End Property

    Public Property indexofjudgementitemschange As Integer
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.indexofjudgementitemschange")
        End Get
        Set(ByVal value As Integer)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.indexofjudgementitemschange") = value
        End Set
    End Property
    Public Property dataID As String
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.dataID")
        End Get
        Set(ByVal value As String)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.dataID") = value
        End Set
    End Property
    Public Property listwatchlistalreadymatch As List(Of NawaDevDAL.AML_SCREENING_CUSTOMER_MATCH)
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.listcutomeralreadyinwatchlist")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_CUSTOMER_MATCH))
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.listcutomeralreadyinwatchlist") = value
        End Set
    End Property

    Public Property listwatchlistalreadymatchWICNO As List(Of NawaDevDAL.AML_SCREENING_WIC_MATCH)
        Get
            Return Session("AML_AMLScreeningJudgmentWIC_AMLJudgmentEdit.listcutomeralreadyinwatchlist")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_WIC_MATCH))
            Session("AML_AMLScreeningJudgmentWIC_AMLJudgmentEdit.listcutomeralreadyinwatchlist") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_CLASS() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.objAML_WATCHLIST_CLASS")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.objAML_WATCHLIST_CLASS") = value
        End Set
    End Property

    Public Property judgementClass() As NawaDevBLL.AMLJudgementClass
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementClass")
        End Get
        Set(ByVal value As NawaDevBLL.AMLJudgementClass)
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.judgementClass") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()
                Dim strmodule As String = Request.Params("ModuleID")
                Dim intmodule As Integer = Common.DecryptQueryString(strmodule, SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)
                If ObjModule IsNot Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Update) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If
                Dim dataStr As String = Request.Params("ID")
                If dataStr IsNot Nothing Then
                    dataID = Common.DecryptQueryString(dataStr, SystemParameterBLL.GetEncriptionKey)
                End If
                If dataID IsNot Nothing Then
                    If AML_WATCHLIST_BLL.IsExistsInApproval(ObjModule.ModuleName, dataID) Then
                        LblConfirmation.Text = "Sorry, this Data is already exists in Pending Approval."

                        Panelconfirmation.Hidden = False
                        PanelInfo.Hidden = True
                    End If
                    loaddata(dataID)
                Else
                    Throw New ApplicationException("An Error Occurred While Loading the Data With Id")
                End If
                DetailCIF.BackColor = Color.Red
                loadcolumn()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ClearSession()
        judgementheader = New AML_TRANSACTION
        screeningtransaction = New List(Of AML_SCREENING_RESULT_TRANSACTION)
        screeningtransactionCounterParty = New List(Of AML_SCREENING_RESULT_TRANSACTION)
        listjudgementitemtransaction = New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        listjudgementitemtransactionCounteryParty = New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        listjudgementitemtransactionAccountNoLawan = New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        judgementrequestforheader = New AML_SCREENING_REQUEST
        listwatchlistalreadymatch = New List(Of NawaDevDAL.AML_SCREENING_CUSTOMER_MATCH)
        listwatchlistalreadymatchWICNO = New List(Of NawaDevDAL.AML_SCREENING_WIC_MATCH)
        listjudgementitemtransactionchange = New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        ClearOBJDetail()
    End Sub

    Private Sub ClearOBJDetail()
        judgementdetail = New AML_SCREENING_RESULT_TRANSACTION_DETAIL
        indexofjudgementitems = Nothing
        judgementdetailchange = New AML_SCREENING_RESULT_TRANSACTION_DETAIL
        indexofjudgementitemschange = Nothing
        objAML_WATCHLIST_CLASS = New AML_WATCHLIST_CLASS

        Name_Score.Clear()
        DoB_Score.Clear()
        Nationality_Score.Clear()
        Identity_Score.Clear()
        Total_Score.Clear()

        WatchListID.Clear()
        WatchListCategory.Clear()
        WatchListType.Clear()
        FullName.Clear()
        PlaceofBirth.Clear()
        DateofBirth.Clear()
        YearofBirth.Clear()
        Nationality.Clear()

        Remark1.Clear()
        Remark2.Clear()
        Remark3.Clear()
        Remark4.Clear()
        Remark5.Clear()

        RBJudgementMatch.Checked = False
        RBJudgementNotMatch.Checked = False
        JudgementComment.Clear()
    End Sub

    Private Sub loadcolumn()
        'ColumnActionLocation(gridJudgementItem, CommandColumn4)
        'ColumnActionLocation(gridJudgementItem, CommandColumn4A)
    End Sub

    Private Sub loaddata(idheader As String)
        judgementheader = AMLJudgementBLL.GetAMLJudgementTransaction(idheader)




        'If judgementrequestforheader.REQUEST_ID IsNot Nothing Then
        '    display_Request_ID.Text = judgementrequestforheader.REQUEST_ID
        'End If
        'If judgementrequestforheader.SOURCE IsNot Nothing Then
        '    display_Request_Source.Text = judgementrequestforheader.SOURCE
        'End If
        'If judgementrequestforheader.SERVICE IsNot Nothing Then
        '    display_Request_Service.Text = judgementrequestforheader.SERVICE
        'End If
        'If judgementrequestforheader.OPERATION IsNot Nothing Then
        '    display_Request_Operation.Text = judgementrequestforheader.OPERATION
        'End If
        'If judgementheader.RESPONSE_ID IsNot Nothing Then
        '    display_Response_ID.Text = judgementheader.RESPONSE_ID
        'End If
        'If judgementrequestforheader.CreatedDate IsNot Nothing Then
        '    display_Request_Date.Text = judgementrequestforheader.CreatedDate.Value.ToString("dd-MMM-yyyy, hh:mm:ss tt")
        ''End If
        'If judgementheader.FK_AML_RESPONSE_CODE_ID IsNot Nothing Then
        '    If judgementheader.FK_AML_RESPONSE_CODE_ID = 1 Then
        '        display_Response_Code.Text = "1 - Found"
        '    ElseIf judgementheader.FK_AML_RESPONSE_CODE_ID = 2 Then
        '        display_Response_Code.Text = "2 - Not Found"
        '    Else
        '        display_Response_Code.Text = "3 - Error"
        '    End If
        'End If
        'If judgementrequestforheader.RESPONSE_DESCRIPTION IsNot Nothing Then
        '    display_Response_Description.Text = judgementrequestforheader.RESPONSE_DESCRIPTION
        'End If
        'If judgementrequestforheader.RESPONSE_DATE IsNot Nothing Then
        '    display_Response_Date.Text = judgementrequestforheader.RESPONSE_DATE.Value.ToString("dd-MMM-yyyy, hh:mm:ss tt")
        'End If



        Dim transaksi = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementTransaction(judgementheader.TRANSACTION_NUMBER)
            If transaksi IsNot Nothing Then
                screeningtransaction = AMLJudgementBLL.GetAMLJudgementTransactionListyCIFNOLAWAN(transaksi.TRANSACTION_NUMBER, transaksi.CIFNO)
                For Each item In screeningtransaction
                    If Not IsDBNull(item.FK_AML_SCREENING_REQUEST_ID) Then
                        If item.WIC_NO IsNot Nothing Then
                            Dim listwicno = AMLJudgementBLL.GetAMLJudgementTransactionWICMATCH(item.WIC_NO)
                            If listwicno IsNot Nothing Then
                                listwatchlistalreadymatchWICNO.Add(listwicno)
                            End If

                    End If
                        If item.CIF_NO IsNot Nothing Then
                            Dim listcifno = AMLJudgementBLL.GetAMLJudgementTransactionCIFMATCH(item.CIF_NO)
                            If listcifno IsNot Nothing Then
                                listwatchlistalreadymatch.Add(listcifno)
                            End If

                    End If
                    judgementrequestforheader = AMLJudgementBLL.GetRequestForHeader(item.FK_AML_SCREENING_REQUEST_ID)

                End If
                Next
                Dim Customer As AML_CUSTOMER = NawaDevBLL.AMLReportRiskRating.GetCustomerBYCIFNO(transaksi.CIFNO)
                With Customer
                    Dim objParamGET_DETAIL_CIF_ADDRESS(0) As SqlParameter
                    objParamGET_DETAIL_CIF_ADDRESS(0) = New SqlParameter
                    objParamGET_DETAIL_CIF_ADDRESS(0).ParameterName = "@CIFNo"
                    objParamGET_DETAIL_CIF_ADDRESS(0).Value = .CIFNo

                    Dim objdt_DETAIL_CIF_Address As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_GET_DETAIL_CIF_ADDRESS_ADHOC", objParamGET_DETAIL_CIF_ADDRESS)
                    If objdt_DETAIL_CIF_Address IsNot Nothing Then
                        gp_DETAIL_CIF_ADDRESS.GetStore.DataSource = objdt_DETAIL_CIF_Address
                        gp_DETAIL_CIF_ADDRESS.GetStore.DataBind()
                    End If

                    Dim objParamGET_DETAIL_CIF_IDENTITY(0) As SqlParameter
                    objParamGET_DETAIL_CIF_IDENTITY(0) = New SqlParameter
                    objParamGET_DETAIL_CIF_IDENTITY(0).ParameterName = "@CIFNo"
                    objParamGET_DETAIL_CIF_IDENTITY(0).Value = .CIFNo

                    Dim objdt_DETAIL_CIF_Identity As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_GET_DETAIL_CIF_IDENTITY_ADHOC", objParamGET_DETAIL_CIF_IDENTITY)
                    If objdt_DETAIL_CIF_Identity IsNot Nothing Then
                        gp_DETAIL_CIF_IDENTITY.GetStore.DataSource = objdt_DETAIL_CIF_Identity
                        gp_DETAIL_CIF_IDENTITY.GetStore.DataBind()
                    End If
                    Dim branch As AML_BRANCH = NawaDevBLL.AMLReportRiskRating.GetBranchBYFK(.FK_AML_Creation_Branch_Code)
                    If branch IsNot Nothing Then
                        DisplayKodeCabang.Value = .FK_AML_Creation_Branch_Code
                        DisplayFieldBranchCode.Value = .FK_AML_Creation_Branch_Code
                        DisplayNamaCabang.Value = branch.REGION_NAME
                        DisplayFieldBranchName.Value = branch.REGION_NAME
                        DisplayKantorWilayah.Value = branch.REGION_CODE
                        DisplayFieldRegionCode.Value = branch.REGION_CODE
                    Else
                        DisplayKodeCabang.Value = Nothing
                        DisplayFieldBranchCode.Value = Nothing
                        DisplayNamaCabang.Value = Nothing
                        DisplayFieldBranchName.Value = Nothing
                        DisplayKantorWilayah.Value = Nothing
                        DisplayFieldRegionCode.Value = Nothing
                    End If
                    DisplayCIFNO.Value = .CIFNo
                    If .CUSTOMERNAME IsNot Nothing Then
                        DisplayCustomerName.Value = .CUSTOMERNAME
                    End If
                    If .PLACEOFBIRTH IsNot Nothing Then
                        DisplayPOB.Value = .PLACEOFBIRTH
                    End If
                    If .DATEOFBIRTH IsNot Nothing Then
                        DisplayDOB.Value = .DATEOFBIRTH.Value.Date.ToString("dd-MMM-yyyy")
                    End If
                    If .NPWP IsNot Nothing Then
                        DisplayNPWP.Value = .NPWP
                    End If
                    If .FK_AML_RISK_CODE IsNot Nothing Then
                        If .FK_AML_RISK_CODE = "L" Then
                            DisplayRiskProfileOneFCC.Value = "Low"
                        ElseIf .FK_AML_RISK_CODE = "M" Then
                            DisplayRiskProfileOneFCC.Value = "Medium"
                        ElseIf .FK_AML_RISK_CODE = "H" Then
                            DisplayRiskProfileOneFCC.Value = "High"
                        Else
                            DisplayRiskProfileOneFCC.Value = Nothing
                        End If

                    End If
                    If .CUSTOMER_RISK IsNot Nothing Then
                        DisplayRiskProfileT24.Value = .CUSTOMER_RISK
                    End If
                    Dim customertype As AML_CUSTOMER_TYPE = NawaDevBLL.AMLReportRiskRating.GetCustomerTypeFK(.FK_AML_Customer_Type_Code)
                    If customertype IsNot Nothing Then
                        DisplayTipeNasabah.Value = customertype.Customer_Type_Name
                    Else
                        DisplayTipeNasabah.Value = Nothing
                    End If

                    Dim country As AML_COUNTRY = NawaDevBLL.AMLReportRiskRating.GetCountryByFK(.FK_AML_CITIZENSHIP_CODE)
                    If country IsNot Nothing Then
                        DisplayNegara.Value = country.AML_COUNTRY_Name
                    Else
                        DisplayNegara.Value = Nothing
                    End If

                End With

                If judgementrequestforheader.CIF_NO IsNot Nothing Then
                    display_Request_CIF.Text = judgementrequestforheader.CIF_NO
                End If
                If judgementrequestforheader.NAME IsNot Nothing Then
                    display_Request_Name.Text = judgementrequestforheader.NAME
                End If
                If judgementrequestforheader.DOB IsNot Nothing Then
                    display_Request_DOB.Text = judgementrequestforheader.DOB.Value.Date.ToString("dd-MMM-yyyy")
                End If
                If judgementrequestforheader.NATIONALITY IsNot Nothing Then
                    display_Request_Nationality.Text = judgementrequestforheader.NATIONALITY
                End If
                If judgementrequestforheader.IDENTITY_NUMBER IsNot Nothing Then
                    display_Request_Identity_Number.Text = judgementrequestforheader.IDENTITY_NUMBER
                End If
                If judgementheader.FK_DebitOrCredit_Code IsNot Nothing Then
                    If judgementheader.FK_DebitOrCredit_Code = "D" Then
                        DisplayFieldDebitorCredit.Text = "Debit"
                    ElseIf judgementheader.FK_DebitOrCredit_Code = "C" Then
                        DisplayFieldDebitorCredit.Text = "Credit"
                    Else
                        DisplayFieldDebitorCredit.Text = Nothing
                    End If
                End If
                If judgementheader.FK_AML_TRANSACTION_CODE IsNot Nothing Then
                    DisplayFieldTransactionCode.Text = judgementheader.FK_AML_TRANSACTION_CODE
                End If
                If judgementheader.TRANSACTION_DATE IsNot Nothing Then
                    DisplayFieldTransactionDate.Text = judgementheader.TRANSACTION_DATE.Value.Date.ToString("dd-MMM-yyyy")
                End If
                If judgementheader.TRANSACTION_NUMBER IsNot Nothing Then
                    DisplayFieldTransactionNumber.Text = judgementheader.TRANSACTION_NUMBER
                End If



                Dim transresultrekening = AMLJudgementBLL.GetAMLJudgementTransactionListyCIFNOLAWAN(transaksi.TRANSACTION_NUMBER, transaksi.CIFNO)
                If transresultrekening IsNot Nothing Then
                    For Each item In transresultrekening
                        Dim resultrekening = AMLJudgementBLL.GetAMLJudgementTransactionDetailByFK(item.PK_AML_SCREENING_RESULT_TRANSACTION_ID)
                        If resultrekening IsNot Nothing Then
                            listjudgementitemtransaction.Add(resultrekening)
                        End If
                    Next

                    If listjudgementitemtransaction IsNot Nothing Then
                        For Each item In listjudgementitemtransaction
                            If item.JUDGEMENT_ISMATCH IsNot Nothing Then
                                CommandColumn4A.Hidden = False
                            Else
                                CommandColumn4.Hidden = False
                            End If
                        Next
                        BindJudgement(judgement_Item, listjudgementitemtransaction)
                    End If
                End If
                If transaksi.ACCOUNT_NAME_LAWAN IsNot Nothing Then
                    DisplayFieldNamaNomorUnik.Text = transaksi.ACCOUNT_NAME_LAWAN
                    Dim screeningaccountlawan As List(Of AML_SCREENING_RESULT_TRANSACTION) = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementTransactionListByTransactionNumber(transaksi.TRANSACTION_NUMBER)
                    If screeningaccountlawan IsNot Nothing Then
                        For Each item In screeningaccountlawan
                            Dim resultaccount = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementTransactionDetailByFK(item.PK_AML_SCREENING_RESULT_TRANSACTION_ID)
                            If resultaccount IsNot Nothing Then
                                listjudgementitemtransactionAccountNoLawan.Add(resultaccount)
                            End If
                        Next
                        If listjudgementitemtransactionAccountNoLawan IsNot Nothing Then
                            BindJudgement(StoreAccountNoLawan, listjudgementitemtransactionAccountNoLawan)
                        End If

                    End If
                End If






                If transaksi.ACCOUNT_NO IsNot Nothing Then
                    DisplayFieldAccountNo.Text = transaksi.ACCOUNT_NO
                End If
                If transaksi.TRANSACTION_AMOUNT IsNot Nothing Then
                    DisplayFieldNominal.Text = transaksi.TRANSACTION_AMOUNT
                End If
                If transaksi.FK_AML_CURRENCY_CODE IsNot Nothing Then
                    Dim currency = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementCurrency(transaksi.FK_AML_CURRENCY_CODE)
                    If currency IsNot Nothing Then
                        If currency.CURRENCY_NAME IsNot Nothing Then
                            DisplayFieldCurrency.Text = currency.CURRENCY_NAME
                        End If
                    End If

                End If

                If transaksi.CIFNO_LAWAN IsNot Nothing And transaksi.WICNo_LAWAN Is Nothing Then
                    DisplayFieldCifNoCounterParty.Hidden = False
                    DisplayFieldAccountNoCounterParty.Hidden = False
                    DisplayFieldCustomerNameCounterParty.Hidden = False
                    DisplayFieldDOBCounterParty.Hidden = False
                    DisplayFieldNationalityCounterParty.Hidden = False
                    DisplayFieldIdentityNumCounterParty.Hidden = False
                    DisplayFieldCifNoCounterParty.Text = transaksi.CIFNO_LAWAN
                    Dim customers As AML_CUSTOMER = NawaDevBLL.AMLReportRiskRating.GetCustomerBYCIFNO(transaksi.CIFNO_LAWAN)
                    If transaksi.ACCOUNT_NO_LAWAN IsNot Nothing Then
                        DisplayFieldAccountNoCounterParty.Text = transaksi.ACCOUNT_NO_LAWAN
                    End If
                    If customers IsNot Nothing Then
                        If customers.CUSTOMERNAME IsNot Nothing Then
                            DisplayFieldCustomerNameCounterParty.Text = customers.CUSTOMERNAME
                        End If
                        If customers.FK_AML_CITIZENSHIP_CODE IsNot Nothing Then
                            Dim country As AML_COUNTRY = NawaDevBLL.AMLReportRiskRating.GetCountryByFK(customers.FK_AML_CITIZENSHIP_CODE)
                            If country IsNot Nothing Then
                                DisplayFieldNationalityCounterParty.Text = country.AML_COUNTRY_Name
                            End If
                        End If
                        If customers.DATEOFBIRTH IsNot Nothing Then
                            DisplayFieldDOBCounterParty.Text = customers.DATEOFBIRTH.Value.Date.ToString("dd-MMM-yyyy")
                        End If
                        Dim identity As AML_CUSTOMER_IDENTITY = NawaDevBLL.AMLReportRiskRating.GetIdentityNumberByCIFNo(customers.CIFNo)
                        If identity IsNot Nothing Then
                            DisplayFieldIdentityNumberCounductor.Text = identity.CUSTOMER_IDENTITY_NUMBER
                        End If
                    End If
                    Dim transresult = AMLJudgementBLL.GetAMLJudgementTransactionListyCIFNOLAWAN(transaksi.TRANSACTION_NUMBER, transaksi.CIFNO_LAWAN)
                    If transresult IsNot Nothing Then
                        For Each item In transresult
                            Dim resultciflawan = AMLJudgementBLL.GetAMLJudgementTransactionDetailByFK(item.PK_AML_SCREENING_RESULT_TRANSACTION_ID)
                            If resultciflawan IsNot Nothing Then
                                listjudgementitemtransactionCounteryParty.Add(resultciflawan)
                            End If
                        Next

                        For Each item In listjudgementitemtransactionCounteryParty
                            If item.JUDGEMENT_ISMATCH IsNot Nothing Then
                                CommandColumn3A.Hidden = False
                            Else
                                CommandColumn3.Hidden = False
                            End If
                        Next
                        screeningtransactionCounterParty = transresult
                        BindJudgement(Store2, listjudgementitemtransactionCounteryParty)
                    End If

                ElseIf transaksi.CIFNO_LAWAN Is Nothing And transaksi.WICNo_LAWAN IsNot Nothing Then
                    DisplayField1WicNoCounterParty.Hidden = False
                    DisplayField1WicFirstNameCounterParty.Hidden = False
                    DisplayFieldWicMiddleNameCounterParty.Hidden = False
                    DisplayFieldWicLastNameCounterParty.Hidden = False
                    DisplayFieldDOBCounterParty.Hidden = False
                    DisplayFieldNationalityCounterParty.Hidden = False
                    DisplayFieldIdentityNumCounterParty.Hidden = False
                    DisplayField1WicNoCounterParty.Text = transaksi.WICNo_LAWAN
                    Dim watchlist = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementWatchlist(transaksi.WICNo_LAWAN)
                    If watchlist IsNot Nothing Then
                        If watchlist.INDV_First_Name IsNot Nothing Then
                            DisplayField1WicFirstNameCounterParty.Text = watchlist.INDV_First_Name
                        End If
                        If watchlist.INDV_Middle_Name IsNot Nothing Then
                            DisplayFieldWicMiddleNameCounterParty.Text = watchlist.INDV_Middle_Name
                        End If
                        If watchlist.INDV_Last_Name IsNot Nothing Then
                            DisplayFieldWicLastNameCounterParty.Text = watchlist.INDV_Last_Name
                        End If
                        If watchlist.INDV_BirthDate IsNot Nothing Then
                            DisplayFieldDOBCounterParty.Text = watchlist.INDV_BirthDate.Value.Date.ToString("dd-MMM-yyyy")
                        End If
                        If watchlist.INDV_Nationality1 IsNot Nothing Or watchlist.INDV_Nationality2 IsNot Nothing Or watchlist.INDV_Nationality3 IsNot Nothing Then
                            DisplayFieldNationalityCounterParty.Text = watchlist.INDV_Nationality1 + "," + watchlist.INDV_Nationality2 + "," + watchlist.INDV_Nationality3
                        End If
                        If watchlist.INDV_ID_Number IsNot Nothing Then
                            DisplayFieldIdentityNumCounterParty.Text = watchlist.INDV_ID_Number
                        End If
                    End If
                    Dim transresult = AMLJudgementBLL.GetAMLJudgementTransactionListByWICNOLAWAN(transaksi.TRANSACTION_NUMBER, transaksi.WICNo_LAWAN)
                    If transresult IsNot Nothing Then
                        For Each item In transresult
                            Dim resultwiclawan = AMLJudgementBLL.GetAMLJudgementTransactionDetailByFK(item.PK_AML_SCREENING_RESULT_TRANSACTION_ID)
                            If resultwiclawan IsNot Nothing Then
                                listjudgementitemtransactionCounteryParty.Add(resultwiclawan)
                            End If
                        Next

                        For Each item In listjudgementitemtransactionCounteryParty
                            If item.JUDGEMENT_ISMATCH IsNot Nothing Then
                                CommandColumn3A.Hidden = False
                            Else
                                CommandColumn3.Hidden = False
                            End If
                        Next
                        screeningtransactionCounterParty = transresult
                        BindJudgement(Store2, listjudgementitemtransactionCounteryParty)
                    End If

                End If


                'If screeningtransaction IsNot Nothing Then
                '    listjudgementitemtransaction = AMLJudgementBLL.GetAMLJudgementTransactionList(screeningtransaction.PK_AML_SCREENING_RESULT_TRANSACTION_ID)
                'End If




            End If





    End Sub

    Private Sub BindJudgement(store As Store, list As List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            objtable.Columns.Add(New DataColumn("CATEGORY_NAME", GetType(String)))
            objtable.Columns.Add(New DataColumn("TYPE_NAME", GetType(String)))
            objtable.Columns.Add(New DataColumn("ISMATCH", GetType(String)))

            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    If Not IsDBNull(item("FK_AML_WATCHLIST_CATEGORY_ID")) Then
                        item("CATEGORY_NAME") = AMLJudgementBLL.GetWatchlistCategoryByID(item("FK_AML_WATCHLIST_CATEGORY_ID"))
                    Else
                        item("CATEGORY_NAME") = ""
                    End If
                    If Not IsDBNull(item("FK_AML_WATCHLIST_TYPE_ID")) Then
                        item("TYPE_NAME") = AMLJudgementBLL.GetWatchlistTypeByID(item("FK_AML_WATCHLIST_TYPE_ID"))
                    Else
                        item("TYPE_NAME") = ""
                    End If

                    If Not IsDBNull(item("JUDGEMENT_ISMATCH")) Then
                        If item("JUDGEMENT_ISMATCH") Then
                            item("ISMATCH") = "Match"
                        Else
                            item("ISMATCH") = "Not Match"
                        End If
                    Else
                        item("ISMATCH") = ""
                    End If
                Next
            End If
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ALIAS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS)
        gridAliasInfo.GetStore().DataSource = objtable
        gridAliasInfo.GetStore().DataBind()
        GridPanel1.GetStore().DataSource = objtable
        GridPanel1.GetStore().DataBind()
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ADDRESS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS)

        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
                If objCountry IsNot Nothing Then
                    item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
                Else
                    item("COUNTRY_NAME") = Nothing
                End If

                Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
                If objAddressType IsNot Nothing Then
                    item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
                Else
                    item("AML_ADDRESS_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gridAddressInfo.GetStore().DataSource = objtable
        gridAddressInfo.GetStore().DataBind()
        GridPanel2.GetStore().DataSource = objtable
        GridPanel2.GetStore().DataBind()

    End Sub
    Protected Sub Bind_AML_WATCHLIST_IDENTITY()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY)

        objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
                If objIDENTITYType IsNot Nothing Then
                    item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
                Else
                    item("AML_IDENTITY_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gridIdentityInfo.GetStore().DataSource = objtable
        gridIdentityInfo.GetStore().DataBind()
        GridPanel3.GetStore().DataSource = objtable
        GridPanel3.GetStore().DataBind()
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandJudgementItem(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "JudgeRekening" Then
                ClearOBJDetail()
                WindowJudge.Hidden = False
                loadobjectJudgement(id)
            ElseIf e.ExtraParams(1).Value = "JudgeCounterParty" Then
                ClearOBJDetail()
                WindowJudge.Hidden = False
                Session("PKGridCounterParty") = id
                loadobjectJudgementCounterParty(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                ClearOBJDetail()
                WindowJudgeDetail.Hidden = False
                loadobjectJudgement(id)
            ElseIf e.ExtraParams(1).Value = "DetailCounterParty" Then
                ClearOBJDetail()
                WindowJudgeDetail.Hidden = False
                loadobjectJudgementCounterParty(id)
            ElseIf e.ExtraParams(1).Value = "DetailAccountNoLawan" Then
                ClearOBJDetail()
                WindowJudgeDetail.Hidden = False
                loadobjectJudgementAccountNoLawan(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
            ElseIf e.ExtraParams(1).Value = "Edit" Then
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub loadobjectJudgement(id As String)
        judgementdetail = listjudgementitemtransaction.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = id).FirstOrDefault
        If judgementdetail IsNot Nothing Then
            txt_MATCH_SCORE_NAME_SEARCH.Value = judgementdetail.NAME
            txt_MATCH_SCORE_NAME_SEARCH2.Value = judgementdetail.NAME
            If IsNothing(judgementdetail.DOB) OrElse CDate(judgementdetail.DOB) = DateTime.MinValue Then
                txt_MATCH_SCORE_DOB_SEARCH.Value = "-"
                txt_MATCH_SCORE_DOB_SEARCH2.Value = "-"
            Else
                txt_MATCH_SCORE_DOB_SEARCH.Value = Convert.ToDateTime(judgementdetail.DOB).ToString("dd-MMM-yyyy")
                txt_MATCH_SCORE_DOB_SEARCH2.Value = Convert.ToDateTime(judgementdetail.DOB).ToString("dd-MMM-yyyy")
            End If
            txt_MATCH_SCORE_Nationality_SEARCH.Value = IIf(String.IsNullOrEmpty(judgementdetail.NATIONALITY), "-", judgementdetail.NATIONALITY)
            txt_MATCH_SCORE_Nationality_SEARCH2.Value = IIf(String.IsNullOrEmpty(judgementdetail.NATIONALITY), "-", judgementdetail.NATIONALITY)
            txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH.Value = IIf(String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER), "-", judgementdetail.IDENTITY_NUMBER)
            txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH2.Value = IIf(String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER), "-", judgementdetail.IDENTITY_NUMBER)

            'Screening Result
            txt_MATCH_SCORE_NAME_WATCHLIST.Value = judgementdetail.NAME_WATCHLIST
            txt_MATCH_SCORE_NAME_WATCHLIST2.Value = judgementdetail.NAME_WATCHLIST
            If IsNothing(judgementdetail.DOB) OrElse CDate(judgementdetail.DOB) = DateTime.MinValue Then
                txt_MATCH_SCORE_DOB_WATCHLIST.Value = ""
                txt_MATCH_SCORE_DOB_WATCHLIST2.Value = ""
            Else
                txt_MATCH_SCORE_DOB_WATCHLIST.Value = Convert.ToDateTime(judgementdetail.DOB_WATCHLIST).ToString("dd-MMM-yyyy")
                txt_MATCH_SCORE_DOB_WATCHLIST2.Value = Convert.ToDateTime(judgementdetail.DOB_WATCHLIST).ToString("dd-MMM-yyyy")
            End If
            txt_MATCH_SCORE_Nationality_WATCHLIST.Value = judgementdetail.NATIONALITY_WATCHLIST
            txt_MATCH_SCORE_Nationality_WATCHLIST2.Value = judgementdetail.NATIONALITY_WATCHLIST
            txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST.Value = judgementdetail.IDENTITY_NUMBER_WATCHLIST
            txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST2.Value = judgementdetail.IDENTITY_NUMBER_WATCHLIST

            '' Add 01-Nov-2021
            Dim NAME_WEIGHT As Double
            Dim DOB_WEIGHT As Double
            Dim NATIONALITY_WEIGHT As Double
            Dim IDENTITY_WEIGHT As Double

            Dim objdt_SCREENING_PARAMETER As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select NAME_WEIGHT, DOB_WEIGHT,NATIONALITY_WEIGHT,IDENTITY_WEIGHT FROM AML_SCREENING_MATCH_PARAMETER", Nothing)
            If objdt_SCREENING_PARAMETER IsNot Nothing Then
                For Each row As DataRow In objdt_SCREENING_PARAMETER.Rows
                    NAME_WEIGHT = row.Item("NAME_WEIGHT")
                    DOB_WEIGHT = row.Item("DOB_WEIGHT")
                    NATIONALITY_WEIGHT = row.Item("NATIONALITY_WEIGHT")
                    IDENTITY_WEIGHT = row.Item("IDENTITY_WEIGHT")
                Next row
            End If

            'Screening Match Score
            Dim dblMatchScoreName As Double = CDbl(judgementdetail.MATCH_SCORE_NAME) * CDbl(NAME_WEIGHT / 100)
            Dim dblMatchScoreDOB As Double = CDbl(judgementdetail.MATCH_SCORE_DOB) * CDbl(DOB_WEIGHT / 100)
            Dim dblMatchScoreNationality As Double = CDbl(judgementdetail.MATCH_SCORE_NATIONALITY) * CDbl(NATIONALITY_WEIGHT / 100)
            Dim dblMatchScoreIdentity As Double = CDbl(judgementdetail.MATCH_SCORE_IDENTITY_NUMBER) * CDbl(IDENTITY_WEIGHT / 100)
            Dim dblMatchScoreTotal = dblMatchScoreName + dblMatchScoreDOB + dblMatchScoreNationality + dblMatchScoreIdentity

            Dim strMatchScoreName As String = NAME_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_NAME).ToString("#,##0.00") & " % = " & dblMatchScoreName.ToString("#,##0.00") & " %"
            Dim strMatchScoreDOB As String = DOB_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_DOB).ToString("#,##0.00") & " % = " & dblMatchScoreDOB.ToString("#,##0.00") & " %"
            Dim strMatchScoreNationality As String = NATIONALITY_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_NATIONALITY).ToString("#,##0.00") & " % = " & dblMatchScoreNationality.ToString("#,##0.00") & " %"
            Dim strMatchScoreIdentity As String = IDENTITY_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_IDENTITY_NUMBER).ToString("#,##0.00") & " % = " & dblMatchScoreIdentity.ToString("#,##0.00") & " %"

            'Show the Formula so the user clear about the score
            Dim strDividend As String = dblMatchScoreName.ToString("#,##0.00")
            Dim strDivisor As String = NAME_WEIGHT.ToString()
            If Not CDate(txt_MATCH_SCORE_DOB_WATCHLIST.Value) = DateTime.MinValue Then
                strDividend = strDividend & " + " & dblMatchScoreDOB.ToString("#,##0.00")
                strDivisor = strDivisor & " + " & DOB_WEIGHT.ToString()
            End If
            If Not String.IsNullOrEmpty(txt_MATCH_SCORE_Nationality_SEARCH.Value) Then
                strDividend = strDividend & " + " & dblMatchScoreNationality.ToString("#,##0.00")
                strDivisor = strDivisor & " + " & NATIONALITY_WEIGHT.ToString()
            End If
            If Not String.IsNullOrEmpty(txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH.Value) Then
                strDividend = strDividend & " + " & dblMatchScoreIdentity.ToString("#,##0.00")
                strDivisor = strDivisor & " + " & IDENTITY_WEIGHT.ToString()
            End If
            strDividend = "( " & strDividend & " % )"
            strDivisor = "( " & strDivisor & " % )"

            txt_MATCH_SCORE_NAME.Value = strMatchScoreName
            txt_MATCH_SCORE_DOB.Value = IIf(CDate(txt_MATCH_SCORE_DOB_WATCHLIST.Value) = DateTime.MinValue, "-", strMatchScoreDOB)
            txt_MATCH_SCORE_Nationality.Value = IIf(String.IsNullOrEmpty(txt_MATCH_SCORE_Nationality_SEARCH.Value), "-", strMatchScoreNationality)
            txt_MATCH_SCORE_IDENTITY_NUMBER.Value = IIf(String.IsNullOrEmpty(txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH.Value), "-", strMatchScoreIdentity)
            'txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(e.ExtraParams(2).Value).ToString("#,##0.00 %")
            txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(judgementdetail.MATCH_SCORE).ToString("#,##0.00") & " %"

            txt_MATCH_SCORE_NAME2.Value = strMatchScoreName
            txt_MATCH_SCORE_DOB2.Value = IIf(CDate(txt_MATCH_SCORE_DOB_WATCHLIST2.Value) = DateTime.MinValue, "-", strMatchScoreDOB)
            txt_MATCH_SCORE_Nationality2.Value = IIf(String.IsNullOrEmpty(txt_MATCH_SCORE_Nationality_SEARCH2.Value), "-", strMatchScoreNationality)
            txt_MATCH_SCORE_IDENTITY_NUMBER2.Value = IIf(String.IsNullOrEmpty(txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH2.Value), "-", strMatchScoreIdentity)
            'txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(e.ExtraParams(2).Value).ToString("#,##0.00 %")
            txt_MATCH_SCORE_TOTAL2.Value = strDividend & " / " & strDivisor & " = " & CDbl(judgementdetail.MATCH_SCORE).ToString("#,##0.00") & " %"
            '' End 01-Nov-2021


            indexofjudgementitems = listjudgementitemtransaction.IndexOf(judgementdetail)
            If judgementdetail.JUDGEMENT_ISMATCH IsNot Nothing Then
                If judgementdetail.JUDGEMENT_ISMATCH Then
                    RBJudgementMatch.Checked = True
                Else
                    RBJudgementNotMatch.Checked = True
                End If
            End If
            If judgementdetail.JUDGEMENT_COMMENT IsNot Nothing Then
                JudgementComment.Text = judgementdetail.JUDGEMENT_COMMENT

            End If
            If judgementdetail.MATCH_SCORE_NAME IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_NAME = 0 Then
                    Name_Score.Text = "-"
                    Name_Score2.Text = "-"
                Else
                    Name_Score.Text = judgementdetail.MATCH_SCORE_NAME
                    Name_Score2.Text = judgementdetail.MATCH_SCORE_NAME
                End If
            End If
            If judgementdetail.MATCH_SCORE_DOB IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_DOB = 0 Then
                    DoB_Score.Text = "-"
                    DoB_Score2.Text = "-"
                Else
                    DoB_Score.Text = judgementdetail.MATCH_SCORE_DOB
                    DoB_Score2.Text = judgementdetail.MATCH_SCORE_DOB
                End If
            End If
            If judgementdetail.MATCH_SCORE_NATIONALITY IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_NATIONALITY = 0 Then
                    Nationality_Score.Text = "-"
                    Nationality_Score2.Text = "-"
                Else
                    Nationality_Score.Text = judgementdetail.MATCH_SCORE_NATIONALITY
                    Nationality_Score2.Text = judgementdetail.MATCH_SCORE_NATIONALITY
                End If
            End If
            If judgementdetail.MATCH_SCORE_IDENTITY_NUMBER IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_IDENTITY_NUMBER = 0 Then
                    Identity_Score.Text = "-"
                    Identity_Score2.Text = "-"
                Else
                    Identity_Score.Text = judgementdetail.MATCH_SCORE_IDENTITY_NUMBER
                    Identity_Score2.Text = judgementdetail.MATCH_SCORE_IDENTITY_NUMBER
                End If
            End If
            If judgementdetail.MATCH_SCORE IsNot Nothing Then
                If judgementdetail.MATCH_SCORE = 0 Then
                    Total_Score.Text = "-"
                    Total_Score2.Text = "-"
                Else
                    Total_Score.Text = judgementdetail.MATCH_SCORE
                    Total_Score2.Text = judgementdetail.MATCH_SCORE
                End If
            End If

            objAML_WATCHLIST_CLASS = AML_WATCHLIST_BLL.GetWatchlistClassByID(judgementdetail.FK_AML_WATCHLIST_ID)
            If objAML_WATCHLIST_CLASS IsNot Nothing Then
                With objAML_WATCHLIST_CLASS.objAML_WATCHLIST
                    WatchListID.Text = .PK_AML_WATCHLIST_ID
                    WatchListID2.Text = .PK_AML_WATCHLIST_ID
                    If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                        Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                        If objWatchlistCategory IsNot Nothing Then
                            WatchListCategory.Text = objWatchlistCategory.CATEGORY_NAME
                            WatchListCategory2.Text = objWatchlistCategory.CATEGORY_NAME
                        End If
                    End If
                    If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                        Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                        If objWatchlistType IsNot Nothing Then
                            WatchListType.Text = objWatchlistType.TYPE_NAME
                            WatchListType2.Text = objWatchlistType.TYPE_NAME
                        End If
                    End If
                    If .NAME IsNot Nothing Then
                        FullName.Text = .NAME
                        FullName2.Text = .NAME
                    End If
                    If .BIRTH_PLACE IsNot Nothing Then
                        PlaceofBirth.Text = .BIRTH_PLACE
                        PlaceofBirth2.Text = .BIRTH_PLACE
                    End If
                    If .DATE_OF_BIRTH IsNot Nothing Then
                        DateofBirth.Text = .DATE_OF_BIRTH.Value.Date.ToString("dd-MMM-yyyy")
                        DateofBirth2.Text = .DATE_OF_BIRTH.Value.Date.ToString("dd-MMM-yyyy")
                    End If
                    If .YOB IsNot Nothing Then
                        YearofBirth.Text = .YOB
                        YearofBirth2.Text = .YOB
                    End If
                    If .NATIONALITY IsNot Nothing Then
                        Nationality.Text = .NATIONALITY
                        Nationality2.Text = .NATIONALITY
                    End If
                    If .CUSTOM_REMARK_1 IsNot Nothing Then
                        Remark1.Text = .CUSTOM_REMARK_1
                        DisplayField14.Text = .CUSTOM_REMARK_1
                    End If
                    If .CUSTOM_REMARK_2 IsNot Nothing Then
                        Remark2.Text = .CUSTOM_REMARK_2
                        DisplayField15.Text = .CUSTOM_REMARK_2
                    End If
                    If .CUSTOM_REMARK_3 IsNot Nothing Then
                        Remark3.Text = .CUSTOM_REMARK_3
                        DisplayField16.Text = .CUSTOM_REMARK_3
                    End If
                    If .CUSTOM_REMARK_4 IsNot Nothing Then
                        Remark4.Text = .CUSTOM_REMARK_4
                        DisplayField17.Text = .CUSTOM_REMARK_4
                    End If
                    If .CUSTOM_REMARK_5 IsNot Nothing Then
                        Remark5.Text = .CUSTOM_REMARK_5
                        DisplayField17.Text = .CUSTOM_REMARK_5
                    End If
                End With
                Bind_AML_WATCHLIST_ALIAS()
                Bind_AML_WATCHLIST_ADDRESS()
                Bind_AML_WATCHLIST_IDENTITY()
            End If
        End If
    End Sub

    Protected Sub loadobjectJudgementCounterParty(id As String)
        judgementdetail = listjudgementitemtransactionCounteryParty.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = id).FirstOrDefault
        If judgementdetail IsNot Nothing Then
            txt_MATCH_SCORE_NAME_SEARCH.Value = judgementdetail.NAME
            txt_MATCH_SCORE_NAME_SEARCH2.Value = judgementdetail.NAME
            If IsNothing(judgementdetail.DOB) OrElse CDate(judgementdetail.DOB) = DateTime.MinValue Then
                txt_MATCH_SCORE_DOB_SEARCH.Value = "-"
                txt_MATCH_SCORE_DOB_SEARCH2.Value = "-"
            Else
                txt_MATCH_SCORE_DOB_SEARCH.Value = Convert.ToDateTime(judgementdetail.DOB).ToString("dd-MMM-yyyy")
                txt_MATCH_SCORE_DOB_SEARCH2.Value = Convert.ToDateTime(judgementdetail.DOB).ToString("dd-MMM-yyyy")
            End If
            txt_MATCH_SCORE_Nationality_SEARCH.Value = IIf(String.IsNullOrEmpty(judgementdetail.NATIONALITY), "-", judgementdetail.NATIONALITY)
            txt_MATCH_SCORE_Nationality_SEARCH2.Value = IIf(String.IsNullOrEmpty(judgementdetail.NATIONALITY), "-", judgementdetail.NATIONALITY)
            txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH.Value = IIf(String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER), "-", judgementdetail.IDENTITY_NUMBER)
            txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH2.Value = IIf(String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER), "-", judgementdetail.IDENTITY_NUMBER)

            'Screening Result
            txt_MATCH_SCORE_NAME_WATCHLIST.Value = judgementdetail.NAME_WATCHLIST
            txt_MATCH_SCORE_NAME_WATCHLIST2.Value = judgementdetail.NAME_WATCHLIST
            If IsNothing(judgementdetail.DOB) OrElse CDate(judgementdetail.DOB) = DateTime.MinValue Then
                txt_MATCH_SCORE_DOB_WATCHLIST.Value = ""
                txt_MATCH_SCORE_DOB_WATCHLIST2.Value = ""
            Else
                txt_MATCH_SCORE_DOB_WATCHLIST.Value = Convert.ToDateTime(judgementdetail.DOB_WATCHLIST).ToString("dd-MMM-yyyy")
                txt_MATCH_SCORE_DOB_WATCHLIST2.Value = Convert.ToDateTime(judgementdetail.DOB_WATCHLIST).ToString("dd-MMM-yyyy")
            End If
            txt_MATCH_SCORE_Nationality_WATCHLIST.Value = judgementdetail.NATIONALITY_WATCHLIST
            txt_MATCH_SCORE_Nationality_WATCHLIST2.Value = judgementdetail.NATIONALITY_WATCHLIST
            txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST.Value = judgementdetail.IDENTITY_NUMBER_WATCHLIST
            txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST2.Value = judgementdetail.IDENTITY_NUMBER_WATCHLIST

            '' Add 03-Aug-2022, copy dari yg Customer, karena di WIC blom di pasang codingnya
            Dim NAME_WEIGHT As Double
            Dim DOB_WEIGHT As Double
            Dim NATIONALITY_WEIGHT As Double
            Dim IDENTITY_WEIGHT As Double

            Dim objdt_SCREENING_PARAMETER As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select NAME_WEIGHT, DOB_WEIGHT,NATIONALITY_WEIGHT,IDENTITY_WEIGHT FROM AML_SCREENING_MATCH_PARAMETER", Nothing)
            If objdt_SCREENING_PARAMETER IsNot Nothing Then
                For Each row As DataRow In objdt_SCREENING_PARAMETER.Rows
                    NAME_WEIGHT = row.Item("NAME_WEIGHT")
                    DOB_WEIGHT = row.Item("DOB_WEIGHT")
                    NATIONALITY_WEIGHT = row.Item("NATIONALITY_WEIGHT")
                    IDENTITY_WEIGHT = row.Item("IDENTITY_WEIGHT")
                Next row
            End If

            'Screening Match Score
            Dim dblMatchScoreName As Double = CDbl(judgementdetail.MATCH_SCORE_NAME) * CDbl(NAME_WEIGHT / 100)
            Dim dblMatchScoreDOB As Double = CDbl(judgementdetail.MATCH_SCORE_DOB) * CDbl(DOB_WEIGHT / 100)
            Dim dblMatchScoreNationality As Double = CDbl(judgementdetail.MATCH_SCORE_NATIONALITY) * CDbl(NATIONALITY_WEIGHT / 100)
            Dim dblMatchScoreIdentity As Double = CDbl(judgementdetail.MATCH_SCORE_IDENTITY_NUMBER) * CDbl(IDENTITY_WEIGHT / 100)
            Dim dblMatchScoreTotal = dblMatchScoreName + dblMatchScoreDOB + dblMatchScoreNationality + dblMatchScoreIdentity

            Dim strMatchScoreName As String = NAME_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_NAME).ToString("#,##0.00") & " % = " & dblMatchScoreName.ToString("#,##0.00") & " %"
            Dim strMatchScoreDOB As String = DOB_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_DOB).ToString("#,##0.00") & " % = " & dblMatchScoreDOB.ToString("#,##0.00") & " %"
            Dim strMatchScoreNationality As String = NATIONALITY_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_NATIONALITY).ToString("#,##0.00") & " % = " & dblMatchScoreNationality.ToString("#,##0.00") & " %"
            Dim strMatchScoreIdentity As String = IDENTITY_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_IDENTITY_NUMBER).ToString("#,##0.00") & " % = " & dblMatchScoreIdentity.ToString("#,##0.00") & " %"

            'Show the Formula so the user clear about the score
            Dim strDividend As String = dblMatchScoreName.ToString("#,##0.00")
            Dim strDivisor As String = NAME_WEIGHT.ToString()
            If Not CDate(txt_MATCH_SCORE_DOB_WATCHLIST.Value) = DateTime.MinValue Then
                strDividend = strDividend & " + " & dblMatchScoreDOB.ToString("#,##0.00")
                strDivisor = strDivisor & " + " & DOB_WEIGHT.ToString()
            End If
            If Not String.IsNullOrEmpty(txt_MATCH_SCORE_Nationality_SEARCH.Value) Then
                strDividend = strDividend & " + " & dblMatchScoreNationality.ToString("#,##0.00")
                strDivisor = strDivisor & " + " & NATIONALITY_WEIGHT.ToString()
            End If
            If Not String.IsNullOrEmpty(txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH.Value) Then
                strDividend = strDividend & " + " & dblMatchScoreIdentity.ToString("#,##0.00")
                strDivisor = strDivisor & " + " & IDENTITY_WEIGHT.ToString()
            End If
            strDividend = "( " & strDividend & " % )"
            strDivisor = "( " & strDivisor & " % )"

            txt_MATCH_SCORE_NAME.Value = strMatchScoreName
            txt_MATCH_SCORE_DOB.Value = IIf(CDate(txt_MATCH_SCORE_DOB_WATCHLIST.Value) = DateTime.MinValue, "-", strMatchScoreDOB)
            txt_MATCH_SCORE_Nationality.Value = IIf(String.IsNullOrEmpty(txt_MATCH_SCORE_Nationality_SEARCH.Value), "-", strMatchScoreNationality)
            txt_MATCH_SCORE_IDENTITY_NUMBER.Value = IIf(String.IsNullOrEmpty(txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH.Value), "-", strMatchScoreIdentity)
            'txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(e.ExtraParams(2).Value).ToString("#,##0.00 %")
            txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(judgementdetail.MATCH_SCORE).ToString("#,##0.00") & " %"

            txt_MATCH_SCORE_NAME2.Value = strMatchScoreName
            txt_MATCH_SCORE_DOB2.Value = IIf(CDate(txt_MATCH_SCORE_DOB_WATCHLIST2.Value) = DateTime.MinValue, "-", strMatchScoreDOB)
            txt_MATCH_SCORE_Nationality2.Value = IIf(String.IsNullOrEmpty(txt_MATCH_SCORE_Nationality_SEARCH2.Value), "-", strMatchScoreNationality)
            txt_MATCH_SCORE_IDENTITY_NUMBER2.Value = IIf(String.IsNullOrEmpty(txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH2.Value), "-", strMatchScoreIdentity)
            'txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(e.ExtraParams(2).Value).ToString("#,##0.00 %")
            txt_MATCH_SCORE_TOTAL2.Value = strDividend & " / " & strDivisor & " = " & CDbl(judgementdetail.MATCH_SCORE).ToString("#,##0.00") & " %"
            '' End 03-Aug-2022

            indexofjudgementitems = listjudgementitemtransactionCounteryParty.IndexOf(judgementdetail)
            If judgementdetail.JUDGEMENT_ISMATCH IsNot Nothing Then
                If judgementdetail.JUDGEMENT_ISMATCH Then
                    RBJudgementMatch.Checked = True

                Else
                    RBJudgementNotMatch.Checked = True

                End If
            End If
            If judgementdetail.JUDGEMENT_COMMENT IsNot Nothing Then
                JudgementComment.Text = judgementdetail.JUDGEMENT_COMMENT

            End If
            If judgementdetail.MATCH_SCORE_NAME IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_NAME = 0 Then
                    Name_Score.Text = "-"
                    Name_Score2.Text = "-"
                Else
                    Name_Score.Text = judgementdetail.MATCH_SCORE_NAME
                    Name_Score2.Text = judgementdetail.MATCH_SCORE_NAME
                End If
            End If
            If judgementdetail.MATCH_SCORE_DOB IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_DOB = 0 Then
                    DoB_Score.Text = "-"
                    DoB_Score2.Text = "-"
                Else
                    DoB_Score.Text = judgementdetail.MATCH_SCORE_DOB
                    DoB_Score2.Text = judgementdetail.MATCH_SCORE_DOB
                End If
            End If
            If judgementdetail.MATCH_SCORE_NATIONALITY IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_NATIONALITY = 0 Then
                    Nationality_Score.Text = "-"
                    Nationality_Score2.Text = "-"
                Else
                    Nationality_Score.Text = judgementdetail.MATCH_SCORE_NATIONALITY
                    Nationality_Score2.Text = judgementdetail.MATCH_SCORE_NATIONALITY
                End If
            End If
            If judgementdetail.MATCH_SCORE_IDENTITY_NUMBER IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_IDENTITY_NUMBER = 0 Then
                    Identity_Score.Text = "-"
                    Identity_Score2.Text = "-"
                Else
                    Identity_Score.Text = judgementdetail.MATCH_SCORE_IDENTITY_NUMBER
                    Identity_Score2.Text = judgementdetail.MATCH_SCORE_IDENTITY_NUMBER
                End If
            End If
            If judgementdetail.MATCH_SCORE IsNot Nothing Then
                If judgementdetail.MATCH_SCORE = 0 Then
                    Total_Score.Text = "-"
                    Total_Score2.Text = "-"
                Else
                    Total_Score.Text = judgementdetail.MATCH_SCORE
                    Total_Score2.Text = judgementdetail.MATCH_SCORE
                End If
            End If

            objAML_WATCHLIST_CLASS = AML_WATCHLIST_BLL.GetWatchlistClassByID(judgementdetail.FK_AML_WATCHLIST_ID)
            If objAML_WATCHLIST_CLASS IsNot Nothing Then
                With objAML_WATCHLIST_CLASS.objAML_WATCHLIST
                    WatchListID.Text = .PK_AML_WATCHLIST_ID
                    WatchListID2.Text = .PK_AML_WATCHLIST_ID
                    If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                        Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                        If objWatchlistCategory IsNot Nothing Then
                            WatchListCategory.Text = objWatchlistCategory.CATEGORY_NAME
                            WatchListCategory2.Text = objWatchlistCategory.CATEGORY_NAME
                        End If
                    End If
                    If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                        Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                        If objWatchlistType IsNot Nothing Then
                            WatchListType.Text = objWatchlistType.TYPE_NAME
                            WatchListType2.Text = objWatchlistType.TYPE_NAME
                        End If
                    End If
                    If .NAME IsNot Nothing Then
                        FullName.Text = .NAME
                        FullName2.Text = .NAME
                    End If
                    If .BIRTH_PLACE IsNot Nothing Then
                        PlaceofBirth.Text = .BIRTH_PLACE
                        PlaceofBirth2.Text = .BIRTH_PLACE
                    End If
                    If .DATE_OF_BIRTH IsNot Nothing Then
                        DateofBirth.Text = .DATE_OF_BIRTH.Value.Date.ToString("dd-MMM-yyyy")
                        DateofBirth2.Text = .DATE_OF_BIRTH.Value.Date.ToString("dd-MMM-yyyy")
                    End If
                    If .YOB IsNot Nothing Then
                        YearofBirth.Text = .YOB
                        YearofBirth2.Text = .YOB
                    End If
                    If .NATIONALITY IsNot Nothing Then
                        Nationality.Text = .NATIONALITY
                        Nationality2.Text = .NATIONALITY
                    End If
                    If .CUSTOM_REMARK_1 IsNot Nothing Then
                        Remark1.Text = .CUSTOM_REMARK_1
                        DisplayField14.Text = .CUSTOM_REMARK_1
                    End If
                    If .CUSTOM_REMARK_2 IsNot Nothing Then
                        Remark2.Text = .CUSTOM_REMARK_2
                        DisplayField15.Text = .CUSTOM_REMARK_2
                    End If
                    If .CUSTOM_REMARK_3 IsNot Nothing Then
                        Remark3.Text = .CUSTOM_REMARK_3
                        DisplayField16.Text = .CUSTOM_REMARK_3
                    End If
                    If .CUSTOM_REMARK_4 IsNot Nothing Then
                        Remark4.Text = .CUSTOM_REMARK_4
                        DisplayField17.Text = .CUSTOM_REMARK_4
                    End If
                    If .CUSTOM_REMARK_5 IsNot Nothing Then
                        Remark5.Text = .CUSTOM_REMARK_5
                        DisplayField17.Text = .CUSTOM_REMARK_5
                    End If
                End With
                Bind_AML_WATCHLIST_ALIAS()
                Bind_AML_WATCHLIST_ADDRESS()
                Bind_AML_WATCHLIST_IDENTITY()
            End If
        End If
    End Sub

    Protected Sub loadobjectJudgementAccountNoLawan(id As String)
        judgementdetail = listjudgementitemtransactionAccountNoLawan.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = id).FirstOrDefault
        If judgementdetail IsNot Nothing Then
            txt_MATCH_SCORE_NAME_SEARCH.Value = judgementdetail.NAME
            txt_MATCH_SCORE_NAME_SEARCH2.Value = judgementdetail.NAME
            If IsNothing(judgementdetail.DOB) OrElse CDate(judgementdetail.DOB) = DateTime.MinValue Then
                txt_MATCH_SCORE_DOB_SEARCH.Value = "-"
                txt_MATCH_SCORE_DOB_SEARCH2.Value = "-"
            Else
                txt_MATCH_SCORE_DOB_SEARCH.Value = Convert.ToDateTime(judgementdetail.DOB).ToString("dd-MMM-yyyy")
                txt_MATCH_SCORE_DOB_SEARCH2.Value = Convert.ToDateTime(judgementdetail.DOB).ToString("dd-MMM-yyyy")
            End If
            txt_MATCH_SCORE_Nationality_SEARCH.Value = IIf(String.IsNullOrEmpty(judgementdetail.NATIONALITY), "-", judgementdetail.NATIONALITY)
            txt_MATCH_SCORE_Nationality_SEARCH2.Value = IIf(String.IsNullOrEmpty(judgementdetail.NATIONALITY), "-", judgementdetail.NATIONALITY)
            txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH.Value = IIf(String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER), "-", judgementdetail.IDENTITY_NUMBER)
            txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH2.Value = IIf(String.IsNullOrEmpty(judgementdetail.IDENTITY_NUMBER), "-", judgementdetail.IDENTITY_NUMBER)

            'Screening Result
            txt_MATCH_SCORE_NAME_WATCHLIST.Value = judgementdetail.NAME_WATCHLIST
            txt_MATCH_SCORE_NAME_WATCHLIST2.Value = judgementdetail.NAME_WATCHLIST
            If IsNothing(judgementdetail.DOB) OrElse CDate(judgementdetail.DOB) = DateTime.MinValue Then
                'txt_MATCH_SCORE_DOB_WATCHLIST.Value = ""
                'txt_MATCH_SCORE_DOB_WATCHLIST2.Value = ""
                '' Edit 03-Aug-2022, karena error kalo ""
                txt_MATCH_SCORE_DOB_WATCHLIST.Value = DateTime.MinValue
                txt_MATCH_SCORE_DOB_WATCHLIST2.Value = DateTime.MinValue

            Else
                txt_MATCH_SCORE_DOB_WATCHLIST.Value = Convert.ToDateTime(judgementdetail.DOB_WATCHLIST).ToString("dd-MMM-yyyy")
                txt_MATCH_SCORE_DOB_WATCHLIST2.Value = Convert.ToDateTime(judgementdetail.DOB_WATCHLIST).ToString("dd-MMM-yyyy")
            End If
            txt_MATCH_SCORE_Nationality_WATCHLIST.Value = judgementdetail.NATIONALITY_WATCHLIST
            txt_MATCH_SCORE_Nationality_WATCHLIST2.Value = judgementdetail.NATIONALITY_WATCHLIST
            txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST.Value = judgementdetail.IDENTITY_NUMBER_WATCHLIST
            txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST2.Value = judgementdetail.IDENTITY_NUMBER_WATCHLIST

            '' Add 03-Aug-2022, copy dari yg Customer, karena di WIC blom di pasang codingnya
            Dim NAME_WEIGHT As Double
            Dim DOB_WEIGHT As Double
            Dim NATIONALITY_WEIGHT As Double
            Dim IDENTITY_WEIGHT As Double

            Dim objdt_SCREENING_PARAMETER As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select NAME_WEIGHT, DOB_WEIGHT,NATIONALITY_WEIGHT,IDENTITY_WEIGHT FROM AML_SCREENING_MATCH_PARAMETER", Nothing)
            If objdt_SCREENING_PARAMETER IsNot Nothing Then
                For Each row As DataRow In objdt_SCREENING_PARAMETER.Rows
                    NAME_WEIGHT = row.Item("NAME_WEIGHT")
                    DOB_WEIGHT = row.Item("DOB_WEIGHT")
                    NATIONALITY_WEIGHT = row.Item("NATIONALITY_WEIGHT")
                    IDENTITY_WEIGHT = row.Item("IDENTITY_WEIGHT")
                Next row
            End If

            'Screening Match Score
            Dim dblMatchScoreName As Double = CDbl(judgementdetail.MATCH_SCORE_NAME) * CDbl(NAME_WEIGHT / 100)
            Dim dblMatchScoreDOB As Double = CDbl(judgementdetail.MATCH_SCORE_DOB) * CDbl(DOB_WEIGHT / 100)
            Dim dblMatchScoreNationality As Double = CDbl(judgementdetail.MATCH_SCORE_NATIONALITY) * CDbl(NATIONALITY_WEIGHT / 100)
            Dim dblMatchScoreIdentity As Double = CDbl(judgementdetail.MATCH_SCORE_IDENTITY_NUMBER) * CDbl(IDENTITY_WEIGHT / 100)
            Dim dblMatchScoreTotal = dblMatchScoreName + dblMatchScoreDOB + dblMatchScoreNationality + dblMatchScoreIdentity

            Dim strMatchScoreName As String = NAME_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_NAME).ToString("#,##0.00") & " % = " & dblMatchScoreName.ToString("#,##0.00") & " %"
            Dim strMatchScoreDOB As String = DOB_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_DOB).ToString("#,##0.00") & " % = " & dblMatchScoreDOB.ToString("#,##0.00") & " %"
            Dim strMatchScoreNationality As String = NATIONALITY_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_NATIONALITY).ToString("#,##0.00") & " % = " & dblMatchScoreNationality.ToString("#,##0.00") & " %"
            Dim strMatchScoreIdentity As String = IDENTITY_WEIGHT.ToString() & " % x " & CDbl(judgementdetail.MATCH_SCORE_IDENTITY_NUMBER).ToString("#,##0.00") & " % = " & dblMatchScoreIdentity.ToString("#,##0.00") & " %"

            'Show the Formula so the user clear about the score
            Dim strDividend As String = dblMatchScoreName.ToString("#,##0.00")
            Dim strDivisor As String = NAME_WEIGHT.ToString()
            If Not CDate(txt_MATCH_SCORE_DOB_WATCHLIST.Value) = DateTime.MinValue Then
                strDividend = strDividend & " + " & dblMatchScoreDOB.ToString("#,##0.00")
                strDivisor = strDivisor & " + " & DOB_WEIGHT.ToString()
            End If
            If Not String.IsNullOrEmpty(txt_MATCH_SCORE_Nationality_SEARCH.Value) Then
                strDividend = strDividend & " + " & dblMatchScoreNationality.ToString("#,##0.00")
                strDivisor = strDivisor & " + " & NATIONALITY_WEIGHT.ToString()
            End If
            If Not String.IsNullOrEmpty(txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH.Value) Then
                strDividend = strDividend & " + " & dblMatchScoreIdentity.ToString("#,##0.00")
                strDivisor = strDivisor & " + " & IDENTITY_WEIGHT.ToString()
            End If
            strDividend = "( " & strDividend & " % )"
            strDivisor = "( " & strDivisor & " % )"

            txt_MATCH_SCORE_NAME.Value = strMatchScoreName
            txt_MATCH_SCORE_DOB.Value = IIf(CDate(txt_MATCH_SCORE_DOB_WATCHLIST.Value) = DateTime.MinValue, "-", strMatchScoreDOB)
            txt_MATCH_SCORE_Nationality.Value = IIf(String.IsNullOrEmpty(txt_MATCH_SCORE_Nationality_SEARCH.Value), "-", strMatchScoreNationality)
            txt_MATCH_SCORE_IDENTITY_NUMBER.Value = IIf(String.IsNullOrEmpty(txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH.Value), "-", strMatchScoreIdentity)
            'txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(e.ExtraParams(2).Value).ToString("#,##0.00 %")
            txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(judgementdetail.MATCH_SCORE).ToString("#,##0.00") & " %"

            txt_MATCH_SCORE_NAME2.Value = strMatchScoreName
            txt_MATCH_SCORE_DOB2.Value = IIf(CDate(txt_MATCH_SCORE_DOB_WATCHLIST2.Value) = DateTime.MinValue, "-", strMatchScoreDOB)
            txt_MATCH_SCORE_Nationality2.Value = IIf(String.IsNullOrEmpty(txt_MATCH_SCORE_Nationality_SEARCH2.Value), "-", strMatchScoreNationality)
            txt_MATCH_SCORE_IDENTITY_NUMBER2.Value = IIf(String.IsNullOrEmpty(txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH2.Value), "-", strMatchScoreIdentity)
            'txt_MATCH_SCORE_TOTAL.Value = strDividend & " / " & strDivisor & " = " & CDbl(e.ExtraParams(2).Value).ToString("#,##0.00 %")
            txt_MATCH_SCORE_TOTAL2.Value = strDividend & " / " & strDivisor & " = " & CDbl(judgementdetail.MATCH_SCORE).ToString("#,##0.00") & " %"
            '' End 03-Aug-2022

            indexofjudgementitems = listjudgementitemtransactionAccountNoLawan.IndexOf(judgementdetail)
            If judgementdetail.JUDGEMENT_ISMATCH IsNot Nothing Then
                If judgementdetail.JUDGEMENT_ISMATCH Then
                    RBJudgementMatch.Checked = True

                Else
                    RBJudgementNotMatch.Checked = True

                End If
            End If
            If judgementdetail.JUDGEMENT_COMMENT IsNot Nothing Then
                JudgementComment.Text = judgementdetail.JUDGEMENT_COMMENT

            End If
            If judgementdetail.MATCH_SCORE_NAME IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_NAME = 0 Then
                    Name_Score.Text = "-"
                    Name_Score2.Text = "-"
                Else
                    Name_Score.Text = judgementdetail.MATCH_SCORE_NAME
                    Name_Score2.Text = judgementdetail.MATCH_SCORE_NAME
                End If
            End If
            If judgementdetail.MATCH_SCORE_DOB IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_DOB = 0 Then
                    DoB_Score.Text = "-"
                    DoB_Score2.Text = "-"
                Else
                    DoB_Score.Text = judgementdetail.MATCH_SCORE_DOB
                    DoB_Score2.Text = judgementdetail.MATCH_SCORE_DOB
                End If
            End If
            If judgementdetail.MATCH_SCORE_NATIONALITY IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_NATIONALITY = 0 Then
                    Nationality_Score.Text = "-"
                    Nationality_Score2.Text = "-"
                Else
                    Nationality_Score.Text = judgementdetail.MATCH_SCORE_NATIONALITY
                    Nationality_Score2.Text = judgementdetail.MATCH_SCORE_NATIONALITY
                End If
            End If
            If judgementdetail.MATCH_SCORE_IDENTITY_NUMBER IsNot Nothing Then
                If judgementdetail.MATCH_SCORE_IDENTITY_NUMBER = 0 Then
                    Identity_Score.Text = "-"
                    Identity_Score2.Text = "-"
                Else
                    Identity_Score.Text = judgementdetail.MATCH_SCORE_IDENTITY_NUMBER
                    Identity_Score2.Text = judgementdetail.MATCH_SCORE_IDENTITY_NUMBER
                End If
            End If
            If judgementdetail.MATCH_SCORE IsNot Nothing Then
                If judgementdetail.MATCH_SCORE = 0 Then
                    Total_Score.Text = "-"
                    Total_Score2.Text = "-"
                Else
                    Total_Score.Text = judgementdetail.MATCH_SCORE
                    Total_Score2.Text = judgementdetail.MATCH_SCORE
                End If
            End If

            objAML_WATCHLIST_CLASS = AML_WATCHLIST_BLL.GetWatchlistClassByID(judgementdetail.FK_AML_WATCHLIST_ID)
            If objAML_WATCHLIST_CLASS IsNot Nothing Then
                With objAML_WATCHLIST_CLASS.objAML_WATCHLIST
                    WatchListID.Text = .PK_AML_WATCHLIST_ID
                    WatchListID2.Text = .PK_AML_WATCHLIST_ID
                    If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                        Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                        If objWatchlistCategory IsNot Nothing Then
                            WatchListCategory.Text = objWatchlistCategory.CATEGORY_NAME
                            WatchListCategory2.Text = objWatchlistCategory.CATEGORY_NAME
                        End If
                    End If
                    If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                        Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                        If objWatchlistType IsNot Nothing Then
                            WatchListType.Text = objWatchlistType.TYPE_NAME
                            WatchListType2.Text = objWatchlistType.TYPE_NAME
                        End If
                    End If
                    If .NAME IsNot Nothing Then
                        FullName.Text = .NAME
                        FullName2.Text = .NAME
                    End If
                    If .BIRTH_PLACE IsNot Nothing Then
                        PlaceofBirth.Text = .BIRTH_PLACE
                        PlaceofBirth2.Text = .BIRTH_PLACE
                    End If
                    If .DATE_OF_BIRTH IsNot Nothing Then
                        DateofBirth.Text = .DATE_OF_BIRTH.Value.Date.ToString("dd-MMM-yyyy")
                        DateofBirth2.Text = .DATE_OF_BIRTH.Value.Date.ToString("dd-MMM-yyyy")
                    End If
                    If .YOB IsNot Nothing Then
                        YearofBirth.Text = .YOB
                        YearofBirth2.Text = .YOB
                    End If
                    If .NATIONALITY IsNot Nothing Then
                        Nationality.Text = .NATIONALITY
                        Nationality2.Text = .NATIONALITY
                    End If
                    If .CUSTOM_REMARK_1 IsNot Nothing Then
                        Remark1.Text = .CUSTOM_REMARK_1
                        DisplayField14.Text = .CUSTOM_REMARK_1
                    End If
                    If .CUSTOM_REMARK_2 IsNot Nothing Then
                        Remark2.Text = .CUSTOM_REMARK_2
                        DisplayField15.Text = .CUSTOM_REMARK_2
                    End If
                    If .CUSTOM_REMARK_3 IsNot Nothing Then
                        Remark3.Text = .CUSTOM_REMARK_3
                        DisplayField16.Text = .CUSTOM_REMARK_3
                    End If
                    If .CUSTOM_REMARK_4 IsNot Nothing Then
                        Remark4.Text = .CUSTOM_REMARK_4
                        DisplayField17.Text = .CUSTOM_REMARK_4
                    End If
                    If .CUSTOM_REMARK_5 IsNot Nothing Then
                        Remark5.Text = .CUSTOM_REMARK_5
                        DisplayField17.Text = .CUSTOM_REMARK_5
                    End If
                End With
                Bind_AML_WATCHLIST_ALIAS()
                Bind_AML_WATCHLIST_ADDRESS()
                Bind_AML_WATCHLIST_IDENTITY()
            End If
        End If
    End Sub

    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            judgementClass = New AMLJudgementClass
            With judgementClass
                If screeningtransactionCounterParty IsNot Nothing Then
                    .AMLJudgementHeaderTransaction = screeningtransaction
                    .AMLJudgementHeaderTransactionCounterParty = screeningtransactionCounterParty
                Else
                    .AMLJudgementHeaderTransaction = screeningtransaction
                End If

                .AMLListResultTransaction = listjudgementitemtransactionchange
            End With
            checkerjudgeitems = New AML_SCREENING_RESULT_TRANSACTION_DETAIL
            checkerjudgeitems = listjudgementitemtransaction.Where(Function(x) x.JUDGEMENT_ISMATCH Is Nothing).FirstOrDefault
            If listjudgementitemtransactionchange.Count = 0 Then
                Throw New ApplicationException("No Data Has Changed, Please Change Some Data Before Saving or Click the Cancel Button to Cancel the Process")
            ElseIf checkerjudgeitems IsNot Nothing Then
                Throw New ApplicationException("There are Some Data which have not been Judged, Please Judge All Data Before Saving")
            End If
            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not ObjModule.IsUseApproval Then
                'tanpa approval
                AMLJudgementBLL.SaveJudgementTanpaApprovalTransaction(judgementClass, ObjModule)
                PanelInfo.Hide()
                Panelconfirmation.Show()
                LblConfirmation.Text = "Data Saved into Database"
            Else
                'with approval
                AMLJudgementBLL.SaveJudgementApprovalTransaction(judgementClass, ObjModule)
                PanelInfo.Hide()
                Panelconfirmation.Show()
                LblConfirmation.Text = "Data Saved into Pending Approval"
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub loadjudgementitemchange(items As AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        judgementdetailchange = listjudgementitemtransactionchange.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = items.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID).FirstOrDefault
        If judgementdetailchange IsNot Nothing Then
            indexofjudgementitemschange = listjudgementitemtransactionchange.IndexOf(judgementdetailchange)
            listjudgementitemtransactionchange(indexofjudgementitemschange) = items
        Else
            listjudgementitemtransactionchange.Add(items)
        End If
    End Sub

    Protected Sub rgjudgement_click(sender As Object, e As DirectEventArgs)
        Try
            Dim objradio As Ext.Net.Radio = CType(sender, Ext.Net.Radio)
            If JudgementComment.Text Is Nothing Or JudgementComment.Text = "" Or JudgementComment.Text = "This Data is Judged Match" Or JudgementComment.Text = "This Data is Judged Not Match" Then
                If objradio.InputValue = "1" And objradio.Checked Then
                    JudgementComment.Text = "This Data is Judged Match"
                ElseIf objradio.InputValue = "0" And objradio.Checked Then
                    JudgementComment.Text = "This Data is Judged Not Match"
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnJudgementSave_Click(sender As Object, e As DirectEventArgs)
        Try
            If RBJudgementMatch.Checked Or RBJudgementNotMatch.Checked Then
                If judgementdetail IsNot Nothing Then
                    If judgementdetail.JUDGEMENT_ISMATCH <> RBJudgementMatch.Checked Or judgementdetail.JUDGEMENT_COMMENT <> JudgementComment.Text Or judgementdetail.JUDGEMENT_ISMATCH Is Nothing Then
                        If RBJudgementMatch.Checked Then
                            judgementdetail.JUDGEMENT_ISMATCH = True
                        Else
                            judgementdetail.JUDGEMENT_ISMATCH = False
                        End If
                        If JudgementComment.Text IsNot Nothing Then
                            judgementdetail.JUDGEMENT_COMMENT = JudgementComment.Text
                        End If
                        If Common.SessionCurrentUser.UserName IsNot Nothing Then
                            judgementdetail.JUDGEMENT_BY = Common.SessionCurrentUser.UserID
                        End If
                        judgementdetail.JUDGEMENT_DATE = Now()
                        If judgementdetail.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID <> Nothing And listjudgementitemtransaction IsNot Nothing Then
                            loadjudgementitemchange(judgementdetail)
                            listjudgementitemtransaction(indexofjudgementitems) = judgementdetail
                            BindJudgement(judgement_Item, listjudgementitemtransaction)
                            BindJudgement(Store2, listjudgementitemtransactionCounteryParty)
                        Else
                            Throw New ApplicationException("Something Wrong While Saving Data")
                        End If
                    Else
                        Throw New ApplicationException("No Data Has Changed, Please Change Some Data Before Saving or Click the Cancel Button to Cancel the Process")
                    End If
                End If
            Else
                Throw New ApplicationException("Must Conduct Judgment Before Saving Data")
            End If
            WindowJudge.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnJudgementCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowJudge.Hidden = True
            WindowJudgeDetail.Hidden = True
            ClearOBJDetail()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnExportAll_Click(sender As Object, e As EventArgs)

    End Sub
    Protected Sub DetailCIF_Click(sender As Object, e As EventArgs)
        WindowDetailCIF.Hidden = False
    End Sub

    Protected Sub DetailCIFCancelButton(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailCIF.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class
