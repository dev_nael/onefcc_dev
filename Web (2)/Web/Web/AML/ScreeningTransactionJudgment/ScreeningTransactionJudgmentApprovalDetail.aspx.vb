﻿Imports Ext
Imports NawaBLL
Imports NawaDevBLL
Imports System.Data
Imports System.Data.SqlClient
Imports Elmah
Imports NawaDAL
Imports NawaDevDAL
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevBLL.GlobalReportFunctionBLL
Imports System.Text
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.IO
Imports System.Globalization

Partial Class AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail
    Inherits Parent

#Region "Session"
    Public Property ObjApproval As NawaDAL.ModuleApproval
        Get
            Return Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.ObjApproval")
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.ObjApproval") = value
        End Set
    End Property
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.ObjModule") = value
        End Set
    End Property
    Public Property IDModule() As String
        Get
            Return Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.IDModule")
        End Get
        Set(ByVal value As String)
            Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.IDModule") = value
        End Set
    End Property

    Public Property IDUnik As Long
        Get
            Return Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.IDUnik") = value
        End Set
    End Property
    Public Property AmJjudgementClassNew As NawaDevBLL.AMLJudgementClass
        Get
            Return Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.AmJjudgementClassNew")
        End Get
        Set(ByVal value As NawaDevBLL.AMLJudgementClass)
            Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.AmJjudgementClassNew") = value
        End Set
    End Property

    Public Property AmlJudgementClassBefore As NawaDevBLL.AMLJudgementClass
        Get
            Return Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.AmlJudgementClassBefore")
        End Get
        Set(ByVal value As NawaDevBLL.AMLJudgementClass)
            Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.AmlJudgementClassBefore") = value
        End Set
    End Property

    Public Property objAML_WATCHLIST_CLASS() As NawaDevBLL.AML_WATCHLIST_CLASS
        Get
            Return Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.objAML_WATCHLIST_CLASS")
        End Get
        Set(ByVal value As NawaDevBLL.AML_WATCHLIST_CLASS)
            Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.objAML_WATCHLIST_CLASS") = value
        End Set
    End Property
    Public Property judgementdetailtransaction As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL
        Get
            Return Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.judgementdetailtransaction")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
            Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.judgementdetailtransaction") = value
        End Set
    End Property

    Public Property judgementheadertransaction As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION
        Get
            Return Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.judgementheadertransaction")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION)
            Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.judgementheadertransaction") = value
        End Set
    End Property
    Public Property judgementrequestforheader As NawaDevDAL.AML_SCREENING_REQUEST
        Get
            Return Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.judgementrequestforheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_SCREENING_REQUEST)
            Session("AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentApprovalDetail.judgementrequestforheader") = value
        End Set
    End Property
    Public Property dataamlcustomer As AML_CUSTOMER
        Get
            Return Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.dataamlcustomer")
        End Get
        Set(ByVal value As AML_CUSTOMER)
            Session("AML_AMLScreeningJudgment_AMLJudgmentEdit.dataamlcustomer") = value
        End Set
    End Property
#End Region
    Sub ClearSession()
        ObjApproval = Nothing
        AmJjudgementClassNew = New AMLJudgementClass
        AmlJudgementClassBefore = New AMLJudgementClass
        listjudgementitemtransactionAccountNoLawan = New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        ClearOBJDetail()
    End Sub

    Private Sub ClearOBJDetail()
        objAML_WATCHLIST_CLASS = New AML_WATCHLIST_CLASS
        judgementdetailtransaction = New AML_SCREENING_RESULT_TRANSACTION_DETAIL
        judgementheadertransaction = New AML_SCREENING_RESULT_TRANSACTION

    End Sub
    Public Property listjudgementitemtransactionAccountNoLawan As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        Get
            Return Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransactionAccountNoLawan")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL))
            Session("AML_ScreeningResult_ScreeningTransactionResultDetail.listjudgementitemtransactionAccountNoLawan") = value
        End Set
    End Property

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()
                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If

                IDModule = Request.Params("ModuleID")
                Dim intModuleID As Integer = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = ModuleBLL.GetModuleByModuleID(intModuleID)
                If ObjModule IsNot Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Approval) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If
                PanelInfo.Title = ObjModule.ModuleLabel & " - Approval"

                LoadModuleApproval()
                If Not String.IsNullOrEmpty(ObjApproval.ModuleFieldBefore) Then
                    LoadDataBefore()
                End If

                If Not String.IsNullOrEmpty(ObjApproval.ModuleField) Then
                    LoadDataAfter()
                End If
                LoadJudgementHeader()
            End If
        Catch ex As Exception
            BtnSave.Visible = False
            BtnReject.Visible = False
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadModuleApproval()
        Try
            ObjApproval = NawaBLL.ModuleApprovalBLL.GetModuleApprovalByID(IDUnik)
            If Not ObjApproval Is Nothing Then
                If ObjApproval.PK_ModuleApproval_ID <> Nothing Then
                    lblModuleKey.Value = ObjApproval.PK_ModuleApproval_ID
                End If
                If ObjApproval.ModuleName IsNot Nothing Then
                    Dim objModuleToApprove = NawaBLL.ModuleBLL.GetModuleByModuleName(ObjApproval.ModuleName)
                    If objModuleToApprove IsNot Nothing Then
                        txt_ModuleLabel.Value = objModuleToApprove.ModuleLabel
                    End If
                End If
                If ObjApproval.ModuleKey IsNot Nothing Then
                    txt_ModuleKey.Text = ObjApproval.ModuleKey
                End If
                If ObjApproval.PK_ModuleAction_ID IsNot Nothing Then
                    Dim temp = NawaBLL.ModuleBLL.GetModuleActionNamebyID(ObjApproval.PK_ModuleAction_ID)
                    If temp IsNot Nothing Then
                        lblAction.Text = temp
                    End If
                End If
                If ObjApproval.CreatedBy IsNot Nothing Then
                    Dim objUser = NawaBLL.MUserBLL.GetMuserbyUSerId(ObjApproval.CreatedBy)
                    If objUser IsNot Nothing Then
                        LblCreatedBy.Text = ObjApproval.CreatedBy & " - " & objUser.UserName
                    Else
                        LblCreatedBy.Text = ObjApproval.CreatedBy
                    End If
                End If
                If ObjApproval.CreatedDate IsNot Nothing And ObjApproval.CreatedDate <> Date.MinValue Then
                    lblCreatedDate.Text = ObjApproval.CreatedDate.Value.ToString("dd-MMM-yyyy")
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub loadobjectJudgement(id As String, oldornew As String)
        If oldornew = "old" Then
            judgementdetailtransaction = AmlJudgementClassBefore.AMLListResultTransaction.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = id).FirstOrDefault
        Else
            judgementdetailtransaction = AmJjudgementClassNew.AMLListResultTransaction.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = id).FirstOrDefault
        End If
        If judgementdetailtransaction IsNot Nothing Then
            If judgementdetailtransaction.JUDGEMENT_ISMATCH IsNot Nothing Then
                If judgementdetailtransaction.JUDGEMENT_ISMATCH Then
                    RBJudgementMatch.Checked = True
                Else
                    RBJudgementNotMatch.Checked = True
                End If
            End If
            Dim transaksi = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementTransaction(judgementheader.TRANSACTION_NUMBER)
            If transaksi IsNot Nothing Then
                If transaksi.ACCOUNT_NAME_LAWAN IsNot Nothing Then
                    DisplayFieldNamaNomorUnik.Text = transaksi.ACCOUNT_NAME_LAWAN
                    Dim screeningaccountlawan As List(Of AML_SCREENING_RESULT_TRANSACTION) = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementTransactionListByTransactionNumber(transaksi.TRANSACTION_NUMBER)
                    If screeningaccountlawan IsNot Nothing Then
                        For Each item In screeningaccountlawan
                            Dim resultaccount = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementTransactionDetailByFK(item.PK_AML_SCREENING_RESULT_TRANSACTION_ID)
                            If resultaccount IsNot Nothing Then
                                listjudgementitemtransactionAccountNoLawan.Add(resultaccount)
                            End If
                        Next
                        If listjudgementitemtransactionAccountNoLawan IsNot Nothing Then
                            BindJudgement(StoreAccountNoLawan, listjudgementitemtransactionAccountNoLawan)
                        End If

                    End If
                End If
            End If

            If judgementdetailtransaction.JUDGEMENT_COMMENT IsNot Nothing Then
                    JudgementComment.Text = judgementdetailtransaction.JUDGEMENT_COMMENT
                End If
                If judgementdetailtransaction.MATCH_SCORE_NAME IsNot Nothing Then
                    Name_Score.Text = judgementdetailtransaction.MATCH_SCORE_NAME
                End If
                If judgementdetailtransaction.MATCH_SCORE_DOB IsNot Nothing Then
                    DoB_Score.Text = judgementdetailtransaction.MATCH_SCORE_DOB
                End If
                If judgementdetailtransaction.MATCH_SCORE_NATIONALITY IsNot Nothing Then
                    Nationality_Score.Text = judgementdetailtransaction.MATCH_SCORE_NATIONALITY
                End If
                If judgementdetailtransaction.MATCH_SCORE_IDENTITY_NUMBER IsNot Nothing Then
                    Identity_Score.Text = judgementdetailtransaction.MATCH_SCORE_IDENTITY_NUMBER
                End If
                If judgementdetailtransaction.MATCH_SCORE IsNot Nothing Then
                    Total_Score.Text = judgementdetailtransaction.MATCH_SCORE
                End If

                objAML_WATCHLIST_CLASS = AML_WATCHLIST_BLL.GetWatchlistClassByID(judgementdetailtransaction.FK_AML_WATCHLIST_ID)
                If objAML_WATCHLIST_CLASS IsNot Nothing Then
                    With objAML_WATCHLIST_CLASS.objAML_WATCHLIST
                        WatchListID.Text = .PK_AML_WATCHLIST_ID
                        If Not IsNothing(.FK_AML_WATCHLIST_CATEGORY_ID) Then
                            Dim objWatchlistCategory = AML_WATCHLIST_BLL.GetWatchlistCategoryByID(.FK_AML_WATCHLIST_CATEGORY_ID)
                            If objWatchlistCategory IsNot Nothing Then
                                WatchListCategory.Text = objWatchlistCategory.CATEGORY_NAME
                            End If
                        End If
                        If Not IsNothing(.FK_AML_WATCHLIST_TYPE_ID) Then
                            Dim objWatchlistType = AML_WATCHLIST_BLL.GetWatchlistTypeByID(.FK_AML_WATCHLIST_TYPE_ID)
                            If objWatchlistType IsNot Nothing Then
                                WatchListType.Text = objWatchlistType.TYPE_NAME
                            End If
                        End If
                        If .NAME IsNot Nothing Then
                            FullName.Text = .NAME
                        End If
                        If .BIRTH_PLACE IsNot Nothing Then
                            PlaceofBirth.Text = .BIRTH_PLACE
                        End If
                        If .DATE_OF_BIRTH IsNot Nothing Then
                            DateofBirth.Text = CDate(.DATE_OF_BIRTH).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture)
                            'DateofBirth.Text = .DATE_OF_BIRTH
                        End If
                        If .YOB IsNot Nothing Then
                            YearofBirth.Text = .YOB
                        End If
                        If .NATIONALITY IsNot Nothing Then
                            Nationality.Text = .NATIONALITY
                        End If
                        If .CUSTOM_REMARK_1 IsNot Nothing Then
                            Remark1.Text = .CUSTOM_REMARK_1
                        End If
                        If .CUSTOM_REMARK_2 IsNot Nothing Then
                            Remark2.Text = .CUSTOM_REMARK_2
                        End If
                        If .CUSTOM_REMARK_3 IsNot Nothing Then
                            Remark3.Text = .CUSTOM_REMARK_3
                        End If
                        If .CUSTOM_REMARK_4 IsNot Nothing Then
                            Remark4.Text = .CUSTOM_REMARK_4
                        End If
                        If .CUSTOM_REMARK_5 IsNot Nothing Then
                            Remark5.Text = .CUSTOM_REMARK_5
                        End If
                    End With
                    Bind_AML_WATCHLIST_ALIAS()
                    Bind_AML_WATCHLIST_ADDRESS()
                    Bind_AML_WATCHLIST_IDENTITY()
                End If
            End If
    End Sub

    Protected Sub LoadDataAfter()
        Try
            If Not String.IsNullOrEmpty(ObjApproval.ModuleField) Then
                AmJjudgementClassNew = NawaBLL.Common.Deserialize(ObjApproval.ModuleField, GetType(NawaDevBLL.AMLJudgementClass))
            End If
            If AmJjudgementClassNew.AMLListResultTransaction IsNot Nothing Then
                Dim counterpartylist As New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
                If AmJjudgementClassNew.AMLJudgementHeaderTransactionCounterParty IsNot Nothing Then
                    For Each itemcounter In AmJjudgementClassNew.AMLListResultTransaction.ToList
                        For Each item In AmJjudgementClassNew.AMLJudgementHeaderTransactionCounterParty
                            If itemcounter.FK_AML_SCREENING_RESULT_TRANSACTION_ID = item.PK_AML_SCREENING_RESULT_TRANSACTION_ID Then
                                counterpartylist.Add(itemcounter)
                            End If
                        Next

                    Next
                End If
                BindJudgement(judgement_CounterPary_Item_New, counterpartylist)
                counterpartylist = Nothing
                Dim rekeninglist As New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
                If AmJjudgementClassNew.AMLJudgementHeaderTransaction IsNot Nothing Then
                    For Each itemrekening In AmJjudgementClassNew.AMLListResultTransaction.ToList
                        For Each item In AmJjudgementClassNew.AMLJudgementHeaderTransaction
                            If itemrekening.FK_AML_SCREENING_RESULT_TRANSACTION_ID = item.PK_AML_SCREENING_RESULT_TRANSACTION_ID Then
                                rekeninglist.Add(itemrekening)
                            End If
                        Next

                    Next

                End If
                BindJudgement(judgement_Item_New, rekeninglist)
                rekeninglist = Nothing
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadDataBefore()
        Try
            If Not String.IsNullOrEmpty(ObjApproval.ModuleFieldBefore) Then
                AmlJudgementClassBefore = NawaBLL.Common.Deserialize(ObjApproval.ModuleFieldBefore, GetType(NawaDevBLL.AMLJudgementClass))
            End If
            If AmlJudgementClassBefore.AMLListResultTransaction IsNot Nothing Then
                Dim counterpartylist As New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
                If AmlJudgementClassBefore.AMLJudgementHeaderTransactionCounterParty IsNot Nothing Then
                    For Each itemcounter In AmlJudgementClassBefore.AMLListResultTransaction.ToList
                        For Each item In AmlJudgementClassBefore.AMLJudgementHeaderTransactionCounterParty
                            If itemcounter.FK_AML_SCREENING_RESULT_TRANSACTION_ID = item.PK_AML_SCREENING_RESULT_TRANSACTION_ID Then
                                counterpartylist.Add(itemcounter)
                            End If
                        Next

                    Next
                End If
                BindJudgement(judgement_CounterPary_Item_Old, counterpartylist)
                counterpartylist = Nothing
                Dim rekeninglist As New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
                If AmlJudgementClassBefore.AMLJudgementHeaderTransaction IsNot Nothing Then
                    For Each itemrekening In AmlJudgementClassBefore.AMLListResultTransaction.ToList
                        For Each item In AmlJudgementClassBefore.AMLJudgementHeaderTransaction
                            If itemrekening.FK_AML_SCREENING_RESULT_TRANSACTION_ID = item.PK_AML_SCREENING_RESULT_TRANSACTION_ID Then
                                rekeninglist.Add(itemrekening)
                            End If
                        Next

                    Next

                End If
                BindJudgement(judgement_Item_Old, rekeninglist)
                rekeninglist = Nothing
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Public Property judgementheader As NawaDevDAL.AML_TRANSACTION
        Get
            Return Session("AML_TransactionResultDetail.judgementheader")
        End Get
        Set(ByVal value As NawaDevDAL.AML_TRANSACTION)
            Session("AML_TransactionResultDetail.judgementheader") = value
        End Set
    End Property

    Public Property screeningtransaction As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION)
        Get
            Return Session("AML_Screening_TransactionResultDetail.judgementheaderApproval")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION))
            Session("AML_Screening_TransactionResultDetail.judgementheaderApproval") = value
        End Set
    End Property

    Public Property screeningtransactionCounterParty As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION)
        Get
            Return Session("AML_Screening_TransactionResultDetailCounterParty.judgementheaderApproval")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION))
            Session("AML_Screening_TransactionResultDetailCounterParty.judgementheaderApproval") = value
        End Set
    End Property

    Protected Sub LoadJudgementHeader()
        Try

            If AmJjudgementClassNew.AMLJudgementHeader IsNot Nothing Then
                For Each item In AmJjudgementClassNew.AMLJudgementHeaderTransaction
                    judgementheadertransaction = item
                Next

            ElseIf AmlJudgementClassBefore.AMLJudgementHeader IsNot Nothing Then
                For Each item In AmlJudgementClassBefore.AMLJudgementHeaderTransaction
                    judgementheadertransaction = item
                Next

            End If

            If judgementheadertransaction IsNot Nothing Then
                judgementrequestforheader = AMLJudgementBLL.GetRequestForHeader(judgementheadertransaction.FK_AML_SCREENING_REQUEST_ID)
                If judgementrequestforheader IsNot Nothing Then
                    'If judgementrequestforheader.REQUEST_ID IsNot Nothing Then
                    '    display_Request_ID.Text = judgementrequestforheader.REQUEST_ID
                    'End If
                    'If judgementrequestforheader.SOURCE IsNot Nothing Then
                    '    display_Request_Source.Text = judgementrequestforheader.SOURCE
                    'End If
                    'If judgementrequestforheader.SERVICE IsNot Nothing Then
                    '    display_Request_Service.Text = judgementrequestforheader.SERVICE
                    'End If
                    'If judgementrequestforheader.OPERATION IsNot Nothing Then
                    '    display_Request_Operation.Text = judgementrequestforheader.OPERATION

                    'End If
                    'belum ada mapping
                    judgementheader = AMLJudgementBLL.GetAMLJudgementTransaction(judgementheadertransaction.TRANSACTION_NUMBER)
                    Dim transaksi = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementTransaction(judgementheadertransaction.TRANSACTION_NUMBER)
                    If transaksi IsNot Nothing Then
                        screeningtransaction = AMLJudgementBLL.GetAMLJudgementTransactionListyCIFNOLAWAN(transaksi.TRANSACTION_NUMBER, transaksi.CIFNO)
                        Dim Customer As AML_CUSTOMER = NawaDevBLL.AMLReportRiskRating.GetCustomerBYCIFNO(transaksi.CIFNO)

                        If judgementheader.CIFNO IsNot Nothing Then
                            display_Request_CIF.Text = judgementheader.CIFNO
                        End If

                        If judgementheader.ACCOUNT_NO IsNot Nothing Then
                            DisplayFieldAccountNo.Value = judgementheader.ACCOUNT_NO
                        End If

                        If judgementrequestforheader.NAME IsNot Nothing Then
                            display_Request_Name.Text = judgementrequestforheader.NAME
                        End If
                        If judgementrequestforheader.DOB IsNot Nothing Then
                            display_Request_DOB.Text = judgementrequestforheader.DOB.Value.Date.ToString("dd-MMM-yyyy")
                        End If
                        If judgementrequestforheader.NATIONALITY IsNot Nothing Then
                            display_Request_Nationality.Text = judgementrequestforheader.NATIONALITY
                        End If
                        If judgementrequestforheader.IDENTITY_NUMBER IsNot Nothing Then
                            display_Request_Identity_Number.Text = judgementrequestforheader.IDENTITY_NUMBER
                        End If
                        If judgementheader.FK_DebitOrCredit_Code IsNot Nothing Then
                            If judgementheader.FK_DebitOrCredit_Code = "D" Then
                                DisplayFieldDebitorCredit.Text = "Debit"
                            ElseIf judgementheader.FK_DebitOrCredit_Code = "C" Then
                                DisplayFieldDebitorCredit.Text = "Credit"
                            Else
                                DisplayFieldDebitorCredit.Text = Nothing
                            End If
                        End If
                        If judgementheader.FK_AML_TRANSACTION_CODE IsNot Nothing Then
                            DisplayFieldTransactionCode.Text = judgementheader.FK_AML_TRANSACTION_CODE
                        End If
                        If judgementheader.TRANSACTION_DATE IsNot Nothing Then
                            DisplayFieldTransactionDate.Text = judgementheader.TRANSACTION_DATE.Value.Date.ToString("dd-MMM-yyyy")
                        End If
                        If judgementheader.TRANSACTION_NUMBER IsNot Nothing Then
                            DisplayFieldTransactionNumber.Text = judgementheader.TRANSACTION_NUMBER
                        End If



                        Dim transresultrekening = AMLJudgementBLL.GetAMLJudgementTransactionListyCIFNOLAWAN(transaksi.TRANSACTION_NUMBER, transaksi.CIFNO)

                        If transaksi.ACCOUNT_NAME_LAWAN IsNot Nothing Then
                            DisplayFieldNamaNomorUnik.Text = transaksi.ACCOUNT_NAME_LAWAN
                            Dim screeningaccountlawan As List(Of AML_SCREENING_RESULT_TRANSACTION) = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementTransactionListByTransactionNumber(transaksi.TRANSACTION_NUMBER)
                            If screeningaccountlawan IsNot Nothing Then
                                For Each item In screeningaccountlawan
                                    Dim resultaccount = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementTransactionDetailByFK(item.PK_AML_SCREENING_RESULT_TRANSACTION_ID)
                                    If resultaccount IsNot Nothing Then
                                        listjudgementitemtransactionAccountNoLawan.Add(resultaccount)
                                    End If
                                Next
                                If listjudgementitemtransactionAccountNoLawan IsNot Nothing Then
                                    BindJudgement(StoreAccountNoLawan, listjudgementitemtransactionAccountNoLawan)
                                End If

                            End If
                        End If






                        If transaksi.ACCOUNT_NO IsNot Nothing Then
                            DisplayFieldAccountNo.Text = transaksi.ACCOUNT_NO
                        End If
                        If transaksi.TRANSACTION_AMOUNT IsNot Nothing Then
                            DisplayFieldNominal.Text = transaksi.TRANSACTION_AMOUNT
                        End If

                        If transaksi.FK_AML_BRANCH_CODE IsNot Nothing Then
                            DisplayFieldBranchCode.Text = transaksi.FK_AML_BRANCH_CODE
                        End If

                        If transaksi.FK_AML_BRANCH_CODE IsNot Nothing Then
                            Dim BranchName As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT branch_name FROM aml_branch WHERE FK_AML_BRANCH_CODE= '" & transaksi.FK_AML_BRANCH_CODE & "'", Nothing)
                            DisplayFieldBranchName.Text = BranchName
                        End If

                        If transaksi.FK_AML_BRANCH_CODE IsNot Nothing Then
                            Dim RegionCode As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT REGION_CODE FROM aml_branch WHERE FK_AML_BRANCH_CODE= '" & transaksi.FK_AML_BRANCH_CODE & "'", Nothing)
                            DisplayFieldRegionCode.Text = RegionCode
                        End If

                        If transaksi.FK_AML_CURRENCY_CODE IsNot Nothing Then
                            Dim currency = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementCurrency(transaksi.FK_AML_CURRENCY_CODE)
                            If currency IsNot Nothing Then
                                If currency.CURRENCY_NAME IsNot Nothing Then
                                    DisplayFieldCurrency.Text = currency.CURRENCY_NAME
                                End If
                            End If

                        End If

                        If transaksi.CIFNO_LAWAN IsNot Nothing And transaksi.WICNo_LAWAN Is Nothing Then
                            DisplayFieldCifNoCounterParty.Hidden = False
                            DisplayFieldAccountNoCounterParty.Hidden = False
                            DisplayFieldCustomerNameCounterParty.Hidden = False
                            DisplayFieldDOBCounterParty.Hidden = False
                            DisplayFieldNationalityCounterParty.Hidden = False
                            DisplayFieldIdentityNumCounterParty.Hidden = False
                            DisplayFieldCifNoCounterParty.Text = transaksi.CIFNO_LAWAN
                            Dim customers As AML_CUSTOMER = NawaDevBLL.AMLReportRiskRating.GetCustomerBYCIFNO(transaksi.CIFNO_LAWAN)
                            If transaksi.ACCOUNT_NO_LAWAN IsNot Nothing Then
                                DisplayFieldAccountNoCounterParty.Text = transaksi.ACCOUNT_NO_LAWAN
                            End If
                            If customers IsNot Nothing Then
                                If customers.CUSTOMERNAME IsNot Nothing Then
                                    DisplayFieldCustomerNameCounterParty.Text = customers.CUSTOMERNAME
                                End If
                                If customers.FK_AML_CITIZENSHIP_CODE IsNot Nothing Then
                                    Dim country As AML_COUNTRY = NawaDevBLL.AMLReportRiskRating.GetCountryByFK(customers.FK_AML_CITIZENSHIP_CODE)
                                    If country IsNot Nothing Then
                                        DisplayFieldNationalityCounterParty.Text = country.AML_COUNTRY_Name
                                    End If
                                End If
                                If customers.DATEOFBIRTH IsNot Nothing Then
                                    DisplayFieldDOBCounterParty.Text = customers.DATEOFBIRTH.Value.Date.ToString("dd-MMM-yyyy")
                                End If
                                Dim identity As AML_CUSTOMER_IDENTITY = NawaDevBLL.AMLReportRiskRating.GetIdentityNumberByCIFNo(customers.CIFNo)
                                If identity IsNot Nothing Then
                                    DisplayFieldIdentityNumberCounductor.Text = identity.CUSTOMER_IDENTITY_NUMBER
                                End If
                            End If
                            Dim transresult = AMLJudgementBLL.GetAMLJudgementTransactionListyCIFNOLAWAN(transaksi.TRANSACTION_NUMBER, transaksi.CIFNO_LAWAN)


                        ElseIf transaksi.CIFNO_LAWAN Is Nothing And transaksi.WICNo_LAWAN IsNot Nothing Then
                            DisplayField1WicNoCounterParty.Hidden = False
                            DisplayField1WicFirstNameCounterParty.Hidden = False
                            DisplayFieldWicMiddleNameCounterParty.Hidden = False
                            DisplayFieldWicLastNameCounterParty.Hidden = False
                            DisplayFieldDOBCounterParty.Hidden = False
                            DisplayFieldNationalityCounterParty.Hidden = False
                            DisplayFieldIdentityNumCounterParty.Hidden = False
                            DisplayField1WicNoCounterParty.Text = transaksi.WICNo_LAWAN
                            Dim watchlist = NawaDevBLL.AMLJudgementBLL.GetAMLJudgementWatchlist(transaksi.WICNo_LAWAN)
                            If watchlist IsNot Nothing Then
                                If watchlist.INDV_First_Name IsNot Nothing Then
                                    DisplayField1WicFirstNameCounterParty.Text = watchlist.INDV_First_Name
                                End If
                                If watchlist.INDV_Middle_Name IsNot Nothing Then
                                    DisplayFieldWicMiddleNameCounterParty.Text = watchlist.INDV_Middle_Name
                                End If
                                If watchlist.INDV_Last_Name IsNot Nothing Then
                                    DisplayFieldWicLastNameCounterParty.Text = watchlist.INDV_Last_Name
                                End If
                                If watchlist.INDV_BirthDate IsNot Nothing Then
                                    DisplayFieldDOBCounterParty.Text = watchlist.INDV_BirthDate.Value.Date.ToString("dd-MMM-yyyy")
                                End If
                                If watchlist.INDV_Nationality1 IsNot Nothing Or watchlist.INDV_Nationality2 IsNot Nothing Or watchlist.INDV_Nationality3 IsNot Nothing Then
                                    DisplayFieldNationalityCounterParty.Text = watchlist.INDV_Nationality1 + "," + watchlist.INDV_Nationality2 + "," + watchlist.INDV_Nationality3
                                End If
                                If watchlist.INDV_ID_Number IsNot Nothing Then
                                    DisplayFieldIdentityNumCounterParty.Text = watchlist.INDV_ID_Number
                                End If
                            End If
                            Dim transresult = AMLJudgementBLL.GetAMLJudgementTransactionListByWICNOLAWAN(transaksi.TRANSACTION_NUMBER, transaksi.WICNo_LAWAN)


                        End If

                    End If



                End If


            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

    Private Sub loadcolumn()
        ColumnActionLocation(gridJudgementItemOld, CommandColumnGridJudgementItemOld)
        ColumnActionLocation(gridJudgementItemNew, CommandColumnGridJudgementItemNew)
    End Sub

    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_AML_WATCHLIST_ALIAS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ALIAS)
        gridAliasInfo.GetStore().DataSource = objtable
        gridAliasInfo.GetStore().DataBind()
    End Sub
    Protected Sub Bind_AML_WATCHLIST_ADDRESS()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_ADDRESS)

        objtable.Columns.Add(New DataColumn("COUNTRY_NAME", GetType(String)))
        objtable.Columns.Add(New DataColumn("AML_ADDRESS_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objCountry = AML_WATCHLIST_BLL.GetCountryByCountryCode(item("FK_AML_COUNTRY_CODE").ToString)
                If objCountry IsNot Nothing Then
                    item("COUNTRY_NAME") = objCountry.AML_COUNTRY_Name
                Else
                    item("COUNTRY_NAME") = Nothing
                End If

                Dim objAddressType = AML_WATCHLIST_BLL.GetAddressTypeByCode(item("FK_AML_ADDRESS_TYPE_CODE").ToString)
                If objAddressType IsNot Nothing Then
                    item("AML_ADDRESS_TYPE_NAME") = objAddressType.ADDRESS_TYPE_NAME
                Else
                    item("AML_ADDRESS_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gridAddressInfo.GetStore().DataSource = objtable
        gridAddressInfo.GetStore().DataBind()
    End Sub
    Protected Sub Bind_AML_WATCHLIST_IDENTITY()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objAML_WATCHLIST_CLASS.objList_AML_WATCHLIST_IDENTITY)

        objtable.Columns.Add(New DataColumn("AML_IDENTITY_TYPE_NAME", GetType(String)))

        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim objIDENTITYType = AML_WATCHLIST_BLL.GetIdentityTypeByCode(item("FK_AML_IDENTITY_TYPE_CODE").ToString)
                If objIDENTITYType IsNot Nothing Then
                    item("AML_IDENTITY_TYPE_NAME") = objIDENTITYType.IDENTITY_TYPE_NAME
                Else
                    item("AML_IDENTITY_TYPE_NAME") = Nothing
                End If
            Next
        End If

        gridIdentityInfo.GetStore().DataSource = objtable
        gridIdentityInfo.GetStore().DataBind()
    End Sub

    Private Sub BindJudgement(store As Store, list As List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL))
        Try
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_CATEGORY", GetType(String)))
            objtable.Columns.Add(New DataColumn("AML_WATCHLIST_TYPE", GetType(String)))
            objtable.Columns.Add(New DataColumn("ISMATCH", GetType(String)))
            If objtable.Rows.Count > 0 Then
                For Each item As Data.DataRow In objtable.Rows
                    If Not IsDBNull(item("FK_AML_WATCHLIST_CATEGORY_ID")) Then
                        item("AML_WATCHLIST_CATEGORY") = AMLJudgementBLL.GetWatchlistCategoryByID(item("FK_AML_WATCHLIST_CATEGORY_ID"))
                    Else
                        item("AML_WATCHLIST_CATEGORY") = ""
                    End If
                    If Not IsDBNull(item("FK_AML_WATCHLIST_TYPE_ID")) Then
                        item("AML_WATCHLIST_TYPE") = AMLJudgementBLL.GetWatchlistTypeByID(item("FK_AML_WATCHLIST_TYPE_ID"))
                    Else
                        item("AML_WATCHLIST_TYPE") = ""
                    End If
                    If Not IsDBNull(item("JUDGEMENT_ISMATCH")) Then
                        If item("JUDGEMENT_ISMATCH") Then
                            item("ISMATCH") = "Match"
                        Else
                            item("ISMATCH") = "Not Match"
                        End If
                    Else
                        item("ISMATCH") = ""
                    End If
                Next
            End If
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#Region "Direct Events"

    Protected Sub GridcommandJudgementItemOld(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Judge" Then
                ClearOBJDetail()
                WindowJudge.Hidden = False
                loadobjectJudgement(id, "old")
            ElseIf e.ExtraParams(1).Value = "Detail" Then
            ElseIf e.ExtraParams(1).Value = "Delete" Then
            ElseIf e.ExtraParams(1).Value = "Edit" Then
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandJudgementItemNew(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Judge" Then
                ClearOBJDetail()
                WindowJudge.Hidden = False
                loadobjectJudgement(id, "new")
            ElseIf e.ExtraParams(1).Value = "Detail" Then
            ElseIf e.ExtraParams(1).Value = "Delete" Then
            ElseIf e.ExtraParams(1).Value = "Edit" Then
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            AMLJudgementBLL.AcceptTransaction(ObjApproval.PK_ModuleApproval_ID)
            LblConfirmation.Text = "Data Approved. Click Ok to Back To " & ObjModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub Btnreject_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            AMLJudgementBLL.RejectTransaction(ObjApproval.PK_ModuleApproval_ID)
            LblConfirmation.Text = "Data rejecttransactioned. Click Ok to Back To " & ObjModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim strURL As String
            strURL = String.Format(Common.GetApplicationPath & "/Parameter/WaitingApproval.aspx?ModuleID={0}", Request.Params("ModuleID"))

            If Not ObjApproval Is Nothing Then
                If ObjApproval.CreatedBy <> Common.SessionCurrentUser.UserID Then
                    strURL = String.Format(Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID"))
                End If
            End If

            Net.X.Redirect(strURL)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs) Handles BtnConfirmation.DirectClick
        Try
            Net.X.Redirect(String.Format(Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

End Class

