﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="ScreeningTransactionJudgmentEdit.aspx.vb" Inherits="AML_ScreeningTransactionJudgment_ScreeningTransactionJudgmentEdit" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
<style type="text/css">
        .buttonClass {
    background-color: Red;
}
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ext:FormPanel ID="PanelInfo" runat="server" Title="Screening Transaction Judgement Edit" BodyStyle="padding:10px"  ButtonAlign="Center" Scrollable="Both">
        <Items>
            <ext:Panel runat="server" Layout="HBoxLayout">
                <Items>
            <ext:Panel runat="server" Flex="1">
                                <Items>

                                     <ext:DisplayField ID="DisplayFieldTransactionNumber" runat="server" FieldLabel="Transaction Number">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldTransactionDate" runat="server" FieldLabel="Transaction Date" >
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldDebitorCredit" runat="server" FieldLabel="Debit/Credit">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldTransactionCode" runat="server" FieldLabel="Transaction Code" >
                                    </ext:DisplayField>
                                    
                                    </Items>
                                    </ext:Panel>
             <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="DisplayFieldCurrency" runat="server" FieldLabel="Currency">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldNominal" runat="server" FieldLabel="Nominal">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldBranchCode" runat="server" FieldLabel="Branch Code">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldBranchName" runat="server" FieldLabel="Branch Name" >
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldRegionCode" runat="server" FieldLabel="Region Code">
                                    </ext:DisplayField>
                                    </Items>
                                    </ext:Panel>
                    </Items>
                 </ext:Panel>
            <ext:FormPanel ID="FormRequest" runat="server" Collapsible="true" BodyPadding="10" Title="Rekening">
                <Items>
                     <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                <Items>
                 <ext:Button ID="DetailCIF" runat="server" Icon="TextListBullets" Text="Detail CIF"> 
                        <DirectEvents>
                            <Click OnEvent="DetailCIF_Click">
                                <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                   <%-- <ext:Button runat="server" ID="DetailCIF" Text="Detail CIF" AutoPostBack="true" Icon="TextListBullets" OnClick="DetailCIF_Click" />--%>
                    </Items>
                </ext:Toolbar>
                    <ext:Panel runat="server" Layout="HBoxLayout">
                       
                        <Items>

                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                     <ext:DisplayField ID="display_Request_CIF" runat="server" FieldLabel="CIF">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldAccountNo" runat="server" FieldLabel="Account No">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Name" runat="server" FieldLabel="Customer Name">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldNationality" runat="server" FieldLabel="Nationality"  Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldDOB" runat="server" FieldLabel="Date Of Birth" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldIdentityNumber" runat="server" FieldLabel="Identity Number" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_ID" runat="server" FieldLabel="Request ID" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Source" runat="server" FieldLabel="Source" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Service" runat="server" FieldLabel="Service" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Operation" runat="server" FieldLabel="Operation" Hidden="true">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                   
                                    <ext:DisplayField ID="display_Request_DOB" runat="server" FieldLabel="Date Of Birth">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Nationality" runat="server" FieldLabel="Nationality" >
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Identity_Number" runat="server" FieldLabel="Identity Number" >
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
             <ext:GridPanel ID="gridJudgementItem" runat="server" MarginSpec="0 0 0 0">
                <Store>
                    <ext:Store ID="judgement_Item" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" IDProperty="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NAME_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_CATEGORY_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CATEGORY_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_TYPE_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="TYPE_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="DOB_WATCHLIST" Type="Date"></ext:ModelField>
                                    <ext:ModelField Name="NATIONALITY_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="IDENTITY_NUMBER_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE" Type="String"></ext:ModelField>
                                     <ext:ModelField Name="ISMATCH" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_DOB" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_NATIONALITY" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_IDENTITY_NUMBER" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_PCT" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                          <ext:CommandColumn ID="CommandColumn4A" runat="server" Text="Action" CellWrap="true" Hidden="true">
                             <Commands>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridcommandJudgementItem">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>

                        <ext:CommandColumn ID="CommandColumn4" runat="server" Text="Action" CellWrap="true" Hidden="true" Width="150">
                            <Commands>
                                <ext:GridCommand Text="Judge" CommandName="JudgeRekening" Icon="ApplicationEdit" MinWidth="70">
                                    <ToolTip Text="JudgeRekening"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridcommandJudgementItem">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                             <Commands>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridcommandJudgementItem">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                        <ext:Column ID="Column19" runat="server" DataIndex="FK_AML_WATCHLIST_ID" Text="Watchlist ID" Flex="1" Hidden="true"></ext:Column>
                        <ext:Column ID="Column412" runat="server" DataIndex="NAME" Text="Name" Flex="1"></ext:Column>
                        <ext:Column ID="Column1" runat="server" DataIndex="NAME_WATCHLIST" Text="Name Watch List" Flex="1"></ext:Column>
                        <ext:Column ID="Column2" runat="server" DataIndex="CATEGORY_NAME" Text="Watch List Category" Flex="1"></ext:Column>
                        <ext:Column ID="Column3" runat="server" DataIndex="TYPE_NAME" Text="Watch List Type" Flex="1"></ext:Column>
                        <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="DOB_WATCHLIST" Text="Date Of Birth" Flex="1" format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column4" runat="server" DataIndex="NATIONALITY_WATCHLIST" Text="Nationality" Flex="1"></ext:Column>
                        <ext:Column ID="Column5" runat="server" DataIndex="IDENTITY_NUMBER_WATCHLIST" Text="ID Number" Flex="1"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn1" runat="server" DataIndex="MATCH_SCORE" Text="Match Score (%)" Flex="1" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                         <ext:Column runat="server" Text="Judgement Result" DataIndex="ISMATCH" CellWrap="true" flex="1" />
                        <ext:NumberColumn ID="NumberColumn2" runat="server" DataIndex="MATCH_SCORE_PCT" Text="Match Score" Flex="1" Format="#,##0.00 %" Hidden="true"></ext:NumberColumn>

                       

                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            <ext:FormPanel ID="FormResponse" runat="server" Collapsible="true" BodyPadding="10" Title="Counter Party">
                <Items>
                    <ext:Panel runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="display_Response_ID" runat="server" FieldLabel="Response ID" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Request_Date" runat="server" FieldLabel="Request Date" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Code" runat="server" FieldLabel="Response Code" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Description" runat="server" FieldLabel="Response Description" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Date" runat="server" FieldLabel="Response Date" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldCifNoCounterParty" runat="server" FieldLabel="CIF No" Hidden="true">
                                    </ext:DisplayField>
                                      <ext:DisplayField ID="DisplayFieldAccountNoCounterParty" runat="server" FieldLabel="Account No" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldCustomerNameCounterParty" runat="server" FieldLabel="Customer Name" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField1WicNoCounterParty" runat="server" FieldLabel="WIC No" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayField1WicFirstNameCounterParty" runat="server" FieldLabel="WIC First Name" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldWicMiddleNameCounterParty" runat="server" FieldLabel="WIC Middle Name" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldWicLastNameCounterParty" runat="server" FieldLabel="WIC Last Name" Hidden="true">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldNationalityCounterParty" runat="server" FieldLabel="Nationality" Hidden="true">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldDOBCounterParty" runat="server" FieldLabel="Date Of Birth" Hidden="true">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldIdentityNumCounterParty" runat="server" FieldLabel="Identity Number" Hidden="true">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                            <%--<ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="display_Response_Operation" runat="server" FieldLabel="Operation">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Code" runat="server" FieldLabel="Response Code">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="display_Response_Description" runat="server" FieldLabel="Response Description">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>--%>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
             <ext:GridPanel ID="GridPanelJudgment2" runat="server" MarginSpec="0 0 0 0">
                <Store>
                    <ext:Store ID="Store2" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" IDProperty="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NAME_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_CATEGORY_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CATEGORY_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_TYPE_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="TYPE_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="DOB_WATCHLIST" Type="Date"></ext:ModelField>
                                    <ext:ModelField Name="NATIONALITY_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="IDENTITY_NUMBER_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE" Type="String"></ext:ModelField>
                                     <ext:ModelField Name="ISMATCH" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_DOB" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_NATIONALITY" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_IDENTITY_NUMBER" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_PCT" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                         <ext:CommandColumn ID="CommandColumn3A" runat="server" Text="Action" CellWrap="true" Hidden="true">
                             <Commands>
                                <ext:GridCommand Text="Detail" CommandName="DetailCounterParty" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="DetailCounterParty"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridcommandJudgementItem">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>

                        <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" CellWrap="true" Hidden="true" Width="150">
                            <Commands>
                                <ext:GridCommand Text="Judge" CommandName="JudgeCounterParty" Icon="ApplicationEdit" MinWidth="70">
                                    <ToolTip Text="JudgeCounterParty"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridcommandJudgementItem">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                             <Commands>
                                <ext:GridCommand Text="Detail" CommandName="DetailCounterParty" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="DetailCounterParty"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridcommandJudgementItem">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                        <ext:Column ID="Column6" runat="server" DataIndex="FK_AML_WATCHLIST_ID" Text="Watchlist ID" Flex="1" Hidden="true"></ext:Column>
                        <ext:Column ID="Column7" runat="server" DataIndex="NAME" Text="Name" Flex="1"></ext:Column>
                        <ext:Column ID="Column16" runat="server" DataIndex="NAME_WATCHLIST" Text="Name Watch List" Flex="1"></ext:Column>
                        <ext:Column ID="Column17" runat="server" DataIndex="CATEGORY_NAME" Text="Watch List Category" Flex="1"></ext:Column>
                        <ext:Column ID="Column18" runat="server" DataIndex="TYPE_NAME" Text="Watch List Type" Flex="1"></ext:Column>
                        <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="DOB_WATCHLIST" Text="Date Of Birth" Flex="1" format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column20" runat="server" DataIndex="NATIONALITY_WATCHLIST" Text="Nationality" Flex="1"></ext:Column>
                        <ext:Column ID="Column21" runat="server" DataIndex="IDENTITY_NUMBER_WATCHLIST" Text="ID Number" Flex="1"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn3" runat="server" DataIndex="MATCH_SCORE" Text="Match Score (%)" Flex="1" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                        <ext:Column runat="server" Text="Judgement Result" DataIndex="ISMATCH" CellWrap="true" flex="1" />
                        <ext:NumberColumn ID="NumberColumn4" runat="server" DataIndex="MATCH_SCORE_PCT" Text="Match Score" Flex="1" Format="#,##0.00 %" Hidden="true"></ext:NumberColumn>

                       
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            <ext:FormPanel ID="FormPanel1" runat="server" Collapsible="true" BodyPadding="10" Title="Conductor" Hidden="true">
                <Items>
                    <ext:Panel runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                     <ext:DisplayField ID="DisplayFieldCIFNoCounductor" runat="server" FieldLabel="CIF" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldAccountNoCounductor" runat="server" FieldLabel="Account No" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldCustomerNameCounductor" runat="server" FieldLabel="Customer Name" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldWICNoCounductor" runat="server" FieldLabel="WIC No" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldWICFirstNameCounductor" runat="server" FieldLabel="WIC First Name" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldWICMiddleNameCounductor" runat="server" FieldLabel="WIC Middle Name" Hidden="true">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayFieldWICLastNameCounductor" runat="server" FieldLabel="WIC Last Name" Hidden="true">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldNationalityCounductor" runat="server" FieldLabel="Nationality" Hidden="true">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldDOBCounductor" runat="server" FieldLabel="Date Of Birth" Hidden="true">
                                    </ext:DisplayField>
                                     <ext:DisplayField ID="DisplayFieldIdentityNumberCounductor" runat="server" FieldLabel="Identity Number" Hidden="true">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
             <ext:GridPanel ID="GridPanelJudgment3" runat="server" MarginSpec="0 0 0 0" Hidden="true">
                <Store>
                    <ext:Store ID="Store1" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" IDProperty="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NAME_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_CATEGORY_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CATEGORY_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_TYPE_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="TYPE_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="DOB_WATCHLIST" Type="Date"></ext:ModelField>
                                    <ext:ModelField Name="NATIONALITY_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="IDENTITY_NUMBER_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE" Type="String"></ext:ModelField>
                                     <ext:ModelField Name="ISMATCH" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_DOB" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_NATIONALITY" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_IDENTITY_NUMBER" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_PCT" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                        <ext:Column ID="Column22" runat="server" DataIndex="FK_AML_WATCHLIST_ID" Text="Watchlist ID" Flex="1" Hidden="true"></ext:Column>
                        <ext:Column ID="Column23" runat="server" DataIndex="NAME" Text="Name" Flex="1"></ext:Column>
                        <ext:Column ID="Column24" runat="server" DataIndex="NAME_WATCHLIST" Text="Name Watch List" Flex="1"></ext:Column>
                        <ext:Column ID="Column25" runat="server" DataIndex="CATEGORY_NAME" Text="Watch List Category" Flex="1"></ext:Column>
                        <ext:Column ID="Column26" runat="server" DataIndex="TYPE_NAME" Text="Watch List Type" Flex="1"></ext:Column>
                        <ext:DateColumn ID="DateColumn3" runat="server" DataIndex="DOB_WATCHLIST" Text="Date Of Birth" Flex="1" format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column27" runat="server" DataIndex="NATIONALITY_WATCHLIST" Text="Nationality" Flex="1"></ext:Column>
                        <ext:Column ID="Column28" runat="server" DataIndex="IDENTITY_NUMBER_WATCHLIST" Text="ID Number" Flex="1"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn5" runat="server" DataIndex="MATCH_SCORE" Text="Match Score (%)" Flex="1" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                        <ext:Column runat="server" Text="Judgement Result" DataIndex="ISMATCH" CellWrap="true" flex="1" />
                        <ext:NumberColumn ID="NumberColumn6" runat="server" DataIndex="MATCH_SCORE_PCT" Text="Match Score" Flex="1" Format="#,##0.00 %" Hidden="true"></ext:NumberColumn>


                        <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" CellWrap="true">
                            <Commands>
                                <ext:GridCommand Text="Judge" CommandName="Judge" Icon="ApplicationEdit" MinWidth="70">
                                    <ToolTip Text="Judge"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridcommandJudgementItem">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
               <ext:FormPanel ID="FormPanelNoUnik" runat="server" Collapsible="true" BodyPadding="10" Title="Account Name Lawan">
                <Items>
                    <ext:Panel runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                     <ext:DisplayField ID="DisplayFieldNamaNomorUnik" runat="server" FieldLabel="Name">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>
             <ext:GridPanel ID="GridPanelNoUnik" runat="server" MarginSpec="0 0 0 0">
                <Store>
                    <ext:Store ID="StoreAccountNoLawan" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" IDProperty="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NAME_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_CATEGORY_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CATEGORY_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="FK_AML_WATCHLIST_TYPE_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="TYPE_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="DOB_WATCHLIST" Type="Date"></ext:ModelField>
                                    <ext:ModelField Name="NATIONALITY_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="IDENTITY_NUMBER_WATCHLIST" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE" Type="String"></ext:ModelField>
                                     <ext:ModelField Name="ISMATCH" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_DOB" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_NATIONALITY" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_IDENTITY_NUMBER" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_SCORE_PCT" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="60"></ext:RowNumbererColumn>
                        <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" CellWrap="true">
                             <Commands>
                                <ext:GridCommand Text="Detail" CommandName="DetailAccountNoLawan" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="DetailAccountNoLawan"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridcommandJudgementItem">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                        <ext:Column ID="Column29" runat="server" DataIndex="FK_AML_WATCHLIST_ID" Text="Watchlist ID" Flex="1" Hidden="true"></ext:Column>
                        <ext:Column ID="Column30" runat="server" DataIndex="NAME" Text="Name" Flex="1"></ext:Column>
                        <ext:Column ID="Column31" runat="server" DataIndex="NAME_WATCHLIST" Text="Name Watch List" Flex="1"></ext:Column>
                        <ext:Column ID="Column32" runat="server" DataIndex="CATEGORY_NAME" Text="Watch List Category" Flex="1"></ext:Column>
                        <ext:Column ID="Column33" runat="server" DataIndex="TYPE_NAME" Text="Watch List Type" Flex="1"></ext:Column>
                        <ext:DateColumn ID="DateColumn4" runat="server" DataIndex="DOB_WATCHLIST" Text="Date Of Birth" Flex="1" format="dd-MMM-yyyy"></ext:DateColumn>
                        <ext:Column ID="Column34" runat="server" DataIndex="NATIONALITY_WATCHLIST" Text="Nationality" Flex="1"></ext:Column>
                        <ext:Column ID="Column35" runat="server" DataIndex="IDENTITY_NUMBER_WATCHLIST" Text="ID Number" Flex="1"></ext:Column>
                        <ext:NumberColumn ID="NumberColumn7" runat="server" DataIndex="MATCH_SCORE" Text="Match Score (%)" Flex="1" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                        <ext:Column runat="server" Text="Judgement Result" DataIndex="ISMATCH" CellWrap="true" flex="1" />
                        <ext:NumberColumn ID="NumberColumn8" runat="server" DataIndex="MATCH_SCORE_PCT" Text="Match Score" Flex="1" Format="#,##0.00 %" Hidden="true"></ext:NumberColumn>


                       
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btnSaveReport" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="BtnSave_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancelReport" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
  
    <ext:Window ID="WindowJudge" Title="Result Detail" runat="server" Modal="true" Maximizable="true" Hidden="true" BodyStyle="padding:20px" Scrollable="Both" ButtonAlign="Center" Width="1000" Height="500">
        <Items>
            <ext:FormPanel ID="ScoreDetail" runat="server" Collapsible="true" BodyPadding="10" Title="Search Score Detail" AnchorHorizontal="100%">
                <Items>
                    <ext:DisplayField ID="Name_Score" runat="server" FieldLabel="Name Score" Hidden="true">
                    </ext:DisplayField>
                    <ext:DisplayField ID="DoB_Score" runat="server" FieldLabel="Date of Birth Score" Hidden="true">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Nationality_Score" runat="server" FieldLabel="Nationality Score" Hidden="true">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Identity_Score" runat="server" FieldLabel="Identity Number Score" Hidden="true">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Total_Score" runat="server" FieldLabel="Total Score" Hidden="true">
                    </ext:DisplayField>

                    <ext:FieldSet runat="server" ID="FieldSet5" Collapsible="false" Title="" Border="false" Layout="ColumnLayout" AnchorHorizontal="100%" Padding="0">
                        <Items>
                            <ext:FieldSet runat="server" ID="FieldSet3" Collapsible="false" ColumnWidth="0.38" Title="Search Data">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Name" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Nationality" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Identity Number" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>
                            <ext:FieldSet runat="server" ID="FieldSet2" Collapsible="false" ColumnWidth="0.35" Title="Search Result" MarginSpec="0 5">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>
                            <ext:FieldSet runat="server" ID="FieldSet1" Collapsible="false" ColumnWidth="0.25" Title="Search Score">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%" ></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>

                            <ext:FieldSet runat="server" ID="FieldSet4" Collapsible="false" ColumnWidth="1" Title="" Border="false">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_TOTAL" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Total Score" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>

                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:FormPanel>
            
            <ext:FormPanel ID="GeneralInformation" runat="server" Collapsible="true" BodyPadding="10" Title="General Information">
                <Items>
                    <ext:DisplayField ID="WatchListID" runat="server" FieldLabel="Watch List ID">
                    </ext:DisplayField>
                    <ext:DisplayField ID="WatchListCategory" runat="server" FieldLabel="Watch List Category">
                    </ext:DisplayField>
                    <ext:DisplayField ID="WatchListType" runat="server" FieldLabel="Watch List Type">
                    </ext:DisplayField>
                    <ext:DisplayField ID="FullName" runat="server" FieldLabel="Full Name">
                    </ext:DisplayField>
                    <ext:DisplayField ID="PlaceofBirth" runat="server" FieldLabel="Place of Birth">
                    </ext:DisplayField>
                    <ext:DisplayField ID="DateofBirth" runat="server" FieldLabel="Date of Birth">
                    </ext:DisplayField>
                    <ext:DisplayField ID="YearofBirth" runat="server" FieldLabel="Year of Birth">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Nationality" runat="server" FieldLabel="Nationality">
                    </ext:DisplayField>
                    <%-- Watch List Alias --%>
                    <ext:GridPanel ID="gridAliasInfo" runat="server" Title="Alias(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <Store>
                            <ext:Store ID="Store_gridAliasInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ALIAS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ALIAS_NAME" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column8" runat="server" DataIndex="ALIAS_NAME" Text="Alias Name" MinWidth="300" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Alias --%>

                    <%-- Watch List Address --%>
                    <ext:GridPanel ID="gridAddressInfo" runat="server" Title="Address(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <Store>
                            <ext:Store ID="Store_gridAddressInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ADDRESS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_AML_COUNTRY_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="POSTAL_CODE" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column9" runat="server" DataIndex="AML_ADDRESS_TYPE_NAME" Text="Type" MinWidth="50"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="ADDRESS" Text="Address" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="FK_AML_COUNTRY_CODE" Text="Country Code" MinWidth="100" Hidden="true"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="COUNTRY_NAME" Text="Country Name" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="POSTAL_CODE" Text="Postal Code" MinWidth="100"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Address --%>

                    <%-- Watch List Identity --%>
                    <ext:GridPanel ID="gridIdentityInfo" runat="server" Title="Identity(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <Store>
                            <ext:Store ID="Store_gridIdentityInfo" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_IDENTITY_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_IDENTITY_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDENTITYNUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column14" runat="server" DataIndex="AML_IDENTITY_TYPE_NAME" Text="Type" MinWidth="200" Flex="1"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="IDENTITYNUMBER" Text="Identity" MinWidth="300"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Identity --%>
                    <ext:DisplayField ID="Remark1" runat="server" FieldLabel="Remark 1">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark2" runat="server" FieldLabel="Remark 2">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark3" runat="server" FieldLabel="Remark 3">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark4" runat="server" FieldLabel="Remark 4">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Remark5" runat="server" FieldLabel="Remark 5">
                    </ext:DisplayField>
                </Items>
            </ext:FormPanel>
            <ext:Panel ID="PanelJudge" runat="server" Layout="AnchorLayout">
                <Items>
                    <ext:RadioGroup runat="server" ID="RGJudgement" AnchorHorizontal="50%" FieldLabel="Is Result Match ?" LabelWidth="150">
                        <Items>
                            <ext:Radio runat="server" ID="RBJudgementMatch" BoxLabel="Match" InputValue="1">
                                <DirectEvents>
                                    <Change OnEvent="rgjudgement_click" />
                                </DirectEvents>
                            </ext:Radio>

                            <ext:Radio runat="server" ID="RBJudgementNotMatch" BoxLabel="Not Match" InputValue="0">
                                <DirectEvents>
                                    <Change OnEvent="rgjudgement_click" />
                                </DirectEvents>
                            </ext:Radio>
                        </Items>
                    </ext:RadioGroup>
                    <ext:TextArea runat="server" ID="JudgementComment" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="1000" LabelWidth="150" EnforceMaxLength="true"/>
                </Items>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="BtnJudgementSave" runat="server" Icon="Disk" Text="Save Result">
                <DirectEvents>
                    <Click OnEvent="BtnJudgementSave_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnJudgementCancel" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnJudgementCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:Window ID="WindowJudgeDetail" Title="Result Detail" runat="server" Modal="true" Maximizable="true" Hidden="true" BodyStyle="padding:20px" Scrollable="Both" ButtonAlign="Center" Width="1000" Height="500">
        <Items>
           <ext:FormPanel ID="FormPanel2" runat="server" Collapsible="true" BodyPadding="10" Title="Search Score Detail" AnchorHorizontal="100%">
                <Items>
                    <ext:DisplayField ID="Name_Score2" runat="server" FieldLabel="Name Score" Hidden="true">
                    </ext:DisplayField>
                    <ext:DisplayField ID="DoB_Score2" runat="server" FieldLabel="Date of Birth Score" Hidden="true">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Nationality_Score2" runat="server" FieldLabel="Nationality Score" Hidden="true">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Identity_Score2" runat="server" FieldLabel="Identity Number Score" Hidden="true">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Total_Score2" runat="server" FieldLabel="Total Score" Hidden="true">
                    </ext:DisplayField>

                    <ext:FieldSet runat="server" ID="FieldSet6" Collapsible="false" Title="" Border="false" Layout="ColumnLayout" AnchorHorizontal="100%" Padding="0">
                        <Items>
                            <ext:FieldSet runat="server" ID="FieldSetDetail" Collapsible="false" ColumnWidth="0.38" Title="Search Data">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME_SEARCH2" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Name" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB_SEARCH2" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality_SEARCH2" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Nationality" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER_SEARCH2" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Identity Number" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>
                            <ext:FieldSet runat="server" ID="FieldSet8" Collapsible="false" ColumnWidth="0.35" Title="Search Result" MarginSpec="0 5">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME_WATCHLIST2" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB_WATCHLIST2" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality_WATCHLIST2" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER_WATCHLIST2" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>
                            <ext:FieldSet runat="server" ID="FieldSet9" Collapsible="false" ColumnWidth="0.25" Title="Search Score">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_NAME2" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_DOB2" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%" ></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_Nationality2" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_IDENTITY_NUMBER2" AllowBlank="false" LabelWidth="70" runat="server" FieldLabel="" AnchorHorizontal="100%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>

                            <ext:FieldSet runat="server" ID="FieldSet10" Collapsible="false" ColumnWidth="1" Title="" Border="false">
                                <Items>
                                    <ext:DisplayField ID="txt_MATCH_SCORE_TOTAL2" AllowBlank="false" LabelWidth="100" runat="server" FieldLabel="Total Score" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FieldSet>

                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:FormPanel>
            
            <ext:FormPanel ID="FormPanel3" runat="server" Collapsible="true" BodyPadding="10" Title="General Information">
                <Items>
                    <ext:DisplayField ID="WatchListID2" runat="server" FieldLabel="Watch List ID">
                    </ext:DisplayField>
                    <ext:DisplayField ID="WatchListCategory2" runat="server" FieldLabel="Watch List Category">
                    </ext:DisplayField>
                    <ext:DisplayField ID="WatchListType2" runat="server" FieldLabel="Watch List Type">
                    </ext:DisplayField>
                    <ext:DisplayField ID="FullName2" runat="server" FieldLabel="Full Name">
                    </ext:DisplayField>
                    <ext:DisplayField ID="PlaceofBirth2" runat="server" FieldLabel="Place of Birth">
                    </ext:DisplayField>
                    <ext:DisplayField ID="DateofBirth2" runat="server" FieldLabel="Date of Birth">
                    </ext:DisplayField>
                    <ext:DisplayField ID="YearofBirth2" runat="server" FieldLabel="Year of Birth">
                    </ext:DisplayField>
                    <ext:DisplayField ID="Nationality2" runat="server" FieldLabel="Nationality">
                    </ext:DisplayField>
                    <%-- Watch List Alias --%>
                    <ext:GridPanel ID="GridPanel1" runat="server" Title="Alias(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <Store>
                            <ext:Store ID="Store4" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ALIAS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ALIAS_NAME" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column40" runat="server" DataIndex="ALIAS_NAME" Text="Alias Name" MinWidth="300" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Alias --%>

                    <%-- Watch List Address --%>
                    <ext:GridPanel ID="GridPanel2" runat="server" Title="Address(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <Store>
                            <ext:Store ID="Store5" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_ADDRESS_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_ADDRESS_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ADDRESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_AML_COUNTRY_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="POSTAL_CODE" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column41" runat="server" DataIndex="AML_ADDRESS_TYPE_NAME" Text="Type" MinWidth="50"></ext:Column>
                                <ext:Column ID="Column42" runat="server" DataIndex="ADDRESS" Text="Address" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                                <ext:Column ID="Column43" runat="server" DataIndex="FK_AML_COUNTRY_CODE" Text="Country Code" MinWidth="100" Hidden="true"></ext:Column>
                                <ext:Column ID="Column44" runat="server" DataIndex="COUNTRY_NAME" Text="Country Name" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column45" runat="server" DataIndex="POSTAL_CODE" Text="Postal Code" MinWidth="100"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Address --%>

                    <%-- Watch List Identity --%>
                    <ext:GridPanel ID="GridPanel3" runat="server" Title="Identity(es)" AutoScroll="true" Border="true" PaddingSpec="10 0">
                        <Store>
                            <ext:Store ID="Store6" runat="server" IsPagingStore="true" PageSize="4">
                                <Model>
                                    <ext:Model ID="Model6" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_AML_WATCHLIST_IDENTITY_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AML_IDENTITY_TYPE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDENTITYNUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="#" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column46" runat="server" DataIndex="AML_IDENTITY_TYPE_NAME" Text="Type" MinWidth="200" Flex="1"></ext:Column>
                                <ext:Column ID="Column47" runat="server" DataIndex="IDENTITYNUMBER" Text="Identity" MinWidth="300"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Watch List Identity --%>
                    <ext:DisplayField ID="DisplayField14" runat="server" FieldLabel="Remark 1">
                    </ext:DisplayField>
                    <ext:DisplayField ID="DisplayField15" runat="server" FieldLabel="Remark 2">
                    </ext:DisplayField>
                    <ext:DisplayField ID="DisplayField16" runat="server" FieldLabel="Remark 3">
                    </ext:DisplayField>
                    <ext:DisplayField ID="DisplayField17" runat="server" FieldLabel="Remark 4">
                    </ext:DisplayField>
                    <ext:DisplayField ID="DisplayField18" runat="server" FieldLabel="Remark 5">
                    </ext:DisplayField>
                </Items>
            </ext:FormPanel>
       
        </Items>
        <Buttons>
            <ext:Button ID="Button3" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnJudgementCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:Window ID="Window1" Title="Detail CIF" runat="server" Modal="true" Hidden="true" BodyStyle="padding:5px" AutoScroll="true" ButtonAlign="Center" Maximizable="true">
        <Items>
            </Items>
          <Buttons>
            <ext:Button ID="Button2" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnJudgementCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        </ext:Window>
  
    <ext:Window ID="WindowDetailCIF" Title="Detail CIF" runat="server" Modal="true" Hidden="true" BodyStyle="padding:5px" AutoScroll="true" ButtonAlign="Center" Maximizable="true" Width="800" Height="500">
       <Items>
               <ext:Panel runat="server" Layout="HBoxLayout">
                        <Items>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                      <ext:DisplayField ID="DisplayCIFNO" runat="server" FieldLabel="CIF No">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayCustomerName" runat="server" FieldLabel="Nama Nasabah">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayKodeCabang" runat="server" FieldLabel="Branch Code">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayNamaCabang" runat="server" FieldLabel="Branch Name">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayKantorWilayah" runat="server" FieldLabel="Regional Office">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayTipeNasabah" runat="server" FieldLabel="Tipe Nasabah">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayNPWP" runat="server" FieldLabel="NPWP">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                            <ext:Panel runat="server" Flex="1">
                                <Items>
                                    <ext:DisplayField ID="DisplayPOB" runat="server" FieldLabel="Place Of Birth">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayDOB" runat="server" FieldLabel="Date Of Birth">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayNegara" runat="server" FieldLabel="Negara">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayRiskProfileT24" runat="server" FieldLabel="Risk Profile T24">
                                    </ext:DisplayField>
                                    <ext:DisplayField ID="DisplayRiskProfileOneFCC" runat="server" FieldLabel="Risk Profile OneFCC">
                                    </ext:DisplayField>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
            <%-- Grid Customer Detail Address --%>
            <ext:GridPanel ID="gp_DETAIL_CIF_ADDRESS" runat="server" Title="Address(es)">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="Store_DETAIL_CIF_ADDRESS" runat="server" IsPagingStore="true" PageSize="20">
                        <Model>
                            <ext:Model runat="server" ID="mdl_DETAIL_CIF_ADDRESS">
                                <Fields>
                                    <ext:ModelField Name="CUSTOMER_ADDRESS" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ADDRESS_TYPE" Type="String"></ext:ModelField>
                                    <%--<ext:ModelField Name="KELURAHAN" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="KECAMATAN" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="KOTAKABUPATEN" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="PROPINSI" Type="String"></ext:ModelField>--%>
                                    <ext:ModelField Name="KODEPOS" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="COUNTRY_CODE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NOTES" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="rn_DETAIL_CIF_ADDRESS" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Col_Address" runat="server" DataIndex="CUSTOMER_ADDRESS" Text="Address" Width="150" ></ext:Column>
                        <ext:Column ID="Col_Address_Type" runat="server" DataIndex="ADDRESS_TYPE" Text="Address Type" Width="150" ></ext:Column>
                        <%--<ext:Column ID="Col_KELURAHAN" runat="server" DataIndex="KELURAHAN" Text="Kelurahan" Flex="1"></ext:Column>
                        <ext:Column ID="Col_KECAMATAN" runat="server" DataIndex="KECAMATAN" Text="Kecamatan" Flex="1"></ext:Column>
                        <ext:Column ID="Col_KOTAKABUPATEN" runat="server" DataIndex="KOTAKABUPATEN" Text="Kota Kabupaten" Flex="1"></ext:Column>
                        <ext:Column ID="Col_PROPINSI" runat="server" DataIndex="PROPINSI" Text="Propinsi" Flex="1"></ext:Column>--%>
                        <ext:Column ID="Col_KODEPOS" runat="server" DataIndex="KODEPOS" Text="Kode Pos" Flex="1"></ext:Column>
                        <ext:Column ID="Col_COUNTRY_CODE" runat="server" DataIndex="COUNTRY_CODE" Text="Country" Flex="1"></ext:Column>
                        <ext:Column ID="Col_NOTES" runat="server" DataIndex="NOTES" Text="Notes" Flex="1"></ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="false" >
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <%-- End of Grid Customer Detail Address --%>
            <%-- Grid Customer Detail Identity --%>
             <ext:GridPanel ID="gp_DETAIL_CIF_IDENTITY" runat="server" Title="Identity(es)">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="Store_DETAIL_CIF_IDENTITY" runat="server" IsPagingStore="true" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="mdl_DETAIL_CIF_IDENTITY">
                                <Fields>
                                    <ext:ModelField Name="CUSTOMER_IDENTITY" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="IDENTITY_TYPE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="COUNTRY_CODE" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NOTES" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column36" runat="server" DataIndex="CUSTOMER_IDENTITY" Text="Identity" Flex="1"></ext:Column>
                        <ext:Column ID="Column37" runat="server" DataIndex="IDENTITY_TYPE" Text="Identity Type" Flex="1"></ext:Column>
                        <ext:Column ID="Column38" runat="server" DataIndex="COUNTRY_CODE" Text="Country" Flex="1"></ext:Column>
                        <ext:Column ID="Column39" runat="server" DataIndex="NOTES" Text="Notes" Flex="1"></ext:Column>

                    </Columns>
                </ColumnModel>
                <Plugins>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            <%-- End of Grid Customer Detail Identity --%>
        </Items>
         <Buttons>
            <ext:Button ID="CancelDetailCIF" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="DetailCIFCancelButton">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>

        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>






