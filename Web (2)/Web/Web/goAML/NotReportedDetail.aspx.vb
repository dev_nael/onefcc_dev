﻿Imports System.Data
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevDAL
Imports NawaDevBLL.NotReportedBLL

Partial Class goAML_NotReportedDetail
    Inherits Parent
#Region "Session"
    Private _IDReq As Long
    Public Property IDReq() As Long
        Get
            Return _IDReq
        End Get
        Set(ByVal value As Long)
            _IDReq = value
        End Set
    End Property
    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("goAML_NotReportedDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_NotReportedDetail.ObjModule") = value
        End Set
    End Property
    Public Property NotReportClass As NawaDevBLL.NotReported
        Get
            Return Session("goAML_NotReportedDetail.NotReportClass")
        End Get
        Set(ByVal value As NawaDevBLL.NotReported)
            Session("goAML_NotReportedDetail.NotReportClass") = value
        End Set
    End Property
    Public Property listTransactionClass As List(Of NawaDevBLL.Transaction_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listTransactionClass")
        End Get
        Set(ByVal value As List(Of NawaDevBLL.Transaction_NotReported))
            Session("goAML_NotReportedDetail.listTransactionClass") = value
        End Set
    End Property
    Public Property TransactionClass As NawaDevBLL.Transaction_NotReported
        Get
            Return Session("goAML_NotReportedDetail.listTransactionClass")
        End Get
        Set(ByVal value As NawaDevBLL.Transaction_NotReported)
            Session("goAML_NotReportedDetail.listTransactionClass") = value
        End Set
    End Property
    Public Property listTransaction As List(Of NawaDevDAL.goAML_Transaction_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Transaction_NotReported))
            Session("goAML_NotReportedDetail.listTransaction") = value
        End Set
    End Property
    Public Property DirectorClass As NawaDevBLL.DirectorClass_NotReported
        Get
            Return Session("goAML_NotReportedDetail.DirectorClass")
        End Get
        Set(ByVal value As NawaDevBLL.DirectorClass_NotReported)
            Session("goAML_NotReportedDetail.DirectorClass") = value
        End Set
    End Property
    Public Property SignatoryClass As NawaDevBLL.SignatoryClass_NotReported
        Get
            Return Session("goAML_NotReportedDetail.SignatoryClass")
        End Get
        Set(ByVal value As NawaDevBLL.SignatoryClass_NotReported)
            Session("goAML_NotReportedDetail.SignatoryClass") = value
        End Set
    End Property
    Public Property DirectorEntityAccountPartyClass As NawaDevBLL.DirectorEntityAccountPartyClass_NotReported
        Get
            Return Session("goAML_NotReportedDetail.DirectorEntityAccountPartyClass")
        End Get
        Set(ByVal value As NawaDevBLL.DirectorEntityAccountPartyClass_NotReported)
            Session("goAML_NotReportedDetail.DirectorEntityAccountPartyClass") = value
        End Set
    End Property
    Public Property DirectorEntityPartyClass As NawaDevBLL.DirectorEntityPartyClass_NotReported
        Get
            Return Session("goAML_NotReportedDetail.DirectorEntityPartyClass")
        End Get
        Set(ByVal value As NawaDevBLL.DirectorEntityPartyClass_NotReported)
            Session("goAML_NotReportedDetail.DirectorEntityPartyClass") = value
        End Set
    End Property
    Public Property SignatoryAccountPartyClass As NawaDevBLL.SignatoryAccountPartyClass_NotReported
        Get
            Return Session("goAML_NotReportedDetail.SignatoryAccountPartyClass")
        End Get
        Set(ByVal value As NawaDevBLL.SignatoryAccountPartyClass_NotReported)
            Session("goAML_NotReportedDetail.SignatoryAccountPartyClass") = value
        End Set
    End Property
    Public Property listDirectorEntityparty As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listDirectorEntityparty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_NotReported))
            Session("goAML_NotReportedDetail.listDirectorEntityparty") = value
        End Set
    End Property
    Public Property listAddressDirectorEntityParty As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressDirectorEntityParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressDirectorEntityParty") = value
        End Set
    End Property
    Public Property listAddressEmployerDirectorEntityParty As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressEmployerDirectorEntityParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressEmployerDirectorEntityParty") = value
        End Set
    End Property
    Public Property listPhoneDirectorEntityParty As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneDirectorEntityParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneDirectorEntityParty") = value
        End Set
    End Property
    Public Property listPhoneEmployerDirectorEntityParty As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityParty") = value
        End Set
    End Property
    Public Property listIdentificationDirectorEntityParty As List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listIdentificationDirectorEntityParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported))
            Session("goAML_NotReportedDetail.listIdentificationDirectorEntityParty") = value
        End Set
    End Property
    Public Property listDirectorentityaccountparty As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listDirectorentityaccountparty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_NotReported))
            Session("goAML_NotReportedDetail.listDirectorentityaccountparty") = value
        End Set
    End Property
    Public Property listPhoneDirectorEntityAccountParty As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneDirectorEntityAccountParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneDirectorEntityAccountParty") = value
        End Set
    End Property
    Public Property listPhoneEmployerDirectorEntityAccountParty As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityAccountParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityAccountParty") = value
        End Set
    End Property
    Public Property listAddressDirectorEntityAccountParty As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressDirectorEntityAccountParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressDirectorEntityAccountParty") = value
        End Set
    End Property
    Public Property listaddressEmployerDirectorEntityAccountParty As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listaddressEmployerDirectorEntityAccountParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listaddressEmployerDirectorEntityAccountParty") = value
        End Set
    End Property
    Public Property listIdentificationDirectorEntityaccountparty As List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listIdentificationDirectorEntityaccountparty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported))
            Session("goAML_NotReportedDetail.listIdentificationDirectorEntityaccountparty") = value
        End Set
    End Property
    Public Property listSignatoryAccountParty As List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listSignatoryAccountParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory_NotReported))
            Session("goAML_NotReportedDetail.listSignatoryAccountParty") = value
        End Set
    End Property
    Public Property listAddressSignatoryAccountParty As List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressSignatoryAccountParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressSignatoryAccountParty") = value
        End Set
    End Property
    Public Property listAddressEmployerSignatoryAccountParty As List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressEmployerSignatoryAccountParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressEmployerSignatoryAccountParty") = value
        End Set
    End Property
    Public Property listPhoneSignatoryAccountParty As List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneSignatoryAccountParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneSignatoryAccountParty") = value
        End Set
    End Property
    Public Property listPhoneEmployerSignatoryAccountParty As List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEmployerSignatoryAccountParty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEmployerSignatoryAccountParty") = value
        End Set
    End Property
    Public Property listIdentificationSignatoryAccountparty As List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listIdentificationSignatoryAccountparty")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported))
            Session("goAML_NotReportedDetail.listIdentificationSignatoryAccountparty") = value
        End Set
    End Property
    Public Property listTrnAcctEntityDiretor As List(Of NawaDevDAL.goAML_Trn_Director_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listTrnAcctEntityDiretor")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Director_NotReported))
            Session("goAML_NotReportedDetail.listTrnAcctEntityDiretor") = value
        End Set
    End Property

    Public Property listTrnEntityDiretor As List(Of NawaDevDAL.goAML_Trn_Director_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listTrnEntityDiretor")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Director_NotReported))
            Session("goAML_NotReportedDetail.listTrnEntityDiretor") = value
        End Set
    End Property
    Public Property listTrnAcctEntityDiretorTo As List(Of NawaDevDAL.goAML_Trn_Director_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listTrnAcctEntityDiretorTo")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Director_NotReported))
            Session("goAML_NotReportedDetail.listTrnAcctEntityDiretorTo") = value
        End Set
    End Property
    Public Property listTrnEntityDiretorTo As List(Of NawaDevDAL.goAML_Trn_Director_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listTrnEntityDiretorTo")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Director_NotReported))
            Session("goAML_NotReportedDetail.listTrnEntityDiretorTo") = value
        End Set
    End Property
    Public Property listTrnAcctSignatory As List(Of NawaDevDAL.goAML_Trn_acc_Signatory_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listTrnAcctSignatory")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_acc_Signatory_NotReported))
            Session("goAML_NotReportedDetail.listTrnAcctSignatory") = value
        End Set
    End Property
    Public Property listTrnAcctSignatoryTo As List(Of NawaDevDAL.goAML_Trn_acc_Signatory_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listTrnAcctSignatoryTo")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_acc_Signatory_NotReported))
            Session("goAML_NotReportedDetail.listTrnAcctSignatoryTo") = value
        End Set
    End Property
    Public Property listPhonedirectorentityfromaccount As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhonedirectorentityfromaccount")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhonedirectorentityfromaccount") = value
        End Set
    End Property
    Public Property listPhonedirectorentityToAccount As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhonedirectorentityToAccount")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhonedirectorentityToAccount") = value
        End Set
    End Property
    Public Property listPhoneEmployerDirectorEntityfromaccount As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityfromaccount")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityfromaccount") = value
        End Set
    End Property
    Public Property listPhoneEmployerDirectorEntityToAccount As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityToAccount")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityToAccount") = value
        End Set
    End Property

    Public Property listPhonedirectorentityfrom As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhonedirectorentityfrom")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhonedirectorentityfrom") = value
        End Set
    End Property
    Public Property listPhoneEmployerDirectorEntityfrom As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityfrom")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityfrom") = value
        End Set
    End Property
    Public Property listPhonedirectorentityTo As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhonedirectorentityTo")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhonedirectorentityTo") = value
        End Set
    End Property
    Public Property listPhoneEmployerDirectorEntityTo As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityTo")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityTo") = value
        End Set
    End Property
    Public Property listPhoneEntityToAccount As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEntityToAccount")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEntityToAccount") = value
        End Set
    End Property
    Public Property listAddressdirectorentityfromaccount As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressdirectorentityfromaccount")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressdirectorentityfromaccount") = value
        End Set
    End Property
    Public Property listAddressdirectorentityToAccount As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressdirectorentityToAccount")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressdirectorentityToAccount") = value
        End Set
    End Property
    Public Property listEmployerAddressdirectorentityfromaccount As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listEmployerAddressdirectorentityfromaccount")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listEmployerAddressdirectorentityfromaccount") = value
        End Set
    End Property
    Public Property listEmployerAddressdirectorentityToAccount As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listEmployerAddressdirectorentityToAccount")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listEmployerAddressdirectorentityToAccount") = value
        End Set
    End Property
    Public Property listAddressdirectorentityfrom As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressdirectorentityfrom")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressdirectorentityfrom") = value
        End Set
    End Property
    Public Property listEmployerAddressdirectorentityfrom As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listEmployerAddressdirectorentityfrom")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listEmployerAddressdirectorentityfrom") = value
        End Set
    End Property
    Public Property listAddressdirectorentityTo As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressdirectorentityTo")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressdirectorentityTo") = value
        End Set
    End Property
    Public Property listEmployerAddressdirectorentityTo As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listEmployerAddressdirectorentityTo")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listEmployerAddressdirectorentityTo") = value
        End Set
    End Property
    Public Property listIdentificationDirectorEntityAccountFrom As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listIdentificationDirectorEntityAccountFrom")

        End Get
        Set(value As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported))
            Session("goAML_NotReportedDetail.listIdentificationDirectorEntityAccountFrom") = value
        End Set
    End Property
    Public Property listIdentificationDirectorEntityAccountTo As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listIdentificationDirectorEntityAccountTo")

        End Get
        Set(value As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported))
            Session("goAML_NotReportedDetail.listIdentificationDirectorEntityAccountTo") = value
        End Set
    End Property
    Public Property listIdentificationDirectorEntityFrom As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listIdentificationDirectorEntityFrom")

        End Get
        Set(value As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported))
            Session("goAML_NotReportedDetail.listIdentificationDirectorEntityFrom") = value
        End Set
    End Property
    Public Property listIdentificationDirectorEntityTo As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listIdentificationDirectorEntityTo")

        End Get
        Set(value As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported))
            Session("goAML_NotReportedDetail.listIdentificationDirectorEntityTo") = value
        End Set
    End Property
    Public Property listPhoneSignatoryToAccount As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneSignatoryToAccount")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneSignatoryToAccount") = value
        End Set
    End Property
    Public Property listPhoneEmployerSignatoryToAccount As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEmployerSignatoryToAccount")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEmployerSignatoryToAccount") = value
        End Set
    End Property
    Public Property listPhoneSignatoryFromAccount As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneSignatoryFromAccount")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneSignatoryFromAccount") = value
        End Set
    End Property
    Public Property listPhoneEmpoyerSignatoryFromAccount As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEmpoyerSignatoryFromAccount")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEmpoyerSignatoryFromAccount") = value
        End Set
    End Property

    Public Property listAddressSignatoryFromAccount As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressSignatoryFromAccount")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressSignatoryFromAccount") = value
        End Set
    End Property
    Public Property listAddressEmployerSignatoryFromAccount As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressEmployerSignatoryFromAccount")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressEmployerSignatoryFromAccount") = value
        End Set
    End Property

    Public Property listAddressSignatorytoAccount As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressSignatorytoAccount")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressSignatorytoAccount") = value
        End Set
    End Property
    Public Property listAddressEmployerSignatoryToAccount As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressEmployerSignatoryToAccount")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressEmployerSignatoryToAccount") = value
        End Set
    End Property
    Public Property listIdentificationSignatoryFromAccount As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listIdentificationSignatoryFromAccount")

        End Get
        Set(value As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported))
            Session("goAML_NotReportedDetail.listIdentificationSignatoryFromAccount") = value
        End Set
    End Property
    Public Property listIdentificationSignatoryToAccount As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listIdentificationSignatoryToAccount")

        End Get
        Set(value As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported))
            Session("goAML_NotReportedDetail.listIdentificationSignatoryToAccount") = value
        End Set
    End Property
#End Region

#Region "Load"
    Private Sub goAML_NotReportedDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer

            intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)


            If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Detail) Then
                Dim strIDCode As String = 1
                strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                Exit Sub
            End If
            If Not Ext.Net.X.IsAjaxRequest Then

                loadLocation()
                clearSession()
                LoadData()
                ColumnActionPosition()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub loadLocation()
        Dim location As New NawaDevDAL.goAML_Ref_Address
        location = NawaDevBLL.GlobalReportFunctionBLL.getAddressbyFKRefDetail(9)
        '--Update dari BTPN  If location IsNot Nothing Then karena ada issue kalo cuma cek not location.pk = nothing
        If location IsNot Nothing Then
            If Not location.PK_Customer_Address_ID = Nothing Then
                txt_addressType.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(location.Address_Type)
                txt_address.Text = location.Address
                txt_town.Text = location.Town
                txt_city.Text = location.City
                txt_zip.Text = location.Zip
                txt_country.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(location.Country_Code)
                txt_state.Text = location.State
                txt_comments.Text = location.Comments
            End If
        End If
    End Sub

    Protected Sub clearSession()
        NotReportClass = New NawaDevBLL.NotReported
        listTransactionClass = New List(Of NawaDevBLL.Transaction_NotReported)
        TransactionClass = New NawaDevBLL.Transaction_NotReported
        listTransaction = New List(Of NawaDevDAL.goAML_Transaction_NotReported)
        DirectorClass = New NawaDevBLL.DirectorClass_NotReported
        SignatoryClass = New NawaDevBLL.SignatoryClass_NotReported
        DirectorEntityAccountPartyClass = New NawaDevBLL.DirectorEntityAccountPartyClass_NotReported
        DirectorEntityPartyClass = New NawaDevBLL.DirectorEntityPartyClass_NotReported
        SignatoryAccountPartyClass = New NawaDevBLL.SignatoryAccountPartyClass_NotReported

        listDirectorEntityparty = New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_NotReported)
        listAddressDirectorEntityParty = New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported)
        listAddressEmployerDirectorEntityParty = New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported)
        listPhoneDirectorEntityParty = New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported)
        listPhoneEmployerDirectorEntityParty = New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported)
        listIdentificationDirectorEntityParty = New List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)

        listDirectorentityaccountparty = New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_NotReported)
        listPhoneDirectorEntityAccountParty = New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported)
        listPhoneEmployerDirectorEntityAccountParty = New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported)
        listAddressDirectorEntityAccountParty = New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported)
        listaddressEmployerDirectorEntityAccountParty = New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported)
        listIdentificationDirectorEntityaccountparty = New List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)

        listSignatoryAccountParty = New List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory_NotReported)
        listAddressSignatoryAccountParty = New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported)
        listAddressEmployerSignatoryAccountParty = New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported)
        listPhoneSignatoryAccountParty = New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported)
        listPhoneEmployerSignatoryAccountParty = New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported)
        listIdentificationSignatoryAccountparty = New List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)

        listTrnAcctEntityDiretor = New List(Of NawaDevDAL.goAML_Trn_Director_NotReported)
        listTrnEntityDiretor = New List(Of NawaDevDAL.goAML_Trn_Director_NotReported)
        listTrnAcctEntityDiretorTo = New List(Of NawaDevDAL.goAML_Trn_Director_NotReported)
        listTrnEntityDiretorTo = New List(Of NawaDevDAL.goAML_Trn_Director_NotReported)

        listTrnAcctSignatory = New List(Of NawaDevDAL.goAML_Trn_acc_Signatory_NotReported)
        listTrnAcctSignatoryTo = New List(Of NawaDevDAL.goAML_Trn_acc_Signatory_NotReported)

        listPhonedirectorentityfromaccount = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhonedirectorentityToAccount = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhoneEmployerDirectorEntityfromaccount = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhoneEmployerDirectorEntityToAccount = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhonedirectorentityfrom = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhoneEmployerDirectorEntityfrom = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhonedirectorentityTo = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhoneEmployerDirectorEntityTo = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhoneEntityToAccount = New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone_NotReported)

        listAddressdirectorentityfromaccount = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listAddressdirectorentityToAccount = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listEmployerAddressdirectorentityfromaccount = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listEmployerAddressdirectorentityToAccount = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listAddressdirectorentityfrom = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listEmployerAddressdirectorentityfrom = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listAddressdirectorentityTo = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listEmployerAddressdirectorentityTo = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)

        listIdentificationDirectorEntityAccountFrom = New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        listIdentificationDirectorEntityAccountTo = New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        listIdentificationDirectorEntityFrom = New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        listIdentificationDirectorEntityTo = New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)

        listPhoneSignatoryToAccount = New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)
        listPhoneEmployerSignatoryToAccount = New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)
        listPhoneSignatoryFromAccount = New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)
        listPhoneEmpoyerSignatoryFromAccount = New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)

        listAddressSignatoryFromAccount = New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)
        listAddressEmployerSignatoryFromAccount = New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)
        listAddressSignatorytoAccount = New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)
        listAddressEmployerSignatoryToAccount = New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)

        listIdentificationSignatoryFromAccount = New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        listIdentificationSignatoryToAccount = New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
    End Sub

    Protected Sub clearSessionTransaction()
        TransactionClass = New NawaDevBLL.Transaction_NotReported
        DirectorClass = New NawaDevBLL.DirectorClass_NotReported
        SignatoryClass = New NawaDevBLL.SignatoryClass_NotReported
        DirectorEntityAccountPartyClass = New NawaDevBLL.DirectorEntityAccountPartyClass_NotReported
        DirectorEntityPartyClass = New NawaDevBLL.DirectorEntityPartyClass_NotReported
        SignatoryAccountPartyClass = New NawaDevBLL.SignatoryAccountPartyClass_NotReported

        listDirectorEntityparty = New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_NotReported)
        listAddressDirectorEntityParty = New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported)
        listAddressEmployerDirectorEntityParty = New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported)
        listPhoneDirectorEntityParty = New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported)
        listPhoneEmployerDirectorEntityParty = New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported)
        listIdentificationDirectorEntityParty = New List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)

        listDirectorentityaccountparty = New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_NotReported)
        listPhoneDirectorEntityAccountParty = New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported)
        listPhoneEmployerDirectorEntityAccountParty = New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported)
        listAddressDirectorEntityAccountParty = New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported)
        listaddressEmployerDirectorEntityAccountParty = New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported)
        listIdentificationDirectorEntityaccountparty = New List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)

        listSignatoryAccountParty = New List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory_NotReported)
        listAddressSignatoryAccountParty = New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported)
        listAddressEmployerSignatoryAccountParty = New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported)
        listPhoneSignatoryAccountParty = New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported)
        listPhoneEmployerSignatoryAccountParty = New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported)
        listIdentificationSignatoryAccountparty = New List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)

        listTrnAcctEntityDiretor = New List(Of NawaDevDAL.goAML_Trn_Director_NotReported)
        listTrnEntityDiretor = New List(Of NawaDevDAL.goAML_Trn_Director_NotReported)
        listTrnAcctEntityDiretorTo = New List(Of NawaDevDAL.goAML_Trn_Director_NotReported)
        listTrnEntityDiretorTo = New List(Of NawaDevDAL.goAML_Trn_Director_NotReported)

        listTrnAcctSignatory = New List(Of NawaDevDAL.goAML_Trn_acc_Signatory_NotReported)
        listTrnAcctSignatoryTo = New List(Of NawaDevDAL.goAML_Trn_acc_Signatory_NotReported)

        listPhonedirectorentityfromaccount = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhonedirectorentityToAccount = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhoneEmployerDirectorEntityfromaccount = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhoneEmployerDirectorEntityToAccount = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhonedirectorentityfrom = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhoneEmployerDirectorEntityfrom = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhonedirectorentityTo = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhoneEmployerDirectorEntityTo = New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        listPhoneEntityToAccount = New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone_NotReported)

        listAddressdirectorentityfromaccount = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listAddressdirectorentityToAccount = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listEmployerAddressdirectorentityfromaccount = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listEmployerAddressdirectorentityToAccount = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listAddressdirectorentityfrom = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listEmployerAddressdirectorentityfrom = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listAddressdirectorentityTo = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        listEmployerAddressdirectorentityTo = New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)

        listIdentificationDirectorEntityAccountFrom = New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        listIdentificationDirectorEntityAccountTo = New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        listIdentificationDirectorEntityFrom = New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        listIdentificationDirectorEntityTo = New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)

        listPhoneSignatoryToAccount = New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)
        listPhoneEmployerSignatoryToAccount = New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)
        listPhoneSignatoryFromAccount = New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)
        listPhoneEmpoyerSignatoryFromAccount = New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)

        listAddressSignatoryFromAccount = New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)
        listAddressEmployerSignatoryFromAccount = New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)
        listAddressSignatorytoAccount = New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)
        listAddressEmployerSignatoryToAccount = New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)

        listIdentificationSignatoryFromAccount = New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        listIdentificationSignatoryToAccount = New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
    End Sub

    Protected Sub LoadData()
        Dim IDData As String = Request.Params("ID")
        Dim reportClass As New NawaDevBLL.Report

        IDReq = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)

        NotReportClass.objReport = NawaDevBLL.NotReportedBLL.getNotReportedByPk(IDReq)

        If getTransaksiORActivity(NotReportClass.objReport.Report_Code) = "TRN" Then
            TransactionGridPanel.Hidden = False
            ActivityGridPanel.Hidden = True

            listTransaction = NawaDevBLL.NotReportedBLL.getListTransactionById(IDReq)

            If listTransaction.Count > 0 Then
                bindTransaction(StoreTransaction, listTransaction)
            End If
        ElseIf getTransaksiORActivity(NotReportClass.objReport.Report_Code) = "ACT" Then
            TransactionGridPanel.Hidden = True
            ActivityGridPanel.Hidden = False
            PanelReportIndicator.Hidden = False

            listActivity = NawaDevBLL.NotReportedBLL.getActReportPartyTypeByFK(IDReq)

            If listActivity.Count > 0 Then
                bindActivity(StoreActifity, listActivity)
            End If
        End If

        Dim totalAmmount As Decimal = 0
        For Each item In listTransaction
            totalAmmount += item.Amount_Local
        Next
        txt_TotalAmountTransactions.Text = totalAmmount.ToString("#,###.00")
        txt_totalAmountTransaction_value.Text = totalAmmount


        With NotReportClass.objReport
            txt_id.Text = .Pk_GoAML_Report_NotReported
            txt_rentityID.Text = .rentity_id
            txt_rentityBranch.Text = .Rentity_Branch
            Cmb_transactionType.Text = NawaDevBLL.GlobalReportFunctionBLL.getJenisTransactionByCode(.FK_Transaction_Type_ID)
            cmb_submisionCode.Text = NawaDevBLL.GlobalReportFunctionBLL.getSubmissionTypeByCode(.Submission_Code)
            Cmb_jenisLaporan.Text = NawaDevBLL.GlobalReportFunctionBLL.getJenisLaporanByCode(.Report_Code)
            txt_entity_reference.Text = .Entity_Reference
            txt_fiu_ref_number.Text = .Fiu_Ref_Number
            If .Submission_Date IsNot Nothing Then
                df_submissionDate.Text = .Submission_Date.Value.ToString("dd-MMM-yyyy")
            End If
            txt_currencyLocal.Text = .Currency_Code_Local
            txt_catatan.Text = .Reason
            txt_action.Text = .Action
            If .FK_Report_Type_ID IsNot Nothing Then
                Cmb_reportType.Text = NawaDevBLL.GlobalReportFunctionBLL.getReportTypebyPK(.FK_Report_Type_ID)
            End If
            txt_reportyValue.Text = .UnikReference

            If .Report_Code = "LTKMT" Or .Report_Code = "LAPT" Or .Report_Code = "LTKM" Then
                PanelReportIndicator.Hidden = False
                NotReportClass.listIndicator = getListIndicatorByFk(.Pk_GoAML_Report_NotReported)
                If NotReportClass.listIndicator.Count > 0 Then
                    bindReportIndicator(StoreReportIndicatorData, NotReportClass.listIndicator)
                End If
            Else
                PanelReportIndicator.Hidden = True
            End If
        End With

    End Sub
#End Region

#Region "GridCommand"

    Protected Sub GridcommandReportIndicator(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editIndicator(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editIndicator(id As String)
        Try
            Cmb_reportIndicator.Clear()

            WindowReportIndicator.Hidden = False
            Dim reportIndicator As NawaDevDAL.goAML_Report_Indicator_NotReported
            reportIndicator = NotReportClass.listIndicator.Where(Function(x) x.PK_Report_Indicator_NotReported = id).FirstOrDefault

            Cmb_reportIndicator.Text = NawaDevBLL.NotReportedBLL.getReportIndicatorByKode(reportIndicator.FK_Indicator)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandWindowAddressEntityParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEntityparty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEntityparty(id As String)
        Try

            WindowAddressEntityParty.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Party_Entity_Address_NotReported
            addressTemp = TransactionClass.listAddressEntityparty.Where(Function(x) x.PK_Trn_Party_Entity_Address_ID_NotReported = id).FirstOrDefault

            txtW_AddressEntityAddressParty.Text = addressTemp.Address
            txtW_cityEntityAddressParty.Text = addressTemp.City
            txtW_CommentEntityAddressParty.Text = addressTemp.Comments
            txtW_stateEntityAddressParty.Text = addressTemp.State
            txtW_townEntityAddressParty.Text = addressTemp.Town
            txtW_zipEntityAddressParty.Text = addressTemp.Zip
            cmbW_addressTypeAddressEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEntityAddressParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandPhoneEntityParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEntityParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneEntityParty(id As String)
        Try
            WindowPhoneEntityParty.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Trn_Party_Entity_Phone_NotReported
            phoneTemp = TransactionClass.listPhoneEntityParty.Where(Function(x) x.PK_Trn_Party_Entity_Phone_ID_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEntityParty.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEntityParty.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEntityParty.Text = phoneTemp.tph_extension
            txtW_commentPhoneEntityParty.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandDirectorEntityParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editDirectorEntityparty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editDirectorEntityparty(id As String)
        Try
            WindowDirectorEntityParty.Hidden = False

            Dim director As New NawaDevDAL.goAML_Trn_Par_Entity_Director_NotReported
            director = listDirectorEntityparty.Where(Function(x) x.PK_Trn_Par_Entity_Director_NotReported_ID = id).FirstOrDefault

            With director
                Cmb_genderDirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                txt_titleDirectorEntityParty.Text = .Title
                txt_lastNameDirectorEntityParty.Text = .Last_Name
                If Not .BirthDate = DateTime.MinValue Then
                    df_birthDateDirectorEntityParty.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                End If
                txt_birthPlaceDirectorEntityParty.Text = .Birth_Place
                txt_motherNameDirectorEntityParty.Text = .Mothers_Name
                txt_aliasDirectorEntityParty.Text = .Alias
                txt_ssnDirectorEntityParty.Text = .SSN
                txt_passportNumberDirectorEntityParty.Text = .Passport_Number
                txt_passportCountryDirectorEntityParty.Text = .Passport_Country
                txt_idNumberFromDirectorEntityAccount.Text = .Id_Number
                Cmb_nationality1DirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                Cmb_nationality2DirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                Cmb_nationality3DirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                Cmb_residenceDirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
                txt_emailDirectorEntityParty.Text = .Email
                txt_emailDirectorEntityParty2.Text = .Email2
                txt_emailDirectorEntityParty3.Text = .Email3
                txt_emailDirectorEntityParty4.Text = .Email4
                txt_emailDirectorEntityParty5.Text = .Email5
                txt_occupationDirectorEntityParty.Text = .Occupation
                txt_employerNameDirectorEntityParty.Text = .Employer_Name
                If .Deceased Then
                    DisplayField_DeceaseDirectorEntityParty.Text = "Ya"
                    If Not .Deceased_Date = DateTime.MinValue Then
                        df_deceased_dateDirectorEntityParty.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                    End If
                Else
                    DisplayField_DeceaseDirectorEntityParty.Text = "Tidak"
                End If
                txt_taxNumberDirectorEntityParty.Text = .Tax_Number
                If .Tax_Reg_Number Then
                    DisplayField_PEPDirectorEntityParty.Text = "Ya"
                Else
                    DisplayField_PEPDirectorEntityParty.Text = "Tidak"
                End If

                txt_source_of_wealthDirectorEntityParty.Text = .Source_Of_Wealth
                txt_CommentDirectorEntityParty.Text = .Comment
                cmb_roleDirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.role)

                listAddressDirectorEntityParty = NawaDevBLL.NotReportedBLL.getListTransactionEntityDirectorAddressByIdParty(.PK_Trn_Par_Entity_Director_NotReported_ID, 0)
                listPhoneDirectorEntityParty = NawaDevBLL.NotReportedBLL.getListTransactionEntityDirectorPhoneByIdParty(.PK_Trn_Par_Entity_Director_NotReported_ID, 0)
                listAddressEmployerDirectorEntityParty = NawaDevBLL.NotReportedBLL.getListTransactionEntityDirectorAddressByIdParty(.PK_Trn_Par_Entity_Director_NotReported_ID, 1)
                listPhoneEmployerDirectorEntityParty = NawaDevBLL.NotReportedBLL.getListTransactionEntityDirectorPhoneByIdParty(.PK_Trn_Par_Entity_Director_NotReported_ID, 1)
                listIdentificationDirectorEntityParty = NawaDevBLL.NotReportedBLL.getListTransactionIdentificationByIdParty(.PK_Trn_Par_Entity_Director_NotReported_ID, 4, NotReportClass.objReport.Pk_GoAML_Report_NotReported)


                If listAddressDirectorEntityParty.Count > 0 Then
                    bindAddressDirectorEntityparty(StoreAddressDirectorEntityParty, listAddressDirectorEntityParty)
                End If
                If listPhoneDirectorEntityParty.Count > 0 Then
                    bindPhoneDirectorEntityParty(StorePhoneDirectorEntityParty, listPhoneDirectorEntityParty)
                End If
                If listAddressEmployerDirectorEntityParty.Count > 0 Then
                    bindAddressDirectorEntityparty(StoreAddressEmployerDirectorEntityParty, listAddressEmployerDirectorEntityParty)
                End If
                If listPhoneEmployerDirectorEntityParty.Count > 0 Then
                    bindPhoneDirectorEntityParty(StorePhoneEmployerDirectorEntityParty, listPhoneEmployerDirectorEntityParty)
                End If
                If listIdentificationDirectorEntityParty.Count > 0 Then
                    bindIdentificationParty(StoreIdentificationDirectorEntityParty, listIdentificationDirectorEntityParty)
                End If
            End With

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressDirectorEntityParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressDirectorEntityParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressDirectorEntityParty(id As String)
        Try
            WindowDirectorEntityParty.Hidden = True
            WindowAddressDirectorEntityParty.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported
            addressTemp = listAddressDirectorEntityParty.Where(Function(x) x.PK_Trn_Par_Entity_Address_NotReported_ID = id).FirstOrDefault

            txtW_AddressDirectorEntityParty.Text = addressTemp.Address
            txtW_cityDirectorEntityAddressParty.Text = addressTemp.City
            txtW_CommentDirectorEntityAddressParty.Text = addressTemp.Comments
            txtW_stateDirectorEntityAddressParty.Text = addressTemp.State
            txtW_townDirectorEntityAddressParty.Text = addressTemp.Town
            txtW_zipDirectorEntityAddressParty.Text = addressTemp.Zip
            cmbW_addressTypeAddressDirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeDirectorEntityAddressParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneDirectorEntityParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneDirectorEntityParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneDirectorEntityParty(id As String)
        Try
            WindowDirectorEntityParty.Hidden = True
            WindowPhoneDirectorEntityParty.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported
            phoneTemp = listPhoneDirectorEntityParty.Where(Function(x) x.PK_Trn_Par_Entity_Director_Phone_NotReported_ID = id).FirstOrDefault

            CmbW_tph_contact_typePhoneDirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneDirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneDirectorEntityParty.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneDirectorEntityParty.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneDirectorEntityParty.Text = phoneTemp.tph_extension
            txtW_commentPhoneDirectorEntityParty.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEmployerDirectorEntityParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEmployerDirectorEntityParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEmployerDirectorEntityParty(id As String)
        Try
            WindowDirectorEntityParty.Hidden = True
            Dim addressTemp As NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported
            addressTemp = listAddressEmployerDirectorEntityParty.Where(Function(x) x.PK_Trn_Par_Entity_Address_NotReported_ID = id).FirstOrDefault

            txtW_AddressEmployerDirectorEntityParty.Text = addressTemp.Address
            txtW_cityEmployerDirectorEntityParty.Text = addressTemp.City
            txtW_CommentEmployerDirectorEntityParty.Text = addressTemp.Comments
            txtW_stateEmployerDirectorEntityParty.Text = addressTemp.State
            txtW_townEmployerDirectorEntityParty.Text = addressTemp.Town
            txtW_zipEmployerDirectorEntityParty.Text = addressTemp.Zip
            cmbW_addressTypeAddressEmployerDirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEmployerDirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)
            WindowAddressEmployerDirectorEntityParty.Hidden = False

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployerDirectorEntityParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditphoneEmployerDirectorEntityParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditphoneEmployerDirectorEntityParty(id As String)
        Try
            WindowDirectorEntityParty.Hidden = True
            Dim phoneTemp As NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported
            phoneTemp = listPhoneEmployerDirectorEntityParty.Where(Function(x) x.PK_Trn_Par_Entity_Director_Phone_NotReported_ID = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEmployerDirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEmployerDirectorEntityIDParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEmployerDirectorEntityIDParty.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEmployerDirectorEntityIDParty.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEmployerDirectorEntityIDParty.Text = phoneTemp.tph_extension
            txtW_commentPhoneEmployerDirectorEntityIDParty.Text = phoneTemp.comments
            WindowPhoneEmployerDirectorEntityParty.Hidden = False

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandIdentificationDirectorEntityParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editIdentificationDirectorEntityParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editIdentificationDirectorEntityParty(id As String)
        Try
            WindowDirectorEntityParty.Hidden = True
            WindowIdentificationDirectorEntityParty.Hidden = False
            Dim identification As NawaDevDAL.goAML_Transaction_Party_Identification_NotReported
            identification = listIdentificationDirectorEntityParty.Where(Function(x) x.PK_Trn_Par_Identification_NotReported_ID = id).FirstOrDefault

            Cmb_jenisIdentitasDirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(identification.Type)
            txt_numberIdentificationDirectorEntityParty.Text = identification.Number
            If Not identification.Issue_Date = DateTime.MinValue Then
                df_IssueDateIdentificationDirectorEntityParty.Text = identification.Issue_Date.Value.ToString("dd-MMM-yy")
            End If
            If Not identification.Expiry_Date = DateTime.MinValue Then
                df_expiry_dateIdentificationDirectorEntityParty.Text = identification.Expiry_Date.Value.ToString("dd-MMM-yy")
            End If

            txt_issueByDirectorEntityParty.Text = identification.Issued_By
            Cmb_countryIdentificationDirectorEntityParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(identification.Issued_Country)
            Txt_commentIdentificationDirectorEntityParty.Text = identification.Identification_Comment

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhonePersonParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhonepersonparty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhonepersonparty(id As String)
        Try
            WindowPhonepersonParty.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Trn_Party_Person_Phone_NotReported
            phoneTemp = TransactionClass.listPhonePersonParty.Where(Function(x) x.PK_Trn_Party_Person_Phone_NotReported_ID = id).FirstOrDefault

            CmbW_tph_contact_typePhonepersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhonepersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhonepersonParty.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhonepersonParty.Text = phoneTemp.tph_number
            txtW_tphExtentionPhonepersonParty.Text = phoneTemp.tph_extension
            txtW_commentPhonepersonParty.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddresspersonParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressPersonParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressPersonParty(id As String)
        Try

            WindowAddressPersonParty.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Party_Person_Address_NotReported
            addressTemp = TransactionClass.listAddressPersonParty.Where(Function(x) x.PK_Trn_Party_Person_Address_NotReported_ID = id).FirstOrDefault

            txtW_AddressPersonParty.Text = addressTemp.Address
            txtW_cityPersonParty.Text = addressTemp.City
            txtW_CommentPersonParty.Text = addressTemp.Comments
            txtW_statePersonParty.Text = addressTemp.State
            txtW_townPersonParty.Text = addressTemp.Town
            txtW_zipPersonParty.Text = addressTemp.Zip
            cmbW_addressTypeAddressPersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodePersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEmployerPersonIDParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEmployerPersonParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEmployerPersonParty(ID As String)
        Try

            WindowAddressEmployerPersonParty.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Party_Person_Address_NotReported
            addressTemp = TransactionClass.listAddressEmployerPersonParty.Where(Function(x) x.PK_Trn_Party_Person_Address_NotReported_ID = ID).FirstOrDefault

            txtW_AddressEmployerPersonIDParty.Text = addressTemp.Address
            txtW_cityEmployerPersonIDParty.Text = addressTemp.City
            txtW_CommentEmployerPersonIDParty.Text = addressTemp.Comments
            txtW_stateEmployerPersonIDParty.Text = addressTemp.State
            txtW_townEmployerPersonIDParty.Text = addressTemp.Town
            txtW_zipEmployerPersonIDParty.Text = addressTemp.Zip
            cmbW_addressTypeAddressEmployerIDPersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEmployerPersonIDParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployerPersonIDParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneemployerpersonparty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneemployerpersonparty(id As String)
        Try
            WindowPhoneEmployerPersonIDParty.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Trn_Party_Person_Phone_NotReported
            phoneTemp = TransactionClass.listPhoneEmployerPersonParty.Where(Function(x) x.PK_Trn_Party_Person_Phone_NotReported_ID = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEmployerPersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEmployerPersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEmployerPersonParty.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEmployerPersonParty.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEmployerPersonParty.Text = phoneTemp.tph_extension
            txtW_commentPhoneEmployerPersonParty.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandIdentificationPersonParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editIdentificationPersonparty(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editIdentificationPersonparty(ID As String, command As String)
        Try

            WindowIdentificationPersonParty.Hidden = False
            Dim identification As NawaDevDAL.goAML_Transaction_Party_Identification_NotReported
            identification = TransactionClass.listIdentificationPersonParty.Where(Function(x) x.PK_Trn_Par_Identification_NotReported_ID = ID).FirstOrDefault

            Cmb_jenisIdentitasPersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(identification.Type)
            txt_numberIdentificationPersonParty.Text = identification.Number
            If Not identification.Issue_Date = DateTime.MinValue Then
                df_IssueDateIdentificationPersonParty.Text = identification.Issue_Date.Value.ToString("dd-MMM-yy")
            End If
            If Not identification.Expiry_Date = DateTime.MinValue Then
                df_expiry_dateIdentificationpersonParty.Text = identification.Expiry_Date.Value.ToString("dd-MMM-yy")
            End If
            txt_issueByPersonParty.Text = identification.Issued_By
            Cmb_countryIdentificationPersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(identification.Issued_Country)
            Txt_commentIdentificationPersonParty.Text = identification.Identification_Comment

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEntityAccountparty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEntityAccountParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneEntityAccountParty(id As String)
        Try
            WindowPhoneEntityAccountparty.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_trn_par_acc_Entity_Phone_NotReported
            phoneTemp = TransactionClass.listPhoneAccountEntityParty.Where(Function(x) x.PK_trn_Par_Acc_Entity_Phone_NotReported_ID = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEntityAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEntityAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEntityAccountparty.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEntityAccountparty.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEntityAccountparty.Text = phoneTemp.tph_extension
            txtW_commentPhoneEntityAccountparty.Text = phoneTemp.comments
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEntityAccountparty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEntityAccountParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEntityAccountParty(id As String)
        Try

            WindowAddressEntityAccountparty.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_par_Acc_Entity_Address_NotReported
            addressTemp = TransactionClass.listAddresAccountEntityParty.Where(Function(x) x.PK_Trn_Par_Acc_Entity_Address_NotReported_ID = id).FirstOrDefault

            txtW_AddressEntityAddressAccountparty.Text = addressTemp.Address
            txtW_cityEntityAddressAccountparty.Text = addressTemp.City
            txtW_CommentEntityAddressAccountparty.Text = addressTemp.Comments
            txtW_stateEntityAddressAccountparty.Text = addressTemp.State
            txtW_townEntityAddressAccountparty.Text = addressTemp.Town
            txtW_zipEntityAddressAccountparty.Text = addressTemp.Zip
            cmbW_addressTypeAddressEntityAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEntityAddressAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandDirectorEntityAccountparty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editDirectorentityaccountparty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editDirectorentityaccountparty(id As String)
        Try
            WindowDirectorEntityAccountParty.Hidden = False

            Dim director As New NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_NotReported
            director = listDirectorentityaccountparty.Where(Function(x) x.PK_Trn_Par_Acc_Ent_Dir_NotReported_ID = id).FirstOrDefault

            With director

                Cmb_genderDirectorEntityAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                txt_titleDirectorEntityAccountParty.Text = .Title
                txt_lastNameDirectorEntityAccountParty.Text = .Last_Name
                If .BirthDate IsNot Nothing Then
                    df_birthDateDirectorEntityAccountParty.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                End If
                txt_birthPlaceDirectorEntityAccountParty.Text = .Birth_Place
                txt_motherNameDirectorEntityAccountParty.Text = .Mothers_Name
                txt_aliasDirectorEntityAccountParty.Text = .Alias
                txt_ssnDirectorEntityAccountParty.Text = .SSN
                txt_passportNumberDirectorEntityAccountParty.Text = .Passport_Number
                txt_passportCountryDirectorEntityAccountParty.Text = .Passport_Country
                txt_idNumberPartyDirectorentityAccountParty.Text = .Id_Number
                Cmb_nationality1DirectorEntityAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                Cmb_nationality2DirectorEntityAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                Cmb_nationality3DirectorEntityAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                Cmb_residenceDirectorEntityAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
                txt_emailDirectorEntityAccountParty.Text = .Email
                txt_emailDirectorEntityAccountParty2.Text = .Email2
                txt_emailDirectorEntityAccountParty3.Text = .Email3
                txt_emailDirectorEntityAccountParty4.Text = .Email4
                txt_emailDirectorEntityAccountParty5.Text = .Email5
                txt_occupationDirectorEntityAccountParty.Text = .Occupation
                txt_employerNameDirectorEntityAccountParty.Text = .Employer_Name
                If .Deceased Then
                    DisplayField_DeceaseDirectorEntityAccountParty.Text = "Ya"
                    df_deceased_dateDirectorEntityAccountParty.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                Else
                    DisplayField_DeceaseDirectorEntityAccountParty.Text = "Tidak"
                End If

                txt_taxNumberDirectorEntityAccountParty.Text = .Tax_Number
                If .Tax_Reg_Number Then
                    DisplayField_PEPDirectorEntityAccountParty.Text = "ya"
                Else
                    DisplayField_PEPDirectorEntityAccountParty.Text = "Tidak"
                End If

                txt_source_of_wealthDirectorEntityAccountParty.Text = .Source_Of_Wealth
                txt_CommentDirectorEntityAccountParty.Text = .Comment
                Cmb_roleTypeDirectorAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getPeranDalamKorporasiByKode(.role)

                listAddressDirectorEntityAccountParty = getListTransactionAccountEntityDirectorAddressByIdParty(.PK_Trn_Par_Acc_Ent_Dir_NotReported_ID, 0)
                listPhoneDirectorEntityAccountParty = getListTransactionAccountEntityDirectorPhoneByIdParty(.PK_Trn_Par_Acc_Ent_Dir_NotReported_ID, 0)
                listaddressEmployerDirectorEntityAccountParty = getListTransactionAccountEntityDirectorAddressByIdParty(.PK_Trn_Par_Acc_Ent_Dir_NotReported_ID, 1)
                listPhoneEmployerDirectorEntityAccountParty = getListTransactionAccountEntityDirectorPhoneByIdParty(.PK_Trn_Par_Acc_Ent_Dir_NotReported_ID, 1)
                listIdentificationDirectorEntityaccountparty = getListTransactionIdentificationByIdParty(.PK_Trn_Par_Acc_Ent_Dir_NotReported_ID, 4, NotReportClass.objReport.Pk_GoAML_Report_NotReported)


                If listAddressDirectorEntityAccountParty.Count > 0 Then
                    bindAddressDirectorEntityAccountParty(StoreAddressDirectorEntityAccountParty, listAddressDirectorEntityAccountParty)
                End If
                If listPhoneDirectorEntityAccountParty.Count > 0 Then
                    bindPhoneDirectorEntityAccountParty(StorePhoneDirectorEntityAccountParty, listPhoneDirectorEntityAccountParty)
                End If
                If listaddressEmployerDirectorEntityAccountParty.Count > 0 Then
                    bindAddressDirectorEntityAccountParty(StoreAddressEmployerDirectorEntityAccountParty, listaddressEmployerDirectorEntityAccountParty)
                End If
                If listPhoneEmployerDirectorEntityAccountParty.Count > 0 Then
                    bindPhoneDirectorEntityAccountParty(StorePhoneEmployerDirectorEntityAccountParty, listPhoneEmployerDirectorEntityAccountParty)
                End If
                If listIdentificationDirectorEntityaccountparty.Count > 0 Then
                    bindIdentificationParty(StoreIdentificationDirectorEntityAccountParty, listIdentificationDirectorEntityaccountparty)
                End If

            End With

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneDirectorEntityAccountParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneDirectorEntityAccountParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneDirectorEntityAccountParty(id As String)
        Try
            WindowDirectorEntityAccountParty.Hidden = True
            WindowPhoneDirectorEntityAccountParty.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported
            phoneTemp = listPhoneDirectorEntityAccountParty.Where(Function(x) x.PK_Trn_Par_Acc_Ent_Dir_Phone_NotReported_ID = id).FirstOrDefault

            CmbW_tph_contact_typePhoneDirectorEntityAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneDirectorEntityAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneDirectorEntityAccountParty.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneDirectorEntityAccountParty.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneDirectorEntityAccountParty.Text = phoneTemp.tph_extension
            txtW_commentPhoneDirectorEntityAccountParty.Text = phoneTemp.comments


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressDirectorEntityAccountParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressDirectorEntityAccountParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressDirectorEntityAccountParty(id As String)
        Try
            WindowDirectorEntityAccountParty.Hidden = True
            WindowAddressDirectorEntityAccountParty.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported
            addressTemp = listAddressDirectorEntityAccountParty.Where(Function(x) x.PK_Trn_Par_Acc_Entity_Address_NotReported_ID = id).FirstOrDefault

            txtW_AddressDirectorEntityAccountParty.Text = addressTemp.Address
            txtW_cityDirectorEntityAddressAccountParty.Text = addressTemp.City
            txtW_CommentDirectorEntityAddressAccountParty.Text = addressTemp.Comments
            txtW_stateDirectorEntityAddressAccountParty.Text = addressTemp.State
            txtW_townDirectorEntityAddressAccountParty.Text = addressTemp.Town
            txtW_zipDirectorEntityAddressAccountParty.Text = addressTemp.Zip
            cmbW_addressTypeAddressDirectorEntityAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeDirectorEntityAddressAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEmployerDirectorEntityAccountParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editaddressEmployerDirectorEntityAccountParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editaddressEmployerDirectorEntityAccountParty(id As String)
        Try
            WindowDirectorEntityAccountParty.Hidden = True
            Dim addressTemp As NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported
            addressTemp = listaddressEmployerDirectorEntityAccountParty.Where(Function(x) x.PK_Trn_Par_Acc_Entity_Address_NotReported_ID = id).FirstOrDefault

            txtW_AddressEmployerDirectorEntityAccountParty.Text = addressTemp.Address
            txtW_cityEmployerDirectorEntityAccountParty.Text = addressTemp.City
            txtW_CommentEmployerDirectorEntityAccountParty.Text = addressTemp.Comments
            txtW_stateEmployerDirectorEntityAccountParty.Text = addressTemp.State
            txtW_townEmployerDirectorEntityAccountParty.Text = addressTemp.Town
            txtW_zipEmployerDirectorEntityAccountParty.Text = addressTemp.Zip
            cmbW_addressTypeAddressEmployerDirectorEntityIDAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEmployerDirectorEntityAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)
            WindowAddressEmployerDirectorEntityAccountParty.Hidden = False

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployerDirectorEntityAccountParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneemployerdirectorentityAccountParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneemployerdirectorentityAccountParty(id As String)
        Try
            WindowDirectorEntityAccountParty.Hidden = True
            WindowPhoneEmployerDirectorEntityIDAccountParty.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported
            phoneTemp = listPhoneEmployerDirectorEntityAccountParty.Where(Function(x) x.PK_Trn_Par_Acc_Ent_Dir_Phone_NotReported_ID = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEmployerDirectorEntityIDAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEmployerDirectorEntityIDAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEmployerDirectorEntityIDAccountParty.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEmployerDirectorEntityIDAccountParty.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEmployerDirectorEntityIDAccountParty.Text = phoneTemp.tph_extension
            txtW_commentPhoneEmployerDirectorEntityIDAccountParty.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandIdentificationDirectorEntityAccountParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editIdentificationDirectorEntityaccountparty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editIdentificationDirectorEntityaccountparty(id As String)
        Try

            WindowDirectorEntityAccountParty.Hidden = True
            WindowIdentificationDirectorEntityAccountParty.Hidden = False
            Dim identification As NawaDevDAL.goAML_Transaction_Party_Identification_NotReported
            identification = listIdentificationDirectorEntityaccountparty.Where(Function(x) x.PK_Trn_Par_Identification_NotReported_ID = id).FirstOrDefault

            Cmb_jenisIdentitasDirectorEntityAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(identification.Type)
            txt_numberIdentificationDirectorEntityAccountParty.Text = identification.Number
            If Not identification.Issue_Date = DateTime.MinValue Then
                df_IssueDateIdentificationDirectorEntityAccountParty.Text = identification.Issue_Date
            End If
            If Not identification.Expiry_Date = DateTime.MinValue Then
                df_expiry_dateIdentificationDirectorEntityAccountParty.Text = identification.Expiry_Date
            End If

            txt_issueByDirectorEntityAccountParty.Text = identification.Issued_By
            Cmb_countryIdentificationDirectorEntityAccountParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(identification.Issued_Country)
            Txt_commentIdentificationDirectorEntityAccountParty.Text = identification.Identification_Comment

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandSignatoryParty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editSignatoryParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editSignatoryParty(ID As String)
        WindowSignatoryParty.Hidden = False

        Dim signatory As New NawaDevDAL.goAML_Trn_par_acc_Signatory_NotReported
        signatory = listSignatoryAccountParty.Where(Function(x) x.PK_Trn_par_acc_Signatory_NotReported_ID = ID).FirstOrDefault

        With signatory
            Cmb_genderSignatoryAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
            txt_titleSignatoryAccountparty.Text = .Title
            txt_lastNameSignatoryAccountparty.Text = .Last_Name
            If .BirthDate IsNot Nothing Then
                df_birthDateSignatoryAccountparty.Text = .BirthDate.Value.ToString("dd-MMM-yy")
            End If
            txt_birthPlaceSignatoryAccountparty.Text = .Birth_Place
            txt_motherNameSignatoryAccountparty.Text = .Mothers_Name
            txt_aliasSignatoryAccountparty.Text = .Alias
            txt_ssnSignatoryAccountparty.Text = .SSN
            txt_passportNumberSignatoryAccountparty.Text = .Passport_Number
            txt_passportCountrySignatoryAccountparty.Text = .Passport_Country
            txt_idNumberFromSignatoryAccountParty.Text = .Id_Number
            Cmb_nationality1SignatoryAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
            Cmb_nationality2SignatoryAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
            Cmb_nationality3SignatoryAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
            Cmb_residenceSignatoryAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
            txt_emailSignatoryAccountparty.Text = .Email
            txt_emailSignatoryAccountparty2.Text = .Email2
            txt_emailSignatoryAccountparty3.Text = .Email3
            txt_emailSignatoryAccountparty4.Text = .Email4
            txt_emailSignatoryAccountparty5.Text = .Email5
            txt_occupationSignatoryAccountparty.Text = .Occupation
            txt_employerNameSignatoryAccountparty.Text = .Employer_Name
            If .Deceased Then
                DisplayField_DeceaseSignatoryAccountparty.Text = "Ya"
                If .Deceased_Date IsNot Nothing Then
                    df_deceased_dateSignatoryAccountparty.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                End If
            Else
                DisplayField_DeceaseSignatoryAccountparty.Text = "Tidak"
            End If
            txt_taxNumberSignatoryAccountparty.Text = .Tax_Number
            If .Tax_Reg_Number Then
                DisplayField_PEPSignatoryAccountparty.Text = "Ya"
            Else
                DisplayField_PEPSignatoryAccountparty.Text = "Tidak"
            End If
            txt_source_of_wealthSignatoryAccountparty.Text = .Source_Of_Wealth
            txt_CommentSignatoryAccountparty.Text = .Comment
            If .isPrimary = True Then
                isPrimaryAccountparty.Text = "Ya"
            ElseIf .isPrimary = False Then
                isPrimaryAccountparty.Text = "Tidak"
            End If
            Cmb_roleAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getRoleTypebyCode(.role)

            listAddressSignatoryAccountParty = NawaDevBLL.NotReportedBLL.getListTransactionAccountSignatoryAddressByIdParty(.PK_Trn_par_acc_Signatory_NotReported_ID, 0)
            listPhoneSignatoryAccountParty = NawaDevBLL.NotReportedBLL.getListTransactionAccountSignatoryPhoneByIdParty(.PK_Trn_par_acc_Signatory_NotReported_ID, 0)
            listAddressEmployerSignatoryAccountParty = NawaDevBLL.NotReportedBLL.getListTransactionAccountSignatoryAddressByIdParty(.PK_Trn_par_acc_Signatory_NotReported_ID, 1)
            listPhoneEmployerSignatoryAccountParty = NawaDevBLL.NotReportedBLL.getListTransactionAccountSignatoryPhoneByIdParty(.PK_Trn_par_acc_Signatory_NotReported_ID, 1)
            listIdentificationSignatoryAccountparty = NawaDevBLL.NotReportedBLL.getListTransactionIdentificationByIdParty(.PK_Trn_par_acc_Signatory_NotReported_ID, 2, NotReportClass.objReport.Pk_GoAML_Report_NotReported)


            If listAddressSignatoryAccountParty.Count > 0 Then
                bindAddressSignatoryAccountParty(StoreAddressSignatoryAccountparty, listAddressSignatoryAccountParty)
            End If
            If listPhoneSignatoryAccountParty.Count > 0 Then
                bindPhoneSignatoryAccountParty(StorePhoneSignatoryAccountparty, listPhoneSignatoryAccountParty)
            End If
            If listAddressEmployerSignatoryAccountParty.Count > 0 Then
                bindAddressSignatoryAccountParty(StoreAddressEmployerSignatoryIDAccountparty, listAddressEmployerSignatoryAccountParty)
            End If
            If listPhoneEmployerSignatoryAccountParty.Count > 0 Then
                bindPhoneSignatoryAccountParty(StorePhoneEmployerSignatoryIDAccountparty, listPhoneEmployerSignatoryAccountParty)
            End If
            If listIdentificationSignatoryAccountparty.Count > 0 Then
                bindIdentificationParty(StoreIdentificationSignatoryAccountparty, listIdentificationSignatoryAccountparty)
            End If
        End With

    End Sub
    Protected Sub GridcommandPhoneSignatoryAccountparty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneSignatoryAccountParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub EditPhoneSignatoryAccountParty(id As String)
        Try
            WindowSignatoryParty.Hidden = True
            WindowPhoneSignatoryAccountparty.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported
            phoneTemp = listPhoneSignatoryAccountParty.Where(Function(x) x.PK_trn_Par_Acc_Sign_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneSignatoryAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneSignatoryAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneSignatoryAccountparty.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneSignatoryAccountparty.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneSignatoryAccountparty.Text = phoneTemp.tph_extension
            txtW_commentPhoneSignatoryAccountparty.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressSignatoryAccountparty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressSignatoryAccountParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressSignatoryAccountParty(id As String)
        Try
            WindowSignatoryParty.Hidden = True
            WindowAddressSignatoryAccountparty.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported
            addressTemp = listAddressSignatoryAccountParty.Where(Function(x) x.PK_Trn_Par_Acc_sign_Address_NotReported_ID = id).FirstOrDefault

            txtW_AddressSignatoryAccountparty.Text = addressTemp.Address
            txtW_citySignatoryAddressAccountparty.Text = addressTemp.City
            txtW_CommentSignatoryAddressAccountparty.Text = addressTemp.Comments
            txtW_stateSignatoryAddressAccountparty.Text = addressTemp.State
            txtW_townSignatoryAddressAccountparty.Text = addressTemp.Town
            txtW_zipSignatoryAddressAccountparty.Text = addressTemp.Zip
            cmbW_addressTypeAddressSignatoryAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeSignatoryAddressAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEmployerSignatoryAccountparty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEmployerSignatoryAccountParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEmployerSignatoryAccountParty(id As String)
        Try
            WindowSignatoryParty.Hidden = True
            WindowAddressEmployerSignatoryIDAccountparty.Hidden = False

            Dim addressTemp As NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported
            addressTemp = listAddressEmployerSignatoryAccountParty.Where(Function(x) x.PK_Trn_Par_Acc_sign_Address_NotReported_ID = id).FirstOrDefault

            txtW_AddressEmployerSignatoryIDAccountparty.Text = addressTemp.Address
            txtW_cityEmployerSignatoryIDAccountparty.Text = addressTemp.City
            txtW_CommentEmployerSignatoryIDAccountparty.Text = addressTemp.Comments
            txtW_stateEmployerSignatoryIDAccountparty.Text = addressTemp.State
            txtW_townEmployerSignatoryIDAccountparty.Text = addressTemp.Town
            txtW_zipEmployerSignatoryIDAccountparty.Text = addressTemp.Zip
            cmbW_addressTypeAddressEmployerSignatoryIDAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEmployerSignatoryIDAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployerSignatoryIDAccountparty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEmployersignatoryAccountParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneEmployersignatoryAccountParty(id As String)
        Try
            WindowSignatoryParty.Hidden = True
            WindowPhoneEmployerSignatoryIDAccountparty.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported
            phoneTemp = listPhoneEmployerSignatoryAccountParty.Where(Function(x) x.PK_trn_Par_Acc_Sign_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEmployerSignatoryIDAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEmployerSignatoryIDAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEmployerSignatoryIDAccountparty.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEmployerSignatoryIDAccountparty.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEmployerSignatoryIDAccountparty.Text = phoneTemp.tph_extension
            txtW_commentPhoneEmployerSignatoryIDAccountparty.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandIdentificationSignatoryAccountparty(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editIdentificationsignatoryAccountParty(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editIdentificationsignatoryAccountParty(id As String)
        Try
            WindowSignatoryParty.Hidden = True
            WindowIdentificationSignatoryAccountparty.Hidden = False
            Dim identification As NawaDevDAL.goAML_Transaction_Party_Identification_NotReported
            identification = listIdentificationSignatoryAccountparty.Where(Function(x) x.PK_Trn_Par_Identification_NotReported_ID = id).FirstOrDefault

            Cmb_jenisIdentitasSignatoryAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(identification.Type)
            txt_numberIdentificationSignatoryAccountparty.Text = identification.Number
            If Not identification.Issue_Date = DateTime.MinValue Then
                df_IssueDateIdentificationSignatoryAccountparty.Text = identification.Issue_Date.Value.ToString("dd-MMM-yy")
            End If
            If Not identification.Expiry_Date = DateTime.MinValue Then
                df_expiry_dateIdentificationSignatoryAccountparty.Text = identification.Expiry_Date.Value.ToString("dd-MMM-yy")
            End If
            txt_issueBySignatoryAccountparty.Text = identification.Issued_By
            Cmb_countryIdentificationSignatoryAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(identification.Issued_Country)
            Txt_commentIdentificationSignatoryAccountparty.Text = identification.Identification_Comment
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandSignatoryFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailSignatoryEntityFromAccount(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandSignatoryTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailSignatoryEntityToAccount(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandDirectorEntityFromAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailDirectorEntityFromAccount(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandDirectorEntityToAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailDirectorEntityToAccount(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandDirectorEntityFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailDirectorEntityFrom(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandDirectorEntityTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailDirectorEntityTo(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandTransaksi(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                detailTransaction(ID)
                LoadDisplayTransaction(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub detailTransaction(id As Integer)
        transaction_Panel.Hidden = False
        ReportGeneralInformationPanel.Hidden = True
        infoValidationResultPanel.Hidden = True
        btnCancelReport.Hidden = True


        Dim objTransaction As goAML_Transaction_NotReported = listTransaction.Where(Function(x) x.Pk_goAML_Transaction_NotReported = id).FirstOrDefault
        'Dim objSubTransaction As NawaDevBLL.Transaction
        'objSubTransaction = NawaDevBLL.GlobalReportFunctionBLL.getTransactionByPK(id)

        'Conductor
        If objTransaction.usedConductor = True Then
            fieldsetConductor.Hidden = False

            TransactionClass.ObjConductor = getConductorById(objTransaction.Pk_goAML_Transaction_NotReported)
            TransactionClass.listObjPhoneConductor = getConductorPhoneById(TransactionClass.ObjConductor.PK_goAML_Trn_Conductor_NotReported, False)
            TransactionClass.listObjAddressConductor = getConductorAddressById(TransactionClass.ObjConductor.PK_goAML_Trn_Conductor_NotReported, False)
            TransactionClass.listObjPhoneEmployerConductor = getConductorPhoneById(TransactionClass.ObjConductor.PK_goAML_Trn_Conductor_NotReported, True)
            TransactionClass.listObjAddressEmployerConductor = getConductorAddressById(TransactionClass.ObjConductor.PK_goAML_Trn_Conductor_NotReported, True)
            TransactionClass.listObjIdentificationConductor = getListTransactionIdentificationById(TransactionClass.ObjConductor.PK_goAML_Trn_Conductor_NotReported, 3, 1)
        Else
            If objTransaction.usedConductor = False Then
                fieldsetConductor.Hidden = True
            End If

        End If

    End Sub

    Sub LoadDisplayTransaction(id As Integer)
        Dim objTransaction As goAML_Transaction_NotReported = listTransaction.Where(Function(x) x.Pk_goAML_Transaction_NotReported = id).FirstOrDefault
        With objTransaction
            'Cmb_jenisLaporanTransaksi.Text = NawaDevBLL.GlobalReportFunctionBLL.getJenisLaporanByCode(.FK_Jenis_Laporan_ID)
            txt_transactionNumber.Text = .TransactionNumber
            txt_internalRefNumber.Text = .Internal_Ref_Number
            txt_transactionLocation.Text = .Transaction_Location
            txt_transactionDescription.Text = .Transaction_Description
            If Not .Date_Transaction.Value = Nothing Then
                df_transaction.Text = .Date_Transaction.Value.ToString("dd-MMM-yy")
            End If
            txt_teller.Text = .Teller
            txt_authorized.Text = .Authorized
            'If .Late_Deposit = True Then
            '    DisplayField_LateDeposit.Text = "Ya"
            '    df_datePosting.Text = .Date_Posting.Value.ToString("dd-MMM-yy")
            'Else
            '    DisplayField_LateDeposit.Text = "Tidak"
            'End If
            'If .Value_Date IsNot Nothing Then
            '    df_valueDate.Text = .Value_Date.Value.ToString("dd-MMM-yy")
            'End If
            Cmb_transmodeCode.Text = NawaDevBLL.GlobalReportFunctionBLL.getJenisTransactionByCode(.Transmode_Code)
            txt_transmodeComent.Text = .Transmode_Comment
            txt_amountLocal.Text = .Amount_Local
            Cmb_transactionType.Text = NawaDevBLL.GlobalReportFunctionBLL.getTransactionTypebyPK(.FK_Transaction_Type)
            txt_transactionComent.Text = .Comments
            If .isSwift = True Then
                DisplayField_isSwift.Text = "Ya"
            Else
                DisplayField_isSwift.Text = "Tidak"
            End If

            'ConductorFrom
            If .usedConductor = True Then
                If Not TransactionClass.ObjConductor.PK_goAML_Trn_Conductor_NotReported = Nothing Then
                    With TransactionClass.ObjConductor
                        Cmb_genderConductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                        txt_titleConductor.Text = .Title
                        txt_lastNameConductor.Text = .Last_Name
                        If .BirthDate IsNot Nothing Then
                            df_birthDateConductor.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                        End If
                        txt_birthPlaceConductor.Text = .Birth_Place
                        txt_motherNameConductor.Text = .Mothers_Name
                        txt_AlisConductor.Text = .Alias
                        txt_ssnConductor.Text = .SSN
                        txt_passportNumberConductor.Text = .Passport_Number
                        txt_passportCountryConductor.Text = .Passport_Country
                        txt_IdentLainConductor.Text = .Id_Number
                        Cmb_nationality1Conductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                        Cmb_nationality2Conductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                        Cmb_nationality3Conductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                        Cmb_residenceConductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
                        txt_emailConductor.Text = .Email
                        txt_emailConductor2.Text = .Email2
                        txt_emailConductor3.Text = .Email3
                        txt_emailConductor4.Text = .Email4
                        txt_emailConductor5.Text = .Email5
                        txt_occupationConductor.Text = .Occupation
                        txt_employerNameConductor.Text = .Employer_Name
                        Cmb_IdentificationConductor.Text = ""
                        If .Deceased Then
                            DisplayField_DeceaseConductor.Text = "Ya"
                            If .Deceased_Date IsNot Nothing Then
                                df_deceased_dateConductor.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                            End If
                        Else
                            DisplayField_DeceaseConductor.Text = "Tidak"
                        End If
                        txt_taxNumberConductor.Text = .Tax_Number
                        DisplayField_PEPConductor.Text = .Tax_Reg_Number
                        txt_source_of_wealthConductor.Text = .Source_Of_Wealth
                        txt_CommentConductor.Text = .Comment

                        If TransactionClass.listObjPhoneConductor.Count > 0 Then
                            bindConductorPhone(StorePhoneConductor, TransactionClass.listObjPhoneConductor)
                        End If
                        If TransactionClass.listObjAddressConductor.Count > 0 Then
                            bindConductorAddress(StoreAddressConductor, TransactionClass.listObjAddressConductor)
                        End If
                        If TransactionClass.listObjPhoneEmployerConductor.Count > 0 Then
                            bindConductorPhone(StorePhoneEmployerConductor, TransactionClass.listObjPhoneEmployerConductor)
                        End If
                        If TransactionClass.listObjAddressEmployerConductor.Count > 0 Then
                            bindConductorAddress(StoreAddressEmployerConductor, TransactionClass.listObjAddressEmployerConductor)
                        End If
                        If TransactionClass.listObjIdentificationConductor.Count > 0 Then
                            bindIdentification(StoreConductorIdent, TransactionClass.listObjIdentificationConductor)
                        End If
                    End With
                End If
            End If

            If .FK_Transaction_Type = 1 Then

                Panel_identitasPengirim.Hidden = False
                panel_identitasPenerima.Hidden = False
                panel_Party.Hidden = True

                'Pengirim
                If .isMyClient_FROM = True Then
                    DisplayField_isMyClientFrom.Text = "Ya"
                Else
                    DisplayField_isMyClientFrom.Text = "Tidak"
                End If
                Cmb_fromFundsCode.Text = NawaDevBLL.GlobalReportFunctionBLL.getInstrumenByCode(.From_Funds_Code)
                txt_fromFundsCodeComment.Text = .From_Funds_Comment
                Cmb_fromForeignCurrencyCode.Text = NawaDevBLL.GlobalReportFunctionBLL.getCurrencyByCode(.From_Foreign_Currency_Code)
                If .From_Foreign_Currency_Amount.HasValue Then
                    txt_from_foreign_currency_amount.Text = .From_Foreign_Currency_Amount
                End If
                If .From_Foreign_Currency_Exchange_Rate.HasValue Then
                    txt_from_foreign_currency_exchange_rate.Text = .From_Foreign_Currency_Exchange_Rate
                End If
                Cmb_fromCountry.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.From_Country)
                If .usedConductor = True Then
                    Cmb_UseConductor.Text = "Ya"
                Else
                    Cmb_UseConductor.Text = "Tidak"
                End If
                If .FK_Sender_From_Information.HasValue Then
                    Cmb_senderInformation.Text = NawaDevBLL.GlobalReportFunctionBLL.getSenderInformationByPK(.FK_Sender_From_Information)
                End If
                'Penerima
                If .isMyClient_TO = True Then
                    DisplayField_istoMyclient.Text = "Ya"
                Else
                    DisplayField_istoMyclient.Text = "Tidak"
                End If
                Cmb_toFundsCode.Text = NawaDevBLL.GlobalReportFunctionBLL.getInstrumenByCode(.To_Funds_Code)
                txt_instrumentFundsComent.Text = .To_Funds_Comment
                Cmb_toForeignCurrencyCode.Text = NawaDevBLL.GlobalReportFunctionBLL.getCurrencyByCode(.To_Foreign_Currency_Code)
                If .To_Foreign_Currency_Amount.HasValue Then
                    txt_to_foreign_currency_amount.Text = .To_Foreign_Currency_Amount
                End If
                If .To_Foreign_Currency_Exchange_Rate.HasValue Then
                    txt_to_foreign_currency_exchange_rate.Text = .To_Foreign_Currency_Exchange_Rate
                End If
                Cmb_Tocountry.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.To_Country)
                If .FK_Sender_To_Information.HasValue Then
                    cmb_ToInformation.Text = NawaDevBLL.GlobalReportFunctionBLL.getSenderInformationByPK(.FK_Sender_To_Information)
                End If

                'Transaction From
                If .FK_Sender_From_Information = 1 Then 'Account
                    fieldset_FromAccount.Hidden = False
                    Dim ObjTrnFromAccount As New goAML_Transaction_Account_NotReported
                    ObjTrnFromAccount = NawaDevBLL.NotReportedBLL.getTransactionAccountByFK(.Pk_goAML_Transaction_NotReported, 1)
                    If Not ObjTrnFromAccount.PK_Account_NotReported_ID = Nothing Then
                        With ObjTrnFromAccount
                            txt_institutionNameFromAccount.Text = .Institution_Name
                            txt_institutionCodeFromAccount.Text = .Intitution_Code
                            txt_swiftFromAccount.Text = .Swift_Code
                            If .Non_Banking_Institution = True Then
                                DisplayField_Non_banking_institutionFromAccount.Text = "Ya"
                            Else
                                DisplayField_Non_banking_institutionFromAccount.Text = "Tidak"
                            End If
                            txt_branchfromAccount.Text = .Branch
                            txt_accountFromAccount.Text = .Account
                            Cmb_currencyCodeFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCurrencyByCode(.Currency_Code)
                            txt_accountNameFromAccount.Text = .Account_Name
                            txt_ibanFromAccount.Text = .iban
                            txt_clientNumberFromAccount.Text = .Client_Number
                            Cmb_PersonalAccountType.Text = NawaDevBLL.GlobalReportFunctionBLL.getAccountTyperByKode(.Personal_Account_Type)
                            Df_openedFromAccount.Text = .Opened.Value.ToString("dd-MMM-yyyy")
                            If .Closed IsNot Nothing Then
                                Df_closedFromAccount.Text = .Closed.Value.ToString("dd-MMM-yyyy")
                            End If
                            txt_balanceFromAccount.Text = .Balance
                            If .Date_Balance IsNot Nothing Then
                                df_DateBalanceFromAccount.Text = .Date_Balance.Value.ToString("dd-MMM-yyyy")
                            End If
                            cmb_statusCodeFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getStatusRekeningByKode(.Status_Code)
                            txt_beneficiaryFromAccount.Text = .Beneficiary
                            txt_beneficiaryCommentFromAccount.Text = .Beneficiary_Comment
                            txt_commentFromAccount.Text = .Comments
                            If .IsRekeningKorporasi Then
                                rb_isentity.Text = "Ya"
                                fieldset_entityfromAccount.Hidden = False
                                Dim ObjAccEntityFrom As New goAML_Trn_Entity_account_NotReported
                                ObjAccEntityFrom = NawaDevBLL.NotReportedBLL.getTransactionAccountEntityByFK(.PK_Account_NotReported_ID, 1)
                                If Not ObjAccEntityFrom.PK_goAML_Trn_Ent_Acc_NotReported = Nothing Then
                                    With ObjAccEntityFrom
                                        txt_nameKorporasi_EntityFromAccount.Text = .Name
                                        txt_commercialNameKorporasi_EntityFromAccount.Text = .Commercial_Name
                                        Cmb_Incorporation_legal_formFromEntityAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getBentukKorporasiByKode(.Incorporation_Legal_Form)
                                        txt_Incorporation_number_EntityFromAccount.Text = .Incorporation_Number
                                        txt_businessEntityFromAccount.Text = .Business
                                        txt_emailEntityFromAccount.Text = .Email
                                        txt_urlEntityFromAccount.Text = .Url
                                        txt_incorporationStateEntityFromAccount.Text = .Incorporation_State
                                        Cmb_IntercorporationCountryCodeEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Incorporation_Country_Code)
                                        If .Incorporation_Date IsNot Nothing Then
                                            txt_incorporation_dateEntityFromAccount.Text = .Incorporation_Date.Value.ToString("dd-MMM-yyyy")
                                        End If
                                        If .Business_Closed = True Then
                                            business_closedEntityFromAccount.Text = "Ya"
                                            df_date_business_closedEntityFromAccount.Text = .Date_Business_Closed.Value.ToString("dd-MMM-yyyy")
                                        Else
                                            business_closedEntityFromAccount.Text = "Tidak"
                                        End If
                                        txt_taxNumberEntityFromAccount.Text = .Tax_Number
                                        txt_commentEntityFromAccount.Text = .Comments

                                        TransactionClass.listObjPhoneAccountEntityFrom = NawaDevBLL.NotReportedBLL.getListTransactionAccountEntityPhoneById(.PK_goAML_Trn_Ent_Acc_NotReported)
                                        bindPhoneEntityFromAccount(StorePhoneEntityFromAccount, TransactionClass.listObjPhoneAccountEntityFrom)

                                        TransactionClass.listObjAddressAccountEntityFrom = NawaDevBLL.NotReportedBLL.getListTransactionAccountEntityAddressById(.PK_goAML_Trn_Ent_Acc_NotReported)
                                        bindAddressEntityFromAccount(StoreAddressEntityFromAccount, TransactionClass.listObjAddressAccountEntityFrom)

                                        listTrnAcctEntityDiretor = NawaDevBLL.NotReportedBLL.getListTransactionAccountEntityDirectorById(.PK_goAML_Trn_Ent_Acc_NotReported, 1, 1)
                                        bindDirectorEntityFromAccount(StoreDirectorEntityFromAccount, listTrnAcctEntityDiretor)
                                    End With
                                End If
                            Else
                                rb_isentity.Text = "Tidak"
                                fieldset_entityfromAccount.Hidden = True
                            End If

                            listTrnAcctSignatory = NawaDevBLL.NotReportedBLL.getListTransactionAccountSignatoryById(.PK_Account_NotReported_ID, 1)
                            bindSignatoryEntityFromAccount(StoreSignatoryFrom, listTrnAcctSignatory)

                        End With
                    End If
                ElseIf .FK_Sender_From_Information = 2 Then 'Person
                    fieldset_personFrom.Hidden = False
                    Dim ObjTrnFromPerson As New goAML_Transaction_Person_NotReported
                    ObjTrnFromPerson = NawaDevBLL.NotReportedBLL.getTransactionPersonByFK(.Pk_goAML_Transaction_NotReported, 1)
                    If Not ObjTrnFromPerson.PK_Person_NotReported_ID = Nothing Then
                        With ObjTrnFromPerson
                            Cmb_genderPersonFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                            txt_titlePersonFrom.Text = .Title
                            txt_lastNamePersonFrom.Text = .Last_Name
                            If .BirthDate IsNot Nothing Then
                                df_birthDatePersonFrom.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                            End If
                            txt_birthPlacePersonFrom.Text = .Birth_Place
                            txt_motherNamePersonFrom.Text = .Mothers_Name
                            txt_aliasPersonFrom.Text = .Alias
                            txt_ssnPersonFrom.Text = .SSN
                            txt_passportNumberPersonFrom.Text = .Passport_Number
                            txt_passportCountryPersonFrom.Text = .Passport_Country
                            txt_idNumberFromPerson.Text = .Id_Number
                            Cmb_nationality1PersonFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                            Cmb_nationality2PersonFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                            Cmb_nationality3PersonFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                            Cmb_residencePersonFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
                            txt_emailPersonFrom.Text = .Email
                            txt_emailPersonFrom2.Text = .Email2
                            txt_emailPersonFrom3.Text = .Email3
                            txt_emailPersonFrom4.Text = .Email4
                            txt_emailPersonFrom5.Text = .Email5
                            txt_occupationPersonFrom.Text = .Occupation
                            txt_employerNamePersonFrom.Text = .Employer_Name
                            If .Deceased Then
                                DisplayField_DeceasePersonFrom.Text = "Ya"
                                If .Deceased_Date IsNot Nothing Then
                                    df_deceased_datePersonFrom.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                                End If
                            Else
                                DisplayField_DeceasePersonFrom.Text = "Tidak"
                            End If
                            txt_taxNumberPersonFrom.Text = .Tax_Number
                            If .Tax_Reg_Number = True Then
                                DisplayField_PEPpersonFrom.Text = "Ya"
                            Else
                                DisplayField_PEPpersonFrom.Text = "Tidak"
                            End If
                            txt_source_of_wealthPersonFrom.Text = .Source_Of_Wealth
                            txt_CommentPersonFrom.Text = .Comment

                            TransactionClass.listObjAddressPersonFrom = getListTransactionPersonAddressById(.PK_Person_NotReported_ID, False)
                            bindAddressPerson(StoreAddressPersonFrom, TransactionClass.listObjAddressPersonFrom)

                            TransactionClass.listObjAddressEmployerPersonFrom = getListTransactionPersonAddressById(.PK_Person_NotReported_ID, True)
                            bindAddressPerson(StoreAddressEmployerPersonFrom, TransactionClass.listObjAddressEmployerPersonFrom)

                            TransactionClass.listObjPhonePersonFrom = getListTransactionPersonPhoneById(.PK_Person_NotReported_ID, False)
                            bindPhonePerson(StorePhonePersonFrom, TransactionClass.listObjPhonePersonFrom)

                            TransactionClass.listObjPhoneEmployerPersonFrom = getListTransactionPersonPhoneById(.PK_Person_NotReported_ID, True)
                            bindPhonePerson(StorePhoneEmployerPersonFrom, TransactionClass.listObjPhoneEmployerPersonFrom)

                            TransactionClass.listObjIdentificationPersonFrom = getListTransactionIdentificationById(.PK_Person_NotReported_ID, 1, 1)
                            bindIdentification(StoreIdentificationPersonFrom, TransactionClass.listObjIdentificationPersonFrom)

                        End With
                    End If
                ElseIf .FK_Sender_From_Information = 3 Then 'Entity
                    fieldset_EntityFrom.Hidden = False
                    Dim ObjTrnFromEntity As New goAML_Transaction_Entity_NotReported
                    ObjTrnFromEntity = getTransactionEntityByFK(.Pk_goAML_Transaction_NotReported, 1)
                    If Not ObjTrnFromEntity.PK_Entity_NotReported_ID = Nothing Then
                        With ObjTrnFromEntity
                            txt_nameCorporasiEntityFrom.Text = .Name
                            txt_commercialNameEntityFrom.Text = .Commercial_Name
                            Cmb_incorporation_legal_formEntityFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getBentukKorporasiByKode(.Incorporation_Legal_Form)
                            txt_incorporationNumberEntityFrom.Text = .Incorporation_Number
                            txt_businessEntityFrom.Text = .Business
                            txt_emailEntityFrom.Text = .Email
                            txt_urlEntityFrom.Text = .Url
                            txt_incorporationStateEntityFrom.Text = .Incorporation_State
                            Cmb_countryEntityFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Incorporation_Country_Code)
                            'Cmb_directorIDEntityFrom.Text = .Director_ID
                            'Cmb_roleEntityFrom.Text = getRoleTypebyCode(.Role)
                            If .Incorporation_Date IsNot Nothing Then
                                txt_incorporationDateEntityFrom.Text = .Incorporation_Date.Value.ToString("dd-MMM-yyyy")
                            End If
                            If .Business_Closed = True Then
                                DisplayField_businessClosedEntityFrom.Text = "Ya"
                            Else
                                DisplayField_businessClosedEntityFrom.Text = "Tidak"
                            End If
                            If .Date_Business_Closed IsNot Nothing Then
                                df_dateBusinessClosedEntityFrom.Text = .Date_Business_Closed.Value.ToString("dd-MMM-yyyy")
                            End If
                            txt_taxNumberEntityFrom.Text = .Tax_Number
                            txt_commentEntityFrom.Text = .Comments

                            TransactionClass.ListObjAddressEntityFrom = getListTransactionEntityAddressById(.PK_Entity_NotReported_ID)
                            bindAddressEntityFrom(StoreAddressEntityFrom, TransactionClass.ListObjAddressEntityFrom)

                            TransactionClass.listObjPhoneEntityFrom = getListTransactionEntityPhoneById(.PK_Entity_NotReported_ID)
                            bindPhoneEntityFrom(StorePhoneEntityFrom, TransactionClass.listObjPhoneEntityFrom)

                            listTrnEntityDiretor = getListTransactionAccountEntityDirectorById(.PK_Entity_NotReported_ID, 1, 3)
                            bindDirectorEntityFromAccount(StoreDirectorEntity, listTrnEntityDiretor)

                        End With
                    End If
                End If

                'Transaction To
                If .FK_Sender_To_Information = 1 Then 'Account
                    fieldSet_InformasiAccountTo.Hidden = False
                    Dim ObjTrnToAccount As New goAML_Transaction_Account_NotReported
                    ObjTrnToAccount = getTransactionAccountByFK(.Pk_goAML_Transaction_NotReported, 2)
                    If Not ObjTrnToAccount.PK_Account_NotReported_ID = Nothing Then
                        With ObjTrnToAccount
                            txt_InstitutionNameAccountTo.Text = .Institution_Name
                            txt_institutionCodeAccountTo.Text = .Intitution_Code
                            txt_swiftAccountTo.Text = .Swift_Code
                            If .Non_Banking_Institution = True Then
                                txt_NonBankingSituationAccountTo.Text = "Ya"
                            Else
                                txt_NonBankingSituationAccountTo.Text = "Tidak"
                            End If
                            txt_branchAccountTo.Text = .Branch
                            txt_accountAccountTo.Text = .Account
                            Cmb_currencyCodeAccountTo.Text = .Currency_Code
                            txt_accountNameAccountTo.Text = .Account_Name
                            txt_ibanAccountTo.Text = .iban
                            txt_clientNumberAccountTo.Text = .Client_Number
                            Cmb_PersonalAccountTypeAccountTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getAccountTyperByKode(.Personal_Account_Type)
                            Df_openedToAccount.Text = .Opened.Value.ToString("dd-MMM-yyyy")
                            If .Closed IsNot Nothing Then
                                Df_closedToAccount.Text = .Closed.Value.ToString("dd-MMM-yyyy")
                            End If
                            If .Balance IsNot Nothing Then 'perubahan firman 20201112 mesti dijaga validasi
                                txt_balanceToAccount.Text = .Balance
                            End If

                            If .Date_Balance IsNot Nothing Then
                                df_DateBalanceToAccount.Text = .Date_Balance.Value.ToString("dd-MMM-yyyy")
                            End If
                            cmb_statusCodeToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getStatusRekeningByKode(.Status_Code)
                            txt_beneficiaryToAccount.Text = .Beneficiary
                            txt_beneficiaryCommentToAccount.Text = .Beneficiary_Comment
                            txt_commentToAccount.Text = .Comments
                            If .IsRekeningKorporasi Then
                                rb_isEntityAccountTo.Text = "Ya"
                                fieldset_EntityAccountTo.Hidden = False
                                Dim ObjAccEntityFrom As New goAML_Trn_Entity_account_NotReported
                                ObjAccEntityFrom = getTransactionAccountEntityByFK(.PK_Account_NotReported_ID, 2)
                                If Not ObjAccEntityFrom.PK_goAML_Trn_Ent_Acc_NotReported = Nothing Then
                                    With ObjAccEntityFrom
                                        txt_nameKorporasiEntityAccountTo.Text = .Name
                                        txt_Commercial_name_EntityAccountTo.Text = .Commercial_Name
                                        Cmb_incorporation_legal_formEntityAccountTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getBentukKorporasiByKode(.Incorporation_Legal_Form)
                                        txt_incorporation_numberEntityAccountTo.Text = .Incorporation_Number
                                        txt_BusinessEntityAccountTo.Text = .Business
                                        txt_emailEntityAccountTo.Text = .Email
                                        txt_urlEntityAccountTo.Text = .Url
                                        txt_incorporation_stateEntityAccountTo.Text = .Incorporation_State
                                        Cmb_incorporation_country_codeEntityAccountTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Incorporation_Country_Code)
                                        If .Incorporation_Date IsNot Nothing Then
                                            df_incorporation_dateEntityAccountTo.Text = .Incorporation_Date.Value.ToString("dd-MMM-yyyy")
                                        End If
                                        If .Business_Closed = True Then
                                            DisplayField_business_closedEntityAccountTo.Text = "Ya"
                                            df_date_business_closedEntityAccountTo.Text = .Date_Business_Closed.Value.ToString("dd-MMM-yyyy")
                                        Else
                                            DisplayField_business_closedEntityAccountTo.Text = "Tidak"
                                        End If
                                        txt_tax_numberEntityAccountTo.Text = .Tax_Number
                                        txt_CommentsEntityAccountTo.Text = .Comments

                                        TransactionClass.listObjPhoneAccountEntityto = getListTransactionAccountEntityPhoneById(.PK_goAML_Trn_Ent_Acc_NotReported)
                                        bindPhoneEntityFromAccount(StorePhoneEntityAccountTo, TransactionClass.listObjPhoneAccountEntityto)

                                        TransactionClass.listObjAddressAccountEntityto = getListTransactionAccountEntityAddressById(.PK_goAML_Trn_Ent_Acc_NotReported)
                                        bindAddressEntityFromAccount(StoreAddressEntityAccountTo, TransactionClass.listObjAddressAccountEntityto)

                                        listTrnAcctEntityDiretorTo = getListTransactionAccountEntityDirectorById(.PK_goAML_Trn_Ent_Acc_NotReported, 2, 1)
                                        bindDirectorEntityFromAccount(StoreDirectorEntityToAccount, listTrnAcctEntityDiretorTo)
                                    End With
                                End If
                            Else
                                rb_isEntityAccountTo.Text = "Tidak"
                                fieldset_entityfromAccount.Hidden = True
                            End If

                            listTrnAcctSignatoryTo = getListTransactionAccountSignatoryById(.PK_Account_NotReported_ID, 2)
                            bindSignatoryEntityFromAccount(StoreSignatoryTo, listTrnAcctSignatoryTo)
                        End With
                    End If

                ElseIf .FK_Sender_To_Information = 2 Then 'Person
                    fieldset_personTo.Hidden = False
                    Dim ObjTrnToPerson As New goAML_Transaction_Person_NotReported
                    ObjTrnToPerson = getTransactionPersonByFK(.Pk_goAML_Transaction_NotReported, 2)
                    If Not ObjTrnToPerson.PK_Person_NotReported_ID = Nothing Then
                        With ObjTrnToPerson
                            Cmb_genderPersonTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                            txt_titlePersonTo.Text = .Title
                            txt_lastNamePersonTo.Text = .Last_Name
                            If .BirthDate IsNot Nothing Then
                                df_birthDatePersonTo.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                            End If
                            txt_birthPlacePersonTo.Text = .Birth_Place
                            txt_motherNamePersonTo.Text = .Mothers_Name
                            txt_aliasPersonTo.Text = .Alias
                            txt_ssnPersonTo.Text = .SSN
                            txt_passportNumberPersonTo.Text = .Passport_Number
                            txt_passportCountryPersonTo.Text = .Passport_Country
                            txt_idNumberPersonTo.Text = .Id_Number
                            Cmb_nationality1PersonTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                            Cmb_nationality2PersonTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                            Cmb_nationality3PersonTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                            Cmb_residencePersonTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
                            txt_emailPersonTo.Text = .Email
                            txt_emailPersonTo2.Text = .Email2
                            txt_emailPersonTo3.Text = .Email3
                            txt_emailPersonTo4.Text = .Email4
                            txt_emailPersonTo5.Text = .Email5
                            txt_occupationPersonTo.Text = .Occupation
                            txt_employerNamePersonTo.Text = .Employer_Name
                            If .Deceased Then
                                DisplayField_DeceasePersonTo.Text = "Ya"
                                If .Deceased_Date IsNot Nothing Then
                                    df_deceased_datePersonTo.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                                End If
                            Else
                                DisplayField_DeceasePersonTo.Text = "Tidak"
                            End If
                            txt_taxNumberPersonTo.Text = .Tax_Number
                            If .Tax_Reg_Number = True Then
                                DisplayField_PEPPersonTo.Text = "Ya"
                            Else
                                DisplayField_PEPPersonTo.Text = "Tidak"
                            End If
                            txt_source_of_wealthPersonTo.Text = .Source_Of_Wealth
                            txt_CommentPersonTo.Text = .Comment

                            TransactionClass.listObjAddressPersonTo = getListTransactionPersonAddressById(.PK_Person_NotReported_ID, False)
                            bindAddressPerson(StoreAddressPersonTo, TransactionClass.listObjAddressPersonTo)

                            TransactionClass.listObjAddressEmployerPersonTo = getListTransactionPersonAddressById(.PK_Person_NotReported_ID, True)
                            bindAddressPerson(StoreAddressEmployerPersonTo, TransactionClass.listObjAddressEmployerPersonTo)

                            TransactionClass.listObjPhonePersonTo = getListTransactionPersonPhoneById(.PK_Person_NotReported_ID, False)
                            bindPhonePerson(StorePhonePersonTo, TransactionClass.listObjPhonePersonTo)

                            TransactionClass.listObjPhoneEmployerPersonTo = getListTransactionPersonPhoneById(.PK_Person_NotReported_ID, True)
                            bindPhonePerson(StorePhoneEmployerPersonTo, TransactionClass.listObjPhoneEmployerPersonTo)

                            TransactionClass.listObjIdentificationPersonTo = getListTransactionIdentificationById(.PK_Person_NotReported_ID, 1, 2)
                            bindIdentification(StoreIdentificationPersonTo, TransactionClass.listObjIdentificationPersonTo)

                        End With
                    End If

                ElseIf .FK_Sender_To_Information = 3 Then 'Entity
                    fieldset_EntityTo.Hidden = False
                    Dim ObjTrnToEntity As New goAML_Transaction_Entity_NotReported
                    ObjTrnToEntity = getTransactionEntityByFK(.Pk_goAML_Transaction_NotReported, 2)
                    If Not ObjTrnToEntity.PK_Entity_NotReported_ID = Nothing Then
                        With ObjTrnToEntity
                            txt_nameCorporasiEntityTo.Text = .Name
                            txt_commercialNameEntityTo.Text = .Commercial_Name
                            Cmb_incorporation_legal_formEntityTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getBentukKorporasiByKode(.Incorporation_Legal_Form)
                            txt_incorporationNumberEntityTo.Text = .Incorporation_Number
                            txt_businessEntityTo.Text = .Business
                            txt_emailEntityTo.Text = .Email
                            txt_urlEntityTo.Text = .Url
                            txt_incorporationStateEntityTo.Text = .Incorporation_State
                            Cmb_countryEntityTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Incorporation_Country_Code)
                            'Cmb_directorIDEntityTo.Text = .Director_ID
                            'Cmb_roleEntityTo.Text = getRoleTypebyCode(.Role)
                            If .Incorporation_Date IsNot Nothing Then
                                txt_incorporationDateEntityTo.Text = .Incorporation_Date.Value.ToString("dd-MMM-yyyy")
                            End If
                            If .Business_Closed = True Then
                                DisplayField_businessClosedEntityTo.Text = "Ya"
                            Else
                                DisplayField_businessClosedEntityTo.Text = "Tidak"
                            End If
                            If .Date_Business_Closed IsNot Nothing Then
                                df_dateBusinessClosedEntityTo.Text = .Date_Business_Closed.Value.ToString("dd-MMM-yyyy")
                            End If
                            txt_taxNumberEntityTo.Text = .Tax_Number
                            txt_commentEntityTo.Text = .Comments

                            TransactionClass.ListObjAddressEntityto = getListTransactionEntityAddressById(.PK_Entity_NotReported_ID)
                            bindAddressEntityFrom(StoreAddressEntityTo, TransactionClass.ListObjAddressEntityto)

                            TransactionClass.listObjPhoneEntityto = getListTransactionEntityPhoneById(.PK_Entity_NotReported_ID)
                            bindPhoneEntityFrom(StorePhoneEntityTo, TransactionClass.listObjPhoneEntityto)

                            listTrnEntityDiretorTo = getListTransactionAccountEntityDirectorById(.PK_Entity_NotReported_ID, 2, 3)
                            bindDirectorEntityFromAccount(StoreDirectorEntityTo, listTrnEntityDiretorTo)

                        End With
                    End If
                End If

            ElseIf .FK_Transaction_Type = 2 Then

                Panel_identitasPengirim.Hidden = True
                panel_identitasPenerima.Hidden = True
                panel_Party.Hidden = False
                'Transaction
                Dim TransactionParty As New goAML_Transaction_Party_NotReported
                TransactionParty = getTransactionByIdParty(.Pk_goAML_Transaction_NotReported)
                With TransactionParty
                    If .isMyClient Then
                        DisplayField_isMyClientParty.Text = "Ya"
                    Else
                        DisplayField_isMyClientParty.Text = "Tidak"
                    End If
                    Cmb_roleParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getRolePartybyCode(.roleParty)
                    Cmb_fundsCodeParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getInstrumenByCode(.funds_code)
                    txt_fundsComentParty.Text = .funds_comment
                    Cmb_currencyCOdeParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCurrencyByCode(.foreign_currency)
                    Cmb_CountryParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.country)
                    If .significance.HasValue Then
                        txt_significanceParty.Text = .significance.Value
                    End If
                    txt_commentParty.Text = .comments
                    Cmb_ObjectParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getSenderInformationByPK(.fk_ref_detail_of)

                    If .fk_ref_detail_of = 1 Then 'Account
                        fieldset_Accountparty.Hidden = False
                        fieldset_personParty.Hidden = True
                        fieldset_EntityParty.Hidden = True

                        Dim ObjTrnAccountParty As New goAML_Trn_Party_Account_NotReported
                        ObjTrnAccountParty = getTransactionAccountByFKParty(.PK_Trn_Party_ID)

                        With ObjTrnAccountParty
                            txt_institutionNameAccountparty.Text = .Institution_Name
                            txt_institutionCodeAccountparty.Text = .Intitution_Code
                            txt_swiftAccountparty.Text = .Swift_Code
                            If .Non_Banking_Institution Then
                                DisplayField_Non_banking_institutionAccountparty.Text = "Ya"
                            Else
                                DisplayField_Non_banking_institutionAccountparty.Text = "Tidak"
                            End If
                            txt_branchAccountparty.Text = .Branch
                            txt_accountAccountparty.Text = .Account
                            Cmb_currencyCodeAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCurrencyByCode(.Currency_Code)
                            txt_accountNameAccountparty.Text = .Account_Name
                            txt_ibanAccountparty.Text = .iban
                            txt_clientNumberAccountparty.Text = .Client_Number
                            Cmb_PersonalAccountType.Text = NawaDevBLL.GlobalReportFunctionBLL.getAccountTyperByKode(.Personal_Account_Type)
                            If .Opened IsNot Nothing Then
                                Df_openedAccountparty.Text = .Opened.Value.ToString("dd-MMM-yyyy")
                            End If
                            If .Closed IsNot Nothing Then
                                Df_closedAccountparty.Text = .Closed.Value.ToString("dd-MMM-yyyy")
                            End If
                            txt_balanceAccountparty.Text = .Balance
                            If .Date_Balance IsNot Nothing Then
                                df_DateBalanceAccountparty.Text = .Date_Balance.Value.ToString("dd-MMM-yyyy")
                            End If
                            cmb_statusCodeAccountparty.Text = NawaDevBLL.GlobalReportFunctionBLL.getStatusRekeningByKode(.Status_Code)
                            txt_beneficiaryAccountparty.Text = .Beneficiary
                            txt_beneficiaryCommentAccountparty.Text = .Beneficiary_Comment
                            txt_commentAccountparty.Text = .Comments
                            If .isRekeningKorporasi Then
                                txt_isEntityAccountparty.Text = "Ya"
                            Else
                                txt_isEntityAccountparty.Text = "Tidak"
                            End If
                            If .isRekeningKorporasi Then
                                Dim AccountEntityparty As New goAML_Trn_Par_Acc_Entity_NotReported
                                AccountEntityparty = getTransactionAccountEntityByFKParty(.PK_Trn_Party_Account_ID_NotReported)
                                fieldset_entityAccountparty.Hidden = False

                                With AccountEntityparty
                                    txt_nameKorporasi_EntityAccountparty.Text = .Name
                                    txt_commercialNameKorporasi_EntityAccountparty.Text = .Commercial_Name
                                    Cmb_Incorporation_legal_formFromEntityAccount.Text = .Incorporation_Legal_Form
                                    txt_Incorporation_number_EntityAccountparty.Text = .Incorporation_Number
                                    txt_businessEntityAccountparty.Text = .Business
                                    txt_emailEntityAccountparty.Text = .Email
                                    txt_urlEntityAccountparty.Text = .Url
                                    txt_incorporationStateEntityAccountparty.Text = .Incorporation_State
                                    Cmb_IntercorporationCountryCodeEntityAccountparty.Text = .Incorporation_Country_Code
                                    If .Incorporation_Date IsNot Nothing Then
                                        txt_incorporation_dateEntityAccountparty.Text = .Incorporation_Date.Value.ToString("dd-MMM-yyyy")
                                    End If
                                    If .Business_Closed Then
                                        DisplayField_business_closedEntityAccountparty.Text = "Ya"
                                    Else
                                        DisplayField_business_closedEntityAccountparty.Text = "Tidak"
                                    End If
                                    If .Date_Business_Closed IsNot Nothing Then
                                        df_date_business_closedEntityAccountparty.Text = .Date_Business_Closed.Value.ToString("dd-MMM-yyyy")
                                    End If
                                    txt_taxNumberEntityAccountparty.Text = .Tax_Number
                                    txt_commentEntityAccountparty.Text = .Comments

                                    TransactionClass.listPhoneAccountEntityParty = getListTransactionAccountEntityPhoneByIdParty(.PK_Trn_Par_acc_Entity_NotReported_ID)
                                    If TransactionClass.listPhoneAccountEntityParty.Count > 0 Then
                                        bindPhoneEntityAccountParty(StorePhoneEntityAccountparty, TransactionClass.listPhoneAccountEntityParty)
                                    End If

                                    TransactionClass.listAddresAccountEntityParty = getListTransactionAccountEntityAddressByIdParty(.PK_Trn_Par_acc_Entity_NotReported_ID)
                                    If TransactionClass.listAddresAccountEntityParty.Count > 0 Then
                                        bindAddressEntityAccountParty(StoreAddressEntityAccountparty, TransactionClass.listAddresAccountEntityParty)
                                    End If

                                    listDirectorentityaccountparty = getListTransactionEntityAccountDirectorByIdParty(.PK_Trn_Par_acc_Entity_NotReported_ID)
                                    If listDirectorentityaccountparty.Count > 0 Then
                                        bindDirectorEntityAccountParty(StoreDirectorEntityAccountparty, listDirectorentityaccountparty)
                                    End If
                                End With

                            End If

                            listSignatoryAccountParty = getListTransactionAccountSignatoryByIdParty(.PK_Trn_Party_Account_ID_NotReported)
                            If listSignatoryAccountParty.Count > 0 Then
                                bindSignatoryAccountParty(StoreSignatoryParty, listSignatoryAccountParty)
                            End If
                        End With

                    ElseIf .fk_ref_detail_of = 2 Then 'Person
                        fieldset_personParty.Hidden = False
                        fieldset_Accountparty.Hidden = True
                        fieldset_EntityParty.Hidden = True

                        Dim ObjTrnPersonParty As New goAML_Trn_Party_Person_NotReported
                        ObjTrnPersonParty = getTransactionPersonByFKParty(.PK_Trn_Party_ID)

                        With ObjTrnPersonParty
                            Cmb_genderPersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                            txt_titlePersonParty.Text = .Title
                            txt_lastNamePersonParty.Text = .Last_Name
                            If .BirthDate IsNot Nothing Then
                                df_birthDatePersonParty.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                            End If
                            txt_birthPlacePersonParty.Text = .Birth_Place
                            txt_motherNamePersonParty.Text = .Mothers_Name
                            txt_aliasPersonParty.Text = .Alias
                            txt_ssnPersonParty.Text = .SSN
                            txt_passportNumberPersonParty.Text = .Passport_Number
                            txt_passportCountryPersonParty.Text = .Passport_Country
                            txt_idNumberPartyPerson.Text = .Id_Number
                            Cmb_nationality1PersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                            Cmb_nationality2PersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                            Cmb_nationality3PersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                            Cmb_residencePersonParty.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
                            txt_emailPersonParty.Text = .Email
                            txt_emailPersonParty2.Text = .Email2
                            txt_emailPersonParty3.Text = .Email3
                            txt_emailPersonParty4.Text = .Email4
                            txt_emailPersonParty5.Text = .Email5
                            txt_occupationPersonParty.Text = .Occupation
                            txt_employerNamePersonParty.Text = .Employer_Name
                            If .Deceased Then
                                DisplayField_DeceasePersonParty.Text = "Ya"
                                If .Deceased_Date IsNot Nothing Then
                                    df_deceased_datePersonParty.Text = .Deceased_Date
                                End If
                            Else
                                DisplayField_DeceasePersonParty.Text = "Tidak"
                            End If
                            txt_taxNumberPersonParty.Text = .Tax_Number
                            If .Tax_Reg_Number Then
                                DisplayField_PEPpersonParty.Text = "Ya"
                            Else
                                DisplayField_PEPpersonParty.Text = "Tidak"
                            End If
                            txt_source_of_wealthPersonParty.Text = .Source_Of_Wealth
                            txt_CommentPersonParty.Text = .Comment

                            TransactionClass.listPhonePersonParty = getListTransactionPersonPhoneByIdParty(.PK_Trn_Party_Person_ID_NotReported, 0)
                            If TransactionClass.listPhonePersonParty.Count > 0 Then
                                bindPhonePersonParty(StorePhonePersonParty, TransactionClass.listPhonePersonParty)
                            End If

                            TransactionClass.listAddressPersonParty = getListTransactionPersonAddressByIdParty(.PK_Trn_Party_Person_ID_NotReported, 0)
                            If TransactionClass.listAddressPersonParty.Count > 0 Then
                                bindAddressPersonParty(StoreAddressPersonParty, TransactionClass.listAddressPersonParty)
                            End If

                            TransactionClass.listAddressEmployerPersonParty = getListTransactionPersonAddressByIdParty(.PK_Trn_Party_Person_ID_NotReported, 1)
                            If TransactionClass.listAddressEmployerPersonParty.Count > 0 Then
                                bindAddressPersonParty(StoreAddressEmployerPersonParty, TransactionClass.listAddressEmployerPersonParty)
                            End If

                            TransactionClass.listPhoneEmployerPersonParty = getListTransactionPersonPhoneByIdParty(.PK_Trn_Party_Person_ID_NotReported, 1)
                            If TransactionClass.listPhoneEmployerPersonParty.Count > 0 Then
                                bindPhonePersonParty(StorePhoneEmployerPersonParty, TransactionClass.listPhoneEmployerPersonParty)
                            End If

                            TransactionClass.listIdentificationPersonParty = getListTransactionIdentificationByIdParty(.PK_Trn_Party_Person_ID_NotReported, 1, NotReportClass.objReport.Pk_GoAML_Report_NotReported)
                            If TransactionClass.listIdentificationPersonParty.Count > 0 Then
                                bindIdentificationParty(StoreIdentificationPersonParty, TransactionClass.listIdentificationPersonParty)
                            End If
                        End With

                    ElseIf .fk_ref_detail_of = 3 Then 'Entity
                        fieldset_EntityParty.Hidden = False
                        fieldset_personParty.Hidden = True
                        fieldset_Accountparty.Hidden = True

                        Dim ObjTrnEntityParty As New goAML_Trn_Party_Entity_NotReported
                        Dim idx As String = .PK_Trn_Party_ID
                        ObjTrnEntityParty = getTransactionEntityByFKParty(.PK_Trn_Party_ID)

                        With ObjTrnEntityParty
                            txt_nameCorporasiEntityParty.Text = .Name
                            txt_commercialNameEntityParty.Text = .Commercial_Name
                            Cmb_incorporation_legal_formEntityParty.Text = .Incorporation_Legal_Form
                            txt_incorporationNumberEntityParty.Text = .Incorporation_Number
                            txt_businessEntityParty.Text = .Business
                            txt_emailEntityParty.Text = .Email
                            txt_urlEntityParty.Text = .Url
                            txt_incorporationStateEntityParty.Text = .Incorporation_State
                            Cmb_countryEntityParty.Text = .Incorporation_Country_Code
                            If .Incorporation_Date IsNot Nothing Then
                                txt_incorporationDateEntityParty.Text = .Incorporation_Date
                            End If
                            If .Business_Closed Then
                                DisplayField_businessClosedEntityParty.Text = "Ya"
                                If .Date_Business_Closed IsNot Nothing Then
                                    df_dateBusinessClosedEntityParty.Text = .Date_Business_Closed
                                End If
                            Else
                                DisplayField_businessClosedEntityParty.Text = "Tidak"
                            End If
                            txt_taxNumberEntityParty.Text = .Tax_Number
                            txt_commentEntityParty.Text = .Comments

                            TransactionClass.listAddressEntityparty = getListTransactionEntityAddressByIdParty(.PK_Trn_Party_Entity_ID_NotReported)
                            If TransactionClass.listAddressEntityparty.Count > 0 Then
                                bindAddressEntityParty(StoreAddressEntityParty, TransactionClass.listAddressEntityparty)
                            End If

                            TransactionClass.listPhoneEntityParty = getListTransactionEntityPhoneByIdParty(.PK_Trn_Party_Entity_ID_NotReported)
                            If TransactionClass.listPhoneEntityParty.Count > 0 Then
                                bindPhoneEntityParty(StorePhoneEntityParty, TransactionClass.listPhoneEntityParty)
                            End If

                            listDirectorEntityparty = getListTransactionEntitytDirectorByIdParty(.PK_Trn_Party_Entity_ID_NotReported)
                            If listDirectorEntityparty.Count > 0 Then
                                bindDirectorEntityParty(StoreDirectorEntityParty, listDirectorEntityparty)
                            End If

                        End With

                    End If
                End With

            End If

        End With
    End Sub

    Protected Sub GridcommandWindowAddressEntityFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEntityFrom(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressPersonFromAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressPersonFromAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEmployerPersonFromAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEmployerPersonIDFromAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEmployerConductor(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEmployerconductor(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandPhonePersonFromAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhonePersonFromAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployerPersonIDFromAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEmployerPersonFromAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEntityFromAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEntityFromAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandAddressEntityTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEntityTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEntityFromAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEntityFromAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEntityFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEntityFrom(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEntityTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEntityTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailDirectorEntityFrom(Id As String)
        WindowDirectorEntityFrom.Hidden = False
        Dim ObjDirector As NawaDevDAL.goAML_Trn_Director_NotReported
        ObjDirector = listTrnEntityDiretor.Where(Function(x) x.PK_goAML_Trn_Director_NotReported_ID = Id).FirstOrDefault
        With ObjDirector
            ToDA_Gender.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
            ToDA_Gelar.Text = .Title
            ToDA_LastName.Text = .Last_Name
            If .BirthDate IsNot Nothing Then
                ToDA_BOD.Text = .BirthDate.Value.ToString("dd-MMM-yy")
            End If
            ToDA_BOP.Text = .Birth_Place
            ToDA_MotherName.Text = .Mothers_Name
            ToDA_Alias.Text = .Alias
            ToDA_SSN.Text = .SSN
            ToDA_Pass.Text = .Passport_Number
            ToDA_PenerbitPass.Text = .Passport_Country
            ToDA_Ident.Text = .Id_Number
            ToDA_Nat1.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
            ToDA_Nat2.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
            ToDA_Nat3.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
            ToDA_Dom.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
            ToDA_Email.Text = .Email
            ToDA_Email2.Text = .Email2
            ToDA_Email3.Text = .Email3
            ToDA_Email4.Text = .Email4
            ToDA_Email5.Text = .Email5
            ToDA_Occupation.Text = .Occupation
            ToDA_EmpoyerName.Text = .Employer_Name
            If .Deceased Then
                ToDA_Dec.Text = "Ya"
                If .Deceased_Date IsNot Nothing Then
                    ToDA_DecDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                End If
            Else
                ToDA_Dec.Text = "Tidak"
            End If
            ToDA_TaxNumber.Text = .Tax_Number
            ToDA_Tax_reg.Text = .Tax_Reg_Number
            ToDA_SourceOfWealth.Text = .Source_Of_Wealth
            ToDA_Comment.Text = .Comment
            ToDA_Role.Text = .role

            listPhonedirectorentityfrom = getListTransactionAccountEntityDirectorPhoneById(.PK_goAML_Trn_Director_NotReported_ID, False)
            bindPhoneDirectorEntityFromAccount(StorePhoneDirectorEntityFrom, listPhonedirectorentityfrom)

            listPhoneEmployerDirectorEntityfrom = getListTransactionAccountEntityDirectorPhoneById(.PK_goAML_Trn_Director_NotReported_ID, True)
            bindPhoneDirectorEntityFromAccount(StorePhoneEmployerDirectorEntityfrom, listPhoneEmployerDirectorEntityfrom)

            listAddressdirectorentityfrom = getListTransactionAccountEntityDirectorAddressById(.PK_goAML_Trn_Director_NotReported_ID, False)
            bindAddressDirectorEntityFromAccount(StoreAddressDirectorEntityFrom, listAddressdirectorentityfrom)

            listEmployerAddressdirectorentityfrom = getListTransactionAccountEntityDirectorAddressById(.PK_goAML_Trn_Director_NotReported_ID, True)
            bindAddressDirectorEntityFromAccount(StoreAddressEmpoyerdirectorentityfrom, listEmployerAddressdirectorentityfrom)

            listIdentificationDirectorEntityFrom = getListTransactionIdentificationById(.PK_goAML_Trn_Director_NotReported_ID, 4, 1)
            bindIdentification(StoreIdentificationDirectorEntityFrom, listIdentificationDirectorEntityFrom)
        End With

    End Sub
    Sub DetailDirectorEntityTo(Id As String)
        WindowDirectorEntityTo.Hidden = False
        Dim ObjDirectorTo As NawaDevDAL.goAML_Trn_Director_NotReported
        ObjDirectorTo = listTrnEntityDiretorTo.Where(Function(x) x.PK_goAML_Trn_Director_NotReported_ID = Id).FirstOrDefault
        With ObjDirectorTo
            ToDA_GenderTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
            ToDA_GelarTo.Text = .Title
            ToDA_LastNameTo.Text = .Last_Name
            If .BirthDate IsNot Nothing Then
                ToDA_BODTo.Text = .BirthDate.Value.ToString("dd-MMM-yy")
            End If
            ToDA_BOPTo.Text = .Birth_Place
            ToDA_MotherNameTo.Text = .Mothers_Name
            ToDA_AliasTo.Text = .Alias
            ToDA_SSNTo.Text = .SSN
            ToDA_PassTo.Text = .Passport_Number
            ToDA_PenerbitPassTo.Text = .Passport_Country
            ToDA_IdentTo.Text = .Id_Number
            ToDA_Nat1To.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
            ToDA_Nat2To.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
            ToDA_Nat3To.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
            ToDA_DomTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
            ToDA_EmailTo.Text = .Email
            ToDA_Email2To.Text = .Email2
            ToDA_Email3To.Text = .Email3
            ToDA_Email4To.Text = .Email4
            ToDA_Email5To.Text = .Email5
            ToDA_OccupationTo.Text = .Occupation
            ToDA_EmpoyerNameTo.Text = .Employer_Name
            If .Deceased Then
                ToDA_DecTo.Text = "Ya"
                If .Deceased_Date IsNot Nothing Then
                    ToDA_DecDateTo.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                End If
            Else
                ToDA_DecTo.Text = "Tidak"
            End If
            ToDA_TaxNumberTo.Text = .Tax_Number
            ToDA_Tax_regTo.Text = .Tax_Reg_Number
            ToDA_SourceOfWealthTo.Text = .Source_Of_Wealth
            ToDA_CommentTo.Text = .Comment
            ToDA_RoleTo.Text = .role

            listPhonedirectorentityTo = getListTransactionAccountEntityDirectorPhoneById(.PK_goAML_Trn_Director_NotReported_ID, False)
            bindPhoneDirectorEntityFromAccount(StorePhoneDirectorEntityTo, listPhonedirectorentityTo)

            listPhoneEmployerDirectorEntityTo = getListTransactionAccountEntityDirectorPhoneById(.PK_goAML_Trn_Director_NotReported_ID, True)
            bindPhoneDirectorEntityFromAccount(StorePhoneEmployerDirectorEntityTo, listPhoneEmployerDirectorEntityTo)

            listAddressdirectorentityTo = getListTransactionAccountEntityDirectorAddressById(.PK_goAML_Trn_Director_NotReported_ID, False)
            bindAddressDirectorEntityFromAccount(StoreAddressDirectorEntityTo, listAddressdirectorentityTo)

            listEmployerAddressdirectorentityTo = getListTransactionAccountEntityDirectorAddressById(.PK_goAML_Trn_Director_NotReported_ID, True)
            bindAddressDirectorEntityFromAccount(StoreAddressEmpoyerdirectorentityTo, listEmployerAddressdirectorentityTo)

            listIdentificationDirectorEntityTo = getListTransactionIdentificationById(.PK_goAML_Trn_Director_NotReported_ID, 4, 2)
            bindIdentification(StoreIdentificationDirectorEntityTo, listIdentificationDirectorEntityTo)
        End With

    End Sub

    Sub DetailDirectorEntityFromAccount(id As String)
        Try
            WindowDirectorEntityFromAccount.Hidden = False
            Dim ObjDirector As NawaDevDAL.goAML_Trn_Director_NotReported
            ObjDirector = listTrnAcctEntityDiretor.Where(Function(x) x.PK_goAML_Trn_Director_NotReported_ID = id).FirstOrDefault

            With ObjDirector
                Cmb_genderDirectorEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                txt_titleDirectorEntityFromAccount.Text = .Title
                txt_lastNameDirectorEntityFromAccount.Text = .Last_Name
                If .BirthDate IsNot Nothing Then
                    df_birthDateDirectorEntityFromAccount.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                End If
                txt_birthPlaceDirectorEntityFromAccount.Text = .Birth_Place
                txt_motherNameDirectorEntityFromAccount.Text = .Mothers_Name
                txt_aliasDirectorEntityFromAccount.Text = .Alias
                txt_ssnDirectorEntityFromAccount.Text = .SSN
                txt_passportNumberDirectorEntityFromAccount.Text = .Passport_Number
                txt_passportCountryDirectorEntityFromAccount.Text = .Passport_Country
                txt_idNumberFromDirectorEntityAccount.Text = .Id_Number
                Cmb_nationality1DirectorEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                Cmb_nationality2DirectorEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                Cmb_nationality3DirectorEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                Cmb_residenceDirectorEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
                txt_emailDirectorEntityFromAccount.Text = .Email
                txt_emailDirectorEntityFromAccount2.Text = .Email2
                txt_emailDirectorEntityFromAccount3.Text = .Email3
                txt_emailDirectorEntityFromAccount4.Text = .Email4
                txt_emailDirectorEntityFromAccount5.Text = .Email5
                txt_occupationDirectorEntityFromAccount.Text = .Occupation
                txt_employerNameDirectorEntityFromAccount.Text = .Employer_Name
                If .Deceased Then
                    DisplayField_DeceaseDirectorEntityFromAccount.Text = "Ya"
                    If .Deceased_Date IsNot Nothing Then
                        df_deceased_dateDirectorEntityFromAccount.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                    End If
                Else
                    DisplayField_DeceaseDirectorEntityFromAccount.Text = "Tidak"
                End If
                txt_taxNumberDirectorEntityFromAccount.Text = .Tax_Number
                DisplayField_PEPDirectorEntityFromAccount.Text = .Tax_Reg_Number
                txt_source_of_wealthDirectorEntityFromAccount.Text = .Source_Of_Wealth
                txt_CommentDirectorEntityFromAccount.Text = .Comment
                Cmb_roleTypeDirector.Text = NawaDevBLL.GlobalReportFunctionBLL.getPeranDalamKorporasiByKode(.role)

                listPhonedirectorentityfromaccount = getListTransactionAccountEntityDirectorPhoneById(.PK_goAML_Trn_Director_NotReported_ID, False)
                bindPhoneDirectorEntityFromAccount(StorePhoneDirectorEntityFromAccount, listPhonedirectorentityfromaccount)

                listAddressdirectorentityfromaccount = getListTransactionAccountEntityDirectorAddressById(.PK_goAML_Trn_Director_NotReported_ID, False)
                bindAddressDirectorEntityFromAccount(StoreAddressDirectorEntityFromAccount, listAddressdirectorentityfromaccount)

                listPhoneEmployerDirectorEntityfromaccount = getListTransactionAccountEntityDirectorPhoneById(.PK_goAML_Trn_Director_NotReported_ID, True)
                bindPhoneDirectorEntityFromAccount(StorePhoneEmployerDirectorEntityFromAccount, listPhoneEmployerDirectorEntityfromaccount)

                listEmployerAddressdirectorentityfromaccount = getListTransactionAccountEntityDirectorAddressById(.PK_goAML_Trn_Director_NotReported_ID, True)
                bindAddressDirectorEntityFromAccount(StoreAddressEmployerDirectorEntityFromAccount, listEmployerAddressdirectorentityfromaccount)

                listIdentificationDirectorEntityAccountFrom = getListTransactionIdentificationById(.PK_goAML_Trn_Director_NotReported_ID, 4, 1)
                bindIdentification(StoreIdentificationDirectorEntityFromAccount, listIdentificationDirectorEntityAccountFrom)

            End With

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DetailDirectorEntityToAccount(id As String)
        Try
            WindowDirectorEntityToAccount.Hidden = False
            Dim ObjDirector As NawaDevDAL.goAML_Trn_Director_NotReported
            ObjDirector = listTrnAcctEntityDiretorTo.Where(Function(x) x.PK_goAML_Trn_Director_NotReported_ID = id).FirstOrDefault

            With ObjDirector
                Cmb_genderDirectorEntityToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                txt_titleDirectorEntityToAccount.Text = .Title
                txt_lastNameDirectorEntityToAccount.Text = .Last_Name
                If .BirthDate IsNot Nothing Then
                    df_birthDateDirectorEntityToAccount.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                End If
                txt_birthPlaceDirectorEntityToAccount.Text = .Birth_Place
                txt_motherNameDirectorEntityToAccount.Text = .Mothers_Name
                txt_aliasDirectorEntityToAccount.Text = .Alias
                txt_ssnDirectorEntityToAccount.Text = .SSN
                txt_passportNumberDirectorEntityToAccount.Text = .Passport_Number
                txt_passportCountryDirectorEntityToAccount.Text = .Passport_Country
                txt_idNumberFromDirectorEntityToAccount.Text = .Id_Number
                Cmb_nationality1DirectorEntityToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                Cmb_nationality2DirectorEntityToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                Cmb_nationality3DirectorEntityToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                Cmb_residenceDirectorEntityToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
                txt_emailDirectorEntityToAccount.Text = .Email
                txt_emailDirectorEntityToAccount2.Text = .Email2
                txt_emailDirectorEntityToAccount3.Text = .Email3
                txt_emailDirectorEntityToAccount4.Text = .Email4
                txt_emailDirectorEntityToAccount5.Text = .Email5
                txt_occupationDirectorEntityToAccount.Text = .Occupation
                txt_employerNameDirectorEntityToAccount.Text = .Employer_Name
                If .Deceased Then
                    DisplayField_DeceaseDirectorEntityToAccount.Text = "Ya"
                    If .Deceased_Date IsNot Nothing Then
                        df_deceased_dateDirectorEntityToAccount.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                    End If
                Else
                    DisplayField_DeceaseDirectorEntityToAccount.Text = "Tidak"
                End If
                txt_taxNumberDirectorEntityToAccount.Text = .Tax_Number
                DisplayField_PEPDirectorEntityToAccount.Text = .Tax_Reg_Number
                txt_source_of_wealthDirectorEntityToAccount.Text = .Source_Of_Wealth
                txt_CommentDirectorEntityToAccount.Text = .Comment
                Cmb_roleTypeDirectorToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getPeranDalamKorporasiByKode(.role)

                listPhonedirectorentityToAccount = getListTransactionAccountEntityDirectorPhoneById(.PK_goAML_Trn_Director_NotReported_ID, False)
                bindPhoneDirectorEntityFromAccount(StorePhoneDirectorEntityToAccount, listPhonedirectorentityToAccount)

                listAddressdirectorentityToAccount = getListTransactionAccountEntityDirectorAddressById(.PK_goAML_Trn_Director_NotReported_ID, False)
                bindAddressDirectorEntityFromAccount(StoreAddressDirectorEntityToAccount, listAddressdirectorentityToAccount)

                listPhoneEmployerDirectorEntityToAccount = getListTransactionAccountEntityDirectorPhoneById(.PK_goAML_Trn_Director_NotReported_ID, True)
                bindPhoneDirectorEntityFromAccount(StorePhoneEmployerDirectorEntityToAccount, listPhoneEmployerDirectorEntityToAccount)

                listEmployerAddressdirectorentityToAccount = getListTransactionAccountEntityDirectorAddressById(.PK_goAML_Trn_Director_NotReported_ID, True)
                bindAddressDirectorEntityFromAccount(StoreAddressEmployerDirectorEntityToAccount, listEmployerAddressdirectorentityToAccount)

                listIdentificationDirectorEntityAccountTo = getListTransactionIdentificationById(.PK_goAML_Trn_Director_NotReported_ID, 4, 2)
                bindIdentification(StoreIdentificationDirectorEntityToAccount, listIdentificationDirectorEntityAccountTo)

            End With

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailSignatoryEntityFromAccount(id As String)
        Try
            Dim signatory As New NawaDevDAL.goAML_Trn_acc_Signatory_NotReported
            signatory = listTrnAcctSignatory.Where(Function(x) x.PK_goAML_Trn_acc_Sig_NotReported_ID = id).FirstOrDefault

            With signatory
                Cmb_genderPersonFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                txt_titlePersonFromAccount.Text = .Title
                txt_lastNamePersonFromAccount.Text = .Last_Name
                If .BirthDate IsNot Nothing Then
                    df_birthDatePersonFromAccount.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                End If
                txt_birthPlacePersonFromAccount.Text = .Birth_Place
                txt_motherNamePersonFromAccount.Text = .Mothers_Name
                txt_aliasPersonFromAccount.Text = .Alias
                txt_ssnPersonFromAccount.Text = .SSN
                txt_passportNumberPersonFromAccount.Text = .Passport_Number
                txt_passportCountryPersonFromAccount.Text = .Passport_Country
                txt_idNumberFromPersonAccount.Text = .Id_Number
                Cmb_nationality1PersonFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                Cmb_nationality2PersonFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                Cmb_nationality3PersonFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                Cmb_residencePersonFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
                txt_emailPersonFromAccount.Text = .Email
                txt_emailPersonFromAccount2.Text = .Email2
                txt_emailPersonFromAccount3.Text = .Email3
                txt_emailPersonFromAccount4.Text = .Email4
                txt_emailPersonFromAccount5.Text = .Email5
                txt_occupationPersonFromAccount.Text = .Occupation
                txt_employerNamePersonFromAccount.Text = .Employer_Name
                If .Deceased Then
                    DisplayField_DeceasePersonFromAccount.Text = "Ya"
                    If .Deceased_Date IsNot Nothing Then
                        df_deceased_datePersonFromAccount.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                    End If
                Else
                    DisplayField_DeceasePersonFromAccount.Text = "Tidak"
                End If
                txt_taxNumberPersonFromAccount.Text = .Tax_Number
                DisplayField_PEPpersonFromAccount.Text = .Tax_Reg_Number
                txt_source_of_wealthPersonFromAccount.Text = .Source_Of_Wealth
                txt_CommentPersonFromAccount.Text = .Comment
                If .isPrimary = True Then
                    isPrimaryFromAccount.Text = "Ya"
                ElseIf .isPrimary = False Then
                    isPrimaryFromAccount.Text = "Tidak"
                End If
                Cmb_roleFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getRoleTypebyCode(.role)
            End With
            WindowSignatoryFrom.Hidden = False

            'Signatory Account Phone, Address & Identification

            listPhoneSignatoryFromAccount = getListTransactionAccountSignatoryPhoneById(signatory.PK_goAML_Trn_acc_Sig_NotReported_ID, False)
            bindPhoneSignatoryFromAccount(StorePhonePersonFromAccount, listPhoneSignatoryFromAccount)

            listPhoneEmpoyerSignatoryFromAccount = getListTransactionAccountSignatoryPhoneById(signatory.PK_goAML_Trn_acc_Sig_NotReported_ID, True)
            bindPhoneSignatoryFromAccount(StorePhoneEmployerPersonIDFromAccount, listPhoneEmpoyerSignatoryFromAccount)

            listAddressSignatoryFromAccount = getListTransactionAccountSignatoryAddressById(signatory.PK_goAML_Trn_acc_Sig_NotReported_ID, False)
            bindAddressSignatoryFromAccount(StoreAddressPersonFromAccount, listAddressSignatoryFromAccount)

            listAddressEmployerSignatoryFromAccount = getListTransactionAccountSignatoryAddressById(signatory.PK_goAML_Trn_acc_Sig_NotReported_ID, True)
            bindAddressSignatoryFromAccount(StoreAddressEmployerPersonIDFromAccount, listAddressEmployerSignatoryFromAccount)

            listIdentificationSignatoryFromAccount = getListTransactionIdentificationById(signatory.PK_goAML_Trn_acc_Sig_NotReported_ID, 2, 1)
            bindIdentification(StoreIdentificationSignatoryFromAccount, listIdentificationSignatoryFromAccount)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailSignatoryEntityToAccount(id As String)
        Try
            Dim signatory As New NawaDevDAL.goAML_Trn_acc_Signatory_NotReported
            signatory = listTrnAcctSignatoryTo.Where(Function(x) x.PK_goAML_Trn_acc_Sig_NotReported_ID = id).FirstOrDefault

            With signatory
                Cmb_genderPersonToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                txt_titlePersonToAcount.Text = .Title
                txt_lastNamePersonToAccount.Text = .Last_Name
                If .BirthDate IsNot Nothing Then
                    df_birthDatePersonToAccount.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                End If
                txt_birthPlacePersonToAccount.Text = .Birth_Place
                txt_motherNamePersonToAccount.Text = .Mothers_Name
                txt_aliasPersonToAccount.Text = .Alias
                txt_ssnPersonToAccount.Text = .SSN
                txt_passportNumberPersonToAccount.Text = .Passport_Number
                txt_passportCountryPersonToAccount.Text = .Passport_Country
                txt_idNumberPersonToAccount.Text = .Id_Number
                Cmb_nationality1PersonToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                Cmb_nationality2PersonToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                Cmb_nationality3PersonToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                Cmb_residencePersonToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
                txt_emailPersonToAccount.Text = .Email
                txt_emailPersonToAccount2.Text = .Email2
                txt_emailPersonToAccount3.Text = .Email3
                txt_emailPersonToAccount4.Text = .Email4
                txt_emailPersonToAccount5.Text = .Email5
                txt_occupationPersonToAccount.Text = .Occupation
                txt_employerNamePersonToAccount.Text = .Employer_Name
                If .Deceased Then
                    DisplayField_DeceasePersonToAccount.Text = "Ya"
                    If .Deceased_Date IsNot Nothing Then
                        df_deceased_datePersonToAccount.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                    End If
                Else
                    DisplayField_DeceasePersonToAccount.Text = "Tidak"
                End If
                txt_taxNumberPersonToAccount.Text = .Tax_Number
                DisplayField__PEPPersonToAccount.Text = .Tax_Reg_Number
                txt_source_of_wealthPersonToAccount.Text = .Source_Of_Wealth
                txt_CommentPersonToAccount.Text = .Comment
                If .isPrimary = True Then
                    isPrimaryTo.Text = "Ya"
                ElseIf .isPrimary = False Then
                    isPrimaryTo.Text = "Tidak"
                End If
                CmbRoleTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getRoleTypebyCode(.role)
            End With
            windowSignatoryTo.Hidden = False

            'Signatory Account Phone, Address & Identification

            listPhoneSignatoryToAccount = getListTransactionAccountSignatoryPhoneById(signatory.PK_goAML_Trn_acc_Sig_NotReported_ID, False)
            bindPhoneSignatoryFromAccount(StorePhonePersonToAccount, listPhoneSignatoryToAccount)

            listPhoneEmployerSignatoryToAccount = getListTransactionAccountSignatoryPhoneById(signatory.PK_goAML_Trn_acc_Sig_NotReported_ID, True)
            bindPhoneSignatoryFromAccount(StorePhoneEmployerPersonToAccount, listPhoneEmployerSignatoryToAccount)

            listAddressSignatorytoAccount = getListTransactionAccountSignatoryAddressById(signatory.PK_goAML_Trn_acc_Sig_NotReported_ID, False)
            bindAddressSignatoryFromAccount(StoreAddressPersonToAccount, listAddressSignatorytoAccount)

            listAddressEmployerSignatoryToAccount = getListTransactionAccountSignatoryAddressById(signatory.PK_goAML_Trn_acc_Sig_NotReported_ID, True)
            bindAddressSignatoryFromAccount(StoreAddressEmployerPersonToAccount, listAddressEmployerSignatoryToAccount)

            listIdentificationSignatoryToAccount = getListTransactionIdentificationById(signatory.PK_goAML_Trn_acc_Sig_NotReported_ID, 2, 2)
            bindIdentification(StoreIdentificationSignatoryToAccount, listIdentificationSignatoryToAccount)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailPhoneDirectorFromAccount(id As String, command As String)
        Try
            WindowPhoneDirectorEntityFromAccount.Hidden = False
            WindowDirectorEntityFromAccount.Hidden = True
            Dim Phone As New NawaDevDAL.goaml_trn_Director_Phone_NotReported
            Phone = listPhonedirectorentityfromaccount.Where(Function(x) x.PK_goAML_trn_Dir_Phone_NotReported = id).FirstOrDefault

            With Phone
                CmbW_tph_contact_typePhoneDirectorEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                CmbW_tph_CommunicationTypePhoneDirectorEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                txtW_tphcountryPrefixPhoneDirectorEntityFromAccount.Text = .tph_country_prefix
                txtW_tphNumberPhoneDirectorEntityFromAccount.Text = .tph_number
                txtW_tphExtentionPhoneDirectorEntityFromAccount.Text = .tph_extension
                txtW_commentPhoneDirectorEntityFromAccount.Text = .comments
            End With

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DetailPhoneDirectorFrom(id As String, command As String)
        Try
            WindowPhoneDirectorEntityFrom.Hidden = False
            WindowDirectorEntityFrom.Hidden = True
            Dim Phone As New NawaDevDAL.goaml_trn_Director_Phone_NotReported
            Phone = listPhonedirectorentityfrom.Where(Function(x) x.PK_goAML_trn_Dir_Phone_NotReported = id).FirstOrDefault

            With Phone
                CmbW_tph_contact_typePhoneDirectorEntityFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                CmbW_tph_CommunicationTypePhoneDirectorEntityFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                txtW_tphcountryPrefixPhoneDirectorEntityFrom.Text = .tph_country_prefix
                txtW_tphNumberPhoneDirectorEntityFrom.Text = .tph_number
                txtW_tphExtentionPhoneDirectorEntityFrom.Text = .tph_extension
                txtW_commentPhoneDirectorEntityFrom.Text = .comments
            End With

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DetailPhoneDirectorTo(id As String, command As String)
        Try
            WindowPhoneDirectorEntityTo.Hidden = False
            WindowDirectorEntityTo.Hidden = True
            Dim Phone As New NawaDevDAL.goaml_trn_Director_Phone_NotReported
            Phone = listPhonedirectorentityTo.Where(Function(x) x.PK_goAML_trn_Dir_Phone_NotReported = id).FirstOrDefault

            With Phone
                CmbW_tph_contact_typePhoneDirectorEntityTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                CmbW_tph_CommunicationTypePhoneDirectorEntityTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                txtW_tphcountryPrefixPhoneDirectorEntityTo.Text = .tph_country_prefix
                txtW_tphNumberPhoneDirectorEntityTo.Text = .tph_number
                txtW_tphExtentionPhoneDirectorEntityTo.Text = .tph_extension
                txtW_commentPhoneDirectorEntityTo.Text = .comments
            End With

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailPhoneDirectorToAccount(id As String, command As String)
        Try
            WindowPhoneDirectorEntityToAccount.Hidden = False
            WindowDirectorEntityToAccount.Hidden = True
            Dim Phone As New NawaDevDAL.goaml_trn_Director_Phone_NotReported
            Phone = listPhonedirectorentityToAccount.Where(Function(x) x.PK_goAML_trn_Dir_Phone_NotReported = id).FirstOrDefault

            With Phone
                CmbW_tph_contact_typePhoneDirectorEntityToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                CmbW_tph_CommunicationTypePhoneDirectorEntityToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                txtW_tphcountryPrefixPhoneDirectorEntityToAccount.Text = .tph_country_prefix
                txtW_tphNumberPhoneDirectorEntityToAccount.Text = .tph_number
                txtW_tphExtentionPhoneDirectorEntityToAccount.Text = .tph_extension
                txtW_commentPhoneDirectorEntityToAccount.Text = .comments
            End With

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub EditPhoneEntityFrom(id As String, command As String)
        Try
            WindowPhoneEntityFrom.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Trn_Entity_Phone_NotReported
            phoneTemp = TransactionClass.listObjPhoneEntityFrom.Where(Function(x) x.PK_goAML_Trn_Ent_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEntityFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEntityFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEntityFrom.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEntityFrom.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEntityFrom.Text = phoneTemp.tph_extension
            txtW_commentPhoneEntityFrom.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneEntityFromAccount(id As String, command As String)
        Try
            WindowPhoneEntityFromAccount.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Trn_Acc_Entity_Phone_NotReported
            phoneTemp = TransactionClass.listObjPhoneAccountEntityFrom.Where(Function(x) x.PK_goAML_Trn_Acc_Ent_Phn_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEntityFromAccount.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEntityFromAccount.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEntityFromAccount.Text = phoneTemp.tph_extension
            txtW_commentPhoneEntityFromAccount.Text = phoneTemp.comments


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhonePersonFromAccount(id As String, command As String)
        Try
            WindowSignatoryFrom.Hidden = True
            WindowPhonePersonFromAccount.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported
            phoneTemp = listPhoneSignatoryFromAccount.Where(Function(x) x.PK_goAML_trn_acc_sign_Phn_NotReported = id).FirstOrDefault
            With phoneTemp
                CmbW_tph_contact_typePhonePersonFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                CmbW_tph_CommunicationTypePhonePersonFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                txtW_tphcountryPrefixPhonePersonFromAccount.Text = .tph_country_prefix
                txtW_tphNumberPhonePersonFromAccount.Text = .tph_number
                txtW_tphExtentionPhonePersonFromAccount.Text = .tph_extension
                txtW_commentPhonePersonFromAccount.Text = .comments
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneEmployerPersonFromAccount(id As String, command As String)
        Try
            WindowSignatoryFrom.Hidden = True
            WindowPhoneEmployerPersonIDFromAccount.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported
            phoneTemp = listPhoneEmpoyerSignatoryFromAccount.Where(Function(x) x.PK_goAML_trn_acc_sign_Phn_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEmployerPersonIDFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEmployerPersonIDFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEmployerPersonIDFromAccount.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEmployerPersonIDFromAccount.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEmployerPersonIDFromAccount.Text = phoneTemp.tph_extension
            txtW_commentPhoneEmployerPersonIDFromAccount.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneEntityTo(id As String, command As String)
        Try
            WindowPhoneEntityTo.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Trn_Entity_Phone_NotReported
            phoneTemp = TransactionClass.listObjPhoneEntityto.Where(Function(x) x.PK_goAML_Trn_Ent_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEntityTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEntityTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEntityTo.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEntityTo.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEntityTo.Text = phoneTemp.tph_extension
            txtW_commentPhoneEntityTo.Text = phoneTemp.comments


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub editAddressEntityFrom(id As String, command As String)
        Try

            WindowAddressEntityFrom.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Entity_Address_NotReported
            addressTemp = TransactionClass.ListObjAddressEntityFrom.Where(Function(x) x.PK_goAML_Trn_Ent_Address_NotReported_ID = id).FirstOrDefault

            txtW_AddressEntityAddressFrom.Text = addressTemp.Address
            txtW_cityEntityAddressFrom.Text = addressTemp.City
            txtW_CommentEntityAddressFrom.Text = addressTemp.Comments
            txtW_stateEntityAddressFrom.Text = addressTemp.State
            txtW_townEntityAddressFrom.Text = addressTemp.Town
            txtW_zipEntityAddressFrom.Text = addressTemp.Zip
            cmbW_addressTypeAddressEntityFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEntityAddressFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEntityFromAccount(id As String, command As String)
        Try

            WindowAddressEntityFromAccount.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Acc_Entity_Address_NotReported
            addressTemp = TransactionClass.listObjAddressAccountEntityFrom.Where(Function(x) x.PK_Trn_Acc_Ent_Add_NotReported_ID = id).FirstOrDefault

            txtW_AddressEntityAddressFromAccount.Text = addressTemp.Address
            txtW_cityEntityAddressFromAccount.Text = addressTemp.City
            txtW_CommentEntityAddressFromAccount.Text = addressTemp.Comments
            txtW_stateEntityAddressFromAccount.Text = addressTemp.State
            txtW_townEntityAddressFromAccount.Text = addressTemp.Town
            txtW_zipEntityAddressFromAccount.Text = addressTemp.Zip
            cmbW_addressTypeAddressEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEntityAddressFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressPersonFromAccount(id As String, command As String)
        Try
            WindowSignatoryFrom.Hidden = True
            WindowAddressPersonFromAccount.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported
            addressTemp = listAddressSignatoryFromAccount.Where(Function(x) x.PK_goAML_Trn_Acc_Ent_Add_NotReported_ID = id).FirstOrDefault

            txtW_AddressPersonFromAccount.Text = addressTemp.Address
            txtW_cityPersonAddressFromAccount.Text = addressTemp.City
            txtW_CommentPersonAddressFromAccount.Text = addressTemp.Comments
            txtW_statePersonAddressFromAccount.Text = addressTemp.State
            txtW_townPersonAddressFromAccount.Text = addressTemp.Town
            txtW_zipPersonAddressFromAccount.Text = addressTemp.Zip
            cmbW_addressTypeAddressPersonFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodePersonAddressFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEmployerPersonIDFromAccount(id As String, command As String)
        Try

            Dim addressTemp As NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported
            addressTemp = listAddressEmployerSignatoryFromAccount.Where(Function(x) x.PK_goAML_Trn_Acc_Ent_Add_NotReported_ID = id).FirstOrDefault

            WindowSignatoryFrom.Hidden = True

            txtW_AddressEmployerPersonIDFromAccount.Text = addressTemp.Address
            txtW_cityEmployerPersonIDFromAccount.Text = addressTemp.City
            txtW_CommentEmployerPersonIDFromAccount.Text = addressTemp.Comments
            txtW_stateEmployerPersonIDFromAccount.Text = addressTemp.State
            txtW_townEmployerPersonIDFromAccount.Text = addressTemp.Town
            txtW_zipEmployerPersonIDFromAccount.Text = addressTemp.Zip
            cmbW_addressTypeAddressEmployerPersonIDFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEmployerPersonIDFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

            WindowAddressEmployerPersonIDFromAccount.Hidden = False

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub editAddressEntityTo(id As String, command As String)
        Try
            WindowAddressEntityTo.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Entity_Address_NotReported
            addressTemp = TransactionClass.ListObjAddressEntityto.Where(Function(x) x.PK_goAML_Trn_Ent_Address_NotReported_ID = id).FirstOrDefault

            txtW_AddressEntityAddressTo.Text = addressTemp.Address
            txtW_cityEntityAddressTo.Text = addressTemp.City
            txtW_CommentEntityAddressTo.Text = addressTemp.Comments
            txtW_stateEntityAddressTo.Text = addressTemp.State
            txtW_townEntityAddressTo.Text = addressTemp.Town
            txtW_zipEntityAddressTo.Text = addressTemp.Zip
            cmbW_addressTypeAddressEntityTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEntityAddressTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressDirectorEntityFromAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressDirectorAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressDirectorEntityToAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressDirectorAccountTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressDirectorEntityFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressDirector(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressDirectorEntityTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressDirectorTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEmployerDirectorEntityFromAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressEmployerDirectorAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEmployerDirectorEntityToAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressEmployerDirectorAccountTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEmployerDirectorEntityFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressEmployerDirector(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEmployerDirectorEntityTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressEmployerDirectorTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandIdentificationDirectorEntityFromAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentificationAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandIdentificationconductor(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentificationConductor(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandIdentificationDirectorEntityToAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentificationAccountTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandIdentificationDirectorEntityFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentification(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandIdentificationDirectorEntityTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentificationTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployerDirectorEntityFromAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneEmployerDirectorAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployerDirectorEntityToAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneEmployerDirectorAccountTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployerDirectorEntityFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneEmployerDirector(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployerDirectorEntityTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneEmployerDirectorTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhonePersonFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhonePersonfrom(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneConductor(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneConductor(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub EditPhonePersonfrom(id As String, command As String)
        Try
            WindowPhonePersonFrom.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_trn_Person_Phone_NotReported
            phoneTemp = TransactionClass.listObjPhonePersonFrom.Where(Function(x) x.PK_goAML_trn_Prsn_Phn_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhonePersonFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhonePersonFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhonePersonFrom.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhonePersonFrom.Text = phoneTemp.tph_number
            txtW_tphExtentionPhonePersonFrom.Text = phoneTemp.tph_extension
            txtW_commentPhonePersonFrom.Text = phoneTemp.comments


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneConductor(id As String, command As String)
        Try
            WindowPhoneConductor.Hidden = False
            Dim phoneTemp As NawaDevDAL.goaml_trn_Conductor_Phone_NotReported
            phoneTemp = TransactionClass.listObjPhoneConductor.Where(Function(x) x.PK_goAML_trn_Cond_Phone_NotReported = id).FirstOrDefault
            With phoneTemp
                CmbW_tph_contact_typePhoneConductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                CmbW_tph_CommunicationTypePhoneConductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                txtW_tphcountryPrefixPhoneConductor.Text = .tph_country_prefix
                txtW_tphNumberPhoneConductor.Text = .tph_number
                txtW_tphExtentionPhoneConductor.Text = .tph_extension
                txtW_commentPhoneConductor.Text = .comments
            End With

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandPhoneEmployerPersonIDFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEmployerpersonFrom(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployerConductor(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEmployerconductor(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneEmployerconductor(id As String, command As String)
        Try
            WindowPhoneEmployerConductor.Hidden = False
            Dim phoneTemp As NawaDevDAL.goaml_trn_Conductor_Phone_NotReported
            phoneTemp = TransactionClass.listObjPhoneEmployerConductor.Where(Function(x) x.PK_goAML_trn_Cond_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEmployerConductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEmployerConductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEmployerConductor.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEmployerConductor.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEmployerConductor.Text = phoneTemp.tph_extension
            txtW_commentPhoneEmployerConductor.Text = phoneTemp.comments


        Catch ex As Exception

        End Try
    End Sub
    Sub EditPhoneEmployerpersonFrom(id As String, command As String)
        Try
            WindowPhoneEmployerPersonIDFrom.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_trn_Person_Phone_NotReported
            phoneTemp = TransactionClass.listObjPhoneEmployerPersonFrom.Where(Function(x) x.PK_goAML_trn_Prsn_Phn_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEmployerPersonIDFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEmployerPersonIDFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEmployerPersonIDFrom.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEmployerPersonIDFrom.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEmployerPersonIDFrom.Text = phoneTemp.tph_extension
            txtW_commentPhoneEmployerPersonIDFrom.Text = phoneTemp.comments

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridcommandEntityAccountTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEntityAccountTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEntityAccountTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editphoneentityToAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editphoneentityToAccount(id As String, command As String)
        Try
            WindowPhoneEntityToAccount.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Trn_Acc_Entity_Phone_NotReported
            phoneTemp = TransactionClass.listObjPhoneAccountEntityto.Where(Function(x) x.PK_goAML_Trn_Acc_Ent_Phn_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEntityToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEntityToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEntityToAccount.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEntityToAccount.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEntityToAccount.Text = phoneTemp.tph_extension
            txtW_commentPhoneEntityToAccount.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEntityAccountTo(id As String, command As String)
        Try

            WindowAddressEntitytoAccount.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Acc_Entity_Address_NotReported
            addressTemp = TransactionClass.listObjAddressAccountEntityto.Where(Function(x) x.PK_Trn_Acc_Ent_Add_NotReported_ID = id).FirstOrDefault

            txtW_AddressEntityAddresstoAccount.Text = addressTemp.Address
            txtW_cityEntityAddresstoAccount.Text = addressTemp.City
            txtW_CommentEntityAddresstoAccount.Text = addressTemp.Comments
            txtW_stateEntityAddresstoAccount.Text = addressTemp.State
            txtW_townEntityAddresstoAccount.Text = addressTemp.Town
            txtW_zipEntityAddresstoAccount.Text = addressTemp.Zip
            cmbW_addressTypeAddressEntitytoAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEntityAddresstoAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandAddressEmployerPersonToAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEmployerPersonToAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEmployerPersonToAccount(id As String, command As String)
        Try
            windowSignatoryTo.Hidden = True
            WindowAddressEmployerIDPersonToAccount.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported
            addressTemp = listAddressEmployerSignatoryToAccount.Where(Function(x) x.PK_goAML_Trn_Acc_Ent_Add_NotReported_ID = id).FirstOrDefault

            txtW_AddressEmployerPersonIDToAccount.Text = addressTemp.Address
            txtW_cityEmployerPersonIDToAccount.Text = addressTemp.City
            txtW_CommentEmployerPersonIDToAccount.Text = addressTemp.Comments
            txtW_stateEmployerPersonIDToAccount.Text = addressTemp.State
            txtW_townEmployerPersonIDToAccount.Text = addressTemp.Town
            txtW_zipEmployerPersonIDToAccount.Text = addressTemp.Zip
            cmbW_addressTypeAddressEmployerIDPersonToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEmployerPersonIDToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandPhonePersonTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhonePersonTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhonePersonTo(id As String, command As String)
        Try
            WindowPhonePersonTo.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_trn_Person_Phone_NotReported
            phoneTemp = TransactionClass.listObjPhonePersonTo.Where(Function(x) x.PK_goAML_trn_Prsn_Phn_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhonePersonTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhonePersonTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhonePersonTo.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhonePersonTo.Text = phoneTemp.tph_number
            txtW_tphExtentionPhonePersonTo.Text = phoneTemp.tph_extension
            txtW_commentPhonePersonTo.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressPersonTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressPersonTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressPersonTo(id As String, command As String)
        Try

            WindowAddressPersonTo.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Person_Address_NotReported
            addressTemp = TransactionClass.listObjAddressPersonTo.Where(Function(x) x.PK_goAML_Trn_Prsn_Add_NotReported_ID = id).FirstOrDefault

            txtW_AddressPersonTo.Text = addressTemp.Address
            txtW_cityPersonTo.Text = addressTemp.City
            txtW_CommentPersonTo.Text = addressTemp.Comments
            txtW_statePersonTo.Text = addressTemp.State
            txtW_townPersonTo.Text = addressTemp.Town
            txtW_zipPersonTo.Text = addressTemp.Zip
            cmbW_addressTypeAddressPersonTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodePersonTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandPhoneEmployerPersonTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEmployerPersonTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneEmployerPersonTo(id As String, command As String)
        Try
            WindowPhoneEmployerPersonIDTo.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_trn_Person_Phone_NotReported

            phoneTemp = TransactionClass.listObjPhoneEmployerPersonTo.Where(Function(x) x.PK_goAML_trn_Prsn_Phn_NotReported = id).FirstOrDefault
            CmbW_tph_contact_typePhoneEmployerPersonIDTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEmployerPersonIDTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEmployerPersonIDTo.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEmployerPersonIDTo.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEmployerPersonIDTo.Text = phoneTemp.tph_extension
            txtW_commentPhoneEmployerPersonIDTo.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressPersonFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddresspersonFrom(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressConductor(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressConductor(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddresspersonFrom(id As String, command As String)
        Try

            WindowAddressPersonFrom.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Person_Address_NotReported
            addressTemp = TransactionClass.listObjAddressPersonFrom.Where(Function(x) x.PK_goAML_Trn_Prsn_Add_NotReported_ID = id).FirstOrDefault

            txtW_AddressPersonFrom.Text = addressTemp.Address
            txtW_cityPersonFrom.Text = addressTemp.City
            txtW_CommentPersonFrom.Text = addressTemp.Comments
            txtW_statePersonFrom.Text = addressTemp.State
            txtW_townPersonFrom.Text = addressTemp.Town
            txtW_zipPersonFrom.Text = addressTemp.Zip
            cmbW_addressTypeAddressPersonFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodePersonFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressConductor(id As String, command As String)
        Try

            WindowAddressConductor.Hidden = False
            Dim addressTemp As NawaDevDAL.goaml_Trn_Conductor_Address_NotReported
            addressTemp = TransactionClass.listObjAddressConductor.Where(Function(x) x.PK_goAML_Trn_Cond_Address_NotReported = id).FirstOrDefault

            txtW_AddressConductor.Text = addressTemp.Address
            txtW_cityConductor.Text = addressTemp.City
            txtW_CommentConductor.Text = addressTemp.Comments
            txtW_stateConductor.Text = addressTemp.State
            txtW_townConductor.Text = addressTemp.Town
            txtW_zipConductor.Text = addressTemp.Zip
            cmbW_addressTypeAddressConductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeConductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandAddressEmployerPersonIDFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEmployerPersonFrom(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEmployerPersonFrom(id As String, command As String)
        Try

            WindowAddressEmployerPersonFrom.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Person_Address_NotReported
            addressTemp = TransactionClass.listObjAddressEmployerPersonFrom.Where(Function(x) x.PK_goAML_Trn_Prsn_Add_NotReported_ID = id).FirstOrDefault

            txtW_AddressEmployerPersonIDFrom.Text = addressTemp.Address
            txtW_cityEmployerPersonIDFrom.Text = addressTemp.City
            txtW_CommentEmployerPersonIDFrom.Text = addressTemp.Comments
            txtW_stateEmployerPersonIDFrom.Text = addressTemp.State
            txtW_townEmployerPersonIDFrom.Text = addressTemp.Town
            txtW_zipEmployerPersonIDFrom.Text = addressTemp.Zip
            cmbW_addressTypeAddressEmployerIDPersonFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEmployerPersonIDFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEmployerconductor(id As String, command As String)
        Try
            WindowAddressEmployerConductor.Hidden = False
            Dim addressTemp As NawaDevDAL.goaml_Trn_Conductor_Address_NotReported
            addressTemp = TransactionClass.listObjAddressEmployerConductor.Where(Function(x) x.PK_goAML_Trn_Cond_Address_NotReported = id).FirstOrDefault

            txtW_AddressEmployerConductor.Text = addressTemp.Address
            txtW_cityEmployerConductor.Text = addressTemp.City
            txtW_CommentEmployerConductor.Text = addressTemp.Comments
            txtW_stateEmployerConductor.Text = addressTemp.State
            txtW_townEmployerConductor.Text = addressTemp.Town
            txtW_zipEmployerConductor.Text = addressTemp.Zip
            cmbW_addressTypeAddressEmployerConductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEmployerConductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandAddressEntityAccountTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEntityAccountTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhonePersonToAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhonePersontoAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhonePersontoAccount(id As String, command As String)
        Try
            windowSignatoryTo.Hidden = True
            WindowPhonePersonToAccount.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported
            phoneTemp = listPhoneSignatoryToAccount.Where(Function(x) x.PK_goAML_trn_acc_sign_Phn_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhonePersonToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhonePersonToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhonePersonToAccount.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhonePersonToAccount.Text = phoneTemp.tph_number
            txtW_tphExtentionPhonePersonToAccount.Text = phoneTemp.tph_extension
            txtW_commentPhonePersonToAccount.Text = phoneTemp.comments



        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployerPersonToAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEmployerPersonToAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneEmployerPersonToAccount(id As String, command As String)
        Try
            windowSignatoryTo.Hidden = True
            WindowPhoneEmployerPersonIDToAccount.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported
            phoneTemp = listPhoneEmployerSignatoryToAccount.Where(Function(x) x.PK_goAML_trn_acc_sign_Phn_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEmployerPersonIDToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEmployerPersonIDToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEmployerPersonIDToAccount.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEmployerPersonIDToAccount.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEmployerPersonIDToAccount.Text = phoneTemp.tph_extension
            txtW_commentPhoneEmployerPersonIDToAccount.Text = phoneTemp.comments


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandAddressPersonToAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressPersonToAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressPersonToAccount(id As String, command As String)
        Try
            windowSignatoryTo.Hidden = True
            WindowAddressPersonToAccount.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported
            addressTemp = listAddressSignatorytoAccount.Where(Function(x) x.PK_goAML_Trn_Acc_Ent_Add_NotReported_ID = id).FirstOrDefault

            txtW_AddressPersonToAccount.Text = addressTemp.Address
            txtW_cityPersonToAccount.Text = addressTemp.City
            txtW_CommentPersonToAccount.Text = addressTemp.Comments
            txtW_statePersonToAccount.Text = addressTemp.State
            txtW_townPersonToAccount.Text = addressTemp.Town
            txtW_zipPersonToAccount.Text = addressTemp.Zip
            cmbW_addressTypeAddressPersonToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodePersonToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEmployerPersonTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEmployerPersonTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEmployerPersonTo(id As String, command As String)
        Try
            WindowAddressEmployerPersonTo.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Trn_Person_Address_NotReported
            addressTemp = TransactionClass.listObjAddressEmployerPersonTo.Where(Function(x) x.PK_goAML_Trn_Prsn_Add_NotReported_ID = id).FirstOrDefault

            txtW_AddressEmployerPersonIDTo.Text = addressTemp.Address
            txtW_cityEmployerPersonIDTo.Text = addressTemp.City
            txtW_CommentEmployerPersonIDTo.Text = addressTemp.Comments
            txtW_stateEmployerPersonIDTo.Text = addressTemp.State
            txtW_townEmployerPersonIDTo.Text = addressTemp.Town
            txtW_zipEmployerPersonIDTo.Text = addressTemp.Zip
            cmbW_addressTypeAddressEmployerIDPersonTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEmployerPersonIDTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandIdentificationPersonTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentificationPersonTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DetailAddressDirectorAccount(id As String, command As String)
        Try
            WindowDirectorEntityFromAccount.Hidden = True
            WindowAddressDirectorEntityFromAccount.Hidden = False
            Dim Address As New NawaDevDAL.goAML_Trn_Director_Address_NotReported
            Address = listAddressdirectorentityfromaccount.Where(Function(x) x.PK_goAML_Trn_Dir_Add_NotReported_ID = id).FirstOrDefault
            With Address
                cmbW_addressTypeAddressDirectorEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                txtW_AddressDirectorEntityFromAccount.Text = .Address
                txtW_townDirectorEntityAddressFromAccount.Text = .Town
                txtW_cityDirectorEntityAddressFromAccount.Text = .City
                txtW_zipDirectorEntityAddressFromAccount.Text = .Zip
                CmbW_countryCodeDirectorEntityAddressFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                txtW_stateDirectorEntityAddressFromAccount.Text = .State
                txtW_CommentDirectorEntityAddressFromAccount.Text = .Comments
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailAddressDirectorAccountTo(id As String, command As String)
        Try
            WindowDirectorEntityToAccount.Hidden = True
            WindowAddressDirectorEntityToAccount.Hidden = False
            Dim Address As New NawaDevDAL.goAML_Trn_Director_Address_NotReported
            Address = listAddressdirectorentityToAccount.Where(Function(x) x.PK_goAML_Trn_Dir_Add_NotReported_ID = id).FirstOrDefault
            With Address
                cmbW_addressTypeAddressDirectorEntityToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                txtW_AddressDirectorEntityToAccount.Text = .Address
                txtW_townDirectorEntityAddressToAccount.Text = .Town
                txtW_cityDirectorEntityAddressToAccount.Text = .City
                txtW_zipDirectorEntityAddressToAccount.Text = .Zip
                CmbW_countryCodeDirectorEntityAddressToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                txtW_stateDirectorEntityAddressToAccount.Text = .State
                txtW_CommentDirectorEntityAddressToAccount.Text = .Comments
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DetailAddressDirector(id As String, command As String)
        Try
            WindowDirectorEntityFrom.Hidden = True
            WindowAddressDirectorEntityFrom.Hidden = False
            Dim Address As New NawaDevDAL.goAML_Trn_Director_Address_NotReported
            Address = listAddressdirectorentityfrom.Where(Function(x) x.PK_goAML_Trn_Dir_Add_NotReported_ID = id).FirstOrDefault
            With Address
                cmbW_addressTypeAddressDirectorEntityFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                txtW_AddressDirectorEntityFrom.Text = .Address
                txtW_townDirectorEntityAddressFrom.Text = .Town
                txtW_cityDirectorEntityAddressFrom.Text = .City
                txtW_zipDirectorEntityAddressFrom.Text = .Zip
                CmbW_countryCodeDirectorEntityAddressFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                txtW_stateDirectorEntityAddressFrom.Text = .State
                txtW_CommentDirectorEntityAddressFrom.Text = .Comments
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailAddressDirectorTo(id As String, command As String)
        Try
            WindowDirectorEntityTo.Hidden = True
            WindowAddressDirectorEntityTo.Hidden = False
            Dim Address As New NawaDevDAL.goAML_Trn_Director_Address_NotReported
            Address = listAddressdirectorentityTo.Where(Function(x) x.PK_goAML_Trn_Dir_Add_NotReported_ID = id).FirstOrDefault
            With Address
                cmbW_addressTypeAddressDirectorEntityTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                txtW_AddressDirectorEntityTo.Text = .Address
                txtW_townDirectorEntityAddressTo.Text = .Town
                txtW_cityDirectorEntityAddressTo.Text = .City
                txtW_zipDirectorEntityAddressTo.Text = .Zip
                CmbW_countryCodeDirectorEntityAddressTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                txtW_stateDirectorEntityAddressTo.Text = .State
                txtW_CommentDirectorEntityAddressTo.Text = .Comments
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailAddressEmployerDirectorAccount(id As String, command As String)
        Try
            WindowDirectorEntityFromAccount.Hidden = True
            Dim address As New NawaDevDAL.goAML_Trn_Director_Address_NotReported
            address = listEmployerAddressdirectorentityfromaccount.Where(Function(x) x.PK_goAML_Trn_Dir_Add_NotReported_ID = id).FirstOrDefault
            With address
                cmbW_addressTypeAddressEmployerDirectorEntityIDFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                txtW_AddressEmployerDirectorEntityFromAccount.Text = .Address
                txtW_townEmployerDirectorEntityFromAccount.Text = .Town
                txtW_cityEmployerDirectorEntityFromAccount.Text = .City
                txtW_zipEmployerDirectorEntityFromAccount.Text = .Zip
                CmbW_countryCodeEmployerDirectorEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                txtW_stateEmployerDirectorEntityFromAccount.Text = .State
                txtW_CommentEmployerDirectorEntityFromAccount.Text = .Comments
            End With
            WindowAddressEmployerDirectorEntityFromAccount.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailAddressEmployerDirectorAccountTo(id As String, command As String)
        Try
            WindowDirectorEntityToAccount.Hidden = True
            Dim address As New NawaDevDAL.goAML_Trn_Director_Address_NotReported
            address = listEmployerAddressdirectorentityToAccount.Where(Function(x) x.PK_goAML_Trn_Dir_Add_NotReported_ID = id).FirstOrDefault
            With address
                cmbW_addressTypeAddressEmployerDirectorEntityIDToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                txtW_AddressEmployerDirectorEntityToAccount.Text = .Address
                txtW_townEmployerDirectorEntityToAccount.Text = .Town
                txtW_cityEmployerDirectorEntityToAccount.Text = .City
                txtW_zipEmployerDirectorEntityToAccount.Text = .Zip
                CmbW_countryCodeEmployerDirectorEntityToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                txtW_stateEmployerDirectorEntityToAccount.Text = .State
                txtW_CommentEmployerDirectorEntityToAccount.Text = .Comments
            End With
            WindowAddressEmployerDirectorEntityToAccount.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailAddressEmployerDirector(id As String, command As String)
        Try
            WindowDirectorEntityFrom.Hidden = True
            Dim address As New NawaDevDAL.goAML_Trn_Director_Address_NotReported
            address = listEmployerAddressdirectorentityfrom.Where(Function(x) x.PK_goAML_Trn_Dir_Add_NotReported_ID = id).FirstOrDefault
            With address
                cmbW_addressTypeAddressEmployerDirectorEntityIDFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                txtW_AddressEmployerDirectorEntityFrom.Text = .Address
                txtW_townEmployerDirectorEntityFrom.Text = .Town
                txtW_cityEmployerDirectorEntityFrom.Text = .City
                txtW_zipEmployerDirectorEntityFrom.Text = .Zip
                CmbW_countryCodeEmployerDirectorEntityFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                txtW_stateEmployerDirectorEntityFrom.Text = .State
                txtW_CommentEmployerDirectorEntityFrom.Text = .Comments
            End With
            WindowAddressEmployerDirectorEntityFrom.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailAddressEmployerDirectorTo(id As String, command As String)
        Try
            WindowDirectorEntityTo.Hidden = True
            Dim address As New NawaDevDAL.goAML_Trn_Director_Address_NotReported
            address = listEmployerAddressdirectorentityTo.Where(Function(x) x.PK_goAML_Trn_Dir_Add_NotReported_ID = id).FirstOrDefault
            With address
                cmbW_addressTypeAddressEmployerDirectorEntityIDTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                txtW_AddressEmployerDirectorEntityTo.Text = .Address
                txtW_townEmployerDirectorEntityTo.Text = .Town
                txtW_cityEmployerDirectorEntityTo.Text = .City
                txtW_zipEmployerDirectorEntityTo.Text = .Zip
                CmbW_countryCodeEmployerDirectorEntityTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                txtW_stateEmployerDirectorEntityTo.Text = .State
                txtW_CommentEmployerDirectorEntityTo.Text = .Comments
            End With
            WindowAddressEmployerDirectorEntityTo.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailPhoneEmployerDirectorAccount(id As String, command As String)
        Try
            WindowDirectorEntityFromAccount.Hidden = True
            WindowPhoneEmployerDirectorEntityIDFromAccount.Hidden = False
            Dim phone As New NawaDevDAL.goaml_trn_Director_Phone_NotReported
            phone = listPhoneEmployerDirectorEntityfromaccount.Where(Function(x) x.PK_goAML_trn_Dir_Phone_NotReported = id).FirstOrDefault
            With phone
                CmbW_tph_contact_typePhoneEmployerDirectorEntityIDFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                CmbW_tph_CommunicationTypePhoneEmployerDirectorEntityIDFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                txtW_tphcountryPrefixPhoneEmployerDirectorEntityIDFromAccount.Text = .tph_country_prefix
                txtW_tphNumberPhoneEmployerDirectorEntityIDFromAccount.Text = .tph_number
                txtW_tphExtentionPhoneEmployerDirectorEntityIDFromAccount.Text = .tph_extension
                txtW_commentPhoneEmployerDirectorEntityIDFromAccount.Text = .comments
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailPhoneEmployerDirectorAccountTo(id As String, command As String)
        Try
            WindowDirectorEntityToAccount.Hidden = True
            WindowPhoneEmployerDirectorEntityIDToAccount.Hidden = False
            Dim phone As New NawaDevDAL.goaml_trn_Director_Phone_NotReported
            phone = listPhoneEmployerDirectorEntityToAccount.Where(Function(x) x.PK_goAML_trn_Dir_Phone_NotReported = id).FirstOrDefault
            With phone
                CmbW_tph_contact_typePhoneEmployerDirectorEntityIDToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                CmbW_tph_CommunicationTypePhoneEmployerDirectorEntityIDToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                txtW_tphcountryPrefixPhoneEmployerDirectorEntityIDToAccount.Text = .tph_country_prefix
                txtW_tphNumberPhoneEmployerDirectorEntityIDToAccount.Text = .tph_number
                txtW_tphExtentionPhoneEmployerDirectorEntityIDToAccount.Text = .tph_extension
                txtW_commentPhoneEmployerDirectorEntityIDToAccount.Text = .comments
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailPhoneEmployerDirector(id As String, command As String)
        Try
            WindowDirectorEntityFrom.Hidden = True
            WindowPhoneEmployerDirectorEntityIDFrom.Hidden = False
            Dim phone As New NawaDevDAL.goaml_trn_Director_Phone_NotReported
            phone = listPhoneEmployerDirectorEntityfrom.Where(Function(x) x.PK_goAML_trn_Dir_Phone_NotReported = id).FirstOrDefault
            With phone
                CmbW_tph_contact_typePhoneEmployerDirectorEntityIDFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                CmbW_tph_CommunicationTypePhoneEmployerDirectorEntityIDFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                txtW_tphcountryPrefixPhoneEmployerDirectorEntityIDFrom.Text = .tph_country_prefix
                txtW_tphNumberPhoneEmployerDirectorEntityIDFrom.Text = .tph_number
                txtW_tphExtentionPhoneEmployerDirectorEntityIDFrom.Text = .tph_extension
                txtW_commentPhoneEmployerDirectorEntityIDFrom.Text = .comments
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailPhoneEmployerDirectorTo(id As String, command As String)
        Try
            WindowDirectorEntityTo.Hidden = True
            WindowPhoneEmployerDirectorEntityIDTo.Hidden = False
            Dim phone As New NawaDevDAL.goaml_trn_Director_Phone_NotReported
            phone = listPhoneEmployerDirectorEntityTo.Where(Function(x) x.PK_goAML_trn_Dir_Phone_NotReported = id).FirstOrDefault
            With phone
                CmbW_tph_contact_typePhoneEmployerDirectorEntityIDTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                CmbW_tph_CommunicationTypePhoneEmployerDirectorEntityIDTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                txtW_tphcountryPrefixPhoneEmployerDirectorEntityIDTo.Text = .tph_country_prefix
                txtW_tphNumberPhoneEmployerDirectorEntityIDTo.Text = .tph_number
                txtW_tphExtentionPhoneEmployerDirectorEntityIDTo.Text = .tph_extension
                txtW_commentPhoneEmployerDirectorEntityIDTo.Text = .comments
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailIdentificationAccount(id As String, command As String)
        Try
            WindowDirectorEntityFromAccount.Hidden = True
            WindowIdentificationDirectorEntityFromAccount.Hidden = False
            Dim identification As New NawaDevDAL.goAML_Transaction_Person_Identification_NotReported
            identification = listIdentificationDirectorEntityAccountFrom.Where(Function(x) x.PK_goAML_Trn_Prsn_Ident_NotReported_ID = id).FirstOrDefault
            With identification
                Cmb_jenisIdentitasDirectorEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                txt_numberIdentificationDirectorEntityFromAccount.Text = .Number
                df_IssueDateIdentificationDirectorEntityFromAccount.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                df_expiry_dateIdentificationDirectorEntityFromAccount.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                txt_issueByDirectorEntityFromAccount.Text = .Issued_By
                Cmb_countryIdentificationDirectorEntityFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                Txt_commentIdentificationDirectorEntityFromAccount.Text = .Identification_Comment
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailIdentificationConductor(id As String, command As String)
        Try
            WindowIdentificationConductor.Hidden = False
            Dim identification As New NawaDevDAL.goAML_Transaction_Person_Identification_NotReported
            identification = TransactionClass.listObjIdentificationConductor.Where(Function(x) x.PK_goAML_Trn_Prsn_Ident_NotReported_ID = id).FirstOrDefault
            With identification
                Cmb_jenisIdentitasConductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                txt_numberIdentificationConductor.Text = .Number
                df_IssueDateIdentificationConductor.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                df_expiry_dateIdentificationConductor.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                txt_issueByConductor.Text = .Issued_By
                Cmb_countryIdentificationConductor.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                Txt_commentIdentificationConductor.Text = .Identification_Comment
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailIdentificationAccountTo(id As String, command As String)
        Try
            WindowDirectorEntityToAccount.Hidden = True
            WindowIdentificationDirectorEntityToAccount.Hidden = False
            Dim identification As New NawaDevDAL.goAML_Transaction_Person_Identification_NotReported
            identification = listIdentificationDirectorEntityAccountTo.Where(Function(x) x.PK_goAML_Trn_Prsn_Ident_NotReported_ID = id).FirstOrDefault
            With identification
                Cmb_jenisIdentitasDirectorEntityToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                txt_numberIdentificationDirectorEntityToAccount.Text = .Number
                df_IssueDateIdentificationDirectorEntityToAccount.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                df_expiry_dateIdentificationDirectorEntityToAccount.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                txt_issueByDirectorEntityToAccount.Text = .Issued_By
                Cmb_countryIdentificationDirectorEntityToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                Txt_commentIdentificationDirectorEntityToAccount.Text = .Identification_Comment
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailIdentification(id As String, command As String)
        Try
            WindowDirectorEntityFrom.Hidden = True
            WindowIdentificationDirectorEntityFrom.Hidden = False
            Dim identification As New NawaDevDAL.goAML_Transaction_Person_Identification_NotReported
            identification = listIdentificationDirectorEntityFrom.Where(Function(x) x.PK_goAML_Trn_Prsn_Ident_NotReported_ID = id).FirstOrDefault
            With identification
                Cmb_jenisIdentitasDirectorEntityFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                txt_numberIdentificationDirectorEntityFrom.Text = .Number
                df_IssueDateIdentificationDirectorEntityFrom.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                df_expiry_dateIdentificationDirectorEntityFrom.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                txt_issueByDirectorEntityFrom.Text = .Issued_By
                Cmb_countryIdentificationDirectorEntityFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                Txt_commentIdentificationDirectorEntityFrom.Text = .Identification_Comment
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailIdentificationTo(id As String, command As String)
        Try
            WindowDirectorEntityTo.Hidden = True
            WindowIdentificationDirectorEntityTo.Hidden = False
            Dim identification As New NawaDevDAL.goAML_Transaction_Person_Identification_NotReported
            identification = listIdentificationDirectorEntityTo.Where(Function(x) x.PK_goAML_Trn_Prsn_Ident_NotReported_ID = id).FirstOrDefault
            With identification
                Cmb_jenisIdentitasDirectorEntityTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                txt_numberIdentificationDirectorEntityTo.Text = .Number
                df_IssueDateIdentificationDirectorEntityTo.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                df_expiry_dateIdentificationDirectorEntityTo.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                txt_issueByDirectorEntityTo.Text = .Issued_By
                Cmb_countryIdentificationDirectorEntityTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                Txt_commentIdentificationDirectorEntityTo.Text = .Identification_Comment
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub DetailIdentificationPersonTo(id As String, command As String)
        Try
            WindowIdentificationPersonTo.Hidden = False
            Dim identification As New NawaDevDAL.goAML_Transaction_Person_Identification_NotReported
            identification = TransactionClass.listObjIdentificationPersonTo.Where(Function(x) x.PK_goAML_Trn_Prsn_Ident_NotReported_ID = id).FirstOrDefault
            With identification
                Cmb_jenisIdentitasPersonTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                txt_numberIdentificationPersonto.Text = .Number
                df_IssueDateIdentificationPersonTo.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                df_expiry_dateIdentificationPersonTo.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                txt_issueByPersonTo.Text = .Issued_By
                Cmb_countryIdentificationPersonTo.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                Txt_commentIdentificationPersonTo.Text = .Identification_Comment
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editIdentificationSignatoryFromAccount(id As String, command As String)
        Try
            WindowSignatoryFrom.Hidden = True
            WindowIdentificationSignatoryFromAccount.Hidden = False
            Dim identification As NawaDevDAL.goAML_Transaction_Person_Identification_NotReported
            identification = listIdentificationSignatoryFromAccount.Where(Function(x) x.PK_goAML_Trn_Prsn_Ident_NotReported_ID = id).FirstOrDefault

            With identification
                Cmb_jenisIdentitasSignatoryFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                txt_numberIdentificationSignatoryFromAccount.Text = .Number
                df_IssueDateIdentificationSignatoryFromAccount.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                df_expiry_dateIdentificationSignatoryFromAccount.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                txt_issueBySignatoryFromAccount.Text = .Issued_By
                Cmb_countryIdentificationSignatoryFromAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                Txt_commentIdentificationSignatoryFromAccount.Text = .Identification_Comment
            End With

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandIdentificationSignatoryFromAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editIdentificationSignatoryFromAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editIdentificationSignatoryToAccount(id As String, command As String)
        Try
            windowSignatoryTo.Hidden = True
            WindowIdentificationSignatoryToAccount.Hidden = False
            Dim identification As NawaDevDAL.goAML_Transaction_Person_Identification_NotReported
            identification = listIdentificationSignatoryToAccount.Where(Function(x) x.PK_goAML_Trn_Prsn_Ident_NotReported_ID = id).FirstOrDefault
            With identification
                Cmb_jenisIdentitasSignatoryToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                txt_numberIdentificationSignatoryToAccount.Text = .Number
                df_IssueDateIdentificationSignatoryToAccount.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                df_expiry_dateIdentificationSignatoryToAccount.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                txt_issueBySignatoryToAccount.Text = .Issued_By
                Cmb_countryIdentificationSignatoryToAccount.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                Txt_commentIdentificationSignatoryToAccount.Text = .Identification_Comment
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandIdentificationSignatoryToAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editIdentificationSignatoryToAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandPhoneDirectorEntityFromAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneDirectorFromAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandPhoneDirectorEntityFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneDirectorFrom(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandPhoneDirectorEntityTo(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneDirectorTo(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneDirectorEntityToAccount(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneDirectorToAccount(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandIdentificationPersonFrom(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editIdentificationPersonFrom(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editIdentificationPersonFrom(id As String, command As String)
        Try

            WindowIdentificationPersonFrom.Hidden = False
            Dim identification As NawaDevDAL.goAML_Transaction_Person_Identification_NotReported
            identification = TransactionClass.listObjIdentificationPersonFrom.Where(Function(x) x.PK_goAML_Trn_Prsn_Ident_NotReported_ID = id).FirstOrDefault
            With identification
                Cmb_jenisIdentitasPersonFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                txt_numberIdentificationPersonFrom.Text = .Number
                df_IssueDateIdentificationPersonFrom.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                df_expiry_dateIdentificationpersonFrom.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                txt_issueByPersonFrom.Text = .Issued_By
                Cmb_countryIdentificationPersonFrom.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                Txt_commentIdentificationPersonFrom.Text = .Identification_Comment
            End With
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#End Region

#Region "Bind Data"
    Sub bindReportIndicator(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Report_Indicator_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        '  objtable.Columns.Add(New DataColumn("Kode_Indicator", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                '     item("Kode_Indicator") = item("FK_Indicator")
                item("Keterangan") = getReportIndicatorByKode(item("FK_Indicator").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhone(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Ref_Phone))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhonePerson(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_trn_Person_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("PK_goAML_trn_Person_P", GetType(String)))
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_trn_Person_P") = item("PK_goAML_trn_Prsn_Phn_NotReported")
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhoneEntityAccountParty(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhoneDirectorEntityParty(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddressDirectorEntityparty(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddressEntityAccountParty(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindDirectorEntityAccountParty(store As Ext.Net.Store, listDirector As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listDirector)
        objtable.Columns.Add(New DataColumn("Nama", GetType(String)))
        objtable.Columns.Add(New DataColumn("Peran", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Nama") = item("Last_Name")
                item("Peran") = NawaDevBLL.GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(item("role").ToString)

            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhoneDirector(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub bindPhoneSignatoryFromAccount(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("PK_goAML_trn_acc_sign_P", GetType(String)))
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_trn_acc_sign_P") = item("PK_goAML_trn_acc_sign_Phn_NotReported")
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhoneEntityFrom(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Trn_Entity_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("PK_goAML_Trn_Entity_P", GetType(String)))
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_Trn_Entity_P") = item("PK_goAML_Trn_Ent_Phone_NotReported")
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddress(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Ref_Address))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddressPerson(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Trn_Person_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("PK_goAML_Trn_Person_A", GetType(String)))
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_Trn_Person_A") = item("PK_goAML_Trn_Prsn_Add_NotReported_ID")
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub bindAddressEntityFromAccount(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("PK_Trn_Acc_EA", GetType(String)))
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_Trn_Acc_EA") = item("PK_Trn_Acc_Ent_Add_NotReported_ID")
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindIdentification(store As Ext.Net.Store, listIdentification As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
        objtable.Columns.Add(New DataColumn("PK_goAML_Transaction_DI", GetType(String)))
        objtable.Columns.Add(New DataColumn("tipe", GetType(String)))
        objtable.Columns.Add(New DataColumn("no", GetType(String)))
        objtable.Columns.Add(New DataColumn("tanggalTerbit", GetType(Date)))
        objtable.Columns.Add(New DataColumn("tanggalKadaluarsa", GetType(Date)))
        objtable.Columns.Add(New DataColumn("negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_Transaction_DI") = item("PK_goAML_Trn_Prsn_Ident_NotReported_ID")
                item("tipe") = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(item("Type").ToString)
                item("no") = item("Number")
                item("tanggalTerbit") = item("Issue_Date")
                item("tanggalKadaluarsa") = item("Expiry_Date")
                item("negara") = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Issued_Country").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub bindAddressEntityFrom(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Trn_Entity_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("PK_goAML_Trn_Entity_A", GetType(String)))
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_Trn_Entity_A") = item("PK_goAML_Trn_Ent_Address_NotReported_ID")
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    'list Signatory Address & Phone
    Sub bindAddressSignatoryAccountParty(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhoneSignatoryAccountParty(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddressSignatoryFromAccount(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("PK_goAML_Trn_Acc_Entity_A", GetType(String)))
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_Trn_Acc_Entity_A") = item("PK_goAML_Trn_Acc_Ent_Add_NotReported_ID")
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    'List Transaction
    Sub bindTransaction(store As Ext.Net.Store, listTransaction As List(Of NawaDevDAL.goAML_Transaction_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listTransaction)
        objtable.Columns.Add(New DataColumn("tangggalTransaksi", GetType(Date)))
        objtable.Columns.Add(New DataColumn("caraTransaksi", GetType(String)))
        objtable.Columns.Add(New DataColumn("amount", GetType(Double)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("tangggalTransaksi") = item("date_transaction")
                item("caraTransaksi") = NawaDevBLL.GlobalReportFunctionBLL.getJenisTransactionByCode(item("transmode_code").ToString)
                item("amount") = item("amount_local")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    'List Conductor Phone & Address
    Sub bindConductorPhone(store As Ext.Net.Store, listConductorPhone As List(Of NawaDevDAL.goaml_trn_Conductor_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listConductorPhone)
        objtable.Columns.Add(New DataColumn("PK_goAML_trn_CP", GetType(Integer)))
        objtable.Columns.Add(New DataColumn("TipeKontak", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_trn_CP") = item("PK_goAML_trn_Cond_Phone_NotReported")
                item("TipeKontak") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindConductorAddress(store As Ext.Net.Store, listConductorPhone As List(Of NawaDevDAL.goaml_Trn_Conductor_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listConductorPhone)
        objtable.Columns.Add(New DataColumn("PK_goAML_Trn_CA", GetType(Integer)))
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_Trn_CA") = item("PK_goAML_Trn_Cond_Address_NotReported")
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                item("Negara") = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    'List Transaction Account From
    Sub bindPhoneEntityFromAccount(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("PK_goAML_Trn_Acc_EP", GetType(String)))
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_Trn_Acc_EP") = item("PK_goAML_Trn_Acc_Ent_Phn_NotReported")
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    'List Transaction Account Director From
    Sub bindDirectorEntityFromAccount(store As Ext.Net.Store, listDirector As List(Of NawaDevDAL.goAML_Trn_Director_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listDirector)
        objtable.Columns.Add(New DataColumn("PK_goAML_trn_D", GetType(String)))
        objtable.Columns.Add(New DataColumn("Nama", GetType(String)))
        objtable.Columns.Add(New DataColumn("Peran", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_trn_D") = item("PK_goAML_Trn_Director_NotReported_ID")
                item("Nama") = item("Last_Name")
                item("Peran") = NawaDevBLL.GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(item("role").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhonePersonParty(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddressPersonParty(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Trn_Party_Person_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhoneEntityParty(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindDirectorEntityParty(store As Ext.Net.Store, listDirector As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listDirector)
        objtable.Columns.Add(New DataColumn("Nama", GetType(String)))
        objtable.Columns.Add(New DataColumn("Peran", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Nama") = item("Last_Name")
                item("Peran") = NawaDevBLL.GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(item("role").ToString)

            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddressEntityParty(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindIdentificationParty(store As Ext.Net.Store, listIdentification As List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
        objtable.Columns.Add(New DataColumn("tipe", GetType(String)))
        objtable.Columns.Add(New DataColumn("no", GetType(String)))
        objtable.Columns.Add(New DataColumn("tanggalTerbit", GetType(Date)))
        objtable.Columns.Add(New DataColumn("tanggalKadaluarsa", GetType(Date)))
        objtable.Columns.Add(New DataColumn("negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("tipe") = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(item("Type").ToString)
                item("no") = item("Number")
                item("tanggalTerbit") = item("Issue_Date")
                item("tanggalKadaluarsa") = item("Expiry_Date")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Issued_Country").ToString)

                item("negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhoneDirectorEntityAccountParty(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddressDirectorEntityAccountParty(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    'Account Entity Director
    Sub bindPhoneDirectorEntityFromAccount(store As Ext.Net.Store, listDirectorP As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listDirectorP)
        objtable.Columns.Add(New DataColumn("PK_goAML_trn_acc_sign_Phone_AE", GetType(String)))
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_trn_acc_sign_Phone_AE") = item("PK_goAML_trn_Dir_Phone_NotReported")
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub bindAddressDirectorEntityFromAccount(store As Ext.Net.Store, listConductorA As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listConductorA)
        objtable.Columns.Add(New DataColumn("PK_goAML_Trn_Acc_Entity_A", GetType(Integer)))
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_Trn_Acc_Entity_A") = item("PK_goAML_Trn_Dir_Add_NotReported_ID")
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                item("Negara") = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindSignatoryAccountParty(store As Ext.Net.Store, listSignatory As List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listSignatory)
        objtable.Columns.Add(New DataColumn("Primary", GetType(String)))
        objtable.Columns.Add(New DataColumn("Nama", GetType(String)))
        objtable.Columns.Add(New DataColumn("Peran", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                If (item("isPrimary") IsNot Nothing AndAlso Not IsDBNull(item("isPrimary"))) Then
                    item("Primary") = IIf(item("isPrimary") = True, "Ya", "Tidak")
                End If
                'item("Primary") = IIf(item("isPrimary") = True, "Ya", "Tidak")
                item("Nama") = item("Last_Name")
                item("Peran") = NawaDevBLL.GlobalReportFunctionBLL.getRoleTypebyCode(item("role").ToString)

            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindSignatoryEntityFromAccount(store As Ext.Net.Store, listSignatory As List(Of NawaDevDAL.goAML_Trn_acc_Signatory_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listSignatory)
        objtable.Columns.Add(New DataColumn("PK_goAML_trn_acc_sign_ID", GetType(Integer)))
        objtable.Columns.Add(New DataColumn("Primary", GetType(String)))
        objtable.Columns.Add(New DataColumn("Nama", GetType(String)))
        objtable.Columns.Add(New DataColumn("Peran", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_trn_acc_sign_ID") = item("PK_goAML_Trn_acc_Sig_NotReported_ID")
                If (item("isPrimary") IsNot Nothing AndAlso Not IsDBNull(item("isPrimary"))) Then
                    item("Primary") = IIf(item("isPrimary") = True, "Ya", "Tidak")
                End If

                'item("Primary") = IIf(item("isPrimary") = True, "Ya", "Tidak")
                item("Nama") = item("Last_Name")
                item("Peran") = NawaDevBLL.GlobalReportFunctionBLL.getRoleTypebyCode(item("role").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
#End Region

#Region "Method / Function"
    Sub ColumnActionPosition()
        ColumnActionLocation(GridPanelReportIndicator, CommandColumn87)
        ColumnActionLocation(GridTransaction, commandTransaksi)
        ColumnActionLocation(GridPanel_phoneConductor, CommandColumn27)
        ColumnActionLocation(GridPanel_addressConductor, CommandColumn28)
        ColumnActionLocation(GridPanel_employerAddressIDConductor, CommandColumn29)
        ColumnActionLocation(GridPanel_EmployerPhoneIDConductor, CommandColumn30)
        ColumnActionLocation(GridPanelConductorIdent, CommandColumn59)


        ColumnActionLocation(GridPanelSignatoryFrom, CommandColumn25)

        ColumnActionLocation(GridPhoneEntityFromAccount, CommandColumn1)
        ColumnActionLocation(GridPanelAddressEntityFromAccount, CommandColumn2)
        ColumnActionLocation(GridPanelDirectorEntityFromAccount, CommandColumn35)

        ColumnActionLocation(GridPanel_phonePersonFrom, CommandColumn5)
        ColumnActionLocation(GridPanel_addressPersonFrom, CommandColumn6)
        ColumnActionLocation(GridPanel_employerAddressIDPersonFrom, CommandColumn7)
        ColumnActionLocation(GridPanel_EmployerPhoneIDPersonFrom, CommandColumn8)
        ColumnActionLocation(GridPanelIdentificationPersonFrom, CommandColumn31)

        ColumnActionLocation(GridPanelTeleponEntityFrom, CommandColumn3)
        ColumnActionLocation(GridPanel_AddressEntityFrom, CommandColumn4)
        ColumnActionLocation(GridPanelDirectorEntity, CommandColumn41)

        ColumnActionLocation(GridPanelSignatoryTo, CommandColumn26)
        ColumnActionLocation(GridPanel_PhoneEntityAccountTo, CommandColumn9)
        ColumnActionLocation(GridPanel_AddressEntityAccountTo, CommandColumn10)
        ColumnActionLocation(GridPanelDirectorEntityToAccount, CommandColumn53)

        ColumnActionLocation(GridPanel_phonePersonTo, CommandColumn11)
        ColumnActionLocation(GridPanel_addressPersonTo, CommandColumn12)
        ColumnActionLocation(GridPanel_employerAddressIDPersonTo, CommandColumn13)
        ColumnActionLocation(GridPanel_EmployerPhoneIDPersonTo, CommandColumn14)
        ColumnActionLocation(GridPanelIdentificationPersonTo, CommandColumn32)

        ColumnActionLocation(GridPanelTeleponEntityTo, CommandColumn15)
        ColumnActionLocation(GridPanel_AddressEntityTo, CommandColumn16)
        ColumnActionLocation(GridPanel_DirectorEntityTo, CommandColumn47)

        ColumnActionLocation(GridPanelSignatoryParty, CommandColumn60)
        ColumnActionLocation(GridPhoneEntityAccountparty, CommandColumn61)
        ColumnActionLocation(GridPanelAddressEntityAccountparty, CommandColumn62)
        ColumnActionLocation(GridPanelDirectorEntityAccountparty, CommandColumn63)

        ColumnActionLocation(GridPanel_phonePersonParty, CommandColumn64)
        ColumnActionLocation(GridPanel_addressPersonParty, CommandColumn75)
        ColumnActionLocation(GridPanel_employerAddressIDPersonParty, CommandColumn76)
        ColumnActionLocation(GridPanel_EmployerPhoneIDPersonParty, CommandColumn77)
        ColumnActionLocation(GridPanelIdentificationPersonParty, CommandColumn78)

        ColumnActionLocation(GridPanelTeleponEntityParty, CommandColumn79)
        ColumnActionLocation(GridPanel_AddressEntityParty, CommandColumn80)
        ColumnActionLocation(GridPanelDirectorEntityParty, CommandColumn81)

        ColumnActionLocation(GridPanel_phoneDirectorEntityFromAccount, CommandColumn36)
        ColumnActionLocation(GridPanel_addressDirectorEntityFromAccount, CommandColumn37)
        ColumnActionLocation(GridPanel_employerAddressDirectorEntityFromAccount, CommandColumn38)
        ColumnActionLocation(GridPanel_EmployerPhoneDirectorEntityFromAccount, CommandColumn39)
        ColumnActionLocation(GridPanelIdentificationDirectorEntityFromAccount, CommandColumn40)

        ColumnActionLocation(GridPanel_phoneSignatoryAccountparty, CommandColumn65)
        ColumnActionLocation(GridPanel_addressSignatoryAccountparty, CommandColumn66)
        ColumnActionLocation(GridPanel_employerAddressIDSignatoryAccountparty, CommandColumn67)
        ColumnActionLocation(GridPanel_EmployerPhoneIDSignatoryAccountparty, CommandColumn68)
        ColumnActionLocation(GridPanelIdentificationSignatoryAccountparty, CommandColumn69)

        ColumnActionLocation(GridPanel_phoneDirectorEntityAccountParty, CommandColumn70)
        ColumnActionLocation(GridPanel_addressDirectorEntityAccountParty, CommandColumn71)
        ColumnActionLocation(GridPanel_employerAddressDirectorEntityAccountParty, CommandColumn72)
        ColumnActionLocation(GridPanel_EmployerPhoneDirectorEntityAccountParty, CommandColumn73)
        ColumnActionLocation(GridPanelIdentificationDirectorEntityAccountParty, CommandColumn74)

        ColumnActionLocation(GridPanel_phoneDirectorEntityParty, CommandColumn82)
        ColumnActionLocation(GridPanel_addressDirectorEntityParty, CommandColumn83)
        ColumnActionLocation(GridPanel_employerAddressDirectorEntityParty, CommandColumn84)
        ColumnActionLocation(GridPanel_EmployerPhoneDirectorEntityParty, CommandColumn85)
        ColumnActionLocation(GridPanelIdentificationDirectorEntityParty, CommandColumn86)

        ColumnActionLocation(GridPanel_phonePersonFromAccount, CommandColumn17)
        ColumnActionLocation(GridPanel_addressPersonFromAccount, CommandColumn18)
        ColumnActionLocation(GridPanel_employerAddressIDPersonFromAccount, CommandColumn19)
        ColumnActionLocation(GridPanel_EmployerPhoneIDPersonFromAccount, CommandColumn20)
        ColumnActionLocation(GridPanelIdentificationSignatoryFromAccount, CommandColumn33)

        ColumnActionLocation(GridPanel_PhoneDirectorEntityFrom, CommandColumn42)
        ColumnActionLocation(GridPanel_AddressDirectorEntityFrom, CommandColumn43)
        ColumnActionLocation(GridPanel_AddressEmpoyerdirectorentityfrom, CommandColumn44)
        ColumnActionLocation(GridPanelPhoneEmployerDirectorEntityfrom, CommandColumn45)
        ColumnActionLocation(GridPanel_IdentificationDirectorEntityFrom, CommandColumn46)

        ColumnActionLocation(GridPanel_phonePersonToAccount, CommandColumn21)
        ColumnActionLocation(GridPanel_addressPersonToAccount, CommandColumn22)
        ColumnActionLocation(GridPanel_employerAddressIDPersonToAccount, CommandColumn23)
        ColumnActionLocation(GridPanel_EmployerPhoneIDPersonToAccount, CommandColumn24)
        ColumnActionLocation(GridPanelIdentificationSignatoryToAccount, CommandColumn34)

        ColumnActionLocation(GridPanelPhoneDirectorEntityToAccount, CommandColumn54)
        ColumnActionLocation(GridPanelAddressDirectorEntityToAccount, CommandColumn55)
        ColumnActionLocation(GridPanelAddressEmployerDirectorEntityToAccount, CommandColumn56)
        ColumnActionLocation(GridPanelPhoneEmployerDirectorEntityToAccount, CommandColumn57)
        ColumnActionLocation(GridPanelIdentificationDirectorEntityToAccount, CommandColumn58)

        ColumnActionLocation(GridPanel_PhoneDirectorEntityTo, CommandColumn48)
        ColumnActionLocation(GridPanelAddressDirectorEntityTo, CommandColumn49)
        ColumnActionLocation(GridPanelAddressEmpoyerdirectorentityTo, CommandColumn50)
        ColumnActionLocation(GridPanelPhoneEmployerDirectorEntityTo, CommandColumn51)
        ColumnActionLocation(GridPanelIdentificationDirectorEntityTo, CommandColumn52)

        '--=========Activity===================

        ColumnActionLocation(GridPanelSignatoryActivity, CommandColumn88)
        ColumnActionLocation(GridPhoneEntityAccountActivity, CommandColumn89)
        ColumnActionLocation(GridPanelAddressEntityAccountActivity, CommandColumn90)
        ColumnActionLocation(GridPanelDirectorEntityAccountActivity, CommandColumn91)

        ColumnActionLocation(GridPanel_phonePersonActivity, CommandColumn92)
        ColumnActionLocation(GridPanel_addressPersonActivity, CommandColumn93)
        ColumnActionLocation(GridPanel_employerAddressIDPersonActivity, CommandColumn94)
        ColumnActionLocation(GridPanel_EmployerPhoneIDPersonActivity, CommandColumn95)
        ColumnActionLocation(GridPanelIdentificationPersonActivity, CommandColumn96)

        ColumnActionLocation(GridPanelTeleponEntityActivity, CommandColumn97)
        ColumnActionLocation(GridPanel_AddressEntityActivity, CommandColumn98)
        ColumnActionLocation(GridPanelDirectorEntityActivity, CommandColumn99)

        ColumnActionLocation(GridPanel_phoneSignatoryAccountActivity, CommandColumn100)
        ColumnActionLocation(GridPanel_addressSignatoryAccountActivity, CommandColumn101)
        ColumnActionLocation(GridPanel_employerAddressIDSignatoryAccountActivity, CommandColumn102)
        ColumnActionLocation(GridPanel_EmployerPhoneIDSignatoryAccountActivity, CommandColumn103)
        ColumnActionLocation(GridPanelIdentificationSignatoryAccountActivity, CommandColumn104)

        ColumnActionLocation(GridPanel_phoneDirectorEntityAccountActivity, CommandColumn105)
        ColumnActionLocation(GridPanel_addressDirectorEntityAccountActivity, CommandColumn106)
        ColumnActionLocation(GridPanel_employerAddressDirectorEntityAccountActivity, CommandColumn107)
        ColumnActionLocation(GridPanel_EmployerPhoneDirectorEntityAccountActivity, CommandColumn108)
        ColumnActionLocation(GridPanelIdentificationDirectorEntityAccountActivity, CommandColumn109)

        ColumnActionLocation(GridPanel_phoneDirectorEntityActivity, CommandColumn110)
        ColumnActionLocation(GridPanel_addressDirectorEntityActivity, CommandColumn111)
        ColumnActionLocation(GridPanel_employerAddressDirectorEntityActivity, CommandColumn112)
        ColumnActionLocation(GridPanel_EmployerPhoneDirectorEntityActivity, CommandColumn113)
        ColumnActionLocation(GridPanelIdentificationDirectorEntityActivity, CommandColumn114)

        ColumnActionLocation(GridPanelActivity, commandcolumnActivity)

    End Sub
    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 1 Then
            'GridPanelReportIndicator.ColumnModel.Columns.Add(CommandColumn87)
        ElseIf bsettingRight = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            'GridPanelReportIndicator.ColumnModel.Columns.RemoveAt(GridPanelReportIndicator.ColumnModel.Columns.Count - 1)
            'GridPanelReportIndicator.ColumnModel.Columns.Insert(1, CommandColumn87)
        End If
    End Sub
#End Region

#Region "Button"
    Protected Sub BtnCancelTransaction_Click(sender As Object, e As DirectEventArgs)
        transaction_Panel.Hidden = True
        clearSessionTransaction()

        fieldsetConductor.Hidden = True
        Panel_identitasPengirim.Hidden = True
        panel_identitasPenerima.Hidden = True

        fieldset_FromAccount.Hidden = True
        fieldset_entityfromAccount.Hidden = True
        fieldset_personFrom.Hidden = True
        fieldset_EntityFrom.Hidden = True

        fieldSet_InformasiAccountTo.Hidden = True
        fieldset_entityfromAccount.Hidden = True
        fieldset_personTo.Hidden = True
        fieldset_EntityTo.Hidden = True

        panel_Party.Hidden = True
        fieldset_Accountparty.Hidden = True
        fieldset_personParty.Hidden = True
        fieldset_EntityParty.Hidden = True
        fieldset_entityAccountparty.Hidden = True

        ReportGeneralInformationPanel.Hidden = False
        btnCancelReport.Hidden = False
    End Sub
    Protected Sub BtnCancelreportIndicator_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowReportIndicator.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_PhoneDirectorEntityParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityParty.Hidden = False
            WindowPhoneDirectorEntityParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelbtnAddressDirectorEntityParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityParty.Hidden = False
            WindowAddressDirectorEntityParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelbtnAddresEmployerDirectorEntityParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityParty.Hidden = False
            WindowAddressEmployerDirectorEntityParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_PhoneEmployerDirectorEntityParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityParty.Hidden = False
            WindowPhoneEmployerDirectorEntityParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_IdentificationDirectorEntityParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityParty.Hidden = False
            WindowIdentificationDirectorEntityParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_DirectorEntityParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneDirectorEntityAccountParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityAccountParty.Hidden = False
            WindowPhoneDirectorEntityAccountParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelbtnAddressDirectorEntityAccountParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityAccountParty.Hidden = False
            WindowAddressDirectorEntityAccountParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelbtnAddresEmployerDirectorEntityAccountParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityAccountParty.Hidden = False
            WindowAddressEmployerDirectorEntityAccountParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_PhoneEmployerDirectorEntityAccountParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityAccountParty.Hidden = False
            WindowPhoneEmployerDirectorEntityIDAccountParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_IdentificationDirectorEntityAccountParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityAccountParty.Hidden = False
            WindowIdentificationDirectorEntityAccountParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_DirectorEntityAccountParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityAccountParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelbtnAddressPersonParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressPersonParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_PhonepersonParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhonepersonParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelbtnAddresEmployerPersonParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEmployerPersonParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_PhoneEmployerPersonIDParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEmployerPersonIDParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_IdentificationPersonParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowIdentificationPersonParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_PhoneEntityParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEntityParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddressEntityParty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEntityParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneEntityAccountparty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEntityAccountparty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelbtnAddressEntityAccountparty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEntityAccountparty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneSignatoryAccountparty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowSignatoryParty.Hidden = False
            WindowPhoneSignatoryAccountparty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_PhoneEmployerSignatoryIDAccountparty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowSignatoryParty.Hidden = False
            WindowPhoneEmployerSignatoryIDAccountparty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelbtnAddressSignatoryAccountparty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowSignatoryParty.Hidden = False
            WindowAddressSignatoryAccountparty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelbtnAddresEmployerSignatoryAccountparty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowSignatoryParty.Hidden = False
            WindowAddressEmployerSignatoryIDAccountparty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_IdentificationSignatoryAccountparty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowSignatoryParty.Hidden = False
            WindowIdentificationSignatoryAccountparty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_SignatoryAccountparty_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowSignatoryParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneEntityToAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEntityToAccount.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelbtnAddresEmployerPersonFromAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowSignatoryFrom.Hidden = False
            WindowAddressEmployerPersonIDFromAccount.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhonePersonToAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhonePersonToAccount.Hidden = True
            windowSignatoryTo.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub Btn_AddEmployerPhoneIDPersonTo_click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEmployerPersonIDTo.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneEmployerPersonIDTo_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEmployerPersonIDTo.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub Btn_AddEmployerAddressIDPersonTo_click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEmployerPersonTo.Hidden = False

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelbtnAddresEmployerPersonTo_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEmployerPersonTo.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_addPhonePersonTo_click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhonePersonTo.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhonePersonTo_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhonePersonTo.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_addAddressPersonTo_click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressPersonTo.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelbtnAddressPersonTo_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressPersonTo.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_addPhoneEntityTo_click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEntityTo.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneEntityTo_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEntityTo.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_addAddressEntityTo_click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEntityTo.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddressEntityTo_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEntityTo.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub Btn_AddEmployerPhoneIDPersonToAccount_click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEmployerPersonIDToAccount.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelSignatoryTo_Click(sender As Object, e As DirectEventArgs)
        Try
            windowSignatoryTo.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_addPhonePersonToAccount_click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhonePersonToAccount.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub Btn_addAddressPersonToAccount_click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressPersonToAccount.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneEmployerPersonIDToAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEmployerPersonIDToAccount.Hidden = True
            windowSignatoryTo.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_addPhoneEntityAccountTo_click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEntityToAccount.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelbtnAddressPersonToAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressPersonToAccount.Hidden = True
            windowSignatoryTo.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub Btn_AddEmployerAddressIDPersonFromAccount_click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEmployerPersonIDFromAccount.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelbtnAddresEmployerPersonToAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEmployerIDPersonToAccount.Hidden = True
            windowSignatoryTo.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelbtnAddressEntitytoAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEntitytoAccount.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhonePersonFromAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowSignatoryFrom.Hidden = False
            WindowPhonePersonFromAccount.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelbtnAddressPersonFromAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowSignatoryFrom.Hidden = False
            WindowAddressPersonFromAccount.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneEmployerPersonIDFrom_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEmployerPersonIDFrom.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneEmployerConductor_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEmployerConductor.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_PhonePersonFrom_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhonePersonFrom.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneConductor_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneConductor.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelbtnAddressPersonFrom_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressPersonFrom.Hidden = True

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelbtnAddressConductor_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressConductor.Hidden = True

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelbtnAddresEmployerPersonFrom_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEmployerPersonFrom.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelbtnAddresEmployerConductor_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEmployerConductor.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneEntityFromAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEntityFromAccount.Hidden = True

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub BtnCancelAddressEntityFrom_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEntityFrom.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelbtnAddressEntityFromAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEntityFromAccount.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub BtnCancel_PhoneEmployerPersonIDFromAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowSignatoryFrom.Hidden = False
            WindowPhoneEmployerPersonIDFromAccount.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneEntityFrom_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEntityFrom.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub BtnCancel_IdentificationPersonTo_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowIdentificationPersonTo.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_IdentificationSignatoryFromAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowSignatoryFrom.Hidden = False
            WindowIdentificationSignatoryFromAccount.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_IdentificationSignatorytoAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowIdentificationSignatoryToAccount.Hidden = True
            windowSignatoryTo.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_SignatoryFromAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowSignatoryFrom.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_IdentificationPersonFrom_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowIdentificationPersonFrom.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneDirectorEntityFromAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityFromAccount.Hidden = False
            WindowPhoneDirectorEntityFromAccount.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneDirectorEntityToAccount_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityToAccount.Hidden = False
            WindowPhoneDirectorEntityToAccount.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneDirectorEntityFrom_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityFrom.Hidden = False
            WindowPhoneDirectorEntityFrom.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_PhoneDirectorEntityTo_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityTo.Hidden = False
            WindowPhoneDirectorEntityTo.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelReport_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub BtnCancelbtnAddresEmployerDirectorEntityFromAccount_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityFromAccount.Hidden = False
        WindowAddressEmployerDirectorEntityFromAccount.Hidden = True
    End Sub
    Protected Sub BtnCancelbtnAddresEmployerDirectorEntityToAccount_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityToAccount.Hidden = False
        WindowAddressEmployerDirectorEntityToAccount.Hidden = True
    End Sub
    Protected Sub BtnCancelbtnAddresEmployerDirectorEntityFrom_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityFrom.Hidden = False
        WindowAddressEmployerDirectorEntityFrom.Hidden = True
    End Sub
    Protected Sub BtnCancelbtnAddresEmployerDirectorEntityTo_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityTo.Hidden = False
        WindowAddressEmployerDirectorEntityTo.Hidden = True
    End Sub
    Protected Sub BtnCancelbtnAddressDirectorEntityFromAccount_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityFromAccount.Hidden = False
        WindowAddressDirectorEntityFromAccount.Hidden = True
    End Sub
    Protected Sub BtnCancelbtnAddressDirectorEntityToAccount_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityToAccount.Hidden = False
        WindowAddressDirectorEntityToAccount.Hidden = True
    End Sub
    Protected Sub BtnCancelbtnAddressDirectorEntityFrom_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityFrom.Hidden = False
        WindowAddressDirectorEntityFrom.Hidden = True
    End Sub
    Protected Sub BtnCancelbtnAddressDirectorEntityTo_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityTo.Hidden = False
        WindowAddressDirectorEntityTo.Hidden = True
    End Sub
    Protected Sub BtnCancel_DirectorEntityFromAccount_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityFromAccount.Hidden = True
    End Sub
    Protected Sub BtnCancel_DirectorEntityToAccount_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityToAccount.Hidden = True
    End Sub
    Protected Sub BtnCancel_DirectorEntityFrom_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityFrom.Hidden = True
    End Sub
    Protected Sub BtnCancel_DirectorEntityTo_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityTo.Hidden = True
    End Sub
    Protected Sub BtnCancel_IdentificationDirectorEntityFromAccount_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityFromAccount.Hidden = False
        WindowIdentificationDirectorEntityFromAccount.Hidden = True
    End Sub
    Protected Sub BtnCancel_IdentificationConductor_Click(sender As Object, e As DirectEventArgs)
        WindowIdentificationConductor.Hidden = True
    End Sub
    Protected Sub BtnCancel_IdentificationDirectorEntityToAccount_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityToAccount.Hidden = False
        WindowIdentificationDirectorEntityToAccount.Hidden = True
    End Sub
    Protected Sub BtnCancel_IdentificationDirectorEntityFrom_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityFrom.Hidden = False
        WindowIdentificationDirectorEntityFrom.Hidden = True
    End Sub
    Protected Sub BtnCancel_IdentificationDirectorEntityTo_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityTo.Hidden = False
        WindowIdentificationDirectorEntityTo.Hidden = True
    End Sub
    Protected Sub BtnCancel_PhoneEmployerDirectorEntityIDFromAccount_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityFromAccount.Hidden = False
        WindowPhoneEmployerDirectorEntityIDFromAccount.Hidden = True
    End Sub
    Protected Sub BtnCancel_PhoneEmployerDirectorEntityIDToAccount_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityToAccount.Hidden = False
        WindowPhoneEmployerDirectorEntityIDToAccount.Hidden = True
    End Sub
    Protected Sub BtnCancel_PhoneEmployerDirectorEntityIDFrom_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityFrom.Hidden = False
        WindowPhoneEmployerDirectorEntityIDFrom.Hidden = True
    End Sub
    Protected Sub BtnCancel_PhoneEmployerDirectorEntityIDTo_Click(sender As Object, e As DirectEventArgs)
        WindowDirectorEntityTo.Hidden = False
        WindowPhoneEmployerDirectorEntityIDTo.Hidden = True
    End Sub
#End Region

#Region "Activity"
    Public Property IDPhoneSignatoryAccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDPhoneSignatoryAccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDPhoneSignatoryAccountActivity") = value
        End Set
    End Property
    Public Property IDAddressSignatoryAccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDAddressSignatoryAccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDAddressSignatoryAccountActivity") = value
        End Set
    End Property

    Public Property listPhoneSignatoryAccountActivity As List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneSignatoryAccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneSignatoryAccountActivity") = value
        End Set
    End Property
    Public Property CommandNameActivity() As String
        Get
            Return Session("goAML_NotReportedDetail.CommandNameActivity")
        End Get
        Set(ByVal value As String)
            Session("goAML_NotReportedDetail.CommandNameActivity") = value
        End Set
    End Property
    Public Property CommandNameSignatoryActivity() As String
        Get
            Return Session("goAML_NotReportedDetail.CommandNameSignatoryActivity")
        End Get
        Set(ByVal value As String)
            Session("goAML_NotReportedDetail.CommandNameSignatoryActivity") = value
        End Set
    End Property
    Public Property listAddressSignatoryAccountActivity As List(Of NawaDevDAL.goAML_Act_Acc_sign_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressSignatoryAccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Acc_sign_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressSignatoryAccountActivity") = value
        End Set
    End Property
    Public Property IDAddressEmployerSignatoryAccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDAddressEmployerSignatoryAccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDAddressEmployerSignatoryAccountActivity") = value
        End Set
    End Property
    Public Property listAddressEmployerSignatoryAccountActivity As List(Of NawaDevDAL.goAML_Act_Acc_sign_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressEmployerSignatoryAccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Acc_sign_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressEmployerSignatoryAccountActivity") = value
        End Set
    End Property
    Public Property listPhoneEmployerSignatoryAccountActivity As List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEmployerSignatoryAccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEmployerSignatoryAccountActivity") = value
        End Set
    End Property
    Public Property IDPhoneemployersignatoryaccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDPhoneemployersignatoryaccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDPhoneemployersignatoryaccountActivity") = value
        End Set
    End Property
    Public Property IDIdentificationsignatoryAccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDIdentificationsignatoryAccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDIdentificationsignatoryAccountActivity") = value
        End Set
    End Property
    Public Property listIdentificationSignatoryAccountActivity As List(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listIdentificationSignatoryAccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported))
            Session("goAML_NotReportedDetail.listIdentificationSignatoryAccountActivity") = value
        End Set
    End Property
    Public Property IDSignatoryAccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDSignatoryAccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDSignatoryAccountActivity") = value
        End Set
    End Property
    Public Property listSignatoryAccountActivityClass As List(Of NawaDevBLL.SignatoryAccountActivityClass_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listSignatoryAccountActivityClass")
        End Get
        Set(value As List(Of NawaDevBLL.SignatoryAccountActivityClass_NotReported))
            Session("goAML_NotReportedDetail.listSignatoryAccountActivityClass") = value
        End Set
    End Property
    Public Property listSignatoryAccountActivity As List(Of NawaDevDAL.goAML_Act_acc_Signatory_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listSignatoryAccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_acc_Signatory_NotReported))
            Session("goAML_NotReportedDetail.listSignatoryAccountActivity") = value
        End Set
    End Property

    Public Property IDPhoneEntityAccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDPhoneEntityAccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDPhoneEntityAccountActivity") = value
        End Set
    End Property
    Public Property listPhoneEntityAccountActivity As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEntityAccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEntityAccountActivity") = value
        End Set
    End Property
    Public Property IDAddressEntityAccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDAddressEntityAccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDAddressEntityAccountActivity") = value
        End Set
    End Property
    Public Property listAddressentityaccountActivity As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressentityaccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressentityaccountActivity") = value
        End Set
    End Property
    Public Property IDDirectorEntityAccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDDirectorEntityAccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDDirectorEntityAccountActivity") = value
        End Set
    End Property
    Public Property listAddressDirectorEntityAccountActivity As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressDirectorEntityAccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address_NotReported))
            Session("goAML_NotReportedDetail.listAddressDirectorEntityAccountActivity") = value
        End Set
    End Property
    Public Property listPhoneDirectorEntityAccountActivity As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneDirectorEntityAccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneDirectorEntityAccountActivity") = value
        End Set
    End Property
    Public Property listaddressEmployerDirectorEntityAccountActivity As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listaddressEmployerDirectorEntityAccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address_NotReported))
            Session("goAML_NotReportedDetail.listaddressEmployerDirectorEntityAccountActivity") = value
        End Set
    End Property
    Public Property listPhoneemployerdirectorentityAccountActivity As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneemployerdirectorentityAccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneemployerdirectorentityAccountActivity") = value
        End Set
    End Property
    Public Property listIdentificationDirectorEntityaccountActivity As List(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listIdentificationDirectorEntityaccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported))
            Session("goAML_NotReportedDetail.listIdentificationDirectorEntityaccountActivity") = value
        End Set
    End Property

    Public Property CommandNameDirectorEntityAccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.CommandNameDirectorEntityAccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.CommandNameDirectorEntityAccountActivity") = value
        End Set
    End Property
    Public Property IDPhoneDirectorEntityAccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDPhoneDirectorEntityAccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDPhoneDirectorEntityAccountActivity") = value
        End Set
    End Property
    Public Property IDAddressDirectorEntityAccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDAddressDirectorEntityAccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDAddressDirectorEntityAccountActivity") = value
        End Set
    End Property
    Public Property IDaddressEmployerDirectorEntityAccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDaddressEmployerDirectorEntityAccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDaddressEmployerDirectorEntityAccountActivity") = value
        End Set
    End Property
    Public Property IDPhonepersonActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDPhonepersonActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDPhonepersonActivity") = value
        End Set
    End Property
    Public Property IDIdentificationDirectorEntityaccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDIdentificationDirectorEntityaccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDIdentificationDirectorEntityaccountActivity") = value
        End Set
    End Property
    Public Property listPhonepersonActivity As List(Of NawaDevDAL.goAML_Act_Person_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhonepersonActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Person_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhonepersonActivity") = value
        End Set
    End Property
    Public Property IDAddresspersonActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDAddresspersonActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDAddresspersonActivity") = value
        End Set
    End Property
    Public Property listAddressPersonActivity As List(Of NawaDevDAL.goAML_Act_Person_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressPersonActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Person_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressPersonActivity") = value
        End Set
    End Property
    Public Property IDAddressEmployerpersonActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDAddressEmployerpersonActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDAddressEmployerpersonActivity") = value
        End Set
    End Property
    Public Property listAddressEmployerPersonActivity As List(Of NawaDevDAL.goAML_Act_Person_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressEmployerPersonActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Person_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressEmployerPersonActivity") = value
        End Set
    End Property
    Public Property IDphoneEmployerpersonActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDphoneEmployerpersonActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDphoneEmployerpersonActivity") = value
        End Set
    End Property
    Public Property listPhoneemployerpersonActivity As List(Of NawaDevDAL.goAML_Act_Person_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.goAML_Act_Person_Phone")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Person_Phone_NotReported))
            Session("goAML_NotReportedDetail.goAML_Act_Person_Phone") = value
        End Set
    End Property
    Public Property IDIdentificationPersonActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDIdentificationPersonActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDIdentificationPersonActivity") = value
        End Set
    End Property
    Public Property listIdentificationPersonActivity As List(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listIdentificationPersonActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported))
            Session("goAML_NotReportedDetail.listIdentificationPersonActivity") = value
        End Set
    End Property
    Public Property IDPhoneEntityActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDPhoneEntityActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDPhoneEntityActivity") = value
        End Set
    End Property
    Public Property listPhoneEntityActivity As List(Of NawaDevDAL.goAML_Act_Entity_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEntityActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Entity_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEntityActivity") = value
        End Set
    End Property
    Public Property IDAddressEntityActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDAddressEntityActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDAddressEntityActivity") = value
        End Set
    End Property
    Public Property listAddressEntityActivity As List(Of NawaDevDAL.goAML_Act_Entity_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressEntityActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Entity_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressEntityActivity") = value
        End Set
    End Property

    Public Property listDirectorentityaccountActivity As List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listDirectorentityaccountActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director_NotReported))
            Session("goAML_NotReportedDetail.listDirectorentityaccountActivity") = value
        End Set
    End Property
    Public Property listDirectorentityaccountActivityClass As List(Of NawaDevBLL.DirectorEntityAccountActivityClass_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listDirectorentityaccountActivityClass")
        End Get
        Set(value As List(Of NawaDevBLL.DirectorEntityAccountActivityClass_NotReported))
            Session("goAML_NotReportedDetail.listDirectorentityaccountActivityClass") = value
        End Set
    End Property
    Public Property CommandNameDirectorEntityActivity As String
        Get
            Return Session("goAML_NotReportedDetail.CommandNameDirectorEntityActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.CommandNameDirectorEntityActivity") = value
        End Set
    End Property
    Public Property IDDirectorEntityActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDDirectorEntityActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDDirectorEntityActivity") = value
        End Set
    End Property
    Public Property listAddressDirectorEntityActivity As List(Of NawaDevDAL.goAML_Act_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressDirectorEntityActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressDirectorEntityActivity") = value
        End Set
    End Property
    Public Property listPhoneDirectorEntityActivity As List(Of NawaDevDAL.goAML_Act_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneDirectorEntityActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneDirectorEntityActivity") = value
        End Set
    End Property
    Public Property listAddressEmployerDirectorEntityActivity As List(Of NawaDevDAL.goAML_Act_Director_Address_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listAddressEmployerDirectorEntityActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Director_Address_NotReported))
            Session("goAML_NotReportedDetail.listAddressEmployerDirectorEntityActivity") = value
        End Set
    End Property
    Public Property listPhoneEmployerDirectorEntityActivity As List(Of NawaDevDAL.goAML_Act_Director_Phone_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Director_Phone_NotReported))
            Session("goAML_NotReportedDetail.listPhoneEmployerDirectorEntityActivity") = value
        End Set
    End Property
    Public Property listIdentificationDirectorEntityActivity As List(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listIdentificationDirectorEntityActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported))
            Session("goAML_NotReportedDetail.listIdentificationDirectorEntityActivity") = value
        End Set
    End Property

    Public Property IDPhoneDirectorEntityActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDPhoneDirectorEntityActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDPhoneDirectorEntityActivity") = value
        End Set
    End Property
    Public Property IDAddressDirectorEntityActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDAddressDirectorEntityActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDAddressDirectorEntityActivity") = value
        End Set
    End Property
    Public Property IDAddressemployerDirectorEntityActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDAddressemployerDirectorEntityActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDAddressemployerDirectorEntityActivity") = value
        End Set
    End Property
    Public Property IDPhoneEmployerDirectorEntityActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDPhoneEmployerDirectorEntityActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDPhoneEmployerDirectorEntityActivity") = value
        End Set
    End Property
    Public Property IDIdentificationDirectorEntityActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDIdentificationDirectorEntityActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDIdentificationDirectorEntityActivity") = value
        End Set
    End Property
    Public Property listDirectorEntityActivityClass As List(Of NawaDevBLL.DirectorEntityActivityClass_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listDirectorEntityActivityClass")
        End Get
        Set(value As List(Of NawaDevBLL.DirectorEntityActivityClass_NotReported))
            Session("goAML_NotReportedDetail.listDirectorEntityActivityClass") = value
        End Set
    End Property
    Public Property listDirectorEntityActivity As List(Of NawaDevDAL.goAML_Act_Director_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listDirectorEntityActivity")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Act_Director_NotReported))
            Session("goAML_NotReportedDetail.listDirectorEntityActivity") = value
        End Set
    End Property
    Public Property IDPhoneemployerdirectorentityaccountActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDPhoneemployerdirectorentityaccountActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDPhoneemployerdirectorentityaccountActivity") = value
        End Set
    End Property
    Public Property listActivityClass As List(Of NawaDevBLL.ActivityClass_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listActivityClass")
        End Get
        Set(ByVal value As List(Of NawaDevBLL.ActivityClass_NotReported))
            Session("goAML_NotReportedDetail.listActivityClass") = value
        End Set
    End Property
    Public Property listActivity As List(Of NawaDevDAL.goAML_Act_ReportPartyType_NotReported)
        Get
            Return Session("goAML_NotReportedDetail.listActivity")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Act_ReportPartyType_NotReported))
            Session("goAML_NotReportedDetail.listActivity") = value
        End Set
    End Property
    Public Property IDActivity As String
        Get
            Return Session("goAML_NotReportedDetail.IDActivity")
        End Get
        Set(value As String)
            Session("goAML_NotReportedDetail.IDActivity") = value
        End Set
    End Property
    Sub clearsessionActivity()
        ClearSessionsignatoryAccountActivity()
        listPhoneEntityAccountActivity = New List(Of goAML_Act_Acc_Entity_Phone_NotReported)
        listAddressentityaccountActivity = New List(Of goAML_Act_Acc_Entity_Address_NotReported)
        listPhoneDirectorEntityAccountActivity = New List(Of goAML_Act_Acc_Entity_Director_Phone_NotReported)
        listAddressDirectorEntityAccountActivity = New List(Of goAML_Act_Acc_Entity_Director_address_NotReported)
        listaddressEmployerDirectorEntityAccountActivity = New List(Of goAML_Act_Acc_Entity_Director_address_NotReported)
        listIdentificationDirectorEntityaccountActivity = New List(Of goAML_activity_Person_Identification_NotReported)
        listPhonepersonActivity = New List(Of goAML_Act_Person_Phone_NotReported)
        listAddressPersonActivity = New List(Of goAML_Act_Person_Address_NotReported)
        listAddressEmployerPersonActivity = New List(Of goAML_Act_Person_Address_NotReported)
        listPhoneemployerpersonActivity = New List(Of goAML_Act_Person_Phone_NotReported)
        listIdentificationPersonActivity = New List(Of goAML_activity_Person_Identification_NotReported)
        listPhoneEntityActivity = New List(Of goAML_Act_Entity_Phone_NotReported)
        listAddressEntityActivity = New List(Of goAML_Act_Entity_Address_NotReported)
        ClearSessionDIrectorEntityActivity()
        listDirectorEntityActivity = New List(Of goAML_Act_Director_NotReported)
        listDirectorEntityActivityClass = New List(Of NawaDevBLL.DirectorEntityActivityClass_NotReported)
        ClearSessionDIrectorEntityAccountActivity()
        listDirectorentityaccountActivity = New List(Of goAML_Act_Acc_Ent_Director_NotReported)
        listDirectorentityaccountActivityClass = New List(Of NawaDevBLL.DirectorEntityAccountActivityClass_NotReported)
        listSignatoryAccountActivityClass = New List(Of NawaDevBLL.SignatoryAccountActivityClass_NotReported)
        listSignatoryAccountActivity = New List(Of goAML_Act_acc_Signatory_NotReported)
    End Sub
    Sub ClearSessionsignatoryAccountActivity()
        listAddressSignatoryAccountActivity = New List(Of goAML_Act_Acc_sign_Address_NotReported)
        listPhoneSignatoryAccountActivity = New List(Of goAML_Act_Acc_sign_Phone_NotReported)
        listAddressEmployerSignatoryAccountActivity = New List(Of goAML_Act_Acc_sign_Address_NotReported)
        listPhoneEmployerSignatoryAccountActivity = New List(Of goAML_Act_Acc_sign_Phone_NotReported)
        listIdentificationSignatoryAccountActivity = New List(Of goAML_activity_Person_Identification_NotReported)
    End Sub
    Sub ClearSessionDIrectorEntityActivity()
        listAddressDirectorEntityActivity = New List(Of goAML_Act_Director_Address_NotReported)
        listPhoneDirectorEntityActivity = New List(Of goAML_Act_Director_Phone_NotReported)
        listAddressEmployerDirectorEntityActivity = New List(Of goAML_Act_Director_Address_NotReported)
        listPhoneEmployerDirectorEntityActivity = New List(Of goAML_Act_Director_Phone_NotReported)
        listIdentificationDirectorEntityActivity = New List(Of goAML_activity_Person_Identification_NotReported)
    End Sub
    Sub ClearSessionDIrectorEntityAccountActivity()
        listAddressDirectorEntityAccountActivity = New List(Of goAML_Act_Acc_Entity_Director_address_NotReported)
        listPhoneDirectorEntityAccountActivity = New List(Of goAML_Act_Acc_Entity_Director_Phone_NotReported)
        listaddressEmployerDirectorEntityAccountActivity = New List(Of goAML_Act_Acc_Entity_Director_address_NotReported)
        listPhoneemployerdirectorentityAccountActivity = New List(Of goAML_Act_Acc_Entity_Director_Phone_NotReported)
        listIdentificationDirectorEntityaccountActivity = New List(Of goAML_activity_Person_Identification_NotReported)
    End Sub

    'Button Cancel Windows Activity
    Sub BtnCancel_PhoneEmployerDirectorEntityAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEmployerDirectorEntityIDAccountActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_IdentificationDirectorEntityActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowIdentificationDirectorEntityActivity.Hidden = True
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_PhoneEmployerDirectorEntityActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEmployerDirectorEntityActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancelbtnAddresEmployerDirectorEntityActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEmployerDirectorEntityActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_IdentificationPersonActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowIdentificationPersonActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancelbtnAddressDirectorEntityActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressDirectorEntityActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_PhoneDirectorEntityActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneDirectorEntityActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_DirectorEntityActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancelAddressEntityActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEntityActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_PhoneEntityActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEntityActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_PhoneEmployerPersonIDActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEmployerPersonIDActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancelbtnAddresEmployerPersonActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEmployerPersonActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancelbtnAddressPersonActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressPersonActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_IdentificationDirectorEntityAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowIdentificationDirectorEntityAccountActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_PhonepersonActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhonepersonActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancelbtnAddresEmployerDirectorEntityAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEmployerDirectorEntityAccountActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancelbtnAddressDirectorEntityAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressDirectorEntityAccountActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_PhoneDirectorEntityAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneDirectorEntityAccountActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_DirectorEntityAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowDirectorEntityAccountActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancelbtnAddressEntityAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEntityAccountActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_PhoneEntityAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEntityAccountActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_IdentificationSignatoryAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowIdentificationSignatoryAccountActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Sub BtnCancel_PhoneEmployerSignatoryIDAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneEmployerSignatoryIDAccountActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Sub BtnCancelbtnAddresEmployerSignatoryAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressEmployerSignatoryIDAccountActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancelbtnAddressSignatoryAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAddressSignatoryAccountActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_PhoneSignatoryAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowPhoneSignatoryAccountActivity.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_SignatoryAccountActivity_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowSignatoryParty.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Gridcommand Activity
    Sub GridcommandPhoneDirectorEntityActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneDirectorEntityActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneDirectorEntityActivity(id As String)
        Try
            WindowPhoneDirectorEntityActivity.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Act_Director_Phone_NotReported
            phoneTemp = listPhoneDirectorEntityActivity.Where(Function(x) x.PK_goAML_Act_Director_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneDirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneDirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneDirectorEntityActivity.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneDirectorEntityActivity.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneDirectorEntityActivity.Text = phoneTemp.tph_extension
            txtW_commentPhoneDirectorEntityActivity.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandAddressDirectorEntityActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressDirectorEntityActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressDirectorEntityActivity(id As String)
        Try

            WindowAddressDirectorEntityActivity.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Act_Director_Address_NotReported
            addressTemp = listAddressDirectorEntityActivity.Where(Function(x) x.PK_goAML_Act_Dir_Address_NotReported = id).FirstOrDefault

            txtW_AddressDirectorEntityActivity.Text = addressTemp.Address
            txtW_cityDirectorEntityAddressActivity.Text = addressTemp.City
            txtW_CommentDirectorEntityAddressActivity.Text = addressTemp.Comments
            txtW_stateDirectorEntityAddressActivity.Text = addressTemp.State
            txtW_townDirectorEntityAddressActivity.Text = addressTemp.Town
            txtW_zipDirectorEntityAddressActivity.Text = addressTemp.Zip
            cmbW_addressTypeAddressDirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeDirectorEntityAddressActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandAddressEmployerDirectorEntityActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEmployerDirectorEntityActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEmployerDirectorEntityActivity(id As String)
        Try

            WindowAddressEmployerDirectorEntityActivity.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Act_Director_Address_NotReported
            addressTemp = listAddressEmployerDirectorEntityActivity.Where(Function(x) x.PK_goAML_Act_Dir_Address_NotReported = id).FirstOrDefault

            txtW_AddressEmployerDirectorEntityActivity.Text = addressTemp.Address
            txtW_cityEmployerDirectorEntityActivity.Text = addressTemp.City
            txtW_CommentEmployerDirectorEntityActivity.Text = addressTemp.Comments
            txtW_stateEmployerDirectorEntityActivity.Text = addressTemp.State
            txtW_townEmployerDirectorEntityActivity.Text = addressTemp.Town
            txtW_zipEmployerDirectorEntityActivity.Text = addressTemp.Zip
            cmbW_addressTypeAddressEmployerDirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEmployerDirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandPhoneEmployerDirectorEntityActivity(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditphoneEmployerDirectorEntityActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditphoneEmployerDirectorEntityActivity(id As String)
        Try
            WindowPhoneEmployerDirectorEntityActivity.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Act_Director_Phone_NotReported
            phoneTemp = listPhoneEmployerDirectorEntityActivity.Where(Function(x) x.PK_goAML_Act_Director_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEmployerDirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEmployerDirectorEntityIDActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEmployerDirectorEntityIDActivity.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEmployerDirectorEntityIDActivity.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEmployerDirectorEntityIDActivity.Text = phoneTemp.tph_extension
            txtW_commentPhoneEmployerDirectorEntityIDActivity.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandIdentificationDirectorEntityActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editIdentificationDirectorEntityActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editIdentificationDirectorEntityActivity(id As String)
        Try
            WindowIdentificationDirectorEntityActivity.Hidden = False
            Dim identification As NawaDevDAL.goAML_activity_Person_Identification_NotReported
            identification = listIdentificationDirectorEntityActivity.Where(Function(x) x.PK_goAML_Act_Person_Ident_NotReported = id).FirstOrDefault

            Cmb_jenisIdentitasDirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(identification.Type)
            txt_numberIdentificationDirectorEntityActivity.Text = identification.Number
            If Not identification.Issue_Date = DateTime.MinValue Then
                df_IssueDateIdentificationDirectorEntityActivity.Text = identification.Issue_Date.Value.ToString("dd-MMM-yy")
            End If
            If Not identification.Expiry_Date = DateTime.MinValue Then
                df_expiry_dateIdentificationDirectorEntityActivity.Text = identification.Expiry_Date.Value.ToString("dd-MMM-yy")
            End If

            txt_issueByDirectorEntityActivity.Text = identification.Issued_By
            Cmb_countryIdentificationDirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(identification.Issued_Country)
            Txt_commentIdentificationDirectorEntityActivity.Text = identification.Identification_Comment

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandPhoneDirectorEntityAccountActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneDirectorEntityAccountActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneDirectorEntityAccountActivity(id As String)
        Try
            WindowPhoneDirectorEntityAccountActivity.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone_NotReported
            phoneTemp = listPhoneDirectorEntityAccountActivity.Where(Function(x) x.PK_goAML_Act_Acc_Ent_Dir_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneDirectorEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneDirectorEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneDirectorEntityAccountActivity.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneDirectorEntityAccountActivity.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneDirectorEntityAccountActivity.Text = phoneTemp.tph_extension
            txtW_commentPhoneDirectorEntityAccountActivity.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandAddressDirectorEntityAccountActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressDirectorEntityAccountActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressDirectorEntityAccountActivity(id As String)
        Try
            WindowAddressDirectorEntityAccountActivity.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Act_Acc_Entity_Director_address_NotReported
            addressTemp = listAddressDirectorEntityAccountActivity.Where(Function(x) x.PK_goAML_Act_Acc_Ent_Dir_address_NotReported = id).FirstOrDefault

            txtW_AddressDirectorEntityAccountActivity.Text = addressTemp.Address
            txtW_cityDirectorEntityAddressAccountActivity.Text = addressTemp.City
            txtW_CommentDirectorEntityAddressAccountActivity.Text = addressTemp.Comments
            txtW_stateDirectorEntityAddressAccountActivity.Text = addressTemp.State
            txtW_townDirectorEntityAddressAccountActivity.Text = addressTemp.Town
            txtW_zipDirectorEntityAddressAccountActivity.Text = addressTemp.Zip
            cmbW_addressTypeAddressDirectorEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeDirectorEntityAddressAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandAddressEmployerDirectorEntityAccountActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editaddressEmployerDirectorEntityAccountActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editaddressEmployerDirectorEntityAccountActivity(id As String)
        Try

            WindowAddressEmployerDirectorEntityAccountActivity.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Act_Acc_Entity_Director_address_NotReported
            addressTemp = listaddressEmployerDirectorEntityAccountActivity.Where(Function(x) x.PK_goAML_Act_Acc_Ent_Dir_address_NotReported = id).FirstOrDefault

            txtW_AddressEmployerDirectorEntityAccountActivity.Text = addressTemp.Address
            txtW_cityEmployerDirectorEntityAccountActivity.Text = addressTemp.City
            txtW_CommentEmployerDirectorEntityAccountActivity.Text = addressTemp.Comments
            txtW_stateEmployerDirectorEntityAccountActivity.Text = addressTemp.State
            txtW_townEmployerDirectorEntityAccountActivity.Text = addressTemp.Town
            txtW_zipEmployerDirectorEntityAccountActivity.Text = addressTemp.Zip
            cmbW_addressTypeAddressEmployerDirectorEntityIDAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEmployerDirectorEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandPhoneEmployerDirectorEntityAccountActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneemployerdirectorentityAccountActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneemployerdirectorentityAccountActivity(id As String)
        Try
            WindowPhoneEmployerDirectorEntityIDAccountActivity.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone_NotReported
            phoneTemp = listPhoneemployerdirectorentityAccountActivity.Where(Function(x) x.PK_goAML_Act_Acc_Ent_Dir_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEmployerDirectorEntityIDAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEmployerDirectorEntityIDAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEmployerDirectorEntityIDAccountActivity.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEmployerDirectorEntityIDAccountActivity.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEmployerDirectorEntityIDAccountActivity.Text = phoneTemp.tph_extension
            txtW_commentPhoneEmployerDirectorEntityIDAccountActivity.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandIdentificationDirectorEntityAccountActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editIdentificationDirectorEntityaccountActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editIdentificationDirectorEntityaccountActivity(id As String)
        Try

            WindowIdentificationDirectorEntityAccountActivity.Hidden = False
            Dim identification As NawaDevDAL.goAML_activity_Person_Identification_NotReported
            identification = listIdentificationDirectorEntityaccountActivity.Where(Function(x) x.PK_goAML_Act_Person_Ident_NotReported = id).FirstOrDefault

            Cmb_jenisIdentitasDirectorEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(identification.Type)
            txt_numberIdentificationDirectorEntityAccountActivity.Text = identification.Number
            If Not identification.Issue_Date = DateTime.MinValue Then
                df_IssueDateIdentificationDirectorEntityAccountActivity.Text = identification.Issue_Date.Value.ToString("dd-MMM-yy")
            End If
            If Not identification.Expiry_Date = DateTime.MinValue Then
                df_expiry_dateIdentificationDirectorEntityAccountActivity.Text = identification.Expiry_Date.Value.ToString("dd-MMM-yy")
            End If

            txt_issueByDirectorEntityAccountActivity.Text = identification.Issued_By
            Cmb_countryIdentificationDirectorEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(identification.Issued_Country)
            Txt_commentIdentificationDirectorEntityAccountActivity.Text = identification.Identification_Comment

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandPhoneSignatoryAccountActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneSignatoryAccountActivity(ID)
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneSignatoryAccountActivity(id As String)
        Try
            WindowPhoneSignatoryAccountActivity.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Act_Acc_sign_Phone_NotReported
            phoneTemp = listPhoneSignatoryAccountActivity.Where(Function(x) x.PK_goAML_act_acc_sign_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneSignatoryAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneSignatoryAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneSignatoryAccountActivity.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneSignatoryAccountActivity.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneSignatoryAccountActivity.Text = phoneTemp.tph_extension
            txtW_commentPhoneSignatoryAccountActivity.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandAddressSignatoryAccountActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressSignatoryAccountActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressSignatoryAccountActivity(id As String)
        Try

            WindowAddressSignatoryAccountActivity.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Act_Acc_sign_Address_NotReported
            addressTemp = listAddressSignatoryAccountActivity.Where(Function(x) x.PK_goAML_Act_Acc_Ent_Address_NotReported = id).FirstOrDefault

            txtW_AddressSignatoryAccountActivity.Text = addressTemp.Address
            txtW_citySignatoryAddressAccountActivity.Text = addressTemp.City
            txtW_CommentSignatoryAddressAccountActivity.Text = addressTemp.Comments
            txtW_stateSignatoryAddressAccountActivity.Text = addressTemp.State
            txtW_townSignatoryAddressAccountActivity.Text = addressTemp.Town
            txtW_zipSignatoryAddressAccountActivity.Text = addressTemp.Zip
            cmbW_addressTypeAddressSignatoryAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeSignatoryAddressAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandAddressEmployerSignatoryAccountActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEmployerSignatoryAccountActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEmployerSignatoryAccountActivity(id As String)
        Try

            WindowAddressEmployerSignatoryIDAccountActivity.Hidden = False

            Dim addressTemp As NawaDevDAL.goAML_Act_Acc_sign_Address_NotReported
            addressTemp = listAddressEmployerSignatoryAccountActivity.Where(Function(x) x.PK_goAML_Act_Acc_Ent_Address_NotReported = id).FirstOrDefault

            txtW_AddressEmployerSignatoryIDAccountActivity.Text = addressTemp.Address
            txtW_cityEmployerSignatoryIDAccountActivity.Text = addressTemp.City
            txtW_CommentEmployerSignatoryIDAccountActivity.Text = addressTemp.Comments
            txtW_stateEmployerSignatoryIDAccountActivity.Text = addressTemp.State
            txtW_townEmployerSignatoryIDAccountActivity.Text = addressTemp.Town
            txtW_zipEmployerSignatoryIDAccountActivity.Text = addressTemp.Zip
            cmbW_addressTypeAddressEmployerSignatoryIDAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEmployerSignatoryIDAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandPhoneEmployerSignatoryIDAccountActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEmployersignatoryAccountActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneEmployersignatoryAccountActivity(id As String)
        Try
            WindowPhoneEmployerSignatoryIDAccountActivity.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Act_Acc_sign_Phone_NotReported
            phoneTemp = listPhoneEmployerSignatoryAccountActivity.Where(Function(x) x.PK_goAML_act_acc_sign_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEmployerSignatoryIDAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEmployerSignatoryIDAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEmployerSignatoryIDAccountActivity.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEmployerSignatoryIDAccountActivity.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEmployerSignatoryIDAccountActivity.Text = phoneTemp.tph_extension
            txtW_commentPhoneEmployerSignatoryIDAccountActivity.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandIdentificationSignatoryAccountActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editIdentificationsignatoryAccountActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editIdentificationsignatoryAccountActivity(id As String)
        Try

            WindowIdentificationSignatoryAccountActivity.Hidden = False
            Dim identification As NawaDevDAL.goAML_activity_Person_Identification_NotReported
            identification = listIdentificationSignatoryAccountActivity.Where(Function(x) x.PK_goAML_Act_Person_Ident_NotReported = id).FirstOrDefault

            Cmb_jenisIdentitasSignatoryAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(identification.Type)
            txt_numberIdentificationSignatoryAccountActivity.Text = identification.Number
            If identification.Issue_Date IsNot Nothing Then
                df_IssueDateIdentificationSignatoryAccountActivity.Text = identification.Issue_Date.Value.ToString("dd-MMM-yy")
            End If
            If identification.Expiry_Date IsNot Nothing Then
                df_expiry_dateIdentificationSignatoryAccountActivity.Text = identification.Expiry_Date.Value.ToString("dd-MMM-yy")
            End If
            txt_issueBySignatoryAccountActivity.Text = identification.Issued_By
            Cmb_countryIdentificationSignatoryAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(identification.Issued_Country)
            Txt_commentIdentificationSignatoryAccountActivity.Text = identification.Identification_Comment

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            CommandNameActivity = e.ExtraParams(1).Value
            IDActivity = ID
            If e.ExtraParams(1).Value = "Detail" Then
                editActivity(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editActivity(ID As String)
        Try
            clearsessionActivity()
            Activity_Panel.Hidden = False
            ReportGeneralInformationPanel.Hidden = True
            infoValidationResultPanel.Hidden = True
            btnCancelReport.Hidden = True

            Dim objAccountActivity As New NawaDevDAL.goAML_Act_Account_NotReported
            Dim objEntityAccountActivity As New NawaDevDAL.goAML_Act_Entity_Account_NotReported
            Dim objPersonActivity As New NawaDevDAL.goAML_Act_Person_NotReported
            Dim objEntityActivity As New NawaDevDAL.goAML_Act_Entity_NotReported

            'Dim ActivityClass As NawaDevBLL.ActivityClass = listActivityClass.Where(Function(x) x.objActivity.PK_goAML_Act_ReportPartyType_ID = ID).FirstOrDefault
            Dim Activity As NawaDevDAL.goAML_Act_ReportPartyType_NotReported = listActivity.Where(Function(x) x.PK_goAML_Act_ReportPartyType_ID = ID).FirstOrDefault



            With Activity

                txt_significanceActivity.Text = .significance
                txt_commentActivity.Text = .comments
                Cmb_ObjectActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getSenderInformationByPK(.SubNodeType)
                'Account Activity
                If .SubNodeType = 1 Then
                    fieldset_AccountActivity.Hidden = False
                    objAccountActivity = NawaDevBLL.NotReportedBLL.getActAccountByFK(.PK_goAML_Act_ReportPartyType_ID)
                    listSignatoryAccountActivity = NawaDevBLL.NotReportedBLL.getListActAccountSignatoryById(objAccountActivity.PK_goAML_Act_Acc_NotReported)
                    If listSignatoryAccountActivity.Count > 0 Then
                        bindSignatoryAccountActivity(StoreSignatoryActivity, listSignatoryAccountActivity)
                    End If
                    With objAccountActivity
                        txt_institutionNameAccountActivity.Text = .Institution_Name
                        txt_institutionCodeAccountActivity.Text = .Intitution_Code
                        txt_swiftAccountActivity.Text = .Swift_Code
                        If .Non_Banking_Institution = True Then
                            checkbox_Non_banking_institutionAccountActivity.Text = "Ya"
                        ElseIf .Non_Banking_Institution = False Then
                            checkbox_Non_banking_institutionAccountActivity.Text = "Tidak"
                        End If
                        txt_branchAccountActivity.Text = .Branch
                        txt_accountAccountActivity.Text = .Account
                        Cmb_currencyCodeAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCurrencyByCode(.Currency_Code)
                        txt_accountNameAccountActivity.Text = .Account_Name
                        txt_ibanAccountActivity.Text = .iban
                        txt_clientNumberAccountActivity.Text = .Client_Number
                        Cmb_PersonalAccountTypeActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getAccountTyperByKode(.Personal_Account_Type)
                        If Not .Opened = DateTime.MinValue Then
                            Df_openedAccountActivity.Text = .Opened.Value.ToString("dd-MMM-yy")
                        End If
                        If Not .Closed = DateTime.MinValue Then
                            Df_closedAccountActivity.Text = .Closed.Value.ToString("dd-MMM-yy")
                        End If
                        If Not .Date_Balance = DateTime.MinValue Then
                            df_DateBalanceAccountActivity.Text = .Date_Balance.Value.ToString("dd-MMM-yy")
                        End If
                        txt_balanceAccountActivity.Text = .Balance
                        cmb_statusCodeAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getStatusRekeningByKode(.Status_Code)
                        txt_beneficiaryAccountActivity.Text = .Beneficiary
                        txt_beneficiaryCommentAccountActivity.Text = .Beneficiary_Comment
                        txt_commentAccountActivity.Text = .Comments

                    End With
                    If objAccountActivity.IsRekeningKorporasi Then
                        fieldset_entityAccountActivity.Hidden = False
                        RadioGroup2.Text = "Ya"
                        objEntityAccountActivity = NawaDevBLL.NotReportedBLL.getActAccountEntityByFK(.PK_goAML_Act_ReportPartyType_ID)
                        listAddressentityaccountActivity = NawaDevBLL.NotReportedBLL.getListActAccountEntityAddressById(objEntityAccountActivity.PK_goAML_Act_Ent_acc_NotReported)
                        listPhoneEntityAccountActivity = NawaDevBLL.NotReportedBLL.getListActAccountEntityPhoneById(objEntityAccountActivity.PK_goAML_Act_Ent_acc_NotReported)
                        listDirectorentityaccountActivity = NawaDevBLL.NotReportedBLL.getListActAccountEntityDirectorById(objEntityAccountActivity.PK_goAML_Act_Ent_acc_NotReported)
                        If listDirectorentityaccountActivity.Count > 0 Then
                            bindDirectorEntityAccountActivity(StoreDirectorEntityAccountActivity, listDirectorentityaccountActivity)
                        End If
                        If listAddressentityaccountActivity.Count > 0 Then
                            bindAddressEntityAccountActivity(StoreAddressEntityAccountActivity, listAddressentityaccountActivity)
                        End If
                        If listPhoneEntityAccountActivity.Count > 0 Then
                            bindPhoneEntityAccountActivity(StorePhoneEntityAccountActivity, listPhoneEntityAccountActivity)
                        End If
                        With objEntityAccountActivity
                            txt_nameKorporasi_EntityAccountActivity.Text = .Name
                            txt_commercialNameKorporasi_EntityAccountActivity.Text = .Commercial_Name
                            Cmb_bentukKorprasiAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getBentukKorporasiByKode(.Incorporation_Legal_Form)
                            txt_Incorporation_number_EntityAccountActivity.Text = .Incorporation_Number
                            txt_businessEntityAccountActivity.Text = .Business
                            txt_emailEntityAccountActivity.Text = .Email
                            txt_urlEntityAccountActivity.Text = .Url
                            txt_incorporationStateEntityAccountActivity.Text = .Incorporation_State
                            Cmb_IntercorporationCountryCodeEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Incorporation_Country_Code)
                            If Not .Incorporation_Date = DateTime.MinValue Then
                                txt_incorporation_dateEntityAccountActivity.Text = .Incorporation_Date.Value.ToString("dd-MMM-yy")
                            End If
                            If .Business_Closed = True Then
                                df_date_business_closedEntityAccountActivity.Hidden = False
                                checkbox_business_closedEntityAccountActivity.Text = "Ya"

                                If Not .Date_Business_Closed = DateTime.MinValue Then
                                    df_date_business_closedEntityAccountActivity.Text = .Date_Business_Closed.Value.ToString("dd-MMM-yy")
                                End If
                            ElseIf .Business_Closed = False Then
                                checkbox_business_closedEntityAccountActivity.Text = "Tidak"
                            End If
                            txt_taxNumberEntityAccountActivity.Text = .Tax_Number
                            txt_commentEntityAccountActivity.Text = .Comments
                        End With
                    Else
                        RadioGroup2.Text = "Tidak"
                    End If
                ElseIf .SubNodeType = 2 Then
                    fieldset_personActivity.Hidden = False
                    objPersonActivity = NawaDevBLL.NotReportedBLL.getActPersonByFK(.PK_goAML_Act_ReportPartyType_ID)
                    With objPersonActivity
                        Cmb_genderPersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                        txt_titlePersonActivity.Text = .Title
                        txt_lastNamePersonActivity.Text = .Last_Name
                        If .BirthDate IsNot Nothing Then
                            df_birthDatePersonActivity.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                        End If
                        txt_birthPlacePersonActivity.Text = .Birth_Place
                        txt_motherNamePersonActivity.Text = .Mothers_Name
                        txt_aliasPersonActivity.Text = .Alias
                        txt_ssnPersonActivity.Text = .SSN
                        txt_passportNumberPersonActivity.Text = .Passport_Number
                        txt_passportCountryPersonActivity.Text = .Passport_Country
                        txt_idNumberActivityPerson.Text = .Id_Number
                        Cmb_nationality1PersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                        Cmb_nationality2PersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                        Cmb_nationality3PersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                        Cmb_residencePersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
                        txt_emailPersonActivity.Text = .Email
                        txt_emailPersonActivity2.Text = .email2
                        txt_emailPersonActivity3.Text = .email3
                        txt_emailPersonActivity4.Text = .email4
                        txt_emailPersonActivity5.Text = .email5
                        txt_occupationPersonActivity.Text = .Occupation
                        txt_employerNamePersonActivity.Text = .Employer_Name
                        If .Deceased = True Then
                            checkBox_DeceasePersonActivity.Text = "Ya"
                            df_deceased_datePersonActivity.Hidden = False
                            If Not .Deceased_Date = DateTime.MinValue Then
                                df_deceased_datePersonActivity.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                            End If
                        Else
                            checkBox_DeceasePersonActivity.Text = "Tidak"
                        End If
                        txt_taxNumberPersonActivity.Text = .Tax_Number
                        If .Tax_Reg_Number = True Then
                            checkBox_PEPpersonActivity.Text = "Ya"
                        ElseIf .Tax_Reg_Number = False Then
                            checkBox_PEPpersonActivity.Text = "Tidak"
                        End If
                        txt_source_of_wealthPersonActivity.Text = .Source_Of_Wealth
                        txt_CommentPersonActivity.Text = .Comment
                    End With
                    listAddressPersonActivity = NawaDevBLL.NotReportedBLL.getListActPersonAddressById(objPersonActivity.PK_goAML_Act_Person_ID, False)
                    listPhonepersonActivity = NawaDevBLL.NotReportedBLL.getListActPersonPhoneById(objPersonActivity.PK_goAML_Act_Person_ID, False)
                    listAddressEmployerPersonActivity = NawaDevBLL.NotReportedBLL.getListActPersonAddressById(objPersonActivity.PK_goAML_Act_Person_ID, True)
                    listPhoneemployerpersonActivity = NawaDevBLL.NotReportedBLL.getListActPersonPhoneById(objPersonActivity.PK_goAML_Act_Person_ID, True)
                    listIdentificationPersonActivity = NawaDevBLL.NotReportedBLL.getListActIdentificationById(objPersonActivity.PK_goAML_Act_Person_ID, 1)
                    If listAddressPersonActivity.Count > 0 Then
                        bindAddressPersonActivity(StoreAddressPersonActivity, listAddressPersonActivity)
                    End If
                    If listPhonepersonActivity.Count > 0 Then
                        bindPhonePersonActivity(StorePhonePersonActivity, listPhonepersonActivity)
                    End If
                    If listAddressEmployerPersonActivity.Count > 0 Then
                        bindAddressPersonActivity(StoreAddressEmployerPersonActivity, listAddressEmployerPersonActivity)
                    End If
                    If listPhoneemployerpersonActivity.Count > 0 Then
                        bindPhonePersonActivity(StorePhoneEmployerPersonActivity, listPhoneemployerpersonActivity)
                    End If
                    If listIdentificationPersonActivity.Count > 0 Then
                        bindIdentificationActivity(StoreIdentificationPersonActivity, listIdentificationPersonActivity)
                    End If
                ElseIf .SubNodeType = 3 Then
                    fieldset_EntityActivity.Hidden = False
                    objEntityActivity = NawaDevBLL.NotReportedBLL.getActEntityByFK(.PK_goAML_Act_ReportPartyType_ID)
                    With objEntityActivity
                        txt_nameCorporasiEntityActivity.Text = .Name
                        txt_commercialNameEntityActivity.Text = .Commercial_Name
                        Cmb_incorporation_legal_formEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getBentukKorporasiByKode(.Incorporation_Legal_Form)
                        txt_incorporationNumberEntityActivity.Text = .Incorporation_Number
                        txt_businessEntityActivity.Text = .Business
                        txt_emailEntityActivity.Text = .Email
                        txt_urlEntityActivity.Text = .Url
                        txt_incorporationStateEntityActivity.Text = .Incorporation_State
                        Cmb_countryEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Incorporation_Country_Code)
                        If Not .Incorporation_Date = DateTime.MinValue Then
                            txt_incorporationDateEntityActivity.Text = .Incorporation_Date.Value.ToString("dd-MMM-yy")
                        End If
                        If .Business_Closed = True Then
                            checkBox_businessClosedEntityActivity.Text = "Ya"
                            df_dateBusinessClosedEntityActivity.Hidden = False
                            If Not .Date_Business_Closed = DateTime.MinValue Then
                                df_dateBusinessClosedEntityActivity.Text = .Date_Business_Closed.Value.ToString("dd-MMM-yy")
                            End If
                        ElseIf .Business_Closed = False Then
                            checkBox_businessClosedEntityActivity.Text = "Tidak"
                        End If
                        txt_taxNumberEntityActivity.Text = .Tax_Number
                        txt_commentEntityActivity.Text = .Comments
                    End With
                    listAddressEntityActivity = NawaDevBLL.NotReportedBLL.getListActEntityAddressById(objEntityActivity.PK_goAML_Act_Ent_NotReported)
                    listPhoneEntityActivity = NawaDevBLL.NotReportedBLL.getListActEntityPhoneById(objEntityActivity.PK_goAML_Act_Ent_NotReported)
                    listDirectorEntityActivity = NawaDevBLL.NotReportedBLL.getListActEntityDirectorById(objEntityActivity.PK_goAML_Act_Ent_NotReported)
                    If listDirectorEntityActivity.Count > 0 Then
                        bindDirectorEntityActivity(StoreDirectorEntityActivity, listDirectorEntityActivity)
                    End If
                    If listAddressEntityActivity.Count > 0 Then
                        bindAddressEntityActivity(StoreAddressEntityActivity, listAddressEntityActivity)
                    End If
                    If listPhoneEntityActivity.Count > 0 Then
                        bindPhoneEntityActivity(StorePhoneEntityActivity, listPhoneEntityActivity)
                    End If
                End If

            End With

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandSignatoryActivity(sender As Object, e As DirectEventArgs)

        Try
            CommandNameSignatoryActivity = e.ExtraParams(1).Value
            Dim ID As String = e.ExtraParams(0).Value
            IDSignatoryAccountActivity = ID
            If e.ExtraParams(1).Value = "Detail" Then
                editSignatoryActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editSignatoryActivity(ID As String)
        ClearSessionsignatoryAccountActivity()
        WindowSignatoryActivity.Hidden = False

        Dim signatory As NawaDevDAL.goAML_Act_acc_Signatory_NotReported = listSignatoryAccountActivity.Where(Function(x) x.PK_goAML_Act_acc_Signatory_NotReported = ID).FirstOrDefault
        With signatory

            Cmb_genderSignatoryAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
            txt_titleSignatoryAccountActivity.Text = .Title
            txt_lastNameSignatoryAccountActivity.Text = .Last_Name
            If Not .BirthDate = DateTime.MinValue Then
                df_birthDateSignatoryAccountActivity.Text = .BirthDate.Value.ToString("dd-MMM-yy")
            End If

            txt_birthPlaceSignatoryAccountActivity.Text = .Birth_Place
            txt_motherNameSignatoryAccountActivity.Text = .Mothers_Name
            txt_aliasSignatoryAccountActivity.Text = .Alias
            txt_ssnSignatoryAccountActivity.Text = .SSN
            txt_passportNumberSignatoryAccountActivity.Text = .Passport_Number
            txt_passportCountrySignatoryAccountActivity.Text = .Passport_Country
            txt_idNumberFromSignatoryAccountActivity.Text = .Id_Number
            Cmb_nationality1SignatoryAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
            Cmb_nationality2SignatoryAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
            Cmb_nationality3SignatoryAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
            Cmb_residenceSignatoryAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
            txt_emailSignatoryAccountActivity.Text = .Email
            txt_emailSignatoryAccountActivity2.Text = .email2
            txt_emailSignatoryAccountActivity3.Text = .email3
            txt_emailSignatoryAccountActivity4.Text = .email4
            txt_emailSignatoryAccountActivity5.Text = .email5
            txt_occupationSignatoryAccountActivity.Text = .Occupation
            txt_employerNameSignatoryAccountActivity.Text = .Employer_Name

            txt_taxNumberSignatoryAccountActivity.Text = .Tax_Number

            txt_source_of_wealthSignatoryAccountActivity.Text = .Source_Of_Wealth
            txt_CommentSignatoryAccountActivity.Text = .Comment

            Cmb_roleAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getRoleTypebyCode(.role)
        End With
        If signatory.Deceased = True Then
            checkbox_DeceaseSignatoryAccountActivity.Text = "Ya"
            df_deceased_dateSignatoryAccountActivity.Hidden = False
            If Not signatory.Deceased_Date = DateTime.MinValue Then
                df_deceased_dateSignatoryAccountActivity.Text = signatory.Deceased_Date.Value.ToString("dd-MMM-yy")
            End If
        ElseIf signatory.Deceased = False Then
            checkbox_DeceaseSignatoryAccountActivity.Text = "Tidak"
        End If
        If signatory.Tax_Reg_Number = True Then
            checkBox_PEPSignatoryAccountActivity.Text = "Ya"
        ElseIf signatory.Tax_Reg_Number = False Then
            checkBox_PEPSignatoryAccountActivity.Text = "Tidak"
        End If
        If signatory.isPrimary = True Then
            isPrimaryAccountActivity.Text = "Ya"

        ElseIf signatory.isPrimary = False Then
            isPrimaryAccountActivity.Text = "Tidak"
        End If
        listAddressSignatoryAccountActivity = NawaDevBLL.NotReportedBLL.getListActAccountSignatoryAddressById(signatory.PK_goAML_Act_acc_Signatory_NotReported, False)
        listPhoneSignatoryAccountActivity = NawaDevBLL.NotReportedBLL.getListActAccountSignatoryPhoneById(signatory.PK_goAML_Act_acc_Signatory_NotReported, False)
        listAddressEmployerSignatoryAccountActivity = NawaDevBLL.NotReportedBLL.getListActAccountSignatoryAddressById(signatory.PK_goAML_Act_acc_Signatory_NotReported, True)
        listPhoneEmployerSignatoryAccountActivity = NawaDevBLL.NotReportedBLL.getListActAccountSignatoryPhoneById(signatory.PK_goAML_Act_acc_Signatory_NotReported, True)
        listIdentificationSignatoryAccountActivity = NawaDevBLL.NotReportedBLL.getListActIdentificationById(signatory.PK_goAML_Act_acc_Signatory_NotReported, 2)


        If listAddressSignatoryAccountActivity.Count > 0 Then
            bindAddressSignatoryAccountActivity(StoreAddressSignatoryAccountActivity, listAddressSignatoryAccountActivity)
        End If
        If listPhoneSignatoryAccountActivity.Count > 0 Then
            bindPhoneSignatoryAccountActivity(StorePhoneSignatoryAccountActivity, listPhoneSignatoryAccountActivity)
        End If
        If listAddressEmployerSignatoryAccountActivity.Count > 0 Then
            bindAddressSignatoryAccountActivity(StoreAddressEmployerSignatoryIDAccountActivity, listAddressEmployerSignatoryAccountActivity)
        End If
        If listPhoneEmployerSignatoryAccountActivity.Count > 0 Then
            bindPhoneSignatoryAccountActivity(StorePhoneEmployerSignatoryIDAccountActivity, listPhoneEmployerSignatoryAccountActivity)
        End If
        If listIdentificationSignatoryAccountActivity.Count > 0 Then
            bindIdentificationActivity(StoreIdentificationSignatoryAccountActivity, listIdentificationSignatoryAccountActivity)
        End If


    End Sub

    Sub GridcommandPhoneEntityAccountActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEntityAccountActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneEntityAccountActivity(id As String)
        Try
            WindowPhoneEntityAccountActivity.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Act_Acc_Entity_Phone_NotReported
            phoneTemp = listPhoneEntityAccountActivity.Where(Function(x) x.PK_goAML_Act_Acc_Entity_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEntityAccountActivity.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEntityAccountActivity.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEntityAccountActivity.Text = phoneTemp.tph_extension
            txtW_commentPhoneEntityAccountActivity.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandAddressEntityAccountActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEntityAccountActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEntityAccountActivity(id As String)
        Try

            WindowAddressEntityAccountActivity.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Act_Acc_Entity_Address_NotReported
            addressTemp = listAddressentityaccountActivity.Where(Function(x) x.PK_Act_Acc_Entity_Address_NotReported = id).FirstOrDefault

            txtW_AddressEntityAddressAccountActivity.Text = addressTemp.Address
            txtW_cityEntityAddressAccountActivity.Text = addressTemp.City
            txtW_CommentEntityAddressAccountActivity.Text = addressTemp.Comments
            txtW_stateEntityAddressAccountActivity.Text = addressTemp.State
            txtW_townEntityAddressAccountActivity.Text = addressTemp.Town
            txtW_zipEntityAddressAccountActivity.Text = addressTemp.Zip
            cmbW_addressTypeAddressEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEntityAddressAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandDirectorEntityAccountActivity(sender As Object, e As DirectEventArgs)
        Try
            CommandNameDirectorEntityAccountActivity = e.ExtraParams(1).Value
            Dim ID As String = e.ExtraParams(0).Value
            IDDirectorEntityAccountActivity = ID
            If e.ExtraParams(1).Value = "Detail" Then
                editDirectorentityaccountActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editDirectorentityaccountActivity(id As String)
        Try
            ClearSessionDIrectorEntityAccountActivity()
            WindowDirectorEntityAccountActivity.Hidden = False

            Dim directorclass As NawaDevBLL.DirectorEntityAccountActivityClass_NotReported = listDirectorentityaccountActivityClass.Where(Function(x) x.objDirectorEntityAccountActivity.PK_Act_Acc_Ent_Director_NotReported = id).FirstOrDefault
            Dim director As NawaDevDAL.goAML_Act_Acc_Ent_Director_NotReported = listDirectorentityaccountActivity.Where(Function(x) x.PK_Act_Acc_Ent_Director_NotReported = id).FirstOrDefault

            With director

                Cmb_genderDirectorEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                txt_titleDirectorEntityAccountActivity.Text = .Title
                txt_lastNameDirectorEntityAccountActivity.Text = .Last_Name
                If Not .BirthDate = DateTime.MinValue Then
                    df_birthDateDirectorEntityAccountActivity.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                End If
                txt_birthPlaceDirectorEntityAccountActivity.Text = .Birth_Place
                txt_motherNameDirectorEntityAccountActivity.Text = .Mothers_Name
                txt_aliasDirectorEntityAccountActivity.Text = .Alias
                txt_ssnDirectorEntityAccountActivity.Text = .SSN
                txt_passportNumberDirectorEntityAccountActivity.Text = .Passport_Number
                txt_passportCountryDirectorEntityAccountActivity.Text = .Passport_Country
                txt_idNumberActivityDirectorentityAccountActivity.Text = .Id_Number
                Cmb_nationality1DirectorEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                Cmb_nationality2DirectorEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                Cmb_nationality3DirectorEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                Cmb_residenceDirectorEntityAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
                txt_emailDirectorEntityAccountActivity.Text = .Email
                txt_emailDirectorEntityAccountActivity2.Text = .email2
                txt_emailDirectorEntityAccountActivity3.Text = .email3
                txt_emailDirectorEntityAccountActivity4.Text = .email4
                txt_emailDirectorEntityAccountActivity5.Text = .email5
                txt_occupationDirectorEntityAccountActivity.Text = .Occupation
                txt_employerNameDirectorEntityAccountActivity.Text = .Employer_Name

                If .Deceased Then
                    checkbox_DeceaseDirectorEntityAccountActivity.Text = "Ya"
                    df_deceased_dateDirectorEntityAccountActivity.Hidden = False
                    If Not .Deceased_Date = DateTime.MinValue Then
                        df_deceased_dateDirectorEntityAccountActivity.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                    End If
                Else
                    checkbox_DeceaseDirectorEntityAccountActivity.Text = "Tidak"
                End If

                txt_taxNumberDirectorEntityAccountActivity.Text = .Tax_Number
                If .Tax_Reg_Number Then
                    checkBox_PEPDirectorEntityAccountActivity.Text = "Ya"
                Else
                    checkBox_PEPDirectorEntityAccountActivity.Text = "Tidak"
                End If

                txt_source_of_wealthDirectorEntityAccountActivity.Text = .Source_Of_Wealth
                txt_CommentDirectorEntityAccountActivity.Text = .Comment
                Cmb_roleTypeDirectorAccountActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.role)
            End With
            listAddressDirectorEntityAccountActivity = NawaDevBLL.NotReportedBLL.getListActAccountEntityDirectorAddressById(director.PK_Act_Acc_Ent_Director_NotReported, False)
            listPhoneDirectorEntityAccountActivity = NawaDevBLL.NotReportedBLL.getListActAccountEntityDirectorPhoneById(director.PK_Act_Acc_Ent_Director_NotReported, False)
            listaddressEmployerDirectorEntityAccountActivity = NawaDevBLL.NotReportedBLL.getListActAccountEntityDirectorAddressById(director.PK_Act_Acc_Ent_Director_NotReported, True)
            listPhoneemployerdirectorentityAccountActivity = NawaDevBLL.NotReportedBLL.getListActAccountEntityDirectorPhoneById(director.PK_Act_Acc_Ent_Director_NotReported, True)
            listIdentificationDirectorEntityaccountActivity = NawaDevBLL.NotReportedBLL.getListActIdentificationById(director.PK_Act_Acc_Ent_Director_NotReported, 5)


            If listAddressDirectorEntityAccountActivity.Count > 0 Then
                bindAddressDirectorEntityAccountActivity(StoreAddressDirectorEntityAccountActivity, listAddressDirectorEntityAccountActivity)
            End If
            If listPhoneDirectorEntityAccountActivity.Count > 0 Then
                bindPhoneDirectorEntityAccountActivity(StorePhoneDirectorEntityAccountActivity, listPhoneDirectorEntityAccountActivity)
            End If
            If listaddressEmployerDirectorEntityAccountActivity.Count > 0 Then
                bindAddressDirectorEntityAccountActivity(StoreAddressEmployerDirectorEntityAccountActivity, listaddressEmployerDirectorEntityAccountActivity)
            End If
            If listPhoneemployerdirectorentityAccountActivity.Count > 0 Then
                bindPhoneDirectorEntityAccountActivity(StorePhoneEmployerDirectorEntityAccountActivity, listPhoneemployerdirectorentityAccountActivity)
            End If
            If listIdentificationDirectorEntityaccountActivity.Count > 0 Then
                bindIdentificationActivity(StoreIdentificationDirectorEntityAccountActivity, listIdentificationDirectorEntityaccountActivity)
            End If


        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandPhonePersonActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhonepersonActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhonepersonActivity(id As String)
        Try
            WindowPhonepersonActivity.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Act_Person_Phone_NotReported
            phoneTemp = listPhonepersonActivity.Where(Function(x) x.PK_goAML_Act_Person_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhonepersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhonepersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhonepersonActivity.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhonepersonActivity.Text = phoneTemp.tph_number
            txtW_tphExtentionPhonepersonActivity.Text = phoneTemp.tph_extension
            txtW_commentPhonepersonActivity.Text = phoneTemp.comments


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandAddresspersonActivity(sender As Object, e As DirectEventArgs)
        Try
            If (e.ExtraParams(1).Value = "Edit" Or e.ExtraParams(1).Value = "Delete") And CommandNameActivity = "Detail" Then
                Throw New ApplicationException("Tidak bisa melakukan action ini")
            End If
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressPersonActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressPersonActivity(id As String)
        Try

            WindowAddressPersonActivity.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Act_Person_Address_NotReported
            addressTemp = listAddressPersonActivity.Where(Function(x) x.PK_goAML_Act_Person_Address_NotReported = id).FirstOrDefault

            txtW_AddressPersonActivity.Text = addressTemp.Address
            txtW_cityPersonActivity.Text = addressTemp.City
            txtW_CommentPersonActivity.Text = addressTemp.Comments
            txtW_statePersonActivity.Text = addressTemp.State
            txtW_townPersonActivity.Text = addressTemp.Town
            txtW_zipPersonActivity.Text = addressTemp.Zip
            cmbW_addressTypeAddressPersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodePersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandAddressEmployerPersonIDActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEmployerPersonActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEmployerPersonActivity(ID As String)
        Try

            WindowAddressEmployerPersonActivity.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Act_Person_Address_NotReported
            addressTemp = listAddressEmployerPersonActivity.Where(Function(x) x.PK_goAML_Act_Person_Address_NotReported = ID).FirstOrDefault

            txtW_AddressEmployerPersonIDActivity.Text = addressTemp.Address
            txtW_cityEmployerPersonIDActivity.Text = addressTemp.City
            txtW_CommentEmployerPersonIDActivity.Text = addressTemp.Comments
            txtW_stateEmployerPersonIDActivity.Text = addressTemp.State
            txtW_townEmployerPersonIDActivity.Text = addressTemp.Town
            txtW_zipEmployerPersonIDActivity.Text = addressTemp.Zip
            cmbW_addressTypeAddressEmployerIDPersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEmployerPersonIDActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandPhoneEmployerPersonIDActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneemployerpersonActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneemployerpersonActivity(id As String)
        Try
            WindowPhoneEmployerPersonIDActivity.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Act_Person_Phone_NotReported
            phoneTemp = listPhoneemployerpersonActivity.Where(Function(x) x.PK_goAML_Act_Person_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEmployerPersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEmployerPersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEmployerPersonActivity.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEmployerPersonActivity.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEmployerPersonActivity.Text = phoneTemp.tph_extension
            txtW_commentPhoneEmployerPersonActivity.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandIdentificationPersonActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editIdentificationPersonActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editIdentificationPersonActivity(ID As String)
        Try

            WindowIdentificationPersonActivity.Hidden = False
            Dim identification As NawaDevDAL.goAML_activity_Person_Identification_NotReported
            identification = listIdentificationPersonActivity.Where(Function(x) x.PK_goAML_Act_Person_Ident_NotReported = ID).FirstOrDefault

            Cmb_jenisIdentitasPersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(identification.Type)
            txt_numberIdentificationPersonActivity.Text = identification.Number
            If Not identification.Issue_Date = DateTime.MinValue Then
                df_IssueDateIdentificationPersonActivity.Text = identification.Issue_Date.Value.ToString("dd-MMM-yy")
            End If
            If Not identification.Expiry_Date = DateTime.MinValue Then
                df_expiry_dateIdentificationpersonActivity.Text = identification.Expiry_Date.Value.ToString("dd-MMM-yy")
            End If

            txt_issueByPersonActivity.Text = identification.Issued_By
            Cmb_countryIdentificationPersonActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(identification.Issued_Country)
            Txt_commentIdentificationPersonActivity.Text = identification.Identification_Comment

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandPhoneEntityActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                EditPhoneEntityActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub EditPhoneEntityActivity(id As String)
        Try
            WindowPhoneEntityActivity.Hidden = False
            Dim phoneTemp As NawaDevDAL.goAML_Act_Entity_Phone_NotReported
            phoneTemp = listPhoneEntityActivity.Where(Function(x) x.PK_goAML_Act_Ent_Phone_NotReported = id).FirstOrDefault

            CmbW_tph_contact_typePhoneEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(phoneTemp.Tph_Contact_Type)
            CmbW_tph_CommunicationTypePhoneEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(phoneTemp.Tph_Communication_Type)
            txtW_tphcountryPrefixPhoneEntityActivity.Text = phoneTemp.tph_country_prefix
            txtW_tphNumberPhoneEntityActivity.Text = phoneTemp.tph_number
            txtW_tphExtentionPhoneEntityActivity.Text = phoneTemp.tph_extension
            txtW_commentPhoneEntityActivity.Text = phoneTemp.comments

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandWindowAddressEntityActivity(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                editAddressEntityActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editAddressEntityActivity(id As String)
        Try
            WindowAddressEntityActivity.Hidden = False
            Dim addressTemp As NawaDevDAL.goAML_Act_Entity_Address_NotReported
            addressTemp = listAddressEntityActivity.Where(Function(x) x.PK_goAML_Act_Entity_Address_NotReported = id).FirstOrDefault

            txtW_AddressEntityAddressActivity.Text = addressTemp.Address
            txtW_cityEntityAddressActivity.Text = addressTemp.City
            txtW_CommentEntityAddressActivity.Text = addressTemp.Comments
            txtW_stateEntityAddressActivity.Text = addressTemp.State
            txtW_townEntityAddressActivity.Text = addressTemp.Town
            txtW_zipEntityAddressActivity.Text = addressTemp.Zip
            cmbW_addressTypeAddressEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(addressTemp.Address_Type)
            CmbW_countryCodeEntityAddressActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(addressTemp.Country_Code)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridcommandDirectorEntityActivity(sender As Object, e As DirectEventArgs)
        Try
            CommandNameDirectorEntityActivity = e.ExtraParams(1).Value
            Dim ID As String = e.ExtraParams(0).Value
            IDDirectorEntityActivity = ID
            If e.ExtraParams(1).Value = "Detail" Then
                editDirectorEntityActivity(ID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editDirectorEntityActivity(id As String)
        Try
            ClearSessionDIrectorEntityActivity()
            WindowDirectorEntityActivity.Hidden = False

            Dim directorclass As NawaDevBLL.DirectorEntityActivityClass_NotReported = listDirectorEntityActivityClass.Where(Function(x) x.objDirectorEntityActivity.PK_goAML_Act_Director_NotReported = id).FirstOrDefault
            Dim director As NawaDevDAL.goAML_Act_Director_NotReported = listDirectorEntityActivity.Where(Function(x) x.PK_goAML_Act_Director_NotReported = id).FirstOrDefault

            With director

                Cmb_genderDirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                txt_titleDirectorEntityActivity.Text = .Title
                txt_lastNameDirectorEntityActivity.Text = .Last_Name
                If Not .BirthDate = DateTime.MinValue Then
                    df_birthDateDirectorEntityActivity.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                End If
                txt_birthPlaceDirectorEntityActivity.Text = .Birth_Place
                txt_motherNameDirectorEntityActivity.Text = .Mothers_Name
                txt_aliasDirectorEntityActivity.Text = .Alias
                txt_ssnDirectorEntityActivity.Text = .SSN
                txt_passportNumberDirectorEntityActivity.Text = .Passport_Number
                txt_passportCountryDirectorEntityActivity.Text = .Passport_Country
                txt_idNumberFromDirectorEntityAccount.Text = .Id_Number
                Cmb_nationality1DirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                Cmb_nationality2DirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                Cmb_nationality3DirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                Cmb_residenceDirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
                txt_emailDirectorEntityActivity.Text = .Email
                txt_emailDirectorEntityActivity2.Text = .email2
                txt_emailDirectorEntityActivity3.Text = .email3
                txt_emailDirectorEntityActivity4.Text = .email4
                txt_emailDirectorEntityActivity5.Text = .email5
                txt_occupationDirectorEntityActivity.Text = .Occupation
                txt_employerNameDirectorEntityActivity.Text = .Employer_Name
                If .Deceased = True Then
                    checkbox_DeceaseDirectorEntityActivity.Text = "Ya"
                    If Not .Deceased_Date = DateTime.MinValue Then
                        df_deceased_dateDirectorEntityActivity.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                    End If
                ElseIf .Deceased = False Then
                    checkbox_DeceaseDirectorEntityActivity.Text = "Tidak"
                End If

                txt_taxNumberDirectorEntityActivity.Text = .Tax_Number
                If .Tax_Reg_Number = True Then
                    checkBox_PEPDirectorEntityActivity.Text = "Ya"
                ElseIf .Tax_Reg_Number = False Then
                    checkBox_PEPDirectorEntityActivity.Text = "Tidak"
                End If

                txt_source_of_wealthDirectorEntityActivity.Text = .Source_Of_Wealth
                txt_CommentDirectorEntityActivity.Text = .Comment
                cmb_roleDirectorEntityActivity.Text = NawaDevBLL.GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.role)
            End With
            listAddressDirectorEntityActivity = NawaDevBLL.NotReportedBLL.getListActEntityDirectorAddressById(director.PK_goAML_Act_Director_NotReported, False)
            listPhoneDirectorEntityActivity = NawaDevBLL.NotReportedBLL.getListActEntityDirectorPhoneById(director.PK_goAML_Act_Director_NotReported, False)
            listAddressEmployerDirectorEntityActivity = NawaDevBLL.NotReportedBLL.getListActEntityDirectorAddressById(director.PK_goAML_Act_Director_NotReported, True)
            listPhoneEmployerDirectorEntityActivity = NawaDevBLL.NotReportedBLL.getListActEntityDirectorPhoneById(director.PK_goAML_Act_Director_NotReported, True)
            listIdentificationDirectorEntityActivity = NawaDevBLL.NotReportedBLL.getListActIdentificationById(director.PK_goAML_Act_Director_NotReported, 6)


            If listAddressDirectorEntityActivity.Count > 0 Then
                bindAddressDirectorEntityActivity(StoreAddressDirectorEntityActivity, listAddressDirectorEntityActivity)
            End If
            If listPhoneDirectorEntityActivity.Count > 0 Then
                bindPhoneDirectorEntityActivity(StorePhoneDirectorEntityActivity, listPhoneDirectorEntityActivity)
            End If
            If listAddressEmployerDirectorEntityActivity.Count > 0 Then
                bindAddressDirectorEntityActivity(StoreAddressEmployerDirectorEntityActivity, listAddressEmployerDirectorEntityActivity)
            End If
            If listPhoneEmployerDirectorEntityActivity.Count > 0 Then
                bindPhoneDirectorEntityActivity(StorePhoneEmployerDirectorEntityActivity, listPhoneEmployerDirectorEntityActivity)
            End If
            If listIdentificationDirectorEntityActivity.Count > 0 Then
                bindIdentificationActivity(StoreIdentificationDirectorEntityActivity, listIdentificationDirectorEntityActivity)
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancelActivity_Click(sender As Object, e As DirectEventArgs)
        Activity_Panel.Hidden = True
        ReportGeneralInformationPanel.Hidden = False
        btnCancelReport.Hidden = False
        fieldset_EntityActivity.Hidden = True
        fieldset_personActivity.Hidden = True
        fieldset_AccountActivity.Hidden = True
        fieldset_entityAccountActivity.Hidden = True
    End Sub

    Shared Function getObjectByKode(kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Sender_To_Information = objdb.goAML_Ref_Sender_To_Information.Where(Function(x) x.PK_Sender_To_Information = kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Sender_To_Information
            End If
            Return StringKeterangan
        End Using
    End Function


    'Bind Activity
    Sub bindPhoneSignatoryAccountActivity(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddressSignatoryAccountActivity(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Act_Acc_sign_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindIdentificationActivity(store As Ext.Net.Store, listIdentification As List(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
        objtable.Columns.Add(New DataColumn("tipe", GetType(String)))
        objtable.Columns.Add(New DataColumn("no", GetType(String)))
        objtable.Columns.Add(New DataColumn("tanggalTerbit", GetType(Date)))
        objtable.Columns.Add(New DataColumn("tanggalKadaluarsa", GetType(Date)))
        objtable.Columns.Add(New DataColumn("negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("tipe") = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(item("Type").ToString)
                item("no") = item("Number")
                item("tanggalTerbit") = item("Issue_Date")
                item("tanggalKadaluarsa") = item("Expiry_Date")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Issued_Country").ToString)

                item("negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindSignatoryAccountActivity(store As Ext.Net.Store, listSignatory As List(Of NawaDevDAL.goAML_Act_acc_Signatory_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listSignatory)
        objtable.Columns.Add(New DataColumn("Primary", GetType(String)))
        objtable.Columns.Add(New DataColumn("Nama", GetType(String)))
        objtable.Columns.Add(New DataColumn("Peran", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                If (item("isPrimary") IsNot Nothing AndAlso Not IsDBNull(item("isPrimary"))) Then
                    item("Primary") = IIf(item("isPrimary") = True, "Ya", "Tidak")
                End If
                item("Nama") = item("Last_Name")
                item("Peran") = NawaDevBLL.GlobalReportFunctionBLL.getRoleTypebyCode(item("role").ToString)

            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhoneEntityAccountActivity(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddressEntityAccountActivity(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddressDirectorEntityAccountActivity(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhoneDirectorEntityAccountActivity(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhonePersonActivity(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Act_Person_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddressPersonActivity(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Act_Person_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub bindPhoneEntityActivity(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Act_Entity_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddressEntityActivity(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Act_Entity_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindDirectorEntityAccountActivity(store As Ext.Net.Store, listDirector As List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director_NotReported))
        'Dim listItemDirector As New List(Of NawaDevDAL.goAML_Trn_Director)
        'For Each a In listDirector
        '    listItemDirector.Add(a.objDirector)
        'Next
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listDirector)
        objtable.Columns.Add(New DataColumn("Nama", GetType(String)))
        objtable.Columns.Add(New DataColumn("Peran", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Nama") = item("Last_Name")
                item("Peran") = NawaDevBLL.GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(item("role").ToString)

            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAddressDirectorEntityActivity(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Act_Director_Address_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("TypeAlamat") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("Negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindPhoneDirectorEntityActivity(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Act_Director_Phone_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("contact_type", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("contact_type") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("tph_contact_type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindDirectorEntityActivity(store As Ext.Net.Store, listDirector As List(Of NawaDevDAL.goAML_Act_Director_NotReported))
        'Dim listItemDirector As New List(Of NawaDevDAL.goAML_Trn_Director)
        'For Each a In listDirector
        '    listItemDirector.Add(a.objDirector)
        'Next
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listDirector)
        objtable.Columns.Add(New DataColumn("Nama", GetType(String)))
        objtable.Columns.Add(New DataColumn("Peran", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Nama") = item("Last_Name")
                item("Peran") = NawaDevBLL.GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(item("role").ToString)

            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindActivity(store As Ext.Net.Store, listActivity As List(Of NawaDevDAL.goAML_Act_ReportPartyType_NotReported))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listActivity)
        objtable.Columns.Add(New DataColumn("object", GetType(String)))
        objtable.Columns.Add(New DataColumn("signifikan", GetType(String)))
        objtable.Columns.Add(New DataColumn("alasan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("object") = getObjectByKode(item("SubNodeType").ToString)
                item("signifikan") = item("significance")
                item("alasan") = item("reason")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Function getTransaksiORActivity(kode As String) As String
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim objects As NawaDevDAL.goAML_odm_ref_Report_TRNorACT = objDb.goAML_odm_ref_Report_TRNorACT.Where(Function(x) x.ReportType = kode).FirstOrDefault
            Return objects.TRNorACT
        End Using
    End Function
#End Region

End Class
