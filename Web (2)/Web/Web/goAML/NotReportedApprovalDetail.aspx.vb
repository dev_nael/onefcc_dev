﻿Imports Ext
Imports System.Data
Imports NawaBLL
Imports NawaDevDAL
Imports NawaDevBLL
Imports NawaDevBLL.GenerateSarBLL
Imports Microsoft.VisualBasic.CompilerServices
Imports NawaBLL.Nawa
Imports NawaDAL
Imports System.Data.Entity
Imports System.Data.SqlClient
Partial Class goAML_NotReportedApprovalDetail
    Inherits Parent
    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("goAML_NotReportedApprovalDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_NotReportedApprovalDetail.ObjModule") = value
        End Set
    End Property
    Private _IDReq As Long
    Public Property IDReq() As Long
        Get
            Return _IDReq
        End Get
        Set(ByVal value As Long)
            _IDReq = value
        End Set
    End Property

    Private _objApproval As NawaDAL.ModuleApproval
    Private _objProposalReportNew As NawaDevDAL.ProposalReport_NotReported
    Private _objProposalReportOld As NawaDevDAL.ProposalReport_NotReported
    Public Property ObjApproval() As NawaDAL.ModuleApproval
        Get
            Return _objApproval
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            _objApproval = value
        End Set
    End Property
    Public Property objProposalReportNew() As NawaDevDAL.ProposalReport_NotReported
        Get
            Return Session("goAML_NotReportedApprovalDetail.objProposalReportNew")
        End Get
        Set(value As NawaDevDAL.ProposalReport_NotReported)
            Session("goAML_NotReportedApprovalDetail.objProposalReportNew") = value
        End Set
    End Property
    Public Property objProposalReportOld() As NawaDevDAL.ProposalReport_NotReported
        Get
            Return Session("goAML_NotReportedApprovalDetail.objProposalReportOld")
        End Get
        Set(value As NawaDevDAL.ProposalReport_NotReported)
            Session("goAML_NotReportedApprovalDetail.objProposalReportOld") = value
        End Set
    End Property

    Private Sub goAML_NotReportedApprovalDetailt_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not NawaBLL.Common.SessionCurrentUser Is Nothing Then

                Dim strid As String = Request.Params("ID")
                Dim strModuleid As String = Request.Params("ModuleID")
                Try

                    IDReq = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Catch ex As Exception
                    Throw New Exception("Invalid ID Approval.")
                End Try

                ObjApproval = NawaBLL.ModuleApprovalBLL.GetModuleApprovalByID(IDReq)

                If Not Ext.Net.X.IsAjaxRequest Then
                    clearSession()

                    If Not ObjApproval Is Nothing Then


                        ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleName(ObjApproval.ModuleName)
                        Dim strmodulelabel As String = ""
                        If Not ObjModule Is Nothing Then
                            strmodulelabel = ObjModule.ModuleLabel
                        End If
                        PanelInfo.Title = strmodulelabel & " Approval Detail"

                        lblModuleName.Text = strmodulelabel
                        lblModuleKey.Text = ObjApproval.ModuleKey
                        lblAction.Text = NawaBLL.ModuleBLL.GetModuleActionNamebyID(ObjApproval.PK_ModuleAction_ID)
                        LblCreatedBy.Text = ObjApproval.CreatedBy
                        lblCreatedDate.Text = ObjApproval.CreatedDate.Value.ToString("dd-MMM-yyyy")

                        BtnSave.Visible = ObjApproval.CreatedBy <> NawaBLL.Common.SessionCurrentUser.UserID
                        BtnReject.Visible = ObjApproval.CreatedBy <> NawaBLL.Common.SessionCurrentUser.UserID

                    End If

                    Dim Arrstr() As String
                    Dim num As Integer = 1

                    Select Case ObjApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            objProposalReportNew = NawaBLL.Common.Deserialize(ObjApproval.ModuleField, GetType(NawaDevDAL.ProposalReport_NotReported))

                            With objProposalReportNew
                                ReportID_New.Text = .Report_ID
                                Reason_New.Text = .Reason
                            End With
                        Case NawaBLL.Common.ModuleActionEnum.Update
                        Case NawaBLL.Common.ModuleActionEnum.Delete
                    End Select
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub clearSession()
        objProposalReportNew = New NawaDevDAL.ProposalReport_NotReported
        objProposalReportOld = New NawaDevDAL.ProposalReport_NotReported
    End Sub

    Protected Sub BtnReject_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            NawaDevBLL.NotReportedBLL.Reject(ObjApproval.PK_ModuleApproval_ID)

            LblConfirmation.Text = "Data Rejected. Click Ok to Back To " & ObjModule.ModuleLabel & " Approval."

            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnSave_Click(sneder As Object, e As DirectEventArgs)
        Try
            NawaDevBLL.NotReportedBLL.Accept(ObjApproval.PK_ModuleApproval_ID)

            LblConfirmation.Text = "Data Approved. Click Ok to Back To " & ObjModule.ModuleLabel & " Approval."

            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs) Handles BtnConfirmation.DirectClick
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

End Class
