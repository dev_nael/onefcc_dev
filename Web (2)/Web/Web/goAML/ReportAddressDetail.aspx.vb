﻿Imports Ext
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL

Partial Class goAML_ReportAddressDetail
    Inherits Parent
    Dim objgoAMLReportAddress As New goAML_Ref_Address
    Public Property StrUnikKey() As String
        Get
            Return Session("goAML_ReportAddressDetail.StrUnikKey")
        End Get
        Set(ByVal value As String)
            Session("goAML_ReportAddressDetail.StrUnikKey") = value
        End Set
    End Property

    Public Property objmodule() As NawaDAL.Module
        Get
            Return Session("goAML_ReportAddressDetail.objmodule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_ReportAddressDetail.objmodule") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                Dim Moduleid As String = Request.Params("ModuleID")
                Dim intModuleid As Integer = Common.DecryptQueryString(Moduleid, SystemParameterBLL.GetEncriptionKey)
                objmodule = ModuleBLL.GetModuleByModuleID(intModuleid)

                Dim IDData As String = Request.Params("ID")
                StrUnikKey = Common.DecryptQueryString(IDData, SystemParameterBLL.GetEncriptionKey)

                If Not objmodule Is Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If

                FormPanelWICDetail.Title = objmodule.ModuleLabel & " Detail"
                Dim objReportAddress As New ReportAddress
                objReportAddress.LoadPanelDetail(FormPanelWICDetail, objmodule.ModuleName, StrUnikKey)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Net.X.Redirect(Common.GetApplicationPath & objmodule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
End Class
