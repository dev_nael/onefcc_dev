﻿Imports System.IO
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Entity

Partial Class GeneratePPATKElection
    Inherits Parent

    Public Property Filetodownload() As String
        Get
            Return Session("GeneratePPATKElection.Filetodownload")
        End Get
        Set(ByVal value As String)
            Session("GeneratePPATKElection.Filetodownload") = value
        End Set
    End Property
    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("GeneratePPATKElection.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("GeneratePPATKElection.ObjModule") = value
        End Set
    End Property

    Public Property ObjGeneratedgoAML() As goAML_Generate_XML
        Get
            Return Session("GeneratePPATKElection.ObjGeneratedgoAML")
        End Get
        Set(ByVal value As goAML_Generate_XML)
            Session("GeneratePPATKElection.ObjGeneratedgoAML") = value
        End Set
    End Property

    '' Add 31-Dec-2021
    Public Property IDListOfGenerated() As Long
        Get
            Return Session("GeneratePPATKElection.IDListOfGenerated")
        End Get
        Set(ByVal value As Long)
            Session("GeneratePPATKElection.IDListOfGenerated") = value
        End Set
    End Property
    '' End 31-Dec-2021

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()

                Dim Moduleid As String = Request.Params("ModuleID")

                Dim intModuleid As Integer
                Dim intIFTIID As Integer
                Try
                    intModuleid = Common.DecryptQueryString(Moduleid, SystemParameterBLL.GetEncriptionKey)
                    intIFTIID = Common.DecryptQueryString(Request.Params("ID"), SystemParameterBLL.GetEncriptionKey)
                    IDListOfGenerated = Common.DecryptQueryString(Request.Params("ID"), SystemParameterBLL.GetEncriptionKey) '' Add 31-Dec-2021
                    ObjModule = ModuleBLL.GetModuleByModuleID(intModuleid)

                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    FormPanelInput.Title = ObjModule.ModuleLabel & " Generate"

                    LoadData()

                    TaskManager1.StartTask("refreshScreeningStatus") '' add 07-Jan-2022
                Catch ex As Exception
                    ErrorSignal.FromCurrentContext.Raise(ex)
                    Net.X.Msg.Alert("Error", ex.Message).Show()
                End Try
            End If


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadData()
        ObjGeneratedgoAML = GeneratePPATKBLL.GetGeneratedgoAMLByID(Common.DecryptQueryString(Request.Params("ID"), SystemParameterBLL.GetEncriptionKey))

        If ObjGeneratedgoAML IsNot Nothing Then
            With ObjGeneratedgoAML
                lbllastupdatedate.Text = Convert.ToDateTime(.Last_Update_Date).ToString("dd-MMM-yyyy")
                lblStatusUpload.Text = .Status
                lblTotalInvalid.Text = .Total_Invalid
                lblTotalValid.Text = .Total_Valid
                lblTransDate.Text = Convert.ToDateTime(.Transaction_Date).ToString("dd-MMM-yyyy")
                lblTransType.Text = .Jenis_Laporan
                If Convert.ToString(.IsAlreadyValidateSchema).ToLower = "true" Then
                    lblAlreadyValidateSchema.Text = "Yes"
                Else

                    lblAlreadyValidateSchema.Text = "No"
                End If

                If Convert.ToString(.ValidateSchemaResultValid).ToLower = "true" Then
                    lblValidateSchemaResultValid.Text = "Valid"
                ElseIf Convert.ToString(.ValidateSchemaResultValid).ToLower = "" Then
                    lblValidateSchemaResultValid.Text = "Not Yet Validate"
                Else
                    lblValidateSchemaResultValid.Text = "Not Valid"
                End If

            End With
        End If

        '' Add 31-Dec-2021 

        Dim objGenerateParalelStatus(0) As SqlParameter
        objGenerateParalelStatus(0) = New SqlParameter
        objGenerateParalelStatus(0).ParameterName = "@PK_ListOfGenerate_ID"
        objGenerateParalelStatus(0).Value = IDListOfGenerated

        Dim dtGenerateParalelStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_GET_GenerateParalelStatus", objGenerateParalelStatus)

        If dtGenerateParalelStatus IsNot Nothing Then
            For Each row As DataRow In dtGenerateParalelStatus.Rows
                lblXMLValidatedCount.Text = row.Item("CounterValidate")

                lblXMLGeneratedCount.Text = row.Item("Counter")
                lblStatusAPI.Text = row.Item("StatusGenerate")

                lblErrorMessageAPI.Text = row.Item("ErrorMessage")

                If row.Item("FileZip") IsNot Nothing And Not (IsDBNull(row.Item("FileZip"))) And Not (IsDBNull(row.Item("FileZipName"))) Then
                    btnDownloadXML.Hidden = False
                    btnSaveUpload.Disabled = True
                Else
                    btnDownloadXML.Hidden = True
                    btnSaveUpload.Disabled = False
                End If
            Next
        End If

        '' End 31-Dec-2021

    End Sub

    Private Sub ClearSession()
        ObjModule = Nothing
        ObjGeneratedgoAML = Nothing

        '5-Aug-2021 Adi : kosongkan agar tidak ke-download saat invalid
        Filetodownload = ""

        '' Add 31-Dec-2021 Felix
        lblXMLValidatedCount.Text = ""
        lblXMLGeneratedCount.Text = ""
        lblStatusAPI.Text = ""
        lblErrorMessageAPI.Text = ""
    End Sub
    Protected Sub StoreValidation_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            StoreView.PageSize = SystemParameterBLL.GetPageSize
            Dim intStart As Integer = e.Start
            Dim intTotalRowCount As Long = 0

            Dim intLimit As Int16 = e.Limit
            Dim strfilter As String = Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)
            Dim strsort As String = ""

            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next


            'Dim strQuerySchema As String = "select * from (SELECT gavsxr.UnikReference,gavsxr.ErrorMessage FROM goAML_ValidateSchemaXSD_Report AS gavsxr WHERE gavsxr.TransactionDate='" & Convert.ToDateTime(ObjGeneratedgoAML.Transaction_Date).ToString("yyyy-MM-dd") & "' AND gavsxr.Jenis_Laporan='" & ObjGeneratedgoAML.Jenis_Laporan & "' AND gavsxr.LastUpdateDate='" & Convert.ToDateTime(ObjGeneratedgoAML.Last_Update_Date).ToString("yyyy-MM-dd") & "')xx "
            '' Pakai DateDiff
            Dim strQuerySchema As String = "select * from (SELECT gavsxr.UnikReference,gavsxr.ErrorMessage FROM goAML_ValidateSchemaXSD_Report AS gavsxr WHERE gavsxr.TransactionDate='" & Convert.ToDateTime(ObjGeneratedgoAML.Transaction_Date).ToString("yyyy-MM-dd") & "' AND gavsxr.Jenis_Laporan='" & ObjGeneratedgoAML.Jenis_Laporan & "' " &
                 " And datediff(day,gavsxr.LastUpdateDate ,'" & Convert.ToDateTime(ObjGeneratedgoAML.Last_Update_Date).ToString("yyyy-MM-dd") & "') = 0)xx "
            If strfilter.Length > 0 Then
                strQuerySchema &= " where " & strfilter
            End If
            Dim objListParam(3) As SqlParameter
            objListParam(0) = New SqlParameter
            objListParam(0).ParameterName = "@querydata"
            objListParam(0).SqlDbType = SqlDbType.VarChar
            objListParam(0).Value = strQuerySchema
            objListParam(1) = New SqlParameter
            objListParam(1).ParameterName = "@orderby"
            objListParam(1).SqlDbType = SqlDbType.VarChar
            objListParam(1).Value = strsort
            objListParam(2) = New SqlParameter
            objListParam(2).ParameterName = "@PageNum"
            objListParam(2).SqlDbType = SqlDbType.Int
            objListParam(2).Value = intStart
            objListParam(3) = New SqlParameter
            objListParam(3).ParameterName = "@PageSize"
            objListParam(3).SqlDbType = SqlDbType.Int
            objListParam(3).Value = intLimit
            'query hanya ambil 1 record untuk simpen schemanya saja(pagesize=1) biar enteng
            Dim DataPaging As DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuerySchema, strsort, intStart, intLimit, intTotalRowCount)
            e.Total = intTotalRowCount

            GridPanelValidation.GetStore.DataSource = DataPaging
            GridPanelValidation.GetStore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnValidate_DirectClick(sender As Object, e As EventArgs)
        Try
            If Not ObjGeneratedgoAML Is Nothing Then
                If ObjGeneratedgoAML.Total_Valid = 0 Then
                    Throw New Exception("Total Valid is 0.Please correct the invalid data first before validate schema.")
                End If

                '' Edit 31-Dec-2021
                'NawaDevBLL.GeneratePPATKBLL.ValidateSchema(ObjGeneratedgoAML.Transaction_Date, ObjGeneratedgoAML.Jenis_Laporan, ObjGeneratedgoAML.Last_Update_Date)

                Dim objGenerateParalelStatus(0) As SqlParameter
                objGenerateParalelStatus(0) = New SqlParameter
                objGenerateParalelStatus(0).ParameterName = "@PK_ListOfGenerate_ID"
                objGenerateParalelStatus(0).Value = IDListOfGenerated

                Dim dtGenerateParalelStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_GET_GenerateParalelStatus", objGenerateParalelStatus)

                If dtGenerateParalelStatus IsNot Nothing And dtGenerateParalelStatus.Rows.Count > 0 Then
                    For Each row As DataRow In dtGenerateParalelStatus.Rows
                        lblXMLValidatedCount.Text = row.Item("CounterValidate")

                        lblXMLGeneratedCount.Text = row.Item("Counter")
                        lblStatusAPI.Text = row.Item("StatusGenerate")

                        lblErrorMessageAPI.Text = row.Item("ErrorMessage")

                        If row.Item("KodeStatusGenerate") <> "1" And row.Item("KodeStatusGenerate") <> "4" Then
                            Dim objParam(1) As SqlParameter
                            objParam(0) = New SqlParameter
                            objParam(0).ParameterName = "@PK_ListOfGenerate_ID"
                            objParam(0).Value = IDListOfGenerated
                            objParam(0).DbType = SqlDbType.BigInt

                            objParam(1) = New SqlParameter
                            objParam(1).ParameterName = "@User_ID"
                            objParam(1).Value = Common.SessionCurrentUser.UserID
                            objParam(1).DbType = SqlDbType.VarChar

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_VALIDATE_XML_SaveEOD", objParam)

                            lblStatusAPI.Text = "Process Validate XML"
                        ElseIf row.Item("KodeStatusGenerate") = "4" Then
                            Throw New Exception("Process Validate XML by API is in progress. Please check again later.")
                        ElseIf row.Item("KodeStatusGenerate") = "1" Then
                            Throw New Exception("Process Generate XML by API is in progress. Please check again later.")
                        End If
                    Next
                Else
                    Dim objParam(1) As SqlParameter
                    objParam(0) = New SqlParameter
                    objParam(0).ParameterName = "@PK_ListOfGenerate_ID"
                    objParam(0).Value = IDListOfGenerated
                    objParam(0).DbType = SqlDbType.BigInt

                    objParam(1) = New SqlParameter
                    objParam(1).ParameterName = "@User_ID"
                    objParam(1).Value = Common.SessionCurrentUser.UserID
                    objParam(1).DbType = SqlDbType.VarChar

                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_VALIDATE_XML_SaveEOD", objParam)

                    lblStatusAPI.Text = "Process Validate XML"
                End If
                '' End 31-Dec-2021

                ObjGeneratedgoAML = Nothing
                LoadData()
                StoreView.Reload()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub BtnSave_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)


        '' Added by Felix 27 Aug 2020
        Dim isHarusValid As String = getParameterGlobalByPK(12) '' Download harus Valid XSD
        '' End of Felix

        Try
            If lblTotalValid.Text = "0" Then
                'Throw New Exception("There is not data valid")
                Throw New Exception("Can't generate XML if there is still invalid data.")
            End If

            If Convert.ToString(ObjGeneratedgoAML.IsAlreadyValidateSchema).ToLower = "false" Or Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "" Then
                Throw New Exception("Please Validate Schema first before Generate XML")
            End If

            ''If Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "false" Or Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "" Then '' Edited By Felix 27 Aug 2020
            If (Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "false" Or Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "") And isHarusValid = "1" Then
                Throw New Exception("There is Validate Schema result Fail. Please fix the fail result first before generate xml")
            End If

            '5-Aug-2021 Adi : Menambahkan validasi jika masih ada report yang akan di generate statusnya 4 (Waiting for Approval) atau 5 (Need Correction)
            Using objdb As New NawaDatadevEntities
                Dim objCekApproval = objdb.goAML_Report.Where(Function(x) x.Report_Code = ObjGeneratedgoAML.Jenis_Laporan And DbFunctions.TruncateTime(x.Transaction_Date) = DbFunctions.TruncateTime(ObjGeneratedgoAML.Transaction_Date) And
                                                                DbFunctions.TruncateTime(x.LastUpdateDate) = DbFunctions.TruncateTime(ObjGeneratedgoAML.Last_Update_Date) And (x.Status = 4 Or x.Status = 5)).ToList
                If objCekApproval IsNot Nothing AndAlso objCekApproval.Count > 0 Then
                    Throw New Exception("Can't generate XML. There is still " & objCekApproval.Count & " invalid reports or are in Approval.")
                End If
            End Using
            'End of 5-Aug-2021 Adi : Menambahkan validasi jika masih ada report yang akan di generate statusnya 4 (Waiting for Approval) atau 5 (Need Correction)


            Dim path As String = Server.MapPath("~\goaml\FolderExport\")
            '  Dim path As String = SystemParameterBLL.GetSystemParameterByPk(9017).SettingValue

            Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
            If Not Directory.Exists(Dirpath) Then
                Directory.CreateDirectory(Dirpath)
            End If

            '' Edit 31-Dec-2021
            'NawaDevBLL.GeneratePPATKBLL.ValidateSchema(ObjGeneratedgoAML.Transaction_Date, ObjGeneratedgoAML.Jenis_Laporan, ObjGeneratedgoAML.Last_Update_Date)

            Dim objGenerateParalelStatus(0) As SqlParameter
            objGenerateParalelStatus(0) = New SqlParameter
            objGenerateParalelStatus(0).ParameterName = "@PK_ListOfGenerate_ID"
            objGenerateParalelStatus(0).Value = IDListOfGenerated

            Dim dtGenerateParalelStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_GET_GenerateParalelStatus", objGenerateParalelStatus)

            If dtGenerateParalelStatus IsNot Nothing Then
                For Each row As DataRow In dtGenerateParalelStatus.Rows
                    lblXMLValidatedCount.Text = row.Item("CounterValidate")

                    lblXMLGeneratedCount.Text = row.Item("Counter")
                    lblStatusAPI.Text = row.Item("StatusGenerate")

                    lblErrorMessageAPI.Text = row.Item("ErrorMessage")

                    If row.Item("FileZip") IsNot Nothing And Not (IsDBNull(row.Item("FileZip"))) And Not (IsDBNull(row.Item("FileZipName"))) Then

                        'Dim listZip As New List(Of String)
                        'Dim dtZip As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text,
                        '                                                        " select FileZipName,FileZip " &
                        '                                                        " FROM goAML_GenerateXMLParalel " &
                        '                                                        " where FK_goAML_Generate_XML_NOID = " & IDListOfGenerated, Nothing)
                        'listZip.Clear() '' Added on 02 Feb 2021, untuk hapus attachemnt report sebelumnya, Thanks to Pak Bom

                        'If dtZip.Rows.Count > 0 Then
                        '    For Each itemattachment As Data.DataRow In dtZip.Rows
                        '        Dim fileattachmentname As String = Dirpath & itemattachment(0)
                        '        IO.File.WriteAllBytes(fileattachmentname, itemattachment(1))
                        '        listZip.Add(fileattachmentname)

                        '    Next
                        'End If
                        'Filetodownload = listZip.Item(0).ToString

                        'If Filetodownload Is Nothing Then
                        '    Throw New Exception("Not Exists Report")

                        'End If

                        'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update goAML_GenerateXMLParalel " &
                        '    " set StatusGenerate = 7 " &
                        '    " ,LastUpdateDate = getdate() " &
                        '" where FK_goAML_Generate_XML_NOID =  '" & IDListOfGenerated & "'", Nothing)

                        'lblStatusAPI.Text = "XML Generated"

                        'Throw New Exception("API has sucessfully Generate XML. If you want to regenerate, please do Validate XML first.")
                    ElseIf row.Item("KodeStatusGenerate") = "6" Then
                        Dim objParam(1) As SqlParameter
                        objParam(0) = New SqlParameter
                        objParam(0).ParameterName = "@PK_ListOfGenerate_ID"
                        objParam(0).Value = IDListOfGenerated
                        objParam(0).DbType = SqlDbType.BigInt

                        objParam(1) = New SqlParameter
                        objParam(1).ParameterName = "@User_ID"
                        objParam(1).Value = Common.SessionCurrentUser.UserID
                        objParam(1).DbType = SqlDbType.VarChar

                        Dim ReportType As String = Request.Params("ReportType")
                        If ReportType.ToLower = "pemilu_d" Then
                            'SP baru akan di develop
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_ELECTION_GENERATE_XML_Excel_PEMILU_D_SaveEOD", objParam)
                        Else
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_GENERATE_XML_SaveEOD", objParam)
                        End If

                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update goAML_GenerateXMLParalel " &
                            " set StatusGenerate = 1 " &
                            " ,LastUpdateDate = getdate() " &
                        " where FK_goAML_Generate_XML_NOID =  '" & IDListOfGenerated & "'", Nothing)
                        lblStatusAPI.Text = "Process Generate XML"
                    ElseIf row.Item("KodeStatusGenerate") = "1" Then
                        Throw New Exception("Process Generate XML by API is in progress. Please check again later.")
                    ElseIf row.Item("KodeStatusGenerate") = "4" Then
                        Throw New Exception("Process Validate XML by API is in progress. Please check again later.")
                    End If

                Next

            End If


            '' remarked 31-Dec-2021 , ini cara lama yg berhasil
            'If Not ObjGeneratedgoAML Is Nothing Then
            '    'FileReturn = GeneratePPATKBLL.GenerateReportXML(ObjGeneratedgoAML.Transaction_Date, ObjGeneratedgoAML.Jenis_Laporan, Common.SessionCurrentUser.UserID, Dirpath, connection, filenamexml, filenamezip, formatdate, ObjGeneratedgoAML.Status)

            '    Filetodownload = GeneratePPATKBLL.GenerateReportXMLNew(ObjGeneratedgoAML.Transaction_Date, ObjGeneratedgoAML.Jenis_Laporan, ObjGeneratedgoAML.Last_Update_Date, Dirpath)
            '    If Filetodownload Is Nothing Then
            '        Throw New Exception("Not Exists Report")

            '    End If

            'End If
            '' end remark

            '' End 31-Dec-2021


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Sub Download()
        If Filetodownload <> "" Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(Filetodownload))
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "application/zip"
            Response.BinaryWrite(IO.File.ReadAllBytes(Filetodownload))
            Response.End()

            ''06-Jan-2022 Felix , hapus setelah download
            If IO.File.Exists(Filetodownload) Then
                IO.File.Delete(Filetodownload)
            End If

            '5-Aug-2021 Adi : kosongkan agar tidak ke-download saat invalid
            Filetodownload = ""
        End If
    End Sub
    Protected Sub btnCancelUpload_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            '2020 09 15 Added By Apuy
            If ObjModule.UrlView.Contains("ReportType") Then
                Ext.Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "ModuleID=" & Moduleid)
            Else
                Ext.Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

    End Sub
    '' added By Felix 27 Aug 2020
    Function getParameterGlobalByPK(id As Integer) As String
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim paramValues As String = ""
            Dim paramGlobal As NawaDevDAL.goAML_Ref_ReportGlobalParameter = objdb.goAML_Ref_ReportGlobalParameter.Where(Function(x) x.PK_GlobalReportParameter_ID = id).FirstOrDefault

            If Not paramGlobal Is Nothing Then
                paramValues = paramGlobal.ParameterValue
            End If
            Return paramValues
        End Using
    End Function
    '' added By Felix 27 Aug 2020
#Region "10-Jan-2022 Felix, tambah Button Download XML dr DB"
    Protected Sub btnDownloadXML_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)

        '' Added by Felix 27 Aug 2020
        Dim isHarusValid As String = getParameterGlobalByPK(12) '' Download harus Valid XSD
        '' End of Felix

        Try
            'If lblTotalValid.Text = "0" Then
            '    'Throw New Exception("There is not data valid")
            '    Throw New Exception("Can't generate XML if there is still invalid data.")
            'End If

            'If Convert.ToString(ObjGeneratedgoAML.IsAlreadyValidateSchema).ToLower = "false" Or Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "" Then
            '    Throw New Exception("Please Validate Schema first before Generate XML")
            'End If

            '''If Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "false" Or Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "" Then '' Edited By Felix 27 Aug 2020
            'If (Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "false" Or Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "") And isHarusValid = "1" Then
            '    Throw New Exception("There is Validate Schema result Fail. Please fix the fail result first before generate xml")
            'End If

            ''5-Aug-2021 Adi : Menambahkan validasi jika masih ada report yang akan di generate statusnya 4 (Waiting for Approval) atau 5 (Need Correction)
            'Using objdb As New NawaDatadevEntities
            '    Dim objCekApproval = objdb.goAML_Report.Where(Function(x) x.Report_Code = ObjGeneratedgoAML.Jenis_Laporan And DbFunctions.TruncateTime(x.Transaction_Date) = DbFunctions.TruncateTime(ObjGeneratedgoAML.Transaction_Date) And
            '                                                    DbFunctions.TruncateTime(x.LastUpdateDate) = DbFunctions.TruncateTime(ObjGeneratedgoAML.Last_Update_Date) And (x.Status = 4 Or x.Status = 5)).ToList
            '    If objCekApproval IsNot Nothing AndAlso objCekApproval.Count > 0 Then
            '        Throw New Exception("Can't generate XML. There is still " & objCekApproval.Count & " invalid reports or are in Approval.")
            '    End If
            'End Using
            ''End of 5-Aug-2021 Adi : Menambahkan validasi jika masih ada report yang akan di generate statusnya 4 (Waiting for Approval) atau 5 (Need Correction)


            Dim path As String = Server.MapPath("~\goaml\FolderExport\")
            '  Dim path As String = SystemParameterBLL.GetSystemParameterByPk(9017).SettingValue

            Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
            If Not Directory.Exists(Dirpath) Then
                Directory.CreateDirectory(Dirpath)
            End If

            '' Edit 31-Dec-2021
            'NawaDevBLL.GeneratePPATKBLL.ValidateSchema(ObjGeneratedgoAML.Transaction_Date, ObjGeneratedgoAML.Jenis_Laporan, ObjGeneratedgoAML.Last_Update_Date)

            Dim objGenerateParalelStatus(0) As SqlParameter
            objGenerateParalelStatus(0) = New SqlParameter
            objGenerateParalelStatus(0).ParameterName = "@PK_ListOfGenerate_ID"
            objGenerateParalelStatus(0).Value = IDListOfGenerated

            Dim dtGenerateParalelStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_GET_GenerateParalelStatus", objGenerateParalelStatus)

            If dtGenerateParalelStatus IsNot Nothing Then
                For Each row As DataRow In dtGenerateParalelStatus.Rows
                    lblXMLValidatedCount.Text = row.Item("CounterValidate")

                    lblXMLGeneratedCount.Text = row.Item("Counter")
                    lblStatusAPI.Text = row.Item("StatusGenerate")

                    lblErrorMessageAPI.Text = row.Item("ErrorMessage")

                    If row.Item("FileZip") IsNot Nothing And Not (IsDBNull(row.Item("FileZip"))) And Not (IsDBNull(row.Item("FileZipName"))) Then

                        Dim listZip As New List(Of String)
                        Dim dtZip As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text,
                                                                                " select FileZipName,FileZip " &
                                                                                " FROM goAML_GenerateXMLParalel " &
                                                                                " where FK_goAML_Generate_XML_NOID = " & IDListOfGenerated, Nothing)
                        listZip.Clear() '' Added on 02 Feb 2021, untuk hapus attachemnt report sebelumnya, Thanks to Pak Bom

                        If dtZip.Rows.Count > 0 Then
                            For Each itemattachment As Data.DataRow In dtZip.Rows
                                Dim fileattachmentname As String = Dirpath & itemattachment(0)
                                IO.File.WriteAllBytes(fileattachmentname, itemattachment(1))
                                listZip.Add(fileattachmentname)

                            Next
                        End If
                        Filetodownload = listZip.Item(0).ToString

                        If Filetodownload Is Nothing Then
                            Throw New Exception("Not Exists Report")

                        End If

                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update goAML_GenerateXMLParalel " &
                            " set StatusGenerate = 7 " &
                            " ,LastUpdateDate = getdate() " &
                        " where FK_goAML_Generate_XML_NOID =  '" & IDListOfGenerated & "'", Nothing)

                        lblStatusAPI.Text = "XML Generated"
                    ElseIf row.Item("KodeStatusGenerate") = "6" Then
                        'Dim objParam(1) As SqlParameter
                        'objParam(0) = New SqlParameter
                        'objParam(0).ParameterName = "@PK_ListOfGenerate_ID"
                        'objParam(0).Value = IDListOfGenerated
                        'objParam(0).DbType = SqlDbType.BigInt

                        'objParam(1) = New SqlParameter
                        'objParam(1).ParameterName = "@User_ID"
                        'objParam(1).Value = Common.SessionCurrentUser.UserID
                        'objParam(1).DbType = SqlDbType.VarChar

                        'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_GENERATE_XML_SaveEOD", objParam)

                        'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update goAML_GenerateXMLParalel " &
                        '    " set StatusGenerate = 1 " &
                        '    " ,LastUpdateDate = getdate() " &
                        '" where FK_goAML_Generate_XML_NOID =  '" & IDListOfGenerated & "'", Nothing)
                        'lblStatusAPI.Text = "Process Generate XML"
                    ElseIf row.Item("KodeStatusGenerate") = "1" Then
                        Throw New Exception("Process Generate XML by API is in progress. Please check again later.")
                    ElseIf row.Item("KodeStatusGenerate") = "4" Then
                        Throw New Exception("Process Validate XML by API is in progress. Please check again later.")
                    End If

                Next

            End If


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class