﻿Imports System.Data
Imports System.Data.SqlClient
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDAL
Imports NawaDevBLL
Imports NawaDevDAL

Partial Class goAML_WICEdit
    Inherits Parent
#Region "Session"
    Public objgoAML_WIC As WICBLL
    Dim con As New SqlConnection(SQLHelper.strConnectionString)
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("goAML_WICEdit.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_WICEdit.ObjModule") = value
        End Set
    End Property
    Public Property objModuleApproval As NawaDevDAL.ModuleApproval
        Get
            'daniel 13 jan 2021
            Return Session("goAML_WICEdit.objModuleApproval")
            'end 13 jan 2021
        End Get
        Set(value As NawaDevDAL.ModuleApproval)
            'daniel 13 jan 2021
            Session("goAML_WICEdit.objModuleApproval") = value
            'end 13 jan 2021
        End Set
    End Property
    Public Property ObjWIC As goAML_Ref_WIC
        Get
            Return Session("goAML_WICEdit.ObjWIC")
        End Get
        Set(ByVal value As goAML_Ref_WIC)
            Session("goAML_WICEdit.ObjWIC") = value
        End Set
    End Property
    Public Property ObjDirector As goAML_Ref_Walk_In_Customer_Director
        Get
            Return Session("goAML_WICEdit.ObjDirector")
        End Get
        Set(ByVal value As goAML_Ref_Walk_In_Customer_Director)
            Session("goAML_WICEdit.ObjDirector") = value
        End Set
    End Property
    Public Property ListDirectorDetail As List(Of goAML_Ref_Walk_In_Customer_Director)
        Get
            If Session("goAML_WICEdit.ListDirectorDetail") Is Nothing Then
                Session("goAML_WICEdit.ListDirectorDetail") = New List(Of goAML_Ref_Walk_In_Customer_Director)
            End If
            Return Session("goAML_WICEdit.ListDirectorDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Walk_In_Customer_Director))
            Session("goAML_WICEdit.ListDirectorDetail") = value
        End Set
    End Property
    Public Property ListDirectorDetailClass As List(Of WICDirectorDataBLL)
        Get
            If Session("goAML_WICEdit.ListDirectorDetailClass") Is Nothing Then
                Session("goAML_WICEdit.ListDirectorDetailClass") = New List(Of WICDirectorDataBLL)
            End If
            Return Session("goAML_WICEdit.ListDirectorDetailClass")
        End Get
        Set(ByVal value As List(Of WICDirectorDataBLL))
            Session("goAML_WICEdit.ListDirectorDetailClass") = value
        End Set
    End Property
    Public Property ListPhoneDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICEdit.ListPhoneDetail") Is Nothing Then
                Session("goAML_WICEdit.ListPhoneDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICEdit.ListPhoneDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICEdit.ListPhoneDetail") = value
        End Set
    End Property
    Public Property ListPhoneEmployerDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICAdd.ListPhoneEmployerDetail") Is Nothing Then
                Session("goAML_WICAdd.ListPhoneEmployerDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICAdd.ListPhoneEmployerDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICAdd.ListPhoneEmployerDetail") = value
        End Set
    End Property
    Public Property ListAddressEmployerDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICAdd.ListAddressEmployerDetail") Is Nothing Then
                Session("goAML_WICAdd.ListAddressEmployerDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICAdd.ListAddressEmployerDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICAdd.ListAddressEmployerDetail") = value
        End Set
    End Property
    Public Property ListIdentificationDetail As List(Of goAML_Person_Identification)
        Get
            If Session("goAML_WICAdd.ListIdentificationDetail") Is Nothing Then
                Session("goAML_WICAdd.ListIdentificationDetail") = New List(Of goAML_Person_Identification)
            End If
            Return Session("goAML_WICAdd.ListIdentificationDetail")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_WICAdd.ListIdentificationDetail") = value
        End Set
    End Property
    Public Property ObjDetailPhoneEmployer As goAML_Ref_Phone
        Get
            Return Session("goAML_WICAdd.ObjDetailPhoneEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICAdd.ObjDetailPhoneEmployer") = value
        End Set
    End Property
    Public Property ObjDetailIdentification As goAML_Person_Identification
        Get
            Return Session("goAML_WICAdd.ObjDetailIdentification")
        End Get
        Set(ByVal value As goAML_Person_Identification)
            Session("goAML_WICAdd.ObjDetailIdentification") = value
        End Set
    End Property
    Public Property ObjDetailAddressEmployer As goAML_Ref_Address
        Get
            Return Session("goAML_WICAdd.ObjDetailAddressEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICAdd.ObjDetailAddressEmployer") = value
        End Set
    End Property
    Public Property ListPhoneDetailDirector As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICEdit.ListPhoneDetailDirector") Is Nothing Then
                Session("goAML_WICEdit.ListPhoneDetailDirector") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICEdit.ListPhoneDetailDirector")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICEdit.ListPhoneDetailDirector") = value
        End Set
    End Property
    Public Property ListPhoneDetailDirectorEmployer As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICEdit.ListPhoneDetailDirectorEmployer") Is Nothing Then
                Session("goAML_WICEdit.ListPhoneDetailDirectorEmployer") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICEdit.ListPhoneDetailDirectorEmployer")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICEdit.ListPhoneDetailDirectorEmployer") = value
        End Set
    End Property
    Public Property ListAddressDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICEdit.ListAddressDetail") Is Nothing Then
                Session("goAML_WICEdit.ListAddressDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICEdit.ListAddressDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICEdit.ListAddressDetail") = value
        End Set
    End Property
    Public Property ListAddressDetailDirector As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICEdit.ListAddressDetailDirector") Is Nothing Then
                Session("goAML_WICEdit.ListAddressDetailDirector") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICEdit.ListAddressDetailDirector")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICEdit.ListAddressDetailDirector") = value
        End Set
    End Property
    Public Property ListAddressDetailDirectorEmployer As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICEdit.ListAddressDetailDirectorEmployer") Is Nothing Then
                Session("goAML_WICEdit.ListAddressDetailDirectorEmployer") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICEdit.ListAddressDetailDirectorEmployer")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICEdit.ListAddressDetailDirectorEmployer") = value
        End Set
    End Property
    Public Property ListIdentificationDirector As List(Of goAML_Person_Identification)
        Get
            If Session("goAML_WICAdd.ListDirectorIdentification") Is Nothing Then
                Session("goAML_WICAdd.ListDirectorIdentification") = New List(Of goAML_Person_Identification)
            End If
            Return Session("goAML_WICAdd.ListDirectorIdentification")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_WICAdd.ListDirectorIdentification") = value
        End Set
    End Property
    Public Property ObjDetailPhone As goAML_Ref_Phone
        Get
            Return Session("goAML_WICEdit.ObjDetailPhone")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICEdit.ObjDetailPhone") = value
        End Set
    End Property
    Public Property ObjDetailPhoneDirector As goAML_Ref_Phone
        Get
            Return Session("goAML_WICEdit.ObjDetailPhoneDirector")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICEdit.ObjDetailPhoneDirector") = value
        End Set
    End Property
    Public Property ObjDetailPhoneDirectorEmployer As goAML_Ref_Phone
        Get
            Return Session("goAML_WICEdit.ObjDetailPhoneDirectorEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICEdit.ObjDetailPhoneDirectorEmployer") = value
        End Set
    End Property
    Public Property ObjDetailAddress As goAML_Ref_Address
        Get
            Return Session("goAML_WICEdit.ObjDetailAddress")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICEdit.ObjDetailAddress") = value
        End Set
    End Property
    Public Property ObjDetailAddressDirector As goAML_Ref_Address
        Get
            Return Session("goAML_WICEdit.ObjDetailAddressDirector")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICEdit.ObjDetailAddressDirector") = value
        End Set
    End Property
    Public Property ObjDetailAddressDirectorEmployer As goAML_Ref_Address
        Get
            Return Session("goAML_WICEdit.ObjDetailAddressDirectorEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICEdit.ObjDetailAddressDirectorEmployer") = value
        End Set
    End Property
    Public Property ObjDetailIdentificationDirector As goAML_Person_Identification
        Get
            Return Session("goAML_WICAdd.ObjDetailIdentificationDirector")
        End Get
        Set(ByVal value As goAML_Person_Identification)
            Session("goAML_WICAdd.ObjDetailIdentificationDirector") = value
        End Set
    End Property
    Public Property DataRowTemp() As DataRow
        Get
            Return Session("goAML_WICEDIT.DataRowTemp")
        End Get
        Set(ByVal value As DataRow)
            Session("goAML_WICEDIT.DataRowTemp") = value
        End Set
    End Property
#End Region

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()

                Dim strmodule As String = Request.Params("ModuleID")
                Dim intmodule As Integer = Common.DecryptQueryString(strmodule, SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)

                If Not ObjModule Is Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Update) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If

                WIC_Panel.Title = "Non Customer" & " Edit"
                Panelconfirmation.Title = ObjModule.ModuleLabel & " Edit"

                Dim dataStr As String = Request.Params("ID")
                Dim dataID As String = Common.DecryptQueryString(dataStr, SystemParameterBLL.GetEncriptionKey)
                ObjWIC = objgoAML_WIC.GetWICByID(dataID)
                If ObjWIC Is Nothing Then
                    ObjWIC = objgoAML_WIC.GetWICByIDD(dataID)
                End If
                ListAddressDetail = objgoAML_WIC.GetWICByPKIDDetailAddress(dataID)
                ListPhoneDetail = objgoAML_WIC.GetWICByPKIDDetailPhone(dataID)
                ListAddressEmployerDetail = objgoAML_WIC.GetWICByPKIDDetailEmployerAddress(dataID)
                ListPhoneEmployerDetail = objgoAML_WIC.GetWICByPKIDDetailEmployerPhone(dataID)
                ListIdentificationDetail = objgoAML_WIC.GetWICByPKIDDetailIdentification(dataID)

                If ObjWIC.FK_Customer_Type_ID = 2 Then
                    'Daniel 2020 Des 30
                    'Dim Director As New WICDirectorDataBLL
                    'end 2020 Des 30
                    ListDirectorDetail = objgoAML_WIC.GetWICByPKIDDetailDirector(dataID)
                    For Each item In ListDirectorDetail
                        'Daniel 2020 Des 30
                        Dim Director As New WICDirectorDataBLL
                        'end 2020 Des 30
                        With Director
                            .ObjDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorDetail(item.NO_ID)
                            .ListDirectorAddress = objgoAML_WIC.GetWICByPKIDDetailDirectorAddress(item.NO_ID)
                            .ListDirectorAddressEmployer = objgoAML_WIC.GetWICByPKIDDetailDirectorAddressEmployer(item.NO_ID)
                            .ListDirectorPhone = objgoAML_WIC.GetWICByPKIDDetailDirectorPhone(item.NO_ID)
                            .ListDirectorPhoneEmployer = objgoAML_WIC.GetWICByPKIDDetailDirectorPhoneEmployer(item.NO_ID)
                            .ListDirectorIdentification = objgoAML_WIC.GetWICByPKIDDetailDirectorIdentification(item.NO_ID)
                        End With
                        ListDirectorDetailClass.Add(Director)
                    Next

                End If

                'LoadNegara()
                LoadBadanUsaha()
                LoadGender()
                LoadParty()
                LoadKategoriKontak()
                LoadJenisAlatKomunikasi()
                LoadTypeIdentitas()
                LoadData()
                LoadPopup()
                LoadColumn()

                Dim strUnikKey As String = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                objModuleApproval = NawaDevBLL.WICBLL.getDataApproval(strUnikKey, ObjModule.ModuleName)
                If objModuleApproval IsNot Nothing Then
                    btnSave.Hidden = True
                    Throw New ApplicationException("This data is already in approval.")
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadColumn()

        ColumnActionLocation(GridPanel1, CommandColumn1)
        ColumnActionLocation(GridPanel2, CommandColumn2)
        ColumnActionLocation(GridPanel3, CommandColumn3)
        ColumnActionLocation(GridPanel4, CommandColumn4)
        ColumnActionLocation(GridPanel5, CommandColumn5)

        ColumnActionLocation(GP_DirectorPhone, CommandColumn11)
        ColumnActionLocation(GP_DirectorAddress, CommandColumn12)
        ColumnActionLocation(GP_DirectorEmpPhone, CommandColumn9)
        ColumnActionLocation(GP_DirectorEmpAddress, CommandColumn10)
        ColumnActionLocation(GP_DirectorIdentification, CommandColumn6)

        ColumnActionLocation(GridPanelPhoneIndividuDetail, CommandColumnPhoneIndividuDetail)
        ColumnActionLocation(GridPanelAddressIndividuDetail, CommandColumnAddressIndividuDetail)
        ColumnActionLocation(GridPanelPhoneIndividualEmployerDetail, CommandColumnPhoneIndividualEmployerDetail)
        ColumnActionLocation(GridPanelAddressIndividualEmployerDetail, CommandColumnAddressIndividualEmployerDetail)
        ColumnActionLocation(GridPanelIdentificationIndividualDetail, CommandColumnIdentificationIndividualDetail)

        ColumnActionLocation(GridPanelPhoneCorporateDetail, CommandColumnPhoneCorporateDetail)
        ColumnActionLocation(GridPanelAddressCorporateDetail, CommandColumnAddressCorporateDetail)
        ColumnActionLocation(GP_Corp_Director, CommandColumnDirector)

    End Sub

    Private Sub LoadPopup()
        WindowDetailAddress.Maximizable = True
        WindowDetailPhone.Maximizable = True
        WindowDetailDirector.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True
        WindowDetailDirectorIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailIdentification.Maximizable = True
    End Sub
    Protected Sub National_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Nama_Negara", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Communication_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Alat_Komunikasi", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Contact_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and Active=1"
            Else
                strfilter += "Active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Kategori_Kontak", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Incorporation_legal_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Bentuk_Badan_Usaha", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Identification_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goaml_ref_jenis_dokumen_identitas", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Gender_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Kelamin", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Director_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_ODM_Reporting_Person", "PK_ODM_Reporting_Person_ID, Last_Name", strfilter, "Last_Name", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Role_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Peran_orang_dalam_Korporasi", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Private Sub LoadGender()
        cmb_Director_Gender.PageSize = SystemParameterBLL.GetPageSize
        StoreGender.Reload()
    End Sub
    Private Sub LoadKategoriKontak()
        Cmbtph_contact_type.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignContact_Type.Reload()
        'Agam 20102020 
        Cmbtph_contact_typeEmployer.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignContact_TypeEmployer.Reload()
        CmbAddress_typeEmployer.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignAddress_TypeEmployer.Reload()
        'Cmbcountry_codeEmployer.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignCountry_CodeAddressEmployer.Reload()

        ' end agam
        CmbAddress_type.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignAddress_Type.Reload()
        Director_Emp_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreEmpDirPContactType.Reload()
        Director_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreDirPContactType.Reload()
        Director_cmb_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        StoreDirKategoryADdressAddress_Type.Reload()
        Director_cmb_emp_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        StoreEmpDirKategoryADdressAddress_Type.Reload()
        'CmbCountryIdentification.PageSize = SystemParameterBLL.GetPageSize
        'StoreCountryCodeIdentification.Reload()
    End Sub
    Private Sub LoadTypeIdentitas()
        CmbTypeIdentification.PageSize = SystemParameterBLL.GetPageSize
        StoreTypeIdentification.Reload()
        CmbTypeIdentificationDirector.PageSize = SystemParameterBLL.GetPageSize
        StoreTypeIdentificationDirector.Reload()
    End Sub
    Private Sub LoadJenisAlatKomunikasi()
        Cmbtph_communication_type.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignCommunication_Type.Reload()
        Director_Emp_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreEmpDirComunicationType.Reload()
        Director_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreDirComunicationType.Reload()
        Cmbtph_communication_typeEmployer.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignCommunication_TypeEmployer.Reload()
    End Sub

    Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
        objgoAML_WIC = New WICBLL(WIC_Panel)
    End Sub

#End Region

#Region "Method"
    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadParty()
        cmb_peran.PageSize = SystemParameterBLL.GetPageSize
        StorePeran.Reload()
    End Sub
    Private Sub LoadBadanUsaha()
        CmbCorp_Incorporation_legal_form.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignIncorporation_legal.Reload()
    End Sub

    Private Sub LoadNegara()
        'agam 22102020
        'TxtINDV_Passport_country.PageSize = SystemParameterBLL.GetPageSize
        'StoreIndividuPassportCountry.Reload()
        'CmbINDV_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignNational1.Reload()
        'CmbINDV_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignNational2.Reload()
        'CmbINDV_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignNational3.Reload()
        'CmbINDV_residence.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignResisdance.Reload()
        'cmb_Director_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirNational1.Reload()
        'cmb_Director_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirNational2.Reload()
        'cmb_Director_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirNational3.Reload()
        'cmb_Director_Residence.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirReisdence.Reload()

        'CmbCountryIdentificationDirector.PageSize = SystemParameterBLL.GetPageSize
        'StoreCountryCodeIdentificationDirector.Reload()
        'CmbCorp_incorporation_country_code.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignCountry_Code.Reload()
        'Cmbcountry_code.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignCountry_CodeAddress.Reload()

        'Director_cmb_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirFromForeignCountry_CodeAddress.Reload()
        'Director_cmb_emp_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'StoreEmpDirFromForeignCountry_CodeAddress.Reload()


    End Sub
    Private Sub LoadData()
        Try
            Dim DirectorClass As New WICDirectorDataBLL
            If Not ObjWIC Is Nothing Then
                With ObjWIC
                    PKWIC.Text = .PK_Customer_ID
                    WICNo.Text = .WIC_No
                    If .isUpdateFromDataSource IsNot Nothing Then
                        CheckboxIsUpdateFromDataSource.Checked = .isUpdateFromDataSource

                    Else
                        CheckboxIsUpdateFromDataSource.Checked = False
                    End If
                    If .FK_Customer_Type_ID = 1 Then
                        TxtINDV_Title.Text = .INDV_Title
                        TxtINDV_last_name.Text = .INDV_Last_Name
                        If .INDV_Gender IsNot Nothing Then
                            CmbINDV_Gender.SetValueAndFireSelect(.INDV_Gender)
                        End If
                        If .INDV_BirthDate IsNot Nothing Then
                            DateINDV_Birthdate.Value = .INDV_BirthDate
                        End If
                        TxtINDV_Birth_place.Text = .INDV_Birth_Place
                        TxtINDV_Mothers_name.Text = .INDV_Mothers_Name
                        TxtINDV_Alias.Text = .INDV_Alias
                        TxtINDV_SSN.Text = .INDV_SSN
                        TxtINDV_Passport_number.Text = .INDV_Passport_Number
                        'agam 20102020
                        ' TxtINDV_Passport_country.Text = .INDV_Passport_Country
                        If Not IsNothing(.INDV_Passport_Country) Then
                            DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .INDV_Passport_Country & "'")
                            If DataRowTemp IsNot Nothing Then
                                TxtINDV_Passport_country.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            End If
                        End If
                        TxtINDV_ID_Number.Text = .INDV_ID_Number
                        If Not IsNothing(.INDV_Nationality1) Then
                            DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .INDV_Nationality1 & "'")
                            If DataRowTemp IsNot Nothing Then
                                CmbINDV_Nationality1.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Nationality2) Then
                            DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .INDV_Nationality2 & "'")
                            If DataRowTemp IsNot Nothing Then
                                CmbINDV_Nationality2.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Nationality3) Then
                            DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .INDV_Nationality3 & "'")
                            If DataRowTemp IsNot Nothing Then
                                CmbINDV_Nationality3.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Residence) Then
                            DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .INDV_Residence & "'")
                            If DataRowTemp IsNot Nothing Then
                                CmbINDV_residence.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            End If
                        End If
                        'CmbINDV_Nationality1.SetValueAndFireSelect(.INDV_Nationality1)
                        'CmbINDV_Nationality2.SetValue(.INDV_Nationality2)
                        'CmbINDV_Nationality3.SetValue(.INDV_Nationality3)
                        'CmbINDV_residence.SetValue(.INDV_Residence)

                        TxtINDV_Email.Focus()
                        TxtINDV_Email.Text = .INDV_Email
                        TxtINDV_Email2.Text = .INDV_Email2
                        TxtINDV_Email3.Text = .INDV_Email3
                        TxtINDV_Email4.Text = .INDV_Email4
                        TxtINDV_Email5.Text = .INDV_Email5
                        TxtINDV_Occupation.Text = .INDV_Occupation
                        TxtINDV_employer_name.Text = .INDV_Employer_Name
                        TxtINDV_Source_Of_Wealth.Text = .INDV_SumberDana
                        TxtINDV_Tax_Number.Text = .INDV_Tax_Number
                        CbINDV_Tax_Reg_Number.Checked = .INDV_Tax_Reg_Number
                        If .INDV_Deceased IsNot Nothing Then
                            CbINDV_Deceased.Checked = .INDV_Deceased
                        Else
                            CbINDV_Deceased.Checked = False
                        End If

                        If CbINDV_Deceased.Checked Then
                            DateINDV_Deceased_Date.Show()
                            If .INDV_Deceased_Date IsNot Nothing Then
                                DateINDV_Deceased_Date.Text = .INDV_Deceased_Date
                            End If
                        End If
                        TxtINDV_Comments.Text = .INDV_Comment
                        FKCustomer.Text = .FK_Customer_Type_ID
                    Else
                        TxtCorp_Name.Text = .Corp_Name
                        TxtCorp_Commercial_name.Text = .Corp_Commercial_Name
                        CmbCorp_Incorporation_legal_form.SetValueAndFireSelect(.Corp_Incorporation_Legal_Form)
                        TxtCorp_Incorporation_number.Text = .Corp_Incorporation_Number
                        TxtCorp_Business.Text = .Corp_Business
                        TxtCorp_Email.Text = .Corp_Email
                        TxtCorp_url.Text = .Corp_Url
                        TxtCorp_incorporation_state.Text = .Corp_Incorporation_State
                        'CmbCorp_incorporation_country_code.SetValueAndFireSelect(.Corp_Incorporation_Country_Code)
                        If Not IsNothing(.Corp_Incorporation_Country_Code) Then
                            DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Corp_Incorporation_Country_Code & "'")
                            If DataRowTemp IsNot Nothing Then
                                CmbCorp_incorporation_country_code.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            End If
                        End If
                        If .Corp_Incorporation_Date IsNot Nothing Then
                            DateCorp_incorporation_date.Text = .Corp_Incorporation_Date
                        End If
                        If .Corp_Business_Closed IsNot Nothing Then
                            chkCorp_business_closed.Checked = .Corp_Business_Closed
                        End If
                        If chkCorp_business_closed.Checked Then
                            If .Corp_Date_Business_Closed IsNot Nothing Then
                                DateCorp_date_business_closed.Text = .Corp_Date_Business_Closed
                            End If
                            DateCorp_date_business_closed.Show()
                        End If
                        TxtCorp_tax_number.Text = .Corp_Tax_Number
                        TxtCorp_Comments.Text = .Corp_Comments
                        FKCustomer.Text = .FK_Customer_Type_ID
                    End If
                End With
                If ObjWIC.FK_Customer_Type_ID = 1 Then
                    BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
                    BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
                    BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)
                    BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)
                    BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
                    WIC_Individu.Show()
                    WIC_Corporate.Hide()
                Else
                    BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
                    BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
                    BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
                    WIC_Individu.Hide()
                    WIC_Corporate.Show()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub ClearSession()
        ObjWIC = Nothing
        ObjDirector = Nothing
        'ObjModule = Nothing
        ListDirectorDetailClass = New List(Of WICDirectorDataBLL)
        ListAddressDetail = New List(Of goAML_Ref_Address)
        ListAddressEmployerDetail = New List(Of goAML_Ref_Address)
        ListAddressDetailDirector = New List(Of goAML_Ref_Address)
        ListAddressDetailDirectorEmployer = New List(Of goAML_Ref_Address)
        ListPhoneDetail = New List(Of goAML_Ref_Phone)
        ListPhoneEmployerDetail = New List(Of goAML_Ref_Phone)
        ListPhoneDetailDirector = New List(Of goAML_Ref_Phone)
        ListPhoneDetailDirectorEmployer = New List(Of goAML_Ref_Phone)
        ListIdentificationDetail = New List(Of goAML_Person_Identification)
        ListIdentificationDirector = New List(Of goAML_Person_Identification)

        ObjDetailAddress = Nothing
        ObjDetailAddressEmployer = Nothing
        ObjDetailAddressDirector = Nothing
        ObjDetailAddressDirectorEmployer = Nothing
        ObjDetailPhone = Nothing
        ObjDetailPhoneEmployer = Nothing
        ObjDetailPhoneDirector = Nothing
        ObjDetailPhoneDirectorEmployer = Nothing
        ObjDetailIdentification = Nothing
        ObjDetailIdentificationDirector = Nothing
    End Sub

    Private Sub clearinputDirector()
        Director_cb_phone_Contact_Type.ClearValue()
        Director_cb_phone_Communication_Type.ClearValue()
        Director_txt_phone_Country_prefix.Clear()
        Director_txt_phone_number.Clear()
        Director_txt_phone_extension.Clear()
        Director_txt_phone_comments.Clear()

        Director_Emp_cb_phone_Contact_Type.ClearValue()
        Director_Emp_cb_phone_Communication_Type.ClearValue()
        Director_Emp_txt_phone_Country_prefix.Clear()
        Director_Emp_txt_phone_number.Clear()
        Director_Emp_txt_phone_extension.Clear()
        Director_Emp_txt_phone_comments.Clear()

        Director_cmb_kategoriaddress.ClearValue()
        Director_Address.Clear()
        Director_Town.Clear()
        Director_City.Clear()
        Director_Zip.Clear()
        Director_cmb_kodenegara.SetTextValue("")
        Director_State.Clear()
        Director_Comment_Address.Clear()

        Director_cmb_emp_kategoriaddress.ClearValue()
        Director_emp_Address.Clear()
        Director_emp_Town.Clear()
        Director_emp_City.Clear()
        Director_emp_Zip.Clear()
        Director_cmb_emp_kodenegara.SetTextValue("")
        Director_emp_State.Clear()
        Director_emp_Comment_Address.Clear()

        CmbTypeIdentificationDirector.ClearValue()
        TxtNumberDirector.Clear()
        TxtIssueDateDirector.Clear()
        TxtExpiryDateDirector.Clear()
        TxtIssuedByDirector.Clear()
        CmbCountryIdentificationDirector.SetTextValue("")
        TxtCommentIdentificationDirector.Clear()

    End Sub
    Private Sub ClearInput()
        Cmbtph_contact_type.ClearValue()
        Cmbtph_communication_type.ClearValue()
        Txttph_country_prefix.Clear()
        Txttph_number.Clear()
        Txttph_extension.Clear()
        TxtcommentsPhone.Clear()

        CmbAddress_type.ClearValue()
        TxtAddress.Clear()
        TxtTown.Clear()
        TxtCity.Clear()
        TxtZip.Clear()
        Cmbcountry_code.SetTextValue("")
        TxtState.Clear()
        TxtcommentsAddress.Clear()

        ListAddressDetailDirector.Clear()
        ListAddressDetailDirectorEmployer.Clear()
        ListPhoneDetailDirector.Clear()
        ListPhoneDetailDirectorEmployer.Clear()
        ListIdentificationDirector.Clear()

        Cmbtph_contact_typeEmployer.ClearValue()
        Cmbtph_communication_typeEmployer.ClearValue()
        Txttph_country_prefixEmployer.Clear()
        Txttph_numberEmployer.Clear()
        Txttph_extensionEmployer.Clear()
        TxtcommentsPhoneEmployer.Clear()

        CmbAddress_typeEmployer.ClearValue()
        TxtAddressEmployer.Clear()
        TxtTownEmployer.Clear()
        TxtCityEmployer.Clear()
        TxtZipEmployer.Clear()
        Cmbcountry_codeEmployer.SetTextValue("")
        TxtStateEmployer.Clear()

        CmbTypeIdentification.ClearValue()
        TxtNumber.Clear()
        TxtIssueDate.Clear()
        TxtExpiryDate.Clear()
        TxtIssuedBy.Clear()
        CmbCountryIdentification.SetTextValue("")
        TxtCommentIdentification.Clear()

        Hidden2.Value = ""
        cmb_peran.ClearValue()
        txt_Director_Title.Clear()
        txt_Director_Last_Name.Clear()
        cmb_Director_Gender.ClearValue()
        txt_Director_BirthDate.Clear()
        txt_Director_Birth_Place.Clear()
        txt_Director_Mothers_Name.Clear()
        txt_Director_Alias.Clear()
        txt_Director_ID_Number.Clear()
        txt_Director_Passport_Number.Clear()
        'txt_Director_Passport_Country.Clear()
        txt_Director_Passport_Country.SetTextValue("")
        txt_Director_SSN.Clear()
        cmb_Director_Nationality1.SetTextValue("")
        cmb_Director_Nationality2.SetTextValue("")
        cmb_Director_Nationality3.SetTextValue("")
        cmb_Director_Residence.SetTextValue("")
        'cmb_Director_Nationality1.ClearValue()
        'cmb_Director_Nationality2.ClearValue()
        'cmb_Director_Nationality3.ClearValue()
        'cmb_Director_Residence.ClearValue()
        txt_Director_Email.Clear()
        txt_Director_Email2.Clear()
        txt_Director_Email3.Clear()
        txt_Director_Email4.Clear()
        txt_Director_Email5.Clear()
        cb_Director_Deceased.Clear()
        txt_Director_Deceased_Date.Clear()
        cb_Director_Tax_Reg_Number.Clear()
        txt_Director_Tax_Number.Clear()
        txt_Director_Source_of_Wealth.Clear()
        txt_Director_Occupation.Clear()
        txt_Director_Comments.Clear()
        txt_Director_employer_name.Clear()

        ObjDirector = Nothing
        'daniel 21 jan 2021
        BindDetailIdentification(StoreIdentificationDirectorDetail, ListIdentificationDirector)
        'end 21 jan 2021
        BindDetailAddress(Store_DirectorAddress, ListAddressDetailDirector)
        BindDetailAddress(Store_Director_Employer_Address, ListAddressDetailDirectorEmployer)

        BindDetailPhone(Store_DirectorPhone, ListPhoneDetailDirector)
        BindDetailPhone(Store_Director_Employer_Phone, ListPhoneDetailDirectorEmployer)



    End Sub

#End Region

#Region "Direct Event"
    Protected Sub checkBoxcb_Director_Deceased_click(sender As Object, e As DirectEventArgs)
        If cb_Director_Deceased.Checked Then
            txt_Director_Deceased_Date.Hidden = False
        Else
            txt_Director_Deceased_Date.Hidden = True
        End If
    End Sub
    Protected Sub checkBoxcb_INDV_Deceased_click(sender As Object, e As DirectEventArgs)
        If CbINDV_Deceased.Checked Then
            DateINDV_Deceased_Date.Hidden = False
        Else
            DateINDV_Deceased_Date.Hidden = True
        End If
    End Sub
    Protected Sub chkCorp_business_closed_Event(sender As Object, e As DirectEventArgs)
        If chkCorp_business_closed.Checked Then
            DateCorp_date_business_closed.Hidden = False
        Else
            DateCorp_date_business_closed.Hidden = True
        End If
    End Sub
    Protected Sub btnAddCustomerDirector_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            WindowDetailDirector.Show()
            FP_Director.Show()
            FormPanelDirectorDetail.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewPhoneIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()

            WindowDetailAddress.Hide()
            WindowDetailPhone.Title = "Add Phone"
            WindowDetailPhone.Show()

            FormPanelAddressDetail.Hide()
            FormPanelPhoneDetail.Show()
            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewPhoneEmployerIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            'Daniel 2020 Des 28
            If ListPhoneEmployerDetail.Count < 1 Then
                'End 2020 Des 28
                ClearInput()

                WindowDetailPhone.Title = "Add Phone Employer"
                WindowDetailPhone.Show()

                FormPanelPhoneDetail.Hide()
                PanelDetailPhone.Hide()
                PanelDetailPhoneEmployer.Hide()
                FormPanelPhoneEmployerDetail.Show()
                'Daniel 2020 Des 28
            Else
                Throw New Exception("can only enter one Workplace Phone Information")
            End If
            'end 2020 Des 28
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()
            WindowDetailDirectorPhone.Title = "Add Director Phone"
            WindowDetailDirectorPhone.Show()
            FormPanelDirectorPhone.Show()
            FormPanelEmpDirectorTaskDetail.Hide()
            PanelPhoneDirector.Hide()
            FP_Director.Show()
            PanelPhoneDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorEmployerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()
            WindowDetailDirectorPhone.Title = "Add Director Phone Employer"
            WindowDetailDirectorPhone.Show()
            FormPanelPhoneDetail.Hide()
            FormPanelEmpDirectorTaskDetail.Show()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()
            FP_Director.Show()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()
            WindowDetailDirectorAddress.Title = "Add Director Address"
            WindowDetailDirectorAddress.Show()
            FP_Address_Director.Show()
            FP_Address_Emp_Director.Hide()
            PanelAddressDirector.Hide()
            FP_Director.Show()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorIdentification_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()
            WindowDetailDirectorIdentification.Title = "Add Identitas"
            WindowDetailDirectorIdentification.Show()
            FormPanelDirectorIdentification.Show()
            PanelDetailDirectorIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddIdentification_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            WindowDetailIdentification.Title = "Add Identitas"
            WindowDetailIdentification.Show()
            FormPanelIdentification.Show()
            PanelDetailIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorEmployerAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()
            WindowDetailDirectorAddress.Title = "Add Director Address Emplpoyer"
            WindowDetailDirectorAddress.Show()
            FP_Address_Director.Hide()
            FP_Address_Emp_Director.Show()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub BtnAddNewAddressIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            FormPanelAddressDetail.Hidden = False
            FormPanelPhoneDetail.Hidden = True
            WindowDetailAddress.Title = "Add Address"
            WindowDetailPhone.Hidden = True
            WindowDetailAddress.Hidden = False
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewAddressEmployerIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            'Daniel 2020 Des 28
            If ListAddressEmployerDetail.Count < 1 Then
                'end 2020 Des 28
                ClearInput()
                FormPanelAddressDetail.Hidden = True
                PanelDetailAddress.Hidden = True
                PanelDetailAddressEmployer.Hidden = True
                FormPanelAddressDetailEmployer.Hidden = False
                WindowDetailAddress.Title = "Add Address Employer"
                WindowDetailAddress.Hidden = False
                'Daniel 2020 Des 28
            Else
                Throw New Exception("can only enter one Workplace Address Information")
            End If
            'end 2020 Des 28
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewPhoneCorporate_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            FormPanelAddressDetail.Hidden = True
            FormPanelPhoneDetail.Hidden = False
            WindowDetailPhone.Title = "Add Phone Corporate"
            WindowDetailPhone.Hidden = False
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewAddressCorporate_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            FormPanelAddressDetail.Hidden = False
            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
            WindowDetailAddress.Title = "Add Address Corporate"
            WindowDetailAddress.Hidden = False
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindDetailIdentification(store As Store, list As List(Of goAML_Person_Identification))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Tipe", GetType(String)))
        objtable.Columns.Add(New DataColumn("No", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssueDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("ExpiryDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssuedCountry", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Tipe") = GlobalReportFunctionBLL.getjenisDokumenByKode(item("Type"))
                item("No") = item("Number")
                item("IssueDate") = item("Issue_Date")
                item("ExpiryDate") = item("Expiry_Date")
                item("IssuedCountry") = GlobalReportFunctionBLL.getCountryByCode(item("Issued_Country"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Private Sub BindDetailPhone(store As Store, list As List(Of goAML_Ref_Phone))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
                item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailAddress(store As Store, list As List(Of goAML_Ref_Address))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Type_Address", GetType(String)))
        objtable.Columns.Add(New DataColumn("Addres", GetType(String)))
        objtable.Columns.Add(New DataColumn("Cty", GetType(String)))
        objtable.Columns.Add(New DataColumn("CountryCode", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Type_Address") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type"))
                item("Addres") = item("Address")
                item("Cty") = item("City")
                item("CountryCode") = GlobalReportFunctionBLL.getCountryByCode(item("Country_Code"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailDirector(store As Store, list As List(Of goAML_Ref_Walk_In_Customer_Director))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("LastName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Roles", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("LastName") = item("Last_Name")
                item("Roles") = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(item("Role"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Protected Sub BtnSaveDetailIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailIdentification Is Nothing Then
                SaveAddDetailIdentification()
            Else

                Dim status As Boolean = True
                Dim ObjDetailIdentificationNew As New goAML_Person_Identification

                With ObjDetailIdentificationNew
                    .Type = CmbTypeIdentification.Text
                    .Number = TxtNumber.Text
                    If TxtIssueDate.Text <> DateTime.MinValue Then
                        .Issue_Date = TxtIssueDate.Text
                    End If
                    If TxtExpiryDate.Text <> DateTime.MinValue Then
                        .Expiry_Date = TxtExpiryDate.Text
                    End If
                    .Issued_By = TxtIssuedBy.Text
                    .Issued_Country = CmbCountryIdentification.TextValue
                    .Identification_Comment = TxtCommentIdentification.Text
                End With

                With ObjDetailIdentification
                    If .Type <> ObjDetailIdentificationNew.Type Then
                        status = False
                    End If
                    If .Number <> ObjDetailIdentificationNew.Number Then
                        status = False
                    End If
                    If .Issue_Date <> ObjDetailIdentificationNew.Issue_Date Then
                        status = False
                    End If
                    If .Expiry_Date <> ObjDetailIdentificationNew.Expiry_Date Then
                        status = False
                    End If
                    If .Issued_By <> ObjDetailIdentificationNew.Issued_By Then
                        status = False
                    End If
                    If .Issued_Country <> ObjDetailIdentificationNew.Issued_Country Then
                        status = False
                    End If
                    If .Identification_Comment <> ObjDetailIdentificationNew.Identification_Comment Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailIdentification()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailIdentificationDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'Daniel 2021 Jan 6
            'If ObjDetailIdentification Is Nothing Then
            If ObjDetailIdentificationDirector Is Nothing Then
                'end 2021 Jan 6
                SaveAddDetailIdentificationDirector()
            Else
                Dim status As Boolean = True
                Dim ObjDetailIdentificationNew As New goAML_Person_Identification

                With ObjDetailIdentificationNew
                    .Type = CmbTypeIdentificationDirector.Text
                    .Number = TxtNumberDirector.Text
                    If TxtIssueDateDirector.Text <> DateTime.MinValue Then
                        .Issue_Date = TxtIssueDateDirector.Value
                    End If
                    If TxtExpiryDateDirector.Text <> DateTime.MinValue Then
                        .Expiry_Date = TxtExpiryDateDirector.Value
                    End If
                    .Issued_By = TxtIssuedByDirector.Text
                    .Issued_Country = CmbCountryIdentificationDirector.TextValue
                    .Identification_Comment = TxtCommentIdentificationDirector.Text
                End With

                'Daniel 2021 Jan 6
                'With ObjDetailIdentification
                With ObjDetailIdentificationDirector
                    'Daniel 2021 Jan 6
                    If .Type <> ObjDetailIdentificationNew.Type Then
                        status = False
                    End If
                    If .Number <> ObjDetailIdentificationNew.Number Then
                        status = False
                    End If
                    If .Issue_Date <> ObjDetailIdentificationNew.Issue_Date Then
                        status = False
                    End If
                    If .Expiry_Date <> ObjDetailIdentificationNew.Expiry_Date Then
                        status = False
                    End If
                    If .Issued_By <> ObjDetailIdentificationNew.Issued_By Then
                        status = False
                    End If
                    If .Issued_Country <> ObjDetailIdentificationNew.Issued_Country Then
                        status = False
                    End If
                    If .Identification_Comment <> ObjDetailIdentificationNew.Identification_Comment Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailIdentificationDirector()
                End If

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailPhone_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhone Is Nothing Then
                SaveAddDetailPhone()
            Else
                Dim status As Boolean = True
                Dim ObjDetailPhoneNew As New goAML_Ref_Phone

                With ObjDetailPhoneNew
                    .Tph_Contact_Type = Cmbtph_contact_type.Text
                    .Tph_Communication_Type = Cmbtph_communication_type.Text
                    .tph_country_prefix = Txttph_country_prefix.Text
                    .tph_number = Txttph_number.Text
                    .tph_extension = Txttph_extension.Text
                    .comments = TxtcommentsPhone.Text
                End With

                With ObjDetailPhone
                    If .Tph_Contact_Type <> ObjDetailPhoneNew.Tph_Contact_Type Then
                        status = False
                    End If
                    If .Tph_Communication_Type <> ObjDetailPhoneNew.Tph_Communication_Type Then
                        status = False
                    End If
                    If .tph_country_prefix <> ObjDetailPhoneNew.tph_country_prefix Then
                        status = False
                    End If
                    If .tph_number <> ObjDetailPhoneNew.tph_number Then
                        status = False
                    End If
                    If .tph_extension <> ObjDetailPhoneNew.tph_extension Then
                        status = False
                    End If
                    If .comments <> ObjDetailPhoneNew.comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailPhone()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailPhoneEmployer_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhoneEmployer Is Nothing Then
                SaveAddDetailPhoneEmployer()
            Else
                Dim status As Boolean = True
                Dim ObjDetailPhoneEmployerNew As New goAML_Ref_Phone

                With ObjDetailPhoneEmployerNew
                    .Tph_Contact_Type = Cmbtph_contact_typeEmployer.Text
                    .Tph_Communication_Type = Cmbtph_communication_typeEmployer.Text
                    .tph_country_prefix = Txttph_country_prefixEmployer.Text
                    .tph_number = Txttph_numberEmployer.Text
                    .tph_extension = Txttph_extensionEmployer.Text
                    .comments = TxtcommentsPhoneEmployer.Text
                End With

                With ObjDetailPhoneEmployer
                    If .Tph_Contact_Type <> ObjDetailPhoneEmployerNew.Tph_Contact_Type Then
                        status = False
                    End If
                    If .Tph_Communication_Type <> ObjDetailPhoneEmployerNew.Tph_Communication_Type Then
                        status = False
                    End If
                    If .tph_country_prefix <> ObjDetailPhoneEmployerNew.tph_country_prefix Then
                        status = False
                    End If
                    If .tph_number <> ObjDetailPhoneEmployerNew.tph_number Then
                        status = False
                    End If
                    If .tph_extension <> ObjDetailPhoneEmployerNew.tph_extension Then
                        status = False
                    End If
                    If .comments <> ObjDetailPhoneEmployerNew.comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailPhoneEmployer()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailIdentification()
        Try
            Dim objNewDetailIdentification As New goAML_Person_Identification
            With objNewDetailIdentification
                ' update agam 21102020
                If ListAddressDetailDirectorEmployer.Count = 0 Then
                    .PK_Person_Identification_ID = -1
                ElseIf ListIdentificationDetail.Count > 0 And ListIdentificationDetail.Min(Function(x) x.PK_Person_Identification_ID) > 0 Then
                    .PK_Person_Identification_ID = -1
                ElseIf ListAddressDetailDirectorEmployer.Count > 0 And ListIdentificationDetail.Min(Function(x) x.PK_Person_Identification_ID) < 0 Then
                    .PK_Person_Identification_ID = ListIdentificationDetail.Min(Function(x) x.PK_Person_Identification_ID) - 1
                End If
                '.PK_Person_Identification_ID = ListIdentificationDetail.Select(Function(x) x.PK_Person_Identification_ID).DefaultIfEmpty(0).Max + 1
                .FK_Person_ID = ObjWIC.PK_Customer_ID
                .FK_Person_Type = 8
                .Type = CmbTypeIdentification.Text
                .Number = TxtNumber.Text
                If TxtIssueDate.Text <> DateTime.MinValue Then
                    .Issue_Date = TxtIssueDate.Text
                End If
                If TxtExpiryDate.Text <> DateTime.MinValue Then
                    .Expiry_Date = TxtExpiryDate.Text
                End If
                .Issued_By = TxtIssuedBy.Text
                .Issued_Country = CmbCountryIdentification.TextValue
                .Identification_Comment = TxtCommentIdentification.Text
            End With

            ListIdentificationDetail.Add(objNewDetailIdentification)
            BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
            FormPanelIdentification.Hidden = True
            WindowDetailIdentification.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailIdentificationDirector()
        Try
            Dim objNewDetailIdentification As New goAML_Person_Identification
            With objNewDetailIdentification
                ' update agam 21102020
                If ListIdentificationDirector.Count = 0 Then
                    .PK_Person_Identification_ID = -1
                ElseIf ListIdentificationDirector.Count > 0 And ListIdentificationDirector.Min(Function(x) x.PK_Person_Identification_ID) > 0 Then
                    .PK_Person_Identification_ID = -1
                ElseIf ListIdentificationDirector.Count > 0 And ListIdentificationDirector.Min(Function(x) x.PK_Person_Identification_ID) < 0 Then
                    .PK_Person_Identification_ID = ListIdentificationDirector.Min(Function(x) x.PK_Person_Identification_ID) - 1
                End If
                ''  .PK_Person_Identification_ID = ListIdentificationDirector.Select(Function(x) x.PK_Person_Identification_ID).DefaultIfEmpty(0).Max + 1
                '.FK_Person_ID = ObjWIC.PK_Customer_ID
                '.PK_Person_Identification_ID = ListIdentificationDirector.Select(Function(x) x.PK_Person_Identification_ID).DefaultIfEmpty(0).Max + 1
                .FK_Person_ID = If(ObjDirector Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, ObjDirector.NO_ID)
                .FK_Person_Type = 7
                .Type = CmbTypeIdentificationDirector.Text
                .Number = TxtNumberDirector.Text
                If TxtIssueDateDirector.Text <> DateTime.MinValue Then
                    .Issue_Date = TxtIssueDateDirector.Text
                End If
                If TxtExpiryDateDirector.Text <> DateTime.MinValue Then
                    .Expiry_Date = TxtExpiryDateDirector.Text
                End If
                .Issued_By = TxtIssuedByDirector.Text
                .Issued_Country = CmbCountryIdentificationDirector.TextValue
                .Identification_Comment = TxtCommentIdentificationDirector.Text
            End With

            ListIdentificationDirector.Add(objNewDetailIdentification)

            'StoreIdentificationDirector.DataSource = ListIdentificationDirector.Where(Function(x) x.FK_Person_ID = ObjWIC.PK_Customer_ID).ToList()
            'StoreIdentificationDirector.DataBind()
            'Daniel 2021 Jan 4
            BindDetailIdentification(StoreIdentificationDirectorDetail, ListIdentificationDirector)
            'end 2021 Jan 4

            FormPanelDirectorIdentification.Hidden = True
            WindowDetailDirectorIdentification.Hidden = True
            'StoreIdentificationDirector.Reload()
            'FormPanelDirectorIdentification.Hidden = True
            'WindowDetailDirectorIdentification.Hidden = True
            'GP_DirectorIdentification.Reload()
            'WindowDetailDirectorIdentification.Render()
            'WIC_Panel.Reload()
            WindowDetailDirector.Reload()
            'PanelDetailDirectorIdentification.Render()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhone()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                ' update agam 21102020
                If ListPhoneDetail.Count = 0 Then
                    .PK_goAML_Ref_Phone = -1
                ElseIf ListPhoneDetail.Count > 0 And ListPhoneDetail.Min(Function(x) x.PK_goAML_Ref_Phone) > 0 Then
                    .PK_goAML_Ref_Phone = -1
                ElseIf ListPhoneDetail.Count > 0 And ListPhoneDetail.Min(Function(x) x.PK_goAML_Ref_Phone) < 0 Then
                    .PK_goAML_Ref_Phone = ListPhoneDetail.Min(Function(x) x.PK_goAML_Ref_Phone) - 1
                End If
                '.PK_goAML_Ref_Phone = ListPhoneDetail.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 3
                .FK_for_Table_ID = ObjWIC.PK_Customer_ID
                .Tph_Contact_Type = Cmbtph_contact_type.Text
                .Tph_Communication_Type = Cmbtph_communication_type.Text
                .tph_country_prefix = Txttph_country_prefix.Text
                .tph_number = Txttph_number.Text
                .tph_extension = Txttph_extension.Text
                .comments = TxtcommentsPhone.Text
            End With

            ListPhoneDetail.Add(objNewDetailPhone)
            If ObjWIC.FK_Customer_Type_ID = 1 Then
                BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
            Else
                BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
            End If
            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhoneEmployer()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                ' update agam 21102020
                If ListPhoneEmployerDetail.Count = 0 Then
                    .PK_goAML_Ref_Phone = -1
                ElseIf ListPhoneEmployerDetail.Count > 0 And ListPhoneEmployerDetail.Min(Function(x) x.PK_goAML_Ref_Phone) > 0 Then
                    .PK_goAML_Ref_Phone = -1
                ElseIf ListPhoneEmployerDetail.Count > 0 And ListPhoneEmployerDetail.Min(Function(x) x.PK_goAML_Ref_Phone) < 0 Then
                    .PK_goAML_Ref_Phone = ListPhoneEmployerDetail.Min(Function(x) x.PK_goAML_Ref_Phone) - 1
                End If
                '.PK_goAML_Ref_Phone = ListPhoneEmployerDetail.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 10
                .FK_for_Table_ID = ObjWIC.PK_Customer_ID
                .Tph_Contact_Type = Cmbtph_contact_typeEmployer.Text
                .Tph_Communication_Type = Cmbtph_communication_typeEmployer.Text
                .tph_country_prefix = Txttph_country_prefixEmployer.Text
                .tph_number = Txttph_numberEmployer.Text
                .tph_extension = Txttph_extensionEmployer.Text
                .comments = TxtcommentsPhoneEmployer.Text
            End With

            ListPhoneEmployerDetail.Add(objNewDetailPhone)
            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)
            FormPanelPhoneEmployerDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhoneDirector()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                ' update agam 21102020
                If ListPhoneDetailDirector.Count = 0 Then
                    .PK_goAML_Ref_Phone = -1
                ElseIf ListPhoneDetailDirector.Count > 0 And ListPhoneDetailDirector.Min(Function(x) x.PK_goAML_Ref_Phone) > 0 Then
                    .PK_goAML_Ref_Phone = -1
                ElseIf ListPhoneDetailDirector.Count > 0 And ListPhoneDetailDirector.Min(Function(x) x.PK_goAML_Ref_Phone) < 0 Then
                    .PK_goAML_Ref_Phone = ListPhoneDetailDirector.Min(Function(x) x.PK_goAML_Ref_Phone) - 1
                End If
                '.PK_goAML_Ref_Phone = ListPhoneDetailDirector.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 8
                .FK_for_Table_ID = If(ObjDirector Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, ObjDirector.NO_ID)
                .Tph_Contact_Type = Director_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_txt_phone_Country_prefix.Text
                .tph_number = Director_txt_phone_number.Text
                .tph_extension = Director_txt_phone_extension.Text
                .comments = Director_txt_phone_comments.Text
            End With

            ListPhoneDetailDirector.Add(objNewDetailPhone)
            BindDetailPhone(Store_DirectorPhone, ListPhoneDetailDirector)
            FormPanelDirectorPhone.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try


    End Sub
    Private Sub SaveAddDetailPhoneDirectorEmployer()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                ' update agam 21102020
                If ListPhoneDetailDirectorEmployer.Count = 0 Then
                    .PK_goAML_Ref_Phone = -1
                ElseIf ListPhoneDetailDirectorEmployer.Count > 0 And ListPhoneDetailDirectorEmployer.Min(Function(x) x.PK_goAML_Ref_Phone) > 0 Then
                    .PK_goAML_Ref_Phone = -1
                ElseIf ListPhoneDetailDirectorEmployer.Count > 0 And ListPhoneDetailDirectorEmployer.Min(Function(x) x.PK_goAML_Ref_Phone) < 0 Then
                    .PK_goAML_Ref_Phone = ListPhoneDetailDirectorEmployer.Min(Function(x) x.PK_goAML_Ref_Phone) - 1
                End If
                '.PK_goAML_Ref_Phone = ListPhoneDetailDirectorEmployer.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 4
                .FK_for_Table_ID = If(ObjDirector Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, ObjDirector.NO_ID)
                .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text
                .tph_number = Director_Emp_txt_phone_number.Text
                .tph_extension = Director_Emp_txt_phone_extension.Text
                .comments = Director_Emp_txt_phone_comments.Text
            End With

            ListPhoneDetailDirectorEmployer.Add(objNewDetailPhone)
            BindDetailPhone(Store_Director_Employer_Phone, ListPhoneDetailDirectorEmployer)
            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailIdentification()
        Try
            With ObjDetailIdentification
                .Type = CmbTypeIdentification.Text
                .Number = TxtNumber.Text
                If TxtIssueDate.Text <> DateTime.MinValue Then
                    .Issue_Date = TxtIssueDate.Text
                End If
                If TxtExpiryDate.Text <> DateTime.MinValue Then
                    .Expiry_Date = TxtExpiryDate.Text
                End If
                .Issued_By = TxtIssuedBy.Text
                .Issued_Country = CmbCountryIdentification.TextValue
                .Identification_Comment = TxtCommentIdentification.Text
            End With

            ObjDetailIdentification = Nothing

            BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
            FormPanelIdentification.Hidden = True
            WindowDetailIdentification.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailIdentificationDirector()
        Try
            With ObjDetailIdentificationDirector
                .Type = CmbTypeIdentificationDirector.Text
                .Number = TxtNumberDirector.Text
                If TxtIssueDateDirector.Text <> DateTime.MinValue Then
                    .Issue_Date = TxtIssueDateDirector.Text
                End If
                If TxtExpiryDateDirector.Text <> DateTime.MinValue Then
                    .Expiry_Date = TxtExpiryDateDirector.Text
                End If
                .Issued_By = TxtIssuedByDirector.Text
                .Issued_Country = CmbCountryIdentificationDirector.TextValue
                .Identification_Comment = TxtCommentIdentificationDirector.Text
            End With

            ObjDetailIdentificationDirector = Nothing

            'Daniel 2021 Jan 6
            'BindDetailIdentification(StoreIdentificationDirector, ListIdentificationDirector)
            BindDetailIdentification(StoreIdentificationDirectorDetail, ListIdentificationDirector)
            'end 2021 Jan 6
            FormPanelIdentification.Hidden = True
            'Daniel 2021 Jan 7
            'WindowDetailIdentification.Hidden = True
            WindowDetailDirectorIdentification.Hidden = True
            'end 2021 Jan 7
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhone()
        Try
            With ObjDetailPhone
                .Tph_Contact_Type = Cmbtph_contact_type.Text
                .Tph_Communication_Type = Cmbtph_communication_type.Text
                .tph_country_prefix = Txttph_country_prefix.Text
                .tph_number = Txttph_number.Text
                .tph_extension = Txttph_extension.Text
                .comments = TxtcommentsPhone.Text
            End With

            ObjDetailPhone = Nothing
            If ObjWIC.FK_Customer_Type_ID = 1 Then
                BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
            Else
                BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
            End If

            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhoneEmployer()
        Try
            With ObjDetailPhoneEmployer
                .Tph_Contact_Type = Cmbtph_contact_typeEmployer.Text
                .Tph_Communication_Type = Cmbtph_communication_typeEmployer.Text
                .tph_country_prefix = Txttph_country_prefixEmployer.Text
                .tph_number = Txttph_numberEmployer.Text
                .tph_extension = Txttph_extensionEmployer.Text
                .comments = TxtcommentsPhoneEmployer.Text
            End With

            ObjDetailPhoneEmployer = Nothing
            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)

            FormPanelPhoneEmployerDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhoneDirector()
        Try
            With ObjDetailPhoneDirector
                .Tph_Contact_Type = Director_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_txt_phone_Country_prefix.Text
                .tph_number = Director_txt_phone_number.Text
                .tph_extension = Director_txt_phone_extension.Text
                .comments = Director_txt_phone_comments.Text
            End With

            ObjDetailPhoneDirector = Nothing
            BindDetailPhone(Store_DirectorPhone, ListPhoneDetailDirector)
            FormPanelDirectorPhone.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhoneDirectorEmployer()
        Try
            With ObjDetailPhoneDirectorEmployer
                .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text
                .tph_number = Director_Emp_txt_phone_number.Text
                .tph_extension = Director_Emp_txt_phone_extension.Text
                .comments = Director_Emp_txt_phone_comments.Text
            End With

            ObjDetailPhoneDirectorEmployer = Nothing
            BindDetailPhone(Store_Director_Employer_Phone, ListPhoneDetailDirectorEmployer)
            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If ObjDetailAddress Is Nothing Then
                SaveAddDetailAddress()
            Else
                Dim status As Boolean = True
                Dim ObjDetailAddressNew As New goAML_Ref_Address

                With ObjDetailAddressNew
                    .Address_Type = CmbAddress_type.Text
                    .Address = TxtAddress.Text
                    .Town = TxtTown.Text
                    .City = TxtCity.Text
                    .Zip = TxtZip.Text
                    .Country_Code = Cmbcountry_code.TextValue
                    .State = TxtState.Text
                    .Comments = TxtcommentsAddress.Text
                End With

                With ObjDetailAddress
                    If .Address_Type <> ObjDetailAddressNew.Address_Type Then
                        status = False
                    End If
                    If .Address <> ObjDetailAddressNew.Address Then
                        status = False
                    End If
                    If .Town <> ObjDetailAddressNew.Town Then
                        status = False
                    End If
                    If .City <> ObjDetailAddressNew.City Then
                        status = False
                    End If
                    If .Zip <> ObjDetailAddressNew.Zip Then
                        status = False
                    End If
                    If .Country_Code <> ObjDetailAddressNew.Country_Code Then
                        status = False
                    End If
                    If .State <> ObjDetailAddressNew.State Then
                        status = False
                    End If
                    If .Comments <> ObjDetailAddressNew.Comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailAddress()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailAddressEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If ObjDetailAddressEmployer Is Nothing Then
                SaveAddDetailAddressEmployer()
            Else
                Dim status As Boolean = True
                Dim ObjDetailAddressEmployerNew As New goAML_Ref_Address

                With ObjDetailAddressEmployerNew
                    .Address_Type = CmbAddress_typeEmployer.Text
                    .Address = TxtAddressEmployer.Text
                    .Town = TxtTownEmployer.Text
                    .City = TxtCityEmployer.Text
                    .Zip = TxtZipEmployer.Text
                    .Country_Code = Cmbcountry_codeEmployer.TextValue
                    .State = TxtStateEmployer.Text
                    .Comments = TxtcommentsAddressEmployer.Text
                End With

                With ObjDetailAddressEmployer
                    If .Address_Type <> ObjDetailAddressEmployerNew.Address_Type Then
                        status = False
                    End If
                    If .Address <> ObjDetailAddressEmployerNew.Address Then
                        status = False
                    End If
                    If .Town <> ObjDetailAddressEmployerNew.Town Then
                        status = False
                    End If
                    If .City <> ObjDetailAddressEmployerNew.City Then
                        status = False
                    End If
                    If .Zip <> ObjDetailAddressEmployerNew.Zip Then
                        status = False
                    End If
                    If .Country_Code <> ObjDetailAddressEmployerNew.Country_Code Then
                        status = False
                    End If
                    If .State <> ObjDetailAddressEmployerNew.State Then
                        status = False
                    End If
                    If .Comments <> ObjDetailAddressEmployerNew.Comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailAddressEmployer()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorPhones_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If ObjDetailPhoneDirector Is Nothing Then
                SaveAddDetailPhoneDirector()
            Else
                Dim status As Boolean = True
                Dim ObjDetailPhoneDirectorNew As New goAML_Ref_Phone

                With ObjDetailPhoneDirectorNew
                    .Tph_Contact_Type = Director_cb_phone_Contact_Type.Text
                    .Tph_Communication_Type = Director_cb_phone_Communication_Type.Text
                    .tph_country_prefix = Director_txt_phone_Country_prefix.Text
                    .tph_number = Director_txt_phone_number.Text
                    .tph_extension = Director_txt_phone_extension.Text
                    .comments = Director_txt_phone_comments.Text
                End With

                With ObjDetailPhoneDirector
                    If .Tph_Contact_Type <> ObjDetailPhoneDirectorNew.Tph_Contact_Type Then
                        status = False
                    End If
                    If .Tph_Communication_Type <> ObjDetailPhoneDirectorNew.Tph_Communication_Type Then
                        status = False
                    End If
                    If .tph_country_prefix <> ObjDetailPhoneDirectorNew.tph_country_prefix Then
                        status = False
                    End If
                    If .tph_number <> ObjDetailPhoneDirectorNew.tph_number Then
                        status = False
                    End If
                    If .tph_extension <> ObjDetailPhoneDirectorNew.tph_extension Then
                        status = False
                    End If
                    If .comments <> ObjDetailPhoneDirectorNew.comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailPhoneDirector()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveAddDetailAddress()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                ' update agam 21102020
                If ListAddressDetail.Count = 0 Then
                    .PK_Customer_Address_ID = -1
                ElseIf ListAddressDetail.Count > 0 And ListAddressDetail.Min(Function(x) x.PK_Customer_Address_ID) > 0 Then
                    .PK_Customer_Address_ID = -1
                ElseIf ListAddressDetail.Count > 0 And ListAddressDetail.Min(Function(x) x.PK_Customer_Address_ID) < 0 Then
                    .PK_Customer_Address_ID = ListAddressDetail.Min(Function(x) x.PK_Customer_Address_ID) - 1
                End If
                '.PK_Customer_Address_ID = ListAddressDetail.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 3
                .FK_To_Table_ID = ObjWIC.PK_Customer_ID
                .Address_Type = CmbAddress_type.Text
                .Address = TxtAddress.Text
                .Town = TxtTown.Text
                .City = TxtCity.Text
                .Zip = TxtZip.Text
                .Country_Code = Cmbcountry_code.TextValue
                .State = TxtState.Text
                .Comments = TxtcommentsAddress.Text
            End With

            ListAddressDetail.Add(objNewDetailAddress)
            If ObjWIC.FK_Customer_Type_ID = 1 Then
                BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
            Else
                BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
            End If

            FormPanelAddressDetail.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailAddressEmployer()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                ' update agam 21102020
                If ListAddressEmployerDetail.Count = 0 Then
                    .PK_Customer_Address_ID = -1
                ElseIf ListAddressEmployerDetail.Count > 0 And ListAddressEmployerDetail.Min(Function(x) x.PK_Customer_Address_ID) > 0 Then
                    .PK_Customer_Address_ID = -1
                ElseIf ListAddressEmployerDetail.Count > 0 And ListAddressEmployerDetail.Min(Function(x) x.PK_Customer_Address_ID) < 0 Then
                    .PK_Customer_Address_ID = ListAddressEmployerDetail.Min(Function(x) x.PK_Customer_Address_ID) - 1
                End If
                '.PK_Customer_Address_ID = ListAddressEmployerDetail.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 10
                .FK_To_Table_ID = ObjWIC.PK_Customer_ID
                .Address_Type = CmbAddress_typeEmployer.Text
                .Address = TxtAddressEmployer.Text
                .Town = TxtTownEmployer.Text
                .City = TxtCityEmployer.Text
                .Zip = TxtZipEmployer.Text
                .Country_Code = Cmbcountry_codeEmployer.TextValue
                .State = TxtStateEmployer.Text
                .Comments = TxtcommentsAddressEmployer.Text
            End With

            ListAddressEmployerDetail.Add(objNewDetailAddress)
            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)

            FormPanelAddressDetailEmployer.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailAddressDirector()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                ' update agam 21102020
                If ListAddressDetailDirector.Count = 0 Then
                    .PK_Customer_Address_ID = -1
                ElseIf ListAddressDetailDirector.Count > 0 And ListAddressDetailDirector.Min(Function(x) x.PK_Customer_Address_ID) > 0 Then
                    .PK_Customer_Address_ID = -1
                ElseIf ListAddressDetailDirector.Count > 0 And ListAddressDetailDirector.Min(Function(x) x.PK_Customer_Address_ID) < 0 Then
                    .PK_Customer_Address_ID = ListAddressDetailDirector.Min(Function(x) x.PK_Customer_Address_ID) - 1
                End If
                '.PK_Customer_Address_ID = ListAddressDetailDirector.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 8
                .FK_To_Table_ID = If(ObjDirector Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, ObjDirector.NO_ID)
                .Address_Type = Director_cmb_kategoriaddress.Text
                .Address = Director_Address.Text
                .Town = Director_Town.Text
                .City = Director_City.Text
                .Zip = Director_Zip.Text
                .Country_Code = Director_cmb_kodenegara.TextValue
                .State = Director_State.Text
                .Comments = Director_Comment_Address.Text
            End With

            ListAddressDetailDirector.Add(objNewDetailAddress)
            BindDetailAddress(Store_DirectorAddress, ListAddressDetailDirector)
            FP_Address_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailAddressDirectorEmployer()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                ' update agam 21102020
                If ListAddressDetailDirectorEmployer.Count = 0 Then
                    .PK_Customer_Address_ID = -1
                ElseIf ListAddressDetailDirectorEmployer.Count > 0 And ListAddressDetailDirectorEmployer.Min(Function(x) x.PK_Customer_Address_ID) > 0 Then
                    .PK_Customer_Address_ID = -1
                ElseIf ListAddressDetailDirectorEmployer.Count > 0 And ListAddressDetailDirectorEmployer.Min(Function(x) x.PK_Customer_Address_ID) < 0 Then
                    .PK_Customer_Address_ID = ListAddressDetailDirector.Min(Function(x) x.PK_Customer_Address_ID) - 1
                End If
                ' .PK_Customer_Address_ID = ListAddressDetailDirectorEmployer.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 4
                .FK_To_Table_ID = If(ObjDirector Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, ObjDirector.NO_ID)
                .Address_Type = Director_cmb_emp_kategoriaddress.Text
                .Address = Director_emp_Address.Text
                .Town = Director_emp_Town.Text
                .City = Director_emp_City.Text
                .Zip = Director_emp_Zip.Text
                .Country_Code = Director_cmb_emp_kodenegara.TextValue
                .State = Director_emp_State.Text
                .Comments = Director_emp_Comment_Address.Text
            End With

            ListAddressDetailDirectorEmployer.Add(objNewDetailAddress)
            BindDetailAddress(Store_Director_Employer_Address, ListAddressDetailDirectorEmployer)
            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveEditDetailAddress()
        Try
            With ObjDetailAddress
                .Address_Type = CmbAddress_type.Text
                .Address = TxtAddress.Text
                .Town = TxtTown.Text
                .City = TxtCity.Text
                .Zip = TxtZip.Text
                .Country_Code = Cmbcountry_code.TextValue
                .State = TxtState.Text
                .Comments = TxtcommentsAddress.Text
            End With

            ObjDetailAddress = Nothing
            If ObjWIC.FK_Customer_Type_ID = 1 Then
                BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
            Else
                BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
            End If

            FormPanelAddressDetail.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailAddressEmployer()
        Try
            With ObjDetailAddressEmployer
                .Address_Type = CmbAddress_typeEmployer.Text
                .Address = TxtAddressEmployer.Text
                .Town = TxtTownEmployer.Text
                .City = TxtCityEmployer.Text
                .Zip = TxtZipEmployer.Text
                .Country_Code = Cmbcountry_codeEmployer.TextValue
                .State = TxtStateEmployer.Text
                .Comments = TxtcommentsAddressEmployer.Text
            End With

            ObjDetailAddressEmployer = Nothing
            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)

            FormPanelAddressDetailEmployer.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailAddressDirector()
        Try
            With ObjDetailAddressDirector
                .Address_Type = Director_cmb_kategoriaddress.Text
                .Address = Director_Address.Text
                .Town = Director_Town.Text
                .City = Director_City.Text
                .Zip = Director_Zip.Text
                .Country_Code = Director_cmb_kodenegara.TextValue
                .State = Director_State.Text
                .Comments = Director_Comment_Address.Text
            End With

            ObjDetailAddressDirector = Nothing
            BindDetailAddress(Store_DirectorAddress, ListAddressDetailDirector)
            FP_Address_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailAddressDirectorEmployer()
        Try
            With ObjDetailAddressDirectorEmployer
                .Address_Type = Director_cmb_emp_kategoriaddress.Text
                .Address = Director_emp_Address.Text
                .Town = Director_emp_Town.Text
                .City = Director_emp_City.Text
                .Zip = Director_emp_Zip.Text
                .Country_Code = Director_cmb_emp_kodenegara.TextValue
                .State = Director_emp_State.Text
                .Comments = Director_emp_Comment_Address.Text
            End With

            ObjDetailAddressDirectorEmployer = Nothing
            BindDetailAddress(Store_Director_Employer_Address, ListAddressDetailDirectorEmployer)
            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveCustomerDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If ObjDirector Is Nothing Then
                SaveAddDetailDirector()
            Else
                SaveEditDetailDirector()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveEditDetailDirector()
        Try
            'If cmb_Director_Gender.Value = "" Then
            '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
            'ElseIf cmb_peran.Value = "" Then
            '    Throw New Exception("Role Tidak boleh kosong")
            'ElseIf txt_Director_Last_Name.Text = "" Then
            '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
            'ElseIf txt_Director_BirthDate.Text = "1/1/0001 12:00:00 AM" Or txt_Director_BirthDate.Value = DateTime.MinValue Then
            '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
            'ElseIf txt_Director_Birth_Place.Text = "" Then
            '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
            'ElseIf cmb_Director_Nationality1.TextValue = "" Then
            '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
            'ElseIf cmb_Director_Residence.TextValue = "" Then
            '    Throw New Exception("Negara Domisili Tidak boleh kosong")
            'ElseIf txt_Director_Occupation.Text = "" Then
            '    Throw New Exception("Pekerjaan Tidak boleh kosong")
            'ElseIf txt_Director_Source_of_Wealth.Text = "" Then
            '    Throw New Exception("Sumber Dana Tidak boleh kosong")
            'ElseIf txt_Director_SSN.Text.Length <> 16 And txt_Director_SSN.Text <> "" Then
            '    Throw New Exception("NIK Must 16 Digit")
            'End If

            Dim status As Boolean = True
            Dim DirectorClass As New WICDirectorDataBLL
            Dim ListDirectorDetailOld As New List(Of goAML_Ref_Walk_In_Customer_Director)
            Dim DirectorOld As New goAML_Ref_Walk_In_Customer_Director
            Dim ListDirectorAddressOld As New List(Of goAML_Ref_Address)
            Dim ListDirectorAddressEmployerOld As New List(Of goAML_Ref_Address)
            Dim ListDirectorPhoneOld As New List(Of goAML_Ref_Phone)
            Dim ListDirectorPhoneEmployerOld As New List(Of goAML_Ref_Phone)
            Dim ListDirectorIdentificationOld As New List(Of goAML_Person_Identification)

            ListDirectorDetailOld = objgoAML_WIC.GetWICByPKIDDetailDirector(ObjWIC.PK_Customer_ID)

            With ObjDirector
                If cmb_peran.Text = "" Then
                    Throw New ApplicationException("Role wajib untuk diisi")
                End If
                .NO_ID = Hidden2.Value
                .Role = cmb_peran.Text
                .Title = txt_Director_Title.Text
                .Last_Name = txt_Director_Last_Name.Text
                .Gender = cmb_Director_Gender.Text
                If txt_Director_BirthDate.Text <> DateTime.MinValue Then
                    .BirthDate = txt_Director_BirthDate.Text
                End If
                .Birth_Place = txt_Director_Birth_Place.Text
                .Mothers_Name = txt_Director_Mothers_Name.Text
                .Alias = txt_Director_Alias.Text
                .ID_Number = txt_Director_ID_Number.Text
                .Passport_Number = txt_Director_Passport_Number.Text
                .Passport_Country = txt_Director_Passport_Country.TextValue
                .SSN = txt_Director_SSN.Text
                .Nationality1 = cmb_Director_Nationality1.TextValue
                .Nationality2 = cmb_Director_Nationality2.TextValue
                .Nationality3 = cmb_Director_Nationality3.TextValue
                .Residence = cmb_Director_Residence.TextValue
                .Email = txt_Director_Email.Text
                .Email2 = txt_Director_Email2.Text
                .Email3 = txt_Director_Email3.Text
                .Email4 = txt_Director_Email4.Text
                .Email5 = txt_Director_Email5.Text
                .Deceased = cb_Director_Deceased.Checked
                If cb_Director_Deceased.Checked Then
                    If txt_Director_Deceased_Date.Text <> DateTime.MinValue Then
                        .Deceased_Date = txt_Director_Deceased_Date.Text
                    End If
                End If
                .Tax_Reg_Number = cb_Director_Tax_Reg_Number.Checked
                .Tax_Number = txt_Director_Tax_Number.Text
                .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
                .Occupation = txt_Director_Occupation.Text
                .Comments = txt_Director_Comments.Text
                'Daniel 2021 Jan 5
                .Employer_Name = txt_Director_employer_name.Text
                'end 2021 Jan 5
            End With

            With ObjDirector
                DirectorOld = ListDirectorDetailOld.Where(Function(x) x.NO_ID = .NO_ID).FirstOrDefault

                If DirectorOld IsNot Nothing Then
                    If .Role <> DirectorOld.Role Then
                        status = False
                    End If
                    If .Gender <> DirectorOld.Gender Then
                        status = False
                    End If
                    If .Title <> DirectorOld.Title Then
                        status = False
                    End If
                    If .Last_Name <> DirectorOld.Last_Name Then
                        status = False
                    End If
                    If .BirthDate <> DirectorOld.BirthDate Then
                        status = False
                    End If
                    If .Birth_Place <> DirectorOld.Birth_Place Then
                        status = False
                    End If
                    If .Mothers_Name <> DirectorOld.Mothers_Name Then
                        status = False
                    End If
                    If .Alias <> DirectorOld.Alias Then
                        status = False
                    End If
                    If .SSN <> DirectorOld.SSN Then
                        status = False
                    End If
                    If .Passport_Number <> DirectorOld.Passport_Number Then
                        status = False
                    End If
                    If .Passport_Country <> DirectorOld.Passport_Country Then
                        status = False
                    End If
                    If .ID_Number <> DirectorOld.ID_Number Then
                        status = False
                    End If
                    If .Nationality1 <> DirectorOld.Nationality1 Then
                        status = False
                    End If
                    If .Nationality2 <> DirectorOld.Nationality2 Then
                        status = False
                    End If
                    If .Nationality3 <> DirectorOld.Nationality3 Then
                        status = False
                    End If
                    If .Residence <> DirectorOld.Residence Then
                        status = False
                    End If
                    If .Email <> DirectorOld.Email Then
                        status = False
                    End If
                    If .Email2 <> DirectorOld.Email2 Then
                        status = False
                    End If
                    If .Email3 <> DirectorOld.Email3 Then
                        status = False
                    End If
                    If .Email4 <> DirectorOld.Email4 Then
                        status = False
                    End If
                    If .Email5 <> DirectorOld.Email5 Then
                        status = False
                    End If
                    If .Occupation <> DirectorOld.Occupation Then
                        status = False
                    End If
                    If .Employer_Name <> DirectorOld.Employer_Name Then
                        status = False
                    End If
                    If .Deceased <> DirectorOld.Deceased Then
                        status = False
                    End If
                    If .Deceased_Date <> DirectorOld.Deceased_Date Then
                        status = False
                    End If
                    If .Tax_Number <> DirectorOld.Tax_Number Then
                        status = False
                    End If
                    If .Tax_Reg_Number <> DirectorOld.Tax_Reg_Number Then
                        status = False
                    End If
                    If .Source_of_Wealth <> DirectorOld.Source_of_Wealth Then
                        status = False
                    End If
                    If .Comments <> DirectorOld.Comments Then
                        status = False
                    End If
                Else
                    status = False
                End If

                If DirectorOld IsNot Nothing Then
                    ListDirectorAddressOld = objgoAML_WIC.GetWICByPKIDDetailDirectorAddress(DirectorOld.NO_ID)
                    ListDirectorAddressEmployerOld = objgoAML_WIC.GetWICByPKIDDetailDirectorAddressEmployer(DirectorOld.NO_ID)
                    ListDirectorPhoneOld = objgoAML_WIC.GetWICByPKIDDetailDirectorPhone(DirectorOld.NO_ID)
                    ListDirectorPhoneEmployerOld = objgoAML_WIC.GetWICByPKIDDetailDirectorPhoneEmployer(DirectorOld.NO_ID)
                    ListDirectorIdentificationOld = objgoAML_WIC.GetWICByPKIDDetailDirectorIdentification(DirectorOld.NO_ID)
                End If

                If ListAddressDetailDirector.Count = ListDirectorAddressOld.Count Then
                    Dim Address As New goAML_Ref_Address
                    For Each item In ListAddressDetailDirector
                        Address = ListDirectorAddressOld.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                        If Address.FK_Ref_Detail_Of IsNot Nothing Then
                            With Address
                                If .Address_Type <> item.Address_Type Then
                                    status = False
                                End If
                                If .Address <> item.Address Then
                                    status = False
                                End If
                                If .Town <> item.Town Then
                                    status = False
                                End If
                                If .City <> item.City Then
                                    status = False
                                End If
                                If .Zip <> item.Zip Then
                                    status = False
                                End If
                                If .Country_Code <> item.Country_Code Then
                                    status = False
                                End If
                                If .State <> item.State Then
                                    status = False
                                End If
                                If .Comments <> item.Comments Then
                                    status = False
                                End If
                            End With
                        Else
                            status = False
                        End If
                        Address = New goAML_Ref_Address
                    Next
                Else
                    status = False
                End If

                If ListAddressDetailDirectorEmployer.Count = ListDirectorAddressEmployerOld.Count Then
                    Dim AddressEmp As New goAML_Ref_Address
                    For Each item In ListAddressDetailDirectorEmployer
                        AddressEmp = ListDirectorAddressEmployerOld.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                        If AddressEmp.FK_Ref_Detail_Of IsNot Nothing Then
                            With AddressEmp
                                If .Address_Type <> item.Address_Type Then
                                    status = False
                                End If
                                If .Address <> item.Address Then
                                    status = False
                                End If
                                If .Town <> item.Town Then
                                    status = False
                                End If
                                If .City <> item.City Then
                                    status = False
                                End If
                                If .Zip <> item.Zip Then
                                    status = False
                                End If
                                If .Country_Code <> item.Country_Code Then
                                    status = False
                                End If
                                If .State <> item.State Then
                                    status = False
                                End If
                                If .Comments <> item.Comments Then
                                    status = False
                                End If
                            End With
                        Else
                            status = False
                        End If
                        AddressEmp = New goAML_Ref_Address
                    Next
                Else
                    status = False
                End If

                If ListPhoneDetailDirector.Count = ListDirectorPhoneOld.Count Then
                    Dim phone As New goAML_Ref_Phone
                    For Each item In ListPhoneDetailDirector
                        phone = ListDirectorPhoneOld.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                        If phone.FK_Ref_Detail_Of IsNot Nothing Then
                            With phone
                                If .Tph_Contact_Type <> item.Tph_Contact_Type Then
                                    status = False
                                End If
                                If .Tph_Communication_Type <> item.Tph_Communication_Type Then
                                    status = False
                                End If
                                If .tph_country_prefix <> item.tph_country_prefix Then
                                    status = False
                                End If
                                If .tph_number <> item.tph_number Then
                                    status = False
                                End If
                                If .tph_extension <> item.tph_extension Then
                                    status = False
                                End If
                                If .comments <> item.comments Then
                                    status = False
                                End If
                            End With
                        Else
                            status = False
                        End If
                        phone = New goAML_Ref_Phone
                    Next
                Else
                    status = False
                End If

                If ListPhoneDetailDirectorEmployer.Count = ListDirectorPhoneEmployerOld.Count Then
                    Dim phoneEmp As New goAML_Ref_Phone
                    For Each item In ListPhoneDetailDirectorEmployer
                        phoneEmp = ListDirectorPhoneEmployerOld.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                        If phoneEmp.FK_Ref_Detail_Of IsNot Nothing Then
                            With phoneEmp
                                If .Tph_Contact_Type <> item.Tph_Contact_Type Then
                                    status = False
                                End If
                                If .Tph_Communication_Type <> item.Tph_Communication_Type Then
                                    status = False
                                End If
                                If .tph_country_prefix <> item.tph_country_prefix Then
                                    status = False
                                End If
                                If .tph_number <> item.tph_number Then
                                    status = False
                                End If
                                If .tph_extension <> item.tph_extension Then
                                    status = False
                                End If
                                If .comments <> item.comments Then
                                    status = False
                                End If
                            End With
                        Else
                            status = False
                        End If
                        phoneEmp = New goAML_Ref_Phone
                    Next
                Else
                    status = False
                End If

                If ListIdentificationDirector.Count = ListDirectorIdentificationOld.Count Then
                    Dim Identification As New goAML_Person_Identification
                    For Each item In ListIdentificationDirector
                        Identification = ListDirectorIdentificationOld.Where(Function(x) x.PK_Person_Identification_ID = item.PK_Person_Identification_ID).FirstOrDefault
                        If Identification.FK_Person_ID <> Nothing Then
                            With Identification
                                If .Type <> item.Type Then
                                    status = False
                                End If
                                If .Number <> item.Number Then
                                    status = False
                                End If
                                If .Issue_Date <> item.Issue_Date Then
                                    status = False
                                End If
                                If .Expiry_Date <> item.Expiry_Date Then
                                    status = False
                                End If
                                If .Issued_By <> item.Issued_By Then
                                    status = False
                                End If
                                If .Issued_Country <> item.Issued_Country Then
                                    status = False
                                End If
                                If .Identification_Comment <> item.Identification_Comment Then
                                    status = False
                                End If
                            End With
                        Else
                            status = False
                        End If
                        Identification = New goAML_Person_Identification
                    Next
                Else
                    status = False
                End If
            End With

            If status Then
                Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
            End If

            With DirectorClass
                .ObjDirector = ObjDirector
                .ListDirectorAddress = ListAddressDetailDirector
                .ListDirectorAddressEmployer = ListAddressDetailDirectorEmployer
                .ListDirectorPhone = ListPhoneDetailDirector
                .ListDirectorPhoneEmployer = ListPhoneDetailDirectorEmployer
                .ListDirectorIdentification = ListIdentificationDirector
            End With
            ListDirectorDetail.Remove(ListDirectorDetail.Where(Function(x) x.NO_ID = DirectorClass.ObjDirector.NO_ID).FirstOrDefault)
            ListDirectorDetailClass.Remove(ListDirectorDetailClass.Where(Function(x) x.ObjDirector.NO_ID = DirectorClass.ObjDirector.NO_ID).FirstOrDefault)
            ListDirectorDetail.Add(DirectorClass.ObjDirector)
            ListDirectorDetailClass.Add(DirectorClass)

            ObjDirector = Nothing
            BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
            FP_Director.Hidden = True
            WindowDetailDirector.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveAddDetailDirector()
        Try
            'If cmb_Director_Gender.Value = "" Then
            '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
            'ElseIf cmb_peran.Value = "" Then
            '    Throw New Exception("Role Tidak boleh kosong")
            'ElseIf txt_Director_Last_Name.Text = "" Then
            '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
            'ElseIf txt_Director_BirthDate.Text = "1/1/0001 12:00:00 AM" Or txt_Director_BirthDate.Value = DateTime.MinValue Then
            '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
            'ElseIf txt_Director_Birth_Place.Text = "" Then
            '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
            'ElseIf cmb_Director_Nationality1.TextValue = "" Then
            '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
            'ElseIf cmb_Director_Residence.TextValue = "" Then
            '    Throw New Exception("Negara Domisili Tidak boleh kosong")
            'ElseIf txt_Director_Occupation.Text = "" Then
            '    Throw New Exception("Pekerjaan Tidak boleh kosong")
            'ElseIf txt_Director_Source_of_Wealth.Text = "" Then
            '    Throw New Exception("Sumber Dana Tidak boleh kosong")
            'ElseIf txt_Director_SSN.Text.Length <> 16 And txt_Director_SSN.Text <> "" Then
            '    Throw New Exception("NIK Must 16 Digit")
            'End If
            'If cmb_Director_Gender.Value = "" Then
            '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
            'ElseIf cmb_peran.Value = "" Then
            '    Throw New Exception("Role Tidak boleh kosong")
            'ElseIf txt_Director_Last_Name.Text = "" Then
            '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
            'ElseIf txt_Director_BirthDate.Text = "1/1/0001 12:00:00 AM" Or txt_Director_BirthDate.Value = DateTime.MinValue Then
            '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
            'ElseIf txt_Director_Birth_Place.Text = "" Then
            '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
            'ElseIf cmb_Director_Nationality1.TextValue = "" Then
            '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
            'ElseIf cmb_Director_Residence.TextValue = "" Then
            '    Throw New Exception("Negara Domisili Tidak boleh kosong")
            'ElseIf ListPhoneDetailDirector.Count <= 0 Then
            '    Throw New Exception("Informasi Telepon Director Tidak boleh kosong")
            'ElseIf ListAddressDetailDirector.Count <= 0 Then
            '    Throw New Exception("Informasi Alamat Director Tidak boleh kosong")
            'ElseIf txt_Director_Occupation.Text = "" Then
            '    Throw New Exception("Pekerjaan Tidak boleh kosong")
            'ElseIf txt_Director_Source_of_Wealth.Text = "" Then
            '    Throw New Exception("Sumber Dana Tidak boleh kosong")
            If cmb_peran.Value = "" Then
                Throw New Exception("Role Tidak boleh kosong")
            ElseIf txt_Director_Last_Name.Text = "" Then
                Throw New Exception("Nama Lengkap Tidak boleh kosong")
            Else
                If txt_Director_SSN.Text.Length <> 16 And txt_Director_SSN.Text.Length > 0 Then
                    Throw New Exception("NIK Must 16 Digit")
                Else
                    Dim DirectorClass As New WICDirectorDataBLL
                    Dim objSaveDirector As New goAML_Ref_Walk_In_Customer_Director
                    With objSaveDirector
                        .NO_ID = -1
                        .Role = cmb_peran.Text
                        .Title = txt_Director_Title.Text
                        .Last_Name = txt_Director_Last_Name.Text
                        .Gender = cmb_Director_Gender.Text
                        If txt_Director_BirthDate.Text <> DateTime.MinValue Then
                            .BirthDate = txt_Director_BirthDate.Text
                        End If
                        .Birth_Place = txt_Director_Birth_Place.Text
                        .Mothers_Name = txt_Director_Mothers_Name.Text
                        .Alias = txt_Director_Alias.Text
                        .ID_Number = txt_Director_ID_Number.Text
                        .Passport_Number = txt_Director_Passport_Number.Text
                        .Passport_Country = txt_Director_Passport_Country.TextValue
                        .SSN = txt_Director_SSN.Text
                        .Nationality1 = cmb_Director_Nationality1.TextValue
                        .Nationality2 = cmb_Director_Nationality2.TextValue
                        .Nationality3 = cmb_Director_Nationality3.TextValue
                        .Residence = cmb_Director_Residence.TextValue
                        .Email = txt_Director_Email.Text
                        .Email2 = txt_Director_Email2.Text
                        .Email3 = txt_Director_Email3.Text
                        .Email4 = txt_Director_Email4.Text
                        .Email5 = txt_Director_Email5.Text
                        .Deceased = cb_Director_Deceased.Checked
                        If cb_Director_Deceased.Checked Then
                            .Deceased_Date = txt_Director_Deceased_Date.Text
                        End If
                        .Tax_Reg_Number = cb_Director_Tax_Reg_Number.Checked
                        .Tax_Number = txt_Director_Tax_Number.Text
                        .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
                        .Occupation = txt_Director_Occupation.Text
                        .Comments = txt_Director_Comments.Text
                        'Daniel 2021 Jan 5
                        .Employer_Name = txt_Director_employer_name.Text
                        'end 2021 Jan 5
                    End With

                    With DirectorClass
                        .ObjDirector = objSaveDirector
                        .ListDirectorAddress = ListAddressDetailDirector
                        .ListDirectorAddressEmployer = ListAddressDetailDirectorEmployer
                        .ListDirectorPhone = ListPhoneDetailDirector
                        .ListDirectorPhoneEmployer = ListPhoneDetailDirectorEmployer
                        .ListDirectorIdentification = ListIdentificationDirector
                    End With

                    ListDirectorDetail.Add(DirectorClass.ObjDirector)
                    ListDirectorDetailClass.Add(DirectorClass)

                    BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
                    FP_Director.Hidden = True
                    WindowDetailDirector.Hidden = True
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Status As Boolean = True
            If FKCustomer.Text = 1 Then
                'If CmbINDV_Gender.Value = "" Then
                '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
                'If TxtINDV_last_name.Text = "" Then
                '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
                'ElseIf DateINDV_Birthdate.Text = "1/1/0001 12:00:00 AM" Or DateINDV_Birthdate.Value = DateTime.MinValue Then
                '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
                'ElseIf TxtINDV_Birth_place.Text = "" Then
                '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
                'ElseIf CmbINDV_Nationality1.TextValue = "" Then
                '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
                'ElseIf CmbINDV_residence.TextValue = "" Then
                '    Throw New Exception("Negara Domisili Tidak boleh kosong")
                'ElseIf TxtINDV_Occupation.Text = "" Then
                '    Throw New Exception("Pekerjaan Tidak boleh kosong")
                'ElseIf TxtINDV_Source_Of_Wealth.Text = "" Then
                '    Throw New Exception("Sumber Dana Tidak boleh kosong")
                'daniel 14 jan 2021
                'If CmbINDV_Gender.Value = "" Or TxtINDV_last_name.Text = "" Or TxtINDV_SSN.Text = "" Or DateINDV_Birthdate.Value = DateTime.MinValue Or TxtINDV_Occupation.Text = "" Or TxtINDV_Source_Of_Wealth.Text = "" Then
                '    Throw New Exception("Jenis Kelamin, Nama, NIK, Tanggal Lahir, pekerjaan, Sumber Penghasilan Tidak boleh kosong")
                'If CmbINDV_Gender.Value = "" Or TxtINDV_last_name.Text = "" Or DateINDV_Birthdate.Value = DateTime.MinValue Or TxtINDV_Occupation.Text = "" Or TxtINDV_Source_Of_Wealth.Text = "" Then
                '    Throw New Exception("Jenis Kelamin, Nama, NIK, Tanggal Lahir, pekerjaan, Sumber Penghasilan Tidak boleh kosong")
                'end 14 jan 2021
                'If CmbINDV_Gender.Value = "" Then
                '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
                'ElseIf TxtINDV_last_name.Text = "" Then
                '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
                'ElseIf DateINDV_Birthdate.Text = "1/1/0001 12:00:00 AM" Then
                '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
                'ElseIf TxtINDV_Birth_place.Text = "" Then
                '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
                'ElseIf CmbINDV_Nationality1.TextValue = "" Then
                '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
                'ElseIf CmbINDV_residence.TextValue = "" Then
                '    Throw New Exception("Negara Domisili Tidak boleh kosong")
                'ElseIf ListPhoneDetail.Count <= 0 Then
                '    Throw New Exception("Informasi Telepon Tidak boleh kosong")
                'ElseIf ListAddressDetail.Count <= 0 Then
                '    Throw New Exception("Informasi Alamat Tidak boleh kosong")
                'ElseIf TxtINDV_Occupation.Text = "" Then
                '    Throw New Exception("Pekerjaan Tidak boleh kosong")
                'ElseIf TxtINDV_Source_Of_Wealth.Text = "" Then
                '    Throw New Exception("Sumber Dana Tidak boleh kosong")
                If TxtINDV_last_name.Text = "" Then
                    Throw New Exception("Nama Lengkap Tidak boleh kosong")
                Else
                    If TxtINDV_SSN.Text.Length <> 16 And TxtINDV_SSN.Text.Length > 0 Then
                        Throw New Exception("NIK Must 16 Digit")
                    Else
                        Dim objSaveOld As New goAML_Ref_WIC
                        Dim ListAddressDetailOld As New List(Of goAML_Ref_Address)
                        Dim ListPhoneDetailOld As New List(Of goAML_Ref_Phone)
                        Dim ListAddressEmployerDetailOld As New List(Of goAML_Ref_Address)
                        Dim ListPhoneEmployerDetailOld As New List(Of goAML_Ref_Phone)
                        Dim ListIdentificationDetailOld As New List(Of goAML_Person_Identification)

                        objSaveOld = ObjWIC
                        ListAddressDetailOld = objgoAML_WIC.GetWICByPKIDDetailAddress(objSaveOld.PK_Customer_ID)
                        ListPhoneDetailOld = objgoAML_WIC.GetWICByPKIDDetailPhone(objSaveOld.PK_Customer_ID)
                        ListAddressEmployerDetailOld = objgoAML_WIC.GetWICByPKIDDetailEmployerAddress(objSaveOld.PK_Customer_ID)
                        ListPhoneEmployerDetailOld = objgoAML_WIC.GetWICByPKIDDetailEmployerPhone(objSaveOld.PK_Customer_ID)
                        ListIdentificationDetailOld = objgoAML_WIC.GetWICByPKIDDetailIdentification(objSaveOld.PK_Customer_ID)
                        Dim objSave As New goAML_Ref_WIC

                        With objSave
                            .PK_Customer_ID = PKWIC.Text
                            .isUpdateFromDataSource = CheckboxIsUpdateFromDataSource.Checked
                            .WIC_No = WICNo.Text
                            .INDV_Title = TxtINDV_Title.Text
                            .INDV_Last_Name = TxtINDV_last_name.Text
                            .INDV_Gender = CmbINDV_Gender.Text
                            'agam 02112020
                            If Not DateINDV_Birthdate.SelectedDate = DateTime.MinValue Then
                                .INDV_BirthDate = DateINDV_Birthdate.SelectedDate
                            End If
                            .INDV_Birth_Place = TxtINDV_Birth_place.Text
                            .INDV_Mothers_Name = TxtINDV_Mothers_name.Text
                            .INDV_Alias = TxtINDV_Alias.Text
                            .INDV_SSN = TxtINDV_SSN.Text
                            .INDV_Passport_Number = TxtINDV_Passport_number.Text
                            'Agam 20102020
                            '.INDV_Passport_Country = TxtINDV_Passport_country.Text
                            .INDV_Passport_Country = TxtINDV_Passport_country.TextValue
                            .INDV_ID_Number = TxtINDV_ID_Number.Value
                            .INDV_Nationality1 = CmbINDV_Nationality1.TextValue
                            .INDV_Nationality2 = CmbINDV_Nationality2.TextValue
                            .INDV_Nationality3 = CmbINDV_Nationality3.TextValue
                            .INDV_Residence = CmbINDV_residence.TextValue
                            .INDV_Email = TxtINDV_Email.Text
                            .INDV_Email2 = TxtINDV_Email2.Text
                            .INDV_Email3 = TxtINDV_Email3.Text
                            .INDV_Email4 = TxtINDV_Email4.Text
                            .INDV_Email5 = TxtINDV_Email5.Text
                            .INDV_Occupation = TxtINDV_Occupation.Text
                            .INDV_Employer_Name = TxtINDV_employer_name.Text
                            .INDV_SumberDana = TxtINDV_Source_Of_Wealth.Text
                            .INDV_Tax_Number = TxtINDV_Tax_Number.Text
                            .INDV_Tax_Reg_Number = CbINDV_Tax_Reg_Number.Checked
                            .INDV_Deceased = CbINDV_Deceased.Checked
                            If CbINDV_Deceased.Checked Then
                                If DateINDV_Deceased_Date.Text <> DateTime.MinValue Then
                                    .INDV_Deceased_Date = DateINDV_Deceased_Date.Text
                                End If
                            End If
                            .INDV_Comment = TxtINDV_Comments.Text
                            If DateCorp_incorporation_date.RawValue <> DateTime.MinValue Then
                                .Corp_Incorporation_Date = DateCorp_incorporation_date.RawValue
                            End If
                            If DateCorp_date_business_closed.RawValue <> DateTime.MinValue Then
                                .Corp_Date_Business_Closed = DateCorp_date_business_closed.RawValue
                            End If
                            .FK_Customer_Type_ID = 1
                            .Active = True
                            .LastUpdateBy = Common.SessionCurrentUser.UserID
                            .LastUpdateDate = DateTime.Now
                        End With

                        'Cek Perubahan Person
                        With objSave
                            If .isUpdateFromDataSource <> objSaveOld.isUpdateFromDataSource Then
                                Status = False
                            End If
                            If .INDV_Title <> objSaveOld.INDV_Title Then
                                Status = False
                            End If
                            If .INDV_Last_Name <> objSaveOld.INDV_Last_Name Then
                                Status = False
                            End If
                            If .INDV_Gender <> objSaveOld.INDV_Gender Then
                                Status = False
                            End If
                            If .INDV_BirthDate <> objSaveOld.INDV_BirthDate Then
                                Status = False
                            End If
                            If .INDV_Birth_Place <> objSaveOld.INDV_Birth_Place Then
                                Status = False
                            End If
                            If .INDV_Mothers_Name <> objSaveOld.INDV_Mothers_Name Then
                                Status = False
                            End If
                            If .INDV_Alias <> objSaveOld.INDV_Alias Then
                                Status = False
                            End If
                            If .INDV_SSN <> objSaveOld.INDV_SSN Then
                                Status = False
                            End If
                            If .INDV_Passport_Number <> objSaveOld.INDV_Passport_Number Then
                                Status = False
                            End If
                            If .INDV_Passport_Country <> objSaveOld.INDV_Passport_Country Then
                                Status = False
                            End If
                            If .INDV_ID_Number <> objSaveOld.INDV_ID_Number Then
                                Status = False
                            End If
                            If .INDV_Nationality1 <> objSaveOld.INDV_Nationality1 Then
                                Status = False
                            End If
                            If .INDV_Nationality2 <> objSaveOld.INDV_Nationality2 Then
                                Status = False
                            End If
                            If .INDV_Nationality3 <> objSaveOld.INDV_Nationality3 Then
                                Status = False
                            End If
                            If .INDV_Residence <> objSaveOld.INDV_Residence Then
                                Status = False
                            End If
                            If .INDV_Email <> objSaveOld.INDV_Email Then
                                Status = False
                            End If
                            If .INDV_Email2 <> objSaveOld.INDV_Email2 Then
                                Status = False
                            End If
                            If .INDV_Email3 <> objSaveOld.INDV_Email3 Then
                                Status = False
                            End If
                            If .INDV_Email4 <> objSaveOld.INDV_Email4 Then
                                Status = False
                            End If
                            If .INDV_Email5 <> objSaveOld.INDV_Email5 Then
                                Status = False
                            End If
                            If .INDV_Occupation <> objSaveOld.INDV_Occupation Then
                                Status = False
                            End If
                            If .INDV_Employer_Name <> objSaveOld.INDV_Employer_Name Then
                                Status = False
                            End If
                            If .INDV_SumberDana <> objSaveOld.INDV_SumberDana Then
                                Status = False
                            End If
                            If .INDV_Tax_Number <> objSaveOld.Corp_Tax_Number Then
                                Status = False
                            End If
                            If .INDV_Tax_Reg_Number <> objSaveOld.INDV_Tax_Reg_Number Then
                                Status = False
                            End If
                            If .INDV_Deceased <> objSaveOld.INDV_Deceased Then
                                Status = False
                            End If
                            If .INDV_Deceased_Date <> objSaveOld.INDV_Deceased_Date Then
                                Status = False
                            End If
                            If .INDV_Comment <> objSaveOld.INDV_Comment Then
                                Status = False
                            End If
                        End With

                        If ListPhoneDetailOld.Count = ListPhoneDetail.Count Then
                            Dim phone As New goAML_Ref_Phone
                            For Each item In ListPhoneDetail
                                phone = ListPhoneDetailOld.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                                'agam 22102020
                                If phone Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                If phone.FK_Ref_Detail_Of IsNot Nothing Then
                                    With phone
                                        If .Tph_Contact_Type <> item.Tph_Contact_Type Then
                                            Status = False
                                        End If
                                        If .Tph_Communication_Type <> item.Tph_Communication_Type Then
                                            Status = False
                                        End If
                                        If .tph_country_prefix <> item.tph_country_prefix Then
                                            Status = False
                                        End If
                                        If .tph_number <> item.tph_number Then
                                            Status = False
                                        End If
                                        If .tph_extension <> item.tph_extension Then
                                            Status = False
                                        End If
                                        If .comments <> item.comments Then
                                            Status = False
                                        End If
                                    End With
                                Else
                                    Status = False
                                End If
                                phone = New goAML_Ref_Phone
                            Next
                        Else
                            Status = False
                        End If

                        If ListPhoneEmployerDetailOld.Count = ListPhoneEmployerDetail.Count Then
                            Dim phoneEmp As New goAML_Ref_Phone
                            For Each item In ListPhoneEmployerDetail
                                phoneEmp = ListPhoneEmployerDetailOld.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                                'agam 22102020
                                If phoneEmp Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                If phoneEmp.FK_Ref_Detail_Of IsNot Nothing Then
                                    With phoneEmp
                                        If .Tph_Contact_Type <> item.Tph_Contact_Type Then
                                            Status = False
                                        End If
                                        If .Tph_Communication_Type <> item.Tph_Communication_Type Then
                                            Status = False
                                        End If
                                        If .tph_country_prefix <> item.tph_country_prefix Then
                                            Status = False
                                        End If
                                        If .tph_number <> item.tph_number Then
                                            Status = False
                                        End If
                                        If .tph_extension <> item.tph_extension Then
                                            Status = False
                                        End If
                                        If .comments <> item.comments Then
                                            Status = False
                                        End If
                                    End With
                                Else
                                    Status = False
                                End If
                                phoneEmp = New goAML_Ref_Phone
                            Next
                        Else
                            Status = False
                        End If

                        If ListAddressDetailOld.Count = ListAddressDetail.Count Then
                            Dim Address As New goAML_Ref_Address
                            For Each item In ListAddressDetail
                                Address = ListAddressDetailOld.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                                'agam 21102020
                                If Address Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                If Address IsNot Nothing Then
                                    If Address.FK_Ref_Detail_Of IsNot Nothing Then
                                        With Address
                                            If .Address_Type <> item.Address_Type Then
                                                Status = False
                                            End If
                                            If .Address <> item.Address Then
                                                Status = False
                                            End If
                                            If .Town <> item.Town Then
                                                Status = False
                                            End If
                                            If .City <> item.City Then
                                                Status = False
                                            End If
                                            If .Zip <> item.Zip Then
                                                Status = False
                                            End If
                                            If .Country_Code <> item.Country_Code Then
                                                Status = False
                                            End If
                                            If .State <> item.State Then
                                                Status = False
                                            End If
                                            If .Comments <> item.Comments Then
                                                Status = False
                                            End If
                                        End With
                                    Else
                                        Status = False
                                    End If
                                End If
                                Address = New goAML_Ref_Address
                            Next
                        Else
                            Status = False
                        End If

                        If ListAddressEmployerDetailOld.Count = ListAddressEmployerDetail.Count Then
                            Dim AddressEmp As New goAML_Ref_Address
                            For Each item In ListAddressEmployerDetail
                                AddressEmp = ListAddressEmployerDetailOld.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                                'agam 22102020
                                If AddressEmp Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                If AddressEmp.FK_Ref_Detail_Of IsNot Nothing Then
                                    With AddressEmp
                                        If .Address_Type <> item.Address_Type Then
                                            Status = False
                                        End If
                                        If .Address <> item.Address Then
                                            Status = False
                                        End If
                                        If .Town <> item.Town Then
                                            Status = False
                                        End If
                                        If .City <> item.City Then
                                            Status = False
                                        End If
                                        If .Zip <> item.Zip Then
                                            Status = False
                                        End If
                                        If .Country_Code <> item.Country_Code Then
                                            Status = False
                                        End If
                                        If .State <> item.State Then
                                            Status = False
                                        End If
                                        If .Comments <> item.Comments Then
                                            Status = False
                                        End If
                                    End With
                                Else
                                    Status = False
                                End If
                                AddressEmp = New goAML_Ref_Address
                            Next
                        Else
                            Status = False
                        End If

                        If ListIdentificationDetailOld.Count = ListIdentificationDetail.Count Then
                            Dim Identification As New goAML_Person_Identification
                            For Each item In ListIdentificationDetail
                                Identification = ListIdentificationDetailOld.Where(Function(x) x.PK_Person_Identification_ID = item.PK_Person_Identification_ID).FirstOrDefault
                                'agam 22102020
                                If Identification Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                If Identification.FK_Person_ID <> Nothing Then
                                    With Identification
                                        If .Type <> item.Type Then
                                            Status = False
                                        End If
                                        If .Number <> item.Number Then
                                            Status = False
                                        End If
                                        If .Issue_Date <> item.Issue_Date Then
                                            Status = False
                                        End If
                                        If .Expiry_Date <> item.Expiry_Date Then
                                            Status = False
                                        End If
                                        If .Issued_By <> item.Issued_By Then
                                            Status = False
                                        End If
                                        If .Issued_Country <> item.Issued_Country Then
                                            Status = False
                                        End If
                                        If .Identification_Comment <> item.Identification_Comment Then
                                            Status = False
                                        End If
                                    End With
                                Else
                                    Status = False
                                End If
                                Identification = New goAML_Person_Identification
                            Next
                        Else
                            Status = False
                        End If

                        If Status Then
                            Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                        End If

                        If Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                            objgoAML_WIC.SaveEditTanpaApproval(objSave, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule)
                            WIC_Panel.Hide()
                            Panelconfirmation.Show()
                            LblConfirmation.Text = "Data Saved into Database"
                        Else
                            objgoAML_WIC.SaveEditApproval(objSave, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule)
                            WIC_Panel.Hide()
                            Panelconfirmation.Show()
                            LblConfirmation.Text = "Data Saved into Pending Approval"
                        End If
                    End If
                End If
            Else
                'daniel 11 jan 2021
                'If TxtCorp_Name.Text = "" Then
                '    Throw New Exception("Nama Korporasi Tidak boleh kosong")
                'end 11 jan 2021
                'If TxtCorp_Name.Text = "" Then
                '    Throw New Exception("Nama Korporasi Tidak boleh kosong")
                'ElseIf TxtCorp_Business.Text = "" Then
                '    Throw New Exception("Bidang Usaha Tidak boleh kosong")
                If TxtCorp_Name.Text = "" Then
                    Throw New Exception("Nama Korporasi Tidak boleh kosong")
                    'ElseIf TxtCorp_Business.Text = "" Then
                    '    Throw New Exception("Bidang Usaha Tidak boleh kosong")
                    'ElseIf ListAddressDetail.Count <= 0 Then
                    '    Throw New Exception("Informasi Alamat Tidak boleh kosong")
                    'ElseIf CmbCorp_incorporation_country_code.TextValue = "" Then
                    '    Throw New Exception("Negara Tidak boleh kosong")
                Else
                    Dim objSaveOld As New goAML_Ref_WIC
                    Dim objSave As New goAML_Ref_WIC
                    Dim ListAddressDetailOld As New List(Of goAML_Ref_Address)
                    Dim ListPhoneDetailOld As New List(Of goAML_Ref_Phone)

                    objSaveOld = ObjWIC
                    ListAddressDetailOld = objgoAML_WIC.GetWICByPKIDDetailAddress(objSaveOld.PK_Customer_ID)
                    ListPhoneDetailOld = objgoAML_WIC.GetWICByPKIDDetailPhone(objSaveOld.PK_Customer_ID)

                    With objSave
                        .PK_Customer_ID = PKWIC.Text
                        .isUpdateFromDataSource = CheckboxIsUpdateFromDataSource.Checked
                        'daniel 13 jan 2021
                        .WIC_No = WICNo.Text
                        'end 13 jan 2021
                        .Corp_Name = TxtCorp_Name.Text
                        .Corp_Commercial_Name = TxtCorp_Commercial_name.Text
                        .Corp_Incorporation_Legal_Form = CmbCorp_Incorporation_legal_form.Value
                        .Corp_Incorporation_Number = TxtCorp_Incorporation_number.Text
                        .Corp_Business = TxtCorp_Business.Text
                        .Corp_Email = TxtCorp_Email.Text
                        .Corp_Url = TxtCorp_url.Text
                        .Corp_Incorporation_State = TxtCorp_incorporation_state.Text
                        .Corp_Incorporation_Country_Code = CmbCorp_incorporation_country_code.TextValue
                        If DateCorp_incorporation_date.Value <> DateTime.MinValue Then
                            .Corp_Incorporation_Date = DateCorp_incorporation_date.Value
                        End If
                        .Corp_Business_Closed = chkCorp_business_closed.Value
                        If chkCorp_business_closed.Checked Then
                            If DateCorp_date_business_closed.Value <> DateTime.MinValue Then
                                .Corp_Date_Business_Closed = DateCorp_date_business_closed.Value
                            End If
                        End If
                        .Corp_Tax_Number = TxtCorp_tax_number.Text
                        .Corp_Comments = TxtCorp_Comments.Text
                        .FK_Customer_Type_ID = 2
                        If DateINDV_Birthdate.RawValue <> DateTime.MinValue Then
                            .INDV_BirthDate = DateINDV_Birthdate.RawValue
                        End If
                    End With

                    'Cek Perubahan Korporasi
                    With objSave
                        If .isUpdateFromDataSource <> objSaveOld.isUpdateFromDataSource Then
                            Status = False
                        End If
                        If .Corp_Name <> objSaveOld.Corp_Name Then
                            Status = False
                        End If
                        If .Corp_Commercial_Name <> objSaveOld.Corp_Commercial_Name Then
                            Status = False
                        End If
                        If .Corp_Incorporation_Legal_Form <> objSaveOld.Corp_Incorporation_Legal_Form Then
                            Status = False
                        End If
                        If .Corp_Incorporation_Number <> objSaveOld.Corp_Incorporation_Number Then
                            Status = False
                        End If
                        If .Corp_Business <> objSaveOld.Corp_Business Then
                            Status = False
                        End If
                        If .Corp_Email <> objSaveOld.Corp_Email Then
                            Status = False
                        End If
                        If .Corp_Url <> objSaveOld.Corp_Url Then
                            Status = False
                        End If
                        If .Corp_Incorporation_State <> objSaveOld.Corp_Incorporation_State Then
                            Status = False
                        End If
                        If .Corp_Incorporation_Country_Code <> objSaveOld.Corp_Incorporation_Country_Code Then
                            Status = False
                        End If
                        If .Corp_Incorporation_Date <> objSaveOld.Corp_Incorporation_Date Then
                            Status = False
                        End If
                        If .Corp_Business_Closed <> objSaveOld.Corp_Business_Closed Then
                            Status = False
                        End If
                        If .Corp_Date_Business_Closed <> objSaveOld.Corp_Date_Business_Closed Then
                            Status = False
                        End If
                        If .Corp_Tax_Number <> objSaveOld.Corp_Tax_Number Then
                            Status = False
                        End If
                        If .Corp_Comments <> objSaveOld.Corp_Comments Then
                            Status = False
                        End If
                    End With

                    If ListPhoneDetailOld.Count = ListPhoneDetail.Count Then
                        Dim phone As New goAML_Ref_Phone
                        For Each item In ListPhoneDetail
                            phone = ListPhoneDetailOld.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                            'agam 22102020
                            If phone Is Nothing Then
                                Status = False
                                Exit For
                            End If
                            If phone.FK_Ref_Detail_Of IsNot Nothing Then
                                With phone
                                    If .Tph_Contact_Type <> item.Tph_Contact_Type Then
                                        Status = False
                                    End If
                                    If .Tph_Communication_Type <> item.Tph_Communication_Type Then
                                        Status = False
                                    End If
                                    If .tph_country_prefix <> item.tph_country_prefix Then
                                        Status = False
                                    End If
                                    If .tph_number <> item.tph_number Then
                                        Status = False
                                    End If
                                    If .tph_extension <> item.tph_extension Then
                                        Status = False
                                    End If
                                    If .comments <> item.comments Then
                                        Status = False
                                    End If
                                End With
                            Else
                                Status = False
                            End If
                            phone = New goAML_Ref_Phone
                        Next
                    Else
                        Status = False
                    End If

                    If ListAddressDetailOld.Count = ListAddressDetail.Count Then
                        Dim Address As New goAML_Ref_Address
                        For Each item In ListAddressDetail
                            Address = ListAddressDetailOld.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                            If Address Is Nothing Then
                                Status = False
                                Exit For
                            End If
                            If Address.FK_Ref_Detail_Of IsNot Nothing Then
                                With Address
                                    If .Address_Type <> item.Address_Type Then
                                        Status = False
                                    End If
                                    If .Address <> item.Address Then
                                        Status = False
                                    End If
                                    If .Town <> item.Town Then
                                        Status = False
                                    End If
                                    If .City <> item.City Then
                                        Status = False
                                    End If
                                    If .Zip <> item.Zip Then
                                        Status = False
                                    End If
                                    If .Country_Code <> item.Country_Code Then
                                        Status = False
                                    End If
                                    If .State <> item.State Then
                                        Status = False
                                    End If
                                    If .Comments <> item.Comments Then
                                        Status = False
                                    End If
                                End With
                            Else
                                Status = False
                            End If
                            Address = New goAML_Ref_Address
                        Next
                    Else
                        Status = False
                    End If

                    'Cek Perubahan Director
                    Dim ListDirectorDetailOld As New List(Of goAML_Ref_Walk_In_Customer_Director)
                    Dim ListDirectorAddressOld As New List(Of goAML_Ref_Address)
                    Dim ListDirectorAddressEmployerOld As New List(Of goAML_Ref_Address)
                    Dim ListDirectorPhoneOld As New List(Of goAML_Ref_Phone)
                    Dim ListDirectorPhoneEmployerOld As New List(Of goAML_Ref_Phone)
                    Dim ListDirectorIdentificationOld As New List(Of goAML_Person_Identification)
                    Dim Director As New goAML_Ref_Walk_In_Customer_Director
                    Dim AddressDir As New goAML_Ref_Address
                    Dim AddressDirEmp As New goAML_Ref_Address
                    Dim PhoneDir As New goAML_Ref_Phone
                    Dim PhoneDirEmp As New goAML_Ref_Phone
                    Dim IdentDir As New goAML_Person_Identification

                    ListDirectorDetailOld = objgoAML_WIC.GetWICByPKIDDetailDirector(objSaveOld.PK_Customer_ID)

                    For Each item In ListDirectorDetailClass
                        Director = ListDirectorDetailOld.Where(Function(x) x.NO_ID = item.ObjDirector.NO_ID).FirstOrDefault
                        'agam 22102020
                        If Director Is Nothing Then
                            Status = False
                            Exit For
                        End If
                        If Director IsNot Nothing Then
                            ListDirectorAddressOld = objgoAML_WIC.GetWICByPKIDDetailDirectorAddress(item.ObjDirector.NO_ID)
                            ListDirectorAddressEmployerOld = objgoAML_WIC.GetWICByPKIDDetailDirectorAddressEmployer(item.ObjDirector.NO_ID)
                            ListDirectorPhoneOld = objgoAML_WIC.GetWICByPKIDDetailDirectorPhone(item.ObjDirector.NO_ID)
                            ListDirectorPhoneEmployerOld = objgoAML_WIC.GetWICByPKIDDetailDirectorPhoneEmployer(item.ObjDirector.NO_ID)
                            ListDirectorIdentificationOld = objgoAML_WIC.GetWICByPKIDDetailDirectorIdentification(item.ObjDirector.NO_ID)

                            'Director
                            With item.ObjDirector
                                If .Role <> Director.Role Then
                                    Status = False
                                End If
                                If .Gender <> Director.Gender Then
                                    Status = False
                                End If
                                If .Title <> Director.Title Then
                                    Status = False
                                End If
                                If .Last_Name <> Director.Last_Name Then
                                    Status = False
                                End If
                                If .BirthDate <> Director.BirthDate Then
                                    Status = False
                                End If
                                If .Birth_Place <> Director.Birth_Place Then
                                    Status = False
                                End If
                                If .Mothers_Name <> Director.Mothers_Name Then
                                    Status = False
                                End If
                                If .Alias <> Director.Alias Then
                                    Status = False
                                End If
                                If .SSN <> Director.SSN Then
                                    Status = False
                                End If
                                If .Passport_Number <> Director.Passport_Number Then
                                    Status = False
                                End If
                                If .Passport_Country <> Director.Passport_Country Then
                                    Status = False
                                End If
                                If .ID_Number <> Director.ID_Number Then
                                    Status = False
                                End If
                                If .Nationality1 <> Director.Nationality1 Then
                                    Status = False
                                End If
                                If .Nationality2 <> Director.Nationality2 Then
                                    Status = False
                                End If
                                If .Nationality3 <> Director.Nationality3 Then
                                    Status = False
                                End If
                                If .Residence <> Director.Residence Then
                                    Status = False
                                End If
                                If .Email <> Director.Email Then
                                    Status = False
                                End If
                                If .Email2 <> Director.Email2 Then
                                    Status = False
                                End If
                                If .Email3 <> Director.Email3 Then
                                    Status = False
                                End If
                                If .Email4 <> Director.Email4 Then
                                    Status = False
                                End If
                                If .Email5 <> Director.Email5 Then
                                    Status = False
                                End If
                                If .Occupation <> Director.Occupation Then
                                    Status = False
                                End If
                                If .Employer_Name <> Director.Employer_Name Then
                                    Status = False
                                End If
                                If .Deceased <> Director.Deceased Then
                                    Status = False
                                End If
                                If .Deceased_Date <> Director.Deceased_Date Then
                                    Status = False
                                End If
                                If .Tax_Number <> Director.Tax_Number Then
                                    Status = False
                                End If
                                If .Tax_Reg_Number <> Director.Tax_Reg_Number Then
                                    Status = False
                                End If
                                If .Source_of_Wealth <> Director.Source_of_Wealth Then
                                    Status = False
                                End If
                                If .Comments <> Director.Comments Then
                                    Status = False
                                End If
                            End With

                            'Director Adress
                            If item.ListDirectorAddress.Count = ListDirectorAddressOld.Count Then

                                For Each itemx In item.ListDirectorAddress
                                    AddressDir = ListDirectorAddressOld.Where(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID).FirstOrDefault
                                    'agam 22102020
                                    If AddressDir Is Nothing Then
                                        Status = False
                                        Exit For
                                    End If
                                    If AddressDir.FK_Ref_Detail_Of IsNot Nothing Then
                                        With AddressDir
                                            If .Address_Type <> itemx.Address_Type Then
                                                Status = False
                                            End If
                                            If .Address <> itemx.Address Then
                                                Status = False
                                            End If
                                            If .Town <> itemx.Town Then
                                                Status = False
                                            End If
                                            If .City <> itemx.City Then
                                                Status = False
                                            End If
                                            If .Zip <> itemx.Zip Then
                                                Status = False
                                            End If
                                            If .Country_Code <> itemx.Country_Code Then
                                                Status = False
                                            End If
                                            If .State <> itemx.State Then
                                                Status = False
                                            End If
                                            If .Comments <> itemx.Comments Then
                                                Status = False
                                            End If
                                        End With
                                    Else
                                        Status = False
                                    End If
                                    AddressDir = New goAML_Ref_Address
                                Next
                            Else
                                Status = False
                            End If

                            'Director Adress Emp
                            If item.ListDirectorAddressEmployer.Count = ListDirectorAddressEmployerOld.Count Then

                                For Each itemx In item.ListDirectorAddressEmployer
                                    AddressDirEmp = ListDirectorAddressEmployerOld.Where(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID).FirstOrDefault
                                    'agam 22102020
                                    If AddressDirEmp Is Nothing Then
                                        Status = False
                                        Exit For
                                    End If
                                    If AddressDirEmp.FK_Ref_Detail_Of IsNot Nothing Then
                                        With AddressDirEmp
                                            If .Address_Type <> itemx.Address_Type Then
                                                Status = False
                                            End If
                                            If .Address <> itemx.Address Then
                                                Status = False
                                            End If
                                            If .Town <> itemx.Town Then
                                                Status = False
                                            End If
                                            If .City <> itemx.City Then
                                                Status = False
                                            End If
                                            If .Zip <> itemx.Zip Then
                                                Status = False
                                            End If
                                            If .Country_Code <> itemx.Country_Code Then
                                                Status = False
                                            End If
                                            If .State <> itemx.State Then
                                                Status = False
                                            End If
                                            If .Comments <> itemx.Comments Then
                                                Status = False
                                            End If
                                        End With
                                    Else
                                        Status = False
                                    End If

                                    AddressDirEmp = New goAML_Ref_Address
                                Next
                            Else
                                Status = False
                            End If

                            'Director Phone
                            If item.ListDirectorPhone.Count = ListDirectorPhoneOld.Count Then
                                For Each itemx In item.ListDirectorPhone
                                    PhoneDir = ListDirectorPhoneOld.Where(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone).FirstOrDefault
                                    'agam 22102020
                                    If PhoneDir Is Nothing Then
                                        Status = False
                                        Exit For
                                    End If
                                    If PhoneDir.FK_Ref_Detail_Of IsNot Nothing Then
                                        With PhoneDir
                                            If .Tph_Contact_Type <> itemx.Tph_Contact_Type Then
                                                Status = False
                                            End If
                                            If .Tph_Communication_Type <> itemx.Tph_Communication_Type Then
                                                Status = False
                                            End If
                                            If .tph_country_prefix <> itemx.tph_country_prefix Then
                                                Status = False
                                            End If
                                            If .tph_number <> itemx.tph_number Then
                                                Status = False
                                            End If
                                            If .tph_extension <> itemx.tph_extension Then
                                                Status = False
                                            End If
                                            If .comments <> itemx.comments Then
                                                Status = False
                                            End If
                                        End With
                                    Else
                                        Status = False
                                    End If
                                    PhoneDir = New goAML_Ref_Phone
                                Next
                            Else
                                Status = False
                            End If

                            'Director Phone Emp
                            If item.ListDirectorPhoneEmployer.Count = ListDirectorPhoneEmployerOld.Count Then
                                For Each itemx In item.ListDirectorPhoneEmployer
                                    PhoneDirEmp = ListDirectorPhoneEmployerOld.Where(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone).FirstOrDefault
                                    'agam 22102020
                                    If PhoneDirEmp Is Nothing Then
                                        Status = False
                                        Exit For
                                    End If
                                    If PhoneDirEmp.FK_Ref_Detail_Of IsNot Nothing Then
                                        With PhoneDirEmp
                                            If .Tph_Contact_Type <> itemx.Tph_Contact_Type Then
                                                Status = False
                                            End If
                                            If .Tph_Communication_Type <> itemx.Tph_Communication_Type Then
                                                Status = False
                                            End If
                                            If .tph_country_prefix <> itemx.tph_country_prefix Then
                                                Status = False
                                            End If
                                            If .tph_number <> itemx.tph_number Then
                                                Status = False
                                            End If
                                            If .tph_extension <> itemx.tph_extension Then
                                                Status = False
                                            End If
                                            If .comments <> itemx.comments Then
                                                Status = False
                                            End If
                                        End With
                                    Else
                                        Status = False
                                    End If
                                    PhoneDirEmp = New goAML_Ref_Phone
                                Next
                            Else
                                Status = False
                            End If

                            'Director Identification
                            If item.ListDirectorIdentification.Count = ListDirectorIdentificationOld.Count Then
                                For Each itemx In item.ListDirectorIdentification
                                    IdentDir = ListDirectorIdentificationOld.Where(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID).FirstOrDefault
                                    'agam 22102020
                                    If IdentDir Is Nothing Then
                                        Status = False
                                        Exit For
                                    End If
                                    If IdentDir.FK_Person_ID <> Nothing Then
                                        With IdentDir
                                            If .Type <> itemx.Type Then
                                                Status = False
                                            End If
                                            If .Number <> itemx.Number Then
                                                Status = False
                                            End If
                                            If .Issue_Date <> itemx.Issue_Date Then
                                                Status = False
                                            End If
                                            If .Expiry_Date <> itemx.Expiry_Date Then
                                                Status = False
                                            End If
                                            If .Issued_By <> itemx.Issued_By Then
                                                Status = False
                                            End If
                                            If .Issued_Country <> itemx.Issued_Country Then
                                                Status = False
                                            End If
                                            If .Identification_Comment <> itemx.Identification_Comment Then
                                                Status = False
                                            End If
                                        End With
                                    Else
                                        Status = False
                                    End If
                                    IdentDir = New goAML_Person_Identification
                                Next
                            Else
                                Status = False
                            End If

                            Director = New goAML_Ref_Walk_In_Customer_Director
                            ListDirectorAddressOld = New List(Of goAML_Ref_Address)
                            ListDirectorAddressEmployerOld = New List(Of goAML_Ref_Address)
                            ListDirectorPhoneOld = New List(Of goAML_Ref_Phone)
                            ListDirectorPhoneEmployerOld = New List(Of goAML_Ref_Phone)
                            ListDirectorIdentificationOld = New List(Of goAML_Person_Identification)

                        Else
                            Status = False
                        End If
                    Next

                    If Status Then
                        Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                    End If

                    If Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                        objgoAML_WIC.SaveEditTanpaApproval(objSave, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule)
                        WIC_Panel.Hide()
                        Panelconfirmation.Show()
                        LblConfirmation.Text = "Data Saved into Database"
                    Else
                        objgoAML_WIC.SaveEditApproval(objSave, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule)
                        WIC_Panel.Hide()
                        Panelconfirmation.Show()
                        LblConfirmation.Text = "Data Saved into Pending Approval"
                    End If
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()
            PanelDetailPhone.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()
            FormPanelDirectorPhone.Hide()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()
            FormPanelEmpDirectorTaskDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorEmployerPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()
            FormPanelDirectorPhone.Hide()
            FormPanelEmpDirectorTaskDetail.Hide()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelPhoneEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            PanelDetailPhone.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailPhoneEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            PanelDetailPhone.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelIdentification_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailIdentification.Hide()
            PanelDetailIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailIdentification_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailIdentification.Hide()
            FormPanelIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelIdentificationDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorIdentification.Hide()
            PanelDetailDirectorIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailIdentificationDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorIdentification.Hide()
            FormPanelDirectorIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailAddress.Hide()
            PanelDetailAddress.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddressEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorDetailAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hidden = True
            FP_Address_Director.Hide()
            FP_Address_Emp_Director.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorDetailEmployerAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            FP_Address_Director.Hide()
            FP_Address_Emp_Director.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'agam 221-2020
            WindowDetailDirectorPhone.Hidden = True
            'FormPanelEmpDirectorTaskDetail.Hidden = True
            'FormPanelDirectorPhone.Hidden = True
            'PanelPhoneDirectorEmployer.Hidden = True
            'PanelPhoneDirector.Hidden = True
            'If ObjDetailPhoneDirector Is Nothing Then
            '    SaveAddDetailPhoneDirector()
            'Else
            '    SaveEditDetailPhoneDirector()
            'End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorEmpPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhoneDirectorEmployer Is Nothing Then
                SaveAddDetailPhoneDirectorEmployer()
            Else
                Dim status As Boolean = True
                Dim ObjDetailPhoneDirectorEmployerNew As New goAML_Ref_Phone

                With ObjDetailPhoneDirectorEmployerNew
                    .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.Text
                    .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.Text
                    .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text
                    .tph_number = Director_Emp_txt_phone_number.Text
                    .tph_extension = Director_Emp_txt_phone_extension.Text
                    .comments = Director_Emp_txt_phone_comments.Text
                End With

                With ObjDetailPhoneDirectorEmployer
                    If .Tph_Contact_Type <> ObjDetailPhoneDirectorEmployerNew.Tph_Contact_Type Then
                        status = False
                    End If
                    If .Tph_Communication_Type <> ObjDetailPhoneDirectorEmployerNew.Tph_Communication_Type Then
                        status = False
                    End If
                    If .tph_country_prefix <> ObjDetailPhoneDirectorEmployerNew.tph_country_prefix Then
                        status = False
                    End If
                    If .tph_number <> ObjDetailPhoneDirectorEmployerNew.tph_number Then
                        status = False
                    End If
                    If .tph_extension <> ObjDetailPhoneDirectorEmployerNew.tph_extension Then
                        status = False
                    End If
                    If .comments <> ObjDetailPhoneDirectorEmployerNew.comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailPhoneDirectorEmployer()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorEmployerPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'agam 22102020
            WindowDetailDirectorPhone.Hidden = True
            'FormPanelEmpDirectorTaskDetail.Hidden = True
            'FormPanelDirectorPhone.Hidden = True
            'PanelPhoneDirectorEmployer.Hidden = True
            'PanelPhoneDirector.Hidden = True


            '    If ObjDetailPhoneDirectorEmployer Is Nothing Then
            '        SaveAddDetailPhoneDirectorEmployer()
            '    Else
            '        SaveEditDetailPhoneDirectorEmployer()
            '    End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailAddress.Hide()
            PanelDetailAddress.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailAddressEmployer_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailAddressDirector Is Nothing Then
                SaveAddDetailAddressDirector()
            Else
                Dim status As Boolean = True
                Dim ObjDetailAddressDirectorNew As New goAML_Ref_Address

                With ObjDetailAddressDirectorNew
                    .Address_Type = Director_cmb_kategoriaddress.Text
                    .Address = Director_Address.Text
                    .Town = Director_Town.Text
                    .City = Director_City.Text
                    .Zip = Director_Zip.Text
                    .Country_Code = Director_cmb_kodenegara.TextValue
                    .State = Director_State.Text
                    .Comments = Director_Comment_Address.Text
                End With

                With ObjDetailAddressDirector
                    If .Address_Type <> ObjDetailAddressDirectorNew.Address_Type Then
                        status = False
                    End If
                    If .Address <> ObjDetailAddressDirectorNew.Address Then
                        status = False
                    End If
                    If .Town <> ObjDetailAddressDirectorNew.Town Then
                        status = False
                    End If
                    If .City <> ObjDetailAddressDirectorNew.City Then
                        status = False
                    End If
                    If .Zip <> ObjDetailAddressDirectorNew.Zip Then
                        status = False
                    End If
                    If .Country_Code <> ObjDetailAddressDirectorNew.Country_Code Then
                        status = False
                    End If
                    If .State <> ObjDetailAddressDirectorNew.State Then
                        status = False
                    End If
                    If .Comments <> ObjDetailAddressDirectorNew.Comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailAddressDirector()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorEmployerAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'agam 22102020
            WindowDetailDirectorAddress.Hidden = True
            'FP_Address_Emp_Director.Hidden = True
            'If ObjDirector Is Nothing Then
            '    SaveAddDetailAddressDirectorEmployer()
            'Else
            '    SaveEditDetailAddressDirectorEmployer()
            'End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveCustomerDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailDirector.Hide()
            FP_Director.Hide()
            FormPanelDirectorDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'agam 22102020
            WindowDetailDirectorAddress.Hidden = True
            'FP_Address_Director.Hidden = True
            'If ObjDetailAddressDirector Is Nothing Then
            '    SaveAddDetailAddressDirector()
            'Else
            '    SaveEditDetailAddressDirector()
            'End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorEmpAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailAddressDirectorEmployer Is Nothing Then
                SaveAddDetailAddressDirectorEmployer()
            Else
                Dim status As Boolean = True
                Dim ObjDetailAddressDirectorEmployerNew As New goAML_Ref_Address

                With ObjDetailAddressDirectorEmployerNew
                    .Address_Type = Director_cmb_emp_kategoriaddress.Text
                    .Address = Director_emp_Address.Text
                    .Town = Director_emp_Town.Text
                    .City = Director_emp_City.Text
                    .Zip = Director_emp_Zip.Text
                    .Country_Code = Director_cmb_emp_kodenegara.TextValue
                    .State = Director_emp_State.Text
                    .Comments = Director_emp_Comment_Address.Text
                End With

                With ObjDetailAddressDirectorEmployer
                    If .Address_Type <> ObjDetailAddressDirectorEmployerNew.Address_Type Then
                        status = False
                    End If
                    If .Address <> ObjDetailAddressDirectorEmployerNew.Address Then
                        status = False
                    End If
                    If .Town <> ObjDetailAddressDirectorEmployerNew.Town Then
                        status = False
                    End If
                    If .City <> ObjDetailAddressDirectorEmployerNew.City Then
                        status = False
                    End If
                    If .Zip <> ObjDetailAddressDirectorEmployerNew.Zip Then
                        status = False
                    End If
                    If .Country_Code <> ObjDetailAddressDirectorEmployerNew.Country_Code Then
                        status = False
                    End If
                    If .State <> ObjDetailAddressDirectorEmployerNew.State Then
                        status = False
                    End If
                    If .Comments <> ObjDetailAddressDirectorEmployerNew.Comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailAddressDirectorEmployer()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            PanelDetailPhone.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdCustDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditIdentificationDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailIdentificationDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordIdentificationDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordIdentification(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhone(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneEmployerIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneIndividuEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneIndividuEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhoneEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhoneDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhoneDirectorEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailIdentificationDirector(id As String)
        btnAddDirectorIdentification_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Person_Identification = ListIdentificationDirector.Find(Function(x) x.PK_Person_Identification_ID = id)
        If Not objDelete Is Nothing Then
            ListIdentificationDirector.Remove(objDelete)
            'Daniel 2021 Jan 5
            'BindDetailIdentification(StoreIdentificationDirector, ListIdentificationDirector)
            BindDetailIdentification(StoreIdentificationDirectorDetail, ListIdentificationDirector)
            'end 2021 Jan 5
            WindowDetailDirectorIdentification.Hide()
            FormPanelIdentification.Hide()
            PanelDetailIdentification.Hide()
            FP_Director.Show()
        End If
    End Sub
    Private Sub DeleteRecordDetailIdentification(id As String)
        btnAddIdentification_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Person_Identification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
        If Not objDelete Is Nothing Then
            ListIdentificationDetail.Remove(objDelete)
            BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
            WindowDetailIdentification.Hide()
            FormPanelIdentification.Hide()
            PanelDetailIdentification.Hide()
        End If
    End Sub
    Private Sub DeleteRecordDetailPhoneIndividu(id As String)
        BtnAddNewPhoneIndividu_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Phone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not objDelete Is Nothing Then
            ListPhoneDetail.Remove(objDelete)
            BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
            WindowDetailPhone.Hide()
            FormPanelPhoneDetail.Hide()
        End If

    End Sub
    Private Sub DeleteRecordDetailPhoneIndividuEmployer(id As String)
        BtnAddNewPhoneEmployerIndividu_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Phone = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not objDelete Is Nothing Then
            ListPhoneEmployerDetail.Remove(objDelete)
            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)
            WindowDetailPhone.Hide()
            FormPanelPhoneEmployerDetail.Hide()
        End If

    End Sub
    Private Sub DeleteRecordDetailPhoneDirector(id As String)
        btnAddDirectorPhones_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Phone = ListPhoneDetailDirector.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not objDelete Is Nothing Then
            ListPhoneDetailDirector.Remove(objDelete)
            BindDetailPhone(Store_DirectorPhone, ListPhoneDetailDirector)
            WindowDetailDirectorPhone.Hide()
            FormPanelPhoneDetail.Hide()
        End If

    End Sub
    Private Sub DeleteRecordDetailPhoneDirectorEmployer(id As String)
        btnAddDirectorEmployerPhones_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Phone = ListPhoneDetailDirectorEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not objDelete Is Nothing Then
            ListPhoneDetailDirectorEmployer.Remove(objDelete)
            BindDetailPhone(Store_Director_Employer_Phone, ListPhoneDetailDirectorEmployer)
            WindowDetailDirectorPhone.Hide()
            FormPanelEmpDirectorTaskDetail.Hide()
        End If

    End Sub
    Private Sub DetailRecordIdentificationDirector(id As String)
        ObjDetailIdentificationDirector = ListIdentificationDirector.Find(Function(x) x.PK_Person_Identification_ID = id And x.FK_Person_Type = 7)
        If Not ObjDetailIdentificationDirector Is Nothing Then

            PanelDetailDirectorIdentification.Show()
            FormPanelDirectorIdentification.Hide()
            WindowDetailDirectorIdentification.Title = "Detail Identitas"
            WindowDetailDirectorIdentification.Show()
            With ObjDetailIdentificationDirector
                DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                DspNumberDirector.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDateDirector.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDateDirector.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedByDirector.Text = .Issued_By
                DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                DspIdentificationCommentDirector.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub DetailRecordIdentification(id As String)
        ObjDetailIdentification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id And x.FK_Person_Type = 8)
        If Not ObjDetailIdentification Is Nothing Then

            PanelDetailIdentification.Show()
            FormPanelIdentification.Hide()
            WindowDetailIdentification.Title = "Detail Identitas"
            WindowDetailIdentification.Show()
            With ObjDetailIdentification
                DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                DspNumber.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDate.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDate.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedBy.Text = .Issued_By
                DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                DspIdentificationComment.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub DetailRecordPhone(id As String)
        ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhone Is Nothing Then

            PanelDetailPhoneEmployer.Hide()
            PanelDetailPhone.Show()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()
            WindowDetailPhone.Title = "Detail Phone"
            WindowDetailPhone.Show()

            With ObjDetailPhone
                Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
    End Sub
    Private Sub DetailRecordPhoneEmployer(id As String)
        ObjDetailPhoneEmployer = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneEmployer Is Nothing Then

            PanelDetailPhoneEmployer.Show()
            PanelDetailPhone.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()
            WindowDetailPhone.Title = "Detail Phone Employer"
            WindowDetailPhone.Show()

            With ObjDetailPhoneEmployer
                Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefixEmployer.Text = .tph_country_prefix
                Dsptph_numberEmployer.Text = .tph_number
                Dsptph_extensionEmployer.Text = .tph_extension
                DspcommentsPhoneEmployer.Text = .comments
            End With
        End If
    End Sub

    'Daniel 2021 Jan 5
    'Private Sub DetailRecordDirector(id As String)
    '    Dim ListObjDirector As WICDirectorDataBLL = ListDirectorDetailClass.Find(Function(x) x.ObjDirector.NO_ID = id)
    '    Dim ObjDirector As goAML_Ref_Walk_In_Customer_Director = ListObjDirector.ObjDirector
    '    ListAddressDetailDirector = ListObjDirector.ListDirectorAddress.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList
    '    ListAddressDetailDirectorEmployer = ListObjDirector.ListDirectorAddressEmployer.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList
    '    ListPhoneDetailDirector = ListObjDirector.ListDirectorPhone.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList
    '    ListPhoneDetailDirectorEmployer = ListObjDirector.ListDirectorPhoneEmployer.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList
    '    ListIdentificationDirector = ListObjDirector.ListDirectorIdentification.Where(Function(x) x.FK_Person_ID = id And x.FK_Person_Type = 7).ToList

    '    If Not ObjDirector Is Nothing Then

    '        FormPanelDirectorDetail.Show()
    '        FP_Director.Hide()
    '        WindowDetailDirector.Show()

    '        With ObjDirector
    '            DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleTypebyCode(.Role)
    '            DspGelarDirector.Text = .Title
    '            DspNamaLengkap.Text = .Last_Name
    '            DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
    '            If .BirthDate IsNot Nothing Then
    '                DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yy")
    '            End If
    '            DspTempatLahir.Text = .Birth_Place
    '            DspNamaIbuKandung.Text = .Mothers_Name
    '            DspNamaAlias.Text = .Alias
    '            DspNIK.Text = .SSN
    '            DspNoPassport.Text = .Passport_Number
    '            DspNegaraPenerbitPassport.Text = .Passport_Country
    '            DspNoIdentitasLain.Text = .ID_Number
    '            DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
    '            DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
    '            DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
    '            DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
    '            DspEmail.Text = .Email
    '            DspEmail2.Text = .Email2
    '            DspEmail3.Text = .Email3
    '            DspEmail4.Text = .Email4
    '            DspEmail5.Text = .Email5
    '            DspDeceased.Text = .Deceased
    '            If DspDeceased.Text = "True" Then
    '                If .Deceased_Date IsNot Nothing Then
    '                    DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
    '                End If
    '            End If
    '            DspPEP.Text = .Tax_Reg_Number
    '            DspNPWP.Text = .Tax_Number
    '            DspSourceofWealth.Text = .Source_of_Wealth
    '            DspOccupation.Text = .Occupation
    '            DspCatatan.Text = .Comments
    '        End With

    '        'BindDetailAddress(StoreAddressDirector, ListAddressDetailDirector)
    '        'BindDetailAddress(StoreAddressDirectorEmployer, ListAddressDetailDirectorEmployer)
    '        'BindDetailPhone(StorePhoneDirector, ListPhoneDetailDirector)
    '        'BindDetailPhone(StorePhoneDirectorEmployer, ListPhoneDetailDirectorEmployer)
    '        'BindDetailIdentification(StoreIdentificationDirector, ListIdentificationDirector)
    '        BindDetailAddress(Store_DirectorAddress, ListAddressDetailDirector)
    '        BindDetailAddress(Store_Director_Employer_Address, ListAddressDetailDirectorEmployer)
    '        BindDetailPhone(Store_DirectorPhone, ListPhoneDetailDirector)
    '        BindDetailPhone(Store_Director_Employer_Phone, ListPhoneDetailDirectorEmployer)
    '        BindDetailIdentification(StoreIdentificationDirectorDetail, ListIdentificationDirector)

    '    End If
    'End Sub

    Private Sub DetailRecordDirector(id As String)
        ObjDirector = ListDirectorDetail.Find(Function(x) x.NO_ID = id)
        If Not ObjDirector Is Nothing Then

            FormPanelDirectorDetail.Show()
            WindowDetailDirector.Show()
            'Daniel 2021 Jan 8
            FP_Director.Hide()
            'end 2021 Jan 8
            With ObjDirector
                If GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role) IsNot Nothing Then
                    DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
                Else
                    DspPeranDirector.Text = ""
                End If

                DspGelarDirector.Text = .Title
                DspNamaLengkap.Text = .Last_Name
                If GlobalReportFunctionBLL.getGenderbyKode(.Gender) IsNot Nothing Then
                    DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                Else
                    DspGender.Text = ""
                End If


                If .BirthDate IsNot Nothing Then
                    DspTanggalLahir.Text = ObjDirector.BirthDate.Value.ToString("dd-MMM-yy")
                Else
                    DspTanggalLahir.Text = ""
                End If
                DspTempatLahir.Text = .Birth_Place
                DspNamaIbuKandung.Text = .Mothers_Name
                DspNamaAlias.Text = .Alias
                DspNIK.Text = .SSN
                DspNoPassport.Text = .Passport_Number
                'daniel 19 jan 2021
                DspNegaraPenerbitPassport.Text = GlobalReportFunctionBLL.getCountryByCode(.Passport_Country)
                DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                'If GlobalReportFunctionBLL.getCountryByCode(.Passport_Country) IsNot Nothing Then
                'End If
                'DspNegaraPenerbitPassport.Text = .Passport_Country
                DspNoIdentitasLain.Text = .ID_Number
                'If GlobalReportFunctionBLL.getCountryByCode(.Nationality1) IsNot Nothing Then
                '    DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.Nationality2) IsNot Nothing Then
                '    DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                'Else
                '    DspKewarganegaraan2.Text = ""
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.Nationality3) IsNot Nothing Then
                '    DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                'Else
                '    DspKewarganegaraan3.Text = ""
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.Residence) IsNot Nothing Then
                '    DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                'Else
                '    DspNegaraDomisili.Text = ""
                'End If
                'end 19 jan 2021

                DspEmail.Text = .Email
                DspEmail2.Text = .Email2
                DspEmail3.Text = .Email3
                DspEmail4.Text = .Email4
                DspEmail5.Text = .Email5
                DspDeceased.Text = .Deceased
                If DspDeceased.Text = "True" Then
                    If .Deceased_Date IsNot Nothing Then
                        DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                        'Daniel 2021 Jan 7
                        DspDeceasedDate.Hidden = False
                    End If
                Else
                    DspDeceasedDate.Hidden = True
                    'end 2021 Jan 7
                End If
                DspPEP.Text = .Tax_Reg_Number
                DspNPWP.Text = .Tax_Number
                DspSourceofWealth.Text = .Source_of_Wealth
                DspOccupation.Text = .Occupation
                DspCatatan.Text = .Comments
                DspTempatBekerja.Text = .Employer_Name
            End With

            ListPhoneDetailDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorPhone(ObjDirector.NO_ID)
            ListPhoneDetailDirectorEmployer = objgoAML_WIC.GetWICByPKIDDetailDirectorPhoneEmployer(ObjDirector.NO_ID)
            ListAddressDetailDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorAddress(ObjDirector.NO_ID)
            ListAddressDetailDirectorEmployer = objgoAML_WIC.GetWICByPKIDDetailDirectorAddressEmployer(ObjDirector.NO_ID)
            ListIdentificationDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorIdentification(ObjDirector.NO_ID)

            BindDetailPhone(StorePhoneDirector, ListPhoneDetailDirector)
            BindDetailPhone(StorePhoneDirectorEmployer, ListPhoneDetailDirectorEmployer)
            BindDetailAddress(StoreAddressDirector, ListAddressDetailDirector)
            BindDetailAddress(StoreAddressDirectorEmployer, ListAddressDetailDirectorEmployer)
            BindDetailIdentification(StoreIdentificationDirector, ListIdentificationDirector)

        End If
    End Sub
    'end 2021 Jan 5

    Private Sub DetailRecordPhoneDirector(id As String)
        ObjDetailPhoneDirector = ListPhoneDetailDirector.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneDirector Is Nothing Then
            WindowDetailDirectorPhone.Title = "Detail Director Phone"
            WindowDetailDirectorPhone.Show()
            PanelPhoneDirector.Show()
            FormPanelEmpDirectorTaskDetail.Hide()
            'agam 22102020
            PanelPhoneDirectorEmployer.Hidden = True
            FormPanelDirectorPhone.Hidden = True

            'ClearInput()
            With ObjDetailPhoneDirector
                DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                DspKodeAreaTelpDirector.Text = .tph_country_prefix
                DspNomorTeleponDirector.Text = .tph_number
                DspNomorExtensiDirector.Text = .tph_extension
                DspCatatanDirectorPhoneDirector.Text = .comments
            End With
        End If
    End Sub
    Private Sub DetailRecordPhoneDirectorEmployer(id As String)
        ObjDetailPhoneDirectorEmployer = ListPhoneDetailDirectorEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneDirectorEmployer Is Nothing Then

            PanelPhoneDirectorEmployer.Show()
            PanelPhoneDirector.Hide()
            WindowDetailDirectorPhone.Title = "Detail Director Phone Employer"
            WindowDetailDirectorPhone.Show()
            'agam 22102020
            FormPanelDirectorPhone.Hidden = True
            FormPanelEmpDirectorTaskDetail.Hidden = True
            clearinputDirector()
            'ClearInput()
            With ObjDetailPhoneDirectorEmployer
                DspKategoriKontakDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                DspJenisAlatKomunikasiDirectorEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                DspKodeAreaTelpDirectorEmployer.Text = .tph_country_prefix
                DspNomorTeleponDirectorEmployer.Text = .tph_number
                DspNomorExtensiDirectorEmployer.Text = .tph_extension
                DspCatatanDirectorPhoneDirectorEmployer.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditIdentificationDirector(id As String)
        ObjDetailIdentificationDirector = ListIdentificationDirector.Find(Function(x) x.PK_Person_Identification_ID = id And x.FK_Person_Type = 7)
        If Not ObjDetailIdentificationDirector Is Nothing Then

            FormPanelDirectorIdentification.Show()
            PanelDetailDirectorIdentification.Hide()
            WindowDetailDirectorIdentification.Title = "Edit identitas"
            WindowDetailDirectorIdentification.Show()

            With ObjDetailIdentificationDirector
                CmbTypeIdentificationDirector.SetValue(.Type)
                TxtNumberDirector.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    TxtIssueDateDirector.Text = .Issue_Date
                End If
                If .Expiry_Date IsNot Nothing Then
                    TxtExpiryDateDirector.Text = .Expiry_Date
                End If
                TxtIssuedByDirector.Text = .Issued_By
                If Not IsNothing(.Issued_Country) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Issued_Country & "'")
                    If DataRowTemp IsNot Nothing Then
                        CmbCountryIdentificationDirector.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                ' CmbCountryIdentificationDirector.SetValueAndFireSelect(.Issued_Country)
                TxtCommentIdentificationDirector.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub LoadDataEditIdentification(id As String)
        ObjDetailIdentification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id And x.FK_Person_Type = 8)
        If Not ObjDetailIdentification Is Nothing Then

            FormPanelIdentification.Show()
            PanelDetailIdentification.Hide()
            WindowDetailIdentification.Title = "Edit Identitas"
            WindowDetailIdentification.Show()

            With ObjDetailIdentification
                CmbTypeIdentification.SetValue(.Type)
                TxtNumber.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    TxtIssueDate.Text = .Issue_Date
                End If
                If .Expiry_Date IsNot Nothing Then
                    TxtExpiryDate.Text = .Expiry_Date
                End If
                TxtIssuedBy.Text = .Issued_By
                If Not IsNothing(.Issued_Country) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Issued_Country & "'")
                    If DataRowTemp IsNot Nothing Then
                        CmbCountryIdentification.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                'CmbCountryIdentification.SetValueAndFireSelect(.Issued_Country)
                TxtCommentIdentification.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub LoadDataEditPhoneIndividu(id As String)
        ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhone Is Nothing Then
            FormPanelPhoneDetail.Show()
            WindowDetailPhone.Title = "Edit Phone"
            WindowDetailPhone.Show()
            ClearInput()
            With ObjDetailPhone
                Cmbtph_contact_type.SetValueAndFireSelect(.Tph_Contact_Type)
                Cmbtph_communication_type.SetValueAndFireSelect(.Tph_Communication_Type)
                Txttph_country_prefix.Text = .tph_country_prefix
                Txttph_number.Text = .tph_number
                Txttph_extension.Text = .tph_extension
                TxtcommentsPhone.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditPhoneIndividuEmployer(id As String)
        ObjDetailPhoneEmployer = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneEmployer Is Nothing Then
            FormPanelPhoneEmployerDetail.Show()
            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            FormPanelPhoneDetail.Hide()
            WindowDetailPhone.Title = "Edit Phone Employer"
            WindowDetailPhone.Show()

            With ObjDetailPhoneEmployer
                Cmbtph_contact_typeEmployer.SetValueAndFireSelect(.Tph_Contact_Type)
                Cmbtph_communication_typeEmployer.SetValueAndFireSelect(.Tph_Communication_Type)
                Txttph_country_prefixEmployer.Text = .tph_country_prefix
                Txttph_numberEmployer.Text = .tph_number
                Txttph_extensionEmployer.Text = .tph_extension
                TxtcommentsPhoneEmployer.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditDirector(id As String)
        Dim listDirectorDetails As WICDirectorDataBLL = ListDirectorDetailClass.Where(Function(x) x.ObjDirector.NO_ID = id).FirstOrDefault
        'ObjDirector = listDirectorDetails.ObjDirector
        ListAddressDetailDirector = listDirectorDetails.ListDirectorAddress.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList
        ListAddressDetailDirectorEmployer = listDirectorDetails.ListDirectorAddressEmployer.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList
        ListPhoneDetailDirector = listDirectorDetails.ListDirectorPhone.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList
        ListPhoneDetailDirectorEmployer = listDirectorDetails.ListDirectorPhoneEmployer.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList
        ListIdentificationDirector = listDirectorDetails.ListDirectorIdentification.Where(Function(x) x.FK_Person_ID = id And x.FK_Person_Type = 7).ToList()
        'daniel 21 jan 2021
        ClearInput()
        ObjDirector = listDirectorDetails.ObjDirector
        'end 21 jan 2021
        If Not ObjDirector Is Nothing Then
            FP_Director.Show()
            WindowDetailDirector.Show()
            FormPanelDirectorDetail.Hide()
            'ClearInput()
            With ObjDirector
                Hidden2.Value = .NO_ID
                cmb_peran.SetValueAndFireSelect(.Role)
                txt_Director_Title.Text = .Title
                txt_Director_Last_Name.Text = .Last_Name
                cmb_Director_Gender.SetValueAndFireSelect(.Gender)
                If .BirthDate IsNot Nothing Then
                    txt_Director_BirthDate.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                End If
                txt_Director_Birth_Place.Text = .Birth_Place
                txt_Director_Mothers_Name.Text = .Mothers_Name
                txt_Director_Alias.Text = .Alias
                txt_Director_ID_Number.Text = .ID_Number
                txt_Director_Passport_Number.Text = .Passport_Number
                If Not IsNothing(.Passport_Country) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Passport_Country & "'")
                    If DataRowTemp IsNot Nothing Then
                        txt_Director_Passport_Country.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                ' txt_Director_Passport_Country.Text = .Passport_Country
                txt_Director_SSN.Text = .SSN
                If Not IsNothing(.Nationality1) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Nationality1 & "'")
                    If DataRowTemp IsNot Nothing Then
                        cmb_Director_Nationality1.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                If Not IsNothing(.Nationality2) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Nationality2 & "'")
                    If DataRowTemp IsNot Nothing Then
                        cmb_Director_Nationality2.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                If Not IsNothing(.Nationality3) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Nationality3 & "'")
                    If DataRowTemp IsNot Nothing Then
                        cmb_Director_Nationality3.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                If Not IsNothing(.Residence) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Residence & "'")
                    If DataRowTemp IsNot Nothing Then
                        cmb_Director_Residence.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                'cmb_Director_Nationality1.SetValueAndFireSelect(.Nationality1)
                'cmb_Director_Nationality2.SetValueAndFireSelect(.Nationality2)
                'cmb_Director_Nationality3.SetValueAndFireSelect(.Nationality3)
                'cmb_Director_Residence.SetValueAndFireSelect(.Residence)
                txt_Director_Email.Text = .Email
                txt_Director_Email2.Text = .Email2
                txt_Director_Email3.Text = .Email3
                txt_Director_Email4.Text = .Email4
                txt_Director_Email5.Text = .Email5
                cb_Director_Deceased.Checked = .Deceased
                If cb_Director_Deceased.Checked Then
                    If .Deceased_Date IsNot Nothing Then
                        txt_Director_Deceased_Date.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                    End If
                End If
                cb_Director_Tax_Reg_Number.Checked = .Tax_Reg_Number
                txt_Director_Tax_Number.Text = .Tax_Number
                txt_Director_Source_of_Wealth.Text = .Source_of_Wealth
                txt_Director_Occupation.Text = .Occupation
                txt_Director_Comments.Text = .Comments
                txt_Director_employer_name.Text = .Employer_Name
            End With

            BindDetailAddress(Store_DirectorAddress, ListAddressDetailDirector)
            BindDetailAddress(Store_Director_Employer_Address, ListAddressDetailDirectorEmployer)
            BindDetailPhone(Store_DirectorPhone, ListPhoneDetailDirector)
            BindDetailPhone(Store_Director_Employer_Phone, ListPhoneDetailDirectorEmployer)
            BindDetailIdentification(StoreIdentificationDirectorDetail, ListIdentificationDirector)

        End If
    End Sub
    Private Sub LoadDataEditPhoneDirector(id As String)
        ObjDetailPhoneDirector = ListPhoneDetailDirector.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneDirector Is Nothing Then
            FormPanelDirectorPhone.Show()
            FormPanelEmpDirectorTaskDetail.Hide()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()
            WindowDetailDirectorPhone.Title = "Edit Director Phone"
            WindowDetailDirectorPhone.Show()
            'ClearInput()
            With ObjDetailPhoneDirector
                Director_cb_phone_Contact_Type.SetValueAndFireSelect(.Tph_Contact_Type)
                Director_cb_phone_Communication_Type.SetValueAndFireSelect(.Tph_Communication_Type)
                Director_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_txt_phone_number.Text = .tph_number
                Director_txt_phone_extension.Text = .tph_extension
                Director_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditPhoneDirectorEmployer(id As String)
        ObjDetailPhoneDirectorEmployer = ListPhoneDetailDirectorEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneDirectorEmployer Is Nothing Then
            FormPanelEmpDirectorTaskDetail.Show()
            FormPanelDirectorPhone.Hide()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()
            WindowDetailDirectorPhone.Title = "Detail Director Phone Employer"
            WindowDetailDirectorPhone.Show()
            'ClearInput()
            With ObjDetailPhoneDirectorEmployer
                Director_Emp_cb_phone_Contact_Type.SetValueAndFireSelect(.Tph_Contact_Type)
                Director_Emp_cb_phone_Communication_Type.SetValueAndFireSelect(.Tph_Communication_Type)
                Director_Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_Emp_txt_phone_number.Text = .tph_number
                Director_Emp_txt_phone_extension.Text = .tph_extension
                Director_Emp_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub

    Protected Sub GridCommandAddressIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddress(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressEmployerIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressIndividuEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressIndividuEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddressEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddressDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddressDirectorEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DeleteRecordDetailAddressIndividu(id As String)
        BtnAddNewAddressIndividu_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Address = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not objDelete Is Nothing Then
            ListAddressDetail.Remove(objDelete)
            FormPanelAddressDetail.Hide()
            WindowDetailAddress.Hide()
            BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
        End If
    End Sub
    Private Sub DeleteRecordDetailAddressIndividuEmployer(id As String)
        BtnAddNewAddressEmployerIndividu_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Address = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not objDelete Is Nothing Then
            ListAddressEmployerDetail.Remove(objDelete)
            FormPanelAddressDetailEmployer.Hide()
            WindowDetailAddress.Hide()
            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)
        End If
    End Sub
    Private Sub DeleteRecordDetailDirector(id As String)
        btnAddCustomerDirector_DirectClick(Nothing, Nothing)
        'daniel 21 jan 2021
        Dim objDirector As WICDirectorDataBLL = ListDirectorDetailClass.Find(Function(x) x.ObjDirector.NO_ID = id)
        Dim objDelete As goAML_Ref_Walk_In_Customer_Director = ListDirectorDetail.Find(Function(x) x.NO_ID = id)
        If Not objDelete Is Nothing Then
            ListDirectorDetail.Remove(objDelete)
        End If
        If objDirector IsNot Nothing Then
            ListDirectorDetailClass.Remove(objDirector)
        End If
        FP_Director.Hide()
        WindowDetailDirector.Hide()
        BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
        'end 21 jan 2021
    End Sub
    Private Sub DeleteRecordDetailAddressDirector(id As String)
        btnAddDirectorAddresses_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Address = ListAddressDetailDirector.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 8)
        If Not objDelete Is Nothing Then
            ListAddressDetailDirector.Remove(objDelete)
            FP_Address_Director.Hide()
            WindowDetailDirectorAddress.Hide()
            BindDetailAddress(Store_DirectorAddress, ListAddressDetailDirector)
        End If
    End Sub
    Private Sub DeleteRecordDetailAddressDirectorEmployer(id As String)
        btnAddDirectorAddresses_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Address = ListAddressDetailDirectorEmployer.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 4)
        If Not objDelete Is Nothing Then
            ListAddressDetailDirectorEmployer.Remove(objDelete)
            FP_Address_Emp_Director.Hide()
            WindowDetailDirectorAddress.Hide()
            BindDetailAddress(Store_Director_Employer_Address, ListAddressDetailDirectorEmployer)
        End If
    End Sub

    Private Sub LoadDataEditAddressIndividu(id As String)
        ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 3)
        If Not ObjDetailAddress Is Nothing Then
            FormPanelAddressDetail.Show()
            WindowDetailAddress.Title = "Edit Address"
            WindowDetailAddress.Show()
            FormPanelAddressDetailEmployer.Hide()
            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()

            With ObjDetailAddress
                CmbAddress_type.SetValueAndFireSelect(.Address_Type)
                TxtAddress.Text = .Address
                TxtTown.Text = .Town
                TxtCity.Text = .City
                TxtZip.Text = .Zip
                If Not IsNothing(.Country_Code) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                    If DataRowTemp IsNot Nothing Then
                        Cmbcountry_code.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                'Cmbcountry_code.SetValueAndFireSelect(.Country_Code)
                TxtState.Text = .State
                TxtcommentsAddress.Text = .Comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditAddressIndividuEmployer(id As String)
        ObjDetailAddressEmployer = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 10)

        If Not ObjDetailAddressEmployer Is Nothing Then
            FormPanelAddressDetailEmployer.Show()
            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()
            FormPanelAddressDetail.Hide()
            WindowDetailAddress.Title = "Edit Address Employer"
            WindowDetailAddress.Hidden = False

            With ObjDetailAddressEmployer
                CmbAddress_typeEmployer.SetValueAndFireSelect(.Address_Type)
                TxtAddressEmployer.Text = .Address
                TxtTownEmployer.Text = .Town
                TxtCityEmployer.Text = .City
                TxtZipEmployer.Text = .Zip
                If Not IsNothing(.Country_Code) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                    If DataRowTemp IsNot Nothing Then
                        Cmbcountry_codeEmployer.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                'Cmbcountry_codeEmployer.SetValueAndFireSelect(.Country_Code)
                TxtStateEmployer.Text = .State
                TxtcommentsAddressEmployer.Text = .Comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditAddressDirector(id As String)
        ObjDetailAddressDirector = ListAddressDetailDirector.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 8)
        If Not ObjDetailAddressDirector Is Nothing Then
            FP_Address_Director.Show()
            'Daniel 2021 Jan 4
            'FP_Address_Emp_Director.Hide()
            PanelAddressDirector.Hide()
            'end 2021 Jan 4
            WindowDetailDirectorAddress.Title = "Edit Director Address"
            WindowDetailDirectorAddress.Show()
            'ClearInput()
            With ObjDetailAddressDirector
                Director_cmb_kategoriaddress.SetValueAndFireSelect(.Address_Type)
                Director_Address.Text = .Address
                Director_Town.Text = .Town
                Director_City.Text = .City
                Director_Zip.Text = .Zip
                If Not IsNothing(.Country_Code) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                    If DataRowTemp IsNot Nothing Then
                        Director_cmb_kodenegara.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                ' Director_cmb_kodenegara.SetValueAndFireSelect(.Country_Code)
                Director_State.Text = .State
                Director_Comment_Address.Text = .Comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditAddressDirectorEmployer(id As String)
        ObjDetailAddressDirectorEmployer = ListAddressDetailDirectorEmployer.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 4)
        If Not ObjDetailAddressDirectorEmployer Is Nothing Then
            'Daniel 2021 Jan 4
            PanelAddressDirectorEmployer.Hide()
            'FP_Address_Director.Hide()
            'end 2021 Jan 4
            FP_Address_Emp_Director.Show()
            WindowDetailDirectorAddress.Title = "Edit Director Address Employer"
            WindowDetailDirectorAddress.Show()
            'ClearInput()
            With ObjDetailAddressDirectorEmployer
                Director_cmb_emp_kategoriaddress.SetValueAndFireSelect(.Address_Type)
                Director_emp_Address.Text = .Address
                Director_emp_Town.Text = .Town
                Director_emp_City.Text = .City
                Director_emp_Zip.Text = .Zip
                If Not IsNothing(.Country_Code) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                    If DataRowTemp IsNot Nothing Then
                        Director_cmb_emp_kodenegara.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                ' Director_cmb_emp_kodenegara.SetValueAndFireSelect(.Country_Code)
                Director_emp_State.Text = .State
                Director_emp_Comment_Address.Text = .Comments
            End With
        End If
    End Sub

    Protected Sub GridCommandPhoneCorporate(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhone(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordAddress(id As String)
        ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddress Is Nothing Then

            PanelDetailAddress.Show()
            WindowDetailAddress.Title = "Detail Address"
            WindowDetailAddress.Show()

            With ObjDetailAddress
                DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
    End Sub
    Private Sub DetailRecordAddressEmployer(id As String)
        ObjDetailAddressEmployer = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddressEmployer Is Nothing Then

            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Show()
            FormPanelAddressDetail.Hide()
            FormPanelAddressDetailEmployer.Hide()
            WindowDetailAddress.Title = "Detail Address Employer"
            WindowDetailAddress.Show()

            With ObjDetailAddressEmployer
                DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddressEmployer.Text = .Address
                DspTownEmployer.Text = .Town
                DspCityEmployer.Text = .City
                DspZipEmployer.Text = .Zip
                Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspStateEmployer.Text = .State
                DspcommentsAddressEmployer.Text = .Comments
            End With
        End If
    End Sub
    Private Sub DetailRecordAddressDirector(id As String)
        ObjDetailAddressDirector = ListAddressDetailDirector.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddressDirector Is Nothing Then

            PanelAddressDirector.Show()
            PanelAddressDirectorEmployer.Hide()
            'agam 22102020
            WindowDetailDirectorAddress.Title = "Detail Director Address"
            WindowDetailDirectorAddress.Hidden = False
            FP_Address_Director.Hidden = True
            FP_Address_Emp_Director.Hidden = True
            'WindowDetailAddress.Show()
            ' ClearInput()
            With ObjDetailAddressDirector
                DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAlamatDirector.Text = .Address
                DspKecamatanDirector.Text = .Town
                DspKotaKabupatenDirector.Text = .City
                DspKodePosDirector.Text = .Zip
                DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspProvinsiDirector.Text = .State
                DspCatatanAddressDirector.Text = .Comments
            End With
        End If
    End Sub
    Private Sub DetailRecordAddressDirectorEmployer(id As String)
        ObjDetailAddressDirectorEmployer = ListAddressDetailDirectorEmployer.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddressDirectorEmployer Is Nothing Then

            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Show()
            'Daniel 2021 Jan 4
            FP_Address_Director.Hide()
            'WindowDetailAddress.Title = "Detail Director Address Employer"
            'WindowDetailAddress.Show()
            WindowDetailDirectorAddress.Title = "Detail Director Address Employer"
            WindowDetailDirectorAddress.Show()
            'end 2021 Jan 4
            'ClearInput()
            'agam 22102020
            'Daniel 2021 Jan 12
            FP_Address_Emp_Director.Hide()
            'FormPanelAddressDetail.Hidden = True
            'FormPanelAddressDetailEmployer.Hidden = True
            'end 2021 Jan 12


            With ObjDetailAddressDirectorEmployer
                DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAlamatDirectorEmployer.Text = .Address
                DspKecamatanDirectorEmployer.Text = .Town
                DspKotaKabupatenDirectorEmployer.Text = .City
                DspKodePosDirectorEmployer.Text = .Zip
                DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspProvinsiDirectorEmployer.Text = .State
                DspCatatanAddressDirectorEmployer.Text = .Comments
            End With
        End If
    End Sub

    Private Sub DeleteRecordDetailPhoneCorporate(id As String)
        BtnAddNewPhoneCorporate_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Phone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not objDelete Is Nothing Then
            ListPhoneDetail.Remove(objDelete)
            WindowDetailPhone.Hide()
            FormPanelPhoneDetail.Hide()
            BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
        End If
    End Sub

    Private Sub LoadDataEditPhoneCorporate(id As String)
        ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhone Is Nothing Then
            FormPanelPhoneDetail.Show()
            WindowDetailPhone.Title = "Edit Phone Corporate"
            WindowDetailPhone.Show()
            'ClearInput()
            With ObjDetailPhone
                Cmbtph_contact_type.SetValueAndFireSelect(.Tph_Contact_Type)
                Cmbtph_communication_type.SetValueAndFireSelect(.Tph_Communication_Type)
                Txttph_country_prefix.Text = .tph_country_prefix
                Txttph_number.Text = .tph_number
                Txttph_extension.Text = .tph_extension
                TxtcommentsPhone.Text = .comments
            End With
        End If
    End Sub

    Protected Sub GridCommandAddressCorporate(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddress(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DeleteRecordDetailAddressCorporate(id As String)
        BtnAddNewAddressCorporate_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Address = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not objDelete Is Nothing Then
            ListAddressDetail.Remove(objDelete)
            WindowDetailAddress.Hide()
            FormPanelAddressDetail.Hide()
            BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
        End If
    End Sub

    Private Sub LoadDataEditAddressCorporate(id As String)
        ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddress Is Nothing Then
            FormPanelAddressDetail.Show()
            WindowDetailAddress.Title = "Edit Address Corporate"
            WindowDetailAddress.Show()
            'ClearInput()
            With ObjDetailAddress
                CmbAddress_type.SetValueAndFireSelect(.Address_Type)
                TxtAddress.Text = .Address
                TxtTown.Text = .Town
                TxtCity.Text = .City
                TxtZip.Text = .Zip
                If Not IsNothing(.Country_Code) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                    If DataRowTemp IsNot Nothing Then
                        Cmbcountry_code.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                ' Cmbcountry_code.SetValueAndFireSelect(.Country_Code)
                TxtState.Text = .State
                TxtcommentsAddress.Text = .Comments
            End With
        End If
    End Sub
#End Region

    Function getDataRowFromDB(strQuery As String) As DataRow
        Try
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Return drResult
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
            Return Nothing
        End Try
    End Function
End Class
