﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="GenerateSARAdd.aspx.vb" Inherits="goAML_GenerateSARAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        Ext.grid.plugin.SelectionMemory.override({
            memoryRestoreState: function () {
                this.selModel.suspendEvents();
                this.callParent();
                this.selModel.resumeEvents();
            }
        });

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:Window ID="WindowReportIndicator" Layout="AnchorLayout" Title="Indikator" runat="server" Modal="true" Hidden="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:ComboBox ID="sar_reportIndicator" runat="server" FieldLabel="Indikator" DisplayField="Keterangan" ValueField="Kode" EmptyText="Pilih Salah Satu" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreReportIndicator" OnReadData="ReportIndicator_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server" ID="Model356">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.4, height: size.height * 0.4});" />

            <Resize Handler="#{WindowReportIndicator}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="BtnsaveReportIndicator" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">

                <DirectEvents>
                    <Click OnEvent="btnSaveReportIndicator_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancelReportIndicator" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancelreportIndicator_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:Window ID="WindowAttachment" Layout="AnchorLayout" Title="Dokumen" runat="server" Modal="true" Hidden="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:FileUploadField ID="FileDoc" runat="server" FieldLabel="File Document" AnchorHorizontal="100%">
            </ext:FileUploadField>
            <ext:DisplayField ID="txtFileName" runat="server" FieldLabel="File Name" AnchorHorizontal="100%">
            </ext:DisplayField>
            <ext:TextArea ID="txtKeterangan" runat="server" FieldLabel="Description" AnchorHorizontal="100%">
            </ext:TextArea>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.4, height: size.height * 0.4});" />

            <Resize Handler="#{WindowAttachment}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="BtnsaveAttachment" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">

                <DirectEvents>
                    <Click OnEvent="btnSaveAttachment_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancelAttachment" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancelAttachment_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:FormPanel ID="FormPanelInput" runat="server" ButtonAlign="Center" Title="" BodyStyle="padding:20px" AutoScroll="true">
        <Items>
            <%-- Edit 27-Dec-2022, Felix. Ganti jenis Dropdown --%>
          <%--  <ext:ComboBox ID="sar_CIF" runat="server" FieldLabel="CIF No" DisplayField="Keterangan" ValueField="Kode" EmptyText="Pilih Salah Satu" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreCIF" OnReadData="CIF_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server" ID="Model3">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
            </ext:ComboBox>--%>

            <ext:Button ID="btn_OpenWindowCustomer" runat="server" Icon="Zoom" Text="Search Data Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_Customer_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:TextField runat="server" ID="txt_CIF" FieldLabel="CIF" EnforceMaxLength="True" maxlength="30" AnchorHorizontal="40%" ReadOnly="true" />
            <ext:TextField runat="server" ID="txt_Cust_Name" FieldLabel="Name" EnforceMaxLength="True" maxlength="100" AnchorHorizontal="40%" ReadOnly="true"/>
            <%-- End 27-Dec-2022, Felix. Ganti jenis Dropdown --%>
            
            <ext:DateField runat="server" ID="sar_DateFrom" FieldLabel="Date From" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false">
                <%--<DirectEvents>
                    <Change OnEvent="sar_DateFrom_DirectSelect"></Change>
                </DirectEvents>--%>
            </ext:DateField>
            <ext:DateField runat="server" ID="sar_DateTo" FieldLabel="Date To" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" >
                <%--<DirectEvents>
                    <Change OnEvent="sar_DateTo_DirectSelect"></Change>
                </DirectEvents>--%>
            </ext:DateField>
            <ext:Button ID="BtnSaveDetail" runat="server" Text="Search Transaction">
                <DirectEvents>
                    <Click OnEvent="BtnSearch_DirectEvent">
                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:DisplayField ID="TextField5" runat="server" AnchorHorizontal="80%" />
            <ext:Checkbox ID="sar_IsSelectedAll" runat="server" FieldLabel="Pilih Semua Transaksi? " />
            <ext:Panel runat="server" ID="PanelChecked" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaction Data" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                <Items>
                    <ext:GridPanel ID="GridPaneldetail" runat="server" AutoScroll="true" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <%--<ext:Store ID="StoreTransaction" runat="server" IsPagingStore="true" PageSize="500">--%>
                            <ext:Store ID="StoreTransaction" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadDataTrans" RemotePaging="true" ClientIDMode="Static">
                                    <Sorters>
                                </Sorters>
                                <Proxy>
                                    <ext:PageProxy></ext:PageProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="ModelDetail" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="NO_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Valid" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NoTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NoRefTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TipeTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LokasiTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KeteranganTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DateTransaction" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="AccountNo" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NamaTeller" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NamaPejabat" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TglPembukuan" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="CaraTranDilakukan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CaraTranLain" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DebitCredit" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="OriginalAmount" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDR" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn28" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column8" runat="server" DataIndex="Valid" Text="Valid Counter Party" Width="150"></ext:Column>
                                <ext:Column ID="colStoreProcedure" runat="server" DataIndex="NoTran" Text="Nomor Transaksi" Width="150"></ext:Column>
                                <ext:Column ID="colIsuseProcessDate" runat="server" DataIndex="NoRefTran" Text="No Ref Transaksi" Width="150"></ext:Column>
                                <ext:Column ID="colTipeTran" runat="server" DataIndex="TipeTran" Text="Data Source" Width="150"></ext:Column>
                                <ext:Column runat="server" DataIndex="LokasiTran" Text="Lokasi Transaksi" Width="150"></ext:Column>
                                <ext:Column ID="Colorder" runat="server" DataIndex="KeteranganTran" Text="Keterangan Transaksi" Width="170"></ext:Column>
                                <ext:DateColumn ID="Column2" runat="server" DataIndex="DateTransaction" Text="Tanggal Transaksi" Width="150"></ext:DateColumn>
                                <ext:Column ID="colAccountNo" runat="server" DataIndex="AccountNo" Text="No Akun" Width="150"></ext:Column>
                                <ext:Column runat="server" DataIndex="NamaTeller" Text="Nama Teller" Width="120"></ext:Column>
                                <ext:Column ID="Column1" runat="server" DataIndex="NamaPejabat" Text="Nama Pejabat" Width="120"></ext:Column>
                                <ext:DateColumn ID="Column3" runat="server" DataIndex="TglPembukuan" Text="Tanggal Pembukuan" Width="160"></ext:DateColumn>
                                <ext:Column runat="server" DataIndex="CaraTranDilakukan" Text="Cara Transaksi Dilakukan" Width="185"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="CaraTranLain" Text="Cara Transaksi Lain" Width="150"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="DebitCredit" Text="Debit Credit" Width="150"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="OriginalAmount" Text="Original Amount" Width="150"></ext:Column>
                                <ext:Column ID="Column5" runat="server" DataIndex="IDR" Text="Nilai Transaksi(IDR)" Width="150"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server" Remote="true"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                                <%--                                <Items>
                                       <ext:Button runat="server" ID="BtnExport" Text="Export" AutoPostBack="true" OnClick="CreateExcel2007WithData"  />
                                </Items>--%>
                                <Items>
                                    <ext:Button runat="server" ID="BtnExport" Text="Export" Icon="Disk">
                                        <DirectEvents>
                                            <Click OnEvent="CreateExcel2007WithData">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <SelectionModel>
                            <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi">
                                <DirectEvents>
                                    <Select OnEvent="OnSelect_DirectEvent">
                                        <ExtraParams>
                                            <ext:Parameter Name="NO_ID" Value="record.data.NO_ID" Mode="Raw" />
                                        </ExtraParams>
                                    </Select>
                                    <Deselect OnEvent="OnDeSelect_DirectEvent">
                                        <ExtraParams>
                                            <ext:Parameter Name="NO_ID" Value="record.data.NO_ID" Mode="Raw" />
                                        </ExtraParams>
                                    </Deselect>
                                </DirectEvents>
                            </ext:CheckboxSelectionModel>
                        </SelectionModel>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            <ext:Panel runat="server" ID="PanelnoCheked" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaction Data" BodyStyle="padding:10px" Margin="10" Collapsible="true" Hidden="true">
                <Items>
                    <ext:GridPanel ID="GridPanel1" runat="server" AutoScroll="true" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <%--<ext:Store ID="StoreTransaksiNoChecked" runat="server" IsPagingStore="true" PageSize="10">--%>
                            <ext:Store ID="StoreTransaksiNoChecked"  runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadDataTransNochecked" RemotePaging="true" ClientIDMode="Static">
                                    <Sorters>
                                </Sorters>
                                <Proxy>
                                    <ext:PageProxy></ext:PageProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="NO_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Valid" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NoTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NoRefTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TipeTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LokasiTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="KeteranganTran" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DateTransaction" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="AccountNo" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NamaTeller" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NamaPejabat" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TglPembukuan" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="CaraTranDilakukan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CaraTranLain" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DebitCredit" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="OriginalAmount" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IDR" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column11" runat="server" DataIndex="Valid" Text="Valid Counter Party" Width="150"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="NoTran" Text="Nomor Transaksi" Width="150"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="NoRefTran" Text="No Ref Transaksi" Width="150"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="TipeTran" Text="Tipe Transaksi" Width="150"></ext:Column>
                                <ext:Column runat="server" DataIndex="LokasiTran" Text="Lokasi Transaksi" Width="150"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="KeteranganTran" Text="Keterangan Transaksi" Width="170"></ext:Column>
                                <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="DateTransaction" Text="Tanggal Transaksi" Width="150"></ext:DateColumn>
                                <ext:Column ID="Column16" runat="server" DataIndex="AccountNo" Text="No Akun" Width="150"></ext:Column>
                                <ext:Column runat="server" DataIndex="NamaTeller" Text="Nama Teller" Width="120"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="NamaPejabat" Text="Nama Pejabat" Width="120"></ext:Column>
                                <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="TglPembukuan" Text="Tanggal Pembukuan" Width="160"></ext:DateColumn>
                                <ext:Column runat="server" DataIndex="CaraTranDilakukan" Text="Cara Transaksi Dilakukan" Width="185"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="CaraTranLain" Text="Cara Transaksi Lain" Width="150"></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="DebitCredit" Text="Debit Credit" Width="150"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="OriginalAmount" Text="Original Amount" Width="150"></ext:Column>
                                <ext:Column ID="Column21" runat="server" DataIndex="IDR" Text="Nilai Transaksi(IDR)" Width="150"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server" Remote="true"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                        <TopBar>
                            <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true">
                                <%--                                <Items>
                                       <ext:Button runat="server" ID="BtnExport" Text="Export" AutoPostBack="true" OnClick="CreateExcel2007WithData"  />
                                </Items>--%>
                                <Items>
                                    <ext:Button runat="server" ID="BtnExportNoChecked" Text="Export" Icon="Disk">
                                        <DirectEvents>
                                            <Click OnEvent="CreateExcel2007WithDataNoChecked">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            <ext:DateField ID="TanggalLaporan" runat="server" Format="dd-MMM-yyyy" AllowBlank="false" FieldLabel="Tanggal Laporan" AnchorHorizontal="40%" />
            <ext:ComboBox ID="sar_jenisLaporan" runat="server" FieldLabel="Jenis Laporan" DisplayField="Keterangan" ValueField="Kode" EmptyText="Pilih Salah Satu" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreJenisLaporan" OnReadData="JenisLaporan_ReadData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server" ID="Model8">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <DirectEvents>
                    <Change OnEvent="sar_jenisLaporan_DirectSelect"></Change>
                </DirectEvents>
            </ext:ComboBox>
            <ext:TextField ID="NoRefPPATK" runat="server" FieldLabel="No Ref PPATK" AnchorHorizontal="80%" Hidden="true" />
            <ext:TextArea ID="alasan" runat="server" FieldLabel="Alasan" AnchorHorizontal="80%" AllowBlank="false" />

            <ext:Panel runat="server" ID="PanelReportIndicator" Hidden="true" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Indikator" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                <Items>
                    <ext:GridPanel ID="GridPanelReportIndicator" runat="server" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="BtnAddReportIndikator" Text="Tambah Indicator" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_addIndicator_click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="500"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreReportIndicatorData" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model355" IDProperty="PK_Report_Indicator">
                                        <Fields>
                                            <ext:ModelField Name="PK_Report_Indicator" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Indicator" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn88" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column312" runat="server" DataIndex="FK_Indicator" Text="Kode" Flex="1"></ext:Column>
                                <ext:Column ID="Column313" runat="server" DataIndex="Keterangan" Text="Keterangan" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn87" runat="server" Text="Action" Flex="1">

                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>

                                        <Command OnEvent="GridcommandReportIndicator">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>

                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Report_Indicator" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>



                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>

            <ext:Panel runat="server" ID="PanelAttachment" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Attachment Document" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                <Items>
                    <ext:GridPanel ID="GridPanelAttachment" runat="server" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="ButtonAttachment" Text="Tambah Dokumen" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_addAttachment_click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="500"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreAttachment" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model1" IDProperty="PK_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FileName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column6" runat="server" DataIndex="FileName" Text="File Name" Flex="1">
                                    <Commands>
                                        <ext:ImageCommand Icon="DiskDownload" CommandName="Download" ToolTip-Text="Download Dokumen"></ext:ImageCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridcommandAttachment">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="Keterangan" Text="Description" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAttachment" runat="server" Text="Action" Flex="1">

                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>

                                        <Command OnEvent="GridcommandAttachment">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>

                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
        </Items>

        <Buttons>
            <ext:Button ID="Button1" runat="server" Icon="Disk" Text="Generate">
                <Listeners>
                    <Click Handler="if (!#{FormPanelInput}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="BtnGenerate_DirectEvent">
                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancelDetail" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_DirectEvent">
                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

        </Buttons>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>

        </Items>
        <Buttons>

            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <%--Add 27-Dec-2022, Felix. Tambah Window Search Data Customer--%>
    <ext:Window ID="window_Import_Customer" runat="server" Modal="true" Hidden="true" BodyStyle="padding:0px" AutoScroll="true" ButtonAlign="Center" MinHeight="430">
        <Items>
            <ext:GridPanel ID="gp_Import_Customer" runat="server" ClientIDMode="Static">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store_Import_Customer" runat="server" IsPagingStore="true" RemoteFilter="true"  RemoteSort="true" 
                        OnReadData="Store_ReadData_Import_Customer" RemotePaging="true" ClientIDMode="Static" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="model_Import_Customer" IDProperty="CIFNo">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="CUSTOMERNAME" Type="String"></ext:ModelField>
                                    <%--<ext:ModelField Name="RISK_RATING_NAME" Type="String"></ext:ModelField>--%>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="filterHeader_Import_Customer" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumber_Import_Customer" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="col_Import_Customer_CIFNo" runat="server" DataIndex="CIFNo" Text="CIF No" Width="150"></ext:Column>
                        <ext:Column ID="col_Import_Customer_CUSTOMERNAME" runat="server" DataIndex="CUSTOMERNAME" Text="Customer Name" Width="700"></ext:Column>
                        <%--<ext:Column ID="col_Import_Customer_FK_AML_RISK_CODE" runat="server" DataIndex="RISK_RATING_NAME" Text="Risk Rating" Flex="1"></ext:Column>--%>
                        <ext:CommandColumn ID="cc_Import_Customer" runat="server" Text="Action">

                        <Commands>
                            <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
                                <ToolTip Text="Select"></ToolTip>
                            </ext:GridCommand>
                        </Commands>

                        <DirectEvents>
                            <Command OnEvent="gc_Import_Customer">
                                <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                                <ExtraParams>
                                    <ext:Parameter Name="CIFNo" Value="record.data.CIFNo" Mode="Raw"></ext:Parameter>
                                    <ext:Parameter Name="CUSTOMERNAME" Value="record.data.CUSTOMERNAME" Mode="Raw"></ext:Parameter>
                                </ExtraParams>
                            </Command>
                        </DirectEvents>
                    </ext:CommandColumn>
                </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_Import_Customer" runat="server" HideRefresh="false" >
                        <Items>  
                            <%--<ext:Button runat="server" Text="Close Picker" ID="Button5">
                                <Listeners>
                                    <Click Handler="#{window_Import_Party}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>--%>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.9});" />
            <Resize Handler="#{window_Import_Party}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_Import_Customer_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="Btn_Import_Customer_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%--End 27-Dec-2022--%>
</asp:Content>
