﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="GOAML_ValidateXML.aspx.vb" Inherits="GOAML_ValidateXML" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        //Show or hide Selection Model gridpanel watchlist
        //var showSMWatchlist = function () {
        //    var grid = App.gp_DownloadFromWatchlist,
        //        sm = grid.getSelectionModel();



        //    if (sm.injectCheckboxWatchlist === false) {
        //        sm.injectCheckboxWatchlist = 0;
        //        sm.addCheckbox(grid.getView());
        //    } else {
        //        grid.getView().headerCt.items.getAt(0).show();
        //    }
        //};



        //var hideSMWatchlist = function () {
        //    App.gp_DownloadFromWatchlist.getView().headerCt.items.getAt(0).hide();
        //};
        //var updateMask = function (text) {
        //    App.df_ScreeningStatus.setValue(text);
        //};
    </script>
    <style type="text/css">
        .text-wrapper .x-form-display-field {
            word-break: break-word;
            word-wrap: break-word;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

  

    <%-- Pop Up Window Screening Add --%>
    
    <%-- End of Pop Up Window Screening Upload --%>

    <ext:FormPanel ID="FormPanelInput" BodyPadding="5" AutoScroll="true" Layout="AnchorLayout" AnchorHorizontal="90%" ButtonAlign="Center" runat="server" Height="300" Title="">
        <Items>
           <%-- <ext:Panel runat="server" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="pnlSiPendarReport" Border="true" BodyStyle="padding:10px">
                <Content>--%>
                    <ext:TextArea ID="txt_XML" LabelWidth="250" Height ="500" runat="server" FieldLabel="XML" AnchorHorizontal="80%" Padding="10"></ext:TextArea>
                    <ext:TextArea ID="txt_InvalidMessage" AllowBlank="true" LabelWidth="250" Height ="250" runat="server" FieldLabel="Invalid Message" AnchorHorizontal="80%" ReadOnly ="true" Padding="10"></ext:TextArea>
               <%-- </Content>
            </ext:Panel>--%>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Validate" ClientIDMode="Static" runat="server" Text="Validate" Enabled="false" Icon="PlayGreen">
                <DirectEvents>
                    <Click OnEvent="btn_Validate_Click">
                        <%--<EventMask ShowMask="true"></EventMask>--%>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <%-- <ext:Button ID="btn_Save" ClientIDMode="Static" runat="server" Text="Save Selected to SiPendar Report" Enabled="false" Icon="DiskBlack" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btn_Save_Click">
                        <EventMask ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>--%>
            <ext:Button ID="btn_Cancel" runat="server" Text="Clear" Icon="PictureEmpty">
                <DirectEvents>
                    <Click OnEvent="btn_Cancel_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

</asp:Content>
