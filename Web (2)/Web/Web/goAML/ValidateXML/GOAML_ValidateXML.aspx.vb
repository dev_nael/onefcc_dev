﻿Imports System.Data
Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Drawing
Imports System.IO
Imports System.Reflection
Imports System.Threading
Imports Elmah
Imports NawaDAL
Imports OfficeOpenXml
Imports OfficeOpenXml.Style
Imports System.Xml.Schema
Imports System.Xml

Partial Class GOAML_ValidateXML
    Inherits ParentPage

    Const PATH_TEMP_DIR As String = "~\temp\"


    Public Property PK_Request_ID() As Long
        Get
            Return Session("GOAML_ValidateXML.PK_Request_ID")
        End Get
        Set(ByVal value As Long)
            Session("GOAML_ValidateXML.PK_Request_ID") = value
        End Set
    End Property


    Private Sub GOAML_ValidateXML_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        Me.ActionType = NawaBLL.Common.ModuleActionEnum.view
    End Sub

    Private Sub GOAML_ValidateXML_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = Me.ObjModule.ModuleLabel


                ClearData()

            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ClearData()
        Try
            txt_XML.Value = Nothing
            txt_InvalidMessage.Value = Nothing

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_Cancel_Click()
        Try
            ClearData()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


#Region "Start Validate"


    Protected Sub btn_Validate_Click()
        Try
            'Dim doc As New XmlDocument()
            'doc.Load(txt_XML.Value)
            'Dim strxsd As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT TOP 1 gatsx.XsdXMLData FROM goAML_Template_Schema_Xsd AS gatsx WHERE gatsx.[Active]=1", Nothing)
            'Dim objxsdreader As XmlReader = XmlReader.Create(New StringReader(strxsd))
            'doc.Schemas.Add(Nothing, objxsdreader)
            'Dim errorBuilder As New XmlValidationErrorBuilder()
            'doc.Validate(New ValidationEventHandler(AddressOf errorBuilder.ValidationEventHandler))
            'Dim errorsText As List(Of String) = errorBuilder.GetErrors()

            If String.IsNullOrEmpty(txt_XML.Value) Then
                Throw New ApplicationException("XML data is required!")
            End If

            '' Create XML into file
            Dim PathParameter As String
            PathParameter = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT ParameterValue FROM goAML_Ref_ReportGlobalParameter WHERE PK_GlobalReportParameter_ID = 20", Nothing)
            'PathParameter = "F:\Temp\CreateFileToValidate"
            Dim Dirpath = PathParameter & "\"
            If Not Directory.Exists(Dirpath) Then
                Directory.CreateDirectory(Dirpath)
            End If

            Dim filexml As String
            filexml = Dirpath & "XMLToValidate.xml" '' Edited by FElix 28 Dec 2020, tambah tanggal generate

            If IO.File.Exists(filexml) Then
                IO.File.Delete(filexml)
            End If

            Dim objdoc As New XmlDocument
            objdoc.LoadXml(txt_XML.Value)
            objdoc.PreserveWhitespace = False
            objdoc.Save(filexml)
            '' End create XML

            Dim listXMLSchemaException As New List(Of XmlSchemaException)
            listXMLSchemaException = GetSchemaErrors(filexml)

            If listXMLSchemaException.Count > 0 Then
                txt_InvalidMessage.Value = Nothing

                For Each item As XmlSchemaException In listXMLSchemaException
                    txt_InvalidMessage.Value = txt_InvalidMessage.Value + "Line Number,Position : (" + item.LineNumber.ToString + "," + item.LinePosition.ToString + ") Message : " + item.Message.ToString + Environment.NewLine
                Next
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Shared Function GetSchemaErrors(strXML As String) As List(Of XmlSchemaException)
        Dim errors As New List(Of XmlSchemaException)()

        Dim settings As New XmlReaderSettings()
        Dim strxsd As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT TOP 1 gatsx.XsdXMLData FROM goAML_Template_Schema_Xsd AS gatsx WHERE gatsx.[Active]=1", Nothing)
        Dim objxsdreader As XmlReader = XmlReader.Create(New StringReader(strxsd))
        settings.Schemas = New XmlSchemaSet()
        settings.Schemas.Add(Nothing, objxsdreader)
        settings.ValidationType = ValidationType.Schema

        AddHandler settings.ValidationEventHandler,
        Sub(sender, args) errors.Add(args.Exception)

        Dim reader As XmlReader = XmlReader.Create(strXML, settings)
        'Dim reader As XmlReader = XmlReader.Create("F:\Temp\20220427-20210323-001-TKLIB\20220401-20210323-202204271054AM-TKLIB-TRN23032021ASD-[30959].xml", settings)
        Dim doc = New XmlDocument
        doc.Load(reader)

        reader.Close()

        Return errors
    End Function

#End Region

End Class

Public Class XmlValidationErrorBuilder
    Private _errors As New List(Of String)
    Public Sub ValidationEventHandler(ByVal sender As Object, ByVal args As ValidationEventArgs)
        If args.Severity = XmlSeverityType.Error Then
            _errors.Add(args.Message)

        End If
    End Sub
    Public Function GetErrors() As List(Of String)
        Return _errors
    End Function
End Class

