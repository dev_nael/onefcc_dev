﻿Imports System.IO
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDAL
Imports NawaDevBLL
Imports NawaDevDAL
Imports System.Data.SqlClient
Imports System.Data
Imports OfficeOpenXml
Imports System.Xml.Schema
Imports System.Xml
Imports Ionic.Zip

Partial Class goAML_GenerateSiPesat
    Inherits Parent

    Public objFormModuleView As FormModuleView
    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("goAML_GenerateSiPesat.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_GenerateSiPesat.ObjModule") = value
        End Set
    End Property
    Public Property ObjGeneratedSiPesat() As AML_Sipesat_GeneratedFileList
        Get
            Return Session("goAML_GenerateSiPesat.ObjGeneratedSiPesat")
        End Get
        Set(ByVal value As AML_Sipesat_GeneratedFileList)
            Session("goAML_GenerateSiPesat.ObjGeneratedSiPesat") = value
        End Set
    End Property
    Public Property strWhereClause() As String
        Get
            Return Session("goAML_GenerateSiPesat.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSiPesat.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("goAML_GenerateSiPesat.strSort")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSiPesat.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("goAML_GenerateSiPesat.indexStart")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSiPesat.indexStart") = value
        End Set
    End Property

    Public Property QueryTable() As String
        Get
            Return Session("goAML_GenerateSiPesat.Table")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSiPesat.Table") = value
        End Set
    End Property

    Public Property QueryField() As String
        Get
            Return Session("goAML_GenerateSiPesat.Field")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSiPesat.Field") = value
        End Set
    End Property

    Public Property stringModuleID As String
        Get
            Return Session("goAML_GenerateSiPesat.StringModuleID")
        End Get
        Set(value As String)
            Session("goAML_GenerateSiPesat.StringModuleID") = value
        End Set
    End Property
    Private Sub Parameterview_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub
    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()

                Dim Moduleid As String = Request.Params("ModuleID")
                stringModuleID = Moduleid
                Try
                    Dim intmodule As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                    Me.ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intmodule)

                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    FormPanelInput.Title = ObjModule.ModuleLabel & " - Generate"

                    LoadData()
                    LoadForm(ObjModule)

                    If Not Ext.Net.X.IsAjaxRequest Then
                        cboExportExcel.SelectedItem.Text = "Excel"
                    End If

                Catch ex As Exception
                    ErrorSignal.FromCurrentContext.Raise(ex)
                    Net.X.Msg.Alert("Error", ex.Message).Show()
                End Try
            End If


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub ClearSession()
        ObjModule = Nothing
        ObjGeneratedSiPesat = Nothing
        stringModuleID = ""
        QueryField = ""
        QueryTable = ""
        indexStart = ""
        strOrder = ""
        strWhereClause = ""
    End Sub
    Private Sub LoadData()
        Dim IDData As String = Request.Params("ID")
        Dim IDReq As String = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)

        ObjGeneratedSiPesat = GenerateSiPesatBLL.GetGeneratedSiPesatByID(IDReq)

        If ObjGeneratedSiPesat IsNot Nothing Then
            With ObjGeneratedSiPesat
                lblIDPJK.Text = .PK_IDPJK
                lblPeriode.Text = .Period
                lblTotalValid.Text = .Total_Valid
                lblTotalInvalid.Text = .Total_Invalid
                lblStatusUpload.Text = NawaDevBLL.GenerateSiPesatBLL.getStatusReportByPK(.Status_Report)
            End With
        End If
    End Sub
    Sub LoadForm(objmodule As NawaDAL.Module)
        objFormModuleView.ModuleID = objmodule.PK_Module_ID
        objFormModuleView.ModuleName = objmodule.ModuleName
        objFormModuleView.AddField("PK_Report_ID", "ID", 1, True, False, NawaBLL.Common.MFieldType.INTValue, , , , , )
        objFormModuleView.AddField("PK_ID", "IDPJK", 2, False, True, NawaBLL.Common.MFieldType.INTValue,,,,, )
        objFormModuleView.AddField("PK_Customer_Type_ID", "Kode Nasabah", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
        objFormModuleView.AddField("Name", "Nama Nasabah", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
        objFormModuleView.AddField("Place_Birth", "Tempat Lahir", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
        objFormModuleView.AddField("PK_Date_Birth", "Tanggal Lahir", 6, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )
        objFormModuleView.AddField("Address", "Alamat", 7, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
        objFormModuleView.AddField("No_KTP", "NIK/Nomor KTP", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
        objFormModuleView.AddField("PK_Other_ID", "Nomor Identitas Lain", 9, False, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
        objFormModuleView.AddField("No_CIF", "Nomor CIF/Kepesertaan", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
        objFormModuleView.AddField("No_NPWP", "Nomor NPWP", 11, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
        objFormModuleView.AddField("Period", "Period", 12, False, False, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
        objFormModuleView.AddField("Jenis_Informasi", "Jenis Informasi", 13, False, False, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
        objFormModuleView.AddField("IsValid", "Valid", 14, False, False, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
        objFormModuleView.AddField("Status", "Status", 15, False, False, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
        objFormModuleView.AddField("LastUpdateDate", "Last Update Date", 16, False, False, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )

    End Sub
    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter
            Me.strOrder = strsort

            If strOrder = "" Then
                strOrder = "LastUpdateDate DESC"
            End If

            QueryTable = "AML_Sipesat_Report"
            QueryField = "PK_Report_ID,PK_ID,PK_Customer_Type_ID,Name,Place_Birth,PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP,Period,Jenis_Informasi,IsValid,Status,LastUpdateDate"

            Dim DataPaging As DataTable = SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, 1, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            e.Total = inttotalRecord
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSave_DirectClick(sender As Object, e As EventArgs)

        Try
            'Update Status List Of Generate
            NawaDevBLL.GenerateSiPesatBLL.GenerateData(ObjModule, ObjGeneratedSiPesat.PK_Sipesat_GeneratedFileList_ID)

            Dim objfileinfo As IO.FileInfo = Nothing

            'Export File XML/CSV/Excel
            Dim namefile As String = ""
            If ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 1 Then
                'namefile = "SIPESAT_" + ObjGeneratedSiPesat.PK_IDPJK.ToString + "_IN_" + Date.Now.ToString("ddMMyyyy") + "_1"
                namefile = "SIPESAT_" + ObjGeneratedSiPesat.PK_IDPJK.ToString + "_IN_" + ObjGeneratedSiPesat.Period.ToString + "_" + Date.Now.ToString("ddMMyyyy") + "_1"
            ElseIf ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 2 Then
                'namefile = "SIPESAT_" + ObjGeneratedSiPesat.PK_IDPJK.ToString + "_TW_" + Date.Now.ToString("ddMMyyyy") + "_1"
                namefile = "SIPESAT_" + ObjGeneratedSiPesat.PK_IDPJK.ToString + "_TW_" + ObjGeneratedSiPesat.Period.ToString + "_" + Date.Now.ToString("ddMMyyyy") + "_1"
            Else
                'namefile = "SIPESAT_" + ObjGeneratedSiPesat.PK_IDPJK.ToString + "_" + Date.Now.ToString("ddMMyyyy") + "_1"
                namefile = "SIPESAT_" + ObjGeneratedSiPesat.PK_IDPJK.ToString + "_" + ObjGeneratedSiPesat.Period.ToString + "_" + Date.Now.ToString("ddMMyyyy") + "_1"
            End If

            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                    'strOrder = "LastUpdateDate DESC"
                    'QueryTable = "AML_Sipesat_Report"
                    'QueryField = "PK_Report_ID,PK_ID,PK_Customer_Type_ID,Name,Place_Birth,PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP,Period,Jenis_Informasi,IsValid,Status,LastUpdateDate"
                    'strWhereClause = "Period = '" + ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1"
                    'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                    'Update Daniel 20210521
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_ID,PK_Customer_Type_ID,Name,Place_Birth,PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP from AML_Sipesat_Report where Period = '" & ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1", Nothing)
                        'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_ID,PK_Customer_Type_ID,Name,Place_Birth,CONVERT(varchar, PK_Date_Birth, 105) PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP from AML_Sipesat_Report where Period = '" & ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1", Nothing)
                        Dim intJumlahField As Integer = 1000000
                        Dim stringjumlahrow As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT ParameterValue FROM dbo.goAML_Ref_ReportGlobalParameter WHERE PK_GlobalReportParameter_ID = 2003", Nothing)
                        If Integer.TryParse(stringjumlahrow, intJumlahField) Then
                            intJumlahField = Integer.Parse(stringjumlahrow)
                            If intJumlahField > 1048570 Then
                                Throw New Exception("Please Check SIPESAT Maximum Excel Data Rows Value On Report Global Parameter")
                            End If
                        Else
                            Throw New Exception("Please Check SIPESAT Maximum Excel Data Rows Value On Report Global Parameter")
                        End If
                        Dim numberoffile As Integer = 0
                        numberoffile = Convert.ToInt32(Math.Ceiling((Convert.ToDecimal(objtbl.Rows.Count()) / Convert.ToDecimal(intJumlahField))))

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As ColumnBase In GridpanelView.ColumnModel.Columns
                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If
                            End If
                        Next

                        Dim dateformat As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT ParameterValue FROM dbo.goAML_Ref_ReportGlobalParameter WHERE PK_GlobalReportParameter_ID = 2002", Nothing)
                        Dim dtTemp As DataTable = objtbl.Clone()
                        For Each item As DataColumn In dtTemp.Columns
                            item.DataType = GetType(String)
                        Next

                        If numberoffile > 1 Then
                            Dim path As String = Server.MapPath("~\goaml\FolderExport\")
                            Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
                            If Not Directory.Exists(Dirpath) Then
                                Directory.CreateDirectory(Dirpath)
                            End If

                            Dim lstFileInZip As New List(Of String)
                            Dim namefilezip As String = ""
                            If ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 1 Then
                                namefilezip = "SIPESAT_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_IN_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & ".zip"
                            ElseIf ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 2 Then
                                namefilezip = "SIPESAT_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_TW_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & ".zip"
                            Else
                                namefilezip = "SIPESAT_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & ".zip"
                            End If
                            namefilezip = Dirpath & namefilezip

                            For i As Integer = 0 To (numberoffile - 1)
                                If ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 1 Then
                                    namefile = "SIPESAT_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_IN_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & "_" & (i + 1) & ".xlsx"
                                ElseIf ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 2 Then
                                    namefile = "SIPESAT_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_TW_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & "_" & (i + 1) & ".xlsx"
                                Else
                                    namefile = "SIPESAT_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & "_" & (i + 1) & ".xlsx"
                                End If
                                tempfilexls = Dirpath & namefile
                                objfileinfo = New IO.FileInfo(tempfilexls)
                                If IO.File.Exists(tempfilexls) Then
                                    IO.File.Delete(tempfilexls)
                                End If
                                If i <> (numberoffile - 1) Then
                                    Dim introw As Integer = 0
                                    For j As Integer = (intJumlahField * i) To ((intJumlahField * (i + 1)) - 1)
                                        dtTemp.ImportRow(objtbl.Rows(j))
                                        If dateformat IsNot Nothing Then
                                            Dim intcolnumber As Integer = 0
                                            For Each item As DataColumn In objtbl.Columns
                                                If item.DataType = GetType(Date) Then
                                                    Dim stringdate As String = ""
                                                    If Not objtbl.Rows(j).IsNull(intcolnumber) Then
                                                        Dim tempdate As DateTime = objtbl.Rows(j).Item(intcolnumber)
                                                        stringdate = tempdate.ToString(dateformat)
                                                    End If

                                                    'If Not DateTime.TryParseExact(stringdate, dateformat,
                                                    '                              System.Globalization.CultureInfo.InvariantCulture,
                                                    '                              System.Globalization.DateTimeStyles.None, tempdate) Then
                                                    '    Throw New Exception("Please Check SIPESAT Format Date Value On Report Global Parameter")
                                                    'End If
                                                    dtTemp.Rows(introw).Item(intcolnumber) = stringdate
                                                End If
                                                intcolnumber = intcolnumber + 1
                                            Next
                                        End If
                                        introw = introw + 1
                                    Next
                                Else
                                    Dim introw As Integer = 0
                                    For j As Integer = (intJumlahField * i) To (objtbl.Rows.Count() - 1)
                                        dtTemp.ImportRow(objtbl.Rows(j))
                                        If dateformat IsNot Nothing Then
                                            Dim intcolnumber As Integer = 0
                                            For Each item As DataColumn In objtbl.Columns
                                                If item.DataType = GetType(Date) Then
                                                    Dim stringdate As String = ""
                                                    If Not objtbl.Rows(j).IsNull(intcolnumber) Then
                                                        Dim tempdate As DateTime = objtbl.Rows(j).Item(intcolnumber)
                                                        stringdate = tempdate.ToString(dateformat)
                                                        'If Not DateTime.TryParseExact(stringdate, dateformat,
                                                        '                              System.Globalization.CultureInfo.InvariantCulture,
                                                        '                              System.Globalization.DateTimeStyles.None, tempdate) Then
                                                        '    Throw New Exception("Please Check SIPESAT Format Date Value On Report Global Parameter")
                                                        'End If
                                                    End If
                                                    dtTemp.Rows(introw).Item(intcolnumber) = stringdate
                                                End If
                                                intcolnumber = intcolnumber + 1
                                            Next
                                        End If
                                        introw = introw + 1
                                    Next
                                End If
                                Using resource As New ExcelPackage(objfileinfo)
                                    Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                                    ws.Cells("A1").LoadFromDataTable(dtTemp, True)
                                    'If dateformat IsNot Nothing Then
                                    '    Dim intcolnumber As Integer = 1
                                    '    For Each item As DataColumn In dtTemp.Columns
                                    '        If item.DataType = GetType(Date) Then
                                    '            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                    '        End If
                                    '        intcolnumber = intcolnumber + 1
                                    '    Next
                                    'End If
                                    ws.Cells(ws.Dimension.Address).AutoFitColumns()
                                    resource.Save()
                                    lstFileInZip.Add(tempfilexls)
                                    Me.EnableViewState = False
                                End Using
                                dtTemp.Clear()
                            Next
                            If lstFileInZip.Count > 0 Then
                                Using zip As New ZipFile()
                                    If IO.File.Exists(namefilezip) Then
                                        IO.File.Delete(namefilezip)
                                    End If

                                    zip.CompressionLevel = CompressionLevel.Level9
                                    zip.AddFiles(lstFileInZip, "")

                                    zip.Save(namefilezip)
                                End Using
                                'buang karena sudah di zip
                                For Each Item1 As String In lstFileInZip
                                    If IO.File.Exists(Item1) Then
                                        IO.File.Delete(Item1)
                                    End If
                                Next
                            End If
                            If IO.File.Exists(namefilezip) Then
                                Response.Clear()
                                Response.ClearHeaders()
                                Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(namefilezip))
                                Response.Charset = ""
                                Response.AddHeader("cache-control", "max-age=0")
                                Me.EnableViewState = False
                                Response.ContentType = "application/zip"
                                Response.BinaryWrite(File.ReadAllBytes(namefilezip))
                                Response.End()
                            Else
                                Throw New Exception("Something Wrong While Downloading Data")
                            End If
                        Else
                            Dim introw As Integer = 0
                            For Each row As DataRow In objtbl.Rows
                                dtTemp.ImportRow(row)
                                If dateformat IsNot Nothing Then
                                    Dim intcolnumber As Integer = 0
                                    For Each item As DataColumn In objtbl.Columns
                                        If item.DataType = GetType(Date) Then
                                            Dim stringdate As String = ""
                                            If Not row.IsNull(intcolnumber) Then
                                                Dim tempdate As DateTime = row.Item(intcolnumber)
                                                stringdate = tempdate.ToString(dateformat)
                                            End If
                                            'If Not DateTime.TryParseExact(tempdate, dateformat,
                                            '                                      System.Globalization.CultureInfo.InvariantCulture,
                                            '                                      System.Globalization.DateTimeStyles.None, tempdate) Then
                                            '    Throw New Exception("Please Check SIPESAT Format Date Value On Report Global Parameter")
                                            'End If
                                            dtTemp.Rows(introw).Item(intcolnumber) = stringdate
                                        End If
                                        intcolnumber = intcolnumber + 1
                                    Next
                                End If
                                introw = introw + 1
                            Next
                            Using resource As New ExcelPackage(objfileinfo)
                                Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                                ws.Cells("A1").LoadFromDataTable(dtTemp, True)
                                'If dateformat IsNot Nothing Then
                                '    Dim intcolnumber As Integer = 1
                                '    For Each item As DataColumn In objtbl.Columns
                                '        If item.DataType = GetType(Date) Then
                                '            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                '        End If
                                '        intcolnumber = intcolnumber + 1
                                '    Next
                                'End If
                                ws.Cells(ws.Dimension.Address).AutoFitColumns()
                                resource.Save()
                                Response.Clear()
                                Response.ClearHeaders()
                                Response.ContentType = "application/vnd.ms-excel"
                                Response.AddHeader("content-disposition", "attachment;filename=" + namefile + ".xlsx")
                                Response.Charset = ""
                                Response.AddHeader("cache-control", "max-age=0")
                                Me.EnableViewState = False
                                Response.ContentType = "ContentType"
                                Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                                Response.End()
                            End Using
                        End If
                    End Using
                    'end Daniel 20210521
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                    'strOrder = "LastUpdateDate DESC"
                    'QueryTable = "AML_Sipesat_Report"
                    'QueryField = "PK_ID,PK_Customer_Type_ID,Name,Place_Birth,PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP"
                    'strWhereClause = "Period = '" + ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1"
                    'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_ID,PK_Customer_Type_ID,Name,Place_Birth,CONVERT(varchar, PK_Date_Birth, 105) PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP from AML_Sipesat_Report where Period = '" & ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1", Nothing)
                        objFormModuleView.changeHeader(objtbl)
                        For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + "|"c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("" & objtbl.Rows(i).Item(k).ToString & "" + "|"c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=" + namefile + ".csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "XML" Then
                    Dim path As String = Server.MapPath("~\goaml\FolderExport\")
                    Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
                    If Not Directory.Exists(Dirpath) Then
                        Directory.CreateDirectory(Dirpath)
                    End If

                    Dim FileReturn As String = NawaDevBLL.GenerateSiPesatBLL.GenerateReportXMLNew(ObjGeneratedSiPesat.PK_IDPJK, ObjGeneratedSiPesat.Period, ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType, Dirpath, "VALID")

                    If FileReturn Is Nothing Then
                        Throw New Exception("Not Exists Report")
                    End If

                    Response.Clear()
                    Response.ClearHeaders()
                    'Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(FileReturn))
                    Response.AddHeader("content-disposition", "attachment;filename=" + namefile + ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.ContentType = "application/zip"
                    Response.BinaryWrite(File.ReadAllBytes(FileReturn))
                    Response.End()

                ElseIf cboExportExcel.SelectedItem.Value = "TXT" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".txt"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                    'strOrder = "LastUpdateDate DESC"
                    'QueryTable = "AML_Sipesat_Report"
                    'QueryField = "PK_ID,PK_Customer_Type_ID,Name,Place_Birth,PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP"
                    'strWhereClause = "Period = '" + ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1"
                    'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_ID,PK_Customer_Type_ID,Name,Place_Birth,CONVERT(varchar, PK_Date_Birth, 105) PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP from AML_Sipesat_Report where Period = '" & ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1", Nothing)
                        objFormModuleView.changeHeader(objtbl)
                        For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + "|"c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("" & objtbl.Rows(i).Item(k).ToString & "" + "|"c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=" + namefile + ".txt")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnGenerateInvalid_Click(sender As Object, e As EventArgs)
        NawaDevBLL.GenerateSiPesatBLL.GenerateData(ObjModule, ObjGeneratedSiPesat.PK_Sipesat_GeneratedFileList_ID)

        Dim objfileinfo As IO.FileInfo = Nothing

        'Export File XML/CSV/Excel
        Dim namefile As String = ""
        If ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 1 Then
            namefile = "SIPESAT_INVALID_" + ObjGeneratedSiPesat.PK_IDPJK.ToString + "_IN_" + Date.Now.ToString("ddMMyyyy") + "_1"
        ElseIf ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 2 Then
            namefile = "SIPESAT_INVALID_" + ObjGeneratedSiPesat.PK_IDPJK.ToString + "_TW_" + Date.Now.ToString("ddMMyyyy") + "_1"
        Else
            namefile = "SIPESAT_INVALID_" + ObjGeneratedSiPesat.PK_IDPJK.ToString + "_" + Date.Now.ToString("ddMMyyyy") + "_1"
        End If

        If Not cboExportExcel.SelectedItem Is Nothing Then
            If cboExportExcel.SelectedItem.Value = "Excel" Then
                Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                'strOrder = "LastUpdateDate DESC"
                'QueryTable = "AML_Sipesat_Report"
                'QueryField = "PK_Report_ID,PK_ID,PK_Customer_Type_ID,Name,Place_Birth,PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP,Period,Jenis_Informasi,IsValid,Status,LastUpdateDate"
                'strWhereClause = "Period = '" + ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1"
                'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                'Update Daniel 20210521
                Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_ID,PK_Customer_Type_ID,Name,Place_Birth,PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP from AML_Sipesat_Report where Period = '" & ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 0", Nothing)
                    'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_ID,PK_Customer_Type_ID,Name,Place_Birth,CONVERT(varchar, PK_Date_Birth, 105) PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP from AML_Sipesat_Report where Period = '" & ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1", Nothing)
                    Dim intJumlahField As Integer = 1000000
                    Dim stringjumlahrow As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT ParameterValue FROM dbo.goAML_Ref_ReportGlobalParameter WHERE PK_GlobalReportParameter_ID = 2003", Nothing)
                    If Integer.TryParse(stringjumlahrow, intJumlahField) Then
                        intJumlahField = Integer.Parse(stringjumlahrow)
                        If intJumlahField > 1048570 Then
                            Throw New Exception("Please Check SIPESAT Maximum Excel Data Rows Value On Report Global Parameter")
                        End If
                    Else
                        Throw New Exception("Please Check SIPESAT Maximum Excel Data Rows Value On Report Global Parameter")
                    End If
                    Dim numberoffile As Integer = 0
                    numberoffile = Convert.ToInt32(Math.Ceiling((Convert.ToDecimal(objtbl.Rows.Count()) / Convert.ToDecimal(intJumlahField))))

                    objFormModuleView.changeHeader(objtbl)
                    For Each item As ColumnBase In GridpanelView.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next

                    Dim dateformat As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT ParameterValue FROM dbo.goAML_Ref_ReportGlobalParameter WHERE PK_GlobalReportParameter_ID = 2002", Nothing)
                    Dim dtTemp As DataTable = objtbl.Clone()
                    For Each item As DataColumn In dtTemp.Columns
                        item.DataType = GetType(String)
                    Next

                    If numberoffile > 1 Then
                        Dim path As String = Server.MapPath("~\goaml\FolderExport\")
                        Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
                        If Not Directory.Exists(Dirpath) Then
                            Directory.CreateDirectory(Dirpath)
                        End If

                        Dim lstFileInZip As New List(Of String)
                        Dim namefilezip As String = ""
                        If ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 1 Then
                            namefilezip = "SIPESAT_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_IN_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & ".zip"
                        ElseIf ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 2 Then
                            namefilezip = "SIPESAT_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_TW_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & ".zip"
                        Else
                            namefilezip = "SIPESAT_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & ".zip"
                        End If
                        namefilezip = Dirpath & namefilezip

                        For i As Integer = 0 To (numberoffile - 1)
                            If ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 1 Then
                                namefile = "SIPESAT_INVALID_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_IN_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & "_" & (i + 1) & ".xlsx"
                            ElseIf ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 2 Then
                                namefile = "SIPESAT_INVALID_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_TW_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & "_" & (i + 1) & ".xlsx"
                            Else
                                namefile = "SIPESAT_INVALID_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & "_" & (i + 1) & ".xlsx"
                            End If
                            tempfilexls = Dirpath & namefile
                            objfileinfo = New IO.FileInfo(tempfilexls)
                            If IO.File.Exists(tempfilexls) Then
                                IO.File.Delete(tempfilexls)
                            End If
                            If i <> (numberoffile - 1) Then
                                Dim introw As Integer = 0
                                For j As Integer = (intJumlahField * i) To ((intJumlahField * (i + 1)) - 1)
                                    dtTemp.ImportRow(objtbl.Rows(j))
                                    If dateformat IsNot Nothing Then
                                        Dim intcolnumber As Integer = 0
                                        For Each item As DataColumn In objtbl.Columns
                                            If item.DataType = GetType(Date) Then
                                                Dim stringdate As String = ""
                                                If Not objtbl.Rows(j).IsNull(intcolnumber) Then
                                                    Dim tempdate As DateTime = objtbl.Rows(j).Item(intcolnumber)
                                                    stringdate = tempdate.ToString(dateformat)
                                                End If

                                                'If Not DateTime.TryParseExact(stringdate, dateformat,
                                                '                              System.Globalization.CultureInfo.InvariantCulture,
                                                '                              System.Globalization.DateTimeStyles.None, tempdate) Then
                                                '    Throw New Exception("Please Check SIPESAT Format Date Value On Report Global Parameter")
                                                'End If
                                                dtTemp.Rows(introw).Item(intcolnumber) = stringdate
                                            End If
                                            intcolnumber = intcolnumber + 1
                                        Next
                                    End If
                                    introw = introw + 1
                                Next
                            Else
                                Dim introw As Integer = 0
                                For j As Integer = (intJumlahField * i) To (objtbl.Rows.Count() - 1)
                                    dtTemp.ImportRow(objtbl.Rows(j))
                                    If dateformat IsNot Nothing Then
                                        Dim intcolnumber As Integer = 0
                                        For Each item As DataColumn In objtbl.Columns
                                            If item.DataType = GetType(Date) Then
                                                Dim stringdate As String = ""
                                                If Not objtbl.Rows(j).IsNull(intcolnumber) Then
                                                    Dim tempdate As DateTime = objtbl.Rows(j).Item(intcolnumber)
                                                    stringdate = tempdate.ToString(dateformat)
                                                    'If Not DateTime.TryParseExact(stringdate, dateformat,
                                                    '                              System.Globalization.CultureInfo.InvariantCulture,
                                                    '                              System.Globalization.DateTimeStyles.None, tempdate) Then
                                                    '    Throw New Exception("Please Check SIPESAT Format Date Value On Report Global Parameter")
                                                    'End If
                                                End If
                                                dtTemp.Rows(introw).Item(intcolnumber) = stringdate
                                            End If
                                            intcolnumber = intcolnumber + 1
                                        Next
                                    End If
                                    introw = introw + 1
                                Next
                            End If
                            Using resource As New ExcelPackage(objfileinfo)
                                Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                                ws.Cells("A1").LoadFromDataTable(dtTemp, True)
                                'If dateformat IsNot Nothing Then
                                '    Dim intcolnumber As Integer = 1
                                '    For Each item As DataColumn In dtTemp.Columns
                                '        If item.DataType = GetType(Date) Then
                                '            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                '        End If
                                '        intcolnumber = intcolnumber + 1
                                '    Next
                                'End If
                                ws.Cells(ws.Dimension.Address).AutoFitColumns()
                                resource.Save()
                                lstFileInZip.Add(tempfilexls)
                                Me.EnableViewState = False
                            End Using
                            dtTemp.Clear()
                        Next
                        If lstFileInZip.Count > 0 Then
                            Using zip As New ZipFile()
                                If IO.File.Exists(namefilezip) Then
                                    IO.File.Delete(namefilezip)
                                End If

                                zip.CompressionLevel = CompressionLevel.Level9
                                zip.AddFiles(lstFileInZip, "")

                                zip.Save(namefilezip)
                            End Using
                            'buang karena sudah di zip
                            For Each Item1 As String In lstFileInZip
                                If IO.File.Exists(Item1) Then
                                    IO.File.Delete(Item1)
                                End If
                            Next
                        End If
                        If IO.File.Exists(namefilezip) Then
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(namefilezip))
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "application/zip"
                            Response.BinaryWrite(File.ReadAllBytes(namefilezip))
                            Response.End()
                        Else
                            Throw New Exception("Something Wrong While Downloading Data")
                        End If
                    Else
                        Dim introw As Integer = 0
                        For Each row As DataRow In objtbl.Rows
                            dtTemp.ImportRow(row)
                            If dateformat IsNot Nothing Then
                                Dim intcolnumber As Integer = 0
                                For Each item As DataColumn In objtbl.Columns
                                    If item.DataType = GetType(Date) Then
                                        Dim stringdate As String = ""
                                        If Not row.IsNull(intcolnumber) Then
                                            Dim tempdate As DateTime = row.Item(intcolnumber)
                                            stringdate = tempdate.ToString(dateformat)
                                        End If
                                        'If Not DateTime.TryParseExact(tempdate, dateformat,
                                        '                                      System.Globalization.CultureInfo.InvariantCulture,
                                        '                                      System.Globalization.DateTimeStyles.None, tempdate) Then
                                        '    Throw New Exception("Please Check SIPESAT Format Date Value On Report Global Parameter")
                                        'End If
                                        dtTemp.Rows(introw).Item(intcolnumber) = stringdate
                                    End If
                                    intcolnumber = intcolnumber + 1
                                Next
                            End If
                            introw = introw + 1
                        Next
                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(dtTemp, True)
                            'If dateformat IsNot Nothing Then
                            '    Dim intcolnumber As Integer = 1
                            '    For Each item As DataColumn In objtbl.Columns
                            '        If item.DataType = GetType(Date) Then
                            '            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                            '        End If
                            '        intcolnumber = intcolnumber + 1
                            '    Next
                            'End If
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=" + namefile + ".xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End If
                End Using
                'end Daniel 20210521
            ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                'strOrder = "LastUpdateDate DESC"
                'QueryTable = "AML_Sipesat_Report"
                'QueryField = "PK_ID,PK_Customer_Type_ID,Name,Place_Birth,PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP"
                'strWhereClause = "Period = '" + ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1"
                'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_ID,PK_Customer_Type_ID,Name,Place_Birth,CONVERT(varchar, PK_Date_Birth, 105) PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP from AML_Sipesat_Report where Period = '" & ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 0", Nothing)
                    objFormModuleView.changeHeader(objtbl)
                    For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next

                    For k As Integer = 0 To objtbl.Columns.Count - 1
                        'add separator
                        stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + "|"c)
                    Next
                    'append new line
                    stringWriter_Temp.Write(vbCr & vbLf)
                    For i As Integer = 0 To objtbl.Rows.Count - 1
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write("" & objtbl.Rows(i).Item(k).ToString & "" + "|"c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                    Next
                    stringWriter_Temp.Close()
                    Response.Clear()
                    Response.AddHeader("content-disposition", "attachment;filename=" + namefile + ".csv")
                    Response.Charset = ""
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Me.EnableViewState = False
                    'Response.ContentType = "application/ms-excel"
                    Response.ContentType = "text/csv"
                    Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            ElseIf cboExportExcel.SelectedItem.Value = "XML" Then
                Dim path As String = Server.MapPath("~\goaml\FolderExport\")
                Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
                If Not Directory.Exists(Dirpath) Then
                    Directory.CreateDirectory(Dirpath)
                End If

                Dim FileReturn As String = NawaDevBLL.GenerateSiPesatBLL.GenerateReportXMLNew(ObjGeneratedSiPesat.PK_IDPJK, ObjGeneratedSiPesat.Period, ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType, Dirpath, "INVALID")

                If FileReturn Is Nothing Then
                    Throw New Exception("Not Exists Report")
                End If

                Response.Clear()
                Response.ClearHeaders()
                'Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(FileReturn))
                Response.AddHeader("content-disposition", "attachment;filename=" + namefile + ".zip")
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "application/zip"
                Response.BinaryWrite(File.ReadAllBytes(FileReturn))
                Response.End()

            ElseIf cboExportExcel.SelectedItem.Value = "TXT" Then
                Dim tempfilexls As String = Guid.NewGuid.ToString & ".txt"
                objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                'strOrder = "LastUpdateDate DESC"
                'QueryTable = "AML_Sipesat_Report"
                'QueryField = "PK_ID,PK_Customer_Type_ID,Name,Place_Birth,PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP"
                'strWhereClause = "Period = '" + ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1"
                'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_ID,PK_Customer_Type_ID,Name,Place_Birth,CONVERT(varchar, PK_Date_Birth, 105) PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP from AML_Sipesat_Report where Period = '" & ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 0", Nothing)
                    objFormModuleView.changeHeader(objtbl)
                    For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next

                    For k As Integer = 0 To objtbl.Columns.Count - 1
                        'add separator
                        stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + "|"c)
                    Next
                    'append new line
                    stringWriter_Temp.Write(vbCr & vbLf)
                    For i As Integer = 0 To objtbl.Rows.Count - 1
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write("" & objtbl.Rows(i).Item(k).ToString & "" + "|"c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                    Next
                    stringWriter_Temp.Close()
                    Response.Clear()
                    Response.AddHeader("content-disposition", "attachment;filename=" + namefile + ".txt")
                    Response.Charset = ""
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Me.EnableViewState = False
                    'Response.ContentType = "application/ms-excel"
                    Response.ContentType = "text/csv"
                    Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Else
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        End If
    End Sub

    Protected Sub BtnGenerateAll_Click(sender As Object, e As EventArgs)
        NawaDevBLL.GenerateSiPesatBLL.GenerateData(ObjModule, ObjGeneratedSiPesat.PK_Sipesat_GeneratedFileList_ID)

        Dim objfileinfo As IO.FileInfo = Nothing

        'Export File XML/CSV/Excel
        Dim namefile As String = ""
        If ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 1 Then
            namefile = "SIPESAT_ALL_" + ObjGeneratedSiPesat.PK_IDPJK.ToString + "_IN_" + Date.Now.ToString("ddMMyyyy") + "_1"
        ElseIf ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 2 Then
            namefile = "SIPESAT_ALL_" + ObjGeneratedSiPesat.PK_IDPJK.ToString + "_TW_" + Date.Now.ToString("ddMMyyyy") + "_1"
        Else
            namefile = "SIPESAT_ALL_" + ObjGeneratedSiPesat.PK_IDPJK.ToString + "_" + Date.Now.ToString("ddMMyyyy") + "_1"
        End If

        If Not cboExportExcel.SelectedItem Is Nothing Then
            If cboExportExcel.SelectedItem.Value = "Excel" Then
                Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                'strOrder = "LastUpdateDate DESC"
                'QueryTable = "AML_Sipesat_Report"
                'QueryField = "PK_Report_ID,PK_ID,PK_Customer_Type_ID,Name,Place_Birth,PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP,Period,Jenis_Informasi,IsValid,Status,LastUpdateDate"
                'strWhereClause = "Period = '" + ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1"
                'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                'Update Daniel 20210521
                Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_ID,PK_Customer_Type_ID,Name,Place_Birth,PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP from AML_Sipesat_Report where Period = '" & ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' ", Nothing)
                    'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_ID,PK_Customer_Type_ID,Name,Place_Birth,CONVERT(varchar, PK_Date_Birth, 105) PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP from AML_Sipesat_Report where Period = '" & ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1", Nothing)
                    Dim intJumlahField As Integer = 1000000
                    Dim stringjumlahrow As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT ParameterValue FROM dbo.goAML_Ref_ReportGlobalParameter WHERE PK_GlobalReportParameter_ID = 2003", Nothing)
                    If Integer.TryParse(stringjumlahrow, intJumlahField) Then
                        intJumlahField = Integer.Parse(stringjumlahrow)
                        If intJumlahField > 1048570 Then
                            Throw New Exception("Please Check SIPESAT Maximum Excel Data Rows Value On Report Global Parameter")
                        End If
                    Else
                        Throw New Exception("Please Check SIPESAT Maximum Excel Data Rows Value On Report Global Parameter")
                    End If
                    Dim numberoffile As Integer = 0
                    numberoffile = Convert.ToInt32(Math.Ceiling((Convert.ToDecimal(objtbl.Rows.Count()) / Convert.ToDecimal(intJumlahField))))

                    objFormModuleView.changeHeader(objtbl)
                    For Each item As ColumnBase In GridpanelView.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next

                    Dim dateformat As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT ParameterValue FROM dbo.goAML_Ref_ReportGlobalParameter WHERE PK_GlobalReportParameter_ID = 2002", Nothing)
                    Dim dtTemp As DataTable = objtbl.Clone()
                    For Each item As DataColumn In dtTemp.Columns
                        item.DataType = GetType(String)
                    Next

                    If numberoffile > 1 Then
                        Dim path As String = Server.MapPath("~\goaml\FolderExport\")
                        Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
                        If Not Directory.Exists(Dirpath) Then
                            Directory.CreateDirectory(Dirpath)
                        End If

                        Dim lstFileInZip As New List(Of String)
                        Dim namefilezip As String = ""
                        If ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 1 Then
                            namefilezip = "SIPESAT_ALL_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_IN_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & ".zip"
                        ElseIf ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 2 Then
                            namefilezip = "SIPESAT_ALL_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_TW_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & ".zip"
                        Else
                            namefilezip = "SIPESAT_ALL_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & ".zip"
                        End If
                        namefilezip = Dirpath & namefilezip

                        For i As Integer = 0 To (numberoffile - 1)
                            If ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 1 Then
                                namefile = "SIPESAT_ALL_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_IN_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & "_" & (i + 1) & ".xlsx"
                            ElseIf ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType = 2 Then
                                namefile = "SIPESAT_ALL_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_TW_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & "_" & (i + 1) & ".xlsx"
                            Else
                                namefile = "SIPESAT_ALL_" & ObjGeneratedSiPesat.PK_IDPJK.ToString & "_" & ObjGeneratedSiPesat.Period.ToString & "_" & Date.Now.ToString("ddMMyyyy") & "_" & (i + 1) & ".xlsx"
                            End If
                            tempfilexls = Dirpath & namefile
                            objfileinfo = New IO.FileInfo(tempfilexls)
                            If IO.File.Exists(tempfilexls) Then
                                IO.File.Delete(tempfilexls)
                            End If
                            If i <> (numberoffile - 1) Then
                                Dim introw As Integer = 0
                                For j As Integer = (intJumlahField * i) To ((intJumlahField * (i + 1)) - 1)
                                    dtTemp.ImportRow(objtbl.Rows(j))
                                    If dateformat IsNot Nothing Then
                                        Dim intcolnumber As Integer = 0
                                        For Each item As DataColumn In objtbl.Columns
                                            If item.DataType = GetType(Date) Then
                                                Dim stringdate As String = ""
                                                If Not objtbl.Rows(j).IsNull(intcolnumber) Then
                                                    Dim tempdate As DateTime = objtbl.Rows(j).Item(intcolnumber)
                                                    stringdate = tempdate.ToString(dateformat)
                                                End If

                                                'If Not DateTime.TryParseExact(stringdate, dateformat,
                                                '                              System.Globalization.CultureInfo.InvariantCulture,
                                                '                              System.Globalization.DateTimeStyles.None, tempdate) Then
                                                '    Throw New Exception("Please Check SIPESAT Format Date Value On Report Global Parameter")
                                                'End If
                                                dtTemp.Rows(introw).Item(intcolnumber) = stringdate
                                            End If
                                            intcolnumber = intcolnumber + 1
                                        Next
                                    End If
                                    introw = introw + 1
                                Next
                            Else
                                Dim introw As Integer = 0
                                For j As Integer = (intJumlahField * i) To (objtbl.Rows.Count() - 1)
                                    dtTemp.ImportRow(objtbl.Rows(j))
                                    If dateformat IsNot Nothing Then
                                        Dim intcolnumber As Integer = 0
                                        For Each item As DataColumn In objtbl.Columns
                                            If item.DataType = GetType(Date) Then
                                                Dim stringdate As String = ""
                                                If Not objtbl.Rows(j).IsNull(intcolnumber) Then
                                                    Dim tempdate As DateTime = objtbl.Rows(j).Item(intcolnumber)
                                                    stringdate = tempdate.ToString(dateformat)
                                                    'If Not DateTime.TryParseExact(stringdate, dateformat,
                                                    '                              System.Globalization.CultureInfo.InvariantCulture,
                                                    '                              System.Globalization.DateTimeStyles.None, tempdate) Then
                                                    '    Throw New Exception("Please Check SIPESAT Format Date Value On Report Global Parameter")
                                                    'End If
                                                End If
                                                dtTemp.Rows(introw).Item(intcolnumber) = stringdate
                                            End If
                                            intcolnumber = intcolnumber + 1
                                        Next
                                    End If
                                    introw = introw + 1
                                Next
                            End If
                            Using resource As New ExcelPackage(objfileinfo)
                                Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                                ws.Cells("A1").LoadFromDataTable(dtTemp, True)
                                'If dateformat IsNot Nothing Then
                                '    Dim intcolnumber As Integer = 1
                                '    For Each item As DataColumn In dtTemp.Columns
                                '        If item.DataType = GetType(Date) Then
                                '            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                '        End If
                                '        intcolnumber = intcolnumber + 1
                                '    Next
                                'End If
                                ws.Cells(ws.Dimension.Address).AutoFitColumns()
                                resource.Save()
                                lstFileInZip.Add(tempfilexls)
                                Me.EnableViewState = False
                            End Using
                            dtTemp.Clear()
                        Next
                        If lstFileInZip.Count > 0 Then
                            Using zip As New ZipFile()
                                If IO.File.Exists(namefilezip) Then
                                    IO.File.Delete(namefilezip)
                                End If

                                zip.CompressionLevel = CompressionLevel.Level9
                                zip.AddFiles(lstFileInZip, "")

                                zip.Save(namefilezip)
                            End Using
                            'buang karena sudah di zip
                            For Each Item1 As String In lstFileInZip
                                If IO.File.Exists(Item1) Then
                                    IO.File.Delete(Item1)
                                End If
                            Next
                        End If
                        If IO.File.Exists(namefilezip) Then
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(namefilezip))
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "application/zip"
                            Response.BinaryWrite(File.ReadAllBytes(namefilezip))
                            Response.End()
                        Else
                            Throw New Exception("Something Wrong While Downloading Data")
                        End If
                    Else
                        Dim introw As Integer = 0
                        For Each row As DataRow In objtbl.Rows
                            dtTemp.ImportRow(row)
                            If dateformat IsNot Nothing Then
                                Dim intcolnumber As Integer = 0
                                For Each item As DataColumn In objtbl.Columns
                                    If item.DataType = GetType(Date) Then
                                        Dim stringdate As String = ""
                                        If Not row.IsNull(intcolnumber) Then
                                            Dim tempdate As DateTime = row.Item(intcolnumber)
                                            stringdate = tempdate.ToString(dateformat)
                                        End If
                                        'If Not DateTime.TryParseExact(tempdate, dateformat,
                                        '                                      System.Globalization.CultureInfo.InvariantCulture,
                                        '                                      System.Globalization.DateTimeStyles.None, tempdate) Then
                                        '    Throw New Exception("Please Check SIPESAT Format Date Value On Report Global Parameter")
                                        'End If
                                        dtTemp.Rows(introw).Item(intcolnumber) = stringdate
                                    End If
                                    intcolnumber = intcolnumber + 1
                                Next
                            End If
                            introw = introw + 1
                        Next
                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(dtTemp, True)
                            'If dateformat IsNot Nothing Then
                            '    Dim intcolnumber As Integer = 1
                            '    For Each item As DataColumn In objtbl.Columns
                            '        If item.DataType = GetType(Date) Then
                            '            ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                            '        End If
                            '        intcolnumber = intcolnumber + 1
                            '    Next
                            'End If
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=" + namefile + ".xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End If
                End Using
                'end Daniel 20210521
            ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                'strOrder = "LastUpdateDate DESC"
                'QueryTable = "AML_Sipesat_Report"
                'QueryField = "PK_ID,PK_Customer_Type_ID,Name,Place_Birth,PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP"
                'strWhereClause = "Period = '" + ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1"
                'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_ID,PK_Customer_Type_ID,Name,Place_Birth,CONVERT(varchar, PK_Date_Birth, 105) PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP from AML_Sipesat_Report where Period = '" & ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' ", Nothing)
                    objFormModuleView.changeHeader(objtbl)
                    For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next

                    For k As Integer = 0 To objtbl.Columns.Count - 1
                        'add separator
                        stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + "|"c)
                    Next
                    'append new line
                    stringWriter_Temp.Write(vbCr & vbLf)
                    For i As Integer = 0 To objtbl.Rows.Count - 1
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write("" & objtbl.Rows(i).Item(k).ToString & "" + "|"c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                    Next
                    stringWriter_Temp.Close()
                    Response.Clear()
                    Response.AddHeader("content-disposition", "attachment;filename=" + namefile + ".csv")
                    Response.Charset = ""
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Me.EnableViewState = False
                    'Response.ContentType = "application/ms-excel"
                    Response.ContentType = "text/csv"
                    Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            ElseIf cboExportExcel.SelectedItem.Value = "XML" Then
                Dim path As String = Server.MapPath("~\goaml\FolderExport\")
                Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
                If Not Directory.Exists(Dirpath) Then
                    Directory.CreateDirectory(Dirpath)
                End If

                Dim FileReturn As String = NawaDevBLL.GenerateSiPesatBLL.GenerateReportXMLNew(ObjGeneratedSiPesat.PK_IDPJK, ObjGeneratedSiPesat.Period, ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType, Dirpath, "ALL")

                If FileReturn Is Nothing Then
                    Throw New Exception("Not Exists Report")
                End If

                Response.Clear()
                Response.ClearHeaders()
                'Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(FileReturn))
                Response.AddHeader("content-disposition", "attachment;filename=" + namefile + ".zip")
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "application/zip"
                Response.BinaryWrite(File.ReadAllBytes(FileReturn))
                Response.End()

            ElseIf cboExportExcel.SelectedItem.Value = "TXT" Then
                Dim tempfilexls As String = Guid.NewGuid.ToString & ".txt"
                objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                'Using objtbl As DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                'strOrder = "LastUpdateDate DESC"
                'QueryTable = "AML_Sipesat_Report"
                'QueryField = "PK_ID,PK_Customer_Type_ID,Name,Place_Birth,PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP"
                'strWhereClause = "Period = '" + ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' AND IsValid = 1"
                'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select PK_ID,PK_Customer_Type_ID,Name,Place_Birth,CONVERT(varchar, PK_Date_Birth, 105) PK_Date_Birth,Address,No_KTP,PK_Other_ID,No_CIF,No_NPWP from AML_Sipesat_Report where Period = '" & ObjGeneratedSiPesat.Period + "' AND Jenis_Informasi = '" + ObjGeneratedSiPesat.PK_AML_Sipesat_InfoType.ToString + "' ", Nothing)
                    objFormModuleView.changeHeader(objtbl)
                    For Each item As ColumnBase In GridpanelView.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next

                    For k As Integer = 0 To objtbl.Columns.Count - 1
                        'add separator
                        stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + "|"c)
                    Next
                    'append new line
                    stringWriter_Temp.Write(vbCr & vbLf)
                    For i As Integer = 0 To objtbl.Rows.Count - 1
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write("" & objtbl.Rows(i).Item(k).ToString & "" + "|"c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                    Next
                    stringWriter_Temp.Close()
                    Response.Clear()
                    Response.AddHeader("content-disposition", "attachment;filename=" + namefile + ".txt")
                    Response.Charset = ""
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Me.EnableViewState = False
                    'Response.ContentType = "application/ms-excel"
                    Response.ContentType = "text/csv"
                    Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                    Response.End()
                End Using
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Else
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        End If
    End Sub
End Class
