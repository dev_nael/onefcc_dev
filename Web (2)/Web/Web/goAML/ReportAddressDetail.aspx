﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="ReportAddressDetail.aspx.vb" Inherits="goAML_ReportAddressDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:FormPanel ID="FormPanelWICDetail" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" ButtonAlign="Center" DefaultAnchor="100%" AutoScroll="true" Flex="1">
        <Buttons>
            <ext:Button ID="btnCancel" runat="server" Text="Cancel" Icon="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>