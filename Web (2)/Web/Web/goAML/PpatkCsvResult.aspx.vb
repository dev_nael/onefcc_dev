﻿Imports NawaBLL
Imports Ext
Imports System.Data.SqlClient
Imports Elmah
Imports System.Data
Imports NawaDAL
Imports System.Text.RegularExpressions
Imports OfficeOpenXml
Imports System.IO

Partial Class goAML_PpatkCsvResult
    Inherits Parent
    Class PPATK
        Private Shared Function ConvertToInteger(ByRef value As String) As Integer
            If String.IsNullOrEmpty(value) Then
                value = "0"
            End If
            Return Convert.ToInt32(value)
        End Function
        'Public Shared Function CreateCSVList(str As String) As List(Of PPATK)
        Public Shared Function CreateCSVList(str As String) As DataTable
            ' Dim list As List(Of PPATK) = New List(Of PPATK)

            ' Create data table columns
            Dim data As DataTable = New DataTable
            data.Columns.Add("ID_Laporan", GetType(String))
            data.Columns.Add("Jenis_Laporan", GetType(String))
            data.Columns.Add("FileName", GetType(String))
            data.Columns.Add("Dibuat_Oleh", GetType(String))
            data.Columns.Add("Pembaruan_terakhir_oleh", GetType(String))
            data.Columns.Add("Nama_Organisasi", GetType(String))
            data.Columns.Add("Pengirim", GetType(String))
            data.Columns.Add("Transaksi", GetType(String))
            data.Columns.Add("Transaksi_Ditolak", GetType(String))
            data.Columns.Add("Status", GetType(String))
            data.Columns.Add("Terkirim_pada", GetType(Date))
            data.Columns.Add("PK_Report", GetType(Integer))

            Dim lines As String() = str.Split(vbCrLf)
            Dim columns As String()
            Dim i As Int16 = 0
            For Each row As String In lines
                columns = row.Split(";")
                '-----------------------------------
                ' Validation (column label length)
                '-----------------------------------
                If i = 0 Then
                    If columns.Length <> 11 Then
                        Throw New Exception("Invalid file structure.")
                    End If
                Else
                    Dim obj As PPATK = New PPATK()
                    obj.RowNum = i
                    If columns.Length <> 11 Then
                        'obj.ErrorString = "Column is less than 11"
                        'obj.RowValid = False
                        Continue For
                    End If
                    obj.IDLaporan = columns(0)
                    obj.JenisLaporan = columns(1)
                    obj.File = columns(2)
                    obj.DibuatOleh = columns(3)
                    obj.PembaruanOleh = columns(4)
                    obj.NamaOrganisasi = columns(5)
                    obj.Pengirim = columns(6)
                    obj.Transaksi = columns(7)
                    obj.Ditolak = columns(8)
                    obj.Status = columns(9)
                    obj.TerkirimSTR = columns(10)
                    obj.Validate(data)

                    'list.Add(obj)
                End If
                i += 1
            Next
            Return data
            'Return List
        End Function
#Region "Properties"
        ' Holds the data
        Public IDLaporan As String
        Public JenisLaporan As String
        Public File As String
        Public DibuatOleh As String
        Public PembaruanOleh As String
        Public NamaOrganisasi As String
        Public Pengirim As String
        Public Transaksi As String
        Public Ditolak As String
        Public Status As String
        Public TerkirimSTR As String
        Public TerkirimPada As Date
        Public PK_Report As Integer
        ' For validation purpose
        Public RowNum As Integer
        'Public RowValid As Boolean
        'Public ErrorString As String
        '=====================================================================
        '-- Validate the data here
        '=====================================================================
        Public Sub Validate(ByRef data As DataTable)
            If IsEmpty() Then
                Return
            End If

            ' Try to convert the date
            Try
                TerkirimPada = DateTime.ParseExact(TerkirimSTR, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture) ' Edit on 18 Dec 2020, dd/MM/yyyy jadi d/M/yyyy. Thanks to Tim SCB
            Catch ex As Exception
                Throw New Exception("Row Number: " + RowNum + " has invalid date format. Please use d/M/yyyy format!") ' Edit on 18 Dec 2020, dd/MM/yyyy jadi d/M/yyyy. Thanks to Tim SCB
            End Try

            ' Get the number inside the brackets
            Dim regex As Regex = New Regex("\[(\d+)\]")
            Dim match As Match = regex.Match(IO.Path.GetFileNameWithoutExtension(File))
            If match.Success Then
                PK_Report = Convert.ToInt32(match.Groups(1).Value)
            Else
                PK_Report = 0
            End If

            Transaksi = ConvertToInteger(Transaksi)
            Ditolak = ConvertToInteger(Ditolak)

            Dim row As DataRow = data.NewRow()
            row("ID_Laporan") = IDLaporan
            row("Jenis_Laporan") = JenisLaporan
            row("FileName") = File
            row("Dibuat_Oleh") = DibuatOleh
            row("Pembaruan_terakhir_oleh") = PembaruanOleh
            row("Nama_Organisasi") = NamaOrganisasi
            row("Pengirim") = Pengirim
            row("Transaksi") = Transaksi
            row("Transaksi_Ditolak") = Ditolak
            row("Status") = Status
            row("Terkirim_pada") = TerkirimPada
            row("PK_Report") = PK_Report
            data.Rows.Add(row)
        End Sub

        Private Function IsEmpty() As Boolean
            Return String.IsNullOrEmpty(IDLaporan) And
                String.IsNullOrEmpty(JenisLaporan) And
                String.IsNullOrEmpty(File) And
                String.IsNullOrEmpty(DibuatOleh) And
                String.IsNullOrEmpty(PembaruanOleh) And
                String.IsNullOrEmpty(NamaOrganisasi) And
                String.IsNullOrEmpty(Pengirim) And
                String.IsNullOrEmpty(Transaksi) And
                String.IsNullOrEmpty(Ditolak) And
                String.IsNullOrEmpty(Status) And
                String.IsNullOrEmpty(TerkirimSTR)
        End Function
#End Region
    End Class
    'Public Property RefJenisLaporan() As DataTable
    '    Get
    '        Return Session("goAML_PpatkCsvResult.JenisLaporan")
    '    End Get
    '    Set(ByVal value As DataTable)
    '        Session("goAML_PpatkCsvResult.JenisLaporan") = value
    '    End Set
    'End Property
    Public Property Debug() As Boolean
        Get
            Return Session("goAML_PpatkCsvResult.Debug")
        End Get
        Set(ByVal value As Boolean)
            Session("goAML_PpatkCsvResult.Debug") = value
        End Set
    End Property
    Protected Sub GoBackClick(sender As Object, e As DirectEventArgs)
        MainPanel.Hidden = False
        Panelconfirmation.Hidden = True
    End Sub

    Protected Sub UploadClick(sender As Object, e As DirectEventArgs)
        If (Me.UploadField.HasFile) Then
            Warning.Text = ""
            'If JenisLaporan.SelectedItem.Text = "" Then
            '    Warning.Text = "Please select type of the report!"
            '    Exit Sub
            'End If
            Try
                'Validasi jika belum ada file yang diupload
                If Not UploadField.HasFile Then
                    Throw New ApplicationException("Belum ada file yang diupload.")
                End If

                'Validasi hanya terima xlsx
                If Not UploadField.FileName.EndsWith(".xlsx") Then
                    Throw New ApplicationException("Format yang valid hanya .xlsx.")
                End If

                Dim dir As String = ""
                ' Save file trial (Skip if it fails)
                Try
                    Dim kue As String = "select ParameterValue as val from goAML_Ref_ReportGlobalParameter where PK_GlobalReportParameter_ID = 17"
                    dir = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, kue).Rows(0)("val")
                    'UploadField.PostedFile.SaveAs(dir + UploadField.PostedFile.FileName)
                Catch ex As Exception

                End Try

                Dim data As DataTable = New DataTable
                data.Columns.Add("ID_Laporan", GetType(String))
                data.Columns.Add("Jenis_Laporan", GetType(String))
                data.Columns.Add("FileName", GetType(String))
                data.Columns.Add("Dibuat_Oleh", GetType(String))
                data.Columns.Add("Pembaruan_terakhir_oleh", GetType(String))
                data.Columns.Add("Nama_Organisasi", GetType(String))
                data.Columns.Add("Pengirim", GetType(String))
                data.Columns.Add("Transaksi", GetType(String))
                data.Columns.Add("Transaksi_Ditolak", GetType(String))
                data.Columns.Add("Status", GetType(String))
                data.Columns.Add("Terkirim_pada", GetType(Date))
                data.Columns.Add("PK_Report", GetType(Integer))


                If UploadField.HasFile AndAlso Not String.IsNullOrWhiteSpace(dir) Then
                    'Directory file fisik yang di-write di server
                    'Dim strFilePath As String = Server.MapPath(dir)
                    'Dim strFilePath As String = Server.MapPath("~\temp\")
                    Dim strFilePath As String = dir
                    'Dim strFileName As String = UploadField.PostedFile.FileName
                    'Dim strFileName As String = Path.GetFileNameWithoutExtension(UploadField.PostedFile.FileName) & Guid.NewGuid.ToString & ".xlsx"
                    Dim strFileName As String = Guid.NewGuid.ToString & ".xlsx"

                    'Write file uploaded to server
                    System.IO.File.WriteAllBytes(strFilePath & "\" & strFileName, UploadField.FileBytes)

                    Dim br As BinaryReader
                    Dim bData As Byte()
                    br = New BinaryReader(System.IO.File.OpenRead(strFilePath & "\" & strFileName))
                    bData = br.ReadBytes(br.BaseStream.Length)
                    Dim ms As MemoryStream = New MemoryStream(bData, 0, bData.Length)
                    ms.Write(bData, 0, bData.Length)

                    Using excelFile As New ExcelPackage(ms)
                        If excelFile.Workbook.Worksheets(1) IsNot Nothing Then
                            data = ReadExcelUpload(excelFile.Workbook.Worksheets(1), data)
                        End If
                    End Using

                    ms.Flush()
                    ms.Close()
                End If

                'Dim str As String = New IO.StreamReader(UploadField.PostedFile.InputStream).ReadToEnd
                'Dim data As DataTable = PPATK.CreateCSVList(str)

                'New SqlParameter("@ReportType", SqlDbType.VarChar) With {.Value = RefJenisLaporan.Rows(JenisLaporan.SelectedItem.Index)("Kode")},
                Dim sqlParam As SqlParameter() = New SqlParameter() {
                    New SqlParameter("@data", SqlDbType.Structured) With {.Value = data},
                    New SqlParameter("@ReportType", SqlDbType.VarChar) With {.Value = Request.Params("Access").ToString},
                    New SqlParameter("@User", SqlDbType.VarChar) With {.Value = NawaBLL.Common.SessionCurrentUser.UserID}
                }

                SQLHelper.ExecuteNonQuery(
                    SQLHelper.strConnectionString,
                    CommandType.StoredProcedure,
                    "usp_goAML_Insert_PPATK_CSV_Result",
                    sqlParam
                )

                MainPanel.Hidden = True
                Panelconfirmation.Hidden = False

                UploadField.Reset()
                JenisLaporan.Clear()
            Catch ex As Exception When TypeOf ex Is ApplicationException
                Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
            Catch ex As Exception
                If Debug Then
                    ConsoleArea.Hidden = False
                    ConsoleArea.Text += ex.Message + " " + ex.StackTrace
                Else
                    Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
                    Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
                End If
            End Try
        Else
            Warning.Text = "Please choose a file!"
        End If
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Debug = False
        Try
            If IsAuthorized() Then
                'RefJenisLaporan = LoadJenisLaporan()
                'JenisLaporan.GetStore.DataSource = RefJenisLaporan
                'JenisLaporan.GetStore.DataBind()
            Else
                Dim strIDCode As String = 1
                strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                Exit Sub
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message + ex.StackTrace).Show()
        End Try
    End Sub
    Private Function IsAuthorized() As Boolean
        If NawaBLL.Common.SessionCurrentUser Is Nothing Then
            Return False
        End If

        If Request.Params("Access") Is Nothing Then
            Return False
        End If
        '--------------------------------------------------------------------------------------------------------------------
        ' Direct page testing without authorization (if the page is directly accessed from the url, for faster development)
        ' Only sysadmin that has privilege to access the page directly
        '--------------------------------------------------------------------------------------------------------------------
        If Request.Params("ModuleID") Is Nothing And NawaBLL.Common.SessionCurrentUser.UserID.ToLower = "sysadmin" Then
            ConsoleArea.Hidden = False
            DebugButton.Hidden = False
            Debug = True
            Return True
        End If
        DebugButton.Hidden = True
        ConsoleArea.Hidden = True
        Debug = False
        Dim Moduleid As String = Request.Params("ModuleID")
        Dim intModuleid As Integer
        intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
        Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

        If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Upload) Then
            Return False
        End If
        Return True
    End Function
    '--------------------------------------------------------------
    ' This method is unused due to the changes of the requirement
    '--------------------------------------------------------------
    Private Function LoadJenisLaporan() As DataTable
        If Request.Params("Access") Is Nothing Then
            If Debug Then
                MainPanel.Title = "Upload File (All Accesses)"
            End If
            Return SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_PPATK_CSV_ReportType_List")
        Else
            Dim accessType As String = Request.Params("Access").ToString
            If Debug Or NawaBLL.Common.SessionCurrentUser.UserID.ToLower = "sysadmin" Then
                MainPanel.Title = "Upload File (Access type: " + accessType + ")"
            End If
            Dim sqlParam As SqlParameter() = New SqlParameter() {
                    New SqlParameter("@AccessType", SqlDbType.VarChar) With {.Value = accessType}
                }
            Return SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_PPATK_CSV_ReportType_List", sqlParam)
        End If
    End Function
    '------------------------------------------------------------------
    ' This method is for testing purpose
    '-----------------------------------------------------------------
    Protected Sub TestClick(sender As Object, e As DirectEventArgs)
        Try
            Dim str As String = New IO.StreamReader(Me.UploadField.PostedFile.InputStream).ReadToEnd
            Dim data As DataTable = PPATK.CreateCSVList(str)

            Dim sqlParam As SqlParameter() = New SqlParameter() {
                New SqlParameter("@data", SqlDbType.Structured) With {.Value = data}
            }
            Dim res As DataTable = SQLHelper.ExecuteTable(
                SQLHelper.strConnectionString,
                CommandType.StoredProcedure,
                "usp_udt_test",
                sqlParam
            )
            Dim tex As String = ""
            For Each item As DataRow In res.Rows
                tex &= item("Jenis_Laporan").ToString & vbLf
            Next
            ConsoleArea.Text = tex
            UploadField.Reset()
            JenisLaporan.Clear()
        Catch ex As Exception
            ConsoleArea.Text = ex.Message + " | " + ex.StackTrace
        End Try
    End Sub
    Private Sub OldUnusedCode()
        'Dim str As String = New IO.StreamReader(Me.UploadField.PostedFile.InputStream).ReadToEnd
        'Dim li As List(Of PPATK) = PPATK.CreateCSVList(str)
        'For Each item As PPATK In li
        '    Dim sqlParam As SqlParameter() = New SqlParameter() {
        '        New SqlParameter("@IDLaporan", SqlDbType.VarChar) With {.Value = item.IDLaporan},
        '        New SqlParameter("@JenisLaporan", SqlDbType.VarChar) With {.Value = item.JenisLaporan},
        '        New SqlParameter("@File", SqlDbType.VarChar) With {.Value = item.File},
        '        New SqlParameter("@DibuatOleh", SqlDbType.VarChar) With {.Value = item.DibuatOleh},
        '        New SqlParameter("@PembaruanOleh", SqlDbType.VarChar) With {.Value = item.PembaruanOleh},
        '        New SqlParameter("@NamaOrganisasi", SqlDbType.VarChar) With {.Value = item.NamaOrganisasi},
        '        New SqlParameter("@Pengirim", SqlDbType.VarChar) With {.Value = item.Pengirim},
        '        New SqlParameter("@Transaksi", SqlDbType.VarChar) With {.Value = item.Transaksi},
        '        New SqlParameter("@Ditolak", SqlDbType.VarChar) With {.Value = item.Ditolak},
        '        New SqlParameter("@Status", SqlDbType.VarChar) With {.Value = item.Status},
        '        New SqlParameter("@Terkirim", SqlDbType.DateTime) With {.Value = item.TerkirimPada},
        '        New SqlParameter("@ReportType", SqlDbType.VarChar) With {.Value = RefJenisLaporan.Rows(JenisLaporan.SelectedItem.Index)("Kode")},
        '        New SqlParameter("@PK_Report", SqlDbType.Int) With {.Value = item.PK_Report},
        '        New SqlParameter("@User", SqlDbType.VarChar) With {.Value = NawaBLL.Common.SessionCurrentUser.UserID}
        '    }
        '    SQLHelper.ExecuteNonQuery(
        '        SQLHelper.strConnectionString,
        '        CommandType.StoredProcedure,
        '        "usp_goAML_Insert_PPATK_CSV_Result",
        '        sqlParam
        '    )
        'Next
        'MainPanel.Hidden = True
        'Panelconfirmation.Hidden = False
    End Sub

    Private Shared Function ReadExcelUpload(ByVal sheet As ExcelWorksheet, data As DataTable) As DataTable
        Try

            '3-Aug-2021 : Validasi jika file excel kosong
            Dim strCheck As String = sheet.Cells(1, 1).Text.Trim
            If String.IsNullOrEmpty(strCheck) Then
                Throw New ApplicationException("Excel file is empty or has no data row or has invalid template.")
            End If

            Dim intIDLaporan As Integer = 0
            Dim intJenisLaporan As Integer = 0
            Dim intFile As Integer = 0
            Dim intDibuatOleh As Integer = 0
            Dim intPembaruanOleh As Integer = 0
            Dim intNamaOrganisasi As Integer = 0
            Dim intPengirim As Integer = 0
            Dim intTransaksi As Integer = 0
            Dim intDitolak As Integer = 0
            Dim intStatus As Integer = 0
            Dim intTerkirimSTR As Integer = 0

            For i As Integer = 1 To 11 Step 1
                If sheet.Cells(1, i).Text.Trim = "Key Laporan" Then
                    intIDLaporan = i
                ElseIf sheet.Cells(1, i).Text.Trim = "Jenis Laporan" Then
                    intJenisLaporan = i
                ElseIf sheet.Cells(1, i).Text.Trim = "Nama File" Then
                    intFile = i
                ElseIf sheet.Cells(1, i).Text.Trim = "Dibuat oleh" Then
                    intDibuatOleh = i
                ElseIf sheet.Cells(1, i).Text.Trim = "Pembaruan terakhir oleh" Then
                    intPembaruanOleh = i
                ElseIf sheet.Cells(1, i).Text.Trim = "Nama Organisasi" Then
                    intNamaOrganisasi = i
                ElseIf sheet.Cells(1, i).Text.Trim = "Pengirim" Then
                    intPengirim = i
                ElseIf sheet.Cells(1, i).Text.Trim = "Transaksi" Then
                    intTransaksi = i
                ElseIf sheet.Cells(1, i).Text.Trim = "Trns. Ditolak" Then
                    intDitolak = i
                ElseIf sheet.Cells(1, i).Text.Trim = "Status" Then
                    intStatus = i
                    'Daniel 20220623 Update PPATK Start
                    'ElseIf sheet.Cells(1, i).Text.Trim = "SubmitDate" Then
                ElseIf sheet.Cells(1, i).Text.Trim = "Terkirim pada" Then
                    'Daniel 20220623 Update PPATK End
                    intTerkirimSTR = i
                End If
            Next

            'If intIDLaporan = 0 OrElse intJenisLaporan = 0 OrElse intFile = 0 OrElse intTerkirimSTR = 0 OrElse intDitolak = 0 OrElse intStatus = 0 Then
            '    Throw New ApplicationException("System cannot find Field with Header 'Key Laporan', 'Jenis Laporan', 'Nama File', 'SubmitDate', 'Trns. Ditolak', 'Status' Which is mandatory")
            'End If
            If intIDLaporan = 0 OrElse intTerkirimSTR = 0 OrElse intFile = 0 Then
                'Daniel 20220623 Update PPATK Start
                'Throw New ApplicationException("System cannot find Field with Header 'Key Laporan', 'SubmitDate', and 'Nama File' Which is mandatory")
                Throw New ApplicationException("System cannot find Field with Header 'Key Laporan', 'Terkirim pada', and 'Nama File' Which is mandatory")
                'Daniel 20220623 Update PPATK End
            End If

            Dim IRow As Integer = 2

            While IRow <> 0
                'Dim strName As String = ""
                Dim IDLaporan As String = ""
                Dim JenisLaporan As String = ""
                Dim File As String = ""
                Dim DibuatOleh As String = ""
                Dim PembaruanOleh As String = ""
                Dim NamaOrganisasi As String = ""
                Dim Pengirim As String = ""
                Dim Transaksi As String = ""
                Dim Ditolak As String = ""
                Dim Status As String = ""
                Dim TerkirimSTR As String = ""
                Dim TerkirimPada As Date
                Dim PK_Report As Integer

                If intIDLaporan <> 0 Then
                    IDLaporan = sheet.Cells(IRow, intIDLaporan).Text.Trim
                End If

                If intJenisLaporan <> 0 Then
                    JenisLaporan = sheet.Cells(IRow, intJenisLaporan).Text.Trim
                End If

                If intFile <> 0 Then
                    File = sheet.Cells(IRow, intFile).Text.Trim
                End If

                If intDibuatOleh <> 0 Then
                    DibuatOleh = sheet.Cells(IRow, intDibuatOleh).Text.Trim
                End If

                If intPembaruanOleh <> 0 Then
                    PembaruanOleh = sheet.Cells(IRow, intPembaruanOleh).Text.Trim
                End If

                If intNamaOrganisasi <> 0 Then
                    NamaOrganisasi = sheet.Cells(IRow, intNamaOrganisasi).Text.Trim
                End If

                If intPengirim <> 0 Then
                    Pengirim = sheet.Cells(IRow, intPengirim).Text.Trim
                End If

                If intTransaksi <> 0 Then
                    Transaksi = sheet.Cells(IRow, intTransaksi).Text.Trim
                End If

                If intDitolak <> 0 Then
                    Ditolak = sheet.Cells(IRow, intDitolak).Text.Trim
                End If

                If intStatus <> 0 Then
                    Status = sheet.Cells(IRow, intStatus).Text.Trim
                End If

                If intTerkirimSTR <> 0 Then
                    TerkirimSTR = sheet.Cells(IRow, intTerkirimSTR).Text.Trim
                    'Daniel 20220623 Update PPATK Start
                    Try
                        TerkirimPada = sheet.Cells(IRow, intTerkirimSTR).Value
                    Catch ex As Exception
                        Dim row As Integer = IRow - 1
                        Throw New Exception("Format 'Terkirim pada' Row Number: " & row & " is not Date")
                    End Try
                    'Daniel 20220623 Update PPATK End
                End If

                'IDLaporan = sheet.Cells(IRow, 1).Text.Trim
                'JenisLaporan = sheet.Cells(IRow, 2).Text.Trim
                'File = sheet.Cells(IRow, 3).Text.Trim
                'DibuatOleh = sheet.Cells(IRow, 4).Text.Trim
                'PembaruanOleh = sheet.Cells(IRow, 5).Text.Trim
                'NamaOrganisasi = sheet.Cells(IRow, 6).Text.Trim
                'Pengirim = sheet.Cells(IRow, 7).Text.Trim
                'Transaksi = sheet.Cells(IRow, 8).Text.Trim
                'Ditolak = sheet.Cells(IRow, 9).Text.Trim
                'Status = sheet.Cells(IRow, 10).Text.Trim
                'TerkirimSTR = sheet.Cells(IRow, 11).Text.Trim

                If Not (String.IsNullOrEmpty(IDLaporan) AndAlso
                    String.IsNullOrEmpty(JenisLaporan) AndAlso
                    String.IsNullOrEmpty(File) AndAlso
                    String.IsNullOrEmpty(DibuatOleh) AndAlso
                    String.IsNullOrEmpty(PembaruanOleh) AndAlso
                    String.IsNullOrEmpty(NamaOrganisasi) AndAlso
                    String.IsNullOrEmpty(Pengirim) AndAlso
                    String.IsNullOrEmpty(Transaksi) AndAlso
                    String.IsNullOrEmpty(Ditolak) AndAlso
                    String.IsNullOrEmpty(Status) AndAlso
                    String.IsNullOrEmpty(TerkirimSTR)) Then

                    Dim RowNum As Integer = IRow - 1
                    If String.IsNullOrEmpty(IDLaporan) Then
                        Throw New Exception("Key Laporan Row Number: " & RowNum & " is Empty")
                    End If
                    If String.IsNullOrEmpty(TerkirimSTR) Then
                        'Daniel 20220623 Update PPATK Start
                        'Throw New Exception("SubmitDate Row Number: " & RowNum & " is Empty")
                        Throw New Exception("Terkirim pada Row Number: " & RowNum & " is Empty")
                        'Daniel 20220623 Update PPATK End
                    End If

                    Dim extension As String = Path.GetExtension(File)
                    If extension.ToLower() = ".xml" Then
                        If String.IsNullOrEmpty(JenisLaporan) Then
                            Throw New Exception("Jenis Laporan Row Number: " & RowNum & " is Empty (Validation for 'Nama File' with extention .xml)")
                        End If
                        If String.IsNullOrEmpty(Status) Then
                            Throw New Exception("Status Row Number: " & RowNum & " is Empty (Validation for 'Nama File' with extention .xml)")
                        End If
                    End If

                    ''If String.IsNullOrEmpty(File) Then
                    ''    Throw New Exception("Nama File Row Number: " & RowNum & " is Empty")
                    ''End If
                    ''If String.IsNullOrEmpty(Ditolak) Then
                    ''    Throw New Exception("Trns. Ditolak Row Number: " & RowNum & " is Empty")
                    ''End If
                    ' Try to convert the date
                    'Try
                    '    'TerkirimPada = DateTime.ParseExact(TerkirimSTR, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", System.Globalization.CultureInfo.InvariantCulture) ' Edit on 18 Dec 2020, dd/MM/yyyy jadi d/M/yyyy. Thanks to Tim SCB
                    '    Dim dateterkirim As DateTimeOffset = DateTimeOffset.ParseExact(TerkirimSTR, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", System.Globalization.CultureInfo.InvariantCulture)
                    '    TerkirimPada = dateterkirim.DateTime
                    'Catch ex As Exception
                    '    Dim RowNum As Integer = IRow - 1
                    '    Throw New Exception("Row Number: " & RowNum & " has invalid date format. Please use yyyy-MM-dd'T'HH:mm:ss.SSS'Z' format! " & ex.ToString()) ' Edit on 18 Dec 2020, dd/MM/yyyy jadi d/M/yyyy. Thanks to Tim SCB
                    'End Try
                    'Daniel 20220623 Update PPATK Start di comment karena dari excel fieldnya sudah date
                    'Try
                    '    'If intTerkirimSTR <> 0 Then
                    '    '    TerkirimSTR = TerkirimSTR.Substring(0, 10)
                    '    '    TerkirimPada = DateTime.ParseExact(TerkirimSTR, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture) 
                    '    'End If
                    '    TerkirimSTR = TerkirimSTR.Substring(0, 10)
                    '    ''TerkirimPada = DateTime.ParseExact(TerkirimSTR, "yyyy-M-d", System.Globalization.CultureInfo.InvariantCulture) ' Edit on 18 Dec 2020, dd/MM/yyyy jadi d/M/yyyy. Thanks to Tim SCB
                    '    'Daniel 20220623 Update PPATK Start
                    '    'TerkirimPada = DateTime.ParseExact(TerkirimSTR, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture)
                    '    'TerkirimPada = DateTime.ParseExact(TerkirimSTR, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                    '    TerkirimPada = DateTime.ParseExact(TerkirimSTR, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                    '    'Daniel 20220623 Update PPATK End
                    'Catch ex As Exception
                    '    'Throw New Exception("Row Number: " & RowNum & " has invalid date format. Please use yyyy-M-d format!") ' Edit on 18 Dec 2020, dd/MM/yyyy jadi d/M/yyyy. Thanks to Tim SCB
                    '    'Daniel 20220623 Update PPATK Start
                    '    'Throw New Exception("SubmitDate Row Number: " & RowNum & " has invalid date format. Please use yyyy-MM-dd format!") ' Edit on 18 Dec 2020, dd/MM/yyyy jadi d/M/yyyy. Thanks to Tim SCB
                    '    'Throw New Exception("Terkirim pada Row Number: " & RowNum & " has invalid date format. Please use dd/MM/yyyy format")
                    '    Throw New Exception("Terkirim pada Row Number: " & RowNum & " has invalid date format. Please use MM/dd/yyyy format")
                    '    'Daniel 20220623 Update PPATK End
                    'End Try
                    'Daniel 20220623 Update PPATK End

                    ' Get the number inside the brackets
                    Dim regex As Regex = New Regex("\[(\d+)\]")
                    Dim match As Match = regex.Match(IO.Path.GetFileNameWithoutExtension(File))
                    If match.Success Then
                        PK_Report = Convert.ToInt32(match.Groups(1).Value)
                    Else
                        PK_Report = 0
                    End If
                    If String.IsNullOrEmpty(Transaksi) Then
                        Transaksi = "0"
                    End If
                    If String.IsNullOrEmpty(Ditolak) Then
                        Ditolak = "0"
                    End If

                    Dim row As DataRow = data.NewRow()
                    row("ID_Laporan") = IDLaporan
                    row("Jenis_Laporan") = JenisLaporan
                    row("FileName") = File
                    row("Dibuat_Oleh") = DibuatOleh
                    row("Pembaruan_terakhir_oleh") = PembaruanOleh
                    row("Nama_Organisasi") = NamaOrganisasi
                    row("Pengirim") = Pengirim
                    row("Transaksi") = Transaksi
                    row("Transaksi_Ditolak") = Ditolak
                    row("Status") = Status
                    row("Terkirim_pada") = TerkirimPada
                    row("PK_Report") = PK_Report
                    data.Rows.Add(row)

                    IRow = IRow + 1
                Else
                    IRow = 0
                End If
            End While
            Return data
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Shared  Function ValidateDataNumberic(data As String) As Boolean
        Try
            If (data.All(AddressOf Char.IsDigit)) Then
                Return True
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
