﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="ReportAddressEdit.aspx.vb" Inherits="goAML_ReportAddressEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:FormPanel runat="server" Title="Report Address Edit Form" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="PanelAddressReport">
        <Defaults>
            <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
        </Defaults>
        <Items>
            <ext:DisplayField ID="PKReportAddress" runat="server" FieldLabel="ID"></ext:DisplayField>
            <ext:DisplayField ID="DspFK_Ref_detail_of" runat="server" Text="Report Address" FieldLabel="Detail Dari"></ext:DisplayField>
            <ext:DisplayField ID="DspFK_to_Table_ID" runat="server" Text="0" FieldLabel="Address Of"></ext:DisplayField>
            <ext:SelectBox ID="SbAddress_type" runat="server" AllowBlank="false" FieldLabel="Tipe Alamat" DisplayField="display" ValueField="code" EmptyText="Pilih Tipe Alamat..." Flex="1" AnchorHorizontal="80%">
                <Store>
                    <ext:Store runat="server">
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="code" />
                                    <ext:ModelField Name="display" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Change Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                        this.clearValue();
                        this.getTrigger(0).hide();}" />
                </Listeners>
            </ext:SelectBox>
            <ext:TextField ID="TxtAddress" runat="server" AllowBlank="false" FieldLabel="Alamat" AnchorHorizontal="80%"></ext:TextField>
            <ext:TextField ID="TxtTown" runat="server" FieldLabel="Kecamatan" AnchorHorizontal="80%"></ext:TextField>
            <ext:TextField ID="TxtCity" runat="server" AllowBlank="false" FieldLabel="Kota/Kabupaten" AnchorHorizontal="80%"></ext:TextField>
            <ext:TextField ID="TxtZip" runat="server" FieldLabel="Kode Pos" AnchorHorizontal="80%"></ext:TextField>
            <ext:SelectBox ID="Sbcountry_code" runat="server" AllowBlank="false" FieldLabel="Negara" DisplayField="display" ValueField="code" EmptyText="Pilih Negara..." Flex="1" AnchorHorizontal="80%">
                <Store>
                    <ext:Store runat="server">
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="code" />
                                    <ext:ModelField Name="display" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Change Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                        this.clearValue();
                        this.getTrigger(0).hide();}" />
                </Listeners>
            </ext:SelectBox>
            <ext:TextField ID="TxtState" runat="server" FieldLabel="Provinsi" AnchorHorizontal="80%"></ext:TextField>
            <ext:TextField ID="Txtcomments" runat="server" FieldLabel="Catatan" AnchorHorizontal="80%"></ext:TextField>
        </Items>
        <Buttons>
            <ext:Button ID="btnSave" runat="server" Icon="Disk" Text="Save Process">
                <Listeners>
                    <Click Handler="if (!#{PanelAddressReport}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="BtnSave_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>

        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
