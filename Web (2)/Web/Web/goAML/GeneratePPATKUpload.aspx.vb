﻿Imports Ext
Imports Elmah
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL

Partial Class goAML_GeneratePPATKUpload
    Inherits System.Web.UI.Page

    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("goAML_GeneratePPATKUpload.Module")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_GeneratePPATKUpload.Module") = value
        End Set
    End Property

    Public Property ObjGeneratedgoAML() As goAML_Generate_XML
        Get
            Return Session("goAML_GeneratePPATKUpload.ObjGeneratedgoAML")
        End Get
        Set(ByVal value As goAML_Generate_XML)
            Session("goAML_GeneratePPATKUpload.ObjGeneratedgoAML") = value
        End Set
    End Property
    Private Sub ClearSession()
        ObjModule = Nothing
        ObjGeneratedgoAML = Nothing
    End Sub
    Private Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

    End Sub
    Private Sub LoadData()
        ObjGeneratedgoAML = GeneratePPATKBLL.GetGeneratedgoAMLByID(Common.DecryptQueryString(Request.Params("ID"), SystemParameterBLL.GetEncriptionKey))

        If ObjGeneratedgoAML IsNot Nothing Then
            With ObjGeneratedgoAML
                lblReportType.Text = .Report_Type
                lblStatusUpload.Text = .Status
                lblTotalInvalid.Text = .Total_Invalid

                lblTotalValid.Text = .Total_Valid
                lblTransDate.Text = .Transaction_Date
                lblTransType.Text = .Jenis_Laporan
                lblLastUpdateDate.Text = Convert.ToDateTime(.Last_Update_Date).ToString("dd-MMM-yyyy")
            End With
        End If
    End Sub
    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()

                Dim Moduleid As String = Request.Params("ModuleID")

                Dim intModuleid As Integer
                Dim intIFTIID As Integer
                Try
                    intModuleid = Common.DecryptQueryString(Moduleid, SystemParameterBLL.GetEncriptionKey)
                    intIFTIID = Common.DecryptQueryString(Request.Params("ID"), SystemParameterBLL.GetEncriptionKey)
                    ObjModule = ModuleBLL.GetModuleByModuleID(intModuleid)

                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Insert) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    FormPanelInput.Title = ObjModule.ModuleLabel & " Submission"

                    LoadData()

                Catch ex As Exception
                    ErrorSignal.FromCurrentContext.Raise(ex)
                    Net.X.Msg.Alert("Error", ex.Message).Show()
                End Try
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSave_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            If txt_NoReferensiPPATK.Text = "" Then
                Throw New Exception("Nomor Referensi Wajib Diisi")
            End If
            If txt_DateReferensi_PPATK.Text = "" Then
                Throw New Exception("Tanggal Referensi Wajib Diisi")
            End If

            Dim objUploadPPATK As New GeneratePPATKBLL
            objUploadPPATK.UploadReportPPATK(ObjGeneratedgoAML, txt_DateReferensi_PPATK.Text, txt_NoReferensiPPATK.Text, ObjModule.ModuleLabel)

            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True
            LblConfirmation.Text = "Data Saved into Database"

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            '2020 09 15 Added By Apuy
            If ObjModule.UrlView.Contains("ReportType") Then
                Ext.Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "ModuleID=" & Moduleid)
            Else
                Ext.Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub btnCancelUpload_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            'Update Ryan 20200915
            'Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "ModuleID=" & Moduleid)

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Net.X.Msg.Alert("Informasi", ex.Message).Show()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class
