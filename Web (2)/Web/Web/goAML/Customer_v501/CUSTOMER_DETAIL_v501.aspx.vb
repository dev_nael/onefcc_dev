﻿Imports System.Data
Imports NawaBLL
Imports GoAMLBLL
Imports GoAMLBLL.goAML
Imports GoAMLBLL.goAML.Services
Imports GoAMLBLL.goAML_Customer.Services
Imports GoAMLDAL
Partial Class CUSTOMER_DETAIL_v501
    Inherits ParentPage
    Public objFormModuleView As NawaBLL.FormModuleDetail

    Private mModuleName As String

    Public Property ModulName() As String
        Get
            Return mModuleName
        End Get
        Set(ByVal value As String)
            mModuleName = value
        End Set
    End Property
    Sub ClearSession()
        objListgoAML_Ref_Director_Phone = New List(Of Vw_Director_Phones)
        objListgoAML_Ref_Director_Address = New List(Of Vw_Director_Addresses)
        objListgoAML_Ref_Director_Employer_Phone = New List(Of Vw_Director_Employer_Phones)
        objListgoAML_Ref_Director_Employer_Address = New List(Of Vw_Director_Employer_Addresses)
        objListgoAML_Ref_Identification_Director = New List(Of goAML_Person_Identification)
        objListgoAML_vw_Customer_Identification_Director = New List(Of Vw_Person_Identification)
        objListgoAML_Ref_Phone = Nothing
        objListgoAML_Ref_Address = Nothing

        'add goAML 5.0.1, Septian, 25 Jan 2023
        obj_Customer2 = New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2()
        objListgoAML_Ref_Previous_Name = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        objListgoAML_vw_Previous_Name = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)

        objListgoAML_Ref_Email = New List(Of DataModel.goAML_Ref_Customer_Email)
        objListgoAML_vw_Email = New List(Of DataModel.goAML_Ref_Customer_Email)

        objListgoAML_Ref_EmploymentHistory = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        objListgoAML_vw_EmploymentHistory = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)

        objListgoAML_Ref_PersonPEP = New List(Of DataModel.goAML_Ref_Customer_PEP)
        objListgoAML_vw_PersonPEP = New List(Of DataModel.goAML_Ref_Customer_PEP)

        objListgoAML_Ref_NetworkDevice = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        objListgoAML_vw_NetworkDevice = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)

        objListgoAML_Ref_SocialMedia = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        objListgoAML_vw_SocialMedia = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)

        objListgoAML_Ref_Sanction = New List(Of DataModel.goAML_Ref_Customer_Sanction)
        objListgoAML_vw_Sanction = New List(Of DataModel.goAML_Ref_Customer_Sanction)

        objListgoAML_Ref_RelatedPerson = New List(Of DataModel.goAML_Ref_Customer_Related_Person)
        objListgoAML_vw_RelatedPerson = New List(Of DataModel.goAML_Ref_Customer_Related_Person)

        objListgoAML_Ref_AdditionalInfo = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        objListgoAML_vw_AdditionalInfo = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)

        objListgoAML_Ref_EntityUrl = New List(Of DataModel.goAML_Ref_Customer_URL)
        objListgoAML_vw_EntityUrl = New List(Of DataModel.goAML_Ref_Customer_URL)

        objListgoAML_Ref_EntityIdentification = New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        objListgoAML_vw_EntityIdentification = New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)

        objListgoAML_Ref_RelatedEntities = New List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        objListgoAML_vw_RelatedEntities = New List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        'End Add
    End Sub

    Public Property objListgoAML_Ref_Address() As List(Of GoAMLDAL.Vw_Customer_Addresses)
        Get
            Return Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Customer_Addresses))
            Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Address") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Phone() As List(Of GoAMLDAL.Vw_Customer_Phones)
        Get
            Return Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Customer_Phones))
            Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Phone") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Address() As List(Of GoAMLDAL.Vw_Director_Addresses)
        Get
            Return Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Director_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Addresses))
            Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Director_Address") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Phone() As List(Of GoAMLDAL.Vw_Director_Phones)
        Get
            Return Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Director_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Phones))
            Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Director_Phone") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Employer_Address() As List(Of GoAMLDAL.Vw_Director_Employer_Addresses)
        Get
            Return Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Director_Employer_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Employer_Addresses))
            Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Director_Employer_Address") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Employer_Phone() As List(Of GoAMLDAL.Vw_Director_Employer_Phones)
        Get
            Return Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Director_Employer_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Employer_Phones))
            Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Director_Employer_Phone") = value
        End Set
    End Property

    Public Property objTempCustomerAddresslEdit() As GoAMLDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_DETAIL_v501.objTempCustomerAddresslEdit")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Address)
            Session("CUSTOMER_DETAIL_v501.objTempCustomerAddresslEdit") = value
        End Set
    End Property

    Public Property objTempCustomerPhoneEdit() As GoAMLDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_DETAIL_v501.objTempCustomerPhoneEdit")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Phone)
            Session("CUSTOMER_DETAIL_v501.objTempCustomerPhoneEdit") = value
        End Set
    End Property

    Public Property obj_Customer As GoAMLDAL.goAML_Ref_Customer
        Get
            Return Session("CUSTOMER_DETAIL_v501.obj_Customer")
        End Get
        Set(value As GoAMLDAL.goAML_Ref_Customer)
            Session("CUSTOMER_DETAIL_v501.obj_Customer") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Identification_Director() As List(Of GoAMLDAL.goAML_Person_Identification)
        Get
            Return Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Identification_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Person_Identification))
            Session("CUSTOMER_DETAIL_v501.objListgoAML_Ref_Identification_Director") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Identification_Director() As List(Of GoAMLDAL.Vw_Person_Identification)
        Get
            Return Session("CUSTOMER_DETAIL_v501.objListgoAML_vw_Customer_Identification_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Person_Identification))
            Session("CUSTOMER_DETAIL_v501.objListgoAML_vw_Customer_Identification_Director") = value
        End Set
    End Property

    '' Add 04-Jan-2023, Septian. GoAML 5.0.1
    Public Property objTempCustomerPreviousNameEdit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomerPreviousName")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
            Session("CUSTOMER_EDIT_v501.objTempCustomerPreviousName") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Previous_Name() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Previous_Name")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Previous_name") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Previous_Name() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Previous_Name")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Previous_Name") = value
        End Set
    End Property

    Public Property objTempCustomer_Email_Edit() As DataModel.goAML_Ref_Customer_Email
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_Email_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Email)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_Email_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Email() As List(Of DataModel.goAML_Ref_Customer_Email)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Email")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Email))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Email") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Email() As List(Of DataModel.goAML_Ref_Customer_Email)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Email")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Email))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Email") = value
        End Set
    End Property

    Public Property objTempCustomer_EmploymentHistory_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_EmploymentHistory_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_EmploymentHistory_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_EmploymentHistory() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_EmploymentHistory")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_EmploymentHistory") = value
        End Set
    End Property

    Public Property objListgoAML_vw_EmploymentHistory() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_EmploymentHistory")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_EmploymentHistory") = value
        End Set
    End Property

    Public Property objTempCustomer_PersonPEP_Edit() As DataModel.goAML_Ref_Customer_PEP
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_PersonPEP_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_PEP)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_PersonPEP_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_PersonPEP() As List(Of DataModel.goAML_Ref_Customer_PEP)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_PersonPEP")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_PEP))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_PersonPEP") = value
        End Set
    End Property

    Public Property objListgoAML_vw_PersonPEP() As List(Of DataModel.goAML_Ref_Customer_PEP)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_PersonPEP")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_PEP))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_PersonPEP") = value
        End Set
    End Property
    Public Property objTempCustomer_NetworkDevice_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_NetworkDevice_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_NetworkDevice_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_NetworkDevice() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_NetworkDevice")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_NetworkDevice") = value
        End Set
    End Property

    Public Property objListgoAML_vw_NetworkDevice() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_NetworkDevice")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_NetworkDevice") = value
        End Set
    End Property

    Public Property objTempCustomer_SocialMedia_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTempCustomer_SocialMedia_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
            Session("CUSTOMER_EDIIT_v501.objTempCustomer_SocialMedia_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_SocialMedia() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_SocialMedia")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_SocialMedia") = value
        End Set
    End Property
    Public Property objListgoAML_vw_SocialMedia() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_SocialMedia")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_SocialMedia") = value
        End Set
    End Property

    Public Property objTempCustomer_Sanction_Edit() As DataModel.goAML_Ref_Customer_Sanction
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_Sanction_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Sanction)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_Sanction_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_Sanction() As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Sanction")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Sanction))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Sanction") = value
        End Set
    End Property
    Public Property objListgoAML_vw_Sanction() As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Sanction")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Sanction))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Sanction") = value
        End Set
    End Property

    Public Property objTempCustomer_RelatedPerson_Edit() As DataModel.goAML_Ref_Customer_Related_Person
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_RelatedPerson_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Person)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_RelatedPerson_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_RelatedPerson() As List(Of DataModel.goAML_Ref_Customer_Related_Person)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_RelatedPerson")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Person))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_RelatedPerson") = value
        End Set
    End Property
    Public Property objListgoAML_vw_RelatedPerson() As List(Of DataModel.goAML_Ref_Customer_Related_Person)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_RelatedPerson")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Person))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_RelatedPerson") = value
        End Set
    End Property

    Public Property objTempCustomer_AdditionalInfo_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_AdditionalInfo_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_AdditionalInfo_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_AdditionalInfo() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_AdditionalInfo")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_AdditionalInfo") = value
        End Set
    End Property
    Public Property objListgoAML_vw_AdditionalInfo() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_AdditionalInfo")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_AdditionalInfo") = value
        End Set
    End Property

    Public Property objTempCustomer_EntityUrl_Edit() As DataModel.goAML_Ref_Customer_URL
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_EntityUrl_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_URL)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_EntityUrl_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityUrl() As List(Of DataModel.goAML_Ref_Customer_URL)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_EntityUrl")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_URL))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_EntityUrl") = value
        End Set
    End Property
    Public Property objListgoAML_vw_EntityUrl() As List(Of DataModel.goAML_Ref_Customer_URL)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_EntityUrl")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_URL))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_EntityUrl") = value
        End Set
    End Property

    Public Property objTempCustomer_EntityIdentification_Edit() As DataModel.goAML_Ref_Customer_Entity_Identification
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_EntityIdentification_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Entity_Identification)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_EntityIdentification_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityIdentification() As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_EntityIdentification")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_EntityIdentification") = value
        End Set
    End Property
    Public Property objListgoAML_vw_EntityIdentification() As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_EntityIdentification")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_EntityIdentification") = value
        End Set
    End Property

    Public Property objTempCustomer_RelatedEntities_Edit() As DataModel.goAML_Ref_Customer_Related_Entities
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_RelatedEntities_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Entities)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_RelatedEntities_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_RelatedEntities() As List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_RelatedEntities")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Entities))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_RelatedEntities") = value
        End Set
    End Property
    Public Property objListgoAML_vw_RelatedEntities() As List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_RelatedEntities")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Entities))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_RelatedEntities") = value
        End Set
    End Property

    Public Property obj_Customer2 As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2
        Get
            Return Session("CUSTOMER_EDIT_v501.obj_Customer2")
        End Get
        Set(value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2)
            Session("CUSTOMER_EDIT_v501.obj_Customer2") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Email() As DataModel.goAML_Ref_Customer_Email
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Email")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Email)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Email") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_PEP() As DataModel.goAML_Ref_Customer_PEP
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_PEP")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_PEP)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_PEP") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Sanction() As DataModel.goAML_Ref_Customer_Sanction
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Sanction")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Sanction)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Sanction") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Related_Person() As DataModel.goAML_Ref_Customer_Related_Person
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Related_Person")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Person)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Related_Person") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_URL() As DataModel.goAML_Ref_Customer_URL
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_URL")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_URL)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_URL") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Entity_Identification() As DataModel.goAML_Ref_Customer_Entity_Identification
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Entity_Identification")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Entity_Identification)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Entity_Identification") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Related_Entities() As DataModel.goAML_Ref_Customer_Related_Entities
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Related_Entities")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Entities)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Related_Entities") = value
        End Set
    End Property
    '' End 04-Jan-2023


    Protected Sub BtnBack_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Private Function GetId() As String
        Dim strid As String = Request.Params("ID")
        Dim id As String = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
        Return id
    End Function

    Sub ClearinputCustomerPhones()

        txt_phone_Country_prefix.Text = ""
        txt_phone_number.Text = ""
        txt_phone_extension.Text = ""

    End Sub

    Private Sub CUSTOMER_DETAIL_v501_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            'Dedy added 31082020 aktif is hidden
            txt_statusrekening.Hidden = True
            'Dedy end added 31082020
            ClearSession()
            GridPanelSetting()
            Maximizeable()
            Dim strid As String = GetId()
            Dim strModuleid As String = Request.Params("ModuleID")
            Dim INDV_gender As GoAMLDAL.goAML_Ref_Jenis_Kelamin = Nothing
            Dim INDV_statusCode As GoAMLDAL.goAML_Ref_Status_Rekening = Nothing
            Dim INDV_nationality1 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_nationality2 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_nationality3 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_residence As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim CORP_Incorporation_legal_form As GoAMLDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
            Dim CORP_incorporation_country_code As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim CORP_Role As GoAMLDAL.goAML_Ref_Party_Role = Nothing

            GridAddressWork_RelatedPerson.IsViewMode = True
            GridAddress_RelatedPerson.IsViewMode = True
            GridPhone_RelatedPerson.IsViewMode = True
            GridPhoneWork_RelatedPerson.IsViewMode = True
            GridIdentification_RelatedPerson.IsViewMode = True
            GridEmail_RelatedPerson.IsViewMode = True
            GridSanction_RelatedPerson.IsViewMode = True
            GridPEP_RelatedPerson.IsViewMode = True
            GridPhone_RelatedEntity.IsViewMode = True
            GridAddress_RelatedEntity.IsViewMode = True
            GridEmail_RelatedEntity.IsViewMode = True
            GridEntityIdentification_RelatedEntity.IsViewMode = True
            GridURL_RelatedEntity.IsViewMode = True
            GridSanction_RelatedEntity.IsViewMode = True
            GridEmail_Director.IsViewMode = True
            GridSanction_Director.IsViewMode = True
            GridPEP_Director.IsViewMode = True

            Try
                obj_Customer = goAML_CustomerBLL.GetCustomerbyCIF(strid)

                'Dedy added 01092020
                If obj_Customer IsNot Nothing Then

                    txt_CIF.Text = obj_Customer.CIF
                    txt_GCN.Text = obj_Customer.GCN
                    If obj_Customer.isGCNPrimary = True Then
                        GCNPrimary.Checked = True
                    End If
                    If obj_Customer.Closing_Date IsNot Nothing Then
                        TextFieldClosingDate.Text = obj_Customer.Closing_Date.Value.ToString("dd-MMM-yyyy")
                    End If
                    If obj_Customer.Opening_Date IsNot Nothing Then
                        TextFieldOpeningDate.Text = obj_Customer.Opening_Date.Value.ToString("dd-MMM-yyyy")
                    End If
                    If obj_Customer.Comments IsNot Nothing Then
                        TextFieldComments.Text = obj_Customer.Comments
                    End If


                    Using objdbs As New GoAMLDAL.GoAMLEntities
                        If obj_Customer.Opening_Branch_Code IsNot Nothing And Not String.IsNullOrEmpty(obj_Customer.Opening_Branch_Code) Then
                            Dim objbranch = objdbs.AML_BRANCH.Where(Function(x) x.FK_AML_BRANCH_CODE = obj_Customer.Opening_Branch_Code).FirstOrDefault
                            If Not objbranch Is Nothing Then
                                cmb_openingBranch.SetTextWithTextValue(objbranch.FK_AML_BRANCH_CODE, objbranch.BRANCH_NAME)
                            End If
                        End If

                        If obj_Customer.Status IsNot Nothing And Not String.IsNullOrEmpty(obj_Customer.Status) Then
                            Dim objstatus = objdbs.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = obj_Customer.Status).FirstOrDefault
                            If Not objstatus Is Nothing Then
                                cmb_Status.SetTextWithTextValue(objstatus.Kode, objstatus.Keterangan)
                            End If
                        End If

                    End Using


                    If obj_Customer.isUpdateFromDataSource = True Then
                        UpdateFromDataSource.Checked = True
                    End If
                    Dim statusrekening = goAML_CustomerBLL.GetStatusRekeningbyID(obj_Customer.Status_Code)
                    'If txt_statusrekening IsNot Nothing Then
                    '    txt_statusrekening.Text = statusrekening.Keterangan
                    'End If

                    'fix object reference set to null saad 20102020
                    If statusrekening IsNot Nothing Then
                        txt_statusrekening.Text = statusrekening.Keterangan
                    End If

                    If obj_Customer.FK_Customer_Type_ID = 1 Then
                        txt_INDV_Title.Text = obj_Customer.INDV_Title

                        INDV_gender = goAML_CustomerBLL.GetJenisKelamin(obj_Customer.INDV_Gender)
                        If INDV_gender IsNot Nothing Then
                            txt_INDV_Gender.Text = INDV_gender.Keterangan
                        End If
                        txt_INDV_Last_Name.Text = obj_Customer.INDV_Last_Name
                        If obj_Customer.INDV_BirthDate.HasValue Then
                            txt_INDV_Birthdate.Text = obj_Customer.INDV_BirthDate.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            txt_INDV_Birthdate.Text = ""
                        End If
                        txt_INDV_Birth_place.Text = obj_Customer.INDV_Birth_Place
                        txt_INDV_Mothers_name.Text = obj_Customer.INDV_Mothers_Name
                        txt_INDV_Alias.Text = obj_Customer.INDV_Alias
                        txt_INDV_SSN.Text = obj_Customer.INDV_SSN
                        txt_INDV_Passport_number.Text = obj_Customer.INDV_Passport_Number
                        If Not String.IsNullOrEmpty(obj_Customer.INDV_Passport_Country) Then
                            txt_INDV_Passport_country.Text = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Passport_Country).Keterangan
                        Else
                            txt_INDV_Passport_country.Text = ""
                        End If
                        txt_INDV_ID_Number.Text = obj_Customer.INDV_ID_Number

                        INDV_nationality1 = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Nationality1)
                        If INDV_nationality1 IsNot Nothing Then
                            txt_INDV_Nationality1.Text = INDV_nationality1.Keterangan
                        End If

                        INDV_nationality2 = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Nationality2)
                        If INDV_nationality2 IsNot Nothing Then
                            txt_INDV_Nationality2.Text = INDV_nationality2.Keterangan
                        End If

                        INDV_nationality3 = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Nationality3)
                        If INDV_nationality3 IsNot Nothing Then
                            txt_INDV_Nationality3.Text = INDV_nationality3.Keterangan
                        End If

                        INDV_residence = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Residence)
                        If INDV_residence IsNot Nothing Then
                            txt_INDV_Residence.Text = INDV_residence.Keterangan
                        End If

                        'comment by Septian, goAML 5.0.1
                        'uncomment by Nael, goAML 5.2
                        txt_INDV_Email.Text = obj_Customer.INDV_Email
                        txt_INDV_Email2.Text = obj_Customer.INDV_Email2
                        txt_INDV_Email3.Text = obj_Customer.INDV_Email3
                        txt_INDV_Email4.Text = obj_Customer.INDV_Email4
                        txt_INDV_Email5.Text = obj_Customer.INDV_Email5
                        'end comment
                        txt_INDV_Tax_Number.Text = obj_Customer.INDV_Tax_Number
                        txt_INDV_Source_of_Wealth.Text = obj_Customer.INDV_Source_of_Wealth

                        If obj_Customer.INDV_Deceased = True Then
                            cb_Deceased.Checked = True
                            txt_Deceased_Date.Hidden = False
                            txt_Deceased_Date.Text = obj_Customer.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        End If
                        If obj_Customer.INDV_Tax_Reg_Number = True Then
                            cb_Tax.Checked = True
                        End If
                        txt_INDV_Occupation.Text = obj_Customer.INDV_Occupation
                        txt_INDV_Employer_Name.Text = obj_Customer.INDV_Employer_Name

                        '' Phone
                        objListgoAML_Ref_Phone = GoAMLBLL.goAML_CustomerBLL.GetVWCustomerPhones(obj_Customer.PK_Customer_ID)
                        Store_INDV_Phones.DataSource = objListgoAML_Ref_Phone.ToList()
                        Store_INDV_Phones.DataBind()

                        '' Address
                        Dim objlist_address = GoAMLBLL.goAML_CustomerBLL.GetVWCustomerAddresses(obj_Customer.PK_Customer_ID)
                        Store_INDVD_Address.DataSource = objlist_address
                        Store_INDVD_Address.DataBind()

                        '' Identification
                        Dim objlist_Identification = GoAMLBLL.goAML_CustomerBLL.GetVWCustomerIdentifications(obj_Customer.PK_Customer_ID)
                        Store_INDV_Identification.DataSource = objlist_Identification
                        Store_INDV_Identification.DataBind()

                        '' Employer Phone
                        Store_Empoloyer_Phone.DataSource = GoAMLBLL.goAML_CustomerBLL.GetVWEmployerPhones(obj_Customer.PK_Customer_ID).ToList()
                        Store_Empoloyer_Phone.DataBind()

                        '' Employer Address
                        Store_Employer_Address.DataSource = GoAMLBLL.goAML_CustomerBLL.GetVWEmployerAddresses(obj_Customer.PK_Customer_ID).ToList()
                        Store_Employer_Address.DataBind()

                        ''Previous Name
                        Store_Customer_Previous_Name.DataSource = GoAMLBLL.goAML_CustomerBLL.GetPreviousName(obj_Customer.CIF).ToList()
                        Store_Customer_Previous_Name.DataBind()

                        ''Email
                        Store_Customer_Email.DataSource = GoAMLBLL.goAML_CustomerBLL.GetEmail(obj_Customer.CIF).ToList()
                        objListgoAML_Ref_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(obj_Customer.CIF).ToList()
                        objListgoAML_vw_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(obj_Customer.CIF).ToList()
                        Store_Customer_Email.DataBind()

                        ''Employment History
                        Store_Customer_Employment_History.DataSource = GoAMLBLL.goAML_CustomerBLL.GetEmploymentHistory(obj_Customer.CIF).ToList()
                        Store_Customer_Employment_History.DataBind()

                        ''Person PEP
                        Store_Customer_Person_PEP.DataSource = GoAMLBLL.goAML_CustomerBLL.GetPEP(obj_Customer.CIF).ToList()
                        objListgoAML_Ref_PersonPEP = GoAMLBLL.goAML_CustomerBLL.GetPEP(obj_Customer.CIF).ToList()
                        objListgoAML_vw_PersonPEP = GoAMLBLL.goAML_CustomerBLL.GetPEP(obj_Customer.CIF).ToList()
                        Store_Customer_Person_PEP.DataBind()

                        ''Network Device
                        Store_Customer_Network_Device.DataSource = GoAMLBLL.goAML_CustomerBLL.GetNetworkDevice(obj_Customer.CIF).ToList()
                        Store_Customer_Network_Device.DataBind()

                        ''Social Media
                        Store_Customer_Social_Media.DataSource = GoAMLBLL.goAML_CustomerBLL.GetSocialMedia(obj_Customer.CIF).ToList()
                        Store_Customer_Social_Media.DataBind()

                        ''Sanction
                        Store_Customer_Sanction.DataSource = GoAMLBLL.goAML_CustomerBLL.GetSanction(obj_Customer.CIF).ToList()
                        objListgoAML_Ref_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(obj_Customer.CIF).ToList()
                        objListgoAML_vw_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(obj_Customer.CIF).ToList()
                        Store_Customer_Sanction.DataBind()

                        ''Related Person
                        Store_Customer_RelatedPerson.DataSource = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(obj_Customer.CIF).ToList()
                        objListgoAML_Ref_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(obj_Customer.CIF).ToList()
                        objListgoAML_vw_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(obj_Customer.CIF).ToList()
                        Store_Customer_RelatedPerson.DataBind()

                        ''Additonal Information
                        Store_Customer_Additional_Info.DataSource = GoAMLBLL.goAML_CustomerBLL.GetAdditionalInformation(obj_Customer.CIF).ToList()
                        Store_Customer_Additional_Info.DataBind()

                        panel_Corp.Visible = False
                        GP_INDVD_Address.Hidden = False
                        'panel_INDV.Visible = False
                    ElseIf obj_Customer.FK_Customer_Type_ID = 2 Then
                        txt_Corp_Name.Text = obj_Customer.Corp_Name
                        txt_Corp_Commercial_Name.Text = obj_Customer.Corp_Commercial_Name


                        CORP_Incorporation_legal_form = goAML_CustomerBLL.GetBentukBadanUsahaByID(obj_Customer.Corp_Incorporation_Legal_Form)
                        If CORP_Incorporation_legal_form IsNot Nothing Then
                            txt_Corp_Incorporation_Legal_Form.Text = CORP_Incorporation_legal_form.Keterangan
                        Else
                            txt_Corp_Incorporation_Legal_Form.Text = ""
                        End If


                        txt_Corp_Incorporation_number.Text = obj_Customer.Corp_Incorporation_Number
                        txt_Corp_Business.Text = obj_Customer.Corp_Business
                        txt_Corp_Email.Text = obj_Customer.Corp_Email
                        txt_Corp_url.Text = obj_Customer.Corp_Url
                        txt_Corp_incorporation_state.Text = obj_Customer.Corp_Incorporation_State

                        'Dedy Remarks 27082020 is nothing
                        'CORP_incorporation_country_code = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.Corp_Incorporation_Country_Code)
                        'txt_Corp_Incorporation_Country_Code.Text = CORP_incorporation_country_code.Keterangan
                        'Dedy End Remarks 27082020

                        'Dedy Added 27082020 for is nothing
                        CORP_incorporation_country_code = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.Corp_Incorporation_Country_Code)
                        If (CORP_incorporation_country_code IsNot Nothing) Then
                            txt_Corp_Incorporation_Country_Code.Text = CORP_incorporation_country_code.Keterangan
                        End If
                        'Dedy End Added 27082020 for is nothing


                        'If Not obj_Customer.Corp_Role Is Nothing And Not obj_Customer.Corp_Role = "" Then
                        '    CORP_Role = goAML_CustomerBLL.GetPartyRolebyID(obj_Customer.Corp_Role)
                        '    txt_Corp_Role.Text = CORP_Role.Keterangan
                        'Else
                        '    txt_Corp_Role.Text = ""
                        'End If

                        If obj_Customer.Corp_Incorporation_Date.HasValue Then
                            txt_Corp_incorporation_date.Text = obj_Customer.Corp_Incorporation_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            txt_Corp_incorporation_date.Text = ""
                        End If
                        If Not obj_Customer.Corp_Business_Closed = "False" Then
                            cbTutup.Text = "Ya"
                        Else
                            cbTutup.Text = "Tidak"
                        End If
                        If obj_Customer.Corp_Date_Business_Closed.HasValue Then
                            txt_Corp_date_business_closed.Text = obj_Customer.Corp_Date_Business_Closed.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            txt_Corp_date_business_closed.Text = ""
                            txt_Corp_date_business_closed.Hidden = True
                        End If
                        txt_Corp_tax_number.Text = obj_Customer.Corp_Tax_Number
                        txt_Corp_Comments.Text = obj_Customer.Corp_Comments

                        GP_Phone_Corp.Hidden = False
                        GP_Address_Corp.Hidden = False

                        '' Phone
                        objListgoAML_Ref_Phone = goAML_CustomerBLL.GetVWCustomerPhones(obj_Customer.PK_Customer_ID)
                        Store_Phone_Corp.DataSource = objListgoAML_Ref_Phone
                        Store_Phone_Corp.DataBind()

                        '' Address
                        Dim obj_list_address_Corp = goAML_CustomerBLL.GetVWCustomerAddresses(obj_Customer.PK_Customer_ID)
                        Store_Address_Corp.DataSource = obj_list_address_Corp
                        Store_Address_Corp.DataBind()

                        gp_Director.Hidden = False
                        ''director
                        store_Director.DataSource = goAML_CustomerBLL.GetVWCustomerDirector(obj_Customer.CIF)
                        store_Director.DataBind()

                        For Each item As Vw_Customer_Director In goAML_CustomerBLL.GetVWCustomerDirector(obj_Customer.CIF)
                            Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                            For Each itemx As Vw_Director_Addresses In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorAddresses(pk_director)
                                objListgoAML_Ref_Director_Address.Add(itemx)
                            Next
                        Next

                        For Each item As Vw_Customer_Director In goAML_CustomerBLL.GetVWCustomerDirector(obj_Customer.CIF)
                            Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                            For Each itemx As Vw_Director_Phones In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorPhones(pk_director)
                                objListgoAML_Ref_Director_Phone.Add(itemx)
                            Next
                        Next

                        For Each item As Vw_Customer_Director In goAML_CustomerBLL.GetVWCustomerDirector(obj_Customer.CIF)
                            Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                            For Each itemx As Vw_Director_Employer_Addresses In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorEmployerAddresses(pk_director)
                                objListgoAML_Ref_Director_Employer_Address.Add(itemx)
                            Next
                        Next

                        For Each item As Vw_Customer_Director In goAML_CustomerBLL.GetVWCustomerDirector(obj_Customer.CIF)
                            Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                            For Each itemx As Vw_Director_Employer_Phones In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorEmployerPhones(pk_director)
                                objListgoAML_Ref_Director_Employer_Phone.Add(itemx)
                            Next
                        Next

                        For Each item As Vw_Customer_Director In goAML_CustomerBLL.GetVWCustomerDirector(obj_Customer.CIF)
                            Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                            For Each itemx As Vw_Person_Identification In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorIdentifications(pk_director)
                                objListgoAML_vw_Customer_Identification_Director.Add(itemx)
                            Next
                        Next

                        panel_INDV.Visible = False

                        ''Add By Septian, goAML v5.0.1, 24 January 2023
                        ''Corp Email
                        Store_corp_email.DataSource = GoAMLBLL.goAML_CustomerBLL.GetEmail(obj_Customer.CIF).ToList()
                        objListgoAML_Ref_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(obj_Customer.CIF).ToList()
                        objListgoAML_vw_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(obj_Customer.CIF).ToList()
                        Store_corp_email.DataBind()

                        ''Corp URL
                        Store_Customer_Entity_URL.DataSource = GoAMLBLL.goAML_CustomerBLL.GetURL(obj_Customer.CIF).ToList()
                        objListgoAML_Ref_EntityUrl = GoAMLBLL.goAML_CustomerBLL.GetURL(obj_Customer.CIF).ToList()
                        objListgoAML_vw_EntityUrl = GoAMLBLL.goAML_CustomerBLL.GetURL(obj_Customer.CIF).ToList()
                        Store_Customer_Entity_URL.DataBind()

                        ''Corp Entity Identification
                        Store_Customer_Entity_Identification.DataSource = GoAMLBLL.goAML_CustomerBLL.GetEntityIdentification(obj_Customer.CIF).ToList()
                        objListgoAML_Ref_EntityIdentification = GoAMLBLL.goAML_CustomerBLL.GetEntityIdentification(obj_Customer.CIF).ToList()
                        objListgoAML_vw_EntityIdentification = GoAMLBLL.goAML_CustomerBLL.GetEntityIdentification(obj_Customer.CIF).ToList()
                        Store_Customer_Entity_Identification.DataBind()

                        ''Corp Network Device
                        StoreDirector_Network_Device.DataSource = GoAMLBLL.goAML_CustomerBLL.GetNetworkDevice(obj_Customer.CIF).ToList()
                        StoreDirector_Network_Device.DataBind()

                        ''Corp Sanction
                        Store_corp_sanction.DataSource = GoAMLBLL.goAML_CustomerBLL.GetSanction(obj_Customer.CIF).ToList()
                        objListgoAML_Ref_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(obj_Customer.CIF).ToList()
                        objListgoAML_vw_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(obj_Customer.CIF).ToList()
                        Store_corp_sanction.DataBind()

                        ''Corp Related Person
                        StoreCorp_Related_person.DataSource = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(obj_Customer.CIF).ToList()
                        objListgoAML_Ref_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(obj_Customer.CIF).ToList()
                        objListgoAML_vw_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(obj_Customer.CIF).ToList()
                        StoreCorp_Related_person.DataBind()

                        ''Corp Related Entities
                        Store_Related_Entities.DataSource = GoAMLBLL.goAML_CustomerBLL.GetRelatedEntities(obj_Customer.CIF).ToList()
                        objListgoAML_Ref_RelatedEntities = GoAMLBLL.goAML_CustomerBLL.GetRelatedEntities(obj_Customer.CIF).ToList()
                        objListgoAML_vw_RelatedEntities = GoAMLBLL.goAML_CustomerBLL.GetRelatedEntities(obj_Customer.CIF).ToList()
                        Store_Related_Entities.DataBind()

                        ''Corp Additonal Information
                        Store_Customer_Corp_Additional_Info.DataSource = GoAMLBLL.goAML_CustomerBLL.GetAdditionalInformation(obj_Customer.CIF).ToList()
                        Store_Customer_Corp_Additional_Info.DataBind()

                        ''end add by septian
                    End If
                End If

                Dim objCustomer2 = goAML_CustomerBLL.GetCustomerbyCIF2(strid)
                Dim qNamaNegara = "SELECT TOP 1 * FROM goAML_Ref_Nama_Negara WHERE Kode = '" + objCustomer2.Country_Of_Birth + "'"
                Dim qEntityStatus = "SELECT TOP 1 * FROM goAML_Ref_Entity_Status WHERE Kode = '" + objCustomer2.Entity_Status + "'"
                Dim refNamaNegara As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, qNamaNegara)
                Dim refEntityStatus As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, qEntityStatus)

                'txt_INDV_Foreign_Full_Name.Text = objCustomer2.Full_Name_Frn
                If refNamaNegara IsNot Nothing Then
                    Dim refNamaNegara_Kode = refNamaNegara("Kode").ToString()
                    Dim refNamaNegara_Keterangan = refNamaNegara("Keterangan").ToString()
                    'cmb_INDV_Country_Of_Birth.SetTextWithTextValue(refNamaNegara_Kode, refNamaNegara_Keterangan)
                    'txt_INDV_Residence_Since.Text = objCustomer2.Residence_Since.Value.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    cb_INDV_Is_Protected.Checked = objCustomer2.Is_Protected
                End If

                If refEntityStatus IsNot Nothing Then
                    Dim refEntityStatus_Kode = refEntityStatus("Kode").ToString()
                    Dim refEntityStatus_Keterangan = refEntityStatus("Keterangan").ToString()
                    cmb_Corp_Entity_Status.SetTextWithTextValue(refEntityStatus_Kode, refEntityStatus_Keterangan)
                    txt_Corp_Entity_Status_Date.Text = objCustomer2.Entity_Status_Date.Value.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                End If

            Catch ex As Exception
                Throw ex
            End Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub GridPanelSetting()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 2 Then
            GP_DirectorIdentification.ColumnModel.Columns.RemoveAt(GP_DirectorIdentification.ColumnModel.Columns.Count - 1)
            GP_DirectorIdentification.ColumnModel.Columns.Insert(1, CommandColumn13)
            GP_Director_Phone.ColumnModel.Columns.RemoveAt(GP_Director_Phone.ColumnModel.Columns.Count - 1)
            GP_Director_Phone.ColumnModel.Columns.Insert(1, CommandColumn6)
            GP_Director_Address.ColumnModel.Columns.RemoveAt(GP_Director_Address.ColumnModel.Columns.Count - 1)
            GP_Director_Address.ColumnModel.Columns.Insert(1, CommandColumn56)
            GP_Director_Employer_Phone.ColumnModel.Columns.RemoveAt(GP_Director_Employer_Phone.ColumnModel.Columns.Count - 1)
            GP_Director_Employer_Phone.ColumnModel.Columns.Insert(1, CommandColumn11)
            GP_Director_Employer_Address.ColumnModel.Columns.RemoveAt(GP_Director_Employer_Address.ColumnModel.Columns.Count - 1)
            GP_Director_Employer_Address.ColumnModel.Columns.Insert(1, CommandColumn10)
            gp_INDV_Phones.ColumnModel.Columns.RemoveAt(gp_INDV_Phones.ColumnModel.Columns.Count - 1)
            gp_INDV_Phones.ColumnModel.Columns.Insert(1, CommandColumn1)
            GP_INDVD_Address.ColumnModel.Columns.RemoveAt(GP_INDVD_Address.ColumnModel.Columns.Count - 1)
            GP_INDVD_Address.ColumnModel.Columns.Insert(1, CommandColumn2)
            GP_INDVD_Identification.ColumnModel.Columns.RemoveAt(GP_INDVD_Identification.ColumnModel.Columns.Count - 1)
            GP_INDVD_Identification.ColumnModel.Columns.Insert(1, CommandColumn7)
            GP_Empoloyer_Phone.ColumnModel.Columns.RemoveAt(GP_Empoloyer_Phone.ColumnModel.Columns.Count - 1)
            GP_Empoloyer_Phone.ColumnModel.Columns.Insert(1, CommandColumn8)
            GP_Employer_Address.ColumnModel.Columns.RemoveAt(GP_Employer_Address.ColumnModel.Columns.Count - 1)
            GP_Employer_Address.ColumnModel.Columns.Insert(1, CommandColumn9)
            GP_Phone_Corp.ColumnModel.Columns.RemoveAt(GP_Phone_Corp.ColumnModel.Columns.Count - 1)
            GP_Phone_Corp.ColumnModel.Columns.Insert(1, CommandColumn3)
            GP_Address_Corp.ColumnModel.Columns.RemoveAt(GP_Address_Corp.ColumnModel.Columns.Count - 1)
            GP_Address_Corp.ColumnModel.Columns.Insert(1, CommandColumn4)
            gp_Director.ColumnModel.Columns.RemoveAt(gp_Director.ColumnModel.Columns.Count - 1)
            gp_Director.ColumnModel.Columns.Insert(1, cmdDirectory)



        End If

    End Sub

    Sub Maximizeable()
        WindowDetailDirector.Maximizable = True
        WindowDetail.Maximizable = True
        WindowDetailAddress.Maximizable = True
        WindowDetailIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True

        'START: 2023-10-14, Nael
        WindowDetail_RelatedEntities.Maximizable = True
        WindowDetail_EntityIdentification.Maximizable = True
        WindowDetail_RelatedPerson.Maximizable = True
        WindowDetail_Person_PEP.Maximizable = True
        WindowDetail_Sanction.Maximizable = True
        WindowDetail_Email.Maximizable = True
        'END: 2023-10-14, Nael

    End Sub


    Protected Sub GrdCmdCustPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Sub LoadDataDetail(id As Long)
        Dim objCustomerPhonesDetail As Vw_Customer_Phones = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            cb_phone_Contact_Type.ReadOnly = True
            cb_phone_Communication_Type.ReadOnly = True
            txt_phone_Country_prefix.ReadOnly = True
            txt_phone_number.ReadOnly = True
            txt_phone_extension.ReadOnly = True
            txt_phone_comments.ReadOnly = True
            'btnSavedetail.Hidden = True
            WindowDetail.Title = "Phone Detail"
            ClearinputCustomerPhones()
            With objCustomerPhonesDetail
                Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                Store_Contact_Type.DataBind()
                Phone_Contact_Type = goAML_CustomerBLL.GetListKategoriKontak.Where(Function(x) x.Keterangan = objCustomerPhonesDetail.Contact_Type).FirstOrDefault
                cb_phone_Contact_Type.SetValue(Phone_Contact_Type.Kode)
                Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                Store_Communication_Type.DataBind()
                Phone_Communication_Type = goAML_CustomerBLL.GetCommunicationTypeByID(objCustomerPhonesDetail.Tph_Communication_Type)
                cb_phone_Communication_Type.SetValue(Phone_Communication_Type.Kode)
                txt_phone_Country_prefix.Text = .tph_country_prefix
                txt_phone_number.Text = .tph_number
                txt_phone_extension.Text = .tph_extension
                txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Protected Sub GrdCmdCustAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataAddressDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataAddressDetail(id As Long)

        Dim objCustomerAddressDetail = goAML_CustomerBLL.GetVWCustomerAddressesbyPK(id)

        If Not objCustomerAddressDetail Is Nothing Then
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            cmb_kategoriaddress.ReadOnly = True
            Address_INDV.ReadOnly = True
            Town_INDV.ReadOnly = True
            City_INDV.ReadOnly = True
            Zip_INDV.ReadOnly = True
            cmb_kodenegara.ReadOnly = True
            State_INDVD.ReadOnly = True
            Comment_Address_INDVD.ReadOnly = True
            'btnSavedetail.Hidden = True
            WindowDetailAddress.Title = "Address Detail"
            ClearinputCustomerAddress()
            Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            Store_KategoriAddress.DataBind()
            Dim kategoriaddress = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
            If kategoriaddress IsNot Nothing Then
                cmb_kategoriaddress.SetValue(kategoriaddress.Kode)
            End If

            Address_INDV.Text = objCustomerAddressDetail.Address
            Town_INDV.Text = objCustomerAddressDetail.Town
            City_INDV.Text = objCustomerAddressDetail.City
            Zip_INDV.Text = objCustomerAddressDetail.Zip
            Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            Store_Kode_Negara.DataBind()
            Dim namanegara = goAML_CustomerBLL.GetNamaNegaraByKeterangan(objCustomerAddressDetail.Country)
            If namanegara IsNot Nothing Then
                cmb_kodenegara.SetValue(namanegara.Kode)
            End If

            State_INDVD.Text = objCustomerAddressDetail.State
            Comment_Address_INDVD.Text = objCustomerAddressDetail.Comments
        End If
    End Sub

    Sub LoadDataIdentificationDetail(id As Long)

        Dim objCustomerIdentificationDetail = goAML_CustomerBLL.GetVWCustomerIdentificationsbyPKPersonIdentification(id)

        If Not objCustomerIdentificationDetail Is Nothing Then
            FP_Identification_INDV.Hidden = False
            WindowDetailIdentification.Hidden = False
            WindowDetailIdentification.Title = "Identification Detail"
            txt_number_identification.ReadOnly = True
            cmb_Jenis_Dokumen.ReadOnly = True
            cmb_issued_country.ReadOnly = True
            txt_issued_by.ReadOnly = True
            txt_issued_date.ReadOnly = True
            txt_issued_expired_date.ReadOnly = True
            txt_identification_comment.ReadOnly = True
            ClearinputCustomerIdentification()
            txt_number_identification.Text = objCustomerIdentificationDetail.Number
            Store_jenis_dokumen.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            Store_jenis_dokumen.DataBind()
            Dim jenisdokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(x) x.Keterangan = objCustomerIdentificationDetail.Type_Document).FirstOrDefault()
            If jenisdokumen IsNot Nothing Then
                cmb_Jenis_Dokumen.SetValue(jenisdokumen.Kode)
            End If

            Store_issued_country.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            Store_issued_country.DataBind()
            Dim issuedcountry = goAML_CustomerBLL.GetNamaNegaraByKeterangan(objCustomerIdentificationDetail.Country)
            If issuedcountry IsNot Nothing Then
                cmb_issued_country.SetValue(issuedcountry.Kode)
            End If

            txt_issued_by.Text = objCustomerIdentificationDetail.Issued_By
            txt_issued_date.Value = IIf(objCustomerIdentificationDetail.Issue_Date.HasValue, objCustomerIdentificationDetail.Issue_Date, Nothing) ' 2023-11-22, Nael
            txt_issued_expired_date.Value = IIf(objCustomerIdentificationDetail.Expiry_Date.HasValue, objCustomerIdentificationDetail.Expiry_Date, Nothing) ' 2023-11-22, Nael
            txt_identification_comment.Text = objCustomerIdentificationDetail.Identification_Comment
        End If
    End Sub

    Private Sub CUSTOMER_DETAIL_v501_Default_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    Protected Sub GrdCmdDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailDirector(id As Long)
        Try
            Dim dateFormat As String = "dd-MM-yyyy"

            Dim objSysParamDateFormat As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(6)

            If objSysParamDateFormat IsNot Nothing Then
                dateFormat = objSysParamDateFormat.SettingValue
            End If

            FormPanelDetailDirector.Hidden = False
            cb_Director_Deceased.ReadOnly = True
            txt_Director_Deceased_Date.ReadOnly = True
            txt_Director_Tax_Number.ReadOnly = True
            cb_Director_Tax_Reg_Number.ReadOnly = True
            txt_Director_Source_of_Wealth.ReadOnly = True
            txt_Director_Comments.ReadOnly = True
            Dim Director_Detail = goAML_CustomerBLL.GetCustomerDirectorByPKDirector(id)

            Name.Text = Director_Detail.Last_Name
            Dim role_detail = goAML_CustomerBLL.GetRoleDirectorByID(Director_Detail.Role)
            If role_detail IsNot Nothing Then
                Role.Text = role_detail.Keterangan
            Else
                Role.Text = ""
            End If
            BirthPlace.Text = Director_Detail.Birth_Place
            BirthDate.Text = IIf(Director_Detail.BirthDate Is Nothing, "", Convert.ToDateTime(Director_Detail.BirthDate).ToString(dateFormat))
            MomName.Text = Director_Detail.Mothers_Name
            Aliase.Text = Director_Detail.Alias
            SSN.Text = Director_Detail.SSN
            PassportNo.Text = Director_Detail.Passport_Number
            PassportFrom.Text = Director_Detail.Passport_Country
            No_Identity.Text = Director_Detail.ID_Number
            txt_Director_employer_name.Text = Director_Detail.Employer_Name
            If Director_Detail.Nationality1 IsNot Nothing And Director_Detail.Nationality1 IsNot "" Then
                Nasionality1.Text = goAML_CustomerBLL.GetNamaNegaraByID(Director_Detail.Nationality1).Keterangan
            End If
            If Not Director_Detail.Nationality2 = "" And Director_Detail.Nationality2 IsNot Nothing Then
                Nasionality2.Text = goAML_CustomerBLL.GetNamaNegaraByID(Director_Detail.Nationality2).Keterangan
            End If
            If Not Director_Detail.Nationality3 = "" And Director_Detail.Nationality3 IsNot Nothing Then
                Nasionality3.Text = goAML_CustomerBLL.GetNamaNegaraByID(Director_Detail.Nationality3).Keterangan
            End If
            If Not Director_Detail.Residence = "" And Director_Detail.Residence IsNot Nothing Then
                Residence.Text = goAML_CustomerBLL.GetNamaNegaraByID(Director_Detail.Residence).Keterangan
            End If
            Email.Text = Director_Detail.Email
            Pekerjaan.Text = Director_Detail.Occupation

            If Director_Detail.Deceased = True Then
                cb_Director_Deceased.Checked = True
                txt_Director_Deceased_Date.Value = Convert.ToDateTime(Director_Detail.Deceased_Date).ToString(dateFormat)
            End If
            txt_Director_Tax_Number.Text = Director_Detail.Tax_Number
            If Director_Detail.Tax_Reg_Number = True Then
                cb_Director_Tax_Reg_Number.Checked = True
            End If
            txt_Director_Source_of_Wealth.Text = Director_Detail.Source_of_Wealth
            txt_Director_Comments.Text = Director_Detail.Comments


            StoreDirectorPhone.DataSource = objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
            StoreDirectorPhone.DataBind()

            StoreDirectorAddress.DataSource = objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
            StoreDirectorAddress.DataBind()

            StoreDirectorEmployerPhone.DataSource = objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
            StoreDirectorEmployerPhone.DataBind()

            StoreDirectorEmployerAddress.DataSource = objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
            StoreDirectorEmployerAddress.DataBind()

            Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
            Store_Director_Identification.DataBind()

            Dim email_list = goAML_Customer_Service.GetEmailByPkFk(id, 6)
            Dim sanction_list = goAML_Customer_Service.GetSanctionByPkFk(id, 6)
            Dim pep_list = goAML_Customer_Service.GetPEPByPkFk(id, 6)

            GridEmail_Director.LoadData(email_list)
            GridSanction_Director.LoadData(sanction_list)
            GridPEP_Director.LoadData(pep_list)

            'Dim objList

            WindowDetailDirector.Hidden = False
            WindowDetailDirector.Title = "Director Detail"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdAddressDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddressDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorIdentificationDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub checkBoxcb_Director_Deceased_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_Director_Deceased.DirectCheck
        Try
            If cb_Director_Deceased.Checked Then
                txt_Director_Deceased_Date.Hidden = False
            Else
                txt_Director_Deceased_Date.Hidden = True
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ClearinputDirectorIdentification()
        cmb_Jenis_Dokumen_Director.Value = Nothing
        txt_identification_comment_Director.Text = ""
        txt_issued_by_Director.Text = ""
        txt_issued_date_Director.Text = ""
        txt_issued_expired_date_Director.Text = ""
        cmb_issued_country_Director.Value = Nothing
        txt_number_identification_Director.Text = ""
    End Sub

    Sub LoadDataDirectorIdentificationDetail(id As Long)

        Dim objCustomerIdentificationDetail = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.PK_Person_Identification_ID = id).FirstOrDefault()

        If Not objCustomerIdentificationDetail Is Nothing Then
            FP_Identification_Director.Hidden = False
            WindowDetailIdentification.Hidden = False
            WindowDetailIdentification.Title = "Identification Detail"
            txt_number_identification_Director.ReadOnly = True
            cmb_Jenis_Dokumen_Director.ReadOnly = True
            cmb_issued_country_Director.ReadOnly = True
            txt_issued_by_Director.ReadOnly = True
            txt_issued_date_Director.ReadOnly = True
            txt_issued_expired_date_Director.ReadOnly = True
            txt_identification_comment_Director.ReadOnly = True
            ClearinputDirectorIdentification()
            txt_number_identification_Director.Text = objCustomerIdentificationDetail.Number
            Storejenisdokumen_Director.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            Storejenisdokumen_Director.DataBind()
            Dim jenisdokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(X) X.Keterangan = objCustomerIdentificationDetail.Type_Document).FirstOrDefault
            If jenisdokumen IsNot Nothing Then
                cmb_Jenis_Dokumen_Director.SetValue(jenisdokumen.Kode)
            End If

            StoreIssueCountry_Director.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            StoreIssueCountry_Director.DataBind()
            Dim namanegara = goAML_CustomerBLL.GetNamaNegaraByKeterangan(objCustomerIdentificationDetail.Country)
            If namanegara IsNot Nothing Then
                cmb_issued_country_Director.SetValue(namanegara.Kode)
            End If
            txt_issued_by_Director.Text = objCustomerIdentificationDetail.Issued_By
            txt_issued_date_Director.Text = IIf(objCustomerIdentificationDetail.Issue_Date.HasValue, objCustomerIdentificationDetail.Issue_Date, Nothing) ' 2023-11-22, Nael
            txt_issued_expired_date_Director.Text = IIf(objCustomerIdentificationDetail.Expiry_Date.HasValue, objCustomerIdentificationDetail.Expiry_Date, Nothing) ' 2023-11-22, Nael
            txt_identification_comment_Director.Text = objCustomerIdentificationDetail.Identification_Comment
        End If
    End Sub


    Sub LoadDataDetailAddressDirector(id As Long)
        Try
            Dim objCustomerAddressDetail = goAML_CustomerBLL.GetAddressByID(id)
            If Not objCustomerAddressDetail Is Nothing Then
                FP_Address_Director.Hidden = False
                WindowDetailDirectorAddress.Hidden = False
                WindowDetailDirectorAddress.Title = "Address Detail"
                Director_cmb_kategoriaddress.ReadOnly = True
                Director_Address.ReadOnly = True
                Director_Town.ReadOnly = True
                Director_City.ReadOnly = True
                Director_Zip.ReadOnly = True
                Director_cmb_kodenegara.ReadOnly = True
                Director_State.ReadOnly = True
                Director_Comment_Address.ReadOnly = True
                Director_Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                Director_Store_KategoriAddress.DataBind()
                Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
                If kategorikontak IsNot Nothing Then
                    Director_cmb_kategoriaddress.SetValue(kategorikontak.Kode)
                End If
                Director_Address.Text = objCustomerAddressDetail.Address
                Director_Town.Text = objCustomerAddressDetail.Town
                Director_City.Text = objCustomerAddressDetail.City
                Director_Zip.Text = objCustomerAddressDetail.Zip
                Director_Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                Director_Store_Kode_Negara.DataBind()
                Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerAddressDetail.Country_Code)
                If kodenegara IsNot Nothing Then
                    Director_cmb_kodenegara.SetValue(kodenegara.Kode)
                End If

                Director_State.Text = objCustomerAddressDetail.State
                Director_Comment_Address.Text = objCustomerAddressDetail.Comments
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdPhoneDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhoneDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailPhoneDirector(id As Long)
        Try
            Dim objCustomerPhonesDetail As goAML_Ref_Phone = goAML_CustomerBLL.GetPhonesByID(id)


            Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

            If Not objCustomerPhonesDetail Is Nothing Then
                FormPanelDirectorPhone.Hidden = False
                WindowDetailDirectorPhone.Hidden = False
                Director_cb_phone_Contact_Type.ReadOnly = True
                Director_cb_phone_Communication_Type.ReadOnly = True
                Director_txt_phone_Country_prefix.ReadOnly = True
                Director_txt_phone_number.ReadOnly = True
                Director_txt_phone_extension.ReadOnly = True
                Director_txt_phone_comments.ReadOnly = True

                WindowDetailDirectorPhone.Title = "Phone Detail"
                With objCustomerPhonesDetail
                    Director_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                    Director_Store_Contact_Type.DataBind()
                    Director_cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)

                    Director_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                    Director_Store_Communication_Type.DataBind()
                    Director_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                    Director_txt_phone_Country_prefix.Text = .tph_country_prefix
                    Director_txt_phone_number.Text = .tph_number
                    Director_txt_phone_extension.Text = .tph_extension
                    Director_txt_phone_comments.Text = .comments
                End With
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btnBackSaveDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelDetailDirector.Hidden = True
            WindowDetailDirector.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelDirectorPhone.Hidden = True
            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_Director.Hidden = True
            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataIdentificationDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ClearinputCustomerIdentification()
        cmb_Jenis_Dokumen.Value = Nothing
        txt_identification_comment.Text = ""
        txt_issued_by.Text = ""
        txt_issued_date.Text = ""
        txt_issued_expired_date.Text = ""
        cmb_issued_country.Value = Nothing
        txt_number_identification.Text = ""
    End Sub

    Sub ClearinputCustomerAddress()
        cmb_kategoriaddress.Value = Nothing
        cmb_kodenegara.Value = Nothing
        Address_INDV.Text = ""
        Town_INDV.Text = ""
        City_INDV.Text = ""
        Zip_INDV.Text = ""
        State_INDVD.Text = ""
        Comment_Address_INDVD.Text = ""
    End Sub

    Protected Sub btnBackSaveCustomerAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_INDVD.Hidden = True
            WindowDetailAddress.Hidden = True
            ClearinputCustomerAddress()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Identification_INDV.Hidden = True
            WindowDetailIdentification.Hidden = True
            ClearinputCustomerIdentification()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailEmployerPhone(id As Long)

        Dim objCustomerPhonesDetail As goAML_Ref_Phone = goAML_CustomerBLL.GetPhonesByID(id)

        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            cb_phone_Contact_Type.ReadOnly = True
            cb_phone_Communication_Type.ReadOnly = True
            txt_phone_Country_prefix.ReadOnly = True
            txt_phone_number.ReadOnly = True
            txt_phone_extension.ReadOnly = True
            txt_phone_comments.ReadOnly = True


            WindowDetail.Title = "Phone Detail"
            ClearinputCustomerPhones()
            With objCustomerPhonesDetail
                Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                Store_Contact_Type.DataBind()
                'Phone_Contact_Type = db.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Keterangan = objCustomerPhonesDetail.Contact_Type).FirstOrDefault
                'cb_phone_Contact_Type.SetValue(Phone_Contact_Type.Kode)
                cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)

                Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                Store_Communication_Type.DataBind()
                'Phone_Communication_Type = db.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Keterangan = objCustomerPhonesDetail.Communcation_Type).FirstOrDefault
                'cb_phone_Communication_Type.SetValue(Phone_Communication_Type.Kode)
                cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                txt_phone_Country_prefix.Text = .tph_country_prefix
                txt_phone_number.Text = .tph_number
                txt_phone_extension.Text = .tph_extension
                txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Protected Sub GrdCmdEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailEmployerPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()
            objTempCustomerPhoneEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Identification_Director.Hidden = True
            WindowDetailIdentification.Hidden = True
            ClearinputDirectorIdentification()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataEmployerAddressDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataEmployerAddressDetail(id As Long)

        Dim objCustomerAddressDetail = goAML_CustomerBLL.GetAddressByID(id)
        If Not objCustomerAddressDetail Is Nothing Then
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            WindowDetailAddress.Title = "Address Detail"
            cmb_kategoriaddress.ReadOnly = True
            Address_INDV.ReadOnly = True
            Town_INDV.ReadOnly = True
            City_INDV.ReadOnly = True
            Zip_INDV.ReadOnly = True
            cmb_kodenegara.ReadOnly = True
            State_INDVD.ReadOnly = True
            Comment_Address_INDVD.ReadOnly = True
            ClearinputCustomerAddress()
            Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            Store_KategoriAddress.DataBind()
            Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
            If kategorikontak IsNot Nothing Then
                cmb_kategoriaddress.SetValue(kategorikontak.Kode)
            End If

            Address_INDV.Text = objCustomerAddressDetail.Address
            Town_INDV.Text = objCustomerAddressDetail.Town
            City_INDV.Text = objCustomerAddressDetail.City
            Zip_INDV.Text = objCustomerAddressDetail.Zip
            Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            Store_Kode_Negara.DataBind()
            Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerAddressDetail.Country_Code)
            If kodenegara IsNot Nothing Then
                cmb_kodenegara.SetValue(kodenegara.Kode)
            End If

            State_INDVD.Text = objCustomerAddressDetail.State
            Comment_Address_INDVD.Text = objCustomerAddressDetail.Comments
        End If
    End Sub

    Protected Sub btnBackCustomer_PreviousName_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailPreviousName.Hidden = True
            FormPanelPreviousName.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_Email_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Email.Hidden = True
            FormPanel_Email.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_EmploymentHistory_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Employment_History.Hidden = True
            FormPanel_Employment_History.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_PersonPEP_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Person_PEP.Hidden = True
            FormPanel_Person_PEP.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_NetworkDevice_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_NetworkDevice.Hidden = True
            FormPanel_NetworkDevice.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_SocialMedia_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_SocialMedia.Hidden = True
            FormPanel_SocialMedia.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_Sanction_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Sanction.Hidden = True
            FormPanel_Sanction.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_RelatedPerson_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_RelatedPerson.Hidden = True
            FormPanel_RelatedPerson.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_AdditionalInfo_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_AdditionalInfo.Hidden = True
            FormPanel_AdditionalInfo.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_Url_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_URL.Hidden = True
            FormPanel_URL.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_EntityIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_EntityIdentification.Hidden = True
            FormPanel_EntityIdentification.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_RelatedEntities_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_RelatedEntities.Hidden = True
            FormPanel_RelatedEntities.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadData_PreviousName(id As Long, isEdit As Boolean)
        Dim objCustomerPreviousName As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name = objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)

        If Not objCustomerPreviousName Is Nothing Then
            FormPanelPreviousName.Hidden = False
            WindowDetailPreviousName.Hidden = False
            txtCustomer_first_name.ReadOnly = Not isEdit
            txtCustomer_last_name.ReadOnly = Not isEdit
            txtCustomer_comments.ReadOnly = Not isEdit
            btn_Customer_SavePreviousName.Hidden = Not isEdit

            WindowDetailPreviousName.Title = "Previous Name " & If(isEdit, "Edit", "Detail")
            'ClearinputCustomerPreviousName()
            With objCustomerPreviousName
                txtCustomer_first_name.Value = .FIRST_NAME
                txtCustomer_last_name.Value = .LAST_NAME
                txtCustomer_comments.Value = .COMMENTS
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadData_Email(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Email = objListgoAML_Ref_Email.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id)
            objTempCustomer_Email_Edit = objListgoAML_vw_Email.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id)

            If Not objTempCustomer_Email_Edit Is Nothing Then
                FormPanel_Email.Hidden = False
                WindowDetail_Email.Hidden = False
                btn_Customer_Save_Email.Hidden = Not isEdit

                txtCustomer_email_address.ReadOnly = Not isEdit

                With objTempCustomer_Email_Edit
                    txtCustomer_email_address.Value = .EMAIL_ADDRESS
                End With

                WindowDetail_Email.Title = "Email " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_EmploymentHistory(id As Long, isEdit As Boolean)
        Try
            Dim objCustomer_EmploymentHistory As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History =
                objListgoAML_Ref_EmploymentHistory.Find(Function(x) x.PK_goAML_Ref_Customer_Employment_History_ID = id)

            If Not objCustomer_EmploymentHistory Is Nothing Then
                FormPanel_Employment_History.Hidden = False
                WindowDetail_Employment_History.Hidden = False
                btn_Customer_Save_Employment_History.Hidden = Not isEdit

                txt_employer_name.ReadOnly = Not isEdit
                txt_employer_business.ReadOnly = Not isEdit
                txt_employer_identifier.ReadOnly = Not isEdit
                df_valid_from.ReadOnly = Not isEdit
                cbx_is_approx_from_date.ReadOnly = Not isEdit
                df_valid_to.ReadOnly = Not isEdit
                cbx_is_approx_to_date.ReadOnly = Not isEdit

                With objCustomer_EmploymentHistory
                    txt_employer_name.Value = .EMPLOYER_NAME
                    txt_employer_business.Value = .EMPLOYER_BUSINESS
                    txt_employer_identifier.Value = .EMPLOYER_IDENTIFIER
                    df_valid_from.Text = .EMPLOYMENT_PERIOD_VALID_FROM
                    cbx_is_approx_from_date.Checked = .EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE
                    df_valid_to.Text = .EMPLOYMENT_PERIOD_VALID_TO
                    cbx_is_approx_to_date.Checked = .EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE
                End With

                WindowDetail_Employment_History.Title = "Employment History " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_PEP(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_PEP = objListgoAML_Ref_PersonPEP.Find(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id)
            objTempCustomer_PersonPEP_Edit = objListgoAML_vw_PersonPEP.Find(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id)

            If Not objTempCustomer_PersonPEP_Edit Is Nothing Then
                FormPanel_Person_PEP.Hidden = False
                WindowDetail_Person_PEP.Hidden = False
                btn_Customer_Save_PEP.Hidden = Not isEdit

                cmb_PersonPEP_Country.IsReadOnly = Not isEdit
                txt_function_name.ReadOnly = Not isEdit
                txt_function_description.ReadOnly = Not isEdit
                df_person_pep_valid_from.ReadOnly = Not isEdit
                cbx_pep_is_approx_from_date.ReadOnly = Not isEdit
                df_pep_valid_to.ReadOnly = Not isEdit
                cbx_pep_is_approx_to_date.ReadOnly = Not isEdit
                txt_pep_comments.ReadOnly = Not isEdit


                With objTempCustomer_PersonPEP_Edit
                    Dim reference = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .PEP_COUNTRY)

                    If reference IsNot Nothing Then
                        cmb_PersonPEP_Country.SetTextWithTextValue(reference.Kode, reference.Keterangan)
                    End If

                    txt_function_name.Value = .FUNCTION_NAME
                    txt_function_description.Value = .FUNCTION_DESCRIPTION
                    'df_person_pep_valid_from.Text = .PEP_DATE_RANGE_VALID_FROM
                    'cbx_pep_is_approx_from_date.Checked = .PEP_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_pep_valid_to.Text = .PEP_DATE_RANGE_VALID_TO
                    'cbx_pep_is_approx_to_date.Checked = .PEP_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_pep_comments.Text = .COMMENTS
                End With

                WindowDetail_Person_PEP.Title = "Person PEP " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_NetworkDevice(id As Long, isEdit As Boolean)
        Try
            Dim objCustomer_NetworkDevice As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device =
                objListgoAML_Ref_NetworkDevice.Find(Function(x) x.PK_goAML_Ref_Customer_Network_Device_ID = id)

            If Not objCustomer_NetworkDevice Is Nothing Then
                FormPanel_NetworkDevice.Hidden = False
                WindowDetail_NetworkDevice.Hidden = False
                btnCustomer_Save_NetworkDevice.Hidden = Not isEdit

                txt_device_number.ReadOnly = Not isEdit
                cmb_network_device_os.IsReadOnly = Not isEdit
                txt_service_provider.ReadOnly = Not isEdit
                txt_ipv6.ReadOnly = Not isEdit
                txt_ipv4.ReadOnly = Not isEdit
                txt_cgn_port.ReadOnly = Not isEdit
                txt_ipv4_ipv6.ReadOnly = Not isEdit
                txt_nat.ReadOnly = Not isEdit
                df_first_seen_date.ReadOnly = Not isEdit
                df_last_seen_date.ReadOnly = Not isEdit
                cbx_using_proxy.ReadOnly = Not isEdit
                txt_city.ReadOnly = Not isEdit
                cmb_network_device_country.IsReadOnly = Not isEdit
                txt_network_device_comments.ReadOnly = Not isEdit


                With objCustomer_NetworkDevice
                    Dim refOs = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Operating_System", .OPERATING_SYSTEM)
                    Dim refCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .COUNTRY)

                    txt_device_number.Value = .DEVICE_NUMBER
                    If refOs IsNot Nothing Then
                        cmb_network_device_os.SetTextWithTextValue(refOs.Kode, refOs.Keterangan)
                    End If

                    txt_service_provider.Value = .SERVICE_PROVIDER
                    txt_ipv6.Value = .IPV6
                    txt_ipv4.Value = .IPV4
                    txt_cgn_port.Value = .CGN_PORT
                    txt_ipv4_ipv6.Value = .IPV4_IPV6
                    txt_nat.Value = .NAT
                    df_first_seen_date.Text = .FIRST_SEEN_DATE
                    df_last_seen_date.Text = .LAST_SEEN_DATE
                    cbx_using_proxy.Checked = .USING_PROXY
                    txt_city.Value = .CITY

                    If refOs IsNot Nothing Then
                        cmb_network_device_country.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                    End If

                    txt_network_device_comments.Value = .COMMENTS

                End With

                WindowDetail_NetworkDevice.Title = "Network Device " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_SocialMedia(id As Long, isEdit As Boolean)
        Try
            Dim objCustomer_SocialMedia As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media =
                objListgoAML_Ref_SocialMedia.Find(Function(x) x.PK_goAML_Ref_Customer_Social_Media_ID = id)

            If Not objCustomer_SocialMedia Is Nothing Then
                FormPanel_SocialMedia.Hidden = False
                WindowDetail_SocialMedia.Hidden = False
                btnSaveCustomer_SocialMedia.Hidden = Not isEdit

                txt_socmed_platform.ReadOnly = Not isEdit
                txt_socmed_username.ReadOnly = Not isEdit
                txt_socmed_comments.ReadOnly = Not isEdit

                With objCustomer_SocialMedia
                    txt_socmed_platform.Value = .PLATFORM
                    txt_socmed_username.Value = .USER_NAME
                    txt_socmed_comments.Value = .COMMENTS
                End With

                WindowDetail_SocialMedia.Title = "SocialMedia " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_Sanction(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Sanction = objListgoAML_Ref_Sanction.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id)
            objTempCustomer_Sanction_Edit = objListgoAML_vw_Sanction.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id)


            If Not objTempCustomer_Sanction_Edit Is Nothing Then
                FormPanel_Sanction.Hidden = False
                WindowDetail_Sanction.Hidden = False
                btnSaveCustomer_Sanction.Hidden = Not isEdit

                txt_sanction_provider.ReadOnly = Not isEdit
                txt_sanction_sanction_list_name.ReadOnly = Not isEdit
                txt_sanction_match_criteria.ReadOnly = Not isEdit
                txt_sanction_link_to_source.ReadOnly = Not isEdit
                txt_sanction_sanction_list_attributes.ReadOnly = Not isEdit
                df_sanction_valid_from.ReadOnly = Not isEdit
                cbx_sanction_is_approx_from_date.ReadOnly = Not isEdit
                df_sanction_valid_to.ReadOnly = Not isEdit
                cbx_sanction_is_approx_to_date.ReadOnly = Not isEdit
                txt_sanction_comments.ReadOnly = Not isEdit


                With objTempCustomer_Sanction_Edit
                    txt_sanction_provider.Value = .PROVIDER
                    txt_sanction_sanction_list_name.Value = .SANCTION_LIST_NAME
                    txt_sanction_match_criteria.Value = .MATCH_CRITERIA
                    txt_sanction_link_to_source.Value = .LINK_TO_SOURCE
                    txt_sanction_sanction_list_attributes.Value = .SANCTION_LIST_ATTRIBUTES
                    'df_sanction_valid_from.Text = .SANCTION_LIST_DATE_RANGE_VALID_FROM
                    'cbx_sanction_is_approx_from_date.Checked = .SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_sanction_valid_to.Text = .SANCTION_LIST_DATE_RANGE_VALID_TO
                    'cbx_sanction_is_approx_to_date.Checked = .SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_sanction_comments.Value = .COMMENTS
                End With

                WindowDetail_Sanction.Title = "Sanction " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_RelatedPerson(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Related_Person = objListgoAML_Ref_RelatedPerson.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id)
            objTempCustomer_RelatedPerson_Edit = objListgoAML_vw_RelatedPerson.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id)

            If Not objTempCustomer_RelatedPerson_Edit Is Nothing Then
                FormPanel_RelatedPerson.Hidden = False
                WindowDetail_RelatedPerson.Hidden = False
                btnSaveCustomer_RelatedPerson.Hidden = Not isEdit

                cmb_rp_person_relation.IsReadOnly = Not isEdit
                'df_relation_date_range_valid_from.ReadOnly = Not isEdit
                'cbx_relation_date_range_is_approx_from_date.ReadOnly = Not isEdit
                'df_relation_date_range_valid_to.ReadOnly = Not isEdit
                'cbx_relation_date_range_is_approx_to_date.ReadOnly = Not isEdit
                txt_rp_comments.ReadOnly = Not isEdit
                txt_rp_relation_comments.ReadOnly = Not isEdit
                cmb_rp_gender.IsReadOnly = Not isEdit
                txt_rp_title.ReadOnly = Not isEdit
                'txt_rp_first_name.ReadOnly = Not isEdit
                'txt_rp_middle_name.ReadOnly = Not isEdit
                'txt_rp_prefix.ReadOnly = Not isEdit
                txt_rp_last_name.ReadOnly = Not isEdit
                df_birthdate.ReadOnly = Not isEdit
                txt_rp_birth_place.ReadOnly = Not isEdit
                'cmb_rp_country_of_birth.IsReadOnly = Not isEdit
                txt_rp_mothers_name.ReadOnly = Not isEdit
                txt_rp_alias.ReadOnly = Not isEdit
                'txt_rp_full_name_frn.ReadOnly = Not isEdit
                txt_rp_ssn.ReadOnly = Not isEdit
                txt_rp_passport_number.ReadOnly = Not isEdit
                cmb_rp_passport_country.IsReadOnly = Not isEdit
                txt_rp_id_number.ReadOnly = Not isEdit
                cmb_rp_nationality1.IsReadOnly = Not isEdit
                cmb_rp_nationality2.IsReadOnly = Not isEdit
                cmb_rp_nationality3.IsReadOnly = Not isEdit
                cmb_rp_residence.IsReadOnly = Not isEdit
                'df_rp_residence_since.ReadOnly = Not isEdit
                txt_rp_occupation.ReadOnly = Not isEdit
                txt_rp_employer_name.ReadOnly = Not isEdit
                cbx_rp_deceased.ReadOnly = Not isEdit
                df_rp_date_deceased.ReadOnly = Not isEdit
                txt_rp_tax_number.ReadOnly = Not isEdit
                cbx_rp_tax_reg_number.ReadOnly = Not isEdit
                txt_rp_source_of_wealth.ReadOnly = Not isEdit
                cbx_rp_is_protected.ReadOnly = Not isEdit


                With objTempCustomer_RelatedPerson_Edit
                    Dim refPersonRelation = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Person_Person_Relationship", .PERSON_PERSON_RELATION)
                    Dim refGender = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Jenis_Kelamin", .GENDER)
                    Dim refCountryBirth = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .COUNTRY_OF_BIRTH)
                    Dim refPassportCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .PASSPORT_COUNTRY)
                    Dim refNationality1 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY1)
                    Dim refNationality2 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY2)
                    Dim refNationality3 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY3)
                    Dim refResidence = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .RESIDENCE)

                    cmb_rp_person_relation.SetTextWithTextValue(refPersonRelation.Kode, refPersonRelation.Keterangan)
                    'df_relation_date_range_valid_from.Text = .RELATION_DATE_RANGE_VALID_FROM
                    'cbx_relation_date_range_is_approx_from_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_relation_date_range_valid_to.Text = .RELATION_DATE_RANGE_VALID_TO
                    'cbx_relation_date_range_is_approx_to_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_rp_comments.Value = .COMMENTS
                    txt_rp_relation_comments.Value = .RELATION_COMMENTS
                    If refGender IsNot Nothing Then
                        cmb_rp_gender.SetTextWithTextValue(refGender.Kode, refGender.Keterangan)
                    End If
                    txt_rp_title.Value = .TITLE
                    'txt_rp_first_name.Value = .FIRST_NAME
                    'txt_rp_middle_name.Value = .MIDDLE_NAME
                    'txt_rp_prefix.Value = .PREFIX
                    txt_rp_last_name.Value = .LAST_NAME
                    df_birthdate.Text = .BIRTHDATE
                    txt_rp_birth_place.Value = .BIRTH_PLACE
                    'cmb_rp_country_of_birth.SetTextWithTextValue(refCountryBirth.Kode, refCountryBirth.Keterangan)
                    txt_rp_mothers_name.Value = .MOTHERS_NAME
                    txt_rp_alias.Value = .ALIAS
                    'txt_rp_full_name_frn.Value = .FULL_NAME_FRN
                    txt_rp_ssn.Value = .SSN
                    txt_rp_passport_number.Value = .PASSPORT_NUMBER
                    If refPassportCountry IsNot Nothing Then
                        cmb_rp_passport_country.SetTextWithTextValue(refPassportCountry.Kode, refPassportCountry.Keterangan)
                    End If
                    txt_rp_id_number.Value = .ID_NUMBER
                    If refNationality1 IsNot Nothing Then
                        cmb_rp_nationality1.SetTextWithTextValue(refNationality1.Kode, refNationality1.Keterangan)
                    End If
                    If refNationality2 IsNot Nothing Then
                        cmb_rp_nationality2.SetTextWithTextValue(refNationality2.Kode, refNationality2.Keterangan)
                    End If
                    If refNationality3 IsNot Nothing Then
                        cmb_rp_nationality3.SetTextWithTextValue(refNationality3.Kode, refNationality3.Keterangan)
                    End If
                    If refResidence IsNot Nothing Then
                        cmb_rp_residence.SetTextWithTextValue(refResidence.Kode, refResidence.Keterangan)
                    End If
                    txt_rp_occupation.Value = .OCCUPATION
                    txt_rp_employer_name.Value = .EMPLOYER_NAME
                    cbx_rp_deceased.Checked = .DECEASED
                    df_rp_date_deceased.Text = IIf(.DATE_DECEASED Is Nothing, "", .DATE_DECEASED)
                    txt_rp_tax_number.Value = .TAX_NUMBER
                    cbx_rp_tax_reg_number.Checked = .TAX_REG_NUMBER
                    txt_rp_source_of_wealth.Text = .SOURCE_OF_WEALTH
                    cbx_rp_is_protected.Checked = .IS_PROTECTED

                    GridAddress_RelatedPerson.LoadData(goAML_Customer_Service.GetAddressByPkFk(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, 31))
                    GridAddressWork_RelatedPerson.LoadData(goAML_Customer_Service.GetAddressByPkFk(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, 32))
                    GridPhone_RelatedPerson.LoadData(goAML_Customer_Service.GetPhoneByPkFk(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, 31))
                    GridPhoneWork_RelatedPerson.LoadData(goAML_Customer_Service.GetPhoneByPkFk(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, 32))
                    GridIdentification_RelatedPerson.LoadData(goAML_Customer_Service.GetIdentificationPkFk(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, 31))

                    GridEmail_RelatedPerson.LoadData(goAML_Customer_Service.GetEmailByPkFk(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, 31))
                    GridSanction_RelatedPerson.LoadData(goAML_Customer_Service.GetSanctionByPkFk(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, 31))
                    GridPEP_RelatedPerson.LoadData(goAML_Customer_Service.GetPEPByPkFk(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, 31))

                End With

                WindowDetail_RelatedPerson.Title = "Related Person " & If(isEdit, "Edit", "Detail")
                'GridEmail_RelatedPerson.SetViewMode()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_AdditionalInformation(id As Long, isEdit As Boolean)
        Try
            Dim objCustomer_AdditionalInformation As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information =
                objListgoAML_Ref_AdditionalInfo.Find(Function(x) x.PK_goAML_Ref_Customer_Additional_Information_ID = id)

            If Not objCustomer_AdditionalInformation Is Nothing Then
                FormPanel_AdditionalInfo.Hidden = False
                WindowDetail_AdditionalInfo.Hidden = False
                btnSaveCustomer_AdditionalInfo.Hidden = Not isEdit

                cmb_info_type.IsReadOnly = Not isEdit
                txt_info_subject.ReadOnly = Not isEdit
                txt_info_text.ReadOnly = Not isEdit
                nfd_info_numeric.ReadOnly = Not isEdit
                df_info_date.ReadOnly = Not isEdit
                cbx_info_boolean.ReadOnly = Not isEdit

                With objCustomer_AdditionalInformation
                    Dim refType = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Additional_Info_Type", .INFO_TYPE)

                    cmb_info_type.SetTextWithTextValue(refType.Kode, refType.Keterangan)
                    txt_info_subject.Value = .INFO_SUBJECT
                    txt_info_text.Value = .INFO_TEXT
                    nfd_info_numeric.Value = .INFO_NUMERIC
                    df_info_date.Text = .INFO_DATE
                    cbx_info_boolean.Checked = .INFO_BOOLEAN

                End With

                WindowDetail_AdditionalInfo.Title = "Additional Information " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_EntityIdentification(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Entity_Identification = objListgoAML_Ref_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id)
            objTempCustomer_EntityIdentification_Edit = objListgoAML_vw_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id)

            If Not objTempCustomer_EntityIdentification_Edit Is Nothing Then
                FormPanel_EntityIdentification.Hidden = False
                WindowDetail_EntityIdentification.Hidden = False
                btnSaveCustomer_EntityIdentification.Hidden = Not isEdit

                cmb_ef_type.IsReadOnly = Not isEdit
                txt_ef_number.ReadOnly = Not isEdit
                df_issue_date.ReadOnly = Not isEdit
                df_expiry_date.ReadOnly = Not isEdit
                txt_ef_issued_by.ReadOnly = Not isEdit
                cmb_ef_issue_country.IsReadOnly = Not isEdit
                txt_ef_comments.ReadOnly = Not isEdit


                With objTempCustomer_EntityIdentification_Edit
                    Dim refType = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Identifier_Type", .TYPE)
                    Dim refIssue = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .ISSUE_COUNTRY)

                    cmb_ef_type.SetTextWithTextValue(refType.Kode, refType.Keterangan)
                    txt_ef_number.Value = .NUMBER
                    df_issue_date.Text = IIf(.ISSUE_DATE Is Nothing, "", .ISSUE_DATE)
                    df_expiry_date.Text = IIf(.EXPIRY_DATE Is Nothing, "", .EXPIRY_DATE)
                    txt_ef_issued_by.Value = IIf(.ISSUED_BY Is Nothing, "", .ISSUED_BY)
                    cmb_ef_issue_country.SetTextWithTextValue(refIssue.Kode, refIssue.Keterangan)
                    txt_ef_comments.Value = .COMMENTS

                End With

                WindowDetail_EntityIdentification.Title = "Entity Identification " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_CustomerURL(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_URL = objListgoAML_Ref_EntityUrl.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id)
            objTempCustomer_EntityUrl_Edit = objListgoAML_vw_EntityUrl.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id)

            If Not objTempCustomer_EntityUrl_Edit Is Nothing Then
                FormPanel_URL.Hidden = False
                WindowDetail_URL.Hidden = False
                btnSaveCustomer_URL.Hidden = Not isEdit

                txt_url.ReadOnly = Not isEdit

                With objTempCustomer_EntityUrl_Edit
                    txt_url.Value = .URL
                End With

                WindowDetail_URL.Title = "Entity URL " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_RelatedEntities(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Related_Entities = objListgoAML_Ref_RelatedEntities.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id)
            objTempCustomer_RelatedEntities_Edit = objListgoAML_vw_RelatedEntities.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id)

            If Not objTempCustomer_RelatedEntities_Edit Is Nothing Then
                FormPanel_RelatedEntities.Hidden = False
                WindowDetail_RelatedEntities.Hidden = False
                btnSaveCustomer_EntityRelation.Hidden = Not isEdit

                cmb_re_entity_relation.IsReadOnly = Not isEdit
                'df_re_relation_date_range_valid_from.ReadOnly = Not isEdit
                'cbx_re_relation_date_range_is_approx_from_date.ReadOnly = Not isEdit
                'df_re_relation_date_range_valid_to.ReadOnly = Not isEdit
                'cbx_re_relation_date_range_is_approx_to_date.ReadOnly = Not isEdit
                'nfd_re_share_percentage.ReadOnly = Not isEdit
                txt_re_comments.ReadOnly = Not isEdit
                txt_re_relation_comments.ReadOnly = Not isEdit
                txt_re_name.ReadOnly = Not isEdit
                txt_re_commercial_name.ReadOnly = Not isEdit
                cmb_re_incorporation_legal_form.IsReadOnly = Not isEdit
                txt_re_incorporation_number.ReadOnly = Not isEdit
                txt_re_business.ReadOnly = Not isEdit
                'cmb_re_entity_status.IsReadOnly = Not isEdit
                'df_re_entity_status_date.ReadOnly = Not isEdit
                txt_re_incorporation_state.ReadOnly = Not isEdit
                cmb_re_incorporation_country_code.IsReadOnly = Not isEdit
                df_re_incorporation_date.ReadOnly = Not isEdit
                cbx_re_business_closed.ReadOnly = Not isEdit
                df_re_date_business_closed.ReadOnly = Not isEdit
                txt_re_tax_number.ReadOnly = Not isEdit
                txt_re_tax_reg_number.ReadOnly = Not isEdit


                With objTempCustomer_RelatedEntities_Edit
                    Dim refRelation = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Entity_Relationship", .ENTITY_ENTITY_RELATION)
                    Dim refLegalForm = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Bentuk_Badan_Usaha", .INCORPORATION_LEGAL_FORM)
                    Dim refEntityStatus = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Status", .ENTITY_STATUS)
                    Dim refCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .INCORPORATION_COUNTRY_CODE)

                    If refRelation IsNot Nothing Then
                        cmb_re_entity_relation.SetTextWithTextValue(refRelation.Kode, refRelation.Keterangan)
                    End If
                    'df_re_relation_date_range_valid_from.Text = .RELATION_DATE_RANGE_VALID_FROM
                    'cbx_re_relation_date_range_is_approx_from_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_re_relation_date_range_valid_to.Text = .RELATION_DATE_RANGE_VALID_TO
                    'cbx_re_relation_date_range_is_approx_to_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    'nfd_re_share_percentage.Value = .SHARE_PERCENTAGE
                    txt_re_comments.Value = .COMMENTS
                    txt_re_relation_comments.Value = .RELATION_COMMENTS
                    txt_re_name.Value = .NAME
                    txt_re_commercial_name.Value = .COMMERCIAL_NAME
                    If refLegalForm IsNot Nothing Then
                        cmb_re_incorporation_legal_form.SetTextWithTextValue(refLegalForm.Kode, refLegalForm.Keterangan)
                    End If
                    txt_re_incorporation_number.Value = .INCORPORATION_NUMBER
                    txt_re_business.Value = .BUSINESS
                    'cmb_re_entity_status.SetTextWithTextValue(refEntityStatus.Kode, refEntityStatus.Keterangan)
                    'df_re_entity_status_date.Text = .ENTITY_STATUS
                    txt_re_incorporation_state.Value = .INCORPORATION_STATE
                    If refCountry IsNot Nothing Then
                        cmb_re_incorporation_country_code.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                    End If
                    df_re_incorporation_date.Text = IIf(.INCORPORATION_DATE Is Nothing, "", .INCORPORATION_DATE.ToString())
                    cbx_re_business_closed.Checked = .BUSINESS_CLOSED
                    df_re_date_business_closed.Text = IIf(.DATE_BUSINESS_CLOSED Is Nothing, "", .DATE_BUSINESS_CLOSED.ToString())
                    txt_re_tax_number.Value = .TAX_NUMBER
                    txt_re_tax_reg_number.Value = .TAX_REG_NUMBER

                    Dim phone_list = goAML_Customer_Service.GetPhoneByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36)
                    Dim address_list = goAML_Customer_Service.GetAddressByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36)
                    Dim email_list = goAML_Customer_Service.GetEmailByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36)
                    Dim entity_identification_list = goAML_Customer_Service.GetEntityIdentificationByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36)
                    Dim url_list = goAML_Customer_Service.GetUrlByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36)
                    Dim sanction_list = goAML_Customer_Service.GetSanctionByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36)

                    GridPhone_RelatedEntity.LoadData(phone_list)
                    GridAddress_RelatedEntity.LoadData(address_list)
                    GridEmail_RelatedEntity.LoadData(email_list)
                    GridEntityIdentification_RelatedEntity.LoadData(entity_identification_list)
                    GridURL_RelatedEntity.LoadData(url_list)
                    GridSanction_RelatedEntity.LoadData(sanction_list)

                End With

                WindowDetail_RelatedEntities.Title = "Related Entities " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GrdCmdCustomerPreviousName(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_PreviousName(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_PreviousName(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_Email(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_Email(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_Email(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecord_Email(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_EmploymentHistory(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_EmploymentHistory(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_EmploymentHistory(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_PEP(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_PEP(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_PEP(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecord_PEP(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_NetworkDevice(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_NetworkDevice(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_NetworkDevice(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_SocialMedia(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_SocialMedia(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_SocialMedia(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_Sanction(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_Sanction(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_Sanction(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecord_Sanction(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_RelatedPerson(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_RelatedPerson(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_RelatedPerson(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecord_RelatedPerson(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_AdditionalInformation(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_AdditionalInformation(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_AdditionalInformation(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_EntityIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_EntityIdentification(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_EntityIdentification(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecord_EntityIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_URL(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_CustomerURL(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_CustomerURL(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecord_URL(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_RelatedEntities(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_RelatedEntities(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_RelatedEntities(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecord_RelatedEntities(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_PreviousName_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'If IsDataCustomer_PreviousName_Valid() Then
            '    If objTempCustomerPreviousNameEdit Is Nothing Then
            '        SaveAddPreviousName()
            '    Else
            '        'SaveEditPreviousName()
            '    End If
            'End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_Email_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'If IsDataCustomer_Email_Valid() Then
            '    If objTempCustomer_Email_Edit Is Nothing Then
            '        Save_Email()
            '    Else
            '        Save_Email(False)
            '    End If
            'End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_EmploymentHistory_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'If IsDataCustomer_EmploymentHistory_Valid() Then
            '    If objTempCustomer_EmploymentHistory_Edit Is Nothing Then
            '        SaveAdd_EmploymentHistory()
            '    Else
            '        'SaveEditPreviousName()
            '    End If
            'End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_PersonPEP_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_NetworkDevice_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_SocialMedia_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_Sanction_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_RelatedPerson_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_AdditionalInfo_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_Url_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_EntityIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_RelatedEntities_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub cb_Director_Deceased_Load(sender As Object, e As EventArgs) Handles cb_Director_Deceased.Load

    End Sub
End Class

