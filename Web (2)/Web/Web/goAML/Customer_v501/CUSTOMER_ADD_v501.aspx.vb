﻿Imports NawaBLL
Imports Ext
Imports GoAMLBLL
Imports System.Data.SqlClient
Imports NawaDAL
Imports GoAMLDAL
Imports Elmah
Imports System.Data
Imports System.Net.Mail
Imports Checkbox = Ext.Net.Checkbox
Imports GoAMLBLL.goAML_CustomerDataBLL
Imports System.ComponentModel
Imports NPOI.Util.Collections
Imports GoAMLBLL.goAML
Imports GoAMLBLL.goAML.Services

Partial Class CUSTOMER_ADD_v501
    Inherits ParentPage
    Public objFormModuleView As NawaBLL.FormModuleDetail
    Public objgoAML_CustomerBLL As New GoAMLBLL.goAML_CustomerBLL
    Private mModuleName As String
    Private TempPK As Int32

    Public Property ModulName() As String
        Get
            Return mModuleName
        End Get
        Set(ByVal value As String)
            mModuleName = value
        End Set
    End Property
    Sub ClearSession()
        tempflag = New Integer
        obj_customer = New goAML_Ref_Customer
        objListgoAML_Ref_Phone = New List(Of GoAMLDAL.goAML_Ref_Phone)
        objListgoAML_Ref_Address = New List(Of GoAMLDAL.goAML_Ref_Address)
        objListgoAML_Ref_Director_Phone = New List(Of GoAMLDAL.goAML_Ref_Phone)
        objListgoAML_Ref_Director_Address = New List(Of GoAMLDAL.goAML_Ref_Address)
        objListgoAML_Ref_Director_Employer_Phone = New List(Of GoAMLDAL.goAML_Ref_Phone)
        objListgoAML_Ref_Director_Employer_Address = New List(Of GoAMLDAL.goAML_Ref_Address)
        objListgoAML_Ref_Identification = New List(Of GoAMLDAL.goAML_Person_Identification)
        objListgoAML_Ref_Identification_Director = New List(Of GoAMLDAL.goAML_Person_Identification)
        objListgoAML_Ref_Director = New List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director)
        objListgoAML_Ref_Employer_Phone = New List(Of GoAMLDAL.goAML_Ref_Phone)
        objListgoAML_Ref_Employer_Address = New List(Of GoAMLDAL.goAML_Ref_Address)
        objListgoAML_vw_Customer_Phones = New List(Of Vw_Customer_Phones)
        objListgoAML_vw_CUSTOMER_ADDresses = New List(Of Vw_Customer_Addresses)
        objListgoAML_vw_Director_Phones = New List(Of Vw_Director_Phones)
        objListgoAML_vw_Director_Addresses = New List(Of Vw_Director_Addresses)
        objListgoAML_vw_Director_Employer_Phones = New List(Of Vw_Director_Employer_Phones)
        objListgoAML_vw_Director_Employer_Addresses = New List(Of Vw_Director_Employer_Addresses)
        objListgoAML_vw_Customer_Identification = New List(Of Vw_Person_Identification)
        objListgoAML_vw_Customer_Identification_Director = New List(Of Vw_Person_Identification)
        objListgoAML_vw_Employer_Phones = New List(Of Vw_Employer_Phones)
        objListgoAML_vw_Employer_Addresses = New List(Of Vw_Employer_Addresses)
        objListgoAML_vw_Customer_Entity_Director = New List(Of Vw_Customer_Director)

        'Add Previous Name, Septian, 4 Jan 2023
        objListgoAML_Ref_Previous_Name = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        objListgoAML_vw_Previous_Name = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)

        objListgoAML_Ref_Email = New List(Of DataModel.goAML_Ref_Customer_Email)
        objListgoAML_vw_Email = New List(Of DataModel.goAML_Ref_Customer_Email)

        objListgoAML_Ref_EmploymentHistory = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        objListgoAML_vw_EmploymentHistory = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)

        objListgoAML_Ref_PersonPEP = New List(Of DataModel.goAML_Ref_Customer_PEP)
        objListgoAML_vw_PersonPEP = New List(Of DataModel.goAML_Ref_Customer_PEP)

        objListgoAML_Ref_NetworkDevice = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        objListgoAML_vw_NetworkDevice = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)

        objListgoAML_Ref_SocialMedia = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        objListgoAML_vw_SocialMedia = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)

        objListgoAML_Ref_Sanction = New List(Of DataModel.goAML_Ref_Customer_Sanction)
        objListgoAML_vw_Sanction = New List(Of DataModel.goAML_Ref_Customer_Sanction)

        objListgoAML_Ref_RelatedPerson = New List(Of DataModel.goAML_Ref_Customer_Related_Person)
        objListgoAML_vw_RelatedPerson = New List(Of DataModel.goAML_Ref_Customer_Related_Person)

        objListgoAML_Ref_AdditionalInfo = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        objListgoAML_vw_AdditionalInfo = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)

        objListgoAML_Ref_EntityUrl = New List(Of DataModel.goAML_Ref_Customer_URL)
        objListgoAML_vw_EntityUrl = New List(Of DataModel.goAML_Ref_Customer_URL)

        objListgoAML_Ref_EntityIdentification = New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        objListgoAML_vw_EntityIdentification = New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)

        objListgoAML_Ref_RelatedEntities = New List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        objListgoAML_vw_RelatedEntities = New List(Of DataModel.goAML_Ref_Customer_Related_Entities)

        objListgoAML_Ref_Director2 = New List(Of DataModel.goAML_Ref_Entity_Director)

        ' 2023-10-10, Nael: Clear juga data temporarynya
        objTempgoAML_Ref_Director2 = New DataModel.goAML_Ref_Entity_Director()
        objTempgoAML_Ref_EmployerAddresses = New GoAMLDAL.goAML_Ref_Address()
        objTempgoAML_Ref_EmployerPhones = New GoAMLDAL.goAML_Ref_Phone()
        objTempgoAML_Ref_Identification = New GoAMLDAL.goAML_Person_Identification()

        objTempCustomer_RelatedEntities_Edit = New DataModel.goAML_Ref_Customer_Related_Entities()
        'nfd_re_share_percentage.RawValue = 0
        'End Add
    End Sub

    Public Property tempflag() As Integer
        Get
            Return Session("CUSTOMER_EDIT.tempflag")
        End Get
        Set(ByVal value As Integer)
            Session("CUSTOMER_EDIT.tempflag") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Employer_Address() As List(Of GoAMLDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Employer_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Address))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Employer_Address") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Employer_Addresses() As List(Of GoAMLDAL.Vw_Employer_Addresses)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_Employer_Addresses")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Employer_Addresses))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_Employer_Addresses") = value
        End Set
    End Property

    Public Property objTempEmployerAddressEdit() As GoAMLDAL.Vw_Employer_Addresses
        Get
            Return Session("CUSTOMER_ADD_v501.objTempEmployerAddressEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Employer_Addresses)
            Session("CUSTOMER_ADD_v501.objTempEmployerAddressEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_EmployerAddresses() As GoAMLDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_EmployerAddresses")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Address)
            Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_EmployerAddresses") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Employer_Phone() As List(Of GoAMLDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Employer_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Phone))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Employer_Phone") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Employer_Phones() As List(Of GoAMLDAL.Vw_Employer_Phones)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_Employer_Phones")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Employer_Phones))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_Employer_Phones") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_EmployerPhones() As GoAMLDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_EmployerPhones")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Phone)
            Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_EmployerPhones") = value
        End Set
    End Property

    Public Property objTempEmployerPhonesEdit() As GoAMLDAL.Vw_Employer_Phones
        Get
            Return Session("CUSTOMER_ADD_v501.objTempEmployerPhonesEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Employer_Phones)
            Session("CUSTOMER_ADD_v501.objTempEmployerPhonesEdit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Identification() As List(Of GoAMLDAL.goAML_Person_Identification)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Identification")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Person_Identification))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Identification") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Identification() As List(Of GoAMLDAL.Vw_Person_Identification)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_Customer_Identification")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Person_Identification))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_Customer_Identification") = value
        End Set
    End Property

    Public Property objTempCustomerIdentificationEdit() As GoAMLDAL.Vw_Person_Identification
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomerIdentificationEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Person_Identification)
            Session("CUSTOMER_ADD_v501.objTempCustomerIdentificationEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Identification() As GoAMLDAL.goAML_Person_Identification
        Get
            Return Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Identification")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Person_Identification)
            Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Identification") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Address() As GoAMLDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Address")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Address)
            Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Address") = value
        End Set
    End Property


    Public Property objListgoAML_Ref_Address() As List(Of GoAMLDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Address))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Address") = value
        End Set
    End Property


    Public Property objTempCustomerAddressEdit() As GoAMLDAL.Vw_Customer_Addresses
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomerAddressEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Customer_Addresses)
            Session("CUSTOMER_ADD_v501.objTempCustomerAddressEdit") = value
        End Set
    End Property

    Public Property objListgoAML_vw_CUSTOMER_ADDresses() As List(Of GoAMLDAL.Vw_Customer_Addresses)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_CUSTOMER_ADDresses")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Customer_Addresses))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_CUSTOMER_ADDresses") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Phones() As List(Of GoAMLDAL.Vw_Customer_Phones)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_Customer_Phones")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Customer_Phones))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_Customer_Phones") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Phone() As List(Of GoAMLDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_ADD_v501.goAML_Ref_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Phone))
            Session("CUSTOMER_ADD_v501.goAML_Ref_Phone") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Phone() As GoAMLDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Phone")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Phone)
            Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Phone") = value
        End Set
    End Property

    Public Property objTempCustomerPhoneEdit() As GoAMLDAL.Vw_Customer_Phones
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomerPhoneEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Customer_Phones)
            Session("CUSTOMER_ADD_v501.objTempCustomerPhoneEdit") = value
        End Set
    End Property

    Public Property obj_customer() As goAML_Ref_Customer
        Get
            If Session("CUSTOMER_ADD_v501.obj_customer") Is Nothing Then
                Session("CUSTOMER_ADD_v501.obj_customer") = New goAML_Ref_Customer
            End If
            Return Session("CUSTOMER_ADD_v501.obj_customer")
        End Get
        Set(ByVal value As goAML_Ref_Customer)
            Session("CUSTOMER_ADD_v501.obj_customer") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director() As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Director") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Entity_Director() As List(Of GoAMLDAL.Vw_Customer_Director)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_Customer_Entity_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Customer_Director))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_Customer_Entity_Director") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director2() As List(Of DataModel.goAML_Ref_Entity_Director)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Director2")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Entity_Director))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Director2") = value
        End Set
    End Property

    ' 2023-10-24, Nael: bikin obj temporary untuk add director
    'Public Property objTempDirectorAdd() As DataModel.goAML_Ref_Entity_Director
    '    Get
    '        Return Session("CUSTOMER_ADD_v501.objTempDirectorAdd")
    '    End Get
    '    Set(ByVal value As DataModel.goAML_Ref_Entity_Director)
    '        Session("CUSTOMER_ADD_v501.objTempDirectorAdd") = value
    '    End Set
    'End Property

    Public Property objTempCustomerDirectorEdit() As GoAMLDAL.Vw_Customer_Director
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomerDirectorEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Customer_Director)
            Session("CUSTOMER_ADD_v501.objTempCustomerDirectorEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director() As GoAMLDAL.goAML_Ref_Customer_Entity_Director
        Get
            Return Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Director")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Customer_Entity_Director)
            Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Director") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director2() As DataModel.goAML_Ref_Entity_Director
        Get
            Return Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Director2")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Entity_Director)
            Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Director2") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Employer_Address() As List(Of GoAMLDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Director_Employer_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Address))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Director_Employer_Address") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Director_Employer_Addresses() As List(Of GoAMLDAL.Vw_Director_Employer_Addresses)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_Director_Employer_Addresses")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Employer_Addresses))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_Director_Employer_Addresses") = value
        End Set
    End Property

    Public Property objTempDirectorEmployerAddressEdit() As GoAMLDAL.Vw_Director_Employer_Addresses
        Get
            Return Session("CUSTOMER_ADD_v501.objTempDirectorEmployerAddressEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Director_Employer_Addresses)
            Session("CUSTOMER_ADD_v501.objTempDirectorEmployerAddressEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director_EmployerAddresses() As GoAMLDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Director_EmployerAddresses")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Address)
            Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Director_EmployerAddresses") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Address() As List(Of GoAMLDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Director_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Address))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Director_Address") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Director_Addresses() As List(Of GoAMLDAL.Vw_Director_Addresses)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_Director_Addresses")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Addresses))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_Director_Addresses") = value
        End Set
    End Property

    Public Property objTempDirectorAddressEdit() As GoAMLDAL.Vw_Director_Addresses
        Get
            Return Session("CUSTOMER_ADD_v501.objTempDirectorAddressEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Director_Addresses)
            Session("CUSTOMER_ADD_v501.objTempDirectorAddressEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director_Addresses() As GoAMLDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Director_Addresses")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Address)
            Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Director_Addresses") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Employer_Phone() As List(Of GoAMLDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Director_Employer_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Phone))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Director_Employer_Phone") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Director_Employer_Phones() As List(Of GoAMLDAL.Vw_Director_Employer_Phones)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_Director_Employer_Phones")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Employer_Phones))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_Director_Employer_Phones") = value
        End Set
    End Property

    Public Property objTempDirectorEmployerPhoneEdit() As GoAMLDAL.Vw_Director_Employer_Phones
        Get
            Return Session("CUSTOMER_ADD_v501.objTempDirectorEmployerPhoneEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Director_Employer_Phones)
            Session("CUSTOMER_ADD_v501.objTempDirectorEmployerPhoneEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director_EmployerPhones() As GoAMLDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Director_EmployerPhones")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Phone)
            Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Director_EmployerPhones") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Phone() As List(Of GoAMLDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Director_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Phone))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Director_Phone") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Director_Phones() As List(Of GoAMLDAL.Vw_Director_Phones)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_Director_Phones")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Phones))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_Director_Phones") = value
        End Set
    End Property

    Public Property objTempDirectorPhoneEdit() As GoAMLDAL.Vw_Director_Phones
        Get
            Return Session("CUSTOMER_ADD_v501.objTempDirectorPhoneEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Director_Phones)
            Session("CUSTOMER_ADD_v501.objTempDirectorPhoneEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director_Phones() As GoAMLDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Director_Phones")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Phone)
            Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Director_Phones") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Identification_Director() As List(Of GoAMLDAL.goAML_Person_Identification)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Identification_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Person_Identification))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Identification_Director") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Identification_Director() As List(Of GoAMLDAL.Vw_Person_Identification)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_Customer_Identification_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Person_Identification))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_Customer_Identification_Director") = value
        End Set
    End Property

    Public Property objTempCustomerIdentificationEdit_Director() As GoAMLDAL.Vw_Person_Identification
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomerIdentificationEdit_Director")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Person_Identification)
            Session("CUSTOMER_ADD_v501.objTempCustomerIdentificationEdit_Director") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Identification_Director() As GoAMLDAL.goAML_Person_Identification
        Get
            Return Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Identification_Director")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Person_Identification)
            Session("CUSTOMER_ADD_v501.objTempgoAML_Ref_Identification_Director") = value
        End Set
    End Property

    '' Add 04-Jan-2023, Septian. GoAML 5.0.1
    Public Property objTempCustomerPreviousNameEdit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomerPreviousName")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
            Session("CUSTOMER_ADD_v501.objTempCustomerPreviousName") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Previous_Name() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Previous_Name")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Previous_name") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Previous_Name() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_Previous_Name")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_Previous_Name") = value
        End Set
    End Property

    Public Property objTempCustomer_Email_Edit() As DataModel.goAML_Ref_Customer_Email
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomer_Email_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Email)
            Session("CUSTOMER_ADD_v501.objTempCustomer_Email_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Email() As List(Of DataModel.goAML_Ref_Customer_Email)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Email")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Email))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Email") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Email() As List(Of DataModel.goAML_Ref_Customer_Email)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_Email")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Email))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_Email") = value
        End Set
    End Property

    Public Property objTempCustomer_EmploymentHistory_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomer_EmploymentHistory_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
            Session("CUSTOMER_ADD_v501.objTempCustomer_EmploymentHistory_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_EmploymentHistory() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_EmploymentHistory")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_EmploymentHistory") = value
        End Set
    End Property

    Public Property objListgoAML_vw_EmploymentHistory() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_EmploymentHistory")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_EmploymentHistory") = value
        End Set
    End Property

    Public Property objTempCustomer_PersonPEP_Edit() As DataModel.goAML_Ref_Customer_PEP
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomer_PersonPEP_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_PEP)
            Session("CUSTOMER_ADD_v501.objTempCustomer_PersonPEP_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_PersonPEP() As List(Of DataModel.goAML_Ref_Customer_PEP)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_PersonPEP")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_PEP))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_PersonPEP") = value
        End Set
    End Property

    Public Property objListgoAML_vw_PersonPEP() As List(Of DataModel.goAML_Ref_Customer_PEP)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_PersonPEP")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_PEP))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_PersonPEP") = value
        End Set
    End Property
    Public Property objTempCustomer_NetworkDevice_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomer_NetworkDevice_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
            Session("CUSTOMER_ADD_v501.objTempCustomer_NetworkDevice_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_NetworkDevice() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_NetworkDevice")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_NetworkDevice") = value
        End Set
    End Property

    Public Property objListgoAML_vw_NetworkDevice() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_NetworkDevice")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_NetworkDevice") = value
        End Set
    End Property

    Public Property objTempCustomer_SocialMedia_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomer_SocialMedia_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
            Session("CUSTOMER_ADD_v501.objTempCustomer_SocialMedia_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_SocialMedia() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_SocialMedia")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_SocialMedia") = value
        End Set
    End Property
    Public Property objListgoAML_vw_SocialMedia() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_SocialMedia")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_SocialMedia") = value
        End Set
    End Property

    Public Property objTempCustomer_Sanction_Edit() As DataModel.goAML_Ref_Customer_Sanction
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomer_Sanction_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Sanction)
            Session("CUSTOMER_ADD_v501.objTempCustomer_Sanction_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_Sanction() As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Sanction")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Sanction))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_Sanction") = value
        End Set
    End Property
    Public Property objListgoAML_vw_Sanction() As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_Sanction")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Sanction))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_Sanction") = value
        End Set
    End Property

    Public Property objTempCustomer_RelatedPerson_Edit() As DataModel.goAML_Ref_Customer_Related_Person
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomer_RelatedPerson_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Person)
            Session("CUSTOMER_ADD_v501.objTempCustomer_RelatedPerson_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_RelatedPerson() As List(Of DataModel.goAML_Ref_Customer_Related_Person)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_RelatedPerson")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Person))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_RelatedPerson") = value
        End Set
    End Property
    Public Property objListgoAML_vw_RelatedPerson() As List(Of DataModel.goAML_Ref_Customer_Related_Person)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_RelatedPerson")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Person))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_RelatedPerson") = value
        End Set
    End Property

    Public Property objTempCustomer_AdditionalInfo_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomer_AdditionalInfo_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
            Session("CUSTOMER_ADD_v501.objTempCustomer_AdditionalInfo_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_AdditionalInfo() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_AdditionalInfo")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_AdditionalInfo") = value
        End Set
    End Property
    Public Property objListgoAML_vw_AdditionalInfo() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_AdditionalInfo")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_AdditionalInfo") = value
        End Set
    End Property

    Public Property objTempCustomer_EntityUrl_Edit() As DataModel.goAML_Ref_Customer_URL
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomer_EntityUrl_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_URL)
            Session("CUSTOMER_ADD_v501.objTempCustomer_EntityUrl_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityUrl() As List(Of DataModel.goAML_Ref_Customer_URL)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_EntityUrl")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_URL))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_EntityUrl") = value
        End Set
    End Property
    Public Property objListgoAML_vw_EntityUrl() As List(Of DataModel.goAML_Ref_Customer_URL)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_EntityUrl")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_URL))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_EntityUrl") = value
        End Set
    End Property

    Public Property objTempCustomer_EntityIdentification_Edit() As DataModel.goAML_Ref_Customer_Entity_Identification
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomer_EntityIdentification_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Entity_Identification)
            Session("CUSTOMER_ADD_v501.objTempCustomer_EntityIdentification_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityIdentification() As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_EntityIdentification")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_EntityIdentification") = value
        End Set
    End Property
    Public Property objListgoAML_vw_EntityIdentification() As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_EntityIdentification")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_EntityIdentification") = value
        End Set
    End Property

    Public Property objTempCustomer_RelatedEntities_Edit() As DataModel.goAML_Ref_Customer_Related_Entities
        Get
            Return Session("CUSTOMER_ADD_v501.objTempCustomer_RelatedEntities_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Entities)
            Session("CUSTOMER_ADD_v501.objTempCustomer_RelatedEntities_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_RelatedEntities() As List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_Ref_RelatedEntities")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Entities))
            Session("CUSTOMER_ADD_v501.objListgoAML_Ref_RelatedEntities") = value
        End Set
    End Property
    Public Property objListgoAML_vw_RelatedEntities() As List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        Get
            Return Session("CUSTOMER_ADD_v501.objListgoAML_vw_RelatedEntities")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Entities))
            Session("CUSTOMER_ADD_v501.objListgoAML_vw_RelatedEntities") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Email() As DataModel.goAML_Ref_Customer_Email
        Get
            Return Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_Email")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Email)
            Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_Email") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_PEP() As DataModel.goAML_Ref_Customer_PEP
        Get
            Return Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_PEP")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_PEP)
            Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_PEP") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Sanction() As DataModel.goAML_Ref_Customer_Sanction
        Get
            Return Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_Sanction")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Sanction)
            Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_Sanction") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Related_Person() As DataModel.goAML_Ref_Customer_Related_Person
        Get
            Return Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_Related_Person")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Person)
            Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_Related_Person") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_URL() As DataModel.goAML_Ref_Customer_URL
        Get
            Return Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_URL")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_URL)
            Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_URL") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Entity_Identification() As DataModel.goAML_Ref_Customer_Entity_Identification
        Get
            Return Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_Entity_Identification")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Entity_Identification)
            Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_Entity_Identification") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Related_Entities() As DataModel.goAML_Ref_Customer_Related_Entities
        Get
            Return Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_Related_Entities")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Entities)
            Session("CUSTOMER_ADD_v501.objTemp_goAML_Ref_Customer_Related_Entities") = value
        End Set
    End Property
    '' End 04-Kan-2023


    Function IsDataCustomerValid() As Boolean
        'Return True
        If Cmb_TypePerson.SelectedItemValue Is Nothing Then
            Throw New Exception("Type Person is required")
        End If
        'If Cmb_TypePerson.SelectedItemValue = 1 Then
        '    If txt_INDV_Occupation.Text = "" Then
        '        Throw New Exception("Occupation is required")
        '    End If
        '    If txt_INDV_Source_of_Wealth.Text = "" Then
        '        Throw New Exception("Source of Wealth is required")
        '    End If
        '    If txt_cif.Text = "" Then
        '        Throw New Exception("CIF is required")
        '    End If
        '    If txt_gcn.Text = "" Then
        '        Throw New Exception("GCN is required")
        '    End If
        '    If GCNPrimary.Checked = False Then
        '        If txt_gcn.Value IsNot Nothing Then
        '            Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_gcn.Value)
        '            If checkGCN Is Nothing Then
        '                'DirectCast(GCNPrimary, Checkbox).Checked = True
        '                Throw New Exception("Untuk GCN " + txt_gcn.Value + " belum memiliki GCNPrimary")
        '            End If
        '        End If
        '    End If
        '    If txt_INDV_Last_Name.Text = "" Then
        '        Throw New Exception("Full Name is required")
        '    End If
        '    If txt_INDV_Birthdate.RawValue = "" Then
        '        Throw New Exception("Birth Date is required")
        '    End If
        '    If txt_INDV_Birth_place.Text = "" Then
        '        Throw New Exception("Birth Place is required")
        '    End If
        '    'Update: Zikri_08102020
        '    If txt_INDV_SSN.Text.Trim IsNot "" Then
        '        If Not IsNumber(txt_INDV_SSN.Text) Then
        '            Throw New Exception("NIK is Invalid")
        '        End If
        '        If txt_INDV_SSN.Text.Trim.Length <> 16 Then
        '            Throw New Exception("NIK must contains 16 digit number")
        '        End If
        '    End If
        '    'End Update
        '    If cmb_INDV_Gender.SelectedItemValue = "" Then
        '        Throw New Exception("Gender is required")
        '    End If
        '    If cmb_INDV_Nationality1.SelectedItemValue = "" Then
        '        Throw New Exception("Nationality 1  is required")
        '    End If
        '    If cmb_INDV_Residence.SelectedItemValue = "" Then
        '        Throw New Exception("Residence is required")
        '    End If
        '    'Update: Zikri_11092020 Menambah Validasi format email
        '    If txt_INDV_Email.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email.Text) Then
        '            Throw New Exception("Invalid Email Address in Email")
        '        End If
        '    End If
        '    If txt_INDV_Email2.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email2.Text) Then
        '            Throw New Exception("Invalid Email Address in Email 2")
        '        End If
        '    End If
        '    If txt_INDV_Email3.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email3.Text) Then
        '            Throw New Exception("Invalid Email Address in Email 3")
        '        End If
        '    End If
        '    If txt_INDV_Email4.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email4.Text) Then
        '            Throw New Exception("Invalid Email Address in Email 4")
        '        End If
        '    End If
        '    If txt_INDV_Email5.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email5.Text) Then
        '            Throw New Exception("Invalid Email Address in Email 5")
        '        End If
        '    End If
        '    'End Update
        '    If Not String.IsNullOrEmpty(txt_INDV_ID_Number.Text) Then
        '        If objListgoAML_Ref_Identification.Count = 0 Then
        '            Throw New Exception("Identification is required")
        '        End If
        '    End If

        '    'If objListgoAML_Ref_Employer_Phone.Count = 0 Then
        '    '    Throw New Exception("Employer Phone is required")
        '    'End If
        '    'If objListgoAML_Ref_Employer_Address.Count = 0 Then
        '    '    Throw New Exception("Employer Address is required")
        '    'End If
        '    If cb_Deceased.Checked = True Then
        '            If txt_deceased_date.RawValue = "" Then
        '                Throw New Exception("Date Deceased is required")
        '            End If
        '        End If

        '    ElseIf Cmb_TypePerson.SelectedItemValue = 2 Then
        '        If GCNPrimary.Checked = False Then
        '        If txt_gcn.Value IsNot Nothing Then
        '            Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_gcn.Value)
        '            If checkGCN Is Nothing Then
        '                'DirectCast(GCNPrimary, Checkbox).Checked = True
        '                Throw New Exception("Untuk GCN " + txt_gcn.Value + " belum memiliki GCNPrimary")
        '            End If
        '        End If
        '    End If
        '    If txt_Corp_Business.Text = "" Then
        '        Throw New Exception("Corp Bussiness is required")
        '    End If
        '    'Update: Zikri_11092020 Menambah Validasi format email
        '    If txt_Corp_Email.Text IsNot "" Then
        '        If Not isEmailAddress(txt_Corp_Email.Text) Then
        '            Throw New Exception("Invalid Email Address in Email Korporasi")
        '        End If
        '    End If
        '    'End Update
        '    If txt_cif.Text = "" Then
        '        Throw New Exception("CIF is required")
        '    End If
        '    If txt_gcn.Text = "" Then
        '        Throw New Exception("GCN is required")
        '    End If
        '    If txt_Corp_Name.Text = "" Then
        '        Throw New Exception("Corp Name is required")
        '    End If
        '    If cmb_Corp_Incorporation_Country_Code.SelectedItemValue = "" Then
        '        Throw New Exception("Country is required")
        '    End If
        '    'If cmb_Corp_Role.RawValue = "" Then
        '    '    Throw New Exception("Role is required")
        '    'End If
        '    If objListgoAML_Ref_Director.Count = 0 Then
        '        Throw New Exception("Director is required")
        '    End If

        'End If
        Dim dataCIF = goAML_CustomerBLL.GetCustomerbyCIF(txt_cif.Text)
        If dataCIF IsNot Nothing Then
            Throw New Exception("CIF Already Exist")
        End If

        'If obj_customer.FK_Customer_Type_ID.Value = 1 Then
        If Cmb_TypePerson.SelectedItemValue = 1 Then ' agam 07122020
            If Replace(txt_cif.Text, " ", "") = "" Then
                Throw New Exception("CIF is required")
            End If
            If Replace(txt_gcn.Text, " ", "") = "" Then
                Throw New Exception("GCN is required")
            End If
            ' START: 2023-10-23, Nael, cek length dari CIF dan CGN (maksimal 20 karakter)
            If txt_gcn.Text.Length > 20 Then
                Throw New Exception("The maximum number of characters for CGN is 20")
            End If

            If txt_cif.Text.Length > 20 Then
                Throw New Exception("The maximum number of characters for CIF is 20")
            End If

            ' END: 2023-10-23, Nael
            'dedy added 25082020
            'dedy end added 25082020
            'Update: Zikri_18092020 Validasi GCN
            If GCNPrimary.Checked = False Then
                If txt_gcn.Value IsNot Nothing Then
                    Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_gcn.Value)
                    If checkGCN Is Nothing Then
                        'DirectCast(GCNPrimary, Checkbox).Checked = True
                        Throw New Exception("Untuk GCN " + txt_gcn.Value + " belum memiliki GCNPrimary")
                        'ElseIf checkGCN.GCN = txt_GCN.Value Then
                        '    Throw New Exception("Untuk GCN " + txt_GCN.Value + " belum memiliki GCNPrimary")
                    End If
                End If
            End If
            If Replace(txt_INDV_Last_Name.Text, " ", "") = "" Then
                Throw New Exception("Full Name is required")
            End If
            If cmb_INDV_Gender.SelectedItemValue = "" Then
                Throw New Exception("Gender is required")
            End If
            If Replace(txt_INDV_Birthdate.RawValue, " ", "") = "" Then
                Throw New Exception("Birth Date is required")
            End If
            If Replace(txt_INDV_Birth_place.Text, " ", "") = "" Then
                Throw New Exception("Birth Place is required")
            End If
            'Update: Zikri_08102020
            If txt_INDV_SSN.Text.Trim IsNot "" Then
                If Not IsNumber(txt_INDV_SSN.Text) Then
                    Throw New Exception("NIK is Invalid")
                End If
                If txt_INDV_SSN.Text.Trim.Length <> 16 Then
                    Throw New Exception("NIK must contains 16 digit number")
                End If
            End If
            'End Update
            If cmb_INDV_Nationality1.SelectedItemValue = "" Then
                Throw New Exception("Nationality 1  is required")
            End If
            If cmb_INDV_Residence.SelectedItemValue = "" Then
                Throw New Exception("Domicile Country is required")
            End If
            If Replace(txt_INDV_Source_of_Wealth.Text, " ", "") = "" Then
                Throw New Exception("Source of Wealth is required")
            End If
            'Update: Zikri_11092020 Menambah Validasi format email
            If Replace(txt_INDV_Occupation.Text, " ", "") = "" Then
                Throw New Exception("Occupation is required")
            End If
            ''End Update

            'comment by septian, goAML 5.0.1, 2023-02-12
            'uncomment by Nael, goAML 5.2, 2023-10-11
            If Replace(txt_INDV_Email.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email.Text) Then
                    Throw New Exception("Invalid Email Address in Email")
                End If
            End If
            If Replace(txt_INDV_Email2.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email2.Text) Then
                    Throw New Exception("Invalid Email Address in Email 2")
                End If
            End If
            If Replace(txt_INDV_Email3.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email3.Text) Then
                    Throw New Exception("Invalid Email Address in Email 3")
                End If
            End If
            If Replace(txt_INDV_Email4.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email4.Text) Then
                    Throw New Exception("Invalid Email Address in Email 4")
                End If
            End If
            If Replace(txt_INDV_Email5.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email5.Text) Then
                    Throw New Exception("Invalid Email Address in Email 5")
                End If
            End If
            'end Comment
            ''End Update
            'If cb_Deceased.Checked = True Then
            '    If txt_deceased_date.RawValue = "" Then
            '        Throw New Exception("Date Deceased is required")
            '    End If
            'End If
            'If Not String.IsNullOrEmpty(txt_INDV_ID_Number.Text) Then
            '    If objListgoAML_Ref_Identification.Count = 0 Then
            '        Throw New Exception("Identification is required")
            '    End If
            'End If
            'If objListgoAML_Ref_Employer_Phone.Count = 0 Then
            '    Throw New Exception("Employer Phone is required")
            'End If
            'If objListgoAML_Ref_Employer_Address.Count = 0 Then
            '    Throw New Exception("Employer Address is required")
            'End If
            ' ElseIf obj_customer.FK_Customer_Type_ID.Value = 2 Then

            'add by Septian, goAML 5.0.1, 2022-02-16
            'If cmb_INDV_Country_Of_Birth.SelectedItemValue Is Nothing Then
            '    Throw New Exception("Country Of Birth is required")
            'End If
            'If txt_INDV_Foreign_Name.Text.Trim = "" Then
            '    Throw New Exception("Fullname foreign is required")
            'End If
            'If dte_INDV_ResidenceSince.Text.Trim = "" Then
            '    Throw New Exception("Residence since is required")
            'End If
            'end add
        ElseIf Cmb_TypePerson.SelectedItemValue = 2 Then ' agam 07122020
            'Update: Zikri_18092020 Validasi GCN
            If GCNPrimary.Checked = False Then
                If txt_gcn.Value IsNot Nothing Then
                    Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_gcn.Value)
                    If checkGCN Is Nothing Then
                        'DirectCast(GCNPrimary, Checkbox).Checked = True
                        Throw New Exception("Untuk GCN " + txt_gcn.Value + " belum memiliki GCNPrimary")
                        'ElseIf checkGCN.GCN = txt_GCN.Value Then
                        '    Throw New Exception("Untuk GCN " + txt_GCN.Value + " belum memiliki GCNPrimary")
                    End If
                End If
            End If
            'Update: Zikri_11092020 Menambah Validasi format email
            'comment by septian, goAML 5.0.1, 2023-02-12
            'uncomment by Nael, goAML 5.2, 2023-10-11
            If Replace(txt_Corp_Email.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_Corp_Email.Text) Then
                    Throw New Exception("Invalid Email Address in Email Korporasi")
                End If
            End If
            'End comment
            'End Update
            If Replace(txt_cif.Text, " ", "") = "" Then
                Throw New Exception("CIF is required")
            End If
            'dedy added 25082020
            If Replace(txt_gcn.Text, " ", "") = "" Then
                Throw New Exception("GCN is required")
            End If
            'dedy end added 25082020
            If Replace(txt_Corp_Name.Text, " ", "") = "" Then
                Throw New Exception("Corp Name is required")
            End If
            'End Update
            If Replace(txt_Corp_Business.Text, " ", "") = "" Then
                Throw New Exception("Line of Bussiness is required")
            End If
            If cmb_Corp_Incorporation_Country_Code.SelectedItemValue = "" Then
                Throw New Exception("Country is required")
            End If
            'If cmb_corp_role.RawValue = "" Then
            '    Throw New Exception("Role is required")
            'End If
            'If objListgoAML_Ref_Director.Count = 0 Then
            '    Throw New Exception("Director is required")
            'End If

            'add by Septian, goAML 5.0.1, 2022-02-16
            'If cmb_Corp_EntityStatus.SelectedItemValue Is Nothing Then
            '    Throw New Exception("Entity Status is required")
            'End If
            'If dte_Corp_EntityStatusDate.Text.Trim = "" Then
            '    Throw New Exception("Entity Status Date is required")
            'End If
            'end add
        End If

        If objListgoAML_Ref_Phone.Count = 0 Then
            Throw New Exception("Phone is required")
        End If
        If objListgoAML_Ref_Address.Count = 0 Then
            Throw New Exception("Address is required")
        End If
        Return True
    End Function
    'Update: Zikri_11092020 Menambah Validasi format email
    Private Function isEmailAddress(text As String) As Boolean
        Try

            Dim email As New Regex("([\w-+]+(?:\.[\w-+]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7})")
            Return email.IsMatch(text)

        Catch ex As Exception
            Return False
        End Try
    End Function
    'End Update
    'Update: Zikri_14092020 Add Validation GCN
    Protected Sub CheckGCN_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If GCNPrimary.Checked = True Then
                If txt_gcn.Value Is Nothing Or txt_gcn.Value = "" Then
                    DirectCast(GCNPrimary, Checkbox).Checked = False
                    Throw New Exception("GCN is required")

                Else
                    Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_gcn.Value)
                    If checkGCN IsNot Nothing Then
                        Throw New ApplicationException("Konfirmasi : Untuk GCN " + txt_gcn.Value + " sudah memiliki GCNPrimary pada CIF " + checkGCN.CIF + " Jika di save, status GCNPrimary di CIF " + checkGCN.CIF + " akan dilepas dan akan dipasang di CIF ini")
                    End If
                End If
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update
    Protected Sub BtnSubmit_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim obj_customer_grid = New goAML_Grid_Customer
            Dim obj_customer2 = New goAML_Ref_Customer2

            If IsDataCustomerValid() Then

                With obj_customer_grid
                    .ObjList_GoAML_PreviousName = objListgoAML_Ref_Previous_Name
                    .ObjList_GoAML_Email = objListgoAML_Ref_Email
                    .ObjList_GoAML_EmploymentHistory = objListgoAML_Ref_EmploymentHistory
                    .ObjList_GoAML_PEP = objListgoAML_Ref_PersonPEP
                    .ObjList_GoAML_NetworkDevice = objListgoAML_Ref_NetworkDevice
                    .ObjList_GoAML_SocialMedia = objListgoAML_Ref_SocialMedia
                    .ObjList_GoAML_Sanction = objListgoAML_Ref_Sanction
                    .ObjList_GoAML_RelatedPerson = objListgoAML_Ref_RelatedPerson
                    .ObjList_GoAML_AdditionalInformation = objListgoAML_Ref_AdditionalInfo
                    .ObjList_GoAML_URL = objListgoAML_Ref_EntityUrl
                    .ObjList_GoAML_EntityIdentification = objListgoAML_Ref_EntityIdentification
                    .ObjList_GoAML_RelatedEntities = objListgoAML_Ref_RelatedEntities
                    .objList_GoAML_Ref_Director = objListgoAML_Ref_Director2
                End With

                With obj_customer
                    ''dedy added
                    'Dim IntResult As Integer = 0
                    'Dim StrQuery As String = ""
                    'StrQuery = "select GCN, count(1) from goAML_Ref_Customer where GCN = '" & txt_gcn.Value & "' GROUP BY GCN"
                    'Dim dtSet As Data.DataSet = SQLHelper.ExecuteDataSet(SQLHelper.strConnectionString, Data.CommandType.Text, StrQuery)
                    'If dtSet.Tables.Count > 0 Then
                    '    Dim dtTable As Data.DataTable = dtSet.Tables(0)
                    '    If dtTable.Rows.Count > 0 Then
                    '        'IntResult = dtTable.Rows(0).Item(0).ToString
                    '        '.isGCNPrimary = Convert.ToBoolean(IIf(cmbisGCNPrimary.Checked = True, 1, 1))
                    '        'GCNPrimary.Checked = True
                    '        GCNPrimary.Checked = True
                    '    End If
                    'End If
                    ''Dedy end added

                    'Dim yesBtn = New MessageBoxButtonConfig
                    'yesBtn.Handler = "NawadataDirect.Yes()"
                    'yesBtn.Text = "Ok"


                    'Dim listBtn = New MessageBoxButtonsConfig
                    'listBtn.Yes = yesBtn

                    'Ext.Net.X.MessageBox.Confirm("Test", "Are you sure?", listBtn).Show()

                    .CIF = txt_cif.Value
                    obj_customer2.CIF = .CIF
                    .GCN = txt_gcn.Value
                    If TextFieldOpeningDate.RawValue IsNot Nothing Then
                        .Opening_Date = Convert.ToDateTime(TextFieldOpeningDate.RawValue)
                    End If

                    If TextFieldClosingDate.RawValue IsNot Nothing Then
                        .Closing_Date = Convert.ToDateTime(TextFieldClosingDate.RawValue)
                    End If

                    .Comments = TextFieldComments.Text

                    .Opening_Branch_Code = cmb_openingBranch.SelectedItemValue
                    .Status = cmb_Status.SelectedItemValue
                    .isGCNPrimary = Convert.ToBoolean(IIf(GCNPrimary.Checked = True, 1, 0))
                    .Status_Code = Cmb_StatusCode.Value
                    .FK_Customer_Type_ID = Convert.ToInt64(Cmb_TypePerson.SelectedItemValue)
                    .isUpdateFromDataSource = Convert.ToBoolean(IIf(UpdateFromDataSource.Checked = True, 1, 0))

                    obj_customer2.FK_Customer_Type_ID = .FK_Customer_Type_ID

                    If Cmb_TypePerson.SelectedItemValue = 1 Then
                        .INDV_Title = txt_INDV_Title.Text
                        .INDV_Gender = cmb_INDV_Gender.SelectedItemValue
                        .INDV_Last_Name = txt_INDV_Last_Name.Text
                        If txt_INDV_Birthdate.RawValue IsNot Nothing Then
                            .INDV_BirthDate = Convert.ToDateTime(txt_INDV_Birthdate.RawValue)
                        Else
                            .INDV_BirthDate = Nothing
                        End If

                        .INDV_Birth_Place = txt_INDV_Birth_place.Text
                        .INDV_Mothers_Name = txt_INDV_Mothers_name.Text
                        .INDV_Alias = txt_INDV_Alias.Text
                        .INDV_SSN = txt_INDV_SSN.Text
                        .INDV_Passport_Number = txt_INDV_Passport_number.Text
                        'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
                        .INDV_Passport_Country = cmb_INDV_Passport_country.SelectedItemValue
                        'End Update
                        .INDV_ID_Number = txt_INDV_ID_Number.Text
                        .INDV_Nationality1 = cmb_INDV_Nationality1.SelectedItemValue
                        .INDV_Nationality2 = cmb_INDV_Nationality2.SelectedItemValue
                        .INDV_Nationality3 = cmb_INDV_Nationality3.SelectedItemValue
                        .INDV_Residence = cmb_INDV_Residence.SelectedItemValue
                        ' uncomment by Nael, goAML 5.2, 2023-10-11
                        'comment by septian, goAML 5.0.1, 2023-02-12
                        .INDV_Email = txt_INDV_Email.Text
                        .INDV_Email2 = txt_INDV_Email2.Text
                        .INDV_Email3 = txt_INDV_Email3.Text
                        .INDV_Email4 = txt_INDV_Email4.Text
                        .INDV_Email5 = txt_INDV_Email5.Text
                        'end comment
                        .INDV_Occupation = txt_INDV_Occupation.Text
                        .INDV_Employer_Name = txt_INDV_Employer_Name.Text
                        .INDV_Deceased = Convert.ToBoolean(IIf(cb_Deceased.Checked = True, 1, 0))
                        If cb_Deceased.Checked Then
                            .INDV_Deceased_Date = Convert.ToDateTime(txt_deceased_date.RawValue)
                        Else
                            .INDV_Deceased_Date = Nothing
                        End If
                        .INDV_Tax_Number = txt_INDV_Tax_Number.Text
                        .INDV_Tax_Reg_Number = Convert.ToBoolean(IIf(cb_tax.Checked = True, 1, 0))
                        .INDV_Source_of_Wealth = txt_INDV_Source_of_Wealth.Text
                        .INDV_Comments = txt_INDV_Comments.Text
                        '.Contry_Of_Birth = cmb_INDV_Country_Of_Birth.SelectedItemValue
                        '.Full_Name_Frn = txt_INDV_foreign_name.Value
                        '.Residence_Since = Convert.ToDateTime(txt_INDV_ResidenceSince.RawValue)
                        '.Is_Protected = Convert.ToBoolean(IIf(cb_IsProtected.Checked = True, 1, 0))

                        '' add by septian, go AML 5.0.1, 2023-1-30
                        'obj_customer2.Country_Of_Birth = cmb_INDV_Country_Of_Birth.SelectedItemValue
                        'obj_customer2.Full_Name_Frn = txt_INDV_Foreign_Name.Value
                        'obj_customer2.Residence_Since = Convert.ToDateTime(dte_INDV_ResidenceSince.RawValue)
                        obj_customer2.Is_Protected = Convert.ToBoolean(IIf(cb_IsProtected.Checked = True, 1, 0))
                        '' end add
                    Else
                        .Corp_Name = txt_Corp_Name.Text
                        '.Corp_Role = cmb_Corp_Role.Value
                        .Corp_Commercial_Name = txt_Corp_Commercial_Name.Text
                        .Corp_Incorporation_Legal_Form = cmb_Corp_Incorporation_Legal_Form.SelectedItemValue
                        .Corp_Incorporation_Number = txt_Corp_Incorporation_number.Text
                        .Corp_Business = txt_Corp_Business.Text
                        'comment by septian, goAML 5.0.1, 2023-02-12
                        'uncomment by Nael, goAML 5.2, 2023-10-11
                        .Corp_Email = txt_Corp_Email.Text
                        .Corp_Url = txt_Corp_url.Text
                        'end comment
                        .Corp_Incorporation_State = txt_Corp_incorporation_state.Text
                        .Corp_Incorporation_Country_Code = cmb_Corp_Incorporation_Country_Code.SelectedItemValue
                        If txt_Corp_incorporation_date.RawValue IsNot Nothing Then
                            .Corp_Incorporation_Date = Convert.ToDateTime(txt_Corp_incorporation_date.RawValue)
                        Else
                            .Corp_Incorporation_Date = Nothing
                        End If
                        .Corp_Business_Closed = cbTutup.Value
                        If cbTutup.Checked Then
                            .Corp_Date_Business_Closed = Convert.ToDateTime(txt_Corp_date_business_closed.RawValue)
                        Else
                            .Corp_Date_Business_Closed = Nothing
                        End If
                        .Corp_Tax_Number = txt_Corp_tax_number.Text
                        .Corp_Comments = txt_Corp_Comments.Text

                        '' add by septian, go AML 5.0.1, 2023-1-30
                        'obj_customer2.Entity_Status = cmb_Corp_EntityStatus.SelectedItemValue
                        'obj_customer2.Entity_Status_Date = Convert.ToDateTime(dte_Corp_EntityStatusDate.RawValue)
                        '' end add
                    End If

                    .Active = True
                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .CreatedDate = DateTime.Now
                    .LastUpdateDate = DateTime.Now
                End With
            End If

            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                objgoAML_CustomerBLL.SaveAddTanpaApproval(obj_customer, objListgoAML_Ref_Director, ObjModule, objListgoAML_Ref_Phone, objListgoAML_Ref_Identification, objListgoAML_Ref_Address, objListgoAML_Ref_Employer_Address, objListgoAML_Ref_Employer_Phone, objListgoAML_Ref_Director_Employer_Address, objListgoAML_Ref_Director_Employer_Phone, objListgoAML_Ref_Director_Address, objListgoAML_Ref_Director_Phone, objListgoAML_Ref_Identification_Director, obj_customer2, obj_customer_grid)

                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Saved into Database"
            Else
                objgoAML_CustomerBLL.SaveAddApproval(obj_customer, objListgoAML_Ref_Director, ObjModule, objListgoAML_Ref_Phone, objListgoAML_Ref_Identification, objListgoAML_Ref_Address, objListgoAML_Ref_Employer_Address, objListgoAML_Ref_Employer_Phone, objListgoAML_Ref_Director_Employer_Address, objListgoAML_Ref_Director_Employer_Phone, objListgoAML_Ref_Director_Address, objListgoAML_Ref_Director_Phone, objListgoAML_Ref_Identification_Director, obj_customer2, obj_customer_grid)
                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Saved into Pending Approval"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnBack_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub btnAddCustomerAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            cmb_kategoriaddress.IsReadOnly = False
            Address_INDV.ReadOnly = False
            Town_INDV.ReadOnly = False
            City_INDV.ReadOnly = False
            Zip_INDV.ReadOnly = False
            cmb_kodenegara.IsReadOnly = False
            State_INDVD.ReadOnly = False
            Comment_Address_INDVD.ReadOnly = False
            Btn_Save_Address.Hidden = False
            WindowDetailAddress.Title = "Customer Address Add"

            'Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Store_KategoriAddress.DataBind()

            'Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Kode_Negara.DataBind()


            ClearinputCustomerAddress()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddDirectorAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FP_Address_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            Director_cmb_kategoriaddress.IsReadOnly = False
            Director_Address.ReadOnly = False
            Director_Town.ReadOnly = False
            Director_City.ReadOnly = False
            Director_Zip.ReadOnly = False
            Director_cmb_kodenegara.IsReadOnly = False
            Director_State.ReadOnly = False
            Director_Comment_Address.ReadOnly = False
            Btn_Save_Director_Address.Hidden = False
            WindowDetailDirectorAddress.Title = "Director Address Add"

            'Director_Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Store_KategoriAddress.DataBind()

            'Director_Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Director_Store_Kode_Negara.DataBind()

            ClearinputDirectorAddress()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btnAddCustomerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            btnSavedetail.Hidden = False
            cb_phone_Contact_Type.IsReadOnly = False
            cb_phone_Communication_Type.IsReadOnly = False
            txt_phone_Country_prefix.ReadOnly = False
            txt_phone_number.ReadOnly = False
            txt_phone_extension.ReadOnly = False
            txt_phone_comments.ReadOnly = False
            btnSavedetail.Hidden = False
            WindowDetail.Title = "Customer Phone Add"

            Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing
            'Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Store_Contact_Type.DataBind()

            ' Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
            'Store_Communication_Type.DataBind()

            ClearinputCustomerPhones()
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddDirectorPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FormPanelDirectorPhone.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            btnSaveDirectorPhonedetail.Hidden = False
            Director_cb_phone_Contact_Type.IsReadOnly = False
            Director_cb_phone_Communication_Type.IsReadOnly = False
            Director_txt_phone_Country_prefix.ReadOnly = False
            Director_txt_phone_number.ReadOnly = False
            Director_txt_phone_extension.ReadOnly = False
            Director_txt_phone_comments.ReadOnly = False
            btnSaveDirectorPhonedetail.Hidden = False
            WindowDetailDirectorPhone.Title = "Director Phone Add"

            Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

            'Director_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Store_Contact_Type.DataBind()

            'Director_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
            'Director_Store_Communication_Type.DataBind()
            ClearinputDirectorPhones()
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()
            objTempCustomerPhoneEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelDirectorPhone.Hidden = True
            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
            ClearinputDirectorEmpPhones()
            ClearinputDirectorPhones()
            objTempDirectorPhoneEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_INDVD.Hidden = True
            WindowDetailAddress.Hidden = True
            ClearinputCustomerAddress()
            objTempCustomerAddressEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_Director.Hidden = True
            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
            ClearinputDirectorAddress()
            ClearinputDirectorEmpAddress()
            objTempCustomerAddressEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Identification_INDV.Hidden = True
            WindowDetailIdentification.Hidden = True
            ClearinputCustomerIdentification()
            objTempCustomerIdentificationEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Identification_Director.Hidden = True
            WindowDetailIdentification.Hidden = True
            ClearinputDirectorIdentification()
            objTempCustomerIdentificationEdit_Director = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddDirectorIdentifications_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FP_Identification_Director.Hidden = False
            WindowDetailIdentification.Hidden = False
            cmb_Jenis_Dokumen_Director.IsReadOnly = False
            txt_identification_comment_Director.ReadOnly = False
            txt_issued_by_Director.ReadOnly = False
            txt_issued_date_Director.ReadOnly = False
            txt_issued_expired_date_Director.ReadOnly = False
            cmb_issued_country_Director.IsReadOnly = False
            txt_number_identification_Director.ReadOnly = False
            btn_save_identification_Director.Hidden = False
            WindowDetailIdentification.Title = "Director Identification Add"

            'Storejenisdokumen_Director.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            'Storejenisdokumen_Director.DataBind()

            'StoreIssueCountry_Director.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'StoreIssueCountry_Director.DataBind()
            ClearinputDirectorIdentification()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomerAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomerAddressValid() Then
                If objTempCustomerAddressEdit Is Nothing Then
                    SaveAddAddressDetail()
                Else
                    SaveEditAddressDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    ' Pantau
    Protected Sub btnSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataDirectorAddressValid() Then
                If objTempDirectorAddressEdit Is Nothing Then
                    SaveAddDirectorAddressDetail()
                Else
                    SaveEditDirectorAddressDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomerEmpAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomerEmpAddressValid() Then
                If objTempEmployerAddressEdit Is Nothing Then
                    SaveAddEmpAddressDetail()
                Else
                    SaveEditEmpAddressDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveDirectorEmpAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataDirectorEmpAddressValid() Then
                If objTempDirectorEmployerAddressEdit Is Nothing Then
                    SaveAddDirectorEmpAddressDetail()
                Else
                    SaveEditDirectorEmpAddressDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btnSaveCustomerPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            If IsDataCustomerPhoneValid() Then
                If objTempCustomerPhoneEdit Is Nothing Then
                    SaveAddTaskDetail()
                Else
                    SaveEditTaskDetail()
                End If
            End If
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveDirectorPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            If IsDataDirectorPhoneValid() Then
                If objTempDirectorPhoneEdit Is Nothing Then
                    SaveAddDirectorTaskDetail()
                Else
                    SaveEditDirectorTaskDetail()
                End If
            End If
            FormPanelDirectorPhone.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
            ClearinputDirectorPhones()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btnSaveCustomerEmpPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            If IsDataCustomerEmpPhoneValid() Then
                If objTempEmployerPhonesEdit Is Nothing Then
                    SaveAddEmpTaskDetail()
                Else
                    SaveEditEmpTaskDetail()
                End If
            End If
            FormPanelEmpTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerEmpPhones()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveDirectorEmpPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataDirectorEmpPhoneValid() Then
                If objTempDirectorEmployerPhoneEdit Is Nothing Then
                    SaveAddDirectorEmpTaskDetail()
                Else
                    SaveEditDirectorEmpTaskDetail()
                End If
            End If
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SaveEditTaskDetail()

        With objTempCustomerPhoneEdit
            .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
            .tph_number = txt_phone_number.Text.Trim
            .tph_extension = txt_phone_extension.Text.Trim
            .comments = txt_phone_comments.Text.Trim
        End With

        With objTempgoAML_Ref_Phone
            .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
            .tph_number = txt_phone_number.Text.Trim
            .tph_extension = txt_phone_extension.Text.Trim
            .comments = txt_phone_comments.Text.Trim
        End With

        objTempCustomerPhoneEdit = Nothing
        objTempgoAML_Ref_Phone = Nothing

        If Cmb_TypePerson.SelectedItemValue = 1 Then
            Store_INDV_Phones.DataSource = objListgoAML_vw_Customer_Phones.ToList()
            Store_INDV_Phones.DataBind()
        Else
            Store_Phone_Corp.DataSource = objListgoAML_vw_Customer_Phones.ToList()
            Store_Phone_Corp.DataBind()
        End If

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerPhones()
    End Sub

    Sub SaveEditDirectorTaskDetail()

        With objTempDirectorPhoneEdit
            .Tph_Contact_Type = Director_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Director_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Director_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Director_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Director_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_txt_phone_number.Text.Trim
            .tph_extension = Director_txt_phone_extension.Text.Trim
            .comments = Director_txt_phone_comments.Text.Trim
        End With

        With objTempgoAML_Ref_Director_Phones
            .Tph_Contact_Type = Director_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Director_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Director_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_txt_phone_number.Text.Trim
            .tph_extension = Director_txt_phone_extension.Text.Trim
            .comments = Director_txt_phone_comments.Text.Trim
        End With

        objTempDirectorPhoneEdit = Nothing
        objTempgoAML_Ref_Director_Phones = Nothing

        Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
        Store_DirectorPhone.DataBind()

        FormPanelDirectorPhone.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorPhones()
    End Sub

    Sub SaveEditEmpTaskDetail()

        With objTempEmployerPhonesEdit
            .Tph_Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Emp_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Emp_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Emp_txt_phone_number.Text.Trim
            .tph_extension = Emp_txt_phone_extension.Text.Trim
            .comments = Emp_txt_phone_comments.Text.Trim
        End With

        With objTempgoAML_Ref_EmployerPhones
            .Tph_Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Emp_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Emp_txt_phone_number.Text.Trim
            .tph_extension = Emp_txt_phone_extension.Text.Trim
            .comments = Emp_txt_phone_comments.Text.Trim
        End With

        objTempEmployerPhonesEdit = Nothing
        objTempgoAML_Ref_EmployerPhones = Nothing

        Store_Empoloyer_Phone.DataSource = objListgoAML_vw_Employer_Phones.ToList()
        Store_Empoloyer_Phone.DataBind()

        FormPanelEmpTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerEmpPhones()
    End Sub

    Sub SaveEditDirectorEmpTaskDetail()

        With objTempDirectorEmployerPhoneEdit
            .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_Emp_txt_phone_number.Text.Trim
            .tph_extension = Director_Emp_txt_phone_extension.Text.Trim
            .comments = Director_Emp_txt_phone_comments.Text.Trim
        End With

        With objTempgoAML_Ref_Director_EmployerPhones
            .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_Emp_txt_phone_number.Text.Trim
            .tph_extension = Director_Emp_txt_phone_extension.Text.Trim
            .comments = Director_Emp_txt_phone_comments.Text.Trim
        End With

        objTempDirectorEmployerPhoneEdit = Nothing
        objTempgoAML_Ref_Director_EmployerPhones = Nothing

        Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
        Store_Director_Employer_Phone.DataBind()

        FormPanelEmpDirectorTaskDetail.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorEmpPhones()
    End Sub

    Sub SaveEditAddressDetail()

        With objTempCustomerAddressEdit
            .Address_Type = cmb_kategoriaddress.SelectedItemValue
            .Type_Address_Detail = cmb_kategoriaddress.SelectedItemText
            .Address = Address_INDV.Value
            .City = City_INDV.Value
            .Town = Town_INDV.Value
            .Country_Code = cmb_kodenegara.SelectedItemValue
            .Country = cmb_kodenegara.SelectedItemText
            .State = State_INDVD.Value
            .Zip = Zip_INDV.Value
            .Comments = Comment_Address_INDVD.Text.Trim
        End With

        With objTempgoAML_Ref_Address
            .Address_Type = cmb_kategoriaddress.SelectedItemValue
            .City = City_INDV.Value
            .Address = Address_INDV.Value
            .Town = Town_INDV.Value
            .Country_Code = cmb_kodenegara.SelectedItemValue
            .State = State_INDVD.Value
            .Zip = Zip_INDV.Value
            .Comments = Comment_Address_INDVD.Text.Trim
        End With

        objTempCustomerAddressEdit = Nothing
        objTempgoAML_Ref_Address = Nothing

        If Cmb_TypePerson.SelectedItemValue = 1 Then
            Store_INDV_Addresses.DataSource = objListgoAML_vw_CUSTOMER_ADDresses.ToList()
            Store_INDV_Addresses.DataBind()
        Else
            Store_Address_Corp.DataSource = objListgoAML_vw_CUSTOMER_ADDresses.ToList()
            Store_Address_Corp.DataBind()
        End If

        FP_Address_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerAddress()
    End Sub

    Sub SaveEditDirectorAddressDetail()

        With objTempDirectorAddressEdit
            .Address_Type = Director_cmb_kategoriaddress.SelectedItemValue
            .Type_Address_Detail = Director_cmb_kategoriaddress.SelectedItemText
            .Address = Director_Address.Value
            .City = Director_City.Value
            .Town = Director_Town.Value
            .Country = Director_cmb_kodenegara.SelectedItemText
            .Country_Code = Director_cmb_kodenegara.SelectedItemValue
            .State = Director_State.Value
            .Zip = Director_Zip.Value
            .Comments = Director_Comment_Address.Text.Trim
        End With

        With objTempgoAML_Ref_Director_Addresses
            .Address_Type = Director_cmb_kategoriaddress.SelectedItemValue '
            .City = Director_City.Value
            .Address = Director_Address.Value
            .Town = Director_Town.Value
            .Country_Code = Director_cmb_kodenegara.SelectedItemValue
            .State = Director_State.Value
            .Zip = Director_Zip.Value
            .Comments = Director_Comment_Address.Text.Trim
        End With

        objTempDirectorAddressEdit = Nothing
        objTempgoAML_Ref_Director_Addresses = Nothing

        Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList()
        Store_DirectorAddress.DataBind()

        FP_Address_Director.Hidden = True
        WindowDetailDirectorAddress.Hidden = True
        ClearinputDirectorAddress()
    End Sub


    Sub SaveEditEmpAddressDetail()

        With objTempEmployerAddressEdit
            .Address_Type = cmb_emp_kategoriaddress.SelectedItemValue
            .Type_Address_Detail = cmb_emp_kategoriaddress.SelectedItemText
            .Address = emp_Address_INDV.Value
            .City = emp_City_INDV.Value
            .Town = emp_Town_INDV.Value
            .Country_Code = cmb_emp_kodenegara.SelectedItemValue
            .Comments = emp_Comment_Address_INDVD.Value
            .Country = cmb_emp_kodenegara.SelectedItemText
            .State = emp_State_INDVD.Value
            .Zip = emp_Zip_INDV.Value
            .Comments = emp_Comment_Address_INDVD.Text.Trim
        End With

        With objTempgoAML_Ref_EmployerAddresses
            .Address_Type = cmb_emp_kategoriaddress.SelectedItemValue '
            .City = emp_City_INDV.Value
            .Address = emp_Address_INDV.Value
            .Town = emp_Town_INDV.Value
            .Country_Code = cmb_emp_kodenegara.SelectedItemValue
            .Comments = emp_Comment_Address_INDVD.Value
            .State = emp_State_INDVD.Value
            .Zip = emp_Zip_INDV.Value
            .Comments = emp_Comment_Address_INDVD.Text.Trim
        End With

        objTempEmployerAddressEdit = Nothing
        objTempgoAML_Ref_EmployerAddresses = Nothing

        Store_Employer_Address.DataSource = objListgoAML_vw_Employer_Addresses.ToList()
        Store_Employer_Address.DataBind()

        FP_Address_Emp_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerEmpAddress()
    End Sub

    Sub SaveEditDirectorEmpAddressDetail()

        With objTempDirectorEmployerAddressEdit
            .Address_Type = Director_cmb_emp_kategoriaddress.SelectedItemValue
            .Type_Address_Detail = Director_cmb_emp_kategoriaddress.SelectedItemText
            .Address = Director_emp_Address.Value
            .City = Director_emp_City.Value
            .Town = Director_emp_Town.Value
            .Country_Code = Director_cmb_emp_kodenegara.SelectedItemValue
            .Country = Director_cmb_emp_kodenegara.SelectedItemText
            .State = Director_emp_State.Value
            .Zip = Director_emp_Zip.Value
            .Comments = Director_emp_Comment_Address.Text.Trim
        End With

        With objTempgoAML_Ref_Director_EmployerAddresses
            .Address_Type = Director_cmb_emp_kategoriaddress.SelectedItemValue '
            .City = Director_emp_City.Value
            .Address = Director_emp_Address.Value
            .Town = Director_emp_Town.Value
            .Country_Code = Director_cmb_emp_kodenegara.SelectedItemValue
            .State = Director_emp_State.Value
            .Zip = Director_emp_Zip.Value
            .Comments = Director_emp_Comment_Address.Text.Trim
        End With

        objTempDirectorEmployerAddressEdit = Nothing
        objTempgoAML_Ref_Director_EmployerAddresses = Nothing

        Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList()
        Store_Director_Employer_Address.DataBind()

        FP_Address_Emp_Director.Hidden = True
        WindowDetailDirectorAddress.Hidden = True
        ClearinputDirectorEmpAddress()
    End Sub


    Sub SaveAddTaskDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Customer_Phones
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Phone
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Phone = intpk

            .FK_Ref_Detail_Of = 1
            .FK_for_Table_ID = obj_customer.PK_Customer_ID
            .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
            .tph_number = txt_phone_number.Text.Trim
            .tph_extension = txt_phone_extension.Text.Trim
            .comments = txt_phone_comments.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_goAML_Ref_Phone = intpk
            .FK_Ref_Detail_Of = 1
            .FK_for_Table_ID = obj_customer.PK_Customer_ID
            .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
            .tph_number = txt_phone_number.Text.Trim
            .tph_extension = txt_phone_extension.Text.Trim
            .comments = txt_phone_comments.Text.Trim
        End With

        objListgoAML_vw_Customer_Phones.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Phone.Add(objNewgoAML_Customer)

        If Cmb_TypePerson.SelectedItemValue = 1 Then
            Store_INDV_Phones.DataSource = objListgoAML_vw_Customer_Phones.ToList()
            Store_INDV_Phones.DataBind()
        Else
            Store_Phone_Corp.DataSource = objListgoAML_vw_Customer_Phones.ToList()
            Store_Phone_Corp.DataBind()
        End If

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerPhones()
    End Sub

    Sub SaveAddDirectorTaskDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Director_Phones
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Phone
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Phone = intpk

            .FK_Ref_Detail_Of = 6
            .FK_for_Table_ID = tempflag
            .Tph_Contact_Type = Director_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Director_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Director_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Director_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Director_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_txt_phone_number.Text.Trim
            .tph_extension = Director_txt_phone_extension.Text.Trim
            .comments = Director_txt_phone_comments.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_goAML_Ref_Phone = intpk
            .FK_Ref_Detail_Of = 6
            .FK_for_Table_ID = tempflag
            .Tph_Contact_Type = Director_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Director_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Director_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_txt_phone_number.Text.Trim
            .tph_extension = Director_txt_phone_extension.Text.Trim
            .comments = Director_txt_phone_comments.Text.Trim
        End With

        objListgoAML_vw_Director_Phones.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Director_Phone.Add(objNewgoAML_Customer)


        Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
        Store_DirectorPhone.DataBind()

        FormPanelDirectorPhone.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorPhones()
    End Sub


    Sub SaveAddEmpTaskDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Employer_Phones
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Phone
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Phone = intpk

            .FK_Ref_Detail_Of = 5
            .FK_for_Table_ID = obj_customer.PK_Customer_ID
            .Tph_Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Emp_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Emp_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Emp_txt_phone_number.Text.Trim
            .tph_extension = Emp_txt_phone_extension.Text.Trim
            .comments = Emp_txt_phone_comments.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_goAML_Ref_Phone = intpk
            .FK_Ref_Detail_Of = 5
            .FK_for_Table_ID = obj_customer.PK_Customer_ID
            .Tph_Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Emp_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Emp_txt_phone_number.Text.Trim
            .tph_extension = Emp_txt_phone_extension.Text.Trim
            .comments = Emp_txt_phone_comments.Text.Trim
        End With

        objListgoAML_vw_Employer_Phones.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Employer_Phone.Add(objNewgoAML_Customer)

        Store_Empoloyer_Phone.DataSource = objListgoAML_vw_Employer_Phones.ToList()
        Store_Empoloyer_Phone.DataBind()

        FormPanelEmpTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerEmpPhones()
    End Sub

    Sub SaveAddDirectorEmpTaskDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Director_Employer_Phones
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Phone
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Phone = intpk

            .FK_Ref_Detail_Of = 7
            .FK_for_Table_ID = tempflag
            .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_Emp_txt_phone_number.Text.Trim
            .tph_extension = Director_Emp_txt_phone_extension.Text.Trim
            .comments = Director_Emp_txt_phone_comments.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_goAML_Ref_Phone = intpk
            .FK_Ref_Detail_Of = 7
            .FK_for_Table_ID = tempflag
            .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_Emp_txt_phone_number.Text.Trim
            .tph_extension = Director_Emp_txt_phone_extension.Text.Trim
            .comments = Director_Emp_txt_phone_comments.Text.Trim
        End With

        objListgoAML_vw_Director_Employer_Phones.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Director_Employer_Phone.Add(objNewgoAML_Customer)

        Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
        Store_Director_Employer_Phone.DataBind()

        FormPanelEmpDirectorTaskDetail.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorEmpPhones()
    End Sub

    Sub SaveAddAddressDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Customer_Addresses
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Address
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 1
            .Address_Type = cmb_kategoriaddress.SelectedItemValue
            .Address = Address_INDV.Value
            .City = City_INDV.Value
            .Town = Town_INDV.Value
            .Country_Code = cmb_kodenegara.SelectedItemValue
            .Comments = Comment_Address_INDVD.Value
            .State = State_INDVD.Value
            .Zip = Zip_INDV.Value
            .Type_Address_Detail = cmb_kategoriaddress.SelectedItemText
            .Country = cmb_kodenegara.SelectedItemText
            .Comments = Comment_Address_INDVD.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 1
            .FK_To_Table_ID = obj_customer.PK_Customer_ID
            .Address_Type = cmb_kategoriaddress.SelectedItemValue
            .City = City_INDV.Value
            .Address = Address_INDV.Value
            .Town = Town_INDV.Value
            .Country_Code = cmb_kodenegara.SelectedItemValue
            .Comments = Comment_Address_INDVD.Value
            .State = State_INDVD.Value
            .Zip = Zip_INDV.Value
            .Comments = Comment_Address_INDVD.Text.Trim
        End With

        objListgoAML_vw_CUSTOMER_ADDresses.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Address.Add(objNewgoAML_Customer)

        If Cmb_TypePerson.SelectedItemValue = 1 Then
            Store_INDV_Addresses.DataSource = objListgoAML_vw_CUSTOMER_ADDresses.ToList()
            Store_INDV_Addresses.DataBind()
        Else
            Store_Address_Corp.DataSource = objListgoAML_vw_CUSTOMER_ADDresses.ToList()
            Store_Address_Corp.DataBind()
        End If

        FP_Address_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerAddress()
    End Sub

    Sub SaveAddDirectorAddressDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Director_Addresses
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Address
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 6
            .FK_To_Table_ID = tempflag
            .Address_Type = Director_cmb_kategoriaddress.SelectedItemValue
            .Address = Director_Address.Value
            .City = Director_City.Value
            .Town = Director_Town.Value
            .Country_Code = Director_cmb_kodenegara.SelectedItemValue
            .State = Director_State.Value
            .Zip = Director_Zip.Value
            .Type_Address_Detail = Director_cmb_kategoriaddress.SelectedItemText
            .Country = Director_cmb_kodenegara.SelectedItemText
            .Comments = Director_Comment_Address.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 6
            .FK_To_Table_ID = tempflag
            .Address_Type = Director_cmb_kategoriaddress.SelectedItemValue '
            .City = Director_City.Value
            .Address = Director_Address.Value
            .Town = Director_Town.Value
            .Country_Code = Director_cmb_kodenegara.SelectedItemValue
            .State = Director_State.Value
            .Zip = Director_Zip.Value
            .Comments = Director_Comment_Address.Text.Trim
        End With

        objListgoAML_vw_Director_Addresses.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Director_Address.Add(objNewgoAML_Customer)

        Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList()
        Store_DirectorAddress.DataBind()

        FP_Address_Director.Hidden = True
        WindowDetailDirectorAddress.Hidden = True
        ClearinputDirectorAddress()
    End Sub

    Sub SaveAddEmpAddressDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Employer_Addresses
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Address
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 5
            .Address_Type = cmb_emp_kategoriaddress.SelectedItemValue
            .Address = emp_Address_INDV.Value
            .City = emp_City_INDV.Value
            .Town = emp_Town_INDV.Value
            .Country_Code = cmb_emp_kodenegara.SelectedItemValue
            .Comments = emp_Comment_Address_INDVD.Value
            .State = emp_State_INDVD.Value
            .Zip = emp_Zip_INDV.Value
            .Type_Address_Detail = cmb_emp_kategoriaddress.SelectedItemText
            .Country = cmb_emp_kodenegara.SelectedItemText
            .Comments = emp_Comment_Address_INDVD.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 5
            .FK_To_Table_ID = obj_customer.PK_Customer_ID
            .Address_Type = cmb_emp_kategoriaddress.SelectedItemValue '
            .City = emp_City_INDV.Value
            .Address = emp_Address_INDV.Value
            .Town = emp_Town_INDV.Value
            .Country_Code = cmb_emp_kodenegara.SelectedItemValue
            .Comments = emp_Comment_Address_INDVD.Value
            .State = emp_State_INDVD.Value
            .Zip = emp_Zip_INDV.Value
            .Comments = emp_Comment_Address_INDVD.Text.Trim
        End With

        objListgoAML_vw_Employer_Addresses.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Employer_Address.Add(objNewgoAML_Customer)

        Store_Employer_Address.DataSource = objListgoAML_vw_Employer_Addresses.ToList()
        Store_Employer_Address.DataBind()

        FP_Address_Emp_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerEmpAddress()
    End Sub

    Sub SaveAddDirectorEmpAddressDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Director_Employer_Addresses
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Address
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Director_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 7
            .FK_To_Table_ID = tempflag
            .Address_Type = Director_cmb_emp_kategoriaddress.SelectedItemValue
            .Address = Director_emp_Address.Value
            .City = Director_emp_City.Value
            .Town = Director_emp_Town.Value
            .Country_Code = Director_cmb_emp_kodenegara.SelectedItemValue
            .State = Director_emp_State.Value
            .Zip = Director_emp_Zip.Value
            .Type_Address_Detail = Director_cmb_emp_kategoriaddress.SelectedItemText
            .Country = Director_cmb_emp_kodenegara.SelectedItemText
            .Comments = Director_emp_Comment_Address.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 7
            .FK_To_Table_ID = tempflag
            .Address_Type = Director_cmb_emp_kategoriaddress.SelectedItemValue '
            .City = Director_emp_City.Value
            .Address = Director_emp_Address.Value
            .Town = Director_emp_Town.Value
            .Country_Code = Director_cmb_emp_kodenegara.SelectedItemValue
            .State = Director_emp_State.Value
            .Zip = Director_emp_Zip.Value
            .Comments = Director_emp_Comment_Address.Text.Trim
        End With

        objListgoAML_vw_Director_Employer_Addresses.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Director_Employer_Address.Add(objNewgoAML_Customer)

        Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList()
        Store_Director_Employer_Address.DataBind()

        FP_Address_Emp_Director.Hidden = True
        WindowDetailDirectorAddress.Hidden = True
        ClearinputDirectorEmpAddress()
    End Sub

    Function IsDataCustomerPhoneValid() As Boolean
        If cb_phone_Contact_Type.SelectedItemValue = "" Then
            Throw New Exception("Contact Type is required")
        End If
        If cb_phone_Communication_Type.SelectedItemValue = "" Then
            Throw New Exception("Communication Type is required")
        End If
        If txt_phone_Country_prefix.Text.Length > 3 Then
            Throw New Exception("Max Country Prefix is 3")
        End If
        If txt_phone_extension.Text.Length > 9 Then
            Throw New Exception("Max Phone Extension is 9")
        End If
        If txt_phone_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(txt_phone_number.Text) Then
            Throw New Exception("Phone Number is Invalid")
        End If
        'End Update
        Return True
    End Function
    'Update: Zikri_08102020
    Private Function IsNumber(text As String) As Boolean
        Try

            Dim number As New Regex("^[0-9 ]+$")
            Return number.IsMatch(text)

        Catch ex As Exception
            Return False
        End Try
    End Function
    'End Update
    Function IsDataDirectorPhoneValid() As Boolean
        If Director_cb_phone_Contact_Type.SelectedItemValue = "" Then
            Throw New Exception("Contact Type is required")
        End If
        If Director_cb_phone_Communication_Type.SelectedItemValue = "" Then
            Throw New Exception("Communication Type is required")
        End If
        If Director_txt_phone_Country_prefix.Text.Length > 3 Then
            Throw New Exception("Max Country Prefix is 3")
        End If
        If Director_txt_phone_extension.Text.Length > 9 Then
            Throw New Exception("Max Phone Extension is 9")
        End If
        If Director_txt_phone_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(Director_txt_phone_number.Text) Then
            Throw New Exception("Phone Number is Invalid")
        End If
        'End Update
        Return True
    End Function

    Function IsDataCustomerEmpPhoneValid() As Boolean
        If Emp_cb_phone_Contact_Type.SelectedItemValue = "" Then
            Throw New Exception("Contact Type is required")
        End If
        If Emp_cb_phone_Communication_Type.SelectedItemValue = "" Then
            Throw New Exception("Communication Type is required")
        End If
        If Emp_txt_phone_Country_prefix.Text.Length > 3 Then
            Throw New Exception("Max Country Prefix is 3")
        End If
        If Emp_txt_phone_extension.Text.Length > 9 Then
            Throw New Exception("Max Phone Extension is 9")
        End If
        If Emp_txt_phone_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(Emp_txt_phone_number.Text) Then
            Throw New Exception("Phone Number is Invalid")
        End If
        'End Update
        Return True
    End Function

    Function IsDataDirectorEmpPhoneValid() As Boolean
        If Director_Emp_cb_phone_Contact_Type.SelectedItemValue = "" Then
            Throw New Exception("Contact Type is required")
        End If
        If Director_Emp_cb_phone_Communication_Type.SelectedItemValue = "" Then
            Throw New Exception("Communication Type is required")
        End If
        If Director_Emp_txt_phone_Country_prefix.Text.Length > 3 Then
            Throw New Exception("Max Country Prefix is 3")
        End If
        If Director_Emp_txt_phone_extension.Text.Length > 9 Then
            Throw New Exception("Max Phone Extension is 9")
        End If
        If Director_Emp_txt_phone_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(Director_Emp_txt_phone_number.Text) Then
            Throw New Exception("Phone Number is Invalid")
        End If
        'End Update
        Return True
    End Function

    Function IsDataCustomerAddressValid() As Boolean
        If cmb_kodenegara.SelectedItemValue = "" Then
            Throw New Exception("Country Code is required")
        End If
        If cmb_kategoriaddress.SelectedItemValue = "" Then
            Throw New Exception("Address Type is required")
        End If
        If Address_INDV.Text.Trim = "" Then
            Throw New Exception("Address is required")
        End If
        If City_INDV.Text.Trim = "" Then
            Throw New Exception("City is required")
        End If
        Return True
    End Function

    Function IsDataDirectorAddressValid() As Boolean
        If Director_cmb_kodenegara.SelectedItemValue = "" Then
            Throw New Exception("Country Code is required")
        End If
        If Director_cmb_kategoriaddress.SelectedItemValue = "" Then
            Throw New Exception("Address Type is required")
        End If
        If Director_Address.Text.Trim = "" Then
            Throw New Exception("Address is required")
        End If
        If Director_City.Text.Trim = "" Then
            Throw New Exception("City is required")
        End If
        Return True
    End Function

    Function IsDataCustomerEmpAddressValid() As Boolean
        If cmb_emp_kodenegara.SelectedItemValue = "" Then
            Throw New Exception("Country Code is required")
        End If
        If cmb_emp_kategoriaddress.SelectedItemValue = "" Then
            Throw New Exception("Address Type is required")
        End If
        If emp_Address_INDV.Text.Trim = "" Then
            Throw New Exception("Address is required")
        End If
        If emp_City_INDV.Text.Trim = "" Then
            Throw New Exception("City is required")
        End If
        Return True
    End Function

    Function IsDataDirectorEmpAddressValid() As Boolean
        If Director_cmb_emp_kodenegara.SelectedItemValue = "" Then
            Throw New Exception("Country Code is required")
        End If
        If Director_cmb_emp_kategoriaddress.SelectedItemValue = "" Then
            Throw New Exception("Address Type is required")
        End If
        If Director_emp_Address.Text.Trim = "" Then
            Throw New Exception("Address is required")
        End If
        If Director_emp_City.Text.Trim = "" Then
            Throw New Exception("City is required")
        End If
        Return True
    End Function

    Sub ClearinputCustomerPhones()
        cb_phone_Contact_Type.SetTextValue("")
        cb_phone_Communication_Type.SetTextValue("")
        txt_phone_Country_prefix.Text = ""
        txt_phone_number.Text = ""
        txt_phone_extension.Text = ""
        txt_phone_comments.Text = ""

    End Sub

    Sub ClearinputDirectorPhones()
        Director_cb_phone_Contact_Type.SetTextValue("")
        Director_cb_phone_Communication_Type.SetTextValue("")
        Director_txt_phone_Country_prefix.Text = ""
        Director_txt_phone_number.Text = ""
        Director_txt_phone_extension.Text = ""
        Director_txt_phone_comments.Text = ""

    End Sub

    Sub ClearinputCustomerEmpPhones()
        Emp_cb_phone_Contact_Type.SetTextValue("")
        Emp_cb_phone_Communication_Type.SetTextValue("")
        Emp_txt_phone_Country_prefix.Text = ""
        Emp_txt_phone_number.Text = ""
        Emp_txt_phone_extension.Text = ""
        Emp_txt_phone_comments.Text = ""
    End Sub


    Sub ClearinputDirectorEmpPhones()
        Director_Emp_cb_phone_Contact_Type.SetTextValue("")
        Director_Emp_cb_phone_Communication_Type.SetTextValue("")
        Director_Emp_txt_phone_Country_prefix.Text = ""
        Director_Emp_txt_phone_number.Text = ""
        Director_Emp_txt_phone_extension.Text = ""
        Director_Emp_txt_phone_comments.Text = ""

    End Sub

    Sub ClearinputCustomerAddress()
        cmb_kategoriaddress.SetTextValue("")
        cmb_kodenegara.SetTextValue("")
        Address_INDV.Text = ""
        Town_INDV.Text = ""
        City_INDV.Text = ""
        Zip_INDV.Text = ""
        State_INDVD.Text = ""
        Comment_Address_INDVD.Text = ""

    End Sub

    Sub ClearinputDirectorAddress()
        Director_cmb_kategoriaddress.SetTextValue("")
        Director_cmb_kodenegara.SetTextValue("")
        Director_Address.Text = ""
        Director_Town.Text = ""
        Director_City.Text = ""
        Director_Zip.Text = ""
        Director_State.Text = ""
        Director_Comment_Address.Text = ""

    End Sub

    Sub ClearinputCustomerEmpAddress()
        cmb_emp_kategoriaddress.SetTextValue("")
        cmb_emp_kodenegara.SetTextValue("")
        emp_Address_INDV.Text = ""
        emp_Town_INDV.Text = ""
        emp_City_INDV.Text = ""
        emp_Zip_INDV.Text = ""
        emp_State_INDVD.Text = ""
        emp_Comment_Address_INDVD.Text = ""

    End Sub

    Sub ClearinputDirectorEmpAddress()
        Director_cmb_emp_kategoriaddress.SetTextValue("")
        Director_cmb_emp_kodenegara.SetTextValue("")
        Director_emp_Address.Text = ""
        Director_emp_Town.Text = ""
        Director_emp_City.Text = ""
        Director_emp_Zip.Text = ""
        Director_emp_State.Text = ""
        Director_emp_Comment_Address.Text = ""

    End Sub

    Sub GridPanelSetting()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 2 Then
            GP_DirectorIdentification.ColumnModel.Columns.RemoveAt(GP_DirectorIdentification.ColumnModel.Columns.Count - 1)
            GP_DirectorIdentification.ColumnModel.Columns.Insert(1, CommandColumn13)

            GP_DirectorPhone.ColumnModel.Columns.RemoveAt(GP_DirectorPhone.ColumnModel.Columns.Count - 1)
            GP_DirectorPhone.ColumnModel.Columns.Insert(1, CommandColumn11)
            GP_DirectorAddress.ColumnModel.Columns.RemoveAt(GP_DirectorAddress.ColumnModel.Columns.Count - 1)
            GP_DirectorAddress.ColumnModel.Columns.Insert(1, CommandColumn12)
            GP_DirectorEmpPhone.ColumnModel.Columns.RemoveAt(GP_DirectorEmpPhone.ColumnModel.Columns.Count - 1)
            GP_DirectorEmpPhone.ColumnModel.Columns.Insert(1, CommandColumn9)
            GP_DirectorEmpAddress.ColumnModel.Columns.RemoveAt(GP_DirectorEmpAddress.ColumnModel.Columns.Count - 1)
            GP_DirectorEmpAddress.ColumnModel.Columns.Insert(1, CommandColumn10)
            gp_INDV_Phones.ColumnModel.Columns.RemoveAt(gp_INDV_Phones.ColumnModel.Columns.Count - 1)
            gp_INDV_Phones.ColumnModel.Columns.Insert(1, CommandColumn1)
            gp_INDV_Address.ColumnModel.Columns.RemoveAt(gp_INDV_Address.ColumnModel.Columns.Count - 1)
            gp_INDV_Address.ColumnModel.Columns.Insert(1, CommandColumn2)
            GP_INDVD_Identification.ColumnModel.Columns.RemoveAt(GP_INDVD_Identification.ColumnModel.Columns.Count - 1)
            GP_INDVD_Identification.ColumnModel.Columns.Insert(1, CommandColumn5)
            GP_Empoloyer_Phone.ColumnModel.Columns.RemoveAt(GP_Empoloyer_Phone.ColumnModel.Columns.Count - 1)
            GP_Empoloyer_Phone.ColumnModel.Columns.Insert(1, CommandColumn6)
            GP_Employer_Address.ColumnModel.Columns.RemoveAt(GP_Employer_Address.ColumnModel.Columns.Count - 1)
            GP_Employer_Address.ColumnModel.Columns.Insert(1, CommandColumn7)
            GP_Phone_Corp.ColumnModel.Columns.RemoveAt(GP_Phone_Corp.ColumnModel.Columns.Count - 1)
            GP_Phone_Corp.ColumnModel.Columns.Insert(1, CommandColumn3)
            GP_Address_Corp.ColumnModel.Columns.RemoveAt(GP_Address_Corp.ColumnModel.Columns.Count - 1)
            GP_Address_Corp.ColumnModel.Columns.Insert(1, CommandColumn4)
            GP_Corp_Director.ColumnModel.Columns.RemoveAt(GP_Corp_Director.ColumnModel.Columns.Count - 1)
            GP_Corp_Director.ColumnModel.Columns.Insert(1, CommandColumn8)

            'Change CRUD Button position to left column, Septian, 4 Jan 23
            GPCustomer_INDV_PreviousName.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_PreviousName.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_PreviousName.ColumnModel.Columns.Insert(1, CC_INDV_PreviousName)
            GPCustomer_INDV_Email.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_Email.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_Email.ColumnModel.Columns.Insert(1, CC_INDV_Email)
            GPCustomer_INDV_EmploymentHistory.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_EmploymentHistory.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_EmploymentHistory.ColumnModel.Columns.Insert(1, CC_INDV_EmploymentHistory)
            GPCustomer_INDV_PEP.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_PEP.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_PEP.ColumnModel.Columns.Insert(1, CC_INDV_PEP)
            GPCustomer_INDV_NetworkDevice.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_NetworkDevice.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_NetworkDevice.ColumnModel.Columns.Insert(1, CC_INDV_NetworkDevice)
            GPCustomer_INDV_SocialMedia.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_SocialMedia.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_SocialMedia.ColumnModel.Columns.Insert(1, CC_INDV_SocialMedia)
            GPCustomer_INDV_Sanction.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_Sanction.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_Sanction.ColumnModel.Columns.Insert(1, CC_INDV_Sanction)
            GPCustomer_INDV_RelatedPerson.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_RelatedPerson.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_RelatedPerson.ColumnModel.Columns.Insert(1, CC_INDV_RelatedPerson)
            GPCustomer_INDV_AdditionalInformation.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_AdditionalInformation.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_AdditionalInformation.ColumnModel.Columns.Insert(1, CC_INDV_AdditionalInformation)
            GPCustomer_CORP_Email.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_Email.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_Email.ColumnModel.Columns.Insert(1, CC_CORP_Email)
            GPCustomer_CORP_URL.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_URL.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_URL.ColumnModel.Columns.Insert(1, CC_CORP_URL)
            GPCustomer_CORP_EntityIdentification.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_EntityIdentification.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_EntityIdentification.ColumnModel.Columns.Insert(1, CC_CORP_EntityIdentification)
            GPCustomer_CORP_NetworkDevice.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_NetworkDevice.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_NetworkDevice.ColumnModel.Columns.Insert(1, CC_CORP_NetworkDevice)
            GPCustomer_CORP_Sanction.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_Sanction.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_Sanction.ColumnModel.Columns.Insert(1, CC_CORP_Sanction)
            'GPCustomer_CORP_RelatedPerson.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_RelatedPerson.ColumnModel.Columns.Count - 1)
            'GPCustomer_CORP_RelatedPerson.ColumnModel.Columns.Insert(1, CC_CORP_RelatedPerson)
            GPCustomer_CORP_RelatedEntities.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_RelatedEntities.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_RelatedEntities.ColumnModel.Columns.Insert(1, CC_CORP_RelatedEntities)
            GPCustomer_CORP_AdditionalInformation.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_AdditionalInformation.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_AdditionalInformation.ColumnModel.Columns.Insert(1, CC_CORP_AdditionalInformation)
            'end add

        End If

    End Sub

    Sub Maximizeable()
        WindowDetailDirector.Maximizable = True
        WindowDetail.Maximizable = True
        WindowDetailAddress.Maximizable = True
        WindowDetailIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True

        'START: 2023-10-14, Nael
        WindowDetail_RelatedEntities.Maximizable = True
        WindowDetail_EntityIdentification.Maximizable = True
        WindowDetail_RelatedPerson.Maximizable = True
        WindowDetail_Person_PEP.Maximizable = True
        WindowDetail_Sanction.Maximizable = True
        WindowDetail_Email.Maximizable = True
        'END: 2023-10-14, Nael

    End Sub

    Private Sub LoadGender()
        'cmb_INDV_Gender.PageSize = SystemParameterBLL.GetPageSize
        ' StoreINDVGender.Reload()
        'cmb_Director_Gender.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Gender.Reload()
    End Sub

    Protected Sub Gender_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Kelamin", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub LoadKontak()
        'cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        'Store_Contact_Type.Reload()

        'Emp_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        'Emp_Store_Contact_Type.Reload()

        'Director_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_Contact_Type.Reload()

        'Director_Emp_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        'Director_Emp_Store_Contact_Type.Reload()


    End Sub

    Private Sub LoadJenisAlatKomunikasi()
        'Director_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_Communication_Type.Reload()
        'Director_Emp_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        'Director_Emp_Store_Communication_Type.Reload()
        'cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        'Store_Communication_Type.Reload()
        'Emp_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        'Emp_Store_Communication_Type.Reload()
    End Sub

    Private Sub LoadKategoriKontak()
        'cmb_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        'Store_KategoriAddress.Reload()
        'cmb_emp_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        'Store_emp_KategoriAddress.Reload()
        'Director_cmb_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_KategoriAddress.Reload()
        'Director_cmb_emp_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_emp_KategoriAddress.Reload()
    End Sub

    Protected Sub Communication_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Alat_Komunikasi", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub Contact_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Kategori_Kontak", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub LoadNegara()

        'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
        'cmb_INDV_Passport_country.PageSize = SystemParameterBLL.GetPageSize
        'Store_INDV_PassportCountry.Reload()
        'cmb_Director_Passport_Country.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_PassportCountry.Reload()
        'End Update
        'cmb_Director_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Nationality1.Reload()
        'cmb_INDV_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'StoreNationality1.Reload()
        'cmb_Director_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Nationality2.Reload()
        'cmb_INDV_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        ' StoreNationality2.Reload()
        'cmb_Director_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Nationality3.Reload()
        'cmb_INDV_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'StoreNationality3.Reload()
        'cmb_Director_Residence.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Residence.Reload()
        'cmb_INDV_Residence.PageSize = SystemParameterBLL.GetPageSize
        ' StoreResidence.Reload()
        ' Director_cmb_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_Kode_Negara.Reload()
        'Director_cmb_emp_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_emp_Kode_Negara.Reload()

        'cmb_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'Store_Kode_Negara.Reload()
        'cmb_emp_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'Store_emp_Kode_Negara.Reload()

        'cmb_issued_country_Director.PageSize = SystemParameterBLL.GetPageSize
        'StoreIssueCountry_Director.Reload()
        'cmb_issued_country.PageSize = SystemParameterBLL.GetPageSize
        'Store_issued_country.Reload()


    End Sub

    Private Sub LoadTypeIdentitas()
        'cmb_Jenis_Dokumen.PageSize = SystemParameterBLL.GetPageSize
        'Store_jenis_dokumen.Reload()

        'cmb_Jenis_Dokumen_Director.PageSize = SystemParameterBLL.GetPageSize
        'Storejenisdokumen_Director.Reload()
    End Sub


    Protected Sub Identification_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goaml_ref_jenis_dokumen_identitas", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub National_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Nama_Negara", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub CUSTOMER_ADD_v501_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                'Dedy added 31082020 aktif is hidden
                Cmb_StatusCode.Hidden = True
                LoadNegara()
                LoadGender()
                LoadKontak()
                LoadJenisAlatKomunikasi()
                LoadKategoriKontak()
                LoadTypeIdentitas()

                'Dedy end added 31082020
                ClearSession()
                'GridEmail_RelatedPerson.Un = "RelatedPerson"
                InitGrid()
                GridPanelSetting()
                Maximizeable()
                'StoreTypePerson.DataSource = goAML_CustomerBLL.GetListCustomerType
                'StoreTypePerson.DataBind()

                StoreStatusCode.DataSource = goAML_CustomerBLL.GetListStatusRekening
                StoreStatusCode.DataBind()
                Dim obj_statuscode = goAML_CustomerBLL.GetStatusRekeningbyID("AKT")
                If obj_statuscode IsNot Nothing Then
                    Cmb_StatusCode.SetValue(obj_statuscode.Kode)
                End If



            End If




        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub GrdCmdCustPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailCustPhone(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditCustPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailCustPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailDirectorPhone(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditDirectorPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailDirectorPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataAddressDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditCustAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailCustAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorAddressDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataDirectorEditAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailDirectorAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Sub LoadDataAddressDetail(id As Long)

        Dim objCustomerPhonesDetail = objListgoAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        If Not objCustomerPhonesDetail Is Nothing Then
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            Btn_Save_Address.Hidden = True
            WindowDetailAddress.Title = "Address Detail"
            cmb_kategoriaddress.IsReadOnly = True
            Address_INDV.ReadOnly = True
            Town_INDV.ReadOnly = True
            City_INDV.ReadOnly = True
            Zip_INDV.ReadOnly = True
            cmb_kodenegara.IsReadOnly = True
            State_INDVD.ReadOnly = True
            Comment_Address_INDVD.ReadOnly = True
            ClearinputCustomerAddress()
            'Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            ' Store_KategoriAddress.DataBind()
            Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(objCustomerPhonesDetail.Address_Type)
            If kategorikontak IsNot Nothing Then
                cmb_kategoriaddress.SetTextWithTextValue(kategorikontak.Kode, kategorikontak.Keterangan)
            End If
            Address_INDV.Text = objCustomerPhonesDetail.Address
            Town_INDV.Text = objCustomerPhonesDetail.Town
            City_INDV.Text = objCustomerPhonesDetail.City
            Zip_INDV.Text = objCustomerPhonesDetail.Zip
            'Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Kode_Negara.DataBind()
            Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerPhonesDetail.Country_Code)
            If kodenegara IsNot Nothing Then
                cmb_kodenegara.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
            End If
            State_INDVD.Text = objCustomerPhonesDetail.State
            Comment_Address_INDVD.Text = objCustomerPhonesDetail.Comments
        End If
    End Sub

    Sub LoadDataDirectorAddressDetail(id As Long)

        Dim objCustomerPhonesDetail = objListgoAML_Ref_Director_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        If Not objCustomerPhonesDetail Is Nothing Then
            FP_Address_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            Btn_Save_Director_Address.Hidden = True
            WindowDetailDirectorAddress.Title = "Address Detail"
            Director_cmb_kategoriaddress.IsReadOnly = True
            Director_Address.ReadOnly = True
            Director_Town.ReadOnly = True
            Director_City.ReadOnly = True
            Director_Zip.ReadOnly = True
            Director_cmb_kodenegara.IsReadOnly = True
            Director_State.ReadOnly = True
            Director_Comment_Address.ReadOnly = True
            ClearinputDirectorAddress()
            'Director_Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Store_KategoriAddress.DataBind()
            Dim kategorikontak_director_detail = goAML_CustomerBLL.GetContactTypeByID(objCustomerPhonesDetail.Address_Type)
            If kategorikontak_director_detail IsNot Nothing Then
                Director_cmb_kategoriaddress.SetTextWithTextValue(kategorikontak_director_detail.Kode, kategorikontak_director_detail.Keterangan)
            End If
            Director_Address.Text = objCustomerPhonesDetail.Address
            Director_Town.Text = objCustomerPhonesDetail.Town
            Director_City.Text = objCustomerPhonesDetail.City
            Director_Zip.Text = objCustomerPhonesDetail.Zip
            'Director_Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Director_Store_Kode_Negara.DataBind()
            Dim kodenegara_director_detail = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerPhonesDetail.Country_Code)
            If kodenegara_director_detail IsNot Nothing Then
                Director_cmb_kodenegara.SetTextWithTextValue(kodenegara_director_detail.Kode, kodenegara_director_detail.Keterangan)
            End If

            Director_State.Text = objCustomerPhonesDetail.State
            Director_Comment_Address.Text = objCustomerPhonesDetail.Comments
        End If
    End Sub

    Sub LoadDataEditCustAddress(id As Long)
        objTempgoAML_Ref_Address = objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        objTempCustomerAddressEdit = objListgoAML_vw_CUSTOMER_ADDresses.Find(Function(x) x.PK_Customer_Address_ID = id)

        Dim Address_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Country As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing

        If Not objTempCustomerAddressEdit Is Nothing Then
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            Btn_Save_Address.Hidden = False

            btnSavedetail.Hidden = False
            cmb_kategoriaddress.IsReadOnly = False
            Address_INDV.ReadOnly = False
            Town_INDV.ReadOnly = False
            City_INDV.ReadOnly = False
            Zip_INDV.ReadOnly = False
            cmb_kodenegara.IsReadOnly = False
            State_INDVD.ReadOnly = False
            Comment_Address_INDVD.ReadOnly = False
            WindowDetailAddress.Title = "Address Edit"
            ClearinputCustomerAddress()
            With objTempCustomerAddressEdit
                'Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_KategoriAddress.DataBind()
                Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                If kategorikontak IsNot Nothing Then
                    cmb_kategoriaddress.SetTextWithTextValue(kategorikontak.Kode, kategorikontak.Keterangan)
                End If

                Address_INDV.Text = .Address
                Town_INDV.Text = .Town
                City_INDV.Text = .City
                Zip_INDV.Text = .Zip
                'Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Store_Kode_Negara.DataBind()
                Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(.Country_Code)
                If kodenegara IsNot Nothing Then
                    cmb_kodenegara.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
                End If

                State_INDVD.Text = .State
                Comment_Address_INDVD.Text = .Comments
            End With

            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataDirectorEditAddress(id As Long)
        objTempgoAML_Ref_Director_Addresses = objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        objTempDirectorAddressEdit = objListgoAML_vw_Director_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)

        Dim Address_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Country As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing

        If Not objTempDirectorAddressEdit Is Nothing Then
            FP_Address_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            Btn_Save_Director_Address.Hidden = False
            Director_cmb_kategoriaddress.IsReadOnly = False
            Director_Address.ReadOnly = False
            Director_Town.ReadOnly = False
            Director_City.ReadOnly = False
            Director_Zip.ReadOnly = False
            Director_cmb_kodenegara.IsReadOnly = False
            Director_State.ReadOnly = False
            Director_Comment_Address.ReadOnly = False
            WindowDetailDirectorAddress.Title = "Address Edit"
            ClearinputDirectorAddress()
            With objTempDirectorAddressEdit
                'Director_Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_KategoriAddress.DataBind()
                Dim kategorikontak_director_edit = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                If kategorikontak_director_edit IsNot Nothing Then
                    Director_cmb_kategoriaddress.SetTextWithTextValue(kategorikontak_director_edit.Kode, kategorikontak_director_edit.Keterangan)
                End If

                Director_Address.Text = .Address
                Director_Town.Text = .Town
                Director_City.Text = .City
                Director_Zip.Text = .Zip
                'Director_Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Director_Store_Kode_Negara.DataBind()
                Dim kodenegara_director_edit = goAML_CustomerBLL.GetNamaNegaraByID(.Country_Code)
                If kodenegara_director_edit IsNot Nothing Then
                    Director_cmb_kodenegara.SetTextWithTextValue(kodenegara_director_edit.Kode, kodenegara_director_edit.Keterangan)
                End If

                Director_State.Text = .State
                Director_Comment_Address.Text = .Comments
            End With
        End If
    End Sub

    Sub DeleteRecordTaskDetailCustAddress(id As Long)
        Dim objDelVWAddress As GoAMLDAL.Vw_Customer_Addresses = objListgoAML_vw_CUSTOMER_ADDresses.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objxDelAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = id)

        If Not objxDelAddress Is Nothing Then
            objListgoAML_Ref_Address.Remove(objxDelAddress)
        End If

        If Not objDelVWAddress Is Nothing Then
            objListgoAML_vw_CUSTOMER_ADDresses.Remove(objDelVWAddress)

            If Cmb_TypePerson.SelectedItemValue = 1 Then
                Store_INDV_Addresses.DataSource = objListgoAML_vw_CUSTOMER_ADDresses.ToList()
                Store_INDV_Addresses.DataBind()
            Else
                Store_Address_Corp.DataSource = objListgoAML_vw_CUSTOMER_ADDresses.ToList()
                Store_Address_Corp.DataBind()
            End If
        End If

        FP_Address_INDVD.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerAddress()

    End Sub

    Sub DeleteRecordTaskDetailDirectorAddress(id As Long)
        Dim objDelVWAddress As GoAMLDAL.Vw_Director_Addresses = objListgoAML_vw_Director_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objxDelAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID = id)

        If Not objxDelAddress Is Nothing Then
            objListgoAML_Ref_Director_Address.Remove(objxDelAddress)
        End If

        If Not objDelVWAddress Is Nothing Then
            objListgoAML_vw_Director_Addresses.Remove(objDelVWAddress)

            Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = objTempCustomerDirectorEdit.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 6).ToList()
            Store_DirectorAddress.DataBind()
        End If

        FP_Address_INDVD.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerAddress()

    End Sub

    Sub LoadDataDetailCustPhone(id As Long)
        Dim objCustomerPhonesDetail As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            cb_phone_Contact_Type.IsReadOnly = True
            cb_phone_Communication_Type.IsReadOnly = True
            txt_phone_Country_prefix.ReadOnly = True
            txt_phone_number.ReadOnly = True
            txt_phone_extension.ReadOnly = True
            txt_phone_comments.ReadOnly = True
            btnSavedetail.Hidden = True

            WindowDetail.Title = "Phone Detail"
            ClearinputCustomerPhones()
            With objCustomerPhonesDetail
                'Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_Contact_Type.DataBind()
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_contact_type IsNot Nothing Then
                    cb_phone_Contact_Type.SetTextWithTextValue(obj_contact_type.Kode, obj_contact_type.Keterangan)
                End If
                'cb_phone_Contact_Type.SetValue(.Tph_Contact_Type,)

                'Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Store_Communication_Type.DataBind()
                Dim obj_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_communication_type IsNot Nothing Then
                    cb_phone_Communication_Type.SetTextWithTextValue(obj_communication_type.Kode, obj_communication_type.Keterangan)
                End If
                'cb_phone_Communication_Type.SetTextValue(.Tph_Communication_Type)
                'End Update
                txt_phone_Country_prefix.Text = .tph_country_prefix
                txt_phone_number.Text = .tph_number
                txt_phone_extension.Text = .tph_extension
                txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataDetailDirectorPhone(id As Long)
        Dim objCustomerPhonesDetail As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelDirectorPhone.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            Director_cb_phone_Contact_Type.IsReadOnly = True
            Director_cb_phone_Communication_Type.IsReadOnly = True
            Director_txt_phone_Country_prefix.ReadOnly = True
            Director_txt_phone_number.ReadOnly = True
            Director_txt_phone_extension.ReadOnly = True
            Director_txt_phone_comments.ReadOnly = True
            btnSaveDirectorPhonedetail.Hidden = True

            WindowDetailDirectorPhone.Title = "Phone Detail"
            ClearinputDirectorPhones()
            With objCustomerPhonesDetail
                'Director_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_Contact_Type.DataBind()
                'Director_cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_director_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_director_contact_type IsNot Nothing Then
                    Director_cb_phone_Contact_Type.SetTextWithTextValue(obj_director_contact_type.Kode, obj_director_contact_type.Keterangan)
                End If
                'Dim obj_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                'If obj_communication_type IsNot Nothing Then
                '    cb_phone_Communication_Type.SetTextWithTextValue(obj_communication_type.Kode, obj_communication_type.Keterangan)
                'End If

                'Director_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Store_Communication_Type.DataBind()
                'Director_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                Dim obj_director_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_director_communication_type IsNot Nothing Then
                    Director_cb_phone_Communication_Type.SetTextWithTextValue(obj_director_communication_type.Kode, obj_director_communication_type.Keterangan)
                End If
                'End Update
                Director_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_txt_phone_number.Text = .tph_number
                Director_txt_phone_extension.Text = .tph_extension
                Director_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub

    Sub LoadDataDetailEmpDirectorPhone(id As Long)
        Dim objCustomerPhonesDetail As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelEmpDirectorTaskDetail.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            Director_Emp_cb_phone_Contact_Type.IsReadOnly = True
            Director_Emp_cb_phone_Communication_Type.IsReadOnly = True
            Director_Emp_txt_phone_Country_prefix.ReadOnly = True
            Director_Emp_txt_phone_number.ReadOnly = True
            Director_Emp_txt_phone_extension.ReadOnly = True
            Director_Emp_txt_phone_comments.ReadOnly = True
            btn_saveDirector_empphone.Hidden = True

            WindowDetailDirectorPhone.Title = "Phone Detail"
            ClearinputDirectorEmpPhones()
            With objCustomerPhonesDetail
                'Director_Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Emp_Store_Contact_Type.DataBind()
                'Director_Emp_cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_director_emp_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_director_emp_contact_type IsNot Nothing Then
                    Director_Emp_cb_phone_Contact_Type.SetTextWithTextValue(obj_director_emp_contact_type.Kode, obj_director_emp_contact_type.Keterangan)
                End If
                'Director_Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Emp_Store_Communication_Type.DataBind()
                'Director_Emp_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                Dim obj_director_emp_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_director_emp_communication_type IsNot Nothing Then
                    Director_Emp_cb_phone_Communication_Type.SetTextWithTextValue(obj_director_emp_communication_type.Kode, obj_director_emp_communication_type.Keterangan)
                End If
                'End Update
                Director_Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_Emp_txt_phone_number.Text = .tph_number
                Director_Emp_txt_phone_extension.Text = .tph_extension
                Director_Emp_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub


    Sub LoadDataEditCustPhone(id As Long)
        objTempgoAML_Ref_Phone = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        objTempCustomerPhoneEdit = objListgoAML_vw_Customer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objTempCustomerPhoneEdit Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            cb_phone_Contact_Type.IsReadOnly = False
            cb_phone_Communication_Type.IsReadOnly = False
            txt_phone_Country_prefix.ReadOnly = False
            txt_phone_number.ReadOnly = False
            txt_phone_extension.ReadOnly = False
            txt_phone_comments.ReadOnly = False
            btnSavedetail.Hidden = False

            WindowDetail.Title = "Phone Edit"
            ClearinputCustomerPhones()
            With objTempCustomerPhoneEdit
                'Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_Contact_Type.DataBind()
                'cb_phone_Contact_Type.SetTextValue(objTempCustomerPhoneEdit.Tph_Contact_Type)
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_contact_type IsNot Nothing Then
                    cb_phone_Contact_Type.SetTextWithTextValue(obj_contact_type.Kode, obj_contact_type.Keterangan)
                End If
                'cb_phone_Contact_Type.SetValue(.Tph_Contact_Type,)

                'Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Store_Communication_Type.DataBind()
                Dim obj_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_communication_type IsNot Nothing Then
                    cb_phone_Communication_Type.SetTextWithTextValue(obj_communication_type.Kode, obj_communication_type.Keterangan)
                End If
                'cb_phone_Communication_Type.SetTextValue(.Tph_Communication_Type)
                'End Update
                'Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Store_Communication_Type.DataBind()
                'cb_phone_Communication_Type.SetTextValue(objTempCustomerPhoneEdit.Tph_Communication_Type)

                txt_phone_Country_prefix.Text = .tph_country_prefix
                txt_phone_number.Text = .tph_number
                txt_phone_extension.Text = .tph_extension
                txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataEditDirectorPhone(id As Long)
        objTempgoAML_Ref_Director_Phones = objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        objTempDirectorPhoneEdit = objListgoAML_vw_Director_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objTempDirectorPhoneEdit Is Nothing Then
            FormPanelDirectorPhone.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            Director_cb_phone_Contact_Type.IsReadOnly = False
            Director_cb_phone_Communication_Type.IsReadOnly = False
            Director_txt_phone_Country_prefix.ReadOnly = False
            Director_txt_phone_number.ReadOnly = False
            Director_txt_phone_extension.ReadOnly = False
            Director_txt_phone_comments.ReadOnly = False
            btnSaveDirectorPhonedetail.Hidden = False

            WindowDetailDirectorPhone.Title = "Phone Edit"
            ClearinputDirectorPhones()
            With objTempDirectorPhoneEdit
                'Director_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_Contact_Type.DataBind()
                'Director_cb_phone_Contact_Type.SetValue(objTempDirectorPhoneEdit.Tph_Contact_Type)
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_director_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_director_contact_type IsNot Nothing Then
                    Director_cb_phone_Contact_Type.SetTextWithTextValue(obj_director_contact_type.Kode, obj_director_contact_type.Keterangan)
                End If
                Dim obj_director_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_director_communication_type IsNot Nothing Then
                    Director_cb_phone_Communication_Type.SetTextWithTextValue(obj_director_communication_type.Kode, obj_director_communication_type.Keterangan)
                End If
                'End Update

                'Director_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Store_Communication_Type.DataBind()
                'Director_cb_phone_Communication_Type.SetValue(objTempDirectorPhoneEdit.Tph_Communication_Type)

                Director_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_txt_phone_number.Text = .tph_number
                Director_txt_phone_extension.Text = .tph_extension
                Director_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub

    Sub LoadDataEditEmpDirectorPhone(id As Long)
        objTempgoAML_Ref_Director_EmployerPhones = objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        objTempDirectorEmployerPhoneEdit = objListgoAML_vw_Director_Employer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objTempDirectorEmployerPhoneEdit Is Nothing Then
            FormPanelEmpDirectorTaskDetail.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            Director_Emp_cb_phone_Contact_Type.IsReadOnly = False
            Director_Emp_cb_phone_Communication_Type.IsReadOnly = False
            Director_Emp_txt_phone_Country_prefix.ReadOnly = False
            Director_Emp_txt_phone_number.ReadOnly = False
            Director_Emp_txt_phone_extension.ReadOnly = False
            Director_Emp_txt_phone_comments.ReadOnly = False
            btn_saveDirector_empphone.Hidden = False

            WindowDetailDirectorPhone.Title = "Phone Edit"
            ClearinputDirectorEmpPhones()
            With objTempDirectorEmployerPhoneEdit
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_director_emp_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_director_emp_contact_type IsNot Nothing Then
                    Director_Emp_cb_phone_Contact_Type.SetTextWithTextValue(obj_director_emp_contact_type.Kode, obj_director_emp_contact_type.Keterangan)
                End If
                Dim obj_director_emp_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_director_emp_communication_type IsNot Nothing Then
                    Director_Emp_cb_phone_Communication_Type.SetTextWithTextValue(obj_director_emp_communication_type.Kode, obj_director_emp_communication_type.Keterangan)
                End If
                'End Update
                'Director_Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Emp_Store_Contact_Type.DataBind()
                'Director_Emp_cb_phone_Contact_Type.SetValue(objTempDirectorEmployerPhoneEdit.Tph_Contact_Type)

                'Director_Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Emp_Store_Communication_Type.DataBind()
                'Director_Emp_cb_phone_Communication_Type.SetValue(objTempDirectorEmployerPhoneEdit.Tph_Communication_Type)

                Director_Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_Emp_txt_phone_number.Text = .tph_number
                Director_Emp_txt_phone_extension.Text = .tph_extension
                Director_Emp_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub

    Sub DeleteRecordTaskDetailEmpDirectorPhone(id As Long)
        Dim objDelVWPhone As GoAMLDAL.Vw_Director_Employer_Phones = objListgoAML_vw_Director_Employer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objxDelPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        If Not objxDelPhone Is Nothing Then
            objListgoAML_Ref_Director_Employer_Phone.Remove(objxDelPhone)
        End If

        If Not objDelVWPhone Is Nothing Then
            objListgoAML_vw_Director_Employer_Phones.Remove(objDelVWPhone)

            Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = objTempCustomerDirectorEdit.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 7).ToList()
            Store_Director_Employer_Phone.DataBind()
        End If

        FormPanelEmpDirectorTaskDetail.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorEmpPhones()
    End Sub

    Sub DeleteRecordTaskDetailDirectorPhone(id As Long)
        Dim objDelVWPhone As GoAMLDAL.Vw_Director_Phones = objListgoAML_vw_Director_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objxDelPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        If Not objxDelPhone Is Nothing Then
            objListgoAML_Ref_Director_Phone.Remove(objxDelPhone)
        End If

        If Not objDelVWPhone Is Nothing Then
            objListgoAML_vw_Director_Phones.Remove(objDelVWPhone)

            Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = objTempCustomerDirectorEdit.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 6).ToList()
            Store_DirectorPhone.DataBind()
        End If

        FormPanelDirectorPhone.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorPhones()
    End Sub

    Sub DeleteRecordTaskDetailCustPhone(id As Long)
        Dim objDelVWPhone As GoAMLDAL.Vw_Customer_Phones = objListgoAML_vw_Customer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objxDelPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        If Not objxDelPhone Is Nothing Then
            objListgoAML_Ref_Phone.Remove(objxDelPhone)
        End If

        If Not objDelVWPhone Is Nothing Then
            objListgoAML_vw_Customer_Phones.Remove(objDelVWPhone)

            If Cmb_TypePerson.SelectedItemValue = 1 Then
                Store_INDV_Phones.DataSource = objListgoAML_vw_Customer_Phones.ToList()
                Store_INDV_Phones.DataBind()
            Else
                Store_Phone_Corp.DataSource = objListgoAML_vw_Customer_Phones.ToList()
                Store_Phone_Corp.DataBind()
            End If
        End If

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerPhones()

    End Sub

    Protected Sub btnBackSaveDetail_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = True
            FP_Address_INDVD.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub CUSTOMER_EDIT_Default_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    'Protected Sub Cmb_TypePerson_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles Cmb_TypePerson.DirectSelect
    Protected Sub Cmb_TypePerson_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            txt_cif.Hidden = False
            txt_gcn.Hidden = False
            'GCNPrimary.Hidden = False
            Dim pk As String = Cmb_TypePerson.SelectedItemValue
            If pk = 1 Then
                panel_INDV.Hidden = False
                panel_Corp.Hidden = True
                'StoreINDVGender.DataSource = goAML_CustomerBLL.GetListJenisKelamin()
                'StoreINDVGender.DataBind()
                'StoreNationality1.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'StoreNationality1.DataBind()
                'StoreNationality2.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'StoreNationality2.DataBind()
                'StoreNationality3.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                ' StoreNationality3.DataBind()
                'StoreResidence.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'StoreResidence.DataBind()

                ' ======= 2023-09-26, Nael: Hide txt_deceased_date, karena hidenya tereset ketika cmb_typeperson berubah, jadi dihide lagi
                txt_deceased_date.Hidden = True
                ' ======================================================

                ClearinputCustomerCorp()
                ClearinputCustomerINDVD()

            Else
                panel_INDV.Hidden = True
                panel_Corp.Hidden = False
                'Store_Corp_Incorporation_Country_Code.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Store_Corp_Incorporation_Country_Code.DataBind()
                'Store_Corp_Incorporation_Legal_Form.DataSource = goAML_CustomerBLL.GetListBentukBadanUsaha()
                'Store_Corp_Incorporation_Legal_Form.DataBind()
                'Store_Corp_Role.DataSource = goAML_CustomerBLL.GetListPartyRole()
                'Store_Corp_Role.DataBind()
                ClearinputCustomerCorp()
                ClearinputCustomerINDVD()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub checkBoxIsClosed_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbTutup.DirectCheck
        Try
            If cbTutup.Checked Then
                txt_Corp_date_business_closed.Hidden = False
            Else
                txt_Corp_date_business_closed.Hidden = True
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub checkBoxcb_Deceased_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_Deceased.DirectCheck
        Try
            If cb_Deceased.Checked Then
                txt_deceased_date.Hidden = False
            Else
                txt_deceased_date.Hidden = True
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub checkBoxcb_Director_Deceased_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_Director_Deceased.DirectCheck
        Try
            If cb_Director_Deceased.Checked Then
                txt_Director_Deceased_Date.Hidden = False
            Else
                txt_Director_Deceased_Date.Hidden = True
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    ' 2023-09-26, Nael: Menambahkan fungsi untuk directEvent check box related person deceased status
    Protected Sub checkBoxcb_RP_Deceased_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbx_rp_deceased.DirectCheck
        Try
            If cbx_rp_deceased.Checked Then
                df_rp_date_deceased.Hidden = False
            Else
                df_rp_date_deceased.Hidden = True
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Protected Sub checkBoxcb_tax_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_tax.DirectCheck
    '    Try
    '        If cb_tax.Checked Then
    '            txt_INDV_Tax_Number.Hidden = False
    '        Else
    '            txt_INDV_Tax_Number.Hidden = True
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub checkBoxcb_Director_tax_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_Director_Tax_Reg_Number.DirectCheck
    '    Try
    '        If cb_Director_Tax_Reg_Number.Checked Then
    '            txt_Director_Tax_Number.Hidden = False
    '        Else
    '            txt_Director_Tax_Number.Hidden = True
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Sub ClearinputCustomerINDVD()
        txt_cif.Text = ""
        txt_gcn.Text = ""
        txt_INDV_Title.Text = ""
        txt_INDV_Last_Name.Text = ""
        txt_INDV_SSN.Text = ""
        txt_INDV_Birthdate.RawValue = Nothing
        txt_INDV_Birth_place.Text = ""
        txt_INDV_Mothers_name.Text = ""
        txt_INDV_Alias.Text = ""
        txt_INDV_Passport_number.Text = ""
        'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
        cmb_INDV_Passport_country.SetTextValue("")
        'End Update
        txt_INDV_ID_Number.Text = ""
        'Update: Zikri_11092020 Change Dropdown to NDSDropdown
        cmb_INDV_Nationality1.SetTextValue("")
        cmb_INDV_Nationality2.SetTextValue("")
        cmb_INDV_Nationality3.SetTextValue("")
        cmb_INDV_Gender.SetTextValue("")
        cmb_INDV_Residence.SetTextValue("")
        'End Update
        'comment by septian, goAML 5.0.1, 2023-02-12
        txt_INDV_Email.Text = ""
        txt_INDV_Email2.Text = ""
        txt_INDV_Email3.Text = ""
        txt_INDV_Email4.Text = ""
        txt_INDV_Email5.Text = ""
        'end comment
        txt_INDV_Occupation.Text = ""
        txt_INDV_Employer_Name.Text = ""
        cb_Deceased.Checked = False
        txt_deceased_date.Value = ""
        txt_INDV_Tax_Number.Text = ""
        cb_tax.Checked = False
        txt_INDV_Source_of_Wealth.Text = ""
        txt_INDV_Comments.Text = ""
        Store_INDV_Addresses.DataBind()
        Store_INDV_Phones.DataBind()
        objListgoAML_Ref_Phone = Nothing
        objListgoAML_Ref_Address = Nothing
        objListgoAML_Ref_Identification = Nothing
        objListgoAML_Ref_Employer_Address = Nothing
        objListgoAML_Ref_Employer_Phone = Nothing
        objListgoAML_Ref_Email = New List(Of DataModel.goAML_Ref_Customer_Email)

        ' 2023-10-11, Nael
        Store_Customer_Email.DataSource = New List(Of DataModel.goAML_Ref_Customer_Email)

        ClearSession()
    End Sub

    Sub ClearinputCustomerCorp()
        txt_cif.Text = ""
        txt_gcn.Text = ""
        txt_Corp_Name.Value = ""
        txt_Corp_Commercial_Name.Value = ""
        cmb_Corp_Incorporation_Legal_Form.SetTextValue("")
        txt_Corp_Incorporation_number.Value = ""
        txt_Corp_Business.Value = ""
        'comment by septian, goAML 5.0.1, 2023-02-12
        txt_Corp_Email.Value = "" ' 2023-10-11, Nael: Munculin lagi
        ''cmb_director.Value = ""
        txt_Corp_url.Value = "" ' 2023-10-11, Nael: Munculin lagi
        'end comment
        txt_Corp_incorporation_state.Value = ""
        cmb_Corp_Incorporation_Country_Code.SetTextValue("")
        txt_Corp_incorporation_date.Value = ""
        txt_Corp_date_business_closed.Value = ""
        txt_Corp_tax_number.Value = ""
        cbTutup.Checked = False
        txt_Corp_Comments.Value = ""
        Store_Address_Corp.DataBind()
        Store_Phone_Corp.DataBind()
        objListgoAML_Ref_Phone = Nothing
        objListgoAML_Ref_Address = Nothing
        objListgoAML_Ref_Director_Phone = Nothing
        objListgoAML_Ref_Director_Address = Nothing
        objListgoAML_Ref_Director_Employer_Phone = Nothing
        objListgoAML_Ref_Director_Employer_Address = Nothing
        objListgoAML_Ref_Director = Nothing
        objListgoAML_Ref_Email = New List(Of DataModel.goAML_Ref_Customer_Email)

        ' 2023-10-11, Nael
        Store_corp_email.DataSource = New List(Of DataModel.goAML_Ref_Customer_Email)

        ClearSession()
    End Sub

    Protected Sub btnAddCustomerIdentifications_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FP_Identification_INDV.Hidden = False
            WindowDetailIdentification.Hidden = False
            cmb_Jenis_Dokumen.IsReadOnly = False
            txt_identification_comment.ReadOnly = False
            txt_issued_by.ReadOnly = False
            txt_issued_date.ReadOnly = False
            txt_issued_expired_date.ReadOnly = False
            cmb_issued_country.IsReadOnly = False
            txt_number_identification.ReadOnly = False
            btn_save_identification.Hidden = False
            WindowDetailIdentification.Title = "Customer Identification Add"

            'Store_jenis_dokumen.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            'Store_jenis_dokumen.DataBind()

            'Store_issued_country.DataSource = goAML_CustomerBLL.GetListNamaNegara
            'Store_issued_country.DataBind()

            ClearinputCustomerIdentification()
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomerIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomerIdentificationValid() Then
                'If objTempCustomerPhoneEdit Is Nothing Then
                If objTempCustomerIdentificationEdit Is Nothing Then
                    SaveAddIdentificationDetail()
                Else
                    SaveEditIdentificationDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveDirectorIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataDirectorIdentificationValid() Then
                'If objTempCustomerPhoneEdit Is Nothing Then
                If objTempCustomerIdentificationEdit_Director Is Nothing Then
                    SaveAddDirectorIdentificationDetail()
                Else
                    SaveEditDirectorIdentificationDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ClearinputCustomerIdentification()
        cmb_Jenis_Dokumen.SetTextValue("")
        txt_identification_comment.Text = ""
        txt_issued_by.Text = ""
        txt_issued_date.Text = ""
        txt_issued_expired_date.Text = ""
        cmb_issued_country.SetTextValue("")
        txt_number_identification.Text = ""
    End Sub

    Function IsDataCustomerIdentificationValid() As Boolean
        If cmb_issued_country.SelectedItemValue = "" Then
            Throw New Exception("Country is required")
        End If
        If cmb_Jenis_Dokumen.SelectedItemValue = "" Then
            Throw New Exception("Identification Type is required")
        End If
        If txt_number_identification.Text.Trim = "" Then
            Throw New Exception("Number Identification is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(txt_number_identification.Text) Then
            Throw New Exception("Identification Number is Invalid")
        End If
        'End Update
        Return True
    End Function

    Function IsDataDirectorIdentificationValid() As Boolean
        If cmb_Jenis_Dokumen_Director.SelectedItemValue = "" Then
            Throw New Exception("Identification Type is required")
        End If
        If txt_number_identification_Director.Text.Trim = "" Then
            Throw New Exception("Number Identification is required")
        End If
        If cmb_issued_country_Director.SelectedItemValue = "" Then
            Throw New Exception("Country is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(txt_number_identification_Director.Text) Then
            Throw New Exception("Identification Number is Invalid")
        End If
        'End Update
        Return True
    End Function

    Sub SaveAddIdentificationDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Person_Identification
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Person_Identification
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Person_Identification_ID = intpk
            .isReportingPerson = 0
            .FK_Person_Type = 1
            .FK_Person_ID = obj_customer.PK_Customer_ID
            .Number = txt_number_identification.Value
            .Issued_By = txt_issued_by.Value
            If txt_issued_date.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment.Value
            .Country = cmb_issued_country.SelectedItemText
            .Type_Document = cmb_Jenis_Dokumen.SelectedItemText
        End With

        With objNewgoAML_Customer
            .PK_Person_Identification_ID = intpk
            .isReportingPerson = 0
            .FK_Person_Type = 1
            .FK_Person_ID = obj_customer.PK_Customer_ID
            .Number = txt_number_identification.Value
            .Issued_Country = cmb_issued_country.SelectedItemValue
            .Issued_By = txt_issued_by.Value
            If txt_issued_date.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment.Value
            .Issued_Country = cmb_issued_country.SelectedItemValue
            .Type = cmb_Jenis_Dokumen.SelectedItemValue

        End With

        objListgoAML_vw_Customer_Identification.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Identification.Add(objNewgoAML_Customer)

        Store_INDV_Identification.DataSource = objListgoAML_vw_Customer_Identification.ToList()
        Store_INDV_Identification.DataBind()

        FP_Identification_INDV.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputCustomerIdentification()
    End Sub

    Sub SaveAddDirectorIdentificationDetail()
        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Person_Identification
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Person_Identification
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Person_Identification_ID = intpk
            .isReportingPerson = 0
            .FK_Person_Type = 4
            .FK_Person_ID = tempflag
            .Number = txt_number_identification_Director.Value
            .Issued_By = txt_issued_by_Director.Value
            If txt_issued_date_Director.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date_Director.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date_Director.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date_Director.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment_Director.Value
            .Country = cmb_issued_country_Director.SelectedItemText
            .Type_Document = cmb_Jenis_Dokumen_Director.SelectedItemText
        End With

        With objNewgoAML_Customer
            .PK_Person_Identification_ID = intpk
            .isReportingPerson = 0
            .FK_Person_Type = 4
            .FK_Person_ID = tempflag
            .Number = txt_number_identification_Director.Value
            .Issued_Country = cmb_issued_country_Director.SelectedItemValue
            .Issued_By = txt_issued_by_Director.Value
            If txt_issued_date_Director.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date_Director.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date_Director.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date_Director.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment_Director.Value
            .Issued_Country = cmb_issued_country_Director.SelectedItemValue
            .Type = cmb_Jenis_Dokumen_Director.SelectedItemValue

        End With

        objListgoAML_vw_Customer_Identification_Director.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Identification_Director.Add(objNewgoAML_Customer)

        Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = tempflag).ToList()
        Store_Director_Identification.DataBind()

        FP_Identification_Director.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputDirectorIdentification()
    End Sub

    Sub ClearinputDirectorIdentification()
        cmb_Jenis_Dokumen_Director.SetTextValue("")
        txt_identification_comment_Director.Text = ""
        txt_issued_by_Director.Text = ""
        txt_issued_date_Director.Text = ""
        txt_issued_expired_date_Director.Text = ""
        cmb_issued_country_Director.SetTextValue("")
        txt_number_identification_Director.Text = ""
    End Sub

    Sub SaveEditIdentificationDetail()


        With objTempCustomerIdentificationEdit
            .isReportingPerson = 0
            .FK_Person_Type = 1
            .FK_Person_ID = obj_customer.PK_Customer_ID
            .Number = txt_number_identification.Value
            .Issued_By = txt_issued_by.Value
            If txt_issued_date.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment.Value
            .Country = cmb_issued_country.SelectedItemText
            .Type_Document = cmb_Jenis_Dokumen.SelectedItemText
        End With

        With objTempgoAML_Ref_Identification
            .isReportingPerson = 0
            .FK_Person_Type = 1
            .FK_Person_ID = obj_customer.PK_Customer_ID
            .Number = txt_number_identification.Value
            .Issued_By = txt_issued_by.Value
            If txt_issued_date.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment.Value
            .Issued_Country = cmb_issued_country.SelectedItemValue
            .Type = cmb_Jenis_Dokumen.SelectedItemValue
        End With

        objTempCustomerIdentificationEdit = Nothing
        objTempgoAML_Ref_Identification = Nothing

        Store_INDV_Identification.DataSource = objListgoAML_vw_Customer_Identification.ToList()
        Store_INDV_Identification.DataBind()

        FP_Identification_INDV.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputCustomerIdentification()
    End Sub

    Sub SaveEditDirectorIdentificationDetail()


        With objTempCustomerIdentificationEdit_Director
            .isReportingPerson = 0
            .FK_Person_Type = 4
            .Number = txt_number_identification_Director.Value
            .Issued_By = txt_issued_by_Director.Value
            If txt_issued_date_Director.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date_Director.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date_Director.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date_Director.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment_Director.Value
            .Country = cmb_issued_country_Director.SelectedItemText
            .Type_Document = cmb_Jenis_Dokumen_Director.SelectedItemText
        End With

        With objTempgoAML_Ref_Identification_Director
            .isReportingPerson = 0
            .Number = txt_number_identification_Director.Value
            .FK_Person_Type = 4
            .Issued_By = txt_issued_by_Director.Value
            If txt_issued_date_Director.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date_Director.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date_Director.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date_Director.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment_Director.Value
            .Issued_Country = cmb_issued_country_Director.SelectedItemValue
            .Type = cmb_Jenis_Dokumen_Director.SelectedItemValue
        End With

        objTempCustomerIdentificationEdit_Director = Nothing
        objTempgoAML_Ref_Identification_Director = Nothing

        Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = tempflag).ToList()
        Store_Director_Identification.DataBind()

        FP_Identification_Director.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputDirectorIdentification()
    End Sub

    Protected Sub GrdCmdCustIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataIdentificationDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditCustIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailCustIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorIdentificationDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditDirectorIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailDirectorIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Sub LoadDataIdentificationDetail(id As Long)

        Dim objCustomerIdentificationDetail = objListgoAML_vw_Customer_Identification.Where(Function(x) x.PK_Person_Identification_ID = id).FirstOrDefault()

        If Not objCustomerIdentificationDetail Is Nothing Then
            FP_Identification_INDV.Hidden = False
            WindowDetailIdentification.Hidden = False
            btn_save_identification.Hidden = True
            WindowDetailIdentification.Title = "Identification Detail"
            txt_number_identification.ReadOnly = True
            cmb_Jenis_Dokumen.IsReadOnly = True
            cmb_issued_country.IsReadOnly = True
            txt_issued_by.ReadOnly = True
            txt_issued_date.ReadOnly = True
            txt_issued_expired_date.ReadOnly = True
            txt_identification_comment.ReadOnly = True
            ClearinputCustomerIdentification()
            txt_number_identification.Text = objCustomerIdentificationDetail.Number
            'Store_jenis_dokumen.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            'Store_jenis_dokumen.DataBind()
            Dim jenisdokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(X) X.Keterangan = objCustomerIdentificationDetail.Type_Document).FirstOrDefault
            If jenisdokumen IsNot Nothing Then
                cmb_Jenis_Dokumen.SetTextWithTextValue(jenisdokumen.Kode, jenisdokumen.Keterangan)
            End If

            'Store_issued_country.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_issued_country.DataBind()
            Dim namanegara = goAML_CustomerBLL.GetNamaNegaraByKeterangan(objCustomerIdentificationDetail.Country)
            If namanegara IsNot Nothing Then
                cmb_issued_country.SetTextWithTextValue(namanegara.Kode, namanegara.Keterangan)
            End If
            txt_issued_by.Text = objCustomerIdentificationDetail.Issued_By
            txt_issued_date.Value = objCustomerIdentificationDetail.Issue_Date
            txt_issued_expired_date.Value = objCustomerIdentificationDetail.Expiry_Date
            txt_identification_comment.Text = objCustomerIdentificationDetail.Identification_Comment
        End If
    End Sub

    Sub LoadDataDirectorIdentificationDetail(id As Long)

        Dim objCustomerIdentificationDetail = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.PK_Person_Identification_ID = id).FirstOrDefault()

        If Not objCustomerIdentificationDetail Is Nothing Then
            FP_Identification_Director.Hidden = False
            WindowDetailIdentification.Hidden = False
            btn_save_identification_Director.Hidden = True
            WindowDetailIdentification.Title = "Identification Detail"
            txt_number_identification_Director.ReadOnly = True
            cmb_Jenis_Dokumen_Director.IsReadOnly = True
            cmb_issued_country_Director.IsReadOnly = True
            txt_issued_by_Director.ReadOnly = True
            txt_issued_date_Director.ReadOnly = True
            txt_issued_expired_date_Director.ReadOnly = True
            txt_identification_comment_Director.ReadOnly = True
            ClearinputDirectorIdentification()
            txt_number_identification_Director.Text = objCustomerIdentificationDetail.Number
            'Storejenisdokumen_Director.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            ' Storejenisdokumen_Director.DataBind()
            Dim jenisdokumen_director_detail = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(X) X.Keterangan = objCustomerIdentificationDetail.Type_Document).FirstOrDefault
            If jenisdokumen_director_detail IsNot Nothing Then
                cmb_Jenis_Dokumen_Director.SetTextWithTextValue(jenisdokumen_director_detail.Kode, jenisdokumen_director_detail.Keterangan)
            End If

            'StoreIssueCountry_Director.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'StoreIssueCountry_Director.DataBind()
            Dim namanegara_director_detail = goAML_CustomerBLL.GetNamaNegaraByKeterangan(objCustomerIdentificationDetail.Country)
            If namanegara_director_detail IsNot Nothing Then
                cmb_issued_country_Director.SetTextWithTextValue(namanegara_director_detail.Kode, namanegara_director_detail.Keterangan)
            End If
            txt_issued_by_Director.Text = objCustomerIdentificationDetail.Issued_By
            txt_issued_date_Director.Value = objCustomerIdentificationDetail.Issue_Date
            txt_issued_expired_date_Director.Value = objCustomerIdentificationDetail.Expiry_Date
            txt_identification_comment_Director.Text = objCustomerIdentificationDetail.Identification_Comment
        End If
    End Sub

    Sub DeleteRecordTaskDetailCustIdentification(id As Long)
        Dim objDelVWIdentification As GoAMLDAL.Vw_Person_Identification = objListgoAML_vw_Customer_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)
        Dim objxDelIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)

        If Not objxDelIdentification Is Nothing Then
            objListgoAML_Ref_Identification.Remove(objxDelIdentification)
        End If

        If Not objDelVWIdentification Is Nothing Then
            objListgoAML_vw_Customer_Identification.Remove(objDelVWIdentification)


            Store_INDV_Identification.DataSource = objListgoAML_vw_Customer_Identification.ToList()
            Store_INDV_Identification.DataBind()
        End If

        FP_Identification_INDV.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputCustomerIdentification()

    End Sub

    Sub DeleteRecordTaskDetailDirectorIdentification(id As Long)
        Dim objDelVWIdentification As GoAMLDAL.Vw_Person_Identification = objListgoAML_vw_Customer_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = id)
        Dim objxDelIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = id)

        If Not objxDelIdentification Is Nothing Then
            objListgoAML_Ref_Identification_Director.Remove(objxDelIdentification)
        End If

        If Not objDelVWIdentification Is Nothing Then
            objListgoAML_vw_Customer_Identification_Director.Remove(objDelVWIdentification)

            If objTempCustomerDirectorEdit IsNot Nothing Then
                Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = objTempCustomerDirectorEdit.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Person_Type = 4).ToList()
                Store_Director_Identification.DataBind()
            End If
        End If

        FP_Identification_Director.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputDirectorIdentification()
    End Sub

    Sub LoadDataEditCustIdentification(id As Long)
        objTempgoAML_Ref_Identification = objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)
        objTempCustomerIdentificationEdit = objListgoAML_vw_Customer_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)

        If Not objTempCustomerIdentificationEdit Is Nothing Then
            FP_Identification_INDV.Hidden = False
            WindowDetailIdentification.Hidden = False
            btn_save_identification.Hidden = False
            cmb_Jenis_Dokumen.IsReadOnly = False
            txt_issued_by.ReadOnly = False
            txt_issued_date.ReadOnly = False
            txt_issued_expired_date.ReadOnly = False
            txt_identification_comment.ReadOnly = False
            cmb_issued_country.IsReadOnly = False
            txt_number_identification.ReadOnly = False
            WindowDetailIdentification.Title = "Identification Edit"
            ClearinputCustomerIdentification()
            With objTempCustomerIdentificationEdit
                'Store_jenis_dokumen.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
                'Store_jenis_dokumen.DataBind()
                Dim jenisdokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(x) x.Keterangan = .Type_Document).FirstOrDefault()
                If jenisdokumen IsNot Nothing Then
                    cmb_Jenis_Dokumen.SetTextWithTextValue(jenisdokumen.Kode, jenisdokumen.Keterangan)
                End If
                txt_issued_by.Text = .Issued_By
                txt_issued_date.Value = .Issue_Date
                txt_issued_expired_date.Value = .Expiry_Date
                txt_identification_comment.Text = .Identification_Comment
                'Store_issued_country.DataSource = goAML_CustomerBLL.GetListNamaNegara().ToList()
                'Store_issued_country.DataBind()
                Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByKeterangan(.Country)
                If kodenegara IsNot Nothing Then
                    cmb_issued_country.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
                End If

                txt_number_identification.Text = .Number
            End With

            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataEditDirectorIdentification(id As Long)
        objTempgoAML_Ref_Identification_Director = objListgoAML_Ref_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = id)
        objTempCustomerIdentificationEdit_Director = objListgoAML_vw_Customer_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = id)

        If Not objTempCustomerIdentificationEdit_Director Is Nothing Then
            FP_Identification_Director.Hidden = False
            WindowDetailIdentification.Hidden = False
            btn_save_identification_Director.Hidden = False
            cmb_Jenis_Dokumen_Director.IsReadOnly = False
            txt_issued_by_Director.ReadOnly = False
            txt_issued_date_Director.ReadOnly = False
            txt_issued_expired_date_Director.ReadOnly = False
            txt_identification_comment_Director.ReadOnly = False
            cmb_issued_country_Director.IsReadOnly = False
            txt_number_identification_Director.ReadOnly = False
            WindowDetailIdentification.Title = "Identification Edit"
            ClearinputDirectorIdentification()
            With objTempCustomerIdentificationEdit_Director
                'Storejenisdokumen_Director.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
                'Storejenisdokumen_Director.DataBind()
                Dim jenisdokumen_director_edit = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(x) x.Keterangan = .Type_Document).FirstOrDefault()
                If jenisdokumen_director_edit IsNot Nothing Then
                    cmb_Jenis_Dokumen_Director.SetTextWithTextValue(jenisdokumen_director_edit.Kode, jenisdokumen_director_edit.Keterangan)
                End If
                txt_number_identification_Director.Value = .Number
                txt_issued_by_Director.Text = .Issued_By
                txt_issued_date_Director.Value = .Issue_Date
                txt_issued_expired_date_Director.Value = .Expiry_Date
                txt_identification_comment_Director.Text = .Identification_Comment
                'StoreIssueCountry_Director.DataSource = goAML_CustomerBLL.GetListNamaNegara().ToList()
                'StoreIssueCountry_Director.DataBind()
                Dim kodenegara_director_edit = goAML_CustomerBLL.GetNamaNegaraByKeterangan(.Country)
                If kodenegara_director_edit IsNot Nothing Then
                    cmb_issued_country_Director.SetTextWithTextValue(kodenegara_director_edit.Kode, kodenegara_director_edit.Keterangan)
                End If
            End With
        End If
    End Sub

    Protected Sub btnAddCustomerDirector_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            GridEmail_Director.IsViewMode = False
            GridSanction_Director.IsViewMode = False
            GridPEP_Director.IsViewMode = False

            ' 2023-09-27, Nael: Clear Data Grid Input ketika ketika add customer dibuka

            GridEmail_Director.LoadData(New List(Of DataModel.goAML_Ref_Customer_Email))
            GridSanction_Director.LoadData(New List(Of DataModel.goAML_Ref_Customer_Sanction))
            GridPEP_Director.LoadData(New List(Of DataModel.goAML_Ref_Customer_PEP))
            ' ============================================================================

            tempflag = 0
            FP_Director.Hidden = False
            WindowDetailDirector.Hidden = False
            'cmb_director.ReadOnly = False
            cmb_peran.IsReadOnly = False
            txt_Director_Title.ReadOnly = False
            txt_Director_Last_Name.ReadOnly = False
            txt_Director_SSN.ReadOnly = False
            txt_Director_BirthDate.ReadOnly = False
            txt_Director_Birth_Place.ReadOnly = False
            txt_Director_Mothers_Name.ReadOnly = False
            txt_Director_Alias.ReadOnly = False
            txt_Director_Passport_Number.ReadOnly = False
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            cmb_Director_Passport_Country.IsReadOnly = False
            'End Update
            txt_Director_ID_Number.ReadOnly = False
            cmb_Director_Nationality1.IsReadOnly = False
            cmb_Director_Nationality2.IsReadOnly = False
            cmb_Director_Nationality3.IsReadOnly = False
            cmb_Director_Gender.IsReadOnly = False
            cmb_Director_Residence.IsReadOnly = False
            'comment by septian, goAML 5.0.1, 2023-02-12
            ' 2023-10-10, Nael: unhide
            txt_Director_Email.ReadOnly = False
            txt_Director_Email2.ReadOnly = False
            txt_Director_Email3.ReadOnly = False
            txt_Director_Email4.ReadOnly = False
            txt_Director_Email5.ReadOnly = False
            'end comment
            txt_Director_Occupation.ReadOnly = False
            txt_Director_employer_name.ReadOnly = False
            cb_Director_Deceased.ReadOnly = False
            txt_Director_Deceased_Date.ReadOnly = False
            txt_Director_Tax_Number.ReadOnly = False
            cb_Director_Tax_Reg_Number.ReadOnly = False
            txt_Director_Source_of_Wealth.ReadOnly = False
            txt_Director_Comments.ReadOnly = False
            btnsaveDirector.Hidden = False
            btnsavedirectorphone.Hidden = False
            btnsavedirectorAddress.Hidden = False
            btnsaveEmpdirectorPhone.Hidden = False
            btnsaveEmpdirectorAddress.Hidden = False
            btnaddidentificationdirector.Hidden = False
            WindowDetailDirector.Title = "Customer Director Add"


            'StorePeran.DataSource = goAML_CustomerBLL.GetListPeranDalamKorporasi()
            'StorePeran.DataBind()
            'Store_Director_Gender.DataSource = goAML_CustomerBLL.GetListJenisKelamin()
            'Store_Director_Gender.DataBind()
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            'Store_Director_PassportCountry.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_PassportCountry.DataBind()
            'End Update
            'Store_Director_Nationality1.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_Nationality1.DataBind()
            'Store_Director_Nationality2.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_Nationality2.DataBind()
            'Store_Director_Nationality3.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_Nationality3.DataBind()
            'Store_Director_Residence.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_Residence.DataBind()

            ClearinputCustomerDirector()

            ' 2023-10-24, Nael: new Temporary director
            'Dim objRandom As New Random

            'objTempDirectorAdd = New DataModel.goAML_Ref_Entity_Director With {
            '    .PK_goAML_Ref_Customer_Entity_Director_ID = -objRandom.Next,
            '}

            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditCustDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailCustDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ClearinputCustomerDirector()
        'cmb_director.Value = Nothing
        cmb_peran.SetTextValue("")
        txt_Director_Title.Text = ""
        txt_Director_Last_Name.Text = ""
        txt_Director_SSN.Text = ""
        txt_Director_BirthDate.Text = ""
        txt_Director_Birth_Place.Text = ""
        txt_Director_Mothers_Name.Text = ""
        txt_Director_Alias.Text = ""
        txt_Director_Passport_Number.Text = ""
        'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
        cmb_Director_Passport_Country.SetTextValue("")
        'End Update
        txt_Director_ID_Number.Text = ""
        cmb_Director_Nationality1.SetTextValue("")
        cmb_Director_Nationality2.SetTextValue("")
        cmb_Director_Nationality3.SetTextValue("")
        cmb_Director_Gender.SetTextValue("")
        cmb_Director_Residence.SetTextValue("")

        'objTempDirectorAdd = Nothing


        'comment by septian, goAML 5.0.1, 2023-02-12
        ' 2023-10-10, Nael: unhide
        txt_Director_Email.Text = ""
        txt_Director_Email2.Text = ""
        txt_Director_Email3.Text = ""
        txt_Director_Email4.Text = ""
        txt_Director_Email5.Text = ""
        'end commenet
        txt_Director_Occupation.Text = ""
        txt_Director_employer_name.Text = ""
        cb_Director_Deceased.Checked = False
        txt_Director_Deceased_Date.Value = Nothing
        txt_Director_Tax_Number.Text = ""
        cb_Director_Tax_Reg_Number.Checked = False
        txt_Director_Source_of_Wealth.Text = ""
        txt_Director_Comments.Text = ""
        Store_Director_Employer_Address.DataBind()
        Store_Director_Employer_Phone.DataBind()
        Store_DirectorAddress.DataBind()
        Store_DirectorPhone.DataBind()
        Store_Director_Identification.DataBind()
    End Sub

    Sub LoadDataDirectorDetail(id As Long)

        Dim objCustomerDirectorDetail = objListgoAML_Ref_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id).FirstOrDefault()

        Dim objCustomerDirectorDetail2 = Nothing
        If objListgoAML_Ref_Director2 IsNot Nothing Then
            objCustomerDirectorDetail2 = objListgoAML_Ref_Director2.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id).FirstOrDefault()
        End If

        If Not objCustomerDirectorDetail Is Nothing Then

            GridEmail_Director.IsViewMode = True
            GridSanction_Director.IsViewMode = True
            GridPEP_Director.IsViewMode = True

            FP_Director.Hidden = False
            WindowDetailDirector.Hidden = False
            btnsaveDirector.Hidden = True
            WindowDetailDirector.Title = "Director Detail"
            'cmb_director.ReadOnly = True
            cmb_peran.IsReadOnly = True
            txt_Director_Title.ReadOnly = True
            txt_Director_Last_Name.ReadOnly = True
            txt_Director_SSN.ReadOnly = True
            txt_Director_BirthDate.ReadOnly = True
            txt_Director_Birth_Place.ReadOnly = True
            txt_Director_Mothers_Name.ReadOnly = True
            txt_Director_Alias.ReadOnly = True
            txt_Director_Passport_Number.ReadOnly = True
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            cmb_Director_Passport_Country.IsReadOnly = True
            'End Update
            txt_Director_ID_Number.ReadOnly = True
            cmb_Director_Nationality1.IsReadOnly = True
            cmb_Director_Nationality2.IsReadOnly = True
            cmb_Director_Nationality3.IsReadOnly = True
            cmb_Director_Gender.IsReadOnly = True
            cmb_Director_Residence.IsReadOnly = True
            'comment by septian, goAML 5.0.1, 2023-02-12
            ' 2023-10-10, Nael: Unhide
            txt_Director_Email.ReadOnly = True
            txt_Director_Email2.ReadOnly = True
            txt_Director_Email3.ReadOnly = True
            txt_Director_Email4.ReadOnly = True
            txt_Director_Email5.ReadOnly = True
            'end comment
            txt_Director_Occupation.ReadOnly = True
            txt_Director_employer_name.ReadOnly = True
            cb_Director_Deceased.ReadOnly = True
            txt_Director_Deceased_Date.ReadOnly = True
            txt_Director_Tax_Number.ReadOnly = True
            cb_Director_Tax_Reg_Number.ReadOnly = True
            txt_Director_Source_of_Wealth.ReadOnly = True
            txt_Director_Comments.ReadOnly = True
            'StorePeran.DataSource = goAML_CustomerBLL.GetListPeranDalamKorporasi()
            'StorePeran.DataBind()
            Dim peran = goAML_CustomerBLL.GetRoleDirectorByID(objCustomerDirectorDetail.Role)
            If peran IsNot Nothing Then
                cmb_peran.SetTextWithTextValue(peran.Kode, peran.Keterangan)
            End If
            txt_Director_Title.Text = objCustomerDirectorDetail.Title
            txt_Director_Last_Name.Text = objCustomerDirectorDetail.Last_Name
            txt_Director_SSN.Text = objCustomerDirectorDetail.SSN
            If objCustomerDirectorDetail.BirthDate IsNot Nothing Then
                txt_Director_BirthDate.Text = objCustomerDirectorDetail.BirthDate
            End If
            txt_Director_Birth_Place.Text = objCustomerDirectorDetail.Birth_Place
            txt_Director_Mothers_Name.Text = objCustomerDirectorDetail.Mothers_Name
            txt_Director_Alias.Text = objCustomerDirectorDetail.Alias
            txt_Director_ID_Number.Text = objCustomerDirectorDetail.ID_Number
            txt_Director_Passport_Number.Text = objCustomerDirectorDetail.Passport_Number
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            Dim director_passport_country = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Passport_Country)
            If director_passport_country IsNot Nothing Then
                cmb_Director_Passport_Country.SetTextWithTextValue(director_passport_country.Kode, director_passport_country.Keterangan)
            End If
            Dim director_Nationality1 = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Nationality1)
            If director_Nationality1 IsNot Nothing Then
                cmb_Director_Nationality1.SetTextWithTextValue(director_Nationality1.Kode, director_Nationality1.Keterangan)
            End If
            Dim director_Nationality2 = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Nationality2)
            If director_Nationality2 IsNot Nothing Then
                cmb_Director_Nationality2.SetTextWithTextValue(director_Nationality2.Kode, director_Nationality2.Keterangan)
            End If
            Dim director_Nationality3 = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Nationality3)
            If director_Nationality3 IsNot Nothing Then
                cmb_Director_Nationality3.SetTextWithTextValue(director_Nationality3.Kode, director_Nationality3.Keterangan)
            End If
            Dim director_Residence = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Residence)
            If director_Residence IsNot Nothing Then
                cmb_Director_Residence.SetTextWithTextValue(director_Residence.Kode, director_Residence.Keterangan)
            End If
            Dim gender = goAML_CustomerBLL.GetJenisKelamin(objCustomerDirectorDetail.Gender)
            If gender IsNot Nothing Then
                cmb_Director_Gender.SetTextWithTextValue(gender.Kode, gender.Keterangan)
            End If
            'End Update
            'If objCustomerDirectorDetail.Nationality1 IsNot Nothing Then
            '    cmb_Director_Nationality1.Value = objCustomerDirectorDetail.Nationality1
            'End If
            'If objCustomerDirectorDetail.Nationality2 IsNot Nothing Then
            '    cmb_Director_Nationality2.Value = objCustomerDirectorDetail.Nationality2
            'End If
            'If objCustomerDirectorDetail.Nationality3 IsNot Nothing Then
            '    cmb_Director_Nationality3.Value = objCustomerDirectorDetail.Nationality3
            'End If
            'If objCustomerDirectorDetail.Residence IsNot Nothing Then
            '    cmb_Director_Residence.Value = objCustomerDirectorDetail.Residence
            'End If

            'If objCustomerDirectorDetail.Gender IsNot Nothing Then
            '    cmb_Director_Gender.Value = objCustomerDirectorDetail.Gender
            'End If
            'comment by septian, goAML 5.0.1, 2023-02-12
            ' 2023-10-10, Nael
            txt_Director_Email.Text = objCustomerDirectorDetail.Email
            txt_Director_Email2.Text = objCustomerDirectorDetail.Email2
            txt_Director_Email3.Text = objCustomerDirectorDetail.Email3
            txt_Director_Email4.Text = objCustomerDirectorDetail.Email4
            txt_Director_Email5.Text = objCustomerDirectorDetail.Email5
            'end comment
            txt_Director_Occupation.Text = objCustomerDirectorDetail.Occupation
            txt_Director_employer_name.Text = objCustomerDirectorDetail.Employer_Name
            If objCustomerDirectorDetail.Deceased = True Then
                cb_Director_Deceased.Checked = True
                txt_Director_Deceased_Date.Value = objCustomerDirectorDetail.Deceased_Date
            End If
            txt_Director_Tax_Number.Text = objCustomerDirectorDetail.Tax_Number
            If objCustomerDirectorDetail.Tax_Reg_Number = True Then
                cb_Director_Tax_Reg_Number.Checked = True
            End If
            txt_Director_Source_of_Wealth.Text = objCustomerDirectorDetail.Source_of_Wealth
            txt_Director_Comments.Text = objCustomerDirectorDetail.Comments
            btnaddidentificationdirector.Hidden = True
            btnsavedirectorphone.Hidden = True
            btnsavedirectorAddress.Hidden = True
            btnsaveEmpdirectorPhone.Hidden = True
            btnsaveEmpdirectorAddress.Hidden = True
            Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
            Store_DirectorAddress.DataBind()
            Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
            Store_DirectorPhone.DataBind()
            Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
            Store_Director_Employer_Address.DataBind()
            Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
            Store_Director_Employer_Phone.DataBind()
            Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()
            Store_Director_Identification.DataBind()

            Dim email_list = goAML_Customer_Service.GetEmailByPkFk(id, 6)
            If objCustomerDirectorDetail2 IsNot Nothing Then
                email_list = objCustomerDirectorDetail2.ObjList_GoAML_Ref_Customer_Email
            End If
            Dim sanction_list = goAML_Customer_Service.GetSanctionByPkFk(id, 6)
            If objCustomerDirectorDetail2 IsNot Nothing Then
                sanction_list = objCustomerDirectorDetail2.ObjList_GoAML_Ref_Customer_Sanction
            End If
            Dim pep_list = goAML_Customer_Service.GetPEPByPkFk(id, 6)
            If objCustomerDirectorDetail2 IsNot Nothing Then
                pep_list = objCustomerDirectorDetail2.ObjList_GoAML_Ref_Customer_PEP
            End If


            GridEmail_Director.LoadData(email_list)
            GridSanction_Director.LoadData(sanction_list)
            GridPEP_Director.LoadData(pep_list)


        End If
    End Sub

    Sub LoadDataEditCustDirector(id As Long)
        objTempgoAML_Ref_Director = objListgoAML_Ref_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
        objTempCustomerDirectorEdit = objListgoAML_vw_Customer_Entity_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)

        Dim objCustomerDirectorDetail2 = Nothing
        If objListgoAML_Ref_Director2 IsNot Nothing Then
            objCustomerDirectorDetail2 = objListgoAML_Ref_Director2.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id).FirstOrDefault()
        End If

        tempflag = id
        If Not objTempCustomerDirectorEdit Is Nothing Then

            GridEmail_Director.IsViewMode = False
            GridSanction_Director.IsViewMode = False
            GridPEP_Director.IsViewMode = False

            FP_Director.Hidden = False
            WindowDetailDirector.Hidden = False
            btnsaveDirector.Hidden = False
            'cmb_director.ReadOnly = False
            cmb_peran.IsReadOnly = False
            txt_Director_Title.ReadOnly = False
            txt_Director_Last_Name.ReadOnly = False
            txt_Director_SSN.ReadOnly = False
            txt_Director_BirthDate.ReadOnly = False
            txt_Director_Birth_Place.ReadOnly = False
            txt_Director_Mothers_Name.ReadOnly = False
            txt_Director_Alias.ReadOnly = False
            txt_Director_Passport_Number.ReadOnly = False
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            cmb_Director_Passport_Country.IsReadOnly = False
            'End Update
            txt_Director_ID_Number.ReadOnly = False
            cmb_Director_Nationality1.IsReadOnly = False
            cmb_Director_Nationality2.IsReadOnly = False
            cmb_Director_Nationality3.IsReadOnly = False
            cmb_Director_Gender.IsReadOnly = False
            cmb_Director_Residence.IsReadOnly = False
            'comment by septian, goAML 5.0.1, 2023-02-12
            txt_Director_Email.ReadOnly = False
            txt_Director_Email2.ReadOnly = False
            txt_Director_Email3.ReadOnly = False
            txt_Director_Email4.ReadOnly = False
            txt_Director_Email5.ReadOnly = False
            'end comment
            txt_Director_Occupation.ReadOnly = False
            txt_Director_employer_name.ReadOnly = False
            cb_Director_Deceased.ReadOnly = False
            txt_Director_Deceased_Date.ReadOnly = False
            txt_Director_Tax_Number.ReadOnly = False
            cb_Director_Tax_Reg_Number.ReadOnly = False
            txt_Director_Source_of_Wealth.ReadOnly = False
            txt_Director_Comments.ReadOnly = False
            btnsavedirectorphone.Hidden = False
            btnsavedirectorAddress.Hidden = False
            btnsaveEmpdirectorPhone.Hidden = False
            btnsaveEmpdirectorAddress.Hidden = False
            btnaddidentificationdirector.Hidden = False
            btn_save_identification_Director.Hidden = False
            WindowDetailDirector.Title = "Director Edit"
            With objTempCustomerDirectorEdit
                'StoreDirector.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,INDV_Last_Name,cb_value from Vw_CB_Signatory_Person", Nothing)
                'StoreDirector.DataBind()
                'cmb_director.SetValue(objTempgoAML_Ref_Director.FK_Customer_ID)
                'StorePeran.DataSource = goAML_CustomerBLL.GetListPeranDalamKorporasi()
                'StorePeran.DataBind()
                Dim peran = goAML_CustomerBLL.GetRoleDirectorByID(objTempgoAML_Ref_Director.Role)
                If peran IsNot Nothing Then
                    cmb_peran.SetTextWithTextValue(peran.Kode, peran.Keterangan)
                End If
                txt_Director_Title.Text = objTempgoAML_Ref_Director.Title
                txt_Director_Last_Name.Text = objTempgoAML_Ref_Director.Last_Name
                txt_Director_SSN.Text = objTempgoAML_Ref_Director.SSN
                If objTempgoAML_Ref_Director.BirthDate IsNot Nothing Then
                    txt_Director_BirthDate.Text = objTempgoAML_Ref_Director.BirthDate
                End If
                txt_Director_Birth_Place.Text = objTempgoAML_Ref_Director.Birth_Place
                txt_Director_Mothers_Name.Text = objTempgoAML_Ref_Director.Mothers_Name
                txt_Director_Alias.Text = objTempgoAML_Ref_Director.Alias
                txt_Director_ID_Number.Text = objTempgoAML_Ref_Director.ID_Number
                txt_Director_Passport_Number.Text = objTempgoAML_Ref_Director.Passport_Number
                'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
                Dim director_passport_country = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Passport_Country)
                If director_passport_country IsNot Nothing Then
                    cmb_Director_Passport_Country.SetTextWithTextValue(director_passport_country.Kode, director_passport_country.Keterangan)
                End If
                Dim director_Nationality1 = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Nationality1)
                If director_Nationality1 IsNot Nothing Then
                    cmb_Director_Nationality1.SetTextWithTextValue(director_Nationality1.Kode, director_Nationality1.Keterangan)
                End If
                Dim director_Nationality2 = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Nationality2)
                If director_Nationality2 IsNot Nothing Then
                    cmb_Director_Nationality2.SetTextWithTextValue(director_Nationality2.Kode, director_Nationality2.Keterangan)
                End If
                Dim director_Nationality3 = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Nationality3)
                If director_Nationality3 IsNot Nothing Then
                    cmb_Director_Nationality3.SetTextWithTextValue(director_Nationality3.Kode, director_Nationality3.Keterangan)
                End If
                Dim director_Residence = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Residence)
                If director_Residence IsNot Nothing Then
                    cmb_Director_Residence.SetTextWithTextValue(director_Residence.Kode, director_Residence.Keterangan)
                End If
                Dim gender = goAML_CustomerBLL.GetJenisKelamin(objTempgoAML_Ref_Director.Gender)
                If gender IsNot Nothing Then
                    cmb_Director_Gender.SetTextWithTextValue(gender.Kode, gender.Keterangan)
                End If
                'End Update
                'End Update
                'If objTempgoAML_Ref_Director.Nationality1 IsNot Nothing Then
                '    cmb_Director_Nationality1.Value = objTempgoAML_Ref_Director.Nationality1
                'End If
                'If objTempgoAML_Ref_Director.Nationality2 IsNot Nothing Then
                '    cmb_Director_Nationality2.Value = objTempgoAML_Ref_Director.Nationality2
                'End If
                'If objTempgoAML_Ref_Director.Nationality3 IsNot Nothing Then
                '    cmb_Director_Nationality3.Value = objTempgoAML_Ref_Director.Nationality3
                'End If
                'If objTempgoAML_Ref_Director.Residence IsNot Nothing Then
                '    cmb_Director_Residence.Value = objTempgoAML_Ref_Director.Residence
                'End If

                'If objTempgoAML_Ref_Director.Gender IsNot Nothing Then
                '    cmb_Director_Gender.SelectedItemValue = objTempgoAML_Ref_Director.Gender
                'End If
                'comment by septian, goAML 5.0.1, 2023-02-12
                txt_Director_Email.Text = objTempgoAML_Ref_Director.Email
                txt_Director_Email2.Text = objTempgoAML_Ref_Director.Email2
                txt_Director_Email3.Text = objTempgoAML_Ref_Director.Email3
                txt_Director_Email4.Text = objTempgoAML_Ref_Director.Email4
                txt_Director_Email5.Text = objTempgoAML_Ref_Director.Email5
                'end comment
                txt_Director_Occupation.Text = objTempgoAML_Ref_Director.Occupation
                txt_Director_employer_name.Text = objTempgoAML_Ref_Director.Employer_Name
                If objTempgoAML_Ref_Director.Deceased = True Then
                    cb_Director_Deceased.Checked = True
                    txt_Director_Deceased_Date.Value = objTempgoAML_Ref_Director.Deceased_Date
                End If
                txt_Director_Tax_Number.Text = objTempgoAML_Ref_Director.Tax_Number
                If objTempgoAML_Ref_Director.Tax_Reg_Number = True Then
                    cb_Director_Tax_Reg_Number.Checked = True
                End If
                txt_Director_Source_of_Wealth.Text = objTempgoAML_Ref_Director.Source_of_Wealth
                txt_Director_Comments.Text = objTempgoAML_Ref_Director.Comments

                Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
                Store_DirectorAddress.DataBind()
                Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
                Store_DirectorPhone.DataBind()
                Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
                Store_Director_Employer_Address.DataBind()
                Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
                Store_Director_Employer_Phone.DataBind()
                Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()
                Store_Director_Identification.DataBind()
                'Dim DirectorEmpAddress = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
                'If DirectorEmpAddress.Count > 0 Then
                '    btnsaveEmpdirectorAddress.Hidden = True
                'End If
                'Dim DirectorEmpPhone = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
                'If DirectorEmpPhone.Count > 0 Then
                '    btnsaveEmpdirectorPhone.Hidden = True
                'End If

            End With

            Dim email_list = goAML_Customer_Service.GetEmailByPkFk(id, 6)
            If objCustomerDirectorDetail2 IsNot Nothing Then
                email_list = objCustomerDirectorDetail2.ObjList_GoAML_Ref_Customer_Email
            End If
            Dim sanction_list = goAML_Customer_Service.GetSanctionByPkFk(id, 6)
            If objCustomerDirectorDetail2 IsNot Nothing Then
                sanction_list = objCustomerDirectorDetail2.ObjList_GoAML_Ref_Customer_Sanction
            End If
            Dim pep_list = goAML_Customer_Service.GetPEPByPkFk(id, 6)
            If objCustomerDirectorDetail2 IsNot Nothing Then
                pep_list = objCustomerDirectorDetail2.ObjList_GoAML_Ref_Customer_PEP
            End If

            GridEmail_Director.LoadData(email_list)
            GridSanction_Director.LoadData(sanction_list)
            GridPEP_Director.LoadData(pep_list)

            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub DeleteRecordTaskDetailCustDirector(id As Long)
        Dim objDelVWDirector As GoAMLDAL.Vw_Customer_Director = objListgoAML_vw_Customer_Entity_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
        Dim objDelVWAddress As List(Of GoAMLDAL.Vw_Director_Addresses) = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        Dim objDelVWPhone As List(Of GoAMLDAL.Vw_Director_Phones) = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        Dim objDelVWEMPAddress As List(Of GoAMLDAL.Vw_Director_Employer_Addresses) = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        Dim objDelEMPVWPhone As List(Of GoAMLDAL.Vw_Director_Employer_Phones) = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        Dim objDelVWIdentification As List(Of GoAMLDAL.Vw_Person_Identification) = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()

        Dim objxDelDirector As GoAMLDAL.goAML_Ref_Customer_Entity_Director = objListgoAML_Ref_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
        Dim objxDelAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        Dim objxDelPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        Dim objxDelEMPAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        Dim objxDelEMPPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        Dim objxDelIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()

        If Not objxDelDirector Is Nothing Then
            objListgoAML_Ref_Director.Remove(objxDelDirector)
            For Each item As GoAMLDAL.goAML_Ref_Address In objxDelAddress
                objListgoAML_Ref_Director_Address.Remove(item)
            Next
            For Each item As GoAMLDAL.goAML_Ref_Phone In objxDelPhone
                objListgoAML_Ref_Director_Phone.Remove(item)
            Next
            For Each item As GoAMLDAL.goAML_Ref_Address In objxDelEMPAddress
                objListgoAML_Ref_Director_Employer_Address.Remove(item)
            Next
            For Each item As GoAMLDAL.goAML_Ref_Phone In objxDelEMPPhone
                objListgoAML_Ref_Director_Employer_Phone.Remove(item)
            Next
            For Each item As GoAMLDAL.goAML_Person_Identification In objxDelIdentification
                objListgoAML_Ref_Identification_Director.Remove(item)
            Next
        End If

        If Not objDelVWDirector Is Nothing Then
            objListgoAML_vw_Customer_Entity_Director.Remove(objDelVWDirector)
            For Each item As Vw_Director_Addresses In objDelVWAddress
                objListgoAML_vw_Director_Addresses.Remove(item)
            Next
            For Each item As Vw_Director_Phones In objDelVWPhone
                objListgoAML_vw_Director_Phones.Remove(item)
            Next
            For Each item As Vw_Director_Employer_Addresses In objDelVWEMPAddress
                objListgoAML_vw_Director_Employer_Addresses.Remove(item)
            Next
            For Each item As Vw_Director_Employer_Phones In objDelEMPVWPhone
                objListgoAML_vw_Director_Employer_Phones.Remove(item)
            Next
            For Each item As Vw_Person_Identification In objDelVWIdentification
                objListgoAML_vw_Customer_Identification_Director.Remove(item)
            Next

            Store_Director_Corp.DataSource = objListgoAML_vw_Customer_Entity_Director.ToList()
            Store_Director_Corp.DataBind()
        End If


        FP_Director.Hidden = True
        WindowDetailDirector.Hidden = True
        ClearinputCustomerDirector()

    End Sub

    Protected Sub btnSaveCustomerDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomerDirectorValid() Then
                'If objTempCustomerPhoneEdit Is Nothing Then
                If objTempCustomerDirectorEdit Is Nothing Then
                    SaveAddDirectorDetail()
                Else
                    SaveEditDirectorDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Function IsDataCustomerDirectorValid() As Boolean
        'If cmb_director.RawValue = "Select One" Then
        '    Throw New Exception("Please Select Person")
        'End If
        'Return True
        If cmb_peran.SelectedItemValue = "" Then
            Throw New Exception("Role is required")
        End If
        'If txt_Director_Occupation.Text = "" Then
        '    Throw New Exception("Occupation  is required")
        'End If
        'If txt_Director_Source_of_Wealth.Text = "" Then
        '    Throw New Exception("Source of Wealth is required")
        'End If
        If txt_Director_Last_Name.Text = "" Then
            Throw New Exception("Full Name is required")
        End If
        'If txt_Director_BirthDate.RawValue = "" Then
        '    Throw New Exception("Birth Date is required")
        'End If
        'If txt_Director_Birth_Place.Text = "" Then
        '    Throw New Exception("Birth Place is required")
        'End If
        'Update: Zikri_08102020
        If txt_Director_SSN.Text.Trim IsNot "" Then
            If Not IsNumber(txt_Director_SSN.Text) Then
                Throw New Exception("NIK is Invalid")
            End If
            If txt_Director_SSN.Text.Trim.Length <> 16 Then
                Throw New Exception("NIK must contains 16 digit number")
            End If
        End If
        'End Update
        'If cmb_Director_Gender.SelectedItemValue = "" Then
        '    Throw New Exception("Gender is required")
        'End If
        'If cmb_Director_Nationality1.SelectedItemValue = "" Then
        '    Throw New Exception("Nationality 1 is required")
        'End If
        'If cmb_Director_Residence.SelectedItemValue = "" Then
        '    Throw New Exception("Domicile Country is required")
        'End If
        If objListgoAML_Ref_Director_Phone.Count = 0 Then
            Throw New Exception("Phone Director is required")
        End If
        If objListgoAML_Ref_Director_Address.Count = 0 Then
            Throw New Exception("Address Director is required")
        End If
        'If Not String.IsNullOrEmpty(txt_Director_ID_Number.Text) Then
        '    If objListgoAML_Ref_Identification_Director.Count = 0 Then
        '        Throw New Exception("Identification is required")
        '    End If
        'End If
        'If objListgoAML_Ref_Director_Employer_Address.Count = 0 Then
        '    Throw New Exception("Employer Address Director is required")
        'End If
        'If objListgoAML_Ref_Director_Employer_Phone.Count = 0 Then
        '    Throw New Exception("Employer Phone Director is required")
        'End If
        Return True
    End Function

    Sub SaveAddDirectorDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Customer_Director
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Customer_Entity_Director
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Customer_Entity_Director_ID = intpk
            .LastName = txt_Director_Last_Name.Text
            .Role = cmb_peran.SelectedItemText
        End With
        objListgoAML_vw_Customer_Entity_Director.Add(objNewVWgoAML_Customer)

        With objNewgoAML_Customer
            .PK_goAML_Ref_Customer_Entity_Director_ID = intpk
            ''.FK_Customer_ID = cmb_director.Value
            .Role = cmb_peran.SelectedItemValue
            .Title = txt_Director_Title.Text
            .Gender = cmb_Director_Gender.SelectedItemValue
            .Last_Name = txt_Director_Last_Name.Text
            If txt_Director_BirthDate.RawValue IsNot Nothing Then
                .BirthDate = Convert.ToDateTime(txt_Director_BirthDate.RawValue)
            Else
                .BirthDate = Nothing
            End If

            .Birth_Place = txt_Director_Birth_Place.Text
            .Mothers_Name = txt_Director_Mothers_Name.Text
            .Alias = txt_Director_Alias.Text
            .SSN = txt_Director_SSN.Text
            .Passport_Number = txt_Director_Passport_Number.Text
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            .Passport_Country = cmb_Director_Passport_Country.SelectedItemValue
            'End Update
            .ID_Number = txt_Director_ID_Number.Text
            .Nationality1 = cmb_Director_Nationality1.SelectedItemValue
            .Nationality2 = cmb_Director_Nationality2.SelectedItemValue
            .Nationality3 = cmb_Director_Nationality3.SelectedItemValue
            .Residence = cmb_Director_Residence.SelectedItemValue
            'comment by septian, goAML 5.0.1, 2023-02-12
            .Email = txt_Director_Email.Text
            .Email2 = txt_Director_Email2.Text
            .Email3 = txt_Director_Email3.Text
            .Email4 = txt_Director_Email4.Text
            .Email5 = txt_Director_Email5.Text
            'end comment
            .Occupation = txt_Director_Occupation.Text
            .Employer_Name = txt_Director_employer_name.Text
            .Deceased = Convert.ToBoolean(IIf(cb_Director_Deceased.Checked = True, 1, 0))
            If cb_Director_Deceased.Checked Then
                .Deceased_Date = Convert.ToDateTime(txt_Director_Deceased_Date.RawValue)
            Else
                .Deceased_Date = Nothing
            End If
            .Tax_Number = txt_Director_Tax_Number.Text
            .Tax_Reg_Number = Convert.ToBoolean(IIf(cb_Director_Tax_Reg_Number.Checked = True, 1, 0))
            .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
            .Comments = txt_Director_Comments.Text
        End With

        objListgoAML_Ref_Director.Add(objNewgoAML_Customer)
        For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
            item.FK_To_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
            item.FK_for_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
            item.FK_To_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
            item.FK_for_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
            item.FK_Person_ID = intpk
        Next

        For Each item As GoAMLDAL.Vw_Person_Identification In objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
            item.FK_Person_ID = intpk
        Next

        For Each item As GoAMLDAL.Vw_Director_Addresses In objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
            item.FK_To_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.Vw_Director_Phones In objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
            item.FK_for_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.Vw_Director_Employer_Addresses In objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
            item.FK_To_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.Vw_Director_Employer_Phones In objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
            item.FK_for_Table_ID = intpk
        Next

        'add by Septian, Director, 2023-04-17
        objTempgoAML_Ref_Director2 = goAML_Ref_Customer_Entity_Director_Mapper.Serialize(objNewgoAML_Customer)
        With objTempgoAML_Ref_Director2
            objTempgoAML_Ref_Director2.ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = .PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_Email = GridEmail_Director.objListgoAML_Ref
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_Sanction = GridSanction_Director.objListgoAML_Ref
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_PEP = GridPEP_Director.objListgoAML_Ref
        End With
        If objListgoAML_Ref_Director2 Is Nothing Then
            objListgoAML_Ref_Director2 = New List(Of DataModel.goAML_Ref_Entity_Director)
            objListgoAML_Ref_Director2.Add(objTempgoAML_Ref_Director2)
        Else
            objListgoAML_Ref_Director2.Add(objTempgoAML_Ref_Director2)
        End If
        'end add


        Store_Director_Corp.DataSource = objListgoAML_vw_Customer_Entity_Director.ToList()
        Store_Director_Corp.DataBind()

        FP_Director.Hidden = True
        WindowDetailDirector.Hidden = True
        ClearinputCustomerDirector()
    End Sub

    Sub SaveEditDirectorDetail()


        With objTempCustomerDirectorEdit
            .Role = cmb_peran.SelectedItemText
            .LastName = txt_Director_Last_Name.Text


        End With

        With objTempgoAML_Ref_Director
            .Role = cmb_peran.SelectedItemValue
            .Title = txt_Director_Title.Text
            .Last_Name = txt_Director_Last_Name.Text
            .SSN = txt_Director_SSN.Text
            If txt_Director_BirthDate.RawValue IsNot Nothing Then
                .BirthDate = Convert.ToDateTime(txt_Director_BirthDate.RawValue)
            Else
                .BirthDate = Nothing
            End If
            .FK_Entity_ID = txt_cif.Text ' 2023-10-06, Nael: Mengikutkan CIF di goAML_Ref_Director
            .Birth_Place = txt_Director_Birth_Place.Text
            .Mothers_Name = txt_Director_Mothers_Name.Text
            .Alias = txt_Director_Alias.Text
            .Passport_Number = txt_Director_Passport_Number.Text
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            .Passport_Country = cmb_Director_Passport_Country.SelectedItemValue
            'End Update
            .ID_Number = txt_Director_ID_Number.Text
            .Nationality1 = cmb_Director_Nationality1.SelectedItemValue
            .Nationality2 = cmb_Director_Nationality2.SelectedItemValue
            .Nationality3 = cmb_Director_Nationality3.SelectedItemValue
            .Residence = cmb_Director_Residence.SelectedItemValue
            .Gender = cmb_Director_Gender.SelectedItemValue

            'comment by septian, goAML 5.0.1, 2023-02-12
            .Email = txt_Director_Email.Text
            .Email2 = txt_Director_Email2.Text
            .Email3 = txt_Director_Email3.Text
            .Email4 = txt_Director_Email4.Text
            .Email5 = txt_Director_Email5.Text
            'end comment
            .Occupation = txt_Director_Occupation.Text
            .Employer_Name = txt_Director_employer_name.Text
            .Deceased = Convert.ToBoolean(IIf(cb_Director_Deceased.Checked = True, 1, 0))
            If cb_Director_Deceased.Checked Then
                .Deceased_Date = Convert.ToDateTime(txt_Director_Deceased_Date.RawValue)
            Else
                .Deceased_Date = Nothing
            End If
            .Tax_Number = txt_Director_Tax_Number.Text
            .Tax_Reg_Number = Convert.ToBoolean(IIf(cb_Director_Tax_Reg_Number.Checked = True, 1, 0))
            .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
            .Comments = txt_Director_Comments.Text

            For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                item.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                item.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                item.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                item.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
                item.FK_Person_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.Vw_Person_Identification In objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
                item.FK_Person_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.Vw_Director_Addresses In objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                item.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.Vw_Director_Phones In objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                item.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.Vw_Director_Employer_Addresses In objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                item.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.Vw_Director_Employer_Phones In objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                item.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next


        End With

        'add by Septian, Director, 2023-04-17
        objTempgoAML_Ref_Director2 = goAML_Ref_Customer_Entity_Director_Mapper.Serialize(objTempgoAML_Ref_Director)
        With objTempgoAML_Ref_Director2
            objTempgoAML_Ref_Director2.ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = objTempgoAML_Ref_Director.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = objTempgoAML_Ref_Director.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = objTempgoAML_Ref_Director.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = objTempgoAML_Ref_Director.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = objTempgoAML_Ref_Director.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_Email = GridEmail_Director.objListgoAML_Ref
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_Sanction = GridSanction_Director.objListgoAML_Ref
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_PEP = GridPEP_Director.objListgoAML_Ref
        End With
        If objListgoAML_Ref_Director2 Is Nothing Then
            objListgoAML_Ref_Director2 = New List(Of DataModel.goAML_Ref_Entity_Director)
            objListgoAML_Ref_Director2.Add(objTempgoAML_Ref_Director2)
        Else
            ' 2023-10-10, Nael: remove dat objTEmpgoAML_REf_Director2  jika sudah ada, ganti dengan yg baru
            objListgoAML_Ref_Director2.RemoveAll(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objTempgoAML_Ref_Director2.PK_goAML_Ref_Customer_Entity_Director_ID)
            objListgoAML_Ref_Director2.Add(objTempgoAML_Ref_Director2)
        End If
        'end add

        objTempCustomerDirectorEdit = Nothing
        objTempgoAML_Ref_Director = Nothing
        objTempgoAML_Ref_Director2 = Nothing

        Store_Director_Corp.DataSource = objListgoAML_vw_Customer_Entity_Director.ToList()
        Store_Director_Corp.DataBind()

        FP_Director.Hidden = True
        WindowDetailDirector.Hidden = True
        ClearinputCustomerDirector()
    End Sub

    Protected Sub btnBackSaveCustomerDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Director.Hidden = True
            WindowDetailDirector.Hidden = True
            ClearinputCustomerDirector()
            objTempCustomerDirectorEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailEmployerPhone(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditEmployerPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailEmployerPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailEmpDirectorPhone(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditEmpDirectorPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailEmpDirectorPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DeleteRecordTaskDetailEmployerPhone(id As Long)
        Dim objDelVWPhone As GoAMLDAL.Vw_Employer_Phones = objListgoAML_vw_Employer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objxDelPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        If Not objxDelPhone Is Nothing Then
            objListgoAML_Ref_Employer_Phone.Remove(objxDelPhone)
        End If

        If objDelVWPhone IsNot Nothing Then
            objListgoAML_vw_Employer_Phones.Remove(objDelVWPhone)
            Store_Empoloyer_Phone.DataSource = objListgoAML_vw_Employer_Phones.ToList()
            Store_Empoloyer_Phone.DataBind()
        End If

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerPhones()

    End Sub

    Sub LoadDataEditEmployerPhone(id As Long)
        objTempgoAML_Ref_EmployerPhones = objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        objTempEmployerPhonesEdit = objListgoAML_vw_Employer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objTempEmployerPhonesEdit Is Nothing Then
            FormPanelEmpTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            Emp_cb_phone_Contact_Type.IsReadOnly = False
            Emp_cb_phone_Communication_Type.IsReadOnly = False
            Emp_txt_phone_Country_prefix.ReadOnly = False
            Emp_txt_phone_number.ReadOnly = False
            Emp_txt_phone_extension.ReadOnly = False
            Emp_txt_phone_comments.ReadOnly = False
            btn_saveempphone.Hidden = False

            WindowDetail.Title = "Edit Phone"
            ClearinputCustomerEmpPhones()
            With objTempEmployerPhonesEdit
                'Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak
                'Emp_Store_Contact_Type.DataBind()
                'Emp_cb_phone_Contact_Type.SetValue(objTempEmployerPhonesEdit.Tph_Contact_Type)
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_emp_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_emp_contact_type IsNot Nothing Then
                    Emp_cb_phone_Contact_Type.SetTextWithTextValue(obj_emp_contact_type.Kode, obj_emp_contact_type.Keterangan)
                End If

                'Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Emp_Store_Communication_Type.DataBind()
                'Emp_cb_phone_Communication_Type.SetValue(objTempEmployerPhonesEdit.Tph_Communication_Type)
                Dim obj_emp_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_emp_communication_type IsNot Nothing Then
                    Emp_cb_phone_Communication_Type.SetTextWithTextValue(obj_emp_communication_type.Kode, obj_emp_communication_type.Keterangan)
                End If
                'End Update

                Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Emp_txt_phone_number.Text = .tph_number
                Emp_txt_phone_extension.Text = .tph_extension
                Emp_txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataDetailEmployerPhone(id As Long)
        Dim objCustomerPhonesDetail As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelEmpTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            Emp_cb_phone_Contact_Type.IsReadOnly = True
            Emp_cb_phone_Communication_Type.IsReadOnly = True
            Emp_txt_phone_Country_prefix.ReadOnly = True
            Emp_txt_phone_number.ReadOnly = True
            Emp_txt_phone_extension.ReadOnly = True
            Emp_txt_phone_comments.ReadOnly = True
            btn_saveempphone.Hidden = True

            WindowDetail.Title = "Phone Detail"
            ClearinputCustomerEmpPhones()
            With objCustomerPhonesDetail
                'Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Emp_Store_Contact_Type.DataBind()
                'Emp_cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_emp_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_emp_contact_type IsNot Nothing Then
                    Emp_cb_phone_Contact_Type.SetTextWithTextValue(obj_emp_contact_type.Kode, obj_emp_contact_type.Keterangan)
                End If

                'Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Emp_Store_Communication_Type.DataBind()
                'Emp_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                Dim obj_emp_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_emp_communication_type IsNot Nothing Then
                    Emp_cb_phone_Communication_Type.SetTextWithTextValue(obj_emp_communication_type.Kode, obj_emp_communication_type.Keterangan)
                End If
                'End Update
                Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Emp_txt_phone_number.Text = .tph_number
                Emp_txt_phone_extension.Text = .tph_extension
                Emp_txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Protected Sub GrdCmdEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataEmployerAddressDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditEmployerAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailEmployerAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorEmployerAddressDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditDirectorEmployerAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailDirectorEmployerAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Sub DeleteRecordTaskDetailEmployerAddress(id As Long)
        Dim objDelVWAddress As GoAMLDAL.Vw_Employer_Addresses = objListgoAML_vw_Employer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objxDelAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = id)

        If Not objxDelAddress Is Nothing Then
            objListgoAML_Ref_Employer_Address.Remove(objxDelAddress)
        End If

        If Not objDelVWAddress Is Nothing Then
            objListgoAML_vw_Employer_Addresses.Remove(objDelVWAddress)
            Store_Employer_Address.DataSource = objListgoAML_vw_Employer_Addresses.ToList()
            Store_Employer_Address.DataBind()
        End If

        FP_Address_Emp_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerEmpAddress()

    End Sub

    Sub DeleteRecordTaskDetailDirectorEmployerAddress(id As Long)
        Dim objDelVWAddress As GoAMLDAL.Vw_Director_Employer_Addresses = objListgoAML_vw_Director_Employer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objxDelAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = id)

        If Not objxDelAddress Is Nothing Then
            objListgoAML_Ref_Director_Employer_Address.Remove(objxDelAddress)
        End If

        If Not objDelVWAddress Is Nothing Then
            objListgoAML_vw_Director_Employer_Addresses.Remove(objDelVWAddress)
            Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = objTempCustomerDirectorEdit.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 7).ToList()
            Store_Director_Employer_Address.DataBind()
        End If

        FP_Address_Emp_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerEmpAddress()

    End Sub

    Sub LoadDataEmployerAddressDetail(id As Long)

        Dim objCustomerAddressDetail = objListgoAML_Ref_Employer_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        If Not objCustomerAddressDetail Is Nothing Then
            FP_Address_Emp_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            btn_saveempaddress.Hidden = True
            WindowDetailAddress.Title = "Address Detail"
            cmb_emp_kategoriaddress.IsReadOnly = True
            emp_Address_INDV.ReadOnly = True
            emp_Town_INDV.ReadOnly = True
            emp_City_INDV.ReadOnly = True
            emp_Zip_INDV.ReadOnly = True
            cmb_emp_kodenegara.IsReadOnly = True
            emp_State_INDVD.ReadOnly = True
            emp_Comment_Address_INDVD.ReadOnly = True
            ClearinputCustomerEmpAddress()
            'Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Store_emp_KategoriAddress.DataBind()
            Dim kategorikontak_emp_detail = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
            If kategorikontak_emp_detail IsNot Nothing Then
                cmb_emp_kategoriaddress.SetTextWithTextValue(kategorikontak_emp_detail.Kode, kategorikontak_emp_detail.Keterangan)
            End If

            emp_Address_INDV.Text = objCustomerAddressDetail.Address
            emp_Town_INDV.Text = objCustomerAddressDetail.Town
            emp_City_INDV.Text = objCustomerAddressDetail.City
            emp_Zip_INDV.Text = objCustomerAddressDetail.Zip
            'Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_emp_Kode_Negara.DataBind()
            Dim kodenegara_emp_detail = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerAddressDetail.Country_Code)
            If kodenegara_emp_detail IsNot Nothing Then
                cmb_emp_kodenegara.SetTextWithTextValue(kodenegara_emp_detail.Kode, kodenegara_emp_detail.Keterangan)
            End If

            emp_State_INDVD.Text = objCustomerAddressDetail.State
            emp_Comment_Address_INDVD.Text = objCustomerAddressDetail.Comments
        End If
    End Sub

    Sub LoadDataDirectorEmployerAddressDetail(id As Long)

        Dim objCustomerAddressDetail = objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        If Not objCustomerAddressDetail Is Nothing Then
            FP_Address_Emp_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            btn_Director_saveempaddress.Hidden = True
            WindowDetailDirectorAddress.Title = "Address Detail"
            Director_cmb_emp_kategoriaddress.IsReadOnly = True
            Director_emp_Address.ReadOnly = True
            Director_emp_Town.ReadOnly = True
            Director_emp_City.ReadOnly = True
            Director_emp_Zip.ReadOnly = True
            Director_cmb_emp_kodenegara.IsReadOnly = True
            Director_emp_State.ReadOnly = True
            Director_emp_Comment_Address.ReadOnly = True
            ClearinputDirectorEmpAddress()
            'Director_Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Store_emp_KategoriAddress.DataBind()
            Dim kategorikontak_director_emp_detail = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
            If kategorikontak_director_emp_detail IsNot Nothing Then
                Director_cmb_emp_kategoriaddress.SetTextWithTextValue(kategorikontak_director_emp_detail.Kode, kategorikontak_director_emp_detail.Keterangan)
            End If

            Director_emp_Address.Text = objCustomerAddressDetail.Address
            Director_emp_Town.Text = objCustomerAddressDetail.Town
            Director_emp_City.Text = objCustomerAddressDetail.City
            Director_emp_Zip.Text = objCustomerAddressDetail.Zip
            'Director_Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Director_Store_emp_Kode_Negara.DataBind()
            Dim kodenegara_director_emp_detail = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerAddressDetail.Country_Code)
            If kodenegara_director_emp_detail IsNot Nothing Then
                Director_cmb_emp_kodenegara.SetTextWithTextValue(kodenegara_director_emp_detail.Kode, kodenegara_director_emp_detail.Keterangan)
            End If

            Director_emp_State.Text = objCustomerAddressDetail.State
            Director_emp_Comment_Address.Text = objCustomerAddressDetail.Comments
        End If
    End Sub

    Sub LoadDataEditDirectorEmployerAddress(id As Long)
        objTempgoAML_Ref_Director_EmployerAddresses = objListgoAML_Ref_Director_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        objTempDirectorEmployerAddressEdit = objListgoAML_vw_Director_Employer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)

        Dim Address_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Country As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing

        If Not objTempDirectorEmployerAddressEdit Is Nothing Then
            FP_Address_Emp_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            btn_Director_saveempaddress.Hidden = False
            Director_cmb_emp_kategoriaddress.IsReadOnly = False
            Director_emp_Address.ReadOnly = False
            Director_emp_Town.ReadOnly = False
            Director_emp_City.ReadOnly = False
            Director_emp_Zip.ReadOnly = False
            Director_cmb_emp_kodenegara.IsReadOnly = False
            Director_emp_State.ReadOnly = False
            Director_emp_Comment_Address.ReadOnly = False

            WindowDetailDirectorAddress.Title = "Address Edit"
            ClearinputDirectorEmpAddress()
            With objTempDirectorEmployerAddressEdit
                'Director_Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_emp_KategoriAddress.DataBind()
                Dim kategorikontak_emp_director = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                If kategorikontak_emp_director IsNot Nothing Then
                    Director_cmb_emp_kategoriaddress.SetTextWithTextValue(kategorikontak_emp_director.Kode, kategorikontak_emp_director.Keterangan)
                End If
                Director_emp_Address.Text = .Address
                Director_emp_Town.Text = .Town
                Director_emp_City.Text = .City
                Director_emp_Zip.Text = .Zip
                'Director_Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Director_Store_emp_Kode_Negara.DataBind()
                Dim kodenegara_emp_director = goAML_CustomerBLL.GetNamaNegaraByID(.Country_Code)
                If kodenegara_emp_director IsNot Nothing Then
                    Director_cmb_emp_kodenegara.SetTextWithTextValue(kodenegara_emp_director.Kode, kodenegara_emp_director.Keterangan)
                End If

                Director_emp_State.Text = .State
                Director_emp_Comment_Address.Text = .Comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataEditEmployerAddress(id As Long)
        objTempgoAML_Ref_EmployerAddresses = objListgoAML_Ref_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        objTempEmployerAddressEdit = objListgoAML_vw_Employer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)

        Dim Address_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Country As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing

        If Not objTempEmployerAddressEdit Is Nothing Then
            FP_Address_Emp_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            btn_saveempaddress.Hidden = False
            cmb_emp_kategoriaddress.IsReadOnly = False
            emp_Address_INDV.ReadOnly = False
            emp_Town_INDV.ReadOnly = False
            emp_City_INDV.ReadOnly = False
            emp_Zip_INDV.ReadOnly = False
            cmb_emp_kodenegara.IsReadOnly = False
            emp_State_INDVD.ReadOnly = False
            emp_Comment_Address_INDVD.ReadOnly = False

            WindowDetailAddress.Title = "Address Edit"
            ClearinputCustomerEmpAddress()
            With objTempEmployerAddressEdit
                'Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_emp_KategoriAddress.DataBind()
                Dim kategorikontak_emp_edit = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                If kategorikontak_emp_edit IsNot Nothing Then
                    cmb_emp_kategoriaddress.SetTextWithTextValue(kategorikontak_emp_edit.Kode, kategorikontak_emp_edit.Keterangan)
                End If
                emp_Address_INDV.Text = .Address
                emp_Town_INDV.Text = .Town
                emp_City_INDV.Text = .City
                emp_Zip_INDV.Text = .Zip
                'Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Store_emp_Kode_Negara.DataBind()
                Dim kodenegara_emp_edit = goAML_CustomerBLL.GetNamaNegaraByID(.Country_Code)
                If kodenegara_emp_edit IsNot Nothing Then
                    cmb_emp_kodenegara.SetTextWithTextValue(kodenegara_emp_edit.Kode, kodenegara_emp_edit.Keterangan)
                End If

                emp_State_INDVD.Text = .State
                emp_Comment_Address_INDVD.Text = .Comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Protected Sub btnAddEmployerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            If objListgoAML_vw_Employer_Phones.Count = 0 Then
                FormPanelEmpTaskDetail.Hidden = False
                WindowDetail.Hidden = False
                btn_saveempphone.Hidden = False
                Emp_cb_phone_Contact_Type.IsReadOnly = False
                Emp_cb_phone_Communication_Type.IsReadOnly = False
                Emp_txt_phone_Country_prefix.ReadOnly = False
                Emp_txt_phone_number.ReadOnly = False
                Emp_txt_phone_extension.ReadOnly = False
                Emp_txt_phone_comments.ReadOnly = False
                btn_saveempphone.Hidden = False
                WindowDetail.Title = "Employer Phone Add"

                Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
                Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

                'Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Emp_Store_Contact_Type.DataBind()

                'Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Emp_Store_Communication_Type.DataBind()
                ClearinputCustomerEmpPhones()
            Else
                Throw New Exception("can only enter one Workplace Phone Information")
            End If
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    ' Pantau
    Protected Sub btnAddDirectorEmployerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ' 2023-10-05, Nael: Menghilangkan pengecekan objdiretorphone, agar popup bisa muncul
            'Dim objdirectorempphone = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()

            FormPanelEmpDirectorTaskDetail.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            btn_saveDirector_empphone.Hidden = False
            Director_Emp_cb_phone_Contact_Type.IsReadOnly = False
            Director_Emp_cb_phone_Communication_Type.IsReadOnly = False
            Director_Emp_txt_phone_Country_prefix.ReadOnly = False
            Director_Emp_txt_phone_number.ReadOnly = False
            Director_Emp_txt_phone_extension.ReadOnly = False
            Director_Emp_txt_phone_comments.ReadOnly = False
            btn_saveDirector_empphone.Hidden = False
            WindowDetailDirectorPhone.Title = "Employer Phone Add"

            Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing
            'Director_Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Emp_Store_Contact_Type.DataBind()

            'Director_Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
            'Director_Emp_Store_Communication_Type.DataBind()
            ClearinputDirectorEmpPhones()
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddEmployerAddresses_DirectClick(sender As Object, e As DirectEventArgs)

        Try
            ' 2023-10-05, Nael: Menghilangkan pengecekan objdiretorphone, agar popup bisa muncul
            FP_Address_Emp_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            cmb_emp_kategoriaddress.IsReadOnly = False
            emp_Address_INDV.ReadOnly = False
            emp_Town_INDV.ReadOnly = False
            emp_City_INDV.ReadOnly = False
            emp_Zip_INDV.ReadOnly = False
            cmb_emp_kodenegara.IsReadOnly = False
            emp_State_INDVD.ReadOnly = False
            emp_Comment_Address_INDVD.ReadOnly = False
            btn_saveempaddress.Hidden = False
            WindowDetailAddress.Title = "Employer Address Add"

            'Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Store_emp_KategoriAddress.DataBind()

            'Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_emp_Kode_Negara.DataBind()
            ClearinputCustomerEmpAddress()

            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddDirectorEmployerAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ' 2023-10-05, Nael: Menghilangkan pengecekan objdiretoraddress, agar popup bisa muncul
            'Dim objdirectorempaddress = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList

            FP_Address_Emp_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            Director_cmb_emp_kategoriaddress.IsReadOnly = False
            Director_emp_Address.ReadOnly = False
            Director_emp_Town.ReadOnly = False
            Director_emp_City.ReadOnly = False
            Director_emp_Zip.ReadOnly = False
            Director_cmb_emp_kodenegara.IsReadOnly = False
            Director_emp_State.ReadOnly = False
            Director_emp_Comment_Address.ReadOnly = False
            btn_Director_saveempaddress.Hidden = False
            WindowDetailDirectorAddress.Title = "Employer Address Add"


            'Director_Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Store_emp_KategoriAddress.DataBind()

            'Director_Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Director_Store_emp_Kode_Negara.DataBind()
            ClearinputDirectorEmpAddress()

            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#Region "4 Januari 2023, Septian, goAML 5.0.1"
    Protected Sub btnAdd_Customer_Previous_Name_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            'If objListgoAML_vw_Previous_Name.Count = 0 Then
            FormPanelPreviousName.Hidden = False
            WindowDetailPreviousName.Hidden = False
            btn_Customer_SavePreviousName.Hidden = False
            txtCustomer_first_name.ReadOnly = False
            txtCustomer_last_name.ReadOnly = False
            txtCustomer_comments.ReadOnly = False

            WindowDetailPreviousName.Title = "Previous Name Add"


            ClearinputCustomerPreviousName()
            'Else
            '    Throw New Exception("can only enter one Workplace Phone Information")
            'End If
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_Email_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            If txt_cif.Text.Trim = "" Then
                Throw New Exception("CIF is required")
            End If
            WindowDetail_Email.Hidden = False
            FormPanel_Email.Hidden = False
            btn_Customer_Save_Email.Hidden = False

            txtCustomer_email_address.ReadOnly = False

            WindowDetail_Email.Title = "Email Add"

            objTempCustomer_Email_Edit = Nothing
            'FormPanel_Email.Reset(False)
            ClearinputCustomer_Email()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_Employment_History_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_Employment_History.Hidden = False
            FormPanel_Employment_History.Hidden = False
            btn_Customer_Save_Employment_History.Hidden = False

            txt_employer_name.ReadOnly = False
            txt_employer_business.ReadOnly = False
            txt_employer_identifier.ReadOnly = False
            df_valid_from.ReadOnly = False
            cbx_is_approx_from_date.ReadOnly = False
            df_valid_to.ReadOnly = False
            cbx_is_approx_to_date.ReadOnly = False

            WindowDetail_Employment_History.Title = "Employment History Add"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_PersonPEP_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ' START: 2023-10-16, Nael
            If txt_cif.Text.Trim = "" Then
                Throw New Exception("CIF is required")
            End If
            ' END: 2023-10-16, Nael

            WindowDetail_Person_PEP.Hidden = False
            FormPanel_Person_PEP.Hidden = False
            btn_Customer_Save_PEP.Hidden = False

            cmb_PersonPEP_Country.IsReadOnly = False
            txt_function_name.ReadOnly = False
            txt_function_description.ReadOnly = False
            df_person_pep_valid_from.ReadOnly = False
            cbx_pep_is_approx_from_date.ReadOnly = False
            df_pep_valid_to.ReadOnly = False
            cbx_pep_is_approx_to_date.ReadOnly = False
            txt_pep_comments.ReadOnly = False

            WindowDetail_Person_PEP.Title = "Person PEP Add"

            objTempCustomer_PersonPEP_Edit = Nothing
            'FormPanel_Person_PEP.Reset()
            ClearinputCustomer_PersonPEP()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_NetworkDevice_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_NetworkDevice.Hidden = False
            FormPanel_NetworkDevice.Hidden = False
            btnCustomer_Save_NetworkDevice.Hidden = False

            txt_device_number.ReadOnly = False
            cmb_network_device_os.IsReadOnly = False
            txt_service_provider.ReadOnly = False
            txt_ipv6.ReadOnly = False
            txt_ipv4.ReadOnly = False
            txt_cgn_port.ReadOnly = False
            txt_ipv4_ipv6.ReadOnly = False
            txt_nat.ReadOnly = False
            df_first_seen_date.ReadOnly = False
            df_last_seen_date.ReadOnly = False
            cbx_using_proxy.ReadOnly = False
            txt_city.ReadOnly = False
            cmb_network_device_country.IsReadOnly = False
            txt_network_device_comments.ReadOnly = False

            WindowDetail_NetworkDevice.Title = "Network Device Add"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_SocialMedia_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_SocialMedia.Hidden = False
            FormPanel_SocialMedia.Hidden = False

            btnSaveCustomer_SocialMedia.Hidden = False

            txt_socmed_platform.ReadOnly = False
            txt_socmed_username.ReadOnly = False
            txt_socmed_comments.ReadOnly = False

            WindowDetail_SocialMedia.Title = "Social Media Add"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_Sanction_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ' START: 2023-10-16, Nael
            If txt_cif.Text.Trim = "" Then
                Throw New Exception("CIF is required")
            End If
            ' END: 2023-10-16, Nael
            objTempCustomer_Sanction_Edit = Nothing
            objTemp_goAML_Ref_Customer_Sanction = Nothing

            WindowDetail_Sanction.Hidden = False
            FormPanel_Sanction.Hidden = False
            btnSaveCustomer_Sanction.Hidden = False

            txt_sanction_provider.ReadOnly = False
            txt_sanction_sanction_list_name.ReadOnly = False
            txt_sanction_match_criteria.ReadOnly = False
            txt_sanction_link_to_source.ReadOnly = False
            txt_sanction_sanction_list_attributes.ReadOnly = False
            df_sanction_valid_from.ReadOnly = False
            cbx_sanction_is_approx_from_date.ReadOnly = False
            df_sanction_valid_to.ReadOnly = False
            cbx_sanction_is_approx_to_date.ReadOnly = False
            txt_sanction_comments.ReadOnly = False

            WindowDetail_Sanction.Title = "Sanction Add"

            objTempCustomer_Sanction_Edit = Nothing
            'FormPanel_Sanction.Reset()
            ClearinputCustomer_Sanction()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_RelatedPerson_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ' START: 2023-10-16, Nael
            If txt_cif.Text.Trim = "" Then
                Throw New Exception("CIF is required")
            End If
            ' END: 2023-10-16, Nael
            GridAddressWork_RelatedPerson.IsViewMode = False
            GridAddress_RelatedPerson.IsViewMode = False
            GridPhone_RelatedPerson.IsViewMode = False
            GridPhoneWork_RelatedPerson.IsViewMode = False
            GridIdentification_RelatedPerson.IsViewMode = False
            GridEmail_RelatedPerson.IsViewMode = False
            GridSanction_RelatedPerson.IsViewMode = False
            GridPEP_RelatedPerson.IsViewMode = False

            GridAddressWork_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Address))
            GridAddress_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Address))
            GridPhone_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Phone))
            GridPhoneWork_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Phone))
            GridIdentification_RelatedPerson.LoadData(New List(Of DataModel.goAML_Person_Identification))
            GridEmail_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Customer_Email))
            GridSanction_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Customer_Sanction))
            GridPEP_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Customer_PEP))

            WindowDetail_RelatedPerson.Hidden = False
            FormPanel_RelatedPerson.Hidden = False
            btnSaveCustomer_RelatedPerson.Hidden = False

            cmb_rp_person_relation.IsReadOnly = False
            txt_rp_relation_comments.ReadOnly = False
            'df_relation_date_range_valid_from.ReadOnly = False
            'cbx_relation_date_range_is_approx_from_date.ReadOnly = False
            'df_relation_date_range_valid_to.ReadOnly = False
            'cbx_relation_date_range_is_approx_to_date.ReadOnly = False
            txt_rp_comments.ReadOnly = False
            cmb_rp_gender.IsReadOnly = False
            txt_rp_title.ReadOnly = False
            'txt_rp_first_name.ReadOnly = False
            'txt_rp_middle_name.ReadOnly = False
            'txt_rp_prefix.ReadOnly = False
            txt_rp_last_name.ReadOnly = False
            df_birthdate.ReadOnly = False
            txt_rp_birth_place.ReadOnly = False
            'cmb_rp_country_of_birth.IsReadOnly = False
            txt_rp_mothers_name.ReadOnly = False
            txt_rp_alias.ReadOnly = False
            'txt_rp_full_name_frn.ReadOnly = False
            txt_rp_ssn.ReadOnly = False
            txt_rp_passport_number.ReadOnly = False
            cmb_rp_passport_country.IsReadOnly = False
            txt_rp_id_number.ReadOnly = False
            cmb_rp_nationality1.IsReadOnly = False
            cmb_rp_nationality2.IsReadOnly = False
            cmb_rp_nationality3.IsReadOnly = False
            cmb_rp_residence.IsReadOnly = False
            'df_rp_residence_since.ReadOnly = False
            txt_rp_occupation.ReadOnly = False
            txt_rp_employer_name.ReadOnly = False
            cbx_rp_deceased.ReadOnly = False
            df_rp_date_deceased.ReadOnly = False
            txt_rp_tax_number.ReadOnly = False
            cbx_rp_tax_reg_number.ReadOnly = False
            txt_rp_source_of_wealth.ReadOnly = False
            cbx_rp_is_protected.ReadOnly = False

            WindowDetail_RelatedPerson.Title = "Related Person Add"

            objTempCustomer_RelatedPerson_Edit = Nothing
            'FormPanel_RelatedPerson.Reset()
            ClearinputCustomer_RelatedPerson()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_AdditionalInfo_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_AdditionalInfo.Hidden = False
            FormPanel_AdditionalInfo.Hidden = False
            btnSaveCustomer_AdditionalInfo.Hidden = False

            cmb_info_type.IsReadOnly = False
            txt_info_subject.ReadOnly = False
            txt_info_text.ReadOnly = False
            nfd_info_numeric.ReadOnly = False
            df_info_date.ReadOnly = False
            cbx_info_boolean.ReadOnly = False

            WindowDetail_AdditionalInfo.Title = "Additional Info Add"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_Url_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            If txt_cif.Text.Trim = "" Then
                Throw New Exception("CIF is required")
            End If
            WindowDetail_URL.Hidden = False
            FormPanel_URL.Hidden = False
            btnSaveCustomer_URL.Hidden = False

            txt_url.ReadOnly = False

            WindowDetail_URL.Title = "Customer Url Add"

            objTempCustomer_EntityUrl_Edit = Nothing
            ClearinputCustomer_Url()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_EntityIdentification_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            If txt_cif.Text.Trim = "" Then
                Throw New Exception("CIF is required")
            End If
            WindowDetail_EntityIdentification.Hidden = False
            FormPanel_EntityIdentification.Hidden = False
            btnSaveCustomer_EntityIdentification.Hidden = False

            cmb_ef_type.IsReadOnly = False
            txt_ef_number.ReadOnly = False
            df_issue_date.ReadOnly = False
            df_expiry_date.ReadOnly = False
            txt_ef_issued_by.ReadOnly = False
            cmb_ef_issue_country.IsReadOnly = False
            txt_ef_comments.ReadOnly = False

            WindowDetail_EntityIdentification.Title = "Entity Identification Add"

            objTempCustomer_EntityIdentification_Edit = Nothing
            'FormPanel_EntityIdentification.Reset()
            ClearinputCustomer_EntityIdentification()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_RelatedEntities_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            If txt_cif.Text.Trim = "" Then
                Throw New Exception("CIF is required")
            End If
            GridPhone_RelatedEntity.IsViewMode = False
            GridAddress_RelatedEntity.IsViewMode = False
            GridEmail_RelatedEntity.IsViewMode = False
            GridEntityIdentification_RelatedEntity.IsViewMode = False
            GridURL_RelatedEntity.IsViewMode = False
            GridSanction_RelatedEntity.IsViewMode = False

            GridPhone_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Phone))
            GridAddress_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Address))
            GridEmail_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_Email))
            GridEntityIdentification_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            GridURL_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_URL))
            GridSanction_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_Sanction))

            WindowDetail_RelatedEntities.Hidden = False
            FormPanel_RelatedEntities.Hidden = False
            btnSaveCustomer_EntityRelation.Hidden = False

            cmb_re_entity_relation.IsReadOnly = False
            'df_re_relation_date_range_valid_from.ReadOnly = False
            'cbx_re_relation_date_range_is_approx_from_date.ReadOnly = False
            'df_re_relation_date_range_valid_to.ReadOnly = False
            'cbx_re_relation_date_range_is_approx_to_date.ReadOnly = False
            'nfd_re_share_percentage.ReadOnly = False
            txt_re_comments.ReadOnly = False
            txt_re_relation_comments.ReadOnly = False
            txt_re_name.ReadOnly = False
            txt_re_commercial_name.ReadOnly = False
            cmb_re_incorporation_legal_form.IsReadOnly = False
            txt_re_incorporation_number.ReadOnly = False
            txt_re_business.ReadOnly = False
            'cmb_re_entity_status.IsReadOnly = False
            'df_re_entity_status_date.ReadOnly = False
            txt_re_incorporation_state.ReadOnly = False
            cmb_re_incorporation_country_code.IsReadOnly = False
            df_re_incorporation_date.ReadOnly = False
            cbx_re_business_closed.ReadOnly = False
            df_re_date_business_closed.ReadOnly = False
            txt_re_tax_number.ReadOnly = False
            txt_re_tax_reg_number.ReadOnly = False

            WindowDetail_RelatedEntities.Title = "Related Entities Add"
            objTempCustomer_RelatedEntities_Edit = Nothing
            'FormPanel_RelatedEntities.Reset(True)
            ClearinputCustomer_RelatedEntities()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_PreviousName_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailPreviousName.Hidden = True
            FormPanelPreviousName.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_Email_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Email.Hidden = True
            FormPanel_Email.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_EmploymentHistory_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Employment_History.Hidden = True
            FormPanel_Employment_History.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_PersonPEP_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Person_PEP.Hidden = True
            FormPanel_Person_PEP.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_NetworkDevice_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_NetworkDevice.Hidden = True
            FormPanel_NetworkDevice.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_SocialMedia_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_SocialMedia.Hidden = True
            FormPanel_SocialMedia.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_Sanction_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Sanction.Hidden = True
            FormPanel_Sanction.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_RelatedPerson_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_RelatedPerson.Hidden = True
            FormPanel_RelatedPerson.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_AdditionalInfo_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_AdditionalInfo.Hidden = True
            FormPanel_AdditionalInfo.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_Url_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_URL.Hidden = True
            FormPanel_URL.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_EntityIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_EntityIdentification.Hidden = True
            FormPanel_EntityIdentification.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_RelatedEntities_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_RelatedEntities.Hidden = True
            FormPanel_RelatedEntities.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_PreviousName_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_PreviousName_Valid() Then
                If objTempCustomerPreviousNameEdit Is Nothing Then
                    SaveAddPreviousName()
                Else
                    SaveEditPreviousName()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_Email_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_Email_Valid() Then
                If objTempCustomer_Email_Edit Is Nothing Then
                    Save_Email()
                Else
                    Save_Email(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_EmploymentHistory_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_EmploymentHistory_Valid() Then
                If objTempCustomer_EmploymentHistory_Edit Is Nothing Then
                    SaveAdd_EmploymentHistory()
                Else
                    SaveEditPreviousName()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_PersonPEP_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_PEP_Valid() Then
                If objTempCustomer_PersonPEP_Edit Is Nothing Then
                    Save_PEP()
                Else
                    Save_PEP(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_NetworkDevice_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_NetworkDevice_Valid() Then
                If objTempCustomer_NetworkDevice_Edit Is Nothing Then
                    SaveAdd_NetworkDevice()
                Else
                    SaveEditPreviousName()
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_SocialMedia_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_SocialMedia_Valid() Then
                If objTempCustomer_SocialMedia_Edit Is Nothing Then
                    SaveAdd_SocialMedia()
                Else
                    SaveEditPreviousName()
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_Sanction_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_Sanction_Valid() Then
                If objTempCustomer_Sanction_Edit Is Nothing Then
                    Save_Sanction()
                Else
                    Save_Sanction(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_RelatedPerson_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_RelatedPerson_Valid() Then
                If objTempCustomer_RelatedPerson_Edit Is Nothing Then
                    Save_RelatedPerson()
                Else
                    Save_RelatedPerson(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_AdditionalInfo_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_AdditionalInformation_Valid() Then
                If objTempCustomer_AdditionalInfo_Edit Is Nothing Then
                    SaveAdd_AdditionalInfo()
                Else
                    SaveEditPreviousName()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_Url_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_URL_Valid() Then
                If objTempCustomer_EntityUrl_Edit Is Nothing Then
                    Save_Url()
                Else
                    Save_Url(False)
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_EntityIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_EntityIdentification_Valid() Then
                If objTempCustomer_EntityIdentification_Edit Is Nothing Then
                    Save_EntityIdentification()
                Else
                    Save_EntityIdentification(False)
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_RelatedEntities_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_RelatedEntities_Valid() Then
                If objTempCustomer_RelatedEntities_Edit Is Nothing Then
                    Save_RelatedEntities()
                Else
                    Save_RelatedEntities(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SaveAddPreviousName()
        Try
            Dim objNewVWgoAML_PreviousName As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name
            Dim objNewgoAML_PreviousName As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name
            Dim objRand As New Random

            Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
            While Not objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = intpk) Is Nothing
                intpk = objRand.Next
            End While

            With objNewVWgoAML_PreviousName
                .PK_goAML_Ref_Customer_Previous_Name_ID = intpk
                .CIF = txt_cif.Value
                .FIRST_NAME = txtCustomer_first_name.Value
                .LAST_NAME = txtCustomer_last_name.Value
                .COMMENTS = txtCustomer_comments.Value
            End With

            With objNewgoAML_PreviousName
                .PK_goAML_Ref_Customer_Previous_Name_ID = intpk
                .CIF = txt_cif.Value
                .FIRST_NAME = txtCustomer_first_name.Value
                .LAST_NAME = txtCustomer_last_name.Value
                .COMMENTS = txtCustomer_comments.Value
            End With

            objListgoAML_vw_Previous_Name.Add(objNewVWgoAML_PreviousName)
            objListgoAML_Ref_Previous_Name.Add(objNewgoAML_PreviousName)

            Store_Customer_Previous_Name.DataSource = objListgoAML_vw_Previous_Name.ToList()
            Store_Customer_Previous_Name.DataBind()

            WindowDetailPreviousName.Hidden = True
            ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub Save_Email(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempCustomer_Email_Edit
                    .CIF = txt_cif.Value
                    .EMAIL_ADDRESS = txtCustomer_email_address.Value
                End With

                With objTemp_goAML_Ref_Customer_Email
                    .CIF = txt_cif.Value
                    .EMAIL_ADDRESS = txtCustomer_email_address.Value
                End With

                objTempCustomer_Email_Edit = Nothing
                objTemp_goAML_Ref_Customer_Email = Nothing
            Else
                Dim objNewVWgoAML_Email As New DataModel.goAML_Ref_Customer_Email
                Dim objNewgoAML_Email As New DataModel.goAML_Ref_Customer_Email
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_Email.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_Email
                    .PK_goAML_Ref_Customer_Email_ID = intpk
                    .CIF = txt_cif.Value
                    .EMAIL_ADDRESS = txtCustomer_email_address.Value
                    .FK_REF_DETAIL_OF = 1 ' 2023-10-09, Nael
                End With

                With objNewgoAML_Email
                    .PK_goAML_Ref_Customer_Email_ID = intpk
                    .CIF = txt_cif.Value
                    .EMAIL_ADDRESS = txtCustomer_email_address.Value
                    .FK_REF_DETAIL_OF = 1 ' 2023-10-09, Nael
                End With

                objListgoAML_vw_Email.Add(objNewVWgoAML_Email)
                objListgoAML_Ref_Email.Add(objNewgoAML_Email)
            End If


            Store_Customer_Email.DataSource = objListgoAML_vw_Email.ToList()
            Store_Customer_Email.DataBind()

            Store_corp_email.DataSource = objListgoAML_vw_Email.ToList()
            Store_corp_email.DataBind()

            WindowDetail_Email.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub SaveAdd_EmploymentHistory()
        Try
            Dim objNewVWgoAML_EmploymentHistory As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History
            Dim objNewgoAML_EmploymentHistory As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History
            Dim objRand As New Random

            Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
            While Not objListgoAML_Ref_EmploymentHistory.Find(Function(x) x.PK_goAML_Ref_Customer_Employment_History_ID = intpk) Is Nothing
                intpk = objRand.Next
            End While

            With objNewVWgoAML_EmploymentHistory
                .PK_goAML_Ref_Customer_Employment_History_ID = intpk
                .CIF = txt_cif.Value
                .EMPLOYER_NAME = txt_employer_name.Value
                .EMPLOYER_BUSINESS = txt_employer_business.Value
                .EMPLOYER_IDENTIFIER = txt_employer_identifier.Value
                .EMPLOYMENT_PERIOD_VALID_FROM = df_valid_from.Value
                .EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE = cbx_is_approx_from_date.Value
                .EMPLOYMENT_PERIOD_VALID_TO = df_valid_to.Value
                .EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE = cbx_is_approx_to_date.Value
            End With

            With objNewgoAML_EmploymentHistory
                .PK_goAML_Ref_Customer_Employment_History_ID = intpk
                .CIF = txt_cif.Value
                .EMPLOYER_NAME = txt_employer_name.Value
                .EMPLOYER_BUSINESS = txt_employer_business.Value
                .EMPLOYER_IDENTIFIER = txt_employer_identifier.Value
                .EMPLOYMENT_PERIOD_VALID_FROM = df_valid_from.Value
                .EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE = cbx_is_approx_from_date.Value
                .EMPLOYMENT_PERIOD_VALID_TO = df_valid_to.Value
                .EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE = cbx_is_approx_to_date.Value
            End With

            objListgoAML_vw_EmploymentHistory.Add(objNewVWgoAML_EmploymentHistory)
            objListgoAML_Ref_EmploymentHistory.Add(objNewgoAML_EmploymentHistory)

            Store_Customer_Employment_History.DataSource = objListgoAML_vw_EmploymentHistory.ToList()
            Store_Customer_Employment_History.DataBind()

            WindowDetail_Employment_History.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub Save_PEP(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempCustomer_PersonPEP_Edit
                    .CIF = txt_cif.Value
                    .PEP_COUNTRY = cmb_PersonPEP_Country.SelectedItemValue
                    .FUNCTION_NAME = txt_function_name.Value
                    .FUNCTION_DESCRIPTION = txt_function_description.Value
                    .PEP_DATE_RANGE_VALID_FROM = df_person_pep_valid_from.Value
                    .PEP_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_pep_is_approx_from_date.Value
                    .PEP_DATE_RANGE_VALID_TO = df_pep_valid_to.Value
                    .PEP_DATE_RANGE_IS_APPROX_TO_DATE = cbx_pep_is_approx_to_date.Value
                    .COMMENTS = txt_pep_comments.Value
                End With

                With objTemp_goAML_Ref_Customer_PEP
                    .CIF = txt_cif.Value
                    .PEP_COUNTRY = cmb_PersonPEP_Country.SelectedItemValue
                    .FUNCTION_NAME = txt_function_name.Value
                    .FUNCTION_DESCRIPTION = txt_function_description.Value
                    .PEP_DATE_RANGE_VALID_FROM = df_person_pep_valid_from.Value
                    .PEP_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_pep_is_approx_from_date.Value
                    .PEP_DATE_RANGE_VALID_TO = df_pep_valid_to.Value
                    .PEP_DATE_RANGE_IS_APPROX_TO_DATE = cbx_pep_is_approx_to_date.Value
                    .COMMENTS = txt_pep_comments.Value
                End With

                objTempCustomer_PersonPEP_Edit = Nothing
                objTemp_goAML_Ref_Customer_PEP = Nothing
            Else
                Dim objNewVWgoAML_PersonPEP As New DataModel.goAML_Ref_Customer_PEP
                Dim objNewgoAML_PersonPEP As New DataModel.goAML_Ref_Customer_PEP
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_PersonPEP.Find(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_PersonPEP
                    .PK_goAML_Ref_Customer_PEP_ID = intpk
                    .CIF = txt_cif.Value
                    .PEP_COUNTRY = cmb_PersonPEP_Country.SelectedItemValue
                    .FUNCTION_NAME = txt_function_name.Value
                    .FUNCTION_DESCRIPTION = txt_function_description.Value
                    .PEP_DATE_RANGE_VALID_FROM = df_person_pep_valid_from.Value
                    .PEP_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_pep_is_approx_from_date.Value
                    .PEP_DATE_RANGE_VALID_TO = df_pep_valid_to.Value
                    .PEP_DATE_RANGE_IS_APPROX_TO_DATE = cbx_pep_is_approx_to_date.Value
                    .COMMENTS = txt_pep_comments.Value
                    .FK_REF_DETAIL_OF = 1 ' 2023-10-10, Nael
                End With

                With objNewgoAML_PersonPEP
                    .PK_goAML_Ref_Customer_PEP_ID = intpk
                    .CIF = txt_cif.Value
                    .PEP_COUNTRY = cmb_PersonPEP_Country.SelectedItemValue
                    .FUNCTION_NAME = txt_function_name.Value
                    .FUNCTION_DESCRIPTION = txt_function_description.Value
                    .PEP_DATE_RANGE_VALID_FROM = df_person_pep_valid_from.Value
                    .PEP_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_pep_is_approx_from_date.Value
                    .PEP_DATE_RANGE_VALID_TO = df_pep_valid_to.Value
                    .PEP_DATE_RANGE_IS_APPROX_TO_DATE = cbx_pep_is_approx_to_date.Value
                    .COMMENTS = txt_pep_comments.Value
                    .FK_REF_DETAIL_OF = 1 ' 2023-10-10, Nael
                End With

                objListgoAML_vw_PersonPEP.Add(objNewVWgoAML_PersonPEP)
                objListgoAML_Ref_PersonPEP.Add(objNewgoAML_PersonPEP)
            End If

            Store_Customer_Person_PEP.DataSource = objListgoAML_vw_PersonPEP.ToList()
            Store_Customer_Person_PEP.DataBind()

            WindowDetail_Person_PEP.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub SaveAdd_NetworkDevice()
        Try
            Dim objNewVWgoAML_Network_Devices As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device
            Dim objNewgoAML_Network_Devices As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device
            Dim objRand As New Random

            Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
            While Not objListgoAML_Ref_NetworkDevice.Find(Function(x) x.PK_goAML_Ref_Customer_Network_Device_ID = intpk) Is Nothing
                intpk = objRand.Next
            End While

            With objNewVWgoAML_Network_Devices
                .PK_goAML_Ref_Customer_Network_Device_ID = intpk
                .CIF = txt_cif.Value
                .DEVICE_NUMBER = txt_device_number.Value
                .OPERATING_SYSTEM = cmb_network_device_os.SelectedItemValue
                .SERVICE_PROVIDER = txt_service_provider.Value
                .IPV6 = txt_ipv6.Value
                .IPV4 = txt_ipv4.Value
                .CGN_PORT = txt_cgn_port.Value
                .IPV4_IPV6 = txt_ipv4_ipv6.Value
                .NAT = txt_nat.Value
                .FIRST_SEEN_DATE = df_first_seen_date.Value
                .LAST_SEEN_DATE = df_last_seen_date.Value
                .USING_PROXY = cbx_using_proxy.Value
                .CITY = txt_city.Value
                .COUNTRY = cmb_network_device_country.SelectedItemValue
                .COMMENTS = txt_network_device_comments.Value
            End With

            With objNewgoAML_Network_Devices
                .PK_goAML_Ref_Customer_Network_Device_ID = intpk
                .CIF = txt_cif.Value
                .DEVICE_NUMBER = txt_device_number.Value
                .OPERATING_SYSTEM = cmb_network_device_os.SelectedItemValue
                .SERVICE_PROVIDER = txt_service_provider.Value
                .IPV6 = txt_ipv6.Value
                .IPV4 = txt_ipv4.Value
                .CGN_PORT = txt_cgn_port.Value
                .IPV4_IPV6 = txt_ipv4_ipv6.Value
                .NAT = txt_nat.Value
                .FIRST_SEEN_DATE = df_first_seen_date.Value
                .LAST_SEEN_DATE = df_last_seen_date.Value
                .USING_PROXY = cbx_using_proxy.Value
                .CITY = txt_city.Value
                .COUNTRY = cmb_network_device_country.SelectedItemValue
                .COMMENTS = txt_network_device_comments.Value
            End With

            objListgoAML_vw_NetworkDevice.Add(objNewVWgoAML_Network_Devices)
            objListgoAML_Ref_NetworkDevice.Add(objNewgoAML_Network_Devices)

            Store_Customer_Network_Device.DataSource = objListgoAML_vw_NetworkDevice.ToList()
            Store_Customer_Network_Device.DataBind()

            StoreCorp_Network_Device.DataSource = objListgoAML_vw_NetworkDevice.ToList()
            StoreCorp_Network_Device.DataBind()

            WindowDetail_NetworkDevice.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
    Sub SaveAdd_SocialMedia()
        Try
            Dim objNewVWgoAML_SocialMedia As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media
            Dim objNewgoAML_SocialMedia As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media
            Dim objRand As New Random

            Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
            While Not objListgoAML_Ref_SocialMedia.Find(Function(x) x.PK_goAML_Ref_Customer_Social_Media_ID = intpk) Is Nothing
                intpk = objRand.Next
            End While

            With objNewVWgoAML_SocialMedia
                .PK_goAML_Ref_Customer_Social_Media_ID = intpk
                .CIF = txt_cif.Value
                .PLATFORM = txt_socmed_platform.Value
                .USER_NAME = txt_socmed_username.Value
                .COMMENTS = txt_socmed_comments.Value
            End With

            With objNewgoAML_SocialMedia
                .PK_goAML_Ref_Customer_Social_Media_ID = intpk
                .CIF = txt_cif.Value
                .PLATFORM = txt_socmed_platform.Value
                .USER_NAME = txt_socmed_username.Value
                .COMMENTS = txt_socmed_comments.Value
            End With

            objListgoAML_vw_SocialMedia.Add(objNewVWgoAML_SocialMedia)
            objListgoAML_Ref_SocialMedia.Add(objNewgoAML_SocialMedia)

            Store_Customer_Social_Media.DataSource = objListgoAML_vw_SocialMedia.ToList()
            Store_Customer_Social_Media.DataBind()

            WindowDetail_SocialMedia.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub Save_Sanction(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempCustomer_Sanction_Edit
                    .CIF = txt_cif.Value
                    .PROVIDER = txt_sanction_provider.Value
                    .SANCTION_LIST_NAME = txt_sanction_sanction_list_name.Value
                    .MATCH_CRITERIA = txt_sanction_match_criteria.Value
                    .LINK_TO_SOURCE = txt_sanction_link_to_source.Value
                    .SANCTION_LIST_ATTRIBUTES = txt_sanction_sanction_list_attributes.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_FROM = df_sanction_valid_from.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_sanction_is_approx_from_date.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_TO = df_sanction_valid_to.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = cbx_sanction_is_approx_to_date.Value
                    .COMMENTS = txt_sanction_comments.Value
                End With

                With objTemp_goAML_Ref_Customer_Sanction
                    .CIF = txt_cif.Value
                    .PROVIDER = txt_sanction_provider.Value
                    .SANCTION_LIST_NAME = txt_sanction_sanction_list_name.Value
                    .MATCH_CRITERIA = txt_sanction_match_criteria.Value
                    .LINK_TO_SOURCE = txt_sanction_link_to_source.Value
                    .SANCTION_LIST_ATTRIBUTES = txt_sanction_sanction_list_attributes.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_FROM = df_sanction_valid_from.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_sanction_is_approx_from_date.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_TO = df_sanction_valid_to.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = cbx_sanction_is_approx_to_date.Value
                    .COMMENTS = txt_sanction_comments.Value
                End With


            Else


                Dim objNewVWgoAML_Sanction As New DataModel.goAML_Ref_Customer_Sanction
                Dim objNewgoAML_Sanction As New DataModel.goAML_Ref_Customer_Sanction
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_Sanction.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_Sanction
                    .PK_goAML_Ref_Customer_Sanction_ID = intpk
                    .CIF = txt_cif.Value
                    .PROVIDER = txt_sanction_provider.Value
                    .SANCTION_LIST_NAME = txt_sanction_sanction_list_name.Value
                    .MATCH_CRITERIA = txt_sanction_match_criteria.Value
                    .LINK_TO_SOURCE = txt_sanction_link_to_source.Value
                    .SANCTION_LIST_ATTRIBUTES = txt_sanction_sanction_list_attributes.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_FROM = df_sanction_valid_from.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_sanction_is_approx_from_date.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_TO = df_sanction_valid_to.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = cbx_sanction_is_approx_to_date.Value
                    .COMMENTS = txt_sanction_comments.Value
                End With

                With objNewgoAML_Sanction
                    .PK_goAML_Ref_Customer_Sanction_ID = intpk
                    .CIF = txt_cif.Value
                    .PROVIDER = txt_sanction_provider.Value
                    .SANCTION_LIST_NAME = txt_sanction_sanction_list_name.Value
                    .MATCH_CRITERIA = txt_sanction_match_criteria.Value
                    .LINK_TO_SOURCE = txt_sanction_link_to_source.Value
                    .SANCTION_LIST_ATTRIBUTES = txt_sanction_sanction_list_attributes.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_FROM = df_sanction_valid_from.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_sanction_is_approx_from_date.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_TO = df_sanction_valid_to.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = cbx_sanction_is_approx_to_date.Value
                    .COMMENTS = txt_sanction_comments.Value
                    ' 2023-10-10, Nael: Karena customer maka fk_ref_detail_of nya 1
                    .FK_REF_DETAIL_OF = 1
                End With

                objListgoAML_vw_Sanction.Add(objNewgoAML_Sanction)
                objListgoAML_Ref_Sanction.Add(objNewgoAML_Sanction)
            End If

            Store_Customer_Sanction.DataSource = objListgoAML_vw_Sanction.ToList()
            Store_Customer_Sanction.DataBind()

            Store_corp_sanction.DataSource = objListgoAML_vw_Sanction.ToList()
            Store_corp_sanction.DataBind()

            WindowDetail_Sanction.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub Save_RelatedPerson(Optional isNew As Boolean = True)
        Try
            If Not isNew Then

                With objTempCustomer_RelatedPerson_Edit
                    .CIF = txt_cif.Value
                    .PERSON_PERSON_RELATION = cmb_rp_person_relation.SelectedItemValue
                    .RELATION_COMMENTS = txt_rp_relation_comments.Value
                    '.RELATION_DATE_RANGE_VALID_FROM = df_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_relation_date_range_is_approx_to_date.Value
                    .COMMENTS = txt_rp_comments.Value
                    .GENDER = cmb_rp_gender.SelectedItemValue
                    .TITLE = txt_rp_title.Value
                    '.FIRST_NAME = txt_rp_first_name.Value
                    '.MIDDLE_NAME = txt_rp_middle_name.Value
                    '.PREFIX = txt_rp_prefix.Value
                    .LAST_NAME = txt_rp_last_name.Value
                    If df_birthdate.RawValue IsNot Nothing Then
                        .BIRTHDATE = df_birthdate.Value
                    End If
                    .BIRTH_PLACE = txt_rp_birth_place.Value
                    '.COUNTRY_OF_BIRTH = cmb_rp_country_of_birth.SelectedItemValue
                    .MOTHERS_NAME = txt_rp_mothers_name.Value
                    .ALIAS = txt_rp_alias.Value
                    '.FULL_NAME_FRN = txt_rp_full_name_frn.Value
                    .SSN = txt_rp_ssn.Value
                    .PASSPORT_NUMBER = txt_rp_passport_number.Value
                    .PASSPORT_COUNTRY = cmb_rp_passport_country.SelectedItemValue
                    .ID_NUMBER = txt_rp_id_number.Value
                    .NATIONALITY1 = cmb_rp_nationality1.SelectedItemValue
                    .NATIONALITY2 = cmb_rp_nationality2.SelectedItemValue
                    .NATIONALITY3 = cmb_rp_nationality3.SelectedItemValue
                    .RESIDENCE = cmb_rp_residence.SelectedItemValue
                    '.RESIDENCE_SINCE = df_rp_residence_since.Value
                    .OCCUPATION = txt_rp_occupation.Value
                    .EMPLOYER_NAME = txt_rp_employer_name.Value ' 2023-10-17, Nael
                    .DECEASED = cbx_rp_deceased.Value
                    If df_rp_date_deceased.RawValue IsNot Nothing Then
                        .DATE_DECEASED = df_rp_date_deceased.Value
                    End If
                    .TAX_NUMBER = txt_rp_tax_number.Value
                    .TAX_REG_NUMBER = cbx_rp_tax_reg_number.Value
                    .SOURCE_OF_WEALTH = txt_rp_source_of_wealth.Value
                    .IS_PROTECTED = cbx_rp_is_protected.Value

                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridIdentification_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridSanction_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPEP_RelatedPerson.objListgoAML_Ref)
                End With

                With objTemp_goAML_Ref_Customer_Related_Person
                    .CIF = txt_cif.Value
                    .PERSON_PERSON_RELATION = cmb_rp_person_relation.SelectedItemValue
                    .RELATION_COMMENTS = txt_rp_relation_comments.Value
                    '.RELATION_DATE_RANGE_VALID_FROM = df_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_relation_date_range_is_approx_to_date.Value
                    .COMMENTS = txt_rp_comments.Value
                    .GENDER = cmb_rp_gender.SelectedItemValue
                    .TITLE = txt_rp_title.Value
                    '.FIRST_NAME = txt_rp_first_name.Value
                    '.MIDDLE_NAME = txt_rp_middle_name.Value
                    '.PREFIX = txt_rp_prefix.Value
                    .LAST_NAME = txt_rp_last_name.Value
                    If df_birthdate.RawValue IsNot Nothing Then
                        .BIRTHDATE = df_birthdate.Value
                    End If
                    .BIRTH_PLACE = txt_rp_birth_place.Value
                    '.COUNTRY_OF_BIRTH = cmb_rp_country_of_birth.SelectedItemValue
                    .MOTHERS_NAME = txt_rp_mothers_name.Value
                    .ALIAS = txt_rp_alias.Value
                    '.FULL_NAME_FRN = txt_rp_full_name_frn.Value
                    .SSN = txt_rp_ssn.Value
                    .PASSPORT_NUMBER = txt_rp_passport_number.Value
                    .PASSPORT_COUNTRY = cmb_rp_passport_country.SelectedItemValue
                    .ID_NUMBER = txt_rp_id_number.Value
                    .NATIONALITY1 = cmb_rp_nationality1.SelectedItemValue
                    .NATIONALITY2 = cmb_rp_nationality2.SelectedItemValue
                    .NATIONALITY3 = cmb_rp_nationality3.SelectedItemValue
                    .RESIDENCE = cmb_rp_residence.SelectedItemValue
                    '.RESIDENCE_SINCE = df_rp_residence_since.Value
                    .OCCUPATION = txt_rp_occupation.Value
                    .EMPLOYER_NAME = txt_rp_employer_name.Value
                    .DECEASED = cbx_rp_deceased.Value
                    If df_rp_date_deceased.RawValue IsNot Nothing Then
                        .DATE_DECEASED = df_rp_date_deceased.Value
                    End If
                    .TAX_NUMBER = txt_rp_tax_number.Value
                    .TAX_REG_NUMBER = cbx_rp_tax_reg_number.Value
                    .SOURCE_OF_WEALTH = txt_rp_source_of_wealth.Value
                    .IS_PROTECTED = cbx_rp_is_protected.Value

                    .ObjList_GoAML_Ref_Customer_Email = New List(Of DataModel.goAML_Ref_Customer_Email)

                    'For Each element In GridEmail_RelatedPerson.objListgoAML_Ref
                    '    Dim obj_goAML_Ref_Customer_Email = New DataModel.goAML_Ref_Customer_Email
                    '    obj_goAML_Ref_Customer_Email.PK_goAML_Ref_Customer_Email_ID = element.PK_goAML_Ref_Customer_Email_ID
                    '    obj_goAML_Ref_Customer_Email.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                    '    obj_goAML_Ref_Customer_Email.CIF = objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID
                    '    obj_goAML_Ref_Customer_Email.EMAIL_ADDRESS = element.EMAIL_ADDRESS

                    '    .ObjList_GoAML_Ref_Customer_Email.Add(obj_goAML_Ref_Customer_Email)
                    'Next
                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridIdentification_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridSanction_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridPEP_RelatedPerson.objListgoAML_Ref)
                End With

                objTempCustomer_RelatedPerson_Edit = Nothing
                objTemp_goAML_Ref_Customer_Related_Person = Nothing
            Else
                Dim objNewVWgoAML_RelatedPerson As New DataModel.goAML_Ref_Customer_Related_Person
                Dim objNewgoAML_RelatedPerson As New DataModel.goAML_Ref_Customer_Related_Person
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_RelatedPerson.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_RelatedPerson
                    .PK_goAML_Ref_Customer_Related_Person_ID = intpk
                    .CIF = txt_cif.Value
                    .PERSON_PERSON_RELATION = cmb_rp_person_relation.SelectedItemValue
                    .RELATION_COMMENTS = txt_rp_relation_comments.Value
                    '.RELATION_DATE_RANGE_VALID_FROM = df_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_relation_date_range_is_approx_to_date.Value
                    .COMMENTS = txt_rp_comments.Value
                    .GENDER = cmb_rp_gender.SelectedItemValue
                    .TITLE = txt_rp_title.Value
                    '.FIRST_NAME = txt_rp_first_name.Value
                    '.MIDDLE_NAME = txt_rp_middle_name.Value
                    '.PREFIX = txt_rp_prefix.Value
                    .LAST_NAME = txt_rp_last_name.Value
                    If df_birthdate.RawValue IsNot Nothing Then
                        .BIRTHDATE = df_birthdate.Value
                    End If
                    .BIRTH_PLACE = txt_rp_birth_place.Value
                    '.COUNTRY_OF_BIRTH = cmb_rp_country_of_birth.SelectedItemValue
                    .MOTHERS_NAME = txt_rp_mothers_name.Value
                    .ALIAS = txt_rp_alias.Value
                    '.FULL_NAME_FRN = txt_rp_full_name_frn.Value
                    .SSN = txt_rp_ssn.Value
                    .PASSPORT_NUMBER = txt_rp_passport_number.Value
                    .PASSPORT_COUNTRY = cmb_rp_passport_country.SelectedItemValue
                    .ID_NUMBER = txt_rp_id_number.Value
                    .NATIONALITY1 = cmb_rp_nationality1.SelectedItemValue
                    .NATIONALITY2 = cmb_rp_nationality2.SelectedItemValue
                    .NATIONALITY3 = cmb_rp_nationality3.SelectedItemValue
                    .RESIDENCE = cmb_rp_residence.SelectedItemValue
                    '.RESIDENCE_SINCE = df_rp_residence_since.Value
                    .OCCUPATION = txt_rp_occupation.Value
                    .EMPLOYER_NAME = txt_rp_employer_name.Value ' 2023-10-17, Nael
                    .DECEASED = cbx_rp_deceased.Value
                    If df_rp_date_deceased.RawValue IsNot Nothing Then
                        .DATE_DECEASED = df_rp_date_deceased.Value
                    End If
                    .TAX_NUMBER = txt_rp_tax_number.Value
                    .TAX_REG_NUMBER = cbx_rp_tax_reg_number.Value
                    .SOURCE_OF_WEALTH = txt_rp_source_of_wealth.Value
                    .IS_PROTECTED = cbx_rp_is_protected.Value

                    .ObjList_GoAML_Ref_Customer_Email = New List(Of DataModel.goAML_Ref_Customer_Email)

                    'For Each element In GridEmail_RelatedPerson.objListgoAML_vw
                    '    Dim obj_goAML_Ref_Customer_Email = New DataModel.goAML_Ref_Customer_Email
                    '    obj_goAML_Ref_Customer_Email.PK_goAML_Ref_Customer_Email_ID = element.PK_goAML_Ref_Customer_Email_ID
                    '    obj_goAML_Ref_Customer_Email.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                    '    obj_goAML_Ref_Customer_Email.CIF = objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID
                    '    obj_goAML_Ref_Customer_Email.EMAIL_ADDRESS = element.EMAIL_ADDRESS

                    '    .ObjList_GoAML_Ref_Customer_Email.Add(obj_goAML_Ref_Customer_Email)
                    'Next
                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridIdentification_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridSanction_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridPEP_RelatedPerson.objListgoAML_Ref)
                End With

                With objNewgoAML_RelatedPerson
                    .PK_goAML_Ref_Customer_Related_Person_ID = intpk
                    .CIF = txt_cif.Value
                    .PERSON_PERSON_RELATION = cmb_rp_person_relation.SelectedItemValue
                    .RELATION_COMMENTS = txt_rp_relation_comments.Value
                    '.RELATION_DATE_RANGE_VALID_FROM = df_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_relation_date_range_is_approx_to_date.Value
                    .COMMENTS = txt_rp_comments.Value
                    .GENDER = cmb_rp_gender.SelectedItemValue
                    .TITLE = txt_rp_title.Value
                    '.FIRST_NAME = txt_rp_first_name.Value
                    '.MIDDLE_NAME = txt_rp_middle_name.Value
                    '.PREFIX = txt_rp_prefix.Value
                    .LAST_NAME = txt_rp_last_name.Value
                    If df_birthdate.RawValue IsNot Nothing Then
                        .BIRTHDATE = df_birthdate.Value
                    End If
                    .BIRTH_PLACE = txt_rp_birth_place.Value
                    '.COUNTRY_OF_BIRTH = cmb_rp_country_of_birth.SelectedItemValue
                    .MOTHERS_NAME = txt_rp_mothers_name.Value
                    .ALIAS = txt_rp_alias.Value
                    '.FULL_NAME_FRN = txt_rp_full_name_frn.Value
                    .SSN = txt_rp_ssn.Value
                    .PASSPORT_NUMBER = txt_rp_passport_number.Value
                    .PASSPORT_COUNTRY = cmb_rp_passport_country.SelectedItemValue
                    .ID_NUMBER = txt_rp_id_number.Value
                    .NATIONALITY1 = cmb_rp_nationality1.SelectedItemValue
                    .NATIONALITY2 = cmb_rp_nationality2.SelectedItemValue
                    .NATIONALITY3 = cmb_rp_nationality3.SelectedItemValue
                    .RESIDENCE = cmb_rp_residence.SelectedItemValue
                    '.RESIDENCE_SINCE = df_rp_residence_since.Value
                    .OCCUPATION = txt_rp_occupation.Value
                    .EMPLOYER_NAME = txt_rp_employer_name.Value ' 2023-10-17, Nael
                    .DECEASED = cbx_rp_deceased.Value
                    If df_rp_date_deceased.RawValue IsNot Nothing Then
                        .DATE_DECEASED = df_rp_date_deceased.Value
                    End If
                    .TAX_NUMBER = txt_rp_tax_number.Value
                    .TAX_REG_NUMBER = cbx_rp_tax_reg_number.Value
                    .SOURCE_OF_WEALTH = txt_rp_source_of_wealth.Value
                    .IS_PROTECTED = cbx_rp_is_protected.Value

                    '.ObjList_GoAML_Ref_Customer_Email = New goAML_Ref_Customer_Email GridEmail_RelatedPerson.objListgoAML_Ref}
                    .ObjList_GoAML_Ref_Customer_Email = New List(Of DataModel.goAML_Ref_Customer_Email)

                    ' Save Email
                    'For Each element In GridEmail_RelatedPerson.objListgoAML_Ref
                    '    Dim obj_goAML_Ref_Customer_Email = New DataModel.goAML_Ref_Customer_Email
                    '    obj_goAML_Ref_Customer_Email.PK_goAML_Ref_Customer_Email_ID = element.PK_goAML_Ref_Customer_Email_ID
                    '    obj_goAML_Ref_Customer_Email.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                    '    obj_goAML_Ref_Customer_Email.CIF = objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID
                    '    obj_goAML_Ref_Customer_Email.EMAIL_ADDRESS = objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID

                    '    .ObjList_GoAML_Ref_Customer_Email.Add(obj_goAML_Ref_Customer_Email)
                    'Next

                    ' 2023-10-11, Nael: Karena related person memiliki FK_Person_Type = 31, di component grid-nya default 1, jadi diset di sini
                    Dim relPersonIdentification = GridIdentification_RelatedPerson.objListgoAML_Ref.Select(Function(x)
                                                                                                               x.FK_Person_Type = 31
                                                                                                               Return x
                                                                                                           End Function).ToList()

                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, relPersonIdentification)
                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridSanction_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridPEP_RelatedPerson.objListgoAML_Ref)
                End With

                objListgoAML_vw_RelatedPerson.Add(objNewVWgoAML_RelatedPerson)
                objListgoAML_Ref_RelatedPerson = objListgoAML_vw_RelatedPerson

            End If

            Store_Customer_RelatedPerson.DataSource = objListgoAML_vw_RelatedPerson.ToList()
            Store_Customer_RelatedPerson.DataBind()

            'StoreCorp_Related_person.DataSource = objListgoAML_vw_RelatedPerson.ToList()
            'StoreCorp_Related_person.DataBind()

            WindowDetail_RelatedPerson.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub SaveAdd_AdditionalInfo()
        Try
            Dim objNewVWgoAML_AdditionalInfo As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information
            Dim objNewgoAML_AdditionalInfo As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information
            Dim objRand As New Random

            Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
            While Not objListgoAML_Ref_AdditionalInfo.Find(Function(x) x.PK_goAML_Ref_Customer_Additional_Information_ID = intpk) Is Nothing
                intpk = objRand.Next
            End While

            With objNewVWgoAML_AdditionalInfo
                .PK_goAML_Ref_Customer_Additional_Information_ID = intpk
                .CIF = txt_cif.Value
                .INFO_TYPE = cmb_info_type.SelectedItemValue
                .INFO_SUBJECT = txt_info_subject.Value
                .INFO_TEXT = txt_info_text.Value
                .INFO_NUMERIC = nfd_info_numeric.Value
                .INFO_DATE = df_info_date.Value
                .INFO_BOOLEAN = cbx_info_boolean.Value
            End With

            With objNewgoAML_AdditionalInfo
                .PK_goAML_Ref_Customer_Additional_Information_ID = intpk
                .CIF = txt_cif.Value
                .INFO_TYPE = cmb_info_type.SelectedItemValue
                .INFO_SUBJECT = txt_info_subject.Value
                .INFO_TEXT = txt_info_text.Value
                .INFO_NUMERIC = nfd_info_numeric.Value
                .INFO_DATE = df_info_date.Value
                .INFO_BOOLEAN = cbx_info_boolean.Value
            End With

            objListgoAML_vw_AdditionalInfo.Add(objNewVWgoAML_AdditionalInfo)
            objListgoAML_Ref_AdditionalInfo.Add(objNewgoAML_AdditionalInfo)

            Store_Customer_Additional_Info.DataSource = objListgoAML_vw_AdditionalInfo.ToList()
            Store_Customer_Additional_Info.DataBind()

            Store_Customer_Corp_Additional_Info.DataSource = objListgoAML_vw_AdditionalInfo.ToList()
            Store_Customer_Corp_Additional_Info.DataBind()

            WindowDetail_AdditionalInfo.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub Save_Url(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempCustomer_EntityUrl_Edit
                    .CIF = txt_cif.Value
                    .URL = txt_url.Value
                End With

                With objTemp_goAML_Ref_Customer_URL
                    .CIF = txt_cif.Value
                    .URL = txt_url.Value
                End With

                objTempCustomer_EntityUrl_Edit = Nothing
                objTemp_goAML_Ref_Customer_URL = Nothing
            Else
                Dim objNewVWgoAML_Url As New DataModel.goAML_Ref_Customer_URL
                Dim objNewgoAML_Url As New DataModel.goAML_Ref_Customer_URL
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_EntityUrl.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_Url
                    .PK_goAML_Ref_Customer_URL_ID = intpk
                    .CIF = txt_cif.Value
                    .URL = txt_url.Value
                End With

                With objNewgoAML_Url
                    .PK_goAML_Ref_Customer_URL_ID = intpk
                    .CIF = txt_cif.Value
                    .URL = txt_url.Value
                End With

                objListgoAML_vw_EntityUrl.Add(objNewVWgoAML_Url)
                objListgoAML_Ref_EntityUrl.Add(objNewgoAML_Url)
            End If

            Store_Customer_Entity_URL.DataSource = objListgoAML_vw_EntityUrl.ToList()
            Store_Customer_Entity_URL.DataBind()

            WindowDetail_URL.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
    Sub Save_EntityIdentification(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempCustomer_EntityIdentification_Edit
                    .CIF = txt_cif.Value
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If Not df_issue_date.Value.ToString().Contains("0001") Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If Not df_expiry_date.Value.ToString().Contains("0001") Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                With objTemp_goAML_Ref_Customer_Entity_Identification
                    .CIF = txt_cif.Value
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If Not df_issue_date.Value.ToString().Contains("0001") Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If Not df_expiry_date.Value.ToString().Contains("0001") Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                objTempCustomer_EntityIdentification_Edit = Nothing
                objTemp_goAML_Ref_Customer_Entity_Identification = Nothing
            Else
                Dim objNewVWgoAML_EntityIdentification As New DataModel.goAML_Ref_Customer_Entity_Identification
                Dim objNewgoAML_EntityIdentification As New DataModel.goAML_Ref_Customer_Entity_Identification
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_EntityIdentification
                    .PK_goAML_Ref_Customer_Entity_Identification_ID = intpk
                    .CIF = txt_cif.Value
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If Not df_issue_date.Value.ToString().Contains("0001") Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If Not df_expiry_date.Value.ToString().Contains("0001") Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                With objNewgoAML_EntityIdentification
                    .PK_goAML_Ref_Customer_Entity_Identification_ID = intpk
                    .CIF = txt_cif.Value
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If Not df_issue_date.Value.ToString().Contains("0001") Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If Not df_expiry_date.Value.ToString().Contains("0001") Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                objListgoAML_vw_EntityIdentification.Add(objNewVWgoAML_EntityIdentification)
                objListgoAML_Ref_EntityIdentification.Add(objNewgoAML_EntityIdentification)
            End If

            Store_Customer_Entity_Identification.DataSource = objListgoAML_vw_EntityIdentification.ToList()
            Store_Customer_Entity_Identification.DataBind()

            WindowDetail_EntityIdentification.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
    Sub Save_RelatedEntities(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempCustomer_RelatedEntities_Edit
                    .CIF = txt_cif.Value
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    '.SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    '.ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    '.ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objTempCustomer_RelatedEntities_Edit.PK_goAML_Ref_Customer_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objTempCustomer_RelatedEntities_Edit.PK_goAML_Ref_Customer_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTempCustomer_RelatedEntities_Edit.PK_goAML_Ref_Customer_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objTempCustomer_RelatedEntities_Edit.PK_goAML_Ref_Customer_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objTempCustomer_RelatedEntities_Edit.PK_goAML_Ref_Customer_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objTempCustomer_RelatedEntities_Edit.PK_goAML_Ref_Customer_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                With objTemp_goAML_Ref_Customer_Related_Entities
                    .CIF = txt_cif.Value
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    '.SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    '.ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    '.ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Entities.PK_goAML_Ref_Customer_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Entities.PK_goAML_Ref_Customer_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Entities.PK_goAML_Ref_Customer_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Entities.PK_goAML_Ref_Customer_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Entities.PK_goAML_Ref_Customer_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Entities.PK_goAML_Ref_Customer_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With
                objTempCustomer_RelatedEntities_Edit = Nothing
                objTemp_goAML_Ref_Customer_Related_Entities = Nothing
            Else
                Dim objNewVWgoAML_RelatedEntities As New DataModel.goAML_Ref_Customer_Related_Entities
                Dim objNewgoAML_RelatedEntities As New DataModel.goAML_Ref_Customer_Related_Entities
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_RelatedEntities.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_RelatedEntities
                    .PK_goAML_Ref_Customer_Related_Entities_ID = intpk
                    .CIF = txt_cif.Value
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    '.SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    '.ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    '.ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                With objNewgoAML_RelatedEntities
                    .PK_goAML_Ref_Customer_Related_Entities_ID = intpk
                    .CIF = txt_cif.Value
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    '.SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    '.ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    '.ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                objListgoAML_vw_RelatedEntities.Add(objNewVWgoAML_RelatedEntities)
                objListgoAML_Ref_RelatedEntities = objListgoAML_vw_RelatedEntities
            End If


            Store_Related_Entities.DataSource = objListgoAML_vw_RelatedEntities.ToList()
            Store_Related_Entities.DataBind()

            WindowDetail_RelatedEntities.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub ClearinputCustomerPreviousName()
        txtCustomer_first_name.Text = ""
        txtCustomer_last_name.Text = ""
        txtCustomer_comments.Text = ""

    End Sub

    Sub ClearinputCustomer_Email()
        txtCustomer_email_address.Text = ""

    End Sub

    Sub ClearinputCustomer_PersonPEP()
        cmb_PersonPEP_Country.SetTextValue("")
        txt_function_name.Text = ""
        txt_function_description.Text = ""
        df_person_pep_valid_from.Text = ""
        cbx_pep_is_approx_from_date.Checked = False
        df_pep_valid_to.Text = ""
        cbx_pep_is_approx_to_date.Checked = False
        txt_pep_comments.Text = ""

    End Sub

    Sub ClearinputCustomer_Sanction()
        txt_sanction_provider.Text = ""
        txt_sanction_sanction_list_name.Text = ""
        txt_sanction_match_criteria.Text = ""
        txt_sanction_link_to_source.Text = ""
        txt_sanction_sanction_list_attributes.Text = ""
        df_sanction_valid_from.Text = ""
        cbx_sanction_is_approx_from_date.Checked = False
        df_sanction_valid_to.Text = ""
        cbx_sanction_is_approx_to_date.Checked = False
        txt_sanction_comments.Text = ""

    End Sub

    Sub ClearinputCustomer_RelatedPerson()
        cmb_rp_person_relation.SetTextValue("")
        txt_rp_relation_comments.Value = ""
        'df_relation_date_range_valid_from.Text = ""
        'cbx_relation_date_range_is_approx_from_date.Checked = False
        'df_relation_date_range_valid_to.Text = ""
        'cbx_relation_date_range_is_approx_to_date.Checked = False
        txt_rp_comments.Text = ""
        cmb_rp_gender.SetTextValue("")
        txt_rp_title.Text = ""
        'txt_rp_first_name.Text = ""
        'txt_rp_middle_name.Text = ""
        'txt_rp_prefix.Text = ""
        txt_rp_last_name.Text = ""
        df_birthdate.Text = ""
        txt_rp_birth_place.Text = ""
        'cmb_rp_country_of_birth.SetTextValue("")
        txt_rp_mothers_name.Text = ""
        txt_rp_alias.Text = ""
        'txt_rp_full_name_frn.Text = ""
        txt_rp_ssn.Text = ""
        txt_rp_passport_number.Text = ""
        cmb_rp_passport_country.SetTextValue("")
        txt_rp_id_number.Text = ""
        cmb_rp_nationality1.SetTextValue("")
        cmb_rp_nationality2.SetTextValue("")
        cmb_rp_nationality3.SetTextValue("")
        cmb_rp_residence.SetTextValue("")
        'df_rp_residence_since.Text = ""
        txt_rp_occupation.Text = ""
        txt_rp_employer_name.Text = ""
        cbx_rp_deceased.Checked = False
        df_rp_date_deceased.Text = ""
        txt_rp_tax_number.Text = ""
        cbx_rp_tax_reg_number.Checked = False
        txt_rp_source_of_wealth.Text = ""
        cbx_rp_is_protected.Checked = False

    End Sub

    Sub ClearinputCustomer_Url()
        txt_url.Text = ""


    End Sub

    Sub ClearinputCustomer_RelatedEntities()
        cmb_re_entity_relation.SetTextValue("")
        'df_re_relation_date_range_valid_from.Text = ""
        'cbx_re_relation_date_range_is_approx_from_date.Checked = False
        'df_re_relation_date_range_valid_to.Text = ""
        'cbx_re_relation_date_range_is_approx_to_date.Checked = False
        'nfd_re_share_percentage.Text = ""
        txt_re_comments.Text = ""
        txt_re_relation_comments.Text = ""
        txt_re_name.Text = ""
        txt_re_commercial_name.Text = ""
        cmb_re_incorporation_legal_form.SetTextValue("")
        txt_re_incorporation_number.Text = ""
        txt_re_business.Text = ""
        'cmb_re_entity_status.SetTextValue("")
        'df_re_entity_status_date.Text = ""
        txt_re_incorporation_state.Text = ""
        cmb_re_incorporation_country_code.SetTextValue("")
        df_re_incorporation_date.Text = ""
        cbx_re_business_closed.Checked = False
        df_re_date_business_closed.Text = ""
        txt_re_tax_number.Text = ""
        txt_re_tax_reg_number.Text = ""

    End Sub

    Sub ClearinputCustomer_EntityIdentification()
        cmb_ef_type.SetTextValue("")
        txt_ef_number.Text = ""
        df_issue_date.Text = ""
        df_expiry_date.Text = ""
        txt_ef_issued_by.Text = ""
        cmb_ef_issue_country.SetTextValue("")
        txt_ef_comments.Text = ""

    End Sub
    Protected Sub btnBackCustomerPreviousName_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)

    End Sub

    Protected Sub GrdCmdCustomerPreviousName(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_PreviousName(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_PreviousName(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_Email(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_Email(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_Email(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_Email(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_EmploymentHistory(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_EmploymentHistory(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_EmploymentHistory(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_PEP(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_PEP(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_PEP(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_PEP(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_NetworkDevice(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_NetworkDevice(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_NetworkDevice(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_SocialMedia(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_SocialMedia(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_SocialMedia(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_Sanction(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_Sanction(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_Sanction(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_Sanction(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_RelatedPerson(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_RelatedPerson(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_RelatedPerson(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_RelatedPerson(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_AdditionalInformation(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_AdditionalInformation(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_AdditionalInformation(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_EntityIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_EntityIdentification(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_EntityIdentification(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_EntityIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_URL(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_CustomerURL(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_CustomerURL(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_URL(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_RelatedEntities(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_RelatedEntities(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_RelatedEntities(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_RelatedEntities(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailPreviousName(id As Long)
        Dim objCustomerPreviousName As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name = objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)

        If Not objCustomerPreviousName Is Nothing Then
            FormPanelPreviousName.Hidden = False
            WindowDetailPreviousName.Hidden = False
            txtCustomer_first_name.ReadOnly = True
            txtCustomer_last_name.ReadOnly = True
            txtCustomer_comments.ReadOnly = True
            btn_Customer_SavePreviousName.Hidden = True

            WindowDetailPreviousName.Title = "Previous Name Detail"
            ClearinputCustomerPreviousName()
            With objCustomerPreviousName
                txtCustomer_first_name.Value = .FIRST_NAME
                txtCustomer_last_name.Value = .LAST_NAME
                txtCustomer_comments.Value = .COMMENTS
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataEditPreviousName(id As Long)
        Dim objCustomerPreviousName As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name = objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)

        If Not objCustomerPreviousName Is Nothing Then
            FormPanelPreviousName.Hidden = False
            WindowDetailPreviousName.Hidden = False
            txtCustomer_first_name.ReadOnly = False
            txtCustomer_last_name.ReadOnly = False
            txtCustomer_comments.ReadOnly = False
            btn_Customer_SavePreviousName.Hidden = False

            WindowDetailPreviousName.Title = "Previous Name Edit"
            ClearinputCustomerPreviousName()
            With objCustomerPreviousName
                txtCustomer_first_name.Value = .FIRST_NAME
                txtCustomer_last_name.Value = .LAST_NAME
                txtCustomer_comments.Value = .COMMENTS
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadData_PreviousName(id As Long, isEdit As Boolean)
        Dim objCustomerPreviousName As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name = objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)

        If Not objCustomerPreviousName Is Nothing Then
            FormPanelPreviousName.Hidden = False
            WindowDetailPreviousName.Hidden = False
            txtCustomer_first_name.ReadOnly = Not isEdit
            txtCustomer_last_name.ReadOnly = Not isEdit
            txtCustomer_comments.ReadOnly = Not isEdit
            btn_Customer_SavePreviousName.Hidden = Not isEdit

            WindowDetailPreviousName.Title = "Previous Name " & If(isEdit, "Edit", "Detail")
            ClearinputCustomerPreviousName()
            With objCustomerPreviousName
                txtCustomer_first_name.Value = .FIRST_NAME
                txtCustomer_last_name.Value = .LAST_NAME
                txtCustomer_comments.Value = .COMMENTS
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadData_Email(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Email = objListgoAML_Ref_Email.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id)
            objTempCustomer_Email_Edit = objListgoAML_vw_Email.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id)

            If Not objTempCustomer_Email_Edit Is Nothing Then
                FormPanel_Email.Hidden = False
                WindowDetail_Email.Hidden = False
                btn_Customer_Save_Email.Hidden = Not isEdit

                txtCustomer_email_address.ReadOnly = Not isEdit

                With objTempCustomer_Email_Edit
                    txtCustomer_email_address.Value = .EMAIL_ADDRESS
                End With

                WindowDetail_Email.Title = "Email " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadData_EmploymentHistory(id As Long, isEdit As Boolean)
        Try
            Dim objCustomer_EmploymentHistory As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History =
                objListgoAML_Ref_EmploymentHistory.Find(Function(x) x.PK_goAML_Ref_Customer_Employment_History_ID = id)

            If Not objCustomer_EmploymentHistory Is Nothing Then
                FormPanel_Employment_History.Hidden = False
                WindowDetail_Employment_History.Hidden = False
                btn_Customer_Save_Employment_History.Hidden = Not isEdit

                txt_employer_name.ReadOnly = Not isEdit
                txt_employer_business.ReadOnly = Not isEdit
                txt_employer_identifier.ReadOnly = Not isEdit
                df_valid_from.ReadOnly = Not isEdit
                cbx_is_approx_from_date.ReadOnly = Not isEdit
                df_valid_to.ReadOnly = Not isEdit
                cbx_is_approx_to_date.ReadOnly = Not isEdit

                With objCustomer_EmploymentHistory
                    txt_employer_name.Value = .EMPLOYER_NAME
                    txt_employer_business.Value = .EMPLOYER_BUSINESS
                    txt_employer_identifier.Value = .EMPLOYER_IDENTIFIER
                    df_valid_from.Text = .EMPLOYMENT_PERIOD_VALID_FROM
                    cbx_is_approx_from_date.Checked = .EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE
                    df_valid_to.Text = .EMPLOYMENT_PERIOD_VALID_TO
                    cbx_is_approx_to_date.Checked = .EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE
                End With

                WindowDetail_Employment_History.Title = "Employment History " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_PEP(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_PEP = objListgoAML_Ref_PersonPEP.Find(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id)
            objTempCustomer_PersonPEP_Edit = objListgoAML_vw_PersonPEP.Find(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id)

            If Not objTempCustomer_PersonPEP_Edit Is Nothing Then
                FormPanel_Person_PEP.Hidden = False
                WindowDetail_Person_PEP.Hidden = False
                btn_Customer_Save_PEP.Hidden = Not isEdit

                cmb_PersonPEP_Country.IsReadOnly = Not isEdit
                txt_function_name.ReadOnly = Not isEdit
                txt_function_description.ReadOnly = Not isEdit
                df_person_pep_valid_from.ReadOnly = Not isEdit
                cbx_pep_is_approx_from_date.ReadOnly = Not isEdit
                df_pep_valid_to.ReadOnly = Not isEdit
                cbx_pep_is_approx_to_date.ReadOnly = Not isEdit
                txt_pep_comments.ReadOnly = Not isEdit


                With objTempCustomer_PersonPEP_Edit
                    Dim reference = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .PEP_COUNTRY)

                    If reference IsNot Nothing Then
                        cmb_PersonPEP_Country.SetTextWithTextValue(reference.Kode, reference.Keterangan)
                    End If

                    txt_function_name.Value = .FUNCTION_NAME
                    txt_function_description.Value = .FUNCTION_DESCRIPTION
                    'df_person_pep_valid_from.Text = .PEP_DATE_RANGE_VALID_FROM
                    'cbx_pep_is_approx_from_date.Checked = .PEP_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_pep_valid_to.Text = .PEP_DATE_RANGE_VALID_TO
                    'cbx_pep_is_approx_to_date.Checked = .PEP_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_pep_comments.Text = .COMMENTS
                End With

                WindowDetail_Person_PEP.Title = "Person PEP " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_NetworkDevice(id As Long, isEdit As Boolean)
        Try
            Dim objCustomer_NetworkDevice As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device =
                objListgoAML_Ref_NetworkDevice.Find(Function(x) x.PK_goAML_Ref_Customer_Network_Device_ID = id)

            If Not objCustomer_NetworkDevice Is Nothing Then
                FormPanel_NetworkDevice.Hidden = False
                WindowDetail_NetworkDevice.Hidden = False
                btnCustomer_Save_NetworkDevice.Hidden = Not isEdit

                txt_device_number.ReadOnly = Not isEdit
                cmb_network_device_os.IsReadOnly = Not isEdit
                txt_service_provider.ReadOnly = Not isEdit
                txt_ipv6.ReadOnly = Not isEdit
                txt_ipv4.ReadOnly = Not isEdit
                txt_cgn_port.ReadOnly = Not isEdit
                txt_ipv4_ipv6.ReadOnly = Not isEdit
                txt_nat.ReadOnly = Not isEdit
                df_first_seen_date.ReadOnly = Not isEdit
                df_last_seen_date.ReadOnly = Not isEdit
                cbx_using_proxy.ReadOnly = Not isEdit
                txt_city.ReadOnly = Not isEdit
                cmb_network_device_country.IsReadOnly = Not isEdit
                txt_network_device_comments.ReadOnly = Not isEdit


                With objCustomer_NetworkDevice
                    Dim refOs = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Operating_System", .OPERATING_SYSTEM)
                    Dim refCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .COUNTRY)

                    txt_device_number.Value = .DEVICE_NUMBER
                    If refOs IsNot Nothing Then
                        cmb_network_device_os.SetTextWithTextValue(refOs.Kode, refOs.Keterangan)
                    End If

                    txt_service_provider.Value = .SERVICE_PROVIDER
                    txt_ipv6.Value = .IPV6
                    txt_ipv4.Value = .IPV4
                    txt_cgn_port.Value = .CGN_PORT
                    txt_ipv4_ipv6.Value = .IPV4_IPV6
                    txt_nat.Value = .NAT
                    df_first_seen_date.Text = .FIRST_SEEN_DATE
                    df_last_seen_date.Text = .LAST_SEEN_DATE
                    cbx_using_proxy.Checked = .USING_PROXY
                    txt_city.Value = .CITY

                    If refOs IsNot Nothing Then
                        cmb_network_device_country.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                    End If

                    txt_network_device_comments.Value = .COMMENTS

                End With

                WindowDetail_NetworkDevice.Title = "Network Device " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_SocialMedia(id As Long, isEdit As Boolean)
        Try
            Dim objCustomer_SocialMedia As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media =
                objListgoAML_Ref_SocialMedia.Find(Function(x) x.PK_goAML_Ref_Customer_Social_Media_ID = id)

            If Not objCustomer_SocialMedia Is Nothing Then
                FormPanel_SocialMedia.Hidden = False
                WindowDetail_SocialMedia.Hidden = False
                btnSaveCustomer_SocialMedia.Hidden = Not isEdit

                txt_socmed_platform.ReadOnly = Not isEdit
                txt_socmed_username.ReadOnly = Not isEdit
                txt_socmed_comments.ReadOnly = Not isEdit

                With objCustomer_SocialMedia
                    txt_socmed_platform.Value = .PLATFORM
                    txt_socmed_username.Value = .USER_NAME
                    txt_socmed_comments.Value = .COMMENTS
                End With

                WindowDetail_SocialMedia.Title = "SocialMedia " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_Sanction(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Sanction = objListgoAML_Ref_Sanction.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id)
            objTempCustomer_Sanction_Edit = objListgoAML_vw_Sanction.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id)


            If Not objTempCustomer_Sanction_Edit Is Nothing Then
                FormPanel_Sanction.Hidden = False
                WindowDetail_Sanction.Hidden = False
                btnSaveCustomer_Sanction.Hidden = Not isEdit

                txt_sanction_provider.ReadOnly = Not isEdit
                txt_sanction_sanction_list_name.ReadOnly = Not isEdit
                txt_sanction_match_criteria.ReadOnly = Not isEdit
                txt_sanction_link_to_source.ReadOnly = Not isEdit
                txt_sanction_sanction_list_attributes.ReadOnly = Not isEdit
                df_sanction_valid_from.ReadOnly = Not isEdit
                cbx_sanction_is_approx_from_date.ReadOnly = Not isEdit
                df_sanction_valid_to.ReadOnly = Not isEdit
                cbx_sanction_is_approx_to_date.ReadOnly = Not isEdit
                txt_sanction_comments.ReadOnly = Not isEdit


                With objTempCustomer_Sanction_Edit
                    txt_sanction_provider.Value = .PROVIDER
                    txt_sanction_sanction_list_name.Value = .SANCTION_LIST_NAME
                    txt_sanction_match_criteria.Value = .MATCH_CRITERIA
                    txt_sanction_link_to_source.Value = .LINK_TO_SOURCE
                    txt_sanction_sanction_list_attributes.Value = .SANCTION_LIST_ATTRIBUTES
                    'df_sanction_valid_from.Text = .SANCTION_LIST_DATE_RANGE_VALID_FROM
                    'cbx_sanction_is_approx_from_date.Checked = .SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_sanction_valid_to.Text = .SANCTION_LIST_DATE_RANGE_VALID_TO
                    'cbx_sanction_is_approx_to_date.Checked = .SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_sanction_comments.Value = .COMMENTS
                End With

                WindowDetail_Sanction.Title = "Sanction " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_RelatedPerson(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Related_Person = objListgoAML_Ref_RelatedPerson.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id)
            objTempCustomer_RelatedPerson_Edit = objListgoAML_vw_RelatedPerson.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id)

            If Not objTempCustomer_RelatedPerson_Edit Is Nothing Then

                GridAddressWork_RelatedPerson.IsViewMode = Not isEdit
                GridAddress_RelatedPerson.IsViewMode = Not isEdit
                GridPhone_RelatedPerson.IsViewMode = Not isEdit
                GridPhoneWork_RelatedPerson.IsViewMode = Not isEdit
                GridIdentification_RelatedPerson.IsViewMode = Not isEdit
                GridEmail_RelatedPerson.IsViewMode = Not isEdit
                GridSanction_RelatedPerson.IsViewMode = Not isEdit
                GridPEP_RelatedPerson.IsViewMode = Not isEdit

                FormPanel_RelatedPerson.Hidden = False
                WindowDetail_RelatedPerson.Hidden = False
                btnSaveCustomer_RelatedPerson.Hidden = Not isEdit

                cmb_rp_person_relation.IsReadOnly = Not isEdit
                txt_rp_relation_comments.ReadOnly = Not isEdit
                'df_relation_date_range_valid_from.ReadOnly = Not isEdit
                'cbx_relation_date_range_is_approx_from_date.ReadOnly = Not isEdit
                'df_relation_date_range_valid_to.ReadOnly = Not isEdit
                'cbx_relation_date_range_is_approx_to_date.ReadOnly = Not isEdit
                txt_rp_comments.ReadOnly = Not isEdit
                cmb_rp_gender.IsReadOnly = Not isEdit
                txt_rp_title.ReadOnly = Not isEdit
                'txt_rp_first_name.ReadOnly = Not isEdit
                'txt_rp_middle_name.ReadOnly = Not isEdit
                'txt_rp_prefix.ReadOnly = Not isEdit
                txt_rp_last_name.ReadOnly = Not isEdit
                df_birthdate.ReadOnly = Not isEdit
                txt_rp_birth_place.ReadOnly = Not isEdit
                'cmb_rp_country_of_birth.IsReadOnly = Not isEdit
                txt_rp_mothers_name.ReadOnly = Not isEdit
                txt_rp_alias.ReadOnly = Not isEdit
                'txt_rp_full_name_frn.ReadOnly = Not isEdit
                txt_rp_ssn.ReadOnly = Not isEdit
                txt_rp_passport_number.ReadOnly = Not isEdit
                cmb_rp_passport_country.IsReadOnly = Not isEdit
                txt_rp_id_number.ReadOnly = Not isEdit
                cmb_rp_nationality1.IsReadOnly = Not isEdit
                cmb_rp_nationality2.IsReadOnly = Not isEdit
                cmb_rp_nationality3.IsReadOnly = Not isEdit
                cmb_rp_residence.IsReadOnly = Not isEdit
                'df_rp_residence_since.ReadOnly = Not isEdit
                txt_rp_occupation.ReadOnly = Not isEdit
                txt_rp_employer_name.ReadOnly = Not isEdit
                cbx_rp_deceased.ReadOnly = Not isEdit
                df_rp_date_deceased.ReadOnly = Not isEdit
                txt_rp_tax_number.ReadOnly = Not isEdit
                cbx_rp_tax_reg_number.ReadOnly = Not isEdit
                txt_rp_source_of_wealth.ReadOnly = Not isEdit
                cbx_rp_is_protected.ReadOnly = Not isEdit


                With objTempCustomer_RelatedPerson_Edit
                    Dim refPersonRelation = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Person_Person_Relationship", .PERSON_PERSON_RELATION)
                    Dim refGender = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Jenis_Kelamin", .GENDER)
                    Dim refCountryBirth = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .COUNTRY_OF_BIRTH)
                    Dim refPassportCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .PASSPORT_COUNTRY)
                    Dim refNationality1 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY1)
                    Dim refNationality2 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY2)
                    Dim refNationality3 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY3)
                    Dim refResidence = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .RESIDENCE)

                    cmb_rp_person_relation.SetTextWithTextValue(refPersonRelation.Kode, refPersonRelation.Keterangan)
                    txt_rp_relation_comments.Value = .RELATION_COMMENTS
                    'df_relation_date_range_valid_from.Text = .RELATION_DATE_RANGE_VALID_FROM
                    'cbx_relation_date_range_is_approx_from_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_relation_date_range_valid_to.Text = .RELATION_DATE_RANGE_VALID_TO
                    'cbx_relation_date_range_is_approx_to_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_rp_comments.Value = .COMMENTS
                    If refGender IsNot Nothing Then
                        cmb_rp_gender.SetTextWithTextValue(refGender.Kode, refGender.Keterangan)
                    End If
                    txt_rp_title.Value = .TITLE
                    'txt_rp_first_name.Value = .FIRST_NAME
                    'txt_rp_middle_name.Value = .MIDDLE_NAME
                    'txt_rp_prefix.Value = .PREFIX
                    txt_rp_last_name.Value = .LAST_NAME
                    df_birthdate.Text = .BIRTHDATE
                    txt_rp_birth_place.Value = .BIRTH_PLACE
                    'cmb_rp_country_of_birth.SetTextWithTextValue(refCountryBirth.Kode, refCountryBirth.Keterangan)
                    txt_rp_mothers_name.Value = .MOTHERS_NAME
                    txt_rp_alias.Value = .ALIAS
                    'txt_rp_full_name_frn.Value = .FULL_NAME_FRN
                    txt_rp_ssn.Value = .SSN
                    txt_rp_passport_number.Value = .PASSPORT_NUMBER
                    If refPassportCountry IsNot Nothing Then
                        cmb_rp_passport_country.SetTextWithTextValue(refPassportCountry.Kode, refPassportCountry.Keterangan)
                    End If
                    txt_rp_id_number.Value = .ID_NUMBER
                    If refNationality1 IsNot Nothing Then
                        cmb_rp_nationality1.SetTextWithTextValue(refNationality1.Kode, refNationality1.Keterangan)
                    End If
                    If refNationality2 IsNot Nothing Then
                        cmb_rp_nationality2.SetTextWithTextValue(refNationality2.Kode, refNationality2.Keterangan)
                    End If
                    If refNationality3 IsNot Nothing Then
                        cmb_rp_nationality3.SetTextWithTextValue(refNationality3.Kode, refNationality3.Keterangan)
                    End If
                    If refResidence IsNot Nothing Then
                        cmb_rp_residence.SetTextWithTextValue(refResidence.Kode, refResidence.Keterangan)
                    End If
                    'df_rp_residence_since.Text = .RESIDENCE_SINCE
                    txt_rp_occupation.Value = .OCCUPATION
                    txt_rp_employer_name.Value = .EMPLOYER_NAME
                    cbx_rp_deceased.Checked = .DECEASED
                    df_rp_date_deceased.Text = IIf(.DATE_DECEASED Is Nothing, "", .DATE_DECEASED)
                    txt_rp_tax_number.Value = .TAX_NUMBER
                    cbx_rp_tax_reg_number.Checked = .TAX_REG_NUMBER
                    txt_rp_source_of_wealth.Text = .SOURCE_OF_WEALTH
                    cbx_rp_is_protected.Checked = .IS_PROTECTED

                    'GridEmail_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Customer_Email)
                    'Dim whereEmail = $"FK_REF_DETAIL_OF = 31 AND CIF = {Helper.CommonHelper.WrapSqlVariable(objReturn.PK_goAML_Ref_Customer_Related_Person_ID)}"
                    'objReturn.ObjList_GoAML_Ref_Customer_Email = emailDAL.GetData(whereEmail)

                    Dim address_list = .ObjList_GoAML_Ref_Address
                    Dim address_work_list = .ObjList_GoAML_Ref_Address_Work
                    Dim phone_list = .ObjList_GoAML_Ref_Phone
                    Dim phone_work_list = .ObjList_GoAML_Ref_Phone_Work
                    Dim identification_list = .ObjList_GoAML_Person_Identification
                    Dim email_list = .ObjList_GoAML_Ref_Customer_Email
                    Dim sanction_list = .ObjList_GoAML_Ref_Customer_Sanction
                    Dim pep_list = .ObjList_GoAML_Ref_Customer_PEP

                    ' 2023-10-10, Nael: fixing country tidak muncul di related person, dengan cara bikind data table yg ada field 'Country'-nya
                    Dim dataTableAddress = goAML_Customer_Service.AddressService.GenerateDataTable(goAML_Ref_Address_Mapper.Serialize(address_list))
                    Dim dataTableAddressWork = goAML_Customer_Service.AddressService.GenerateDataTable(goAML_Ref_Address_Mapper.Serialize(address_work_list))

                    GridAddress_RelatedPerson.LoadData(address_list)
                    GridAddressWork_RelatedPerson.LoadData(address_work_list)
                    GridPhone_RelatedPerson.LoadData(phone_list)
                    GridPhoneWork_RelatedPerson.LoadData(phone_work_list)
                    GridIdentification_RelatedPerson.LoadData(identification_list)

                    GridEmail_RelatedPerson.LoadData(email_list)
                    GridSanction_RelatedPerson.LoadData(sanction_list)
                    GridPEP_RelatedPerson.LoadData(pep_list)

                End With

                WindowDetail_RelatedPerson.Title = "Related Person " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_AdditionalInformation(id As Long, isEdit As Boolean)
        Try
            Dim objCustomer_AdditionalInformation As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information =
                objListgoAML_Ref_AdditionalInfo.Find(Function(x) x.PK_goAML_Ref_Customer_Additional_Information_ID = id)

            If Not objCustomer_AdditionalInformation Is Nothing Then
                FormPanel_AdditionalInfo.Hidden = False
                WindowDetail_AdditionalInfo.Hidden = False
                btnSaveCustomer_AdditionalInfo.Hidden = Not isEdit

                cmb_info_type.IsReadOnly = Not isEdit
                txt_info_subject.ReadOnly = Not isEdit
                txt_info_text.ReadOnly = Not isEdit
                nfd_info_numeric.ReadOnly = Not isEdit
                df_info_date.ReadOnly = Not isEdit
                cbx_info_boolean.ReadOnly = Not isEdit

                With objCustomer_AdditionalInformation
                    Dim refType = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Additional_Info_Type", .INFO_TYPE)

                    cmb_info_type.SetTextWithTextValue(refType.Kode, refType.Keterangan)
                    txt_info_subject.Value = .INFO_SUBJECT
                    txt_info_text.Value = .INFO_TEXT
                    nfd_info_numeric.Value = .INFO_NUMERIC
                    df_info_date.Text = .INFO_DATE
                    cbx_info_boolean.Checked = .INFO_BOOLEAN

                End With

                WindowDetail_AdditionalInfo.Title = "Additional Information " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_EntityIdentification(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Entity_Identification = objListgoAML_Ref_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id)
            objTempCustomer_EntityIdentification_Edit = objListgoAML_vw_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id)

            If Not objTempCustomer_EntityIdentification_Edit Is Nothing Then
                FormPanel_EntityIdentification.Hidden = False
                WindowDetail_EntityIdentification.Hidden = False
                btnSaveCustomer_EntityIdentification.Hidden = Not isEdit

                cmb_ef_type.IsReadOnly = Not isEdit
                txt_ef_number.ReadOnly = Not isEdit
                df_issue_date.ReadOnly = Not isEdit
                df_expiry_date.ReadOnly = Not isEdit
                txt_ef_issued_by.ReadOnly = Not isEdit
                cmb_ef_issue_country.IsReadOnly = Not isEdit
                txt_ef_comments.ReadOnly = Not isEdit


                With objTempCustomer_EntityIdentification_Edit
                    Dim refType = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Identifier_Type", .TYPE)
                    Dim refIssue = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .ISSUE_COUNTRY)

                    cmb_ef_type.SetTextWithTextValue(refType.Kode, refType.Keterangan)
                    txt_ef_number.Value = .NUMBER
                    df_issue_date.Text = IIf(.ISSUE_DATE Is Nothing, "", .ISSUE_DATE)
                    df_expiry_date.Text = IIf(.EXPIRY_DATE Is Nothing, "", .EXPIRY_DATE)
                    txt_ef_issued_by.Value = .ISSUED_BY
                    cmb_ef_issue_country.SetTextWithTextValue(refIssue.Kode, refIssue.Keterangan)
                    txt_ef_comments.Value = .COMMENTS

                End With

                WindowDetail_EntityIdentification.Title = "Entity Identification " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_CustomerURL(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_URL = objListgoAML_Ref_EntityUrl.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id)
            objTempCustomer_EntityUrl_Edit = objListgoAML_vw_EntityUrl.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id)

            If Not objTempCustomer_EntityUrl_Edit Is Nothing Then
                FormPanel_URL.Hidden = False
                WindowDetail_URL.Hidden = False
                btnSaveCustomer_URL.Hidden = Not isEdit

                txt_url.ReadOnly = Not isEdit

                With objTempCustomer_EntityUrl_Edit
                    txt_url.Value = .URL
                End With

                WindowDetail_URL.Title = "Entity URL " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_RelatedEntities(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Related_Entities = objListgoAML_Ref_RelatedEntities.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id)
            objTempCustomer_RelatedEntities_Edit = objListgoAML_vw_RelatedEntities.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id)

            If Not objTempCustomer_RelatedEntities_Edit Is Nothing Then
                GridPhone_RelatedEntity.IsViewMode = Not isEdit
                GridAddress_RelatedEntity.IsViewMode = Not isEdit
                GridEmail_RelatedEntity.IsViewMode = Not isEdit
                GridEntityIdentification_RelatedEntity.IsViewMode = Not isEdit
                GridURL_RelatedEntity.IsViewMode = Not isEdit
                GridSanction_RelatedEntity.IsViewMode = Not isEdit

                FormPanel_RelatedEntities.Hidden = False
                WindowDetail_RelatedEntities.Hidden = False
                btnSaveCustomer_EntityRelation.Hidden = Not isEdit

                cmb_re_entity_relation.IsReadOnly = Not isEdit
                'df_re_relation_date_range_valid_from.ReadOnly = Not isEdit
                'cbx_re_relation_date_range_is_approx_from_date.ReadOnly = Not isEdit
                'df_re_relation_date_range_valid_to.ReadOnly = Not isEdit
                'cbx_re_relation_date_range_is_approx_to_date.ReadOnly = Not isEdit
                'nfd_re_share_percentage.ReadOnly = Not isEdit
                txt_re_comments.ReadOnly = Not isEdit
                txt_re_relation_comments.ReadOnly = Not isEdit
                txt_re_name.ReadOnly = Not isEdit
                txt_re_commercial_name.ReadOnly = Not isEdit
                cmb_re_incorporation_legal_form.IsReadOnly = Not isEdit
                txt_re_incorporation_number.ReadOnly = Not isEdit
                txt_re_business.ReadOnly = Not isEdit
                'cmb_re_entity_status.IsReadOnly = Not isEdit
                'df_re_entity_status_date.ReadOnly = Not isEdit
                txt_re_incorporation_state.ReadOnly = Not isEdit
                cmb_re_incorporation_country_code.IsReadOnly = Not isEdit
                df_re_incorporation_date.ReadOnly = Not isEdit
                cbx_re_business_closed.ReadOnly = Not isEdit
                df_re_date_business_closed.ReadOnly = Not isEdit
                txt_re_tax_number.ReadOnly = Not isEdit
                txt_re_tax_reg_number.ReadOnly = Not isEdit


                With objTempCustomer_RelatedEntities_Edit
                    Dim refRelation = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Entity_Relationship", .ENTITY_ENTITY_RELATION)
                    Dim refLegalForm = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Bentuk_Badan_Usaha", .INCORPORATION_LEGAL_FORM)
                    Dim refEntityStatus = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Status", .ENTITY_STATUS)
                    Dim refCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .INCORPORATION_COUNTRY_CODE)

                    If refRelation IsNot Nothing Then
                        cmb_re_entity_relation.SetTextWithTextValue(refRelation.Kode, refRelation.Keterangan)
                    End If
                    'df_re_relation_date_range_valid_from.Text = .RELATION_DATE_RANGE_VALID_FROM
                    'cbx_re_relation_date_range_is_approx_from_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_re_relation_date_range_valid_to.Text = .RELATION_DATE_RANGE_VALID_TO
                    'cbx_re_relation_date_range_is_approx_to_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    'nfd_re_share_percentage.Value = .SHARE_PERCENTAGE
                    txt_re_comments.Value = .COMMENTS
                    txt_re_relation_comments.Value = .RELATION_COMMENTS
                    txt_re_name.Value = .NAME
                    txt_re_commercial_name.Value = .COMMERCIAL_NAME
                    If refLegalForm IsNot Nothing Then
                        cmb_re_incorporation_legal_form.SetTextWithTextValue(refLegalForm.Kode, refLegalForm.Keterangan)
                    End If
                    txt_re_incorporation_number.Value = .INCORPORATION_NUMBER
                    txt_re_business.Value = .BUSINESS
                    'cmb_re_entity_status.SetTextWithTextValue(refEntityStatus.Kode, refEntityStatus.Keterangan)
                    'df_re_entity_status_date.Text = .ENTITY_STATUS
                    txt_re_incorporation_state.Value = .INCORPORATION_STATE
                    If refCountry IsNot Nothing Then
                        cmb_re_incorporation_country_code.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                    End If
                    df_re_incorporation_date.Text = IIf(.INCORPORATION_DATE Is Nothing, "", .INCORPORATION_DATE)
                    cbx_re_business_closed.Checked = .BUSINESS_CLOSED
                    df_re_date_business_closed.Text = IIf(.DATE_BUSINESS_CLOSED Is Nothing, "", .DATE_BUSINESS_CLOSED)
                    txt_re_tax_number.Value = .TAX_NUMBER
                    txt_re_tax_reg_number.Value = .TAX_REG_NUMBER

                    GridPhone_RelatedEntity.LoadData(.ObjList_GoAML_Ref_Customer_Phone)
                    GridAddress_RelatedEntity.LoadData(.ObjList_GoAML_Ref_Customer_Address)
                    GridEmail_RelatedEntity.LoadData(.ObjList_GoAML_Ref_Customer_Email)
                    GridEntityIdentification_RelatedEntity.LoadData(.ObjList_GoAML_Ref_Customer_Entity_Identification)
                    GridURL_RelatedEntity.LoadData(.ObjList_GoAML_Ref_Customer_URL)
                    GridSanction_RelatedEntity.LoadData(.ObjList_GoAML_Ref_Customer_Sanction)

                End With

                WindowDetail_RelatedEntities.Title = "Related Entities " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub DeleteRecordPreviousName(id As Long)

        Dim objDelVWPreviousName As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name = objListgoAML_vw_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)
        Dim objxDelPreviousName As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name = objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)

        If Not objxDelPreviousName Is Nothing Then
            objListgoAML_Ref_Previous_Name.Remove(objxDelPreviousName)
        End If

        If Not objDelVWPreviousName Is Nothing Then
            objListgoAML_vw_Previous_Name.Remove(objDelVWPreviousName)

            Store_Customer_Previous_Name.DataSource = objListgoAML_vw_Previous_Name.ToList()
            Store_Customer_Previous_Name.DataBind()
        End If

        FormPanelPreviousName.Hidden = True
        WindowDetailPreviousName.Hidden = True
        ClearinputCustomerPreviousName()

    End Sub

    Sub DeleteRecord_Email(id As Long)
        objListgoAML_vw_Email.Remove(objListgoAML_vw_Email.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id))
        objListgoAML_Ref_Email.Remove(objListgoAML_Ref_Email.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id))

        Store_Customer_Email.DataSource = objListgoAML_vw_Email.ToList()
        Store_Customer_Email.DataBind()

        Store_corp_email.DataSource = objListgoAML_vw_Email.ToList()
        Store_corp_email.DataBind()

    End Sub

    Sub DeleteRecord_PEP(id As Long)
        objListgoAML_vw_PersonPEP.Remove(objListgoAML_vw_PersonPEP.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id))
        objListgoAML_Ref_PersonPEP.Remove(objListgoAML_Ref_PersonPEP.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id))

        Store_Customer_Person_PEP.DataSource = objListgoAML_vw_PersonPEP.ToList()
        Store_Customer_Person_PEP.DataBind()

    End Sub

    Sub DeleteRecord_Sanction(id As Long)
        objListgoAML_vw_Sanction.Remove(objListgoAML_vw_Sanction.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id))
        objListgoAML_Ref_Sanction.Remove(objListgoAML_Ref_Sanction.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id))

        Store_Customer_Sanction.DataSource = objListgoAML_vw_Sanction.ToList()
        Store_Customer_Sanction.DataBind()

        Store_corp_sanction.DataSource = objListgoAML_vw_Sanction.ToList()
        Store_corp_sanction.DataBind()

    End Sub

    Sub DeleteRecord_RelatedPerson(id As Long)
        objListgoAML_vw_RelatedPerson.Remove(objListgoAML_vw_RelatedPerson.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id))
        objListgoAML_Ref_RelatedPerson.Remove(objListgoAML_Ref_RelatedPerson.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id))

        Store_Customer_RelatedPerson.DataSource = objListgoAML_vw_RelatedPerson.ToList()
        Store_Customer_RelatedPerson.DataBind()

        'StoreCorp_Related_person.DataSource = objListgoAML_vw_RelatedPerson.ToList()
        'StoreCorp_Related_person.DataBind()

    End Sub

    Sub DeleteRecord_URL(id As Long)
        objListgoAML_vw_EntityUrl.Remove(objListgoAML_vw_EntityUrl.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id))
        objListgoAML_Ref_EntityUrl.Remove(objListgoAML_Ref_EntityUrl.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id))

        Store_Customer_Entity_URL.DataSource = objListgoAML_vw_EntityUrl.ToList()
        Store_Customer_Entity_URL.DataBind()

    End Sub

    Sub DeleteRecord_EntityIdentification(id As Long)
        objListgoAML_vw_EntityIdentification.Remove(objListgoAML_vw_EntityIdentification.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id))
        objListgoAML_Ref_EntityIdentification.Remove(objListgoAML_Ref_EntityIdentification.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id))

        Store_Customer_Entity_Identification.DataSource = objListgoAML_vw_EntityIdentification.ToList()
        Store_Customer_Entity_Identification.DataBind()

    End Sub

    Sub DeleteRecord_RelatedEntities(id As Long)
        objListgoAML_vw_RelatedEntities.Remove(objListgoAML_vw_RelatedEntities.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id))
        objListgoAML_Ref_RelatedEntities.Remove(objListgoAML_Ref_RelatedEntities.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id))

        Store_Related_Entities.DataSource = objListgoAML_vw_RelatedEntities.ToList()
        Store_Related_Entities.DataBind()

    End Sub

    Function IsDataCustomer_PreviousName_Valid() As Boolean
        If txt_cif.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If
        If txtCustomer_first_name.Text.Trim = "" Then
            Throw New Exception("First Name is required")
        End If
        If txtCustomer_last_name.Text.Trim = "" Then
            Throw New Exception("Last Name is required")
        End If
        Return True
        Return True
    End Function

    Function IsDataCustomer_Email_Valid() As Boolean
        ' 2023-10-16, Nael: comment
        'If txt_cif.Text.Trim = "" Then
        '    Throw New Exception("CIF is required") ' 2023-10-16, Nael
        'End If

        'If txtCustomer_email_address.Text.Trim = "" Then
        '    Throw New Exception("CIF is required")
        'End If

        If txtCustomer_email_address.Text.Trim = "" Then
            Throw New Exception("Email Address is required")
        End If

        If Not IsFieldValid(txtCustomer_email_address.Text.Trim, "email_address") Then
            Throw New Exception("Email Address format is not valid")
        End If

        Return True
    End Function

    Function IsDataCustomer_EmploymentHistory_Valid() As Boolean
        If txt_cif.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If
        Return True
    End Function

    Function IsDataCustomer_PEP_Valid() As Boolean
        If txt_cif.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        If cmb_PersonPEP_Country.SelectedItemText Is Nothing Then
            Throw New Exception("Country is required")
        End If

        'If txt_function_name.Text.Trim = "" Then
        '    Throw New Exception("Function Name is required")
        'End If
        'If txt_function_description.Text.Trim = "" Then
        '    Throw New Exception("Function Description is required")
        'End If
        'If df_person_pep_valid_from.RawValue Is Nothing Then
        '    Throw New Exception("Valid From is required")
        'End If

        'If df_pep_valid_to.RawValue Is Nothing Then
        '    Throw New Exception("Valid To is required")
        'End If

        'If txt_pep_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If

        Return True
    End Function

    Function IsDataCustomer_NetworkDevice_Valid() As Boolean
        If txt_cif.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If
        Return True
    End Function

    Function IsDataCustomer_SocialMedia_Valid() As Boolean
        If txt_cif.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If
        Return True
    End Function

    Function IsDataCustomer_Sanction_Valid() As Boolean
        If txt_cif.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        If txt_sanction_provider.Text.Trim = "" Then
            Throw New Exception("Provider is required")
        End If
        If txt_sanction_sanction_list_name.Text.Trim = "" Then
            Throw New Exception("Sanction List Name is required")
        End If
        'If txt_sanction_match_criteria.Text.Trim = "" Then
        '    Throw New Exception("Match Criteria is required")
        'End If
        'If txt_sanction_link_to_source.Text.Trim = "" Then
        '    Throw New Exception("Link To Source is required")
        'End If
        'If txt_sanction_sanction_list_attributes.Text.Trim = "" Then
        '    Throw New Exception("Sanction List Attributes is required")
        'End If
        'If df_sanction_valid_from.RawValue Is Nothing Then
        '    Throw New Exception("Valid From is required")
        'End If
        'If df_sanction_valid_to.RawValue Is Nothing Then
        '    Throw New Exception("Valid To is required")
        'End If
        'If txt_sanction_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If

        Return True
    End Function

    Function IsDataCustomer_RelatedPerson_Valid() As Boolean
        If txt_cif.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        If cmb_rp_person_relation.SelectedItemText Is Nothing Then
            Throw New Exception("Person Relation is required")
        End If
        'If df_relation_date_range_valid_from.RawValue Is Nothing Then
        '    Throw New Exception("Valid From is required")
        'End If
        'If df_relation_date_range_valid_to.RawValue Is Nothing Then
        '    Throw New Exception("Valid To is required")
        'End If
        'If txt_rp_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If
        'If cmb_rp_gender.SelectedItemText Is Nothing Then
        '    Throw New Exception("Gender is required")
        'End If
        'If txt_rp_title.Text.Trim = "" Then
        '    Throw New Exception("Title is required")
        'End If
        'If txt_rp_first_name.Text.Trim = "" Then
        '    Throw New Exception("First Name is required")
        'End If
        'If txt_rp_middle_name.Text.Trim = "" Then
        '    Throw New Exception("Middle Name is required")
        'End If
        'If txt_rp_prefix.Text.Trim = "" Then
        '    Throw New Exception("Prefix is required")
        'End If
        If txt_rp_last_name.Text.Trim = "" Then
            Throw New Exception("Full Name is required")
        End If
        If GridPhone_RelatedPerson.objListgoAML_Ref Is Nothing Then
            Throw New Exception("Phone is required")
        End If
        If GridPhone_RelatedPerson.objListgoAML_Ref.Count = 0 Then
            Throw New Exception("Phone is required")
        End If
        If GridAddress_RelatedPerson.objListgoAML_Ref Is Nothing Then
            Throw New Exception("Address is required")
        End If
        If GridAddress_RelatedPerson.objListgoAML_Ref.Count = 0 Then
            Throw New Exception("Address is required")
        End If
        'If df_birthdate.RawValue Is Nothing Then
        '    Throw New Exception("Birth Date is required")
        'End If
        'If txt_rp_birth_place.Text.Trim = "" Then
        '    Throw New Exception("Birth Place is required")
        'End If
        'If Not IsFieldValid(txt_rp_birth_place.Text.Trim, "birth_place") Then
        '    Throw New Exception("Birth Place format is not valid")
        'End If
        'If cmb_rp_country_of_birth.SelectedItemText Is Nothing Then
        '    Throw New Exception("Country Of Birth is required")
        'End If
        'If txt_rp_mothers_name.Text.Trim = "" Then
        '    Throw New Exception("Mother's Name is required")
        'End If
        'If Not IsFieldValid(txt_rp_mothers_name.Text.Trim, "mothers_name") Then
        '    Throw New Exception("Mothers Name format is not valid")
        'End If
        'If txt_rp_alias.Text.Trim = "" Then
        '    Throw New Exception("Alias is required")
        'End If
        'If Not IsFieldValid(txt_rp_alias.Text.Trim, "alias") Then
        '    Throw New Exception("Alias format is not valid")
        'End If
        'If txt_rp_full_name_frn.Text.Trim = "" Then
        '    Throw New Exception("Full Name is required")
        'End If
        'If txt_rp_ssn.Text.Trim = "" Then
        '    Throw New Exception("SSN is required")
        'End If
        'If Not IsFieldValid(txt_rp_ssn.Text.Trim, "ssn") Then
        '    Throw New Exception("SSN format is not valid")
        'End If
        'If txt_rp_passport_number.Text.Trim = "" Then
        '    Throw New Exception("Passport Number is required")
        'End If
        'If cmb_rp_passport_country.SelectedItemText Is Nothing Then
        '    Throw New Exception("Paassport Country is required")
        'End If
        'If txt_rp_id_number.Text.Trim = "" Then
        '    Throw New Exception("ID Number is required")
        'End If
        'If cmb_rp_nationality1.SelectedItemText Is Nothing Then
        '    Throw New Exception("Nationality 1 is required")
        'End If
        'If cmb_rp_nationality2.SelectedItemText Is Nothing Then
        '    Throw New Exception("Nationality 2 is required")
        'End If
        'If cmb_rp_nationality3.SelectedItemText Is Nothing Then
        '    Throw New Exception("Nationality 3 is required")
        'End If
        'If cmb_rp_residence.SelectedItemText Is Nothing Then
        '    Throw New Exception("Domicile Country is required")
        'End If
        'If df_rp_residence_since.RawValue Is Nothing Then
        '    Throw New Exception("Residence Since is required")
        'End If
        'If txt_rp_occupation.Text.Trim = "" Then
        '    Throw New Exception("Occupation is required")
        'End If
        'If df_rp_date_deceased.RawValue Is Nothing Then
        '    Throw New Exception("Date Deceased is required")
        'End If
        'If txt_rp_tax_number.Text.Trim = "" Then
        '    Throw New Exception("Tax Number is required")
        'End If
        'If txt_rp_source_of_wealth.Text.Trim = "" Then
        '    Throw New Exception("Source of Wealth is required")
        'End If

        Return True
    End Function

    Function IsDataCustomer_AdditionalInformation_Valid() As Boolean
        If txt_cif.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If
        Return True
    End Function

    Function IsDataCustomer_EntityIdentification_Valid() As Boolean
        If txt_cif.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        If cmb_ef_type.SelectedItemText Is Nothing Then
            Throw New Exception("Type is required")
        End If
        If txt_ef_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'If df_issue_date.RawValue Is Nothing Then
        '    Throw New Exception("Issue Date is required")
        'End If
        'If df_expiry_date.RawValue Is Nothing Then
        '    Throw New Exception("Expiry Date is required")
        'End If
        'If txt_ef_issued_by.Text.Trim = "" Then
        '    Throw New Exception("Issued By is required")
        'End If
        If cmb_ef_issue_country.SelectedItemText Is Nothing Then
            Throw New Exception("Issue Country is required")
        End If
        'If txt_ef_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If


        Return True
    End Function

    Function IsDataCustomer_URL_Valid() As Boolean
        If txt_cif.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        If txt_url.Text.Trim = "" Then
            Throw New Exception("Url is required")
        End If

        Return True
    End Function

    Function IsDataCustomer_RelatedEntities_Valid() As Boolean
        If txt_cif.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        If cmb_re_entity_relation.SelectedItemText Is Nothing Then
            Throw New Exception("Entity Relation is required")
        End If
        'If df_re_relation_date_range_valid_from.RawValue Is Nothing Then
        '    Throw New Exception("Valid From is required")
        'End If
        'If df_re_relation_date_range_valid_to.RawValue Is Nothing Then
        '    Throw New Exception("Valid To is required")
        'End If
        'If nfd_re_share_percentage.Text.Trim = "" Then
        '    Throw New Exception("Share Percentage is required")
        'End If
        'If txt_re_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If
        'If txt_re_name.Text.Trim = "" Then
        '    Throw New Exception("Name is required")
        'End If
        If txt_re_commercial_name.Text.Trim = "" Then
            Throw New Exception("Commercial Name is required")
        End If
        If GridPhone_RelatedEntity.objListgoAML_Ref Is Nothing Then
            Throw New Exception("Phone is required")
        End If
        If GridPhone_RelatedEntity.objListgoAML_Ref.Count = 0 Then
            Throw New Exception("Phone is required")
        End If
        If GridAddress_RelatedEntity.objListgoAML_Ref Is Nothing Then
            Throw New Exception("Address is required")
        End If
        If GridAddress_RelatedEntity.objListgoAML_Ref.Count = 0 Then
            Throw New Exception("Address is required")
        End If

        'If cmb_re_incorporation_legal_form.SelectedItemText Is Nothing Then
        '    Throw New Exception("Incorporation Legal is required")
        'End If
        'If txt_re_incorporation_number.Text.Trim = "" Then
        '    Throw New Exception("Incorporation Number is required")
        'End If
        'If txt_re_business.Text.Trim = "" Then
        '    Throw New Exception("Business is required")
        'End If
        'If cmb_re_entity_status.SelectedItemText Is Nothing Then
        '    Throw New Exception("Entity Status is required")
        'End If
        'If df_re_entity_status_date.RawValue Is Nothing Then
        '    Throw New Exception("Entity Status Date is required")
        'End If
        'If txt_re_incorporation_state.Text.Trim = "" Then
        '    Throw New Exception("Incorporation State is required")
        'End If
        'If Not IsFieldValid(txt_re_incorporation_state.Text.Trim, "incorporation_state") Then
        '    Throw New Exception("Incorporation State format is not valid")
        'End If
        'If cmb_re_incorporation_country_code.SelectedItemText Is Nothing Then
        '    Throw New Exception("Incorporation Country Code is required")
        'End If
        'If df_re_incorporation_date.RawValue Is Nothing Then
        '    Throw New Exception("Incorporation Date is required")
        'End If
        'If df_re_date_business_closed.RawValue Is Nothing Then
        '    Throw New Exception("Business Closed is required")
        'End If
        'If txt_re_tax_number.Text.Trim = "" Then
        '    Throw New Exception("Tax Number is required")
        'End If
        'If txt_re_tax_reg_number.Text.Trim = "" Then
        '    Throw New Exception("Tax Reg Number is required")
        'End If

        Return True
    End Function

    Sub SaveEditPreviousName()


    End Sub

    Private Function IsFieldValid(text As String, fieldName As String) As Boolean
        Try
            Dim regex As New Regex("^[0-9 ]+$")

            Select Case fieldName
                Case "incorporation_state"
                    regex = New Regex("[ a-zA-Z]*")
                Case "tax_number"
                    regex = New Regex("[0-9]{15}")
                Case "last_name"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "birth_place"
                    regex = New Regex("[ a-zA-Z]*")
                Case "mothers_name"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "alias"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "ssn"
                    regex = New Regex("[0-9]{22}")
                Case "city"
                    regex = New Regex("[ a-zA-Z]*")
                Case "state"
                    regex = New Regex("[ a-zA-Z]*")
                Case "email_address"
                    regex = New Regex("[_a-zA-Z0-9-+]+(\.[_a-zA-Z0-9-\+]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})")
                Case "ipv4_address_type"
                    regex = New Regex("((1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])")
                Case "ipv6_address_type"
                    regex = New Regex("([A-Fa-f0-9]{1,4}:){7}[A-Fa-f0-9]{1,4}")
                Case Else
                    Return True
            End Select

            Return regex.IsMatch(text)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Protected Sub SetCIFToGrid(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            GridEmail_RelatedPerson.CIF = txt_cif.Value

            GridEmail_Director.CIF = txt_cif.Value
            GridSanction_Director.CIF = txt_cif.Value
            GridPEP_Director.CIF = txt_cif.Value

            GridEmail_RelatedPerson.CIF = txt_cif.Value
            GridIdentification_RelatedPerson.CIF = txt_cif.Value
            GridPhone_RelatedPerson.CIF = txt_cif.Value
            GridAddress_RelatedPerson.CIF = txt_cif.Value
            GridPhoneWork_RelatedPerson.CIF = txt_cif.Value
            GridAddressWork_RelatedPerson.CIF = txt_cif.Value

            GridPhone_RelatedEntity.CIF = txt_cif.Value
            GridAddress_RelatedEntity.CIF = txt_cif.Value
            GridEmail_RelatedEntity.CIF = txt_cif.Value
            GridEntityIdentification_RelatedEntity.CIF = txt_cif.Value
            GridSanction_RelatedEntity.CIF = txt_cif.Value
            GridURL_RelatedEntity.CIF = txt_cif.Value
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub InitGrid()

        InitChildGrid_RelatedPerson()
        InitChildGrid_RelatedEntity()
        InitChildGrid_Director()
    End Sub

    Sub InitChildGrid_RelatedPerson()
        Dim UniqueName = "RelatedPerson"

        'GridAddress_RelatedPerson.UniqueName = UniqueName
        GridAddress_RelatedPerson.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Address)
        GridAddress_RelatedPerson.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Address)

        'GridAddressWork_RelatedPerson.UniqueName = UniqueName & "2"
        GridAddressWork_RelatedPerson.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Address)
        GridAddressWork_RelatedPerson.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Address)

        'GridPhone_RelatedPerson.UniqueName = UniqueName
        GridPhone_RelatedPerson.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Phone)
        GridPhone_RelatedPerson.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Phone)

        'GridPhoneWork_RelatedPerson.UniqueName = UniqueName & "2"
        GridPhoneWork_RelatedPerson.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Phone)
        GridPhoneWork_RelatedPerson.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Phone)

        'GridIdentification_RelatedPerson.UniqueName = UniqueName
        GridIdentification_RelatedPerson.objListgoAML_Ref = New List(Of DataModel.goAML_Person_Identification)
        GridIdentification_RelatedPerson.objListgoAML_vw = New List(Of DataModel.goAML_Person_Identification)

        'GridEmail_RelatedPerson.UniqueName = UniqueName
        GridEmail_RelatedPerson.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Customer_Email)
        GridEmail_RelatedPerson.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Customer_Email)

        GridSanction_RelatedPerson.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Customer_Sanction)
        GridSanction_RelatedPerson.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Customer_Sanction)

        GridPEP_RelatedPerson.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Customer_PEP)
        GridPEP_RelatedPerson.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Customer_PEP)
    End Sub

    Sub InitChildGrid_RelatedEntity()
        Dim UniqueName = "RelatedEntity"

        'GridEmail_RelatedEntity.UniqueName = UniqueName
        GridPhone_RelatedEntity.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Phone)
        GridPhone_RelatedEntity.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Phone)

        'GridEmail_RelatedEntity.UniqueName = UniqueName
        GridAddress_RelatedEntity.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Address)
        GridAddress_RelatedEntity.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Address)

        'GridEmail_RelatedEntity.UniqueName = UniqueName
        GridEmail_RelatedEntity.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Customer_Email)
        GridEmail_RelatedEntity.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Customer_Email)

        'GridEntityIdentification_RelatedEntity.UniqueName = UniqueName
        GridEntityIdentification_RelatedEntity.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        GridEntityIdentification_RelatedEntity.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)

        'GridURL_RelatedEntity.UniqueName = UniqueName
        GridURL_RelatedEntity.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Customer_URL)
        GridURL_RelatedEntity.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Customer_URL)

        'GridSanction_RelatedEntity.UniqueName = UniqueName
        GridSanction_RelatedEntity.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Customer_Sanction)
        GridSanction_RelatedEntity.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Customer_Sanction)
    End Sub

    Sub InitChildGrid_Director()
        Dim UniqueName = "Director"

        'GridEmail_Director.UniqueName = UniqueName
        GridEmail_Director.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Customer_Email)
        GridEmail_Director.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Customer_Email)

        'GridSanction_Director.UniqueName = UniqueName
        GridSanction_Director.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Customer_Sanction)
        GridSanction_Director.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Customer_Sanction)

        'GridPEP_Director.UniqueName = UniqueName
        GridPEP_Director.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Customer_PEP)
        GridPEP_Director.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Customer_PEP)
    End Sub

#End Region
End Class


