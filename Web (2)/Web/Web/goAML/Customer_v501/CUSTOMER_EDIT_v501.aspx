﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CUSTOMER_EDIT_v501.aspx.vb" Inherits="CUSTOMER_EDIT_v501" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<%@ Register Src="~/goAML/Customer_v501/Component/GridAddress.ascx" TagPrefix="nds" TagName="GridAddress" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridAddressWork.ascx" TagPrefix="nds" TagName="GridAddressWork" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridEmail.ascx" TagPrefix="nds" TagName="GridEmail" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridIdentification.ascx" TagPrefix="nds" TagName="GridIdentification" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridPEP.ascx" TagPrefix="nds" TagName="GridPEP" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridPhone.ascx" TagPrefix="nds" TagName="GridPhone" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridPhoneWork.ascx" TagPrefix="nds" TagName="GridPhoneWork" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridSanction.ascx" TagPrefix="nds" TagName="GridSanction" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridEntityIdentification.ascx" TagPrefix="nds" TagName="GridEntityIdentification" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridURL.ascx" TagPrefix="nds" TagName="GridURL" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridRelatedPerson.ascx" TagPrefix="nds" TagName="GridRelatedPerson" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridRelatedEntity.ascx" TagPrefix="nds" TagName="GridRelatedEntity" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridDirector.ascx" TagPrefix="nds" TagName="GridDirector" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Pop Up--%>
    <ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Phone Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSetCustomerPhoneAdd" runat="server" Title="Phones" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanelTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Hidden runat="server" ID="fld_sender"></ext:Hidden>
                            <%--<ext:TextField ID="txtCet" runat="server" LabelWidth="180" FieldLabel="CET No" AnchorHorizontal="90%"></ext:TextField>--%>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cb_phone_Contact_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cb_phone_Contact_Type" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Kategori_Kontak" StringFilter="Active = 1" EmptyText="Select One" Label="Contact Type" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_cb_phone_Communication_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cb_phone_Communication_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringFilter="Active = 1" EmptyText="Select One" StringTable="goAML_Ref_Jenis_Alat_Komunikasi" Label="Communication Type" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cb_phone_Contact_Type" AllowBlank="false" runat="server" FieldLabel="Contact Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Contact_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model_Contact_Type">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>
                            <%--<ext:ComboBox ID="cb_phone_Communication_Type" runat="server" AllowBlank="false" FieldLabel="Communication Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Communication_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model_Comminication_Type">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                            --%>        <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>

                            <ext:TextField ID="txt_phone_Country_prefix" runat="server" FieldLabel="Country Prefix" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_phone_number" runat="server" AllowBlank="false" FieldLabel="Phone Number" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_phone_extension" runat="server" FieldLabel="Extension" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_phone_comments" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSavedetail" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomerPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBackSaveDetail" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>

                    <ext:FormPanel ID="FormPanelEmpTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Emp_cb_phone_Contact_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Emp_cb_phone_Contact_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Kategori_Kontak" StringFilter="Active = 1" EmptyText="Select One" Label="Contact Type" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_Emp_cb_phone_Communication_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Emp_cb_phone_Communication_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Jenis_Alat_Komunikasi" StringFilter="Active = 1" EmptyText="Select One" Label="Communication Type" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%-- <ext:ComboBox ID="Emp_cb_phone_Contact_Type" runat="server" AllowBlank="false" FieldLabel="Contact Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Emp_Store_Contact_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model17">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%-- </DirectEvents>
                            </ext:ComboBox>
                            <ext:ComboBox ID="Emp_cb_phone_Communication_Type" runat="server" AllowBlank="false" FieldLabel="Communication Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Emp_Store_Communication_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model18">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>

                            <ext:TextField ID="Emp_txt_phone_Country_prefix" runat="server" FieldLabel="Country Prefix" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Emp_txt_phone_number" AllowBlank="false" runat="server" FieldLabel="Phone Number" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Emp_txt_phone_extension" runat="server" FieldLabel="Extension" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Emp_txt_phone_comments" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_saveempphone" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomerEmpPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button14" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailAddress" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Address Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet1" runat="server" Title="Address" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FP_Address_INDVD" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Hidden runat="server" ID="Hidden1"></ext:Hidden>
                            <%--<ext:TextField ID="txtCet" runat="server" LabelWidth="180" FieldLabel="CET No" AnchorHorizontal="90%"></ext:TextField>--%>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_kategoriaddress" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_kategoriaddress" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" EmptyText="Select One" StringFilter="Active = 1" StringTable="goAML_Ref_Kategori_Kontak" Label="Address Type" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cmb_kategoriaddress" runat="server" AllowBlank="false" FieldLabel="Address Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_KategoriAddress">
                                        <Model>
                                            <ext:Model runat="server" ID="ModelKategoriAddress">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="Address_INDV" runat="server" AllowBlank="false" FieldLabel="Address" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Town_INDV" runat="server" FieldLabel="Town" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="City_INDV" AllowBlank="false" runat="server" FieldLabel="City" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Zip_INDV" runat="server" FieldLabel="Zip Code" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_kodenegara" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_kodenegara" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" EmptyText="Select One" StringFilter="Active = 1" StringTable="goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cmb_kodenegara" runat="server" AllowBlank="false" FieldLabel="Country" DisplayField="Keterangan"
                                ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                     <ext:Store runat="server" ID="Store_Kode_Negara" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                    <ext:Store runat="server" ID="Store_Kode_Negara">
                                        <Model>
                                            <ext:Model runat="server" ID="ModelKodeNegara">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                         <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>
                                </DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="State_INDVD" runat="server" FieldLabel="Province" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Comment_Address_INDVD" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Btn_Save_Address" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomerAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Btn_Back_Address" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>

                    <ext:FormPanel ID="FP_Address_Emp_INDVD" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Hidden runat="server" ID="Hidden5"></ext:Hidden>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_emp_kategoriaddress" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_emp_kategoriaddress" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Kategori_Kontak" StringFilter="Active = 1" EmptyText="Select One" Label="Address Type" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cmb_emp_kategoriaddress" runat="server" AllowBlank="false" FieldLabel="Address Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_emp_KategoriAddress">
                                        <Model>
                                            <ext:Model runat="server" ID="Model19">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%-- </DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="emp_Address_INDV" runat="server" AllowBlank="false" FieldLabel="Address" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="emp_Town_INDV" runat="server" FieldLabel="Town" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="emp_City_INDV" runat="server" AllowBlank="false" FieldLabel="City" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="emp_Zip_INDV" runat="server" FieldLabel="Zip Code" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_emp_kodenegara" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_emp_kodenegara" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%-- <ext:ComboBox ID="cmb_emp_kodenegara" runat="server" AllowBlank="false" FieldLabel="Country" DisplayField="Keterangan"
                                ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ID="Store_emp_Kode_Negara" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model20">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="emp_State_INDVD" runat="server" FieldLabel="Province" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="emp_Comment_Address_INDVD" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_saveempaddress" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomerEmpAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button16" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailDirectorPhone" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Director Phone Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSetDirectorPhoneAdd" runat="server" Title="Phones" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanelDirectorPhone" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Director_cb_phone_Contact_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_cb_phone_Contact_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Kategori_Kontak" StringFilter="Active = 1" EmptyText="Select One" Label="Contact Type" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_Director_cb_phone_Communication_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_cb_phone_Communication_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Jenis_Alat_Komunikasi" StringFilter="Active = 1" EmptyText="Select One" Label="Communication Type" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="Director_cb_phone_Contact_Type" runat="server" AllowBlank="false" FieldLabel="Contact Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_Contact_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model21">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <ext:ComboBox ID="Director_cb_phone_Communication_Type" runat="server" AllowBlank="false" FieldLabel="Communication Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_Communication_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model22">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>

                            <ext:TextField ID="Director_txt_phone_Country_prefix" runat="server" FieldLabel="Country Prefix" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_txt_phone_number" AllowBlank="false" runat="server" FieldLabel="Phone Number" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_txt_phone_extension" runat="server" FieldLabel="Extension" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_txt_phone_comments" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSaveDirectorPhonedetail" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveDirectorPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBackSaveDirectorPhoneDetail" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>

                    <ext:FormPanel ID="FormPanelEmpDirectorTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Director_Emp_cb_phone_Contact_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_Emp_cb_phone_Contact_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Kategori_Kontak" StringFilter="Active = 1" EmptyText="Select One" Label="Contact Type" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_Director_Emp_cb_phone_Communication_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_Emp_cb_phone_Communication_Type" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Jenis_Alat_Komunikasi" StringFilter="Active = 1" EmptyText="Select One" Label="Communication Type" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="Director_Emp_cb_phone_Contact_Type" AllowBlank="false" runat="server" FieldLabel="Contact Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Emp_Store_Contact_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model23">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <ext:ComboBox ID="Director_Emp_cb_phone_Communication_Type" runat="server" AllowBlank="false" FieldLabel="Communication Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Emp_Store_Communication_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model24">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>

                            <ext:TextField ID="Director_Emp_txt_phone_Country_prefix" runat="server" FieldLabel="Country Prefix" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Emp_txt_phone_number" AllowBlank="false" runat="server" FieldLabel="Phone Number" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Emp_txt_phone_extension" runat="server" FieldLabel="Extension" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Emp_txt_phone_comments" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_saveDirector_empphone" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveDirectorEmpPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button13" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailDirectorAddress" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Director Address Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet4" runat="server" Title="Address" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FP_Address_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                             <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Director_cmb_kategoriaddress" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_cmb_kategoriaddress" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Kategori_Kontak" StringFilter="Active = 1" EmptyText="Select One" Label="Address Type" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="Director_cmb_kategoriaddress" AllowBlank="false" runat="server" FieldLabel="Address Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_KategoriAddress">
                                        <Model>
                                            <ext:Model runat="server" ID="Model25">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="Director_Address" AllowBlank="false" runat="server" FieldLabel="Address" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Town" runat="server" FieldLabel="Town" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_City" runat="server" AllowBlank="false" FieldLabel="City" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Zip" runat="server" FieldLabel="Zip Code" AnchorHorizontal="90%"></ext:TextField>
                             <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Director_cmb_kodenegara" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_cmb_kodenegara" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="Director_cmb_kodenegara" runat="server" AllowBlank="false" FieldLabel="Country"
                                DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_Kode_Negara" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model26">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="Director_State" runat="server" FieldLabel="Province" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Comment_Address" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Btn_Save_Director_Address" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveDirectorAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Btn_Back_Director_Address" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                    <ext:FormPanel ID="FP_Address_Emp_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                             <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Director_cmb_emp_kategoriaddress" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_cmb_emp_kategoriaddress" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Kategori_Kontak" StringFilter="Active = 1" EmptyText="Select One" Label="Address Type" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="Director_cmb_emp_kategoriaddress" AllowBlank="false" runat="server" FieldLabel="Address Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_emp_KategoriAddress">
                                        <Model>
                                            <ext:Model runat="server" ID="Model27">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="Director_emp_Address" AllowBlank="false" runat="server" FieldLabel="Address" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_Town" runat="server" FieldLabel="Town" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_City" runat="server" AllowBlank="false" FieldLabel="City" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_Zip" runat="server" FieldLabel="Zip Code" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Director_cmb_emp_kodenegara" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_cmb_emp_kodenegara" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="Director_cmb_emp_kodenegara" runat="server" AllowBlank="false" FieldLabel="Country" DisplayField="Keterangan"
                                ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_emp_Kode_Negara" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model28">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="Director_emp_State" runat="server" FieldLabel="Province" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_Comment_Address" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_Director_saveempaddress" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveDirectorEmpAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button15" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailDirector" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Director Add" Hidden="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet3" runat="server" Title="Director" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>

                <Content>
                    <ext:FormPanel ID="FP_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Hidden runat="server" ID="Hidden2"></ext:Hidden>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_peran" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_peran" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Peran_orang_dalam_Korporasi" StringFilter="Active = 1" EmptyText="Select One" Label="Peran" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cmb_peran" runat="server" FieldLabel="Peran" AllowBlank="false" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" LabelWidth="250" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="StorePeran">
                                        <Model>
                                            <ext:Model runat="server" ID="Model7">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                </DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="txt_Director_Title" LabelWidth="250" runat="server" FieldLabel="Title" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Last_Name" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_Director_Gender" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Director_Gender" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Jenis_Kelamin" StringFilter="Active = 1" EmptyText="Select One" Label="Gender" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%-- <ext:ComboBox LabelWidth="250" ID="cmb_Director_Gender" runat="server" FieldLabel="Gender" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" AllowBlank="false" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Director_Gender" OnReadData="Gender_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model9">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Date of Birth" ID="txt_Director_BirthDate" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_Director_Birth_Place" LabelWidth="250" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Mothers_Name" LabelWidth="250" runat="server" FieldLabel="Mother Maiden Name" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Alias" LabelWidth="250" runat="server" FieldLabel="Alias Name" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_SSN" LabelWidth="250" runat="server" FieldLabel="NIK/SSN" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Passport_Number" LabelWidth="250" runat="server" FieldLabel="Passport No" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_11092020 Mengubah Passport Issued Country menjadi dropdown--%>
                            <ext:Panel ID="Pnl_cmb_Director_Passport_Country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Director_Passport_Country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Passport Issued Country" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <%--end Update--%>
                            <ext:TextField ID="txt_Director_ID_Number" LabelWidth="250" runat="server" FieldLabel="Other ID Number" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_Director_Nationality1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Director_Nationality1" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 1" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_cmb_Director_Nationality2" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Director_Nationality2" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 2" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_cmb_Director_Nationality3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Director_Nationality3" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 3" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_cmb_Director_Residence" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Director_Residence" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Domicile Country" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox LabelWidth="250" AllowBlank="False" ID="cmb_Director_Nationality1" runat="server" FieldLabel="Nationality 1"
                                DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="90%" ForceSelection="true" IndicatorText="*">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Director_Nationality1" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model10">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>
                            <ext:ComboBox ID="cmb_Director_Nationality2" LabelWidth="250" runat="server" FieldLabel="Nationality 2" DisplayField="Keterangan" ValueField="Kode"
                                EmptyText="Select One" AnchorHorizontal="90%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Director_Nationality2" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model11">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>
                            <ext:ComboBox ID="cmb_Director_Nationality3" LabelWidth="250" runat="server" FieldLabel="Nationality 3" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="90%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Director_Nationality3" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model12">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>
                            <ext:ComboBox ID="cmb_Director_Residence" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Domicile Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="90%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Director_Residence" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model13">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%-- </DirectEvents>
                            </ext:ComboBox>--%>
                            
                            <%--<ext:TextField ID="txt_Director_Email" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Email2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Email3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Email4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Email5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%"></ext:TextField>--%>

                            <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Director_Deceased" FieldLabel="Has Passed Away ?" AnchorHorizontal="90%">
                                <DirectEvents>
                                    <Change OnEvent="checkBoxcb_Director_Deceased_click" />
                                </DirectEvents>
                            </ext:Checkbox>
                            <ext:DateField Editable="false" runat="server" LabelWidth="250" Hidden="true" FieldLabel="Date of Passed Away " Format="dd-MMM-yyyy" ID="txt_Director_Deceased_Date" AnchorHorizontal="90%" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Director_Tax_Reg_Number" FieldLabel="Is PEP?">
                            </ext:Checkbox>
                            <ext:TextField runat="server" LabelWidth="250" FieldLabel="Tax Number" ID="txt_Director_Tax_Number" />
                            <ext:TextField runat="server" LabelWidth="250" FieldLabel="Source of Wealth" ID="txt_Director_Source_of_Wealth" AnchorHorizontal="90%" />
                            <ext:TextField ID="txt_Director_Occupation" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%"></ext:TextField>

                            <ext:TextField runat="server" LabelWidth="250" FieldLabel="Notes" ID="txt_Director_Comments" />
                            <ext:TextField ID="txt_Director_employer_name" LabelWidth="250" runat="server" FieldLabel="Work Place" AnchorHorizontal="90%"></ext:TextField>
                            <%--Director Identificiation--%>
                            <ext:GridPanel ID="GP_DirectorIdentification" runat="server" Title="Identification" AutoScroll="true">
                                <TopBar>
                                    <ext:Toolbar ID="toolbar12" runat="server">
                                        <Items>
                                            <ext:Button ID="btnaddidentificationdirector" runat="server" Text="Add Identification" Icon="Add">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAddDirectorIdentifications_DirectClick">
                                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="Store_Director_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model8" runat="server">
                                                <%--to do--%>
                                                <Fields>
                                                    <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Type_Document" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Issued_By" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Issue_Date" Type="Date"></ext:ModelField>
                                                    <ext:ModelField Name="Expiry_Date" Type="Date"></ext:ModelField>
                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Identification_Comment" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                        <ext:Column ID="Column75" runat="server" DataIndex="Number" Text="Identification Number" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column77" runat="server" DataIndex="Type_Document" Text="Identification Type" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column78" runat="server" DataIndex="Issued_By" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:DateColumn ID="Column79" runat="server" DataIndex="Issue_Date" Text="Issued Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                        <ext:DateColumn ID="Column80" runat="server" DataIndex="Expiry_Date" Text="Expired Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                        <ext:Column ID="Column81" runat="server" DataIndex="Country" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column82" runat="server" DataIndex="Identification_Comment" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn13" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdDirectorIdentification">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--end of Director Identification--%>


                            <ext:GridPanel ID="GP_DirectorPhone" runat="server" Title="Phone" AutoScroll="true">
                                <TopBar>
                                    <ext:Toolbar ID="toolbar10" runat="server">
                                        <Items>
                                            <ext:Button ID="btnsavedirectorphone" runat="server" Text="Add Phones" Icon="Add">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAddDirectorPhones_DirectClick">
                                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="Store_DirectorPhone" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model6" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                        <ext:Column ID="Column61" runat="server" DataIndex="Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column62" runat="server" DataIndex="Communcation_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column63" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column64" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column65" runat="server" DataIndex="tph_extension" Text="Extension" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column66" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn11" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdDirectorPhone">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>

                            <%--Corp Address--%>
                            <ext:GridPanel ID="GP_DirectorAddress" runat="server" Title="Address" AutoScroll="true">
                                <TopBar>
                                    <ext:Toolbar ID="toolbar11" runat="server">
                                        <Items>
                                            <ext:Button ID="btnsavedirectorAddress" runat="server" Text="Add Addresses" Icon="Add">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAddDirectorAddresses_DirectClick">
                                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="Store_DirectorAddress" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model16" runat="server">
                                                <%--to do--%>
                                                <Fields>
                                                    <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                        <ext:Column ID="Column67" runat="server" DataIndex="Type_Address_Detail" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column68" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column69" runat="server" DataIndex="Town" Text="Town" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column70" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column71" runat="server" DataIndex="Zip" Text="Zip Code" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column72" runat="server" DataIndex="Country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column73" runat="server" DataIndex="State" Text="Province" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column74" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn12" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdDirectorAddress">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--end of Corp Address--%>
                        
                        <ext:GridPanel ID="GP_DirectorEmpPhone" runat="server" Title="Work Place Phone" AutoScroll="true">
                                <TopBar>
                                    <ext:Toolbar ID="toolbar8" runat="server">
                                        <Items>
                                            <ext:Button ID="btnsaveEmpdirectorPhone" runat="server" Text="Add Phones" Icon="Add">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAddDirectorEmployerPhones_DirectClick">
                                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="Store_Director_Employer_Phone" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model14" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                        <ext:Column ID="Column47" runat="server" DataIndex="Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column48" runat="server" DataIndex="Communcation_Type" Text="Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column49" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column50" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column51" runat="server" DataIndex="tph_extension" Text="Extension" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column52" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn9" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdDirectorEmployerPhone">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>

                            <%--Employer Address--%>
                            <ext:GridPanel ID="GP_DirectorEmpAddress" runat="server" Title="Work Place Address" AutoScroll="true">
                                <TopBar>
                                    <ext:Toolbar ID="toolbar9" runat="server">
                                        <Items>
                                            <ext:Button ID="btnsaveEmpdirectorAddress" runat="server" Text="Add Addresses" Icon="Add">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAddDirectorEmployerAddresses_DirectClick">
                                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="Store_Director_Employer_Address" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model15" runat="server">
                                                <%--to do--%>
                                                <Fields>
                                                    <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                        <ext:Column ID="Column53" runat="server" DataIndex="Type_Address_Detail" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column54" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column55" runat="server" DataIndex="Town" Text="Town" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column56" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column57" runat="server" DataIndex="Zip" Text="Zip Code" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column58" runat="server" DataIndex="Country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column59" runat="server" DataIndex="State" Text="Province" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column60" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn10" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdDirectorEmployerAddress">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--end of Employer Address--%>

                            <ext:Panel ID="Panel_Director_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEmail runat="server" ID="GridEmail_Director" FK_REF_DETAIL_OF="31"/>
                                </Content>
                            </ext:Panel>

                            <ext:Panel ID="Panel_Director_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridSanction runat="server" ID="GridSanction_Director" UniqueName="SanctionDirector"/>
                                </Content>
                            </ext:Panel>

                            <ext:Panel ID="Panel_Director_GridPEP" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridPEP runat="server" ID="GridPEP_Director"/>
                                </Content>
                            </ext:Panel>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnsaveDirector" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomerDirector_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button9" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerDirector_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailIdentification" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet2" runat="server" Title="Identification" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FP_Identification_INDV" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_Jenis_Dokumen" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Jenis_Dokumen" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringFilter="Active = 1" StringTable="goaml_ref_jenis_dokumen_identitas" EmptyText="Select One" Label="Identification Type" AnchorHorizontal="80%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_number_identification" runat="server" FieldLabel="Identification Number" AnchorHorizontal="90%" AllowBlank="false"></ext:TextField>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cmb_Jenis_Dokumen" runat="server" FieldLabel="Identification Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_jenis_dokumen">
                                        <Model>
                                            <ext:Model runat="server" ID="Model3">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:ComboBox ID="cmb_issued_country" runat="server" FieldLabel="Issued Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_issued_country">
                                        <Model>
                                            <ext:Model runat="server" ID="Model4">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%-- </DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="txt_issued_by" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%"></ext:TextField>
                            <ext:DateField Editable="false" ID="txt_issued_date" runat="server" FieldLabel="Issued Date" AnchorHorizontal="90%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:DateField Editable="false" ID="txt_issued_expired_date" runat="server" FieldLabel="Issued Expired Date" AnchorHorizontal="90%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Panel ID="Pnl_cmb_issued_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_issued_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Issued Country" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_identification_comment" runat="server" FieldLabel="Comment" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_save_identification" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomerIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button5" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <ext:FormPanel ID="FP_Identification_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_number_identification_Director" runat="server" FieldLabel="Identification Number" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_Jenis_Dokumen_Director" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Jenis_Dokumen_Director" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goaml_ref_jenis_dokumen_identitas" StringFilter="Active = 1" EmptyText="Select One" Label="Identification Type" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_cmb_issued_country_Director" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_issued_country_Director" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Issued Country" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cmb_Jenis_Dokumen_Director" runat="server" FieldLabel="Identification Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Storejenisdokumen_Director">
                                        <Model>
                                            <ext:Model runat="server" ID="Model29">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:ComboBox ID="cmb_issued_country_Director" runat="server" FieldLabel="Issued Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreIssueCountry_Director">
                                        <Model>
                                            <ext:Model runat="server" ID="Model30">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="txt_issued_by_Director" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%"></ext:TextField>
                            <ext:DateField Editable="false" ID="txt_issued_date_Director" runat="server" FieldLabel="Issued Date" AnchorHorizontal="90%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:DateField Editable="false" ID="txt_issued_expired_date_Director" runat="server" FieldLabel="Issued Expired Date" AnchorHorizontal="90%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:TextField ID="txt_identification_comment_Director" runat="server" FieldLabel="Comment" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_save_identification_Director" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveDirectorIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button8" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up, Add by Septian, goAML v5.0.1--%>
    <ext:Window ID="WindowDetailPreviousName" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Previous Name Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet5" runat="server" Title="Previous Name" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanelPreviousName" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txtCustomer_first_name" runat="server" FieldLabel="First Name" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:TextField ID="txtCustomer_last_name" AllowBlank="False" runat="server" FieldLabel="Last Name" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txtCustomer_comments" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" ></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_Customer_SavePreviousName" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomer_PreviousName_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btn_Customer_BackPreviousName" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_PreviousName_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_Email" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Email Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet6" runat="server" Title="Email" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_Email" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txtCustomer_email_address" runat="server" FieldLabel="Email Address" AnchorHorizontal="90%" AllowBlank="False" MaxLength="255"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_Customer_Save_Email" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomer_Email_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btn_Customer_Back_Email" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_Email_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_Employment_History" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Email Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet7" runat="server" Title="Employment History" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_Employment_History" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_employer_name" runat="server" FieldLabel="Employer Name" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:TextField ID="txt_employer_business" runat="server" FieldLabel="Employer Business" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:TextField ID="txt_employer_identifier" runat="server" FieldLabel="Employer Identifier" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:DateField ID="df_valid_from" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Valid From" AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_is_approx_from_date" FieldLabel="Is Approx From Date?"></ext:Checkbox>
                            <ext:DateField ID="df_valid_to" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Valid To" AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_is_approx_to_date" FieldLabel="Is Approx To Date?"></ext:Checkbox>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_Customer_Save_Employment_History" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomer_EmploymentHistory_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btn_Customer_Back_Employment_History" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_EmploymentHistory_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_Person_PEP" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Person PEP Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet8" runat="server" Title="Person PEP" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_Person_PEP" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_Person_PEP_Country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_PersonPEP_Country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%" AllowBlank="false" />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_function_name" LabelWidth="250" runat="server" FieldLabel="Function Name" AnchorHorizontal="90%" AllowBlank="True" MaxLength="255"></ext:TextField>
                            <ext:TextField ID="txt_function_description" LabelWidth="250" runat="server" FieldLabel="Function Description" AnchorHorizontal="90%" AllowBlank="True" MaxLength="8000"></ext:TextField>
                            <ext:DateField Hidden="true" ID="df_person_pep_valid_from" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Valid From" AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Checkbox Hidden="true" runat="server" LabelWidth="250" ID="cbx_pep_is_approx_from_date" FieldLabel="Is Approx From Date?"></ext:Checkbox>
                            <ext:DateField Hidden="true" ID="df_pep_valid_to" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Valid To" AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Checkbox Hidden="true" runat="server" LabelWidth="250" ID="cbx_pep_is_approx_to_date" FieldLabel="Is Approx To Date?"></ext:Checkbox>
                            <ext:TextField ID="txt_pep_comments" LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" AllowBlank="True" MaxLength="8000"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_Customer_Save_PEP" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomer_PersonPEP_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btn_Customer_Back_PEP" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_PersonPEP_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_NetworkDevice" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Network Device Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet9" runat="server" Title="Network Device" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_NetworkDevice" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_device_number" runat="server" FieldLabel="Device Number" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:Panel ID="Panel_Network_Device_OS" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_network_device_os" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Operating_System" StringFilter="Active = 1" EmptyText="Select One" Label="Operating System" AnchorHorizontal="70%" AllowBlank="false" />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_service_provider" runat="server" FieldLabel="Service Provider" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:TextField ID="txt_ipv6" runat="server" FieldLabel="IP v6" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:TextField ID="txt_ipv4" runat="server" FieldLabel="IP v4" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:TextField ID="txt_cgn_port" runat="server" FieldLabel="CGN Port" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:TextField ID="txt_ipv4_ipv6" runat="server" FieldLabel="IPV6 with IPV4" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:TextField ID="txt_nat" runat="server" FieldLabel="NAT" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:DateField ID="df_first_seen_date" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="First Seen Date" AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:DateField ID="df_last_seen_date" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Last Seen Date" AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_using_proxy" FieldLabel="Using Proxy?"></ext:Checkbox>
                            <ext:TextField ID="txt_city" runat="server" FieldLabel="City" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:Panel ID="Panel_NetworkDevice_Country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_network_device_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%" AllowBlank="false" />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_network_device_comments" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnCustomer_Save_NetworkDevice" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomer_NetworkDevice_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnCustomer_Back_NetworkDevice" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_NetworkDevice_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_SocialMedia" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Social Media Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet10" runat="server" Title="Social Media" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_SocialMedia" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_socmed_platform" runat="server" FieldLabel="Platform" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:TextField ID="txt_socmed_username" runat="server" FieldLabel="Username" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:TextField ID="txt_socmed_comments" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSaveCustomer_SocialMedia" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomer_SocialMedia_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBackCustomer_SocialMedia" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_SocialMedia_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_Sanction" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Sanction Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet15" runat="server" Title="Sanction" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_Sanction" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_sanction_provider" runat="server" FieldLabel="Provider" AnchorHorizontal="90%" AllowBlank="False" MaxLength="255"></ext:TextField>
                            <ext:TextField ID="txt_sanction_sanction_list_name" runat="server" FieldLabel="Sanction List Name" AnchorHorizontal="90%" AllowBlank="False" MaxLength="255"></ext:TextField>
                            <ext:TextField ID="txt_sanction_match_criteria" runat="server" FieldLabel="Match Criteria" AnchorHorizontal="90%" AllowBlank="True" MaxLength="8000"></ext:TextField>
                            <ext:TextField ID="txt_sanction_link_to_source" runat="server" FieldLabel="Link to Source" AnchorHorizontal="90%" AllowBlank="True" MaxLength="255"></ext:TextField>
                            <ext:TextField ID="txt_sanction_sanction_list_attributes" runat="server" FieldLabel="Sanction List Attributes" AnchorHorizontal="90%" AllowBlank="True" MaxLength="8000"></ext:TextField>
                            <ext:DateField Hidden="true" Editable="false" AllowBlank="False" runat="server" FieldLabel="Valid From" ID="df_sanction_valid_from" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox Hidden="true" runat="server" ID="cbx_sanction_is_approx_from_date" FieldLabel="Is Approx From Date?"></ext:Checkbox>
                            <ext:DateField Hidden="true" Editable="false" AllowBlank="False" runat="server" FieldLabel="Valid To" ID="df_sanction_valid_to" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox Hidden="true" runat="server" ID="cbx_sanction_is_approx_to_date" FieldLabel="Is Approx To Date?"></ext:Checkbox>
                            <ext:TextField ID="txt_sanction_comments" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" AllowBlank="True" MaxLength="8000"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSaveCustomer_Sanction" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomer_Sanction_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button17" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_Sanction_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_RelatedPerson" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Related Person Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet11" runat="server" Title="Related Person" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_RelatedPerson" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_cmb_rp_person_relation" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <%-- 2023-10-11, Nael: 'Person Person Relation' -> 'Person Relation' --%>
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_person_relation" AllowBlank="false" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Person_Person_Relationship" StringFilter="Active = 1" EmptyText="Select One" Label="Person Relation" AnchorHorizontal="70%"  />
	                            </Content>
                            </ext:Panel>
                            <%-- 2023-10-14, Nael: Memindahkan field relation comments --%>
                            <ext:TextField ID="txt_rp_relation_comments" LabelWidth="250" runat="server" FieldLabel="Relation Comments" AnchorHorizontal="90%"  MaxLength="8000"></ext:TextField>
                            <%--<ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid From" ID="df_relation_date_range_valid_from" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_relation_date_range_is_approx_from_date" FieldLabel="Relation Date Range -  Is Approx From Date?"></ext:Checkbox>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid To" ID="df_relation_date_range_valid_to" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_relation_date_range_is_approx_to_date" FieldLabel="Relation Date Range -  Is Approx To Date?"></ext:Checkbox>--%>
                            <ext:Panel ID="Panel_cmb_rp_gender" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_gender" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Jenis_Kelamin" StringFilter="Active = 1" EmptyText="Select One" Label="Gender" AnchorHorizontal="70%"  />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_rp_title" LabelWidth="250" runat="server" FieldLabel="Title" AnchorHorizontal="90%"  MaxLength="30"></ext:TextField>
                            <ext:TextField ID="txt_rp_first_name" Hidden="true" LabelWidth="250" runat="server" FieldLabel="First Name" AnchorHorizontal="90%"  MaxLength="100"></ext:TextField>
                            <ext:TextField ID="txt_rp_middle_name" Hidden="true" LabelWidth="250" runat="server" FieldLabel="Middle Name" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                            <ext:TextField ID="txt_rp_prefix" Hidden="true" LabelWidth="250" runat="server" FieldLabel="Prefix" AnchorHorizontal="90%"  MaxLength="100"></ext:TextField>
                            <ext:TextField ID="txt_rp_last_name" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="90%"  MaxLength="100"></ext:TextField>
                            <%-- 2023-10-11, Nael: 'Birth Date' -> 'Date of Birth' --%>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Date of Birth" ID="df_birthdate" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%-- 2023-10-11, Nael: 'Birth Place' -> 'Place of Birth' --%>
                            <ext:TextField ID="txt_rp_birth_place" LabelWidth="250" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:Panel ID="Panel_cmb_rp_country_of_birth" Hidden="true" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_country_of_birth" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country Of Birth" AnchorHorizontal="70%"  />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_rp_mothers_name" LabelWidth="250" runat="server" FieldLabel="Mothers Name" AnchorHorizontal="90%"  MaxLength="100"></ext:TextField>
                            <ext:TextField ID="txt_rp_alias" LabelWidth="250" runat="server" FieldLabel="Alias" AnchorHorizontal="90%"  MaxLength="100"></ext:TextField>
                           <%-- <ext:TextField ID="txt_rp_full_name_frn" runat="server" FieldLabel="Full Foreign Name" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>--%>
                            <ext:TextField ID="txt_rp_ssn" LabelWidth="250" runat="server" FieldLabel="SSN" AnchorHorizontal="90%"  MaxLength="16"></ext:TextField>
                            <ext:TextField ID="txt_rp_passport_number" LabelWidth="250" runat="server" FieldLabel="Passport Number" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:Panel ID="Panel_cmb_rp_passport_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_passport_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Passport Country" AnchorHorizontal="70%"  />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_rp_id_number" LabelWidth="250" runat="server" FieldLabel="ID Number" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:Panel ID="Panel_cmb_rp_nationality1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_nationality1" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 1" AnchorHorizontal="70%"  />
	                            </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_cmb_rp_nationality2" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_nationality2" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 2" AnchorHorizontal="70%" />
	                            </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_cmb_rp_nationality3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_nationality3" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 3" AnchorHorizontal="70%" />
	                            </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_cmb_rp_residence" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_residence" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Domicile Country" AnchorHorizontal="70%"  />
	                            </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridPhone" Title="Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridPhone runat="server" ID="GridPhone_RelatedPerson" FK_REF_DETAIL_OF="31" UniqueName="RelatedPerson"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridAddress" Title="Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridAddress runat="server" ID="GridAddress_RelatedPerson" FK_REF_DETAIL_OF="31" UniqueName="RelatedPerson"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEmail runat="server" ID="GridEmail_RelatedPerson" FK_REF_DETAIL_OF="31" UniqueName="RelatedPerson" />
                                </Content>
                            </ext:Panel>
                            <%--<ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Residence Since" ID="df_rp_residence_since" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />--%>
                            <ext:TextField ID="txt_rp_occupation" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <%-- 2023-10-17, Nael: Menambahkan textfield Work Place --%>
                            <ext:TextField ID="txt_rp_employer_name" LabelWidth="250" runat="server" FieldLabel="Work Place" AnchorHorizontal="90%"></ext:TextField>
                            <ext:Panel ID="Panel_RelatedPerson_GridPhoneWork" Title="Work Place Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridPhoneWork runat="server" ID="GridPhoneWork_RelatedPerson" FK_REF_DETAIL_OF="31" UniqueName="RelatedPerson"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridAddressWork" Title="Work Place Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridAddressWork runat="server" ID="GridAddressWork_RelatedPerson" FK_REF_DETAIL_OF="31" UniqueName="RelatedPerson"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridIdentification" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridIdentification runat="server" ID="GridIdentification_RelatedPerson" FK_REF_DETAIL_OF="31" UniqueName="RelatedPerson"/>
                                </Content>
                            </ext:Panel>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_rp_deceased" FieldLabel="Has Passed Away?"></ext:Checkbox>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Date of Passed Away" ID="df_rp_date_deceased" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_rp_tax_number" LabelWidth="250" runat="server" FieldLabel="Tax Number" AnchorHorizontal="90%"  MaxLength="16"></ext:TextField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_rp_tax_reg_number" FieldLabel="Is PEP?"></ext:Checkbox>
                            <ext:TextField ID="txt_rp_source_of_wealth" LabelWidth="250" runat="server" FieldLabel="Source Of Wealth" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_rp_is_protected" FieldLabel="Is Protected?"></ext:Checkbox>

                            <ext:Panel ID="Panel_RelatedPerson_GridPEP" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridPEP runat="server" ID="GridPEP_RelatedPerson" UniqueName="RelatedPerson" FK_REF_DETAIL_OF="31" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridSanction runat="server" ID="GridSanction_RelatedPerson" UniqueName="RelatedPerson" FK_REF_DETAIL_OF="31" />
                                </Content>
                            </ext:Panel>

                            <ext:TextField ID="txt_rp_comments" LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                            
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSaveCustomer_RelatedPerson" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomer_RelatedPerson_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBackCustomer_RelatedPerson" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_RelatedPerson_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_AdditionalInfo" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Additional Info Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet16" runat="server" Title="Additional Info" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_AdditionalInfo" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_InfoType" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_info_type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Additional_Info_Type" StringFilter="Active = 1" EmptyText="Select One" Label="Residence" AnchorHorizontal="70%" AllowBlank="false" />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_info_subject" runat="server" FieldLabel="Info Subject" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:TextField ID="txt_info_text" runat="server" FieldLabel="Info Text" AnchorHorizontal="90%" AllowBlank="False"></ext:TextField>
                            <ext:NumberField runat="server" LabelWidth="200" ID="nfd_info_numeric" FieldLabel="Info Numeric" AnchorHorizontal="100%" MouseWheelEnabled="false" FormatText="#,##0.00" />
                            <ext:DateField Editable="false" LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Info Date" ID="df_info_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_info_boolean" FieldLabel="Info Boolean"></ext:Checkbox>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSaveCustomer_AdditionalInfo" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomer_AdditionalInfo_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button19" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_AdditionalInfo_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_URL" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity URL/Website Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet12" runat="server" Title="Entity URL/Website" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_URL" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_url" runat="server" FieldLabel="URL" AnchorHorizontal="90%" AllowBlank="False" MaxLength="255"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSaveCustomer_URL" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomer_URL_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBackCustomer_URL" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_URL_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_EntityIdentification" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet13" runat="server" Title="Entity Identification" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_EntityIdentification" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_cmb_ef_type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_ef_type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Identifier_Type" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Identifier Type" AnchorHorizontal="70%" AllowBlank="false" />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_ef_number" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Number" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Issued Date" ID="df_issue_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Expiry Date" ID="df_expiry_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_ef_issued_by" LabelWidth="250" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:Panel ID="pnl_ef_issue_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_ef_issue_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country Of Birth" AnchorHorizontal="70%" AllowBlank="false" />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_ef_comments" LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="50"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSaveCustomer_EntityIdentification" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomer_EntityIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBackCustomer_EntityIdentification" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_EntityIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_RelatedEntities" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet14" runat="server" Title="Related Entity" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_RelatedEntities" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--5 --%>
                            <ext:Panel ID="Panel_cmb_re_entity_relation" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_re_entity_relation" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Entity_Relationship" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Relation" AnchorHorizontal="70%" AllowBlank="false" />
	                            </Content>
                            </ext:Panel>
                         
                           <%-- <ext:DateField Editable="false" LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Relation Date Range -  Valid From" ID="df_re_relation_date_range_valid_from" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                          
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_re_relation_date_range_is_approx_from_date" FieldLabel="Relation Date Range -  Is Approx From Date?"></ext:Checkbox>
                         
                            <ext:DateField Editable="false" LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Relation Date Range -  Valid To" ID="df_re_relation_date_range_valid_to" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                           
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_re_relation_date_range_is_approx_to_date" FieldLabel="Relation Date Range -  Is Approx To Date?"></ext:Checkbox>
                           
                            <ext:NumberField runat="server" LabelWidth="250" ID="nfd_re_share_percentage" FieldLabel="Share Percentage" AnchorHorizontal="70%" MouseWheelEnabled="false" AllowBlank="False" MinValue="0" EmptyNumber="0" Number="0" />--%>
                            <%--11 --%>
                            <ext:TextField ID="txt_re_relation_comments" LabelWidth="250" runat="server" FieldLabel="Relation Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                            <%--12 --%>
                            <%-- 2023-11-22, Nael: txt_re_name harusnya mandatory --%>
                            <ext:TextField ID="txt_re_name" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Corporate Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--13 --%>
                            <ext:TextField ID="txt_re_commercial_name" LabelWidth="250" runat="server" FieldLabel="Commercial Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--14 --%>
                            <ext:Panel ID="Panel_cmb_re_incorporation_legal_form" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_re_incorporation_legal_form" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Bentuk_Badan_Usaha" StringFilter="Active = 1" EmptyText="Select One" Label="Legal Form" AnchorHorizontal="70%" />
	                            </Content>
                            </ext:Panel>
                            <%--15 --%>
                            <ext:TextField ID="txt_re_incorporation_number" LabelWidth="250" runat="server" FieldLabel="Corporate License Number" AnchorHorizontal="90%" MaxLength="50"></ext:TextField>
                            <%--16 --%>
                            <ext:TextField ID="txt_re_business" LabelWidth="250" runat="server" FieldLabel="Line of Business" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                        
                           <%-- <ext:Panel ID="Panel_cmb_re_entity_status" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_re_entity_status" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Status" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Status" AnchorHorizontal="70%" AllowBlank="false" />
	                            </Content>
                            </ext:Panel>
                        
                            <ext:DateField Editable="false" LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Entity Status Date" ID="df_re_entity_status_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />--%>
                            <%--19 --%>
                            <ext:TextField ID="txt_re_incorporation_state" LabelWidth="250" runat="server" FieldLabel="Province" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--20 --%>
                            <ext:Panel ID="Panel_cmb_re_incorporation_country_code" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_re_incorporation_country_code" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%" />
	                            </Content>
                            </ext:Panel>
                            <%--21 --%>
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Date of Established" ID="df_re_incorporation_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%--22 --%>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_re_business_closed" FieldLabel="Businesss Closed?"></ext:Checkbox>
                            <%--23 --%>
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Date Business Closed" ID="df_re_date_business_closed" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%--24 --%>
                            <ext:TextField ID="txt_re_tax_number" LabelWidth="250" runat="server" FieldLabel="Tax Number" AnchorHorizontal="90%" MaxLength="16"></ext:TextField>
                            <%--25 --%>
                            <ext:TextField ID="txt_re_tax_reg_number" LabelWidth="250" Hidden="true" runat="server" FieldLabel="Tax Reg Number" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                            
                            <ext:Panel ID="Panel_RelatedEntity_GridPhone" Title="Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridPhone runat="server" ID="GridPhone_RelatedEntity" IsViewMode="false" UniqueName="RelatedEntity"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridAddress" Title="Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridAddress runat="server" ID="GridAddress_RelatedEntity" IsViewMode="false" UniqueName="RelatedEntity"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEmail runat="server" ID="GridEmail_RelatedEntity" IsViewMode="false" UniqueName="RelatedEntity"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_EntityIdentification" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEntityIdentification runat="server" ID="GridEntityIdentification_RelatedEntity" IsViewMode="false" UniqueName="RelatedEntity"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridURL" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridURL runat="server" ID="GridURL_RelatedEntity" IsViewMode="false" UniqueName="RelatedEntity"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridSanction runat="server" ID="GridSanction_RelatedEntity" IsViewMode="false" UniqueName="RelatedEntity"/>
                                </Content>
                            </ext:Panel>

                            <ext:TextField ID="txt_re_comments" LabelWidth="250" runat="server" FieldLabel="Relation Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSaveCustomer_EntityRelation" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomer_RelatedEntities_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button12" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_RelatedEntities_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--End Popup--%>

    <ext:FormPanel ID="FormPanelInput" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="Customer Edit">
        <Items>
            <ext:Hidden runat="server" ID="PK_Customer" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="CIF" ID="txt_CIF" />
            <%--Dedy Added 25082020--%>
            <%--<ext:TextField ID="TextField1" LabelWidth="250" runat="server" FieldLabel="Gelar" AnchorHorizontal="90%"></ext:TextField>--%>
            <%-- <ext:TextField ID="txt_GCN" runat="server" LabelWidth="250" FieldLabel="GCN"  />--%>
            <ext:TextField ID="txt_GCN" runat="server" LabelWidth="250" FieldLabel="GCN">
                <%--Update: Zikri_14092020 Add Validation GCN--%>
                <Listeners>
                    <Change Handler="#{GCNPrimary}.setValue(false)" />
                </Listeners>
                <%--End Update--%>
            </ext:TextField>
            <ext:Checkbox runat="server" LabelWidth="250" ID="GCNPrimary" FieldLabel="GCN Primary ?">
                <%--Update: Zikri_14092020 Add Validation GCN--%>
                <DirectEvents>
                    <Change OnEvent="CheckGCN_DirectEvent"></Change>
                </DirectEvents>
                <%--End Update--%>
            </ext:Checkbox>
              <ext:DateField ID="TextFieldOpeningDate" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Opening Date"  AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>
            <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                  <content>
                        <nds:NDSDropDownField ID="cmb_openingBranch" ValueField="FK_AML_BRANCH_CODE" DisplayField="BRANCH_NAME" runat="server" StringField="FK_AML_BRANCH_CODE,BRANCH_NAME"  StringTable="AML_BRANCH" Label="Opening Branch Code" AnchorHorizontal="70%" AllowBlank="true" LabelWidth="250" />
                        <nds:NDSDropDownField ID="cmb_Status" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan"  StringTable="goAML_Ref_Status_Rekening" Label="Status" AnchorHorizontal="70%" AllowBlank="True" LabelWidth="250" />
             </content>
            </ext:Panel> 
            <ext:DateField ID="TextFieldClosingDate" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Closing Date" AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>
            
            <%-- 2023-10-10, Nael: Hidden TextiFieldComments, disuruh mba Cici --%>
            <ext:TextField ID="TextFieldComments" LabelWidth="250" AllowBlank="True" Hidden="true" runat="server" FieldLabel="Comments" AnchorHorizontal="70%" MaxLength="8000"></ext:TextField>   
            <%--Dedy end added 25082020--%>

            <%--<ext:DateField runat="server" LabelWidth="250" FieldLabel="Tanggal Pembukaan Rekening" ID="txt_Opened" Format="dd-MMM-yyyy"/>
            <ext:DateField runat="server" LabelWidth="250" FieldLabel="Tanggal Penutupan Rekening" ID="txt_Closed" Format="dd-MMM-yyyy"/>

            <ext:NumberField runat="server" LabelWidth="250" FieldLabel="Saldo Akhir" ID="txt_Balance" DecimalPrecision="2"/>
            <ext:DateField runat="server" LabelWidth="250" FieldLabel="Tanggal Saldo" ID="txt_Balance_Date" Format="dd-MMM-yyyy"/>--%>
            <ext:ComboBox ID="Cmb_StatusCode" LabelWidth="250" runat="server" FieldLabel="Status Nasabah" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreStatusCode">
                        <Model>
                            <ext:Model runat="server" ID="ModelStatusCode">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <DirectEvents>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                </DirectEvents>
            </ext:ComboBox>
            <ext:Checkbox runat="server" LabelWidth="250" ID="UpdateFromDataSource" FieldLabel="Update From Data Source ?">
            </ext:Checkbox>
            <%--<ext:TextField ID="txt_beneficiary" LabelWidth="250" runat="server" FieldLabel="Nama Penerima Manfaat Utama" AnchorHorizontal="90%"></ext:TextField>
            <ext:TextField ID="txt_beneficiary_comment" LabelWidth="250" runat="server" FieldLabel="Catatan Terkait Penerima Manfaat Utama" AnchorHorizontal="90%"></ext:TextField>
            <ext:TextField ID="txt_comments" LabelWidth="250" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>--%>
            <%--bikin panel buat Individu ama Korporasi--%>

            <ext:Panel runat="server" Title="Individual Customer" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="panel_INDV" Border="true" BodyStyle="padding:10px">
                <Items>
                    <ext:TextField ID="txt_INDV_Title" LabelWidth="250" runat="server" FieldLabel="Title" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Last_Name" LabelWidth="250" runat="server"  FieldLabel="Full Name" AnchorHorizontal="90%" AllowBlank="false"></ext:TextField>
                  <%--  <ext:TextField ID="txt_INDV_Foreign_Name" LabelWidth="250" runat="server"  FieldLabel="Foreign Language Full Name" AnchorHorizontal="90%"></ext:TextField>--%>
                    <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                    <ext:Panel ID="Pnl_cmb_INDV_Gender" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField AllowBlank="false" ID="cmb_INDV_Gender" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Jenis_Kelamin" StringFilter="Active = 1" LabelWidth="250" Label="Gender" AnchorHorizontal="70%"  />
                        </Content>
                    </ext:Panel>
                    <ext:GridPanel ID="GPCustomer_INDV_PreviousName" runat="server" Title="Previous Name" AutoScroll="true" Visible="false">
                        <TopBar>
                            <ext:Toolbar ID="toolbar13" runat="server">
                                <Items>
                                    <ext:Button ID="Button10" runat="server" Text="Add Previous Name" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_Previous_Name_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Customer_Previous_Name" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Previous_Name_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FIRST_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LAST_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn13" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column23" runat="server" DataIndex="FIRST_NAME" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column76" runat="server" DataIndex="LAST_NAME" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column83" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_INDV_PreviousName" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar13" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--End Update--%>
                    <%--<ext:ComboBox LabelWidth="250" ID="cmb_INDV_Gender" runat="server" FieldLabel="Gender" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%"  ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreINDVGender" OnReadData="Gender_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model2">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                    --%>        <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%--</DirectEvents>
                    </ext:ComboBox>--%>
                    <ext:DateField AllowBlank="false" Editable="false" LabelWidth="250" runat="server" FieldLabel="Date of Birth"  ID="txt_INDV_Birthdate" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                    <ext:TextField AllowBlank="false" ID="txt_INDV_Birth_place" LabelWidth="250" runat="server"  FieldLabel="Place of Birth" AnchorHorizontal="90%"></ext:TextField>
                   <%-- <ext:Panel ID="pnl_cmb_INDV_Country_Of_Birth" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_INDV_Country_Of_Birth" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan"  StringTable="goAML_Ref_Nama_Negara" Label="Country Of Birth" AnchorHorizontal="70%" AllowBlank="true" LabelWidth="250" />
                        </Content>
                    </ext:Panel>--%>
                    <ext:TextField ID="txt_INDV_Mothers_name" LabelWidth="250" runat="server" FieldLabel="Mother Maiden Name" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Alias" LabelWidth="250" runat="server" FieldLabel="Alias Name" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_SSN" LabelWidth="250" runat="server" FieldLabel="NIK/SSN" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Passport_number" LabelWidth="250" runat="server" FieldLabel="Passport No" AnchorHorizontal="90%"></ext:TextField>
                    <%--Update: Zikri_11092020 Mengubah Passport Issued Country menjadi dropdown--%>
                    <ext:Panel ID="Pnl_cmb_INDV_Passport_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_INDV_Passport_country" ValueField="kode" DisplayField="Nama Negara" runat="server" StringField="kode,keterangan" StringFilter="Active = 1" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" Label="Passport Issued Country" AnchorHorizontal="70%" />
                        </Content>
                    </ext:Panel>
                    <%--End Update--%>
                    <%-- <ext:ComboBox LabelWidth="250" ID="cmb_INDV_Passport_country" runat="server" FieldLabel="Passport Issued Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="Store_INDV_PassportCountry" OnReadData="National_readData">
                                <Model>
                                    <ext:Model runat="server" ID="ModelPassportCountry">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <%--end Update--%>
                    <ext:TextField ID="txt_INDV_ID_Number" LabelWidth="250" runat="server" FieldLabel="Other ID Number" AnchorHorizontal="90%"></ext:TextField>
                    <ext:Panel ID="Pnl_cmb_INDV_Nationality1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField AllowBlank="false" ID="cmb_INDV_Nationality1" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringFilter="Active = 1" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" Label="Nationality 1" AnchorHorizontal="70%"  />
                        </Content>
                    </ext:Panel>
                    <%--<ext:ComboBox LabelWidth="250" ID="cmb_INDV_Nationality1"  runat="server" FieldLabel="Nationality 1"
                        DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreNationality1" OnReadData="National_readData">
                                <Model>
                                    <ext:Model runat="server" ID="ModelNationality1">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>--%>
                    <%-- <ext:Panel runat="server" ID="panel9" Layout="AnchorLayout">
                        <Content>
                            <uc1:NDSDropDownField runat="server" ID="cmb_INDV_Nationality1" 
                                StringField="kode, keterangan"
                                StringTable="goAML_Ref_Nama_Negara"
                               StringFilter="Active = 1"
                                StringValue="Kode"
                                
                                Label="Nationality 1" AnchorHorizontal="70%" 
                                />
                        </Content>
                    </ext:Panel>--%>
                    <ext:Panel ID="Pnl_cmb_INDV_Nationality2" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_INDV_Nationality2" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringFilter="Active = 1" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" Label="Nationality 2" AnchorHorizontal="70%" />
                        </Content>
                    </ext:Panel>
                    <%--<ext:ComboBox ID="cmb_INDV_Nationality2" LabelWidth="250" runat="server" FieldLabel="Nationality 2" DisplayField="Keterangan"
                        ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <%--<ext:Store runat="server" ID="StoreNationality2" OnReadData="National_readData" >--%>
                    <%--<ext:Store runat="server" ID="StoreNationality2" OnReadData="National_readData">
                                <Model>
                                    <ext:Model runat="server" ID="ModelNationality2">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%--</DirectEvents>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Pnl_cmb_INDV_Nationality3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_INDV_Nationality3" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringFilter="Active = 1" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" Label="Nationality 3" AnchorHorizontal="70%" />
                        </Content>
                    </ext:Panel>
                    <%--<ext:ComboBox ID="cmb_INDV_Nationality3" LabelWidth="250" runat="server" FieldLabel="Nationality 3" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>--%>
                    <%--<ext:Store runat="server" ClientIDMode="Static" ID="StoreNationality3" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">--%>
                    <%--<ext:Store runat="server" ClientIDMode="Static" ID="StoreNationality3" OnReadData="National_readData">
                                <Model>
                                    <ext:Model runat="server" ID="ModelNationality3">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%--</DirectEvents>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Pnl_cmb_INDV_Residence" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField AllowBlank="false" ID="cmb_INDV_Residence" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringFilter="Active = 1" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" Label="Domicile Country" AnchorHorizontal="70%"  />
                        </Content>
                    </ext:Panel>
                    <%--<ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Residence Since" ID="dte_INDV_ResidenceSince" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />--%>
                   
                    <ext:GridPanel ID="gp_INDV_Phones" runat="server" Title="Phone" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar" runat="server">
                                <Items>
                                    <ext:Button ID="btnAdd_INDV_Phones" runat="server" Text="Add Phones" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddCustomerPhones_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_INDV_Phones" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_INDV_Phones" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumber_INDV_Phones" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="coltph_contact_type" runat="server" DataIndex="Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="coltph_communication_type" runat="server" DataIndex="Communcation_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="coltph_country_prefix" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="coltph_number" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Coltph_extension" runat="server" DataIndex="tph_extension" Text="Extension" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColComments" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <%--Indv Address--%>
                    <ext:GridPanel ID="gp_INDV_Address" runat="server" Title="Address" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnAdd_INDV_Addresses" runat="server" Text="Add Addresses" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddCustomerAddresses_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_INDV_Addresses" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_INDV_Addresses" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumber_INDV_Address" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column1" runat="server" DataIndex="Type_Address_Detail" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column3" runat="server" DataIndex="Town" Text="Town" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column5" runat="server" DataIndex="Zip" Text="Zip Code" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="Country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="State" Text="Province" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end of INDV Address--%>
                    <%-- Email Address --%>
                    <ext:GridPanel ID="GPCustomer_INDV_Email" runat="server" Title="Email" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="tbr_email" runat="server">
                                <Items>
                                    <ext:Button ID="btn_add_email" runat="server" Text="Add Email" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_Email_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Customer_Email" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="model_email" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Email_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EMAIL_ADDRESS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_indv_email" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Customer_Previous_Name_ID" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_indv_email_address" runat="server" DataIndex="EMAIL_ADDRESS" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_INDV_Email" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_Email">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Email_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_indv_email" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Email Address --%>
                    <%--<ext:ComboBox ID="cmb_INDV_Residence" LabelWidth="250"  runat="server" FieldLabel="Domicile Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>--%>
                    <%--<ext:Store runat="server" ClientIDMode="Static" ID="Store1" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">--%>
                    <%--<ext:Store runat="server" ID="StoreResidence" OnReadData="National_readData">
                                <Model>
                                    <ext:Model runat="server" ID="ModelResidence">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%--</DirectEvents>
                    </ext:ComboBox>--%>
                    <%-- comment by septian, goAML 5.0.1 --%>
                    <%-- uncomment by Nael, goAML 5.2 --%>
                    <ext:TextField ID="txt_INDV_Email" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Email2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Email3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Email4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Email5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%"></ext:TextField>
                    <%-- end comment --%>
                    <ext:TextField AllowBlank="false" ID="txt_INDV_Occupation" LabelWidth="250"  runat="server" FieldLabel="Occupation" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Employer_Name" LabelWidth="250" runat="server" FieldLabel="Work Place" AnchorHorizontal="90%"></ext:TextField>
                    
                    <ext:GridPanel ID="GP_Empoloyer_Phone" runat="server" Title="Work Place Phone" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar5" runat="server">
                                <Items>
                                    <ext:Button ID="Button2" runat="server" Text="Add Phones" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddEmployerPhones_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Empoloyer_Phone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column37" runat="server" DataIndex="Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column42" runat="server" DataIndex="Communcation_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column33" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column34" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column35" runat="server" DataIndex="tph_extension" Text="Extension" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column36" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn6" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdEmployerPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <%--Employer Address--%>
                    <ext:GridPanel ID="GP_Employer_Address" runat="server" Title="Work Place Address" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar6" runat="server">
                                <Items>
                                    <ext:Button ID="Button6" runat="server" Text="Add Addresses" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddEmployerAddresses_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Employer_Address" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>

                                            <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column45" runat="server" DataIndex="Type_Address_Detail" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column38" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column39" runat="server" DataIndex="Town" Text="Town" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column40" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column41" runat="server" DataIndex="Zip" Text="Zip Code" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column46" runat="server" DataIndex="Country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column43" runat="server" DataIndex="State" Text="Province" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column44" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn7" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdEmployerAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end of Employer Address--%>
                    <%--Indv Identificiation--%>
                    <ext:GridPanel ID="GP_INDV_Identification" runat="server" Title="Identification" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar4" runat="server">
                                <Items>
                                    <ext:Button ID="Button1" runat="server" Text="Add Identification" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddCustomerIdentifications_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_INDV_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_INDV_Identification" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Document" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Issued_By" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Issue_Date" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Expiry_Date" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identification_Comment" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column26" runat="server" DataIndex="Number" Text="Identification Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column25" runat="server" DataIndex="Type_Document" Text="Identification Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column27" runat="server" DataIndex="Issued_By" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                                <ext:DateColumn ID="Column28" runat="server" DataIndex="Issue_Date" Text="Issued Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                <ext:DateColumn ID="Column29" runat="server" DataIndex="Expiry_Date" Text="Expired Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                <ext:Column ID="Column30" runat="server" DataIndex="Country" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="Identification_Comment" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn5" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end of INDV Identification--%>
                    <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Deceased" FieldLabel="Has Passed Away ?">
                        <DirectEvents>
                            <Change OnEvent="checkBoxcb_Deceased_click" />
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField Editable="false" runat="server" LabelWidth="250" FieldLabel="Date of Passed Away" Hidden="true" Format="dd-MMM-yyyy" ID="txt_deceased_date" AnchorHorizontal="90%" />
                    <ext:Checkbox runat="server" LabelWidth="250" ID="cb_tax" FieldLabel="Is PEP?">
                    </ext:Checkbox>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Tax Number" ID="txt_INDV_Tax_Number" AnchorHorizontal="90%" />
                    <ext:TextField AllowBlank="false" runat="server" LabelWidth="250"  FieldLabel="Source of Wealth" ID="txt_INDV_Source_of_Wealth" AnchorHorizontal="90%" />
                     <ext:Checkbox runat="server" LabelWidth="250" ID="cb_IsProtected" FieldLabel="Is Protected?">
                    </ext:Checkbox>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Notes" ID="txt_INDV_Comments" AnchorHorizontal="90%" />
                    
                    <%--INDV Employment History--%>
                    <ext:GridPanel ID="GPCustomer_INDV_EmploymentHistory" runat="server" Title="Employment History" AutoScroll="true" Visible="false">
                        <TopBar>
                            <ext:Toolbar ID="tbr_employment_history" runat="server">
                                <Items>
                                    <ext:Button ID="btn_employment_history" runat="server" Text="Add Employment History" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_Employment_History_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Customer_Employment_History" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_employment_history" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Employment_History_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EMPLOYER_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EMPLOYER_BUSINESS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EMPLOYER_IDENTIFIER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EMPLOYMENT_PERIOD_VALID_FROM" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE" Type="Boolean"></ext:ModelField>
                                            <ext:ModelField Name="EMPLOYMENT_PERIOD_VALID_TO" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE" Type="Boolean"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_employment_history" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_first_name" runat="server" DataIndex="EMPLOYER_NAME" Text="Employer Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_add_info_last_name" runat="server" DataIndex="EMPLOYER_BUSINESS" Text="Employer Business" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_comments" runat="server" DataIndex="EMPLOYER_IDENTIFIER" Text="Employer Identifier" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_valid_from" runat="server" DataIndex="EMPLOYMENT_PERIOD_VALID_FROM" Text="Employment Period - Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_is_approx_from_date" runat="server" DataIndex="EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE" Text="Employment Period - Is Approx From Date?" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_valid_to" runat="server" DataIndex="EMPLOYMENT_PERIOD_VALID_TO" Text="Employment Period - Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="xlm_is_approx_to_date" runat="server" DataIndex="EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE" Text="Employment Period - Is Approx To Date?" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_INDV_EmploymentHistory" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_EmploymentHistory">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Employment_History_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_employment_history" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end of INDV Employment History--%>
                    
                    <%--INDV Person PEP, Add by Septian--%>
                    <ext:GridPanel ID="GPCustomer_INDV_PEP" runat="server" Title="Person PEP" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="tbr_person_pep" runat="server">
                                <Items>
                                    <ext:Button ID="btn_add_person_pep" runat="server" Text="Add Person PEP" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_PersonPEP_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Customer_Person_PEP" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_person_pep" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_PEP_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PEP_COUNTRY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FUNCTION_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FUNCTION_DESCRIPTION" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PEP_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PEP_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PEP_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PEP_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_person_pep" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_person_pep_first_name" runat="server" DataIndex="PEP_COUNTRY" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_person_pep_function_name" runat="server" DataIndex="FUNCTION_NAME" Text="Function Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_person_pep_description" runat="server" DataIndex="FUNCTION_DESCRIPTION" Text="Description" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="clm_person_pep_valid_from" runat="server" DataIndex="PEP_DATE_RANGE_VALID_FROM" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_person_is_approx_from_date" runat="server" DataIndex="PEP_DATE_RANGE_IS_APPROX_FROM_DATE" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_person_is_valid_to" runat="server" DataIndex="PEP_DATE_RANGE_VALID_TO" Text="Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_person_is_approx_to_date" runat="server" DataIndex="PEP_DATE_RANGE_IS_APPROX_TO_DATE" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="clm_person_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_INDV_PEP" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_PEP">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_PEP_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_add_info" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end of Person PEP--%>

                    <%--INDV Network Device, Add by Septian--%>
                    <ext:GridPanel ID="GPCustomer_INDV_NetworkDevice" runat="server" Title="Network Device" AutoScroll="true" Visible="false">
                        <TopBar>
                            <ext:Toolbar ID="tbr_network_device" runat="server">
                                <Items>
                                    <ext:Button ID="btn_add_network_device" runat="server" Text="Add Network Device" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_NetworkDevice_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Customer_Network_Device" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_network_device" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Network_Device_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DEVICE_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="OPERATING_SYSTEM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SERVICE_PROVIDER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IPV6" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IPV4" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CGN_PORT" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IPV4_IPV6" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAT" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FIRST_SEEN_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LAST_SEEN_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="USING_PROXY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_network_device" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_device_number" runat="server" DataIndex="DEVICE_NUMBER" Text="Device Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_operating_system" runat="server" DataIndex="OPERATING_SYSTEM" Text="Operating System" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_service_provider" runat="server" DataIndex="SERVICE_PROVIDER" Text="Service Provider" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_ip_v6" runat="server" DataIndex="IPV6" Text="Ip V6" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_ip_v4" runat="server" DataIndex="IPV4" Text="Ip V4" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_cgn_port" runat="server" DataIndex="CGN_PORT" Text="CGN Port" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_ip_v6_with_ip_v4" runat="server" DataIndex="IPV4_IPV6" Text="Ip V6 with Ip V4" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_nat" runat="server" DataIndex="NAT" Text="NAT" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_first_seen_date" runat="server" DataIndex="FIRST_SEEN_DATE" Text="First Seen Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_last_seen_date" runat="server" DataIndex="LAST_SEEN_DATE" Text="Last Seen Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_using_proxy" runat="server" DataIndex="USING_PROXY" Text="Using Proxy" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_city" runat="server" DataIndex="CITY" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_country" runat="server" DataIndex="COUNTRY" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_net_device_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_INDV_NetworkDevice" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_NetworkDevice">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Network_Device_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_net_device" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- end of INDV Network Device--%>

                    <%-- INDV Social Media, Add by Septian--%>
                    <ext:GridPanel ID="GPCustomer_INDV_SocialMedia" runat="server" Title="Social Media" AutoScroll="true" Visible="false">
                        <TopBar>
                            <ext:Toolbar ID="tbr_soc_med" runat="server">
                                <Items>
                                    <ext:Button ID="btn_soc_med_add" runat="server" Text="Add Social Media" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_SocialMedia_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Customer_Social_Media" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_soc_med" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Social_Media_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PLATFORM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="USER_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_soc_med" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_soc_med_first_name" runat="server" DataIndex="PLATFORM" Text="Platform" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_soc_med_last_name" runat="server" DataIndex="USER_NAME" Text="Username" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_soc_med_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_INDV_SocialMedia" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_SocialMedia">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Social_Media_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_soc_med" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- end of INDV Social Media --%>

                    <%-- INDV Sanction, Add by Septian--%>
                    <ext:GridPanel ID="GPCustomer_INDV_Sanction" runat="server" Title="Sanction" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="tbr_sanction" runat="server">
                                <Items>
                                    <ext:Button ID="btn_saction_add" runat="server" Text="Add Sanction" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_Sanction_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Customer_Sanction" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_sanction" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Sanction_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PROVIDER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SANCTION_LIST_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="MATCH_CRITERIA" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LINK_TO_SOURCE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SANCTION_LIST_ATTRIBUTES" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_sanction" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_provider" runat="server" DataIndex="PROVIDER" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_sanction_list_name" runat="server" DataIndex="SANCTION_LIST_NAME" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_match_criteria" runat="server" DataIndex="MATCH_CRITERIA" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_link_to_source" runat="server" DataIndex="LINK_TO_SOURCE" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_sanction_attributes" runat="server" DataIndex="SANCTION_LIST_ATTRIBUTES" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="clm_saction_valid_from" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_VALID_FROM" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_sanction_is_approx_from_date" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_sanction_valid_to" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_VALID_TO" Text="Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_sanction_is_approx_to_date" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="clm_sanction_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_INDV_Sanction" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_Sanction">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_sanction" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- end of INDV Sanction --%>

                     <%-- INDV Related Person, Add by Septian--%>
                    <ext:GridPanel ID="GPCustomer_INDV_RelatedPerson"
                        runat="server" Title="Related Person" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="tbr_related_person" runat="server">
                                <Items>
                                    <ext:Button ID="btn_add_related_person" runat="server" Text="Add Related Person" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_RelatedPerson_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Customer_RelatedPerson" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_related_person" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Related_Person_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PERSON_PERSON_RELATION" Type="String"></ext:ModelField>
                                    <%--        <ext:ModelField Name="RELATION_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RELATION_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RELATION_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RELATION_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="GENDER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TITLE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FIRST_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="MIDDLE_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PREFIX" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LAST_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="BIRTH_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="BIRTH_PLACE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY_OF_BIRTH" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="MOTHERS_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ALIAS" Type="String"></ext:ModelField>
                                           <%-- <ext:ModelField Name="FULL_NAME_FRN" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PASSPORT_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PASSPORT_COUNTRY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NATIONALITY1" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NATIONALITY2" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NATIONALITY3" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RESIDENCE" Type="String"></ext:ModelField>
                                           <%-- <ext:ModelField Name="RESIDENCE_SINCE" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="OCCUPATION" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DECEASED" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DATE_DECEASED" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SOURCE_OF_WEALTH" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IS_PROTECTED" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_related_person" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <%-- 2023-10-11, Nael: 'Person Person Relation' -> 'Person Relation' --%>
                                <ext:Column ID="clm_rp_person_relation" runat="server" DataIndex="PERSON_PERSON_RELATION" Text="Person Relation" MinWidth="130" Flex="1"></ext:Column>
                        <%--        <ext:Column ID="clm_rp_valid_from" runat="server" DataIndex="RELATION_DATE_RANGE_VALID_FROM" Text="Relation Date Range -  Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_is_a_from_date" runat="server" DataIndex="RELATION_DATE_RANGE_IS_APPROX_FROM_DATE" Text="Relation Date Range -  Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_valid_to" runat="server" DataIndex="RELATION_DATE_RANGE_VALID_TO" Text="Relation Date Range -  Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_is_a_to_date" runat="server" DataIndex="RELATION_DATE_RANGE_IS_APPROX_TO_DATE" Text="Relation Date Range -  Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                <%--<ext:Column ID="clm_rp_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_gender" runat="server" DataIndex="GENDER" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_title" runat="server" DataIndex="TITLE" Text="Title" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_first_name" runat="server" DataIndex="FIRST_NAME" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_middle_name" runat="server" DataIndex="MIDDLE_NAME" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_prefix" runat="server" DataIndex="PREFIX" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="clm_rp_last_name" runat="server" DataIndex="LAST_NAME" Text="Full Name" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="clm_rp_birth_date" runat="server" DataIndex="BIRTH_DATE" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_birth_place" runat="server" DataIndex="BIRTH_PLACE" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_country_of_birth" runat="server" DataIndex="COUNTRY_OF_BIRTH" Text="Country of Birth" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_mother_name" runat="server" DataIndex="MOTHERS_NAME" Text="Mother name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_alias" runat="server" DataIndex="ALIAS" Text="Alias" MinWidth="130" Flex="1"></ext:Column>--%>
                                <%--<ext:Column ID="clm_rp_foreign_name" runat="server" DataIndex="FULL_NAME_FRN" Text="Full Foreign Name" MinWidth="130" Flex="1"></ext:Column>--%>
                                <%--<ext:Column ID="clm_rp_ssn" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_passport_number" runat="server" DataIndex="PASSPORT_NUMBER" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_passport_country" runat="server" DataIndex="PASSPORT_COUNTRY" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_id_number" runat="server" DataIndex="ID_NUMBER" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>--%>
                                <%-- 2023-10-11, Nael: Hide kolom nationality1 --%>
                                <ext:Column ID="clm_rp_nationality1" runat="server" DataIndex="NATIONALITY1" Hidden="true" Text="Nationality 1" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="clm_rp_nationality2" runat="server" DataIndex="NATIONALITY2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_nationality3" runat="server" DataIndex="NATIONALITY3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_residence" runat="server" DataIndex="RESIDENCE" Text="Residence" MinWidth="130" Flex="1"></ext:Column>--%>
                                <%--<ext:Column ID="clm_rp_residence_since" runat="server" DataIndex="RESIDENCE_SINCE" Text="Residence Since" MinWidth="130" Flex="1"></ext:Column>--%>
                                <%--<ext:Column ID="clm_rp_occupation" runat="server" DataIndex="OCCUPATION" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_deceased" runat="server" DataIndex="DECEASED" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_date_deceased" runat="server" DataIndex="DATE_DECEASED" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_tax_number" runat="server" DataIndex="TAX_NUMBER" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_is_pep" runat="server" DataIndex="TAX_REG_NUMBER" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_source_of_wealth" runat="server" DataIndex="SOURCE_OF_WEALTH" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_rp_is_protected" runat="server" DataIndex="IS_PROTECTED" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:CommandColumn ID="CC_INDV_RelatedPerson" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_RelatedPerson">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_related_person" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- end of INDV Related Person --%>

                    <%-- INDV Additional Information, Add by Septian --%>
                    <ext:GridPanel ID="GPCustomer_INDV_AdditionalInformation" runat="server" Title="Additional Information" AutoScroll="true" Visible="false">
                        <TopBar>
                            <ext:Toolbar ID="tbr_indv_additional_info" runat="server">
                                <Items>
                                    <ext:Button ID="btn_additional_info" runat="server" Text="Add Additional Information" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_AdditionalInfo_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Customer_Additional_Info" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_additional_info" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Additional_Information_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INFO_TYPE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INFO_SUBJECT" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INFO_TEXT" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INFO_NUMERIC" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INFO_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INFO_BOOLEAN" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_additional_info" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_info_type" runat="server" DataIndex="INFO_TYPE" Text="Info Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_info_subject" runat="server" DataIndex="INFO_SUBJECT" Text="Info Subject" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_info_text" runat="server" DataIndex="INFO_TEXT" Text="Info Text" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_info_numeric" runat="server" DataIndex="INFO_NUMERIC" Text="Info Numeric" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_info_date" runat="server" DataIndex="INFO_DATE" Text="Info Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_info_boolean" runat="server" DataIndex="INFO_BOOLEAN" Text="Info Boolean" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_INDV_AdditionalInformation" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_AdditionalInformation">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Additional_Information_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_additional_info" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Additional Information --%>
                    
                </Items>
            </ext:Panel>

            <ext:Panel runat="server" Title="Corporate Customer" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" Border="true" BodyStyle="padding:10px" ID="panel_Corp">
                <Items>
                    <ext:TextField runat="server" LabelWidth="250" AllowBlank="False" FieldLabel="Corporate Name" ID="txt_Corp_Name" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Commercial Name" ID="txt_Corp_Commercial_Name" AnchorHorizontal="90%" />
                    <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                    <ext:Panel ID="Pnl_cmb_Corp_Incorporation_Legal_Form" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_Corp_Incorporation_Legal_Form" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Bentuk_Badan_Usaha" StringFilter="Active = 1" EmptyText="Select One" Label="Legal Form" AnchorHorizontal="70%" />
                        </Content>
                    </ext:Panel>
                    <%--End Update--%>
                    <%--<ext:ComboBox ID="cmb_Corp_Incorporation_Legal_Form" runat="server" LabelWidth="250" FieldLabel="Legal Form" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="Store_Corp_Incorporation_Legal_Form">
                                <Model>
                                    <ext:Model runat="server" ID="Model_Corp_Incorporation_Legal_Form">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%--</DirectEvents>
                    </ext:ComboBox>--%>
                    <%--                     <ext:ComboBox ID="cmb_corp_role" runat="server" LabelWidth="250" allowBlank="False" FieldLabel="Peran" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="Store_Corp_Role">
                                <Model>
                                    <ext:Model runat="server" ID="Model_Corp_Role">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                            <Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>
                        </DirectEvents>
                   </ext:ComboBox>--%>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Corporate License Number" ID="txt_Corp_Incorporation_number" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" AllowBlank="False" FieldLabel="Line Of Business" ID="txt_Corp_Business" AnchorHorizontal="90%" />
                    <%--<ext:Panel ID="Panel1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_Corp_EntityStatus" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan"  StringTable="goAML_Ref_Entity_Status" Label="Entity Status" AnchorHorizontal="70%" AllowBlank="true" LabelWidth="250" />
                        </Content>
                    </ext:Panel>
                    <ext:DateField Editable="false" LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Entity Status Date" ID="dte_Corp_EntityStatusDate" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />--%>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Email Korporasi" ID="txt_Corp_Email" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Website Korporasi" ID="txt_Corp_url" AnchorHorizontal="90%" />
                    
                    <ext:GridPanel ID="GP_Phone_Corp" runat="server" Title="Phone" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar2" runat="server">
                                <Items>
                                    <ext:Button ID="Button3" runat="server" Text="Add Phones" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddCustomerPhones_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Phone_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_Phone_Corp" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column9" runat="server" DataIndex="Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="Communcation_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="tph_extension" Text="Extension" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <%--Corp Address--%>
                    <ext:GridPanel ID="GP_Address_Corp" runat="server" Title="Address" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar3" runat="server">
                                <Items>
                                    <ext:Button ID="Button4" runat="server" Text="Add Addresses" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddCustomerAddresses_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Address_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_Address_Corp" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column15" runat="server" DataIndex="Type_Address_Detail" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="Town" Text="Town" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="Zip" Text="Zip Code" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="Country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column21" runat="server" DataIndex="State" Text="Province" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn4" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end of Corp Address--%>
                
                    <%-- Corporate Email --%>
                    <ext:GridPanel ID="GPCustomer_CORP_Email" runat="server" Title="Email" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="tbr_corp_email" runat="server">
                                <Items>
                                    <ext:Button ID="btn_add_corp_email" runat="server" Text="Add Email" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_Email_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_corp_email" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_corp_email" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Email_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EMAIL_ADDRESS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rcn_cor_email" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Customer_Previous_Name_ID" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_corp_email_address" runat="server" DataIndex="EMAIL_ADDRESS" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_CORP_Email" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_Email">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Email_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_corp_email" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Corporate Email %>

                    <%-- Entity URL / Website --%>
                    <ext:GridPanel ID="GPCustomer_CORP_URL" runat="server" Title="Entity URL" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="tbr_entity_url" runat="server">
                                <Items>
                                    <ext:Button ID="btn_add_entity_url" runat="server" Text="Add Entity URL" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_Url_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Customer_Entity_URL" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_entity_url" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_URL_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="URL" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_entity_url" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_url_type" runat="server" DataIndex="URL" Text="URL" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_CORP_URL" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_URL">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_URL_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_entity_url" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End Of Entity URL --%>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Province" ID="txt_Corp_incorporation_state" AnchorHorizontal="90%" />
                    <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                    <ext:Panel ID="Pnl_cmb_Corp_Incorporation_Country_Code" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_Corp_Incorporation_Country_Code" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <%--End Update--%>
                    <%--<ext:ComboBox ID="cmb_Corp_Incorporation_Country_Code" runat="server" AllowBlank="false" LabelWidth="250" FieldLabel="Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" Editable="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="Store_Corp_Incorporation_Country_Code">
                                <Model>
                                    <ext:Model runat="server" ID="Model_Corp_Incorporation_Country_Code">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%-- </DirectEvents>
                    </ext:ComboBox>--%>
                    <%--<ext:ComboBox ID="cmb_Corp_Role" runat="server" LabelWidth="250" FieldLabel="Peran" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" Editable="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="Store_Corp_Role">
                                <Model>
                                    <ext:Model runat="server" ID="Model1">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%--</DirectEvents>
                   </ext:ComboBox>--%>
                    <ext:DateField Editable="false" runat="server" LabelWidth="250" FieldLabel="Date of Established" ID="txt_Corp_incorporation_date" Format="dd-MMM-yyyy" AnchorHorizontal="90%" />
                    <ext:Checkbox runat="server" ID="cbTutup" LabelWidth="250" FieldLabel="Is Closed?" Checked="true">
                        <%--   <DirectEvents>
                            <Change OnEvent="checkBoxIsEntityFrom_click" />
                        </DirectEvents>--%>
                    </ext:Checkbox>
                    <ext:DateField Editable="false" runat="server" LabelWidth="250" FieldLabel="Closing Date" Hidden="true" ID="txt_Corp_date_business_closed" Format="dd-MMM-yyyy" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Tax Number" ID="txt_Corp_tax_number" AnchorHorizontal="90%" />
                    <%--<ext:TextField runat="server" LabelWidth="250" FieldLabel="NPWP Registasi Number" ID="txt_Corp_Tax_Registeration_Number" />
                    --%><ext:TextField runat="server" LabelWidth="250" FieldLabel="Notes" ID="txt_Corp_Comments" AnchorHorizontal="90%" />

                    <%-- Entity Identification --%>
                    <ext:GridPanel ID="GPCustomer_CORP_EntityIdentification" runat="server" Title="Entity Identification" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="tbr_ent_identify" runat="server">
                                <Items>
                                    <ext:Button ID="btn_add_ent_identify" runat="server" Text="Add Entity Identification" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_EntityIdentification_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Customer_Entity_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_ent_identify" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Entity_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TYPE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUE_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EXPIRY_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUED_BY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUE_COUNTRY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_identify" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_identify_type" runat="server" DataIndex="TYPE" Text="Entity Identifier Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_number" runat="server" DataIndex="NUMBER" Text="Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_issued_date" runat="server" DataIndex="ISSUE_DATE" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_expiry_date" runat="server" DataIndex="EXPIRY_DATE" Text="Expiry Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_issued_by" runat="server" DataIndex="ISSUED_BY" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_issued_country" runat="server" DataIndex="ISSUE_COUNTRY" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_CORP_EntityIdentification" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_EntityIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_identify" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- end of Director --%>

                    <%--Corp Director--%>
                    <ext:GridPanel ID="GP_Corp_Director" runat="server" Title="Director Corp" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar7" runat="server">
                                <Items>
                                    <ext:Button ID="Button7" runat="server" Text="Add Director" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddCustomerDirector_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Director_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_Director_Corp" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Entity_Director_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Role" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column31" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column32" runat="server" DataIndex="Role" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn8" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end of Corp Director--%>
                
                    <%--Corp Network Device--%>
                    <ext:GridPanel ID="GPCustomer_CORP_NetworkDevice" runat="server" Title="Network Device" AutoScroll="true" Visible="false">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar14" runat="server">
                                <Items>
                                    <ext:Button ID="btn_director_add_network_device" runat="server" Text="Add Network Device" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_NetworkDevice_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreCorp_Network_Device" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_director_network_device" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Network_Device_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DEVICE_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="OPERATING_SYSTEM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SERVICE_PROVIDER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IPV6" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IPV4" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CGN_PORT" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IPV4_IPV6" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAT" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FIRST_SEEN_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LAST_SEEN_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="USING_PROXY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COUNTRY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rcn_director_network_device" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_director_device_number" runat="server" DataIndex="DEVICE_NUMBER" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_director_operating_system" runat="server" DataIndex="OPERATING_SYSTEM" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_director_service_provider" runat="server" DataIndex="SERVICE_PROVIDER" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_director_ip_v6" runat="server" DataIndex="IPV6" Text="IP V6" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_director_ip_v4" runat="server" DataIndex="IPV4" Text="IP V4" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_director_cgn_port" runat="server" DataIndex="CGN_PORT" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_director_ip_v6_with_ip_v4" runat="server" DataIndex="IPV4_IPV6" Text="IPV6 WITH IPV4" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_director_nat" runat="server" DataIndex="NAT" Text="NAT" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_director_first_seen_date" runat="server" DataIndex="FIRST_SEEN_DATE" Text="First Seen Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_director_last_seen_date" runat="server" DataIndex="LAST_SEEN_DATE" Text="Last Seen Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_director_using_proxy" runat="server" DataIndex="USING_PROXY" Text="Using Proxy" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_director_city" runat="server" DataIndex="CITY" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_director_country" runat="server" DataIndex="COUNTRY" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_director_comment" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_CORP_NetworkDevice" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_NetworkDevice">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Network_Device_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_network_device" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- end of Corp Network Device--%>

                    <%-- INDV Sanction --%>
                    <ext:GridPanel ID="GPCustomer_CORP_Sanction" runat="server" Title="Sanction" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="tbr_corp_sanction" runat="server">
                                <Items>
                                    <ext:Button ID="btn_add_corp_sanction" runat="server" Text="Add Sanction" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_Sanction_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_corp_sanction" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_corp_sanction" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Sanction_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PROVIDER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SANCTION_LIST_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="MATCH_CRITERIA" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LINK_TO_SOURCE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SANCTION_LIST_ATTRIBUTES" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_corp_sanction" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_corp_sanction" runat="server" DataIndex="PROVIDER" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_corp_sanction_list_name" runat="server" DataIndex="SANCTION_LIST_NAME" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_corp_match_criteria" runat="server" DataIndex="MATCH_CRITERIA" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_corp_link_to_source" runat="server" DataIndex="LINK_TO_SOURCE" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_corp_sanction_list_attributes" runat="server" DataIndex="SANCTION_LIST_ATTRIBUTES" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="clm_corp_valid_from" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_VALID_FROM" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_corp_is_approx_from_date" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_corp_is_valid_to" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_VALID_TO" Text="Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_corp_is_approx_to_date" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="clm_corp_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_CORP_Sanction" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_Sanction">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_corp_sanction" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- end of INDV Sanction --%>

                    <%-- Related Entities --%>
                    <ext:GridPanel ID="GPCustomer_CORP_RelatedEntities" runat="server" Title="Related Entities" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="tbr_corp_related_entities" runat="server">
                                <Items>
                                    <ext:Button ID="btn_corp_re_add_email" runat="server" Text="Add Related Entities" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_RelatedEntities_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Related_Entities" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_related_entities" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Related_Entities_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ENTITY_ENTITY_RELATION" Type="String"></ext:ModelField>
                                       <%--     <ext:ModelField Name="RELATION_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RELATION_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RELATION_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RELATION_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SHARE_PERCENTAGE" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMERCIAL_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INCORPORATION_LEGAL_FORM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INCORPORATION_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="BUSINESS" Type="String"></ext:ModelField>
                                      <%--      <ext:ModelField Name="ENTITY_STATUS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ENTITY_STATUS_DATE" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="INCORPORATION_STATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INCORPORATION_COUNTRY_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INCORPORATION_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DATE_BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_related_etities" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Customer_Previous_Name_ID" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_re_email_address" runat="server" DataIndex="ENTITY_ENTITY_RELATION" Text="Entity Relation" MinWidth="130" Flex="1"></ext:Column>
                             <%--   <ext:Column ID="Column84" runat="server" DataIndex="RELATION_DATE_RANGE_VALID_FROM" Text="Relation Date Range - Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column85" runat="server" DataIndex="RELATION_DATE_RANGE_IS_APPROX_FROM_DATE" Text="Relation Date Range - Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column86" runat="server" DataIndex="RELATION_DATE_RANGE_VALID_TO" Text="Relation Date Range - Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column87" runat="server" DataIndex="RELATION_DATE_RANGE_IS_APPROX_TO_DATE" Text="Relation Date Range - Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column88" runat="server" DataIndex="SHARE_PERCENTAGE" Text="Share Percentage" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column89" runat="server" Hidden="true" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column90" runat="server" DataIndex="NAME" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column91" runat="server" Hidden="true" DataIndex="COMMERCIAL_NAME" Text="Commercial Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column92" runat="server" Hidden="true" DataIndex="INCORPORATION_LEGAL_FORM" Text="Incorporation Legal Form" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column93" runat="server" Hidden="true" DataIndex="INCORPORATION_NUMBER" Text="Incorporation Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column94" runat="server" Hidden="true" DataIndex="BUSINESS" Text="Business" MinWidth="130" Flex="1"></ext:Column>
                              <%--  <ext:Column ID="Column95" runat="server" DataIndex="ENTITY_STATUS" Text="Entity Status" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column96" runat="server" DataIndex="ENTITY_STATUS_DATE" Text="Entity Status Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column97" runat="server" Hidden="true" DataIndex="INCORPORATION_STATE" Text="Incorporation State" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column98" runat="server" Hidden="true" DataIndex="INCORPORATION_COUNTRY_CODE" Text="Incorporation Country Code" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column99" runat="server" Hidden="true" DataIndex="INCORPORATION_DATE" Text="Incorporation Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column100" runat="server" Hidden="true" DataIndex="BUSINESS_CLOSED" Text="Businesss Closed" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column101" runat="server" Hidden="true" DataIndex="DATE_BUSINESS_CLOSED" Text="Date Business Closed" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column102" runat="server" Hidden="true" DataIndex="TAX_NUMBER" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column103" runat="server" Hidden="true" DataIndex="TAX_REG_NUMBER" Text="Tax Reg Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_CORP_RelatedEntities" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_RelatedEntities">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Entities_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_related_entities" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Related Entities --%>

                    <%-- Corp Additional Information --%>
                    <ext:GridPanel ID="GPCustomer_CORP_AdditionalInformation" runat="server" Title="Additional Information" AutoScroll="true" Visible="false">
                        <TopBar>
                            <ext:Toolbar ID="tbr_corp_additional_info" runat="server">
                                <Items>
                                    <ext:Button ID="btn_corp_additional_info" runat="server" Text="Add Additional Information" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Customer_AdditionalInfo_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Customer_Corp_Additional_Info" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_corp_additional_info" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Additional_Information_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INFO_TYPE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INFO_SUBJECT" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INFO_TEXT" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INFO_NUMERIC" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INFO_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INFO_BOOLEAN" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_corp_additional_info" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="col_corp_PK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_corp_info_type" runat="server" DataIndex="INFO_TYPE" Text="Info Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_corp_info_subject" runat="server" DataIndex="INFO_SUBJECT" Text="Info Subject" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_corp_info_text" runat="server" DataIndex="INFO_TEXT" Text="Info Text" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_corp_info_numeric" runat="server" DataIndex="INFO_NUMERIC" Text="Info Numeric" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_corp_info_date" runat="server" DataIndex="INFO_DATE" Text="Info Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_corp_info_boolean" runat="server" DataIndex="INFO_BOOLEAN" Text="Info Boolean" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CC_CORP_AdditionalInformation" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustomer_AdditionalInformation">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Additional_Information_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_corp_additional_info" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Additional Information --%>
                </Items>
            </ext:Panel>



            <%--   <ext:Panel runat="server" Title="Notes History" Collapsible="true" AnchorHorizontal="100%">
                <Items>
                    <ext:GridPanel ID="gp_history" runat="server">
                        <Store>
                            <ext:Store ID="gpStore_history" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" IDProperty="CIF">
                                        <Fields>
                                            <ext:ModelField Name="PK_TX_IFTI_Flow_History_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Module_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UnikID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="UserID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UserName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="RoleName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Notes" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>

                        <ColumnModel runat="server">
                            <Columns>
                                <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="70"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="User Name" DataIndex="UserName" CellWrap="true" flex="1" />
                                <ext:DateColumn runat="server" Text="Created Date" DataIndex="CreatedDate" CellWrap="true" flex="1" Format="dd-MMM-yyyy" />
                                <ext:Column runat="server" Text="Status" DataIndex="Status" CellWrap="true" Flex="1" />
                                <ext:Column runat="server" Text="Notes" DataIndex="Notes" CellWrap="true" flex="4" />
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>--%>
        </Items>

        <Buttons>
            <ext:Button ID="btnSubmit" runat="server" Icon="Disk" Text="Submit">
                <DirectEvents>
                    <Click OnEvent="BtnSubmit_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancelIncoming" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnBack_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>

        </Items>

        <Buttons>

            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>

