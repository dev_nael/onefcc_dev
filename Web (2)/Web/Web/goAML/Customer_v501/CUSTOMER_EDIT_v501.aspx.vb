﻿Imports NawaBLL
Imports Ext
Imports GoAMLBLL
Imports NawaDAL
Imports GoAMLDAL
Imports Elmah
Imports System.Data
Imports Checkbox = Ext.Net.Checkbox
Imports GoAMLBLL.goAML_CustomerDataBLL
Imports GoAMLBLL.goAML
Imports GoAMLBLL.goAML.Services
Imports NPOI.Util.Collections

Partial Class CUSTOMER_EDIT_v501
    Inherits ParentPage
    Public objFormModuleView As NawaBLL.FormModuleDetail
    Public objgoAML_CustomerBLL As New GoAMLBLL.goAML_CustomerBLL
    Private mModuleName As String
    Private TempPK As Int32

    Public Property ModulName() As String
        Get
            Return mModuleName
        End Get
        Set(ByVal value As String)
            mModuleName = value
        End Set
    End Property
    Sub ClearSession()
        tempflag = New Integer
        obj_Customer = New goAML_Ref_Customer()
        objListgoAML_vw_Customer_Phones = New List(Of Vw_Customer_Phones)
        objListgoAML_vw_Customer_Addresses = New List(Of Vw_Customer_Addresses)
        objListgoAML_Ref_Director_Phone = New List(Of goAML_Ref_Phone)
        objListgoAML_Ref_Director_Address = New List(Of goAML_Ref_Address)
        objListgoAML_Ref_Director_Employer_Phone = New List(Of goAML_Ref_Phone)
        objListgoAML_Ref_Director_Employer_Address = New List(Of goAML_Ref_Address)
        objListgoAML_Ref_Phone = New List(Of goAML_Ref_Phone)
        objListgoAML_Ref_Address = New List(Of goAML_Ref_Address)
        objListgoAML_vw_Employer_Phones = New List(Of Vw_Employer_Phones)
        objListgoAML_vw_Employer_Addresses = New List(Of Vw_Employer_Addresses)
        objListgoAML_Ref_Employer_Phone = New List(Of goAML_Ref_Phone)
        objListgoAML_Ref_Employer_Address = New List(Of goAML_Ref_Address)
        objListgoAML_vw_Director_Phones = New List(Of Vw_Director_Phones)
        objListgoAML_vw_Director_Addresses = New List(Of Vw_Director_Addresses)
        objListgoAML_vw_Director_Employer_Phones = New List(Of Vw_Director_Employer_Phones)
        objListgoAML_vw_Director_Employer_Addresses = New List(Of Vw_Director_Employer_Addresses)
        objListgoAML_Ref_Identification = New List(Of goAML_Person_Identification)
        objListgoAML_Ref_Identification_Director = New List(Of goAML_Person_Identification)
        objListgoAML_vw_Customer_Identification = New List(Of Vw_Person_Identification)
        objListgoAML_vw_Customer_Identification_Director = New List(Of Vw_Person_Identification)
        objListgoAML_vw_Customer_Entity_Director = New List(Of Vw_Customer_Director)
        objListgoAML_Ref_Director = New List(Of goAML_Ref_Customer_Entity_Director)
        objListgoAML_Ref_Director2 = New List(Of DataModel.goAML_Ref_Entity_Director)

        'add goAML 5.0.1, Septian, 25 Jan 2023
        obj_Customer2 = New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2()
        objListgoAML_Ref_Previous_Name = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        objListgoAML_vw_Previous_Name = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)

        objListgoAML_Ref_Email = New List(Of DataModel.goAML_Ref_Customer_Email)
        objListgoAML_vw_Email = New List(Of DataModel.goAML_Ref_Customer_Email)

        objListgoAML_Ref_EmploymentHistory = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        objListgoAML_vw_EmploymentHistory = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)

        objListgoAML_Ref_PersonPEP = New List(Of DataModel.goAML_Ref_Customer_PEP)
        objListgoAML_vw_PersonPEP = New List(Of DataModel.goAML_Ref_Customer_PEP)

        objListgoAML_Ref_NetworkDevice = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        objListgoAML_vw_NetworkDevice = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)

        objListgoAML_Ref_SocialMedia = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        objListgoAML_vw_SocialMedia = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)

        objListgoAML_Ref_Sanction = New List(Of DataModel.goAML_Ref_Customer_Sanction)
        objListgoAML_vw_Sanction = New List(Of DataModel.goAML_Ref_Customer_Sanction)

        objListgoAML_Ref_RelatedPerson = New List(Of DataModel.goAML_Ref_Customer_Related_Person)
        objListgoAML_vw_RelatedPerson = New List(Of DataModel.goAML_Ref_Customer_Related_Person)

        objListgoAML_Ref_AdditionalInfo = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        objListgoAML_vw_AdditionalInfo = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)

        objListgoAML_Ref_EntityUrl = New List(Of DataModel.goAML_Ref_Customer_URL)
        objListgoAML_vw_EntityUrl = New List(Of DataModel.goAML_Ref_Customer_URL)

        objListgoAML_Ref_EntityIdentification = New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        objListgoAML_vw_EntityIdentification = New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)

        objListgoAML_Ref_RelatedEntities = New List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        objListgoAML_vw_RelatedEntities = New List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        'End Add
    End Sub

    Public Property objListgoAML_Ref_Address() As List(Of GoAMLDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Address))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Address") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Employer_Address() As List(Of GoAMLDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Employer_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Address))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Employer_Address") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_Employer_Phone() As List(Of GoAMLDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Employer_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Phone))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Employer_Phone") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Employer_Addresses() As List(Of GoAMLDAL.Vw_Employer_Addresses)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Employer_Addresses")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Employer_Addresses))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Employer_Addresses") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Employer_Phones() As List(Of GoAMLDAL.Vw_Employer_Phones)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Employer_Phones")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Employer_Phones))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Employer_Phones") = value
        End Set
    End Property

    Public Property objTempEmployerAddressEdit() As GoAMLDAL.Vw_Employer_Addresses
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempEmployerAddressEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Employer_Addresses)
            Session("CUSTOMER_EDIT_v501.objTempEmployerAddressEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_EmployerPhones() As GoAMLDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_EmployerPhones")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Phone)
            Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_EmployerPhones") = value
        End Set
    End Property

    Public Property objTempEmployerPhonesEdit() As GoAMLDAL.Vw_Employer_Phones
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempEmployerPhonesEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Employer_Phones)
            Session("CUSTOMER_EDIT_v501.objTempEmployerPhonesEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_EmployerAddresses() As GoAMLDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_EmployerAddresses")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Address)
            Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_EmployerAddresses") = value
        End Set
    End Property


    Public Property objListgoAML_vw_Customer_Addresses() As List(Of GoAMLDAL.Vw_Customer_Addresses)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Customer_Addresses")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Customer_Addresses))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Customer_Addresses") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Identification() As List(Of GoAMLDAL.goAML_Person_Identification)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Identification")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Person_Identification))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Identification") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Identification() As List(Of GoAMLDAL.Vw_Person_Identification)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Customer_Identification")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Person_Identification))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Customer_Identification") = value
        End Set
    End Property

    Public Property objTempCustomerIdentificationEdit() As GoAMLDAL.Vw_Person_Identification
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomerIdentificationEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Person_Identification)
            Session("CUSTOMER_EDIT_v501.objTempCustomerIdentificationEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Identification() As GoAMLDAL.goAML_Person_Identification
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Identification")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Person_Identification)
            Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Identification") = value
        End Set
    End Property

    Public Property objTempCustomerAddressEdit() As GoAMLDAL.Vw_Customer_Addresses
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomerAddressEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Customer_Addresses)
            Session("CUSTOMER_EDIT_v501.objTempCustomerAddressEdit") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Phones() As List(Of GoAMLDAL.Vw_Customer_Phones)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Customer_Phones")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Customer_Phones))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Customer_Phones") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Phone() As List(Of GoAMLDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_EDIT_v501.goAML_Ref_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Phone))
            Session("CUSTOMER_EDIT_v501.goAML_Ref_Phone") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Phone() As GoAMLDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Phone")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Phone)
            Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Phone") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Address() As GoAMLDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Address")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Address)
            Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Address") = value
        End Set
    End Property

    Public Property objTempCustomerPhoneEdit() As GoAMLDAL.Vw_Customer_Phones
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomerPhoneEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Customer_Phones)
            Session("CUSTOMER_EDIT_v501.objTempCustomerPhoneEdit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director() As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Entity_Director() As List(Of GoAMLDAL.Vw_Customer_Director)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Customer_Entity_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Customer_Director))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Customer_Entity_Director") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director2() As List(Of DataModel.goAML_Ref_Entity_Director)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director2")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Entity_Director))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director2") = value
        End Set
    End Property

    Public Property objTempCustomerDirectorEdit() As GoAMLDAL.Vw_Customer_Director
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomerDirectorEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Customer_Director)
            Session("CUSTOMER_EDIT_v501.objTempCustomerDirectorEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director() As GoAMLDAL.goAML_Ref_Customer_Entity_Director
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Director")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Customer_Entity_Director)
            Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Director") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director2() As DataModel.goAML_Ref_Entity_Director
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Director2")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Entity_Director)
            Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Director2") = value
        End Set
    End Property

    Public Property obj_Customer As GoAMLDAL.goAML_Ref_Customer
        Get
            Return Session("CUSTOMER_EDIT_v501.obj_Customer")
        End Get
        Set(value As GoAMLDAL.goAML_Ref_Customer)
            Session("CUSTOMER_EDIT_v501.obj_Customer") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Employer_Address() As List(Of GoAMLDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_Employer_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Address))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_Employer_Address") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_Director_Email() As List(Of DataModel.goAML_Ref_Customer_Email)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_Email")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Email))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_Email") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_Director_Sanction() As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_Sanction")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Sanction))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_Sanction") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_Director_PEP() As List(Of DataModel.goAML_Ref_Customer_PEP)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_PEP")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_PEP))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_PEP") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Director_Employer_Addresses() As List(Of GoAMLDAL.Vw_Director_Employer_Addresses)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Director_Employer_Addresses")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Employer_Addresses))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Director_Employer_Addresses") = value
        End Set
    End Property

    Public Property objTempDirectorEmployerAddressEdit() As GoAMLDAL.Vw_Director_Employer_Addresses
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempDirectorEmployerAddressEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Director_Employer_Addresses)
            Session("CUSTOMER_EDIT_v501.objTempDirectorEmployerAddressEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director_EmployerAddresses() As GoAMLDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Director_EmployerAddresses")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Address)
            Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Director_EmployerAddresses") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Address() As List(Of GoAMLDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Address))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_Address") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Director_Addresses() As List(Of GoAMLDAL.Vw_Director_Addresses)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Director_Addresses")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Addresses))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Director_Addresses") = value
        End Set
    End Property

    Public Property objTempDirectorAddressEdit() As GoAMLDAL.Vw_Director_Addresses
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempDirectorAddressEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Director_Addresses)
            Session("CUSTOMER_EDIT_v501.objTempDirectorAddressEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director_Addresses() As GoAMLDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Director_Addresses")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Address)
            Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Director_Addresses") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Employer_Phone() As List(Of GoAMLDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_Employer_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Phone))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_Employer_Phone") = value
        End Set
    End Property
    Public Property tempflag() As Integer
        Get
            Return Session("CUSTOMER_EDIT_v501.tempflag")
        End Get
        Set(ByVal value As Integer)
            Session("CUSTOMER_EDIT_v501.tempflag") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Director_Employer_Phones() As List(Of GoAMLDAL.Vw_Director_Employer_Phones)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Director_Employer_Phones")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Employer_Phones))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Director_Employer_Phones") = value
        End Set
    End Property

    Public Property objTempDirectorEmployerPhoneEdit() As GoAMLDAL.Vw_Director_Employer_Phones
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempDirectorEmployerPhoneEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Director_Employer_Phones)
            Session("CUSTOMER_EDIT_v501.objTempDirectorEmployerPhoneEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director_EmployerPhones() As GoAMLDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Director_EmployerPhones")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Phone)
            Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Director_EmployerPhones") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Phone() As List(Of GoAMLDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Phone))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Director_Phone") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Director_Phones() As List(Of GoAMLDAL.Vw_Director_Phones)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Director_Phones")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Phones))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Director_Phones") = value
        End Set
    End Property

    Public Property objTempDirectorPhoneEdit() As GoAMLDAL.Vw_Director_Phones
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempDirectorPhoneEdit")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Director_Phones)
            Session("CUSTOMER_EDIT_v501.objTempDirectorPhoneEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director_Phones() As GoAMLDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Director_Phones")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Phone)
            Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Director_Phones") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Identification_Director() As List(Of GoAMLDAL.goAML_Person_Identification)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Identification_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Person_Identification))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Identification_Director") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Identification_Director() As List(Of GoAMLDAL.Vw_Person_Identification)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Customer_Identification_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Person_Identification))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Customer_Identification_Director") = value
        End Set
    End Property

    Public Property objTempCustomerIdentificationEdit_Director() As GoAMLDAL.Vw_Person_Identification
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomerIdentificationEdit_Director")
        End Get
        Set(ByVal value As GoAMLDAL.Vw_Person_Identification)
            Session("CUSTOMER_EDIT_v501.objTempCustomerIdentificationEdit_Director") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Identification_Director() As GoAMLDAL.goAML_Person_Identification
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Identification_Director")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Person_Identification)
            Session("CUSTOMER_EDIT_v501.objTempgoAML_Ref_Identification_Director") = value
        End Set
    End Property

    '' Add 04-Jan-2023, Septian. GoAML 5.0.1
    Public Property objTempCustomerPreviousNameEdit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomerPreviousName")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
            Session("CUSTOMER_EDIT_v501.objTempCustomerPreviousName") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Previous_Name() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Previous_Name")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Previous_name") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Previous_Name() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Previous_Name")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Previous_Name") = value
        End Set
    End Property

    Public Property objTempCustomer_Email_Edit() As DataModel.goAML_Ref_Customer_Email
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_Email_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Email)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_Email_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Email() As List(Of DataModel.goAML_Ref_Customer_Email)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Email")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Email))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Email") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Email() As List(Of DataModel.goAML_Ref_Customer_Email)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Email")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Email))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Email") = value
        End Set
    End Property

    Public Property objTempCustomer_EmploymentHistory_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_EmploymentHistory_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_EmploymentHistory_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_EmploymentHistory() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_EmploymentHistory")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_EmploymentHistory") = value
        End Set
    End Property

    Public Property objListgoAML_vw_EmploymentHistory() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_EmploymentHistory")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_EmploymentHistory") = value
        End Set
    End Property

    Public Property objTempCustomer_PersonPEP_Edit() As DataModel.goAML_Ref_Customer_PEP
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_PersonPEP_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_PEP)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_PersonPEP_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_PersonPEP() As List(Of DataModel.goAML_Ref_Customer_PEP)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_PersonPEP")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_PEP))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_PersonPEP") = value
        End Set
    End Property

    Public Property objListgoAML_vw_PersonPEP() As List(Of DataModel.goAML_Ref_Customer_PEP)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_PersonPEP")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_PEP))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_PersonPEP") = value
        End Set
    End Property
    Public Property objTempCustomer_NetworkDevice_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_NetworkDevice_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_NetworkDevice_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_NetworkDevice() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_NetworkDevice")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_NetworkDevice") = value
        End Set
    End Property

    Public Property objListgoAML_vw_NetworkDevice() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_NetworkDevice")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_NetworkDevice") = value
        End Set
    End Property

    Public Property objTempCustomer_SocialMedia_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTempCustomer_SocialMedia_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
            Session("CUSTOMER_EDIIT_v501.objTempCustomer_SocialMedia_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_SocialMedia() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_SocialMedia")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_SocialMedia") = value
        End Set
    End Property
    Public Property objListgoAML_vw_SocialMedia() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_SocialMedia")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_SocialMedia") = value
        End Set
    End Property

    Public Property objTempCustomer_Sanction_Edit() As DataModel.goAML_Ref_Customer_Sanction
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_Sanction_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Sanction)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_Sanction_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_Sanction() As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Sanction")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Sanction))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_Sanction") = value
        End Set
    End Property
    Public Property objListgoAML_vw_Sanction() As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Sanction")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Sanction))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_Sanction") = value
        End Set
    End Property

    Public Property objTempCustomer_RelatedPerson_Edit() As DataModel.goAML_Ref_Customer_Related_Person
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_RelatedPerson_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Person)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_RelatedPerson_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_RelatedPerson() As List(Of DataModel.goAML_Ref_Customer_Related_Person)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_RelatedPerson")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Person))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_RelatedPerson") = value
        End Set
    End Property
    Public Property objListgoAML_vw_RelatedPerson() As List(Of DataModel.goAML_Ref_Customer_Related_Person)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_RelatedPerson")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Person))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_RelatedPerson") = value
        End Set
    End Property

    Public Property objTempCustomer_AdditionalInfo_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_AdditionalInfo_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_AdditionalInfo_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_AdditionalInfo() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_AdditionalInfo")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_AdditionalInfo") = value
        End Set
    End Property
    Public Property objListgoAML_vw_AdditionalInfo() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_AdditionalInfo")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_AdditionalInfo") = value
        End Set
    End Property

    Public Property objTempCustomer_EntityUrl_Edit() As DataModel.goAML_Ref_Customer_URL
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_EntityUrl_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_URL)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_EntityUrl_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityUrl() As List(Of DataModel.goAML_Ref_Customer_URL)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_EntityUrl")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_URL))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_EntityUrl") = value
        End Set
    End Property
    Public Property objListgoAML_vw_EntityUrl() As List(Of DataModel.goAML_Ref_Customer_URL)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_EntityUrl")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_URL))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_EntityUrl") = value
        End Set
    End Property

    Public Property objTempCustomer_EntityIdentification_Edit() As DataModel.goAML_Ref_Customer_Entity_Identification
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_EntityIdentification_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Entity_Identification)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_EntityIdentification_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityIdentification() As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_EntityIdentification")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_EntityIdentification") = value
        End Set
    End Property
    Public Property objListgoAML_vw_EntityIdentification() As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_EntityIdentification")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_EntityIdentification") = value
        End Set
    End Property

    Public Property objTempCustomer_RelatedEntities_Edit() As DataModel.goAML_Ref_Customer_Related_Entities
        Get
            Return Session("CUSTOMER_EDIT_v501.objTempCustomer_RelatedEntities_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Entities)
            Session("CUSTOMER_EDIT_v501.objTempCustomer_RelatedEntities_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_RelatedEntities() As List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_RelatedEntities")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Entities))
            Session("CUSTOMER_EDIT_v501.objListgoAML_Ref_RelatedEntities") = value
        End Set
    End Property
    Public Property objListgoAML_vw_RelatedEntities() As List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        Get
            Return Session("CUSTOMER_EDIT_v501.objListgoAML_vw_RelatedEntities")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Entities))
            Session("CUSTOMER_EDIT_v501.objListgoAML_vw_RelatedEntities") = value
        End Set
    End Property

    Public Property obj_Customer2 As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2
        Get
            Return Session("CUSTOMER_EDIT_v501.obj_Customer2")
        End Get
        Set(value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2)
            Session("CUSTOMER_EDIT_v501.obj_Customer2") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Email() As DataModel.goAML_Ref_Customer_Email
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Email")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Email)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Email") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_PEP() As DataModel.goAML_Ref_Customer_PEP
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_PEP")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_PEP)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_PEP") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Sanction() As DataModel.goAML_Ref_Customer_Sanction
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Sanction")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Sanction)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Sanction") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Related_Person() As DataModel.goAML_Ref_Customer_Related_Person
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Related_Person")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Person)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Related_Person") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_URL() As DataModel.goAML_Ref_Customer_URL
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_URL")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_URL)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_URL") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Entity_Identification() As DataModel.goAML_Ref_Customer_Entity_Identification
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Entity_Identification")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Entity_Identification)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Entity_Identification") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_Customer_Related_Entities() As DataModel.goAML_Ref_Customer_Related_Entities
        Get
            Return Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Related_Entities")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Entities)
            Session("CUSTOMER_EDIIT_v501.objTemp_goAML_Ref_Customer_Related_Entities") = value
        End Set
    End Property
    '' End 04-Jan-2023

    Function IsDataCustomerValid() As Boolean
        'Return True
        'If Cmb_TypePerson.SelectedItemValue = 1 Then
        '    If txt_INDV_Occupation.Text = "" Then
        '        Throw New Exception("Occupation is required")
        '    End If
        '    If txt_INDV_Source_of_Wealth.Text = "" Then
        '        Throw New Exception("Source of Wealth is required")
        '    End If
        '    If txt_cif.Text = "" Then
        '        Throw New Exception("CIF is required")
        '    End If
        '    If txt_gcn.Text = "" Then
        '        Throw New Exception("GCN is required")
        '    End If
        '    If GCNPrimary.Checked = False Then
        '        If txt_gcn.Value IsNot Nothing Then
        '            Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_gcn.Value)
        '            If checkGCN Is Nothing Then
        '                'DirectCast(GCNPrimary, Checkbox).Checked = True
        '                Throw New Exception("Untuk GCN " + txt_gcn.Value + " belum memiliki GCNPrimary")
        '            End If
        '        End If
        '    End If
        '    If txt_INDV_Last_Name.Text = "" Then
        '        Throw New Exception("Full Name is required")
        '    End If
        '    If txt_INDV_Birthdate.RawValue = "" Then
        '        Throw New Exception("Birth Date is required")
        '    End If
        '    If txt_INDV_Birth_place.Text = "" Then
        '        Throw New Exception("Birth Place is required")
        '    End If
        '    'Update: Zikri_08102020
        '    If txt_INDV_SSN.Text.Trim IsNot "" Then
        '        If Not IsNumber(txt_INDV_SSN.Text) Then
        '            Throw New Exception("NIK is Invalid")
        '        End If
        '        If txt_INDV_SSN.Text.Trim.Length <> 16 Then
        '            Throw New Exception("NIK must contains 16 digit number")
        '        End If
        '    End If
        '    'End Update
        '    If cmb_INDV_Gender.SelectedItemValue = "" Then
        '        Throw New Exception("Gender is required")
        '    End If
        '    If cmb_INDV_Nationality1.SelectedItemValue = "" Then
        '        Throw New Exception("Nationality 1  is required")
        '    End If
        '    If cmb_INDV_Residence.SelectedItemValue = "" Then
        '        Throw New Exception("Residence is required")
        '    End If
        '    'Update: Zikri_11092020 Menambah Validasi format email
        '    If txt_INDV_Email.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email.Text) Then
        '            Throw New Exception("Invalid Email Address in Email")
        '        End If
        '    End If
        '    If txt_INDV_Email2.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email2.Text) Then
        '            Throw New Exception("Invalid Email Address in Email 2")
        '        End If
        '    End If
        '    If txt_INDV_Email3.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email3.Text) Then
        '            Throw New Exception("Invalid Email Address in Email 3")
        '        End If
        '    End If
        '    If txt_INDV_Email4.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email4.Text) Then
        '            Throw New Exception("Invalid Email Address in Email 4")
        '        End If
        '    End If
        '    If txt_INDV_Email5.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email5.Text) Then
        '            Throw New Exception("Invalid Email Address in Email 5")
        '        End If
        '    End If
        '    'End Update
        '    If Not String.IsNullOrEmpty(txt_INDV_ID_Number.Text) Then
        '        If objListgoAML_Ref_Identification.Count = 0 Then
        '            Throw New Exception("Identification is required")
        '        End If
        '    End If

        '    'If objListgoAML_Ref_Employer_Phone.Count = 0 Then
        '    '    Throw New Exception("Employer Phone is required")
        '    'End If
        '    'If objListgoAML_Ref_Employer_Address.Count = 0 Then
        '    '    Throw New Exception("Employer Address is required")
        '    'End If
        '    If cb_Deceased.Checked = True Then
        '            If txt_deceased_date.RawValue = "" Then
        '                Throw New Exception("Date Deceased is required")
        '            End If
        '        End If

        '    ElseIf Cmb_TypePerson.SelectedItemValue = 2 Then
        '        If GCNPrimary.Checked = False Then
        '        If txt_gcn.Value IsNot Nothing Then
        '            Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_gcn.Value)
        '            If checkGCN Is Nothing Then
        '                'DirectCast(GCNPrimary, Checkbox).Checked = True
        '                Throw New Exception("Untuk GCN " + txt_gcn.Value + " belum memiliki GCNPrimary")
        '            End If
        '        End If
        '    End If
        '    If txt_Corp_Business.Text = "" Then
        '        Throw New Exception("Corp Bussiness is required")
        '    End If
        '    'Update: Zikri_11092020 Menambah Validasi format email
        '    If txt_Corp_Email.Text IsNot "" Then
        '        If Not isEmailAddress(txt_Corp_Email.Text) Then
        '            Throw New Exception("Invalid Email Address in Email Korporasi")
        '        End If
        '    End If
        '    'End Update
        '    If txt_cif.Text = "" Then
        '        Throw New Exception("CIF is required")
        '    End If
        '    If txt_gcn.Text = "" Then
        '        Throw New Exception("GCN is required")
        '    End If
        '    If txt_Corp_Name.Text = "" Then
        '        Throw New Exception("Corp Name is required")
        '    End If
        '    If cmb_Corp_Incorporation_Country_Code.SelectedItemValue = "" Then
        '        Throw New Exception("Country is required")
        '    End If
        '    'If cmb_Corp_Role.RawValue = "" Then
        '    '    Throw New Exception("Role is required")
        '    'End If
        '    If objListgoAML_Ref_Director.Count = 0 Then
        '        Throw New Exception("Director is required")
        '    End If

        'End If
        'Return True
        If obj_Customer.FK_Customer_Type_ID.Value = 1 Then
            If Replace(txt_CIF.Text, " ", "") = "" Then
                Throw New Exception("CIF is required")
            End If
            'dedy added 25082020
            If Replace(txt_GCN.Text, " ", "") = "" Then
                Throw New Exception("GCN is required")
            End If
            'Update: Zikri_18092020 Validasi GCN
            If GCNPrimary.Checked = False Then
                If txt_GCN.Value IsNot Nothing Then
                    Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_GCN.Value)
                    If checkGCN Is Nothing Then
                        'DirectCast(GCNPrimary, Checkbox).Checked = True
                        Throw New Exception("Untuk GCN " + txt_GCN.Value + " belum memiliki GCNPrimary")
                        'ElseIf checkGCN.GCN = txt_GCN.Value Then
                        '    Throw New Exception("Untuk GCN " + txt_GCN.Value + " belum memiliki GCNPrimary")
                    End If
                End If
            End If
            'End Update
            'dedy end added 25082020
            If Replace(txt_INDV_Last_Name.Text, " ", "") = "" Then
                Throw New Exception("Full Name is required")
            End If
            'End Update
            If cmb_INDV_Gender.SelectedItemValue = "" Then
                Throw New Exception("Gender is required")
            End If
            If Replace(txt_INDV_Birthdate.RawValue, " ", "") = "" Then
                Throw New Exception("Birth Date is required")
            End If
            If Replace(txt_INDV_Birth_place.Text, " ", "") = "" Then
                Throw New Exception("Birth Place is required")
            End If
            'Update: Zikri_08102020
            'If txt_INDV_SSN.Text.Trim IsNot "" Then
            '    If Not IsNumber(txt_INDV_SSN.Text) Then
            '        Throw New Exception("NIK is Invalid")
            '    End If
            '    If txt_INDV_SSN.Text.Trim.Length <> 16 Then
            '        Throw New Exception("NIK must contains 16 digit number")
            '    End If
            'End If
            If cmb_INDV_Nationality1.SelectedItemValue = "" Then
                Throw New Exception("Nationality 1  is required")
            End If
            If cmb_INDV_Residence.SelectedItemValue = "" Then
                Throw New Exception("Domicile Country is required")
            End If
            If Replace(txt_INDV_Source_of_Wealth.Text, " ", "") = "" Then
                Throw New Exception("Source of Wealth is required")
            End If
            If Replace(txt_INDV_Occupation.Text, " ", "") = "" Then
                Throw New Exception("Occupation is required")
            End If
            'Update: Zikri_11092020 Menambah Validasi format email
            'comment by septian, goAML 5.0.1
            If Replace(txt_INDV_Email.Text, " ", "") IsNot "" And Replace(txt_INDV_Email.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email.Text) Then
                    Throw New Exception("Invalid Email Address in Email")
                End If
            End If
            If Replace(txt_INDV_Email2.Text, " ", "") IsNot "" And Replace(txt_INDV_Email2.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email2.Text) Then
                    Throw New Exception("Invalid Email Address in Email 2")
                End If
            End If
            If Replace(txt_INDV_Email3.Text, " ", "") IsNot "" And Replace(txt_INDV_Email3.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email3.Text) Then
                    Throw New Exception("Invalid Email Address in Email 3")
                End If
            End If
            If Replace(txt_INDV_Email4.Text, " ", "") IsNot "" And Replace(txt_INDV_Email4.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email4.Text) Then
                    Throw New Exception("Invalid Email Address in Email 4")
                End If
            End If
            If Replace(txt_INDV_Email5.Text, " ", "") IsNot "" And Replace(txt_INDV_Email5.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email5.Text) Then
                    Throw New Exception("Invalid Email Address in Email 5")
                End If
            End If
            'end comment
            ''End Update
            'If cb_Deceased.Checked = True Then
            '    If txt_deceased_date.RawValue = "" Then
            '        Throw New Exception("Date Deceased is required")
            '    End If
            'End If
            'If Not String.IsNullOrEmpty(txt_INDV_ID_Number.Text) Then
            '    If objListgoAML_Ref_Identification.Count = 0 Then
            '        Throw New Exception("Identification is required")
            '    End If
            'End If
            'If objListgoAML_Ref_Employer_Phone.Count = 0 Then
            '    Throw New Exception("Employer Phone is required")
            'End If
            'If objListgoAML_Ref_Employer_Address.Count = 0 Then
            '    Throw New Exception("Employer Address is required")
            'End If

            'add by Septian, goAML 5.0.1, 2022-02-16
            'If cmb_INDV_Country_Of_Birth.SelectedItemValue Is Nothing Then
            '    Throw New Exception("Country Of Birth is required")
            'End If
            'If txt_INDV_Foreign_Name.Text.Trim = "" Then
            '    Throw New Exception("Fullname foreign is required")
            'End If
            'If dte_INDV_ResidenceSince.Text.Trim = "" Then
            '    Throw New Exception("Residence since is required")
            'End If
            'end add
        ElseIf obj_Customer.FK_Customer_Type_ID.Value = 2 Then
            If Replace(txt_CIF.Text, " ", "") = "" Then
                Throw New Exception("CIF is required")
            End If
            'dedy added 25082020
            If Replace(txt_GCN.Text, " ", "") = "" Then
                Throw New Exception("GCN is required")
            End If
            'Update: Zikri_18092020 Validasi GCN
            If GCNPrimary.Checked = False Then
                If txt_GCN.Value IsNot Nothing Then
                    Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_GCN.Value)
                    If checkGCN Is Nothing Then
                        'DirectCast(GCNPrimary, Checkbox).Checked = True
                        Throw New Exception("Untuk GCN " + txt_GCN.Value + " belum memiliki GCNPrimary")
                        'ElseIf checkGCN.GCN = txt_GCN.Value Then
                        '    Throw New Exception("Untuk GCN " + txt_GCN.Value + " belum memiliki GCNPrimary")
                    End If
                End If
            End If
            'dedy end added 25082020
            If Replace(txt_Corp_Name.Text, " ", "") = "" Then
                Throw New Exception("Corp Name is required")
            End If
            'End Update
            If Replace(txt_Corp_Business.Text, " ", "") = "" Then
                Throw New Exception("Corp Bussiness is required")
            End If
            'Update: Zikri_11092020 Menambah Validasi format email
            'comment by septian, goAML 5.0.1
            If Replace(txt_Corp_Email.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_Corp_Email.Text) Then
                    Throw New Exception("Invalid Email Address in Email Korporasi")
                End If
            End If
            'end comment
            'End Update
            If cmb_Corp_Incorporation_Country_Code.SelectedItemValue = "" Then
                Throw New Exception("Country is required")
            End If
            'If cmb_corp_role.RawValue = "" Then
            '    Throw New Exception("Role is required")
            'End If
            'If objListgoAML_Ref_Director.Count = 0 Then
            '    Throw New Exception("Director is required")
            'End If

            'add by Septian, goAML 5.0.1, 2022-02-16
            'If cmb_Corp_EntityStatus.SelectedItemValue Is Nothing Then
            '    Throw New Exception("Entity Status is required")
            'End If
            'If dte_Corp_EntityStatusDate.Text.Trim = "" Then
            '    Throw New Exception("Entity Status Date is required")
            'End If
            'end add
        End If

        If objListgoAML_Ref_Phone.Count = 0 Then
            Throw New Exception("Phone is required")
        End If
        If objListgoAML_Ref_Address.Count = 0 Then
            Throw New Exception("Address is required")
        End If
        Return True
    End Function
    'Update: Zikri_11092020 Menambah Validasi format email
    Private Function isEmailAddress(text As String) As Boolean
        Try

            Dim email As New Regex("([\w-+]+(?:\.[\w-+]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7})")
            Return email.IsMatch(text)

        Catch ex As Exception
            Return False
        End Try
    End Function
    'End Update

    Function IfHaveChanges() As Boolean
        Dim IsSame As Boolean = True
        Dim oldobj_Customer = goAML_CustomerBLL.GetCustomerbyCIF(obj_Customer.CIF)
        If approvalchecker() = False Then
            Throw New ApplicationException("There is Already Pending Approval Please Confirm Approval First Before Do Another Change For This Data")
        End If
        'Return True
        If obj_Customer.FK_Customer_Type_ID.Value = 1 Then
            If oldobj_Customer.isUpdateFromDataSource Is Nothing Then
                If UpdateFromDataSource.Checked = True Then
                    oldobj_Customer.isUpdateFromDataSource = False
                Else
                    oldobj_Customer.isUpdateFromDataSource = obj_Customer.isUpdateFromDataSource
                End If
            End If
            If obj_Customer.isUpdateFromDataSource <> oldobj_Customer.isUpdateFromDataSource Then
                IsSame = False
            End If
            If obj_Customer.CIF <> oldobj_Customer.CIF Then
                IsSame = False
            End If
            If oldobj_Customer.Opening_Date IsNot Nothing Then
                Dim openingdate As DateTime = oldobj_Customer.Opening_Date
                If obj_Customer.Opening_Date.ToString <> openingdate Then
                    IsSame = False
                End If
            End If

            If oldobj_Customer.Closing_Date IsNot Nothing Then
                Dim closingdate As DateTime = oldobj_Customer.Closing_Date
                If obj_Customer.Closing_Date.ToString <> closingdate Then
                    IsSame = False
                End If
            End If

            If oldobj_Customer.Comments <> obj_Customer.Comments Then
                IsSame = False
            End If

            If oldobj_Customer.Opening_Branch_Code IsNot Nothing Then
                Dim openingbranch As String = oldobj_Customer.Opening_Branch_Code
                If obj_Customer.Opening_Branch_Code.ToString <> openingbranch Then
                    IsSame = False
                End If
            End If

            If oldobj_Customer.Status IsNot Nothing Then
                Dim statuss As String = oldobj_Customer.Status
                If obj_Customer.Status.ToString <> statuss Then
                    IsSame = False
                End If
            End If



            'dedy added 25082020
            If oldobj_Customer.isGCNPrimary Is Nothing Then
                If UpdateFromDataSource.Checked = True Then
                    oldobj_Customer.isGCNPrimary = False
                Else
                    oldobj_Customer.isGCNPrimary = obj_Customer.isGCNPrimary
                End If
            End If

            If obj_Customer.isGCNPrimary <> oldobj_Customer.isGCNPrimary Then
                IsSame = False
            End If
            If obj_Customer.GCN <> oldobj_Customer.GCN Then
                IsSame = False
            End If
            'dedy end added 25082020
            If obj_Customer.INDV_Prefix <> oldobj_Customer.INDV_Prefix Then
                IsSame = False
            End If
            If obj_Customer.INDV_Title <> oldobj_Customer.INDV_Title Then
                IsSame = False
            End If
            If obj_Customer.INDV_BirthDate <> oldobj_Customer.INDV_BirthDate Then
                IsSame = False
            End If
            If obj_Customer.INDV_Last_Name <> oldobj_Customer.INDV_Last_Name Then
                IsSame = False
            End If
            If obj_Customer.INDV_Birth_Place <> oldobj_Customer.INDV_Birth_Place Then
                IsSame = False
            End If
            If obj_Customer.INDV_Mothers_Name <> oldobj_Customer.INDV_Mothers_Name Then
                IsSame = False
            End If
            If obj_Customer.INDV_Alias <> oldobj_Customer.INDV_Alias Then
                IsSame = False
            End If
            If obj_Customer.INDV_SSN <> oldobj_Customer.INDV_SSN Then
                IsSame = False
            End If
            If obj_Customer.INDV_Passport_Number <> oldobj_Customer.INDV_Passport_Number Then
                IsSame = False
            End If
            If obj_Customer.INDV_Passport_Country <> oldobj_Customer.INDV_Passport_Country Then
                IsSame = False
            End If
            If obj_Customer.INDV_ID_Number <> oldobj_Customer.INDV_ID_Number Then
                IsSame = False
            End If
            If obj_Customer.INDV_Nationality1 <> oldobj_Customer.INDV_Nationality1 Then
                IsSame = False
            End If
            If obj_Customer.INDV_Nationality2 <> oldobj_Customer.INDV_Nationality2 Then
                IsSame = False
            End If
            If obj_Customer.INDV_Nationality3 <> oldobj_Customer.INDV_Nationality3 Then
                IsSame = False
            End If
            If obj_Customer.INDV_Residence <> oldobj_Customer.INDV_Residence Then
                IsSame = False
            End If
            If obj_Customer.INDV_Email <> oldobj_Customer.INDV_Email Then
                IsSame = False
            End If
            If obj_Customer.INDV_Email2 <> oldobj_Customer.INDV_Email2 Then
                IsSame = False
            End If
            If obj_Customer.INDV_Email3 <> oldobj_Customer.INDV_Email3 Then
                IsSame = False
            End If
            If obj_Customer.INDV_Email4 <> oldobj_Customer.INDV_Email4 Then
                IsSame = False
            End If
            If obj_Customer.INDV_Email5 <> oldobj_Customer.INDV_Email5 Then
                IsSame = False
            End If
            If obj_Customer.INDV_Occupation <> oldobj_Customer.INDV_Occupation Then
                IsSame = False
            End If
            If obj_Customer.INDV_Employer_Name <> oldobj_Customer.INDV_Employer_Name Then
                IsSame = False
            End If
            If obj_Customer.INDV_Gender <> oldobj_Customer.INDV_Gender Then
                IsSame = False
            End If
            If obj_Customer.INDV_Deceased <> oldobj_Customer.INDV_Deceased Then
                IsSame = False
            End If
            If obj_Customer.INDV_Deceased_Date <> oldobj_Customer.INDV_Deceased_Date Then
                IsSame = False
            End If
            If obj_Customer.INDV_Tax_Number <> oldobj_Customer.INDV_Tax_Number Then
                IsSame = False
            End If
            If obj_Customer.INDV_Tax_Reg_Number <> oldobj_Customer.INDV_Tax_Reg_Number Then
                IsSame = False
            End If
            If obj_Customer.INDV_Source_of_Wealth <> oldobj_Customer.INDV_Source_of_Wealth Then
                IsSame = False
            End If
            If obj_Customer.INDV_Comments <> oldobj_Customer.INDV_Comments Then
                IsSame = False
            End If
            Dim oldobjListgoAML_Ref_Phone = GoAMLBLL.goAML_CustomerBLL.GetCustomerPhones(obj_Customer.PK_Customer_ID)
            If oldobjListgoAML_Ref_Phone.Count <> objListgoAML_Ref_Phone.Count Then
                IsSame = False
            End If
            For Each item As goAML_Ref_Phone In objListgoAML_Ref_Phone
                Dim objPhone = oldobjListgoAML_Ref_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                If objPhone IsNot Nothing Then
                    If item.FK_Ref_Detail_Of <> objPhone.FK_Ref_Detail_Of Then
                        IsSame = False
                    End If
                    If item.FK_for_Table_ID <> objPhone.FK_for_Table_ID Then
                        IsSame = False
                    End If
                    If item.Tph_Contact_Type <> objPhone.Tph_Contact_Type Then
                        IsSame = False
                    End If
                    If item.Tph_Communication_Type <> objPhone.Tph_Communication_Type Then
                        IsSame = False
                    End If
                    If item.tph_country_prefix <> objPhone.tph_country_prefix Then
                        IsSame = False
                    End If
                    If item.tph_extension <> objPhone.tph_extension Then
                        IsSame = False
                    End If
                    If item.tph_number <> objPhone.tph_number Then
                        IsSame = False
                    End If
                    If item.comments <> objPhone.comments Then
                        IsSame = False
                    End If
                Else
                    IsSame = False
                End If
            Next

            Dim oldobjListgoAML_Ref_Employer_Phone = GoAMLBLL.goAML_CustomerBLL.GetEmployerPhones(obj_Customer.PK_Customer_ID)
            If oldobjListgoAML_Ref_Employer_Phone.Count <> objListgoAML_Ref_Employer_Phone.Count Then
                IsSame = False
            End If
            For Each item As goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                Dim objPhoneEmpINDV = oldobjListgoAML_Ref_Employer_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                If objPhoneEmpINDV IsNot Nothing Then
                    If item.FK_Ref_Detail_Of <> objPhoneEmpINDV.FK_Ref_Detail_Of Then
                        IsSame = False
                    End If
                    If item.FK_for_Table_ID <> objPhoneEmpINDV.FK_for_Table_ID Then
                        IsSame = False
                    End If
                    If item.Tph_Contact_Type <> objPhoneEmpINDV.Tph_Contact_Type Then
                        IsSame = False
                    End If
                    If item.Tph_Communication_Type <> objPhoneEmpINDV.Tph_Communication_Type Then
                        IsSame = False
                    End If
                    If item.tph_country_prefix <> objPhoneEmpINDV.tph_country_prefix Then
                        IsSame = False
                    End If
                    If item.tph_extension <> objPhoneEmpINDV.tph_extension Then
                        IsSame = False
                    End If
                    If item.tph_number <> objPhoneEmpINDV.tph_number Then
                        IsSame = False
                    End If
                    If item.comments <> objPhoneEmpINDV.comments Then
                        IsSame = False
                    End If
                Else
                    IsSame = False
                End If
            Next
            Dim oldobjListgoAML_Ref_Address = GoAMLBLL.goAML_CustomerBLL.GetCustomerAddresses(obj_Customer.PK_Customer_ID)
            If oldobjListgoAML_Ref_Address.Count <> objListgoAML_Ref_Address.Count Then
                IsSame = False
            End If
            For Each item As goAML_Ref_Address In objListgoAML_Ref_Address
                Dim objAddress = oldobjListgoAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                If objAddress IsNot Nothing Then
                    If item.FK_Ref_Detail_Of <> objAddress.FK_Ref_Detail_Of Then
                        IsSame = False
                    End If
                    If item.FK_To_Table_ID <> objAddress.FK_To_Table_ID Then
                        IsSame = False
                    End If
                    If item.Address_Type <> objAddress.Address_Type Then
                        IsSame = False
                    End If
                    If item.Town <> objAddress.Town Then
                        IsSame = False
                    End If
                    If item.Address <> objAddress.Address Then
                        IsSame = False
                    End If
                    If item.City <> objAddress.City Then
                        IsSame = False
                    End If
                    If item.Country_Code <> objAddress.Country_Code Then
                        IsSame = False
                    End If
                    If item.Zip <> objAddress.Zip Then
                        IsSame = False
                    End If
                    If item.State <> objAddress.State Then
                        IsSame = False
                    End If
                    If item.Comments <> objAddress.Comments Then
                        IsSame = False
                    End If
                Else
                    IsSame = False
                End If
            Next
            Dim oldobjListgoAML_Ref_Identification = GoAMLBLL.goAML_CustomerBLL.GetCustomerIdentifications(obj_Customer.PK_Customer_ID)
            If oldobjListgoAML_Ref_Identification.Count <> objListgoAML_Ref_Identification.Count Then
                IsSame = False
            End If
            For Each item As goAML_Person_Identification In objListgoAML_Ref_Identification
                Dim objIdentificationINDV = oldobjListgoAML_Ref_Identification.Where(Function(x) x.PK_Person_Identification_ID = item.PK_Person_Identification_ID).FirstOrDefault
                If objIdentificationINDV IsNot Nothing Then
                    If item.isReportingPerson <> objIdentificationINDV.isReportingPerson Then
                        IsSame = False
                    End If
                    If item.FK_Person_ID <> objIdentificationINDV.FK_Person_ID Then
                        IsSame = False
                    End If
                    If item.Type <> objIdentificationINDV.Type Then
                        IsSame = False
                    End If
                    If item.Number <> objIdentificationINDV.Number Then
                        IsSame = False
                    End If
                    If item.Issue_Date.ToString <> objIdentificationINDV.Issue_Date.ToString Then
                        IsSame = False
                    End If
                    If item.Issued_By <> objIdentificationINDV.Issued_By Then
                        IsSame = False
                    End If
                    If item.Expiry_Date.ToString <> objIdentificationINDV.Expiry_Date.ToString Then
                        IsSame = False
                    End If
                    If item.Issued_Country <> objIdentificationINDV.Issued_Country Then
                        IsSame = False
                    End If
                    If item.FK_Person_Type <> objIdentificationINDV.FK_Person_Type Then
                        IsSame = False
                    End If
                    If item.Identification_Comment <> objIdentificationINDV.Identification_Comment Then
                        IsSame = False
                    End If
                Else
                    IsSame = False
                End If
            Next
            Dim oldobjListgoAML_Ref_Employer_Address = GoAMLBLL.goAML_CustomerBLL.GetEmployerAddresses(obj_Customer.PK_Customer_ID)
            If oldobjListgoAML_Ref_Employer_Address.Count <> objListgoAML_Ref_Employer_Address.Count Then
                IsSame = False
            End If
            For Each item As goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                Dim objAddressEnpINDV = oldobjListgoAML_Ref_Employer_Address.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                If objAddressEnpINDV IsNot Nothing Then
                    If item.FK_Ref_Detail_Of <> objAddressEnpINDV.FK_Ref_Detail_Of Then
                        IsSame = False
                    End If
                    If item.FK_To_Table_ID <> objAddressEnpINDV.FK_To_Table_ID Then
                        IsSame = False
                    End If
                    If item.Address_Type <> objAddressEnpINDV.Address_Type Then
                        IsSame = False
                    End If
                    If item.Town <> objAddressEnpINDV.Town Then
                        IsSame = False
                    End If
                    If item.Address <> objAddressEnpINDV.Address Then
                        IsSame = False
                    End If
                    If item.City <> objAddressEnpINDV.City Then
                        IsSame = False
                    End If
                    If item.Country_Code <> objAddressEnpINDV.Country_Code Then
                        IsSame = False
                    End If
                    If item.Zip <> objAddressEnpINDV.Zip Then
                        IsSame = False
                    End If
                    If item.State <> objAddressEnpINDV.State Then
                        IsSame = False
                    End If
                    If item.Comments <> objAddressEnpINDV.Comments Then
                        IsSame = False
                    End If
                End If
            Next

            'add by Septian, goAML 5.0.1, 2023-02-15
            If IsEmailChange() Then
                Return True
            End If
            If IsPersonPEPChange() Then
                Return True
            End If
            If IsSanctionChange() Then
                Return True
            End If
            If IsRelatedPersonChange() Then
                Return True
            End If
            'end add



        ElseIf obj_Customer.FK_Customer_Type_ID.Value = 2 Then
            If oldobj_Customer.isUpdateFromDataSource Is Nothing Then
                If UpdateFromDataSource.Checked = True Then
                    oldobj_Customer.isUpdateFromDataSource = False
                Else
                    oldobj_Customer.isUpdateFromDataSource = obj_Customer.isUpdateFromDataSource
                End If
            End If
            If obj_Customer.isUpdateFromDataSource <> oldobj_Customer.isUpdateFromDataSource Then
                IsSame = False
            End If
            If obj_Customer.CIF <> oldobj_Customer.CIF Then
                IsSame = False
            End If
            'dedy added 25082020
            If oldobj_Customer.isGCNPrimary Is Nothing Then
                If UpdateFromDataSource.Checked = True Then
                    oldobj_Customer.isGCNPrimary = False
                Else
                    oldobj_Customer.isGCNPrimary = obj_Customer.isGCNPrimary
                End If
            End If
            If obj_Customer.isGCNPrimary <> oldobj_Customer.isGCNPrimary Then
                IsSame = False
            End If


            If oldobj_Customer.Opening_Date IsNot Nothing Then
                Dim openingdate As DateTime = oldobj_Customer.Opening_Date
                If obj_Customer.Opening_Date.ToString <> openingdate Then
                    IsSame = False
                End If
            End If

            If oldobj_Customer.Comments <> obj_Customer.Comments Then
                IsSame = False
            End If

            'If oldobj_Customer.Closing_Date IsNot Nothing Then
            '    Dim closingdate As DateTime = oldobj_Customer.Closing_Date
            '    If obj_Customer.Closing_Date.ToString <> closingdate Then
            '        IsSame = False
            '    End If
            'End If

            'If oldobj_Customer.Opening_Branch_Code IsNot Nothing Then
            '    Dim openingbranch As String = oldobj_Customer.Opening_Branch_Code
            '    If obj_Customer.Opening_Branch_Code.ToString <> openingbranch Then
            '        IsSame = False
            '    End If
            'End If

            'If oldobj_Customer.Status IsNot Nothing Then
            '    Dim statuss As String = oldobj_Customer.Status
            '    If obj_Customer.Status.ToString <> statuss Then
            '        IsSame = False
            '    End If
            'End If


            'If obj_Customer.Opening_Date IsNot Nothing And oldobj_Customer.Opening_Date IsNot Nothing Then
            '    If obj_Customer.Opening_Date.ToString <> oldobj_Customer.Opening_Date.ToString Then
            '        IsSame = False
            '    End If
            'ElseIf obj_Customer.Opening_Date Is Nothing And oldobj_Customer.Opening_Date Is Nothing Then
            'Else
            '    IsSame = False
            'End If

            'If oldobj_Customer.Closing_Date IsNot Nothing And obj_Customer.Closing_Date IsNot Nothing Then
            '    If obj_Customer.Closing_Date.ToString <> oldobj_Customer.Closing_Date.ToString Then
            '        IsSame = False
            '    End If
            'ElseIf oldobj_Customer.Closing_Date Is Nothing And obj_Customer.Closing_Date Is Nothing Then
            'Else
            '    IsSame = False
            'End If

            'If obj_Customer.Opening_Branch_Code.ToString <> oldobj_Customer.Opening_Branch_Code.ToString Then
            '    IsSame = False
            'End If
            'If obj_Customer.Status.ToString <> oldobj_Customer.Status.ToString Then
            '    IsSame = False
            'End If
            If obj_Customer.GCN <> oldobj_Customer.GCN Then
                IsSame = False
            End If
            'dedy end added 25082020
            If obj_Customer.Corp_Name <> oldobj_Customer.Corp_Name Then
                IsSame = False
            End If
            If obj_Customer.Corp_Commercial_Name <> oldobj_Customer.Corp_Commercial_Name Then
                IsSame = False
            End If
            If obj_Customer.Corp_Incorporation_Legal_Form <> oldobj_Customer.Corp_Incorporation_Legal_Form Then
                IsSame = False
            End If
            If obj_Customer.Corp_Incorporation_Number <> oldobj_Customer.Corp_Incorporation_Number Then
                IsSame = False
            End If
            If obj_Customer.Corp_Email <> oldobj_Customer.Corp_Email Then
                IsSame = False
            End If
            If obj_Customer.Corp_Business <> oldobj_Customer.Corp_Business Then
                IsSame = False
            End If
            If obj_Customer.Corp_Url <> oldobj_Customer.Corp_Url Then
                IsSame = False
            End If
            If obj_Customer.Corp_Incorporation_State <> oldobj_Customer.Corp_Incorporation_State Then
                IsSame = False
            End If
            If obj_Customer.Corp_Incorporation_Country_Code <> oldobj_Customer.Corp_Incorporation_Country_Code Then
                IsSame = False
            End If
            If obj_Customer.Corp_Director_ID <> oldobj_Customer.Corp_Director_ID Then
                IsSame = False
            End If
            If obj_Customer.Corp_Role <> oldobj_Customer.Corp_Role Then
                IsSame = False
            End If
            If obj_Customer.Corp_Incorporation_Date <> oldobj_Customer.Corp_Incorporation_Date Then
                IsSame = False
            End If
            If obj_Customer.Corp_Business_Closed <> oldobj_Customer.Corp_Business_Closed Then
                IsSame = False
            End If
            If obj_Customer.Corp_Date_Business_Closed <> oldobj_Customer.Corp_Date_Business_Closed Then
                IsSame = False
            End If
            If obj_Customer.Corp_Tax_Number <> oldobj_Customer.Corp_Tax_Number Then
                IsSame = False
            End If
            If obj_Customer.Corp_Comments <> oldobj_Customer.Corp_Comments Then
                IsSame = False
            End If
            If obj_Customer.Corp_Tax_Registeration_Number <> oldobj_Customer.Corp_Tax_Registeration_Number Then
                IsSame = False
            End If
            Dim oldobjListgoAML_Ref_PhoneCorp = GoAMLBLL.goAML_CustomerBLL.GetCustomerPhones(obj_Customer.PK_Customer_ID)
            If oldobjListgoAML_Ref_PhoneCorp.Count <> objListgoAML_Ref_Phone.Count Then
                IsSame = False
            End If
            For Each item As goAML_Ref_Phone In objListgoAML_Ref_Phone
                Dim objPhoneCORP = oldobjListgoAML_Ref_PhoneCorp.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                If objPhoneCORP IsNot Nothing Then
                    If item.FK_Ref_Detail_Of <> objPhoneCORP.FK_Ref_Detail_Of Then
                        IsSame = False
                    End If
                    If item.FK_for_Table_ID <> objPhoneCORP.FK_for_Table_ID Then
                        IsSame = False
                    End If
                    If item.Tph_Contact_Type <> objPhoneCORP.Tph_Contact_Type Then
                        IsSame = False
                    End If
                    If item.Tph_Communication_Type <> objPhoneCORP.Tph_Communication_Type Then
                        IsSame = False
                    End If
                    If item.tph_country_prefix <> objPhoneCORP.tph_country_prefix Then
                        IsSame = False
                    End If
                    If item.tph_extension <> objPhoneCORP.tph_extension Then
                        IsSame = False
                    End If
                    If item.tph_number <> objPhoneCORP.tph_number Then
                        IsSame = False
                    End If
                    If item.comments <> objPhoneCORP.comments Then
                        IsSame = False
                    End If
                Else
                    IsSame = False
                End If
            Next
            Dim oldobjListgoAML_Ref_AddressCorp = GoAMLBLL.goAML_CustomerBLL.GetCustomerAddresses(obj_Customer.PK_Customer_ID)
            If oldobjListgoAML_Ref_AddressCorp.Count <> objListgoAML_Ref_Address.Count Then
                IsSame = False
            End If
            For Each item As goAML_Ref_Address In objListgoAML_Ref_Address
                Dim objAddressCORP = oldobjListgoAML_Ref_AddressCorp.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                If objAddressCORP IsNot Nothing Then
                    If item.FK_Ref_Detail_Of <> objAddressCORP.FK_Ref_Detail_Of Then
                        IsSame = False
                    End If
                    If item.FK_To_Table_ID <> objAddressCORP.FK_To_Table_ID Then
                        IsSame = False
                    End If
                    If item.Address_Type <> objAddressCORP.Address_Type Then
                        IsSame = False
                    End If
                    If item.Town <> objAddressCORP.Town Then
                        IsSame = False
                    End If
                    If item.Address <> objAddressCORP.Address Then
                        IsSame = False
                    End If
                    If item.City <> objAddressCORP.City Then
                        IsSame = False
                    End If
                    If item.Country_Code <> objAddressCORP.Country_Code Then
                        IsSame = False
                    End If
                    If item.Zip <> objAddressCORP.Zip Then
                        IsSame = False
                    End If
                    If item.State <> objAddressCORP.State Then
                        IsSame = False
                    End If
                    If item.Comments <> objAddressCORP.Comments Then
                        IsSame = False
                    End If
                Else
                    IsSame = False
                End If
            Next
            Dim oldobjListgoAML_Ref_Director = GoAMLBLL.goAML_CustomerBLL.GetCustomerDirector(obj_Customer.CIF)
            If oldobjListgoAML_Ref_Director.Count <> objListgoAML_Ref_Director.Count Then
                IsSame = False
            End If
            For Each item As goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                Dim objDirectors = oldobjListgoAML_Ref_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).FirstOrDefault
                If objDirectors IsNot Nothing Then
                    If item.FK_Entity_ID <> objDirectors.FK_Entity_ID Then
                        IsSame = False
                    End If
                    If item.Title <> objDirectors.Title Then
                        IsSame = False
                    End If
                    If item.First_name <> objDirectors.First_name Then
                        IsSame = False
                    End If
                    If item.Alias <> objDirectors.Alias Then
                        IsSame = False
                    End If
                    If item.Birth_Place <> objDirectors.Birth_Place Then
                        IsSame = False
                    End If
                    If item.BirthDate <> objDirectors.BirthDate Then
                        IsSame = False
                    End If
                    If item.Comments <> objDirectors.Comments Then
                        IsSame = False
                    End If
                    If item.Deceased <> objDirectors.Deceased Then
                        IsSame = False
                    End If
                    If item.Deceased_Date <> objDirectors.Deceased_Date Then
                        IsSame = False
                    End If
                    If item.Email <> objDirectors.Email Then
                        IsSame = False
                    End If
                    If item.Email2 <> objDirectors.Email2 Then
                        IsSame = False
                    End If
                    If item.Email3 <> objDirectors.Email3 Then
                        IsSame = False
                    End If
                    If item.Email4 <> objDirectors.Email4 Then
                        IsSame = False
                    End If
                    If item.Email5 <> objDirectors.Email5 Then
                        IsSame = False
                    End If
                    If item.Employer_Name <> objDirectors.Employer_Name Then
                        IsSame = False
                    End If
                    If item.Last_Name <> objDirectors.Last_Name Then
                        IsSame = False
                    End If
                    If item.Mothers_Name <> objDirectors.Mothers_Name Then
                        IsSame = False
                    End If
                    If item.SSN <> objDirectors.SSN Then
                        IsSame = False
                    End If
                    If item.Gender <> objDirectors.Gender Then
                        IsSame = False
                    End If
                    If item.ID_Number <> objDirectors.ID_Number Then
                        IsSame = False
                    End If
                    If item.Nationality1 <> objDirectors.Nationality1 Then
                        IsSame = False
                    End If
                    If item.Nationality2 <> objDirectors.Nationality2 Then
                        IsSame = False
                    End If
                    If item.Nationality3 <> objDirectors.Nationality3 Then
                        IsSame = False
                    End If
                    If item.Occupation <> objDirectors.Occupation Then
                        IsSame = False
                    End If
                    If item.Passport_Country <> objDirectors.Passport_Country Then
                        IsSame = False
                    End If
                    If item.Passport_Number <> objDirectors.Passport_Number Then
                        IsSame = False
                    End If
                    If item.Residence <> objDirectors.Residence Then
                        IsSame = False
                    End If
                    If item.Source_of_Wealth <> objDirectors.Source_of_Wealth Then
                        IsSame = False
                    End If
                    If item.Tax_Number <> objDirectors.Tax_Number Then
                        IsSame = False
                    End If
                    If item.Tax_Reg_Number <> objDirectors.Tax_Reg_Number Then
                        IsSame = False
                    End If
                Else
                    IsSame = False
                End If
            Next
            Dim oldobjListgoAML_Ref_Director_Address As New List(Of goAML_Ref_Address)
            For Each item As goAML_Ref_Customer_Entity_Director In oldobjListgoAML_Ref_Director
                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                For Each itemx As goAML_Ref_Address In GoAMLBLL.goAML_CustomerBLL.GetDirectorAddresses(pk_director)
                    oldobjListgoAML_Ref_Director_Address.Add(itemx)
                Next
            Next
            If oldobjListgoAML_Ref_Director_Address.Count <> objListgoAML_Ref_Director_Address.Count Then
                IsSame = False
            End If
            For Each item As goAML_Ref_Address In objListgoAML_Ref_Director_Address
                Dim objAddressEMPCORP = oldobjListgoAML_Ref_Director_Address.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                If objAddressEMPCORP IsNot Nothing Then
                    If item.FK_Ref_Detail_Of <> objAddressEMPCORP.FK_Ref_Detail_Of Then
                        IsSame = False
                    End If
                    If item.FK_To_Table_ID <> objAddressEMPCORP.FK_To_Table_ID Then
                        IsSame = False
                    End If
                    If item.Address_Type <> objAddressEMPCORP.Address_Type Then
                        IsSame = False
                    End If
                    If item.Town <> objAddressEMPCORP.Town Then
                        IsSame = False
                    End If
                    If item.Address <> objAddressEMPCORP.Address Then
                        IsSame = False
                    End If
                    If item.City <> objAddressEMPCORP.City Then
                        IsSame = False
                    End If
                    If item.Country_Code <> objAddressEMPCORP.Country_Code Then
                        IsSame = False
                    End If
                    If item.Zip <> objAddressEMPCORP.Zip Then
                        IsSame = False
                    End If
                    If item.State <> objAddressEMPCORP.State Then
                        IsSame = False
                    End If
                    If item.Comments <> objAddressEMPCORP.Comments Then
                        IsSame = False
                    End If
                Else
                    IsSame = False
                End If
            Next
            Dim oldobjListgoAML_Ref_Director_Employer_Address As New List(Of goAML_Ref_Address)
            For Each item As goAML_Ref_Customer_Entity_Director In oldobjListgoAML_Ref_Director
                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                For Each itemx As goAML_Ref_Address In GoAMLBLL.goAML_CustomerBLL.GetDirectorEmployerAddresses(pk_director)
                    oldobjListgoAML_Ref_Director_Employer_Address.Add(itemx)
                Next
            Next
            If oldobjListgoAML_Ref_Director_Employer_Address.Count <> objListgoAML_Ref_Director_Employer_Address.Count Then
                IsSame = False
            End If
            For Each item As goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                Dim objAddressEMPDIRECTOR = oldobjListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                If objAddressEMPDIRECTOR IsNot Nothing Then
                    If item.FK_Ref_Detail_Of <> objAddressEMPDIRECTOR.FK_Ref_Detail_Of Then
                        IsSame = False
                    End If
                    If item.FK_To_Table_ID <> objAddressEMPDIRECTOR.FK_To_Table_ID Then
                        IsSame = False
                    End If
                    If item.Address_Type <> objAddressEMPDIRECTOR.Address_Type Then
                        IsSame = False
                    End If
                    If item.Town <> objAddressEMPDIRECTOR.Town Then
                        IsSame = False
                    End If
                    If item.Address <> objAddressEMPDIRECTOR.Address Then
                        IsSame = False
                    End If
                    If item.City <> objAddressEMPDIRECTOR.City Then
                        IsSame = False
                    End If
                    If item.Country_Code <> objAddressEMPDIRECTOR.Country_Code Then
                        IsSame = False
                    End If
                    If item.Zip <> objAddressEMPDIRECTOR.Zip Then
                        IsSame = False
                    End If
                    If item.State <> objAddressEMPDIRECTOR.State Then
                        IsSame = False
                    End If
                    If item.Comments <> objAddressEMPDIRECTOR.Comments Then
                        IsSame = False
                    End If
                Else
                    IsSame = False
                End If
            Next
            Dim oldobjListgoAML_Ref_Director_Phone As New List(Of goAML_Ref_Phone)

            For Each item As goAML_Ref_Customer_Entity_Director In oldobjListgoAML_Ref_Director
                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                For Each itemx As goAML_Ref_Phone In GoAMLBLL.goAML_CustomerBLL.GetDirectorPhone(pk_director)
                    oldobjListgoAML_Ref_Director_Phone.Add(itemx)
                Next
            Next
            If oldobjListgoAML_Ref_Director_Phone.Count <> objListgoAML_Ref_Director_Phone.Count Then
                IsSame = False
            End If
            For Each item As goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                Dim objPhoneEMPDirector = oldobjListgoAML_Ref_Director_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                If objPhoneEMPDirector IsNot Nothing Then
                    If item.FK_Ref_Detail_Of <> objPhoneEMPDirector.FK_Ref_Detail_Of Then
                        IsSame = False
                    End If
                    If item.FK_for_Table_ID <> objPhoneEMPDirector.FK_for_Table_ID Then
                        IsSame = False
                    End If
                    If item.Tph_Contact_Type <> objPhoneEMPDirector.Tph_Contact_Type Then
                        IsSame = False
                    End If
                    If item.Tph_Communication_Type <> objPhoneEMPDirector.Tph_Communication_Type Then
                        IsSame = False
                    End If
                    If item.tph_country_prefix <> objPhoneEMPDirector.tph_country_prefix Then
                        IsSame = False
                    End If
                    If item.tph_extension <> objPhoneEMPDirector.tph_extension Then
                        IsSame = False
                    End If
                    If item.tph_number <> objPhoneEMPDirector.tph_number Then
                        IsSame = False
                    End If
                    If item.comments <> objPhoneEMPDirector.comments Then
                        IsSame = False
                    End If
                Else
                    IsSame = False
                End If
            Next
            Dim oldobjListgoAML_Ref_Director_Employer_Phone As New List(Of goAML_Ref_Phone)
            For Each item As goAML_Ref_Customer_Entity_Director In oldobjListgoAML_Ref_Director
                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                For Each itemx As goAML_Ref_Phone In GoAMLBLL.goAML_CustomerBLL.GetDirectorEmployerPhones(pk_director)
                    oldobjListgoAML_Ref_Director_Employer_Phone.Add(itemx)
                Next
            Next
            If oldobjListgoAML_Ref_Director_Employer_Phone.Count <> objListgoAML_Ref_Director_Employer_Phone.Count Then
                IsSame = False
            End If
            For Each item As goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                Dim objPhoneDirector = oldobjListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                If objPhoneDirector IsNot Nothing Then
                    If item.FK_Ref_Detail_Of <> objPhoneDirector.FK_Ref_Detail_Of Then
                        IsSame = False
                    End If
                    If item.FK_for_Table_ID <> objPhoneDirector.FK_for_Table_ID Then
                        IsSame = False
                    End If
                    If item.Tph_Contact_Type <> objPhoneDirector.Tph_Contact_Type Then
                        IsSame = False
                    End If
                    If item.Tph_Communication_Type <> objPhoneDirector.Tph_Communication_Type Then
                        IsSame = False
                    End If
                    If item.tph_country_prefix <> objPhoneDirector.tph_country_prefix Then
                        IsSame = False
                    End If
                    If item.tph_extension <> objPhoneDirector.tph_extension Then
                        IsSame = False
                    End If
                    If item.tph_number <> objPhoneDirector.tph_number Then
                        IsSame = False
                    End If
                    If item.comments <> objPhoneDirector.comments Then
                        IsSame = False
                    End If
                Else
                    IsSame = False
                End If
            Next
            Dim oldobjListgoAML_Ref_Identification_Director As New List(Of goAML_Person_Identification)
            For Each item As goAML_Ref_Customer_Entity_Director In oldobjListgoAML_Ref_Director
                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                For Each itemx As goAML_Person_Identification In GoAMLBLL.goAML_CustomerBLL.GetDirectorIdentifications(pk_director)
                    oldobjListgoAML_Ref_Identification_Director.Add(itemx)
                Next
            Next
            If oldobjListgoAML_Ref_Identification_Director.Count <> objListgoAML_Ref_Identification_Director.Count Then
                IsSame = False
            End If
            For Each item As goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                Dim objIdentificationCORP = oldobjListgoAML_Ref_Identification_Director.Where(Function(x) x.PK_Person_Identification_ID = item.PK_Person_Identification_ID).FirstOrDefault
                If objIdentificationCORP IsNot Nothing Then
                    If item.isReportingPerson <> objIdentificationCORP.isReportingPerson Then
                        IsSame = False
                    End If
                    If item.FK_Person_ID <> objIdentificationCORP.FK_Person_ID Then
                        IsSame = False
                    End If
                    If item.Type <> objIdentificationCORP.Type Then
                        IsSame = False
                    End If
                    If item.Number <> objIdentificationCORP.Number Then
                        IsSame = False
                    End If
                    If item.Issue_Date.ToString <> objIdentificationCORP.Issue_Date.ToString Then
                        IsSame = False
                    End If
                    If item.Issued_By <> objIdentificationCORP.Issued_By Then
                        IsSame = False
                    End If
                    If item.Expiry_Date.ToString <> objIdentificationCORP.Expiry_Date.ToString Then
                        IsSame = False
                    End If
                    If item.Issued_Country <> objIdentificationCORP.Issued_Country Then
                        IsSame = False
                    End If
                    If item.FK_Person_Type <> objIdentificationCORP.FK_Person_Type Then
                        IsSame = False
                    End If
                    If item.Identification_Comment <> objIdentificationCORP.Identification_Comment Then
                        IsSame = False
                    End If
                Else
                    IsSame = False
                End If
            Next

            If IsEmailChange() Then
                Return True
            End If
            If IsUrlChange() Then
                Return True
            End If
            If IsEntityIdentificationChange() Then
                Return True
            End If
            If IsSanctionChange() Then
                Return True
            End If
            If IsRelatedPersonChange() Then
                Return True
            End If
            If IsRelatedEntitiesChange() Then
                Return True
            End If

            'add by Nael, 2023-10-10
            If IsDirectorChange() Then
                Return True
            End If
            'end add



        End If

        If IsSame Then
            Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
        End If
        Return True '' Added on 16 Nov 2020, Thanks to CIMB
    End Function
    'Update: Zikri_15092020 Add Validation GCN
    Protected Sub CheckGCN_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            'Dim strid As String = GetId()
            'obj_Customer = goAML_CustomerBLL.GetCustomerbyCIF(strid)
            If GCNPrimary.Checked = True Then
                If txt_GCN.Value IsNot Nothing Then
                    Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_GCN.Value)
                    If checkGCN IsNot Nothing Then
                        If checkGCN.CIF <> obj_Customer.CIF Then
                            Throw New ApplicationException("Konfirmasi : Untuk GCN " + txt_GCN.Value + " sudah memiliki GCNPrimary pada CIF " + checkGCN.CIF + " Jika di save, status GCNPrimary di CIF " + checkGCN.CIF + " akan dilepas dan akan dipasang di CIF ini")
                        End If
                    End If
                Else
                    DirectCast(GCNPrimary, Checkbox).Checked = False
                    Throw New Exception("GCN is required")
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update
    Protected Sub BtnSubmit_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim obj_customer_grid = New goAML_CustomerDataBLL.goAML_Grid_Customer
            Dim obj_customer2 = New goAML_Ref_Customer2



            If IsDataCustomerValid() Then
                With obj_customer_grid
                    .ObjList_GoAML_PreviousName = objListgoAML_Ref_Previous_Name
                    .ObjList_GoAML_Email = objListgoAML_Ref_Email
                    .ObjList_GoAML_EmploymentHistory = objListgoAML_Ref_EmploymentHistory
                    .ObjList_GoAML_PEP = objListgoAML_Ref_PersonPEP
                    .ObjList_GoAML_NetworkDevice = objListgoAML_Ref_NetworkDevice
                    .ObjList_GoAML_SocialMedia = objListgoAML_Ref_SocialMedia
                    .ObjList_GoAML_Sanction = objListgoAML_Ref_Sanction
                    .ObjList_GoAML_RelatedPerson = objListgoAML_Ref_RelatedPerson
                    .ObjList_GoAML_AdditionalInformation = objListgoAML_Ref_AdditionalInfo
                    .ObjList_GoAML_URL = objListgoAML_Ref_EntityUrl
                    .ObjList_GoAML_EntityIdentification = objListgoAML_Ref_EntityIdentification
                    .ObjList_GoAML_RelatedEntities = objListgoAML_Ref_RelatedEntities
                    .objList_GoAML_Ref_Director = objListgoAML_Ref_Director2
                End With
                With obj_Customer
                    'dedy added
                    'Dim IntResult As Integer = 0
                    'Dim StrQuery As String = ""
                    'StrQuery = "select GCN, count(1) from goAML_Ref_Customer where GCN = '" & txt_GCN.Value & "' GROUP BY GCN"
                    'Dim dtSet As Data.DataSet = SQLHelper.ExecuteDataSet(SQLHelper.strConnectionString, Data.CommandType.Text, StrQuery)
                    'If dtSet.Tables.Count > 0 Then
                    '    Dim dtTable As Data.DataTable = dtSet.Tables(0)
                    '    If dtTable.Rows.Count > 0 Then
                    '        'IntResult = dtTable.Rows(0).Item(0).ToString
                    '        '.isGCNPrimary = Convert.ToBoolean(IIf(cmbisGCNPrimary.Checked = True, 1, 1))
                    '        'GCNPrimary.Checked = True
                    '        GCNPrimary.Checked = True
                    '    End If
                    'End If
                    ''Dedy end added

                    .CIF = txt_CIF.Text
                    .Status_Code = Cmb_StatusCode.Value
                    .isUpdateFromDataSource = Convert.ToBoolean(IIf(UpdateFromDataSource.Checked = True, 1, 0))
                    'dedy added 25082020
                    .isGCNPrimary = Convert.ToBoolean(IIf(GCNPrimary.Checked = True, 1, 0))
                    'dedy added 25082020
                    'Update: Zikri_15092020 Add Validation GCN
                    .GCN = txt_GCN.Value
                    If TextFieldOpeningDate.RawValue IsNot Nothing Then
                        .Opening_Date = Convert.ToDateTime(TextFieldOpeningDate.RawValue)
                    End If

                    If TextFieldClosingDate.RawValue IsNot Nothing Then
                        .Closing_Date = Convert.ToDateTime(TextFieldClosingDate.RawValue)
                    End If
                    .Comments = TextFieldComments.Text

                    .Opening_Branch_Code = cmb_openingBranch.SelectedItemValue
                    .Status = cmb_Status.SelectedItemValue
                    'End Update

                    'add by Septian, goAML 5.0.1
                    obj_customer2.CIF = .CIF
                    obj_customer2.PK_Customer_ID = .PK_Customer_ID
                    obj_customer2.FK_Customer_Type_ID = .FK_Customer_Type_ID
                    'end add
                    If obj_Customer.FK_Customer_Type_ID = 1 Then
                        .INDV_Title = txt_INDV_Title.Text
                        .INDV_Gender = cmb_INDV_Gender.SelectedItemValue
                        .INDV_Last_Name = txt_INDV_Last_Name.Text
                        If txt_INDV_Birthdate.RawValue IsNot Nothing Then
                            .INDV_BirthDate = Convert.ToDateTime(txt_INDV_Birthdate.RawValue)
                        Else
                            .INDV_BirthDate = Nothing
                        End If
                        .INDV_Birth_Place = txt_INDV_Birth_place.Text
                        .INDV_Mothers_Name = txt_INDV_Mothers_name.Text
                        .INDV_Alias = txt_INDV_Alias.Text
                        .INDV_SSN = txt_INDV_SSN.Text
                        .INDV_Passport_Number = txt_INDV_Passport_number.Text
                        'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
                        'coba zik, pengen lihat value ketika di bind, sama ketika d save
                        .INDV_Passport_Country = cmb_INDV_Passport_country.SelectedItemValue
                        'End Update
                        .INDV_ID_Number = txt_INDV_ID_Number.Text
                        .INDV_Nationality1 = cmb_INDV_Nationality1.SelectedItemValue
                        .INDV_Nationality2 = cmb_INDV_Nationality2.SelectedItemValue
                        .INDV_Nationality3 = cmb_INDV_Nationality3.SelectedItemValue
                        .INDV_Residence = cmb_INDV_Residence.SelectedItemValue
                        'comment by septian
                        'uncomment by Nael, goAML 5.2
                        .INDV_Email = txt_INDV_Email.Text
                        .INDV_Email2 = txt_INDV_Email2.Text
                        .INDV_Email3 = txt_INDV_Email3.Text
                        .INDV_Email4 = txt_INDV_Email4.Text
                        .INDV_Email5 = txt_INDV_Email5.Text
                        .INDV_Email5 = txt_INDV_Email5.Text
                        'end comment
                        .INDV_Occupation = txt_INDV_Occupation.Text
                        .INDV_Employer_Name = txt_INDV_Employer_Name.Text
                        .INDV_Deceased = Convert.ToBoolean(IIf(cb_Deceased.Checked = True, 1, 0))
                        If cb_Deceased.Checked Then
                            .INDV_Deceased_Date = Convert.ToDateTime(txt_deceased_date.RawValue)
                        Else
                            .INDV_Deceased_Date = Nothing
                        End If
                        .INDV_Tax_Number = txt_INDV_Tax_Number.Text
                        .INDV_Tax_Reg_Number = Convert.ToBoolean(IIf(cb_tax.Checked = True, 1, 0))
                        .INDV_Source_of_Wealth = txt_INDV_Source_of_Wealth.Text
                        .INDV_Comments = txt_INDV_Comments.Text

                        '' add by septian, go AML 5.0.1, 2023-1-30
                        'obj_customer2.Country_Of_Birth = cmb_INDV_Country_Of_Birth.SelectedItemValue
                        'obj_customer2.Full_Name_Frn = txt_INDV_Foreign_Name.Value
                        'obj_customer2.Residence_Since = Convert.ToDateTime(dte_INDV_ResidenceSince.RawValue)
                        obj_customer2.Is_Protected = Convert.ToBoolean(IIf(cb_IsProtected.Checked = True, 1, 0))
                        '' end add
                    Else
                        .Corp_Name = txt_Corp_Name.Text
                        '.Corp_Role = cmb_corp_role.Value
                        .Corp_Commercial_Name = txt_Corp_Commercial_Name.Text
                        .Corp_Incorporation_Legal_Form = cmb_Corp_Incorporation_Legal_Form.SelectedItemValue
                        .Corp_Incorporation_Number = txt_Corp_Incorporation_number.Text
                        .Corp_Business = txt_Corp_Business.Text
                        'comment by septian, goAML 5.0.1
                        'uncomment by Nael, goAML 5.2
                        .Corp_Email = txt_Corp_Email.Text
                        .Corp_Url = txt_Corp_url.Text
                        'end comment
                        .Corp_Incorporation_State = txt_Corp_incorporation_state.Text
                        .Corp_Incorporation_Country_Code = cmb_Corp_Incorporation_Country_Code.SelectedItemValue
                        If txt_Corp_incorporation_date.RawValue IsNot Nothing Then
                            .Corp_Incorporation_Date = Convert.ToDateTime(txt_Corp_incorporation_date.RawValue)
                        Else
                            .Corp_Incorporation_Date = Nothing
                        End If

                        .Corp_Business_Closed = Convert.ToBoolean(IIf(cbTutup.Checked = True, 1, 0))
                        If cbTutup.Checked Then
                            .Corp_Date_Business_Closed = Convert.ToDateTime(txt_Corp_date_business_closed.RawValue)
                        Else
                            .Corp_Date_Business_Closed = Nothing
                        End If
                        .Corp_Tax_Number = txt_Corp_tax_number.Text
                        .Corp_Comments = txt_Corp_Comments.Text

                        '' add by septian, go AML 5.0.1, 2023-1-30
                        'obj_customer2.Entity_Status = cmb_Corp_EntityStatus.SelectedItemValue
                        'obj_customer2.Entity_Status_Date = Convert.ToDateTime(dte_Corp_EntityStatusDate.RawValue)
                        '' end add
                    End If


                    .Active = True
                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .CreatedDate = DateTime.Now
                    .LastUpdateDate = DateTime.Now
                End With
                IfHaveChanges()
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                    objgoAML_CustomerBLL.SaveEditTanpaApproval(obj_Customer, objListgoAML_Ref_Director, ObjModule, objListgoAML_Ref_Phone, objListgoAML_Ref_Identification, objListgoAML_Ref_Address, objListgoAML_Ref_Employer_Address, objListgoAML_Ref_Employer_Phone, objListgoAML_Ref_Director_Employer_Address, objListgoAML_Ref_Director_Employer_Phone, objListgoAML_Ref_Director_Address, objListgoAML_Ref_Director_Phone, objListgoAML_Ref_Identification_Director, obj_customer2, obj_customer_grid)
                    Panelconfirmation.Hidden = False
                    FormPanelInput.Hidden = True
                    LblConfirmation.Text = "Data Saved into Database"
                Else
                    objgoAML_CustomerBLL.SaveEditApproval(obj_Customer, objListgoAML_Ref_Director, ObjModule, objListgoAML_Ref_Phone, objListgoAML_Ref_Identification, objListgoAML_Ref_Address, objListgoAML_Ref_Employer_Address, objListgoAML_Ref_Employer_Phone, objListgoAML_Ref_Director_Employer_Address, objListgoAML_Ref_Director_Employer_Phone, objListgoAML_Ref_Director_Address, objListgoAML_Ref_Director_Phone, objListgoAML_Ref_Identification_Director, obj_customer2, obj_customer_grid)
                    Panelconfirmation.Hidden = False
                    FormPanelInput.Hidden = True
                    LblConfirmation.Text = "Data Saved into Pending Approval"
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Daniel 2020/11/05
    Public Function approvalchecker()
        Try
            Using objdb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
                Dim temp As String = obj_Customer.PK_Customer_ID
                'If objdb.ModuleApprovals.Where(Function(x) x.ModuleKey = temp).Count > 0 Then 
                If objdb.ModuleApprovals.Where(Function(x) x.ModuleKey = temp And x.ModuleName = "goAML_Ref_Customer").Count > 0 Then '' Fix 2-Nov-2022, Felix. Tambah baca ModuleName
                    Return False
                End If
            End Using
            'tambahan return true tertinggal
            Return True
        Catch ex As Exception
            Throw New ApplicationException("There is An Error Occurred While Checking Approval")
        End Try
    End Function
    'end Daniel
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnBack_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Private Function GetId() As String
        Dim strid As String = Request.Params("ID")
        Dim id As String = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
        Return id
    End Function

    Protected Sub btnAddCustomerAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            cmb_kategoriaddress.IsReadOnly = False
            Address_INDV.ReadOnly = False
            Town_INDV.ReadOnly = False
            City_INDV.ReadOnly = False
            Zip_INDV.ReadOnly = False
            cmb_kodenegara.IsReadOnly = False
            State_INDVD.ReadOnly = False
            Btn_Save_Address.Hidden = False
            WindowDetailAddress.Title = "Customer Address Add"

            'Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Store_KategoriAddress.DataBind()

            'Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Kode_Negara.DataBind()
            ClearinputCustomerAddress()
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddCustomerDirector_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            GridEmail_Director.IsViewMode = False
            GridSanction_Director.IsViewMode = False
            GridPEP_Director.IsViewMode = False

            tempflag = 0
            FP_Director.Hidden = False
            WindowDetailDirector.Hidden = False
            'cmb_director.ReadOnly = False
            cmb_peran.IsReadOnly = False
            txt_Director_Title.ReadOnly = False
            txt_Director_Last_Name.ReadOnly = False
            txt_Director_SSN.ReadOnly = False
            txt_Director_BirthDate.ReadOnly = False
            txt_Director_Birth_Place.ReadOnly = False
            txt_Director_Mothers_Name.ReadOnly = False
            txt_Director_Alias.ReadOnly = False
            txt_Director_Passport_Number.ReadOnly = False
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            cmb_Director_Passport_Country.IsReadOnly = False
            'End Update
            txt_Director_ID_Number.ReadOnly = False
            cmb_Director_Nationality1.IsReadOnly = False
            cmb_Director_Nationality2.IsReadOnly = False
            cmb_Director_Nationality3.IsReadOnly = False
            cmb_Director_Gender.IsReadOnly = False
            cmb_Director_Residence.IsReadOnly = False
            'txt_Director_Email.ReadOnly = False
            'txt_Director_Email2.ReadOnly = False
            'txt_Director_Email3.ReadOnly = False
            'txt_Director_Email4.ReadOnly = False
            'txt_Director_Email5.ReadOnly = False
            txt_Director_Occupation.ReadOnly = False
            txt_Director_employer_name.ReadOnly = False
            cb_Director_Deceased.ReadOnly = False
            txt_Director_Deceased_Date.ReadOnly = False
            txt_Director_Tax_Number.ReadOnly = False
            cb_Director_Tax_Reg_Number.ReadOnly = False
            txt_Director_Source_of_Wealth.ReadOnly = False
            txt_Director_Comments.ReadOnly = False
            btnsaveDirector.Hidden = False
            btnsavedirectorphone.Hidden = False
            btnsavedirectorAddress.Hidden = False
            btnsaveEmpdirectorPhone.Hidden = False
            btnsaveEmpdirectorAddress.Hidden = False
            WindowDetailDirector.Title = "Customer Director Add"


            GridEmail_Director.LoadData(New List(Of DataModel.goAML_Ref_Customer_Email))
            GridEmail_Director.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Customer_Email)
            GridEmail_Director.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Customer_Email)

            GridSanction_Director.LoadData(New List(Of DataModel.goAML_Ref_Customer_Sanction))
            GridSanction_Director.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Customer_Sanction)
            GridSanction_Director.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Customer_Sanction)

            GridPEP_Director.LoadData(New List(Of DataModel.goAML_Ref_Customer_PEP))
            GridPEP_Director.objListgoAML_Ref = New List(Of DataModel.goAML_Ref_Customer_PEP)
            GridPEP_Director.objListgoAML_vw = New List(Of DataModel.goAML_Ref_Customer_PEP)


            'StorePeran.DataSource = goAML_CustomerBLL.GetListPeranDalamKorporasi()
            'StorePeran.DataBind()
            'Store_Director_Gender.DataSource = goAML_CustomerBLL.GetListJenisKelamin()
            'Store_Director_Gender.DataBind()

            'Store_Director_Nationality1.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_Nationality1.DataBind()
            'Store_Director_Nationality2.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_Nationality2.DataBind()
            'Store_Director_Nationality3.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_Nationality3.DataBind()
            'Store_Director_Residence.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_Residence.DataBind()
            ClearinputCustomerDirector()

            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddEmployerAddresses_DirectClick(sender As Object, e As DirectEventArgs)

        Try
            If objListgoAML_vw_Employer_Addresses.Count = 0 Then
                FP_Address_Emp_INDVD.Hidden = False
                WindowDetailAddress.Hidden = False
                cmb_emp_kategoriaddress.IsReadOnly = False
                emp_Address_INDV.ReadOnly = False
                emp_Town_INDV.ReadOnly = False
                emp_City_INDV.ReadOnly = False
                emp_Zip_INDV.ReadOnly = False
                cmb_emp_kodenegara.IsReadOnly = False
                emp_State_INDVD.ReadOnly = False
                btn_saveempaddress.Hidden = False
                WindowDetailAddress.Title = "Employer Address Add"

                'Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_emp_KategoriAddress.DataBind()

                'Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Store_emp_Kode_Negara.DataBind()

                ClearinputCustomerEmpAddress()
            Else
                Throw New Exception("can only enter one Workplace Address Information")
            End If
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddCustomerIdentifications_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FP_Identification_INDV.Hidden = False
            WindowDetailIdentification.Hidden = False
            cmb_Jenis_Dokumen.IsReadOnly = False
            txt_identification_comment.ReadOnly = False
            txt_issued_by.ReadOnly = False
            txt_issued_date.ReadOnly = False
            txt_issued_expired_date.ReadOnly = False
            cmb_issued_country.IsReadOnly = False
            txt_number_identification.ReadOnly = False
            btn_save_identification.Hidden = False
            WindowDetailIdentification.Title = "Customer Identification Add"

            'Store_jenis_dokumen.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            'Store_jenis_dokumen.DataBind()

            'Store_issued_country.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_issued_country.DataBind()
            ClearinputCustomerIdentification()
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SaveEditDirectorIdentificationDetail()

        With objTempCustomerIdentificationEdit_Director

            .Number = txt_number_identification_Director.Value
            .Issued_By = txt_issued_by_Director.Value
            If txt_issued_date_Director.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date_Director.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date_Director.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date_Director.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment_Director.Value
            .Country = cmb_issued_country_Director.SelectedItemText
            .Type_Document = cmb_Jenis_Dokumen_Director.SelectedItemText
            .FK_Person_Type = 4
        End With

        With objTempgoAML_Ref_Identification_Director
            .Number = txt_number_identification_Director.Value
            .Issued_By = txt_issued_by_Director.Value
            If txt_issued_date_Director.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date_Director.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date_Director.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date_Director.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment_Director.Value
            .Issued_Country = cmb_issued_country_Director.SelectedItemValue
            .Type = cmb_Jenis_Dokumen_Director.SelectedItemValue
            .FK_Person_Type = 4
        End With

        objTempCustomerIdentificationEdit_Director = Nothing
        objTempgoAML_Ref_Identification_Director = Nothing

        Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = tempflag).ToList()
        Store_Director_Identification.DataBind()

        FP_Identification_Director.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputDirectorIdentification()
    End Sub


    Protected Sub btnSaveDirectorIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataDirectorIdentificationValid() Then
                'If objTempCustomerPhoneEdit Is Nothing Then
                If objTempCustomerIdentificationEdit_Director Is Nothing Then
                    SaveAddDirectorIdentificationDetail()
                Else
                    SaveEditDirectorIdentificationDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SaveAddDirectorIdentificationDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Person_Identification
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Person_Identification
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Person_Identification_ID = intpk
            .isReportingPerson = 0
            .FK_Person_Type = 4
            .FK_Person_ID = tempflag
            .Number = txt_number_identification_Director.Value
            .Issued_By = txt_issued_by_Director.Value
            If txt_issued_date_Director.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date_Director.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date_Director.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date_Director.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment_Director.Value
            .Country = cmb_issued_country_Director.SelectedItemText
            .Type_Document = cmb_Jenis_Dokumen_Director.SelectedItemText
        End With

        With objNewgoAML_Customer
            .PK_Person_Identification_ID = intpk
            .isReportingPerson = 0
            .FK_Person_Type = 4
            .FK_Person_ID = tempflag
            .Number = txt_number_identification_Director.Value
            .Issued_Country = cmb_issued_country_Director.SelectedItemValue
            .Issued_By = txt_issued_by_Director.Value
            If txt_issued_date_Director.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date_Director.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date_Director.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date_Director.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment_Director.Value
            .Issued_Country = cmb_issued_country_Director.SelectedItemValue
            .Type = cmb_Jenis_Dokumen_Director.SelectedItemValue

        End With

        objListgoAML_vw_Customer_Identification_Director.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Identification_Director.Add(objNewgoAML_Customer)

        Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(X) X.FK_Person_ID = tempflag).ToList()
        Store_Director_Identification.DataBind()

        FP_Identification_Director.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputDirectorIdentification()
    End Sub


    Function IsDataDirectorIdentificationValid() As Boolean
        If cmb_issued_country_Director.SelectedItemValue = "" Then
            Throw New Exception("Country is required")
        End If
        If cmb_Jenis_Dokumen_Director.SelectedItemValue = "" Then
            Throw New Exception("Identification Type is required")
        End If
        If txt_number_identification_Director.Text.Trim = "" Then
            Throw New Exception("Number Identification is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(txt_number_identification_Director.Text) Then
            Throw New Exception("Identification Number is Invalid")
        End If
        'End Update
        Return True
    End Function

    Protected Sub btnAddDirectorIdentifications_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FP_Identification_Director.Hidden = False
            WindowDetailIdentification.Hidden = False
            cmb_Jenis_Dokumen_Director.IsReadOnly = False
            txt_identification_comment_Director.ReadOnly = False
            txt_issued_by_Director.ReadOnly = False
            txt_issued_date_Director.ReadOnly = False
            txt_issued_expired_date_Director.ReadOnly = False
            cmb_issued_country_Director.IsReadOnly = False
            txt_number_identification_Director.ReadOnly = False
            btn_save_identification_Director.Hidden = False
            WindowDetailIdentification.Title = "Director Identification Add"

            'Storejenisdokumen_Director.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            'Storejenisdokumen_Director.DataBind()

            'StoreIssueCountry_Director.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'StoreIssueCountry_Director.DataBind()
            ClearinputDirectorIdentification()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddCustomerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            btnSavedetail.Hidden = False
            cb_phone_Contact_Type.IsReadOnly = False
            cb_phone_Communication_Type.IsReadOnly = False
            txt_phone_Country_prefix.ReadOnly = False
            txt_phone_number.ReadOnly = False
            txt_phone_extension.ReadOnly = False
            txt_phone_comments.ReadOnly = False
            btnSavedetail.Hidden = False
            WindowDetail.Title = "Customer Phone Add"

            Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing
            'Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Store_Contact_Type.DataBind()

            'Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
            'Store_Communication_Type.DataBind()
            ClearinputCustomerPhones()
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddEmployerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            If objListgoAML_vw_Employer_Phones.Count = 0 Then
                FormPanelEmpTaskDetail.Hidden = False
                WindowDetail.Hidden = False
                btn_saveempphone.Hidden = False
                Emp_cb_phone_Contact_Type.IsReadOnly = False
                Emp_cb_phone_Communication_Type.IsReadOnly = False
                Emp_txt_phone_Country_prefix.ReadOnly = False
                Emp_txt_phone_number.ReadOnly = False
                Emp_txt_phone_extension.ReadOnly = False
                Emp_txt_phone_comments.ReadOnly = False
                btn_saveempphone.Hidden = False
                WindowDetail.Title = "Employer Phone Add"

                Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
                Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing
                'Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Emp_Store_Contact_Type.DataBind()

                'Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Emp_Store_Communication_Type.DataBind()
                ClearinputCustomerEmpPhones()
            Else
                Throw New Exception("can only enter one Workplace Phone Information")
            End If
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()
            objTempCustomerPhoneEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomerAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomerAddressValid() Then
                'If objTempCustomerPhoneEdit Is Nothing Then
                If objTempCustomerAddressEdit Is Nothing Then
                    SaveAddAddressDetail()
                Else
                    SaveEditAddressDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomerDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomerDirectorValid() Then
                'If objTempCustomerPhoneEdit Is Nothing Then
                If objTempCustomerDirectorEdit Is Nothing Then
                    SaveAddDirectorDetail()
                Else
                    SaveEditDirectorDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btnSaveCustomerIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomerIdentificationValid() Then
                'If objTempCustomerPhoneEdit Is Nothing Then
                If objTempCustomerIdentificationEdit Is Nothing Then
                    SaveAddIdentificationDetail()
                Else
                    SaveEditIdentificationDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomerPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            If IsDataCustomerPhoneValid() Then
                'If objTempCustomerPhoneEdit Is Nothing Then
                If objTempCustomerPhoneEdit Is Nothing Then
                    SaveAddTaskDetail()
                Else
                    SaveEditTaskDetail()
                End If
            End If

            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SaveEditTaskDetail()


        If objTempgoAML_Ref_Phone IsNot Nothing Then
            With objTempCustomerPhoneEdit
                .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
                .Contact_Type = cb_phone_Contact_Type.SelectedItemText
                .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
                .Communcation_Type = cb_phone_Communication_Type.SelectedItemText
                .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
                .tph_number = txt_phone_number.Text.Trim
                .tph_extension = txt_phone_extension.Text.Trim
                .comments = txt_phone_comments.Text.Trim
            End With

            With objTempgoAML_Ref_Phone
                .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
                .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
                .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
                .tph_number = txt_phone_number.Text.Trim
                .tph_extension = txt_phone_extension.Text.Trim
                .comments = txt_phone_comments.Text.Trim
            End With

            objTempCustomerPhoneEdit = Nothing
            objTempgoAML_Ref_Phone = Nothing

            If obj_Customer.FK_Customer_Type_ID = 1 Then
                Store_INDV_Phones.DataSource = objListgoAML_vw_Customer_Phones.ToList()
                Store_INDV_Phones.DataBind()
            Else
                Store_Phone_Corp.DataSource = objListgoAML_vw_Customer_Phones.ToList()
                Store_Phone_Corp.DataBind()
            End If

        Else
            With objTempEmployerPhonesEdit
                .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
                .Contact_Type = cb_phone_Contact_Type.SelectedItemText
                .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
                .Communcation_Type = cb_phone_Communication_Type.SelectedItemText
                .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
                .tph_number = txt_phone_number.Text.Trim
                .tph_extension = txt_phone_extension.Text.Trim
                .comments = txt_phone_comments.Text.Trim
            End With

            With objTempgoAML_Ref_EmployerPhones
                .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
                .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
                .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
                .tph_number = txt_phone_number.Text.Trim
                .tph_extension = txt_phone_extension.Text.Trim
                .comments = txt_phone_comments.Text.Trim
            End With

            objTempEmployerPhonesEdit = Nothing
            objTempgoAML_Ref_EmployerPhones = Nothing

            Store_Empoloyer_Phone.DataSource = objListgoAML_vw_Employer_Phones.ToList()
            Store_Empoloyer_Phone.DataBind()
        End If

        'objListgoAML_Ref_Phone.Add(objTempgoAML_Ref_Phone)

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerPhones()
    End Sub

    Sub SaveEditAddressDetail()

        If objTempgoAML_Ref_Address IsNot Nothing Then
            With objTempCustomerAddressEdit
                .Address_Type = cmb_kategoriaddress.SelectedItemValue
                .Type_Address_Detail = cmb_kategoriaddress.SelectedItemText
                .Address = Address_INDV.Value
                .City = City_INDV.Value
                .Town = Town_INDV.Value
                .Country_Code = cmb_kodenegara.SelectedItemValue
                .Country = cmb_kodenegara.SelectedItemText
                .Comments = Comment_Address_INDVD.Value
                .State = State_INDVD.Value
                .Zip = Zip_INDV.Value
                .Comments = Comment_Address_INDVD.Text.Trim
            End With

            With objTempgoAML_Ref_Address
                .Address_Type = cmb_kategoriaddress.SelectedItemValue '
                .City = City_INDV.Value
                .Address = Address_INDV.Value
                .Town = Town_INDV.Value
                .Country_Code = cmb_kodenegara.SelectedItemValue
                .Comments = Comment_Address_INDVD.Value
                .State = State_INDVD.Value
                .Zip = Zip_INDV.Value
                .Comments = Comment_Address_INDVD.Text.Trim
            End With

            objTempCustomerAddressEdit = Nothing
            objTempgoAML_Ref_Address = Nothing

            If obj_Customer.FK_Customer_Type_ID = 1 Then
                Store_INDV_Addresses.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
                Store_INDV_Addresses.DataBind()
            Else
                Store_Address_Corp.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
                Store_Address_Corp.DataBind()
            End If

        Else
            With objTempEmployerAddressEdit
                .Address_Type = cmb_kategoriaddress.SelectedItemValue
                .Address = Address_INDV.Value
                .City = City_INDV.Value
                .Town = Town_INDV.Value
                .Country_Code = cmb_kodenegara.SelectedItemValue
                .Comments = Comment_Address_INDVD.Value
                .State = State_INDVD.Value
                .Zip = Zip_INDV.Value
                .Comments = Comment_Address_INDVD.Text.Trim
            End With

            With objTempgoAML_Ref_EmployerAddresses
                .Address_Type = cmb_kategoriaddress.SelectedItemValue '
                .City = City_INDV.Value
                .Address = Address_INDV.Value
                .Town = Town_INDV.Value
                .Country_Code = cmb_kodenegara.SelectedItemValue
                .Comments = Comment_Address_INDVD.Value
                .State = State_INDVD.Value
                .Zip = Zip_INDV.Value
                .Comments = Comment_Address_INDVD.Text.Trim
            End With

            objTempEmployerAddressEdit = Nothing
            objTempgoAML_Ref_EmployerAddresses = Nothing

            Store_Employer_Address.DataSource = objListgoAML_vw_Employer_Addresses.ToList()
            Store_Employer_Address.DataBind()
        End If

        FP_Address_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerAddress()
    End Sub

    Sub SaveEditIdentificationDetail()


        With objTempCustomerIdentificationEdit
            .isReportingPerson = 0
            .FK_Person_ID = obj_Customer.PK_Customer_ID
            .FK_Person_Type = 1
            .Number = txt_number_identification.Value
            .Issued_By = txt_issued_by.Value
            If txt_issued_date.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment.Value
            .Country = cmb_issued_country.SelectedItemText
            .Type_Document = cmb_Jenis_Dokumen.SelectedItemText
        End With

        With objTempgoAML_Ref_Identification
            .isReportingPerson = 0
            .FK_Person_Type = 1
            .FK_Person_ID = obj_Customer.PK_Customer_ID
            .Number = txt_number_identification.Value
            .Issued_By = txt_issued_by.Value
            If txt_issued_date.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment.Value
            .Issued_Country = cmb_issued_country.SelectedItemValue
            .Type = cmb_Jenis_Dokumen.SelectedItemValue
        End With

        'objListgoAML_Ref_Phone.Add(objTempgoAML_Ref_Phone)

        objTempCustomerIdentificationEdit = Nothing
        objTempgoAML_Ref_Identification = Nothing

        Store_INDV_Identification.DataSource = objListgoAML_vw_Customer_Identification.ToList()
        Store_INDV_Identification.DataBind()

        FP_Identification_INDV.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputCustomerIdentification()
    End Sub

    Sub SaveEditDirectorDetail()

        With objTempCustomerDirectorEdit
            .Role = cmb_peran.SelectedItemText
            .LastName = txt_Director_Last_Name.Text

            '.fk_customer_id = cmb_director.Value
        End With

        With objTempgoAML_Ref_Director
            '.FK_Customer_ID = cmb_director.Value
            .Role = cmb_peran.SelectedItemValue
            .Title = txt_Director_Title.Text
            .Last_Name = txt_Director_Last_Name.Text
            .SSN = txt_Director_SSN.Text
            If txt_Director_BirthDate.RawValue IsNot Nothing Then
                .BirthDate = Convert.ToDateTime(txt_Director_BirthDate.RawValue)
            Else
                .BirthDate = Nothing
            End If
            .Birth_Place = txt_Director_Birth_Place.Text
            .Mothers_Name = txt_Director_Mothers_Name.Text
            .Alias = txt_Director_Alias.Text
            .Passport_Number = txt_Director_Passport_Number.Text
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            .Passport_Country = cmb_Director_Passport_Country.SelectedItemValue
            'End Update
            .ID_Number = txt_Director_ID_Number.Text
            .Nationality1 = cmb_Director_Nationality1.SelectedItemValue
            .Nationality2 = cmb_Director_Nationality2.SelectedItemValue
            .Nationality3 = cmb_Director_Nationality3.SelectedItemValue
            .Residence = cmb_Director_Residence.SelectedItemValue
            .Gender = cmb_Director_Gender.SelectedItemValue

            '.Email = txt_Director_Email.Text
            '.Email2 = txt_Director_Email2.Text
            '.Email3 = txt_Director_Email3.Text
            '.Email4 = txt_Director_Email4.Text
            '.Email5 = txt_Director_Email5.Text
            .Occupation = txt_Director_Occupation.Text
            .Employer_Name = txt_Director_employer_name.Text
            .Deceased = Convert.ToBoolean(IIf(cb_Director_Deceased.Checked = True, 1, 0))
            If cb_Director_Deceased.Checked Then
                .Deceased_Date = Convert.ToDateTime(txt_Director_Deceased_Date.RawValue)
            Else
                .Deceased_Date = Nothing
            End If
            .Tax_Number = txt_Director_Tax_Number.Text
            .Tax_Reg_Number = Convert.ToBoolean(IIf(cb_Director_Tax_Reg_Number.Checked = True, 1, 0))
            .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
            .Comments = txt_Director_Comments.Text

            For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                item.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                item.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                item.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                item.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.Vw_Director_Addresses In objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                item.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.Vw_Director_Phones In objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                item.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
                item.FK_Person_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.Vw_Person_Identification In objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
                item.FK_Person_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next


            For Each item As GoAMLDAL.Vw_Director_Employer_Addresses In objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                item.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As GoAMLDAL.Vw_Director_Employer_Phones In objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                item.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next


        End With

        'objListgoAML_Ref_Phone.Add(objTempgoAML_Ref_Phone)
        'add by Septian, Director, 2023-04-17
        objTempgoAML_Ref_Director2 = goAML_Ref_Customer_Entity_Director_Mapper.Serialize(objTempgoAML_Ref_Director)
        With objTempgoAML_Ref_Director2
            objTempgoAML_Ref_Director2.ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = objTempgoAML_Ref_Director.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = objTempgoAML_Ref_Director.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = objTempgoAML_Ref_Director.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = objTempgoAML_Ref_Director.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = objTempgoAML_Ref_Director.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(.PK_goAML_Ref_Customer_Entity_Director_ID.ToString(), GridEmail_Director.objListgoAML_Ref)
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(.PK_goAML_Ref_Customer_Entity_Director_ID, GridSanction_Director.objListgoAML_Ref)
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(.PK_goAML_Ref_Customer_Entity_Director_ID, GridPEP_Director.objListgoAML_Ref)
        End With
        If objListgoAML_Ref_Director2 Is Nothing Then
            objListgoAML_Ref_Director2 = New List(Of DataModel.goAML_Ref_Entity_Director)
            objListgoAML_Ref_Director2.Add(objTempgoAML_Ref_Director2)
        Else
            ' 2023-10-09, Nael: remove object jika sudah ada di objListgoAML_Ref_Director2
            objListgoAML_Ref_Director2.RemoveAll(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objTempgoAML_Ref_Director2.PK_goAML_Ref_Customer_Entity_Director_ID)
            objListgoAML_Ref_Director2.Add(objTempgoAML_Ref_Director2)
        End If
        'end add

        objTempCustomerDirectorEdit = Nothing
        objTempgoAML_Ref_Director = Nothing

        Store_Director_Corp.DataSource = objListgoAML_vw_Customer_Entity_Director.ToList()
        Store_Director_Corp.DataBind()

        FP_Director.Hidden = True
        WindowDetailDirector.Hidden = True
        ClearinputCustomerDirector()
    End Sub


    Sub SaveAddTaskDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Customer_Phones
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Phone
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Phone = intpk

            .FK_Ref_Detail_Of = 1
            .FK_for_Table_ID = obj_Customer.PK_Customer_ID
            .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
            .tph_number = txt_phone_number.Text.Trim
            .tph_extension = txt_phone_extension.Text.Trim
            .comments = txt_phone_comments.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_goAML_Ref_Phone = intpk
            .FK_Ref_Detail_Of = 1
            .FK_for_Table_ID = obj_Customer.PK_Customer_ID
            .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
            .tph_number = txt_phone_number.Text.Trim
            .tph_extension = txt_phone_extension.Text.Trim
            .comments = txt_phone_comments.Text.Trim
        End With

        objListgoAML_vw_Customer_Phones.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Phone.Add(objNewgoAML_Customer)

        If obj_Customer.FK_Customer_Type_ID = 1 Then
            Store_INDV_Phones.DataSource = objListgoAML_vw_Customer_Phones.ToList()
            Store_INDV_Phones.DataBind()
        Else
            Store_Phone_Corp.DataSource = objListgoAML_vw_Customer_Phones.ToList()
            Store_Phone_Corp.DataBind()
        End If
        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerPhones()
    End Sub

    Sub SaveAddAddressDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Customer_Addresses
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Address
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 1
            .Address_Type = cmb_kategoriaddress.SelectedItemValue
            .Address = Address_INDV.Value
            .City = City_INDV.Value
            .Town = Town_INDV.Value
            .Country_Code = cmb_kodenegara.SelectedItemValue
            .Comments = Comment_Address_INDVD.Value
            .State = State_INDVD.Value
            .Zip = Zip_INDV.Value
            .Type_Address_Detail = cmb_kategoriaddress.SelectedItemText
            .Country = cmb_kodenegara.SelectedItemText
            .Comments = Comment_Address_INDVD.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 1
            .FK_To_Table_ID = obj_Customer.PK_Customer_ID
            .Address_Type = cmb_kategoriaddress.SelectedItemValue '
            .City = City_INDV.Value
            .Address = Address_INDV.Value
            .Town = Town_INDV.Value
            .Country_Code = cmb_kodenegara.SelectedItemValue
            .Comments = Comment_Address_INDVD.Value
            .State = State_INDVD.Value
            .Zip = Zip_INDV.Value
            .Comments = Comment_Address_INDVD.Text.Trim
        End With

        objListgoAML_vw_Customer_Addresses.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Address.Add(objNewgoAML_Customer)

        If obj_Customer.FK_Customer_Type_ID = 1 Then
            Store_INDV_Addresses.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
            Store_INDV_Addresses.DataBind()
        Else
            Store_Address_Corp.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
            Store_Address_Corp.DataBind()
        End If

        FP_Address_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerAddress()
    End Sub

    Sub SaveAddDirectorDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Customer_Director
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Customer_Entity_Director
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Customer_Entity_Director_ID = intpk
            .LastName = txt_Director_Last_Name.Text
            .Role = cmb_peran.SelectedItemText
        End With
        objListgoAML_vw_Customer_Entity_Director.Add(objNewVWgoAML_Customer)

        With objNewgoAML_Customer
            .PK_goAML_Ref_Customer_Entity_Director_ID = intpk
            ''.FK_Customer_ID = cmb_director.Value
            .Role = cmb_peran.SelectedItemValue
            .Title = txt_Director_Title.Text
            .Gender = cmb_Director_Gender.SelectedItemValue
            .Last_Name = txt_Director_Last_Name.Text
            If txt_Director_BirthDate.RawValue IsNot Nothing Then
                .BirthDate = Convert.ToDateTime(txt_Director_BirthDate.RawValue)
            Else
                .BirthDate = Nothing
            End If

            .Birth_Place = txt_Director_Birth_Place.Text
            .Mothers_Name = txt_Director_Mothers_Name.Text
            .Alias = txt_Director_Alias.Text
            .SSN = txt_Director_SSN.Text
            .Passport_Number = txt_Director_Passport_Number.Text
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            .Passport_Country = cmb_Director_Passport_Country.SelectedItemValue
            'End Update
            .ID_Number = txt_Director_ID_Number.Text
            .Nationality1 = cmb_Director_Nationality1.SelectedItemValue
            .Nationality2 = cmb_Director_Nationality2.SelectedItemValue
            .Nationality3 = cmb_Director_Nationality3.SelectedItemValue
            .Residence = cmb_Director_Residence.SelectedItemValue
            '.Email = txt_Director_Email.Text
            '.Email2 = txt_Director_Email2.Text
            '.Email3 = txt_Director_Email3.Text
            '.Email4 = txt_Director_Email4.Text
            '.Email5 = txt_Director_Email5.Text
            .Occupation = txt_Director_Occupation.Text
            .Employer_Name = txt_Director_employer_name.Text
            .Deceased = Convert.ToBoolean(IIf(cb_Director_Deceased.Checked = True, 1, 0))
            If cb_Director_Deceased.Checked Then
                .Deceased_Date = Convert.ToDateTime(txt_Director_Deceased_Date.RawValue)
            Else
                .Deceased_Date = Nothing
            End If
            .Tax_Number = txt_Director_Tax_Number.Text
            .Tax_Reg_Number = Convert.ToBoolean(IIf(cb_Director_Tax_Reg_Number.Checked = True, 1, 0))
            .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
            .Comments = txt_Director_Comments.Text
        End With

        objListgoAML_Ref_Director.Add(objNewgoAML_Customer)
        For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
            item.FK_To_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
            item.FK_for_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
            item.FK_To_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
            item.FK_for_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.Vw_Director_Addresses In objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
            item.FK_To_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
            item.FK_Person_ID = intpk
        Next

        For Each item As GoAMLDAL.Vw_Person_Identification In objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
            item.FK_Person_ID = intpk
        Next

        For Each item As GoAMLDAL.Vw_Director_Phones In objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
            item.FK_for_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.Vw_Director_Employer_Addresses In objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
            item.FK_To_Table_ID = intpk
        Next

        For Each item As GoAMLDAL.Vw_Director_Employer_Phones In objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
            item.FK_for_Table_ID = intpk
        Next

        'add by septian, goAML 5.0.1 Enhancement, 2023-04-26
        objTempgoAML_Ref_Director2 = goAML_Ref_Customer_Entity_Director_Mapper.Serialize(objNewgoAML_Customer)
        With objTempgoAML_Ref_Director2
            objTempgoAML_Ref_Director2.ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = objNewgoAML_Customer.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = objNewgoAML_Customer.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = objNewgoAML_Customer.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = objNewgoAML_Customer.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = objNewgoAML_Customer.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(intpk.ToString(), GridEmail_Director.objListgoAML_Ref)
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(intpk.ToString(), GridSanction_Director.objListgoAML_Ref)
            objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(intpk, GridPEP_Director.objListgoAML_Ref)
        End With
        If objListgoAML_Ref_Director2 Is Nothing Then
            objListgoAML_Ref_Director2 = New List(Of DataModel.goAML_Ref_Entity_Director)
            objListgoAML_Ref_Director2.Add(objTempgoAML_Ref_Director2)
        Else
            objListgoAML_Ref_Director2.Add(objTempgoAML_Ref_Director2)
        End If
        'end add

        Store_Director_Corp.DataSource = objListgoAML_vw_Customer_Entity_Director.ToList()
        Store_Director_Corp.DataBind()

        FP_Director.Hidden = True
        WindowDetailDirector.Hidden = True
        ClearinputCustomerDirector()
    End Sub

    Sub SaveAddIdentificationDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Person_Identification
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Person_Identification
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Person_Identification_ID = intpk
            .isReportingPerson = 0
            .FK_Person_Type = 1
            .FK_Person_ID = obj_Customer.PK_Customer_ID
            .Number = txt_number_identification.Value
            .Issued_By = txt_issued_by.Value
            If txt_issued_date.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment.Value
            .Country = cmb_issued_country.SelectedItemText
            .Type_Document = cmb_Jenis_Dokumen.SelectedItemText
        End With

        With objNewgoAML_Customer
            .PK_Person_Identification_ID = intpk
            .isReportingPerson = 0
            .FK_Person_Type = 1
            .FK_Person_ID = obj_Customer.PK_Customer_ID
            .Number = txt_number_identification.Value
            .Issued_Country = cmb_issued_country.SelectedItemValue
            .Issued_By = txt_issued_by.Value
            If txt_issued_date.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment.Value
            .Issued_Country = cmb_issued_country.SelectedItemValue
            .Type = cmb_Jenis_Dokumen.SelectedItemValue

        End With

        objListgoAML_vw_Customer_Identification.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Identification.Add(objNewgoAML_Customer)

        Store_INDV_Identification.DataSource = objListgoAML_vw_Customer_Identification.ToList()
        Store_INDV_Identification.DataBind()

        FP_Identification_INDV.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputCustomerIdentification()
    End Sub

    Function IsDataCustomerPhoneValid() As Boolean
        If cb_phone_Contact_Type.SelectedItemValue = "" Then
            Throw New Exception("Contact Type is required")
        End If
        If cb_phone_Communication_Type.SelectedItemValue = "" Then
            Throw New Exception("Communication Type is required")
        End If
        If txt_phone_Country_prefix.Text.Length > 3 Then
            Throw New Exception("Max Country Prefix is 3")
        End If
        If txt_phone_extension.Text.Length > 9 Then
            Throw New Exception("Max Phone Extension is 9")
        End If
        If txt_phone_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(txt_phone_number.Text) Then
            Throw New Exception("Phone Number is Invalid")
        End If
        'End Update
        Return True
    End Function
    'Update: Zikri_08102020
    Private Function IsNumber(text As String) As Boolean
        Try

            Dim number As New Regex("^[0-9 ]+$")
            Return number.IsMatch(text)

        Catch ex As Exception
            Return False
        End Try
    End Function
    'End Update
    Function IsDataCustomerAddressValid() As Boolean
        If cmb_kodenegara.SelectedItemValue = "" Then
            Throw New Exception("Country Code is required")
        End If
        If cmb_kategoriaddress.SelectedItemValue = "" Then
            Throw New Exception("Address Type is required")
        End If
        If Address_INDV.Text.Trim = "" Then
            Throw New Exception("Address is required")
        End If
        If City_INDV.Text.Trim = "" Then
            Throw New Exception("City is required")
        End If
        Return True
    End Function

    Function IsDataCustomerDirectorValid() As Boolean
        'If cmb_director.RawValue = "Select One" Then
        '    Throw New Exception("Please Select Person")
        'End If
        If cmb_peran.SelectedItemValue = "" Then
            Throw New Exception("Role is required")
        End If
        'If txt_Director_Occupation.Text = "" Then
        '    Throw New Exception("Occupation  is required")
        'End If
        'If txt_Director_Source_of_Wealth.Text = "" Then
        '    Throw New Exception("Source of Wealth is required")
        'End If
        If txt_Director_Last_Name.Text = "" Then
            Throw New Exception("Full Name is required")
        End If
        'If txt_Director_BirthDate.Text = "" Then
        '    Throw New Exception("Birth Date is required")
        'End If
        'If txt_Director_Birth_Place.Text = "" Then
        '    Throw New Exception("Birth Place is required")
        'End If
        'If cmb_Director_Gender.SelectedItemValue = "" Then
        '    Throw New Exception("Gender is required")
        'End If
        'Update: Zikri_08102020
        If txt_Director_SSN.Text.Trim IsNot "" Then
            If Not IsNumber(txt_Director_SSN.Text) Then
                Throw New Exception("NIK is Invalid")
            End If
            If txt_Director_SSN.Text.Trim.Length <> 16 Then
                Throw New Exception("NIK must contains 16 digit number")
            End If
        End If
        'End Update
        'If cmb_Director_Nationality1.SelectedItemValue = "" Then
        '    Throw New Exception("Nationality 1 is required")
        'End If
        'If cmb_Director_Residence.SelectedItemValue = "" Then
        '    Throw New Exception("Domicile Country is required")
        'End If
        If objListgoAML_Ref_Director_Phone.Count = 0 Then
            Throw New Exception("Phone Director is required")
        End If
        If objListgoAML_Ref_Director_Address.Count = 0 Then
            Throw New Exception("Address Director is required")
        End If
        'If Not String.IsNullOrEmpty(txt_Director_ID_Number.Text) Then
        '    If objListgoAML_Ref_Identification_Director.Count = 0 Then
        '        Throw New Exception("Identification is required")
        '    End If
        'End If
        'If objListgoAML_Ref_Director_Employer_Address.Count = 0 Then
        '    Throw New Exception("Employer Address Director is required")
        'End If
        'If objListgoAML_Ref_Director_Employer_Phone.Count = 0 Then
        '    Throw New Exception("Employer Phone Director is required")
        'End If
        Return True
    End Function

    Function IsDataCustomerIdentificationValid() As Boolean
        If cmb_issued_country.SelectedItemValue = "" Then
            Throw New Exception("Country is required")
        End If
        If cmb_Jenis_Dokumen.SelectedItemValue = "" Then
            Throw New Exception("Identification Type is required")
        End If
        If txt_number_identification.Text.Trim = "" Then
            Throw New Exception("Number Identification is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(txt_number_identification.Text) Then
            Throw New Exception("Identification Number is Invalid")
        End If
        'End Update
        Return True
    End Function

    Sub ClearinputCustomerPhones()
        cb_phone_Contact_Type.SetTextValue("")
        cb_phone_Communication_Type.SetTextValue("")
        txt_phone_Country_prefix.Text = ""
        txt_phone_number.Text = ""
        txt_phone_extension.Text = ""
        txt_phone_comments.Text = ""

    End Sub

    Sub ClearinputCustomerAddress()
        cmb_kategoriaddress.SetTextValue("")
        cmb_kodenegara.SetTextValue("")
        Address_INDV.Text = ""
        Town_INDV.Text = ""
        City_INDV.Text = ""
        Zip_INDV.Text = ""
        State_INDVD.Text = ""
        Comment_Address_INDVD.Text = ""
    End Sub

    Sub ClearinputCustomerDirector()
        'cmb_director.Value = Nothing
        cmb_peran.SetTextValue("")
        txt_Director_Title.Text = ""
        txt_Director_Last_Name.Text = ""
        txt_Director_SSN.Text = ""
        txt_Director_BirthDate.Text = ""
        txt_Director_Birth_Place.Text = ""
        txt_Director_Mothers_Name.Text = ""
        txt_Director_Alias.Text = ""
        txt_Director_Passport_Number.Text = ""
        'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
        cmb_Director_Passport_Country.SetTextValue("")
        'End Update
        txt_Director_ID_Number.Text = ""
        cmb_Director_Nationality1.SetTextValue("")
        cmb_Director_Nationality2.SetTextValue("")
        cmb_Director_Nationality3.SetTextValue("")
        cmb_Director_Gender.SetTextValue("")
        cmb_Director_Residence.SetTextValue("")
        'txt_Director_Email.Text = ""
        'txt_Director_Email2.Text = ""
        'txt_Director_Email3.Text = ""
        'txt_Director_Email4.Text = ""
        'txt_Director_Email5.Text = ""
        txt_Director_Occupation.Text = ""
        txt_Director_employer_name.Text = ""
        cb_Director_Deceased.Checked = False
        txt_Director_Deceased_Date.Value = Nothing
        txt_Director_Tax_Number.Text = ""
        cb_Director_Tax_Reg_Number.Checked = False
        txt_Director_Source_of_Wealth.Text = ""
        txt_Director_Comments.Text = ""
        Store_Director_Employer_Address.DataBind()
        Store_Director_Employer_Phone.DataBind()
        Store_DirectorAddress.DataBind()
        Store_DirectorPhone.DataBind()
        Store_Director_Identification.DataBind()
    End Sub


    Sub ClearinputCustomerIdentification()
        cmb_Jenis_Dokumen.SetTextValue("")
        txt_identification_comment.Text = ""
        txt_issued_by.Text = ""
        txt_issued_date.Text = ""
        txt_issued_expired_date.Text = ""
        cmb_issued_country.SetTextValue("")
        txt_number_identification.Text = ""
    End Sub

    Sub ClearinputDirectorIdentification()
        cmb_Jenis_Dokumen_Director.SetTextValue("")
        txt_identification_comment_Director.Text = ""
        txt_issued_by_Director.Text = ""
        txt_issued_date_Director.Text = ""
        txt_issued_expired_date_Director.Text = ""
        cmb_issued_country_Director.SetTextValue("")
        txt_number_identification_Director.Text = ""
    End Sub

    Private Sub LoadGender()
        'cmb_INDV_Gender.PageSize = SystemParameterBLL.GetPageSize
        'StoreINDVGender.Reload()
        'cmb_Director_Gender.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Gender.Reload()
    End Sub

    Protected Sub Gender_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Kelamin", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub LoadNegara()

        'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
        'cmb_INDV_Passport_country.PageSize = SystemParameterBLL.GetPageSize
        'Store_INDV_PassportCountry.Reload()
        'cmb_Director_Passport_Country.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_PassportCountry.Reload()
        'End Update
        'cmb_Director_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Nationality1.Reload()
        'cmb_INDV_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'StoreNationality1.Reload()
        'cmb_Director_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Nationality2.Reload()
        'cmb_INDV_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        'StoreNationality2.Reload()
        'cmb_Director_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Nationality3.Reload()
        'cmb_INDV_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'StoreNationality3.Reload()
        'cmb_Director_Residence.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Residence.Reload()
        'cmb_INDV_Residence.PageSize = SystemParameterBLL.GetPageSize
        'StoreResidence.Reload()
        'Director_cmb_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_Kode_Negara.Reload()
        'Director_cmb_emp_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_emp_Kode_Negara.Reload()

        'cmb_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'Store_Kode_Negara.Reload()
        'cmb_emp_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'Store_emp_Kode_Negara.Reload()

        'cmb_issued_country_Director.PageSize = SystemParameterBLL.GetPageSize
        'StoreIssueCountry_Director.Reload()
        'cmb_issued_country.PageSize = SystemParameterBLL.GetPageSize
        'Store_issued_country.Reload()


    End Sub

    Protected Sub Communication_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = " Keterangan  like '%" & query & "%'"
            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Alat_Komunikasi", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub Contact_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = " Keterangan  like '%" & query & "%'"
            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Kategori_Kontak", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub National_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Nama_Negara", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub LoadKontak()
        'cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        'Store_Contact_Type.Reload()
        'Emp_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        'Emp_Store_Contact_Type.Reload()
        'Director_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_Contact_Type.Reload()
        'Director_Emp_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        'Director_Emp_Store_Contact_Type.Reload()
    End Sub

    Private Sub LoadJenisAlatKomunikasi()
        'Director_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_Communication_Type.Reload()
        'Director_Emp_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        'Director_Emp_Store_Communication_Type.Reload()
        'cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        'Store_Communication_Type.Reload()
        'Emp_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        'Emp_Store_Communication_Type.Reload()
    End Sub

    Private Sub LoadKategoriKontak()
        'cmb_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        'Store_KategoriAddress.Reload()
        'cmb_emp_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        'Store_emp_KategoriAddress.Reload()
        'Director_cmb_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_KategoriAddress.Reload()
        'Director_cmb_emp_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_emp_KategoriAddress.Reload()
    End Sub

    Private Sub LoadTypeIdentitas()
        'cmb_Jenis_Dokumen.PageSize = SystemParameterBLL.GetPageSize
        'Store_jenis_dokumen.Reload()

        'cmb_Jenis_Dokumen_Director.PageSize = SystemParameterBLL.GetPageSize
        'Storejenisdokumen_Director.Reload()
    End Sub

    Protected Sub Identification_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = " Keterangan  like '%" & query & "%'"
            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goaml_ref_jenis_dokumen_identitas", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub


    Private Sub CUSTOMER_EDIT_v501_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then

                'Dedy added 31082020 aktif is hidden
                Cmb_StatusCode.Hidden = True
                LoadGender()
                LoadKontak()
                LoadJenisAlatKomunikasi()
                LoadKategoriKontak()
                LoadTypeIdentitas()
                LoadNegara()
                'Dedy end added 31082020

                ClearSession()
                GridPanelSetting()
                Maximizeable()
                Dim strid As String = GetId()
                Dim strModuleid As String = Request.Params("ModuleID")
                Dim INDV_gender As GoAMLDAL.goAML_Ref_Jenis_Kelamin = Nothing
                Dim INDV_statusCode As GoAMLDAL.goAML_Ref_Status_Rekening = Nothing
                'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
                Dim INDV_PassportCountry As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
                'End Update
                Dim INDV_nationality1 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
                Dim INDV_nationality2 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
                Dim INDV_nationality3 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
                Dim INDV_residence As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
                Dim CORP_Incorporation_legal_form As GoAMLDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
                Dim CORP_incorporation_country_code As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
                Dim CORP_Role As GoAMLDAL.goAML_Ref_Party_Role = Nothing

                Try
                    obj_Customer = goAML_CustomerBLL.GetCustomerbyCIF(strid)
                    'Add by Septian, goAML 5.0.1
                    obj_Customer2 = goAML_CustomerBLL.GetCustomerbyCIF2(strid)
                    'End add
                    'Dedy added 01092020
                    If obj_Customer IsNot Nothing Then
                        PK_Customer.Value = obj_Customer.PK_Customer_ID
                        If obj_Customer.isUpdateFromDataSource = True Then
                            UpdateFromDataSource.Checked = True
                        End If
                        'dedy added 25082020
                        If obj_Customer.isGCNPrimary = True Then
                            GCNPrimary.Checked = True
                        End If
                        txt_GCN.Text = obj_Customer.GCN
                        'dedy end added 25082020

                        If obj_Customer.isGCNPrimary = True Then
                            txt_GCN.ReadOnly = True
                            GCNPrimary.ReadOnly = True
                        End If

                        txt_CIF.Text = obj_Customer.CIF
                        If obj_Customer.Closing_Date IsNot Nothing Then
                            TextFieldClosingDate.Text = obj_Customer.Closing_Date.Value.ToString("dd-MMM-yyyy")
                        End If
                        If obj_Customer.Opening_Date IsNot Nothing Then
                            TextFieldOpeningDate.Text = obj_Customer.Opening_Date.Value.ToString("dd-MMM-yyyy")
                        End If
                        If obj_Customer.Comments IsNot Nothing Then
                            TextFieldComments.Text = obj_Customer.Comments
                        End If
                        Using objdbs As New GoAMLDAL.GoAMLEntities
                            If obj_Customer.Opening_Branch_Code IsNot Nothing And Not String.IsNullOrEmpty(obj_Customer.Opening_Branch_Code) Then
                                Dim objbranch = objdbs.AML_BRANCH.Where(Function(x) x.FK_AML_BRANCH_CODE = obj_Customer.Opening_Branch_Code).FirstOrDefault
                                If Not objbranch Is Nothing Then
                                    cmb_openingBranch.SetTextWithTextValue(objbranch.FK_AML_BRANCH_CODE, objbranch.BRANCH_NAME)
                                End If
                            End If

                            If obj_Customer.Status IsNot Nothing And Not String.IsNullOrEmpty(obj_Customer.Status) Then
                                Dim objstatus = objdbs.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = obj_Customer.Status).FirstOrDefault
                                If Not objstatus Is Nothing Then
                                    cmb_Status.SetTextWithTextValue(objstatus.Kode, objstatus.Keterangan)
                                End If
                            End If

                        End Using
                        StoreStatusCode.DataSource = goAML_CustomerBLL.GetListStatusRekening()
                        StoreStatusCode.DataBind()
                        Dim objrekeningstatus = goAML_CustomerBLL.GetListStatusRekening().Where(Function(x) x.Kode = obj_Customer.Status_Code).FirstOrDefault()
                        If objrekeningstatus IsNot Nothing Then
                            Cmb_StatusCode.SetValue(objrekeningstatus.Kode)
                        End If

                        If obj_Customer.FK_Customer_Type_ID = 1 Then
                            txt_INDV_Title.Text = obj_Customer.INDV_Title
                            INDV_gender = goAML_CustomerBLL.GetJenisKelamin(obj_Customer.INDV_Gender)


                            txt_INDV_Last_Name.Text = obj_Customer.INDV_Last_Name
                            If obj_Customer.INDV_BirthDate.HasValue Then
                                txt_INDV_Birthdate.Text = obj_Customer.INDV_BirthDate.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                            Else
                                txt_INDV_Birthdate.Text = ""
                            End If

                            'StoreINDVGender.DataSource = goAML_CustomerBLL.GetListJenisKelamin()
                            'StoreINDVGender.DataBind()

                            Dim obj_gender = goAML_CustomerBLL.GetJenisKelamin(obj_Customer.INDV_Gender)
                            If obj_gender IsNot Nothing Then
                                cmb_INDV_Gender.SetTextWithTextValue(obj_gender.Kode, obj_gender.Keterangan)
                            End If

                            txt_INDV_Birth_place.Text = obj_Customer.INDV_Birth_Place

                            txt_INDV_Mothers_name.Text = obj_Customer.INDV_Mothers_Name
                            txt_INDV_Alias.Text = obj_Customer.INDV_Alias
                            txt_INDV_SSN.Text = obj_Customer.INDV_SSN
                            txt_INDV_Passport_number.Text = obj_Customer.INDV_Passport_Number

                            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
                            'Store_INDV_PassportCountry.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                            ' Store_INDV_PassportCountry.DataBind()

                            Dim obj_passportcountry = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Passport_Country)
                            If obj_passportcountry IsNot Nothing Then
                                cmb_INDV_Passport_country.SetTextWithTextValue(obj_passportcountry.Kode, obj_passportcountry.Keterangan)
                                'cmb_INDV_Nationality1.DoQuery(obj_passportcountry.Keterangan, True)
                                'cmb_INDV_Nationality1.SetValueAndFireSelect(obj_Customer.INDV_Passport_Country)
                            End If
                            'End Update

                            txt_INDV_ID_Number.Text = obj_Customer.INDV_ID_Number

                            'StoreNationality1.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                            'StoreNationality1.DataBind()

                            Dim obj_nationality1 = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Nationality1)
                            If obj_nationality1 IsNot Nothing Then
                                cmb_INDV_Nationality1.SetTextWithTextValue(obj_nationality1.Kode, obj_nationality1.Keterangan)
                                'cmb_INDV_Nationality1.DoQuery(INDV_nationality1.Kode, True)
                                'cmb_INDV_Nationality1.SetValueAndFireSelect(INDV_nationality1.Kode)
                            End If

                            'StoreNationality2.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                            'StoreNationality2.DataBind()
                            Dim obj_nationality2 = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Nationality2)
                            If obj_nationality2 IsNot Nothing Then
                                cmb_INDV_Nationality2.SetTextWithTextValue(obj_nationality2.Kode, obj_nationality2.Keterangan)
                                'cmb_INDV_Nationality2.DoQuery(obj_Customer.INDV_Nationality2, True)
                                'cmb_INDV_Nationality2.SetValueAndFireSelect(obj_Customer.INDV_Nationality2)
                            End If

                            'StoreNationality3.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                            'StoreNationality3.DataBind()

                            Dim obj_nationality3 = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Nationality3)
                            If obj_nationality3 IsNot Nothing Then
                                cmb_INDV_Nationality3.SetTextWithTextValue(obj_nationality3.Kode, obj_nationality3.Keterangan)
                                'cmb_INDV_Nationality3.DoQuery(obj_Customer.INDV_Nationality3, True)
                                'cmb_INDV_Nationality3.SetValueAndFireSelect(obj_Customer.INDV_Nationality3)
                            End If

                            'StoreResidence.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                            'StoreResidence.DataBind()

                            Dim obj_residence = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Residence)
                            If obj_residence IsNot Nothing Then
                                cmb_INDV_Residence.SetTextWithTextValue(obj_residence.Kode, obj_residence.Keterangan)
                            End If

                            'comment by septian, goAMl 5.0.1
                            'uncomment by Nael, goAML 5.2
                            txt_INDV_Email.Text = obj_Customer.INDV_Email
                            txt_INDV_Email2.Text = obj_Customer.INDV_Email2
                            txt_INDV_Email3.Text = obj_Customer.INDV_Email3
                            txt_INDV_Email4.Text = obj_Customer.INDV_Email4
                            txt_INDV_Email5.Text = obj_Customer.INDV_Email5
                            'end comment
                            txt_INDV_Occupation.Text = obj_Customer.INDV_Occupation
                            txt_INDV_Employer_Name.Text = obj_Customer.INDV_Employer_Name
                            txt_INDV_Tax_Number.Text = obj_Customer.INDV_Tax_Number
                            txt_INDV_Source_of_Wealth.Text = obj_Customer.INDV_Source_of_Wealth

                            If obj_Customer.INDV_Deceased = True Then
                                cb_Deceased.Checked = True
                                txt_deceased_date.Hidden = False
                                txt_deceased_date.Text = obj_Customer.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                            End If
                            If obj_Customer.INDV_Tax_Reg_Number = True Then
                                cb_tax.Checked = True
                            End If
                            txt_INDV_Comments.Text = obj_Customer.INDV_Comments

                            'add by Septian, goAML 5.0.1
                            'Dim qNamaNegara = "SELECT TOP 1 * FROM goAML_Ref_Nama_Negara WHERE Kode = '" + obj_Customer2.Country_Of_Birth + "'"
                            'Dim refNamaNegara As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, qNamaNegara)

                            'If refNamaNegara IsNot Nothing Then
                            '    Dim refNamaNegara_Kode = refNamaNegara("Kode").ToString()
                            '    Dim refNamaNegara_Keterangan = refNamaNegara("Keterangan").ToString()

                            '    cmb_INDV_Country_Of_Birth.SetTextWithTextValue(refNamaNegara_Kode, refNamaNegara_Keterangan)
                            'End If

                            'txt_INDV_Foreign_Name.Value = obj_Customer2.Full_Name_Frn

                            'If obj_Customer2.Residence_Since IsNot Nothing Then
                            '    dte_INDV_ResidenceSince.Text = obj_Customer2.Residence_Since.Value.ToString("dd-MMM-yyyy")
                            'End If

                            If obj_Customer2.Is_Protected = True Then
                                cb_IsProtected.Checked = True
                            End If
                            'end add

                            '' Phone
                            objListgoAML_vw_Customer_Phones = GoAMLBLL.goAML_CustomerBLL.GetVWCustomerPhones(obj_Customer.PK_Customer_ID)
                            objListgoAML_Ref_Phone = GoAMLBLL.goAML_CustomerBLL.GetCustomerPhones(obj_Customer.PK_Customer_ID)
                            Store_INDV_Phones.DataSource = objListgoAML_vw_Customer_Phones.ToList()
                            Store_INDV_Phones.DataBind()

                            '' Address
                            objListgoAML_vw_Customer_Addresses = GoAMLBLL.goAML_CustomerBLL.GetVWCustomerAddresses(obj_Customer.PK_Customer_ID)
                            objListgoAML_Ref_Address = GoAMLBLL.goAML_CustomerBLL.GetCustomerAddresses(obj_Customer.PK_Customer_ID)
                            Store_INDV_Addresses.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
                            Store_INDV_Addresses.DataBind()

                            '' Identification
                            objListgoAML_vw_Customer_Identification = GoAMLBLL.goAML_CustomerBLL.GetVWCustomerIdentifications(obj_Customer.PK_Customer_ID)
                            objListgoAML_Ref_Identification = GoAMLBLL.goAML_CustomerBLL.GetCustomerIdentifications(obj_Customer.PK_Customer_ID)
                            Store_INDV_Identification.DataSource = objListgoAML_vw_Customer_Identification.ToList()
                            Store_INDV_Identification.DataBind()

                            '' Employer Phone
                            objListgoAML_vw_Employer_Phones = GoAMLBLL.goAML_CustomerBLL.GetVWEmployerPhones(obj_Customer.PK_Customer_ID)
                            objListgoAML_Ref_Employer_Phone = GoAMLBLL.goAML_CustomerBLL.GetEmployerPhones(obj_Customer.PK_Customer_ID)
                            Store_Empoloyer_Phone.DataSource = objListgoAML_vw_Employer_Phones.ToList()
                            Store_Empoloyer_Phone.DataBind()

                            '' Employer Address
                            objListgoAML_vw_Employer_Addresses = GoAMLBLL.goAML_CustomerBLL.GetVWEmployerAddresses(obj_Customer.PK_Customer_ID)
                            objListgoAML_Ref_Employer_Address = GoAMLBLL.goAML_CustomerBLL.GetEmployerAddresses(obj_Customer.PK_Customer_ID)
                            Store_Employer_Address.DataSource = objListgoAML_vw_Employer_Addresses.ToList()
                            Store_Employer_Address.DataBind()

                            ''add by Septian, goAML 5.0.1, 25 Jan 2023
                            '' Previous Name
                            objListgoAML_vw_Previous_Name = GoAMLBLL.goAML_CustomerBLL.GetPreviousName(obj_Customer.CIF)
                            objListgoAML_Ref_Previous_Name = GoAMLBLL.goAML_CustomerBLL.GetPreviousName(obj_Customer.CIF)
                            Store_Customer_Previous_Name.DataSource = objListgoAML_vw_Previous_Name.ToList()
                            Store_Customer_Previous_Name.DataBind()

                            '' Email
                            objListgoAML_vw_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(obj_Customer.CIF)
                            objListgoAML_Ref_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(obj_Customer.CIF)
                            Store_Customer_Email.DataSource = objListgoAML_vw_Email.ToList()
                            Store_Customer_Email.DataBind()

                            '' Employment History
                            objListgoAML_vw_EmploymentHistory = GoAMLBLL.goAML_CustomerBLL.GetEmploymentHistory(obj_Customer.CIF)
                            objListgoAML_Ref_EmploymentHistory = GoAMLBLL.goAML_CustomerBLL.GetEmploymentHistory(obj_Customer.CIF)
                            Store_Customer_Employment_History.DataSource = objListgoAML_vw_EmploymentHistory.ToList()
                            Store_Customer_Employment_History.DataBind()

                            '' Person PEP
                            objListgoAML_vw_PersonPEP = GoAMLBLL.goAML_CustomerBLL.GetPEP(obj_Customer.CIF)
                            objListgoAML_Ref_PersonPEP = GoAMLBLL.goAML_CustomerBLL.GetPEP(obj_Customer.CIF)
                            Store_Customer_Person_PEP.DataSource = objListgoAML_vw_PersonPEP.ToList()
                            Store_Customer_Person_PEP.DataBind()

                            '' Network Device
                            objListgoAML_vw_NetworkDevice = GoAMLBLL.goAML_CustomerBLL.GetNetworkDevice(obj_Customer.CIF)
                            objListgoAML_Ref_NetworkDevice = GoAMLBLL.goAML_CustomerBLL.GetNetworkDevice(obj_Customer.CIF)
                            Store_Customer_Network_Device.DataSource = objListgoAML_vw_NetworkDevice.ToList()
                            Store_Customer_Network_Device.DataBind()

                            '' Social Media
                            objListgoAML_vw_SocialMedia = GoAMLBLL.goAML_CustomerBLL.GetSocialMedia(obj_Customer.CIF)
                            objListgoAML_Ref_SocialMedia = GoAMLBLL.goAML_CustomerBLL.GetSocialMedia(obj_Customer.CIF)
                            Store_Customer_Social_Media.DataSource = objListgoAML_vw_SocialMedia.ToList()
                            Store_Customer_Social_Media.DataBind()

                            '' Sanction
                            objListgoAML_vw_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(obj_Customer.CIF)
                            objListgoAML_Ref_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(obj_Customer.CIF)
                            Store_Customer_Sanction.DataSource = objListgoAML_vw_Sanction.ToList()
                            Store_Customer_Sanction.DataBind()

                            '' Related Person
                            objListgoAML_vw_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(obj_Customer.CIF)
                            ' 2023-10-04, Fixing Defect No 230 data related person tidak terload ke session saat menu edit dibuka
                            objListgoAML_Ref_RelatedPerson = objListgoAML_vw_RelatedPerson
                            Store_Customer_RelatedPerson.DataSource = objListgoAML_vw_RelatedPerson.ToList()
                            Store_Customer_RelatedPerson.DataBind()

                            '' Additional Information
                            objListgoAML_vw_AdditionalInfo = GoAMLBLL.goAML_CustomerBLL.GetAdditionalInformation(obj_Customer.CIF)
                            objListgoAML_Ref_AdditionalInfo = GoAMLBLL.goAML_CustomerBLL.GetAdditionalInformation(obj_Customer.CIF)
                            Store_Customer_Additional_Info.DataSource = objListgoAML_vw_AdditionalInfo.ToList()
                            Store_Customer_Additional_Info.DataBind()
                            '' End add

                            panel_Corp.Visible = False
                            'panel_INDV.Visible = False
                        ElseIf obj_Customer.FK_Customer_Type_ID = 2 Then
                            txt_Corp_Name.Text = obj_Customer.Corp_Name
                            txt_Corp_Commercial_Name.Text = obj_Customer.Corp_Commercial_Name

                            'Store_Corp_Incorporation_Legal_Form.DataSource = goAML_CustomerBLL.GetListBentukBadanUsaha()
                            'Store_Corp_Incorporation_Legal_Form.DataBind()

                            CORP_Incorporation_legal_form = goAML_CustomerBLL.GetBentukBadanUsahaByID(obj_Customer.Corp_Incorporation_Legal_Form)
                            If CORP_Incorporation_legal_form IsNot Nothing Then
                                cmb_Corp_Incorporation_Legal_Form.SetTextWithTextValue(CORP_Incorporation_legal_form.Kode, CORP_Incorporation_legal_form.Keterangan)
                            End If

                            'Store_Corp_Role.DataSource = goAML_CustomerBLL.GetListPartyRole()
                            'Store_Corp_Role.DataBind()
                            'CORP_Role = goAML_CustomerBLL.GetPartyRolebyID(obj_Customer.Corp_Role)
                            'cmb_corp_role.SetValue(CORP_Role.Kode)
                            If obj_Customer.Corp_Incorporation_Date.HasValue Then
                                txt_Corp_incorporation_date.Text = obj_Customer.Corp_Incorporation_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                            Else
                                txt_Corp_incorporation_date.Text = ""
                            End If

                            txt_Corp_Incorporation_number.Text = obj_Customer.Corp_Incorporation_Number
                            txt_Corp_Business.Text = obj_Customer.Corp_Business
                            'comment by septian, goAML 5.0.1
                            txt_Corp_Email.Text = obj_Customer.Corp_Email
                            txt_Corp_url.Text = obj_Customer.Corp_Url
                            'end comment
                            txt_Corp_incorporation_state.Text = obj_Customer.Corp_Incorporation_State

                            'Store_Corp_Incorporation_Country_Code.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                            'Store_Corp_Incorporation_Country_Code.DataBind()

                            CORP_incorporation_country_code = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.Corp_Incorporation_Country_Code)
                            'cmb_Corp_Incorporation_Country_Code.SetValue(CORP_incorporation_country_code.Kode)
                            'If (cmb_Corp_Incorporation_Country_Code IsNot Nothing) Then
                            If (CORP_incorporation_country_code IsNot Nothing) Then
                                cmb_Corp_Incorporation_Country_Code.SetTextWithTextValue(CORP_incorporation_country_code.Kode, CORP_incorporation_country_code.Keterangan)
                            End If


                            If obj_Customer.Corp_Incorporation_Date.HasValue Then
                                txt_Corp_incorporation_date.Text = obj_Customer.Corp_Incorporation_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                            Else
                                txt_Corp_incorporation_date.Text = ""
                            End If
                            cbTutup.Value = obj_Customer.Corp_Business_Closed
                            If obj_Customer.Corp_Date_Business_Closed.HasValue Then
                                txt_Corp_date_business_closed.Text = obj_Customer.Corp_Date_Business_Closed.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                            Else
                                txt_Corp_date_business_closed.Text = ""
                            End If
                            txt_Corp_tax_number.Text = obj_Customer.Corp_Tax_Number
                            txt_Corp_Comments.Text = obj_Customer.Corp_Comments

                            'add by Septian, goAML 5.0.1, 2023-01-02
                            Dim qEntityStatus = "SELECT TOP 1 * FROM goAML_Ref_Entity_Status WHERE Kode = '" + obj_Customer2.Entity_Status + "'"
                            Dim refEntityStatus As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, qEntityStatus)

                            If refEntityStatus IsNot Nothing Then
                                Dim refEntityStatus_Kode = refEntityStatus("Kode").ToString()
                                Dim refEntityStatus_Keterangan = refEntityStatus("Keterangan").ToString()
                                'cmb_Corp_EntityStatus.SetTextWithTextValue(refEntityStatus_Kode, refEntityStatus_Keterangan)
                                'dte_Corp_EntityStatusDate.Text = obj_Customer2.Entity_Status_Date.Value.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                            End If
                            'end add

                            '' Phone
                            objListgoAML_vw_Customer_Phones = GoAMLBLL.goAML_CustomerBLL.GetVWCustomerPhones(obj_Customer.PK_Customer_ID)
                            objListgoAML_Ref_Phone = GoAMLBLL.goAML_CustomerBLL.GetCustomerPhones(obj_Customer.PK_Customer_ID)
                            Store_Phone_Corp.DataSource = objListgoAML_vw_Customer_Phones.ToList()
                            Store_Phone_Corp.DataBind()

                            '' Address
                            objListgoAML_vw_Customer_Addresses = GoAMLBLL.goAML_CustomerBLL.GetVWCustomerAddresses(obj_Customer.PK_Customer_ID)
                            objListgoAML_Ref_Address = GoAMLBLL.goAML_CustomerBLL.GetCustomerAddresses(obj_Customer.PK_Customer_ID)
                            Store_Address_Corp.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
                            Store_Address_Corp.DataBind()

                            '' Director
                            objListgoAML_vw_Customer_Entity_Director = GoAMLBLL.goAML_CustomerBLL.GetVWCustomerDirector(obj_Customer.CIF)
                            objListgoAML_Ref_Director = GoAMLBLL.goAML_CustomerBLL.GetCustomerDirector(obj_Customer.CIF)
                            Store_Director_Corp.DataSource = objListgoAML_vw_Customer_Entity_Director.ToList()
                            Store_Director_Corp.DataBind()

                            '' Employer Address
                            objListgoAML_vw_Employer_Addresses = GoAMLBLL.goAML_CustomerBLL.GetVWEmployerAddresses(obj_Customer.PK_Customer_ID)
                            objListgoAML_Ref_Employer_Address = GoAMLBLL.goAML_CustomerBLL.GetEmployerAddresses(obj_Customer.PK_Customer_ID)
                            Store_Employer_Address.DataSource = objListgoAML_vw_Employer_Addresses.ToList()
                            Store_Employer_Address.DataBind()

                            ''Add By Septian, goAML v5.0.1, 25 January 2023
                            '' Corp Email
                            objListgoAML_vw_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(obj_Customer.CIF)
                            objListgoAML_Ref_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(obj_Customer.CIF)
                            Store_corp_email.DataSource = objListgoAML_vw_Email.ToList()
                            Store_corp_email.DataBind()

                            '' Corp URL
                            objListgoAML_vw_EntityUrl = GoAMLBLL.goAML_CustomerBLL.GetURL(obj_Customer.CIF)
                            objListgoAML_Ref_EntityUrl = GoAMLBLL.goAML_CustomerBLL.GetURL(obj_Customer.CIF)
                            Store_Customer_Entity_URL.DataSource = objListgoAML_vw_EntityUrl.ToList()
                            Store_Customer_Entity_URL.DataBind()

                            '' Corp Entity Identification
                            objListgoAML_vw_EntityIdentification = GoAMLBLL.goAML_CustomerBLL.GetEntityIdentification(obj_Customer.CIF)
                            objListgoAML_Ref_EntityIdentification = GoAMLBLL.goAML_CustomerBLL.GetEntityIdentification(obj_Customer.CIF)
                            Store_Customer_Entity_Identification.DataSource = objListgoAML_vw_EntityIdentification.ToList()
                            Store_Customer_Entity_Identification.DataBind()

                            '' Corp Network Device
                            objListgoAML_vw_NetworkDevice = GoAMLBLL.goAML_CustomerBLL.GetNetworkDevice(obj_Customer.CIF)
                            objListgoAML_Ref_NetworkDevice = GoAMLBLL.goAML_CustomerBLL.GetNetworkDevice(obj_Customer.CIF)
                            StoreCorp_Network_Device.DataSource = objListgoAML_vw_NetworkDevice.ToList()
                            StoreCorp_Network_Device.DataBind()

                            '' Corp Sanction
                            objListgoAML_vw_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(obj_Customer.CIF)
                            objListgoAML_Ref_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(obj_Customer.CIF)
                            Store_corp_sanction.DataSource = objListgoAML_vw_Sanction.ToList()
                            Store_corp_sanction.DataBind()

                            '' Corp Related Person
                            objListgoAML_vw_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(obj_Customer.CIF)
                            objListgoAML_Ref_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(obj_Customer.CIF)
                            'StoreCorp_Related_person.DataSource = objListgoAML_vw_RelatedPerson.ToList()
                            'StoreCorp_Related_person.DataBind()

                            '' Corp Related Entities
                            objListgoAML_vw_RelatedEntities = GoAMLBLL.goAML_CustomerBLL.GetRelatedEntities(obj_Customer.CIF)
                            objListgoAML_Ref_RelatedEntities = GoAMLBLL.goAML_CustomerBLL.GetRelatedEntities(obj_Customer.CIF)
                            Store_Related_Entities.DataSource = objListgoAML_vw_RelatedEntities.ToList()
                            Store_Related_Entities.DataBind()

                            '' Corp Additonal Information
                            objListgoAML_vw_AdditionalInfo = GoAMLBLL.goAML_CustomerBLL.GetAdditionalInformation(obj_Customer.CIF)
                            objListgoAML_Ref_AdditionalInfo = GoAMLBLL.goAML_CustomerBLL.GetAdditionalInformation(obj_Customer.CIF)
                            Store_Customer_Corp_Additional_Info.DataSource = objListgoAML_vw_AdditionalInfo.ToList()
                            Store_Customer_Corp_Additional_Info.DataBind()
                            '' End Add

                            If objListgoAML_Ref_Director.Count > 0 Then
                                For Each item As goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                                    Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                    objTempgoAML_Ref_Director2 = New DataModel.goAML_Ref_Entity_Director()
                                    With objTempgoAML_Ref_Director2
                                        objTempgoAML_Ref_Director2.PK_goAML_Ref_Customer_Entity_Director_ID = pk_director
                                        objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_Email = goAML_Customer_Service.GetEmailByPkFk(pk_director, 6)
                                        objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_Sanction = goAML_Customer_Service.GetSanctionByPkFk(pk_director, 6)
                                        objTempgoAML_Ref_Director2.ObjList_GoAML_Ref_Customer_PEP = goAML_Customer_Service.GetPEPByPkFk(pk_director, 6)
                                    End With
                                    If objListgoAML_Ref_Director2 Is Nothing Then
                                        objListgoAML_Ref_Director2 = New List(Of DataModel.goAML_Ref_Entity_Director)
                                    End If
                                    objListgoAML_Ref_Director2.Add(objTempgoAML_Ref_Director2)
                                Next
                            End If


                            For Each item As goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As goAML_Ref_Address In GoAMLBLL.goAML_CustomerBLL.GetDirectorAddresses(pk_director)
                                    objListgoAML_Ref_Director_Address.Add(itemx)
                                Next
                            Next

                            For Each item As goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As goAML_Ref_Phone In GoAMLBLL.goAML_CustomerBLL.GetDirectorPhone(pk_director)
                                    objListgoAML_Ref_Director_Phone.Add(itemx)
                                Next
                            Next

                            For Each item As goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As goAML_Person_Identification In GoAMLBLL.goAML_CustomerBLL.GetDirectorIdentifications(pk_director)
                                    objListgoAML_Ref_Identification_Director.Add(itemx)
                                Next
                            Next

                            For Each item As goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As Vw_Person_Identification In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorIdentifications(pk_director)
                                    objListgoAML_vw_Customer_Identification_Director.Add(itemx)
                                Next
                            Next

                            For Each item As goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As Vw_Director_Addresses In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorAddresses(pk_director)
                                    objListgoAML_vw_Director_Addresses.Add(itemx)
                                Next
                            Next

                            For Each item As goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As Vw_Director_Phones In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorPhones(pk_director)
                                    objListgoAML_vw_Director_Phones.Add(itemx)
                                Next
                            Next

                            For Each item As goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As goAML_Ref_Address In GoAMLBLL.goAML_CustomerBLL.GetDirectorEmployerAddresses(pk_director)
                                    objListgoAML_Ref_Director_Employer_Address.Add(itemx)
                                Next
                            Next

                            For Each item As goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As goAML_Ref_Phone In GoAMLBLL.goAML_CustomerBLL.GetDirectorEmployerPhones(pk_director)
                                    objListgoAML_Ref_Director_Employer_Phone.Add(itemx)
                                Next
                            Next

                            For Each item As goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As Vw_Director_Employer_Addresses In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorEmployerAddresses(pk_director)
                                    objListgoAML_vw_Director_Employer_Addresses.Add(itemx)
                                Next
                            Next

                            For Each item As goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As Vw_Director_Employer_Phones In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorEmployerPhones(pk_director)
                                    objListgoAML_vw_Director_Employer_Phones.Add(itemx)
                                Next
                            Next

                            panel_INDV.Visible = False

                        End If
                    End If

                Catch ex As Exception
                    Throw ex
                End Try

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub GridPanelSetting()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 2 Then
            GP_DirectorIdentification.ColumnModel.Columns.RemoveAt(GP_DirectorIdentification.ColumnModel.Columns.Count - 1)
            GP_DirectorIdentification.ColumnModel.Columns.Insert(1, CommandColumn13)
            GP_DirectorPhone.ColumnModel.Columns.RemoveAt(GP_DirectorPhone.ColumnModel.Columns.Count - 1)
            GP_DirectorPhone.ColumnModel.Columns.Insert(1, CommandColumn11)
            GP_DirectorAddress.ColumnModel.Columns.RemoveAt(GP_DirectorAddress.ColumnModel.Columns.Count - 1)
            GP_DirectorAddress.ColumnModel.Columns.Insert(1, CommandColumn12)
            GP_DirectorEmpPhone.ColumnModel.Columns.RemoveAt(GP_DirectorEmpPhone.ColumnModel.Columns.Count - 1)
            GP_DirectorEmpPhone.ColumnModel.Columns.Insert(1, CommandColumn9)
            GP_DirectorEmpAddress.ColumnModel.Columns.RemoveAt(GP_DirectorEmpAddress.ColumnModel.Columns.Count - 1)
            GP_DirectorEmpAddress.ColumnModel.Columns.Insert(1, CommandColumn10)
            gp_INDV_Phones.ColumnModel.Columns.RemoveAt(gp_INDV_Phones.ColumnModel.Columns.Count - 1)
            gp_INDV_Phones.ColumnModel.Columns.Insert(1, CommandColumn1)
            gp_INDV_Address.ColumnModel.Columns.RemoveAt(gp_INDV_Address.ColumnModel.Columns.Count - 1)
            gp_INDV_Address.ColumnModel.Columns.Insert(1, CommandColumn2)
            GP_INDV_Identification.ColumnModel.Columns.RemoveAt(GP_INDV_Identification.ColumnModel.Columns.Count - 1)
            GP_INDV_Identification.ColumnModel.Columns.Insert(1, CommandColumn5)
            GP_Empoloyer_Phone.ColumnModel.Columns.RemoveAt(GP_Empoloyer_Phone.ColumnModel.Columns.Count - 1)
            GP_Empoloyer_Phone.ColumnModel.Columns.Insert(1, CommandColumn6)
            GP_Employer_Address.ColumnModel.Columns.RemoveAt(GP_Employer_Address.ColumnModel.Columns.Count - 1)
            GP_Employer_Address.ColumnModel.Columns.Insert(1, CommandColumn7)
            GP_Phone_Corp.ColumnModel.Columns.RemoveAt(GP_Phone_Corp.ColumnModel.Columns.Count - 1)
            GP_Phone_Corp.ColumnModel.Columns.Insert(1, CommandColumn3)
            GP_Address_Corp.ColumnModel.Columns.RemoveAt(GP_Address_Corp.ColumnModel.Columns.Count - 1)
            GP_Address_Corp.ColumnModel.Columns.Insert(1, CommandColumn4)
            GP_Corp_Director.ColumnModel.Columns.RemoveAt(GP_Corp_Director.ColumnModel.Columns.Count - 1)
            GP_Corp_Director.ColumnModel.Columns.Insert(1, CommandColumn8)

            'Change CRUD Button position to left column, Septian, 4 Jan 23
            GPCustomer_INDV_PreviousName.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_PreviousName.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_PreviousName.ColumnModel.Columns.Insert(1, CC_INDV_PreviousName)
            GPCustomer_INDV_Email.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_Email.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_Email.ColumnModel.Columns.Insert(1, CC_INDV_Email)
            GPCustomer_INDV_EmploymentHistory.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_EmploymentHistory.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_EmploymentHistory.ColumnModel.Columns.Insert(1, CC_INDV_EmploymentHistory)
            GPCustomer_INDV_PEP.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_PEP.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_PEP.ColumnModel.Columns.Insert(1, CC_INDV_PEP)
            GPCustomer_INDV_NetworkDevice.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_NetworkDevice.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_NetworkDevice.ColumnModel.Columns.Insert(1, CC_INDV_NetworkDevice)
            GPCustomer_INDV_SocialMedia.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_SocialMedia.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_SocialMedia.ColumnModel.Columns.Insert(1, CC_INDV_SocialMedia)
            GPCustomer_INDV_Sanction.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_Sanction.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_Sanction.ColumnModel.Columns.Insert(1, CC_INDV_Sanction)
            GPCustomer_INDV_RelatedPerson.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_RelatedPerson.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_RelatedPerson.ColumnModel.Columns.Insert(1, CC_INDV_RelatedPerson)
            GPCustomer_INDV_AdditionalInformation.ColumnModel.Columns.RemoveAt(GPCustomer_INDV_AdditionalInformation.ColumnModel.Columns.Count - 1)
            GPCustomer_INDV_AdditionalInformation.ColumnModel.Columns.Insert(1, CC_INDV_AdditionalInformation)
            GPCustomer_CORP_Email.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_Email.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_Email.ColumnModel.Columns.Insert(1, CC_CORP_Email)
            GPCustomer_CORP_URL.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_URL.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_URL.ColumnModel.Columns.Insert(1, CC_CORP_URL)
            GPCustomer_CORP_EntityIdentification.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_EntityIdentification.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_EntityIdentification.ColumnModel.Columns.Insert(1, CC_CORP_EntityIdentification)
            GPCustomer_CORP_NetworkDevice.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_NetworkDevice.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_NetworkDevice.ColumnModel.Columns.Insert(1, CC_CORP_NetworkDevice)
            GPCustomer_CORP_Sanction.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_Sanction.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_Sanction.ColumnModel.Columns.Insert(1, CC_CORP_Sanction)
            'GPCustomer_CORP_RelatedPerson.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_RelatedPerson.ColumnModel.Columns.Count - 1)
            'GPCustomer_CORP_RelatedPerson.ColumnModel.Columns.Insert(1, CC_CORP_RelatedPerson)
            GPCustomer_CORP_RelatedEntities.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_RelatedEntities.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_RelatedEntities.ColumnModel.Columns.Insert(1, CC_CORP_RelatedEntities)
            GPCustomer_CORP_AdditionalInformation.ColumnModel.Columns.RemoveAt(GPCustomer_CORP_AdditionalInformation.ColumnModel.Columns.Count - 1)
            GPCustomer_CORP_AdditionalInformation.ColumnModel.Columns.Insert(1, CC_CORP_AdditionalInformation)
            'end add

        End If

    End Sub

    Sub Maximizeable()
        WindowDetailDirector.Maximizable = True
        WindowDetail.Maximizable = True
        WindowDetailAddress.Maximizable = True
        WindowDetailIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True

        'START: 2023-10-14, Nael
        WindowDetail_RelatedEntities.Maximizable = True
        WindowDetail_EntityIdentification.Maximizable = True
        WindowDetail_RelatedPerson.Maximizable = True
        WindowDetail_Person_PEP.Maximizable = True
        WindowDetail_Sanction.Maximizable = True
        WindowDetail_Email.Maximizable = True
        'END: 2023-10-14, Nael

    End Sub


    Protected Sub GrdCmdCustPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailCustPhone(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditCustPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailCustPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataAddressDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditCustAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailCustAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataEmployerAddressDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditEmployerAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailEmployerAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditCustDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailCustDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub GrdCmdCustIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataIdentificationDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditCustIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailCustIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataIdentificationDetail(id As Long)

        Dim objCustomerIdentificationDetail = objListgoAML_vw_Customer_Identification.Where(Function(X) X.PK_Person_Identification_ID = id).FirstOrDefault

        If Not objCustomerIdentificationDetail Is Nothing Then
            FP_Identification_INDV.Hidden = False
            WindowDetailIdentification.Hidden = False
            btn_save_identification.Hidden = True
            WindowDetailIdentification.Title = "Identification Detail"
            ClearinputCustomerIdentification()
            txt_number_identification.ReadOnly = True
            cmb_Jenis_Dokumen.IsReadOnly = True
            cmb_issued_country.IsReadOnly = True
            txt_issued_by.ReadOnly = True
            txt_issued_date.ReadOnly = True
            txt_issued_expired_date.ReadOnly = True
            txt_identification_comment.ReadOnly = True

            txt_number_identification.Text = objCustomerIdentificationDetail.Number
            'Store_jenis_dokumen.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            'Store_jenis_dokumen.DataBind()
            If Not objCustomerIdentificationDetail.Type_Document Is Nothing Then
                Dim dokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas.Where(Function(x) x.Keterangan = objCustomerIdentificationDetail.Type_Document).FirstOrDefault
                cmb_Jenis_Dokumen.SetTextWithTextValue(dokumen.Kode, dokumen.Keterangan)
            End If
            'Store_issued_country.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_issued_country.DataBind()
            Dim negara = goAML_CustomerBLL.GetNamaNegaraByKeterangan(objCustomerIdentificationDetail.Country)
            If Not negara Is Nothing Then
                cmb_issued_country.SetTextWithTextValue(negara.Kode, negara.Keterangan)
            End If
            txt_issued_by.Text = objCustomerIdentificationDetail.Issued_By
            txt_issued_date.Value = IIf(objCustomerIdentificationDetail.Issue_Date.HasValue, objCustomerIdentificationDetail.Issue_Date, Nothing) ' 2023-11-22, Nael
            txt_issued_expired_date.Value = IIf(objCustomerIdentificationDetail.Expiry_Date.HasValue, objCustomerIdentificationDetail.Expiry_Date, Nothing) ' 2023-11-22, Nael
            txt_identification_comment.Text = objCustomerIdentificationDetail.Identification_Comment
        End If
    End Sub

    Sub LoadDataDetailCustPhone(id As Long)
        Dim objCustomerPhonesDetail As goAML_Ref_Phone = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If objCustomerPhonesDetail IsNot Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            cb_phone_Contact_Type.IsReadOnly = True
            cb_phone_Communication_Type.IsReadOnly = True
            txt_phone_Country_prefix.ReadOnly = True
            txt_phone_number.ReadOnly = True
            txt_phone_extension.ReadOnly = True
            txt_phone_comments.ReadOnly = True
            btnSavedetail.Hidden = True

            WindowDetail.Title = "Phone Detail"
            ClearinputCustomerPhones()
            With objCustomerPhonesDetail
                'Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_Contact_Type.DataBind()
                'cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_contact_type IsNot Nothing Then
                    cb_phone_Contact_Type.SetTextWithTextValue(obj_contact_type.Kode, obj_contact_type.Keterangan)
                End If
                Dim obj_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_communication_type IsNot Nothing Then
                    cb_phone_Communication_Type.SetTextWithTextValue(obj_communication_type.Kode, obj_communication_type.Keterangan)
                End If
                'End Update

                'Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Store_Communication_Type.DataBind()
                'cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                txt_phone_Country_prefix.Text = .tph_country_prefix
                txt_phone_number.Text = .tph_number
                txt_phone_extension.Text = .tph_extension
                txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataDetailEmployerPhone(id As Long)
        Dim objCustomerPhonesDetail As goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelEmpTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            Emp_cb_phone_Contact_Type.IsReadOnly = True
            Emp_cb_phone_Communication_Type.IsReadOnly = True
            Emp_txt_phone_Country_prefix.ReadOnly = True
            Emp_txt_phone_number.ReadOnly = True
            Emp_txt_phone_extension.ReadOnly = True
            Emp_txt_phone_comments.ReadOnly = True
            btn_saveempphone.Hidden = True

            WindowDetail.Title = "Phone Detail"
            ClearinputCustomerEmpPhones()
            With objCustomerPhonesDetail
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_emp_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_emp_contact_type IsNot Nothing Then
                    Emp_cb_phone_Contact_Type.SetTextWithTextValue(obj_emp_contact_type.Kode, obj_emp_contact_type.Keterangan)
                End If

                'Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Emp_Store_Communication_Type.DataBind()
                'Emp_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                Dim obj_emp_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_emp_communication_type IsNot Nothing Then
                    Emp_cb_phone_Communication_Type.SetTextWithTextValue(obj_emp_communication_type.Kode, obj_emp_communication_type.Keterangan)
                End If
                'End Update
                'Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Emp_Store_Contact_Type.DataBind()
                'Emp_cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)

                'Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Emp_Store_Communication_Type.DataBind()
                'Emp_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Emp_txt_phone_number.Text = .tph_number
                Emp_txt_phone_extension.Text = .tph_extension
                Emp_txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataDirectorDetail(id As Long)

        Dim objCustomerDirectorDetail = objListgoAML_Ref_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id).FirstOrDefault()
        Dim objCustomerDirectorDetail2 = Nothing
        If objListgoAML_Ref_Director2 IsNot Nothing Then
            objCustomerDirectorDetail2 = objListgoAML_Ref_Director2.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id).FirstOrDefault()
        End If

        If objCustomerDirectorDetail IsNot Nothing Then

            GridEmail_Director.IsViewMode = True
            GridSanction_Director.IsViewMode = True
            GridPEP_Director.IsViewMode = True

            FP_Director.Hidden = False
            WindowDetailDirector.Hidden = False
            btnsaveDirector.Hidden = True
            WindowDetailDirector.Title = "Director Detail"
            cmb_peran.IsReadOnly = True
            txt_Director_Title.ReadOnly = True
            txt_Director_Last_Name.ReadOnly = True
            txt_Director_SSN.ReadOnly = True
            txt_Director_BirthDate.ReadOnly = True
            txt_Director_Birth_Place.ReadOnly = True
            txt_Director_Mothers_Name.ReadOnly = True
            txt_Director_Alias.ReadOnly = True
            txt_Director_ID_Number.ReadOnly = True
            txt_Director_Passport_Number.ReadOnly = True
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            cmb_Director_Passport_Country.IsReadOnly = True
            'End Update
            cmb_Director_Nationality1.IsReadOnly = True
            cmb_Director_Nationality2.IsReadOnly = True
            cmb_Director_Nationality3.IsReadOnly = True
            cmb_Director_Gender.IsReadOnly = True
            cmb_Director_Residence.IsReadOnly = True
            'txt_Director_Email.ReadOnly = True
            'txt_Director_Email2.ReadOnly = True
            'txt_Director_Email3.ReadOnly = True
            'txt_Director_Email4.ReadOnly = True
            'txt_Director_Email5.ReadOnly = True
            txt_Director_Occupation.ReadOnly = True
            txt_Director_employer_name.ReadOnly = True
            cb_Director_Deceased.ReadOnly = True
            txt_Director_Deceased_Date.ReadOnly = True
            txt_Director_Tax_Number.ReadOnly = True
            cb_Director_Tax_Reg_Number.ReadOnly = True
            txt_Director_Source_of_Wealth.ReadOnly = True
            txt_Director_Comments.ReadOnly = True
            'StorePeran.DataSource = goAML_CustomerBLL.GetListPeranDalamKorporasi()
            'StorePeran.DataBind()
            Dim peran = goAML_CustomerBLL.GetRoleDirectorByID(objCustomerDirectorDetail.Role)
            If peran IsNot Nothing Then
                cmb_peran.SetTextWithTextValue(peran.Kode, peran.Keterangan)
            End If
            txt_Director_Title.Text = objCustomerDirectorDetail.Title
            txt_Director_Last_Name.Text = objCustomerDirectorDetail.Last_Name
            txt_Director_SSN.Text = objCustomerDirectorDetail.SSN
            If objCustomerDirectorDetail.BirthDate IsNot Nothing Then
                txt_Director_BirthDate.Text = objCustomerDirectorDetail.BirthDate
            End If
            txt_Director_Birth_Place.Text = objCustomerDirectorDetail.Birth_Place
            txt_Director_Mothers_Name.Text = objCustomerDirectorDetail.Mothers_Name
            txt_Director_Alias.Text = objCustomerDirectorDetail.Alias
            txt_Director_ID_Number.Text = objCustomerDirectorDetail.ID_Number
            txt_Director_Passport_Number.Text = objCustomerDirectorDetail.Passport_Number

            'If objCustomerDirectorDetail.Passport_Country IsNot Nothing Then
            '    Store_Director_PassportCountry.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            '    Store_Director_PassportCountry.DataBind()
            '    Dim director_passportcountry = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Passport_Country)
            '    If director_passportcountry IsNot Nothing Then
            '        cmb_Director_Passport_Country.SetValue(director_passportcountry.Kode)
            '    End If
            'End If
            ''End Update
            'If objCustomerDirectorDetail.Nationality1 IsNot Nothing Then
            '    Store_Director_Nationality1.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            '    Store_Director_Nationality1.DataBind()
            '    Dim nasionality1 = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Nationality1)
            '    If nasionality1 IsNot Nothing Then
            '        cmb_Director_Nationality1.SetValue(nasionality1.Kode)
            '    End If
            'End If
            'If objCustomerDirectorDetail.Nationality2 IsNot Nothing Then
            '    Store_Director_Nationality2.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            '    Store_Director_Nationality2.DataBind()
            '    Dim nasionality2 = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Nationality2)
            '    If nasionality2 IsNot Nothing Then
            '        cmb_Director_Nationality2.SetValue(nasionality2.Kode)
            '    End If
            'End If
            'If objCustomerDirectorDetail.Nationality3 IsNot Nothing Then
            '    Store_Director_Nationality3.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            '    Store_Director_Nationality3.DataBind()
            '    Dim nasionality3 = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Nationality3)
            '    If nasionality3 IsNot Nothing Then
            '        cmb_Director_Nationality3.SetValue(nasionality3.Kode)
            '    End If
            'End If
            'If objCustomerDirectorDetail.Residence IsNot Nothing Then
            '    Store_Director_Residence.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            '    Store_Director_Residence.DataBind()
            '    Dim Residence = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Residence)
            '    If Residence IsNot Nothing Then
            '        cmb_Director_Residence.SetValue(Residence.Kode)
            '    End If
            'End If
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            Dim director_passport_country = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Passport_Country)
            If director_passport_country IsNot Nothing Then
                cmb_Director_Passport_Country.SetTextWithTextValue(director_passport_country.Kode, director_passport_country.Keterangan)
            End If
            Dim director_Nationality1 = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Nationality1)
            If director_Nationality1 IsNot Nothing Then
                cmb_Director_Nationality1.SetTextWithTextValue(director_Nationality1.Kode, director_Nationality1.Keterangan)
            End If
            Dim director_Nationality2 = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Nationality2)
            If director_Nationality2 IsNot Nothing Then
                cmb_Director_Nationality2.SetTextWithTextValue(director_Nationality2.Kode, director_Nationality2.Keterangan)
            End If
            Dim director_Nationality3 = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Nationality3)
            If director_Nationality3 IsNot Nothing Then
                cmb_Director_Nationality3.SetTextWithTextValue(director_Nationality3.Kode, director_Nationality3.Keterangan)
            End If
            Dim director_Residence = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Residence)
            If director_Residence IsNot Nothing Then
                cmb_Director_Residence.SetTextWithTextValue(director_Residence.Kode, director_Residence.Keterangan)
            End If
            Dim gender = goAML_CustomerBLL.GetJenisKelamin(objCustomerDirectorDetail.Gender)
            If gender IsNot Nothing Then
                cmb_Director_Gender.SetTextWithTextValue(gender.Kode, gender.Keterangan)
            End If
            'End Update
            'Store_Director_Gender.DataSource = goAML_CustomerBLL.GetListJenisKelamin()
            'Store_Director_Gender.DataBind()
            Dim JenisKelamin = goAML_CustomerBLL.GetListJenisKelamin.Where(Function(x) x.Kode = objCustomerDirectorDetail.Gender).FirstOrDefault
            If JenisKelamin IsNot Nothing Then
                cmb_Director_Gender.SetTextWithTextValue(JenisKelamin.Kode, JenisKelamin.Keterangan)
            End If
            'txt_Director_Email.Text = objCustomerDirectorDetail.Email
            'txt_Director_Email2.Text = objCustomerDirectorDetail.Email2
            'txt_Director_Email3.Text = objCustomerDirectorDetail.Email3
            'txt_Director_Email4.Text = objCustomerDirectorDetail.Email4
            'txt_Director_Email5.Text = objCustomerDirectorDetail.Email5
            txt_Director_Occupation.Text = objCustomerDirectorDetail.Occupation
            txt_Director_employer_name.Text = objCustomerDirectorDetail.Employer_Name
            If objCustomerDirectorDetail.Deceased = True Then
                cb_Director_Deceased.Checked = True
                txt_Director_Deceased_Date.Value = objCustomerDirectorDetail.Deceased_Date
            End If
            txt_Director_Tax_Number.Text = objCustomerDirectorDetail.Tax_Number
            If objCustomerDirectorDetail.Tax_Reg_Number = True Then
                cb_Director_Tax_Reg_Number.Checked = True
            End If
            txt_Director_Source_of_Wealth.Text = objCustomerDirectorDetail.Source_of_Wealth
            txt_Director_Comments.Text = objCustomerDirectorDetail.Comments
            btnaddidentificationdirector.Hidden = True
            btnsavedirectorphone.Hidden = True
            btnsavedirectorAddress.Hidden = True
            btnsaveEmpdirectorPhone.Hidden = True
            btnsaveEmpdirectorAddress.Hidden = True
            Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
            Store_DirectorAddress.DataBind()
            Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
            Store_DirectorPhone.DataBind()
            Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
            Store_Director_Employer_Address.DataBind()
            Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
            Store_Director_Employer_Phone.DataBind()
            Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()
            Store_Director_Identification.DataBind()

            Dim email_list = goAML_Customer_Service.GetEmailByPkFk(id, 6)
            If objCustomerDirectorDetail2 IsNot Nothing Then
                email_list = objCustomerDirectorDetail2.ObjList_GoAML_Ref_Customer_Email
            End If
            Dim sanction_list = goAML_Customer_Service.GetSanctionByPkFk(id, 6)
            If objCustomerDirectorDetail2 IsNot Nothing Then
                sanction_list = objCustomerDirectorDetail2.ObjList_GoAML_Ref_Customer_Sanction
            End If
            Dim pep_list = goAML_Customer_Service.GetPEPByPkFk(id, 6)
            If objCustomerDirectorDetail2 IsNot Nothing Then
                pep_list = objCustomerDirectorDetail2.ObjList_GoAML_Ref_Customer_PEP
            End If

            GridEmail_Director.LoadData(email_list)
            GridSanction_Director.LoadData(sanction_list)
            GridPEP_Director.LoadData(pep_list)

            'Dim email_list = goAML_Customer_Service.GetEmailByPkFk(id, 6)
            'Dim sanction_list = goAML_Customer_Service.GetSanctionByPkFk(id, 6)
            'Dim pep_list = goAML_Customer_Service.GetPEPByPkFk(id, 6)

            'GridEmail_Director.LoadData(email_list)
            'GridSanction_Director.LoadData(sanction_list)
            'GridPEP_Director.LoadData(pep_list)
        End If


    End Sub


    Sub LoadDataAddressDetail(id As Long)

        Dim objCustomerPhonesDetail = objListgoAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        If Not objCustomerPhonesDetail Is Nothing Then
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            Btn_Save_Address.Hidden = True
            WindowDetailAddress.Title = "Address Detail"
            cmb_kategoriaddress.IsReadOnly = True
            Address_INDV.ReadOnly = True
            Town_INDV.ReadOnly = True
            City_INDV.ReadOnly = True
            Zip_INDV.ReadOnly = True
            cmb_kodenegara.IsReadOnly = True
            State_INDVD.ReadOnly = True
            Comment_Address_INDVD.ReadOnly = True
            ClearinputCustomerAddress()
            'Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Store_KategoriAddress.DataBind()
            Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(objCustomerPhonesDetail.Address_Type)
            If kategorikontak IsNot Nothing Then
                cmb_kategoriaddress.SetTextWithTextValue(kategorikontak.Kode, kategorikontak.Keterangan)
            End If

            Address_INDV.Text = objCustomerPhonesDetail.Address
            Town_INDV.Text = objCustomerPhonesDetail.Town
            City_INDV.Text = objCustomerPhonesDetail.City
            Zip_INDV.Text = objCustomerPhonesDetail.Zip
            'Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Kode_Negara.DataBind()
            Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerPhonesDetail.Country_Code)
            If kodenegara IsNot Nothing Then
                cmb_kodenegara.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
            End If

            State_INDVD.Text = objCustomerPhonesDetail.State
            Comment_Address_INDVD.Text = objCustomerPhonesDetail.Comments
        End If
    End Sub

    Sub LoadDataEmployerAddressDetail(id As Long)

        Dim objCustomerAddressDetail = objListgoAML_Ref_Employer_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        If Not objCustomerAddressDetail Is Nothing Then
            FP_Address_Emp_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            btn_saveempaddress.Hidden = True
            WindowDetailAddress.Title = "Address Detail"
            cmb_emp_kategoriaddress.IsReadOnly = True
            emp_Address_INDV.ReadOnly = True
            emp_Town_INDV.ReadOnly = True
            emp_City_INDV.ReadOnly = True
            emp_Zip_INDV.ReadOnly = True
            cmb_emp_kodenegara.IsReadOnly = True
            emp_State_INDVD.ReadOnly = True
            emp_Comment_Address_INDVD.ReadOnly = True
            ClearinputCustomerEmpAddress()
            'Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Store_emp_KategoriAddress.DataBind()
            Dim kategorikontak_emp_detail = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
            If kategorikontak_emp_detail IsNot Nothing Then
                cmb_emp_kategoriaddress.SetTextWithTextValue(kategorikontak_emp_detail.Kode, kategorikontak_emp_detail.Keterangan)
            End If

            emp_Address_INDV.Text = objCustomerAddressDetail.Address
            emp_Town_INDV.Text = objCustomerAddressDetail.Town
            emp_City_INDV.Text = objCustomerAddressDetail.City
            emp_Zip_INDV.Text = objCustomerAddressDetail.Zip
            'Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            ' Store_emp_Kode_Negara.DataBind()
            Dim kodenegara_emp_detail = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerAddressDetail.Country_Code)
            If kodenegara_emp_detail IsNot Nothing Then
                cmb_emp_kodenegara.SetTextWithTextValue(kodenegara_emp_detail.Kode, kodenegara_emp_detail.Keterangan)
            End If

            emp_State_INDVD.Text = objCustomerAddressDetail.State
            emp_Comment_Address_INDVD.Text = objCustomerAddressDetail.Comments
        End If
    End Sub

    Sub LoadDataEditCustPhone(id As Long)
        objTempgoAML_Ref_Phone = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        objTempCustomerPhoneEdit = objListgoAML_vw_Customer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objTempCustomerPhoneEdit Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            cb_phone_Contact_Type.IsReadOnly = False
            cb_phone_Communication_Type.IsReadOnly = False
            txt_phone_Country_prefix.ReadOnly = False
            txt_phone_number.ReadOnly = False
            txt_phone_extension.ReadOnly = False
            txt_phone_comments.ReadOnly = False
            btnSavedetail.Hidden = False

            WindowDetail.Title = "Phone Edit"
            ClearinputCustomerPhones()
            With objTempCustomerPhoneEdit
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_contact_type IsNot Nothing Then
                    cb_phone_Contact_Type.SetTextWithTextValue(obj_contact_type.Kode, obj_contact_type.Keterangan)
                End If

                Dim obj_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_communication_type IsNot Nothing Then
                    cb_phone_Communication_Type.SetTextWithTextValue(obj_communication_type.Kode, obj_communication_type.Keterangan)
                End If
                'End Update
                'Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_Contact_Type.DataBind()
                'cb_phone_Contact_Type.SetValue(objTempCustomerPhoneEdit.Tph_Contact_Type)

                'Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Store_Communication_Type.DataBind()
                'cb_phone_Communication_Type.SetValue(objTempCustomerPhoneEdit.Tph_Communication_Type)

                txt_phone_Country_prefix.Text = .tph_country_prefix
                txt_phone_number.Text = .tph_number
                txt_phone_extension.Text = .tph_extension
                txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If

    End Sub

    Sub LoadDataEditEmployerPhone(id As Long)
        objTempgoAML_Ref_EmployerPhones = objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        objTempEmployerPhonesEdit = objListgoAML_vw_Employer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objTempEmployerPhonesEdit Is Nothing Then
            FormPanelEmpTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            Emp_cb_phone_Contact_Type.IsReadOnly = False
            Emp_cb_phone_Communication_Type.IsReadOnly = False
            Emp_txt_phone_Country_prefix.ReadOnly = False
            Emp_txt_phone_number.ReadOnly = False
            Emp_txt_phone_extension.ReadOnly = False
            Emp_txt_phone_comments.ReadOnly = False
            btn_saveempphone.Hidden = False

            WindowDetail.Title = "Edit Phone"
            ClearinputCustomerEmpPhones()
            With objTempEmployerPhonesEdit
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_emp_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_emp_contact_type IsNot Nothing Then
                    Emp_cb_phone_Contact_Type.SetTextWithTextValue(obj_emp_contact_type.Kode, obj_emp_contact_type.Keterangan)
                End If

                'Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Emp_Store_Communication_Type.DataBind()
                'Emp_cb_phone_Communication_Type.SetValue(objTempEmployerPhonesEdit.Tph_Communication_Type)
                Dim obj_emp_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_emp_communication_type IsNot Nothing Then
                    Emp_cb_phone_Communication_Type.SetTextWithTextValue(obj_emp_communication_type.Kode, obj_emp_communication_type.Keterangan)
                End If
                'End Update
                'Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Emp_Store_Contact_Type.DataBind()
                'Emp_cb_phone_Contact_Type.SetValue(objTempEmployerPhonesEdit.Tph_Contact_Type)

                'Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Emp_Store_Communication_Type.DataBind()
                'Emp_cb_phone_Communication_Type.SetValue(objTempEmployerPhonesEdit.Tph_Communication_Type)

                Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Emp_txt_phone_number.Text = .tph_number
                Emp_txt_phone_extension.Text = .tph_extension
                Emp_txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If

    End Sub

    Sub LoadDataEditCustAddress(id As Long)
        objTempgoAML_Ref_Address = objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        objTempCustomerAddressEdit = objListgoAML_vw_Customer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)

        Dim Address_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Country As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing

        If Not objTempCustomerAddressEdit Is Nothing Then
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            Btn_Save_Address.Hidden = False
            cmb_kategoriaddress.IsReadOnly = False
            Address_INDV.ReadOnly = False
            Town_INDV.ReadOnly = False
            City_INDV.ReadOnly = False
            Zip_INDV.ReadOnly = False
            cmb_kodenegara.IsReadOnly = False
            State_INDVD.ReadOnly = False
            Comment_Address_INDVD.ReadOnly = False

            WindowDetailAddress.Title = "Address Edit"
            ClearinputCustomerAddress()
            With objTempCustomerAddressEdit
                'Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_KategoriAddress.DataBind()
                Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                If kategorikontak IsNot Nothing Then
                    cmb_kategoriaddress.SetTextWithTextValue(kategorikontak.Kode, kategorikontak.Keterangan)
                End If
                Address_INDV.Text = .Address
                Town_INDV.Text = .Town
                City_INDV.Text = .City
                Zip_INDV.Text = .Zip
                'Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Store_Kode_Negara.DataBind()
                Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(.Country_Code)
                If kodenegara IsNot Nothing Then
                    cmb_kodenegara.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
                End If

                State_INDVD.Text = .State
                Comment_Address_INDVD.Text = .Comments
            End With

            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataEditEmployerAddress(id As Long)
        objTempgoAML_Ref_EmployerAddresses = objListgoAML_Ref_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        objTempEmployerAddressEdit = objListgoAML_vw_Employer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)

        Dim Address_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Country As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing

        If Not objTempEmployerAddressEdit Is Nothing Then
            FP_Address_Emp_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            btn_saveempaddress.Hidden = False
            cmb_emp_kategoriaddress.IsReadOnly = False
            emp_Address_INDV.ReadOnly = False
            emp_Town_INDV.ReadOnly = False
            emp_City_INDV.ReadOnly = False
            emp_Zip_INDV.ReadOnly = False
            cmb_emp_kodenegara.IsReadOnly = False
            emp_State_INDVD.ReadOnly = False
            emp_Comment_Address_INDVD.ReadOnly = False

            WindowDetailAddress.Title = "Address Edit"
            ClearinputCustomerEmpAddress()
            With objTempEmployerAddressEdit
                'Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_emp_KategoriAddress.DataBind()
                Dim kategorikontak_emp = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                If kategorikontak_emp IsNot Nothing Then
                    cmb_emp_kategoriaddress.SetTextWithTextValue(kategorikontak_emp.Kode, kategorikontak_emp.Keterangan)
                End If
                emp_Address_INDV.Text = .Address
                emp_Town_INDV.Text = .Town
                emp_City_INDV.Text = .City
                emp_Zip_INDV.Text = .Zip
                'Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Store_emp_Kode_Negara.DataBind()
                Dim kodenegara_emp = goAML_CustomerBLL.GetNamaNegaraByID(.Country_Code)
                If kodenegara_emp IsNot Nothing Then
                    cmb_emp_kodenegara.SetTextWithTextValue(kodenegara_emp.Kode, kodenegara_emp.Keterangan)
                End If

                emp_State_INDVD.Text = .State
                emp_Comment_Address_INDVD.Text = .Comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataEditCustDirector(id As Long)
        objTempgoAML_Ref_Director = objListgoAML_Ref_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
        Dim objCustomerDirectorDetail2 = Nothing
        If objListgoAML_Ref_Director2 IsNot Nothing Then
            objCustomerDirectorDetail2 = objListgoAML_Ref_Director2.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
        End If
        objTempCustomerDirectorEdit = objListgoAML_vw_Customer_Entity_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
        tempflag = id
        'Dim Director As GoAMLDAL.goaml_ref_customer_entity_director = Nothing
        'Dim Peran As GoAMLDAL.goaml_ref_peran_orang_dalam_korporasi = Nothing

        If Not objTempCustomerDirectorEdit Is Nothing Then

            GridEmail_Director.IsViewMode = False
            GridSanction_Director.IsViewMode = False
            GridPEP_Director.IsViewMode = False

            FP_Director.Hidden = False
            WindowDetailDirector.Hidden = False
            btnsaveDirector.Hidden = False
            'cmb_director.ReadOnly = False
            cmb_peran.IsReadOnly = False
            txt_Director_Title.ReadOnly = False
            txt_Director_Last_Name.ReadOnly = False
            txt_Director_SSN.ReadOnly = False
            txt_Director_BirthDate.ReadOnly = False
            txt_Director_Birth_Place.ReadOnly = False
            txt_Director_Mothers_Name.ReadOnly = False
            txt_Director_Alias.ReadOnly = False
            txt_Director_Passport_Number.ReadOnly = False
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            cmb_Director_Passport_Country.IsReadOnly = False
            'End Update
            txt_Director_ID_Number.ReadOnly = False
            cmb_Director_Nationality1.IsReadOnly = False
            cmb_Director_Nationality2.IsReadOnly = False
            cmb_Director_Nationality3.IsReadOnly = False
            cmb_Director_Gender.IsReadOnly = False
            cmb_Director_Residence.IsReadOnly = False
            'txt_Director_Email.ReadOnly = False
            'txt_Director_Email2.ReadOnly = False
            'txt_Director_Email3.ReadOnly = False
            'txt_Director_Email4.ReadOnly = False
            'txt_Director_Email5.ReadOnly = False
            txt_Director_Occupation.ReadOnly = False
            txt_Director_employer_name.ReadOnly = False
            cb_Director_Deceased.ReadOnly = False
            txt_Director_Deceased_Date.ReadOnly = False
            txt_Director_Tax_Number.ReadOnly = False
            cb_Director_Tax_Reg_Number.ReadOnly = False
            txt_Director_Source_of_Wealth.ReadOnly = False
            txt_Director_Comments.ReadOnly = False
            btnsavedirectorphone.Hidden = False
            btnsavedirectorAddress.Hidden = False
            btnsaveEmpdirectorPhone.Hidden = False
            btnsaveEmpdirectorAddress.Hidden = False
            btnaddidentificationdirector.Hidden = False
            WindowDetailDirector.Title = "Director Edit"
            With objTempCustomerDirectorEdit
                'StoreDirector.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,INDV_Last_Name,cb_value from Vw_CB_Signatory_Person", Nothing)
                'StoreDirector.DataBind()
                'cmb_director.SetValue(objTempgoAML_Ref_Director.FK_Customer_ID)
                'StorePeran.DataSource = goAML_CustomerBLL.GetListPeranDalamKorporasi()
                'StorePeran.DataBind()
                Dim peran = goAML_CustomerBLL.GetRoleDirectorByID(objTempgoAML_Ref_Director.Role)
                If peran IsNot Nothing Then
                    cmb_peran.SetTextWithTextValue(peran.Kode, peran.Keterangan)
                End If
                txt_Director_Title.Text = objTempgoAML_Ref_Director.Title
                txt_Director_Last_Name.Text = objTempgoAML_Ref_Director.Last_Name
                txt_Director_SSN.Text = objTempgoAML_Ref_Director.SSN
                If objTempgoAML_Ref_Director.BirthDate IsNot Nothing Then
                    txt_Director_BirthDate.Text = objTempgoAML_Ref_Director.BirthDate
                End If
                txt_Director_Birth_Place.Text = objTempgoAML_Ref_Director.Birth_Place
                txt_Director_Mothers_Name.Text = objTempgoAML_Ref_Director.Mothers_Name
                txt_Director_Alias.Text = objTempgoAML_Ref_Director.Alias
                txt_Director_ID_Number.Text = objTempgoAML_Ref_Director.ID_Number
                txt_Director_Passport_Number.Text = objTempgoAML_Ref_Director.Passport_Number
                'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
                Dim director_passport_country = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Passport_Country)
                If director_passport_country IsNot Nothing Then
                    cmb_Director_Passport_Country.SetTextWithTextValue(director_passport_country.Kode, director_passport_country.Keterangan)
                End If
                Dim director_Nationality1 = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Nationality1)
                If director_Nationality1 IsNot Nothing Then
                    cmb_Director_Nationality1.SetTextWithTextValue(director_Nationality1.Kode, director_Nationality1.Keterangan)
                End If
                Dim director_Nationality2 = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Nationality2)
                If director_Nationality2 IsNot Nothing Then
                    cmb_Director_Nationality2.SetTextWithTextValue(director_Nationality2.Kode, director_Nationality2.Keterangan)
                End If
                Dim director_Nationality3 = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Nationality3)
                If director_Nationality3 IsNot Nothing Then
                    cmb_Director_Nationality3.SetTextWithTextValue(director_Nationality3.Kode, director_Nationality3.Keterangan)
                End If
                Dim director_Residence = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Residence)
                If director_Residence IsNot Nothing Then
                    cmb_Director_Residence.SetTextWithTextValue(director_Residence.Kode, director_Residence.Keterangan)
                End If
                Dim gender = goAML_CustomerBLL.GetJenisKelamin(objTempgoAML_Ref_Director.Gender)
                If gender IsNot Nothing Then
                    cmb_Director_Gender.SetTextWithTextValue(gender.Kode, gender.Keterangan)
                End If
                'End Update
                ' If objTempgoAML_Ref_Director.Nationality1 IsNot Nothing Then
                '    Store_Director_Nationality1.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                '    Store_Director_Nationality1.DataBind()
                '    Dim nasionality1 = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Nationality1)
                '    If nasionality1 IsNot Nothing Then
                '        cmb_Director_Nationality1.SetValue(nasionality1.Kode)
                '    End If
                'End If
                'If objTempgoAML_Ref_Director.Nationality2 IsNot Nothing Then
                '    Store_Director_Nationality2.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                '    Store_Director_Nationality2.DataBind()
                '    Dim nasionality2 = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Nationality2)
                '    If nasionality2 IsNot Nothing Then
                '        cmb_Director_Nationality2.SetValue(nasionality2.Kode)
                '    End If
                'End If
                'If objTempgoAML_Ref_Director.Nationality3 IsNot Nothing Then
                '    Store_Director_Nationality3.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                '    Store_Director_Nationality3.DataBind()
                '    Dim nasionality3 = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Nationality3)
                '    If nasionality3 IsNot Nothing Then
                '        cmb_Director_Nationality3.SetValue(nasionality3.Kode)
                '    End If
                'End If
                'If objTempgoAML_Ref_Director.Residence IsNot Nothing Then
                '    Store_Director_Residence.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                '    Store_Director_Residence.DataBind()
                '    Dim Residence = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Residence)
                '    If Residence IsNot Nothing Then
                '        cmb_Director_Residence.SetValue(Residence.Kode)
                '    End If
                'End If

                'Store_Director_Gender.DataSource = goAML_CustomerBLL.GetListJenisKelamin()
                'Store_Director_Gender.DataBind()
                'Dim JenisKelamin = goAML_CustomerBLL.GetListJenisKelamin.Where(Function(x) x.Kode = objTempgoAML_Ref_Director.Gender).FirstOrDefault
                'If JenisKelamin IsNot Nothing Then
                '    cmb_Director_Gender.SetValue(JenisKelamin.Kode)
                'End If
                'txt_Director_Email.Text = objTempgoAML_Ref_Director.Email
                'txt_Director_Email2.Text = objTempgoAML_Ref_Director.Email2
                'txt_Director_Email3.Text = objTempgoAML_Ref_Director.Email3
                'txt_Director_Email4.Text = objTempgoAML_Ref_Director.Email4
                'txt_Director_Email5.Text = objTempgoAML_Ref_Director.Email5
                txt_Director_Occupation.Text = objTempgoAML_Ref_Director.Occupation
                txt_Director_employer_name.Text = objTempgoAML_Ref_Director.Employer_Name
                If objTempgoAML_Ref_Director.Deceased = True Then
                    cb_Director_Deceased.Checked = True
                    txt_Director_Deceased_Date.Value = objTempgoAML_Ref_Director.Deceased_Date
                End If
                txt_Director_Tax_Number.Text = objTempgoAML_Ref_Director.Tax_Number
                If objTempgoAML_Ref_Director.Tax_Reg_Number = True Then
                    cb_Director_Tax_Reg_Number.Checked = True
                End If
                txt_Director_Source_of_Wealth.Text = objTempgoAML_Ref_Director.Source_of_Wealth
                txt_Director_Comments.Text = objTempgoAML_Ref_Director.Comments

                Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
                Store_DirectorAddress.DataBind()
                Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
                Store_DirectorPhone.DataBind()
                Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
                Store_Director_Employer_Address.DataBind()
                Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
                Store_Director_Employer_Phone.DataBind()
                Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()
                Store_Director_Identification.DataBind()
                'Dim DirectorEmpAddress = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
                'If DirectorEmpAddress.Count > 0 Then
                '    btnsaveEmpdirectorAddress.Hidden = True
                'End If
                'Dim DirectorEmpPhone = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
                'If DirectorEmpPhone.Count > 0 Then
                '    btnsaveEmpdirectorPhone.Hidden = True
                'End If


            End With

            Dim email_list = goAML_Customer_Service.GetEmailByPkFk(id, 6)
            If objCustomerDirectorDetail2 IsNot Nothing Then
                email_list = objCustomerDirectorDetail2.ObjList_GoAML_Ref_Customer_Email
            End If
            Dim sanction_list = goAML_Customer_Service.GetSanctionByPkFk(id, 6)
            If objCustomerDirectorDetail2 IsNot Nothing Then
                sanction_list = objCustomerDirectorDetail2.ObjList_GoAML_Ref_Customer_Sanction
            End If
            Dim pep_list = goAML_Customer_Service.GetPEPByPkFk(id, 6)
            If objCustomerDirectorDetail2 IsNot Nothing Then
                pep_list = objCustomerDirectorDetail2.ObjList_GoAML_Ref_Customer_PEP
            End If

            GridSanction_Director.objListgoAML_Ref = sanction_list
            GridSanction_Director.objListgoAML_vw = sanction_list

            GridEmail_Director.objListgoAML_Ref = email_list
            GridEmail_Director.objListgoAML_vw = email_list

            GridPEP_Director.objListgoAML_Ref = pep_list
            GridPEP_Director.objListgoAML_vw = pep_list

            GridEmail_Director.LoadData(email_list)
            GridSanction_Director.LoadData(sanction_list)
            GridPEP_Director.LoadData(pep_list)

            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataEditCustIdentification(id As Long)
        objTempgoAML_Ref_Identification = objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)
        objTempCustomerIdentificationEdit = objListgoAML_vw_Customer_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)

        If Not objTempCustomerIdentificationEdit Is Nothing Then
            FP_Identification_INDV.Hidden = False
            WindowDetailIdentification.Hidden = False
            btn_save_identification.Hidden = False

            WindowDetailIdentification.Title = "Identification Edit"
            ClearinputCustomerIdentification()
            cmb_Jenis_Dokumen.IsReadOnly = False
            txt_issued_by.ReadOnly = False
            txt_issued_date.ReadOnly = False
            txt_issued_expired_date.ReadOnly = False
            txt_identification_comment.ReadOnly = False
            cmb_issued_country.IsReadOnly = False
            txt_number_identification.ReadOnly = False
            With objTempCustomerIdentificationEdit
                'Store_jenis_dokumen.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
                'Store_jenis_dokumen.DataBind()
                If .Type_Document IsNot Nothing Then
                    Dim dokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas.Where(Function(x) x.Keterangan = .Type_Document).FirstOrDefault
                    cmb_Jenis_Dokumen.SetTextWithTextValue(dokumen.Kode, dokumen.Keterangan)
                End If
                txt_issued_by.Text = .Issued_By
                txt_issued_date.Value = IIf(.Issue_Date.HasValue, .Issue_Date, Nothing) ' 2023-11-22, Nael
                txt_issued_expired_date.Value = IIf(.Expiry_Date.HasValue, .Expiry_Date, Nothing) ' 2023-11-22, Nael
                txt_identification_comment.Text = .Identification_Comment
                'Store_issued_country.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Store_issued_country.DataBind()
                Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByKeterangan(.Country)
                If kodenegara IsNot Nothing Then
                    cmb_issued_country.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
                End If

                txt_number_identification.Text = .Number
            End With

            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub DeleteRecordTaskDetailCustPhone(id As Long)
        Dim objDelVWPhone As GoAMLDAL.Vw_Customer_Phones = objListgoAML_vw_Customer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objxDelPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        If Not objxDelPhone Is Nothing Then
            objListgoAML_Ref_Phone.Remove(objxDelPhone)
        End If

        If Not objDelVWPhone Is Nothing Then
            objListgoAML_vw_Customer_Phones.Remove(objDelVWPhone)

            If obj_Customer.FK_Customer_Type_ID = 1 Then
                Store_INDV_Phones.DataSource = objListgoAML_vw_Customer_Phones.ToList()
                Store_INDV_Phones.DataBind()
            Else
                Store_Phone_Corp.DataSource = objListgoAML_vw_Customer_Phones.ToList()
                Store_Phone_Corp.DataBind()
            End If
        End If

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerPhones()

    End Sub

    Sub DeleteRecordTaskDetailEmployerPhone(id As Long)
        Dim objDelVWPhone As GoAMLDAL.Vw_Employer_Phones = objListgoAML_vw_Employer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objxDelPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        If Not objxDelPhone Is Nothing Then
            objListgoAML_Ref_Employer_Phone.Remove(objxDelPhone)
        End If

        If Not objDelVWPhone Is Nothing Then
            objListgoAML_vw_Employer_Phones.Remove(objDelVWPhone)
            Store_Empoloyer_Phone.DataSource = objListgoAML_vw_Employer_Phones.ToList()
            Store_Empoloyer_Phone.DataBind()
        End If

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerPhones()

    End Sub

    Sub DeleteRecordTaskDetailCustAddress(id As Long)
        Dim objDelVWAddress As GoAMLDAL.Vw_Customer_Addresses = objListgoAML_vw_Customer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objxDelAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = id)

        If Not objxDelAddress Is Nothing Then
            objListgoAML_Ref_Address.Remove(objxDelAddress)
        End If

        If Not objDelVWAddress Is Nothing Then
            objListgoAML_vw_Customer_Addresses.Remove(objDelVWAddress)

            If obj_Customer.FK_Customer_Type_ID = 1 Then
                Store_INDV_Addresses.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
                Store_INDV_Addresses.DataBind()
            Else
                Store_Address_Corp.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
                Store_Address_Corp.DataBind()
            End If
        End If

        FP_Address_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerAddress()

    End Sub

    Sub DeleteRecordTaskDetailEmployerAddress(id As Long)
        Dim objDelVWAddress As GoAMLDAL.Vw_Employer_Addresses = objListgoAML_vw_Employer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objxDelAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = id)

        If Not objxDelAddress Is Nothing Then
            objListgoAML_Ref_Employer_Address.Remove(objxDelAddress)
        End If

        If Not objDelVWAddress Is Nothing Then
            objListgoAML_vw_Employer_Addresses.Remove(objDelVWAddress)
            Store_Employer_Address.DataSource = objListgoAML_vw_Employer_Addresses.ToList()
            Store_Employer_Address.DataBind()
        End If

        FP_Address_Emp_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerEmpAddress()

    End Sub

    Sub DeleteRecordTaskDetailCustDirector(id As Long)
        Dim objDelVWDirector As GoAMLDAL.Vw_Customer_Director = objListgoAML_vw_Customer_Entity_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
        Dim objDelVWAddress As List(Of GoAMLDAL.Vw_Director_Addresses) = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        Dim objDelVWPhone As List(Of GoAMLDAL.Vw_Director_Phones) = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        Dim objDelVWEMPAddress As List(Of GoAMLDAL.Vw_Director_Employer_Addresses) = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        Dim objDelEMPVWPhone As List(Of GoAMLDAL.Vw_Director_Employer_Phones) = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        Dim objDelVWIdentification As List(Of GoAMLDAL.Vw_Person_Identification) = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()

        Dim objxDelDirector As GoAMLDAL.goAML_Ref_Customer_Entity_Director = objListgoAML_Ref_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
        Dim objxDelAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        Dim objxDelPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        Dim objxDelEMPAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        Dim objxDelEMPPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        Dim objxDelIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()

        If Not objxDelDirector Is Nothing Then
            objListgoAML_Ref_Director.Remove(objxDelDirector)
            For Each item As goAML_Ref_Address In objxDelAddress
                objListgoAML_Ref_Director_Address.Remove(item)
            Next
            For Each item As goAML_Ref_Phone In objxDelPhone
                objListgoAML_Ref_Director_Phone.Remove(item)
            Next
            For Each item As goAML_Ref_Address In objxDelEMPAddress
                objListgoAML_Ref_Director_Employer_Address.Remove(item)
            Next
            For Each item As goAML_Ref_Phone In objxDelEMPPhone
                objListgoAML_Ref_Director_Employer_Phone.Remove(item)
            Next
            For Each item As goAML_Person_Identification In objxDelIdentification
                objListgoAML_Ref_Identification_Director.Remove(item)
            Next
        End If

        If Not objDelVWDirector Is Nothing Then
            objListgoAML_vw_Customer_Entity_Director.Remove(objDelVWDirector)
            For Each item As Vw_Director_Addresses In objDelVWAddress
                objListgoAML_vw_Director_Addresses.Remove(item)
            Next
            For Each item As Vw_Director_Phones In objDelVWPhone
                objListgoAML_vw_Director_Phones.Remove(item)
            Next
            For Each item As Vw_Director_Employer_Addresses In objDelVWEMPAddress
                objListgoAML_vw_Director_Employer_Addresses.Remove(item)
            Next
            For Each item As Vw_Director_Employer_Phones In objDelEMPVWPhone
                objListgoAML_vw_Director_Employer_Phones.Remove(item)
            Next
            For Each item As Vw_Person_Identification In objDelVWIdentification
                objListgoAML_vw_Customer_Identification_Director.Remove(item)
            Next

            Store_Director_Corp.DataSource = objListgoAML_vw_Customer_Entity_Director.ToList()
            Store_Director_Corp.DataBind()
        End If


        FP_Director.Hidden = True
        WindowDetailDirector.Hidden = True
        ClearinputCustomerDirector()

    End Sub


    Sub DeleteRecordTaskDetailCustIdentification(id As Long)
        Dim objDelVWIdentification As GoAMLDAL.Vw_Person_Identification = objListgoAML_vw_Customer_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)
        Dim objxDelIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)

        If Not objxDelIdentification Is Nothing Then
            objListgoAML_Ref_Identification.Remove(objxDelIdentification)
        End If

        If Not objDelVWIdentification Is Nothing Then
            objListgoAML_vw_Customer_Identification.Remove(objDelVWIdentification)


            Store_INDV_Identification.DataSource = objListgoAML_vw_Customer_Identification.ToList()
            Store_INDV_Identification.DataBind()
        End If

        FP_Identification_INDV.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerIdentification()

    End Sub


    Protected Sub btnBackSaveDetail_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = True
            FP_Address_INDVD.Hidden = True
            FP_Identification_INDV.Hidden = True
            WindowDetail.Hidden = True
            WindowDetailAddress.Hidden = True
            WindowDetailIdentification.Hidden = True
            ClearinputCustomerPhones()
            ClearinputCustomerAddress()
            ClearinputCustomerIdentification()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub checkBoxIsClosed_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbTutup.DirectCheck
        Try
            If cbTutup.Checked Then
                txt_Corp_date_business_closed.Hidden = False
            Else
                txt_Corp_date_business_closed.Hidden = True
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub



    Private Sub CUSTOMER_EDIT_v501_Default_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Update
    End Sub

    Protected Sub btnBackSaveCustomerAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_INDVD.Hidden = True
            WindowDetailAddress.Hidden = True
            ClearinputCustomerAddress()
            objTempCustomerAddressEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Director.Hidden = True
            WindowDetailDirector.Hidden = True
            ClearinputCustomerDirector()
            objTempCustomerDirectorEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Identification_INDV.Hidden = True
            WindowDetailIdentification.Hidden = True
            ClearinputCustomerIdentification()
            objTempCustomerIdentificationEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Identification_Director.Hidden = True
            WindowDetailIdentification.Hidden = True
            ClearinputDirectorIdentification()
            objTempCustomerIdentificationEdit = Nothing
            For Each item As goAML_Person_Identification In objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList
                objListgoAML_Ref_Identification_Director.Remove(item)
            Next
            For Each item As Vw_Person_Identification In objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList
                objListgoAML_vw_Customer_Identification_Director.Remove(item)
            Next
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub



    Protected Sub checkBoxcb_Deceased_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_Deceased.DirectCheck
        Try
            If cb_Deceased.Checked Then
                txt_deceased_date.Hidden = False
            Else
                txt_deceased_date.Hidden = True
                txt_deceased_date.Value = Nothing
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Protected Sub checkBoxcb_tax_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_tax.DirectCheck
    '    Try
    '        If cb_Deceased.Checked Then
    '            txt_INDV_Tax_Number.Hidden = False
    '        Else
    '            txt_INDV_Tax_Number.Hidden = True
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Sub ClearinputCustomerEmpPhones()
        Emp_cb_phone_Contact_Type.SetTextValue("")
        Emp_cb_phone_Communication_Type.SetTextValue("")
        Emp_txt_phone_Country_prefix.Text = ""
        Emp_txt_phone_number.Text = ""
        Emp_txt_phone_extension.Text = ""
        Emp_txt_phone_comments.Text = ""

    End Sub

    Protected Sub GrdCmdEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailEmployerPhone(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditEmployerPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailEmployerPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomerEmpPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            If IsDataCustomerEmpPhoneValid() Then
                If objTempEmployerPhonesEdit Is Nothing Then
                    SaveAddEmpTaskDetail()
                Else
                    SaveEditEmpTaskDetail()
                End If
            End If
            FormPanelEmpTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerEmpPhones()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Function IsDataCustomerEmpPhoneValid() As Boolean
        If Emp_cb_phone_Contact_Type.SelectedItemValue = "" Then
            Throw New Exception("Contact Type is required")
        End If
        If Emp_cb_phone_Communication_Type.SelectedItemValue = "" Then
            Throw New Exception("Communication Type is required")
        End If
        If Emp_txt_phone_Country_prefix.Text.Length > 3 Then
            Throw New Exception("Max Country Prefix is 3")
        End If
        If Emp_txt_phone_extension.Text.Length > 9 Then
            Throw New Exception("Max Phone Extension is 9")
        End If
        If Emp_txt_phone_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(Emp_txt_phone_number.Text) Then
            Throw New Exception("Phone Number is Invalid")
        End If
        'End Update
        Return True
    End Function

    Sub SaveAddEmpTaskDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Employer_Phones
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Phone
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Phone = intpk

            .FK_Ref_Detail_Of = 5
            .FK_for_Table_ID = obj_Customer.PK_Customer_ID
            .Tph_Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Emp_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Emp_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Emp_txt_phone_number.Text.Trim
            .tph_extension = Emp_txt_phone_extension.Text.Trim
            .comments = Emp_txt_phone_comments.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_goAML_Ref_Phone = intpk
            .FK_Ref_Detail_Of = 5
            .FK_for_Table_ID = obj_Customer.PK_Customer_ID
            .Tph_Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Emp_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Emp_txt_phone_number.Text.Trim
            .tph_extension = Emp_txt_phone_extension.Text.Trim
            .comments = Emp_txt_phone_comments.Text.Trim
        End With

        objListgoAML_vw_Employer_Phones.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Employer_Phone.Add(objNewgoAML_Customer)

        Store_Empoloyer_Phone.DataSource = objListgoAML_vw_Employer_Phones.ToList()
        Store_Empoloyer_Phone.DataBind()

        FormPanelEmpTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerEmpPhones()
    End Sub

    Sub SaveEditEmpTaskDetail()

        With objTempEmployerPhonesEdit
            .Tph_Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Emp_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Emp_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Emp_txt_phone_number.Text.Trim
            .tph_extension = Emp_txt_phone_extension.Text.Trim
            .comments = Emp_txt_phone_comments.Text.Trim
        End With

        With objTempgoAML_Ref_EmployerPhones
            .Tph_Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Emp_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Emp_txt_phone_number.Text.Trim
            .tph_extension = Emp_txt_phone_extension.Text.Trim
            .comments = Emp_txt_phone_comments.Text.Trim
        End With

        objTempEmployerPhonesEdit = Nothing
        objTempgoAML_Ref_EmployerPhones = Nothing

        Store_Empoloyer_Phone.DataSource = objListgoAML_vw_Employer_Phones.ToList()
        Store_Empoloyer_Phone.DataBind()

        FormPanelEmpTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerEmpPhones()
    End Sub

    Sub ClearinputCustomerEmpAddress()
        cmb_emp_kategoriaddress.SetTextValue("")
        cmb_emp_kodenegara.SetTextValue("")
        emp_Address_INDV.Text = ""
        emp_Town_INDV.Text = ""
        emp_City_INDV.Text = ""
        emp_Zip_INDV.Text = ""
        emp_State_INDVD.Text = ""
        emp_Comment_Address_INDVD.Text = ""

    End Sub

    Protected Sub btnSaveCustomerEmpAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomerEmpAddressValid() Then
                If objTempEmployerAddressEdit Is Nothing Then
                    SaveAddEmpAddressDetail()
                Else
                    SaveEditEmpAddressDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SaveEditEmpAddressDetail()

        With objTempEmployerAddressEdit
            .Address_Type = cmb_emp_kategoriaddress.SelectedItemValue
            .Type_Address_Detail = cmb_emp_kategoriaddress.SelectedItemText
            .Address = emp_Address_INDV.Value
            .City = emp_City_INDV.Value
            .Town = emp_Town_INDV.Value
            .Country_Code = cmb_emp_kodenegara.SelectedItemValue
            .Comments = emp_Comment_Address_INDVD.Value
            .Country = cmb_emp_kodenegara.SelectedItemText
            .State = emp_State_INDVD.Value
            .Zip = emp_Zip_INDV.Value
            .Comments = emp_Comment_Address_INDVD.Text.Trim
        End With

        With objTempgoAML_Ref_EmployerAddresses
            .Address_Type = cmb_emp_kategoriaddress.SelectedItemValue '
            .City = emp_City_INDV.Value
            .Address = emp_Address_INDV.Value
            .Town = emp_Town_INDV.Value
            .Country_Code = cmb_emp_kodenegara.SelectedItemValue
            .Comments = emp_Comment_Address_INDVD.Value
            .State = emp_State_INDVD.Value
            .Zip = emp_Zip_INDV.Value
            .Comments = emp_Comment_Address_INDVD.Text.Trim
        End With

        objTempEmployerAddressEdit = Nothing
        objTempgoAML_Ref_EmployerAddresses = Nothing

        Store_Employer_Address.DataSource = objListgoAML_vw_Employer_Addresses.ToList()
        Store_Employer_Address.DataBind()

        FP_Address_Emp_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerEmpAddress()
    End Sub

    Sub SaveAddEmpAddressDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Employer_Addresses
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Address
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 5
            .Address_Type = cmb_emp_kategoriaddress.SelectedItemValue
            .Address = emp_Address_INDV.Value
            .City = emp_City_INDV.Value
            .Town = emp_Town_INDV.Value
            .Country_Code = cmb_emp_kodenegara.SelectedItemValue
            .Comments = emp_Comment_Address_INDVD.Value
            .State = emp_State_INDVD.Value
            .Zip = emp_Zip_INDV.Value
            .Type_Address_Detail = cmb_emp_kategoriaddress.SelectedItemText
            .Country = cmb_emp_kodenegara.SelectedItemText
            .Comments = emp_Comment_Address_INDVD.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 5
            .FK_To_Table_ID = obj_Customer.PK_Customer_ID
            .Address_Type = cmb_emp_kategoriaddress.SelectedItemValue '
            .City = emp_City_INDV.Value
            .Address = emp_Address_INDV.Value
            .Town = emp_Town_INDV.Value
            .Country_Code = cmb_emp_kodenegara.SelectedItemValue
            .Comments = emp_Comment_Address_INDVD.Value
            .State = emp_State_INDVD.Value
            .Zip = emp_Zip_INDV.Value
            .Comments = emp_Comment_Address_INDVD.Text.Trim
        End With

        objListgoAML_vw_Employer_Addresses.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Employer_Address.Add(objNewgoAML_Customer)

        Store_Employer_Address.DataSource = objListgoAML_vw_Employer_Addresses.ToList()
        Store_Employer_Address.DataBind()

        FP_Address_Emp_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerEmpAddress()
    End Sub


    Function IsDataCustomerEmpAddressValid() As Boolean
        If cmb_emp_kodenegara.SelectedItemValue = "" Then
            Throw New Exception("Country Code is required")
        End If
        If cmb_emp_kategoriaddress.SelectedItemValue = "" Then
            Throw New Exception("Address Type is required")
        End If
        If emp_Address_INDV.Text.Trim = "" Then
            Throw New Exception("Address is required")
        End If
        If emp_City_INDV.Text.Trim = "" Then
            Throw New Exception("City is required")
        End If
        Return True
    End Function

    Protected Sub btnSaveDirectorPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            If IsDataDirectorPhoneValid() Then
                If objTempDirectorPhoneEdit Is Nothing Then
                    SaveAddDirectorTaskDetail()
                Else
                    SaveEditDirectorTaskDetail()
                End If
            End If
            FormPanelDirectorPhone.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
            ClearinputDirectorPhones()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Function IsDataDirectorPhoneValid() As Boolean
        If Director_cb_phone_Contact_Type.SelectedItemValue = "" Then
            Throw New Exception("Contact Type is required")
        End If
        If Director_cb_phone_Communication_Type.SelectedItemValue = "" Then
            Throw New Exception("Communication Type is required")
        End If
        If Director_txt_phone_Country_prefix.Text.Length > 3 Then
            Throw New Exception("Max Country Prefix is 3")
        End If
        If Director_txt_phone_extension.Text.Length > 9 Then
            Throw New Exception("Max Phone Extension is 9")
        End If
        If Director_txt_phone_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(Director_txt_phone_number.Text) Then
            Throw New Exception("Phone Number is Invalid")
        End If
        'End Update
        Return True
    End Function

    Sub SaveAddDirectorTaskDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Director_Phones
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Phone
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Phone = intpk

            .FK_Ref_Detail_Of = 6
            .FK_for_Table_ID = tempflag
            .Tph_Contact_Type = Director_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Director_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Director_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Director_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Director_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_txt_phone_number.Text.Trim
            .tph_extension = Director_txt_phone_extension.Text.Trim
            .comments = Director_txt_phone_comments.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_goAML_Ref_Phone = intpk
            .FK_Ref_Detail_Of = 6
            .FK_for_Table_ID = tempflag
            .Tph_Contact_Type = Director_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Director_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Director_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_txt_phone_number.Text.Trim
            .tph_extension = Director_txt_phone_extension.Text.Trim
            .comments = Director_txt_phone_comments.Text.Trim
        End With

        objListgoAML_vw_Director_Phones.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Director_Phone.Add(objNewgoAML_Customer)


        Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
        Store_DirectorPhone.DataBind()

        FormPanelDirectorPhone.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorPhones()
    End Sub

    Sub ClearinputDirectorPhones()
        Director_cb_phone_Contact_Type.SetTextValue("")
        Director_cb_phone_Communication_Type.SetTextValue("")
        Director_txt_phone_Country_prefix.Text = ""
        Director_txt_phone_number.Text = ""
        Director_txt_phone_extension.Text = ""
        Director_txt_phone_comments.Text = ""

    End Sub

    Sub SaveEditDirectorTaskDetail()

        With objTempDirectorPhoneEdit
            .Tph_Contact_Type = Director_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Director_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Director_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Director_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Director_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_txt_phone_number.Text.Trim
            .tph_extension = Director_txt_phone_extension.Text.Trim
            .comments = Director_txt_phone_comments.Text.Trim
        End With

        With objTempgoAML_Ref_Director_Phones
            .Tph_Contact_Type = Director_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Director_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Director_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_txt_phone_number.Text.Trim
            .tph_extension = Director_txt_phone_extension.Text.Trim
            .comments = Director_txt_phone_comments.Text.Trim
        End With

        objTempDirectorPhoneEdit = Nothing
        objTempgoAML_Ref_Director_Phones = Nothing

        Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
        Store_DirectorPhone.DataBind()

        FormPanelDirectorPhone.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorPhones()
    End Sub

    Protected Sub btnBackSaveDirectorPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelDirectorPhone.Hidden = True
            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
            ClearinputDirectorEmpPhones()
            ClearinputDirectorPhones()
            objTempDirectorPhoneEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ClearinputDirectorEmpPhones()
        Director_Emp_cb_phone_Contact_Type.SetTextValue("")
        Director_Emp_cb_phone_Communication_Type.SetTextValue("")
        Director_Emp_txt_phone_Country_prefix.Text = ""
        Director_Emp_txt_phone_number.Text = ""
        Director_Emp_txt_phone_extension.Text = ""
        Director_Emp_txt_phone_comments.Text = ""

    End Sub

    Protected Sub btnSaveDirectorEmpPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            If IsDataDirectorEmpPhoneValid() Then
                If objTempDirectorEmployerPhoneEdit Is Nothing Then
                    SaveAddDirectorEmpTaskDetail()
                Else
                    SaveEditDirectorEmpTaskDetail()
                End If
            End If
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SaveEditDirectorEmpTaskDetail()

        With objTempDirectorEmployerPhoneEdit
            .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_Emp_txt_phone_number.Text.Trim
            .tph_extension = Director_Emp_txt_phone_extension.Text.Trim
            .comments = Director_Emp_txt_phone_comments.Text.Trim
        End With

        With objTempgoAML_Ref_Director_EmployerPhones
            .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_Emp_txt_phone_number.Text.Trim
            .tph_extension = Director_Emp_txt_phone_extension.Text.Trim
            .comments = Director_Emp_txt_phone_comments.Text.Trim
        End With

        objTempDirectorEmployerPhoneEdit = Nothing
        objTempgoAML_Ref_Director_EmployerPhones = Nothing

        Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
        Store_Director_Employer_Phone.DataBind()

        FormPanelEmpDirectorTaskDetail.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorEmpPhones()
    End Sub

    Sub SaveAddDirectorEmpTaskDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Director_Employer_Phones
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Phone
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Phone = intpk

            .FK_Ref_Detail_Of = 7
            .FK_for_Table_ID = tempflag
            .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_Emp_txt_phone_number.Text.Trim
            .tph_extension = Director_Emp_txt_phone_extension.Text.Trim
            .comments = Director_Emp_txt_phone_comments.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_goAML_Ref_Phone = intpk
            .FK_Ref_Detail_Of = 7
            .FK_for_Table_ID = tempflag
            .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_Emp_txt_phone_number.Text.Trim
            .tph_extension = Director_Emp_txt_phone_extension.Text.Trim
            .comments = Director_Emp_txt_phone_comments.Text.Trim
        End With

        objListgoAML_vw_Director_Employer_Phones.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Director_Employer_Phone.Add(objNewgoAML_Customer)

        Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
        Store_Director_Employer_Phone.DataBind()

        FormPanelEmpDirectorTaskDetail.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorEmpPhones()
    End Sub

    Function IsDataDirectorEmpPhoneValid() As Boolean
        If Director_Emp_cb_phone_Contact_Type.SelectedItemValue = "" Then
            Throw New Exception("Contact Type is required")
        End If
        If Director_Emp_cb_phone_Communication_Type.SelectedItemValue = "" Then
            Throw New Exception("Communication Type is required")
        End If
        If Director_Emp_txt_phone_Country_prefix.Text.Length > 3 Then
            Throw New Exception("Max Country Prefix is 3")
        End If
        If Director_Emp_txt_phone_extension.Text.Length > 9 Then
            Throw New Exception("Max Phone Extension is 9")
        End If
        If Director_Emp_txt_phone_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(Director_Emp_txt_phone_number.Text) Then
            Throw New Exception("Phone Number is Invalid")
        End If
        'End Update
        Return True
    End Function


    Protected Sub btnSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataDirectorAddressValid() Then
                If objTempDirectorAddressEdit Is Nothing Then
                    SaveAddDirectorAddressDetail()
                Else
                    SaveEditDirectorAddressDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SaveEditDirectorAddressDetail()

        With objTempDirectorAddressEdit
            .Address_Type = Director_cmb_kategoriaddress.SelectedItemValue
            .Type_Address_Detail = Director_cmb_kategoriaddress.SelectedItemText
            .Address = Director_Address.Value
            .City = Director_City.Value
            .Town = Director_Town.Value
            .Country = Director_cmb_kodenegara.SelectedItemText
            .Country_Code = Director_cmb_kodenegara.SelectedItemValue
            .State = Director_State.Value
            .Zip = Director_Zip.Value
            .Comments = Director_Comment_Address.Text.Trim
        End With

        With objTempgoAML_Ref_Director_Addresses
            .Address_Type = Director_cmb_kategoriaddress.SelectedItemValue '
            .City = Director_City.Value
            .Address = Director_Address.Value
            .Town = Director_Town.Value
            .Country_Code = Director_cmb_kodenegara.SelectedItemValue
            .State = Director_State.Value
            .Zip = Director_Zip.Value
            .Comments = Director_Comment_Address.Text.Trim
        End With

        objTempDirectorAddressEdit = Nothing
        objTempgoAML_Ref_Director_Addresses = Nothing

        Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList()
        Store_DirectorAddress.DataBind()

        FP_Address_Director.Hidden = True
        WindowDetailDirectorAddress.Hidden = True
        ClearinputDirectorAddress()
    End Sub


    Function IsDataDirectorAddressValid() As Boolean
        If Director_cmb_kodenegara.SelectedItemValue = "" Then
            Throw New Exception("Country Code is required")
        End If
        If Director_cmb_kategoriaddress.SelectedItemValue = "" Then
            Throw New Exception("Address Type is required")
        End If
        If Director_Address.Text.Trim = "" Then
            Throw New Exception("Address is required")
        End If
        If Director_City.Text.Trim = "" Then
            Throw New Exception("City is required")
        End If
        Return True
    End Function

    Sub SaveAddDirectorAddressDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Director_Addresses
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Address
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 6
            .FK_To_Table_ID = tempflag
            .Address_Type = Director_cmb_kategoriaddress.SelectedItemValue
            .Address = Director_Address.Value
            .City = Director_City.Value
            .Town = Director_Town.Value
            .Country_Code = Director_cmb_kodenegara.SelectedItemValue
            .State = Director_State.Value
            .Zip = Director_Zip.Value
            .Type_Address_Detail = Director_cmb_kategoriaddress.SelectedItemText
            .Country = Director_cmb_kodenegara.SelectedItemText
            .Comments = Director_Comment_Address.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 6
            .FK_To_Table_ID = tempflag
            .Address_Type = Director_cmb_kategoriaddress.SelectedItemValue
            .City = Director_City.Value
            .Address = Director_Address.Value
            .Town = Director_Town.Value
            .Country_Code = Director_cmb_kodenegara.SelectedItemValue
            .State = Director_State.Value
            .Zip = Director_Zip.Value
            .Comments = Director_Comment_Address.Text.Trim
        End With

        objListgoAML_vw_Director_Addresses.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Director_Address.Add(objNewgoAML_Customer)

        Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList()
        Store_DirectorAddress.DataBind()

        FP_Address_Director.Hidden = True
        WindowDetailDirectorAddress.Hidden = True
        ClearinputDirectorAddress()
    End Sub

    Sub ClearinputDirectorAddress()
        Director_cmb_kategoriaddress.SetTextValue("")
        Director_cmb_kodenegara.SetTextValue("")
        Director_Address.Text = ""
        Director_Town.Text = ""
        Director_City.Text = ""
        Director_Zip.Text = ""
        Director_State.Text = ""
        Director_Comment_Address.Text = ""

    End Sub

    Protected Sub btnBackSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_Director.Hidden = True
            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
            ClearinputDirectorAddress()
            ClearinputDirectorEmpAddress()
            objTempCustomerAddressEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ClearinputDirectorEmpAddress()
        Director_cmb_emp_kategoriaddress.SetTextValue("")
        Director_cmb_emp_kodenegara.SetTextValue("")
        Director_emp_Address.Text = ""
        Director_emp_Town.Text = ""
        Director_emp_City.Text = ""
        Director_emp_Zip.Text = ""
        Director_emp_State.Text = ""
        Director_emp_Comment_Address.Text = ""

    End Sub

    Protected Sub btnSaveDirectorEmpAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataDirectorEmpAddressValid() Then
                If objTempDirectorEmployerAddressEdit Is Nothing Then
                    SaveAddDirectorEmpAddressDetail()
                Else
                    SaveEditDirectorEmpAddressDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub checkBoxcb_Director_Deceased_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_Director_Deceased.DirectCheck
        Try
            If cb_Director_Deceased.Checked Then
                txt_Director_Deceased_Date.Hidden = False
            Else
                txt_Director_Deceased_Date.Hidden = True
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Protected Sub checkBoxcb_Director_tax_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_Director_Tax_Reg_Number.DirectCheck
    '    Try
    '        If cb_Director_Tax_Reg_Number.Checked Then
    '            txt_Director_Tax_Number.Hidden = False
    '        Else
    '            txt_Director_Tax_Number.Hidden = True
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub GrdCmdDirectorPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailDirectorPhone(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditDirectorPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailDirectorPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddDirectorAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FP_Address_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            Director_cmb_kategoriaddress.IsReadOnly = False
            Director_Address.ReadOnly = False
            Director_Town.ReadOnly = False
            Director_City.ReadOnly = False
            Director_Zip.ReadOnly = False
            Director_cmb_kodenegara.IsReadOnly = False
            Director_State.ReadOnly = False
            Director_Comment_Address.ReadOnly = False
            Btn_Save_Director_Address.Hidden = False
            WindowDetailDirectorAddress.Title = "Director Address Add"

            'Director_Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Store_KategoriAddress.DataBind()

            'Director_Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Director_Store_Kode_Negara.DataBind()
            ClearinputDirectorAddress()
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDirectorEditAddress(id As Long)
        objTempgoAML_Ref_Director_Addresses = objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        objTempDirectorAddressEdit = objListgoAML_vw_Director_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)

        Dim Address_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Country As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing

        If Not objTempDirectorAddressEdit Is Nothing Then
            FP_Address_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            Btn_Save_Director_Address.Hidden = False
            Director_cmb_kategoriaddress.IsReadOnly = False
            Director_Address.ReadOnly = False
            Director_Town.ReadOnly = False
            Director_City.ReadOnly = False
            Director_Zip.ReadOnly = False
            Director_cmb_kodenegara.IsReadOnly = False
            Director_State.ReadOnly = False
            Director_Comment_Address.ReadOnly = False
            WindowDetailDirectorAddress.Title = "Address Edit"
            ClearinputDirectorAddress()
            With objTempDirectorAddressEdit
                'Director_Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_KategoriAddress.DataBind()
                Dim kategorikontak_director = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                If kategorikontak_director IsNot Nothing Then
                    Director_cmb_kategoriaddress.SetTextWithTextValue(kategorikontak_director.Kode, kategorikontak_director.Keterangan)
                End If

                Director_Address.Text = .Address
                Director_Town.Text = .Town
                Director_City.Text = .City
                Director_Zip.Text = .Zip
                'Director_Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Director_Store_Kode_Negara.DataBind()
                Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(.Country_Code)
                If kodenegara IsNot Nothing Then
                    Director_cmb_kodenegara.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
                End If

                Director_State.Text = .State
                Director_Comment_Address.Text = .Comments
            End With
        End If
    End Sub

    Protected Sub GrdCmdDirectorAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorAddressDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataDirectorEditAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailDirectorAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DeleteRecordTaskDetailDirectorAddress(id As Long)
        Dim objDelVWAddress As GoAMLDAL.Vw_Director_Addresses = objListgoAML_vw_Director_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objxDelAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID = id)

        If Not objxDelAddress Is Nothing Then
            objListgoAML_Ref_Director_Address.Remove(objxDelAddress)
        End If

        If Not objDelVWAddress Is Nothing Then
            objListgoAML_vw_Director_Addresses.Remove(objDelVWAddress)

            Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.ToList()
            Store_DirectorAddress.DataBind()
        End If

        FP_Address_INDVD.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerAddress()

    End Sub

    Sub LoadDataDirectorAddressDetail(id As Long)

        Dim objCustomerAddressDetail = objListgoAML_Ref_Director_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        If Not objCustomerAddressDetail Is Nothing Then
            FP_Address_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            Btn_Save_Director_Address.Hidden = True
            WindowDetailDirectorAddress.Title = "Address Detail"
            Director_cmb_kategoriaddress.IsReadOnly = True
            Director_Address.ReadOnly = True
            Director_Town.ReadOnly = True
            Director_City.ReadOnly = True
            Director_Zip.ReadOnly = True
            Director_cmb_kodenegara.IsReadOnly = True
            Director_State.ReadOnly = True
            Director_Comment_Address.ReadOnly = True
            ClearinputDirectorAddress()
            'Director_Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Store_KategoriAddress.DataBind()
            Dim kategorikontak_director_detail = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
            If kategorikontak_director_detail IsNot Nothing Then
                Director_cmb_kategoriaddress.SetTextWithTextValue(kategorikontak_director_detail.Kode, kategorikontak_director_detail.Keterangan)
            End If
            Director_Address.Text = objCustomerAddressDetail.Address
            Director_Town.Text = objCustomerAddressDetail.Town
            Director_City.Text = objCustomerAddressDetail.City
            Director_Zip.Text = objCustomerAddressDetail.Zip
            'Director_Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Director_Store_Kode_Negara.DataBind()
            Dim kodenegara_director_detail = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerAddressDetail.Country_Code)
            If kodenegara_director_detail IsNot Nothing Then
                Director_cmb_kodenegara.SetTextWithTextValue(kodenegara_director_detail.Kode, kodenegara_director_detail.Keterangan)
            End If

            Director_State.Text = objCustomerAddressDetail.State
            Director_Comment_Address.Text = objCustomerAddressDetail.Comments
        End If
    End Sub

    Protected Sub btnAddDirectorEmployerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try ' 2023-11-23, Nael: fixing issue dari btpn
            Dim objdirectorempphone = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
            If objdirectorempphone.Count <> 0 Then
                FormPanelEmpDirectorTaskDetail.Hidden = False
                WindowDetailDirectorPhone.Hidden = False
                btn_saveDirector_empphone.Hidden = False
                Director_Emp_cb_phone_Contact_Type.IsReadOnly = False
                Director_Emp_cb_phone_Communication_Type.IsReadOnly = False
                Director_Emp_txt_phone_Country_prefix.ReadOnly = False
                Director_Emp_txt_phone_number.ReadOnly = False
                Director_Emp_txt_phone_extension.ReadOnly = False
                Director_Emp_txt_phone_comments.ReadOnly = False
                btn_saveDirector_empphone.Hidden = False
                WindowDetailDirectorPhone.Title = "Employer Phone Add"

                Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
                Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing
                'Director_Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Emp_Store_Contact_Type.DataBind()

                'Director_Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Emp_Store_Communication_Type.DataBind()
                ClearinputDirectorEmpPhones()
            End If
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailEmpDirectorPhone(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditEmpDirectorPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailEmpDirectorPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DeleteRecordTaskDetailEmpDirectorPhone(id As Long)
        Dim objDelVWPhone As GoAMLDAL.Vw_Director_Employer_Phones = objListgoAML_vw_Director_Employer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objxDelPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        If Not objxDelPhone Is Nothing Then
            objListgoAML_Ref_Director_Employer_Phone.Remove(objxDelPhone)
        End If

        If Not objDelVWPhone Is Nothing Then
            objListgoAML_vw_Director_Employer_Phones.Remove(objDelVWPhone)

            Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.ToList()
            Store_Director_Employer_Phone.DataBind()
        End If

        FormPanelEmpDirectorTaskDetail.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorEmpPhones()
    End Sub

    Protected Sub btnAddDirectorEmployerAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim objdirectorempaddress = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList
            If objdirectorempaddress.Count <> 0 Then ' 2023-11-22, Nael: fixing issue dari bptn, button add emp phone nggak memunculkan panel, ternyata karena comparatornya salah
                FP_Address_Emp_Director.Hidden = False
                WindowDetailDirectorAddress.Hidden = False
                Director_cmb_emp_kategoriaddress.IsReadOnly = False
                Director_emp_Address.ReadOnly = False
                Director_emp_Town.ReadOnly = False
                Director_emp_City.ReadOnly = False
                Director_emp_Zip.ReadOnly = False
                Director_cmb_emp_kodenegara.IsReadOnly = False
                Director_emp_State.ReadOnly = False
                btn_Director_saveempaddress.Hidden = False
                WindowDetailDirectorAddress.Title = "Employer Address Add"

                'Director_Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_emp_KategoriAddress.DataBind()

                'Director_Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Director_Store_emp_Kode_Negara.DataBind()
                ClearinputDirectorEmpAddress()

            End If
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorEmployerAddressDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditDirectorEmployerAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailDirectorEmployerAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DeleteRecordTaskDetailDirectorEmployerAddress(id As Long)
        Dim objDelVWAddress As GoAMLDAL.Vw_Director_Employer_Addresses = objListgoAML_vw_Director_Employer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objxDelAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = id)

        If Not objxDelAddress Is Nothing Then
            objListgoAML_Ref_Director_Employer_Address.Remove(objxDelAddress)
        End If

        If Not objDelVWAddress Is Nothing Then
            objListgoAML_vw_Director_Employer_Addresses.Remove(objDelVWAddress)
            Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.ToList()
            Store_Director_Employer_Address.DataBind()
        End If

        FP_Address_Emp_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerEmpAddress()

    End Sub


    Sub LoadDataDirectorEmployerAddressDetail(id As Long)

        Dim objCustomerAddressDetail = objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        If Not objCustomerAddressDetail Is Nothing Then
            FP_Address_Emp_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            btn_Director_saveempaddress.Hidden = True
            WindowDetailDirectorAddress.Title = "Address Detail"
            Director_cmb_emp_kategoriaddress.IsReadOnly = True
            Director_emp_Address.ReadOnly = True
            Director_emp_Town.ReadOnly = True
            Director_emp_City.ReadOnly = True
            Director_emp_Zip.ReadOnly = True
            Director_cmb_emp_kodenegara.IsReadOnly = True
            Director_emp_State.ReadOnly = True
            Director_emp_Comment_Address.ReadOnly = True
            ClearinputDirectorEmpAddress()
            'Director_Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Store_emp_KategoriAddress.DataBind()
            Dim kategorikontak_director_emp = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
            If kategorikontak_director_emp IsNot Nothing Then
                Director_cmb_emp_kategoriaddress.SetTextWithTextValue(kategorikontak_director_emp.Kode, kategorikontak_director_emp.Keterangan)
            End If

            Director_emp_Address.Text = objCustomerAddressDetail.Address
            Director_emp_Town.Text = objCustomerAddressDetail.Town
            Director_emp_City.Text = objCustomerAddressDetail.City
            Director_emp_Zip.Text = objCustomerAddressDetail.Zip
            'Director_Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Director_Store_emp_Kode_Negara.DataBind()
            Dim kodenegara_director_emp = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerAddressDetail.Country_Code)
            If kodenegara_director_emp IsNot Nothing Then
                Director_cmb_emp_kodenegara.SetTextWithTextValue(kodenegara_director_emp.Kode, kodenegara_director_emp.Keterangan)
            End If

            Director_emp_State.Text = objCustomerAddressDetail.State
            Director_emp_Comment_Address.Text = objCustomerAddressDetail.Comments
        End If
    End Sub

    Sub LoadDataEditDirectorEmployerAddress(id As Long)
        objTempgoAML_Ref_Director_EmployerAddresses = objListgoAML_Ref_Director_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        objTempDirectorEmployerAddressEdit = objListgoAML_vw_Director_Employer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)

        Dim Address_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Country As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing

        If Not objTempDirectorEmployerAddressEdit Is Nothing Then
            FP_Address_Emp_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            btn_Director_saveempaddress.Hidden = False
            Director_cmb_emp_kategoriaddress.IsReadOnly = False
            Director_emp_Address.ReadOnly = False
            Director_emp_Town.ReadOnly = False
            Director_emp_City.ReadOnly = False
            Director_emp_Zip.ReadOnly = False
            Director_cmb_emp_kodenegara.IsReadOnly = False
            Director_emp_State.ReadOnly = False
            Director_emp_Comment_Address.ReadOnly = False

            WindowDetailDirectorAddress.Title = "Address Edit"
            ClearinputDirectorEmpAddress()
            With objTempDirectorEmployerAddressEdit
                'Director_Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_emp_KategoriAddress.DataBind()
                Dim kategorikontak_director_emp = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                If kategorikontak_director_emp IsNot Nothing Then
                    Director_cmb_emp_kategoriaddress.SetTextWithTextValue(kategorikontak_director_emp.Kode, kategorikontak_director_emp.Keterangan)
                End If
                Director_emp_Address.Text = .Address
                Director_emp_Town.Text = .Town
                Director_emp_City.Text = .City
                Director_emp_Zip.Text = .Zip
                'Director_Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Director_Store_emp_Kode_Negara.DataBind()
                Dim kodenegara_director_emp = goAML_CustomerBLL.GetNamaNegaraByID(.Country_Code)
                If kodenegara_director_emp IsNot Nothing Then
                    Director_cmb_emp_kodenegara.SetTextWithTextValue(kodenegara_director_emp.Kode, kodenegara_director_emp.Keterangan)
                End If

                Director_emp_State.Text = .State
                Director_emp_Comment_Address.Text = .Comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataDetailEmpDirectorPhone(id As Long)
        Dim objCustomerPhonesDetail As goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelEmpDirectorTaskDetail.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            Director_Emp_cb_phone_Contact_Type.IsReadOnly = True
            Director_Emp_cb_phone_Communication_Type.IsReadOnly = True
            Director_Emp_txt_phone_Country_prefix.ReadOnly = True
            Director_Emp_txt_phone_number.ReadOnly = True
            Director_Emp_txt_phone_extension.ReadOnly = True
            Director_Emp_txt_phone_comments.ReadOnly = True
            btn_saveDirector_empphone.Hidden = True

            WindowDetailDirectorPhone.Title = "Phone Detail"
            ClearinputDirectorEmpPhones()
            With objCustomerPhonesDetail
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_director_emp_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_director_emp_contact_type IsNot Nothing Then
                    Director_Emp_cb_phone_Contact_Type.SetTextWithTextValue(obj_director_emp_contact_type.Kode, obj_director_emp_contact_type.Keterangan)
                End If
                'Director_Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Emp_Store_Communication_Type.DataBind()
                'Director_Emp_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                Dim obj_director_emp_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_director_emp_communication_type IsNot Nothing Then
                    Director_Emp_cb_phone_Communication_Type.SetTextWithTextValue(obj_director_emp_communication_type.Kode, obj_director_emp_communication_type.Keterangan)
                End If
                'End Update
                'Director_Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Emp_Store_Contact_Type.DataBind()
                'Director_Emp_cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)

                'Director_Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Emp_Store_Communication_Type.DataBind()
                'Director_Emp_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                Director_Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_Emp_txt_phone_number.Text = .tph_number
                Director_Emp_txt_phone_extension.Text = .tph_extension
                Director_Emp_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub

    Sub LoadDataEditEmpDirectorPhone(id As Long)
        objTempgoAML_Ref_Director_EmployerPhones = objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        objTempDirectorEmployerPhoneEdit = objListgoAML_vw_Director_Employer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objTempDirectorEmployerPhoneEdit Is Nothing Then
            FormPanelEmpDirectorTaskDetail.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            Director_Emp_cb_phone_Contact_Type.IsReadOnly = False
            Director_Emp_cb_phone_Communication_Type.IsReadOnly = False
            Director_Emp_txt_phone_Country_prefix.ReadOnly = False
            Director_Emp_txt_phone_number.ReadOnly = False
            Director_Emp_txt_phone_extension.ReadOnly = False
            Director_Emp_txt_phone_comments.ReadOnly = False
            btn_saveDirector_empphone.Hidden = False

            WindowDetailDirectorPhone.Title = "Phone Edit"
            ClearinputDirectorEmpPhones()
            With objTempDirectorEmployerPhoneEdit
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_director_emp_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_director_emp_contact_type IsNot Nothing Then
                    Director_Emp_cb_phone_Contact_Type.SetTextWithTextValue(obj_director_emp_contact_type.Kode, obj_director_emp_contact_type.Keterangan)
                End If
                Dim obj_director_emp_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_director_emp_communication_type IsNot Nothing Then
                    Director_Emp_cb_phone_Communication_Type.SetTextWithTextValue(obj_director_emp_communication_type.Kode, obj_director_emp_communication_type.Keterangan)
                End If
                'End Update
                'Director_Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Emp_Store_Contact_Type.DataBind()
                'Director_Emp_cb_phone_Contact_Type.SetValue(objTempDirectorEmployerPhoneEdit.Tph_Contact_Type)

                'Director_Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Emp_Store_Communication_Type.DataBind()
                'Director_Emp_cb_phone_Communication_Type.SetValue(objTempDirectorEmployerPhoneEdit.Tph_Communication_Type)

                Director_Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_Emp_txt_phone_number.Text = .tph_number
                Director_Emp_txt_phone_extension.Text = .tph_extension
                Director_Emp_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub

    Sub DeleteRecordTaskDetailDirectorPhone(id As Long)
        Dim objDelVWPhone As GoAMLDAL.Vw_Director_Phones = objListgoAML_vw_Director_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objxDelPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        If Not objxDelPhone Is Nothing Then
            objListgoAML_Ref_Director_Phone.Remove(objxDelPhone)
        End If

        If Not objDelVWPhone Is Nothing Then
            objListgoAML_vw_Director_Phones.Remove(objDelVWPhone)

            Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.ToList()
            Store_DirectorPhone.DataBind()
        End If

        FormPanelDirectorPhone.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorPhones()
    End Sub


    Sub LoadDataEditDirectorPhone(id As Long)
        objTempgoAML_Ref_Director_Phones = objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        objTempDirectorPhoneEdit = objListgoAML_vw_Director_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objTempDirectorPhoneEdit Is Nothing Then
            FormPanelDirectorPhone.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            Director_cb_phone_Contact_Type.IsReadOnly = False
            Director_cb_phone_Communication_Type.IsReadOnly = False
            Director_txt_phone_Country_prefix.ReadOnly = False
            Director_txt_phone_number.ReadOnly = False
            Director_txt_phone_extension.ReadOnly = False
            Director_txt_phone_comments.ReadOnly = False
            btnSaveDirectorPhonedetail.Hidden = False

            WindowDetailDirectorPhone.Title = "Phone Edit"
            ClearinputDirectorPhones()
            With objTempDirectorPhoneEdit
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_director_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_director_contact_type IsNot Nothing Then
                    Director_cb_phone_Contact_Type.SetTextWithTextValue(obj_director_contact_type.Kode, obj_director_contact_type.Keterangan)
                End If
                Dim obj_director_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_director_communication_type IsNot Nothing Then
                    Director_cb_phone_Communication_Type.SetTextWithTextValue(obj_director_communication_type.Kode, obj_director_communication_type.Keterangan)
                End If
                'End Update
                'Director_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_Contact_Type.DataBind()
                'Director_cb_phone_Contact_Type.SetValue(objTempDirectorPhoneEdit.Tph_Contact_Type)

                'Director_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Store_Communication_Type.DataBind()
                'Director_cb_phone_Communication_Type.SetValue(objTempDirectorPhoneEdit.Tph_Communication_Type)

                Director_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_txt_phone_number.Text = .tph_number
                Director_txt_phone_extension.Text = .tph_extension
                Director_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub

    Sub LoadDataDetailDirectorPhone(id As Long)
        Dim objCustomerPhonesDetail As goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelDirectorPhone.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            Director_cb_phone_Contact_Type.IsReadOnly = True
            Director_cb_phone_Communication_Type.IsReadOnly = True
            Director_txt_phone_Country_prefix.ReadOnly = True
            Director_txt_phone_number.ReadOnly = True
            Director_txt_phone_extension.ReadOnly = True
            Director_txt_phone_comments.ReadOnly = True
            btnSaveDirectorPhonedetail.Hidden = True

            WindowDetailDirectorPhone.Title = "Phone Detail"
            ClearinputDirectorPhones()
            With objCustomerPhonesDetail
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_director_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_director_contact_type IsNot Nothing Then
                    Director_cb_phone_Contact_Type.SetTextWithTextValue(obj_director_contact_type.Kode, obj_director_contact_type.Keterangan)
                End If

                Dim obj_director_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_director_communication_type IsNot Nothing Then
                    Director_cb_phone_Communication_Type.SetTextWithTextValue(obj_director_communication_type.Kode, obj_director_communication_type.Keterangan)
                End If
                'End Update
                'Director_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_Contact_Type.DataBind()
                'Director_cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)

                'Director_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Store_Communication_Type.DataBind()
                'Director_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                Director_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_txt_phone_number.Text = .tph_number
                Director_txt_phone_extension.Text = .tph_extension
                Director_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub


    Protected Sub btnAddDirectorPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FormPanelDirectorPhone.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            btnSaveDirectorPhonedetail.Hidden = False
            Director_cb_phone_Contact_Type.IsReadOnly = False
            Director_cb_phone_Communication_Type.IsReadOnly = False
            Director_txt_phone_Country_prefix.ReadOnly = False
            Director_txt_phone_number.ReadOnly = False
            Director_txt_phone_extension.ReadOnly = False
            Director_txt_phone_comments.ReadOnly = False
            btnSaveDirectorPhonedetail.Hidden = False
            WindowDetailDirectorPhone.Title = "Director Phone Add"

            Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing
            'Director_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Store_Contact_Type.DataBind()

            'Director_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
            'Director_Store_Communication_Type.DataBind()
            ClearinputDirectorPhones()
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SaveAddDirectorEmpAddressDetail()

        Dim objNewVWgoAML_Customer As New GoAMLDAL.Vw_Director_Employer_Addresses
        Dim objNewgoAML_Customer As New GoAMLDAL.goAML_Ref_Address
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Director_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 7
            .FK_To_Table_ID = tempflag
            .Address_Type = Director_cmb_emp_kategoriaddress.SelectedItemValue
            .Address = Director_emp_Address.Value
            .City = Director_emp_City.Value
            .Town = Director_emp_Town.Value
            .Country_Code = Director_cmb_emp_kodenegara.SelectedItemValue
            .State = Director_emp_State.Value
            .Zip = Director_emp_Zip.Value
            .Type_Address_Detail = Director_cmb_emp_kategoriaddress.SelectedItemText
            .Country = Director_cmb_emp_kodenegara.SelectedItemText
            .Comments = Director_emp_Comment_Address.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 7
            .FK_To_Table_ID = tempflag
            .Address_Type = Director_cmb_emp_kategoriaddress.SelectedItemValue '
            .City = Director_emp_City.Value
            .Address = Director_emp_Address.Value
            .Town = Director_emp_Town.Value
            .Country_Code = Director_cmb_emp_kodenegara.SelectedItemValue
            .State = Director_emp_State.Value
            .Zip = Director_emp_Zip.Value
            .Comments = Director_emp_Comment_Address.Text.Trim
        End With

        objListgoAML_vw_Director_Employer_Addresses.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Director_Employer_Address.Add(objNewgoAML_Customer)

        Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList()
        Store_Director_Employer_Address.DataBind()

        FP_Address_Emp_Director.Hidden = True
        WindowDetailDirectorAddress.Hidden = True
        ClearinputDirectorEmpAddress()
    End Sub

    Sub SaveEditDirectorEmpAddressDetail()

        With objTempDirectorEmployerAddressEdit
            .Address_Type = Director_cmb_emp_kategoriaddress.SelectedItemValue
            .Address = Director_emp_Address.Value
            .City = Director_emp_City.Value
            .Town = Director_emp_Town.Value
            .Country_Code = Director_cmb_emp_kodenegara.SelectedItemValue
            .Country = Director_cmb_emp_kodenegara.SelectedItemText
            .State = Director_emp_State.Value
            .Zip = Director_emp_Zip.Value
            .Comments = Director_emp_Comment_Address.Text.Trim
        End With

        With objTempgoAML_Ref_Director_EmployerAddresses
            .Address_Type = Director_cmb_emp_kategoriaddress.SelectedItemValue '
            .City = Director_emp_City.Value
            .Address = Director_emp_Address.Value
            .Town = Director_emp_Town.Value
            .Country_Code = Director_cmb_emp_kodenegara.SelectedItemValue
            .State = Director_emp_State.Value
            .Zip = Director_emp_Zip.Value
            .Comments = Director_emp_Comment_Address.Text.Trim
        End With

        objTempDirectorEmployerAddressEdit = Nothing
        objTempgoAML_Ref_Director_EmployerAddresses = Nothing

        Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList()
        Store_Director_Employer_Address.DataBind()

        FP_Address_Emp_Director.Hidden = True
        WindowDetailDirectorAddress.Hidden = True
        ClearinputDirectorEmpAddress()
    End Sub


    Function IsDataDirectorEmpAddressValid() As Boolean
        If Director_cmb_emp_kodenegara.SelectedItemValue = "" Then
            Throw New Exception("Country Code is required")
        End If
        If Director_cmb_emp_kategoriaddress.SelectedItemValue = "" Then
            Throw New Exception("Address Type is required")
        End If
        If Director_emp_Address.Text.Trim = "" Then
            Throw New Exception("Address is required")
        End If
        If Director_emp_City.Text.Trim = "" Then
            Throw New Exception("City is required")
        End If
        Return True
    End Function


    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorIdentificationDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditDirectorIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailDirectorIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DeleteRecordTaskDetailDirectorIdentification(id As Long)
        Dim objDelVWIdentification As GoAMLDAL.Vw_Person_Identification = objListgoAML_vw_Customer_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = id)
        Dim objxDelIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = id)

        If Not objxDelIdentification Is Nothing Then
            objListgoAML_Ref_Identification_Director.Remove(objxDelIdentification)
        End If

        If Not objDelVWIdentification Is Nothing Then
            objListgoAML_vw_Customer_Identification_Director.Remove(objDelVWIdentification)


            Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.ToList()
            Store_Director_Identification.DataBind()
        End If

        FP_Identification_Director.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputDirectorIdentification()

    End Sub


    Sub LoadDataEditDirectorIdentification(id As Long)
        objTempgoAML_Ref_Identification_Director = objListgoAML_Ref_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = id)
        objTempCustomerIdentificationEdit_Director = objListgoAML_vw_Customer_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = id)


        If Not objTempCustomerIdentificationEdit_Director Is Nothing Then
            FP_Identification_Director.Hidden = False
            WindowDetailIdentification.Hidden = False
            btn_save_identification_Director.Hidden = False
            cmb_Jenis_Dokumen_Director.IsReadOnly = False
            txt_issued_by_Director.ReadOnly = False
            txt_issued_date_Director.ReadOnly = False
            txt_issued_expired_date_Director.ReadOnly = False
            txt_identification_comment_Director.ReadOnly = False
            cmb_issued_country_Director.IsReadOnly = False
            txt_number_identification_Director.ReadOnly = False
            WindowDetailIdentification.Title = "Identification Edit"
            ClearinputDirectorIdentification()
            With objTempCustomerIdentificationEdit_Director
                'Storejenisdokumen_Director.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
                'Storejenisdokumen_Director.DataBind()
                Dim jenisdokumen_director_edit = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(x) x.Keterangan = .Type_Document).FirstOrDefault()
                If jenisdokumen_director_edit IsNot Nothing Then
                    cmb_Jenis_Dokumen_Director.SetTextWithTextValue(jenisdokumen_director_edit.Kode, jenisdokumen_director_edit.Keterangan)
                End If
                txt_issued_by_Director.Text = .Issued_By
                txt_issued_date_Director.Value = IIf(.Issue_Date.HasValue, .Issue_Date, Nothing)
                txt_issued_expired_date_Director.Value = IIf(.Expiry_Date.HasValue, .Expiry_Date, Nothing)
                txt_identification_comment_Director.Text = .Identification_Comment
                'StoreIssueCountry_Director.DataSource = goAML_CustomerBLL.GetListNamaNegara().ToList()
                'StoreIssueCountry_Director.DataBind()
                Dim kodenegara_director_edit = goAML_CustomerBLL.GetNamaNegaraByKeterangan(.Country)
                If kodenegara_director_edit IsNot Nothing Then
                    cmb_issued_country_Director.SetTextWithTextValue(kodenegara_director_edit.Kode, kodenegara_director_edit.Keterangan)
                End If

                txt_number_identification_Director.Text = .Number
            End With

            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataDirectorIdentificationDetail(id As Long)

        Dim objCustomerIdentificationDetail = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.PK_Person_Identification_ID = id).FirstOrDefault()

        If Not objCustomerIdentificationDetail Is Nothing Then
            FP_Identification_Director.Hidden = False
            WindowDetailIdentification.Hidden = False
            btn_save_identification_Director.Hidden = True
            WindowDetailIdentification.Title = "Identification Detail"
            txt_number_identification_Director.ReadOnly = True
            cmb_Jenis_Dokumen_Director.IsReadOnly = True
            cmb_issued_country_Director.IsReadOnly = True
            txt_issued_by_Director.ReadOnly = True
            txt_issued_date_Director.ReadOnly = True
            txt_issued_expired_date_Director.ReadOnly = True
            txt_identification_comment_Director.ReadOnly = True
            ClearinputDirectorIdentification()
            txt_number_identification_Director.Text = objCustomerIdentificationDetail.Number
            'Storejenisdokumen_Director.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            'Storejenisdokumen_Director.DataBind()
            Dim jenisdokumen_director_detail = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(X) X.Keterangan = objCustomerIdentificationDetail.Type_Document).FirstOrDefault
            If jenisdokumen_director_detail IsNot Nothing Then
                cmb_Jenis_Dokumen_Director.SetTextWithTextValue(jenisdokumen_director_detail.Kode, jenisdokumen_director_detail.Keterangan)
            End If

            'StoreIssueCountry_Director.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'StoreIssueCountry_Director.DataBind()
            Dim namanegara = goAML_CustomerBLL.GetNamaNegaraByKeterangan(objCustomerIdentificationDetail.Country)
            If namanegara IsNot Nothing Then
                cmb_issued_country_Director.SetTextWithTextValue(namanegara.Kode, namanegara.Keterangan)
            End If
            txt_issued_by_Director.Text = objCustomerIdentificationDetail.Issued_By
            txt_issued_date_Director.Value = IIf(objCustomerIdentificationDetail.Issue_Date.HasValue, objCustomerIdentificationDetail.Issue_Date, Nothing)
            txt_issued_expired_date_Director.Value = IIf(objCustomerIdentificationDetail.Expiry_Date.HasValue, objCustomerIdentificationDetail.Expiry_Date, Nothing)
            txt_identification_comment_Director.Text = objCustomerIdentificationDetail.Identification_Comment
        End If
    End Sub

#Region "Add By Septian, goAML 5.0.1, 26 Jan 2023"
    Protected Sub btnAdd_Customer_Previous_Name_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            'If objListgoAML_vw_Previous_Name.Count = 0 Then
            FormPanelPreviousName.Hidden = False
            WindowDetailPreviousName.Hidden = False
            btn_Customer_SavePreviousName.Hidden = False
            txtCustomer_first_name.ReadOnly = False
            txtCustomer_last_name.ReadOnly = False
            txtCustomer_comments.ReadOnly = False

            WindowDetailPreviousName.Title = "Previous Name Add"


            'ClearinputCustomerPreviousName()
            'Else
            '    Throw New Exception("can only enter one Workplace Phone Information")
            'End If
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_Email_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_Email.Hidden = False
            FormPanel_Email.Hidden = False
            btn_Customer_Save_Email.Hidden = False

            txtCustomer_email_address.ReadOnly = False

            WindowDetail_Email.Title = "Email Add"

            objTempCustomer_Email_Edit = Nothing
            'FormPanel_Email.Reset(False)
            ClearinputCustomer_Email()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_Employment_History_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_Employment_History.Hidden = False
            FormPanel_Employment_History.Hidden = False
            btn_Customer_Save_Employment_History.Hidden = False

            txt_employer_name.ReadOnly = False
            txt_employer_business.ReadOnly = False
            txt_employer_identifier.ReadOnly = False
            df_valid_from.ReadOnly = False
            cbx_is_approx_from_date.ReadOnly = False
            df_valid_to.ReadOnly = False
            cbx_is_approx_to_date.ReadOnly = False

            WindowDetail_Employment_History.Title = "Employment History Add"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_PersonPEP_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_Person_PEP.Hidden = False
            FormPanel_Person_PEP.Hidden = False
            btn_Customer_Save_PEP.Hidden = False

            cmb_PersonPEP_Country.IsReadOnly = False
            txt_function_name.ReadOnly = False
            txt_function_description.ReadOnly = False
            df_person_pep_valid_from.ReadOnly = False
            cbx_pep_is_approx_from_date.ReadOnly = False
            df_pep_valid_to.ReadOnly = False
            cbx_pep_is_approx_to_date.ReadOnly = False
            txt_pep_comments.ReadOnly = False

            WindowDetail_Person_PEP.Title = "Person PEP Add"

            objTempCustomer_PersonPEP_Edit = Nothing
            'FormPanel_Person_PEP.Reset()
            ClearinputCustomer_PersonPEP()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_NetworkDevice_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_NetworkDevice.Hidden = False
            FormPanel_NetworkDevice.Hidden = False
            btnCustomer_Save_NetworkDevice.Hidden = False

            txt_device_number.ReadOnly = False
            cmb_network_device_os.IsReadOnly = False
            txt_service_provider.ReadOnly = False
            txt_ipv6.ReadOnly = False
            txt_ipv4.ReadOnly = False
            txt_cgn_port.ReadOnly = False
            txt_ipv4_ipv6.ReadOnly = False
            txt_nat.ReadOnly = False
            df_first_seen_date.ReadOnly = False
            df_last_seen_date.ReadOnly = False
            cbx_using_proxy.ReadOnly = False
            txt_city.ReadOnly = False
            cmb_network_device_country.IsReadOnly = False
            txt_network_device_comments.ReadOnly = False

            WindowDetail_NetworkDevice.Title = "Network Device Add"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_SocialMedia_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_SocialMedia.Hidden = False
            FormPanel_SocialMedia.Hidden = False

            btnSaveCustomer_SocialMedia.Hidden = False

            txt_socmed_platform.ReadOnly = False
            txt_socmed_username.ReadOnly = False
            txt_socmed_comments.ReadOnly = False

            WindowDetail_SocialMedia.Title = "Social Media Add"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_Sanction_DirectClick(sender As Object, e As DirectEventArgs)
        Try

            objTempCustomer_Sanction_Edit = Nothing
            objTemp_goAML_Ref_Customer_Sanction = Nothing

            WindowDetail_Sanction.Hidden = False
            FormPanel_Sanction.Hidden = False
            btnSaveCustomer_Sanction.Hidden = False

            txt_sanction_provider.ReadOnly = False
            txt_sanction_sanction_list_name.ReadOnly = False
            txt_sanction_match_criteria.ReadOnly = False
            txt_sanction_link_to_source.ReadOnly = False
            txt_sanction_sanction_list_attributes.ReadOnly = False
            df_sanction_valid_from.ReadOnly = False
            cbx_sanction_is_approx_from_date.ReadOnly = False
            df_sanction_valid_to.ReadOnly = False
            cbx_sanction_is_approx_to_date.ReadOnly = False
            txt_sanction_comments.ReadOnly = False

            WindowDetail_Sanction.Title = "Sanction Add"

            objTempCustomer_Sanction_Edit = Nothing
            'FormPanel_Sanction.Reset()
            ClearinputCustomer_Sanction()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_RelatedPerson_DirectClick(sender As Object, e As DirectEventArgs)
        Try

            GridAddressWork_RelatedPerson.IsViewMode = False
            GridAddress_RelatedPerson.IsViewMode = False
            GridPhone_RelatedPerson.IsViewMode = False
            GridPhoneWork_RelatedPerson.IsViewMode = False
            GridIdentification_RelatedPerson.IsViewMode = False
            GridEmail_RelatedPerson.IsViewMode = False
            GridSanction_RelatedPerson.IsViewMode = False
            GridPEP_RelatedPerson.IsViewMode = False

            GridAddressWork_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Address))
            GridAddress_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Address))
            GridPhone_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Phone))
            GridPhoneWork_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Phone))
            GridIdentification_RelatedPerson.LoadData(New List(Of DataModel.goAML_Person_Identification))
            GridEmail_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Customer_Email))
            GridSanction_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Customer_Sanction))
            GridPEP_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Customer_PEP))

            'GridAddress_RelatedPerson.

            WindowDetail_RelatedPerson.Hidden = False
            FormPanel_RelatedPerson.Hidden = False
            btnSaveCustomer_RelatedPerson.Hidden = False

            cmb_rp_person_relation.IsReadOnly = False
            'df_relation_date_range_valid_from.ReadOnly = False
            'cbx_relation_date_range_is_approx_from_date.ReadOnly = False
            'df_relation_date_range_valid_to.ReadOnly = False
            'cbx_relation_date_range_is_approx_to_date.ReadOnly = False
            txt_rp_comments.ReadOnly = False
            txt_rp_relation_comments.ReadOnly = False
            cmb_rp_gender.IsReadOnly = False
            txt_rp_title.ReadOnly = False
            'txt_rp_first_name.ReadOnly = False
            'txt_rp_middle_name.ReadOnly = False
            'txt_rp_prefix.ReadOnly = False
            txt_rp_last_name.ReadOnly = False
            df_birthdate.ReadOnly = False
            txt_rp_birth_place.ReadOnly = False
            'cmb_rp_country_of_birth.IsReadOnly = False
            txt_rp_mothers_name.ReadOnly = False
            txt_rp_alias.ReadOnly = False
            'txt_rp_full_name_frn.ReadOnly = False
            txt_rp_ssn.ReadOnly = False
            txt_rp_passport_number.ReadOnly = False
            cmb_rp_passport_country.IsReadOnly = False
            txt_rp_id_number.ReadOnly = False
            cmb_rp_nationality1.IsReadOnly = False
            cmb_rp_nationality2.IsReadOnly = False
            cmb_rp_nationality3.IsReadOnly = False
            cmb_rp_residence.IsReadOnly = False
            'df_rp_residence_since.ReadOnly = False
            txt_rp_occupation.ReadOnly = False
            txt_rp_employer_name.ReadOnly = False
            cbx_rp_deceased.ReadOnly = False
            df_rp_date_deceased.ReadOnly = False
            txt_rp_tax_number.ReadOnly = False
            cbx_rp_tax_reg_number.ReadOnly = False
            txt_rp_source_of_wealth.ReadOnly = False
            cbx_rp_is_protected.ReadOnly = False

            WindowDetail_RelatedPerson.Title = "Related Person Add"

            objTempCustomer_RelatedPerson_Edit = Nothing
            'FormPanel_RelatedPerson.Reset()
            ClearinputCustomer_RelatedPerson()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_AdditionalInfo_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_AdditionalInfo.Hidden = False
            FormPanel_AdditionalInfo.Hidden = False
            btnSaveCustomer_AdditionalInfo.Hidden = False

            cmb_info_type.IsReadOnly = False
            txt_info_subject.ReadOnly = False
            txt_info_text.ReadOnly = False
            nfd_info_numeric.ReadOnly = False
            df_info_date.ReadOnly = False
            cbx_info_boolean.ReadOnly = False

            WindowDetail_AdditionalInfo.Title = "Additional Info Add"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_Url_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_URL.Hidden = False
            FormPanel_URL.Hidden = False
            btnSaveCustomer_URL.Hidden = False

            txt_url.ReadOnly = False

            WindowDetail_URL.Title = "Customer Url Add"

            objTempCustomer_EntityUrl_Edit = Nothing
            ClearinputCustomer_Url()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_EntityIdentification_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_EntityIdentification.Hidden = False
            FormPanel_EntityIdentification.Hidden = False
            btnSaveCustomer_EntityIdentification.Hidden = False

            cmb_ef_type.IsReadOnly = False
            txt_ef_number.ReadOnly = False
            df_issue_date.ReadOnly = False
            df_expiry_date.ReadOnly = False
            txt_ef_issued_by.ReadOnly = False
            cmb_ef_issue_country.IsReadOnly = False
            txt_ef_comments.ReadOnly = False

            WindowDetail_EntityIdentification.Title = "Entity Identification Add"

            objTempCustomer_EntityIdentification_Edit = Nothing
            'FormPanel_EntityIdentification.Reset()
            ClearinputCustomer_EntityIdentification()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_Customer_RelatedEntities_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            GridPhone_RelatedEntity.IsViewMode = False
            GridAddress_RelatedEntity.IsViewMode = False
            GridEmail_RelatedEntity.IsViewMode = False
            GridEntityIdentification_RelatedEntity.IsViewMode = False
            GridURL_RelatedEntity.IsViewMode = False
            GridSanction_RelatedEntity.IsViewMode = False

            GridPhone_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Phone))
            GridAddress_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Address))
            GridEmail_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_Email))
            GridEntityIdentification_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            GridURL_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_URL))
            GridSanction_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_Sanction))

            WindowDetail_RelatedEntities.Hidden = False
            FormPanel_RelatedEntities.Hidden = False
            btnSaveCustomer_EntityRelation.Hidden = False

            cmb_re_entity_relation.IsReadOnly = False
            'df_re_relation_date_range_valid_from.ReadOnly = False
            'cbx_re_relation_date_range_is_approx_from_date.ReadOnly = False
            'df_re_relation_date_range_valid_to.ReadOnly = False
            'cbx_re_relation_date_range_is_approx_to_date.ReadOnly = False
            'nfd_re_share_percentage.ReadOnly = False
            txt_re_comments.ReadOnly = False
            txt_re_relation_comments.ReadOnly = False
            txt_re_name.ReadOnly = False
            txt_re_commercial_name.ReadOnly = False
            cmb_re_incorporation_legal_form.IsReadOnly = False
            txt_re_incorporation_number.ReadOnly = False
            txt_re_business.ReadOnly = False
            'cmb_re_entity_status.IsReadOnly = False
            'df_re_entity_status_date.ReadOnly = False
            txt_re_incorporation_state.ReadOnly = False
            cmb_re_incorporation_country_code.IsReadOnly = False
            df_re_incorporation_date.ReadOnly = False
            cbx_re_business_closed.ReadOnly = False
            df_re_date_business_closed.ReadOnly = False
            txt_re_tax_number.ReadOnly = False
            txt_re_tax_reg_number.ReadOnly = False

            WindowDetail_RelatedEntities.Title = "Related Entities Add"
            objTempCustomer_RelatedEntities_Edit = Nothing
            'FormPanel_RelatedEntities.Reset(True)
            ClearinputCustomer_RelatedEntities()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_PreviousName_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailPreviousName.Hidden = True
            FormPanelPreviousName.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_Email_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Email.Hidden = True
            FormPanel_Email.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_EmploymentHistory_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Employment_History.Hidden = True
            FormPanel_Employment_History.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_PersonPEP_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Person_PEP.Hidden = True
            FormPanel_Person_PEP.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_NetworkDevice_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_NetworkDevice.Hidden = True
            FormPanel_NetworkDevice.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_SocialMedia_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_SocialMedia.Hidden = True
            FormPanel_SocialMedia.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_Sanction_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Sanction.Hidden = True
            FormPanel_Sanction.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_RelatedPerson_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_RelatedPerson.Hidden = True
            FormPanel_RelatedPerson.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_AdditionalInfo_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_AdditionalInfo.Hidden = True
            FormPanel_AdditionalInfo.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_Url_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_URL.Hidden = True
            FormPanel_URL.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_EntityIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_EntityIdentification.Hidden = True
            FormPanel_EntityIdentification.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_RelatedEntities_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_RelatedEntities.Hidden = True
            FormPanel_RelatedEntities.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_PreviousName_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_PreviousName_Valid() Then
                If objTempCustomerPreviousNameEdit Is Nothing Then
                    SaveAddPreviousName()
                Else
                    'SaveEditPreviousName()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_Email_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_Email_Valid() Then
                If objTempCustomer_Email_Edit Is Nothing Then
                    Save_Email()
                Else
                    Save_Email(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_EmploymentHistory_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_EmploymentHistory_Valid() Then
                If objTempCustomer_EmploymentHistory_Edit Is Nothing Then
                    SaveAdd_EmploymentHistory()
                Else
                    'SaveEditPreviousName()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_PersonPEP_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_PEP_Valid() Then
                If objTempCustomer_PersonPEP_Edit Is Nothing Then
                    Save_PEP()
                Else
                    Save_PEP(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_NetworkDevice_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_NetworkDevice_Valid() Then
                If objTempCustomer_NetworkDevice_Edit Is Nothing Then
                    SaveAdd_NetworkDevice()
                Else
                    'SaveEditPreviousName()
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_SocialMedia_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_SocialMedia_Valid() Then
                If objTempCustomer_SocialMedia_Edit Is Nothing Then
                    SaveAdd_SocialMedia()
                Else
                    'SaveEditPreviousName()
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_Sanction_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_Sanction_Valid() Then
                If objTempCustomer_Sanction_Edit Is Nothing Then
                    Save_Sanction()
                Else
                    Save_Sanction(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_RelatedPerson_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_RelatedPerson_Valid() Then
                If objTempCustomer_RelatedPerson_Edit Is Nothing Then
                    Save_RelatedPerson()
                Else
                    Save_RelatedPerson(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_AdditionalInfo_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_AdditionalInformation_Valid() Then
                If objTempCustomer_AdditionalInfo_Edit Is Nothing Then
                    SaveAdd_AdditionalInfo()
                Else
                    'SaveEditPreviousName()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_Url_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_URL_Valid() Then
                If objTempCustomer_EntityUrl_Edit Is Nothing Then
                    Save_Url()
                Else
                    Save_Url(False)
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_EntityIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_EntityIdentification_Valid() Then
                If objTempCustomer_EntityIdentification_Edit Is Nothing Then
                    Save_EntityIdentification()
                Else
                    Save_EntityIdentification(False)
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomer_RelatedEntities_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomer_RelatedEntities_Valid() Then
                If objTempCustomer_RelatedEntities_Edit Is Nothing Then
                    Save_RelatedEntities()
                Else
                    Save_RelatedEntities(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SaveAddPreviousName()
        Try
            Dim objNewVWgoAML_PreviousName As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name
            Dim objNewgoAML_PreviousName As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name
            Dim objRand As New Random

            Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
            While Not objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = intpk) Is Nothing
                intpk = objRand.Next
            End While

            With objNewVWgoAML_PreviousName
                .PK_goAML_Ref_Customer_Previous_Name_ID = intpk
                .CIF = txt_CIF.Text
                .FIRST_NAME = txtCustomer_first_name.Value
                .LAST_NAME = txtCustomer_last_name.Value
                .COMMENTS = txtCustomer_comments.Value
            End With

            With objNewgoAML_PreviousName
                .PK_goAML_Ref_Customer_Previous_Name_ID = intpk
                .CIF = txt_CIF.Text
                .FIRST_NAME = txtCustomer_first_name.Value
                .LAST_NAME = txtCustomer_last_name.Value
                .COMMENTS = txtCustomer_comments.Value
            End With

            objListgoAML_vw_Previous_Name.Add(objNewVWgoAML_PreviousName)
            objListgoAML_Ref_Previous_Name.Add(objNewgoAML_PreviousName)

            Store_Customer_Previous_Name.DataSource = objListgoAML_vw_Previous_Name.ToList()
            Store_Customer_Previous_Name.DataBind()

            WindowDetailPreviousName.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub Save_Email(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempCustomer_Email_Edit
                    .CIF = txt_CIF.Text
                    .EMAIL_ADDRESS = txtCustomer_email_address.Value
                End With

                With objTemp_goAML_Ref_Customer_Email
                    .CIF = txt_CIF.Text
                    .EMAIL_ADDRESS = txtCustomer_email_address.Value
                End With

                objTempCustomer_Email_Edit = Nothing
                objTemp_goAML_Ref_Customer_Email = Nothing
            Else
                Dim objNewVWgoAML_Email As New DataModel.goAML_Ref_Customer_Email
                Dim objNewgoAML_Email As New DataModel.goAML_Ref_Customer_Email
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_Email.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_Email
                    .PK_goAML_Ref_Customer_Email_ID = intpk
                    .CIF = txt_CIF.Text
                    .EMAIL_ADDRESS = txtCustomer_email_address.Value
                    .FK_REF_DETAIL_OF = 1 ' 2023-10-10, Nael
                End With

                With objNewgoAML_Email
                    .PK_goAML_Ref_Customer_Email_ID = intpk
                    .CIF = txt_CIF.Text
                    .EMAIL_ADDRESS = txtCustomer_email_address.Value
                    .FK_REF_DETAIL_OF = 1 ' 2023-10-10, Nael
                End With

                objListgoAML_vw_Email.Add(objNewVWgoAML_Email)
                objListgoAML_Ref_Email.Add(objNewgoAML_Email)
            End If


            Store_Customer_Email.DataSource = objListgoAML_vw_Email.ToList()
            Store_Customer_Email.DataBind()

            Store_corp_email.DataSource = objListgoAML_vw_Email.ToList()
            Store_corp_email.DataBind()

            WindowDetail_Email.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub SaveAdd_EmploymentHistory()
        Try
            Dim objNewVWgoAML_EmploymentHistory As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History
            Dim objNewgoAML_EmploymentHistory As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History
            Dim objRand As New Random

            Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
            While Not objListgoAML_Ref_EmploymentHistory.Find(Function(x) x.PK_goAML_Ref_Customer_Employment_History_ID = intpk) Is Nothing
                intpk = objRand.Next
            End While

            With objNewVWgoAML_EmploymentHistory
                .PK_goAML_Ref_Customer_Employment_History_ID = intpk
                .CIF = txt_CIF.Text
                .EMPLOYER_NAME = txt_employer_name.Value
                .EMPLOYER_BUSINESS = txt_employer_business.Value
                .EMPLOYER_IDENTIFIER = txt_employer_identifier.Value
                .EMPLOYMENT_PERIOD_VALID_FROM = df_valid_from.Value
                .EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE = cbx_is_approx_from_date.Value
                .EMPLOYMENT_PERIOD_VALID_TO = df_valid_to.Value
                .EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE = cbx_is_approx_to_date.Value
            End With

            With objNewgoAML_EmploymentHistory
                .PK_goAML_Ref_Customer_Employment_History_ID = intpk
                .CIF = txt_CIF.Text
                .EMPLOYER_NAME = txt_employer_name.Value
                .EMPLOYER_BUSINESS = txt_employer_business.Value
                .EMPLOYER_IDENTIFIER = txt_employer_identifier.Value
                .EMPLOYMENT_PERIOD_VALID_FROM = df_valid_from.Value
                .EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE = cbx_is_approx_from_date.Value
                .EMPLOYMENT_PERIOD_VALID_TO = df_valid_to.Value
                .EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE = cbx_is_approx_to_date.Value
            End With

            objListgoAML_vw_EmploymentHistory.Add(objNewVWgoAML_EmploymentHistory)
            objListgoAML_Ref_EmploymentHistory.Add(objNewgoAML_EmploymentHistory)

            Store_Customer_Employment_History.DataSource = objListgoAML_vw_EmploymentHistory.ToList()
            Store_Customer_Employment_History.DataBind()

            WindowDetail_Employment_History.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub Save_PEP(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempCustomer_PersonPEP_Edit
                    .CIF = txt_CIF.Text
                    .PEP_COUNTRY = cmb_PersonPEP_Country.SelectedItemValue
                    .FUNCTION_NAME = txt_function_name.Value
                    .FUNCTION_DESCRIPTION = txt_function_description.Value
                    .PEP_DATE_RANGE_VALID_FROM = df_person_pep_valid_from.Value
                    .PEP_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_pep_is_approx_from_date.Value
                    .PEP_DATE_RANGE_VALID_TO = df_pep_valid_to.Value
                    .PEP_DATE_RANGE_IS_APPROX_TO_DATE = cbx_pep_is_approx_to_date.Value
                    .COMMENTS = txt_pep_comments.Value
                End With

                With objTemp_goAML_Ref_Customer_PEP
                    .CIF = txt_CIF.Text
                    .PEP_COUNTRY = cmb_PersonPEP_Country.SelectedItemValue
                    .FUNCTION_NAME = txt_function_name.Value
                    .FUNCTION_DESCRIPTION = txt_function_description.Value
                    .PEP_DATE_RANGE_VALID_FROM = df_person_pep_valid_from.Value
                    .PEP_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_pep_is_approx_from_date.Value
                    .PEP_DATE_RANGE_VALID_TO = df_pep_valid_to.Value
                    .PEP_DATE_RANGE_IS_APPROX_TO_DATE = cbx_pep_is_approx_to_date.Value
                    .COMMENTS = txt_pep_comments.Value
                End With

                objTempCustomer_PersonPEP_Edit = Nothing
                objTemp_goAML_Ref_Customer_PEP = Nothing
            Else
                Dim objNewVWgoAML_PersonPEP As New DataModel.goAML_Ref_Customer_PEP
                Dim objNewgoAML_PersonPEP As New DataModel.goAML_Ref_Customer_PEP
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_PersonPEP.Find(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_PersonPEP
                    .PK_goAML_Ref_Customer_PEP_ID = intpk
                    .CIF = txt_CIF.Text
                    .PEP_COUNTRY = cmb_PersonPEP_Country.SelectedItemValue
                    .FUNCTION_NAME = txt_function_name.Value
                    .FUNCTION_DESCRIPTION = txt_function_description.Value
                    .PEP_DATE_RANGE_VALID_FROM = df_person_pep_valid_from.Value
                    .PEP_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_pep_is_approx_from_date.Value
                    .PEP_DATE_RANGE_VALID_TO = df_pep_valid_to.Value
                    .PEP_DATE_RANGE_IS_APPROX_TO_DATE = cbx_pep_is_approx_to_date.Value
                    .COMMENTS = txt_pep_comments.Value
                    .FK_REF_DETAIL_OF = 1 ' 2023-10-10, Nael
                End With

                With objNewgoAML_PersonPEP
                    .PK_goAML_Ref_Customer_PEP_ID = intpk
                    .CIF = txt_CIF.Text
                    .PEP_COUNTRY = cmb_PersonPEP_Country.SelectedItemValue
                    .FUNCTION_NAME = txt_function_name.Value
                    .FUNCTION_DESCRIPTION = txt_function_description.Value
                    .PEP_DATE_RANGE_VALID_FROM = df_person_pep_valid_from.Value
                    .PEP_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_pep_is_approx_from_date.Value
                    .PEP_DATE_RANGE_VALID_TO = df_pep_valid_to.Value
                    .PEP_DATE_RANGE_IS_APPROX_TO_DATE = cbx_pep_is_approx_to_date.Value
                    .COMMENTS = txt_pep_comments.Value
                    .FK_REF_DETAIL_OF = 1 ' 2023-10-10, Nael
                End With

                objListgoAML_vw_PersonPEP.Add(objNewVWgoAML_PersonPEP)
                objListgoAML_Ref_PersonPEP.Add(objNewgoAML_PersonPEP)
            End If

            Store_Customer_Person_PEP.DataSource = objListgoAML_vw_PersonPEP.ToList()
            Store_Customer_Person_PEP.DataBind()

            WindowDetail_Person_PEP.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub SaveAdd_NetworkDevice()
        Try
            Dim objNewVWgoAML_Network_Devices As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device
            Dim objNewgoAML_Network_Devices As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device
            Dim objRand As New Random

            Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
            While Not objListgoAML_Ref_NetworkDevice.Find(Function(x) x.PK_goAML_Ref_Customer_Network_Device_ID = intpk) Is Nothing
                intpk = objRand.Next
            End While

            With objNewVWgoAML_Network_Devices
                .PK_goAML_Ref_Customer_Network_Device_ID = intpk
                .CIF = txt_CIF.Text
                .DEVICE_NUMBER = txt_device_number.Value
                .OPERATING_SYSTEM = cmb_network_device_os.SelectedItemValue
                .SERVICE_PROVIDER = txt_service_provider.Value
                .IPV6 = txt_ipv6.Value
                .IPV4 = txt_ipv4.Value
                .CGN_PORT = txt_cgn_port.Value
                .IPV4_IPV6 = txt_ipv4_ipv6.Value
                .NAT = txt_nat.Value
                .FIRST_SEEN_DATE = df_first_seen_date.Value
                .LAST_SEEN_DATE = df_last_seen_date.Value
                .USING_PROXY = cbx_using_proxy.Value
                .CITY = txt_city.Value
                .COUNTRY = cmb_network_device_country.SelectedItemValue
                .COMMENTS = txt_network_device_comments.Value
            End With

            With objNewgoAML_Network_Devices
                .PK_goAML_Ref_Customer_Network_Device_ID = intpk
                .CIF = txt_CIF.Text
                .DEVICE_NUMBER = txt_device_number.Value
                .OPERATING_SYSTEM = cmb_network_device_os.SelectedItemValue
                .SERVICE_PROVIDER = txt_service_provider.Value
                .IPV6 = txt_ipv6.Value
                .IPV4 = txt_ipv4.Value
                .CGN_PORT = txt_cgn_port.Value
                .IPV4_IPV6 = txt_ipv4_ipv6.Value
                .NAT = txt_nat.Value
                .FIRST_SEEN_DATE = df_first_seen_date.Value
                .LAST_SEEN_DATE = df_last_seen_date.Value
                .USING_PROXY = cbx_using_proxy.Value
                .CITY = txt_city.Value
                .COUNTRY = cmb_network_device_country.SelectedItemValue
                .COMMENTS = txt_network_device_comments.Value
            End With

            objListgoAML_vw_NetworkDevice.Add(objNewVWgoAML_Network_Devices)
            objListgoAML_Ref_NetworkDevice.Add(objNewgoAML_Network_Devices)

            Store_Customer_Network_Device.DataSource = objListgoAML_vw_NetworkDevice.ToList()
            Store_Customer_Network_Device.DataBind()

            StoreCorp_Network_Device.DataSource = objListgoAML_vw_NetworkDevice.ToList()
            StoreCorp_Network_Device.DataBind()

            WindowDetail_NetworkDevice.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
    Sub SaveAdd_SocialMedia()
        Try
            Dim objNewVWgoAML_SocialMedia As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media
            Dim objNewgoAML_SocialMedia As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media
            Dim objRand As New Random

            Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
            While Not objListgoAML_Ref_SocialMedia.Find(Function(x) x.PK_goAML_Ref_Customer_Social_Media_ID = intpk) Is Nothing
                intpk = objRand.Next
            End While

            With objNewVWgoAML_SocialMedia
                .PK_goAML_Ref_Customer_Social_Media_ID = intpk
                .CIF = txt_CIF.Text
                .PLATFORM = txt_socmed_platform.Value
                .USER_NAME = txt_socmed_username.Value
                .COMMENTS = txt_socmed_comments.Value
            End With

            With objNewgoAML_SocialMedia
                .PK_goAML_Ref_Customer_Social_Media_ID = intpk
                .CIF = txt_CIF.Text
                .PLATFORM = txt_socmed_platform.Value
                .USER_NAME = txt_socmed_username.Value
                .COMMENTS = txt_socmed_comments.Value
            End With

            objListgoAML_vw_SocialMedia.Add(objNewVWgoAML_SocialMedia)
            objListgoAML_Ref_SocialMedia.Add(objNewgoAML_SocialMedia)

            Store_Customer_Social_Media.DataSource = objListgoAML_vw_SocialMedia.ToList()
            Store_Customer_Social_Media.DataBind()

            WindowDetail_SocialMedia.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub Save_Sanction(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempCustomer_Sanction_Edit
                    .CIF = txt_CIF.Text
                    .PROVIDER = txt_sanction_provider.Value
                    .SANCTION_LIST_NAME = txt_sanction_sanction_list_name.Value
                    .MATCH_CRITERIA = txt_sanction_match_criteria.Value
                    .LINK_TO_SOURCE = txt_sanction_link_to_source.Value
                    .SANCTION_LIST_ATTRIBUTES = txt_sanction_sanction_list_attributes.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_FROM = df_sanction_valid_from.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_sanction_is_approx_from_date.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_TO = df_sanction_valid_to.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = cbx_sanction_is_approx_to_date.Value
                    .COMMENTS = txt_sanction_comments.Value
                End With

                With objTemp_goAML_Ref_Customer_Sanction
                    .CIF = txt_CIF.Text
                    .PROVIDER = txt_sanction_provider.Value
                    .SANCTION_LIST_NAME = txt_sanction_sanction_list_name.Value
                    .MATCH_CRITERIA = txt_sanction_match_criteria.Value
                    .LINK_TO_SOURCE = txt_sanction_link_to_source.Value
                    .SANCTION_LIST_ATTRIBUTES = txt_sanction_sanction_list_attributes.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_FROM = df_sanction_valid_from.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_sanction_is_approx_from_date.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_TO = df_sanction_valid_to.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = cbx_sanction_is_approx_to_date.Value
                    .COMMENTS = txt_sanction_comments.Value
                End With


            Else


                Dim objNewVWgoAML_Sanction As New DataModel.goAML_Ref_Customer_Sanction
                Dim objNewgoAML_Sanction As New DataModel.goAML_Ref_Customer_Sanction
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_Sanction.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_Sanction
                    .PK_goAML_Ref_Customer_Sanction_ID = intpk
                    .CIF = txt_CIF.Text
                    .PROVIDER = txt_sanction_provider.Value
                    .SANCTION_LIST_NAME = txt_sanction_sanction_list_name.Value
                    .MATCH_CRITERIA = txt_sanction_match_criteria.Value
                    .LINK_TO_SOURCE = txt_sanction_link_to_source.Value
                    .SANCTION_LIST_ATTRIBUTES = txt_sanction_sanction_list_attributes.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_FROM = df_sanction_valid_from.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_sanction_is_approx_from_date.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_TO = df_sanction_valid_to.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = cbx_sanction_is_approx_to_date.Value
                    .COMMENTS = txt_sanction_comments.Value
                    .FK_REF_DETAIL_OF = 1 ' 2023-10-10, Nael
                End With

                With objNewgoAML_Sanction
                    .PK_goAML_Ref_Customer_Sanction_ID = intpk
                    .CIF = txt_CIF.Text
                    .PROVIDER = txt_sanction_provider.Value
                    .SANCTION_LIST_NAME = txt_sanction_sanction_list_name.Value
                    .MATCH_CRITERIA = txt_sanction_match_criteria.Value
                    .LINK_TO_SOURCE = txt_sanction_link_to_source.Value
                    .SANCTION_LIST_ATTRIBUTES = txt_sanction_sanction_list_attributes.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_FROM = df_sanction_valid_from.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_sanction_is_approx_from_date.Value
                    .SANCTION_LIST_DATE_RANGE_VALID_TO = df_sanction_valid_to.Value
                    .SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = cbx_sanction_is_approx_to_date.Value
                    .COMMENTS = txt_sanction_comments.Value
                    .FK_REF_DETAIL_OF = 1 ' 2023-10-10, Nael
                End With

                objListgoAML_vw_Sanction.Add(objNewgoAML_Sanction)
                objListgoAML_Ref_Sanction.Add(objNewgoAML_Sanction)
            End If


            Store_Customer_Sanction.DataSource = objListgoAML_vw_Sanction.ToList()
            Store_Customer_Sanction.DataBind()

            Store_corp_sanction.DataSource = objListgoAML_vw_Sanction.ToList()
            Store_corp_sanction.DataBind()

            WindowDetail_Sanction.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub Save_RelatedPerson(Optional isNew As Boolean = True)
        Try
            If Not isNew Then

                With objTempCustomer_RelatedPerson_Edit
                    .CIF = txt_CIF.Text
                    .PERSON_PERSON_RELATION = cmb_rp_person_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_relation_date_range_is_approx_to_date.Value
                    .COMMENTS = txt_rp_comments.Value
                    .RELATION_COMMENTS = txt_rp_relation_comments.Value
                    .GENDER = cmb_rp_gender.SelectedItemValue
                    .TITLE = txt_rp_title.Value
                    '.FIRST_NAME = txt_rp_first_name.Value
                    '.MIDDLE_NAME = txt_rp_middle_name.Value
                    '.PREFIX = txt_rp_prefix.Value
                    .LAST_NAME = txt_rp_last_name.Value
                    If df_birthdate.RawValue IsNot Nothing Then
                        .BIRTHDATE = df_birthdate.Value
                    End If
                    .BIRTH_PLACE = txt_rp_birth_place.Value
                    '.COUNTRY_OF_BIRTH = cmb_rp_country_of_birth.SelectedItemValue
                    .MOTHERS_NAME = txt_rp_mothers_name.Value
                    .ALIAS = txt_rp_alias.Value
                    '.FULL_NAME_FRN = txt_rp_full_name_frn.Value
                    .SSN = txt_rp_ssn.Value
                    .PASSPORT_NUMBER = txt_rp_passport_number.Value
                    .PASSPORT_COUNTRY = cmb_rp_passport_country.SelectedItemValue
                    .ID_NUMBER = txt_rp_id_number.Value
                    .NATIONALITY1 = cmb_rp_nationality1.SelectedItemValue
                    .NATIONALITY2 = cmb_rp_nationality2.SelectedItemValue
                    .NATIONALITY3 = cmb_rp_nationality3.SelectedItemValue
                    .RESIDENCE = cmb_rp_residence.SelectedItemValue
                    '.RESIDENCE_SINCE = df_rp_residence_since.Value
                    .OCCUPATION = txt_rp_occupation.Value
                    .EMPLOYER_NAME = txt_rp_employer_name.Value
                    .DECEASED = cbx_rp_deceased.Value
                    If df_rp_date_deceased.RawValue IsNot Nothing Then
                        .DATE_DECEASED = df_rp_date_deceased.Value
                    End If
                    .TAX_NUMBER = txt_rp_tax_number.Value
                    .TAX_REG_NUMBER = cbx_rp_tax_reg_number.Value
                    .SOURCE_OF_WEALTH = txt_rp_source_of_wealth.Value
                    .IS_PROTECTED = cbx_rp_is_protected.Value

                    ' 2023-10-11, Nael: Karena related person memiliki FK_Person_Type = 31, di component grid-nya default 1, jadi diset di sini
                    Dim relPersonIdentification = GridIdentification_RelatedPerson.objListgoAML_Ref.Select(Function(x)
                                                                                                               x.FK_Person_Type = 31
                                                                                                               Return x
                                                                                                           End Function).ToList()

                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, relPersonIdentification)
                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridSanction_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPEP_RelatedPerson.objListgoAML_Ref)
                End With

                With objTemp_goAML_Ref_Customer_Related_Person
                    .CIF = txt_CIF.Text
                    .PERSON_PERSON_RELATION = cmb_rp_person_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_relation_date_range_is_approx_to_date.Value
                    .COMMENTS = txt_rp_comments.Value
                    .RELATION_COMMENTS = txt_rp_relation_comments.Value
                    .GENDER = cmb_rp_gender.SelectedItemValue
                    .TITLE = txt_rp_title.Value
                    '.FIRST_NAME = txt_rp_first_name.Value
                    '.MIDDLE_NAME = txt_rp_middle_name.Value
                    '.PREFIX = txt_rp_prefix.Value
                    .LAST_NAME = txt_rp_last_name.Value
                    If df_birthdate.RawValue IsNot Nothing Then
                        .BIRTHDATE = df_birthdate.Value
                    End If
                    .BIRTH_PLACE = txt_rp_birth_place.Value
                    '.COUNTRY_OF_BIRTH = cmb_rp_country_of_birth.SelectedItemValue
                    .MOTHERS_NAME = txt_rp_mothers_name.Value
                    .ALIAS = txt_rp_alias.Value
                    '.FULL_NAME_FRN = txt_rp_full_name_frn.Value
                    .SSN = txt_rp_ssn.Value
                    .PASSPORT_NUMBER = txt_rp_passport_number.Value
                    .PASSPORT_COUNTRY = cmb_rp_passport_country.SelectedItemValue
                    .ID_NUMBER = txt_rp_id_number.Value
                    .NATIONALITY1 = cmb_rp_nationality1.SelectedItemValue
                    .NATIONALITY2 = cmb_rp_nationality2.SelectedItemValue
                    .NATIONALITY3 = cmb_rp_nationality3.SelectedItemValue
                    .RESIDENCE = cmb_rp_residence.SelectedItemValue
                    '.RESIDENCE_SINCE = df_rp_residence_since.Value
                    .OCCUPATION = txt_rp_occupation.Value
                    .EMPLOYER_NAME = txt_rp_employer_name.Value
                    .DECEASED = cbx_rp_deceased.Value
                    If df_rp_date_deceased.RawValue IsNot Nothing Then
                        .DATE_DECEASED = df_rp_date_deceased.Value
                    End If
                    .TAX_NUMBER = txt_rp_tax_number.Value
                    .TAX_REG_NUMBER = cbx_rp_tax_reg_number.Value
                    .SOURCE_OF_WEALTH = txt_rp_source_of_wealth.Value
                    .IS_PROTECTED = cbx_rp_is_protected.Value

                    ' 2023-10-11, Nael: Karena related person memiliki FK_Person_Type = 31, di component grid-nya default 1, jadi diset di sini
                    Dim relPersonIdentification = GridIdentification_RelatedPerson.objListgoAML_Ref.Select(Function(x)
                                                                                                               x.FK_Person_Type = 31
                                                                                                               Return x
                                                                                                           End Function).ToList()

                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, relPersonIdentification)
                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridSanction_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Person.PK_goAML_Ref_Customer_Related_Person_ID, GridPEP_RelatedPerson.objListgoAML_Ref)
                End With

                objTempCustomer_RelatedPerson_Edit = Nothing
                objTemp_goAML_Ref_Customer_Related_Person = Nothing
            Else
                Dim objNewVWgoAML_RelatedPerson As New DataModel.goAML_Ref_Customer_Related_Person
                Dim objNewgoAML_RelatedPerson As New DataModel.goAML_Ref_Customer_Related_Person
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_RelatedPerson.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_RelatedPerson
                    .PK_goAML_Ref_Customer_Related_Person_ID = intpk
                    .CIF = txt_CIF.Text
                    .PERSON_PERSON_RELATION = cmb_rp_person_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_relation_date_range_is_approx_to_date.Value
                    .COMMENTS = txt_rp_comments.Value
                    .RELATION_COMMENTS = txt_rp_relation_comments.Value
                    .GENDER = cmb_rp_gender.SelectedItemValue
                    .TITLE = txt_rp_title.Value
                    '.FIRST_NAME = txt_rp_first_name.Value
                    '.MIDDLE_NAME = txt_rp_middle_name.Value
                    '.PREFIX = txt_rp_prefix.Value
                    .LAST_NAME = txt_rp_last_name.Value
                    If df_birthdate.RawValue IsNot Nothing Then
                        .BIRTHDATE = df_birthdate.Value
                    End If
                    .BIRTH_PLACE = txt_rp_birth_place.Value
                    '.COUNTRY_OF_BIRTH = cmb_rp_country_of_birth.SelectedItemValue
                    .MOTHERS_NAME = txt_rp_mothers_name.Value
                    .ALIAS = txt_rp_alias.Value
                    '.FULL_NAME_FRN = txt_rp_full_name_frn.Value
                    .SSN = txt_rp_ssn.Value
                    .PASSPORT_NUMBER = txt_rp_passport_number.Value
                    .PASSPORT_COUNTRY = cmb_rp_passport_country.SelectedItemValue
                    .ID_NUMBER = txt_rp_id_number.Value
                    .NATIONALITY1 = cmb_rp_nationality1.SelectedItemValue
                    .NATIONALITY2 = cmb_rp_nationality2.SelectedItemValue
                    .NATIONALITY3 = cmb_rp_nationality3.SelectedItemValue
                    .RESIDENCE = cmb_rp_residence.SelectedItemValue
                    '.RESIDENCE_SINCE = df_rp_residence_since.Value
                    .OCCUPATION = txt_rp_occupation.Value
                    .EMPLOYER_NAME = txt_rp_employer_name.Value
                    .DECEASED = cbx_rp_deceased.Value
                    If df_rp_date_deceased.RawValue IsNot Nothing Then
                        .DATE_DECEASED = df_rp_date_deceased.Value
                    End If
                    .TAX_NUMBER = txt_rp_tax_number.Value
                    .TAX_REG_NUMBER = cbx_rp_tax_reg_number.Value
                    .SOURCE_OF_WEALTH = txt_rp_source_of_wealth.Value
                    .IS_PROTECTED = cbx_rp_is_protected.Value

                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridIdentification_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridSanction_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(objNewVWgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridPEP_RelatedPerson.objListgoAML_Ref)
                End With

                With objNewgoAML_RelatedPerson
                    .PK_goAML_Ref_Customer_Related_Person_ID = intpk
                    .CIF = txt_CIF.Text
                    .PERSON_PERSON_RELATION = cmb_rp_person_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_relation_date_range_is_approx_to_date.Value
                    .COMMENTS = txt_rp_comments.Value
                    .RELATION_COMMENTS = txt_rp_relation_comments.Value
                    .GENDER = cmb_rp_gender.SelectedItemValue
                    .TITLE = txt_rp_title.Value
                    '.FIRST_NAME = txt_rp_first_name.Value
                    '.MIDDLE_NAME = txt_rp_middle_name.Value
                    '.PREFIX = txt_rp_prefix.Value
                    .LAST_NAME = txt_rp_last_name.Value
                    If df_birthdate.RawValue IsNot Nothing Then
                        .BIRTHDATE = df_birthdate.Value
                    End If
                    .BIRTH_PLACE = txt_rp_birth_place.Value
                    '.COUNTRY_OF_BIRTH = cmb_rp_country_of_birth.SelectedItemValue
                    .MOTHERS_NAME = txt_rp_mothers_name.Value
                    .ALIAS = txt_rp_alias.Value
                    '.FULL_NAME_FRN = txt_rp_full_name_frn.Value
                    .SSN = txt_rp_ssn.Value
                    .PASSPORT_NUMBER = txt_rp_passport_number.Value
                    .PASSPORT_COUNTRY = cmb_rp_passport_country.SelectedItemValue
                    .ID_NUMBER = txt_rp_id_number.Value
                    .NATIONALITY1 = cmb_rp_nationality1.SelectedItemValue
                    .NATIONALITY2 = cmb_rp_nationality2.SelectedItemValue
                    .NATIONALITY3 = cmb_rp_nationality3.SelectedItemValue
                    .RESIDENCE = cmb_rp_residence.SelectedItemValue
                    '.RESIDENCE_SINCE = df_rp_residence_since.Value
                    .OCCUPATION = txt_rp_occupation.Value
                    .EMPLOYER_NAME = txt_rp_employer_name.Value
                    .DECEASED = cbx_rp_deceased.Value
                    If df_rp_date_deceased.RawValue IsNot Nothing Then
                        .DATE_DECEASED = df_rp_date_deceased.Value
                    End If
                    .TAX_NUMBER = txt_rp_tax_number.Value
                    .TAX_REG_NUMBER = cbx_rp_tax_reg_number.Value
                    .SOURCE_OF_WEALTH = txt_rp_source_of_wealth.Value
                    .IS_PROTECTED = cbx_rp_is_protected.Value

                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridIdentification_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridSanction_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(objNewgoAML_RelatedPerson.PK_goAML_Ref_Customer_Related_Person_ID, GridPEP_RelatedPerson.objListgoAML_Ref)
                End With

                objListgoAML_vw_RelatedPerson.Add(objNewgoAML_RelatedPerson)
                objListgoAML_Ref_RelatedPerson = objListgoAML_vw_RelatedPerson

            End If

            Store_Customer_RelatedPerson.DataSource = objListgoAML_vw_RelatedPerson.ToList()
            Store_Customer_RelatedPerson.DataBind()

            'StoreCorp_Related_person.DataSource = objListgoAML_vw_RelatedPerson.ToList()
            'StoreCorp_Related_person.DataBind()

            WindowDetail_RelatedPerson.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub SaveAdd_AdditionalInfo()
        Try
            Dim objNewVWgoAML_AdditionalInfo As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information
            Dim objNewgoAML_AdditionalInfo As New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information
            Dim objRand As New Random

            Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
            While Not objListgoAML_Ref_AdditionalInfo.Find(Function(x) x.PK_goAML_Ref_Customer_Additional_Information_ID = intpk) Is Nothing
                intpk = objRand.Next
            End While

            With objNewVWgoAML_AdditionalInfo
                .PK_goAML_Ref_Customer_Additional_Information_ID = intpk
                .CIF = txt_CIF.Text
                .INFO_TYPE = cmb_info_type.SelectedItemValue
                .INFO_SUBJECT = txt_info_subject.Value
                .INFO_TEXT = txt_info_text.Value
                .INFO_NUMERIC = nfd_info_numeric.Value
                .INFO_DATE = df_info_date.Value
                .INFO_BOOLEAN = cbx_info_boolean.Value
            End With

            With objNewgoAML_AdditionalInfo
                .PK_goAML_Ref_Customer_Additional_Information_ID = intpk
                .CIF = txt_CIF.Text
                .INFO_TYPE = cmb_info_type.SelectedItemValue
                .INFO_SUBJECT = txt_info_subject.Value
                .INFO_TEXT = txt_info_text.Value
                .INFO_NUMERIC = nfd_info_numeric.Value
                .INFO_DATE = df_info_date.Value
                .INFO_BOOLEAN = cbx_info_boolean.Value
            End With

            objListgoAML_vw_AdditionalInfo.Add(objNewVWgoAML_AdditionalInfo)
            objListgoAML_Ref_AdditionalInfo.Add(objNewgoAML_AdditionalInfo)

            Store_Customer_Additional_Info.DataSource = objListgoAML_vw_AdditionalInfo.ToList()
            Store_Customer_Additional_Info.DataBind()

            Store_Customer_Corp_Additional_Info.DataSource = objListgoAML_vw_AdditionalInfo.ToList()
            Store_Customer_Corp_Additional_Info.DataBind()

            WindowDetail_AdditionalInfo.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub Save_Url(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempCustomer_EntityUrl_Edit
                    .CIF = txt_CIF.Text
                    .URL = txt_url.Value
                End With

                With objTemp_goAML_Ref_Customer_URL
                    .CIF = txt_CIF.Text
                    .URL = txt_url.Value
                End With

                objTempCustomer_EntityUrl_Edit = Nothing
                objTemp_goAML_Ref_Customer_URL = Nothing
            Else
                Dim objNewVWgoAML_Url As New DataModel.goAML_Ref_Customer_URL
                Dim objNewgoAML_Url As New DataModel.goAML_Ref_Customer_URL
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_EntityUrl.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_Url
                    .PK_goAML_Ref_Customer_URL_ID = intpk
                    .CIF = txt_CIF.Text
                    .URL = txt_url.Value
                End With

                With objNewgoAML_Url
                    .PK_goAML_Ref_Customer_URL_ID = intpk
                    .CIF = txt_CIF.Text
                    .URL = txt_url.Value
                End With

                objListgoAML_vw_EntityUrl.Add(objNewVWgoAML_Url)
                objListgoAML_Ref_EntityUrl.Add(objNewgoAML_Url)
            End If

            Store_Customer_Entity_URL.DataSource = objListgoAML_vw_EntityUrl.ToList()
            Store_Customer_Entity_URL.DataBind()

            WindowDetail_URL.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
    Sub Save_EntityIdentification(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempCustomer_EntityIdentification_Edit
                    .CIF = txt_CIF.Text
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If Not df_issue_date.Value.ToString().Contains("0001") Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If Not df_expiry_date.Value.ToString().Contains("0001") Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                With objTemp_goAML_Ref_Customer_Entity_Identification
                    .CIF = txt_CIF.Text
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If Not df_issue_date.Value.ToString().Contains("0001") Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If Not df_expiry_date.Value.ToString().Contains("0001") Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                objTempCustomer_EntityIdentification_Edit = Nothing
                objTemp_goAML_Ref_Customer_Entity_Identification = Nothing
            Else
                Dim objNewVWgoAML_EntityIdentification As New DataModel.goAML_Ref_Customer_Entity_Identification
                Dim objNewgoAML_EntityIdentification As New DataModel.goAML_Ref_Customer_Entity_Identification
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_EntityIdentification
                    .PK_goAML_Ref_Customer_Entity_Identification_ID = intpk
                    .CIF = txt_CIF.Text
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If Not df_issue_date.Value.ToString().Contains("0001") Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If Not df_expiry_date.Value.ToString().Contains("0001") Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                objListgoAML_vw_EntityIdentification.Add(objNewVWgoAML_EntityIdentification)
                objListgoAML_Ref_EntityIdentification = objListgoAML_vw_EntityIdentification
            End If

            Store_Customer_Entity_Identification.DataSource = objListgoAML_vw_EntityIdentification.ToList()
            Store_Customer_Entity_Identification.DataBind()

            WindowDetail_EntityIdentification.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
    Sub Save_RelatedEntities(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempCustomer_RelatedEntities_Edit
                    .CIF = txt_CIF.Text
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    '.SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    '.ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    '.ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objTempCustomer_RelatedEntities_Edit.PK_goAML_Ref_Customer_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objTempCustomer_RelatedEntities_Edit.PK_goAML_Ref_Customer_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTempCustomer_RelatedEntities_Edit.PK_goAML_Ref_Customer_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objTempCustomer_RelatedEntities_Edit.PK_goAML_Ref_Customer_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objTempCustomer_RelatedEntities_Edit.PK_goAML_Ref_Customer_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objTempCustomer_RelatedEntities_Edit.PK_goAML_Ref_Customer_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                With objTemp_goAML_Ref_Customer_Related_Entities
                    .CIF = txt_CIF.Text
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    '.SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    '.ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    '.ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Entities.PK_goAML_Ref_Customer_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Entities.PK_goAML_Ref_Customer_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Entities.PK_goAML_Ref_Customer_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Entities.PK_goAML_Ref_Customer_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Entities.PK_goAML_Ref_Customer_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objTemp_goAML_Ref_Customer_Related_Entities.PK_goAML_Ref_Customer_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                objTempCustomer_RelatedEntities_Edit = Nothing
                objTemp_goAML_Ref_Customer_Related_Entities = Nothing
            Else
                Dim objNewVWgoAML_RelatedEntities As New DataModel.goAML_Ref_Customer_Related_Entities
                Dim objNewgoAML_RelatedEntities As New DataModel.goAML_Ref_Customer_Related_Entities
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_RelatedEntities.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_RelatedEntities
                    .PK_goAML_Ref_Customer_Related_Entities_ID = intpk
                    .CIF = txt_CIF.Text
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    '.SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    '.ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    '.ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                With objNewgoAML_RelatedEntities
                    .PK_goAML_Ref_Customer_Related_Entities_ID = intpk
                    .CIF = txt_CIF.Text
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    '.SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    '.ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    '.ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_Customer_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                objListgoAML_vw_RelatedEntities.Add(objNewVWgoAML_RelatedEntities)
                objListgoAML_Ref_RelatedEntities = objListgoAML_vw_RelatedEntities
            End If


            Store_Related_Entities.DataSource = objListgoAML_vw_RelatedEntities.ToList()
            Store_Related_Entities.DataBind()

            WindowDetail_RelatedEntities.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub GrdCmdCustomerPreviousName(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_PreviousName(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_PreviousName(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_Email(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_Email(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_Email(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_Email(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_EmploymentHistory(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_EmploymentHistory(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_EmploymentHistory(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_PEP(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_PEP(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_PEP(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_PEP(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_NetworkDevice(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_NetworkDevice(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_NetworkDevice(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_SocialMedia(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_SocialMedia(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_SocialMedia(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_Sanction(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_Sanction(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_Sanction(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_Sanction(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_RelatedPerson(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_RelatedPerson(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_RelatedPerson(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_RelatedPerson(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_AdditionalInformation(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_AdditionalInformation(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_AdditionalInformation(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordPreviousName(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_EntityIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_EntityIdentification(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_EntityIdentification(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_EntityIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_URL(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_CustomerURL(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_CustomerURL(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_URL(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustomer_RelatedEntities(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_RelatedEntities(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_RelatedEntities(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_RelatedEntities(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Sub LoadDataDetailPreviousName(id As Long)
    '    Dim objCustomerPreviousName As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name = objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)

    '    If Not objCustomerPreviousName Is Nothing Then
    '        FormPanelPreviousName.Hidden = False
    '        WindowDetailPreviousName.Hidden = False
    '        txtCustomer_first_name.ReadOnly = True
    '        txtCustomer_last_name.ReadOnly = True
    '        txtCustomer_comments.ReadOnly = True
    '        btn_Customer_SavePreviousName.Hidden = True

    '        WindowDetailPreviousName.Title = "Previous Name Detail"
    '        ClearinputCustomerPreviousName()
    '        With objCustomerPreviousName
    '            txtCustomer_first_name.Value = .FIRST_NAME
    '            txtCustomer_last_name.Value = .LAST_NAME
    '            txtCustomer_comments.Value = .COMMENTS
    '        End With
    '        'EnvironmentForm("Detail")
    '    End If
    'End Sub

    'Sub LoadDataEditPreviousName(id As Long)
    '    Dim objCustomerPreviousName As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name = objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id

    '    If Not objCustomerPreviousName Is Nothing Then
    '        FormPanelPreviousName.Hidden = False
    '        WindowDetailPreviousName.Hidden = False
    '        txtCustomer_first_name.ReadOnly = False
    '        txtCustomer_last_name.ReadOnly = False
    '        txtCustomer_comments.ReadOnly = False
    '        btn_Customer_SavePreviousName.Hidden = False

    '        WindowDetailPreviousName.Title = "Previous Name Edit"
    '        ClearinputCustomerPreviousName()
    '        With objCustomerPreviousName
    '            txtCustomer_first_name.Value = .FIRST_NAME
    '            txtCustomer_last_name.Value = .LAST_NAME
    '            txtCustomer_comments.Value = .COMMENTS
    '        End With
    '        'EnvironmentForm("Detail")
    '    End If
    'End Sub

    Sub LoadDataDetailPreviousName(id As Long)
        Dim objCustomerPreviousName As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name = objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)

        If Not objCustomerPreviousName Is Nothing Then
            FormPanelPreviousName.Hidden = False
            WindowDetailPreviousName.Hidden = False
            txtCustomer_first_name.ReadOnly = True
            txtCustomer_last_name.ReadOnly = True
            txtCustomer_comments.ReadOnly = True
            btn_Customer_SavePreviousName.Hidden = True

            WindowDetailPreviousName.Title = "Previous Name Detail"
            'ClearinputCustomerPreviousName()
            With objCustomerPreviousName
                txtCustomer_first_name.Value = .FIRST_NAME
                txtCustomer_last_name.Value = .LAST_NAME
                txtCustomer_comments.Value = .COMMENTS
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataEditPreviousName(id As Long)
        Dim objCustomerPreviousName As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name = objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)

        If Not objCustomerPreviousName Is Nothing Then
            FormPanelPreviousName.Hidden = False
            WindowDetailPreviousName.Hidden = False
            txtCustomer_first_name.ReadOnly = False
            txtCustomer_last_name.ReadOnly = False
            txtCustomer_comments.ReadOnly = False
            btn_Customer_SavePreviousName.Hidden = False

            WindowDetailPreviousName.Title = "Previous Name Edit"
            'ClearinputCustomerPreviousName()
            With objCustomerPreviousName
                txtCustomer_first_name.Value = .FIRST_NAME
                txtCustomer_last_name.Value = .LAST_NAME
                txtCustomer_comments.Value = .COMMENTS
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadData_PreviousName(id As Long, isEdit As Boolean)
        Dim objCustomerPreviousName As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name = objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)

        If Not objCustomerPreviousName Is Nothing Then
            FormPanelPreviousName.Hidden = False
            WindowDetailPreviousName.Hidden = False
            txtCustomer_first_name.ReadOnly = Not isEdit
            txtCustomer_last_name.ReadOnly = Not isEdit
            txtCustomer_comments.ReadOnly = Not isEdit
            btn_Customer_SavePreviousName.Hidden = Not isEdit

            WindowDetailPreviousName.Title = "Previous Name " & If(isEdit, "Edit", "Detail")
            'ClearinputCustomerPreviousName()
            With objCustomerPreviousName
                txtCustomer_first_name.Value = .FIRST_NAME
                txtCustomer_last_name.Value = .LAST_NAME
                txtCustomer_comments.Value = .COMMENTS
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadData_Email(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Email = objListgoAML_Ref_Email.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id)
            objTempCustomer_Email_Edit = objListgoAML_vw_Email.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id)

            If Not objTempCustomer_Email_Edit Is Nothing Then
                FormPanel_Email.Hidden = False
                WindowDetail_Email.Hidden = False
                btn_Customer_Save_Email.Hidden = Not isEdit

                txtCustomer_email_address.ReadOnly = Not isEdit

                With objTempCustomer_Email_Edit
                    txtCustomer_email_address.Value = .EMAIL_ADDRESS
                End With

                WindowDetail_Email.Title = "Email " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_EmploymentHistory(id As Long, isEdit As Boolean)
        Try
            Dim objCustomer_EmploymentHistory As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History =
                objListgoAML_Ref_EmploymentHistory.Find(Function(x) x.PK_goAML_Ref_Customer_Employment_History_ID = id)

            If Not objCustomer_EmploymentHistory Is Nothing Then
                FormPanel_Employment_History.Hidden = False
                WindowDetail_Employment_History.Hidden = False
                btn_Customer_Save_Employment_History.Hidden = Not isEdit

                txt_employer_name.ReadOnly = Not isEdit
                txt_employer_business.ReadOnly = Not isEdit
                txt_employer_identifier.ReadOnly = Not isEdit
                df_valid_from.ReadOnly = Not isEdit
                cbx_is_approx_from_date.ReadOnly = Not isEdit
                df_valid_to.ReadOnly = Not isEdit
                cbx_is_approx_to_date.ReadOnly = Not isEdit

                With objCustomer_EmploymentHistory
                    txt_employer_name.Value = .EMPLOYER_NAME
                    txt_employer_business.Value = .EMPLOYER_BUSINESS
                    txt_employer_identifier.Value = .EMPLOYER_IDENTIFIER
                    df_valid_from.Text = .EMPLOYMENT_PERIOD_VALID_FROM
                    cbx_is_approx_from_date.Checked = .EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE
                    df_valid_to.Text = .EMPLOYMENT_PERIOD_VALID_TO
                    cbx_is_approx_to_date.Checked = .EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE
                End With

                WindowDetail_Employment_History.Title = "Employment History " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_PEP(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_PEP = objListgoAML_Ref_PersonPEP.Find(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id)
            objTempCustomer_PersonPEP_Edit = objListgoAML_vw_PersonPEP.Find(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id)

            If Not objTempCustomer_PersonPEP_Edit Is Nothing Then
                FormPanel_Person_PEP.Hidden = False
                WindowDetail_Person_PEP.Hidden = False
                btn_Customer_Save_PEP.Hidden = Not isEdit

                cmb_PersonPEP_Country.IsReadOnly = Not isEdit
                txt_function_name.ReadOnly = Not isEdit
                txt_function_description.ReadOnly = Not isEdit
                df_person_pep_valid_from.ReadOnly = Not isEdit
                cbx_pep_is_approx_from_date.ReadOnly = Not isEdit
                df_pep_valid_to.ReadOnly = Not isEdit
                cbx_pep_is_approx_to_date.ReadOnly = Not isEdit
                txt_pep_comments.ReadOnly = Not isEdit

                With objTempCustomer_PersonPEP_Edit
                    Dim reference = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .PEP_COUNTRY)

                    If reference IsNot Nothing Then
                        cmb_PersonPEP_Country.SetTextWithTextValue(reference.Kode, reference.Keterangan)
                    End If

                    txt_function_name.Value = .FUNCTION_NAME
                    txt_function_description.Value = .FUNCTION_DESCRIPTION
                    'df_person_pep_valid_from.Text = .PEP_DATE_RANGE_VALID_FROM
                    'cbx_pep_is_approx_from_date.Checked = .PEP_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_pep_valid_to.Text = .PEP_DATE_RANGE_VALID_TO
                    'cbx_pep_is_approx_to_date.Checked = .PEP_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_pep_comments.Text = .COMMENTS
                End With

                WindowDetail_Person_PEP.Title = "Person PEP " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_NetworkDevice(id As Long, isEdit As Boolean)
        Try
            Dim objCustomer_NetworkDevice As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device =
                objListgoAML_Ref_NetworkDevice.Find(Function(x) x.PK_goAML_Ref_Customer_Network_Device_ID = id)

            If Not objCustomer_NetworkDevice Is Nothing Then
                FormPanel_NetworkDevice.Hidden = False
                WindowDetail_NetworkDevice.Hidden = False
                btnCustomer_Save_NetworkDevice.Hidden = Not isEdit

                txt_device_number.ReadOnly = Not isEdit
                cmb_network_device_os.IsReadOnly = Not isEdit
                txt_service_provider.ReadOnly = Not isEdit
                txt_ipv6.ReadOnly = Not isEdit
                txt_ipv4.ReadOnly = Not isEdit
                txt_cgn_port.ReadOnly = Not isEdit
                txt_ipv4_ipv6.ReadOnly = Not isEdit
                txt_nat.ReadOnly = Not isEdit
                df_first_seen_date.ReadOnly = Not isEdit
                df_last_seen_date.ReadOnly = Not isEdit
                cbx_using_proxy.ReadOnly = Not isEdit
                txt_city.ReadOnly = Not isEdit
                cmb_network_device_country.IsReadOnly = Not isEdit
                txt_network_device_comments.ReadOnly = Not isEdit


                With objCustomer_NetworkDevice
                    Dim refOs = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Operating_System", .OPERATING_SYSTEM)
                    Dim refCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .COUNTRY)

                    txt_device_number.Value = .DEVICE_NUMBER
                    If refOs IsNot Nothing Then
                        cmb_network_device_os.SetTextWithTextValue(refOs.Kode, refOs.Keterangan)
                    End If

                    txt_service_provider.Value = .SERVICE_PROVIDER
                    txt_ipv6.Value = .IPV6
                    txt_ipv4.Value = .IPV4
                    txt_cgn_port.Value = .CGN_PORT
                    txt_ipv4_ipv6.Value = .IPV4_IPV6
                    txt_nat.Value = .NAT
                    df_first_seen_date.Text = .FIRST_SEEN_DATE
                    df_last_seen_date.Text = .LAST_SEEN_DATE
                    cbx_using_proxy.Checked = .USING_PROXY
                    txt_city.Value = .CITY

                    If refOs IsNot Nothing Then
                        cmb_network_device_country.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                    End If

                    txt_network_device_comments.Value = .COMMENTS

                End With

                WindowDetail_NetworkDevice.Title = "Network Device " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_SocialMedia(id As Long, isEdit As Boolean)
        Try
            Dim objCustomer_SocialMedia As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media =
                objListgoAML_Ref_SocialMedia.Find(Function(x) x.PK_goAML_Ref_Customer_Social_Media_ID = id)

            If Not objCustomer_SocialMedia Is Nothing Then
                FormPanel_SocialMedia.Hidden = False
                WindowDetail_SocialMedia.Hidden = False
                btnSaveCustomer_SocialMedia.Hidden = Not isEdit

                txt_socmed_platform.ReadOnly = Not isEdit
                txt_socmed_username.ReadOnly = Not isEdit
                txt_socmed_comments.ReadOnly = Not isEdit

                With objCustomer_SocialMedia
                    txt_socmed_platform.Value = .PLATFORM
                    txt_socmed_username.Value = .USER_NAME
                    txt_socmed_comments.Value = .COMMENTS
                End With

                WindowDetail_SocialMedia.Title = "SocialMedia " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_Sanction(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Sanction = objListgoAML_Ref_Sanction.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id)
            objTempCustomer_Sanction_Edit = objListgoAML_vw_Sanction.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id)


            If Not objTempCustomer_Sanction_Edit Is Nothing Then
                FormPanel_Sanction.Hidden = False
                WindowDetail_Sanction.Hidden = False
                btnSaveCustomer_Sanction.Hidden = Not isEdit

                txt_sanction_provider.ReadOnly = Not isEdit
                txt_sanction_sanction_list_name.ReadOnly = Not isEdit
                txt_sanction_match_criteria.ReadOnly = Not isEdit
                txt_sanction_link_to_source.ReadOnly = Not isEdit
                txt_sanction_sanction_list_attributes.ReadOnly = Not isEdit
                df_sanction_valid_from.ReadOnly = Not isEdit
                cbx_sanction_is_approx_from_date.ReadOnly = Not isEdit
                df_sanction_valid_to.ReadOnly = Not isEdit
                cbx_sanction_is_approx_to_date.ReadOnly = Not isEdit
                txt_sanction_comments.ReadOnly = Not isEdit


                With objTempCustomer_Sanction_Edit
                    txt_sanction_provider.Value = .PROVIDER
                    txt_sanction_sanction_list_name.Value = .SANCTION_LIST_NAME
                    txt_sanction_match_criteria.Value = .MATCH_CRITERIA
                    txt_sanction_link_to_source.Value = .LINK_TO_SOURCE
                    txt_sanction_sanction_list_attributes.Value = .SANCTION_LIST_ATTRIBUTES
                    'df_sanction_valid_from.Text = .SANCTION_LIST_DATE_RANGE_VALID_FROM
                    'cbx_sanction_is_approx_from_date.Checked = .SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_sanction_valid_to.Text = .SANCTION_LIST_DATE_RANGE_VALID_TO
                    'cbx_sanction_is_approx_to_date.Checked = .SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_sanction_comments.Value = .COMMENTS
                End With

                WindowDetail_Sanction.Title = "Sanction " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_RelatedPerson(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Related_Person = objListgoAML_Ref_RelatedPerson.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id)
            objTempCustomer_RelatedPerson_Edit = objListgoAML_Ref_RelatedPerson.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id)

            If Not objTempCustomer_RelatedPerson_Edit Is Nothing Then

                GridAddressWork_RelatedPerson.IsViewMode = Not isEdit
                GridAddress_RelatedPerson.IsViewMode = Not isEdit
                GridPhone_RelatedPerson.IsViewMode = Not isEdit
                GridPhoneWork_RelatedPerson.IsViewMode = Not isEdit
                GridIdentification_RelatedPerson.IsViewMode = Not isEdit
                GridEmail_RelatedPerson.IsViewMode = Not isEdit
                GridSanction_RelatedPerson.IsViewMode = Not isEdit
                GridPEP_RelatedPerson.IsViewMode = Not isEdit

                FormPanel_RelatedPerson.Hidden = False
                WindowDetail_RelatedPerson.Hidden = False
                btnSaveCustomer_RelatedPerson.Hidden = Not isEdit

                cmb_rp_person_relation.IsReadOnly = Not isEdit
                'df_relation_date_range_valid_from.ReadOnly = Not isEdit
                'cbx_relation_date_range_is_approx_from_date.ReadOnly = Not isEdit
                'df_relation_date_range_valid_to.ReadOnly = Not isEdit
                'cbx_relation_date_range_is_approx_to_date.ReadOnly = Not isEdit
                txt_rp_comments.ReadOnly = Not isEdit
                txt_rp_relation_comments.ReadOnly = Not isEdit
                cmb_rp_gender.IsReadOnly = Not isEdit
                txt_rp_title.ReadOnly = Not isEdit
                'txt_rp_first_name.ReadOnly = Not isEdit
                'txt_rp_middle_name.ReadOnly = Not isEdit
                'txt_rp_prefix.ReadOnly = Not isEdit
                txt_rp_last_name.ReadOnly = Not isEdit
                df_birthdate.ReadOnly = Not isEdit
                txt_rp_birth_place.ReadOnly = Not isEdit
                'cmb_rp_country_of_birth.IsReadOnly = Not isEdit
                txt_rp_mothers_name.ReadOnly = Not isEdit
                txt_rp_alias.ReadOnly = Not isEdit
                'txt_rp_full_name_frn.ReadOnly = Not isEdit
                txt_rp_ssn.ReadOnly = Not isEdit
                txt_rp_passport_number.ReadOnly = Not isEdit
                cmb_rp_passport_country.IsReadOnly = Not isEdit
                txt_rp_id_number.ReadOnly = Not isEdit
                cmb_rp_nationality1.IsReadOnly = Not isEdit
                cmb_rp_nationality2.IsReadOnly = Not isEdit
                cmb_rp_nationality3.IsReadOnly = Not isEdit
                cmb_rp_residence.IsReadOnly = Not isEdit
                'df_rp_residence_since.ReadOnly = Not isEdit
                txt_rp_occupation.ReadOnly = Not isEdit
                txt_rp_employer_name.ReadOnly = Not isEdit
                cbx_rp_deceased.ReadOnly = Not isEdit
                df_rp_date_deceased.ReadOnly = Not isEdit
                txt_rp_tax_number.ReadOnly = Not isEdit
                cbx_rp_tax_reg_number.ReadOnly = Not isEdit
                txt_rp_source_of_wealth.ReadOnly = Not isEdit
                cbx_rp_is_protected.ReadOnly = Not isEdit


                With objTempCustomer_RelatedPerson_Edit
                    Dim refPersonRelation = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Person_Person_Relationship", .PERSON_PERSON_RELATION)
                    Dim refGender = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Jenis_Kelamin", .GENDER)
                    Dim refCountryBirth = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .COUNTRY_OF_BIRTH)
                    Dim refPassportCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .PASSPORT_COUNTRY)
                    Dim refNationality1 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY1)
                    Dim refNationality2 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY2)
                    Dim refNationality3 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY3)
                    Dim refResidence = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .RESIDENCE)

                    cmb_rp_person_relation.SetTextWithTextValue(refPersonRelation.Kode, refPersonRelation.Keterangan)
                    'df_relation_date_range_valid_from.Text = .RELATION_DATE_RANGE_VALID_FROM
                    'cbx_relation_date_range_is_approx_from_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_relation_date_range_valid_to.Text = .RELATION_DATE_RANGE_VALID_TO
                    'cbx_relation_date_range_is_approx_to_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_rp_comments.Value = .COMMENTS
                    txt_rp_relation_comments.Value = .RELATION_COMMENTS
                    If refGender IsNot Nothing Then
                        cmb_rp_gender.SetTextWithTextValue(refGender.Kode, refGender.Keterangan)
                    End If
                    txt_rp_title.Value = .TITLE
                    'txt_rp_first_name.Value = .FIRST_NAME
                    'txt_rp_middle_name.Value = .MIDDLE_NAME
                    'txt_rp_prefix.Value = .PREFIX
                    txt_rp_last_name.Value = .LAST_NAME
                    df_birthdate.Text = .BIRTHDATE
                    txt_rp_birth_place.Value = .BIRTH_PLACE
                    'cmb_rp_country_of_birth.SetTextWithTextValue(refCountryBirth.Kode, refCountryBirth.Keterangan)
                    txt_rp_mothers_name.Value = .MOTHERS_NAME
                    txt_rp_alias.Value = .ALIAS
                    'txt_rp_full_name_frn.Value = .FULL_NAME_FRN
                    txt_rp_ssn.Value = .SSN
                    txt_rp_passport_number.Value = .PASSPORT_NUMBER
                    If refPassportCountry IsNot Nothing Then
                        cmb_rp_passport_country.SetTextWithTextValue(refPassportCountry.Kode, refPassportCountry.Keterangan)
                    End If
                    txt_rp_id_number.Value = .ID_NUMBER
                    If refNationality1 IsNot Nothing Then
                        cmb_rp_nationality1.SetTextWithTextValue(refNationality1.Kode, refNationality1.Keterangan)
                    End If
                    If refNationality2 IsNot Nothing Then
                        cmb_rp_nationality2.SetTextWithTextValue(refNationality2.Kode, refNationality2.Keterangan)
                    End If
                    If refNationality3 IsNot Nothing Then
                        cmb_rp_nationality3.SetTextWithTextValue(refNationality3.Kode, refNationality3.Keterangan)
                    End If
                    If refResidence IsNot Nothing Then
                        cmb_rp_residence.SetTextWithTextValue(refResidence.Kode, refResidence.Keterangan)
                    End If
                    'df_rp_residence_since.Text = .RESIDENCE_SINCE
                    txt_rp_occupation.Value = .OCCUPATION
                    txt_rp_employer_name.Value = .EMPLOYER_NAME
                    cbx_rp_deceased.Checked = .DECEASED
                    df_rp_date_deceased.Text = IIf(.DATE_DECEASED Is Nothing, "", .DATE_DECEASED)
                    txt_rp_tax_number.Value = .TAX_NUMBER
                    cbx_rp_tax_reg_number.Checked = .TAX_REG_NUMBER
                    txt_rp_source_of_wealth.Text = .SOURCE_OF_WEALTH
                    cbx_rp_is_protected.Checked = .IS_PROTECTED

                    Dim pk_id = objTempCustomer_RelatedPerson_Edit.PK_goAML_Ref_Customer_Related_Person_ID

                    Dim address_list = If(.ObjList_GoAML_Ref_Address IsNot Nothing, .ObjList_GoAML_Ref_Address,
                        goAML_Customer_Service.GetAddressByPkFk(pk_id, 31))
                    Dim address_work_list = If(.ObjList_GoAML_Ref_Address_Work IsNot Nothing, .ObjList_GoAML_Ref_Address_Work,
                        goAML_Customer_Service.GetAddressByPkFk(pk_id, 32))
                    Dim phone_list = If(.ObjList_GoAML_Ref_Phone IsNot Nothing, .ObjList_GoAML_Ref_Phone,
                        goAML_Customer_Service.GetPhoneByPkFk(pk_id, 31))
                    Dim phone_work_list = If(.ObjList_GoAML_Ref_Phone_Work IsNot Nothing, .ObjList_GoAML_Ref_Phone_Work,
                        goAML_Customer_Service.GetPhoneByPkFk(pk_id, 32))
                    Dim identification_list = If(.ObjList_GoAML_Person_Identification IsNot Nothing, .ObjList_GoAML_Person_Identification,
                        goAML_Customer_Service.GetIdentificationPkFk(pk_id, 31))
                    '' Edit 21-Sep-2023, Felix.
                    'Dim email_list = If(.ObjList_GoAML_Ref_Customer_Email IsNot Nothing, .ObjList_GoAML_Ref_Customer_Email,
                    '    goAML_Customer_Service.GetEmailByPkFk(pk_id, 31)) '' Edit 21-Sep-2023, Felix.
                    Dim email_list = If(.ObjList_GoAML_Ref_Customer_Email IsNot Nothing, .ObjList_GoAML_Ref_Customer_Email,
                        goAML_Customer_Service.GetRPEmailByPkFk(pk_id, 31))
                    'Dim sanction_list = If(.ObjList_GoAML_Ref_Customer_Sanction IsNot Nothing, .ObjList_GoAML_Ref_Customer_Sanction,
                    '    goAML_Customer_Service.GetSanctionByPkFk(pk_id, 31))
                    Dim sanction_list = If(.ObjList_GoAML_Ref_Customer_Sanction IsNot Nothing, .ObjList_GoAML_Ref_Customer_Sanction,
                        goAML_Customer_Service.GetRPSanctionByPkFk(pk_id, 31))
                    'Dim pep_list = If(.ObjList_GoAML_Ref_Customer_PEP IsNot Nothing, .ObjList_GoAML_Ref_Customer_PEP,
                    '    goAML_Customer_Service.GetPEPByPkFk(pk_id, 31))
                    Dim pep_list = If(.ObjList_GoAML_Ref_Customer_PEP IsNot Nothing, .ObjList_GoAML_Ref_Customer_PEP,
                        goAML_Customer_Service.GetRPPEPByPkFk(pk_id, 31))
                    '' end 21-Sep-2023
                    GridAddress_RelatedPerson.LoadData(address_list)
                    GridAddressWork_RelatedPerson.LoadData(address_work_list)
                    GridPhone_RelatedPerson.LoadData(phone_list)
                    GridPhoneWork_RelatedPerson.LoadData(phone_work_list)
                    GridIdentification_RelatedPerson.LoadData(identification_list)

                    GridEmail_RelatedPerson.LoadData(email_list)
                    GridSanction_RelatedPerson.LoadData(sanction_list)
                    GridPEP_RelatedPerson.LoadData(pep_list)

                End With

                WindowDetail_RelatedPerson.Title = "Related Person " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_AdditionalInformation(id As Long, isEdit As Boolean)
        Try
            Dim objCustomer_AdditionalInformation As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information =
                objListgoAML_Ref_AdditionalInfo.Find(Function(x) x.PK_goAML_Ref_Customer_Additional_Information_ID = id)

            If Not objCustomer_AdditionalInformation Is Nothing Then
                FormPanel_AdditionalInfo.Hidden = False
                WindowDetail_AdditionalInfo.Hidden = False
                btnSaveCustomer_AdditionalInfo.Hidden = Not isEdit

                cmb_info_type.IsReadOnly = Not isEdit
                txt_info_subject.ReadOnly = Not isEdit
                txt_info_text.ReadOnly = Not isEdit
                nfd_info_numeric.ReadOnly = Not isEdit
                df_info_date.ReadOnly = Not isEdit
                cbx_info_boolean.ReadOnly = Not isEdit

                With objCustomer_AdditionalInformation
                    Dim refType = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Additional_Info_Type", .INFO_TYPE)

                    cmb_info_type.SetTextWithTextValue(refType.Kode, refType.Keterangan)
                    txt_info_subject.Value = .INFO_SUBJECT
                    txt_info_text.Value = .INFO_TEXT
                    nfd_info_numeric.Value = .INFO_NUMERIC
                    df_info_date.Text = .INFO_DATE
                    cbx_info_boolean.Checked = .INFO_BOOLEAN

                End With

                WindowDetail_AdditionalInfo.Title = "Additional Information " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_EntityIdentification(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Entity_Identification = objListgoAML_Ref_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id)
            objTempCustomer_EntityIdentification_Edit = objListgoAML_vw_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id)

            If Not objTempCustomer_EntityIdentification_Edit Is Nothing Then
                FormPanel_EntityIdentification.Hidden = False
                WindowDetail_EntityIdentification.Hidden = False
                btnSaveCustomer_EntityIdentification.Hidden = Not isEdit

                cmb_ef_type.IsReadOnly = Not isEdit
                txt_ef_number.ReadOnly = Not isEdit
                df_issue_date.ReadOnly = Not isEdit
                df_expiry_date.ReadOnly = Not isEdit
                txt_ef_issued_by.ReadOnly = Not isEdit
                cmb_ef_issue_country.IsReadOnly = Not isEdit
                txt_ef_comments.ReadOnly = Not isEdit


                With objTempCustomer_EntityIdentification_Edit
                    Dim refType = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Identifier_Type", .TYPE)
                    Dim refIssue = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .ISSUE_COUNTRY)

                    cmb_ef_type.SetTextWithTextValue(refType.Kode, refType.Keterangan)
                    txt_ef_number.Value = .NUMBER
                    df_issue_date.Text = IIf(.ISSUE_DATE Is Nothing, "", .ISSUE_DATE)
                    df_expiry_date.Text = IIf(.EXPIRY_DATE Is Nothing, "", .EXPIRY_DATE)
                    txt_ef_issued_by.Value = IIf(.ISSUED_BY Is Nothing, "", .ISSUED_BY)
                    cmb_ef_issue_country.SetTextWithTextValue(refIssue.Kode, refIssue.Keterangan)
                    txt_ef_comments.Value = .COMMENTS

                End With

                WindowDetail_EntityIdentification.Title = "Entity Identification " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_CustomerURL(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_URL = objListgoAML_Ref_EntityUrl.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id)
            objTempCustomer_EntityUrl_Edit = objListgoAML_vw_EntityUrl.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id)

            If Not objTempCustomer_EntityUrl_Edit Is Nothing Then
                FormPanel_URL.Hidden = False
                WindowDetail_URL.Hidden = False
                btnSaveCustomer_URL.Hidden = Not isEdit

                txt_url.ReadOnly = Not isEdit

                With objTempCustomer_EntityUrl_Edit
                    txt_url.Value = .URL
                End With

                WindowDetail_URL.Title = "Entity URL " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub ReBindGridUserControl()
        Dim strBtnAdd As String = "btnAdd"
        Dim strGridPanel As String = "GridPanel"

        Dim btnAdd As Ext.Net.Button = DirectCast(GridEmail_RelatedEntity.FindControl(strBtnAdd), Ext.Net.Button)
        Dim gridPanel As Ext.Net.GridPanel = DirectCast(GridEmail_RelatedEntity.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridEmail_RelatedEntity.IsViewMode
        Dim cmdColumn As CommandColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridEmail_RelatedEntity.IsViewMode
            cmd2.Hidden = GridEmail_RelatedEntity.IsViewMode
        End If

        btnAdd = DirectCast(GridAddressWork_RelatedPerson.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridAddressWork_RelatedPerson.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridAddressWork_RelatedPerson.IsViewMode
        cmdColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridAddressWork_RelatedPerson.IsViewMode
            cmd2.Hidden = GridAddressWork_RelatedPerson.IsViewMode
        End If

        btnAdd = DirectCast(GridEntityIdentification_RelatedEntity.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridEntityIdentification_RelatedEntity.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridEntityIdentification_RelatedEntity.IsViewMode
        cmdColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridEntityIdentification_RelatedEntity.IsViewMode
            cmd2.Hidden = GridEntityIdentification_RelatedEntity.IsViewMode
        End If

        btnAdd = DirectCast(GridURL_RelatedEntity.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridURL_RelatedEntity.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridURL_RelatedEntity.IsViewMode
        cmdColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridURL_RelatedEntity.IsViewMode
            cmd2.Hidden = GridURL_RelatedEntity.IsViewMode
        End If

        btnAdd = DirectCast(GridSanction_RelatedEntity.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridSanction_RelatedEntity.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridSanction_RelatedEntity.IsViewMode
        cmdColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridSanction_RelatedEntity.IsViewMode
            cmd2.Hidden = GridSanction_RelatedEntity.IsViewMode
        End If

        btnAdd = DirectCast(GridAddress_RelatedPerson.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridAddress_RelatedPerson.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridAddress_RelatedPerson.IsViewMode
        cmdColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            If (GridAddress_RelatedPerson.IsViewMode) Then
                cmdColumn.Commands.RemoveAt(1)
                cmdColumn.Commands.RemoveAt(1)
            Else
                Dim cmd1 As GridCommand = New GridCommand
                cmd1.CommandName = "Edit"
                cmd1.Icon = "Pencil"
                cmd1.Text = "Edit"
                cmd1.ToolTip.Text = "Edit"
                Dim cmd2 As GridCommand = New GridCommand
                cmd2.CommandName = "Edit"
                cmd2.Icon = "Pencil"
                cmd2.Text = "Edit"
                cmd2.ToolTip.Text = "Edit"
                cmdColumn.Commands.Add(cmd1)
                cmdColumn.Commands.Add(cmd2)
            End If
        End If

        btnAdd = DirectCast(GridPhone_RelatedPerson.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridPhone_RelatedPerson.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridPhone_RelatedPerson.IsViewMode
        cmdColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridPhone_RelatedPerson.IsViewMode
            cmd2.Hidden = GridPhone_RelatedPerson.IsViewMode
        End If

        btnAdd = DirectCast(GridPhoneWork_RelatedPerson.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridPhoneWork_RelatedPerson.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridPhoneWork_RelatedPerson.IsViewMode
        cmdColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridPhoneWork_RelatedPerson.IsViewMode
            cmd2.Hidden = GridPhoneWork_RelatedPerson.IsViewMode
        End If

        btnAdd = DirectCast(GridIdentification_RelatedPerson.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridIdentification_RelatedPerson.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridIdentification_RelatedPerson.IsViewMode
        cmdColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridIdentification_RelatedPerson.IsViewMode
            cmd2.Hidden = GridIdentification_RelatedPerson.IsViewMode
        End If

        btnAdd = DirectCast(GridEmail_RelatedPerson.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridEmail_RelatedPerson.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridEmail_RelatedPerson.IsViewMode
        cmdColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridEmail_RelatedPerson.IsViewMode
            cmd2.Hidden = GridEmail_RelatedPerson.IsViewMode
        End If

        btnAdd = DirectCast(GridSanction_RelatedPerson.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridSanction_RelatedPerson.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridSanction_RelatedPerson.IsViewMode
        cmdColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridSanction_RelatedPerson.IsViewMode
            cmd2.Hidden = GridSanction_RelatedPerson.IsViewMode
        End If

        btnAdd = DirectCast(GridPEP_RelatedPerson.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridPEP_RelatedPerson.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridPEP_RelatedPerson.IsViewMode
        cmdColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridPEP_RelatedPerson.IsViewMode
            cmd2.Hidden = GridPEP_RelatedPerson.IsViewMode
        End If

        btnAdd = DirectCast(GridEmail_Director.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridEmail_Director.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridEmail_Director.IsViewMode
        cmdColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridEmail_Director.IsViewMode
            cmd2.Hidden = GridEmail_Director.IsViewMode
        End If

        btnAdd = DirectCast(GridSanction_Director.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridSanction_Director.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridSanction_Director.IsViewMode
        cmdColumn = DirectCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridSanction_Director.IsViewMode
            cmd2.Hidden = GridSanction_Director.IsViewMode
        End If

        btnAdd = DirectCast(GridPEP_Director.FindControl(strBtnAdd), Ext.Net.Button)
        gridPanel = DirectCast(GridPEP_Director.FindControl(strGridPanel), Ext.Net.GridPanel)
        btnAdd.Hidden = GridPEP_Director.IsViewMode
        cmdColumn = TryCast(gridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            cmd1.Hidden = GridPEP_Director.IsViewMode
            cmd2.Hidden = GridPEP_Director.IsViewMode
        End If
    End Sub
    Sub LoadData_RelatedEntities(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_Customer_Related_Entities = objListgoAML_Ref_RelatedEntities.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id)
            objTempCustomer_RelatedEntities_Edit = objListgoAML_Ref_RelatedEntities.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id)

            If Not objTempCustomer_RelatedEntities_Edit Is Nothing Then
                GridPhone_RelatedEntity.IsViewMode = Not isEdit
                GridAddress_RelatedEntity.IsViewMode = Not isEdit
                GridEmail_RelatedEntity.IsViewMode = Not isEdit
                GridEntityIdentification_RelatedEntity.IsViewMode = Not isEdit
                GridURL_RelatedEntity.IsViewMode = Not isEdit
                GridSanction_RelatedEntity.IsViewMode = Not isEdit

                FormPanel_RelatedEntities.Hidden = False
                WindowDetail_RelatedEntities.Hidden = False
                btnSaveCustomer_EntityRelation.Hidden = Not isEdit

                cmb_re_entity_relation.IsReadOnly = Not isEdit
                'df_re_relation_date_range_valid_from.ReadOnly = Not isEdit
                'cbx_re_relation_date_range_is_approx_from_date.ReadOnly = Not isEdit
                'df_re_relation_date_range_valid_to.ReadOnly = Not isEdit
                'cbx_re_relation_date_range_is_approx_to_date.ReadOnly = Not isEdit
                'nfd_re_share_percentage.ReadOnly = Not isEdit
                txt_re_comments.ReadOnly = Not isEdit
                txt_re_relation_comments.ReadOnly = Not isEdit
                txt_re_name.ReadOnly = Not isEdit
                txt_re_commercial_name.ReadOnly = Not isEdit
                cmb_re_incorporation_legal_form.IsReadOnly = Not isEdit
                txt_re_incorporation_number.ReadOnly = Not isEdit
                txt_re_business.ReadOnly = Not isEdit
                'cmb_re_entity_status.IsReadOnly = Not isEdit
                'df_re_entity_status_date.ReadOnly = Not isEdit
                txt_re_incorporation_state.ReadOnly = Not isEdit
                cmb_re_incorporation_country_code.IsReadOnly = Not isEdit
                df_re_incorporation_date.ReadOnly = Not isEdit
                cbx_re_business_closed.ReadOnly = Not isEdit
                df_re_date_business_closed.ReadOnly = Not isEdit
                txt_re_tax_number.ReadOnly = Not isEdit
                txt_re_tax_reg_number.ReadOnly = Not isEdit

                With objTempCustomer_RelatedEntities_Edit
                    Dim refRelation = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Entity_Relationship", .ENTITY_ENTITY_RELATION)
                    Dim refLegalForm = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Bentuk_Badan_Usaha", .INCORPORATION_LEGAL_FORM)
                    Dim refEntityStatus = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Status", .ENTITY_STATUS)
                    Dim refCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .INCORPORATION_COUNTRY_CODE)

                    If refRelation IsNot Nothing Then
                        cmb_re_entity_relation.SetTextWithTextValue(refRelation.Kode, refRelation.Keterangan)
                    End If
                    'df_re_relation_date_range_valid_from.Text = .RELATION_DATE_RANGE_VALID_FROM
                    'cbx_re_relation_date_range_is_approx_from_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_re_relation_date_range_valid_to.Text = .RELATION_DATE_RANGE_VALID_TO
                    'cbx_re_relation_date_range_is_approx_to_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    'nfd_re_share_percentage.Value = .SHARE_PERCENTAGE
                    txt_re_comments.Value = .COMMENTS
                    txt_re_relation_comments.Value = .RELATION_COMMENTS
                    txt_re_name.Value = .NAME
                    txt_re_commercial_name.Value = .COMMERCIAL_NAME
                    If refLegalForm IsNot Nothing Then
                        cmb_re_incorporation_legal_form.SetTextWithTextValue(refLegalForm.Kode, refLegalForm.Keterangan)
                    End If
                    txt_re_incorporation_number.Value = .INCORPORATION_NUMBER
                    txt_re_business.Value = .BUSINESS
                    'cmb_re_entity_status.SetTextWithTextValue(refEntityStatus.Kode, refEntityStatus.Keterangan)
                    'df_re_entity_status_date.Text = .ENTITY_STATUS
                    txt_re_incorporation_state.Value = .INCORPORATION_STATE
                    If refCountry IsNot Nothing Then
                        cmb_re_incorporation_country_code.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                    End If
                    If .INCORPORATION_DATE Is Nothing Then
                        df_re_incorporation_date.Text = ""
                    Else
                        df_re_incorporation_date.Text = .INCORPORATION_DATE
                    End If
                    cbx_re_business_closed.Checked = .BUSINESS_CLOSED
                    df_re_date_business_closed.Text = IIf(.DATE_BUSINESS_CLOSED Is Nothing, "", .DATE_BUSINESS_CLOSED)
                    txt_re_tax_number.Value = .TAX_NUMBER

                    txt_re_tax_reg_number.Value = .TAX_REG_NUMBER

                    Dim phone_list = If(.ObjList_GoAML_Ref_Customer_Phone IsNot Nothing, .ObjList_GoAML_Ref_Customer_Phone,
                        goAML_Customer_Service.GetPhoneByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36))
                    Dim address_list = If(.ObjList_GoAML_Ref_Customer_Address IsNot Nothing, .ObjList_GoAML_Ref_Customer_Address,
                        goAML_Customer_Service.GetAddressByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36))
                    Dim email_list = If(.ObjList_GoAML_Ref_Customer_Email IsNot Nothing, .ObjList_GoAML_Ref_Customer_Email,
                        goAML_Customer_Service.GetEmailByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36))
                    Dim entity_identification_list = If(.ObjList_GoAML_Ref_Customer_Entity_Identification IsNot Nothing, .ObjList_GoAML_Ref_Customer_Entity_Identification,
                        goAML_Customer_Service.GetEntityIdentificationByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36))
                    Dim url_list = If(.ObjList_GoAML_Ref_Customer_URL IsNot Nothing, .ObjList_GoAML_Ref_Customer_URL,
                        goAML_Customer_Service.GetUrlByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36))
                    Dim sanction_list = If(.ObjList_GoAML_Ref_Customer_Sanction IsNot Nothing, .ObjList_GoAML_Ref_Customer_Sanction,
                        goAML_Customer_Service.GetSanctionByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36))

                    GridPhone_RelatedEntity.LoadData(phone_list)
                    GridAddress_RelatedEntity.LoadData(address_list)
                    GridEmail_RelatedEntity.LoadData(email_list)
                    GridEntityIdentification_RelatedEntity.LoadData(entity_identification_list)
                    GridURL_RelatedEntity.LoadData(url_list)
                    GridSanction_RelatedEntity.LoadData(sanction_list)

                End With

                WindowDetail_RelatedEntities.Title = "Related Entities " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub DeleteRecordPreviousName(id As Long)

        Dim objDelVWPreviousName As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name = objListgoAML_vw_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)
        Dim objxDelPreviousName As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name = objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)

        If Not objxDelPreviousName Is Nothing Then
            objListgoAML_Ref_Previous_Name.Remove(objxDelPreviousName)
        End If

        If Not objDelVWPreviousName Is Nothing Then
            objListgoAML_vw_Previous_Name.Remove(objDelVWPreviousName)

            Store_Customer_Previous_Name.DataSource = objListgoAML_vw_Previous_Name.ToList()
            Store_Customer_Previous_Name.DataBind()
        End If

        FormPanelPreviousName.Hidden = True
        WindowDetailPreviousName.Hidden = True
        'ClearinputCustomerPreviousName()

    End Sub

    Sub DeleteRecord_Email(id As Long)
        objListgoAML_vw_Email.Remove(objListgoAML_vw_Email.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id))
        objListgoAML_Ref_Email.Remove(objListgoAML_Ref_Email.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id))

        Store_Customer_Email.DataSource = objListgoAML_vw_Email.ToList()
        Store_Customer_Email.DataBind()

        Store_corp_email.DataSource = objListgoAML_vw_Email.ToList()
        Store_corp_email.DataBind()

    End Sub

    Sub DeleteRecord_PEP(id As Long)
        objListgoAML_vw_PersonPEP.Remove(objListgoAML_vw_PersonPEP.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id))
        objListgoAML_Ref_PersonPEP.Remove(objListgoAML_Ref_PersonPEP.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id))

        Store_Customer_Person_PEP.DataSource = objListgoAML_vw_PersonPEP.ToList()
        Store_Customer_Person_PEP.DataBind()

    End Sub

    Sub DeleteRecord_Sanction(id As Long)
        objListgoAML_vw_Sanction.Remove(objListgoAML_vw_Sanction.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id))
        objListgoAML_Ref_Sanction.Remove(objListgoAML_Ref_Sanction.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id))

        Store_Customer_Sanction.DataSource = objListgoAML_vw_Sanction.ToList()
        Store_Customer_Sanction.DataBind()

        Store_corp_sanction.DataSource = objListgoAML_vw_Sanction.ToList()
        Store_corp_sanction.DataBind()

    End Sub

    Sub DeleteRecord_RelatedPerson(id As Long)
        objListgoAML_vw_RelatedPerson.Remove(objListgoAML_vw_RelatedPerson.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id))
        objListgoAML_Ref_RelatedPerson.Remove(objListgoAML_Ref_RelatedPerson.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id))

        Store_Customer_RelatedPerson.DataSource = objListgoAML_vw_RelatedPerson.ToList()
        Store_Customer_RelatedPerson.DataBind()

        'StoreCorp_Related_person.DataSource = objListgoAML_vw_RelatedPerson.ToList()
        'StoreCorp_Related_person.DataBind()

    End Sub

    Sub DeleteRecord_URL(id As Long)
        objListgoAML_vw_EntityUrl.Remove(objListgoAML_vw_EntityUrl.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id))
        objListgoAML_Ref_EntityUrl.Remove(objListgoAML_Ref_EntityUrl.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id))

        Store_Customer_Entity_URL.DataSource = objListgoAML_vw_EntityUrl.ToList()
        Store_Customer_Entity_URL.DataBind()

    End Sub

    Sub DeleteRecord_EntityIdentification(id As Long)
        objListgoAML_vw_EntityIdentification.Remove(objListgoAML_vw_EntityIdentification.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id))
        objListgoAML_Ref_EntityIdentification.Remove(objListgoAML_Ref_EntityIdentification.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id))

        Store_Customer_Entity_Identification.DataSource = objListgoAML_vw_EntityIdentification.ToList()
        Store_Customer_Entity_Identification.DataBind()

    End Sub

    Sub DeleteRecord_RelatedEntities(id As Long)
        objListgoAML_vw_RelatedEntities.Remove(objListgoAML_vw_RelatedEntities.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id))
        objListgoAML_Ref_RelatedEntities.Remove(objListgoAML_Ref_RelatedEntities.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id))

        Store_Related_Entities.DataSource = objListgoAML_vw_RelatedEntities.ToList()
        Store_Related_Entities.DataBind()

    End Sub

    Function IsDataCustomer_PreviousName_Valid() As Boolean
        If txt_CIF.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If
        If txtCustomer_first_name.Text.Trim = "" Then
            Throw New Exception("First Name is required")
        End If
        If txtCustomer_last_name.Text.Trim = "" Then
            Throw New Exception("Last Name is required")
        End If
        Return True
        Return True
    End Function

    Function IsDataCustomer_Email_Valid() As Boolean
        If txt_CIF.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        'If txtCustomer_email_address.Text.Trim = "" Then
        '    Throw New Exception("CIF is required")
        'End If

        If txtCustomer_email_address.Text.Trim = "" Then
            Throw New Exception("Email Address is required")
        End If

        If Not IsFieldValid(txtCustomer_email_address.Text.Trim, "email_address") Then
            Throw New Exception("Email Address format is not valid")
        End If

        Return True
    End Function

    Function IsDataCustomer_EmploymentHistory_Valid() As Boolean
        If txt_CIF.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If
        Return True
    End Function

    Function IsDataCustomer_PEP_Valid() As Boolean
        If txt_CIF.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        If cmb_PersonPEP_Country.SelectedItemText Is Nothing Then
            Throw New Exception("Country is required")
        End If

        'If txt_function_name.Text.Trim = "" Then
        '    Throw New Exception("Function Name is required")
        'End If
        'If txt_function_description.Text.Trim = "" Then
        '    Throw New Exception("Function Description is required")
        'End If
        'If df_person_pep_valid_from.RawValue Is Nothing Then
        '    Throw New Exception("Valid From is required")
        'End If

        'If df_pep_valid_to.RawValue Is Nothing Then
        '    Throw New Exception("Valid To is required")
        'End If

        'If txt_pep_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If

        Return True
    End Function

    Function IsDataCustomer_NetworkDevice_Valid() As Boolean
        If txt_CIF.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If
        Return True
    End Function

    Function IsDataCustomer_SocialMedia_Valid() As Boolean
        If txt_CIF.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If
        Return True
    End Function

    Function IsDataCustomer_Sanction_Valid() As Boolean
        If txt_CIF.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        If txt_sanction_provider.Text.Trim = "" Then
            Throw New Exception("Provider is required")
        End If
        If txt_sanction_sanction_list_name.Text.Trim = "" Then
            Throw New Exception("Sanction List Name is required")
        End If
        'If txt_sanction_match_criteria.Text.Trim = "" Then
        '    Throw New Exception("Match Criteria is required")
        'End If
        'If txt_sanction_link_to_source.Text.Trim = "" Then
        '    Throw New Exception("Link To Source is required")
        'End If
        'If txt_sanction_sanction_list_attributes.Text.Trim = "" Then
        '    Throw New Exception("Sanction List Attributes is required")
        'End If
        'If df_sanction_valid_from.RawValue Is Nothing Then
        '    Throw New Exception("Valid From is required")
        'End If
        'If df_sanction_valid_to.RawValue Is Nothing Then
        '    Throw New Exception("Valid To is required")
        'End If
        'If txt_sanction_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If

        Return True
    End Function

    Function IsDataCustomer_RelatedPerson_Valid() As Boolean
        If txt_CIF.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        If cmb_rp_person_relation.SelectedItemText Is Nothing Then
            Throw New Exception("Person Relation is required")
        End If
        'If df_relation_date_range_valid_from.RawValue Is Nothing Then
        '    Throw New Exception("Valid From is required")
        'End If
        'If df_relation_date_range_valid_to.RawValue Is Nothing Then
        '    Throw New Exception("Valid To is required")
        'End If
        'If txt_rp_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If
        'If cmb_rp_gender.SelectedItemText Is Nothing Then
        '    Throw New Exception("Gender is required")
        'End If
        'If txt_rp_title.Text.Trim = "" Then
        '    Throw New Exception("Title is required")
        'End If
        'If txt_rp_first_name.Text.Trim = "" Then
        '    Throw New Exception("First Name is required")
        'End If
        'If txt_rp_middle_name.Text.Trim = "" Then
        '    Throw New Exception("Middle Name is required")
        'End If
        'If txt_rp_prefix.Text.Trim = "" Then
        '    Throw New Exception("Prefix is required")
        'End If
        If txt_rp_last_name.Text.Trim = "" Then
            Throw New Exception("Full Name is required")
        End If
        If GridPhone_RelatedPerson.objListgoAML_Ref Is Nothing Then
            Throw New Exception("Phone is required")
        End If
        If GridPhone_RelatedPerson.objListgoAML_Ref.Count = 0 Then
            Throw New Exception("Phone is required")
        End If
        If GridAddress_RelatedPerson.objListgoAML_Ref Is Nothing Then
            Throw New Exception("Address is required")
        End If
        If GridAddress_RelatedPerson.objListgoAML_Ref.Count = 0 Then
            Throw New Exception("Address is required")
        End If
        'If df_birthdate.RawValue Is Nothing Then
        '    Throw New Exception("Birth Date is required")
        'End If
        'If txt_rp_birth_place.Text.Trim = "" Then
        '    Throw New Exception("Birth Place is required")
        'End If
        'If Not IsFieldValid(txt_rp_birth_place.Text.Trim, "birth_place") Then
        '    Throw New Exception("Birth Place format is not valid")
        'End If
        'If cmb_rp_country_of_birth.SelectedItemText Is Nothing Then
        '    Throw New Exception("Country Of Birth is required")
        'End If
        'If txt_rp_mothers_name.Text.Trim = "" Then
        '    Throw New Exception("Mother's Name is required")
        'End If
        'If Not IsFieldValid(txt_rp_mothers_name.Text.Trim, "mothers_name") Then
        '    Throw New Exception("Mothers Name format is not valid")
        'End If
        'If txt_rp_alias.Text.Trim = "" Then
        '    Throw New Exception("Alias is required")
        'End If
        'If Not IsFieldValid(txt_rp_alias.Text.Trim, "alias") Then
        '    Throw New Exception("Alias format is not valid")
        'End If
        'If txt_rp_full_name_frn.Text.Trim = "" Then
        '    Throw New Exception("Full Name is required")
        'End If
        'If txt_rp_ssn.Text.Trim = "" Then
        '    Throw New Exception("SSN is required")
        'End If
        'If Not IsFieldValid(txt_rp_ssn.Text.Trim, "ssn") Then
        '    Throw New Exception("SSN format is not valid")
        'End If
        'If txt_rp_passport_number.Text.Trim = "" Then
        '    Throw New Exception("Passport Number is required")
        'End If
        'If cmb_rp_passport_country.SelectedItemText Is Nothing Then
        '    Throw New Exception("Paassport Country is required")
        'End If
        'If txt_rp_id_number.Text.Trim = "" Then
        '    Throw New Exception("ID Number is required")
        'End If
        'If cmb_rp_nationality1.SelectedItemText Is Nothing Then
        '    Throw New Exception("Nationality 1 is required")
        'End If
        'If cmb_rp_nationality2.SelectedItemText Is Nothing Then
        '    Throw New Exception("Nationality 2 is required")
        'End If
        'If cmb_rp_nationality3.SelectedItemText Is Nothing Then
        '    Throw New Exception("Nationality 3 is required")
        'End If
        'If cmb_rp_residence.SelectedItemText Is Nothing Then
        '    Throw New Exception("Domicile Country is required")
        'End If
        'If df_rp_residence_since.RawValue Is Nothing Then
        '    Throw New Exception("Residence Since is required")
        'End If
        'If txt_rp_occupation.Text.Trim = "" Then
        '    Throw New Exception("Occupation is required")
        'End If
        'If df_rp_date_deceased.RawValue Is Nothing Then
        '    Throw New Exception("Date Deceased is required")
        'End If
        'If txt_rp_tax_number.Text.Trim = "" Then
        '    Throw New Exception("Tax Number is required")
        'End If
        'If txt_rp_source_of_wealth.Text.Trim = "" Then
        '    Throw New Exception("Source of Wealth is required")
        'End If

        Return True
    End Function

    Function IsDataCustomer_AdditionalInformation_Valid() As Boolean
        If txt_CIF.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If
        Return True
    End Function

    Function IsDataCustomer_EntityIdentification_Valid() As Boolean
        If txt_CIF.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        If cmb_ef_type.SelectedItemText Is Nothing Then
            Throw New Exception("Type is required")
        End If
        If txt_ef_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'If df_issue_date.RawValue Is Nothing Then
        '    Throw New Exception("Issue Date is required")
        'End If
        'If df_expiry_date.RawValue Is Nothing Then
        '    Throw New Exception("Expiry Date is required")
        'End If
        'If txt_ef_issued_by.Text.Trim = "" Then
        '    Throw New Exception("Issued By is required")
        'End If
        If cmb_ef_issue_country.SelectedItemText Is Nothing Then
            Throw New Exception("Issue Country is required")
        End If
        'If txt_ef_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If


        Return True
    End Function

    Function IsDataCustomer_URL_Valid() As Boolean
        If txt_CIF.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        If txt_url.Text.Trim = "" Then
            Throw New Exception("Url is required")
        End If

        Return True
    End Function

    Function IsDataCustomer_RelatedEntities_Valid() As Boolean
        If txt_CIF.Text.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        If cmb_re_entity_relation.SelectedItemText Is Nothing Then
            Throw New Exception("Entity Relation is required")
        End If
        'If df_re_relation_date_range_valid_from.RawValue Is Nothing Then
        '    Throw New Exception("Valid From is required")
        'End If
        'If df_re_relation_date_range_valid_to.RawValue Is Nothing Then
        '    Throw New Exception("Valid To is required")
        'End If
        'If nfd_re_share_percentage.Text.Trim = "" Then
        '    Throw New Exception("Share Percentage is required")
        'End If
        'If txt_re_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If
        'If txt_re_name.Text.Trim = "" Then
        '    Throw New Exception("Name is required")
        'End If
        If txt_re_commercial_name.Text.Trim = "" Then
            Throw New Exception("Commercial Name is required")
        End If
        If GridPhone_RelatedEntity.objListgoAML_Ref Is Nothing Then
            Throw New Exception("Phone is required")
        End If
        If GridPhone_RelatedEntity.objListgoAML_Ref.Count = 0 Then
            Throw New Exception("Phone is required")
        End If
        If GridAddress_RelatedEntity.objListgoAML_Ref Is Nothing Then
            Throw New Exception("Address is required")
        End If
        If GridAddress_RelatedEntity.objListgoAML_Ref.Count = 0 Then
            Throw New Exception("Address is required")
        End If
        'If cmb_re_incorporation_legal_form.SelectedItemText Is Nothing Then
        '    Throw New Exception("Incorporation Legal is required")
        'End If
        'If txt_re_incorporation_number.Text.Trim = "" Then
        '    Throw New Exception("Incorporation Number is required")
        'End If
        'If txt_re_business.Text.Trim = "" Then
        '    Throw New Exception("Business is required")
        'End If
        'If cmb_re_entity_status.SelectedItemText Is Nothing Then
        '    Throw New Exception("Entity Status is required")
        'End If
        'If df_re_entity_status_date.RawValue Is Nothing Then
        '    Throw New Exception("Entity Status Date is required")
        'End If
        'If txt_re_incorporation_state.Text.Trim = "" Then
        '    Throw New Exception("Incorporation State is required")
        'End If
        'If Not IsFieldValid(txt_re_incorporation_state.Text.Trim, "incorporation_state") Then
        '    Throw New Exception("Incorporation State format is not valid")
        'End If
        'If cmb_re_incorporation_country_code.SelectedItemText Is Nothing Then
        '    Throw New Exception("Incorporation Country Code is required")
        'End If
        'If df_re_incorporation_date.RawValue Is Nothing Then
        '    Throw New Exception("Incorporation Date is required")
        'End If
        'If cbx_re_business_closed.Checked And df_re_date_business_closed.RawValue Is Nothing Then
        '    Throw New Exception("Business Closed is required")
        'End If
        'If txt_re_tax_number.Text.Trim = "" Then
        '    Throw New Exception("Tax Number is required")
        'End If
        'If txt_re_tax_reg_number.Text.Trim = "" Then
        '    Throw New Exception("Tax Reg Number is required")
        'End If

        Return True
    End Function

    Private Function IsEmailChange() As Boolean
        Dim oldobjListgoAML_Ref_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(obj_Customer.CIF)

        If oldobjListgoAML_Ref_Email.Count <> objListgoAML_Ref_Email.Count Then
            Return True
        End If

        For Each item As DataModel.goAML_Ref_Customer_Email In objListgoAML_Ref_Email
            Dim objEmail = oldobjListgoAML_Ref_Email.Where(Function(x) x.PK_goAML_Ref_Customer_Email_ID = item.PK_goAML_Ref_Customer_Email_ID).FirstOrDefault
            If objEmail IsNot Nothing Then
                If item.EMAIL_ADDRESS <> objEmail.EMAIL_ADDRESS Then
                    Return True
                End If
            Else
                Return True
            End If
        Next

        Return False
    End Function

    Private Function IsPersonPEPChange() As Boolean
        Dim oldobjListgoAML_Ref_PersonPEP = GoAMLBLL.goAML_CustomerBLL.GetPEP(obj_Customer.CIF)

        If oldobjListgoAML_Ref_PersonPEP.Count <> objListgoAML_Ref_PersonPEP.Count Then
            Return True
        End If

        For Each item As DataModel.goAML_Ref_Customer_PEP In objListgoAML_Ref_PersonPEP
            Dim objPEP = oldobjListgoAML_Ref_PersonPEP.Where(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = item.PK_goAML_Ref_Customer_PEP_ID).FirstOrDefault
            If objPEP IsNot Nothing Then
                If item.PEP_COUNTRY <> objPEP.PEP_COUNTRY Then
                    Return True
                End If
                If item.FUNCTION_NAME <> objPEP.FUNCTION_NAME Then
                    Return True
                End If
                If item.FUNCTION_DESCRIPTION <> objPEP.FUNCTION_DESCRIPTION Then
                    Return True
                End If
                If item.PEP_DATE_RANGE_VALID_FROM <> objPEP.PEP_DATE_RANGE_VALID_FROM Then
                    Return True
                End If
                If item.PEP_DATE_RANGE_IS_APPROX_FROM_DATE <> objPEP.PEP_DATE_RANGE_IS_APPROX_FROM_DATE Then
                    Return True
                End If
                If item.PEP_DATE_RANGE_VALID_TO <> objPEP.PEP_DATE_RANGE_VALID_TO Then
                    Return True
                End If
                If item.PEP_DATE_RANGE_IS_APPROX_TO_DATE <> objPEP.PEP_DATE_RANGE_IS_APPROX_TO_DATE Then
                    Return True
                End If
            Else
                Return True
            End If
        Next

        Return False
    End Function

    Private Function IsSanctionChange() As Boolean
        Dim oldobjListgoAML_Ref_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(obj_Customer.CIF)

        If oldobjListgoAML_Ref_Sanction.Count <> objListgoAML_Ref_Sanction.Count Then
            Return True
        End If

        For Each item As DataModel.goAML_Ref_Customer_Sanction In objListgoAML_Ref_Sanction
            Dim objSanction = oldobjListgoAML_Ref_Sanction.Where(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = item.PK_goAML_Ref_Customer_Sanction_ID).FirstOrDefault
            If objSanction IsNot Nothing Then
                If item.PROVIDER <> objSanction.PROVIDER Then
                    Return True
                End If
                If item.SANCTION_LIST_NAME <> objSanction.SANCTION_LIST_NAME Then
                    Return True
                End If
                If item.MATCH_CRITERIA <> objSanction.MATCH_CRITERIA Then
                    Return True
                End If
                If item.LINK_TO_SOURCE <> objSanction.LINK_TO_SOURCE Then
                    Return True
                End If
                If item.SANCTION_LIST_ATTRIBUTES <> objSanction.SANCTION_LIST_ATTRIBUTES Then
                    Return True
                End If
                If item.SANCTION_LIST_DATE_RANGE_VALID_FROM <> objSanction.SANCTION_LIST_DATE_RANGE_VALID_FROM Then
                    Return True
                End If
                If item.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE <> objSanction.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE Then
                    Return True
                End If
                If item.SANCTION_LIST_DATE_RANGE_VALID_TO <> objSanction.SANCTION_LIST_DATE_RANGE_VALID_TO Then
                    Return True
                End If
                If item.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE <> objSanction.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE Then
                    Return True
                End If
            Else
                Return True
            End If
        Next

        Return False
    End Function

    Private Function IsRelatedPersonChange() As Boolean
        Dim oldobjListgoAML_Ref_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(obj_Customer.CIF)

        If oldobjListgoAML_Ref_RelatedPerson.Count <> objListgoAML_Ref_RelatedPerson.Count Then
            Return True
        End If

        For Each item As DataModel.goAML_Ref_Customer_Related_Person In objListgoAML_Ref_RelatedPerson
            Dim objRelatedPerson = oldobjListgoAML_Ref_RelatedPerson.Where(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = item.PK_goAML_Ref_Customer_Related_Person_ID).FirstOrDefault
            If objRelatedPerson IsNot Nothing Then
                If item.PERSON_PERSON_RELATION <> objRelatedPerson.PERSON_PERSON_RELATION Then
                    Return True
                End If
                If item.RELATION_DATE_RANGE_VALID_FROM <> objRelatedPerson.RELATION_DATE_RANGE_VALID_FROM Then
                    Return True
                End If
                If item.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE <> objRelatedPerson.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE Then
                    Return True
                End If
                If item.RELATION_DATE_RANGE_VALID_TO <> objRelatedPerson.RELATION_DATE_RANGE_VALID_TO Then
                    Return True
                End If
                If item.RELATION_DATE_RANGE_IS_APPROX_TO_DATE <> objRelatedPerson.RELATION_DATE_RANGE_IS_APPROX_TO_DATE Then
                    Return True
                End If
                If item.COMMENTS <> objRelatedPerson.COMMENTS Then
                    Return True
                End If
                If item.GENDER <> objRelatedPerson.GENDER Then
                    Return True
                End If
                If item.TITLE <> objRelatedPerson.TITLE Then
                    Return True
                End If
                If item.FIRST_NAME <> objRelatedPerson.FIRST_NAME Then
                    Return True
                End If
                If item.MIDDLE_NAME <> objRelatedPerson.MIDDLE_NAME Then
                    Return True
                End If
                If item.PREFIX <> objRelatedPerson.PREFIX Then
                    Return True
                End If
                If item.LAST_NAME <> objRelatedPerson.LAST_NAME Then
                    Return True
                End If
                If item.BIRTHDATE <> objRelatedPerson.BIRTHDATE Then
                    Return True
                End If
                If item.BIRTH_PLACE <> objRelatedPerson.BIRTH_PLACE Then
                    Return True
                End If
                If item.COUNTRY_OF_BIRTH <> objRelatedPerson.COUNTRY_OF_BIRTH Then
                    Return True
                End If
                If item.MOTHERS_NAME <> objRelatedPerson.MOTHERS_NAME Then
                    Return True
                End If
                If item.ALIAS <> objRelatedPerson.ALIAS Then
                    Return True
                End If
                If item.FULL_NAME_FRN <> objRelatedPerson.FULL_NAME_FRN Then
                    Return True
                End If
                If item.SSN <> objRelatedPerson.SSN Then
                    Return True
                End If
                If item.PASSPORT_NUMBER <> objRelatedPerson.PASSPORT_NUMBER Then
                    Return True
                End If
                If item.PASSPORT_COUNTRY <> objRelatedPerson.PASSPORT_COUNTRY Then
                    Return True
                End If
                If item.ID_NUMBER <> objRelatedPerson.ID_NUMBER Then
                    Return True
                End If
                If item.NATIONALITY1 <> objRelatedPerson.NATIONALITY1 Then
                    Return True
                End If
                If item.NATIONALITY2 <> objRelatedPerson.NATIONALITY2 Then
                    Return True
                End If
                If item.NATIONALITY3 <> objRelatedPerson.NATIONALITY3 Then
                    Return True
                End If
                If item.RESIDENCE <> objRelatedPerson.RESIDENCE Then
                    Return True
                End If
                If item.RESIDENCE_SINCE <> objRelatedPerson.RESIDENCE_SINCE Then
                    Return True
                End If
                If item.OCCUPATION <> objRelatedPerson.OCCUPATION Then
                    Return True
                End If
                If item.DECEASED <> objRelatedPerson.DECEASED Then
                    Return True
                End If
                If item.DATE_DECEASED <> objRelatedPerson.DATE_DECEASED Then
                    Return True
                End If
                If item.TAX_NUMBER <> objRelatedPerson.TAX_NUMBER Then
                    Return True
                End If
                If item.TAX_REG_NUMBER <> objRelatedPerson.TAX_REG_NUMBER Then
                    Return True
                End If
                If item.SOURCE_OF_WEALTH <> objRelatedPerson.SOURCE_OF_WEALTH Then
                    Return True
                End If
                If item.IS_PROTECTED <> objRelatedPerson.IS_PROTECTED Then
                    Return True
                End If

            Else
                Return True
            End If
        Next

        If GridAddress_RelatedPerson.IsDataChange Then
            Return True
        End If

        If GridAddressWork_RelatedPerson.IsDataChange Then
            Return True
        End If

        If GridPhone_RelatedPerson.IsDataChange Then
            Return True
        End If

        If GridPhoneWork_RelatedPerson.IsDataChange Then
            Return True
        End If

        If GridIdentification_RelatedPerson.IsDataChange Then
            Return True
        End If

        If GridEmail_RelatedPerson.IsDataChange Then
            Return True
        End If

        ' = 2023-10-03, Nael: Menambahkan pengecekan untuk GridPEP dan GridSanction =
        If GridPEP_RelatedPerson.IsDataChange Then
            Return True
        End If

        If GridSanction_RelatedPerson.IsDataChange Then
            Return True
        End If


        ' ========================================================================

        Return False
    End Function

    Private Function IsUrlChange() As Boolean
        Dim oldobjListgoAML_Ref_Url = GoAMLBLL.goAML_CustomerBLL.GetURL(obj_Customer.CIF)

        If oldobjListgoAML_Ref_Url.Count <> objListgoAML_Ref_EntityUrl.Count Then
            Return True
        End If

        For Each item As DataModel.goAML_Ref_Customer_URL In objListgoAML_Ref_EntityUrl
            Dim objUrl = oldobjListgoAML_Ref_Url.Where(Function(x) x.PK_goAML_Ref_Customer_URL_ID = item.PK_goAML_Ref_Customer_URL_ID).FirstOrDefault
            If objUrl IsNot Nothing Then
                If item.URL <> objUrl.URL Then
                    Return True
                End If
            Else
                Return True
            End If
        Next

        Return False
    End Function

    Private Function IsEntityIdentificationChange() As Boolean
        Dim oldobjListgoAML_Ref_EntityIdentification = GoAMLBLL.goAML_CustomerBLL.GetEntityIdentification(obj_Customer.CIF)

        If oldobjListgoAML_Ref_EntityIdentification.Count <> objListgoAML_Ref_EntityIdentification.Count Then
            Return True
        End If

        For Each item As DataModel.goAML_Ref_Customer_Entity_Identification In objListgoAML_Ref_EntityIdentification
            Dim obj = oldobjListgoAML_Ref_EntityIdentification.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = item.PK_goAML_Ref_Customer_Entity_Identification_ID).FirstOrDefault
            If obj IsNot Nothing Then
                If item.TYPE <> obj.TYPE Then
                    Return True
                End If
                If item.NUMBER <> obj.NUMBER Then
                    Return True
                End If
                If item.ISSUE_DATE <> obj.ISSUE_DATE Then
                    Return True
                End If
                If item.EXPIRY_DATE <> obj.EXPIRY_DATE Then
                    Return True
                End If
                If item.ISSUED_BY <> obj.ISSUED_BY Then
                    Return True
                End If
                If item.ISSUE_COUNTRY <> obj.ISSUE_COUNTRY Then
                    Return True
                End If
                If item.COMMENTS <> obj.COMMENTS Then
                    Return True
                End If
            Else
                Return True
            End If
        Next

        Return False
    End Function

    Private Function IsRelatedEntitiesChange() As Boolean
        Dim oldobjListgoAML_Ref_RelatedEntities = GoAMLBLL.goAML_CustomerBLL.GetRelatedEntities(obj_Customer.CIF)

        If oldobjListgoAML_Ref_RelatedEntities.Count <> objListgoAML_Ref_RelatedEntities.Count Then
            Return True
        End If

        For Each item As DataModel.goAML_Ref_Customer_Related_Entities In objListgoAML_Ref_RelatedEntities
            Dim obj = oldobjListgoAML_Ref_RelatedEntities.Where(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = item.PK_goAML_Ref_Customer_Related_Entities_ID).FirstOrDefault
            If obj IsNot Nothing Then
                If item.ENTITY_ENTITY_RELATION <> obj.ENTITY_ENTITY_RELATION Then
                    Return True
                End If
                If item.RELATION_DATE_RANGE_VALID_FROM <> obj.RELATION_DATE_RANGE_VALID_FROM Then
                    Return True
                End If
                If item.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE <> obj.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE Then
                    Return True
                End If
                If item.RELATION_DATE_RANGE_VALID_TO <> obj.RELATION_DATE_RANGE_VALID_TO Then
                    Return True
                End If
                If item.RELATION_DATE_RANGE_IS_APPROX_TO_DATE <> obj.RELATION_DATE_RANGE_IS_APPROX_TO_DATE Then
                    Return True
                End If
                If item.SHARE_PERCENTAGE <> obj.SHARE_PERCENTAGE Then
                    Return True
                End If
                If item.COMMENTS <> obj.COMMENTS Then
                    Return True
                End If
                If item.NAME <> obj.NAME Then
                    Return True
                End If
                If item.COMMERCIAL_NAME <> obj.COMMERCIAL_NAME Then
                    Return True
                End If
                If item.INCORPORATION_LEGAL_FORM <> obj.INCORPORATION_LEGAL_FORM Then
                    Return True
                End If
                If item.INCORPORATION_NUMBER <> obj.INCORPORATION_NUMBER Then
                    Return True
                End If
                If item.BUSINESS <> obj.BUSINESS Then
                    Return True
                End If
                If item.ENTITY_STATUS <> obj.ENTITY_STATUS Then
                    Return True
                End If
                If item.ENTITY_STATUS_DATE <> obj.ENTITY_STATUS_DATE Then
                    Return True
                End If
                If item.INCORPORATION_STATE <> obj.INCORPORATION_STATE Then
                    Return True
                End If
                If item.INCORPORATION_COUNTRY_CODE <> obj.INCORPORATION_COUNTRY_CODE Then
                    Return True
                End If
                If item.INCORPORATION_DATE <> obj.INCORPORATION_DATE Then
                    Return True
                End If
                If item.BUSINESS_CLOSED <> obj.BUSINESS_CLOSED Then
                    Return True
                End If
                If item.DATE_BUSINESS_CLOSED <> obj.DATE_BUSINESS_CLOSED Then
                    Return True
                End If
                If item.TAX_NUMBER <> obj.TAX_NUMBER Then
                    Return True
                End If
                If item.TAX_REG_NUMBER <> obj.TAX_REG_NUMBER Then
                    Return True
                End If

            Else
                Return True
            End If
        Next

        If GridPhone_RelatedEntity.IsDataChange Then
            Return True
        End If

        If GridAddress_RelatedEntity.IsDataChange Then
            Return True
        End If

        If GridEmail_RelatedEntity.IsDataChange Then
            Return True
        End If

        If GridEntityIdentification_RelatedEntity.IsDataChange Then
            Return True
        End If

        If GridURL_RelatedEntity.IsDataChange Then
            Return True
        End If

        If GridSanction_RelatedEntity.IsDataChange Then
            Return True
        End If

        Return False
    End Function

    Private Function IsDirectorChange() As Boolean
        If GridEmail_Director.IsDataChange Then
            Return True
        End If

        If GridSanction_Director.IsDataChange Then
            Return True
        End If

        If GridPEP_Director.IsDataChange Then
            Return True
        End If

        Return False
    End Function

    Sub ClearinputCustomer_Email()
        txtCustomer_email_address.Text = ""

    End Sub

    Sub ClearinputCustomer_PersonPEP()
        cmb_PersonPEP_Country.SetTextValue("")
        txt_function_name.Text = ""
        txt_function_description.Text = ""
        df_person_pep_valid_from.Text = ""
        cbx_pep_is_approx_from_date.Checked = False
        df_pep_valid_to.Text = ""
        cbx_pep_is_approx_to_date.Checked = False
        txt_pep_comments.Text = ""

    End Sub

    Sub ClearinputCustomer_Sanction()
        txt_sanction_provider.Text = ""
        txt_sanction_sanction_list_name.Text = ""
        txt_sanction_match_criteria.Text = ""
        txt_sanction_link_to_source.Text = ""
        txt_sanction_sanction_list_attributes.Text = ""
        df_sanction_valid_from.Text = ""
        cbx_sanction_is_approx_from_date.Checked = False
        df_sanction_valid_to.Text = ""
        cbx_sanction_is_approx_to_date.Checked = False
        txt_sanction_comments.Text = ""

    End Sub

    Sub ClearinputCustomer_RelatedPerson()
        cmb_rp_person_relation.SetTextValue("")
        'df_relation_date_range_valid_from.Text = ""
        'cbx_relation_date_range_is_approx_from_date.Checked = False
        'df_relation_date_range_valid_to.Text = ""
        'cbx_relation_date_range_is_approx_to_date.Checked = False
        txt_rp_comments.Text = ""
        txt_rp_relation_comments.Text = ""
        cmb_rp_gender.SetTextValue("")
        txt_rp_title.Text = ""
        'txt_rp_first_name.Text = ""
        'txt_rp_middle_name.Text = ""
        'txt_rp_prefix.Text = ""
        txt_rp_last_name.Text = ""
        df_birthdate.Text = ""
        txt_rp_birth_place.Text = ""
        'cmb_rp_country_of_birth.SetTextValue("")
        txt_rp_mothers_name.Text = ""
        txt_rp_alias.Text = ""
        'txt_rp_full_name_frn.Text = ""
        txt_rp_ssn.Text = ""
        txt_rp_passport_number.Text = ""
        cmb_rp_passport_country.SetTextValue("")
        txt_rp_id_number.Text = ""
        cmb_rp_nationality1.SetTextValue("")
        cmb_rp_nationality2.SetTextValue("")
        cmb_rp_nationality3.SetTextValue("")
        cmb_rp_residence.SetTextValue("")
        'df_rp_residence_since.Text = ""
        txt_rp_occupation.Text = ""
        txt_rp_employer_name.Text = "" ' 2023-10-17, Nael
        cbx_rp_deceased.Checked = False
        df_rp_date_deceased.Text = ""
        txt_rp_tax_number.Text = ""
        cbx_rp_tax_reg_number.Checked = False
        txt_rp_source_of_wealth.Text = ""
        cbx_rp_is_protected.Checked = False

    End Sub

    Sub ClearinputCustomer_Url()
        txt_url.Text = ""


    End Sub

    Sub ClearinputCustomer_RelatedEntities()
        cmb_re_entity_relation.SetTextValue("")
        'df_re_relation_date_range_valid_from.Text = ""
        'cbx_re_relation_date_range_is_approx_from_date.Checked = False
        'df_re_relation_date_range_valid_to.Text = ""
        'cbx_re_relation_date_range_is_approx_to_date.Checked = False
        'nfd_re_share_percentage.Text = ""
        txt_re_comments.Text = ""
        txt_re_relation_comments.Text = ""
        txt_re_name.Text = ""
        txt_re_commercial_name.Text = ""
        cmb_re_incorporation_legal_form.SetTextValue("")
        txt_re_incorporation_number.Text = ""
        txt_re_business.Text = ""
        'cmb_re_entity_status.SetTextValue("")
        'df_re_entity_status_date.Text = ""
        txt_re_incorporation_state.Text = ""
        cmb_re_incorporation_country_code.SetTextValue("")
        df_re_incorporation_date.Text = ""
        cbx_re_business_closed.Checked = False
        df_re_date_business_closed.Text = ""
        txt_re_tax_number.Text = ""
        txt_re_tax_reg_number.Text = ""

    End Sub

    Sub ClearinputCustomer_EntityIdentification()
        cmb_ef_type.SetTextValue("")
        txt_ef_number.Text = ""
        df_issue_date.Text = ""
        df_expiry_date.Text = ""
        txt_ef_issued_by.Text = ""
        cmb_ef_issue_country.SetTextValue("")
        txt_ef_comments.Text = ""

    End Sub
    Private Function IsFieldValid(text As String, fieldName As String) As Boolean
        Try
            Dim regex As New Regex("^[0-9 ]+$")

            Select Case fieldName
                Case "incorporation_state"
                    regex = New Regex("[ a-zA-Z]*")
                Case "tax_number"
                    regex = New Regex("[0-9]{15}")
                Case "last_name"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "birth_place"
                    regex = New Regex("[ a-zA-Z]*")
                Case "mothers_name"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "alias"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "ssn"
                    regex = New Regex("[0-9]{22}")
                Case "city"
                    regex = New Regex("[ a-zA-Z]*")
                Case "state"
                    regex = New Regex("[ a-zA-Z]*")
                Case "email_address"
                    regex = New Regex("[_a-zA-Z0-9-+]+(\.[_a-zA-Z0-9-\+]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})")
                Case "ipv4_address_type"
                    regex = New Regex("((1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])")
                Case "ipv6_address_type"
                    regex = New Regex("([A-Fa-f0-9]{1,4}:){7}[A-Fa-f0-9]{1,4}")
                Case Else
                    Return True
            End Select

            Return regex.IsMatch(text)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub CUSTOMER_EDIT_v501_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

    End Sub
#End Region
End Class
